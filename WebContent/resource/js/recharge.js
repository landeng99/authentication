﻿var money, moneyIndex = 0, withMoney, widthMoneyIndex = 0, card, cardIndex = 0, trueName, trueNameIndex = 0, bank, bankIndex = 0, modifyProvince, modifyProvinceIndex = 0, modifyCity, modifyCityIndex = 0, qzoneBank, qzoneBankIndex, remark, remarkIndex = 0;
var layerIndex = 0;
$(function () {
    $("ul.List06 input[type='text']").css("padding-left", "5px");
    money = $("#chargeMoney");
    withMoney = $("#withMoney");
    card = $("#card");
    trueName = $("#trueName");
    bank = $("#bank");
    modifyProvince = $("#modifyProvince");
    modifyCity = $("#modifyCity");
    qzoneBank = $("#qzoneBank");

    money.focus();
    LoadAddressOption(modifyProvince, 1, 6, 0); //加载省市信息

    SumAmount();

    money.on("focus keyup blur", function () {
        CheckMoney(money);
    });
    $("input[type='radio'][name='payStyle']").click(function () {
        if (this.id == "payPal") {
            $("li.payPal").show();
            $("input[type='radio'][name='payPal']").attr("checked", false);
        } else {
            $("li.payPal").hide();
        }
    });
    $(".control").click(function () {
        if (moneyIndex > 0 || widthMoneyIndex > 0 || widthMoneyIndex > 0 || cardIndex > 0 || trueNameIndex > 0 || bankIndex > 0 || modifyProvinceIndex > 0 || modifyCityIndex > 0 || qzoneBankIndex > 0) {
            $.BDEX.Close();
        }
        $("#resetCharge").click();
        $("#resetWith").click();
        $(".control").removeClass("current");
        $(this).addClass("current").attr("checked", true);
        var item = $(this).attr("item"); //当前操作的内容
        $("input[type='radio'][name='payPal']").attr("checked", false);
        if (item == "recharge") {
            $("#showType").html("充值<span style='font-size:12px;margin-left:5px;'>[信用卡充值的帐户余额不可提现]</span>");
            $("#payPal").attr("checked", true);
            $("#recharge").show();
            $("#withdrawal").hide();
            money.focus();
        } else {
            $("#showType").html("提现<span style='font-size:12px;margin-left:5px;'>[信用卡充值的帐户余额不可提现]</span>");
            $("#recharge").hide();
            $("#withdrawal").show();
            $("#chargeMoney").val("");
            withMoney.focus();
        }
    });
    $("input[type='radio'][name='payPal']").click(function () {
        if (this.checked) {
            $("#payPal").attr("checked", true);
        }
    });
    //=============充值提交================
    $("#submitCharge").click(function () {
        var payStyle = "";
        var bankStyle = "";
        if (!CheckMoney(money)) {
            return false;
        }
        var payStyle = $("input[type='radio'][name='payStyle']:checked").val();
        if ($.BDEX.CheckNullOrEmpty(payStyle)) {
            //alert("请选择充值方式！！！");
            return false;
        }
        if ($("input[type='radio'][name='payStyle']:checked")[0].id == "payPal") {//判断是否选择子银行
            if ($("input[type='radio'][name='payPal']:checked").length > 0) {
                bankStyle = $("input[type='radio'][name='payPal']:checked").val();
            }
        }
        var completePayUrl = PayUrl + "GoToBank.aspx?a=in&m=" + escape(money.val()) + "&b=0&p=" + payStyle + "&bankname=" + bankStyle;

        $(this).attr("href", completePayUrl);
        $("#payError").attr("href", completePayUrl);
        layerIndex = $.layer({
            closeBtn: false,
            type: 1,
            title: false,
            shade: [0.5, '#ccc', true],
            border: [1, 0.3, '#666', true],
            area: ['680px', '160px'],
            page: { dom: '#Success' }
        });

        return true;
    });

    $("#Success .close").click(function () {
        $(this).css("background-color", "#fff");
        layer.close(layerIndex)
    });

    //===================提现提交=====================
    $("#submitWith").click(function () {
        //if (CheckWithMoney(withMoney) && CheckCard(card) && CheckTrueName(trueName) && CheckBank(bank) && CheckBankProvince(modifyProvince) && CheckBankCity(modifyCity) && CheckQzoneBank(qzoneBank)) {
        if (CheckWithMoney(withMoney) && CheckTrueName(trueName) && CheckCard(card)) {
            Insert();
        }
        return false;
    });
    //=======充值重置=========
    $("#resetCharge").click(function () {
        $("#chargeMoney").val("");
        $("input[type='radio'][name='payPal']").attr("checked", false);
        if (moneyIndex > 0 || widthMoneyIndex > 0 || widthMoneyIndex > 0 || cardIndex > 0 || trueNameIndex > 0 || bankIndex > 0 || modifyProvinceIndex > 0 || modifyCityIndex > 0 || qzoneBankIndex > 0) {
            $.BDEX.Close();
        }
    });
    //==============提款重置==============
    $("#resetWith").click(function () {
        $("#withdrawal input[type='text']").val("");
        $("#withdrawal select").val("-1");
        if (moneyIndex > 0 || widthMoneyIndex > 0 || widthMoneyIndex > 0 || cardIndex > 0 || trueNameIndex > 0 || bankIndex > 0 || modifyProvinceIndex > 0 || modifyCityIndex > 0 || qzoneBankIndex > 0) {
            $.BDEX.Close();
        }
    });
    //===============充值成功===============
    $("#paySuccess").click(function () {
        parent.location.href = "/record.html";
    });
    //================重新提交充值===========
    $("#payError").click(function () {
        return true;
    });

    withMoney.on("focus keyup blur", function () {
        CheckWithMoney(withMoney);
    });
    card.on("focus keyup blur", function () {
        CheckCard(card);
    });
    trueName.on("focus keyup blur", function () {
        CheckTrueName(trueName);
    });
    bank.on("focus keyup blur", function () {
        CheckBank(bank);
    });
    modifyProvince.on("focus keyup blur", function () {
        CheckBankProvince(modifyProvince);
    });
    modifyCity.on("focus keyup blur", function () {
        CheckBankCity(modifyCity);
    });
    qzoneBank.on("focus keyup blur", function () {
        CheckQzoneBank(qzoneBank);
    });

    $("#resetCharge,#resetWith").css("background-image", "url(" + CDNImgServerUrl + "/images/input37.png);");

    var moneyVal = $.BDEX.QueryValByKey("m");
    if (moneyVal != undefined && moneyVal != null && !isNaN(parseFloat(moneyVal))) {
        money.val(parseInt(moneyVal));
    }
});
//=================提款申请数据提交===================
function Insert() {
    //加载数据是否成功
    function onResponseSucess(result) {
        if (result.BoolOut) {
            $.BDEX.Success("申请成功！系统或客服将尽快为您处理提现！！");
            setTimeout("parent.location.href='/record.html'", 1500);
        } else {
            $.BDEX.Error(result.ErrMsg);
        }
    }
    function sparam(param) {
        param.Amount = withMoney.val().trim();
        //param.BankName = bank.val().trim() + "  " + $('#modifyProvince option:selected').text() + "  " + $('#modifyCity option:selected').text() + "  " + qzoneBank.val().trim();
        param.BankName = "";
        param.BankTrueName = trueName.val().trim();
        param.BankNo = card.val().trim();
        param.Reason = $("#remark").val().trim();
        return param;
    }

    //初始化数据
    var param = {
        AssemblyName: "Beyond.MemberCore.Business.dll",
        ClassName: "Beyond.MemberCore.Business.Men_MoneyApplyBLL",
        MethodName: "AddTakeCashTable",
        ParamModelName: "Beyond.MemberCore.Entity.DBEntity.Men_MoneyApply",
        ParamAssemblyName: "Beyond.MemberCore.Entity.dll",
        onResponse: onResponseSucess,
        onRequest: sparam
    }
    $.ajaxRequest(param);
}
//=================获取用户消费统计额度================
function SumAmount() {
    //加载数据是否成功
    function OnLoadSucess(results) {
        $("#TotalSpendi").html(results);
    };
    function sparam(param) {
        return 1;
    };
    var param = {
        AssemblyName: "Beyond.MemberCore.Business.dll",
        ClassName: "Beyond.MemberCore.Business.Mem_UserWalletTransactionBLL",
        MethodName: "TotalSpending",
        ParamModelName: "System.Object",
        onResponse: OnLoadSucess,
        onRequest: sparam
    };
    $.ajaxRequest(param);
};
//===========检测金额填写===========
function CheckMoney(obj) {
    var moneyVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(moneyVal)) {
        moneyIndex = $.BDEX.Tips('请填写充值金额！', obj[0], '110');
        return false;
    }
    if (!RegRule.Currency.test(moneyVal)) {
        moneyIndex = $.BDEX.Tips('请正确填写充值金额', obj[0], '110');
        return false;
    }
    if (parseFloat(moneyVal) < 0.01) {
        moneyIndex = $.BDEX.Tips('充值金额不能小于0.01元！', obj[0], '110');
        return false;
    }
    $.BDEX.Close(moneyIndex);
    moneyIndex = 0;
    return true;
};
//===========检查提现金额===========
function CheckWithMoney(obj) {
    var withMoneyVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(withMoneyVal)) {
        widthMoneyIndex = $.BDEX.Tips('请填写提现金额！', obj[0], '110');
        return false;
    }
    if (!RegRule.Currency.test(withMoneyVal)) {
        widthMoneyIndex = $.BDEX.Tips('请正确填写提现金额', obj[0], '110');
        return false;
    }
    if (parseFloat(withMoneyVal) > parseFloat(backMoney)) {
        widthMoneyIndex = $.BDEX.Tips('提现金额不能大于可退金额！', obj[0], '110');
        return false;
    }
    if (parseFloat(withMoneyVal) < 0.01) {
        widthMoneyIndex = $.BDEX.Tips('提现金额不能小于0.01元！', obj[0], '110');
        return false;
    }
    $.BDEX.Close(widthMoneyIndex);
    widthMoneyIndex = 0;
    return true;
};
//===========检查卡号===========
function CheckCard(obj) {
    var cardVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(cardVal)) {
        cardIndex = $.BDEX.Tips('请填写提现支付宝账号', obj[0], '50');
        return false;
    }
    $.BDEX.Close(cardIndex);
    cardIndex = 0;
    return true;
};
//===========检查姓名===========
function CheckTrueName(obj) {
    var trueNameVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(trueNameVal)) {
        trueNameIndex = $.BDEX.Tips('请填写客户姓名', obj[0], '110');
        return false;
    }
    $.BDEX.Close(trueNameIndex);
    trueNameIndex = 0;
    return true;
};
//===========检查开户行===========
function CheckBank(obj) {
    var bankVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(bankVal)) {
        bankIndex = $.BDEX.Tips('请填写提现开户行！', obj[0], '110');
        return false;
    }
    $.BDEX.Close(bankIndex);
    bankIndex = 0;
    return true;
};
//===========检查开户省份===========
function CheckBankProvince(obj) {
    var bankProvinceVal = obj.val().trim();
    if (bankProvinceVal == "-1") {
        modifyProvinceIndex = $.BDEX.Tips('请选择开户省市！', obj[0], 0, 1);
        return false;
    }
    $.BDEX.Close(modifyProvinceIndex);
    modifyProvinceIndex = 0;
    return true;
};
//===========检查开户城市===========
function CheckBankCity(obj) {
    var bankProvinceVal = obj.val().trim();
    if (bankProvinceVal == "-1") {
        modifyCityIndex = $.BDEX.Tips('请选择开户城市！', obj[0], 0, 1);
        return false;
    }
    $.BDEX.Close(modifyCityIndex);
    modifyCityIndex = 0;
    return true;
};
//===========检查支行名称===========
function CheckQzoneBank(obj) {
    var qzoneBankVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(qzoneBankVal)) {
        qzoneBankIndex = $.BDEX.Tips('请填写支行名称！', obj[0], '110');
        return false;
    }
    $.BDEX.Close(qzoneBankIndex);
    qzoneBankIndex = 0;
    return true;
};