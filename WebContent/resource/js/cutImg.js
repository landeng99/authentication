﻿$(document).ready(function () {
    $("#showIDCard,#showIDCardEx").click(function () {
        var imgPath = $(this).attr("imgPath");
        var imgUrl = $(this).attr("imgUrl");
        var cutImgUrl = $(this).attr("src");
        if (imgUrl == "" || imgUrl == undefined)
            return;

        var x = $(this).attr("x");
        var y = $(this).attr("y");
        var x2 = $(this).attr("x2");
        var y2 = $(this).attr("y2");

        ShowCutImg(this.id, imgPath, imgUrl, cutImgUrl, x, y, x2, y2);
    });
});

function SetCutImg(cutImgUrl, ID, imgPath, imgUrl, cutImgPath, cutImgWaterPath, x, y, x2, y2) {
    $("#" + ID).attr("src", cutImgUrl + "?" + new Date().getTime());
    $("#" + ID).attr("imgPath", imgPath);
    $("#" + ID).attr("imgUrl", imgUrl);

    $("#" + ID).attr("x", x);
    $("#" + ID).attr("y", y);
    $("#" + ID).attr("x2", x2);
    $("#" + ID).attr("y2", y2);

    if (ID == "showIDCard") {
        $('#saveOriginalIdentity').val(cutImgPath);
        $('#saveIdentity').val(cutImgWaterPath);
    }
    else if (ID == "showIDCardEx") {
        $('#saveOriginalIdentityEx').val(cutImgPath);
        $('#saveIdentityEx').val(cutImgWaterPath);
    }
}

function ShowCutImg(ID, imgPath, imgUrl, cutImgUrl, x, y, x2, y2) {
    var off = ($(window).height() - 760) / 2;
    $.layer({
        bgcolor: '#fff',
        type: 2,
        title: false,
        shadeClose: true,
        fix: false,
        shade: [0.5, '#ccc', true],
        border: [1, 0.3, '#666', true],
        offset: [off, ''],
        area: ['820px', '500px'],
        closeBtn: [0, false], //显示关闭按钮
        iframe: { src: '/UserBase/cutImg.aspx?ID=' + ID + '&imgPath=' + imgPath + "&imgUrl=" + imgUrl + "&cutImgUrl=" + cutImgUrl + "&x=" + x + "&y=" + y + "&x2=" + x2 + "&y2=" + y2 },
        success: function () {

        },
        end: function () {
        }
    });
}