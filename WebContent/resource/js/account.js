﻿var money = 0;
var item = 1;
$(function () {
    GetCouponItem(item, 0, true);

    $(".couponli").click(function () {
        $(".couponli").removeClass("current");
        $(this).addClass("current");
        item = $(this).attr("item");
        GetCouponItem(item, 0, true);
    });
});
//=================获取优惠券的显示信息=====================
function GetCouponItem(showType,pageIndex, type) {
    //-----------------数据遮罩层，表示数据正在加载中----------------------
    $.BDEX.Wait();
    //加载数据是否成功
    function OnLoadSucess(results) {
        var BodyHTML = "<tr><th class=\"span2\">来源</th><th class=\"span2\">面额</th><th class=\"span2\">数量</th>";

        if (showType == 1) {
            BodyHTML += "<th style='width:25%; float:left; overflow:hidden;'>是否可以叠加使用</th>";
            BodyHTML += "<th class=\"span3\">有效期</th></tr>";
        } else if (showType == 2) {
            BodyHTML += "<th style='width:25%; float:left; overflow:hidden;'>订单号</th>";
            BodyHTML += "<th class=\"span3\">使用日期</th></tr>";
        } else if (showType == 3) {
            BodyHTML += "<th style='width:25%; float:left; overflow:hidden;'>状态</th>";
            BodyHTML += "<th class=\"span3\">有效期</th></tr>";
        }
        
        //var BodyHTML = "<tr><th class=\"span2\">来源</th><th class=\"span2\">面额</th><th class=\"span2\">数量</th><th class=\"span3\">有效期</th></tr>";
        if (results != undefined && results.length > 0) {
            $(results).each(function (index, item) {
                if ((index + 1) % 2 == 0) {
                    BodyHTML += "<tr class=\"background-color-gray\">";
                } else {
                    BodyHTML += "<tr>";
                }
                BodyHTML += "<td class=\"span2\">" + item.CouponTitle + "</td>";
                BodyHTML += "<td class=\"span2\">" + item.CouponMoney + "</td>";
                BodyHTML += "<td class=\"span2\">" + item.TotalCount + "</td>";
                if (showType == 1) {
                    BodyHTML += "<td style='width:25%; float:left; overflow:hidden;'>不可以</td>";
                    BodyHTML += "<td class=\"span3\"><span>" + item.InvalidTime + "</span></td>";
                } else if (showType == 2) {
                    BodyHTML += "<td style='width:25%; float:left; overflow:hidden;'>" + item.OrderCode + "</td>";
                    BodyHTML += "<td class=\"span3\"><span>" + item.UseTime + "</span></td>";
                } else if (showType == 3) {
                    BodyHTML += "<td style='width:25%; float:left; overflow:hidden;'>已过期</td>";
                    BodyHTML += "<td class=\"span3\"><span>" + item.InvalidTime + "</span></td>";
                }
                
                BodyHTML += "</tr>";
                $("#totalMoney").html(item.TotalMoney);
            });
        } else {
            if (showType == 1) {
                BodyHTML += "<tr><td colspan='4' style='text-align:center;'>您没有可使用的优惠券哦！</td></tr>";
            } else if (showType == 2) {
                BodyHTML += "<tr><td colspan='4' style='text-align:center;'>您没有使用过优惠券！</td></tr>";
            } else if (showType == 3) {
                BodyHTML += "<tr><td colspan='4' style='text-align:center;'>您没有已过期的优惠券！</td></tr>";
            }
            
            $("#totalMoney").html("￥0.00");
         }
        $("#couponList").html(BodyHTML);
    }
    function sparam(param) {
        param.IntIn = showType;
        param.PageSize = 5;
        return param;
    };
    var param = {
        AssemblyName: "Beyond.Birdex.Business.dll",
        ClassName: "Beyond.Birdex.Business.Market.Mar_CouponBLL",
        MethodName: "GetCouponInfoListByPage",
        ParamModelName: "Beyond.WebFrame.Param.Common.PageParam",
        ParamAssemblyName: "Beyond.WebFrame.Param.dll",
        page: { AllowPaging: true, PageSize: 5, PageIndex: parseInt(pageIndex) + 1 },
        onResponse: OnLoadSucess,
        onRequest: sparam,
        onComplete: function (result, pages) {
            //-------------------------------------数据加载成功，关闭遮罩层------------------------------
            setTimeout("$.BDEX.Close()", 500);

            if (type) {
                setTimeout("AlertCharge()", 1000);
            }
            if (showType == 1) {
                if (pages.RecordCount > 0) {
                    $(".count").show();
                    $(".count").html(pages.TotalPage);
                    $(".totalCount").html(pages.TotalPage);
                } else {
                    $(".count").hide();
                    $(".totalCount").html("0");
                }
            }

            if (type) {
                $(".pageShow").pagination(pages.RecordCount, {
                    callback: PageselectCallback, //PageCallback() 为翻页调用次函数。
                    first_text: "首页",
                    prev_text: " 上一页",
                    next_text: "下一页 ",
                    last_text: "尾页",
                    ellipse_text: "…",
                    items_per_page: 5, //每页的数据个数
                    num_display_entries: 8,
                    num_edge_entries: 1,
                    current_page: pageIndex   //当前页码
                });
            }
        }
    };
    $.ajaxRequest(param);
};

function PageselectCallback(clickPage) {
    GetCouponItem(item,clickPage);
};


function AlertCharge() {
    money = parseInt($.BDEX.QueryValByKey("money"));
    if (money != undefined && !isNaN(money) && money > 0) {
        var rechargeOffHeight = ($(window).height() - 570) / 2;
        $.layer({
            closeBtn: false,
            type: 2,
            fix: false,
            title: false,
            shade: [0.5, '#ccc', true],
            border: [1, 0.3, '#666', true],
            offset: [rechargeOffHeight + 'px', ''],
            area: ['900px', '570px'],
            iframe: { src: '/UserBase/Recharge.aspx?overdomain=' + isOverDomain + '&m=' + money }
        });
    }
}