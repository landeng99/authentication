/**
 * 验证用户会话是否存在，如果不存在则跳转至登录界面
 */
function validateLogin(isParent){
	var url=mainServer+"/member/validateLogin";
	var isValid=true;
	var loginUrl=mainServer+'/login';
	$.ajax({
		url:url,
		dataType:'json',
		type:'post',
		async:false,
		success:function(data){
			if(!data.isLogin){
				if(isParent){
			       parent.location.href=loginUrl;	
				}else{
					//如果用户会话过期，或未登录跳转至登录页面
					window.location.href=loginUrl;
				}
				isValid=false;
			}
			
		}
	});
	
	return isValid;
}

 
