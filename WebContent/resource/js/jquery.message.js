/* 时间格式化函数 */
 jQuery.utility = {
     leading:function (char, width, value) {
         var ret = value.toString();
         while (width > 1) {
             if (value >= (Math.pow(10,width - 1))) {
                 break;
            } else {
                ret = char + ret;
             }
             width--;
         }
         return ret;
     },
  
     now : function () {
         var date = new Date();
         var ret = {};
         ret.hours = $.utility.leading('0', 2, date.getHours());
         ret.minutes = $.utility.leading('0', 2, date.getMinutes());
         ret.seconds = $.utility.leading('0', 2, date.getSeconds());
         return ret;
     }
 };
 /* 消息插件函数 */
 $.fn.message = function () {
     this.show = function (msg) {
         this.html(msg);
     }
     this.clear = function () {
         this.html('');
     }
     this.append = function (tag, msg) {
         var date = $.utility.now();
         var tagContent = "<span class='msg-" + tag + "'>" + tag + "</span><span class='msg-datetime'>" + date.hours + ":" + date.minutes + ":" + date.seconds + "</span>";
         this.html(this.html() + "<br/>" + tagContent + "<span class='msg-message'>" + msg + "</span>");
     }
     this.warning = function (msg) {
         this.append("warning", msg);
     }
     this.error = function (msg) {
         this.append("error", msg);
     }
     this.succeed= function (msg) {
         this.append("succeed", msg);
     }
     this.debug = function (msg) {
         this.append("debug", msg);
     }
     this.info = function (msg) {
         this.append("info", msg);
     }
     return this;
 }