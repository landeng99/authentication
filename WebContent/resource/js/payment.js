﻿ function showhide(divid,abtn){
		 var node=$('#'+divid);
		 if(node.is(':hidden')){ 
			 node.show();
	 		 $('#'+abtn).text("回收");  
			}else{ 
               node.hide(); 
               $('#'+abtn).text("展开");  
			} 
	 }
	 // 支付成功，跳转回个人中心页
	 function  callbackPay(){
		 javascript:history.go(-1);
	     location.reload();
	 }
	 // 刷星当前页
	 function  reflesh(){
         window.location.href="${mainServer}/account/torecharge"; 
	} 
	 //设置菜单被选中 我要提交
	 $("#pesernsenter").addClass("selected");
      $("#submitCharge").click(function () { 
          var rechargeAmount =$('#rechargeAmount').val();
          var bankCode =$("input[name='bankCode']:checked").val();
          if(bankCode == null || bankCode.trim() == "" )
          {
        	  msginfo("支付方式不能为空！");  
        	
        	  return;
          }
          if(rechargeAmount==null||rechargeAmount.trim()==""){
        	  msginfo("金额不能为空！");
              $('#rechargeAmount').focus();
              return;
          }
          var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
          if(!exp.test(rechargeAmount)){ 
        	  msginfo("金额格式错误！");
              $('#rechargeAmount').focus();
              return;
            }
          if(parseFloat(rechargeAmount) < parseFloat(0.1)){
        	  msginfo("最小金额为0.1！");
              $('#rechargeAmount').focus();
              return;
          } 
     	 $("#prompt").modal("show");
          var url="${mainServer}/member/recharge?rechargeAmount="+rechargeAmount+"&bankCode="+bankCode; 
          window.open(url); 
      });
  	// 结算运费
 function toSettlement() {
          var count1=$("input[name='select1']:checked").length;
          var count2=$("input[name='select2']:checked").length;
		  if(count2==0 && count1 == 0){
			 // $.BDEX.Warn("包裹未选择！");
			 // $.BDEX.Success("包裹未选择", 1);
			  msginfo("亲，请选择一个包裹。");
		      // alert("包裹未选择！");
		       return;
		  } 
          var id_array=new Array(); 
          $("input[name='select1']:checked").each(function () {
              id_array.push($(this).attr('id'));
          });
		  var logisticsCodes=id_array.join(',');
		  console.log(logisticsCodes);
		  //还要结算税费
		
		  var id_array2=new Array(); 
		  $("input[name='select2']:checked").each(function () {
			  id_array2.push($(this).attr('id'));
		  });
		  var taxtlogisticsCodes=id_array2.join(',');
		  
		  console.log(taxtlogisticsCodes);
		  var mainServerurl = $("#mainServer").val();
  		  if((logisticsCodes != null && logisticsCodes != "") || (taxtlogisticsCodes != null && taxtlogisticsCodes != ""))
		  {
		  	  window.location.href = mainServerurl+ "/payment/paymentList?logisticsCodes=" + logisticsCodes+"&taxtlogisticsCodes="+taxtlogisticsCodes;
		  } 
     }; 
     function  goback(){
		 javascript:history.go(-1);
	     location.reload();
 		 // window.location.href="${mainServer}/personalCenter"; 
	 } 
  // 选择框选中  运费
     function checkALLItemsport(){
    	// var $isChecked = $("#id").is(":checked");
   	  if($("#checkALLItemsport").is(':checked')){
   		  $("input[name='select1']").prop("checked",true); 
   	  }else{
   		  $("input[name='select1']").prop("checked",false); 
   	  } 
   	  sumSportcount();
     }
     // 选择框选中  税金
     function checkALLItemstaxt(){
   	  if($("#checkALLItemstaxt").is(':checked')){
   	  $("input[name='select2']").prop("checked",true);
   	  }else{
   		  $("input[name='select2']").prop("checked",false);
   	   } 
      	sumTaxcount();
     }
	 function sumSportcount(){
		  var summoney =0;
		  var id_array=new Array(); 
		  $("input[name='select1']:checked").each(function () {
			  summoney += Number($(this).val());
 		  });
		  $("#sportmoney").html("$"+Math.round(summoney*100)/100);
		  
		  var count1=$("input[name='select1']:checked").length;
		  $("#sportcount").text(count1);
	  }
	 function sumTaxcount(){
		  var summoney =0;
		  var id_array=new Array(); 
		  $("input[name='select2']:checked").each(function () {
			  summoney += Number($(this).val());
 		  });
		  $("#payTaxtListmoney").html("$"+Math.round(summoney*100)/100);
		  
		  var count1=$("input[name='select2']:checked").length;
		  $("#payTaxtListcount").text(count1);
	  }