$(document).ready(function() {

    //左侧高度与屏幕高度相等开始
    var bodyHeight = $(document.body).height();
    var rightHeight=$('.index-right').height();
    if(bodyHeight<rightHeight){
        $(".index-left").css("height", rightHeight);
    }else{
        $(".index-left").css("height", bodyHeight);
    }

    $(window).resize(function () {
        bodyHeight = $(document.body).height();
        rightHeight=$('.index-right').height();
        if(bodyHeight<rightHeight){
            $(".index-left").css("height", rightHeight);
        }else{
            $(".index-left").css("height", bodyHeight);
        }
    });
    //左侧高度与屏幕高度相等结束

    //右侧导航菜单开始
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.am-panel>a');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.hidden-list').not($next).slideUp().parent().removeClass('open');
        };
    }

    var accordion = new Accordion($('#upAndDown'), false);


    $('.hidden-list').find('li').bind('click',function(i,d){
        $('.hidden-list').find('li').not($(this)).children().removeClass('current');
        $(this).children().attr('class','current');
    });

    //右侧导航菜单结束


   // $('#table-mutual tr:odd').css('background','#f7f7f7');

})