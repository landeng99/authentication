var StringBuffer = function(){
	var array = [];
	this.append = function( string ) {
		array.push( string );
	}
	this.toString = function(){
		return array.join('');
	}
	this.length = function(){
		return this.toString().length();
	}
};