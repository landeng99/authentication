﻿var searchType = 1;
$(function () {
    $(".recordli").click(function () {
        $(".recordli").removeClass("current");
        $(this).addClass("current");
        searchType = $(this).attr("type");
        GetUserTransaction(0, true);
    });
    pageIndex = 0;
    GetUserTransaction(0, true);

    SumAmount();
});
//=================获取用户交易记录===================
function GetUserTransaction(pageIndex,isload) {
    //-----------------数据遮罩层，表示数据正在加载中----------------------
    $.BDEX.Wait();
    //加载数据是否成功
    function OnLoadSucess(results) {
        $("#recordlist").html("");
        var recordHTML = "<li class=\"th\"><ol><li class=\"l no\" style='text-align:center;padding-left:0;'>时间</li><li class=\"m\" style='text-align:center;padding-left:0;'>涉及订单号</li><li class=\"n\" style='text-align:center;padding-left:0;'>金额</li><li class=\"o\" style='text-align:center;width:360px;'>备注</li></ol></li>";
        if (results != undefined && results != null && results.length > 0) {
            $(results).each(function (index, item) {
                recordHTML += "<li>";
                if ((index + 1) % 2 == 0) {
                    recordHTML += "<ol>";
                } else {
                    recordHTML += "<ol style='background-color:White;'>";
                }
                recordHTML += "<li class=\"l\" style='padding-left:0;text-align:center;'>" + item.CreateTime + "</li>";
                recordHTML += "<li class=\"m\" style='padding-left:0;text-align:center;'>" + item.PayRelateCode + "</li>"; //
                if (parseFloat(item.Amount) > 0) {
                    recordHTML += "<li class=\"n\" style='padding-left:0;text-align:center;'><div style='float:left;color:#9ab995;margin-left:15px;'>￥</div><span class=\"green\" style='display:block;float:left;text-align:right;width:55px;'>+" + item.Amount.toString().numFormat() + "</span></li>";
                } else {
                    recordHTML += "<li class=\"n\" style='padding-left:0;text-align:center;'><div style='float:left;color:#ff8b00;margin-left:15px;'>￥</div><span class=\"orange\" style='display:block;float:left;text-align:right;width:55px;'>" + item.Amount.toString().numFormat() + "</span></li>";
                }
                if (item.Remarks != undefined && item.Remarks != null) {
                    if (item.Remarks.length >= 20) {
                        recordHTML += "<li class=\"o\" style='height:auto;width:360px;vertical-align: botton;overflow:hidden;line-height:30px;'>" + item.Remarks + "</li>";
                    } else {
                        recordHTML += "<li class=\"o\" style='width:360px;vertical-align: botton;'>" + item.Remarks + "</li>";
                    }
                } else {
                    recordHTML += "<li class=\"o\" style='width:360px;vertical-align: botton;'>" + item.Remarks + "</li>";
                }
                recordHTML += "</ol>";
                recordHTML += "</li>";
            });
        } else {
            recordHTML += "<li style='height:48px;line-height:48px;text-align:center;background-color:White;'>暂无消费记录</li>";
        }
        $("#recordlist").html(recordHTML);
    }
    function sparam(param) {
        param.IntIn = searchType;
        param.PageSize = 10;
        return param;
    };
    var param = {
        AssemblyName: "Beyond.MemberCore.Business.dll",
        ClassName: "Beyond.MemberCore.Business.Mem_UserWalletTransactionBLL",
        MethodName: "GetUserWalletTransactionByPageForPortal",
        ParamModelName: "Beyond.WebFrame.Param.Common.PageParam",
        ParamAssemblyName: "Beyond.WebFrame.Param.dll",
        page: { AllowPaging: true, PageSize: 10, PageIndex: parseInt(pageIndex) + 1 },
        onResponse: OnLoadSucess,
        onRequest: sparam,
        onComplete: function (result, pages) {
            if (isload) {
                $(".pageShow").pagination(pages.RecordCount, {
                    callback: PageselectCallback, //PageCallback() 为翻页调用次函数。
                    first_text: "首页", 
                    prev_text: " 上一页",
                    next_text: "下一页 ",
                    last_text: "尾页",
                    ellipse_text: "…",
                    items_per_page: 10, //每页的数据个数
                    num_display_entries: 8,
                    num_edge_entries: 1,
                    current_page: 0   //当前页码
                });
            }
            //-------------------------------------数据加载成功，关闭遮罩层------------------------------
            setTimeout("$.BDEX.Close()", 500);
        }
    };
    $.ajaxRequest(param);
};
//执行翻页操作
function PageselectCallback(clickPage) {
    GetUserTransaction(clickPage,false);
};
//=================获取用户消费统计额度================
function SumAmount() {
//加载数据是否成功
    function OnLoadSucess(results) {
        $("#TotalSpendi").html(results);
    };
    function sparam(param) {
        return 1;
    };
    var param = {
        AssemblyName: "Beyond.MemberCore.Business.dll",
        ClassName: "Beyond.MemberCore.Business.Mem_UserWalletTransactionBLL",
        MethodName: "TotalSpending",
        ParamModelName: "System.Object",
        onResponse: OnLoadSucess,
        onRequest: sparam
    };
    $.ajaxRequest(param);
};