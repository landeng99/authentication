﻿var RegRule = {
    Phone: new RegExp("^0{0,1}(13[0-9]|14[0-9]|15[0-9]|153|156|17[0-9]|18[0-9])[0-9]{8}$"),
    Email: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
    Pass: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9a-zA-Z_#!@%^&*$]{6,16}/,
    IdenCode: /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/,
    Extention: /^\d{1,}$/,
    Code: /^\d{3,4}$/,
    Tel: /^\d{7,8}$/,
    QQ: /\d{5,}/,
    Decimal: /^[0-9]+(.[0-9]{1,})?$/,
    URL: /((^http)|(^https)|(^ftp)):\/\/(\\w)+\.(\\w)+/,
    Number: /^(\d+)$/,
    ZipCode: /^[0-9]{6}$/,
    Currency: /^(([1-9]{1}\d*)|([0]{1}))(\.(\d){0,2})?$/
};
var CDNImgServerUrl = 'http://img.cdn.birdex.cn/';
var ImgServerUrl = 'http://img.birdex.cn/';
var ImageUrl = "http://imgs.birdex.cn/";
var PayUrl = "http://pay.birdex.cn/";
var UploadUrl = "http://upload.birdex.cn/";
var IDCardUrl = "http://upload.birdex.cn/";
var domainUrl = "birdex.cn";
var ImageServer = [
    { "Name": "/IDCard/", "Code": "MemberIDCard", "path": "/File/Images/" },
];
function GetFilePath(code, filepath) {
    if (code == undefined || code == "") {
        return filepath;
    }
    for (var obj in ImageServer) {
        if (ImageServer[obj].Code == code) {
            return ImageUrl + ImageServer[obj].path + ImageServer[obj].Name + filepath;
        }
    }
};
$.BDEX = {
    //--------------------------------警告弹窗------------------------------
    Warn: function (info, timeout) {
        timeout = timeout || 3;
        info = info || "数据处理异常";
        layer.msg(info, timeout, 5);
    },
    //--------------------------------错误弹窗-----------------------------
    Error: function (info, timeout) {
        timeout = timeout || 3;
        info = info || "操作失败";
        layer.msg(info, timeout, 3);
    },
    //---------------------------------成功弹窗-----------------------------
    Success: function (info, timeout) {
        timeout = timeout || 3;
        info = info || "操作成功";
        layer.msg(info, timeout, 1);
    },
    //-------------------------------关闭窗口-------------------------------
    Close: function (index) {
        if (index == 0) { return; }
        if (index != undefined && index != null && index > 0) {
            layer.close(index);
        } else {
            layer.closeAll();
        }
    },
    //-------------------------------等待窗口-------------------------------
    Wait: function (timeout) {
        timeout = timeout || 2;
        layer.load(0, timeout);
    },
    QueryParams: function () {
        var url = window.document.location.href.toString();
        var u = url.split("?");
        if (typeof (u[1]) == "string") {
            u = u[1].split("&");
            var get = {};
            for (var i in u) {
                var j = u[i].split("=");
                get[j[0]] = j[1];
            }
            return get;
        } else {
            return {};
        }
    },
    QueryValByKey: function (key) {
        var parms = $.BDEX.QueryParams();
        for (var item in parms) {
            if (item == key) {
                return parms[key];
            }
        }
    },
    CheckNullOrEmpty: function (val) {
        if (val == undefined || val == null || val.trim() == "") {
            return true;
        }
        return false;
    },
    Tips: function (msg, obj, left, guide) {
        var index = 0;
        if (guide == 1) {
            index = layer.tips(msg, obj, {
                guide:1,
                style: ['background-color:#0FA6D8; color:#fff;', '#0FA6D8'],
                maxWidth: 400
            });
        } else {
            index = layer.tips(msg, obj, {
                style: ['background-color:#0FA6D8; color:#fff;left:' + left + 'px;', '#0FA6D8'],
                maxWidth: 400
            });
        }
        return index;
    },
    ChangeValidCode: function (imgParam, type) {//更换验证码
        if (type == "WeiXin") {
            imgParam.attr("src", "/common/ValidateForWetchat.aspx?c=" + type + "&r=" + Math.random());
        }
        else {
            imgParam.attr("src", "/common/ValidateCode.aspx?c=" + type + "&r=" + Math.random());
        }
    },
    EmailSMTP: function (parm) {
        var Google = "http://accounts.google.com";
        var urlstr = parm.substring(parm.indexOf("@") + 1);
        if (urlstr == "gmail.com") {
            return Google;
        } else {
            return "http://mail." + urlstr;
        }
    }
};
String.prototype.trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}
String.prototype.Trim = function () {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}
//----------------指定点击某一元素时的事件------------------------
function clickEvent() {
    var _event = event ? event : window.event;
    var _target = _event.target ? _event.target : _event.srcElement;
    var _id = _target.id;
    if (_id == "") {
        if (_target.parentNode != null) {
            _id = _target.parentNode.id;
        }
    }
    if (_id !== "btnSubmit" && _id !== "btnRegisterMobile" && _id !== "findPasswordMobile" && _id !== "findPayPasswordByEmail" && _id !== "findPayPasswordByMobile" && _id !=="save") {
        submitClick = false;
    }
    else {
        submitClick = true;
    }
}
//------------------------获取浏览器版本----------------
function getBrowserInfo() {
    var agent = navigator.userAgent.toLowerCase();
    var regStr_ie = /msie [\d.]+;/gi;
    var regStr_ff = /firefox\/[\d.]+/gi
    var regStr_chrome = /chrome\/[\d.]+/gi;
    var regStr_saf = /safari\/[\d.]+/gi;
    //IE
    if (agent.indexOf("msie") > 0) {
        return agent.match(regStr_ie);
    }
    //firefox
    if (agent.indexOf("firefox") > 0) {
        return agent.match(regStr_ff);
    }
    //Chrome
    if (agent.indexOf("chrome") > 0) {
        return agent.match(regStr_chrome);
    }
    //Safari
    if (agent.indexOf("safari") > 0 && agent.indexOf("chrome") < 0) {
        return agent.match(regStr_saf);
    }
	
if (agent.indexOf("trident/7.0") > -1 && agent.indexOf("rv:11.0") > -1) {
        return "msie 11.0";
    }
}
//================获取当前分辨率设置当前高度=====================
var IsShowTop;
$(function () {
        var height = $(window).height();
if ((parseInt(height) - 93 - 142) < 600) {
        $("#wrap").css("height", 600);
        $("#wrapModal").css("height", 600);
    } else {
        $("#wrap").css("height", parseInt(height) - 93 - 142);
        $("#wrapModal").css("height", parseInt(height) - 93 - 142);
    }
    $("#wrap").css("height", parseInt(height) - 93 - 142 - 3);
    if (IsShowTop != undefined && !IsShowTop) {

    } else {
        //注意最后一个参数配置 后面 不要有 英文逗号 , 
        new lrGoTop({
            bookUrl: '/', 	//更多联系方式的链接地址
            qrCode: "./resource/img/gototop03.jpg"	//二维码地址，如果不配置这一项，将生成当前页地址二维码
        })
    }
	
//    $("#lrToolRb").mouseover(function () {
//        $(this).find("a.lr-gotop").css("color", "#0097dc");
//        $(this).find("a.lr-QQ").css("color", "#0097dc");
//        $(this).find("a.lr-qrcode").css("color", "#0097dc");
//        $(".iconArrow").css("background-image", "url('" + ImgServerUrl + "/images/gototop01_1.jpg')");
//    }).mouseout(function () {
//        $(this).find("a.lr-gotop").css("color", "#666");
//        $(this).find("a.lr-QQ").css("color", "#666");
//        $(this).find("a.lr-qrcode").css("color", "#666");
//        $(".iconArrow").css("background-image", "url('" + ImgServerUrl + "/images/gotop.jpg')");
//    });	
});
//=========获取客户端浏览器高度============
function getClientHeight() {
    //可见高
    var clientHeight = document.body.clientHeight; //其它浏览器默认值
    if (navigator.userAgent.indexOf("MSIE 6.0") != -1) {
        clientHeight = document.body.clientHeight;
    }
    else if (navigator.userAgent.indexOf("MSIE") != -1) {
        //IE7 IE8
        clientHeight = document.documentElement.offsetHeight
    }

    if (navigator.userAgent.indexOf("Chrome") != -1) {
        clientHeight = document.body.scrollHeight;
    }

    if (navigator.userAgent.indexOf("Firefox") != -1) {
        clientHeight = document.documentElement.scrollHeight;
    }
    return clientHeight;
}

function SetQQ() {
    $("#QQ").click();
}
//=========返回头部JS==================
function lrGoTop(options) {
    this.config = {
        H: 0, 		//滚动到的距离 0即滚动到顶部
        T: 600, 		//600毫秒完成动画
        bookUrl: '', 	//如果为空，留言按钮将被隐藏
        qrCode: 'api'	//如果为"api" 则自动生成当前页面二维码，如果是一个图片路径将使用指定的二维码图片
    };
    var opts = $.extend(this.config, options), timeOut;



    this.Animate = function () {
        $("html,body").animate({ scrollTop: opts.H }, opts.T)
    }
    this.Close = function () {
        $(".lr-pop").hide();
    }
    this.init = function () {
        if (!("maxHeight" in document.createElement("div").style)) { return }; //IE6返回不打算兼容IE6
        var This = this;
  /*      if (!$("#lrToolRb").length) {
            $("body").append(this.tpl.replace(/\{book\}/gi, opts.bookUrl));
        }*/
        var lrToolRb = $("#lrToolRb")
			lrPop = lrToolRb.children(".lr-pop"),
			qcImg = lrPop.children(".lr-qcimg");

        if (!opts.bookUrl) { lrToolRb.children(".lr-book").hide() }
        if (!opts.qrCode) { lrToolRb.children(".lr-qrcode").hide() }

        lrToolRb.on("click", ".lr-gotop", function () {
            This.Animate();
            return false;
        });
        //单击生成二维码
        var url = encodeURIComponent(window.location.href);
        lrToolRb.on("click", "a.lr-qrcode", function () {
            //clearTimeout(timeOut);
            //timeOut = setTimeout(This.Close,10000);
            if (qcImg.children("img").length > 0) {
                lrPop.show();
                return
            }
            if (This.config.qrCode == 'api') {
                lrPop.show();
                qcImg.html(This.makeQrcode(url));
            } else {
                lrPop.show();
                qcImg.html("<img src='" + This.config.qrCode + "' width='200' height='200' alt='手机扫一扫' />");
            }
        });
        lrToolRb.on("click", ".lr-close", This.Close);
    }
    this.init();
}

//显示地址信息
function LoadAddressOption(showParam, showType, parentId, currentId) {
    var addressJson;
    var tipInfo = "";
    if (showType == 1) {
        tipInfo = "<option value='0'>请选择省市...</option>";
        addressJson = $(province);
    } else if (showType == 2) {
        tipInfo = "<option value='0'>请选择城市...</option>";
        addressJson = $(city);
        //alert(addressJson);
    } else if (showType == 3) {
        tipInfo = "<option value='0'>请选择区/县...</option>";
        addressJson = $(area);
        addPostCode = $(city);
    } else { return; }
    var itemOption = ""; //承载option数据
    //alert(parentId);
    addressJson.each(function (index, data) {
        if (data.father_id == parentId && data.city_id == currentId) {
            itemOption += "<option selected='selected' value='" + data.city_id + "' p='" + data.father_id + "'>" + data.city_name + "</option>";
        } else if (data.father_id == parentId) {
            itemOption += "<option value='" + data.city_id + "' p='" + data.father_id + "'>" + data.city_name + "</option>";
            //alert("OK");
            $("#PostCode").val(data.postal_code);
        }
    });
    if(showType == 3){
	    addPostCode.each(function (index, data) {
	    	if (data.city_id == parentId){
	    		//alert("NO");
	    	$("#PostCode").val(data.postal_code);
	    	}
	    });
    }
    showParam.empty().prepend(tipInfo);
    showParam.append(itemOption);
};
///select 选项改动
function SelectChange(param, changeType) {
    var selectValue = $(param).val();
    if (changeType == 2) {
        addCity.empty().prepend("<option value='0'>请选择城市...</option>");
        addQzone.empty().prepend("<option value='0'>请选择区/县...</option>");
        LoadAddressOption(addCity, 2, selectValue, 0);
    } else if (changeType == 3) {
        addQzone.empty().prepend("<option value='0'>请选择区/县...</option>");
        LoadAddressOption(addQzone, 3, selectValue, 0);
    }
};
function SelectModifyChange(param, changeType) {
    var selectValue = $(param).val();
    var modifyProvince, modifyCity,modifyQzone,PostCode;
    modifyProvince = $("#modifyProvince");
    modifyCity = $("#modifyCity");
    modifyQzone = $("#modifyQzone");
    PostCode = $("#PostCode");
    //alert("DSFSD");
    if (changeType == 2) {
        modifyCity.empty().prepend("<option value='0'>请选择城市...</option>");
        modifyQzone.empty().prepend("<option value='0'>请选择区/县...</option>");
        LoadAddressOption(modifyCity, 2, selectValue, 0);
    } else if (changeType == 3) {
        modifyQzone.empty().prepend("<option value='0'>请选择区/县...</option>");
        LoadAddressOption(modifyQzone, 3, selectValue, 0);
    }
};
//交易只显示城市不显示区域
function SelectTradeChange(param) {
    var selectValue = $(param).val();
    modifyCity.empty().prepend("<option value='0'>请选择城市...</option>");
    LoadAddressOption(modifyCity, 2, selectValue, 0);
};
function DrawImage(ImgD,FitWidth,FitHeight){var image=new Image();image.src=ImgD.src;if(image.width>0&&image.height>0){if(image.width/image.height>=FitWidth/FitHeight){if(image.width>FitWidth){ImgD.width=FitWidth;ImgD.height=(image.height*FitWidth)/image.width;}else{ImgD.width=image.width;ImgD.height=image.height;}}else{if(image.height>FitHeight){ImgD.height=FitHeight;ImgD.width=(image.width*FitHeight)/image.height;}else{ImgD.width=image.width;ImgD.height=image.height;}}}}

//-------------------获取get指定key的值----------------------
function GetValueByKey(key) {
    var parms = GetParams();
    for (var item in parms) {
        if (item == key) {
            return parms[key];
        }
    }
};
//-------------------获取get所有参数----------------------
function GetParams() {
    var url = window.document.location.href.toString();
    var u = url.split("?");
    if (typeof (u[1]) == "string") {
        u = u[1].split("&");
        var get = {};
        for (var i in u) {
            var j = u[i].split("=");
            get[j[0]] = j[1];
        }
        return get;
    } else {
        return {};
    }
}