 function pageNow(index){
	    if(index == $("#nowpage").val()){
	    	return;
	    }
    	$("#nowpage").val(index);
    	pagerClick(index);
    }
    function gotopage(){
   	 var pagenoto = document.getElementById("pagenoto").value;
   	 if($("#nowpage").val() == pagenoto){
   		 return;
   	 }
   	 var allpage = document.getElementById("allpageno").value;
   	 if(pagenoto > allpage){
   		layer.msg("输入页码不能大于总页码", 2, 5);
   		return;
   	 }
   	 pageNow(pagenoto);
   }
    // 前一页
    function goPrent(){
    	 var pagenow =  $("#nowpage").val();
    	 if(parseInt(pagenow) > 1){
    		 var index = parseInt(pagenow)  -1;
    		 $("#nowpage").val(index);
    	     pagerClick(index);
    	 }
    }
    //  后一页
     function goAfter(){
    	 var pagenow =  $("#nowpage").val();
    	 var allpage = document.getElementById("allpageno").value;
    	 if(pagenow < allpage){
    		 var index =parseInt( pagenow)  + 1;
    		 $("#nowpage").val(index);
    	     pagerClick(index);
    	 }
    }