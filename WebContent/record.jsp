<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0032)http://www.birdex.cn/record.html -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" charset="utf-8" async="" src="${mainServer}/resource/img/crmqq.php"></script${mainServer}>
<script type="text/javascript" charset="utf-8" async="" src="${mainServer}/resource/js/contains.js"></script>
<script type="text/javascript" charset="utf-8" async="" src="${mainServer}/resource/js/localStorage.js"></script>
<script type="text/javascript" charset="utf-8" async="" src="${mainServer}/resource/js/Panel.js"></script>
    <title>账户记录- 最注重海淘用户体验的转运公司。</title>
    <meta name="keywords" content="翔锐物流，笨鸟转运，转运公司,美国快递、美国转运">
    <meta name="description" content="首家转运免首重，免税州直飞，24小时出库，丢货全赔，6*10小时贴心客服。翔锐物流用互联网思维打造转运公司，颠覆转运行业时效慢，体验差，安全性低，价格高的用户体验。">
    <link href="${mainServer}/resource/css/account.css" rel="stylesheet" type="text/css">
<link href="${mainServer}/resource/css/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${mainServer}/resource/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="${mainServer}/resource/js/layer.min.js"></script>
<script type="text/javascript" src="${mainServer}/resource/js/common.js"></script>
<script type="text/javascript">
    $(function () {
        var userName = $.cookie('BEYOND_NickName');
        if (userName == undefined || userName == "") {
            userName = $.cookie('BEYOND_UserName');
        }
        if (userName == undefined || userName == "") {
            userName = "会员用户";
        } else {
            if (userName.length > 8 && userName.indexOf("@") > -1) {
                userName = userName.substring(0, 7) + "...";
            }
        }
        $("#headerUserName").html(userName).attr("title", userName);

        var url = document.location.href;
        if (url == "http://www.birdex.cn/")
            url = "http://www.birdex.cn/index.html";
        else if (url == "http://birdex.cn/")
            url = "http://www.birdex.cn/index.html";

        $("ul.nav>li").each(function () {
            if (url.indexOf($(this).attr("val")) > -1) {
                $(this).addClass("current");
            }
        });

        var userID = $.cookie('User::TUserID');
        if (userID != undefined && userID != null && parseInt(userID) > 0) {
            $("#logout").show();
            $(".nav").css("right", "0");
        } else {
            $(".nav").css("right", "0");
            $("#login").show();
        }

        $(".ggnew").click(function () {
            if ($("#ggtab").is(':visible')) {
                $("#ggtab").slideUp("slow");
            } else {
                $("#ggtab").slideDown("slow");
            };
            var img = $(this).find("span").css("background-image");
            if (img.indexOf('header01') > -1) {
                $(this).find("span").css("background-image", img.replace('header01.png', 'header_up.png'));
            } else {
                $(this).find("span").css("background-image", img.replace('header_up.png', 'header01.png'));
            }
        });

        $("#tags1 li").mouseover(function (index, item) {
            $("#tags1 li").removeClass("selectTag");
            $(this).addClass("selectTag");
            $("#tags1").parent().find("div").each(function () {
                if ($(this).hasClass("selectTag")) {
                    $(this).removeClass("selectTag").addClass("tagContent");
                }
            });
            $("#" + $(this).attr("obj")).removeClass("tagContent").addClass("selectTag");
        });

        if (url.indexOf('index') > -1 || document.URL == 'http://www.birdex.cn/' || document.location.href.indexOf('index') > -1) {
            $(".ggnew").click();
        }

        var headerName = $.cookie('User::NickName');
        if (headerName == undefined || headerName == null || headerName == "") {
            headerName = $.cookie('User::TrueName');
            if (headerName == undefined || headerName == null || headerName == "") {
                headerName = $.cookie('BEYOND_UserName');
                if (headerName == undefined || headerName == null) {
                    headerName = "";
                }
            }
        }

        $("#headername").html(headerName);
    });
</script>
<meta property="qc:admins" content="15411542256212450636">
<style type="text/css">
    #header{ border-bottom:1px solid #dcdcdc; overflow:hidden; height:32px; line-height:32px;}
    #header a{ font-size:14px; color:#6a6a6b;}
    #header .pull-right a{ margin:0 10px;}
    #header .ggnew{ cursor:pointer;}
    #header .ggnew i{ color:#eb7424; font-style:normal;}
    #header .ggnew span{ float:left; display:block; width:42px; height:32px; background:url('http://img.cdn.birdex.cn/images/header01.png') no-repeat center center #0593d3; overflow:hidden; margin-right:5px;}
    #ggtab{ display:none; background-color:#0593d3; padding:10px 0; overflow:hidden;}
    #ggtab .hbt{ font-size:22px; font-weight:bold; color:#f57121; padding-left:32px; background:url('http://img.cdn.birdex.cn/images/header02.png') no-repeat left center; overflow:hidden; line-height:32px;}
    #sevedu{ padding-top:5px; overflow:hidden;}
    #sevedu .tagContent{display:none;}
    #sevedu .tagContent p,
    #sevedu .selectTag p{ font-size:16px; color:#edf6ff;}
    #sevedu .tagContent p a,
    #sevedu .selectTag p a{ color:#f57121; text-decoration:underline; margin-left:8px; font-weight:bold;}
    #sevedu .tag{ width:100%; padding-top:5px; text-align:right; overflow:hidden;}
    #sevedu .tag li{display:inline;}
    #sevedu .tag a{display:inline-block; width:8px; height:9px; background:url('http://img.cdn.birdex.cn/images/header03.png') no-repeat; overflow:hidden; margin:0 3px;}
    #sevedu .tag a:hover,
    #sevedu .tag .selectTag a{ width:56px; background-image:url('http://img.cdn.birdex.cn/images/header04.png');}
</style>
</head>
<body>
<div id="ggtab">
     <div class="mian">
          <div class="hbt">公告</div>
          
          <div id="sevedu">
                <div class="selectTag" id="li0"><p>中国清明节（4月4日至4月6日）假期安排通知：除4月5日（星期天）清明节当天放假外，其余时间均有国内客服正<a href="http://www.birdex.cn/Information-14.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li1"><p>关于新增渠道详细介绍公告<a href="http://www.birdex.cn/Information-21.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li2"><p>关于系统问题导致订单申报数据错误的相关处理方案<a href="http://www.birdex.cn/Information-19.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li3"><p>所有笨鸟的客户以及淘友，经过对目前现状的考察和认真考虑，笨鸟不得已今天对税单相关政策作出调整如下：<a href="http://www.birdex.cn/Information-16.html" target="_blank">查看详情</a></p></div><ul id="tags1" class="tag"><li obj="li0" class="selectTag"><a href="javascript:void(0)"></a></li><li obj="li1"><a href="javascript:void(0)"></a></li><li obj="li2"><a href="javascript:void(0)"></a></li><li obj="li3"><a href="javascript:void(0)"></a></li></ul>
          </div>
     </div>
</div>
<div id="header" class="noticeHeader">
     <div class="mian">
         <div class="pull-right" id="login" style="display:none;">
              <a href="http://passport.birdex.cn/Login.aspx">登录</a>|<a href="http://passport.birdex.cn/Register.aspx">注册</a>
         </div>
         <div class="pull-right" id="logout" style="">
               <span id="headername">502725157@qq.com</span><a href="http://passport.birdex.cn/LogOut.aspx" style="">退出</a>
          </div>
         <a class="ggnew"><span></span>网站最新公告 <i>4</i> 条</a>
     </div>
</div>
<div id="head">
     <div class="mian">
          <a href="http://www.birdex.cn/index.html" class="logo"><img src="${mainServer}/resource/img/logo.jpg" alt="翔锐物流" title="翔锐物流"><span>笨鸟</span></a>
          
          
          <ul class="nav" style="right: 0px;">
              <li val="help" style="padding-right:5px;"><a href="http://www.birdex.cn/help.html">帮助中心</a></li>
              <li val="Information" id="headInformation"><a href="http://www.birdex.cn/Information/prohibition.aspx">禁运物品</a></li>
              <li val="account" id="headAccount" class="current"><a href="http://www.birdex.cn/transport.html">我的帐户</a></li>
              <li val="index"><a href="http://www.birdex.cn/index.html">首页</a></li>
          </ul>
     </div>
</div>
<div id="content">
     
<script type="text/javascript">
    var isOverDomain = 0;
    var old = '1';
    $(function () {
        $("ul.sideaav li").each(function () {
            if (document.location.href.toLowerCase().indexOf($(this).attr("item").toLowerCase()) > -1) {
                $(this).addClass("current");
                $(this).find("i").css("background-image", "url('" + $(this).attr("img") + "')");
                $("ul.nav>li").removeClass("current");
                $("#headAccount").addClass("current");
            }
        });

        var overDomainArray = ["destination", "transport", "store", "order"];
        for (var item in overDomainArray) {
            if (document.location.href.toLowerCase().indexOf(overDomainArray[item]) > -1) {
                document.domain = domainUrl;
                isOverDomain = 1;
            }
        }
        $("#charge,#accountCharge,#accountLeveUp").click(function () {
            var rechargeOffHeight = ($(window).height() - 570) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                fix: false,
                title: false,
                shade: [0.5, '#ccc', true],
                border: [1, 0.3, '#666', true],
                offset: [rechargeOffHeight + 'px', ''],
                area: ['900px', '570px'],
                iframe: { src: '/UserBase/Recharge.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#editPass,#accountEditPass").click(function () {
            var editPassOffHeight = 0;
            var height = 338;
            if (old > 0) {
                editPassOffHeight = ($(window).height() - 338) / 2;
            } else {
                editPassOffHeight = ($(window).height() - 270) / 2;
                height = 270;
            }
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#fff', true],
                border: [0, 0.3, '#000', true],
                offset: [editPassOffHeight + 'px', ''],
                area: ['335px', height + 'px'],
                iframe: { src: '/UserBase/ModifyP.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#emailActive,#accountValidEmail").click(function () {
            var emailActiveOffHeight = ($(window).height() - 320) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [emailActiveOffHeight + 'px', ''],
                //area: ['850px', '400px'],
                area: ['335px', '278px'],
                //iframe: { src: '/UserBase/ActiveEmail.aspx?overdomain=' + isOverDomain }
                iframe: { src: '/UserBase/ActiveE.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#mobileActive,#accountActiveMobile").click(function () {
            var mobileActiveOffHeight = ($(window).height() - 277) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [mobileActiveOffHeight + 'px', ''],
                area: ['335px', '277px'],
                iframe: { src: '/UserBase/ActiveMobile.aspx?overdomain=' + isOverDomain }
            });
        });
    });
</script>
<div class="left" style="margin-bottom:250px;">
          <h3 class="size14"></h3>
          <p><span style="float:right;">已激活</span>502725157@q...</p>
          <p><!----></p>
          <ul class="recharge">
            
              <li class="span5 pull-right text-center">
                  <i class="icon06"></i>
                  <h3 class="size14">5张</h3>
                  <span style="font-size:13px;"><a href="http://www.birdex.cn/account.html#Coupon" class="blue">优惠劵</a></span>
                  
              </li>
              
              <li class="span5 pull-left text-center" style="width:auto;">
                  <a title="钱包余额" style="text-decoration:none;cursor:pointer;"><i class="icon05"></i>
                  <h3 class="size14">$<span style="padding-left:1px;">0.00</span></h3></a>
                  <a style="cursor:pointer;font-size:13px;" class="blue" id="charge">充值</a>
              </li>

          </ul>

          <ul class="modification">
              <li class="span6"><a style="cursor:pointer;" class="blue icon07" id="editPass">修改密码</a></li>
              <li class="span6" style="display:none;"><a href="" class="blue icon08">修改订阅</a></li>
              <li class="span6" style="display:none;"><a href="" class="blue icon09">使用习惯</a></li>
          </ul>

           <ul class="sideaav">
            <!--
              <li class="span12" item="transport" img="/images/icon10_fff.png"><a href="/transport.html" class="size16"><i class="icon10"></i>我的包裹管理</a></li>
              <li class="span12" item="transport" img="/images/icon10_fff.png"><a href="/transport.html" class="size16"><i class="icon10"></i>我的订单</a></li>
              -->
              <li class="span12" item="transport" img="${mainServer}/resource/img/icon10_fff.png"><a href="${mainServer}/transport" class="size16"><i class="icon10"></i>我的包裹管理</a></li>
              <li class="span12" item="warehouse" img="${mainServer}/resource/img/icon11_fff.png"><a href="${mainServer}/warehouse.jsp" class="size16"><i class="icon11"></i>海外仓库地址</a></li>
              <li class="span12" item="destination" img="${mainServer}/resource/img/icon12_fff.png"><a href="${mainServer}/destination.jsp" class="size16"><i class="icon12"></i>收货地址管理</a></li>
              <li class="span12 current" item="record" img="${mainServer}/resource/img/icon13_fff.png"><a href="${mainServer}/record.jsp" class="size16"><i class="icon13"></i>账户记录</a></li>
              <li class="span12" item="account" img="${mainServer}/resource/img/icon14_fff.png"><a href="${mainServer}/account.jsp" class="size16"><i class="icon14" style="background-image: url(${mainServer}/resource/img/icon14.png);"></i>账户设置</a></li>
          </ul>
</div>
     <div class="right">
                  <div class="record">
               <div class="bt">
                    <p class="size14"><strong>我的消费记录</strong></p>
                    <p>近一年实际消费额：<span class="orange size16" id="TotalSpendi">$<span style="" padding-left:1px;="">0</span></span><!--<a href="#" class="blue pd10">查看您的会员级别</a>--></p>
               </div>
               
               <div class="hr mg18"></div>
               
               <div class="opt">
                   <ul class="function pull-left">
                       <li class="span6 current recordli" type="1"><a style="cursor:pointer;"><i class="first"></i>近三个月记录</a></li>
                       <li class="span6 recordli" type="2"><a style="cursor:pointer;">三个月前数据<i class="ultimate"></i></a></li>
                   </ul>
               </div>
              
               <ul class="list01 radius3" id="recordlist">
               	<li class="th">
               		<ol><li class="l no" style="text-align:center;padding-left:0;">时间</li>
               			<li class="m" style="text-align:center;padding-left:0;">订单号</li>
               			<li class="n" style="text-align:center;padding-left:0;">金额</li>
               			<li class="o" style="text-align:center;width:360px;">备注</li>
               		</ol></li>
               	<li style="height:48px;line-height:48px;text-align:center;background-color:White;">暂无消费记录</li>
               	</ul>
               
               <div class="paging text-right">
                    <div class="page pageShow"></div>
               </div>
              
               <div class="hr mg18"></div>
               
               <div class="bt">
                    <p class="size14"><strong>特别说明</strong></p>
                    <p>交易记录中不包括使用优惠券、礼品卡、手机红包支付或退款至礼品卡、优惠券、手机红包的金额。</p>
                    <p>系统仅显示您两年之内的余额明细，更早的余额明细不再显示。</p>
               </div>
          </div>
     </div>
</div>
<!--
<script type="text/javascript" src="${mainServer}/resource/js/record.js"></script>
-->
<script type="text/javascript" src="${mainServer}/resource/js/jquery.pagination.js"></script>
 <div id="foot">
     <div class="mian">
          <span class="pull-right"><img src="${mainServer}/resource/img/logo.gif" alt="翔锐物流"></span>
          
          <div class="pull-left">
              <h5>联系我们</h5>
              <p>4008-890-788</p>
              <p>0755-86054577</p>
              <p id="QQ"><!-- WPA Button Begin -->
			<script charset="utf-8" type="text/javascript" src="${mainServer}/resource/img/wpa.php"></script>
<!-- WPA Button End --></p>
          </div>
          <div class="pull-left">
              <h5>邮件</h5>
              <p><a href="mailto:service@birdex.cn" style="color:#FFFFFF">service@birdex.cn</a></p>
          </div>
          <div class="pull-left" style="width:200px">
            <h5>友情链接</h5>
            <a href="http://www.etao.com/tuan/index.html?partnerid=4852" target="_blank" style="color:#FFFFFF">一淘海淘团</a><br>
<a href="http://www.smzdm.com/" target="_blank" style="color:#FFFFFF">什么值得买</a><br>
            <a href="http://www.55haitao.com/" target="_blank" style="color:#FFFFFF">55海淘</a>
            
          </div>
     </div>
    <div class="mian" style="margin:0 auto;text-align:center;color:#818181;font-size:14px;vertical-align:bottom;">
        <div>粤ICP备14015306号-1</div> 
     </div>
</div>
<div style="display:none;">
<script type="text/javascript">
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F68c503fc431b1f102540a9c79f1ffc78' type='text/javascript'%3E%3C/script%3E"));
</script>
<script src="${mainServer}/resource/js/h.js" type="text/javascript"></script>
<a href="http://tongji.baidu.com/hm-web/welcome/ico?s=68c503fc431b1f102540a9c79f1ffc78" target="_blank">
<img border="0" src="${mainServer}/resource/img/21.gif" width="20" height="20"></a>
</div>
<div id="lrToolRb" class="lr-tool-rb" style="z-index:10000;">
<a class="lr-gotop" title="返回顶部"><span class="lricon iconArrow"></span><br>TOP</a>
<a class="lr-QQ" title="单击联系QQ客服" onclick="SetQQ();"><span class="lricon"></span><br>联系客服</a>
<a class="lr-qrcode" title="关注获取最新优惠，实时跟踪包裹进度"><span class="lricon"></span><br>微信关注</a>
<div class="lr-pop"><span class="lr-close">关闭</span><span class="lr-qcimg"></span></div></div></body></html>