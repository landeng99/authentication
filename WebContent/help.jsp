<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0030)http://www.birdex.cn/help.html -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/img/crmqq.php"></script>
<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/contains.js"></script>
<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/localStorage.js"></script>
<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/Panel.js"></script>
    <title>帮助中心- 最注重海淘用户体验的转运公司。</title>
    <meta name="keywords" content="翔锐物流，笨鸟转运，转运公司,美国快递、美国转运">
    <meta name="description" content="首家转运免首重，免税州直飞，24小时出库，丢货全赔，6*10小时贴心客服。翔锐物流用互联网思维打造转运公司，颠覆转运行业时效慢，体验差，安全性低，价格高的用户体验。">
    <link rel="shortcut icon" type="image/x-icon" href="http://www.birdex.cn/icon_16x16.png" media="screen">
<link href="/lmp/resource/css/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/lmp/resource/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="/lmp/resource/js/layer.min.js"></script>
<script type="text/javascript" src="/lmp/resource/js/common.js"></script>
<script type="text/javascript">
    $(function () {
        var userName = $.cookie('BEYOND_NickName');
        if (userName == undefined || userName == "") {
            userName = $.cookie('BEYOND_UserName');
        }
        if (userName == undefined || userName == "") {
            userName = "会员用户";
        } else {
            if (userName.length > 8 && userName.indexOf("@") > -1) {
                userName = userName.substring(0, 7) + "...";
            }
        }
        $("#headerUserName").html(userName).attr("title", userName);

        var url = document.location.href;
        if (url == "http://www.birdex.cn/")
            url = "http://www.birdex.cn/index.html";
        else if (url == "http://birdex.cn/")
            url = "http://www.birdex.cn/index.html";

        $("ul.nav>li").each(function () {
            if (url.indexOf($(this).attr("val")) > -1) {
                $(this).addClass("current");
            }
        });

        var userID = $.cookie('User::TUserID');
        if (userID != undefined && userID != null && parseInt(userID) > 0) {
            $("#logout").show();
            $(".nav").css("right", "0");
        } else {
            $(".nav").css("right", "0");
            $("#login").show();
        }

        $(".ggnew").click(function () {
            if ($("#ggtab").is(':visible')) {
                $("#ggtab").slideUp("slow");
            } else {
                $("#ggtab").slideDown("slow");
            };
            var img = $(this).find("span").css("background-image");
            if (img.indexOf('header01') > -1) {
                $(this).find("span").css("background-image", img.replace('header01.png', 'header_up.png'));
            } else {
                $(this).find("span").css("background-image", img.replace('header_up.png', 'header01.png'));
            }
        });

        $("#tags1 li").mouseover(function (index, item) {
            $("#tags1 li").removeClass("selectTag");
            $(this).addClass("selectTag");
            $("#tags1").parent().find("div").each(function () {
                if ($(this).hasClass("selectTag")) {
                    $(this).removeClass("selectTag").addClass("tagContent");
                }
            });
            $("#" + $(this).attr("obj")).removeClass("tagContent").addClass("selectTag");
        });

        if (url.indexOf('index') > -1 || document.URL == 'http://www.birdex.cn/' || document.location.href.indexOf('index') > -1) {
            $(".ggnew").click();
        }

        var headerName = $.cookie('User::NickName');
        if (headerName == undefined || headerName == null || headerName == "") {
            headerName = $.cookie('User::TrueName');
            if (headerName == undefined || headerName == null || headerName == "") {
                headerName = $.cookie('BEYOND_UserName');
                if (headerName == undefined || headerName == null) {
                    headerName = "";
                }
            }
        }

        $("#headername").html(headerName);
    });
</script>
<meta property="qc:admins" content="15411542256212450636">
<style type="text/css">
    #header{ border-bottom:1px solid #dcdcdc; overflow:hidden; height:32px; line-height:32px;}
    #header a{ font-size:14px; color:#6a6a6b;}
    #header .pull-right a{ margin:0 10px;}
    #header .ggnew{ cursor:pointer;}
    #header .ggnew i{ color:#eb7424; font-style:normal;}
    #header .ggnew span{ float:left; display:block; width:42px; height:32px; background:url('http://img.cdn.birdex.cn/images/header01.png') no-repeat center center #0593d3; overflow:hidden; margin-right:5px;}
    #ggtab{ display:none; background-color:#0593d3; padding:10px 0; overflow:hidden;}
    #ggtab .hbt{ font-size:22px; font-weight:bold; color:#f57121; padding-left:32px; background:url('http://img.cdn.birdex.cn/images/header02.png') no-repeat left center; overflow:hidden; line-height:32px;}
    #sevedu{ padding-top:5px; overflow:hidden;}
    #sevedu .tagContent{display:none;}
    #sevedu .tagContent p,
    #sevedu .selectTag p{ font-size:16px; color:#edf6ff;}
    #sevedu .tagContent p a,
    #sevedu .selectTag p a{ color:#f57121; text-decoration:underline; margin-left:8px; font-weight:bold;}
    #sevedu .tag{ width:100%; padding-top:5px; text-align:right; overflow:hidden;}
    #sevedu .tag li{display:inline;}
    #sevedu .tag a{display:inline-block; width:8px; height:9px; background:url('http://img.cdn.birdex.cn/images/header03.png') no-repeat; overflow:hidden; margin:0 3px;}
    #sevedu .tag a:hover,
    #sevedu .tag .selectTag a{ width:56px; background-image:url('http://img.cdn.birdex.cn/images/header04.png');}
</style>
</head>
<body><iframe style="display: none;"></iframe><style type="text/css">.WPA3-SELECT-PANEL { z-index:2147483647; width:463px; height:292px; margin:0; padding:0; border:1px solid #d4d4d4; background-color:#fff; border-radius:5px; box-shadow:0 0 15px #d4d4d4;}.WPA3-SELECT-PANEL * { position:static; z-index:auto; top:auto; left:auto; right:auto; bottom:auto; width:auto; height:auto; max-height:auto; max-width:auto; min-height:0; min-width:0; margin:0; padding:0; border:0; clear:none; clip:auto; background:transparent; color:#333; cursor:auto; direction:ltr; filter:; float:none; font:normal normal normal 12px "Helvetica Neue", Arial, sans-serif; line-height:16px; letter-spacing:normal; list-style:none; marks:none; overflow:visible; page:auto; quotes:none; -o-set-link-source:none; size:auto; text-align:left; text-decoration:none; text-indent:0; text-overflow:clip; text-shadow:none; text-transform:none; vertical-align:baseline; visibility:visible; white-space:normal; word-spacing:normal; word-wrap:normal; -webkit-box-shadow:none; -moz-box-shadow:none; -ms-box-shadow:none; -o-box-shadow:none; box-shadow:none; -webkit-border-radius:0; -moz-border-radius:0; -ms-border-radius:0; -o-border-radius:0; border-radius:0; -webkit-opacity:1; -moz-opacity:1; -ms-opacity:1; -o-opacity:1; opacity:1; -webkit-outline:0; -moz-outline:0; -ms-outline:0; -o-outline:0; outline:0; -webkit-text-size-adjust:none; font-family:Microsoft YaHei,Simsun;}.WPA3-SELECT-PANEL a { cursor:auto;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-TOP { height:25px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CLOSE { float:right; display:block; width:47px; height:25px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CLOSE:hover { background-position:0 -25px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-MAIN { padding:23px 20px 45px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-GUIDE { margin-bottom:42px; font-family:"Microsoft Yahei"; font-size:16px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-SELECTS { width:246px; height:111px; margin:0 auto;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CHAT { float:right; display:block; width:88px; height:111px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat 0 -80px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CHAT:hover { background-position:-88px -80px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-AIO-CHAT { float:left;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-QQ { display:block; width:76px; height:76px; margin:6px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat -50px 0;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-QQ-ANONY { background-position:-130px 0;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-LABEL { display:block; padding-top:10px; color:#00a2e6; text-align:center;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-BOTTOM { padding:0 20px; text-align:right;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-INSTALL { color:#8e8e8e;}</style><style type="text/css">.WPA3-CONFIRM { z-index:2147483647; width:285px; height:141px; margin:0; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR0AAACNCAMAAAC9pV6+AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjU5QUIyQzVCNUIwQTExRTJCM0FFRDNCMTc1RTI3Nzg4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjU5QUIyQzVDNUIwQTExRTJCM0FFRDNCMTc1RTI3Nzg4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTlBQjJDNTk1QjBBMTFFMkIzQUVEM0IxNzVFMjc3ODgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTlBQjJDNUE1QjBBMTFFMkIzQUVEM0IxNzVFMjc3ODgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6QoyAtAAADAFBMVEW5xdCkvtNjJhzf6Ozo7/LuEQEhHifZ1tbv8vaibw7/9VRVXGrR3en4+vuveXwZGCT///82N0prTRrgU0MkISxuEg2uTUqvEwO2tbb2mwLn0dHOiQnExMacpKwoJzT29/n+qAF0mbf9xRaTm6abm5vTNBXJ0tvFFgH/KgD+ugqtra2yJRSkq7YPDxvZGwDk7O//2zfoIgH7/f1GSV6PEAhERUtWWF2EiZHHNix1dXWLk53/ySLppQt/gID9IAH7Mgj0JQCJNTTj4+QaIi0zNDr/0Cvq9f/s+/5eYGrn9fZ0eYXZ5O3/tBD8/f5udHy6naTV2t9obHl8gY9ubW/19fXq8fXN2uT/5z/h7PC2oaVmZWoqJR6mMCL3+f33KQM1Fhr6NRT9///w/v/ftrjJDQby9vpKkcWHc3vh7vvZ5uvpPycrMEHu7/De7fne5+709voyKSTi7PVbjrcuLTnnNAzHFhD7/P3aDwDfNxTj6vHz9fj09vj3///19/ny9PevuMI9PEPw8/bw8vbx9PdhYWHx8/fy9ff19vj19vny9fjw8/fc6fOosbza5/LX5fDV4+/U4u7S4e3R4O3O3uvd6vTe6vTd6fPb6PPb6PLW5PDZ5/HW5O/Z5vHV5O/T4e7T4u7Y5vHY5fHO3evR4OzP3+vP3uvQ3+xGt/9Lg7Dz9vjv8/X7+/3d5+vi6+7g6ezh6u3w9Pbc5+rt8vTl7fDn7vHr8fP2+Pr3+fv6+/zq8PPc5urb5en4+/7Y5epGsvjN3erW4OXf6+/s8/bn8PPk7vLv9fiAyfdHrO6Aorz09vnx9fnz9Pb09/vv8fVHsfd+zP/jwyLdExFekLipYWLN3OjR3Oa0k5n/9fXX6PDh7vDU4ey6fAzV4+5HOSHIoBP+/v3b6OppaGrT4Ovk6/Lw8PE8P1Pz+v/w8/nZ5vDW4erOztL/LgT3+Pn2+PvY5/Ta5/HvuxfZ5Ojm8f6lrrrI1uPw0iZPT1Sps7r19/iqtLzxKgjZ3N9RVFtQSkbL2ujM2+ku4f1qAAAIDklEQVR42uzcC3ATdR7A8S3QhZajm+RSEmxZEhIT2vKvjU1aWqAPWr1IsRTkoRZb4Qoi6XmFYHued5coQe8wFLSoFOXV0oeIShG13ANURBmoeme9Z6dXnbP34OF517MOUo/7JykNySXZjPP/rzPb37d0y7Yz/5n9zP43u9tNmUnqHBcUqpzUakatf2QaFKqz+lQm5931T0KhWv9uDuNavwMK3XoX43oq+koYXemQxem0WLMv/fYp6Yd1Hou2v39RarHzvBLHsnyWbtmOxyRe9Do7DaWWfjmPYVjWu2CzLo0CnaejyzGUmSm3Yx0fjafi3B1PSzqsszOqHJkYx2bz6iiv7j189j93SqnTzZ5l8+mr61hnazQxg5mZ/XhisRw+6CiVHOK8POW5u7ZKqFZt8/DCV5Q6zdZ+Lw7vVCKMg8oH7cjLY78kJZ2tzdpW/G/rNTq7oihX3i+Xy21yxzy1HSmRXV17zom8s2to2S4pdUCrbfCvYZ1nBdtnGLTZMI4yVSbrU+NZpcdfkznf5Mp9Vkp9qNW2+Newzj7hdLzdZrNx/Z/Ikj9OHkLF86bqO5dYULlHx2L4wz7J1KBtOKFtGFnFOvsF+5ZVqeR5O7J2Lsmy6F3IlfqVRd3p8h55lPzU/ZKpSdu0f/8Jz8IX1qkXjHF6zo95ZL2wZLB87sdoSK/WZ1+403dcrindXS+VTl/xLE+cbhxej0Zn34D36kGJnNWyVGfqnaj4XOe8eZ84fTOLz1pWL9WwTqNgOtZ3Dsip+1b2jecR0nuPzsOnPBamvlGiYZ1nBGrcne3DwTtP8o2XMxGHlDOPJg/vOixvYZ6Ralhnt1B/uqfIe4LMsogfcpb3evpKOXy2zNqL79i7W6JhnW0CNS5M9F4+4JnUq4j7868//3z6Z3OSehS9rHdu2SoLDdskWhQ627pVlZiH43p75sxevjw+Pn55xvQFGo2mR8Fx5UVFiebflUhXZ3vk9pwrNKoQp+TjNJqUjPh4r87sBVOmaDRTemqKUKLK2L1dognrbF9oVpnSEKpJSkmaM/2mjIzlGTfNXqCZgm00SeUo0agyTm6Qrs5egRaqVMYv01hUE9ejSEqZjkvxzau4uCLObDIajd17JRrW2SOQI81oTP/y+jEIKTlWkfRZSkqKZk6PAq+gyrQK/DPVPdv3SDOs83jkmuYnpmMC092zxrAcQlyNQqHorUH4f2PSzs9IN6Ybzbapj0szYZ1cnjWn40wVd69bUdhbiV/HucrKyjErrs+vqMDfNpkriyzMHqnqPBGp1gG5HR9dqtJN2KEiPz9/48Yf4Dbm558/P6PAZDLVmdki3r7ov09IMSEdw0Q5PtUpKlRhHJOpoGDGtVUUmKoKeY7l7M4Bqeo0R+iArt+Or6/kzMIVRg9ORcVVmfP4s6BOlWCYiFhOKS/9sFmCYZ3WCP3HKvdcXk08u6rbbMb7T0HeVZ28vNi6tG71pzcvRizeeQaZllbpFVmnxeHZdVg0f+XvZ1UZsY+qqq4uFldXd3/a5ITkW/567GYdvtrilHZdqzR1DkQo13Pfi0XZfdfNqsvDZ8UrEhIme+pOuCO5Y5VM9v0H/j2TxVOL5ecfkGCRdVpLec+NCw7r3B+bZ0rPW1f2nT9+1PHRyVtW/UiGqz1439qZnkt1jrVKVKclQlbvAxdoft93q2JnFOTlrbtOdk19XeNK1uKZ5eHJapFgWKchfE0TfTeUrauwTh7mCdSp/dtfSr6XjWrs2MfaIMEi6zQswjaLM5GzxDOz8AvVuvHX4KzsOnZf/adWtCgX65S2SFOnKUI6JV96ZTHLDtyY8JtY/CL+7aN9/i4ufeAfa5libuoVF8vqmiQY1nFH1SX8EaEv3FIM60R8KvXiRc9i2rQLOLwcZc/kCumM7kAHdEAHdL4BnR9D4QId0AEd0AEd0AEd0BkFOj+FwgU6AjqPQuECHQGdB6FwgQ7ogA7ogA7ogA7ogA7oQKDztXR+CIULdEAHdEAHdEAHdEAHdEAHAp2vpfMzKFygI6DzCBQu0BHQ+QkULtABHdABHdABHdABnTAx2nZCaZnVm/zjljEDNN99zpSF0NlEuFMxa95pI9Q7a2JGxj1rYKplFOurZgxBm0JBZ9OG4+//klDvH99weGRcxwXZrVR71HGWvk572121hLqrrd0/rltWSzn3JlF0nidUkM7zlBNJp5NQQTqdlBNHp2sSoboCdSZRTiSd1wgVpPMa5cTRWf0qoVYH6rxKuRA6m0nX3naG1JvrzrS1+8d1y2i/l88dtCV0dE49R6hTgTrPUU4kHVI3doN0aN9HFkfnzcOEejNQ5zDlxNFZepBQSwN1DlJOJJ0jhArSOUI5cXROvkKok4E6r1AuhM4W0mGdY4TCOv5x3bJjlHMHbQkdnbfGEeqtQJ1xlBNJ5yihgnSOUk4cndtfJtTtgTovU04cnTduINQbgTo3UC6EzkOkwzovEArr+Md1y16gnDtoS+jojH2JUGMDdV6inDg6h14k1KFAnRcpJ45Ox1hCdQTqjKWcODr3HiLUvYE6hygnkk4HoYJ0Oignhs6G997+FaHefu8D/7iOaT+n2+sOEXRi1hwn9Zvi42tizoyMa0j+1y9o9jpTNoG6zpYjMRtIPWXwQUzXyLibNxscVP/GvaPswf/fdx4m3oQJxIbasuXhbzAqOpIJdAR0JkDhAh3QAR3QAR3QAR3QAZ3RrZNzGRTCdPk2JnUu8ITBmatnqlNzXFCobtOP/58AAwA/1aMkKhXCbQAAAABJRU5ErkJggg==) no-repeat;}.WPA3-CONFIRM { *background-image:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/panel.png);}.WPA3-CONFIRM * { position:static; z-index:auto; top:auto; left:auto; right:auto; bottom:auto; width:auto; height:auto; max-height:auto; max-width:auto; min-height:0; min-width:0; margin:0; padding:0; border:0; clear:none; clip:auto; background:transparent; color:#333; cursor:auto; direction:ltr; filter:; float:none; font:normal normal normal 12px "Helvetica Neue", Arial, sans-serif; line-height:16px; letter-spacing:normal; list-style:none; marks:none; overflow:visible; page:auto; quotes:none; -o-set-link-source:none; size:auto; text-align:left; text-decoration:none; text-indent:0; text-overflow:clip; text-shadow:none; text-transform:none; vertical-align:baseline; visibility:visible; white-space:normal; word-spacing:normal; word-wrap:normal; -webkit-box-shadow:none; -moz-box-shadow:none; -ms-box-shadow:none; -o-box-shadow:none; box-shadow:none; -webkit-border-radius:0; -moz-border-radius:0; -ms-border-radius:0; -o-border-radius:0; border-radius:0; -webkit-opacity:1; -moz-opacity:1; -ms-opacity:1; -o-opacity:1; opacity:1; -webkit-outline:0; -moz-outline:0; -ms-outline:0; -o-outline:0; outline:0; -webkit-text-size-adjust:none;}.WPA3-CONFIRM * { font-family:Microsoft YaHei,Simsun;}.WPA3-CONFIRM .WPA3-CONFIRM-TITLE { height:40px; margin:0; padding:0; line-height:40px; color:#2b6089; font-weight:normal; font-size:14px; text-indent:80px;}.WPA3-CONFIRM .WPA3-CONFIRM-CONTENT { height:55px; margin:0; line-height:55px; color:#353535; font-size:14px; text-indent:29px;}.WPA3-CONFIRM .WPA3-CONFIRM-PANEL { height:30px; margin:0; padding-right:16px; text-align:right;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON { position:relative; display:inline-block!important; display:inline; zoom:1; width:99px; height:30px; margin-left:10px; line-height:30px; color:#000; text-decoration:none; font-size:12px; text-align:center;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON-FOCUS { width:78px;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON .WPA3-CONFIRM-BUTTON-TEXT { line-height:30px; text-align:center; cursor:pointer;}.WPA3-CONFIRM-CLOSE { position:absolute; top:7px; right:7px; width:10px; height:10px; cursor:pointer;}</style>
<div id="ggtab">
     <div class="mian">
          <div class="hbt">公告</div>
          
          <div id="sevedu">
                <div class="selectTag" id="li0"><p>中国清明节（4月4日至4月6日）假期安排通知：除4月5日（星期天）清明节当天放假外，其余时间均有国内客服正<a href="http://www.birdex.cn/Information-14.html" target="_blank" style="cursor: pointer;">查看详情</a></p></div><div class="tagContent" id="li1"><p>关于新增渠道详细介绍公告<a href="http://www.birdex.cn/Information-21.html" target="_blank" style="cursor: pointer;">查看详情</a></p></div><div class="tagContent" id="li2"><p>关于系统问题导致订单申报数据错误的相关处理方案<a href="http://www.birdex.cn/Information-19.html" target="_blank" style="cursor: pointer;">查看详情</a></p></div><div class="tagContent" id="li3"><p>所有笨鸟的客户以及淘友，经过对目前现状的考察和认真考虑，笨鸟不得已今天对税单相关政策作出调整如下：<a href="http://www.birdex.cn/Information-16.html" target="_blank" style="cursor: pointer;">查看详情</a></p></div><ul id="tags1" class="tag"><li obj="li0" class="selectTag"><a href="javascript:void(0)" style="cursor: pointer;"></a></li><li obj="li1"><a href="javascript:void(0)" style="cursor: pointer;"></a></li><li obj="li2"><a href="javascript:void(0)" style="cursor: pointer;"></a></li><li obj="li3"><a href="javascript:void(0)" style="cursor: pointer;"></a></li></ul>
          </div>
     </div>
</div>
<div id="header" class="noticeHeader">
     <div class="mian">
         <div class="pull-right" id="login" style="display:none;">
              <a href="http://passport.birdex.cn/Login.aspx" style="cursor: pointer;">登录</a>|<a href="http://passport.birdex.cn/Register.aspx" style="cursor: pointer;">注册</a>
         </div>
         <div class="pull-right" id="logout" style="">
               <span id="headername">LuFuTian</span><a href="http://passport.birdex.cn/LogOut.aspx" style="cursor: pointer;">退出</a>
          </div>
         <a class="ggnew" style="cursor: pointer;"><span></span>网站最新公告 <i>4</i> 条</a>
     </div>
</div>
<div id="head">
     <div class="mian">
          <a href="http://www.birdex.cn/index.html" class="logo" style="cursor: pointer;"><img src="/lmp/resource/img/logo.jpg" alt="翔锐物流" title="翔锐物流"><span>笨鸟</span></a>
          
          
          <ul class="nav" style="right: 0px;">
              <li val="help" style="padding-right:5px;" class="current"><a href="/lmp/help.jsp" style="cursor: pointer;">帮助中心</a></li>
              <li val="Information" id="headInformation"><a href="/lmp/prohibition.jsp" style="cursor: pointer;">禁运物品</a></li>
              <li val="account" id="headAccount"><a href="/lmp/transport.jsp" style="cursor: pointer;">我的帐户</a></li>
              <li val="index"><a href="/lmp/index.jsp" style="cursor: pointer;">首页</a></li>
          </ul>
     </div>
</div>
    <link rel="stylesheet" type="text/css" href="/lmp/resource/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/lmp/resource/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="/lmp/resource/css/less.css">
    <link rel="stylesheet" type="text/css" href="/lmp/resource/css/style.css">
    <link href="/lmp/resource/css/help.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        body{background-color:transparent;}
        table.tab tr td{height:50px;border:1px solid #000;}
    </style>
         <div class="container" id="content" style="height:auto;min-height:650px;overflow:hidden;width:1020px;">
          <div class="row">
               <div class="col-md-2" id="aside" style="width:168px;float:left;">
                   <div class="panel panel-default">
                        <div class="panel-heading">帮助中心</div>
                        
                        <div class="panel-body">
                             <div class="list-group">
                                  <ul class="nav nav-list" style="right: 0px;">
                                       <li class="active">
                                           <a href="javascript:void(0);" style="cursor: pointer;">新手指南</a>
                                           <ul class="nav nav-list" style="right: 0px;">
                                               <li class="active helpline" item="Process"><a href="javascript:void(0);" style="cursor: pointer;">网站流程</a></li>
                                               <li class="helpline" item="Tax"><a href="javascript:void(0);" style="cursor: pointer;">通关&amp;关税</a></li>
                                               <li class="helpline" item="oneTax"><a href="http://www.birdex.cn/Help/TaxTactics.html" target="_blank" style="cursor: pointer;">一张图看懂通关策略</a></li>
                                               
                                           </ul>
                                       </li>
                                       <li>
                                        <a href="javascript:void(0);" style="cursor: pointer;">常见问题</a>
                                            <ul class="nav nav-list" style="right: 0px;">
                                                <li class="helpline" item="price"><a href="javascript:void(0);" style="cursor: pointer;">价格渠道</a></li>
                                               <li class="helpline" item="service"><a href="javascript:void(0);" style="cursor: pointer;">增值服务</a></li>
                                               <li class="helpline" item="control"><a href="javascript:void(0);" style="cursor: pointer;">网站操作</a></li>
                                               <li class="helpline" item="claim"><a href="javascript:void(0);" style="cursor: pointer;">理赔细则</a></li>
                                            </ul>
                                       </li>
                                  </ul>
                             </div>
                        </div>
                    </div>
              </div>
               
              <div class="col-md-10 Tax helpdetail" style="display:none;width:845px;float:left;">
                   <div class="title">进口税费入门知识 <a href="http://www.birdex.cn/Help/TaxTactics.html" target="_blank" style="color: rgb(0, 151, 219); font-size: 14px; margin-left: 10px; font-weight: bold; cursor: pointer;" title="一张图看懂通关策略">一张图看懂通关策略</a></div>
                   
                   <div class="tax-page">
                        <h5>海关的收税规则？</h5>
                        <p>海关规定“个人邮寄进境物品，海关依法征收进口税。应征进口税税额在人民币50元(含50元)以下的,予以免征,超过50元的一律按商品价值全额征税。”</p>
                        <p><span class="label-warning">注：这里的50元不是指物品价值，而是应缴税额。</span></p>
                        <p>&nbsp;</p>
                        <h5>如何计算进口税税额？</h5>
                        <p class="text-center">进口税的计算公式为：<strong>进口税税额</strong> = <strong>完税价格</strong> × <strong>进口税税率</strong></p>
                        <p><strong>进境物品进行归类：</strong></p>
                        <p>进境商品先按照《进境物品归类表》进行归类，进境物品按以下原则归类。</p>
                        
                        <div class="row guilei">
                             <div class="col-md-5 col-sm-5 col-xs-12 text-right" style="width:350px;">《归类表》已列明物品</div>
                             <div class="col-md-1 col-sm-2 col-xs-12 text-center" style="width:70px;"><img src="/lmp/resource/img/arrowright.png" alt=""></div>
                             <div class="col-md-6 col-sm-5 col-xs-12" style="width:420px;">归入其列明类别</div>
                        </div>
                        
                        <div class="row guilei">
                             <div class="col-md-5 col-sm-5 col-xs-12 text-right" style="width:350px;">《归类表》未列明物品</div>
                             <div class="col-md-1 col-sm-2 col-xs-12 text-center" style="width:70px;"><img src="/lmp/resource/img/arrowright.png" alt=""></div>
                             <div class="col-md-6 col-sm-5 col-xs-12" style="width:420px;">按其主要功能或用途归入相应类别</div>
                        </div>
                        
                        <div class="row guilei">
                             <div class="col-md-5 col-sm-5 col-xs-12 text-right" style="width:350px;">不能按照上述原则归入相应类别的物品</div>
                             <div class="col-md-1 col-sm-2 col-xs-12 text-center" style="width:70px;"><img src="/lmp/resource/img/arrowright.png" alt=""></div>
                             <div class="col-md-6 col-sm-5 col-xs-12" style="width:420px;">归入“其他物品”类别</div>
                        </div>
                        
                        <p><strong>确定完税价格：</strong></p>
                        <p>完成归类后对照《进境物品完税价格表》里面规定的商品完税价格及税率计算税费。完税价格遵循以下原则：</p>
                        
                        <div class="row paixu">
                             <div class="col-md-4 col-sm-4 col-xs-12" style="width:270px;">
                                  <dl class="dl-horizontal">
                                      <dt style="width: 30px; font-weight: 700; float: left; text-align: right; padding-right: 5px;font-size:30px;">1</dt>
                                      <dd style="width: 210px; float: left;margin-left:0;">完税价格表已列明价格物品，<br>清关时按照完税价格表计算商品税费</dd>
                                  </dl>
                             </div>
                             <div class="col-md-4 col-sm-4 col-xs-12" style="width:270px;">
                                  <dl class="dl-horizontal">
                                      <dt style="width: 30px; font-weight: 700; float: left; text-align: right; padding-right: 5px;font-size:30px;">2</dt>
                                      <dd style="width: 210px; float: left;margin-left:0;">完税价格表未列明价格物品，按相同物品相同来源地最近时间的主要市场零售价确定完税价格</dd>
                                  </dl>
                             </div>
                             <div class="col-md-4 col-sm-4 col-xs-12" style="width:270px;">
                                  <dl class="dl-horizontal">
                                      <dt style="width: 30px; font-weight: 700; float: left; text-align: right; padding-right: 5px;font-size:30px;">3</dt>
                                      <dd style="width: 210px; float: left;margin-left:0;">但对于实际价格超过完税价格的2倍及以上，或是低于完税价格的1/2以及以下物品，物品所有人向海关提供销售方开具的真实购物发票及收据凭证，海关依据凭证确定应税物品的完税价格</dd>
                                  </dl>
                             </div>
                        </div>
                        
                        <p><strong>常见物品税率：</strong></p>
                        <p>目前进口税税率共设四档，分别为10%、20%、30%和50%。</p>
                        
                        <div class="table-responsive">
                             <table class="table table-bordered table-hover">
                                   <thead>
                                         <tr>
                                             <th>税率</th>
                                             <th>品类</th>
                                         </tr>
                                   </thead>
                                   <tbody>
                                          <tr>
                                                <td>10%</td>
                                                <td>食品，饮料，皮革服饰及配饰，金银珠宝及制品，艺术品、家用医疗、保健、美容器材，计算机及外围设备，书报及刊物，体育用品类</td>
                                         </tr>
                                         <tr>
                                                <td>20%</td>
                                                <td>纺织品及其制品，表、钟及其配件（高档表除外），摄影设备及配件，电器类厨房用具，电器类卫生用具洁具，自行车，童车及配件</td>
                                         </tr>
                                         <tr>
                                                <td>30%</td>
                                                <td>高档手表，高尔夫球及配件等</td>
                                         </tr>
                                         <tr>
                                                <td>50%</td>
                                                <td>酒，烟，化妆品</td>
                                         </tr>
                                   </tbody>
                              </table>
                         </div>
                         
                         <h5>哪些商品容易被税?</h5>
                         <p>以下几类商品的完税价格和税率较高属于易税商品：高档皮革服饰、高档手表、护肤品、笔记本电脑、单反等数码产品。</p>
                         
                         <h5>进口邮包清关缴税案例?</h5>
                         
                         <div class="table-responsive">
                             <table class="table table-bordered table-hover text-center">
                                   <thead>
                                         <tr>
                                             <th>案例</th>
                                             <th>购买商品</th>
                                             <th>购买数量</th>
                                             <th>购买价格</th>
                                             <th>完税价格</th>
                                             <th>征税价格</th>
                                             <th>税率</th>
                                             <th>关税</th>
                                             <th>是否收税</th>
                                         </tr>
                                   </thead>
                                   <tbody>
                                          <tr>
                                              <td>案例1</td>
                                              <td>运动鞋</td>
                                              <td>1</td>
                                              <td>$190</td>
                                              <td>$200</td>
                                              <td>$200</td>
                                              <td>10%</td>
                                              <td>$20</td>
                                              <td>关税小于50，不需要征税</td>
                                         </tr>
                                         <tr>
                                              <td>案例2</td>
                                              <td>笔记本电脑</td>
                                              <td>1</td>
                                              <td>$2500</td>
                                              <td>$2000</td>
                                              <td>$2000</td>
                                              <td>10%</td>
                                              <td>$200</td>
                                              <td>关税大于50，需要征税</td>
                                         </tr>
                                         <tr>
                                              <td>案例3</td>
                                              <td>数码相机</td>
                                              <td>1</td>
                                              <td>$800</td>
                                              <td>$200</td>
                                              <td>$800</td>
                                              <td>10%</td>
                                              <td>$80</td>
                                              <td>关税大于50，需要征税</td>
                                         </tr>
                                   </tbody>
                              </table>
                         </div>
                         
                         <p><strong>案例1</strong>：运动鞋完税价格为200元/双，税率10%，购买价格在完税价格上下50%范围内，税额按完税价格计算为200*10%=20元，税额未超过50元，不需征税。</p>
                         <p><strong>案例2</strong>：笔记本电脑完税价格为2000元/台，税率10%，购买价格在完税价格上下50%范围内，税额按完税价格计算为2000*10%=200元，税额超过50元，需征税。</p>
                         <p><strong>案例3</strong>：数码相机完税价格为2000元/个，税率10%，按完税价格算为2000*10%=200元，但数码相机购买价格为800元，低于完税价格的50%，提供订单小票及购物截图，按实际购买价格算为800*10%=80元</p>
                         <p><span class="label-warning">注：如果同时购买多个商品，计算出的单个商品关税相加即为要缴纳的关税。</span></p>

                         <p>注：附件<a href="http://imgs.birdex.cn/file/down/20150312.doc" target="_blank" style="text-decoration: underline; cursor: pointer;">《中华人民共和国进境物品完税价格表》</a>。（点击下载）</p>
                   </div>
              </div>

              <div class="col-md-9 Process helpdetail" style="width:848px;float:left;">
                <div class="title">网站流程</div>
                <div class="help" id="Steps01">
                     <ul class="nav-sz">
                         <li class="first-child"><a class="step step1" index="1" showimg="http://img.cdn.birdex.cn/images/help_01.jpg" aimg="http://img.cdn.birdex.cn/images/help01.jpg" aclickimg="http://img.cdn.birdex.cn/images/help01_h.jpg" showh2="&lt;span&gt;1.&lt;/span&gt;注册笨鸟账户" showp="关注笨鸟微信公众号或官网活动获取邀请码注册哦" style="cursor: pointer;"><img src="/lmp/resource/img/help01_h.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step2" index="2" showimg="http://img.cdn.birdex.cn/images/help_02.jpg" aimg="http://img.cdn.birdex.cn/images/help02.jpg" aclickimg="http://img.cdn.birdex.cn/images/help02_h.jpg" showh2="&lt;span&gt;2.&lt;/span&gt;获取仓库地址" showp="进入我的账户 &gt; 海外仓库地址，&lt;br/&gt;查看笨鸟在美国波特兰仓库地址" style="cursor: pointer;"><img src="/lmp/resource/img/help02.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step3" index="3" showimg="http://img.cdn.birdex.cn/images/help_03.jpg" aimg="http://img.cdn.birdex.cn/images/help03.jpg" aclickimg="http://img.cdn.birdex.cn/images/help03_h.jpg" showh2="&lt;span&gt;3.&lt;/span&gt;海外网站下单" showp="海外网站购物时，&lt;br/&gt;收件地址填写笨鸟美国仓库地址" style="cursor: pointer;"><img src="/lmp/resource/img/help03.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step4" index="4" showimg="http://img.cdn.birdex.cn/images/help_04.jpg" aimg="http://img.cdn.birdex.cn/images/help04.jpg" aclickimg="http://img.cdn.birdex.cn/images/help04_h.jpg" showh2="&lt;span&gt;4.&lt;/span&gt;创建转运预报" showp="登录笨鸟网站 &gt; 我的帐户 &gt; +新包裹，&lt;br/&gt;填写海外快递单号" style="cursor: pointer;"><img src="/lmp/resource/img/help04.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step5" index="5" showimg="http://img.cdn.birdex.cn/images/help_05.jpg" aimg="http://img.cdn.birdex.cn/images/help05.jpg" aclickimg="http://img.cdn.birdex.cn/images/help05_h.jpg" showh2="&lt;span&gt;5.&lt;/span&gt;仓库揽收包裹" showp="笨鸟仓库对收到包裹进行扫码及称重" style="cursor: pointer;"><img src="/lmp/resource/img/help05.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step6" index="6" showimg="http://img.cdn.birdex.cn/images/help_06.jpg" aimg="http://img.cdn.birdex.cn/images/help06.jpg" aclickimg="http://img.cdn.birdex.cn/images/help06_h.jpg" showh2="&lt;span&gt;6.&lt;/span&gt;支付运费" showp="支付运费后包裹会下架出库并发送至机场" style="cursor: pointer;"><img src="/lmp/resource/img/help06.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step7" index="7" showimg="http://img.cdn.birdex.cn/images/help_07.jpg" aimg="http://img.cdn.birdex.cn/images/help07.jpg" aclickimg="http://img.cdn.birdex.cn/images/help07_h.jpg" showh2="&lt;span&gt;7.&lt;/span&gt;空运至..." showp="包裹将被空运至最合适的清关口岸" style="cursor: pointer;"><img src="/lmp/resource/img/help07.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step8" index="8" showimg="http://img.cdn.birdex.cn/images/help_08.jpg" aimg="http://img.cdn.birdex.cn/images/help08.jpg" aclickimg="http://img.cdn.birdex.cn/images/help08_h.jpg" showh2="&lt;span&gt;8.&lt;/span&gt;海关清关" showp="跟进清关情况，包裹转交国内快递" style="cursor: pointer;"><img src="/lmp/resource/img/help08.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step9" index="9" showimg="http://img.cdn.birdex.cn/images/help_09.jpg" aimg="http://img.cdn.birdex.cn/images/help09.jpg" aclickimg="http://img.cdn.birdex.cn/images/help09_h.jpg" showh2="&lt;span&gt;9.&lt;/span&gt;国内快递" showp="国内快递送货，坐等收货吧" style="cursor: pointer;"><img src="/lmp/resource/img/help09.jpg" alt="笨鸟"></a></li>
                         <li class="xx"></li>
                         <li><a class="step step10" index="10" showimg="http://img.cdn.birdex.cn/images/help_10.jpg" aimg="http://img.cdn.birdex.cn/images/help10.jpg" aclickimg="http://img.cdn.birdex.cn/images/help10_h.jpg" showh2="&lt;span&gt;10.&lt;/span&gt;用户签收" showp="收货后登陆笨鸟网站晒单有优惠券哦！" style="cursor: pointer;"><img src="/lmp/resource/img/help10.jpg" alt="笨鸟"></a></li>
                     </ul>
                     
                     <ul class="nav-wz">
                         <li class="blue" style="margin-left:-18px;">注册笨鸟账户</li>
                         <li>获取仓库地址</li>
                         <li>海外网站下单</li>
                         <li>创建转运预报</li>
                         <li>仓库揽收包裹</li>
                         <li>支付运费</li>
                         <li>空运至...</li>
                         <li>清关通过</li>
                         <li>国内快递</li>
                         <li>用户签收</li>
                     </ul>
                     
                     <div class="row">
                          <div class="left">
                               <h2 id="showH2"><span>1.</span>注册笨鸟账户</h2>
                               <p id="showP">关注笨鸟微信公众号或官网活动获取邀请码注册哦</p>
                          </div>
          
                          <div class="right text-center">
                               <a class="help_arr_left" item="1" style="cursor: pointer;"><img src="/lmp/resource/img/help_arr_left.png" alt="笨鸟"></a>
                               <a class="help_arr_right" item="2" style="cursor: pointer;"><img src="/lmp/resource/img/help_arr_right.png" alt="笨鸟"></a>
                               <img id="showContent" src="/lmp/resource/img/help_01.jpg" alt="笨鸟">
                          </div>
                     </div>
                </div>
              </div>

              <div class="col-md-10 price helpdetail" style="display:none;width:845px;float:left;">
                   <div class="title">价格渠道服务篇</div>
                   
                   <div class="tax-page">
                        <p>&nbsp;</p>
                        <h5>笨鸟标准服务（非邮盟清关）</h5>
                        <p>根据笨鸟现有非邮盟渠道进行随机分配国内海关清关口岸（目前暂有天津，广州两个口岸），不收体积费，不包税，税费为代缴方式。国内由中通或EMS安排派送，整体时效7-15工作日。</p>
                        <p>温馨提示：建议包裹内相同的商品数量不超过3-5个，每个包裹的价值不超过RMB<span style="background-color:Yellow;">1000</span> 元。不可分割的包裹不超过RMB<span style="background-color:Yellow;">3000</span>元。包裹单边尺寸不大于<span style="background-color:Yellow;">120CM</span>。包裹合箱重量最好不超过<span style="background-color:Yellow;">5KG</span>，单件不可分割商品最重不要超过<span style="background-color:Yellow;">20KG</span>。如有特殊情况可与在线客服进行确认。另外如手表，手机，笔记本电脑等价值偏高的商品，建议一个包裹一台，不与其他商品合箱。以免因数量太多而影响清关或退运。
                        </p>
                        <p>举例说明1-2.1KG内的包裹价格，以便大家方便核算，显示如下：</p>
                        <table border="0" bgcolor="000000" cellspacing="1" width="520px" style="text-align:center;margin: 10px 0 10px  150px;" class="tab">
                            <tbody><tr bgcolor="ffffff">
                                    <td>
        	                            <div style="text-align:right;font-size:13px;height: 30px;line-height:30px;">会员等级</div>
                                        <div style="text-align:left;font-size:13px;height: 30px;line-height:30px;">重量分段</div>
                                    </td>
                                    <td>基础会员</td>
                                    <td>高级会员</td>
                                    <td>是否可用优惠券</td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.00--1.10KG</td>
                                    <td><p>76（合箱）</p><p style="margin-top:-10px;">70（单票）</p></td>
                                    <td><p>64（合箱）</p><p style="margin-top:-10px;">60（单票）</p></td>
                                    <td rowspan="3">可以</td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.11--1.60KG</td>
                                    <td><p>114（合箱）</p><p style="margin-top:-10px;">105（单票）</p></td>
                                    <td><p>96（合箱）</p><p style="margin-top:-10px;">90（单票）</p></td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.61--2.10KG</td>
                                    <td><p>152（合箱）</p><p style="margin-top:-10px;">140（单票）</p></td>
                                    <td><p>128（合箱）</p><p style="margin-top:-10px;">120（单票）</p></td>
                            </tr>
                        </tbody></table>
                        <p>注解：<span style="background-color:Yellow;">分段重量核算，不够1KG按1KG计费，1.01-- 1.10KG按1KG计费，1.11--1.60KG按1.5KG计费，1.61---2.10KG按2KG计费，以此类推。</span>（100G以内抹零不计）</p>
                        <p>&nbsp;</p>
                        <h5>笨鸟香港自提</h5>
                        <p>此转运方式是由美国仓库出库发出，抵达香港仓库后系统会发出提货通知短信（客户已验证手机号码的账户），由收件人提供提货码或代理收件人（需具备收件人可提货资料信息）在规定的时间范围内去仓库提取货物；无提货登记费。时效约为5至7个工作日。</p>
                        <p>仓库地址：<span style="background-color:Yellow;">香港屯门石排路5号伟昌工业大厦8F</span>（仓库联系电话目前处于申请阶段，待申请下来会尽快告知）</p>
                        <p>举例说明1-2.1KG内的包裹价格，以便大家方便核算，显示如下：</p>
                        <table border="0" bgcolor="000000" cellspacing="1" width="520px" style="text-align:center;margin: 10px 0 10px  150px;" class="tab">
                            <tbody><tr bgcolor="ffffff">
                                    <td>
        	                            <div style="text-align:right;font-size:13px;height: 30px;line-height:30px;">会员等级</div>
                                        <div style="text-align:left;font-size:13px;height: 30px;line-height:30px;">重量分段</div>
                                    </td>
                                    <td>基础会员</td>
                                    <td>高级会员</td>
                                    <td>是否可用优惠券</td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.00--1.10KG</td>
                                    <td><p>54（合箱）</p><p style="margin-top:-10px;">50（单票）</p></td>
                                    <td><p>44（合箱）</p><p style="margin-top:-10px;">40（单票）</p></td>
                                    <td rowspan="3">可以</td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.11--1.60KG</td>
                                    <td><p>81（合箱）</p><p style="margin-top:-10px;">75（单票）</p></td>
                                    <td><p>66（合箱）</p><p style="margin-top:-10px;">60（单票）</p></td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.61--2.10KG</td>
                                    <td><p>108（合箱）</p><p style="margin-top:-10px;">88（单票）</p></td>
                                    <td><p>100（合箱）</p><p style="margin-top:-10px;">80（单票）</p></td>
                            </tr>
                        </tbody></table>
                         <p>注解：<span style="background-color:Yellow;">与标准服务计费重量一致（100G以内抹零）。</span>（详情可见标准服务介绍）</p>
                        <p>&nbsp;</p>
                        <h5>笨鸟香港E特快（邮盟清关）</h5>
                        <p>包裹到达香港后提货更换EMS面单交邮局转寄国内，到达收件地址就近可代理邮局或就近邮政海关监管仓清关，产生税费由当地海关出具税单并收取；正常情况下时效约7--15个工作日。特殊时期无法确认整体时效（要看当地清关及中转情况而定）。</p>
                        <p>渠道禁运商品：<span style="background-color:Yellow;">长度大于或者等于70cm的商品，体积较大商品如：行李箱，安全座椅等单一不可分割大件商品；奶粉，以及重量大于等于7KG的商品；</span></p>
                        <p>举例说明1-2KG内的包裹价格，以便大家方便核算，显示如下：</p>
                         <table border="0" bgcolor="000000" cellspacing="1" width="520px" style="text-align:center;margin: 10px 0 10px  150px;" class="tab">
                            <tbody><tr bgcolor="ffffff">
                                    <td>
        	                            <div style="text-align:right;font-size:13px;height: 30px;line-height:30px;">会员等级</div>
                                        <div style="text-align:left;font-size:13px;height: 30px;line-height:30px;">重量分段</div>
                                    </td>
                                    <td>基础会员</td>
                                    <td>高级会员</td>
                                    <td>是否可用优惠券</td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.00KG</td>
                                    <td><p>76（合箱）</p><p style="margin-top:-10px;">70（单票）</p></td>
                                    <td><p>64（合箱）</p><p style="margin-top:-10px;">60（单票）</p></td>
                                    <td rowspan="3">不可以</td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.01--1.50KG</td>
                                    <td><p>114（合箱）</p><p style="margin-top:-10px;">105（单票）</p></td>
                                    <td><p>96（合箱）</p><p style="margin-top:-10px;">90（单票）</p></td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>1.51--2.00KG</td>
                                    <td><p>152（合箱）</p><p style="margin-top:-10px;">140（单票）</p></td>
                                    <td><p>128（合箱）</p><p style="margin-top:-10px;">120（单票）</p></td>
                            </tr>
                        </tbody></table>
                         <p>注解：<span style="background-color:Yellow;">分段重量核算，不够1KG按1KG计费，1.01-- 1.50KG按1.5KG计费，1.51—2.00KG按2KG计费，以此类推。与标准服务计费单价一致，但100G以内不抹零。</span></p>
                         <p>温馨提示:因目前国内有三大EMS处理中心：上海、北京、广州，经转地区如下表所示（具体情况以E特快实际中转为准）：</p>
                         <p>但是经由上海EMS处理中心分拨中转的周边城市会受上海EMS处理中心货物量的影响而导致不同程度的收货时效延缓情况；同时福建省、武汉等根据当地海关会收取代理清关费用；另涉及售后查询等事宜将会延长，<span style="background-color:Yellow;">高价值货物建议购买保险，请大家根据自身情况谨慎选择</span></p>
                         <table border="0" bgcolor="000000" cellspacing="1" width="520px" style="text-align:center;margin: 10px 0 10px  150px;" class="tab">
                            <tbody><tr bgcolor="ffffff">
                                    <td style="width:120px;">上海</td>
                                    <td>上海，浙江，江苏，安徽，湖南，湖北，江西，福建等</td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>北京</td>
                                    <td>北京，河北，河南，山西，山东，陕西，哈尔滨，内蒙古等北方城市</td>
                            </tr>
                            <tr bgcolor="ffffff">
                                    <td>广州</td>
                                    <td>广东，广西，海南等</td>
                            </tr>
                        </tbody></table>
                        <p>&nbsp;</p>
                        <h5>一般什么时间出库？</h5>
                        <p>美国时间周一至周五正常操作，付款后一般1至2个工作日安排出库。如果标准服务渠道同个收件人有多个包裹，那一批只能走一个包裹，其他包裹顺延至下个工作日出。</p>
                        <p>&nbsp;</p>
                        <h5>你好，请问你们有能否接受香港澳门地址？</h5>
                        <p>你好，抱歉，标准服务渠道是不接受香港澳门的收件地址，只接受中国大陆的派送地址服务。</p>
                        <p>&nbsp;</p>
                        <h5>你好，请问可以转运手机和电脑么？</h5>
                        <p>可以的，标准渠道中的广州口岸可以转运，此类物品不承运二手的，建议一个包裹一台，不与其他商品合箱。另外建议购买保险。</p>
                        <p>&nbsp;</p>
                        <h5>剃须刀可以转运么？</h5>
                        <p>可以转运，一个包裹最好不要超过2个。但含酒精清洁液的需取出方可转运，因其液体是不允许上机。如博朗790CC款。建议购买时查看商品说明。</p>
                        <p>&nbsp;</p>
                        <h5>如何办理退货？</h5>
                        <p>请第一时间与商家联系，经过商家同意再与我们客服联系。退货收取退货服务费，基础会员30元/票。</p>
                        <p>1）	有提供退货标签的网站：将申请到的已付费的退货标签或上门取件的退货标签提供给客服（如亚马逊），扣好退货费用，客服登记好给仓库办理即可。</p>
                        <p>2）	无退货标签的网站：将申请到的退货码和退货地址提供给客服，及网站的一些退货要求详细告知客服，邮费另计的，除了退货费用之外，要另预付100元的邮费给我们，多退少补。客服登记好给仓库办理，仓库办理好后次日将寄出的单号告知客户。</p>
                    </div>
              </div>

              <div class="col-md-10 service helpdetail" style="display:none;width:845px;float:left;">
                   <div class="title">增值服务介绍篇</div>
                   
                   <div class="tax-page">
                        <h5>合箱，分箱及跨包裹合箱介绍</h5>
                        <p>合箱，是指商家发了好几个包裹，你要将这几个合在一起生成新的包裹，按一票货出。这叫合箱。按合箱转运价格计费。</p>
                        <p>例如，小刘是国内代购卖家，他同时在亚马逊为3个用户采购了2个包裹，分别是A包裹和B包裹。 客户1 需要A包裹里的1件货物和B包裹的2件货物，我们就把这种服务叫跨包裹合箱。</p>
                        <p>分箱：是指包裹超过自用范围数量，包裹超重。根据情况进行将其包裹进行拆分。分箱独立收费，如一个包裹要拆分成两箱，则要收取20元的分箱费。分三箱就30，以此类推。</p>
                        <p>&nbsp;</p>
                        <p>清点服务：根据客户申报资料及入库收到实际商品进行核查，核查内容包括数量，产品属性，以及产品是否有破损等，如有异常会及时通知客户反馈。</p>
                        <p>&nbsp;</p>
                        <p>取发票：取出商家平台打印的发货清单（即发票） 基础会员：10元/票</p>
                        <p>&nbsp;</p>
                        <p>购买保险：是否购买客人自选择，无强制要求，对于高价值商品建议购买保险。保险目前普货保险费率是1.5%，高价值电子产品及小家电保险费率是3%。最低消费是22一票。例如：一笔记本电脑，货值申报3000RMB。那保险费就是3000*3%=90元。</p>
                        <p>&nbsp;</p>
                        <p>加固服务：根据客人所买商品进行合理加固，加固材料：牛皮纸，气泡膜，外套纸箱等。仓库会根据商品类别不同进行合理处理。</p>
                        <p>&nbsp;</p>
                        <p>去鞋盒：此项属免费服务，客户在下预报的时候可以勾选上，并根据自身购买鞋款进行合理选择。一般皮鞋，皮靴等去掉易变形类不建议去除。</p>
                        <p>&nbsp;</p>
                        <p>拍照服务：拍照服务按清点服务价格收取。如需拍照的，请与客服沟通处理。</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                   </div>
              </div>

              <div class="col-md-10 control helpdetail" style="display:none;width:845px;float:left;">
                   <div class="title">网站操作篇</div>
                   
                   <div class="tax-page">
                        <p>&nbsp;</p>
                        <h5>你好，如何使用优惠券？</h5>
                        <p>您好，登录账户后在左上方会显示优惠券张数，在你进行支付的时候系统会自动进行使用，在你的账户里也可以看得到使用的次数。有效期是三个月，过期无效。在【账户设置】里面可以看到有效时间，付一次款用一张，用完即止。</p>
                        <p>&nbsp;</p>
                        <h5>如何能成为你们的高级会员？</h5>
                        <p>24小时内充值5000元人民币可直接升级为永久高级会员，享受高级会员的服务和优惠。首次充值的5000元可作为帐户余额抵扣运费、增值费和关税。</p>
                        <p>&nbsp;</p>
                        <h5>你好，请问你们合箱重量是多个包裹费用叠加计费么？</h5>
                        <p>您好，目前我们合箱后的计费为准。您支付是根据你多个包裹的重量叠加的总重量支付，出库称完重之后如果重量有差会把多收的费用第一时间退至您的笨鸟账户钱包里。</p>
                        <p>&nbsp;</p>
                        <h5>海外收货地址如何填写？</h5>
                        <p>注册成功后，在海外仓库页面可以看到美国仓库地址，收件人一定要填写FIRST NAME+LAST NAME，另外有些网站C/O字符保存不了的可以不用填写，有些网站县/郡那栏也是可以不用填的。客户根据实际购买的网站要求格式进行对应填写即可。</p>
                        <p>&nbsp;</p>
                        <h5>您好，我是海淘新手，请问要怎么样才能使用笨鸟转运呢？</h5>
                        <p>1．登录主页注册成为我们的会员。</p>
                        <p>2．点击海外仓库完善个人信息。</p>
                        <p>3．到海外商家平台下单，填写海外仓库上面的笨鸟仓库的地址。</p>
                        <p>4．待商家发货后有了物流号，点“新包裹”进行添加。将购买的产品如实填写清楚进行如实申报。提醒：不要写成<span style="background-color:Yellow;">商家订单号</span>，填写物流号的时候，只要写物流号，不用将物流公司的名字一起写上。如：UPS发货，物流号是：1ZR2E*****，就只写后面的那长串编号即可。如需合箱的包裹点“需合箱”。</p>
                        <p>5．填写收件地址并上传身份证图片保存。</p>
                        <p>6．待商品入库后，第一时间安排支付运费，若需合箱的勾选好包裹合并后再进行支付。仓库就会第一时间接受指令进行操作出库了。</p>
                        <p>7． 如产生关税，会收到邮件，第一时间在笨鸟账户里支付即可。</p>
                        <p>8．坐等收货。</p>
                        <p>&nbsp;</p>
                        <h5>你好，请问账户是否要激活，怎么绑定手机？</h5>
                        <p>您好，注册成功后，账户左上角有【激活】，点击即可激活。手机通过验证即可绑定手机。操作：点【海外仓库】-输入名字与手机，生成FIRST NAME(检查一下是否有误，如果有错请及时通知在线客服更正），再点【账户设置】----手机号的右边有个【验证】，验证即可。 好处：能及时收到相关活动信息以及货物状态的相关提示信息。但不激活也不影响您的账户使用。另外还可以绑定我们的微信【翔锐物流】，绑定后微信也会推送订单最新状态。</p>
                        <p>&nbsp;</p>
                        <h5>如何进行合箱操作？分箱如何操作？</h5>
                        <p>您好，预报的时候可以点击“需合箱”按纽，待其他要合箱的包裹全部<span style="background-color:Yellow;">入库后</span>，再选中打勾要合箱的包裹号后，再点击下面的“合并”按纽，随即生成新的包裹，仓库便会按指令进行合箱操作。合箱如果发现有问题，在20分钟内可自行取消，即可还原。</p>
                        <p>分箱：包裹入库后，在包裹右边有个分箱图标，点击后进行操作。</p>
                        <p>&nbsp;</p>
                        <h5>你好，如何上传身份证么？</h5>
                        <p>在您填写收货地址管理中点击上传正面和上传反面即可，不可以后补。身份证的人名要与收件人名保持一致，身份证上传可加水印。要求JPG或PNG格式，大小不要超过200KB。</p>
                        <p>&nbsp;</p>
                        <h5>货入库了为何不见支付按纽，要如何支付？</h5>
                        <p>无预报入库的包裹，请先点【添加】或【编辑】将订单里的产品信息全部填好再点【完成】，再点右边的【补充地址】保存地址，才会弹出支付按纽，再进行支付即可。</p>
                        <p>&nbsp;</p>
                        <h5>如何修改收件地址？</h5>
                        <p>入库后付款前是可以在<span style="background-color:Yellow;">订单里的【修改地址】</span>，进行修改，付款后则不可进行修改。</p>
                        <p>&nbsp;</p>
                        <h5>如何更改转运渠道？</h5>
                        <p>在生成转运单CN号之前，是可以点击物流号旁边的提示，进行重新选择渠道，并进行支付。如果已经付款仓库并已打单操作，则无法更改。</p>
                        <p>&nbsp;</p>
                        <h5>如何跟踪包裹最新状态？</h5>
                        <p>在订单里的右下方点【历史轨迹】可以查看到最新的物流状态。</p>
                        <p>&nbsp;</p>
                        <h5>如何提交晒单申请？</h5>
                        <p>【已交付】的订单里面有【晒单】提交晒单地址即可。审核通过后，会有券返的。<span style="background-color:Yellow;">注：仔细查阅晒单要求规则。</span></p>
                        <p>&nbsp;</p>
                        <h5>如何申请提现？</h5>
                        <p>登录账户后，左上角有个【充值】按纽，点击进去后旁边就有个【提现】，账户里有可退余额即可申请，提交申请后，提交的金额会冻结，客服及财务审核通过后即可转到您指定的支付宝账户，处理时间约2--5个工作日。高级会员若要将余额提现，则会降级为基础会员，专业会员提现则会降为高级会员。无手续费。如无消费记录提现的，请单独与客服说明情况方可审核通过。<span style="background-color:Yellow;">注：疑涉及洗钱违法行为的信用卡充值不可提现。</span></p>
                        <p>&nbsp;</p>
                        <h5>如何交税及查看相关税费及税单？</h5>
                        <p>1、香港邮盟线路税单由邮政提供，通常会在客户收件的同时获得，税单信息理论上可以通过12360热线或网站直接查询到，该税单由中国邮政负责提供，如有发生争议，笨鸟可以协助处理但无法承诺结果。</p>
                        <p>2、所有非邮盟线路目前都不能够在包裹被税的同时获得税单，同时12360电话热线及网站都无法查到，笨鸟会尽最大努力帮助用户获取税单，但是由于目前税单处理流程依然还不标准，笨鸟不承诺一定能获取到税单，如果能够获得税单通常情况下税单的时间是1-6个月,回来后我们会统一上传到系统里的，您到时候可以点开关税费用那里看到税单扫描件,验证邮箱的用户可收到税单上传邮件；如果需正本寄出，请要再与我们客服人员联系，邮费另计。</p>
                        <p>（上述公告从2015年1月20日凌晨开始生效，详细公告内容未来两天内会更新到公司网站和相关地方，更新的公告在文字描述上可能会有差异，笨鸟保留公告内容变更无需通知用户的权利和公告的解释权）</p>
                        <p>&nbsp;</p>
                        </div>
              </div>

              <div class="col-md-10 claim helpdetail" style="display:none;width:845px;float:left;">
                   <div class="title">理赔细则</div>
                   
                   <div class="tax-page">
                        <h5>加固商品全部破损：</h5>
                        <p>破损导致完全无法正常使用，按申报价值全赔。最高金额不超过3000RMB。</p>
                        <p>&nbsp;</p>
                        <h5>加固商品部分破损：</h5>
                        <p>破损商品占整个包裹的比例进行赔偿申报价值，最高不超过3000RMB。</p>
                        <p>&nbsp;</p>
                        <h5>未加固全部破损：</h5>
                        <p>完全不能正常使用的最高赔偿申报价值的50%。</p>
                        <p>&nbsp;</p>
                        <h5>未加固部分破损：</h5>
                        <p>最高赔偿申报价值的30%。</p>
                        <p>外包装破损并未影响实际货物功能的(非物流包装)，有加固的，最高按申报金额的10%进行赔偿。未加固的不予理赔。</p>
                        <p>&nbsp;</p>
                        <h5>漏液理赔：</h5>
                        <p>未加固商品漏液不予赔偿。已加固的根据漏液比例进行赔偿，最高比例不超过申报价值的30%。</p>
                        <p>&nbsp;</p>
                        <h5>丢失理赔：</h5>
                        <p>未购买保险若货物全部丢失，按货物申报金额全赔+运费，最高不超过3000RMB，部分丢失，按丢货商品的申报金额，最高不超过3000RMB。已购保险若货物全部丢失，按货物申报金额全赔+运费，最高不超过7000RMB，若部分丢失，按丢失商品申报金额，最高不超过7000RMB。若购买了清点服务，出现部分丢失，则按丢失原则进行赔偿。</p>
                        <p>&nbsp;</p>
                        <h5>差重理赔：</h5>
                        <p>在您收到包裹后不分箱的情况下复称，差重在100克以内不予受理。100克以上（含100克）可向客服申请差重运费补偿。申请所要提供图片：包裹完整面单图片，不拆包完整称重图片（重量精确到小数点后两位）。</p>
                        <p>&nbsp;</p>
                        <h5>索赔流程：</h5>
                        <p>签收之日起15天内提出,过期不予受理。要提交的资料有：收到产品的外包装图片，内件商品图片，海外平台购买的订单截图发送到SERVICE@BIRDEX.CN,标题为CN号+丢件/少件/破损索赔，收到后将在三个工作日内进行处理回复<span style="background-color:Yellow;">【倘若提供凭证不足将不受理赔付】</span>。 赔偿汇率按当天汇率进行赔付。</p>
                        <p>&nbsp;</p>
                        <h5>免责条款：</h5>
                        <p>1.易碎商品：无论是否有选择加固，破损或漏液不予赔偿，丢失理赔。（如玻璃制品）。</p>
                        <p>2.包裹内赠品、小样丢失或损坏不受理赔付</p>
                        <p>3.天气自然灾害,暴乱,战争等不可抗因素造成的损失</p>
                        <p>4.因清关时效、航班延误、快递延误、等导致的商品性质变化过保质期、过保修期等不可抗因素</p>
                        <p>5.被海关没收(如未能通过海关商检)</p>
                        <p>6.禁运物品转寄或退货造成的丢件,破损</p>
                        <p>7.未预报商品</p>
                        <p>8.商家发货漏发,少发,错发</p>
                        <p>9.禁运物品退货或转寄至其他转运公司导致的丢失,破损</p>
                        <p>&nbsp;</p>
                   </div>
              </div>
        </div>
      
     </div>
             <script type="text/javascript">
                 $(function () {
                     $("a").css("cursor", "pointer");
                     $(".step").click(function () {
                         var showh2 = $(this).attr("showh2");
                         var showp = $(this).attr("showp");
                         var showImg = $(this).attr("showImg");
                         $(".step").find("img").each(function () {
                             $(this).attr("src", $(this).parent().attr("aImg"));
                         });
                         $(this).find("img").attr("src", $(this).attr("aClickImg"));

                         $("#showH2").html(showh2);
                         $("#showP").html(showp);
                         $("#showContent").attr("src", showImg);

                         var index = parseInt($(this).attr("index"));
                         var left = 0, right = 0;
                         if (index == 1) {
                             left = 1;
                             right = 2;
                         } else if (index == 10) {
                             left = 9;
                             right = 10;
                         } else {
                             left = index - 1;
                             right = index + 1;
                         }
                         $("a.help_arr_left").attr("item", left);
                         $("a.help_arr_right").attr("item", right);

                         $("ul li").removeClass("blue");
                         $("ul.nav-wz li:eq(" + (index - 1) + ")").addClass("blue");
                     });
                     $("a.help_arr_left").click(function () {
                         var item = $(this).attr("item");
                         $(".step" + item).click();
                     });
                     $("a.help_arr_right").click(function () {
                         var item = $(this).attr("item");
                         $(".step" + item).click();
                     });

                     $(".helpline").click(function () {
                         var item = $(this).attr("item");
                         if (item != "oneTax") {
                             $(".helpline").removeClass("active");
                             $(this).addClass("active");
                             $(".helpdetail").hide();
                             $("." + item).show();
                         } else {
                             $(this).find("a").css("background-color", "transparent").css("color", "#434343");
                         }
                     });
                 });
    </script>
    <div id="foot">
     <div class="mian">
          <span class="pull-right"><img src="/lmp/resource/img/logo.gif" alt="翔锐物流"></span>
          
          <div class="pull-left">
              <h5>联系我们</h5>
              <p>4008-890-788</p>
              <p>0755-86054577</p>
              <p id="QQ"><!-- WPA Button Begin -->
	<script charset="utf-8" type="text/javascript" src="/lmp/resource/img/wpa.php"></script>
<!-- WPA Button End --></p>
          </div>
          <div class="pull-left">
              <h5>邮件</h5>
              <p><a href="mailto:service@birdex.cn" style="color: rgb(255, 255, 255); cursor: pointer;">service@birdex.cn</a></p>
          </div>
          <div class="pull-left" style="width:200px">
            <h5>友情链接</h5>
            <a href="http://www.etao.com/tuan/index.html?partnerid=4852" target="_blank" style="color: rgb(255, 255, 255); cursor: pointer;">一淘海淘团</a><br>
<a href="http://www.smzdm.com/" target="_blank" style="color: rgb(255, 255, 255); cursor: pointer;">什么值得买</a><br>
            <a href="http://www.55haitao.com/" target="_blank" style="color: rgb(255, 255, 255); cursor: pointer;">55海淘</a>
            
          </div>
     </div>
    <div class="mian" style="margin:0 auto;text-align:center;color:#818181;font-size:14px;vertical-align:bottom;">
        <div>粤ICP备14015306号-1</div> 
     </div>
</div>
<div style="display:none;">
<script type="text/javascript">
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F68c503fc431b1f102540a9c79f1ffc78' type='text/javascript'%3E%3C/script%3E"));
</script>
<script src="/lmp/resource/js/h.js" type="text/javascript"></script>
<a href="http://tongji.baidu.com/hm-web/welcome/ico?s=68c503fc431b1f102540a9c79f1ffc78" target="_blank" style="cursor: pointer;">
<img border="0" src="/lmp/resource/img/21.gif" width="20" height="20"></a>
</div>
<div id="lrToolRb" class="lr-tool-rb" style="z-index:10000;">
<a class="lr-gotop" title="返回顶部" style="cursor: pointer;"><span class="lricon iconArrow"></span><br>TOP</a>
<a class="lr-QQ" title="单击联系QQ客服" onclick="SetQQ();" style="cursor: pointer;"><span class="lricon"></span><br>联系客服</a>
<a class="lr-qrcode" title="关注获取最新优惠，实时跟踪包裹进度" style="cursor: pointer;"><span class="lricon"></span><br>微信关注</a>
<div class="lr-pop"><span class="lr-close">关闭</span><span class="lr-qcimg"></span></div></div></body></html>