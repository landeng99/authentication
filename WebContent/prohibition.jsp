<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0058)/lmp/prohibition.jsp#tag_eb01 -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/img/crmqq.php"></script>
	<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/contains.js"></script>
	<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/localStorage.js"></script>
	<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/Panel.js"></script>
    <title>禁运物品细则</title>
    <meta name="keywords" content="翔锐物流，笨鸟转运，转运公司,美国快递、美国转运">
    <meta name="description" content="首家转运免首重，免税州直飞，24小时出库，丢货全赔，6*10小时贴心客服。翔锐物流用互联网思维打造转运公司，颠覆转运行业时效慢，体验差，安全性低，价格高的用户体验。">
    <link rel="shortcut icon" type="image/x-icon" href="http://www.birdex.cn/icon_16x16.png" media="screen">
    <link href="/lmp/resource/css/common.css" rel="stylesheet" type="text/css">
    <link href="/lmp/resource/css/index.css" rel="stylesheet" type="text/css">
    <link href="/lmp/resource/css/prohibition.css" rel="stylesheet" type="text/css">
        <style>
            .tip_showMore {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #CDCDCD;
    border-radius: 5px;
    color: #333333;
    margin: 1em 0 3em;
    padding: 10px;
    position: relative;
	box-shadow:-2px 0px 8px #e6e6e6,0px -2px 8px #e6e6e6,0px 2px 8px #e6e6e6,2px 0px 8px #e6e6e6;
}
        .tip_showMore div
        {
            height: auto;
            min-height: 20px;
        }
.tip_showMore.left {
    margin-left: 10px;
}
.tip_showMore.right {
    margin-right: 10px;
}
.tip_showMore:before {
    border-color: #CDCDCD rgba(0, 0, 0, 0);
    border-style: solid;
    border-width: 20px 20px 0;
    bottom: -20px;
    content: "";
    display: block;
    left: 40px;
    position: absolute;
    width: 0;
}
.tip_showMore:after {
    border-color: #FFFFFF rgba(0, 0, 0, 0);
    border-style: solid;
    border-width: 13px 13px 0;
    bottom: -13px;
    content: "";
    display: block;
    left: 47px;
    position: absolute;
    width: 0;
}
.tip_showMore.top:before {
    border-width: 0 20px 20px;
    bottom: auto;
    left: auto;
    right: 40px;
    top: -20px;
}
.tip_showMore.top:after {
    border-width: 0 13px 13px;
    bottom: auto;
    left: auto;
    right: 47px;
    top: -13px;
}
.tip_showMore.left:before {
    border-color: rgba(0, 0, 0, 0) #CDCDCD;
    border-width: 7px 10px 7px 0;
    bottom: auto;
    left: -10px;
    top: 32px;
}
.tip_showMore.left:after {
    border-color: rgba(0, 0, 0, 0) #FFFFFF;
    border-width: 7px 10px 7px 0;
    bottom: auto;
    left: -7px;
    top: 32px;
}
.tip_showMore.right:before {
    border-color: rgba(0, 0, 0, 0) #CDCDCD;
    border-width: 6px 0 8px 10px;
    bottom: auto;
    left: auto;
    right: -11px;
    top: 32px;
}
.tip_showMore.right:after {
   border-color: rgba(0, 0, 0, 0) #FFFFFF;
    border-width: 6px 0 8px 10px;
    bottom: auto;
    left: auto;
    right: -10px;
    top: 32px;
}
        </style>
</head>
<body><iframe style="display: none;"></iframe><style type="text/css">.WPA3-SELECT-PANEL { z-index:2147483647; width:463px; height:292px; margin:0; padding:0; border:1px solid #d4d4d4; background-color:#fff; border-radius:5px; box-shadow:0 0 15px #d4d4d4;}.WPA3-SELECT-PANEL * { position:static; z-index:auto; top:auto; left:auto; right:auto; bottom:auto; width:auto; height:auto; max-height:auto; max-width:auto; min-height:0; min-width:0; margin:0; padding:0; border:0; clear:none; clip:auto; background:transparent; color:#333; cursor:auto; direction:ltr; filter:; float:none; font:normal normal normal 12px "Helvetica Neue", Arial, sans-serif; line-height:16px; letter-spacing:normal; list-style:none; marks:none; overflow:visible; page:auto; quotes:none; -o-set-link-source:none; size:auto; text-align:left; text-decoration:none; text-indent:0; text-overflow:clip; text-shadow:none; text-transform:none; vertical-align:baseline; visibility:visible; white-space:normal; word-spacing:normal; word-wrap:normal; -webkit-box-shadow:none; -moz-box-shadow:none; -ms-box-shadow:none; -o-box-shadow:none; box-shadow:none; -webkit-border-radius:0; -moz-border-radius:0; -ms-border-radius:0; -o-border-radius:0; border-radius:0; -webkit-opacity:1; -moz-opacity:1; -ms-opacity:1; -o-opacity:1; opacity:1; -webkit-outline:0; -moz-outline:0; -ms-outline:0; -o-outline:0; outline:0; -webkit-text-size-adjust:none; font-family:Microsoft YaHei,Simsun;}.WPA3-SELECT-PANEL a { cursor:auto;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-TOP { height:25px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CLOSE { float:right; display:block; width:47px; height:25px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CLOSE:hover { background-position:0 -25px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-MAIN { padding:23px 20px 45px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-GUIDE { margin-bottom:42px; font-family:"Microsoft Yahei"; font-size:16px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-SELECTS { width:246px; height:111px; margin:0 auto;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CHAT { float:right; display:block; width:88px; height:111px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat 0 -80px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CHAT:hover { background-position:-88px -80px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-AIO-CHAT { float:left;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-QQ { display:block; width:76px; height:76px; margin:6px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat -50px 0;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-QQ-ANONY { background-position:-130px 0;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-LABEL { display:block; padding-top:10px; color:#00a2e6; text-align:center;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-BOTTOM { padding:0 20px; text-align:right;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-INSTALL { color:#8e8e8e;}</style><style type="text/css">.WPA3-CONFIRM { z-index:2147483647; width:285px; height:141px; margin:0; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR0AAACNCAMAAAC9pV6+AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjU5QUIyQzVCNUIwQTExRTJCM0FFRDNCMTc1RTI3Nzg4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjU5QUIyQzVDNUIwQTExRTJCM0FFRDNCMTc1RTI3Nzg4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTlBQjJDNTk1QjBBMTFFMkIzQUVEM0IxNzVFMjc3ODgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTlBQjJDNUE1QjBBMTFFMkIzQUVEM0IxNzVFMjc3ODgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6QoyAtAAADAFBMVEW5xdCkvtNjJhzf6Ozo7/LuEQEhHifZ1tbv8vaibw7/9VRVXGrR3en4+vuveXwZGCT///82N0prTRrgU0MkISxuEg2uTUqvEwO2tbb2mwLn0dHOiQnExMacpKwoJzT29/n+qAF0mbf9xRaTm6abm5vTNBXJ0tvFFgH/KgD+ugqtra2yJRSkq7YPDxvZGwDk7O//2zfoIgH7/f1GSV6PEAhERUtWWF2EiZHHNix1dXWLk53/ySLppQt/gID9IAH7Mgj0JQCJNTTj4+QaIi0zNDr/0Cvq9f/s+/5eYGrn9fZ0eYXZ5O3/tBD8/f5udHy6naTV2t9obHl8gY9ubW/19fXq8fXN2uT/5z/h7PC2oaVmZWoqJR6mMCL3+f33KQM1Fhr6NRT9///w/v/ftrjJDQby9vpKkcWHc3vh7vvZ5uvpPycrMEHu7/De7fne5+709voyKSTi7PVbjrcuLTnnNAzHFhD7/P3aDwDfNxTj6vHz9fj09vj3///19/ny9PevuMI9PEPw8/bw8vbx9PdhYWHx8/fy9ff19vj19vny9fjw8/fc6fOosbza5/LX5fDV4+/U4u7S4e3R4O3O3uvd6vTe6vTd6fPb6PPb6PLW5PDZ5/HW5O/Z5vHV5O/T4e7T4u7Y5vHY5fHO3evR4OzP3+vP3uvQ3+xGt/9Lg7Dz9vjv8/X7+/3d5+vi6+7g6ezh6u3w9Pbc5+rt8vTl7fDn7vHr8fP2+Pr3+fv6+/zq8PPc5urb5en4+/7Y5epGsvjN3erW4OXf6+/s8/bn8PPk7vLv9fiAyfdHrO6Aorz09vnx9fnz9Pb09/vv8fVHsfd+zP/jwyLdExFekLipYWLN3OjR3Oa0k5n/9fXX6PDh7vDU4ey6fAzV4+5HOSHIoBP+/v3b6OppaGrT4Ovk6/Lw8PE8P1Pz+v/w8/nZ5vDW4erOztL/LgT3+Pn2+PvY5/Ta5/HvuxfZ5Ojm8f6lrrrI1uPw0iZPT1Sps7r19/iqtLzxKgjZ3N9RVFtQSkbL2ujM2+ku4f1qAAAIDklEQVR42uzcC3ATdR7A8S3QhZajm+RSEmxZEhIT2vKvjU1aWqAPWr1IsRTkoRZb4Qoi6XmFYHued5coQe8wFLSoFOXV0oeIShG13ANURBmoeme9Z6dXnbP34OF517MOUo/7JykNySXZjPP/rzPb37d0y7Yz/5n9zP43u9tNmUnqHBcUqpzUakatf2QaFKqz+lQm5931T0KhWv9uDuNavwMK3XoX43oq+koYXemQxem0WLMv/fYp6Yd1Hou2v39RarHzvBLHsnyWbtmOxyRe9Do7DaWWfjmPYVjWu2CzLo0CnaejyzGUmSm3Yx0fjafi3B1PSzqsszOqHJkYx2bz6iiv7j189j93SqnTzZ5l8+mr61hnazQxg5mZ/XhisRw+6CiVHOK8POW5u7ZKqFZt8/DCV5Q6zdZ+Lw7vVCKMg8oH7cjLY78kJZ2tzdpW/G/rNTq7oihX3i+Xy21yxzy1HSmRXV17zom8s2to2S4pdUCrbfCvYZ1nBdtnGLTZMI4yVSbrU+NZpcdfkznf5Mp9Vkp9qNW2+Newzj7hdLzdZrNx/Z/Ikj9OHkLF86bqO5dYULlHx2L4wz7J1KBtOKFtGFnFOvsF+5ZVqeR5O7J2Lsmy6F3IlfqVRd3p8h55lPzU/ZKpSdu0f/8Jz8IX1qkXjHF6zo95ZL2wZLB87sdoSK/WZ1+403dcrindXS+VTl/xLE+cbhxej0Zn34D36kGJnNWyVGfqnaj4XOe8eZ84fTOLz1pWL9WwTqNgOtZ3Dsip+1b2jecR0nuPzsOnPBamvlGiYZ1nBGrcne3DwTtP8o2XMxGHlDOPJg/vOixvYZ6Ralhnt1B/uqfIe4LMsogfcpb3evpKOXy2zNqL79i7W6JhnW0CNS5M9F4+4JnUq4j7868//3z6Z3OSehS9rHdu2SoLDdskWhQ627pVlZiH43p75sxevjw+Pn55xvQFGo2mR8Fx5UVFiebflUhXZ3vk9pwrNKoQp+TjNJqUjPh4r87sBVOmaDRTemqKUKLK2L1dognrbF9oVpnSEKpJSkmaM/2mjIzlGTfNXqCZgm00SeUo0agyTm6Qrs5egRaqVMYv01hUE9ejSEqZjkvxzau4uCLObDIajd17JRrW2SOQI81oTP/y+jEIKTlWkfRZSkqKZk6PAq+gyrQK/DPVPdv3SDOs83jkmuYnpmMC092zxrAcQlyNQqHorUH4f2PSzs9IN6Ybzbapj0szYZ1cnjWn40wVd69bUdhbiV/HucrKyjErrs+vqMDfNpkriyzMHqnqPBGp1gG5HR9dqtJN2KEiPz9/48Yf4Dbm558/P6PAZDLVmdki3r7ov09IMSEdw0Q5PtUpKlRhHJOpoGDGtVUUmKoKeY7l7M4Bqeo0R+iArt+Or6/kzMIVRg9ORcVVmfP4s6BOlWCYiFhOKS/9sFmCYZ3WCP3HKvdcXk08u6rbbMb7T0HeVZ28vNi6tG71pzcvRizeeQaZllbpFVmnxeHZdVg0f+XvZ1UZsY+qqq4uFldXd3/a5ITkW/567GYdvtrilHZdqzR1DkQo13Pfi0XZfdfNqsvDZ8UrEhIme+pOuCO5Y5VM9v0H/j2TxVOL5ecfkGCRdVpLec+NCw7r3B+bZ0rPW1f2nT9+1PHRyVtW/UiGqz1439qZnkt1jrVKVKclQlbvAxdoft93q2JnFOTlrbtOdk19XeNK1uKZ5eHJapFgWKchfE0TfTeUrauwTh7mCdSp/dtfSr6XjWrs2MfaIMEi6zQswjaLM5GzxDOz8AvVuvHX4KzsOnZf/adWtCgX65S2SFOnKUI6JV96ZTHLDtyY8JtY/CL+7aN9/i4ufeAfa5libuoVF8vqmiQY1nFH1SX8EaEv3FIM60R8KvXiRc9i2rQLOLwcZc/kCumM7kAHdEAHdL4BnR9D4QId0AEd0AEd0AEd0BkFOj+FwgU6AjqPQuECHQGdB6FwgQ7ogA7ogA7ogA7ogA7oQKDztXR+CIULdEAHdEAHdEAHdEAHdEAHAp2vpfMzKFygI6DzCBQu0BHQ+QkULtABHdABHdABHdABnTAx2nZCaZnVm/zjljEDNN99zpSF0NlEuFMxa95pI9Q7a2JGxj1rYKplFOurZgxBm0JBZ9OG4+//klDvH99weGRcxwXZrVR71HGWvk572121hLqrrd0/rltWSzn3JlF0nidUkM7zlBNJp5NQQTqdlBNHp2sSoboCdSZRTiSd1wgVpPMa5cTRWf0qoVYH6rxKuRA6m0nX3naG1JvrzrS1+8d1y2i/l88dtCV0dE49R6hTgTrPUU4kHVI3doN0aN9HFkfnzcOEejNQ5zDlxNFZepBQSwN1DlJOJJ0jhArSOUI5cXROvkKok4E6r1AuhM4W0mGdY4TCOv5x3bJjlHMHbQkdnbfGEeqtQJ1xlBNJ5yihgnSOUk4cndtfJtTtgTovU04cnTduINQbgTo3UC6EzkOkwzovEArr+Md1y16gnDtoS+jojH2JUGMDdV6inDg6h14k1KFAnRcpJ45Ox1hCdQTqjKWcODr3HiLUvYE6hygnkk4HoYJ0Oignhs6G997+FaHefu8D/7iOaT+n2+sOEXRi1hwn9Zvi42tizoyMa0j+1y9o9jpTNoG6zpYjMRtIPWXwQUzXyLibNxscVP/GvaPswf/fdx4m3oQJxIbasuXhbzAqOpIJdAR0JkDhAh3QAR3QAR3QAR3QAZ3RrZNzGRTCdPk2JnUu8ITBmatnqlNzXFCobtOP/58AAwA/1aMkKhXCbQAAAABJRU5ErkJggg==) no-repeat;}.WPA3-CONFIRM { *background-image:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/panel.png);}.WPA3-CONFIRM * { position:static; z-index:auto; top:auto; left:auto; right:auto; bottom:auto; width:auto; height:auto; max-height:auto; max-width:auto; min-height:0; min-width:0; margin:0; padding:0; border:0; clear:none; clip:auto; background:transparent; color:#333; cursor:auto; direction:ltr; filter:; float:none; font:normal normal normal 12px "Helvetica Neue", Arial, sans-serif; line-height:16px; letter-spacing:normal; list-style:none; marks:none; overflow:visible; page:auto; quotes:none; -o-set-link-source:none; size:auto; text-align:left; text-decoration:none; text-indent:0; text-overflow:clip; text-shadow:none; text-transform:none; vertical-align:baseline; visibility:visible; white-space:normal; word-spacing:normal; word-wrap:normal; -webkit-box-shadow:none; -moz-box-shadow:none; -ms-box-shadow:none; -o-box-shadow:none; box-shadow:none; -webkit-border-radius:0; -moz-border-radius:0; -ms-border-radius:0; -o-border-radius:0; border-radius:0; -webkit-opacity:1; -moz-opacity:1; -ms-opacity:1; -o-opacity:1; opacity:1; -webkit-outline:0; -moz-outline:0; -ms-outline:0; -o-outline:0; outline:0; -webkit-text-size-adjust:none;}.WPA3-CONFIRM * { font-family:Microsoft YaHei,Simsun;}.WPA3-CONFIRM .WPA3-CONFIRM-TITLE { height:40px; margin:0; padding:0; line-height:40px; color:#2b6089; font-weight:normal; font-size:14px; text-indent:80px;}.WPA3-CONFIRM .WPA3-CONFIRM-CONTENT { height:55px; margin:0; line-height:55px; color:#353535; font-size:14px; text-indent:29px;}.WPA3-CONFIRM .WPA3-CONFIRM-PANEL { height:30px; margin:0; padding-right:16px; text-align:right;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON { position:relative; display:inline-block!important; display:inline; zoom:1; width:99px; height:30px; margin-left:10px; line-height:30px; color:#000; text-decoration:none; font-size:12px; text-align:center;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON-FOCUS { width:78px;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON .WPA3-CONFIRM-BUTTON-TEXT { line-height:30px; text-align:center; cursor:pointer;}.WPA3-CONFIRM-CLOSE { position:absolute; top:7px; right:7px; width:10px; height:10px; cursor:pointer;}</style>
    <link rel="shortcut icon" type="image/x-icon" href="http://www.birdex.cn/icon_16x16.png" media="screen">
<link href="/lmp/resource/css/common(1).css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/lmp/resource/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="/lmp/resource/js/layer.min.js"></script>
<script type="text/javascript" src="/lmp/resource/js/common.js"></script>
<script type="text/javascript">
    $(function () {
        var userName = $.cookie('BEYOND_NickName');
        if (userName == undefined || userName == "") {
            userName = $.cookie('BEYOND_UserName');
        }
        if (userName == undefined || userName == "") {
            userName = "会员用户";
        } else {
            if (userName.length > 8 && userName.indexOf("@") > -1) {
                userName = userName.substring(0, 7) + "...";
            }
        }
        $("#headerUserName").html(userName).attr("title", userName);

        var url = document.location.href;
        if (url == "http://www.birdex.cn/")
            url = "http://www.birdex.cn/index.html";
        else if (url == "http://birdex.cn/")
            url = "http://www.birdex.cn/index.html";

        $("ul.nav>li").each(function () {
            if (url.indexOf($(this).attr("val")) > -1) {
                $(this).addClass("current");
            }
        });

        var userID = $.cookie('User::TUserID');
        if (userID != undefined && userID != null && parseInt(userID) > 0) {
            $("#logout").show();
            $(".nav").css("right", "0");
        } else {
            $(".nav").css("right", "0");
            $("#login").show();
        }

        $(".ggnew").click(function () {
            if ($("#ggtab").is(':visible')) {
                $("#ggtab").slideUp("slow");
            } else {
                $("#ggtab").slideDown("slow");
            };
            var img = $(this).find("span").css("background-image");
            if (img.indexOf('header01') > -1) {
                $(this).find("span").css("background-image", img.replace('header01.png', 'header_up.png'));
            } else {
                $(this).find("span").css("background-image", img.replace('header_up.png', 'header01.png'));
            }
        });

        $("#tags1 li").mouseover(function (index, item) {
            $("#tags1 li").removeClass("selectTag");
            $(this).addClass("selectTag");
            $("#tags1").parent().find("div").each(function () {
                if ($(this).hasClass("selectTag")) {
                    $(this).removeClass("selectTag").addClass("tagContent");
                }
            });
            $("#" + $(this).attr("obj")).removeClass("tagContent").addClass("selectTag");
        });

        if (url.indexOf('index') > -1 || document.URL == 'http://www.birdex.cn/' || document.location.href.indexOf('index') > -1) {
            $(".ggnew").click();
        }

        var headerName = $.cookie('User::NickName');
        if (headerName == undefined || headerName == null || headerName == "") {
            headerName = $.cookie('User::TrueName');
            if (headerName == undefined || headerName == null || headerName == "") {
                headerName = $.cookie('BEYOND_UserName');
                if (headerName == undefined || headerName == null) {
                    headerName = "";
                }
            }
        }

        $("#headername").html(headerName);
    });
</script>
<meta property="qc:admins" content="15411542256212450636">
<style type="text/css">
    #header{ border-bottom:1px solid #dcdcdc; overflow:hidden; height:32px; line-height:32px;}
    #header a{ font-size:14px; color:#6a6a6b;}
    #header .pull-right a{ margin:0 10px;}
    #header .ggnew{ cursor:pointer;}
    #header .ggnew i{ color:#eb7424; font-style:normal;}
    #header .ggnew span{ float:left; display:block; width:42px; height:32px; background:url('http://img.cdn.birdex.cn/images/header01.png') no-repeat center center #0593d3; overflow:hidden; margin-right:5px;}
    #ggtab{ display:none; background-color:#0593d3; padding:10px 0; overflow:hidden;}
    #ggtab .hbt{ font-size:22px; font-weight:bold; color:#f57121; padding-left:32px; background:url('http://img.cdn.birdex.cn/images/header02.png') no-repeat left center; overflow:hidden; line-height:32px;}
    #sevedu{ padding-top:5px; overflow:hidden;}
    #sevedu .tagContent{display:none;}
    #sevedu .tagContent p,
    #sevedu .selectTag p{ font-size:16px; color:#edf6ff;}
    #sevedu .tagContent p a,
    #sevedu .selectTag p a{ color:#f57121; text-decoration:underline; margin-left:8px; font-weight:bold;}
    #sevedu .tag{ width:100%; padding-top:5px; text-align:right; overflow:hidden;}
    #sevedu .tag li{display:inline;}
    #sevedu .tag a{display:inline-block; width:8px; height:9px; background:url('http://img.cdn.birdex.cn/images/header03.png') no-repeat; overflow:hidden; margin:0 3px;}
    #sevedu .tag a:hover,
    #sevedu .tag .selectTag a{ width:56px; background-image:url('http://img.cdn.birdex.cn/images/header04.png');}
</style>


<div id="ggtab" style="display: none;">
     <div class="mian">
          <div class="hbt">公告</div>
          
          <div id="sevedu">
                <div class="selectTag" id="li0"><p>中国清明节（4月4日至4月6日）假期安排通知：除4月5日（星期天）清明节当天放假外，其余时间均有国内客服正<a href="http://www.birdex.cn/Information-14.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li1"><p>关于新增渠道详细介绍公告<a href="http://www.birdex.cn/Information-21.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li2"><p>关于系统问题导致订单申报数据错误的相关处理方案<a href="http://www.birdex.cn/Information-19.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li3"><p>所有笨鸟的客户以及淘友，经过对目前现状的考察和认真考虑，笨鸟不得已今天对税单相关政策作出调整如下：<a href="http://www.birdex.cn/Information-16.html" target="_blank">查看详情</a></p></div><ul id="tags1" class="tag"><li obj="li0" class="selectTag"><a href="javascript:void(0)"></a></li><li obj="li1"><a href="javascript:void(0)"></a></li><li obj="li2"><a href="javascript:void(0)"></a></li><li obj="li3"><a href="javascript:void(0)"></a></li></ul>
          </div>
     </div>
</div>
<div id="header" class="noticeHeader">
     <div class="mian">
         <div class="pull-right" id="login" style="">
              <a href="http://passport.birdex.cn/Login.aspx">登录</a>|<a href="http://passport.birdex.cn/Register.aspx">注册</a>
         </div>
         <div class="pull-right" id="logout" style="display:none;">
               <span id="headername"></span><a href="http://passport.birdex.cn/LogOut.aspx" style="">退出</a>
          </div>
         <a class="ggnew"><span style="background-image: url(http://img.cdn.birdex.cn/images/header01.png);"></span>网站最新公告 <i>4</i> 条</a>
     </div>
</div>
<div id="head" style="border-bottom-style: none;">
     <div class="mian">
          <a href="http://www.birdex.cn/index.html" class="logo"><img src="/lmp/resource/img/logo.jpg" alt="翔锐物流" title="翔锐物流"><span>笨鸟</span></a>
          
          
          <ul class="nav" style="right: 0px;">
              <li val="help" style="padding-right:5px;"><a href="/lmp/help.jsp">帮助中心</a></li>
              <li val="Information" id="headInformation" class="current"><a href="/lmp/prohibition.jsp">禁运物品</a></li>
              <li val="account" id="headAccount"><a href="/lmp/transport.jsp">我的帐户</a></li>
              <li val="index"><a href="/lmp/index.jsp">首页</a></li>
          </ul>
     </div>
</div>
    <form method="post" action="/lmp/resource/img/prohibition.html" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTY0MDE1MTU5OA9kFgICAQ9kFgZmDxYCHgtfIUl0ZW1Db3VudAICFgRmD2QWAmYPFQNUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTgxNjE3ZmVzdDAuanBnCem4oeiCieazpQg35pyIMeaXpWQCAQ9kFgJmDxUDVGh0dHA6Ly9pbWdzLmJpcmRleC5jbi8vRmlsZS9JbWFnZXMvUHJvaGliaXRpb25JbWcvMjAxNC8wOC8xOC8yMDE0MDgxODE4MTU0MncxejEwLmpwZwnniZvogonms6UIN+aciDHml6VkAgEPFgIfAAIGFgxmD2QWAmYPFQdUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIyOTMwdTBnczAuanBnG+atpuWZqOW8ueiNr+WPiuS7v+ecn+atpuWZqE48c3Bhbj7mnqrmlK/lvLnoja88L3NwYW4+PHNwYW4+5Lu/55yf5p6q5Y+K6YWN5Lu2PC9zcGFuPjxzcGFuPueOqeWFt+aeqjwvc3Bhbj4BOAzmn6XnnIvlhajpg6gBOJYB5p6q5pSv44CB5a2Q5by544CB54Ku5by544CB5omL5qa05by544CB5Zyw6Zu344CB54K45by544CB5byT566t44CB5Y+v5bCE5Ye75a2Q5by555qE546p5YW35p6q44CB5Lu/55yf5p6q44CB556E5YeG6ZWc44CB5p6q566h44CB5Lu/55yf5q2m5Zmo5qih5Z6L562JZAIBD2QWAmYPFQdUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIyOTUwNWZ5MjAuanBnEuaYk+eHg+aYk+eIhueJqeWTgTw8c3Bhbj7nlLLmsrk8L3NwYW4+PHNwYW4+6aaZ5rC0PC9zcGFuPjxzcGFuPuaJk+eBq+acujwvc3Bhbj4BNwzmn6XnnIvlhajpg6gBN7QB6Zu3566h44CB54K46I2v44CB54Gr6I2v44CB6Z6t54Ku44CB57qv55S15rGg44CB5rG95rK544CB54Wk5rK544CB5qGQ5rK544CB6YWS57K+44CB55Sf5ryG44CB5p+05rK544CB55Om5pav5rCU55O244CB56O344CB56Gr56O644CB54Gr5p+044CB5omT54Gr5py644CB5oyH55Sy5rK544CB6aaZ5rC044CB6aaZ6JawZAICD2QWAmYPFQdUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIzMDAydWQ2NzAuanBnDOeuoeWItuWIgOWFt0U8c3Bhbj7ljJXpppY8L3NwYW4+PHNwYW4+5pS26JeP5YiAPC9zcGFuPjxzcGFuPuernuaKgOexu+WIgOWJkTwvc3Bhbj4BNgzmn6XnnIvlhajpg6gBNlPlkITnsbvlm73lrrbnrqHliLbliIDlhbfjgIHotoXov4c1MOWOmOexs+mVv+eahOWOqOaIv+WIgOWFt+OAgei/kOWKqOernuaKgOexu+WIgOWJkWQCAw9kFgJmDxUHVGh0dHA6Ly9pbWdzLmJpcmRleC5jbi8vRmlsZS9JbWFnZXMvUHJvaGliaXRpb25JbWcvMjAxNC8wOC8xOC8yMDE0MDgxODEyMzAxMzlwNjEwLmpwZw/mtrLljovoo4Xnianlk4FFPHNwYW4+6Ziy5pmS5Za36Zu+PC9zcGFuPjxzcGFuPueUn+WPkeWJgjwvc3Bhbj48c3Bhbj7msJTpm77liYI8L3NwYW4+ATUM5p+l55yL5YWo6YOoATVC6ZOd572Q5ray5Y6L6KOF55qE54mp5ZOB5aaC6Ziy5pmS5Za36Zu+44CB55Sf5Y+R5YmC44CB5rCU6Zu+5YmC562JZAIED2QWAmYPFQdUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIzNDI3djhqOTAuanBnDOi/neemgeiNr+eJqTg8c3Bhbj7purvphonnsbvoja/niak8L3NwYW4+PHNwYW4+54OI5q+S5oCn6I2v54mpPC9zcGFuPgE0DOafpeeci+WFqOmDqAE0xgHlkITnsbvng4jmgKfmr5Loja/vvIzlpoLpk4rjgIHmsLDljJbnianjgIHnoJLpnJzvvJsK5ZCE57G76bq76YaJ6I2v54mp77yM5aaC6bim54mHKOWMheaLrOe9gueyn+Wjs+OAgeiKseOAgeiLnuOAgeWPtinjgIHlkJfllaHjgIHlj6/ljaHlm6DjgIHmtbfmtJvlm6DjgIHlpKfpurvjgIHlhrDmr5LjgIHpurvpu4TntKDlj4rlhbblroPliLblk4HnrYlkAgUPZBYCZg8VB1RodHRwOi8vaW1ncy5iaXJkZXguY24vL0ZpbGUvSW1hZ2VzL1Byb2hpYml0aW9uSW1nLzIwMTQvMDgvMTgvMjAxNDA4MTgxMjMwNDhxemt6MC5qcGcP5Y2x6Zmp5YyW5a2m5ZOBWjxzcGFuPuiFkOiagOaAp+eJqeWTgTwvc3Bhbj48c3Bhbj7mlL7lsITmgKflhYPntKA8L3NwYW4+PHNwYW4+55Sf5YyW5Yi25ZOB5Y+K5a655ZmoPC9zcGFuPgEzDOafpeeci+WFqOmDqAEzswLlkITnsbvohZDomoDmgKfnianlk4HvvJrnoavphbjjgIHnm5DphbjjgIHnoZ3phbjjgIHmnInmnLrmurbliYLjgIHlhpzoja/jgIHlj4zmsKfmsLTnrYk7CuWQhOexu+aUvuWwhOaAp+WFg+e0oOWPiuWuueWZqO+8mumTgOOAgemStOOAgemVreOAgemSmuetie+8jOebm+aIluaUvui/h+ivpeexu+eJqei0qOeahOWuueWZqOaIluiiq+ivpeexu+eJqei0qOaxoeafk+eahOeJqeWTgTsK5ZCE57G755Sf5YyW5Yi25ZOB5ZKM5Lyg5p+T5oCn54mp5ZOB77ya54Kt55a944CB5Y2x6Zmp5oCn55eF6I+M44CB5Yy76I2v55So5bqf5byD54mp562J44CCZAICDxYCHwACEBYgZg9kFgJmDxUCVGh0dHA6Ly9pbWdzLmJpcmRleC5jbi8vRmlsZS9JbWFnZXMvUHJvaGliaXRpb25JbWcvMjAxNC8wOC8xOC8yMDE0MDgxODEyMjMwOG00NHIwLmpwZx7msb3ovabmkanmiZjovabphY3ku7blj4rpmYTku7ZkAgEPZBYCZg8VAlRodHRwOi8vaW1ncy5iaXJkZXguY24vL0ZpbGUvSW1hZ2VzL1Byb2hpYml0aW9uSW1nLzIwMTQvMDgvMTgvMjAxNDA4MTgxMjIzNDM1azFxMC5qcGcS5b6F5Yqg5bel5Y2K5oiQ5ZOBZAICD2QWAmYPFQJUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIyMzU3bDh0MjAuanBnDOacuuaisOaJi+ihqGQCAw9kFgJmDxUCVGh0dHA6Ly9pbWdzLmJpcmRleC5jbi8vRmlsZS9JbWFnZXMvUHJvaGliaXRpb25JbWcvMjAxNC8wOC8xOC8yMDE0MDgxODE3MjMwN25qYjgwLmpwZwzkuozmiYvllYblk4FkAgQPZBYCZg8VAlRodHRwOi8vaW1ncy5iaXJkZXguY24vL0ZpbGUvSW1hZ2VzL1Byb2hpYml0aW9uSW1nLzIwMTQvMDgvMTgvMjAxNDA4MTgxMjI1MDBqeGdhMC5qcGcP56aB6L+Q5Ye654mI54mpZAIFD2QWAmYPFQJUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIyNTExeThwaDAuanBnEuWbveWutuacuuWvhui1hOaWmWQCBg9kFgJmDxUCVGh0dHA6Ly9pbWdzLmJpcmRleC5jbi8vRmlsZS9JbWFnZXMvUHJvaGliaXRpb25JbWcvMjAxNC8wOC8xOC8yMDE0MDgxODEyMjUyOGdmZ3AwLmpwZxLliqjnianlj4rlhbbliLblk4FkAgcPZBYCZg8VAlRodHRwOi8vaW1ncy5iaXJkZXguY24vL0ZpbGUvSW1hZ2VzL1Byb2hpYml0aW9uSW1nLzIwMTQvMDgvMTgvMjAxNDA4MTgxMjI1NDFtcWx3MC5qcGcP5qSN54mp5Y+K56eN5a2QZAIID2QWAmYPFQJUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIyNjAyMjUzbTAuanBnDOePjei0teaWh+eJqWQCCQ9kFgJmDxUCVGh0dHA6Ly9pbWdzLmJpcmRleC5jbi8vRmlsZS9JbWFnZXMvUHJvaGliaXRpb25JbWcvMjAxNC8wOC8xOC8yMDE0MDgxODEyMjYxN3Rnam4wLmpwZwzlm73lrrbotKfluIFkAgoPZBYCZg8VAlRodHRwOi8vaW1ncy5iaXJkZXguY24vL0ZpbGUvSW1hZ2VzL1Byb2hpYml0aW9uSW1nLzIwMTQvMDgvMTgvMjAxNDA4MTgxMjI4MDF2eHdqMC5qcGcM5Lyq6YCg6LSn5biBZAILD2QWAmYPFQJUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIyODI1dWJwczAuanBnDOacieS7t+ivgeWIuGQCDA9kFgJmDxUCVGh0dHA6Ly9pbWdzLmJpcmRleC5jbi8vRmlsZS9JbWFnZXMvUHJvaGliaXRpb25JbWcvMjAxNC8wOC8xOC8yMDE0MDgxODEyMjkwOW56dmQwLmpwZyrlkKvom4vjgIHogonjgIHlhoXohI/jgIHlpbbnsbvliqDlt6Xpo5/lk4FkAg0PZBYCZg8VAlRodHRwOi8vaW1ncy5iaXJkZXguY24vL0ZpbGUvSW1hZ2VzL1Byb2hpYml0aW9uSW1nLzIwMTQvMDgvMTgvMjAxNDA4MTgxMjI4NTN2cmI0MC5qcGcM5pWj6KOF6aOf5ZOBZAIOD2QWAmYPFQJUaHR0cDovL2ltZ3MuYmlyZGV4LmNuLy9GaWxlL0ltYWdlcy9Qcm9oaWJpdGlvbkltZy8yMDE0LzA4LzE4LzIwMTQwODE4MTIzMzA2d2hkYjAucG5nDOWQhOexu+iCieazpWQCDw9kFgJmDxUCVGh0dHA6Ly9pbWdzLmJpcmRleC5jbi8vRmlsZS9JbWFnZXMvUHJvaGliaXRpb25JbWcvMjAxNC8wOC8xOC8yMDE0MDgxODEyMjIxMXFid2EwLmpwZxLlkITnsbvlrqDnianpo5/lk4FkZC+Dcjkg/INtx8BugAOKpgvnmIfsSTdfFSx3jnRyCCbk">
</div>

    <div class="embargo">
        <div class="page-heading">
            <p>
                <img src="/lmp/resource/img/embargo01.gif" alt="笨鸟"></p>
            <p style="font-size: 14px;">
                转运物品需符合海关总署令第43号《中华人民共和国禁止进出境物品表》和《中华人民共和国限制进出境物品表》，对禁运物品笨鸟不承接转运、不负责理赔。</p>
        </div>
        <div class="page-subject" id="tag_eb01" style="display:block;">
            <a href="/lmp/prohibition.jsp#tag_eb03" style="display:block;">海关管制类</a> <a href="/lmp/prohibition.jsp#tag_eb02" style="display:block;">航空管制类</a> <a href="/lmp/prohibition.jsp" class="active">
                最新禁运物品</a> <strong class="ico01">最新禁运物品</strong>
        </div>
        <ul class="list-pic-top" style="display:block;">
            
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818181617fest0.jpg"></em>
                        <h3>
                            鸡肉泥</h3>
                        <p>
                            <span>
                                7月1日</span>起禁运</p>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818181542w1z10.jpg"></em>
                        <h3>
                            牛肉泥</h3>
                        <p>
                            <span>
                                7月1日</span>起禁运</p>
                    </li>
                
        </ul>

        <div class="page-subject" id="tag_eb02" style="display:block;">
            <a href="/lmp/prohibition.jsp#tag_eb03" style="display:block;">海关管制类</a> <a href="/lmp/prohibition.jsp#tag_eb02" class="active">航空管制类</a> <a href="/lmp/prohibition.jsp" style="display:block;">
                最新禁运物品</a> <strong class="ico02">航空管制物品</strong>
        </div>
        <ul class="list-pic-left" style="display:block;">
            
                    <li>
                        <div class="pull-left">
                            <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122930u0gs0.jpg"></div>
                        <p>
                            航空管制类</p>
                        <h3>
                            <strong>
                                武器弹药及仿真武器</strong></h3>
                        <p>
                            <span>枪支弹药</span><span>仿真枪及配件</span><span>玩具枪</span></p>
                        <p>
                            <a href="javascript:void(0);" id="showMore" class="blue showMore" value="8">查看全部</a></p>
                            <input type="hidden" id="hdItemDetail_8" value="枪支、子弹、炮弹、手榴弹、地雷、炸弹、弓箭、可射击子弹的玩具枪、仿真枪、瞄准镜、枪管、仿真武器模型等">
                    </li>
                
                    <li>
                        <div class="pull-left">
                            <img alt="" width="150px" height="150px" src="/lmp/resource/img/201408181229505fy20.jpg"></div>
                        <p>
                            航空管制类</p>
                        <h3>
                            <strong>
                                易燃易爆物品</strong></h3>
                        <p>
                            <span>甲油</span><span>香水</span><span>打火机</span></p>
                        <p>
                            <a href="javascript:void(0);" id="showMore" class="blue showMore" value="7">查看全部</a></p>
                            <input type="hidden" id="hdItemDetail_7" value="雷管、炸药、火药、鞭炮、纯电池、汽油、煤油、桐油、酒精、生漆、柴油、瓦斯气瓶、磷、硫磺、火柴、打火机、指甲油、香水、香薰">
                    </li>
                
                    <li>
                        <div class="pull-left">
                            <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818123002ud670.jpg"></div>
                        <p>
                            航空管制类</p>
                        <h3>
                            <strong>
                                管制刀具</strong></h3>
                        <p>
                            <span>匕首</span><span>收藏刀</span><span>竞技类刀剑</span></p>
                        <p>
                            <a href="javascript:void(0);" id="showMore" class="blue showMore" value="6">查看全部</a></p>
                            <input type="hidden" id="hdItemDetail_6" value="各类国家管制刀具、超过50厘米长的厨房刀具、运动竞技类刀剑">
                    </li>
                
                    <li>
                        <div class="pull-left">
                            <img alt="" width="150px" height="150px" src="/lmp/resource/img/201408181230139p610.jpg"></div>
                        <p>
                            航空管制类</p>
                        <h3>
                            <strong>
                                液压装物品</strong></h3>
                        <p>
                            <span>防晒喷雾</span><span>生发剂</span><span>气雾剂</span></p>
                        <p>
                            <a href="javascript:void(0);" id="showMore" class="blue showMore" value="5">查看全部</a></p>
                            <input type="hidden" id="hdItemDetail_5" value="铝罐液压装的物品如防晒喷雾、生发剂、气雾剂等">
                    </li>
                
                    <li>
                        <div class="pull-left">
                            <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818123427v8j90.jpg"></div>
                        <p>
                            航空管制类</p>
                        <h3>
                            <strong>
                                违禁药物</strong></h3>
                        <p>
                            <span>麻醉类药物</span><span>烈毒性药物</span></p>
                        <p>
                            <a href="javascript:void(0);" id="showMore" class="blue showMore" value="4">查看全部</a></p>
                            <input type="hidden" id="hdItemDetail_4" value="各类烈性毒药，如铊、氰化物、砒霜；
各类麻醉药物，如鸦片(包括罂粟壳、花、苞、叶)、吗啡、可卡因、海洛因、大麻、冰毒、麻黄素及其它制品等">
                    </li>
                
                    <li>
                        <div class="pull-left">
                            <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818123048qzkz0.jpg"></div>
                        <p>
                            航空管制类</p>
                        <h3>
                            <strong>
                                危险化学品</strong></h3>
                        <p>
                            <span>腐蚀性物品</span><span>放射性元素</span><span>生化制品及容器</span></p>
                        <p>
                            <a href="javascript:void(0);" id="showMore" class="blue showMore" value="3">查看全部</a></p>
                            <input type="hidden" id="hdItemDetail_3" value="各类腐蚀性物品：硫酸、盐酸、硝酸、有机溶剂、农药、双氧水等;
各类放射性元素及容器：铀、钴、镭、钚等，盛或放过该类物质的容器或被该类物质污染的物品;
各类生化制品和传染性物品：炭疽、危险性病菌、医药用废弃物等。">
                    </li>
                
        </ul>

        <div class="page-subject" id="tag_eb03" style="display:block;">
            <a href="/lmp/prohibition.jsp#tag_eb03" class="active">海关管制类</a> <a href="/lmp/prohibition.jsp#tag_eb02" style="display:block;">航空管制类</a> <a href="/lmp/prohibition.jsp" style="display:block;">
                最新禁运物品</a> <strong class="ico03">海关管制物品</strong>
        </div>
        <ul class="list-pic-top" style="display:block;">
            
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122308m44r0.jpg"></em><h3>
                            汽车摩托车配件及附件</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/201408181223435k1q0.jpg"></em><h3>
                            待加工半成品</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122357l8t20.jpg"></em><h3>
                            机械手表</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818172307njb80.jpg"></em><h3>
                            二手商品</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122500jxga0.jpg"></em><h3>
                            禁运出版物</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122511y8ph0.jpg"></em><h3>
                            国家机密资料</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122528gfgp0.jpg"></em><h3>
                            动物及其制品</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122541mqlw0.jpg"></em><h3>
                            植物及种子</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122602253m0.jpg"></em><h3>
                            珍贵文物</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122617tgjn0.jpg"></em><h3>
                            国家货币</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122801vxwj0.jpg"></em><h3>
                            伪造货币</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122825ubps0.jpg"></em><h3>
                            有价证券</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122909nzvd0.jpg"></em><h3>
                            含蛋、肉、内脏、奶类加工食品</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122853vrb40.jpg"></em><h3>
                            散装食品</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818123306whdb0.png"></em><h3>
                            各类肉泥</h3>
                    </li>
                
                    <li><em>
                        <img alt="" width="150px" height="150px" src="/lmp/resource/img/20140818122211qbwa0.jpg"></em><h3>
                            各类宠物食品</h3>
                    </li>
                
        </ul>
    </div>
    </form>
    <script type="text/javascript" src="/lmp/resource/js/index.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".signBtn").click(function () {
                document.location.href = "http://passport.birdex.cn/Register.aspx";
            });
        });
    </script>
    <div id="foot">
     <div class="mian">
          <span class="pull-right"><img src="/lmp/resource/img/logo.gif" alt="翔锐物流"></span>
          
          <div class="pull-left">
              <h5>联系我们</h5>
              <p>4008-890-788</p>
              <p>0755-86054577</p>
              <p id="QQ"><!-- WPA Button Begin -->
<iframe width="92" height="22" scrolling="no" frameborder="0" allowtransparency="true" src="/lmp/resource/img/prohibition.html"></iframe><script charset="utf-8" type="text/javascript" src="/lmp/resource/js/wpa.php"></script>
<!-- WPA Button End --></p>
          </div>
          <div class="pull-left">
              <h5>邮件</h5>
              <p><a href="mailto:service@birdex.cn" style="color:#FFFFFF">service@birdex.cn</a></p>
          </div>
          <div class="pull-left" style="width:200px">
            <h5>友情链接</h5>
            <a href="http://www.etao.com/tuan/index.html?partnerid=4852" target="_blank" style="color:#FFFFFF">一淘海淘团</a><br>
<a href="http://www.smzdm.com/" target="_blank" style="color:#FFFFFF">什么值得买</a><br>
            <a href="http://www.55haitao.com/" target="_blank" style="color:#FFFFFF">55海淘</a>
            
          </div>
     </div>
    <div class="mian" style="margin:0 auto;text-align:center;color:#818181;font-size:14px;vertical-align:bottom;">
        <div>粤ICP备14015306号-1</div> 
     </div>
</div>
<div style="display:none;">
<script type="text/javascript">
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F68c503fc431b1f102540a9c79f1ffc78' type='text/javascript'%3E%3C/script%3E"));
</script><script src="/lmp/resource/js/h.js" type="text/javascript"></script><a href="http://tongji.baidu.com/hm-web/welcome/ico?s=68c503fc431b1f102540a9c79f1ffc78" target="_blank"><img border="0" src="/lmp/resource/img/21.gif" width="20" height="20"></a>
</div>



    <div class="tip_showMore" style="width: 200px; height: auto; font-size: 13px; overflow: visible; position: absolute; left: 420px; top: 1137px; display: none;" id="reciveDiv"></div>

<script>
    function pageX(elem) {
        return elem.offsetParent ? (elem.offsetLeft + pageX(elem.offsetParent)) : elem.offsetLeft;
    }

    function pageY(elem) {
        return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
    }

    $(document).ready(function () {
        $("#divItemDetail .close").click(function () {
            $.BDEX.Close();
            $('#divItemDetail').hide();
        });

        $("a.showMore").mouseover(function () {
            var id = $(this).attr("value");
            var str = $("#hdItemDetail_" + id).val();

            var elem = $(this)[0];
            var left = pageX(elem);
            var top = pageY(elem);

            var showHTML = "<div style='word-wrap:break-word;word-break:keep-all;'>" + str + "</div><div></div>";
            //======当前浏览器窗口宽度===========
            var windowWidth = $(window).width();
            if (left + 70 + 200 > windowWidth) {
                $("#reciveDiv").html(showHTML).addClass("right").css("position", "absolute").css("left", left - 242).css("top", top - 45).show();
            } else {
                $("#reciveDiv").html(showHTML).addClass("left").css("position", "absolute").css("left", left + 70).css("top", top - 45).show();
            }
        }).mouseout(function () {
            $("#reciveDiv").removeClass("left").removeClass("right").html("").hide();
        });
    });
</script>

<div id="lrToolRb" class="lr-tool-rb" style="z-index:10000;"><a class="lr-gotop" title="返回顶部"><span class="lricon iconArrow"></span><br>TOP</a><a class="lr-QQ" title="单击联系QQ客服" onclick="SetQQ();"><span class="lricon"></span><br>联系客服</a><a class="lr-qrcode" title="关注获取最新优惠，实时跟踪包裹进度"><span class="lricon"></span><br>微信关注</a><div class="lr-pop"><span class="lr-close">关闭</span><span class="lr-qcimg"></span></div></div></body></html>