<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0033)http://www.birdex.cn/account.html -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/img/crmqq.php"></script>
<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/contains.js"></script>
<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/localStorage.js"></script>
<script type="text/javascript" charset="utf-8" async="" src="/lmp/resource/js/Panel.js"></script>
<title>跨境物流 专业的跨境转运</title>
<meta name="keywords" content="跨境物流，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="跨境物流，拥有美国仓库及专业物流配送体系"/>
    <link href="/lmp/resource/css/account.css" rel="stylesheet" type="text/css">
    <link href="/lmp/resource/css/default.css" rel="stylesheet" type="text/css">
  
<link href="/lmp/resource/css/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/lmp/resource/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="/lmp/resource/js/layer.min.js"></script>
<script type="text/javascript" src="/lmp/resource/js/common.js"></script>
<script type="text/javascript">
    $(function () {
        var userName = $.cookie('BEYOND_NickName');
        if (userName == undefined || userName == "") {
            userName = $.cookie('BEYOND_UserName');
        }
        if (userName == undefined || userName == "") {
            userName = "会员用户";
        } else {
            if (userName.length > 8 && userName.indexOf("@") > -1) {
                userName = userName.substring(0, 7) + "...";
            }
        }
        $("#headerUserName").html(userName).attr("title", userName);

        var url = document.location.href;
        if (url == "http://www.birdex.cn/")
            url = "http://www.birdex.cn/index.html";
        else if (url == "http://birdex.cn/")
            url = "http://www.birdex.cn/index.html";

        $("ul.nav>li").each(function () {
            if (url.indexOf($(this).attr("val")) > -1) {
                $(this).addClass("current");
            }
        });

        var userID = $.cookie('User::TUserID');
        if (userID != undefined && userID != null && parseInt(userID) > 0) {
            $("#logout").show();
            $(".nav").css("right", "0");
        } else {
            $(".nav").css("right", "0");
            $("#login").show();
        }

        $(".ggnew").click(function () {
            if ($("#ggtab").is(':visible')) {
                $("#ggtab").slideUp("slow");
            } else {
                $("#ggtab").slideDown("slow");
            };
            var img = $(this).find("span").css("background-image");
            if (img.indexOf('header01') > -1) {
                $(this).find("span").css("background-image", img.replace('header01.png', 'header_up.png'));
            } else {
                $(this).find("span").css("background-image", img.replace('header_up.png', 'header01.png'));
            }
        });

        $("#tags1 li").mouseover(function (index, item) {
            $("#tags1 li").removeClass("selectTag");
            $(this).addClass("selectTag");
            $("#tags1").parent().find("div").each(function () {
                if ($(this).hasClass("selectTag")) {
                    $(this).removeClass("selectTag").addClass("tagContent");
                }
            });
            $("#" + $(this).attr("obj")).removeClass("tagContent").addClass("selectTag");
        });

        if (url.indexOf('index') > -1 || document.URL == 'http://www.birdex.cn/' || document.location.href.indexOf('index') > -1) {
            $(".ggnew").click();
        }

        var headerName = $.cookie('User::NickName');
        if (headerName == undefined || headerName == null || headerName == "") {
            headerName = $.cookie('User::TrueName');
            if (headerName == undefined || headerName == null || headerName == "") {
                headerName = $.cookie('BEYOND_UserName');
                if (headerName == undefined || headerName == null) {
                    headerName = "";
                }
            }
        }

        $("#headername").html(headerName);
    });
</script>
<meta property="qc:admins" content="15411542256212450636">
<style type="text/css">
    #header{ border-bottom:1px solid #dcdcdc; overflow:hidden; height:32px; line-height:32px;}
    #header a{ font-size:14px; color:#6a6a6b;}
    #header .pull-right a{ margin:0 10px;}
    #header .ggnew{ cursor:pointer;}
    #header .ggnew i{ color:#eb7424; font-style:normal;}
    #header .ggnew span{ float:left; display:block; width:42px; height:32px; background:url('http://img.cdn.birdex.cn/images/header01.png') no-repeat center center #0593d3; overflow:hidden; margin-right:5px;}
    #ggtab{ display:none; background-color:#0593d3; padding:10px 0; overflow:hidden;}
    #ggtab .hbt{ font-size:22px; font-weight:bold; color:#f57121; padding-left:32px; background:url('http://img.cdn.birdex.cn/images/header02.png') no-repeat left center; overflow:hidden; line-height:32px;}
    #sevedu{ padding-top:5px; overflow:hidden;}
    #sevedu .tagContent{display:none;}
    #sevedu .tagContent p,
    #sevedu .selectTag p{ font-size:16px; color:#edf6ff;}
    #sevedu .tagContent p a,
    #sevedu .selectTag p a{ color:#f57121; text-decoration:underline; margin-left:8px; font-weight:bold;}
    #sevedu .tag{ width:100%; padding-top:5px; text-align:right; overflow:hidden;}
    #sevedu .tag li{display:inline;}
    #sevedu .tag a{display:inline-block; width:8px; height:9px; background:url('http://img.cdn.birdex.cn/images/header03.png') no-repeat; overflow:hidden; margin:0 3px;}
    #sevedu .tag a:hover,
    #sevedu .tag .selectTag a{ width:56px; background-image:url('http://img.cdn.birdex.cn/images/header04.png');}
</style>
</head>
<body><iframe style="display: none;"></iframe><style type="text/css">.WPA3-SELECT-PANEL { z-index:2147483647; width:463px; height:292px; margin:0; padding:0; border:1px solid #d4d4d4; background-color:#fff; border-radius:5px; box-shadow:0 0 15px #d4d4d4;}.WPA3-SELECT-PANEL * { position:static; z-index:auto; top:auto; left:auto; right:auto; bottom:auto; width:auto; height:auto; max-height:auto; max-width:auto; min-height:0; min-width:0; margin:0; padding:0; border:0; clear:none; clip:auto; background:transparent; color:#333; cursor:auto; direction:ltr; filter:; float:none; font:normal normal normal 12px "Helvetica Neue", Arial, sans-serif; line-height:16px; letter-spacing:normal; list-style:none; marks:none; overflow:visible; page:auto; quotes:none; -o-set-link-source:none; size:auto; text-align:left; text-decoration:none; text-indent:0; text-overflow:clip; text-shadow:none; text-transform:none; vertical-align:baseline; visibility:visible; white-space:normal; word-spacing:normal; word-wrap:normal; -webkit-box-shadow:none; -moz-box-shadow:none; -ms-box-shadow:none; -o-box-shadow:none; box-shadow:none; -webkit-border-radius:0; -moz-border-radius:0; -ms-border-radius:0; -o-border-radius:0; border-radius:0; -webkit-opacity:1; -moz-opacity:1; -ms-opacity:1; -o-opacity:1; opacity:1; -webkit-outline:0; -moz-outline:0; -ms-outline:0; -o-outline:0; outline:0; -webkit-text-size-adjust:none; font-family:Microsoft YaHei,Simsun;}.WPA3-SELECT-PANEL a { cursor:auto;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-TOP { height:25px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CLOSE { float:right; display:block; width:47px; height:25px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CLOSE:hover { background-position:0 -25px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-MAIN { padding:23px 20px 45px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-GUIDE { margin-bottom:42px; font-family:"Microsoft Yahei"; font-size:16px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-SELECTS { width:246px; height:111px; margin:0 auto;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CHAT { float:right; display:block; width:88px; height:111px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat 0 -80px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-CHAT:hover { background-position:-88px -80px;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-AIO-CHAT { float:left;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-QQ { display:block; width:76px; height:76px; margin:6px; background:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/SelectPanel-sprites.png) no-repeat -50px 0;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-QQ-ANONY { background-position:-130px 0;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-LABEL { display:block; padding-top:10px; color:#00a2e6; text-align:center;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-BOTTOM { padding:0 20px; text-align:right;}.WPA3-SELECT-PANEL .WPA3-SELECT-PANEL-INSTALL { color:#8e8e8e;}</style><style type="text/css">.WPA3-CONFIRM { z-index:2147483647; width:285px; height:141px; margin:0; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR0AAACNCAMAAAC9pV6+AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjU5QUIyQzVCNUIwQTExRTJCM0FFRDNCMTc1RTI3Nzg4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjU5QUIyQzVDNUIwQTExRTJCM0FFRDNCMTc1RTI3Nzg4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTlBQjJDNTk1QjBBMTFFMkIzQUVEM0IxNzVFMjc3ODgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTlBQjJDNUE1QjBBMTFFMkIzQUVEM0IxNzVFMjc3ODgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6QoyAtAAADAFBMVEW5xdCkvtNjJhzf6Ozo7/LuEQEhHifZ1tbv8vaibw7/9VRVXGrR3en4+vuveXwZGCT///82N0prTRrgU0MkISxuEg2uTUqvEwO2tbb2mwLn0dHOiQnExMacpKwoJzT29/n+qAF0mbf9xRaTm6abm5vTNBXJ0tvFFgH/KgD+ugqtra2yJRSkq7YPDxvZGwDk7O//2zfoIgH7/f1GSV6PEAhERUtWWF2EiZHHNix1dXWLk53/ySLppQt/gID9IAH7Mgj0JQCJNTTj4+QaIi0zNDr/0Cvq9f/s+/5eYGrn9fZ0eYXZ5O3/tBD8/f5udHy6naTV2t9obHl8gY9ubW/19fXq8fXN2uT/5z/h7PC2oaVmZWoqJR6mMCL3+f33KQM1Fhr6NRT9///w/v/ftrjJDQby9vpKkcWHc3vh7vvZ5uvpPycrMEHu7/De7fne5+709voyKSTi7PVbjrcuLTnnNAzHFhD7/P3aDwDfNxTj6vHz9fj09vj3///19/ny9PevuMI9PEPw8/bw8vbx9PdhYWHx8/fy9ff19vj19vny9fjw8/fc6fOosbza5/LX5fDV4+/U4u7S4e3R4O3O3uvd6vTe6vTd6fPb6PPb6PLW5PDZ5/HW5O/Z5vHV5O/T4e7T4u7Y5vHY5fHO3evR4OzP3+vP3uvQ3+xGt/9Lg7Dz9vjv8/X7+/3d5+vi6+7g6ezh6u3w9Pbc5+rt8vTl7fDn7vHr8fP2+Pr3+fv6+/zq8PPc5urb5en4+/7Y5epGsvjN3erW4OXf6+/s8/bn8PPk7vLv9fiAyfdHrO6Aorz09vnx9fnz9Pb09/vv8fVHsfd+zP/jwyLdExFekLipYWLN3OjR3Oa0k5n/9fXX6PDh7vDU4ey6fAzV4+5HOSHIoBP+/v3b6OppaGrT4Ovk6/Lw8PE8P1Pz+v/w8/nZ5vDW4erOztL/LgT3+Pn2+PvY5/Ta5/HvuxfZ5Ojm8f6lrrrI1uPw0iZPT1Sps7r19/iqtLzxKgjZ3N9RVFtQSkbL2ujM2+ku4f1qAAAIDklEQVR42uzcC3ATdR7A8S3QhZajm+RSEmxZEhIT2vKvjU1aWqAPWr1IsRTkoRZb4Qoi6XmFYHued5coQe8wFLSoFOXV0oeIShG13ANURBmoeme9Z6dXnbP34OF517MOUo/7JykNySXZjPP/rzPb37d0y7Yz/5n9zP43u9tNmUnqHBcUqpzUakatf2QaFKqz+lQm5931T0KhWv9uDuNavwMK3XoX43oq+koYXemQxem0WLMv/fYp6Yd1Hou2v39RarHzvBLHsnyWbtmOxyRe9Do7DaWWfjmPYVjWu2CzLo0CnaejyzGUmSm3Yx0fjafi3B1PSzqsszOqHJkYx2bz6iiv7j189j93SqnTzZ5l8+mr61hnazQxg5mZ/XhisRw+6CiVHOK8POW5u7ZKqFZt8/DCV5Q6zdZ+Lw7vVCKMg8oH7cjLY78kJZ2tzdpW/G/rNTq7oihX3i+Xy21yxzy1HSmRXV17zom8s2to2S4pdUCrbfCvYZ1nBdtnGLTZMI4yVSbrU+NZpcdfkznf5Mp9Vkp9qNW2+Newzj7hdLzdZrNx/Z/Ikj9OHkLF86bqO5dYULlHx2L4wz7J1KBtOKFtGFnFOvsF+5ZVqeR5O7J2Lsmy6F3IlfqVRd3p8h55lPzU/ZKpSdu0f/8Jz8IX1qkXjHF6zo95ZL2wZLB87sdoSK/WZ1+403dcrindXS+VTl/xLE+cbhxej0Zn34D36kGJnNWyVGfqnaj4XOe8eZ84fTOLz1pWL9WwTqNgOtZ3Dsip+1b2jecR0nuPzsOnPBamvlGiYZ1nBGrcne3DwTtP8o2XMxGHlDOPJg/vOixvYZ6Ralhnt1B/uqfIe4LMsogfcpb3evpKOXy2zNqL79i7W6JhnW0CNS5M9F4+4JnUq4j7868//3z6Z3OSehS9rHdu2SoLDdskWhQ627pVlZiH43p75sxevjw+Pn55xvQFGo2mR8Fx5UVFiebflUhXZ3vk9pwrNKoQp+TjNJqUjPh4r87sBVOmaDRTemqKUKLK2L1dognrbF9oVpnSEKpJSkmaM/2mjIzlGTfNXqCZgm00SeUo0agyTm6Qrs5egRaqVMYv01hUE9ejSEqZjkvxzau4uCLObDIajd17JRrW2SOQI81oTP/y+jEIKTlWkfRZSkqKZk6PAq+gyrQK/DPVPdv3SDOs83jkmuYnpmMC092zxrAcQlyNQqHorUH4f2PSzs9IN6Ybzbapj0szYZ1cnjWn40wVd69bUdhbiV/HucrKyjErrs+vqMDfNpkriyzMHqnqPBGp1gG5HR9dqtJN2KEiPz9/48Yf4Dbm558/P6PAZDLVmdki3r7ov09IMSEdw0Q5PtUpKlRhHJOpoGDGtVUUmKoKeY7l7M4Bqeo0R+iArt+Or6/kzMIVRg9ORcVVmfP4s6BOlWCYiFhOKS/9sFmCYZ3WCP3HKvdcXk08u6rbbMb7T0HeVZ28vNi6tG71pzcvRizeeQaZllbpFVmnxeHZdVg0f+XvZ1UZsY+qqq4uFldXd3/a5ITkW/567GYdvtrilHZdqzR1DkQo13Pfi0XZfdfNqsvDZ8UrEhIme+pOuCO5Y5VM9v0H/j2TxVOL5ecfkGCRdVpLec+NCw7r3B+bZ0rPW1f2nT9+1PHRyVtW/UiGqz1439qZnkt1jrVKVKclQlbvAxdoft93q2JnFOTlrbtOdk19XeNK1uKZ5eHJapFgWKchfE0TfTeUrauwTh7mCdSp/dtfSr6XjWrs2MfaIMEi6zQswjaLM5GzxDOz8AvVuvHX4KzsOnZf/adWtCgX65S2SFOnKUI6JV96ZTHLDtyY8JtY/CL+7aN9/i4ufeAfa5libuoVF8vqmiQY1nFH1SX8EaEv3FIM60R8KvXiRc9i2rQLOLwcZc/kCumM7kAHdEAHdL4BnR9D4QId0AEd0AEd0AEd0BkFOj+FwgU6AjqPQuECHQGdB6FwgQ7ogA7ogA7ogA7ogA7oQKDztXR+CIULdEAHdEAHdEAHdEAHdEAHAp2vpfMzKFygI6DzCBQu0BHQ+QkULtABHdABHdABHdABnTAx2nZCaZnVm/zjljEDNN99zpSF0NlEuFMxa95pI9Q7a2JGxj1rYKplFOurZgxBm0JBZ9OG4+//klDvH99weGRcxwXZrVR71HGWvk572121hLqrrd0/rltWSzn3JlF0nidUkM7zlBNJp5NQQTqdlBNHp2sSoboCdSZRTiSd1wgVpPMa5cTRWf0qoVYH6rxKuRA6m0nX3naG1JvrzrS1+8d1y2i/l88dtCV0dE49R6hTgTrPUU4kHVI3doN0aN9HFkfnzcOEejNQ5zDlxNFZepBQSwN1DlJOJJ0jhArSOUI5cXROvkKok4E6r1AuhM4W0mGdY4TCOv5x3bJjlHMHbQkdnbfGEeqtQJ1xlBNJ5yihgnSOUk4cndtfJtTtgTovU04cnTduINQbgTo3UC6EzkOkwzovEArr+Md1y16gnDtoS+jojH2JUGMDdV6inDg6h14k1KFAnRcpJ45Ox1hCdQTqjKWcODr3HiLUvYE6hygnkk4HoYJ0Oignhs6G997+FaHefu8D/7iOaT+n2+sOEXRi1hwn9Zvi42tizoyMa0j+1y9o9jpTNoG6zpYjMRtIPWXwQUzXyLibNxscVP/GvaPswf/fdx4m3oQJxIbasuXhbzAqOpIJdAR0JkDhAh3QAR3QAR3QAR3QAZ3RrZNzGRTCdPk2JnUu8ITBmatnqlNzXFCobtOP/58AAwA/1aMkKhXCbQAAAABJRU5ErkJggg==) no-repeat;}.WPA3-CONFIRM { *background-image:url(http://combo.b.qq.com/crm/wpa/release/3.3/wpa/views/panel.png);}.WPA3-CONFIRM * { position:static; z-index:auto; top:auto; left:auto; right:auto; bottom:auto; width:auto; height:auto; max-height:auto; max-width:auto; min-height:0; min-width:0; margin:0; padding:0; border:0; clear:none; clip:auto; background:transparent; color:#333; cursor:auto; direction:ltr; filter:; float:none; font:normal normal normal 12px "Helvetica Neue", Arial, sans-serif; line-height:16px; letter-spacing:normal; list-style:none; marks:none; overflow:visible; page:auto; quotes:none; -o-set-link-source:none; size:auto; text-align:left; text-decoration:none; text-indent:0; text-overflow:clip; text-shadow:none; text-transform:none; vertical-align:baseline; visibility:visible; white-space:normal; word-spacing:normal; word-wrap:normal; -webkit-box-shadow:none; -moz-box-shadow:none; -ms-box-shadow:none; -o-box-shadow:none; box-shadow:none; -webkit-border-radius:0; -moz-border-radius:0; -ms-border-radius:0; -o-border-radius:0; border-radius:0; -webkit-opacity:1; -moz-opacity:1; -ms-opacity:1; -o-opacity:1; opacity:1; -webkit-outline:0; -moz-outline:0; -ms-outline:0; -o-outline:0; outline:0; -webkit-text-size-adjust:none;}.WPA3-CONFIRM * { font-family:Microsoft YaHei,Simsun;}.WPA3-CONFIRM .WPA3-CONFIRM-TITLE { height:40px; margin:0; padding:0; line-height:40px; color:#2b6089; font-weight:normal; font-size:14px; text-indent:80px;}.WPA3-CONFIRM .WPA3-CONFIRM-CONTENT { height:55px; margin:0; line-height:55px; color:#353535; font-size:14px; text-indent:29px;}.WPA3-CONFIRM .WPA3-CONFIRM-PANEL { height:30px; margin:0; padding-right:16px; text-align:right;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON { position:relative; display:inline-block!important; display:inline; zoom:1; width:99px; height:30px; margin-left:10px; line-height:30px; color:#000; text-decoration:none; font-size:12px; text-align:center;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON-FOCUS { width:78px;}.WPA3-CONFIRM .WPA3-CONFIRM-BUTTON .WPA3-CONFIRM-BUTTON-TEXT { line-height:30px; text-align:center; cursor:pointer;}.WPA3-CONFIRM-CLOSE { position:absolute; top:7px; right:7px; width:10px; height:10px; cursor:pointer;}</style>
<div id="ggtab">
     <div class="mian">
          <div class="hbt">公告</div>
          
          <div id="sevedu">
                <div class="selectTag" id="li0"><p>中国清明节（4月4日至4月6日）假期安排通知：除4月5日（星期天）清明节当天放假外，其余时间均有国内客服正<a href="http://www.birdex.cn/Information-14.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li1"><p>关于新增渠道详细介绍公告<a href="http://www.birdex.cn/Information-21.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li2"><p>关于系统问题导致订单申报数据错误的相关处理方案<a href="http://www.birdex.cn/Information-19.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li3"><p>所有跨境物流的客户以及淘友，经过对目前现状的考察和认真考虑，跨境物流不得已今天对税单相关政策作出调整如下：<a href="http://www.birdex.cn/Information-16.html" target="_blank">查看详情</a></p></div><ul id="tags1" class="tag"><li obj="li0" class="selectTag"><a href="javascript:void(0)"></a></li><li obj="li1"><a href="javascript:void(0)"></a></li><li obj="li2"><a href="javascript:void(0)"></a></li><li obj="li3"><a href="javascript:void(0)"></a></li></ul>
          </div>
     </div>
</div>
<div id="header" class="noticeHeader">
     <div class="mian">
         <div class="pull-right" id="login" style="display:none;">
              <a href="http://passport.birdex.cn/Login.aspx">登录</a>|<a href="http://passport.birdex.cn/Register.aspx">注册</a>
         </div>
         <div class="pull-right" id="logout" style="">
               <span id="headername">LuFuTian</span><a href="http://passport.birdex.cn/LogOut.aspx" style="">退出</a>
          </div>
         <a class="ggnew"><span></span>网站最新公告 <i>4</i> 条</a>
     </div>
</div>
<div id="head">
     <div class="mian">
          <a href="http://www.birdex.cn/index.html" class="logo"><img src="${mainServer}/resource/img/logo.jpg" alt="跨境物流" title="跨境物流"><span>跨境</span></a>
          
          
          <ul class="nav" style="right: 0px;">
              <li val="help" style="padding-right:5px;"><a href="http://www.birdex.cn/help.html">帮助中心</a></li>
              <li val="Information" id="headInformation"><a href="http://www.birdex.cn/Information/prohibition.aspx">禁运物品</a></li>
              <li val="account" id="headAccount" class="current"><a href="http://www.birdex.cn/transport.html">我的帐户</a></li>
              <li val="index"><a href="http://www.birdex.cn/index.html">首页</a></li>
          </ul>
     </div>
</div>
<div id="content">
    
<script type="text/javascript">
    var isOverDomain = 0;
    var old = '1';
    $(function () {
        $("ul.sideaav li").each(function () {
            if (document.location.href.toLowerCase().indexOf($(this).attr("item").toLowerCase()) > -1) {
                $(this).addClass("current");
                $(this).find("i").css("background-image", "url('" + $(this).attr("img") + "')");
                $("ul.nav>li").removeClass("current");
                $("#headAccount").addClass("current");
            }
        });

        var overDomainArray = ["destination", "transport", "store", "order"];
        for (var item in overDomainArray) {
            if (document.location.href.toLowerCase().indexOf(overDomainArray[item]) > -1) {
                document.domain = domainUrl;
                isOverDomain = 1;
            }
        }
        $("#charge,#accountCharge,#accountLeveUp").click(function () {
            var rechargeOffHeight = ($(window).height() - 570) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                fix: false,
                title: false,
                shade: [0.5, '#ccc', true],
                border: [1, 0.3, '#666', true],
                offset: [rechargeOffHeight + 'px', ''],
                area: ['900px', '570px'],
                iframe: { src: '/UserBase/Recharge.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#editPass,#accountEditPass").click(function () {
            var editPassOffHeight = 0;
            var height = 338;
            if (old > 0) {
                editPassOffHeight = ($(window).height() - 338) / 2;
            } else {
                editPassOffHeight = ($(window).height() - 270) / 2;
                height = 270;
            }
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#fff', true],
                border: [0, 0.3, '#000', true],
                offset: [editPassOffHeight + 'px', ''],
                area: ['335px', height + 'px'],
                iframe: { src: '/UserBase/ModifyP.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#emailActive,#accountValidEmail").click(function () {
            var emailActiveOffHeight = ($(window).height() - 320) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [emailActiveOffHeight + 'px', ''],
                //area: ['850px', '400px'],
                area: ['335px', '278px'],
                //iframe: { src: '/UserBase/ActiveEmail.aspx?overdomain=' + isOverDomain }
                iframe: { src: '/UserBase/ActiveE.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#mobileActive,#accountActiveMobile").click(function () {
            var mobileActiveOffHeight = ($(window).height() - 277) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [mobileActiveOffHeight + 'px', ''],
                area: ['335px', '277px'],
                iframe: { src: '/UserBase/ActiveMobile.aspx?overdomain=' + isOverDomain }
            });
        });
    });
</script>
<div class="left" style="margin-bottom:250px;">
          <h3 class="size14">LuFuTian</h3>
          <p><span style="float:right;">已激活</span>342428216@q...</p>
          <p><!---->18620393058</p>
          <ul class="recharge">
            
              <li class="span5 pull-right text-center">
                  <i class="icon06"></i>
                  <h3 class="size14">5张</h3>
                  <span style="font-size:13px;"><a href="http://www.birdex.cn/account.html#Coupon" class="blue">优惠劵</a></span>
                  
              </li>
              
              <li class="span5 pull-left text-center" style="width:auto;">
                  <a title="钱包余额" style="text-decoration:none;cursor:pointer;"><i class="icon05"></i>
                  <h3 class="size14">$<span style="padding-left:1px;">0.00</span></h3></a>
                  <a style="cursor:pointer;font-size:13px;" class="blue" id="charge">充值</a>
              </li>

          </ul>

          <ul class="modification">
              <li class="span6"><a style="cursor:pointer;" class="blue icon07" id="editPass">修改密码</a></li>
              <li class="span6" style="display:none;"><a href="" class="blue icon08">修改订阅</a></li>
              <li class="span6" style="display:none;"><a href="" class="blue icon09">使用习惯</a></li>
          </ul>

          <ul class="sideaav">
            <!--
              <li class="span12" item="transport" img="/images/icon10_fff.png"><a href="/transport.html" class="size16"><i class="icon10"></i>我的包裹管理</a></li>
              <li class="span12" item="transport" img="/images/icon10_fff.png"><a href="/transport.html" class="size16"><i class="icon10"></i>我的订单</a></li>
              -->
              <li class="span12" item="transport" img="/lmp/resource/img/icon10_fff.png"><a href="/lmp/transport" class="size16"><i class="icon10"></i>我的包裹管理</a></li>
              <li class="span12" item="warehouse" img="/lmp/resource/img/icon11_fff.png"><a href="/lmp/warehouse.jsp" class="size16"><i class="icon11"></i>海外仓库地址</a></li>
              <li class="span12" item="destination" img="/lmp/resource/img/icon12_fff.png"><a href="/lmp/destination.jsp" class="size16"><i class="icon12"></i>收货地址管理</a></li>
              <li class="span12" item="record" img="/lmp/resource/img/icon13_fff.png"><a href="/lmp/record.jsp" class="size16"><i class="icon13"></i>账户记录</a></li>
              <li class="span12 current" item="account" img="/lmp/resource/img/icon14_fff.png"><a href="/lmp/account.jsp" class="size16"><i class="icon14" style="background-image: url(/lmp/resource/img/icon14_fff.png);"></i>账户设置</a></li>
          </ul>
</div>
         <div class="right">
          <p class="size20"><strong>账户设置</strong></p>
          <div class="hr mg18"></div>
          
          <ul class="list07">
              <li class="border-top-none">
                  <ol>
                      <li class="span2 size14">电子邮件地址：</li>
                      <li class="span6 size14">342428216@qq.com</li>
                      <li class="span4 size14 text-right"><a href="javascript:void(0);" class="blue" id="accountEditPass" style="margin-right:-5px;">[修改登录密码]</a>  <span style="margin-left:5px;">已激活</span></li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">手机号码：</li>
                      <li class="span6 size14">18620393058</li>
                      <li class="span4 size14 text-right"><a href="javascript:void(0);" class="blue" id="modifyMobile">[修改手机]</a><a href="javascript:void(0);" class="blue" id="accountActiveMobile">[立即验证]</a></li>
                  </ol>
              </li>
                            <li>
                  <ol>
                      <li class="span2 size14">真实姓名：</li>
                      <li class="span6 size14">陆富田</li>
                      <li class="span4 size14 text-right"><a href="javascript:void(0);" class="blue" id="accountModifyTrueName">[修改]</a></li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">注册时间：</li>
                      <li class="span6 size14">2015年01月05日</li>
                      <li class="span4 size14 text-right"></li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">账户余额：</li>
                      <li class="span6 size14">$<span style="padding-left:1px;">0.00</span></li>
                      <li class="span4 size14 text-right"><a href="javascript:void(0);" class="blue" id="accountCharge">[马上充值]</a></li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">会员类型：</li>
                      <li class="span6 size14">
                          <input name="" type="button" value="基础会员" class="Block input26 size14">
                      </li>
                      <li class="span4 size14 text-right"><a href="javascript:void(0);" class="blue" id="accountLeveUp">[立即升级]</a></li>
                  </ol>
              </li>
          </ul>
          <div class="install">
               <ul>
                  <li class="th background-none">会员分级</li>
                  <li style="background-color:#888788;color:White;height: 65px;margin-top:2px;"><strong class="size24">标准服务</strong><p><span class="size14 white">1公斤起运</span></p></li>
                  <li style="background-color:#888788;color:White;height: 65px;margin-top:2px;"><strong class="size24">香港自提</strong><p><span class="size14 white">1公斤起运</span></p></li>
                  <li style="background-color:#888788;color:White;height: 65px;margin-top:2px;"><strong class="size24">E特快</strong><p><span class="size14 white">1公斤起运</span></p></li>

                  <li><span class="size16">合箱（标准，E特快）</span></li>
                  <li><span class="size16">合箱（香港自提）</span></li>
                  <li><span class="size16">包裹分箱</span></li>
                  <li><span class="size16">跨包裹合箱</span></li>
                  <li><span class="size16">清点服务</span></li>
                  <li><span class="size16">加固服务</span></li>
                  <li><span class="size16">退货服务</span></li>
                  <li><span class="size16">取发票服务</span></li>
                  <li><span class="size16">免仓租期</span></li>
                  <li><span class="size16">会员获取</span></li>
                  <li></li>
               </ul>
               <ul group="1" class="GroupUL current">
                  <li class="th">基础会员<img src="/lmp/resource/img/question.png" style="width:13px;height:13px;vertical-align:middle;margin-top:-3px;margin-left:1px;" alt="基础会员" class="levelicon" content="通过邀请注册或直接注册为基础会员，享受基础会员的服务。"></li>
                  <li style="background-color:#65bb6e;color:White;height: 65px;margin-top:2px;">
                        <div style="margin-top: 12px;">
                            <span class="size24">$</span><span class="size36">35</span><span class="size14">每500克</span>
                        </div>
                    </li>
                  <li style="background-color:#65bb6e;color:White;height: 65px;margin-top:2px;">
                    <div style="margin-top: 12px;">
                        <span class="size24">$</span><span class="size36">25</span><span class="size14">每500克</span>
                    </div>    
                  </li>
                  <li style="background-color:#65bb6e;color:White;height: 65px;margin-top:2px;">
                    <div style="margin-top: 12px;">
                        <span class="size24">$</span><span class="size36">35</span><span class="size14">每500克</span>
                    </div>
                  </li>
                  <li><span class="size16">$</span><span class="size18">38</span>/500克</li>
                  <li><span class="size16">$</span><span class="size18">27</span>/500克</li>
                  <li>$10/票（分箱后）</li>
                  <li><span class="box_red" style="margin-top:15px;"></span></li>
                  <li>$10/票</li>
                  <li>$8/票</li>
                  <li>$30/票</li>
                  <li>$10/票</li>
                  <li>10天</li>
                  <li>邀请注册</li>
                  <li class="GroupBtn" style="height:45px;"></li>
               </ul>
               <ul class="GroupUL" group="2">
                  <li class="th">高级会员<img src="/lmp/resource/img/question.png" style="width:13px;height:13px;vertical-align:middle;margin-top:-3px;margin-left:1px;" alt="高级会员" class="levelicon" content="24小时内充值5000元人民币可直接升级为永久高级会员，享受高级会员的服务和优惠。首次充值的5000元可作为帐户余额抵扣运费、增值费和关税。"></li>
                  <li style="background-color:#069de2;color:White;height: 65px;margin-top:2px;"><div style="margin-top: 12px;"><span class="size24">$</span><span class="size36">30</span><span class="size14">每500克</span></div></li>
                  <li style="background-color:#069de2;color:White;height: 65px;margin-top:2px;"><div style="margin-top: 12px;"><span class="size24">$</span><span class="size36">20</span><span class="size14">每500克</span></div></li>
                  <li style="background-color:#069de2;color:White;height: 65px;margin-top:2px;"><div style="margin-top: 12px;"><span class="size24">$</span><span class="size36">30</span><span class="size14">每500克</span></div></li>
                  <li><span class="size16">$</span><span class="size18">32</span>/500克</li>
                  <li><span class="size16">$</span><span class="size18">22</span>/500克</li>
                  <li>$10/票（分箱后）</li>
                  <li><span class="box_red" style="margin-top:15px;"></span></li>
                  <li>$8/票</li>
                  <li>$4/票</li>
                  <li>$20/票</li>
                  <li>$5/票</li>
                  <li>30天</li>
                  <li>充值5000</li>
                  <li class="GroupBtn" style="height:45px;"><input name="tdLevelUp" type="button" class="Block input29 size14 button tdLevelUp" value="立即升级" money="5000"></li>
               </ul>
               <ul class="GroupUL" group="3">
                  <li class="th">专业会员<img src="/lmp/resource/img/question.png" style="width:13px;height:13px;vertical-align:middle;margin-top:-3px;margin-left:1px;" alt="专业会员" class="levelicon" content="24小时内充值20000元人民币可直接升级为永久专业会员，享受专业会员的服务和优惠。首次充值的20000元可作为帐户余额抵扣运费、增值费和关税。"></li>
                  <li style="background-color:#fb7c51;color:White;height: 65px;margin-top:2px;"><div style="margin-top: 12px;"><span class="size24">$</span><span class="size36">30</span><span class="size14">每500克</span></div></li>
                  <li style="background-color:#fb7c51;color:White;height: 65px;margin-top:2px;"><div style="margin-top: 12px;"><span class="size24">$</span><span class="size36">20</span><span class="size14">每500克</span></div></li>
                  <li style="background-color:#fb7c51;color:White;height: 65px;margin-top:2px;"><div style="margin-top: 12px;"><span class="size24">$</span><span class="size36">30</span><span class="size14">每500克</span></div></li>
                  <li><span class="size16">$</span><span class="size18">32</span>/500克</li>
                  <li><span class="size16">$</span><span class="size18">22</span>/500克</li>
                  <li>$8/票（分箱后）</li>
                  <li>$12/票（按出库）</li>
                  <li>免费</li>
                  <li>免费</li>
                  <li>免费</li>
                  <li>免费</li>
                  <li>45天</li>
                  <li>充值20000</li>
                  <li class="GroupBtn" style="height:45px;"><input name="tdLevelUp" type="button" class="Block input29 size14 button tdLevelUp" value="立即升级" money="20000"></li>
               </ul>
          </div>
          
          <div class="hr mg18 lastline"></div>
          <a name="Coupon"></a>
        <p class="size14" style="margin-left:0;"><strong class="pd10">优惠券：</strong>共 <span class="totalCount">5</span> 张</p>
        <div class="opt" style="margin:15px 0 0 40px;">
            <ul class="function pull-left" style="width:400px;">
                <li class="span3 current couponli" item="1" style="cursor:pointer;"><a><i class="first"></i><span style="margin-left:-20px;">可使用优惠券</span></a><span class="count" style="float: left; position: absolute;margin:6px 0 0 -21px;">5</span></li>
                <li class="span3 couponli" item="2" style="cursor:pointer;"><a>已使用优惠券</a></li>
                <li class="span3 couponli" item="3" style="cursor:pointer;"><a>已过期优惠券<i class="ultimate"></i></a></li>
            </ul>
        </div>
          
          
          <table cellspacing="0" cellpadding="0" class="list08" id="couponList" style="margin-top:5px;"><tbody><tr><th class="span2">来源</th><th class="span2">面额</th><th class="span2">数量</th><th style="width:25%; float:left; overflow:hidden;">是否可以叠加使用</th><th class="span3">有效期</th></tr><tr><td class="span2">注册送券</td><td class="span2">$<span style="padding-left:1px;">20.00</span></td><td class="span2">5</td><td style="width:25%; float:left; overflow:hidden;">不可以</td><td class="span3"><span>2015年04月05日</span></td></tr></tbody></table>
          <div class="paging text-right">
                <div class="page pageShow"></div>
         </div>
     </div>
</div>
<!--
<script type="text/javascript" src="/lmp/resource/js/account.js"></script>-->
<script type="text/javascript" src="/lmp/resource/js/jquery.pagination.js"></script>
<script type="text/javascript">
    $(function () {
        $("ul.GroupUL").each(function () {
            if (parseInt($(this).attr("group")) == parseInt('1')) {
                $(this).addClass("current");
            }
            if (parseInt($(this).attr("group")) > parseInt('1')) {
                if ($(this).attr("group") == "2") {
                    $(this).find("li.GroupBtn").html("<input name='tdLevelUp' type='button' class='Block input29 size14 button tdLevelUp' value='立即升级' money='5000' />");
                } else if ($(this).attr("group") == "3") {
                    $(this).find("li.GroupBtn").html("<input name='tdLevelUp' type='button' class='Block input29 size14 button tdLevelUp' value='立即升级' money='20000' />");
                }
            }
        });

        $(".tdLevelUp").click(function () {
            var obj = $(this);
            var rechargeOffHeight = ($(window).height() - 570) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                fix: false,
                title: false,
                shade: [0.5, '#ccc', true],
                border: [1, 0.3, '#666', true],
                offset: [rechargeOffHeight + 'px', ''],
                area: ['900px', '570px'],
                iframe: { src: '/UserBase/Recharge.aspx?overdomain=' + isOverDomain + '&m=' + obj.attr("money") }
            });
        });
        $("#modifyMobile").click(function () {
            var mobileOffHeight = ($(window).height() - 310) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                fix: false,
                title: false,
                shade: [0.5, '#ccc', true],
                border: [1, 0.3, '#666', true],
                offset: [mobileOffHeight + 'px', ''],
                area: ['400px', '310px'],
                iframe: { src: '/UserBase/ModifyMobile.aspx' }
            });
        });
        $("#accountModifyTrueName").click(function () {
            var perfectOffHeight = ($(window).height() - parseInt(360)) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [perfectOffHeight + 'px', ''],
                area: ['355px', '360px'],
                iframe: { src: '/UserBase/PerfectName.aspx' }
            });
        });

        $("#accountEditEmail").click(function(){
            $("#accountModifyTrueName").click();
        });

        $(".levelicon").mouseenter(function () {
            var elem = $(this)[0];
            var left = pageX(elem);
            var top = pageY(elem);
            var content = $(this).attr("content");
            var showHTML = "<div style='margin:0;padding:0;line-height:18px;'>" + content + "</div>";
            $("#tip").html(showHTML).css("position", "absolute").css("left", left - 41).css("top", top + 14).show();
        }).mouseleave(function (e) {
            var ex = e.pageX;
            var ey = e.pageY;
            var sx = $(this).offset().top;
            var sxx = $(this).offset().top + 100;
            var sy = $(this).offset().left;
            var syy = $(this).offset().left + 8;
            if (ey > sx && ey <= sxx && ex > sy && ex <= syy) {
                $("#tip").mouseleave(function () { $("#tip").hide(); });
                return false;
            }
            $("#tip").html("").hide();
        });
    });
    function pageX(elem) {
        return elem.offsetParent ? (elem.offsetLeft + pageX(elem.offsetParent)) : elem.offsetLeft;
    }

    function pageY(elem) {
        return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
    }
</script>
<div class="triangle-level top" style="width: auto;min-width:150px;max-width:200px;height: auto; overflow: visible; display: none;" id="tip">
    
</div>
<style type="text/css">
    .count
        {
            background: none repeat scroll 0 0 #E0725B;
            border-radius: 12px;
            color: #FFFFFF;
            cursor: default;
            float: left;
            font-weight: bold;
            height: 20px;
            line-height: 20px;
            margin: 0 4px;
            text-align: center;
            width: 20px;
        }
</style>
 <div id="foot">
     <div class="mian">
          <span class="pull-right"><img src="/lmp/resource/img/logo.gif" alt="翔锐物流"></span>
          
          <div class="pull-left">
              <h5>联系我们</h5>
              <p>4008-890-788</p>
              <p>0755-86054577</p>
              <p id="QQ"><!-- WPA Button Begin -->
<script charset="utf-8" type="text/javascript" src="/lmp/resource/img/wpa.php"></script>
<!-- WPA Button End --></p>
          </div>
          <div class="pull-left">
              <h5>邮件</h5>
              <p><a href="mailto:service@birdex.cn" style="color:#FFFFFF">service@birdex.cn</a></p>
          </div>
          <div class="pull-left" style="width:200px">
            <h5>友情链接</h5>
            <a href="http://www.etao.com/tuan/index.html?partnerid=4852" target="_blank" style="color:#FFFFFF">一淘海淘团</a><br>
<a href="http://www.smzdm.com/" target="_blank" style="color:#FFFFFF">什么值得买</a><br>
            <a href="http://www.55haitao.com/" target="_blank" style="color:#FFFFFF">55海淘</a>
            
          </div>
     </div>
    <div class="mian" style="margin:0 auto;text-align:center;color:#818181;font-size:14px;vertical-align:bottom;">
        <div>粤ICP备14015306号-1</div> 
     </div>
</div>
<div style="display:none;">
<script type="text/javascript">
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F68c503fc431b1f102540a9c79f1ffc78' type='text/javascript'%3E%3C/script%3E"));
</script>
<script src="/lmp/resource/js/h.js" type="text/javascript"></script>
<a href="http://tongji.baidu.com/hm-web/welcome/ico?s=68c503fc431b1f102540a9c79f1ffc78" target="_blank">
<img border="0" src="/lmp/resource/img/21.gif" width="20" height="20"></a>
</div>
<div id="lrToolRb" class="lr-tool-rb" style="z-index:10000;">
<a class="lr-gotop" title="返回顶部"><span class="lricon iconArrow"></span><br>TOP</a>
<a class="lr-QQ" title="单击联系QQ客服" onclick="SetQQ();"><span class="lricon"></span><br>联系客服</a>
<a class="lr-qrcode" title="关注获取最新优惠，实时跟踪包裹进度"><span class="lricon"></span><br>微信关注</a>
<div class="lr-pop"><span class="lr-close">关闭</span><span class="lr-qcimg"></span></div></div></body></html>