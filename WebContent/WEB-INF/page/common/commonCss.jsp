<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >

<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/svg-iconfont.css"> <!-- SVG彩色字体图标 -->
<link rel="stylesheet" type="text/css" href="${resource_path}/font/iconfont.css"> <!-- 字体图标 -->
<link rel="stylesheet" type="text/css" href="${resource_path}/bootstrap-3.3.7-dist/css/bootstrap.min.css"> <!-- 框架样式 -->
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/normalize.css-v7.0.0/normalize.css"> <!-- 公共修复浏览器BUG样式统一浏览器样式 -->
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/common.css"> <!-- 前店公共样式 尾部、按钮、字体、颜色……-->
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/frame_qd.css"> <!-- 前店公共框架样式 头部 -->

