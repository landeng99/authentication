<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <title>SU8-个人中心-海外仓库</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="../common/commonCss.jsp"></jsp:include>
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css"> <!-- 前店公共框架样式 头部 -->
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/layernew.css">
    
    <script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script>
</head>

<body>
	<!-- 头部页面 -->
	<jsp:include page="topPage.jsp"></jsp:include>
	
	<!-- banner -->
	<div class="min-banner">
	    <div class="bgImg"></div>
	    <div class="logo"></div>
	</div>
	
	<!-- 内容 -->
	<div class="wrap bg-gray">
        <div class="wrap-box pb20 pt20 clearfix">
            <div class="p-content">
                <div class="tab-box warehouse">
                    <ul class="nav nav-tabs " role="tablist">
	                    <c:forEach items="${overseasAddresses}" var="overPojo" varStatus="addrStatus">
	                       <c:if test="${addrStatus.count == 1}">
	                       		<li class="active">
	                       </c:if>
	                       <c:if test="${addrStatus.count != 1}">
	                       		<li>
	                       </c:if>
	                       	<a href="#${overPojo.id}" data-toggle="tab" role="tab" name="liClassColor" onClick="rightSpan(${overPojo.id})">
	                       	 	<svg class="icon" aria-hidden="true">
                               		<use xlink:href="${overPojo.flag_path}"></use>
                           		</svg>
                           		<span class="pr5">${overPojo.country}</span>
                                <span><c:if test="${overPojo.is_tax_free==2}">非</c:if>免税州</span>
                             </a>
                            </li>
	                   	</c:forEach>
                        <li>
                            <a class="txt-row">
                                <svg class="icon" aria-hidden="true">
                                    <use xlink:href="#icon-gugediqiu"></use>
                                </svg>
                               <p class="text-blue">COMMING SOON</p>
                               <p>其他仓库即将上线！</p>
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content ">
	                    <c:forEach items="${overseasAddresses}" var="overPojo" varStatus="addrStatus">
	               			<div role="tabpanel" <c:if test="${addrStatus.count==1}"> class="tab-pane active" </c:if> <c:if test="${addrStatus.count!=1}"> class="tab-pane" </c:if> id="${overPojo.id}">
	               				<h5 class="pt20">NAME/名称填写</h5>
	                          	<div class="container-fluid bg-gray pb20 pt20">
	                              	<form class="form-horizontal ">
	                                  	<div class="row  pl20 pr20 ">
	                                      	<div class="alert alert-warning col-sm-12" role="alert">
		                                      	First name固定是KJ，Last name是系统自动生成的用户唯一代码，作为用户身份的识别标志，
		                                      	请在购物网站填写收货人时将First name 以及Last name分别按此处内容填写(或者可直接点击复制按钮复制过去)，如：KJ NSNMGE。
	                                      	</div>
	                                  	</div>
	                                  	<div class="form-group">
		                                      <label class="col-sm-3 control-label">First name：</label>
		                                      <div class="col-sm-4 input-group clip_container">
		                                          <input class="form-control copycnt" name="trueName" id="trueName" type="text" value="KJ" placeholder="" type="text" disabled="disabled"> 
		                                          <span class="input-group-btn">
		                                              <button class="btn btn-default copyit" type="button">复制</button>
		                                          </span>
		                                      </div>
		                                  </div>
		                                  <div class="form-group">
		                                      <label class="col-sm-3 control-label">Last name：</label>
		                                      <div class="col-sm-4 input-group clip_container">
		                                          <input class="form-control copycnt" name="userCode" id="userCode" type="text" value="${frontUser.last_name}" placeholder="" type="text" disabled="disabled"> 
		                                          <span class="input-group-btn">
		                                              <button class="btn btn-default copyit" type="button">复制</button>
		                                          </span>
		                                      </div>
		                                  </div>
		                              </form>
	                          	</div>
	                          	<h5 class="pt20">ADDRESS/地址填写</h5>
	                          	<div class="container-fluid bg-gray pb20 pt20">
		                              <form class="form-horizontal ">
		                                  <div class="row  pl20 pr20 ">
		                                      <div class="alert alert-warning col-sm-12" role="alert">您在购物网站上的收货地址会有两行，请按照此格式填写，以确保C/O后的编号能够打印到快递单上。如购物网站不认可此 收货地址，请删除”C/O”，但保留”C/O”后的内容再次提交收货地址。
		                                      </div>
		                                  </div>
		                                  <div class="form-group">
		                                      <label class="col-sm-3 control-label">地址（Address）：</label>
		                                      <div class="col-sm-8 input-group clip_container">
		                                          <input class="form-control copycnt" name="AddressInfo" id="AddressInfo" type="text" value="${overPojo.address_first}" placeholder="" type="text" disabled="disabled"> 
		                                          <span class="input-group-btn">
		                                              <button class="btn btn-default copyit" type="button">复制</button>
		                                          </span>
		                                      </div>
		                                      <label class="col-sm-3 control-label mt10"></label>
		                                      <div class="col-sm-8 input-group mt10 clip_container">
		                                          <input class="form-control copycnt" name="AddressInfo2" id="AddressInfo2" type="text" value="C/O&nbsp;${frontUser.last_name}" placeholder="" type="text" disabled="disabled"> 
		                                          <span class="input-group-btn">
		                                              <button class="btn btn-default copyit" type="button">复制</button>
		                                          </span>
		                                      </div>
		                                  </div>
		                                  <div class="form-group">
		                                      <label class="col-sm-3 control-label">城市（City）：</label>
		                                      <div class="col-sm-4 input-group clip_container">
		                                          <input class="form-control copycnt" value="${overPojo.city}" name="Province" id="Province" placeholder="" type="text" disabled="disabled"> 
		                                          <span class="input-group-btn">
		                                              <button class="btn btn-default copyit" type="button">复制</button>
		                                          </span>
		                                      </div>
		                                  </div>
		                                  <div class="form-group">
		                                      <label class="col-sm-3 control-label">州（State）：</label>
		                                      <div class="col-sm-4 input-group clip_container">
		                                          <input class="form-control copycnt" value="${overPojo.state}" name="City" id="City" placeholder="" type="text" disabled="disabled"> 
		                                          <span class="input-group-btn">
		                                              <button class="btn btn-default copyit" type="button">复制</button>
		                                          </span>
		                                      </div>
		                                  </div>
		                                  <div class="form-group">
		                                      <label class="col-sm-3 control-label">县/郡（County）：</label>
		                                      <div class="col-sm-4 input-group clip_container">
		                                          <input class="form-control copycnt" value="${overPojo.county}" name="District" id="District" placeholder="" type="text" disabled="disabled"> 
		                                          <span class="input-group-btn">
		                                              <button class="btn btn-default copyit" type="button">复制</button>
		                                          </span>
		                                      </div>
		                                  </div>
		                                  <div class="form-group">
		                                      <label class="col-sm-3 control-label">国家（Country）：</label>
		                                      <div class="col-sm-4 input-group clip_container">
		                                          <input class="form-control copycnt" value="${overPojo.country}" name="Country" id="Country" placeholder="" type="text" disabled="disabled"> 
		                                          <span class="input-group-btn">
		                                              <button class="btn btn-default copyit" type="button">复制</button>
		                                          </span>
		                                      </div>
		                                  </div>
		                              </form>
		                          </div>
	                          	<h5 class="pt20">联系方式填写</h5>
	                          	<div class="container-fluid bg-gray pb20 pt20">
	                              	<form class="form-horizontal ">
	                                  	<div class="form-group">
	                                      	<label class="col-sm-3 control-label">邮编(Zip)：</label>
	                                      	<div class="col-sm-4 input-group clip_container">
                                         	 	<input class="form-control copycnt" value="${overPojo.postcode}" name="PostCode" id="PostCode" placeholder="" type="text" disabled="disabled" > 
	                                          	<span class="input-group-btn">
	                                              	<button class="btn btn-default copyit" type="button">复制</button>
	                                          	</span>
	                                      	</div>
	                                  	</div>
	                                  	<div class="form-group">
	                                      	<label class="col-sm-3 control-label">电话（Tell）：</label>
	                                      	<div class="col-sm-4 input-group clip_container">
	                                          	<input class="form-control copycnt" value="${overPojo.tell}" name="Tel" id="Tel" placeholder="" type="text" disabled="disabled"> 
	                                          	<span class="input-group-btn">
	                                              	<button class="btn btn-default copyit" type="button">复制</button>
	                                          	</span>
	                                      	</div>
	                                  	</div>
	                              	</form>
	                          	</div>
		               		</div>
		               	</c:forEach>	
                    </div>
                </div>
            </div>
            <!-- 左侧页面 -->
			<jsp:include page="leftPage.jsp"></jsp:include>
        </div>
       </div>
    
	<!-- 底部页面 -->
	<jsp:include page="bottomPage.jsp"></jsp:include>
	
	<script type="text/javascript" src="${resource_path}/font/iconfont.js"></script>
	<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="${resource_path}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	
	<script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/common.js"></script>
	<script type="text/javascript" src="${resource_path}/js/warehouse.js"></script>
 	<script type="text/javascript" src="${resource_path}/js/ZeroClipboard.js"></script>
 	
</body>
</html>