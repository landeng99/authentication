<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="overflow: hidden;">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
	<link href="${resource_path}/css/syaccount.css" rel="stylesheet" type="text/css">
    <link href="${resource_path}/css/syfault.css" rel="stylesheet" type="text/css">
    <link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
	<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
	<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
	<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
	<script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>
	<script type="text/javascript" src="${resource_path}/js/common.js"></script>
	<script type="text/javascript" src="${resource_path}/js/json2.js"></script>
	 <script type="text/javascript" src="${resource_path}/js/kindeditor-min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/cutImg.js"></script>
    <script type="text/javascript" src="${resource_path}/js/jquery.uploadify.v2.1.0.js"></script>
    <script src="${resource_path}/js/upfile.js" type="text/javascript"></script>      
</head>
<body>    
<div style="padding-top:20px;">
	<input type="hidden" name="package_id" value="${package_id}" id="package_id"/>
	<div class="Whole"  style="margin-bottom:10px;overflow: hidden;">
		<div style="margin-left:25px">
		<ul>	
				<li style="float:left;" class="blue">当前收货地址：</li>
				<li style="float:left;">${frontUserAddress.addressStr}|${frontUserAddress.receiver}|${frontUserAddress.mobile}</li>
				<li style="float:left;"><a href="javascript:;" style="cursor:pointer;margin-left: 10px;" class="blue edit">修改</a></li>
				<input type="hidden" name="address_id" value="${frontUserAddress.address_id}" />
				<input type="hidden" name="receiver"  value="${frontUserAddress.receiver}"/>
				<input type="hidden" name="region"  value='${frontUserAddress.region}'/>
				<input type="hidden" name="street"  value="${frontUserAddress.street}"/>
				<input type="hidden" name="postal_code"  value="${frontUserAddress.postal_code}"/>
				<input type="hidden" name="mobile"  value="${frontUserAddress.mobile}"/>
				<input type="hidden" name="isdefault"  value="${frontUserAddress.isdefault}"/>
				<input type="hidden" name="idcard" value="${frontUserAddress.idcard}"/>
				<input type="hidden" name="front_img"  value="${frontUserAddress.front_img}"/>
				<input type="hidden" name="back_img"  value="${frontUserAddress.back_img}"/>
			</ul>		
		</div>
	</div>
	<div class="Whole" id="addressid" style="margin-bottom:10px;">
		<div class="blue" style="margin-left:25px">使用新的收货地址：&nbsp;姓名：<input type="text" id="q_userName" name="q_userName"/>&nbsp;&nbsp;手机号码：<input type="text" name="q_moblieNum" id="q_moblieNum"/> &nbsp;&nbsp;<input type="button" id="query_button_id" value="&nbsp;查&nbsp;&nbsp;询&nbsp;" onclick="query_button_click();"/></div>
		<div style="margin-top: 20px;">
			<select id="addrid" name="addrid" style="margin-left: 25px;border:1px solid #ccc;height:42px;" onchange="SelectAddressChange();">
				<option value="0">----------------------------------------------选择收货地址----------------------------------------------</option>
				<c:forEach var="address" items="${addressList}">
					<option value="${address.address_id}">${address.addressStr}|${address.receiver}|${address.mobile}</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="upAddress" style="margin-left:30px;margin-top: 30px;display:none;" id="upAddress">
		<div class="bt">
            <strong class="size14 blue pd10" style="cursor: pointer;" id="addAddress">修改收货地址</strong>电话号码、手机号选填一项，其余均为必填项</div>
			<form  method="post" id="destinationFrom" action="${mainServer}/useraddr/addAddress">
                <ul class="destination">
                    <li><span>所在地区：<i class="red" style="color: Red;">*</i></span>
                        <select name="modifyProvince" id="modifyProvince" onchange="SelectModifyChange(this,2);" style="color: #666; font-family: @微软雅黑;width:150px;">
							<option value="0">请选择省市...</option>
							<option value="110000" p="6">北京市</option>
							<option value="120000" p="6">天津市</option>
							<option value="130000" p="6">河北省</option>
							<option value="140000" p="6">山西省</option>
							<option value="150000" p="6">内蒙古自治区</option>
							<option value="210000" p="6">辽宁省</option>
							<option value="220000" p="6">吉林省</option>
							<option value="230000" p="6">黑龙江省</option>
							<option value="310000" p="6">上海市</option>
							<option value="320000" p="6">江苏省</option>
							<option value="330000" p="6">浙江省</option>
							<option value="340000" p="6">安徽省</option>
							<option value="350000" p="6">福建省</option>
							<option value="360000" p="6">江西省</option>
							<option value="370000" p="6">山东省</option>
							<option value="410000" p="6">河南省</option>
							<option value="420000" p="6">湖北省</option>
							<option value="430000" p="6">湖南省</option>
							<option value="440000" p="6">广东省</option>
							<option value="450000" p="6">广西壮族自治区</option>
							<option value="460000" p="6">海南省</option>
							<option value="500000" p="6">重庆市</option>
							<option value="510000" p="6">四川省</option>
							<option value="520000" p="6">贵州省</option>
							<option value="530000" p="6">云南省</option>
							<option value="540000" p="6">西藏自治区</option>
							<option value="610000" p="6">陕西省</option>
							<option value="620000" p="6">甘肃省</option>
							<option value="630000" p="6">青海省</option>
							<option value="640000" p="6">宁夏回族自治区</option>
							<option value="650000" p="6">新疆维吾尔自治区</option>
							<option value="710000" p="6">台湾省</option>
							<option value="810000" p="6">香港特别行政区</option>
							<option value="820000" p="6">澳门特别行政区</option>
						</select>
                        <select name="modifyCity" id="modifyCity" onchange="SelectModifyChange(this,3);" style="color: #666; font-family: @微软雅黑;width:150px;">
                            <option value="0">请选择城市...</option>
                        </select>
                        <select name="modifyQzone" id="modifyQzone" style="color: #666; font-family: @微软雅黑;width:150px;">
                            <option value="0">请选择区/县...</option>
                        </select>
                    </li>
                    <li><span>邮政编码：<i class="red" style="color: Red;">*</i></span>
                        <input name="postal_code" id="PostCode" type="text" class="text05" value="请填写邮政编码"  style="overflow: auto; height: 32px; line-height: normal;width:150px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;">
                    </li>
                    <li><span>街道地址：<i class="red" style="color: Red;">*</i></span>
                        <textarea name="street" id="Address" cols="" rows="" style="color: #666; font-family: @微软雅黑;resize: none;width:400px;height:70px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;" maxlength="120" onchange="this.value=this.value.substring(0, 120)" onkeydown="this.value=this.value.substring(0, 120)" onkeyup="this.value=this.value.substring(0, 120)">不需要重复填写省市区，必须大于5个字符，小于120个字符</textarea>
                    </li>
                    <li><span>收货人姓名：<i class="red" style="color: Red;">*</i></span>
                        <input name="receiver" id="TrueName" type="text" class="text05" maxlength="25" value="长度不超过25个字"  style="overflow: auto; height: 32px; line-height: normal;width:150px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;">
                    </li>
                    <li><span>手机号码：<i class="red"></i></span>
                        <input name="mobile" id="Mobile" type="text" class="text05" value="电话、手机号码必须填一项"  style="overflow: auto; height: 32px; line-height: normal;width:150px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;">
                    </li>
                    <li><span>电话号码：<i class="red"></i></span>
                        <input name="Code" id="Code" type="text" class="text06" value="区号"  style="overflow: auto; height: 32px; line-height: normal;width:45px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;">
                        <input name="Tel" id="Tel" type="text" class="text07" value="电话号码"  style="overflow: auto; height: 32px; line-height: normal;width:120px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;">
                        <input name="Extension" id="Extension" type="text" class="text06" value="分机"  style="overflow: auto; height: 32px; line-height: normal;width:42px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;">
                    </li>
                    <li><span>身份证：<i class="red"></i></span>
                        <input name="id_card" id="id_card" type="text" class="text05" maxlength="25" value=""  style="overflow: auto; height: 32px; line-height: normal;width:150px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;">
                    </li>
                    <li style="display: none;"><span>设为默认地址：<i class="red"></i></span>
                        <input name="isdefault" value="0" id="IsDefault" type="checkbox">
                        设置为默认收货地址 </li>
                    <li style="display: none;"><span>证件号码：<i class="red" style="color: Red;">*</i></span>
                        <input name="idcard" id="Identity" type="text" class="text05" value="证件号码" tip="证件号码" style="overflow: auto; height: 32px; line-height: normal;width:150px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;">
                        <div style="width: 55px; height: 32px; background-color: #0fa6d8; float: left; border-radius: 5px;
                            color: #fff; text-align: center; display: none; cursor: pointer;" id="auditDiv" title="该身份证号已审核通过">
                            已审核</div>
                    </li>
                    <li class="Special" style="display: none;"><span>证件照片：<i class="red"></i></span>
                        <div style="width: 120px; height: auto; min-height: 40px; overflow: hidden; float: left;">
                            <div style="float: left; margin-top: 8px;" class="normal">
                                <a href="javascript:;" class="notSafari">
                                    <div class="ke-inline-block " style="position:relative;">
									<span class="ke-button-common" style="width: 120px; height: 36px; cursor: pointer; background-image: url(http://img.cdn.birdex.cn//images/btn_front.png);">
									<input type="button" class="ke-button-common ke-button" value="" style="margin-right: 0px; width: 120px; height: 36px; cursor: pointer; background-image: url(http://img.cdn.birdex.cn//images/btn_front.png); background-position: 100% 50%;">
									</span>
									<input type="file" id="frontFace" class="ke-upload-file" name="imgFile" tabindex="-1" style="position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;font-size: 30px;text-align: right;filter: alpha(opacity=0);opacity: 0;outline: none;background: white;cursor: pointer;display: block;">
									 
									</div>
									<input type="file" name="IdentityImg" id="IdentityImg" style="display: none;"></a> 
									<a href="javascript:;" class="isSafari" style="display: none;">
                                        <input type="file" id="safariIdentityImg" name="safariIdentityImg"></a>
                            </div>
                            <div style="float: left; margin-top: 5px; width: 116px; margin-left: 2px;" class="pass">
                                <img src="${resource_path}/img/Iden.jpg" width="116" id="showIDCard" alt="证件照正面">
								<input type="hidden" name="front_img"  id="front_img" value="${resource_path}/img/Iden.jpg"/>
                            </div>
                        </div>
                        <div style="width: 130px; height: auto; min-height: 40px; overflow: hidden; float: left;">
                            <div style="float: left; margin-left: 10px; margin-top: 8px;" class="normal">
                                <a href="javascript:;" class="notSafari">
                                    <div class="ke-inline-block" style="position:relative;">									
									<span class="ke-button-common" style="width: 120px; height: 36px; cursor: pointer;background-image: url(http://img.cdn.birdex.cn//images/btn_verso.png);">
									<input type="button" class="ke-button-common ke-button" value="" style="margin-right: 0px; width: 120px; height: 36px; cursor: pointer; background-image: url(http://img.cdn.birdex.cn//images/btn_verso.png); background-position: 100% 50%;">
									</span>
									<input type="file" id="backFace" class="ke-upload-file" name="imgFile" tabindex="-1" style="position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;font-size: 30px;text-align: right;filter: alpha(opacity=0);opacity: 0;outline: none;background: white;cursor: pointer;display: block;">
								 
									</div>
									<input type="file" name="IdentityImgEx" id="IdentityImgEx" style="display: none;"></a> 
									<a href="javascript:;" class="isSafari" style="display: none;">
                                        <input type="file" id="safariIdentityImgEx" name="safariIdentityImgEx"></a>
                            </div>
                            <div style="float: left; margin-top: 5px; width: 116px; margin-left: 12px;" class="pass" id="lastPass">
                                <img src="${resource_path}/img/IdenEx.jpg" width="116" id="showIDCardEx" alt="证件照反面">
								<input type="hidden" name="back_img" id="back_img" value="${resource_path}/img/IdenEx.jpg"/>
                            </div>
                        </div>
                        <div style="float: left;" class="normal">
                            <label id="show" style="margin-left: 5px;">
                            </label>
                        </div>
                    </li>
                </ul>
               	<input type="hidden" id="address_id" name="address_id" value="-1"/>
                <input type="hidden" name="region" id="region" value=""/>
                <input type="hidden" name="tel"  id="tel" value=""/>
                </form>
	</div>
	<div class="PushButton" id="newAddrsave" style="margin-top: 40px;">
				<a class="Block input14 white" href="javascript:saveNewAddr()" style="margin-right:30px;">保存</a> 
				<a style="text-decoration: none;
					cursor: pointer; color: #666; width: 70px; font-family: &#39;微软雅黑&#39;; background-image: url(http://img.cdn.birdex.cn/images/input36.png);
					text-align: center;" class="button" id="cancelNew" href="javascript:close()">取 消</a>
	</div>
	<div class="PushButton" id="currentAddrsave" style="margin-top: 40px;display:none;">
				<a class="Block input14 white" href="javascript:saveCurrentAddr()" style="margin-right:30px;">保存</a> 
				<a style="text-decoration: none;
					cursor: pointer; color: #666; width: 70px; font-family: &#39;微软雅黑&#39;; background-image: url(http://img.cdn.birdex.cn/images/input36.png);
					text-align: center;" class="button" id="cancelNew" href="javascript:close()">取 消</a>
	</div>
</div>
<script type="text/javascript">
var domUrl='${resource_path}';
var index = parent.layer.getFrameIndex(window.name);
var faceType=1;
 $('#frontFace').click(function(){
 	faceType=1;
 		
 });
 
 $('#backFace').click(function(){
 	faceType=2;
 	
 });
 var element=document.getElementById("frontFace");
	UpfileObject.initialize({
		elements:[element],
		baseURL:"${mainServer}/upload/initUploadIdCard",
		complete: function(response, index){				
		}
	});
	
	var element=document.getElementById("backFace");
	UpfileObject.initialize({
		elements:[element],
		baseURL:"${mainServer}/upload/initUploadIdCard",
		complete: function(response, index){
			
		}
	});
	//裁切完成之后显示身份证图片路径
	function showIdcard(path){
		var mainServer='${mainServer}';
		var newSrc=mainServer+"/"+path;
		if(faceType ==1 ){
			$('#showIDCard').attr("src",newSrc);
			$('#front_img').val(path);
		}
	 	if(faceType ==2){
	 		$('#showIDCardEx').attr("src",newSrc); 	
	 		$('#back_img').val(path);
	 	}
	}	

//提交新地址
function saveNewAddr(){
	var address_id=$("#addrid").val();
	var package_id=$("#package_id").val();
	var url = "${mainServer}/useraddr/updateAddress";
	$.ajax({
		url:url,
		type:'post',
		dataType:'json',
		async:false,
		data:{address_id:address_id,package_id:package_id},
		success:function(data){
			if(data){
				var addressStr=data['addressStr'];
				var receiver=data['receiver'];
				var mobile=data['mobile'];
				var package_id=data['package_id'];
				var str=addressStr+"|"+receiver+"|"+mobile;
				window.parent.updateUserAddress(str,receiver,package_id);
				window.parent.layer.closeAll();
			}
		}
	});
}
//提交修改后的地址
function saveCurrentAddr(){
	var modifyProvince=$('#modifyProvince').val();
	var modifyProvinceText=$("#modifyProvince").find("option:selected").text();
	if(modifyProvince == 0){
		layer.msg("请选择省", 2, 5);
		return;
	}
	var modifyCity=$('#modifyCity').val();
	var modifyCityText=$("#modifyCity").find("option:selected").text();
	if(modifyCity == 0){
		layer.msg("请选择市", 2, 5);
		return;
	}
	var modifyQzone=$('#modifyQzone').val();
	var modifyQzoneText=$("#modifyQzone").find("option:selected").text();
	
	//拼接省，市，区								
	var data=[];
	if(modifyCity == 0){
		data=[{"id":modifyProvince,"name":modifyProvinceText,"level":"1"}];
	}else if(modifyCity != 0 && modifyQzone == 0){
		data=[{"id":modifyProvince,"name":modifyProvinceText,"level":"1"},{"id":modifyCity,"name":modifyCityText,"level":"2"}];
	}else{
		data=[{"id":modifyProvince,"name":modifyProvinceText,"level":"1"},{"id":modifyCity,"name":modifyCityText,"level":"2"},{"id":modifyQzone,"name":modifyQzoneText,"level":"3"}];
	}
	var jsonStr =JSON.stringify(data);
	//检验邮政编码
	var reg=/^\d+$/;
	var postCodeVal=$("#PostCode").val();
	if(reg.test(postCodeVal)==false){
		layer.msg("邮政编码只能为数字", 2, 5);
		return;
	}
	//获取街道地址长度
	var textLength=$("#Address").val().length;
	var textVal=$("#Address").val();
	if(textLength<=5 || textVal == "不需要重复填写省市区，必须大于5个字符，小于120个字符"){
		layer.msg("地址长度必须大于5个字符", 2, 5);
		return;
	}
	//获取收件人姓名
	var trueNameVal=$("#TrueName").val();
	if(trueNameVal =="长度不超过25个字"){
		layer.msg("请输入收件人姓名", 2, 5);
		return;
	}			
	//获取手机号码
	var mobileVal=$("#Mobile").val();
	
	//获取电话号码
	var Code=$('#Code').val();
	var Tel=$('#Tel').val();
	var Extension=$('#Extension').val();
	var tel=Code+"-"+Tel+"-"+Extension;
	if(!mobileVal.match(/^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/)){
		layer.msg("手机号码格式不正确", 2, 5);
		return;
	}
	if(mobileVal=="电话、手机号码必须填一项" && (Code =="区号" || Tel =="电话号码")){
		layer.msg("电话、手机号码必须填一项", 2, 5);
		return;
	}			
	var address_id=$("#address_id").val();
	var package_id=$("#package_id").val();
	
	var id_card=$('#id_card').val();
	var regIdCard=/^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;

	if (id_card != null && id_card != '') {
			if (!(regIdCard.test(id_card))) {
				alert("身份证号码有误，请重填");
				return false;
			}
	}
		var url = "${mainServer}/useraddr/updateCurrentAddress";
		$.ajax({
			url : url,
			type : 'post',
			dataType : 'json',
			async : false,
			data : {
				address_id : address_id,
				region : jsonStr,
				postal_code : postCodeVal,
				street : textVal,
				receiver : trueNameVal,
				mobile : mobileVal,
				tel : tel,
				package_id : package_id,
				idcard:id_card
			},
			success : function(data) {
				if (data) {
					var addressStr = data['addressStr'];
					var receiver = data['receiver'];
					var mobile = data['mobile'];
					var package_id = data['package_id'];
					var idcard = data['idcard'];
					var str = addressStr + "|" + receiver + "|" + mobile+"|"+idcard;
					window.parent.updateUserAddress(str, receiver, package_id);
					window.parent.layer.closeAll();
				}
			}
		});

	}
	function close() {
		window.parent.layer.closeAll();
	}
	function SelectAddressChange() {
		var addressVal = $("#addrid").val();
		if (addressVal != 0) {
			$("#upAddress").hide();
			$("#currentAddrsave").hide();
			$("#newAddrsave").show();
		} else {
			$("#upAddress").show();
			$("#currentAddrsave").show();
			$("#newAddrsave").hide();
		}
	}
	//修改收货地址
	$('.edit').click(function() {
		$("#upAddress").show();
		$("#addrid").val("0");
		$("#newAddrsave").hide();
		$("#currentAddrsave").show();
		var $ul = $(this).parent().parent();
		var inputs = $ul.find('input');
		$.each(inputs, function(i, input) {
			var name = input.name;
			var val = input.value;

			if ('front_img' == name) {
				var mainServer = '${mainServer}';
				var newSrc = mainServer + "/" + val;
				$('#showIDCard').attr("src", newSrc);
				$('#front_img').val(val);
				$('.ke-inline-block').css("display", "none");
			}
			if ('back_img' == name) {
				var mainServer = '${mainServer}';
				var newSrc = mainServer + "/" + val;
				$('#showIDCardEx').attr("src", newSrc);
				$('#back_img').val(val);
				$('.ke-inline-block').css("display", "none");
			}
			if ('receiver' == name) {
				$('#TrueName').val(val);
			}
			if ('street' == name) {
				$('#Address').val(val);
			}
			if ('postal_code' == name) {
				$('#PostCode').val(val);
			}
			if ('mobile' == name) {
				$('#Mobile').val(val);
			}
			if ('idcard' == name) {
				$('#id_card').val(val);
			}
			if ('isdefault' == name) {
				if (val == "1") {
					$('#IsDefault').attr("checked", true);
				} else {
					$('#IsDefault').attr("checked", false);
				}
			}
			if ('idcard' == name) {
				$('#Identity').val(val);
			}
			if ('address_id' == name) {
				$('#address_id').val(val);
			}
			if ('region' == name) {
				var array = $.parseJSON(val);
				//初始化地区的默认值
				configRegion(array);
			}

		});
	});
	//初始化地区数据
	function configRegion(array) {
		var province = '';
		var city = '';
		var area = '';

		for (i = 0; i < array.length; i++) {
			var item = array[i];

			if (item.level == 1) {
				province = item.id;
			}
			if (item.level == 2) {
				city = item.id;
			}
			if (item.level == 3) {
				area = item.id;
			}

		}
		if (province != '') {
			$('#modifyProvince').val(province);

			var modifyProvince = $('#modifyProvince')[0];
			//初始化下拉菜单

			SelectModifyChange(modifyProvince, 2);
			if (city != '') {

				$('#modifyCity').val(city);
				var modifyCity = $('#modifyCity')[0];
				SelectModifyChange(modifyCity, 3);
				if (area != '') {
					$('#modifyQzone').val(area);
				}
			}

		}
	}
	var provinceString = '${provinceString}';
	var cityString = '${cityString}';
	var areaString = '${areaString}';
	var province = $.parseJSON(provinceString);
	var city = $.parseJSON(cityString);
	var area = $.parseJSON(areaString);

	function query_button_click() {
		var q_userName = $("#q_userName").val();
		var q_moblieNum = $("#q_moblieNum").val();
		$.ajax({
			url : '${mainServer}/useraddr/queryAddressByUserNameAndMobile',
			type : 'post',
			data : {
				userName : q_userName,
				moblieNum : q_moblieNum
			},
			dataType : "json",
			async : false,
			success : function(data) {
				if (data.flag == 'Y') {
					$("#addrid").val(data.address_id);
				}
			}
		});
	}
</script>
</body>
</html>