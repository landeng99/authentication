<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syproblem.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syembgo.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syproblem.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syembgo.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/img/favicon.ico" media="screen" rel="shortcut icon" type="image/x-icon" />
<script type="text/javascript" src="${resource_path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<jsp:include page="header-1.jsp"></jsp:include>

<div id="standard" class="standard">
	<div class="pricest" style="font-size:24px;">资费标准</div>
	<div class="pricest-line" style="margin-bottom: 40px;"></div>

	
<div id="price01" style="margin-left:auto; margin-right:auto; width:100%">
<table cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse; border-color:#7A8EC1">
 <tr style="height:40px;">
    <td colspan="2" width="821" style="padding-left:5px; background-color:#7A8EC1; color:#FFF; font-size:18px; font-weight:bold; border-color:#000000">电商清关模式</td>
  </tr>
  <col width="25%" />
  <col width="75%" />
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">渠道</td>
    <td style="padding-left:5px; font-size:14px; ">F渠道（不做关税补贴）</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">收费标准</td>
    <td style="padding-left:5px; font-size:14px;">首3.5$/磅，续1.75$/0.5磅</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">产品特性</td>
	 <td style="padding-left:5px; font-size:14px;">征收11.9%关税（无50元免征额），税费=申报货值*税率（如）税费=500（申报货值）*11.9%（税率）=59.5 元</td>
  </tr>
  
   <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">身份证</td>
	 <td style="padding-left:5px; font-size:14px;">需提供国内收件人身份证号码</td>
  </tr>
  
    <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;color: red;">注：</td>
	 <td style="padding-left:5px; font-size:14px;">单个包裹价值小于等于2000元（RMB），不区分包裹可拆分或不可拆分；个人年度消费额小于等于20000元（RMB）；由跨境物流代收代缴。凡货值超过2000rmb的包裹都会被退运或者罚没。</td>
  </tr>
</table>
</div>
<br/>

<div id="price02" style="margin-left:auto; margin-right:auto; width:100%">
<table cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse; border-color:#7A8EC1" width="100%">
 <tr style="height:40px;">
    <td colspan="2" width="821" style="padding-left:5px; background-color:#7A8EC1; color:#FFF; font-size:18px; font-weight:bold; border-color:#000000">关税补贴模式</td>
  </tr>
  <col width="25%" />
  <col width="75%" />
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">渠道</td>
    <td style="padding-left:5px; font-size:14px; ">A渠道（普货类）</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">收费标准</td>
    <td style="padding-left:5px; font-size:14px;">5$/磅，续2.5$/0.5磅</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">打包要求</td>
	 <td style="padding-left:5px; font-size:14px;">衣服（T恤6件以内、外套3件以内）、鞋子（3双以内）、裤子、奶粉、母婴用品、保健品、食品、日用品等</td>
  </tr>
  
   <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">身份证</td>
	 <td style="padding-left:5px; font-size:14px;">需提供国内收件人身份证号码</td>
  </tr>
  
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;color: red;">注：</td>
	 <td style="padding-left:5px; font-size:14px;">单个包裹货值不得超过2000元人民币，个人年度消费额小于等于20000元（RMB）普通衣服只收AF、Tommy、CK等同性质品牌，奶粉注明段数和克数。</td>
  </tr>
</table>
<br/>

<div id="price02" style="margin-left:auto; margin-right:auto; width:100%">
<table cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse; border-color:#7A8EC1" width="100%">
 <tr style="height:40px;">
    <td colspan="2" width="821" style="padding-left:5px; background-color:#7A8EC1; color:#FFF; font-size:18px; font-weight:bold; border-color:#000000">关税补贴模式</td>
  </tr>
  <col width="25%" />
  <col width="75%" />
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">渠道</td>
    <td style="padding-left:5px; font-size:14px; ">B渠道（包包、化妆品类）</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">收费标准</td>
    <td style="padding-left:5px; font-size:14px;">首7$/磅，续3.5$/0.5磅</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">打包要求</td>
	 <td style="padding-left:5px; font-size:14px;">钱包一个包裹不得超过4个、挎包一个包裹不得超过两个。</td>
  </tr>
  
   <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">身份证</td>
	 <td style="padding-left:5px; font-size:14px;">需提供国内收件人身份证号码</td>
  </tr>
  
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;color: red;">注：</td>
	 <td style="padding-left:5px; font-size:14px;">单个包裹货值不得超过2000元人民币个人年度消费额小于等于20000元（RMB）。包包、化妆品不接受一线品牌，如LV，Gucci等</td>
  </tr>
</table>
<br></br>
<td style="padding-left:5px; font-size:14px;">注：手表、首饰、电子产品，小家电等其他没有包含在A、B渠道中的产品，请选择走F渠道。</td>
</div>


<div id="price02" style="margin-top:50px; margin-left:auto; margin-right:auto; width:100% ">
<table cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse; border-color:#2473ba">
  <col width="10%" />
  <col width="15%" />
  <col width="20%" />
  <col width="40%" />
  <tr>
    <td colspan="4" width="821" style="text-align:center; height:40px; background-color:#7A8EC1; color:#FFF;  font-size:18px; font-weight:bold; border-color:#000000">增值服务价格</td>
  </tr>
  <tr style="font-size:16px; height:40px; background-color:#7A8EC1; color:#FFF;  font-weight:bold; border-color:#000000">
    <td>　</td>
    <td style="padding-left:5px">普通会员</td>
    <td style="padding-left:5px">大客户</td>
    <td style="padding-left:5px">备注说明</td>
  </tr>
  <tr style="font-size:16px; height:40px; ">
    <td style="padding-left:5px">加固</td>
    <td style="padding-left:5px">$8.00</td>
    <td style="padding-left:5px">$6.00</td>
    <td style="padding-left:5px">为包裹做加固操作</td>
  </tr>
  <tr style="font-size:16px; height:40px; ">
    <td style="padding-left:5px">拍照</td>
    <td style="padding-left:5px">$8.00</td>
    <td style="padding-left:5px">$6.00</td>
    <td style="padding-left:5px">为包裹内件进行拍照</td>
  </tr>
  <tr style="font-size:16px; height:40px; ">
    <td style="padding-left:5px">去鞋盒</td>
    <td style="padding-left:5px">免费</td>
    <td style="padding-left:5px">免费</td>
    <td style="padding-left:5px">取出包裹中的鞋盒</td>
  </tr>
  <tr style="font-size:16px; height:40px;">
    <td style="padding-left:5px">取小票</td>
    <td style="padding-left:5px">免费</td>
    <td style="padding-left:5px">免费</td>
    <td style="padding-left:5px">取出包裹中的小票</td>
  </tr>
  <tr style="font-size:16px; height:40px;">
    <td style="padding-left:5px">清点</td>
    <td style="padding-left:5px">$10.00</td>
    <td style="padding-left:5px">$8.00</td>
    <td style="padding-left:5px">清点包裹内件</td>
  </tr>
  <tr style="font-size:16px; height:40px;">
    <td style="padding-left:5px">分箱</td>
    <td style="padding-left:5px">免费</td>
    <td style="padding-left:5px">免费</td>
    <td style="padding-left:5px">按分箱后每个箱子收费，仅提供一次分箱服务</td>
  </tr>
  <tr style="font-size:16px; height:40px; ">
    <td style="padding-left:5px">合箱</td>
    <td style="padding-left:5px">$10.00</td>
    <td style="padding-left:5px">$8.00</td>
    <td style="padding-left:5px">按合箱后的箱子收费，最多3票合1，仅提供一次合箱服务</td>
  </tr>
  <tr style="font-size:16px; height:40px; ">
    <td style="padding-left:5px">保价</td>
    <td colspan="3" style="padding-left:5px">需交保费=保价额*1%</td>
  </tr>
</table>
</div>
	
</div>

</div> 
<br></br>
	<jsp:include page="footer-1.jsp"></jsp:include>
</body>
</html>