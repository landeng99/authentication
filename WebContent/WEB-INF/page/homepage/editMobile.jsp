<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">
p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}


.mg18 {
    margin-bottom: 18px;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
}
.hr {
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.List06 {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.List06 li {
    height: 52px;
    list-style-type:none
}
.List06 li span.pull-left {
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: right;
    width: 63px;
}
.List06 li input {
    margin-right: 4px;
}
.List06 li .text08 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 178px;
}

.List06 li .text09 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 78px;
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 250px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button2.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 72px;
}

.span4 .label{
     width:70px;
     float: left;
}

</style>
<body>
<div style="background-image:none;border-right:none;width:350px;padding-top:5px;"/>
<input type="hidden" id="checkMobileResult" value="1">
<input type="hidden" id="old_mobile" value="${old_mobile}">
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
        <div>
            <div class="hr mg18"></div>
            <div style="display: block;" id="withdrawal">
                <ul class="List06">
                      <li class="span4">
                         <p>
                            <span class="label">旧号码：</span>
                            <span>${old_mobile}</span>
                            <input id="mobile" type="hidden" value="${old_mobile}"/>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label" style="margin-top:5px;">新号码：</span>
                            <input type="text" class="text08" id="newmobile" name="newmobile" style="padding-left: 5px;"onblur="checkMobile()">
                            <span style="color:Red;">*</span>
                         </p>
                         <p>
                             <span class="label" >&nbsp;</span>
                             <span id ="message1" style="color: red;"></span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label" style="margin-top:5px;">验证码：</span>
                            <input type="text" class="text09" id="code" name="code" style="padding-left: 5px;">
                            <span style="color:Red;">*</span>
                            <input type="button" value="获取验证码" style="width:100px;height:30px;" id="getCode" name="getCode"/>
                         </p>
                         <p>
                             <span class="label" >&nbsp;</span>
                             <span id ="message2" style="color: red;"></span>
                         </p>
                      </li>
                  </ul>
                  <div class="PushButton">
                       <a id="no"  class="button" style="cursor:pointer;">取消</a>
                       <a id="yes" class="button" style="cursor:pointer;">确定</a>
                  </div>
           </div>
      </div>
   </div>

    <script type="text/javascript">
    
    function checkMobile(){

        var url = "${mainServer}/account/checkMobile";
        var mobile =$('#newmobile').val();
        var old_mobile =$('#old_mobile').val();

        if(mobile==null || mobile.trim()=="" ||old_mobile == mobile){
            $('#checkMobileResult').val("1");
            return;
        }
        $.ajax({
            url:url,
            type:'post',
            async:false,
            data:{"mobile":mobile},
            success:function(data){
                $('#checkMobileResult').val(data);
                if(data!="1"){
                  $('#message1').html("手机号已经存在！");
                }
            }
            
        });
    }
    
    var mobile_bk=null;
    var second=120;
    var recover=function (){
        $("#getCode").removeAttr('disabled');
        $("#getCode").val('免费获取验证码');
        clearInterval(sh);
        clearInterval(timer_sh);
        //120秒之后重新设置为120
        second=120;
    };
    
  //计时器开始
    var timer=function(){
        second=second-1;
        $("#getCode").val(second+" 获取中......");
    }
    var sh=null;
    var timer_sh=null;
    $("#getCode").click(function () {
        var newmobile =$('#newmobile').val();
        var mobile=$('#mobile').val();
        if(newmobile==null || newmobile.trim()==""){
            $('#message1').html("新号码不能为空！");
            $('#newmobile').focus();
            return;
        }
        
        if($('#checkMobileResult').val()!=1){
            $('#message1').html("手机号已经存在！");
            return;
         }
        
        $(this).attr('disabled','disabled');
        $(this).val('120 获取中......');
        sh=setInterval(recover,120000);
        timer_sh=setInterval(timer,1000);
        // 备份手机号
        mobile_bk=newmobile;
        $.ajax({
            url:'${mainServer}/sms/sendRegistCode',
            type:'post',
            data:{"mobile":mobile,"uncheckImgVC":"Y"},
            success:function(data){ 
            }
        });
    })

    //获取窗口索引
    var index = parent.layer.getFrameIndex(window.name);
    $("#no").click(function () {
        // 关闭窗口
        parent.layer.close(index);
    }); 

      $("#yes").click(function () {

          var mobile =$('#newmobile').val();
          var code =$('#code').val();
          var checkresult="0";
          if(mobile==null || mobile.trim()==""){
              $('#message1').html("新号码不能为空！");
              checkresult="1";
          }else if(!mobile.match(/^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/)){
             
            $('#message1').html("手机号格式错误！");

            checkresult="1";
          }
          
          if($('#checkMobileResult').val()!=1){
              $('#message1').html("手机号已经存在！");
              checkresult="1";
           }
          
          if(code==null || code.trim()==""){
              $('#message2').html("验证码不能为空！");
              checkresult="1";
          }
          
          if(checkresult=="1"){
             return;
          }
          
          if(mobile != mobile_bk){
              $('#message2').html("手机号码已变更，请重新获取验证码！");
              $("#getCode").removeAttr('disabled');
              $("#getCode").val('免费获取验证码');
              clearInterval(sh);
              clearInterval(timer_sh);
              second=120;
              return;
          }
          $('#message1').empty();
          $('#message2').empty();
          var url="${mainServer}/account/saveMobile";
          $.ajax({
              url:url,
              data:{"mobile":mobile,"code":code },
              type:'post',
              async:false,
              dataType:'json',
              success:function(data){
                  alert(data.message);
                  
                  if(data.result){
                	  var newMobile=data['mobile'];
                	  //alert(newMobile);
                	  //将新的手机号码传给父页面
                	  //showNewMobile(newMobile);
                      // 标识点击了保存 给父窗口传值标识修改成功
                      parent.$('#returnCode').val(newMobile);
                      // 关闭窗口
                      parent.layer.close(index);
                  }
              }
          });
      }); 

        </script>
</body>
</html>