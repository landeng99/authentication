<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
<div class="foot1"><div class="wrap">
	<div class="foot01">
    	<div class="foot-add"><ul>
        	<li><i class="ico12">&nbsp;</i>地址：广东省深圳市宝安区福永街道桥南新区宝华大厦301</li>
            <li><i class="ico14">&nbsp;</i>电话：0755-23204850</li>
            <li><i class="ico13">&nbsp;</i>营业时间：周一至周五：09:00-18:00，周六：09:00-12:00</li>
            <li><i class="ico15">&nbsp;</i>客服邮箱：service@su8exp.com</li>
        </ul></div>
        <p>深圳市程露祥供应链管理有限公司版权所有  粤ICP备17086482 Copyright 2017 su8exp.com  Inc. All Rights Reserved.</p>
    </div>


 <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=3181427009&site=qq&menu=yes"><img id="QQ" border="0" src="http://wpa.qq.com/pa?p=2:3181427009:51" alt="点击这里给我发消息" title="点击这里给我发消息"/></a>
 
</div></div>

<div id="lrToolRb" class="lr-tool-rb" style="z-index:10000;">
<a class="lr-gotop" title="返回顶部">
<span class="lricon iconArrow"><img src="${resource_path}/img/gotop.jpg" style="width:100%;" alt="soe"/></span></a>
<a class="lr-QQ" title="单击联系QQ客服" onclick="SetQQ();">
<span class="lricon"><img src="${resource_path}/img/qqservice.jpg" style="width:100%;" alt="soe"/></span></a>
<a class="lr-qrcode" title="关注获取最新优惠，实时跟踪包裹进度">
<span class="lricon"><img src="${resource_path}/img/weixin.jpg" style="width:100%;" alt="soe"/></span></a><div class="lr-pop">
<span class="lr-close">关闭</span><span class="lr-qcimg"></span></div></div>
 --%>
    <!-- 尾部 -->
    <div class="wrap footer">
        <div class="wrap-box ">
            <div class="item">
                <div class="logo-foot">
                    
                </div>
                <p>在这里，我们真实传递速8快递发生的每件事情；在这里，你们及时分享速8快递发展的点滴故事。</p>
            </div>
            <div class="item">
                <h4><i class="iconfont icon-zhinanzhen"></i>办公地址</h4>
                <table class="table table-nobor">

                    <tbody>
                        <tr>
                            <td width="50">地址：</td>
                            <td>广东省深圳市宝安区福永街道桥南新区宝华大厦301</td>
                        </tr>
                        <tr>
                            <td>电话：</td>
                            <td>0755-23204850</td>
                        </tr>
                        <tr>
                            <td>邮箱：</td>
                            <td>service@su8exp.com</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="item">
                <h4><i class="iconfont icon-kefu1"></i>在线客服</h4>
                <div class="share">
                    <a href="javascript:void(0)"><i class="iconfont icon-weixin"></i><span>微信</span></a>
                    <a href="http://wpa.qq.com/msgrd?v=3&uin=3181427009&site=qq&menu=yes" target="_blank"><i class="iconfont icon-qq"></i><span>QQ</span></a>
                </div>
            </div>
            <div class="item">
                <h4><i class="iconfont icon-shijian"></i>营业时间</h4>
                <p>周一至周五（9：00-18：00）</p>
                <p>周六（9：00-12：00）</p>
            </div>
        </div>
         <div class="copyright">© 深圳市程露祥供应链管理有限公司版权所有 粤ICP备17086482 Copyright 2017 su8exp.com Inc. All Rights Reserved.</div>
    </div>
    
       <!-- 工具栏 -->
       <div class="toolbar">
            <div class="barbox">
                <a href="javascript:void(0)" class="tool-btn weixin " onclick="showWx()">                  
                    <i class="iconfont icon-weixin"></i>
                   	 微信
                    <div class="qrcode_weixin" id="qrcode_weixin"> <!-- selected 显示，去掉隐藏 -->
                      <img class="img" src="${resource_path}/images/weixinQrcedo.jpg">
                      <a href="javascript:void(0)" class="closeBar" onclick="closeWx(this)">关闭</a>
                    </div>
                </a>
                <a href="http://wpa.qq.com/msgrd?v=3&uin=3181427009&site=qq&menu=yes" target="_blank" class="tool-btn qq">
                    <i class="iconfont icon-qq"></i>
                    QQ
                </a>
            </div>
            <div class="barbox">
                <a href="javascript:void(0)" onclick="toolGoTop()" class="tool-btn go-top">
                    <i class="iconfont icon-top"></i>
                </a>
            </div>
       </div>
       <script type="text/javascript">
       function toolGoTop(){
    		{$('html,body').animate({scrollTop: '0px'}, 800);} 
    	}
       function showWx() {
    	   $('#qrcode_weixin').addClass('selected');
       }
       function closeWx(obj) {
    	   $(obj).parent().removeClass('selected');
       }
		</script>
</body>
</html>