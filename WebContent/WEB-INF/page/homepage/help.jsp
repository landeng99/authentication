<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0021)http://www.birdex.cn/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<!-- 
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syproblem.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syembgo.css" rel="stylesheet" type="text/css"/>
 -->
<link href="${resource_path}/img/favicon.ico" media="screen" rel="shortcut icon" type="image/x-icon" />
</head>
<body>

<jsp:include page="topIndexPage.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/subPage.css"></link>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<script src="${backJsFile}/json2.js"></script>

<input type="hidden" id="requestItem" value="${item}" />

<div class="wrap-box pb20 pt20 clearfix">
	<ul class="breadcrumb">
		<li><a><i class="iconfont icon-shouye"></i>首页</a></li>
		<li class="active"><span>用户指南</span></li>
	</ul>
	<div class="clearfix" id="s-content">
		
	</div>
</div>

<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.js"></script>

<script type="text/javascript">
$(function () {
	queryLeftIndexPage();
});

function queryLeftIndexPage() {
	$.ajax({
		type: "post",
		dataType: "html",
		url: '${mainServer}/queryLeftIndexPage',
		data: {},
		success: function (data) {
			$("#s-content").empty();
			$("#s-content").append(data);
		}
	});
}
</script> 
<jsp:include page="footer-1.jsp"></jsp:include>
</body>
</html>