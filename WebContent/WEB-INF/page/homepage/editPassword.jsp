<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">

p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}


.mg18 {
    margin-bottom: 18px;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
}
.hr {
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.List06 {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.List06 li {
    height: 52px;
    list-style-type:none
}
.List06 li span.pull-left {
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: right;
    width: 63px;
}
.List06 li input {
    margin-right: 4px;
}
.List06 li .text08 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 178px;
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 250px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button2.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 72px;
}

.span4 .label{
     width:70px;
     float: left;
}

</style>
<body>
<div style="background-image:none;border-right:none;width:350px;padding-top:5px;"/>
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
        <div>
            <div class="hr mg18"></div>
            <div style="display: block;" id="withdrawal">
                <ul class="List06">
                      <li class="span4">
                         <p>
                            <span class="label" style="margin-top:5px;">旧密码：</span>
                            <input type="password" class="text08" id="old_pwd" name="old_pwd" style="padding-left: 5px;">
                            <span style="color:Red;">*</span>
                         </p>
                         <p>
                             <span class="label" >&nbsp;</span>
                             <span id ="message1" style="color: red;"></span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label" style="margin-top:5px;">新密码：</span>
                            <input type="password" class="text08" id="user_pwd" name="user_pwd" style="padding-left: 5px;">
                            <span style="color:Red;">*</span>
                         </p>
                         <p>
                             <span class="label" >&nbsp;</span>
                             <span id ="message2" style="color: red;"></span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label" style="margin-top:5px;">确认密码：</span>
                            <input type="password" class="text08" id="user_password" name="user_password" style="padding-left: 5px;">
                            <span style="color:Red;">*</span>
                         </p>
                         <p>
                             <span class="label" >&nbsp;</span>
                             <span id ="message3" style="color: red;"></span>
                         </p>
                      </li>
                  </ul>
                  <div class="PushButton">
                       <a id="no"  class="button" style="cursor:pointer;">取消</a>
                       <a id="yes" class="button" style="cursor:pointer;">确定</a>
                  </div>
           </div>
      </div>
   </div>

    <script type="text/javascript">

    //获取窗口索引
    var index = parent.layer.getFrameIndex(window.name);
    $("#no").click(function () {
        // 关闭窗口
        parent.layer.close(index);
    }); 

      $("#yes").click(function () {
		  
    	  //清空提示信息
    	  clearPrompt();
    	  
          var old_pwd =$('#old_pwd').val();
          var user_pwd =$('#user_pwd').val();
          var user_password =$('#user_password').val();
          var checkResult ="0";
          if(old_pwd==null || old_pwd.trim()==""){
              $('#message1').html("旧密码不能为空！");
              checkResult = "1";
          }

          if(user_pwd==null||user_pwd.trim()==""){
              $('#message2').html("新密码不能为空！");
              checkResult = "1";
          }
          
      	  var bValidate = RegExp(/^((?=.*\d)(?=.*\D)|(?=.*[a-zA-Z])(?=.*[^a-zA-Z]))^.{8,16}$/).test(user_pwd);
    	  if(!bValidate){
    		  $("#message2").html("<font color='red'>长度8~16位，数字、字母、符号至少包含两种!</font>");
    		  checkResult = "1";
    	  }
          
          if(user_pwd!=user_password){
        	  $('#message3').html("两次输入密码不一致！");
              checkResult = "1";
          }
          
          if(checkResult=="1"){
              return;
          }
          $('#message1').empty();
          $('#message2').empty();
          $('#message3').empty();
          var url="${mainServer}/account/savePassword";
          $.ajax({
              url:url,
              data:{
                  "old_pwd":old_pwd,
                  "user_pwd":user_pwd},
              type:'post',
              async:false,
              dataType:'json',
              success:function(data){
                  alert(data.message);
                  
                  if(data.result){
                      // 关闭窗口
                      parent.layer.close(index);
                  }
              }
          });
      }); 
      
      //清空所有的提示信息
      function clearPrompt(){
    	  $('#message1').html("");
    	  $('#message2').html("");
    	  $('#message3').html("");
    	  
      }
        </script>
</body>
</html>