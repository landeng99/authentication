<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>海外地址</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script>
    <script type="text/javascript" src="${resource_path}/js/common.js"></script>
	<script type="text/javascript" src="${resource_path}/js/warehouse.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
    
</head>
<body class="bg-f1">
<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->
<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="#"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li><a href="#"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li class="active"><a href="#"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="#"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="#"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="#"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="#"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <!--<li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>-->
        </ul>
    </div>
    <!--左侧导航栏结束-->
    <!--右侧内容开始-->
    <div class="right">
        <div class="column mt25 p20">
        	<div class="address">
              <div class="opt">
                   <ul class="function pull-left" id="warehouse">
                   <c:forEach items="${overseasAddresses}" var="overPojo" varStatus="addrStatus">
                       <c:if test="${addrStatus.count==1}">
                       		<li class="span3 current" item="${overPojo.id}">
                       </c:if><c:if test="${addrStatus.count!=1}">
                       <li class="span3" item="${overPojo.id}">
                       </c:if>
                       	<a name="liClassColor" href="javascript:void(0);" onclick="rightSpan(${overPojo.id})">
                       	<i class="first"></i>
                       	<img style="width: 21px;height: 21px;float: left;margin-top: 5px;margin-left: 8px;" src="${resource_path}/${overPojo.flag_path}"/>${overPojo.country}&nbsp;&nbsp;&nbsp;<c:if test="${overPojo.is_tax_free==2}">非</c:if>免税州</a></li>
                   </c:forEach>
                       <li class="span9" id="wait"><a>Coming soon! 其他仓库即将上线~<i class="ultimate"></i></a></li>
                   </ul>
              </div>
              <div class="BOX" style="display:none;">
                   <a href="http://www.birdex.cn/warehouse.html#" class="icon17 pull-right"></a>
                   <p>温馨提示：如选用德国仓库，请将Frist Name，填写为: Dan Xu，LAST NAME保持不变。才能顺利收货哦！</p>
              </div>
              
              
              <c:forEach items="${overseasAddresses}" var="overPojo" varStatus="addrStatus">
               <c:if test="${addrStatus.count==1}">
               		<ul class="Detail" style="display: inline;" item="${overPojo.id}">
               </c:if><c:if test="${addrStatus.count!=1}">
               		<ul class="Detail" style="display: none;" item="${overPojo.id}">
               </c:if>
               
                  <li>
                      <div class="bt">First name：</div>
                      <div class="nr clip_container"><input name="trueName" id="trueName" type="text" value="KJ" class="copycnt" disabled="disabled"><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                      <div class="bt">Last name：</div>
                      <div class="nr clip_container"><input name="userCode" id="userCode" type="text" value="${frontUser.last_name}" class="copycnt" disabled="disabled"><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                      
                      <div class="BOX">
                           <a href="http://www.birdex.cn/warehouse.html#" class="icon17 pull-right" style="display:none;"></a>
                           <p>First name固定是KJ，Last name是系统自动生成的用户唯一代码，作为用户身份的识别标志，请在购物网站填写收货人时将First name以及Last name分别按此处内容填写(或者可直接点击复制按钮复制过去)，如：KJ NSNMGE。</p>
                      </div>
                  </li>
                  <li class="long">
                      <div class="bt">地址(Address)：</div>
                      <div class="nr clip_container"><input name="AddressInfo" id="AddressInfo" type="text" value="${overPojo.address_first}" class="copycnt" disabled="disabled"><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                      <div class="bt"></div>
                      <div class="nr clip_container"><input name="AddressInfo2" id="AddressInfo2" type="text" value="C/O&nbsp;${frontUser.last_name}" class="copycnt" disabled="disabled"><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                      
                      <div class="BOX">
                           <a href="http://www.birdex.cn/warehouse.html#" class="icon17 pull-right" style="display:none;"></a>
                           <p>您在购物网站上的收货地址会有两行，请按照此格式填写，以确保C/O后的编号能够打印到快递单上。如购物网站不认可此
收货地址，请删除”C/O”，但保留”C/O”后的内容再次提交收货地址。</p>
                      </div>
                  </li>
                  <li>
                      <div class="bt">城市(City)：</div>
                      <div class="nr clip_container"><input value="${overPojo.city}" name="Province" id="Province" type="text" class="copycnt"  disabled="disabled"/><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                      <div class="bt">州(State)：</div>
                      <div class="nr clip_container"><input value="${overPojo.state}" name="City" id="City" type="text" class="copycnt" disabled="disabled"/><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                  </li>
                  <li>
                      <div class="bt">县/郡(County)：</div>
                      <div class="nr clip_container"><input value="${overPojo.county}" name="District" id="District" type="text" class="copycnt" disabled="disabled"><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                      <div class="bt">国家(Country)：</div>
                      <div class="nr clip_container"><input value="${overPojo.country}" name="Country" id="Country" type="text" class="copycnt" disabled="disabled"><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                  </li>
                  <li>
                      <div class="bt">邮编(Zip Code)：</div>
                      <div class="nr clip_container"><input value="${overPojo.postcode}" name="PostCode" id="PostCode" type="text" class="copycnt" disabled="disabled"><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                      <div class="bt">电话(Tell)：</div>
                      <div class="nr clip_container"><input value="${overPojo.tell}" name="Tel" id="Tel" type="text" class="copycnt" disabled="disabled"><a href="javascript:void(0);" class="blue copyit">复制</a></div>
                  </li>
              </ul>
              
              </c:forEach>
          </div>
     	</div>
     </div>
</div>
<script type="text/javascript" src="${resource_path}/js/ZeroClipboard.js"></script>
<script type="text/javascript">
$(function(){
	$(".left-nav").hover(function () {
		$(this).css("background","url(${resource_path}/img/is5.png) no-repeat");
	}, function () {
		$(this).css("background","url(${resource_path}/img/is6.png) no-repeat");
		});
	$("#charge").click(function () {
        var rechargeOffHeight = ($(window).height() - 570) / 2;
        $.layer({
            title :'充值',
            type: 2,
            fix: false,
            shadeClose: true,
            shade: [0.5, '#ccc', true],
            border: [1, 0.3, '#666', true],
            offset: [rechargeOffHeight + 'px', ''],
            area: ['950px', '570px'],
            iframe: { src: '${mainServer}/member/rechargeInit'}
        });
    });
});
function rightSpan(itemId){
	$('li[class="span3 current"][item').removeClass("current");
	
	$('a[name="liClassColor"][href="javascript:void(0);"][onclick]')
		.css({"background-color":"#CCC","color":"#001;","background-image":"none;"});
	
	$('a[name="liClassColor"][href="javascript:void(0);"][onclick="rightSpan('+itemId+')"]')
		.css({"background-color":"#0097db","color":"#fff;","background-image":"none;"});
	
	$('ul[class="Detail"][item').hide();
	$('ul[class="Detail"][item="'+itemId+'"]').show();
}
</script>
</body>
</html>