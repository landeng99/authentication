<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>速翼个人中心首页</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">

</head>
<body class="bg-f1">
<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li class="active"><a href="${mainServer}/personalCenter"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li><a href="${mainServer}/transport"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="${mainServer}/warehouse"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="${mainServer}/useraddr/destination"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="${mainServer}/member/record"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="${mainServer}/account/accountInit"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="${mainServer}/account/couponInit"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
        <div class="tip mt15 mb10"><span class="btn-close floatR"><i class="icon iconfont icon-cuowu"></i></span> <i class="icon iconfont icon-laba"></i>暂停E通道收货通知——由于海关政策调整，现暂停E通道收货，如有不便敬请谅解，感谢您对速翼的支持和立即，谢谢...<a href="#" class="col_blue">查看全文</a> </div>

        <div class="column p20">
            <p class="data pb20">Hi，<small class="bg-red mr5"><c:if test="${frontUser.user_type==1}">直客</c:if><c:if test="${frontUser.user_type==2}">同行</c:if></small>
            <strong class="col_red">${frontUser.user_name}&nbsp;${frontUser.last_name}</strong><a href="#" class="col_blue"><i class="icon iconfont icon-bianji"></i>修改资料/密码</a> </p>
            <p class="data pb20">
                <span class="email"><i class="icon iconfont icon-youxiang"></i>邮箱：${frontUser.email}<small class="bg-green ml5">
                <c:if test="${frontUser.status==0}">未激活</c:if> 
                <c:if test="${frontUser.status==1}">正常</c:if> 
                <c:if test="${frontUser.status==2}">禁用</c:if>
                </small> </span>
                <span class="phone"><i class="icon iconfont icon-shouji"></i>手机号：<i id="mobile">${frontUser.mobile}</i><a href="javascript:;" class="col_blue ml10" id="editMobileHead">修改</a> </span>
            </p>
            <p class="data pb20">可用余额：<span class="col_red"><fmt:formatNumber value="${frontUser.able_balance+0.00001}" type="currency" pattern="$#,###.##" /></span>元
            <button class="btn btn-primary" id="charge">充值</button><button class="btn btn-default">明细</button> </p>
            <p class="data"><span class="pl15">优惠券：</span><span class="col_blue">${coupon_num}</span>张<button class="btn btn-default">查看</button> </p>
        </div>

        <div class="column02 mt25">
            <div class="order-list floatL">
                <p><small class="bg-orange"><i class="icon iconfont icon-dengpao"></i></small><a href="#">待处理订单（<span class="col_red">${t1}</span> ）</a> </p>
                <p><small class="bg-blue"><i class="icon iconfont icon-wodeqianbao"></i></small><a href="#">待支付运费（<span class="col_red">${t2}</span> ）</a> </p>
                <p><small class="bg-blue"><i class="icon iconfont icon-icon"></i></small><a href="#">待支付关税（<span class="col_red">${t3}</span> ）</a> </p>
            </div>
            <div class="package floatR pt15">
                <h3  class="title"><span>我的包裹</span></h3>
                <ul>
                    <li><a href="#"><span class="col_blue">${c1}</span><span>待入库</span></a> </li>
                    <li><a href="#"><span class="col_blue">${c2}</span><span>已入库</span></a> </li>
                    <li><a href="#"><span class="col_blue">${c3}</span><span>待发货</span></a> </li>
                    <li><a href="#"><span class="col_blue">${c4}</span><span>转运中</span></a> </li>
                    <li><a href="#"><span class="col_blue">${c5}</span><span>已签收</span></a> </li>
                    <li><a href="#"><span class="col_blue">${c6}</span><span>全部包裹</span></a> </li>
                </ul>
            </div>
        </div>
    </div>
    <!--右侧内容结束-->
<input type="hidden" id="returnCode" />
</div>
<!--中间内容结束-->

<!--底部开始-->
<div class="footer">
    粤ICP备14073551-2号，Copyright © 2016 www.su8exp.com Inc.All Rights Reserved.07693329337
</div>
<!--底部结束-->
<!--右侧固定客服开始-->
<div class="service">
    <a href="#"><i class="icon iconfont icon-qq"></i></a>
    <a href="#"><i class="icon iconfont icon-weixin"></i></a>
    <a href="#"><i class="icon iconfont icon-44"></i></a>
    <a href="#top"><i class="icon iconfont icon-top"></i></a>
</div>
<!--右侧固定客服结束-->
<script>
$(function(){
//修改手机号码
  $("#editMobileHead").click(function () {
      var height = ($(window).height() - 320) / 2;
      $.layer({
          title :'修改手机号',
          type: 2,
          fix: true,
          shade: [0.5, '#ccc', true],
          border: [0, 0.3, '#666', true],
          offset: [height + 'px', ''],
          area: ['400px', '330px',],
          iframe: { src: '${mainServer}/account/editMobileInit' },
          end : function(index) {
              // 点击保存的时候 刷新手机号码
              if ($('#returnCode').val() != "") {
                  $("#mobile").html($('#returnCode').val());
              }
          }
      });
  });
  //充值
  $("#charge").click(function () {
	  if(!validateLogin()){
		  return;
	  }
        var url = "${mainServer}/member/frontUserStatus";
        $.ajax({
            url:url,
            type:'post',
            async:false,
            dataType:'json',
            success:function(data){
                // 账户正常
                if(data.result =="1"){
                    var rechargeOffHeight = ($(window).height() - 570) / 2;
                    $.layer({
                        title :'充值',
                        type: 2,
                        fix: false,
                        shadeClose: true,
                        shade: [0.5, '#ccc', true],
                        border: [1, 0.3, '#666', true],
                        offset: [rechargeOffHeight + 'px', ''],
                        area: ['950px', '570px'],
                        iframe: { src: '${mainServer}/member/rechargeInit'}
                    });
                }
                else{
                   alert(data.message);
                }
            }
        });
    });
});
</script>
</body>
</html>