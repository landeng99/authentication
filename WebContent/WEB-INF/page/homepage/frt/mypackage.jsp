<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>速8我的包裹</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
	<script src="${resource_path}/js/layer.min.js"></script>
	<script src="${resource_path}/js/common.js"></script>
	<script src="${backJsFile}/My97DatePicker/WdatePicker.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
	<link rel="stylesheet" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" href="${resource_path}/css/common.css">
<script type="text/javascript">
	function checkBoxClick(checkBoxElem,v_package_id,package_status)
	{
		var handle_flag=false;
		var needEdit=false;
		if(package_status==0)
		{
			$.ajax({
				url:'${mainServer}/homepkg/packageMergeSplitCheck',
				type:'post',
				data:{packageIds:v_package_id,checkException:'N'},
				dataType:"json",
				async:false,
				success:function(data){
					if("S" == data.result){
						handle_flag=true;
					}
					if("Y" ==data.needEdit)
					{
						needEdit=true;
					}
				}			
			});
		}
		var v_waitting_merge_flag="";
		if(handle_flag)
		{
			if(/checked/ig.test(checkBoxElem.className))
			{
				checkBoxElem.className=checkBoxElem.className.replace('checked','');
				v_waitting_merge_flag="N";
			}else
			{
				checkBoxElem.className=(checkBoxElem.className+' checked');
				v_waitting_merge_flag="Y";
			}
		}else
		{
			if(needEdit)
			{
				alert("包裹信息不完整，无法进行分箱合箱");
			}else
			{
				alert("您所选择包裹已进行分箱｜合箱服务，不能再选择待合箱服务!");
			}
		}
		$("#waitting_merge_flag_div").html("<input type=\"hidden\" id=\"waitting_merge_flag"+v_package_id+"\" value=\""+v_waitting_merge_flag+"\"/>");
	}
</script>
</head>
<body class="bg-f1">

<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="#"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li class="active"><a href="#"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="#"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="#"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="#"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="#"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="#"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <!--<li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>-->
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
		<div id="waitting_merge_flag_div"></div>
        <div class="column mt25 p20">
            <div class="btn-list">
                <p class="upload-ID floatR">
                    <button class="btn btn-orange" onClick="uploadId()"><i class="icon iconfont icon-nanrenzhuanhuan01"></i>上传身份证</button>
                    <input type="file">
                </p>
                <p class="btn-merge">
                    <button class="btn btn-primary"><i class="icon iconfont icon-zhaohuo2press"></i>新增包裹</button>
                    <button class="btn btn-black">导入包裹</button>
                    <button class="btn btn-black" onClick="showExportPackageWindow()">导出包裹</button>
                </p>
            </div>
			<div class="leightbox2 modal-dialog" id="exportPkgWinId" style="background-image: url('');width:550px;">
				<a href="javascript:;"
					class="lbAction modal-dialog-title-close close" rel="deactivate"
					style="background: transparent url(${resource_path}/img/close-x.png) no-repeat scroll;background-position: 25% 25%;"
					id="closeExportPkgWinId"></a>
				<div class="Whole" style="width: 100%;">
					<h1 class="blue">
						<span style="float: left;">请导出时间段（不选默认导出全部）：</span>
					</h1>
					<div class="Search1">
						从<input type="text" class="input" id="fromDate" name="fromDate"
							style="width: 200px"
							onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							value="${params.fromDate}"
							onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
							onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
							&nbsp;&nbsp;到 <input type="text" class="input" id="toDate"
							name="toDate" style="width: 200px"
							onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							value="${params.toDate}"
							onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
							onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
					</div>
					<div class="PushButton"
						style="width: 120px; padding-top: 10px; padding-left: 250px;">
						<a class="Block input14 white" href="javascript:exportPkgConfirm()">导出包裹</a>
					</div>
				</div>
			</div>
			<form id="updatePkg" name="updatePkg">								
            <div class="package-tab">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="javascript:showAllTransport()">全部包裹</a></li>
                    <li><a href="javascript:showTransportStatus(0)" >待入库</a></li>
                    <li><a href="javascript:showTransportStatus(1)" >已入库</a></li>
                    <li><a href="javascript:showTransportStatus(2)" >待发货</a></li>
                    <li><a href="javascript:showTransportStatus(-1)">待处理</a></li>
                    <li><a href="javascript:showTransportStatus(3)">转运中</a></li>
                    <li><a href="javascript:showTransportStatus(9)">已签收</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab01">
                        <div class="package-search pt30 pb20">
                            <p class="package-search-detail floatR">
                                <input type="text" placeholder="请输入运单号/关联号/收件人/手机号" id="SearchKey">
                                <a style="width: 64px;height: 30px;" class="btn btn-black" onClick="ShowData();"><i class="icon iconfont icon-sousuo-liebiao"></i>搜索</a>
                            </p>
                            <p class="package-search-select">
                                <label>筛选：</label>
                                <select>
                                    <option>请选择打印状态</option>
                                </select>
                                <select>
                                    <option>请选择支付状态</option>
                                </select>
                            </p>
                        </div>
						<input type="hidden" value="${frontUser.user_type}" id="userType" name="userType" />
						<input type="hidden" id="statusId" value="${status}" /> 
						<input type="hidden" id="payStatusId" value="${payStatus}" />
                        <div class="package-table">
                            <table class="table" cellspacing="0" cellpadding="0">
                                <thead>
                                <tr>
                                    <th>商品/运单信息</th>
                                    <th colspan="3">费用详细</th>
                                    <th width="118">金额</th>
                                    <th width="100">操作</th>
                                </tr>
                                </thead>
                                <tbody  class="merge-btn">
                                <tr>
                                    <td colspan="7">
                                        <div class="merge-btn-list">
                                            <p class="merge-btn-right floatR">
                                                <!-- 展开/收起状态-->
                                                <a href="#" class="show-btn open contr"><i class="glyphicon glyphicon-menu-down" temp="true"></i>展开/收起</a>
                                                <a href="#" class="slide-btn"><i class="glyphicon glyphicon-menu-left"></i></a>
                                                <a href="#" class="slide-btn"><i class="glyphicon glyphicon-menu-right"></i></a>
                                            </p>
                                            <label><input type="checkbox" class="" name="checkALL" id="checkALL2" onClick="checkALLItem(2);">全选</label>
                                            <span>已选择包裹：<span class="col_red" id="orderCount2">0</span>件 </span>
                                            <button class="btn btn-default">合箱</button>
                                            <button class="btn btn-default">批量删除</button>
                                            <button class="btn btn-default">批量打印</button>
                                            <button class="btn btn-default">运费/关税支付</button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
								<c:forEach var="pkg" items="${pkgList}">
                                <!--展开/隐藏列表开始 循环内容-->
                                <tbody class="table-merge <c:if test="${pkg.status==-1}">merge-red</c:if>">
                                <tr class="title-merge">
                                    <td colspan="7">
                                        <div class="title-merge-detail">
                                            <p class="title-merge-right floatR">
                                                <button onClick="showPrint(${pkg.package_id})">打印</button>
                                                <c:if test="${pkg.status ==0 && (pkg.attach_status ==0 || pkg.attach_status ==1)}">
                                                <button relateid="${pkg.package_id}" class="disassemble">分箱(分箱状态)</button>
                                                </c:if>
                                                <c:if test="${pkg.status==0 && (pkg.attach_status ==0 || pkg.attach_status ==1)}">
                                                <button onClick="Modify(${pkg.package_id})" id="edit_${pkg.package_id}">编辑</button>
                                                </c:if>
                                                <c:if test="${pkg.status==0 &&(pkg.attach_status ==0 || pkg.attach_status ==1)&&pkg.arrive_status==0}">
                                                <button onClick="delPkg(${pkg.package_id})">删除</button>
                                                </c:if>
                                                <c:if test="${pkg.status==-1}">
                                                <button onClick="delPkg(${pkg.package_id})">下单</button>
                                                </c:if>
                                                <a href="javascript:telesp(1)" class="open"><i class="glyphicon glyphicon-menu-down"></i></a>
                                            </p>
											<input type="checkbox" id="item${pkg.package_id}" name="chkOrder" express_package = "${pkg.express_package}" attach_id="${pkg.attach_id}" attach_status="${pkg.attach_status}" arrive_status="${pkg.arrive_status}" pkg_status="${pkg.status}" onClick="checkItem();" value="${pkg.package_id}" />
                                            <span>运单号：<label class="wbill">${pkg.logistics_code}</label></span>
                                            <span class="pr20">关联单号：<label class="rela" id="label_d${pkg.package_id}">${pkg.original_num}</label><input type="hidden" id="old_originalNum${pkg.package_id}" value="${pkg.original_num}" /></span>
                                            <span class="pr20">状态：<b>待入库(未付款)</b> 2010-05-02</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                                <!--展开内容请用块display: table-row-group;  开始-->
                                <tbody class="merge-detail"  style="display: table-row-group;position: relative" id="glt${pkg.package_id}">
                                <tr>
                                    <td rowspan="3" class="border-left-no">
                                        <ul id="ul${pkg.package_id}">
											<li class="fist">
												<ol>
													<li class="addgods">内件名</li>
													<li class="addgods">物品类别</li>
													<li class="addgods">申报价值($)</li>
													<li class="addgods">品牌</li>
													<li class="addgods">数量</li>
												</ol>
											</li>                                           											
                                        </ul>
                                    </td>
                                    <td class="text-center bg-gray" width="106">包裹重量</td>
                                    <td width="90" class="text-center">${pkg.weight}磅</td>
                                    <td rowspan="5"  class="border-bottom-no text-center bg-gray" width="24">装运总费用</td>
                                    <td rowspan="5"  class="border-bottom-no text-center">
                                        <p class="col_red">
                                        	<fmt:formatNumber value="${pkg.transport_cost+0.00001}" type="currency" pattern="#,###.##" />元
                                        </p>
                                        <small class="col_666">税费2元</small>
                                    </td>
                                    <td rowspan="5"  class="border-bottom-no handle-list border-right-blue">
                                    	<c:if test="${pkg.pay_status==1 and pkg.transport_cost>0.0001 and pkg.exception_package_flag !='Y'}">
                                        <button class="btn btn-orange" onClick="payment('${pkg.logistics_code}')">待支付</button>
                                        </c:if>
                                        <c:if test="${pkg.pay_status==2}">
                                        <button class="btn btn-orange">支付运费</button>
                                        </c:if>
                                        <c:if test="${pkg.pay_status ==3 and pkg.pay_tax>0.0001 and pkg.exception_package_flag !='Y'}">
                                        <button class="btn btn-orange" onClick="payTax('${pkg.logistics_code}')">待支付</button>
                                        </c:if>
                                        <c:if test="${pkg.pay_status==4}">
                                        <button class="btn btn-orange">已支付</button>
                                        </c:if>
                                        <c:if test="${pkg.pay_status==5}">
                                        <button class="btn btn-orange">已支付[预扣]</button>
                                        </c:if>
                                        <c:if test="${pkg.pay_status==6}">
                                        <button class="btn btn-orange">预扣失败</button>
                                        </c:if>                                        
                                        <a href="javascript:query_logistics_detail(${pkg.logistics_code});" class="col_blue">物流追踪</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center bg-gray">运费</td>
                                    <td class="text-center">
										<fmt:formatNumber value="${pkg.freight+0.00001}" type="currency" pattern="#,###.##" />元
									</td>
                                </tr>
                                <tr style="">
                                    <td class="text-center bg-gray">
										<div style="position:relative;">
											<div item="${pkg.status}" item1="${pkg.waitting_merge_flag}" item2="${pkg.exception_package_flag}" id="cliSev${pkg.package_id}" onMouseOver="cliService(${pkg.package_id},this)">增值服务</div>											
											<div id="divService_${pkg.package_id}" val="${pkg.package_id}" class="divService" style="display: none; position: absolute; top: -50px; left: 50px;">
												<div style="background-color: #fff; border: 1px solid #0097db; padding: 0px 12px 6px 12px; width: 120px;">
													<ul class="pd" style="line-height: 26px;" id="divService_ul_${pkg.package_id}">
														
													</ul>
												</div>
												<ss> <si></si></ss>
											</div>
										</div>
									</td>
                                    <td class="text-center" id="attachPrice${pkg.package_id}">${pkg.attach_service_price}元</td>
									<td>
										
									</td>
                                </tr>
                                <tr>
                                    <td rowspan="2" class="border-left-no border-bottom-no">
                                        <div class="address-box">
                                            <p class="address-detail"><span class="col_blue">仓库信息：</span><input type="hidden"value="${pkg.osaddr_id}" />${pkg.warehouseIndexof}
                                            <c:if test="${pkg.express_package == 0}">
                                            	<a href="#" class="col_blue" style="margin-left: 5px;">F渠道</a>
                                            	<c:if test="${pkg.status == 0 }">
													<a href="javascript:updateExpressPackage(${pkg.package_id});" class="col_blue" id="ExpressPackage" style="margin-left: 5px;">[修改]</a>
												</c:if>
                                            </c:if>
                                            <c:if test="${pkg.express_package == 1}">
												<a href="#" class="col_blue" style="margin-left: 5px;">A渠道</a>
												<c:if test="${pkg.status == 0 }">
													<a href="javascript:updateExpressPackage(${pkg.package_id});" class="col_blue" id="ExpressPackage" style="margin-left: 5px;">[修改]</a>
												</c:if>
											</c:if>
											<c:if test="${pkg.express_package == 2}">
												<a href="#" class="col_blue" style="margin-left: 5px;">B渠道</a>
												<c:if test="${pkg.status == 0 }">
													<a href="javascript:updateExpressPackage(${pkg.package_id});" class="col_blue" id="ExpressPackage" style="margin-left: 5px;">[修改]</a>
												</c:if>
											</c:if> 
                                            </p>
                                            <p class="address-detail"><span class="col_blue">收货地址：</span>${pkg.frontUserAddress.addressStr} ${pkg.frontUserAddress.receiver}  ${pkg.frontUserAddress.mobile}
                                            <c:if test="${pkg.status < 2}"> 
                                            	<a href="javascript:ModifyAddress(${pkg.package_id})" class="col_blue">[修改]</a> 
                                            </c:if>
                                            </p>
                                        </div>
										<div class="Collapse2_1" id="BtnModify${pkg.package_id}" style="display: none;">
											<input type="hidden" name="packageId" value="" /> 
											<a class="Block input06 pull-right size16 white" href="javascript:ModifyProduct(${pkg.package_id});">完成</a> 
											<a class="pull-right size16" href="javascript:Cancel(${pkg.package_id});" style="">取消</a>
											<a class="Block input06 pull-left size16 white" href="javascript:AddListProduct(${pkg.package_id});">+ 添加</a>
											<span style="float: left; padding-left: 10px; height: 50px; width: 200px; line-height: 1.5; font-family: initial;"><span>
											<span style="float: left;"><span class="cbox" onClick="checkBoxClick(this,${pkg.package_id},${pkg.status});"></span></span>
											<span style="padding: 5px">需等待合箱</span></span> 
											<br />提示：若勾选需等待合箱，<br />则包裹到库后仓库不会进行打包
											</span>
										</div>
                                    </td>
                                    <td class="text-center bg-gray">申报总价值</td>
                                    <td class="text-center">
                                    <fmt:formatNumber value="${pkg.total_worth+0.00001}" type="currency" pattern="#,###.##" />元
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-bottom-no text-center bg-gray">关税总费用</td>
                                    <td class="border-bottom-no text-center">
                                    <fmt:formatNumber value="${pkg.customs_cost+0.00001}" type="currency" pattern="#,###.##" />元
                                    </td>
                                </tr>
								
                                </tbody>
                                <!--展开内容块结束-->
                                <tbody>
                                    <tr>
                                        <td colspan="7" class="ht15"></td>
                                    </tr>
                                </tbody>
                                </c:forEach>
                            </table>
                        </div>
						<div class="page-list">
							<jsp:include page="../webfenye.jsp"></jsp:include>
						</div>
                        <!-- <div class="page-list">
                            <p class="page-btn floatR">
                                <a href="#"><i class="glyphicon glyphicon-menu-left"></i></a>
                                <a href="#" class="active">1</a>
                                <a href="#">2</a>
                                <a href="#">3</a>
                                <a href="#"><i class="glyphicon glyphicon-menu-right"></i> </a>
                            </p>
                            <p class="word floatL">
                                <span>总记录数：7条</span>
                                <span>每页显示：15条</span>
                                <span>总页数：7页</span>
                            </p>
                        </div> -->
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!--右侧内容结束-->

</div>
<!--中间内容结束-->

<!--底部开始-->
<div class="footer">
    粤ICP备14073551-2号，Copyright © 2016 www.su8exp.com Inc.All Rights Reserved.07693329337
</div>
<!--底部结束-->
<script>
$(function(){
	$(".divService").hover(function () {
		}, function () {
		var tempid = $(this).attr("val");
		$("#divService_" + tempid).hide();
	});
	$('.contr').click(function(){
		var flag=$(this).find('i').attr("temp");
		if(flag){
			$("tbody[id^=glt]").slideUp();
			$(this).removeClass('open');
			$(this).find('i').attr("temp","false");
		}
		if(!$("tbody[id^=glt]").is(':visible')){
			$("tbody[id^=glt]").slideDown();
			$(this).addClass('open');
			$(this).find('i').attr("temp","true");
		}		
	});
});
var exportPackageWindow;
function showExportPackageWindow() {
	showIndex = 0;
	var off = 0;
	off = ($(window).height() - 200) / 2;
	exportPackageWindow = $.layer({
		bgcolor: '#fff',
		type: 1,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [0, 0.3, '#666', true],          
		area: ['450px', '150px'],
		offset: [off, ''],
		shadeClose:true,
		title: false,
		zIndex:10002,
		page: { dom: '#exportPkgWinId' },
		closeBtn: [0, false],//显示关闭按钮
		close: function (index) {
			layer.close(index);
			$('#exportPkgWinId').hide();
			//ActivityID = 0;
		}

	});
}
$('#closeExportPkgWinId').on('click', function(){
	layer.close(exportPackageWindow);
});
//导出包裹
function exportPkgConfirm(){
	var fromDate=$("#fromDate").val();
	var toDate=$("#toDate").val();
	window.location.href = "${mainServer}/homepkg/exportFrontEndPackage?fromDate="
			+ fromDate
			+ "&toDate="
			+ toDate
			;
	layer.close(exportPackageWindow);
}
//上传身份证
function uploadId(){
	window.open("${mainServer}/id"); 
}
//列表产品新增
function AddListProduct(ID) {
	var key = new Date().getMilliseconds();
	while (2 > 1) {
		key = new Date().getMilliseconds();
	}


	var row = $("<li class=\"show\" id=\"item" + key + "\" name=\"item" + ID + "\" type=\"new\" dtl='"+key+"'></li>");

	var ol = $("<ol></ol>");

	var td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"name" + ID + "\" id=\"name" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_goods_name"+ID+"\" value=''>"));
	td.append($("<input type='text'  name=\"mname" + ID + "\" id=\"mname" + key + "\"  value=''  style='width:90px;' placeholder=\"请输入商品名称\" />"));
	ol.append(td);
		
	td = $("<li class=\"addgods\"></li>");
	td.append($("<input style=\"width:90px;\" id=\"mcatalog" + key + "\" onclick=\"ShowCataDialog(" + key + ");\" name=\"mcatalog" + ID + "\" style='width:80px;'  placeholder=\"请选择类别\" /><input type='hidden' id=\"catalogKey" + key + "\" name=\"pg_goods_type"+ID+"\">"));
	td.append($("<p name=\"catalog" + ID + "\" id=\"catalog" + key + "\" style=\"display: none;\"></p>"));
	ol.append(td);

	td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"price" + ID + "\" id=\"price" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_price"+ID+"\" value='300'>"));
	td.append($("<input type=\"text\"  name=\"mprice" + ID + "\" id=\"mprice" + key + "\"  value=\"\" style='width:90px;'  placeholder=\"请填写单价\">"));
	ol.append(td);
													
	td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"brand" + ID + "\" id=\"brand" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_brand"+ID+"\" value='300'>"));
	td.append($("<input type=\"text\"  name=\"mbrand" + ID + "\" id=\"mbrand" + key + "\"  value=\"\" style='width:90px;'  placeholder=\"请填写品牌\">"));
	ol.append(td);
	
	td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"num" + ID + "\" id=\"num" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_quantity"+ID+"\" value='2'>"));
	td.append($("<input type=\"text\" name=\"mnum" + ID + "\" id=\"mnum" + key + "\" class=\"miut\" value=\"1\">"));
	td.append($("<div class=\"PM\" name=\"modifyNum" + ID + "\"><a class=\"icon33\" href=\"javascript:ModifyListNum(1," + key + ")\"></a><a class=\"icon34\" href=\"javascript:ModifyListNum(-1," + key + ")\"></a></div>"));
	td.append($("<div class=\"delgods\"><a href=\"javascript:DelItem(this," + key + ")\" name=\"mDel" + ID + "\" >删除</a></div>"));
	ol.append(td);

	row.append(ol);
	$('#ul').append(row);
}
//删除产品信息
function DelItem(o,ID) {
	//a->div->li->ol->li
	var cont=$(o).parent().parent().parent().parent().parent().find('.show').length;
	if(cont==1){
		layer.msg("必须最少有一个商品", 2, 5);
		return;
	}
	var k = layer.confirm('确定删除该产品？', function () {
		$("#item" + ID).hide();
		$("#item" + ID).removeClass('show');
		$("#item" + ID).addClass('dele');
		layer.close(k);
	});
}
//列表取消修改
function Cancel(id) {
	var old_originalNum=$("#old_originalNum"+ id).val();
	$("#label_d"+ id).html(old_originalNum);
	//ActivityID = 0;
	$("p[name=name" + id + "]").show();
	$("input[name=mname" + id + "]").hide();
	$("p[name=catalog" + id + "]").show();
	$("input[name=mcatalog" + id + "]").hide();
	$("p[name=price" + id + "]").show();
	$("input[name=mprice" + id + "]").hide();
	$("p[name=brand" + id + "]").show();
	$("input[name=mbrand" + id + "]").hide();
	$("p[name=num" + id + "]").show();
	$("input[name=mnum" + id + "]").hide();
	$("div[name=modifyNum" + id + "]").hide();

	$("a[name=mDel" + id + "]").hide();
	$("a[name=mDel" + id + "]").parent().parent().css({"height":"0","line-height":"0"});

	$("input[name=mprice" + id + "]").parent().css("width","110px");
	
	$("p[name=suppnum" + id + "]").attr("style","");

	$(".editCatalog"+id).remove();
	//还原值
	$("p[name=name" + id + "]").each(function () {
		var id = this.id.replace("name", "");
		if($("#item"+id).attr("type") != "new")
		{
			$("#mname" + id).val($("#name" + id).html());
			$("#mcatalog" + id).val($("#catalog" + id).html());
			$("#mprice" + id).val($("#price" + id).html());
			$("#mnum" + id).val($("#num" + id).html());
		}
	});
	
	$("#ul" + id).find('.dele').each(function(){                   
		$(this).removeClass('dele');
		$(this).addClass('show');
		$(this).css("display","block");
		//$(this).attr("Del","");
		//$(this).attr("style","");
	});

	if ($("#ul" + id + " li").length == 0) {
		$("#ul" + id).html("<li></li>");
	}

	$("#BtnModify" + id).hide();
}
//提交修改包裹数据
  function ModifyProduct(ID) {
	var list=$("#ul" + ID).find('.show');
	var isValid=true;
 
	//获取用户的类型：同行2，直客1
	var userType=$("#userType").val();
	 
	$.each(list,function(i,li){
		var inputs=$(this).find("input[type='text']");		
		$.each(inputs,function(j,input){
			var name=input.name;
			var val=input.value;
			if(('mname'+ID)==name){
				if($.trim(val)=="" || val=="请输入商品名称"){
					layer.msg("请输入商品名称", 2, 5);
					isValid=false;
					return false;
				}
			}
			if(('mcatalog'+ID)==name){
				if(userType!="2"){
					if(val==""){
						layer.msg("请选择商品类别", 2, 5);
						isValid=false;
						return false;
					}
				}
				
			}
			if(('mprice'+ID)==name){
				var reg = /^\d+(?=\.{0,1}\d+$|$)/;
				if(reg.test(val)==false){
					layer.msg("商品价格只能为数字", 2, 5);
					isValid=false;
					return false;
				}
				if(val==""){
					layer.msg("请输入商品价格", 2, 5);
					isValid=false;
					return false;
				}
			}
			if(('mnum'+ID)==name){
				var reg=/^\d+$/;
				if(reg.test(val)==false){
					layer.msg("商品数量只能为数字", 2, 5);
					isValid=false;
					return false;
				}
				if(val==""){
					layer.msg("请输入商品数量", 2, 5);
					isValid=false;
					return false;
				}
			}
		});
	});
	
	var old_originalNum=$("#old_originalNum"+ID).val();
	var new_originalNum=$("#new_originalNum"+ID).val();
	var arrive_status=$("#item"+ID).attr("arrive_status");
	var reg = /^[0-9a-zA-Z]+$/;
	if(arrive_status==0)
	{

	if(old_originalNum!=null&&old_originalNum!='')
	{
		if(new_originalNum==null||new_originalNum==''){
			layer.msg("请输入包裹关联单号", 2, 5);
			return false;
		}else if(!reg.test(new_originalNum))
		{
			layer.msg("关联单号只能是数字或英文", 2, 5);
			return false;
		}
	}
	
	if(old_originalNum!=new_originalNum)
	{
		var url = "${mainServer}/homepkg/checkOriginalnum";
		var needReturn=false;
		$.ajax({
		   url:url,
		   type:'post',
		   dataType:'json',
		   async:false,
		   data:{'original_num':new_originalNum},
		   success:function(data){
				var msg=data['msg'];
				if(msg=="1"){
					layer.msg("关联单号已存在", 2, 5);
					needReturn=true;
				}else if(msg=="3"){
					layer.msg("该包裹已到仓库为未认领状态，请联系客服", 2, 5);
					needReturn=true;
				}
		   }
		});
		if(needReturn)
		{
			return false;
		}
	}
	
	}

	if(isValid==false){
		return;
	}
	var url = "${mainServer}/homepkg/updatepkg";
	var goodslist=[];
	
	$.each(list,function(i,li){
		var goods={};	
		var self=$(li);	
		var goods_nameVal=self.find("input[name=mname"+ID+"]").val();
		var goods_typeVal=self.find("input[name=pg_goods_type"+ID+"]").val();
		var priceVal=self.find("input[name=mprice"+ID+"]").val();
		var brandVal=self.find("input[name=mbrand"+ID+"]").val();
		var quantityVal=self.find("input[name=mnum"+ID+"]").val();
			
		goods['goods_name']=goods_nameVal;
		goods['goods_type_id']=goods_typeVal;
		goods['price']=priceVal;
		goods['brand']=brandVal;
		goods['quantity']=quantityVal;
		goodslist.push(goods);
	});
	
	if(goodslist.length==0)
	{
		layer.msg("内件为空，不能保存", 2, 5);
		return;
	}
	var v_waitting_merge_flag=$("#waitting_merge_flag"+ID).val();
	var jsStr=JSON.stringify(goodslist);

	
	$.ajax({
	   url:url,
	   data:{'package_id':ID,'goodsList':jsStr,'originalNum':new_originalNum,'waitting_merge_flag':v_waitting_merge_flag},
	   type:'post',
	   success:function(data){
		   if(data){
			layer.msg("修改包裹成功", 3, 1);
			 if(data=='OK_AND_ALERT'){
				 alert("请编辑包裹内件并选择收货地址");
			 }
			window.location.href = "${mainServer}/transport";
		   }				
	   }
	});	 
}
//全选
function checkALLItem(key) {
	var checkedOfAll = $("#checkALL" + key).attr("checked") == "checked";
	$("input[name='checkALL']").attr("checked",!checkedOfAll);	
	$("input[name='chkOrder']").each(function () {
		if (this.checked == checkedOfAll) {               
			this.click();
		}
	});
	var count=$("input[name='chkOrder']:checked").length;
	if(!checkedOfAll){
		$("#orderCount2").html(count);
	}else{
		$("#orderCount2").html(0);
	}		
}
//选择包裹选项
function checkItem() {
	var num=$("input[name='chkOrder']:checked").length;
	$("#orderCount2").html(num);
}
// 已入库状态渠道修改  
function updateExpressPackage(ID,status){
	$.layer({
		bgcolor: '#fff',
		type: 2,
		title: "渠道修改",
		shadeClose: false,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [1, 0.3, '#666', true],
		area: ['400px', '300px'],
		closeBtn: [0, true], //显示关闭按钮
		iframe: { src: '${mainServer}/expressPackage?package_id=' + ID},
		success: function () {
			$("#susu").attr("item",express_package);
		}
	});
};
//===================打印面单==========
function showPrint(package_id) {
	$.ajax({
		url:'${mainServer}/homepkg/canPrint',
		type:'post',
		data:{pkgid:package_id},
		dataType:"json",
		async:false,
		success:function(data){
			/*if(0 == data.canprint){
				var needP=$("#needPrintd").html();
				var np=Number(needP)-1; 
				$("#needPrintd").html(np);
				var doneP=$("#donePrintd").html();
				var dp=Number(doneP)+1; 
				$("#donePrintd").html(dp);
			}*/
		}			
	});
	
	$.layer({
		title :'打印面单',
		type: 2,
		shadeClose: true,
		shade: 0.8,
		offset: ['10px', '400px'],
		area: ['590px', '800px'],
		iframe: { src: '${mainServer}/homepage/print?package_id='+package_id }
	}); 
}
//分箱按钮
$(".disassemble").click(function(){
	var pkgid = $(this).attr("relateid").trim();
	$.ajax({
		url:'${mainServer}/homepkg/packageMergeSplitCheck',
		type:'post',
		data:{packageIds:pkgid,checkException:'Y'},
		dataType:"json",
		async:false,
		success:function(data){
			if("F" == data.result){
				if("Y" == data.needEdit){
					alert("包裹信息不完整，无法进行分箱合箱");
				}else
				{
					alert("您所选择包裹已进行分箱｜合箱服务，不能再次分箱!");
				}
			}else
			{
				$.layer({
					bgcolor: '#fff',
					type: 2,
					title: "分箱",
					shadeClose: false,
					fix: false,
					shade: [0.5, '#ccc', true],
					border: [1, 0.3, '#666', true],
					area: ['1000px', '500px'],
					closeBtn: [0, true], //显示关闭钮
					iframe: { src: '${mainServer}/homepkg/unboxHomepkg?pkgid='+pkgid},
					success: function () {
					},
					end: function () {
					}
					});
			}
		}			
	});
});
//列表修改
function Modify(id,type) {
	var arrive_status=$("#item"+id).attr("arrive_status");
	var isMargeSplit=false;
	$.ajax({
		url:'${mainServer}/homepkg/packageMergeSplitCheck',
		type:'post',
		data:{packageIds:id,checkException:'N'},
		dataType:"json",
		async:false,
		success:function(data){
			if("F" == data.result){
				isMargeSplit=true;
			}
		}			
	});
	if(arrive_status==0&&!isMargeSplit)
	{
		var old_originalNum=$("#old_originalNum"+ id).val();
		$("#label_d"+ id).html("<input type=\"text\" id=\"new_originalNum"+id+"\" value=\""+old_originalNum+"\"/>");					
	}
	
	$("p[name=name" + id + "]").hide();
	$("input[name=mname" + id + "]").show();
	$("p[name=catalog" + id + "]").hide();
	$("input[name=mcatalog" + id + "]").show();
	$("p[name=price" + id + "]").hide();
	$("input[name=mprice" + id + "]").show();
	$("p[name=brand" + id + "]").hide();
	$("input[name=mbrand" + id + "]").show();
	$("p[name=num" + id + "]").hide();
	$("input[name=mnum" + id + "]").show();
	$("div[name=modifyNum" + id + "]").show();
	
	$("p[name=suppnum" + id + "]").attr("style","padding-top:10px;"); 
	
   $("a[name=mDel" + id + "]").show();
   $("a[name=mDel" + id + "]").parent().parent().css({"height":"40px","line-height":"40px"});

	
	$("input[name=mprice" + id + "]").css("width","64px");
	$("input[name=mprice" + id + "]").parent().css("width","80px");

	$("#BtnModify" + id).show();

	$("input[name=mname" + id + "]").attr("placeholder","请输入商品名称");
	$("input[name=mcatalog" + id + "]").attr("placeholder","请选择类别");
	$("input[name=mprice" + id + "]").attr("placeholder","请填写单价");

	$("input[name=mname" + id + "]").unbind("click,blur").bind("click",function(){
		if($(this).val().trim() == "请输入商品名称"){
			$(this).val("");
		}
	}).bind("blur",function(){
		if($(this).val().trim() == ""){
			$(this).val("请输入商品名称");
		}
	});

	$("input[name=mcatalog" + id + "]").unbind("blur").bind("click",function(){
		if($(this).val().trim() == "请选择类别"){
			$(this).val("");
		}
	}).bind("blur",function(){
		if($(this).val().trim() == ""){
			$(this).val("请选择类别");
		}
	});

	$("input[name=mprice" + id + "]").unbind("click,blur").bind("click",function(){
		if($(this).val().trim() == "请填写单价"){
			$(this).val("");
		}
	}).bind("blur",function(){
		if($(this).val().trim() == ""){
			$(this).val("请填写单价");
		}
	});
}
//删除包裹
function delPkg(package_id){
	var pageIndex=$("#pageIndex").val();
	var status=$("#statusId").val();
	var paystatus=$("#payStatusId").val();
	var attachId=$("#item"+package_id).attr("attach_id");
	if(attachId == 4){
		var k = layer.confirm('分箱相关包裹将全部被删除，若想重新分箱请重新下单', function () {
		var url = "${mainServer}/homepkg/delHomepkg?package_id="+package_id+"&pageIndex="+pageIndex+"&status="+status+"&paystatus="+paystatus;
		window.location.href = url;
		layer.close(k);
		});
	}else if(attachId == 7){
		var k = layer.confirm('合箱相关包裹将全部被删除，若想重新合箱请重新下单', function () {
		var url = "${mainServer}/homepkg/delHomepkg?package_id="+package_id+"&pageIndex="+pageIndex+"&status="+status+"&paystatus="+paystatus;
		window.location.href = url;
		layer.close(k);
		});
	}else{
		var k = layer.confirm('确定删除该包裹？', function () {
		var url = "${mainServer}/homepkg/delHomepkg?package_id="+package_id+"&pageIndex="+pageIndex+"&status="+status+"&paystatus="+paystatus;
		window.location.href = url;
		layer.close(k);
		});
	}				
}
//展开收缩
function telesp(id){
	if (!$("#glt" + id).is(":visible")) {
        $("#glt" + id).show();                  
    }else{
		$("#glt" + id).hide();
	}
}
//物流追踪
function query_logistics_detail(logistics_code){
	$.layer({
		bgcolor: '#fff',
		type: 2,
		title: "物流追踪",
		shadeClose: false,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [1, 0.3, '#666', true],
		area: ['500px', '600px'],
		closeBtn: [0, true], //显示关闭按钮
		iframe: { src: '${mainServer}/query_logistics_detail?logistics_code='+logistics_code},
		success: function () {
		},
		end: function () {
		}
	});
}
//包裹状态
function showTransportStatus(status){
	window.location.href="${mainServer}/transport?status="+status;
}
//增值服务
function cliService(tempid, elem) {
	var needShow=false;
	$.ajax({
		url:'${mainServer}/homepkg/needShowAttachPhoto',
		type:'post',
		data:{package_id:tempid},
		dataType:"json",
		success:function(data){
			if(data.needShow=='Y')
			{
				needShow=true;
			}
		}
	});
	$(".divService").hide();				
	$("#divService_ul_" + tempid).html("");
	$.ajax({
		url:'${mainServer}/homepkg/cliService',
		type:'post',
		data:{package_id:tempid},
		dataType:"json",
		success:function(data){
			$("#divService_ul_" + tempid).html("");
			var list=data.list;
			//alert(list.length);
			if(list!=null && list.length>0){
				for(var i=0;i<list.length;i++){
					var item=list[i];			 
					var $itemLi=$('<li></li>');
					var $itemA=$('<a class="'+item.service_class+' icon59" href="javascript:SetService('+tempid+',\''+item.service_name+'\','+item.attach_id+')">'+item.service_name+'</a>');
					//拍照服务
					if(item.attach_id==18)
					{
						if("blue"==item.service_class&&needShow)
						{
							$itemA=$('<a class="'+item.service_class+' icon59" href="javascript:SetService('+tempid+',\''+item.service_name+'\','+item.attach_id+')">'+item.service_name+'</a><a target="_blank" href="${mainServer}/homepkg/showAttachPhoto?package_id='+tempid+'">[点击查看]</a>');											
						}
					}									
					
					//保价服务
					if(item.attach_id==32)
					{
						$itemA=$('<a class="'+item.service_class+' icon59" href="javascript:keepPriceDisplay('+tempid+')">'+item.service_name+'</a>');
					}
					$itemLi.append($itemA);
					$("#divService_ul_" + tempid).append($itemLi);	
				}
			}
		}			
	});
}
function SetService(package_id,service_name,attach_id){
	var pkg_status=$("#cliSev"+package_id).attr("item");
	//var freight_cost=$("#freightCost"+package_id).attr("item");
	//包裹已入库状态后就不能选择增值服务
	if(pkg_status>0){
		layer.msg("该包裹已无法添加增值服务", 2, 5);
		return;
	}
	
	var waitting_merge_flag=$("#cliSev"+package_id).attr("item1");
	if(waitting_merge_flag=='Y'){
		layer.msg("您的包裹需等待合箱，无法选择其他增值服务", 2, 5);
		return;
	}
	var exception_package_flag=$("#cliSev"+package_id).attr("item2");
	if(exception_package_flag=='Y'){
		layer.msg("您的包裹信息不完整，无法选择其他增值服务", 2, 5);
		return;
	}
	$.ajax({
		url:'${mainServer}/homepkg/setService',
		type:'post',
		data:{package_id:package_id,attach_id:attach_id,service_name:service_name,service_price:0},
		dataType:"json",
		async:false,
		success:function(data){
			var status=data['status'];
			if("0" == status){
				window.location.href="${mainServer}/login";
			}
			var attach_price=data['attach_price'];
			if("1" == status){
				alert("您已成功选择["+service_name+"]服务");
			}else if("2" == status){
				alert("您已取消["+service_name+"]服务");
			}else{
				alert("本包裹["+service_name+"]增值服务已经完成,不可取消");
			}
			if(attach_price!=null || attach_price!=""){
				//var transpCost=parseFloat(freight_cost)+parseFloat(attach_price);
				$("#attachPrice"+package_id).html(attach_price);
				//$("#transpCost"+package_id).html(transpCost);
			}
			$("#divService_ul_" + package_id).html("");
			$("#divService_" + package_id).hide();
			//$("#service" + package_id).html("");
		}			
	});
}
//保价
function keepPriceDisplay(package_id){
	var pkg_status=$("#cliSev"+package_id).attr("item");
	if(pkg_status>0){
		layer.msg("该包裹已无法添加增值服务", 2, 5);
		return;
	}
	var waitting_merge_flag=$("#cliSev"+package_id).attr("item1");
	if(waitting_merge_flag=='Y'){
		layer.msg("您的包裹需等待合箱，无法选择其他增值服务", 2, 5);
		return;
	}
	var exception_package_flag=$("#cliSev"+package_id).attr("item2");
	if(exception_package_flag=='Y'){
		layer.msg("您的包裹信息不完整，无法选择其他增值服务", 2, 5);
		return;
	}
	$.layer({
		bgcolor: '#fff',
		type: 2,
		title: "包裹保价",
		shadeClose: false,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [1, 0.3, '#666', true],
		area: ['400px', '330px'],
		closeBtn: [0, false], //显示关闭按钮
		iframe: { src: '${mainServer}/homepkg/keepPrice?pkgid='+package_id},
		success: function () {
		},
		end: function () {
		}
		});
}
//查询
function ShowData(){
	var searchVal=$("#SearchKey").val();
	if(searchVal=="输入运单号、关联单号、收件人、手机号、商品名称"){
		layer.msg("请输入搜索名称", 2, 5);
		return;
	}
	searchVal = encodeURI(encodeURI(searchVal));
	window.location.href="${mainServer}/transport?searchVal="+searchVal;
}
function payment(logistics_code) {
   if(!validateLogin()){
	   return ;
   }	
 var url = "${mainServer}/member/frontUserStatus";
 $.ajax({
     url:url,
     type:'post',
     async:false,
     dataType:'json',
     success:function(data){
         // 账户正常
         if(data.result =="1"){
             window.location.href = "${mainServer}/payment/paymentList?logisticsCodes="+logistics_code;
         }
         else{
            alert(data.message);
         }
     }
 });
}
//支付关税
function payTax(logistics_code) {
   if(!validateLogin()){
	   return ;
   }
     var url = "${mainServer}/member/frontUserStatus";
     $.ajax({
         url:url,
         type:'post',
         async:false,
         dataType:'json',
         success:function(data){
             // 账户正常
             if(data.result =="1"){
                 window.location.href = "${mainServer}/payment/taxList?logisticsCodes="+logistics_code;
             }
             else{
                alert(data.message);
             }
         }
     });
 }
//修改收货地址
function ModifyAddress(ID){
	$.ajax({
		url:'${mainServer}/homepkg/sessionCheck',
		type:'post',
		dataType:"json",
		async:false,
		success:function(data){
			var status=data['status'];
			if("0" == status){
				window.location.href="${mainServer}/login";
			}else{
				var off = ($(window).height() - 760) / 2;
				$.layer({
				bgcolor: '#fff',
				type: 2,
				title: false,
				shadeClose: true,
				fix: false,
				shade: [0.5, '#ccc', true],
				scrollbar: true,
				border: [1, 0.3, '#666', true],
				area: ['750px', '700px'],
				iframe: { src: '${mainServer}/useraddr/initupdateAddress?package_id='+ID},
				success: function () {
				},
				end: function () {
				}
				});	
			}
		}			
	});							
}
</script>

</body>
</html>