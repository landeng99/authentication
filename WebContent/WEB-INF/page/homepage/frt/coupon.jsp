<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>优惠券</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/common.js"></script>
    <script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script> 
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
    <link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css">
	<style type="text/css">
	    .couponli{width:33%; float:left; overflow:hidden;}
	</style>
</head>
<body class="bg-f1">
<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li ><a href="#"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li><a href="#"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="#"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="#"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="#"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="#"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li class="active"><a href="#"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <!--<li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>-->
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
    	<div class="column mt25 p20">
    		<div class="record">
	          <div class="hr mg18 lastline"></div>			  
			  <div class="size14">优惠券说明：</div> 
				<li class="border-top-none">
	                  <ol>
	                      <li class="span10 size14">1.只有单个包裹进行支付时才能使用优惠券，批量支付不支持使用优惠券</li>
	                      <li class="span10 size14">2.一个包裹只能使用一张优惠券</li>
						  <li class="span10 size14">3.到期后的优惠券将会无效且无法使用</li>
						  <li class="span10 size14">4.优惠券最终解释权归跨境物流所有</li>
					  </ol>
	              </li> 
			  <br><br><br><br><br><br>
	          <a name="Coupon"></a>
	          <p class="size14" style="margin-left:0;"><strong class="pd10">优惠券：</strong>共 <span class="totalCount">${countAll}</span> 张</p>
	               <div class="opt" >
	                   <ul class="function pull-left couponul" style="width:400px;">
	                       <li class="current couponli" id="one">
	                       <a style="cursor:pointer;">可使用优惠券</a>
	                       <span class="count" style="float: left; position: absolute;margin:6px 0 0 -21px;">${countAvailable}</span>
	                       </li>
	                       <li class="couponli" id="two"><a style="cursor:pointer;">已使用优惠券</a></li>
	                       <li class="couponli" id="three"><a style="cursor:pointer;">已过期优惠券</a></li>
	                   </ul>
	               </div>
	               
	               <div id="avliable">
	               <ul class="list01 radius3" id="recordlist">
	                  <c:forEach var="userCoupon"  items="${avliableCouponList}">
	                   	<div style="color:##bbbbbb; width:280px; height:180px; border:1px dashed #9CC; float:left; margin:5px 20px 20px 2px;"> 
						<div style="font-size:16px; background-color:#bbbbbb; color:#FFF; text-align:center; padding-top:5px; padding-bottom:5px">${userCoupon.coupon_name}</div>
						<div style="padding-left:5px;">
							优惠券代码：${userCoupon.coupon_code}<br /><br />
							优惠券金额：<b style="font-size:16px; color:#F00"><fmt:formatNumber value="${userCoupon.denomination}" type="currency" pattern="$#,###.00"/></b><br />
							数量：${userCoupon.avalidCount}<br /><br />
							有效期至：${userCoupon.valid_date_toStr}<br />
						</div>
	                    </div>
	                 </c:forEach>
	               </ul>
	               </div>
	               
	               <div id="used" style="display: none">
	                   <ul class="list01 radius3" id="recordlist">
	                  <c:forEach var="couponUsed"  items="${usedCouponList}">
	                  	<div style="color:##bbbbbb; width:280px; height:180px; border:1px dashed #9CC; float:left; margin:5px 20px 20px 2px;">     
						<div style="font-size:16px; background-color:#bbbbbb; color:#FFF; text-align:center; padding-top:5px; padding-bottom:5px">${couponUsed.coupon_name}</div>
						<div style="padding-left:5px;">
							优惠券代码：${couponUsed.coupon_code}<br /><br />
							优惠券金额：<b style="font-size:16px; color:#F00"><fmt:formatNumber value="${couponUsed.denomination}" type="currency" pattern="$#,###.00"/></b><br />
							使用数量：${couponUsed.quantity}<br />
							使用运单号：${couponUsed.order_code}<br />
							使用日期：${couponUsed.use_date}<br />
						</div>
						</div>
	                 </c:forEach>
	               </ul> 
	               </div>
	               
	               <div id="expired" style="display: none">
	               <ul class="list01 radius3" id="recordlist">
	                  <c:forEach var="userCoupon"  items="${expiredCouponList}">
	                	<div style="color:##bbbbbb; width:280px; height:180px; border:1px dashed #9CC; float:left; margin:5px 20px 20px 2px;">   
						<div style="font-size:16px; background-color:#bbbbbb; color:#FFF; text-align:center; padding-top:5px; padding-bottom:5px">${userCoupon.coupon_name}</div>
	 					<div style="padding-left:5px;">
							优惠券代码：${userCoupon.coupon_code}<br /><br />
							优惠券金额：<b style="font-size:16px; color:#F00"><fmt:formatNumber value="${userCoupon.denomination}" type="currency" pattern="$#,###.00"/></b><br /><br />
							优惠券数量：${userCoupon.quantity}<br />
							有效期至：${userCoupon.valid_date_toStr}<br />
						</div>
						</div>
	                 </c:forEach>
	               </ul>
	               </div>                              
	          </div>
    	</div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        // 优惠券
        $(".couponli").click(function () {
            
            $("ul.couponul>li").removeClass("current");
            $(this).addClass("current");
            var url = "${mainServer}/account/coupon";
            var id =(this).id;
            if(id=="one"){
                $("#avliable").show();
                $("#used").hide();
                $("#expired").hide();
            }else if(id=="two"){
                $("#avliable").hide();
                $("#used").show();
                $("#expired").hide();
            }else{
                $("#avliable").hide();
                $("#used").hide();
                $("#expired").show();
            }           
        });
        

        
        //格式化金额
    function fmoney(s, n) {
            n = n > 0 && n <= 20 ? n : 2;
            s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
            var l = s.split(".")[0].split("").reverse();
            var r = s.split(".")[1];
            var t = "";
            for ( var i = 0; i < l.length; i++) {
                t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
            }
            return t.split("").reverse().join("") + "." + r;
        }
    });
</script>
<script type="text/javascript" src="${resource_path}/js/ZeroClipboard.js"></script>
<script type="text/javascript">
$(function(){
    $(".left-nav").hover(function () {
        $(this).css("background","url(${resource_path}/img/is5.png) no-repeat");
    }, function () {
        $(this).css("background","url(${resource_path}/img/is6.png) no-repeat");
        });
    
    $("#charge").click(function () {
        var url = "${mainServer}/member/frontUserStatus";
        $.ajax({
            url:url,
            type:'post',
            async:false,
            dataType:'json',
            success:function(data){
                // 账户正常
                if(data.result =="1"){
                    var rechargeOffHeight = ($(window).height() - 570) / 2;
                    $.layer({
                        title :'充值',
                        type: 2,
                        fix: false,
                        shadeClose: true,
                        shade: [0.5, '#ccc', true],
                        border: [1, 0.3, '#666', true],
                        offset: [rechargeOffHeight + 'px', ''],
                        area: ['950px', '570px'],
                        iframe: { src: '${mainServer}/member/rechargeInit'}
                    });
                }
                else{
                   alert(data.message);
                }
            }
        });
    });
});
//包裹状态
function showTransportStatus(status){
	
	window.location.href="${mainServer}/transport?status="+status;
}
//所有状态的包裹
function showAllTransport(){
	window.location.href="${mainServer}/transport";
}
//包裹支付状态
function showTransportPayStatus(payStatus){

	window.location.href="${mainServer}/transport?payStatus="+payStatus;
}
</script>
</body>
</html>