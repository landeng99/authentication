<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>资金记录</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/common.js"></script>
   	<script src="${backJsFile}/My97DatePicker/WdatePicker.js"></script>
   	<script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script>  
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
    <link href="${resource_path}/css/syaccount.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/common.css">
<style type="text/css">
  .recordli{width:24%;
      float:left;
      overflow:hidden;
    }

  .bt span {
      display:-moz-inline-box;
      display:inline-block;
      width:120px; 
      
   }
   
   .main .right .record .list-two {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-bottom-color: #dcdcdc;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color: #dcdcdc;
    border-left-style: solid;
    border-left-width: 1px;
    border-right-color: #dcdcdc;
    border-right-style: solid;
    border-right-width: 1px;
    border-top-color: #dcdcdc;
    border-top-style: solid;
    border-top-width: 1px;
    margin-bottom: 18px;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 817px;
}
.main .right .record .list-two li {
    float: left;
}
.main .right .record .list-two li ol {
    float: left;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
    border-top-color: #dcdcdc;
    border-top-style: solid;
    border-top-width: 1px;
}

.main .right .record .list01 li ol {
    float: left;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
    border-top-color: #dcdcdc;
    border-top-style: solid;
    border-top-width: 1px;
}
.main .right .record .list-two li ol li p {
    line-height: 22px;
}
.main .right .record .list-two li ol li.m {
    width: 180px;
}
.main .right .record .list-two li ol li.n {
    width: 120px;
}
.main .right .record .list-two li ol li.o {
    width: 300px;
}
.main .right .record .list-two li ol li.q {
    width: 110px;
}
.main .right .record .list-two li ol li.r {
    padding-left: 0;
    text-align: center;
    width: 68px;
}
.main .right .record .list-two li ol li.r a {
    padding-bottom: 0;
    padding-left: 2px;
    padding-right: 2px;
    padding-top: 0;
}
.main .right .record .list-two li.th {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #f3f3f3;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color: -moz-use-text-color;
    border-left-style: none;
    border-left-width: medium;
    border-right-color: -moz-use-text-color;
    border-right-style: none;
    border-right-width: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;
}
.main .right .record .list-two li.th ol li {
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/heading_li.png");
    background-origin: padding-box;
    background-position: left center;
    background-repeat: no-repeat;
    background-size: auto auto;
    line-height: 48px;
}
.main .right .record .list-two li.th ol li.no {
    background-image: none;
}
.main .right .record .list-two li.white {
    background-color: #fff;
}
.border-left{
    border-left-color: #dcdcdc;
    border-left-style: solid;
    border-left-width: 1px;
}

</style>
</head>
<body class="bg-f1">
<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="#"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li><a href="#"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="#"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="#"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li class="active"><a href="#"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="#"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="#"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <!--<li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>-->
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
    	<div class="column mt25 p20">
    		<input type="hidden" id="flag" value="${flag}">
	    		<div class="record">
	               <div class="bt">
	                    <p class="size14"><strong>我的消费记录</strong></p>
	                    <p>近一年实际消费金额：<span class="orange size16" id="TotalSpendi"><fmt:formatNumber value="${sumAmount}" type="currency" pattern="$#,###.##"/></span></p>
	                    <p>账户余额：
	                      <span class="orange size16">
	                        <fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#,###.##"/>
	                      </span>冻结余额：
	                      <span class="orange size16">
	                       <fmt:formatNumber value="${frontUser.frozen_balance}" type="currency" pattern="$#,###.##"/>
	                      </span>可用余额：
	                    <span class="orange size16">
	                       <fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#,###.##"/>
	                    </span>
	                   </p>
	              
	               </div>
	               <div class="hr mg18"></div>
	               <div class="opt">
	                   <ul class="function pull-left recordul">
	                       <li class="current recordli border-left" id="one"><a style="cursor:pointer;">充值</a></li>
	                       <li class="recordli" id="two"><a style="cursor:pointer;">消费</a></li>
	                       <li class="recordli" id="three"><a style="cursor:pointer;">提现</a></li>
	                       <li class="recordli" id="four"><a style="cursor:pointer;">索赔</a></li>
	                      <!--  <li class="recordli" id="export_id"></li> -->
	                   </ul>
	                   <div class="pull-right">
								<a class="Block input50 size16 white pull-left"
									style="margin-right: 20px;"
									href="javascript:showExportPackageWindow()"> 导出记录</a>							
							</div>
	               </div>
	               
	               
	               <ul class="list01 radius3" id="recordOne">
	                   <li class="th">
	                       <ol>
	                           <li style="text-align:center;width:150px;padding:0;">时间</li>
	                           <li style="text-align:center;width:110px;padding:0;">金额</li>
	                           <li style="text-align:center;width:557px;padding:0;">备注</li>
	                       </ol>
	                   </li>
	                  <c:forEach var="frontAccountLog"  items="${frontAccountLogListRecharge}">
	                   <li style="background-color:White;">
	                       <ol>
	                           <li style="text-align:center;width:150px;padding:0; height:48px;">${frontAccountLog.opr_time_string}</li>
	                           <li style="width:110px;padding:0;">
	                               <div style="float:right;margin-right:10px;">
	                                 <fmt:formatNumber value="${frontAccountLog.amount}" type="currency" pattern="$#,###.##"/>
	                               </div>
	                           </li>
	                           <li style="width:557px;padding:0;"><div style="float:left;margin-left:10px;">${frontAccountLog.description}</div></li>
	                       </ol>
	                   </li>
	                 </c:forEach>
	               </ul>
	               
	               
	               <ul class="list-two radius3" id="recordTwo" style="display: none;" >
	                   <li class="th">
	                       <ol>
	                           <li style="text-align:center;width:150px;padding:0;">时间</li>
	                           <li style="text-align:center;width:150px;padding:0;">运单号</li>
	                           <li style="text-align:center;width:110px;padding:0;">金额</li>
	                           <li style="text-align:center;width:110px;padding:0;">税金</li>
	                           <li style="text-align:center;width:297px;padding:0;">备注</li>
	                       </ol>
	                   </li>
	                  <c:forEach var="frontAccountLog"  items="${frontAccountLogListConsume}">
	                   <li style="background-color:White;">
	                       <ol>
	                           <li style="text-align:center;width:150px;padding:0; height:48px;">${frontAccountLog.opr_time_string}
	                           </li>
	                           <li style="width:150px;padding:0;">
	                               ${frontAccountLog.logistics_code}
	                           </li>
	                           <li style="width:110px;padding:0;">
	                               <div style="float:right;margin-right:10px;">
	                                 <fmt:formatNumber value="${frontAccountLog.amount}" type="currency" pattern="$#,###.##"/>
	                               </div>
	                           </li>
	                           <li style="width:110px;padding:0;">
	                               <div style="float:right;margin-right:10px;">
	                                 <fmt:formatNumber value="${frontAccountLog.customs_cost}" type="currency" pattern="$#,###.##"/>
	                               </div>
	                           </li>
	                           <li style="width:407px;padding:0;"><div style="float:left;margin-left:10px;">${frontAccountLog.description}</div></li>
	                       </ol>
	                   </li>
	                 </c:forEach>
	               </ul>
	               
	               
	               <ul class="list01 radius3" id="recordThree" style="display: none;" >
	                   <li class="th">
	                       <ol>
	                           <li style="text-align:center;width:150px;padding:0;">时间</li>
	                           <li style="text-align:center;width:110px;padding:0;">金额</li>
	                           <li style="text-align:center;width:110px;padding:0;">状态</li>
	                           <li style="text-align:center;width:110px;padding:0;">操作</li>
	                           <li style="text-align:center;width:337px;padding:0;">备注</li>
	                       </ol>
	                   </li>
	                  <c:forEach var="frontAccountLog"  items="${frontAccountLogListWithdraw}">
	                   <li style="background-color:White;">
	                       <ol>
	                           <li style="text-align:center;width:150px;padding:0; height:48px;">${frontAccountLog.opr_time_string}</li>
	                           <li style="width:110px;padding:0;">
	                               <div style="float:right;margin-right:10px;">
	                                 <fmt:formatNumber value="${frontAccountLog.amount}" type="currency" pattern="$#,###.##"/>
	                               </div>
	                           </li>
	                           <li style="text-align:center;width:110px;padding:0;">
	                               <c:if test="${frontAccountLog.status==1}">申请中</c:if>
	                               <c:if test="${frontAccountLog.status==2}">完成</c:if>
	                               <c:if test="${frontAccountLog.status==3}">拒绝</c:if>
	                               <c:if test="${frontAccountLog.status==4}">已取消</c:if>
	                           </li>
	                           <li style="text-align:center;width:110px;padding:0;">
	                              <c:if test="${frontAccountLog.status==1}">
	                                  <a class="blue" href="javascript:void(0);" onclick="cancel('${frontAccountLog.log_id}')" style="cursor:pointer;">取消</a>
	                              </c:if>
	                           </li>
	                           <li style="width:337px;padding:0;"><div style="float:left;margin-left:10px;">${frontAccountLog.description}</div></li>
	                       </ol>
	                   </li>
	                 </c:forEach>
	               </ul>
	               
	               
	               <ul class="list01 radius3" id="recordFour" style="display: none;" >
	                   <li class="th">
	                       <ol>
	                           <li style="text-align:center;width:150px;padding:0;">时间</li>
	                           <li style="text-align:center;width:150px;padding:0;">运单号</li>
	                           <li style="text-align:center;width:110px;padding:0;">金额</li>
	                           <li style="text-align:center;width:407px;padding:0;">备注</li>
	                       </ol>
	                   </li>
	                  <c:forEach var="frontAccountLog"  items="${frontAccountLogListCompensation}">
	                   <li style="background-color:White;">
	                       <ol>
	                           <li style="text-align:center;width:150px;padding:0; height:48px;">${frontAccountLog.opr_time_string}</li>
	                           <li style="text-align:center;width:150px;padding:0;">${frontAccountLog.logistics_code}</li>
	                           <li style="width:110px;padding:0;">
	                               <div style="float:right;margin-right:10px;">
	                                 <fmt:formatNumber value="${frontAccountLog.amount}" type="currency" pattern="$#,###.##"/>
	                               </div>
	                           </li>
	                           <li style="width:407px;padding:0;"><div style="float:left;margin-left:10px;">${frontAccountLog.description}</div></li>
	                       </ol>
	                   </li>
	                 </c:forEach>
	               </ul>
	               <div class="bt">
	                    <p class="size14"><strong>特别说明</strong></p>
	                    <p>交易记录中不包括使用优惠券的金额。</p>
	               </div>
	          </div>
    	</div>
    </div>
    <div class="leightbox2 modal-dialog" id="exportPkgWinId" style="background-image: url('');">
				<a href="javascript:;"
					class="lbAction modal-dialog-title-close close" rel="deactivate"
					style="background: transparent url(${resource_path}/img/close-x.png) no-repeat scroll;background-position: 25% 25%;"
					id="closeExportPkgWinId"></a>
				<div class="Whole" style="width: 100%;">
					<h1 class="blue">
						<span style="float: left;">请导出时间段（不选默认导出全部）：</span>
					</h1>
					<div class="Search1">
					从<input type="text" class="input" id="fromDate" name="fromDate"
						style="width: 200px"
						onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						value="${params.fromDate}"
						onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
						onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
					&nbsp;&nbsp;到
					<input type="text" class="input" id="toDate" name="toDate"
						style="width: 200px"
						onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						value="${params.toDate}"
						onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
						onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
					</div>
					<div class="PushButton" style="width: 120px; padding-top: 10px; padding-left: 150px;">
						<a class="Block input14 white"
							href="javascript:exportPkgConfirm()">导出消费记录</a>
					</div>
				</div>
		</div>
 </div>
 <script type="text/javascript">

    $(function () {
        //  最近三月，三月以前
        $(".recordliXXXXX").click(function () {
            
            $("ul.recordul>li").removeClass("current");
            $(this).addClass("current");
            var url = "${mainServer}/member/allConsumption";
           $.ajax({
                url:url,
                type:'post',
                async:false,
                dataType:'json',
                data:{"id":(this).id,
                    "status":status},
                success:function(data){
                   $("#recordlist>li").not(":first").remove();
                   $.each(data, function(i, ele) {

                   var li = "<li style='background-color:White;'><ol>"
                          + "<li style='text-align:center;width:150px;padding-left:0;'>" + ele.opr_time_string + "</li>"
                          + "<li style='text-align:center;width:150px;padding-left:0;'>" + ele.logistics_code + "</li>"
                          + "<li style='width:110px;padding-left:0;'><div style='float:right;margin-right:10px;'>$" + fmoney(ele.amount,2) + "</div></li>"
                          + "<li style='width:407px;padding-left:0;'><div style='float:left;margin-left:10px;'>" + ele.description + "</div></li>"
                          + "</ol></li>";
                          
                   $("#recordlist").append(li);
                   });
                }
            });
  
        });
        // 提现取消刷新页面
        $(document).ready(function(){

              if($("#flag").val() ==3){
              $("#three").click(); 
              }

            }); 
        //  充值消费提现索赔
        $(".recordli").click(function () {
            
            $("ul.recordul>li").removeClass("current");
            $(this).addClass("current");
            var clickId =(this).id;
            
            if(clickId =="one"){
                    $("#recordOne").show();
                    $("#recordTwo").hide();
                    $("#recordThree").hide();
                    $("#recordFour").hide();
                
            }else if(clickId =="two"){
                   $("#recordOne").hide();
                   $("#recordTwo").show();
                   $("#recordThree").hide();
                   $("#recordFour").hide();
                
            }else if(clickId =="three"){
                   $("#recordOne").hide();
                   $("#recordTwo").hide();
                   $("#recordThree").show();
                   $("#recordFour").hide();
                
            }else if(clickId =="four"){
                   $("#recordOne").hide();
                   $("#recordTwo").hide();
                   $("#recordThree").hide();
                   $("#recordFour").show();
                
            }
   
        });

        //格式化金额
    function fmoney(s, n) {
            n = n > 0 && n <= 20 ? n : 2;
            s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
            var l = s.split(".")[0].split("").reverse();
            var r = s.split(".")[1];
            var t = "";
            for ( var i = 0; i < l.length; i++) {
                t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
            }

            var str = t.split("").reverse().join("") + "." + r;

            return  str;
        }
    });
</script>
<script type="text/javascript" src="${resource_path}/js/ZeroClipboard.js"></script>
<script type="text/javascript">
$(function(){
    $(".left-nav").hover(function () {
        $(this).css("background","url(${resource_path}/img/is5.png) no-repeat");
    }, function () {
        $(this).css("background","url(${resource_path}/img/is6.png) no-repeat");
        });
    
    $("#charge").click(function () {
        var url = "${mainServer}/member/frontUserStatus";
        $.ajax({
            url:url,
            type:'post',
            async:false,
            dataType:'json',
            success:function(data){
                // 账户正常
                if(data.result =="1"){
                    var rechargeOffHeight = ($(window).height() - 570) / 2;
                    $.layer({
                        title :'充值',
                        type: 2,
                        fix: false,
                        shadeClose: true,
                        shade: [0.5, '#ccc', true],
                        border: [1, 0.3, '#666', true],
                        offset: [rechargeOffHeight + 'px', ''],
                        area: ['950px', '570px'],
                        iframe: { src: '${mainServer}/member/rechargeInit'}
                    });
                }
                else{
                   alert(data.message);
                }
            }
        });
    });
});

//取消提现
function cancel(log_id){
     var url = "${mainServer}/member/withdrawalCancel";
     $.ajax({
          url:url,
          type:'post',
          async:false,
          dataType:'json',
          data:{"log_id":log_id},
          success:function(data){
              alert(data.message);
              if(data.result){
                 window.location.href="${mainServer}/member/record?flag="+3; 
              }

          }
      });
}

//包裹状态
function showTransportStatus(status){
    window.location.href="${mainServer}/transport?status="+status;
}
//所有状态的包裹
function showAllTransport(){
	window.location.href="${mainServer}/transport";
}
//包裹支付状态
function showTransportPayStatus(payStatus){
    window.location.href="${mainServer}/transport?payStatus="+payStatus;
}

	var exportPackageWindow;
	function showExportPackageWindow() {
		showIndex = 0;
		var off = 0;
		off = ($(window).height() - 200) / 2;
		exportPackageWindow = $.layer({
			bgcolor: '#fff',
			type: 1,
			fix: false,
			shade: [0.5, '#ccc', true],
			border: [0, 0.3, '#666', true],          
			area: ['450px', '150px'],
			offset: [off, ''],
			shadeClose:true,
			title: false,
			zIndex:10002,
			page: { dom: '#exportPkgWinId' },
			closeBtn: [0, false],//显示关闭按钮
			close: function (index) {
				layer.close(index);
				$('#exportPkgWinId').hide();
				//ActivityID = 0;
			}

		});
	}
	$('#closeExportPkgWinId').on('click', function(){
		layer.close(exportPackageWindow);
	});
	function exportPkgConfirm()
	{
		var timeStart=$("#fromDate").val();
		var timeEnd=$("#toDate").val();
		window.location.href="${mainServer}/member/frontEndexportList?timeStart="+timeStart+"&timeEnd="+timeEnd;
			layer.close(exportPackageWindow);
	}
</script>
</body>
</html>