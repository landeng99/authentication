<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0070)http://www.birdex.cn/Transport/Disassembling.aspx?ID=371464&Code=22222 -->
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>速8快递包裹分箱</title>
    <link rel="stylesheet" href="${resource_path}/v2.0/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/v2.0/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/v2.0/css/global.css">
	<link rel="stylesheet" href="${resource_path}/css/indexs.css">
	<link href="${resource_path}/css/syaccount.css" rel="stylesheet" type="text/css">
    <link href="${resource_path}/css/syfault.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/common.css"> 
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css"> 
	<link rel="stylesheet" href="${resource_path}/css/indexs.css">
	<script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
	<script src="${resource_path}/js/layer.min.js"></script>
	<script src="${resource_path}/js/common.js"></script>
	<script src="${backJsFile}/My97DatePicker/WdatePicker.js"></script>
	<script  src="${resource_path}/js/jquery-ui-1.10.4.custom.min.js"></script>

	<script src="${resource_path}/js/jquery.nicescroll.min.js"></script>   
	<script src="${resource_path}/js/json2.js"></script>
	<script src="${resource_path}/js/application.js"></script>
	<script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script>
</head>
<body class="bg-f1">
<jsp:include page="header-3.jsp"></jsp:include>

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="${mainServer}/personalCenter"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li class="active"><a href="${mainServer}/transport"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="${mainServer}/warehouse"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="${mainServer}/useraddr/destination"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="${mainServer}/member/record"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="${mainServer}/account/accountInit"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="${mainServer}/account/couponInit"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
         </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
        <h3 class="public-title mt20"><i class="title-icon03"></i>包裹分箱<a href="#" class="back">返回</a> </h3>
        <div class="column p20 pb60">
            <div class="split-box">
                <div class="split-left floatL">
                    <div class="split-title"><span>订单：TEST20170501</span><h3>需分箱包裹</h3></div>
                    <div class="table-list change-gray">
                    	<input type="hidden" id="package_id" value="${package_id}"/>
                        <div class="table-box">
                            <table class="table text-center">
                                <thead>
                                <tr>
                                    <th>商品名称</th>
                                    <th>品类</th>
                                    <th>商品数量</th>
                                    <th>分箱操作</th>
                                </tr>
                                </thead>
                                <tbody id="source">
								<c:forEach var="pg"  items="${goodsList}">
									<tr id="tr${pg.goods_id}" class="ui-draggable">
										<td>${pg.goods_name}
											<input type="hidden" value="${pg.goods_id}" name="goods_id"/>
											<input type="hidden" value="${pg.goods_name}" name="goods_name"/>
											<input type="hidden" value="${pg.goods_type_id}" name="goods_type_id"/>
											<input type="hidden" value="${pg.name_specs}" name="name_specs"/>
											<input type="hidden" value="${pg.price}" name="price"/>
											<input type="hidden" value="${pg.brand}" name="brand"/>
										
										</td>
										<td>${pg.name_specs}</td>
										<td><p>${pg.quantity}</p><span id="numtr${pg.goods_id}" style="display:none;">${pg.quantity}</span></td>
										<td>拖拽至右侧</td>
									</tr>
     							</c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <p class="word">拖拽至右侧</p>
                    </div>
                </div>
                <div class="split-right floatR" id="items">
      
					<p class="word">将左侧包裹商品拖拽到此处</p>
                    <button class="btn btn-primary" onclick="NewTransport()">+新增</button>
                </div>
            </div>
            <div class="public-btn text-right">
                <button class="btn btn-orange mrNo" onclick="Save()">保存分箱</button>
            </div>
        </div>
    </div>
    <!--右侧内容结束-->

</div>
<!--中间内容结束-->

<!--底部开始-->
<div class="footer">
    粤ICP备17086482 Copyright 2017 su8exp.com Inc.All Rights Reserved. 0755-23204850
</div>
<!--底部结束-->


</body>

   <script>

   		var addressList=[];
   		var expressList=[];
        $(function () {
            //----------------------首次加载页面时加载首页数据----------------------------
            loadAddress();
            loadExpress();
            BindItems();
            
        });
        
        //加载地址
        function loadAddress(){
        	 
        	$.ajax({
        		url:'${mainServer}/useraddr/getAddressByUserId?userId=${sessionScope.frontUser.user_id}',
        		async:false,
        		dataType:'json',
        		type:'get',
        		success:function(data){
        			addressList=data;
        		}
        	});
        	
        }
        
        //加载地址
        function loadExpress(){
        	 
        	$.ajax({
        		url:'${mainServer}/getExpressAll',
        		async:false,
        		dataType:'json',
        		type:'get',
        		success:function(data){
        			expressList=data;
        			 
        		}
        	});
        	
        }
        
        //设定保险
        function BindItems() {    
                $("tr[id^=tr]").draggable({
                        helper:"clone",
                        start: function(event,ui) {
//                            $(this).attr("style", $(this).attr("style") + "border:1px solid orange;");

                           $(this).attr("style", "background: none repeat scroll 0 0 #d9ffff;");
                        },
                        stop: function (event, ui) {

                            $(this).attr("style", $(this).attr("style").replace("background: none repeat scroll 0 0 #d9ffff;", ""));
                        },
                        revert: "invalid", cursor: "move", cursorAt: { top: 56, left: 56 } 
                    });
                   NewTransport();
        }

		
		 //修改产品数量
        function ModifyNum(num, id) {
            if ($("#count" + id).val() == "" || isNaN($("#count" + id).val())) {
                $("#count" + id).val(1);
            }

            if ($("#count" + id).val() == "1" && num < 0) {
                return;
            }

            var key = id.substring(id.indexOf("tr"), id.length);
			 
            //校验原始包裹物品数量，如果数量为0，则不可以继续分箱
            if ($("#" + key).find("td").eq(2).text() == "0" && num > 0) {
            	layer.alert($("#" + key).find("td").eq(0).text() + "已分完，不能再分配到该包裹！");
                return;
            }

            var count=0;
            
            $("input[id$=" + key + "]").each(function () {
                
               count =  count + parseInt($(this).val(),0);
            });
	 
           if (parseInt($("#num" + key).html()) < parseInt(count + num))
            {
                return;
            }
            
           //剩余数量
            var limitQty=parseInt($("#num" + key).html() - parseInt(count + num));
			 
            $("#" + key).find("td").eq(2).find("p").text(limitQty);
		 
            $("#count" + id).val(parseInt($("#count" + id).val()) + num);

            if (parseInt($("#" + key).find("td").eq(2).find("p").text()) > parseInt($("#" + key).find("td").eq(3).find("p").text())) {
                $("#" + key).find("td").eq(2).find("p").attr("style", "color:red");
            }

            if (parseInt($("#" + key).find("td").eq(2).find("p").text()) <= parseInt($("#" + key).find("td").eq(3).find("p").text())) {
                $("#" + key).find("td").eq(2).find("p").attr("style", "");
            }



            if ($("#" + key).find("td").eq(2).find("p").text() == "0") {
                $("#" + key).draggable("disable");
            }
            else {
                //$("#" + key).removeAttr("style");=false;
                $("#" + key).draggable("enable");
            }
            
        }
		
        
        function delProduct(key) {

            var k = layer.confirm('确定删除该产品？', function () {
                $("#" + key).remove();
                var $ul=$("#" + key.substring(0,key.indexOf("tr"))).find("ul").eq(1);
                var text=$.trim($ul.html());
                if (text == "") {
                	$ul.html("<li style=\"height:40px;\"></li>");
                }
                layer.close(k);
                ModifyNum(0, key);
            });
        }
		
		

        function dropItems(id)
        {
            $("table[id^=New]").droppable({
                drop: function (event, ui) {
					
					$this=$(this);
				
                    var key = $this.attr("id") + ui.draggable.attr("id");
					
                    var oldkey = key.substring(key.indexOf("tr"), key.length);
					
                    if ($("#" + oldkey).find("td").eq(2).text() == "0") {
                        alert($("#" + oldkey).find("td").eq(0).text() + "已分完，不能再分配到该包裹！");
                    }
                    else {
						
						//当前已存在其他商品则不能新增当前商品
						if($this.find('tbody').find('tr').eq(0).find('td').eq(0).text()!=''
							&&$this.find("#" + key).length == 0){	
							layer.alert('当前包裹已有商品');
							return ;
						}
						
						//新包裹列表无该商品则拼装数据，反之则累加数据
                        if ($this.find('tbody').find("#" + key).length == 0) {
							
							$this.find('tbody').find("tr").eq(0).remove();
							var str=addRow(key);
							var $row=$(str);
							var ss = ui.draggable;					
							setData($row,ss);
                         	$this.find('.address').before($row);
							
                        }
                        
						ModifyNum(1, key);

                        $(this).attr("style", $(this).attr("style").replace("border:1px solid red;", "border:1px solid #ececec;"));
                        $(this).attr("style", $(this).attr("style").replace("border: 1px solid red;", "border:1px solid #ececec;"));
                      
                    }
                },
                over: function (event, ui) {
                    $(this).attr("style", $(this).attr("style") + "border:1px solid red;");

                    $("#msg" + $(this).attr("id").replace("New", "")).html("（已到拖放区域请释放鼠标）");
                },
                out: function (event, ui) {
                    $(this).attr("style", $(this).attr("style").replace("border:1px solid red;", "border:1px solid #ececec;"));
                    $(this).attr("style", $(this).attr("style").replace("border: 1px solid red;", "border:1px solid #ececec;"));
                    $("#msg" + $(this).attr("id").replace("New", "")).html("（将左侧包裹商品拖拽至下框区域）");
                },
                activeClass: "ui-state-hover",
                hoverClass: "ui-state-active"
            });
        }
		
		function setData(tt,ss){
			var pkgName=ss.find("td").eq(0).text();
			var clsName=ss.find("td").eq(1).text();
			tt.find("td").eq(0).text(pkgName);
			tt.find("td").eq(1).text(clsName);
		}
		
        //新增运单
        function NewTransport()
        {
            var key = new Date().getMilliseconds();
            while (2 > 1) {
                if ($("#New" + key).html() == undefined)
                    break;
                key = new Date().getMilliseconds();
            }
			var str=newPkg(key);
            $("#items").find('.word').before(str);
  
            dropItems("New" + ($("table[id^=New]").length + 1));
        }
		
		//包裹名称index
		var pkgIndex=1;
		
		//拼接创建新标签
		function newPkg(key){
			console.log('xxx');
			var buffer=new StringBuffer();
			console.log('x333xx');
				buffer.append('<div id="msgdv'+key+'" class="table-list change-blue current-blue">');
				buffer.append('<h4 class="col_blue">新包裹1</h4>');
				buffer.append('<div class="table-box">');
				buffer.append('<a href="javascript:DelTransport(\'New' + key + '\')" class="table-close"><i class="icon iconfont icon-cuowu1"></i></a>');
				buffer.append('<table id="New'+key+'" class="table text-center">');
				buffer.append('<thead>');
				buffer.append('<tr>');
				buffer.append('<th>商品名称</th>');
				buffer.append('<th>品类</th>');
				buffer.append('<th width="100">商品数量</th>');
				buffer.append('<th>分箱操作</th>');
				buffer.append('</tr>');
				buffer.append('</thead>');
				buffer.append('<tbody class="goods-item" >');
				buffer.append('<tr>');
				buffer.append('<td></td>');
				buffer.append('<td></td>');
				buffer.append('<td>');
				buffer.append('<input type="text" name="quantity" class="wt40 floatL ml10">');
				buffer.append('<p class="up-down floatL">');
				buffer.append('<button><i class="glyphicon glyphicon-menu-up"></i> </button>');
				buffer.append('<button><i class="glyphicon glyphicon-menu-down"></i></button>');
				buffer.append('</p>');
				buffer.append('</td>');
				buffer.append('<td>');
				buffer.append('<p class="wt80">');
				buffer.append('<select class="channelId">');
				buffer.append('<option>选择渠道</option>');
				for(var i=0;i<expressList.length;i++){
					buffer.append('<option value="'+expressList[i].id+'">'+expressList[i].msg+'</option>');					
				}
				buffer.append('</select>');
				buffer.append('</p>');
				buffer.append('</td>');
				buffer.append('</tr>');
				buffer.append('<tr class="address">');
				buffer.append('<td colspan="4">');
				buffer.append('<p class="select">');
				buffer.append('<select class="addressId">');
				buffer.append('<option>选择收货地址</option>');
				 
				for(var i=0;i<addressList.length;i++){
					buffer.append('<option value="'+addressList[i].address_id+'">'+addressList[i].addressStr+'</option>');
				}
				buffer.append('</select>');
				buffer.append('</p>');
				buffer.append('</td>');
				buffer.append('</tr>');
				buffer.append('</tbody>');
				buffer.append('</table>');
				buffer.append('</div>');
				buffer.append('</div>');
			
			return buffer.toString();
		}
		
		function addRow(key){
				var buffer=new StringBuffer();
				buffer.append('<tr id="'+key+'">');
				buffer.append('<td></td>');
				buffer.append('<td></td>');
				buffer.append('<td>');
				buffer.append('<input type="text" readonly="readonly" name="quantity" id="count'+key+'" value=0 class="wt40 floatL ml10">');
				buffer.append('<p class="up-down floatL">');
				buffer.append('<button onclick=\"ModifyNum(1,\'' + key +'\')"><i class="glyphicon glyphicon-menu-up"></i> </button>');
				buffer.append('<button onclick=\"ModifyNum(-1,\'' + key +'\')"><i class="glyphicon glyphicon-menu-down"></i></button>');
				buffer.append('</p>');
				buffer.append('</td>');
				buffer.append('<td>');
				buffer.append('<p class="wt80">');
				buffer.append('<select>');
				buffer.append('<option>选择渠道</option>');
				for(var i=0;i<expressList.length;i++){
					buffer.append('<option value="'+expressList[i].id+'">'+expressList[i].msg+'</option>');					
				}
				buffer.append('</select>');
				buffer.append('</p>');
				buffer.append('</td>');
				buffer.append('</tr>');
				buffer.append('<tr>');
				return buffer.toString();
		}

        function DelTransport(key) {
			
            var k = layer.confirm('确定删除该新预报？', function () {
				
                $("tr[id^=" + key + "]").each(function () {
                    $(this).remove();
                  
                    ModifyNum(0, this.id);
                });
                $("#" + key).remove();
                $("#msgdv" + key.replace("New", "")).remove();
                layer.close(k);

            });
        }
		//校验产品数量
        function CheckItemsCount()
        {
            Save();
        }

        //保存JSON信息
        function Save() {
       
        	var n = 0;
        	var tableCount = $("table[id^=New]").length;
        	
        	var  pkgList=[];
        	
        	//新的包裹数据
        	for(var i=0; i<tableCount; i++){
        		var tabVal = $("table[id^=New]")[i];
        		var newIdVal = tabVal.id;
        	
        		var goodsList=[];
				var inputs=$(tabVal).find('input');
				var channelId=$(tabVal).find('.channelId').val();
				var addressId=$(tabVal).find('.addressId').val();
				
				//封装成包裹物品json
				var goods={};
				$.each(inputs,function(i,input){
						goods[input.name]=input.value;
				});        		
        		goodsList.push(goods);
				
        		//包裹集合列表
        		if(goodsList.length>0){
					//保存至新包裹
					var address=1;
					var channelId=1
					var pkg={};
					pkg['addressId']=addressId;
					pkg['channelId']=channelId;
					pkg['goodsList']=goodsList;
            		pkgList.push(pkg);
        		}

        	}
        	
        	if(pkgList.length==0){
        		layer.alert('包裹未分箱不能保存');
        		return ;
        	}
        	
        	//包裹剩余物品合成一个新包裹
        	var items=$('#source >tr');
        	var limitPkg=[];
        	$.each(items,function(i,item){
        		
        		var inputs=$(item).find('input');
        		var quantity=$(item).find('td').eq(2).find("p").text();
        		
        		//只有数量不为0才放入剩余包裹中
        		if(quantity!=0){
	        		var goods={};
	        		goods['quantity']=quantity;
	        		$.each(inputs,function(i,input){
	        			goods[input.name]=input.value;
	        		});
	        		limitPkg.push(goods);
        		}
        	});
        	if(limitPkg.length>0){
	        	pkgList.push(limitPkg);
        	}
        	
        	//不能拆分为一个包裹
        	if(limitPkg.length==0&&pkgList.length==1){
        		layer.alert('不能拆分为一个包裹');
        		return ;
        	}
        	//防止重复提交
        	$('#saveButton_id').attr("href","#");
        	var pkgData={'newPkgs':JSON.stringify(pkgList),'package_id':$('#package_id').val()};
        	var url = "${mainServer}/homepkg/splitHomepkg";
        	$.ajax({
        		url:url,
                data : pkgData,
                type : 'post',
                dataType : 'json',
                async:false,
                success : function(data) {
                	var msg=data['msg'];
                	if(msg=="2"){
                		window.parent.location.href="${mainServer}/login";
                	}else if(msg=="3"){
                		alert("原始包裹信息找不到!");
                	}else{
                		layer.alert('完成分箱', {icon: 6});
                        window.parent.location.href=window.parent.location.href;
                	}
                	
                }
        	}); 
        }
        
        function Close() {
            parent.layer.closeAll();
        }
    </script>
</html>