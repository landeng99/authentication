﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0021)http://www.birdex.cn/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<!-- 
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/sylogin.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
<script type="text/javascript" src="${resource_path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
 -->
<%-- <script src="${resource_path}/js/jq.snow.js"></script> --%>
<!--
<script>
$(function(){
	$.fn.snow({ 
		minSize: 5,		//雪花的最小尺寸
		maxSize: 50, 	//雪花的最大尺寸
		newOn: 700		//雪花出现的频率 这个数值越小雪花越多
	});
});
</script>
-->

</head>
<body onkeydown="doKeyDown()">
<%-- <jsp:include page="header-1.jsp"></jsp:include> --%>
<jsp:include page="topIndexPage.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/login.css"></link>
<%--
<form method="post" action="${mainServer}/flogin" id="loginform">
<div id="wrap" style="height:660px;width:100%;">
<div id="subwrap">
    <div class="forgotpassword" style="padding-top: 0px; padding-bottom: 0px; height:660px;" id="wrapModal">
         <div class="modal-dialog" style="margin-left:35%;margin-top:40px;background-color: #ECFBFB; ">
              <div class="bt2"><span style="font-size:20px;">用户登录</span><span style="margin-left:5px;color:#B2A9A9;">UserLogin</span></div>  
              <dl class="Box02">
                  <dt><span>邮箱/手机号码：</span></dt>
                  <dd><input name="email" type="text" id="email" class="input_Light_red input_all_red" value="请输入邮箱或者手机号码"  style="width:280px;height:40px;" onFocus="focusEmail()"  onblur="blurEmail()"></dd>
                  <dt><span>登录密码：</span></dt>
                  <dd><input name="user_pwd" type="password" id="user_pwd" class="input_Light_blue" value="" style="width:280px;height:40px;" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;" onpaste="return false"></dd>
                  <dd style="margin-top:5px;display:none;" id="code">
                    <input id="txtCode" type="text" style="width: 150px;height:40px;vertical-align: middle;" size="15" maxlength="4" name="txtCode" value="请输入验证码"  onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;"/>
                    <input type="hidden" value="${count}" name="count" id="count"/>
					<img id="imgVC" src="${mainServer}/authImg" alt="看不清？点击刷新。"  style="width:80px;height:40px;vertical-align:middle;cursor: pointer;" hspace="2" onclick="refresh()"/>
					<br />
                    <label id="errCode" class="txtErr" style="color:#F50D0D;">
                    ${CodeErr}
                    </label>
				  </dd>
                  <dd><span class="pull-right">
                      <a href="${mainServer}/registe">注册</a>
                  </span><a href="${mainServer}/retrievePassword" style="vertical-align:middle;" class="blue">忘记密码？</a></dd>
              </dl>
			  <div style="margin:0 auto;width:90%;margin-top:10px;text-align:center;font-size:16px;color:#F50D0D;">
				   ${fail}
		      </div>
              <div class="Bottm2">
                   <input  type="button" id="btnlogin" value="登录" class="button_bule button pull-left" style="width:110px;margin-left:10px;"/>
				   <input  type="button" id="btnReset" value="重置" class="button_bule button pull-left" style="width:110px;margin-left:40px;"/>
              </div>
         </div>
         
    </div>
</div>
</div>
</form>
 --%>
	<div class="wrap bgImg">
        <div class="panel ">
            <h1 class="tit">用户登录</h1>
            <form class="floating-labels" method="post" action="${mainServer}/flogin" id="loginform">
                <div class="form-group mb40">
                    <input class="form-control" name="email" id="email" type="text" placeholder="输入邮箱/电话号码" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="">邮箱/手机号码</label> 
                    <span class="help-block hidden"><small>请输入邮箱或者手机号码</small></span>
                </div>
               
                 <div class="form-group mb40"><!-- has-success has-error -->
                    <input class="form-control" name="user_pwd" type="password" id="user_pwd" placeholder="最少6位密码" onpaste="return false" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="">密码</label>
                    <span class="help-block hidden"><small>请输入密码</small></span>
                </div>

                <div class="form-group form-group-btn mb40 hidden" id="code">
                    <input type="hidden" value="${count}" name="count" id="count"/>
                    <input class="form-control" id="txtCode" name="txtCode" type="text" placeholder="输入右边图形验证码" />
                    <span class="input-group-btn">
                        <img id="imgVC" src="${mainServer}/authImg" alt="看不清？点击刷新。" class="ver-code" hspace="2" onclick="refresh()" />
                    </span>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="">验证码</label>
                    <span class="help-block hidden"><small>请输入验证码</small></span>
                </div>
            </form>
            <a href="${mainServer}/retrievePassword" style="vertical-align:middle;" class="blue">忘记密码？</a>
            <div class="text-center mb20"><button id="btnlogin" class="btn-primary btn-input btn">登录</button></div>           
        </div>
    </div>
<script>
function doKeyDown(event) {
	 var e=event || window.event || arguments.callee.caller.arguments[0];

   if (e.keyCode == 13) {
       Login();
   }
}
function refresh(){
	document.getElementById("imgVC").src="${mainServer}/authImg?change="+(new Date()).getTime();
}
$(function () {
	var count=$("#count").val().trim();
	$("#count").val(count);
	if(count >= 3){
		$("#code").removeClass('hidden');
	}
	$("#btnlogin").click(Login);
	//$("#btnReset").click(Reset);
	$('form.floating-labels').on('blur', 'input.form-control', function(){
		var val=$(this).val().trim();
		$(this).val(val);
		if(val==""){
			$(this).parent().removeClass('has-success');
			$(this).parent().addClass('has-error');
			$(this).siblings('span.help-block').removeClass('hidden');
			$(this).siblings('span.glyphicon-remove').removeClass('hidden');
			$(this).siblings('span.glyphicon-ok').addClass('hidden');
		} else {
			$(this).parent().removeClass('has-error');
			$(this).siblings('span.help-block').addClass('hidden');
			$(this).siblings('span.glyphicon-remove').addClass('hidden');
			$(this).siblings('span.glyphicon-ok').removeClass('hidden');
			$(this).parent().addClass('has-success');
		}
	});
});
function Login(){
	var varemail = $("#email").val().trim();
	$("#email").val(varemail);
	if (varemail == "") {
		$("#email").blur();
		$("#email").focus(); 
		return false; 
	}
	var varuser_pwd = $("#user_pwd").val().trim();
	$("#user_pwd").val(varuser_pwd);
	if(varuser_pwd == ""){
		$("#user_pwd").blur();
		return false; 
	}
	if ($("#count").val() > 3) {
		var txtCode = $("#txtCode").val().trim();
		$("#txtCode").val(txtCode);
		if(txtCode == ""){
			$("#txtCode").blur();
			return false; 
		}
	}
	
// 	var phone = varemail;
// 	var isTrue = false;
// 	if (phone.match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/)) { 
// 		isTrue = true;
// 	}
// 	if((/^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/.test(phone))){
// 		isTrue = true;
// 	}
// 	if(!isTrue){
// 		alert("用户既不是手机格式,也不是邮件格式,请正确输入!");
// 		$("#email").focus(); 
// 		return flase;
// 	}
	$('#loginform').submit();
	return true; 
}
function Reset(){
	$("#email").val("请输入用户名");
	$("#user_pwd").val("");
	$("#txtCode").val("请输入验证码");
}
</script>
 <jsp:include page="footer-1.jsp"></jsp:include>
</body>
</html>