<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>9577批量支付</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
	<script src="${resource_path}/js/layer.min.js"></script>
	<script src="${resource_path}/js/common.js"></script>
	<script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
	<link rel="stylesheet" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" href="${resource_path}/css/common.css">
	<script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script>
<style type="text/css">
a {
    color: #666;
    text-decoration-color: -moz-use-text-color;
    text-decoration-line: none;
    text-decoration-style: solid;
}

 ul, ol, li {
    color: #666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}

body {
    color: #666;
    font-family: "微软雅黑";
    font-size: 12px;
    line-height: 22px;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}

 .p1 {
    font-size: 14px;
    height:30px;
}

.span-select {
display:-moz-inline-box;
display:inline-block;
width:100%;
margin-top: 16px;
font-size: 14px;
font-family:"微软雅黑";
}
.listpay {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.listpay li {
 
    width: 870px;
    float:left;

}

.li-list{
    border-left-color: #dcdcdc;
    border-left-style: solid;
    border-left-width: 1px;
    border-right-color: #dcdcdc;
    border-right-style: solid;
    border-right-width: 1px;
    border-top-color: #dcdcdc;
    border-top-style: solid;
    border-top-width: 1px;
    padding-top:8px;
}

.li-title{
  background-color:#f3f3f3;
}

.li-total{
    border-top-color: #dcdcdc;
    border-top-style: solid;
    border-top-width: 1px;
}

.size18 {
    font-size: 18px;
}

p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}

.orange, a.orange {
    color: #FF8B00;
}


.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
    height:80px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button1.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 119px;
}
.blue, a.blue {
    color: #0097DB;
}
</style>
</head>
<body class="bg-f1">

<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="../images/logo.jpg"></a>
            <h3><img src="../images/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="#"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li class="active"><a href="#"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="#"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="#"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="#"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="#"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="#"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <!--<li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>-->
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
        <h3 class="public-title mt20"><i class="icon iconfont icon-icon"></i>批量支付<a href="#" class="back">返回</a> </h3>
        <div class="column track">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab01" data-toggle="tab">运费</a></li>
                <li><a href="#tab02" data-toggle="tab">关税</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="tab01">
					<c:if test="${payTransportCostList!=null && fn:length(payTransportCostList)>0}">
					<dl>
						<dt style="margin-top:30px;">
							<a id="selectAll1" class="blue" href="javascript:void(0);" style="margin-left: 5px;color: #0097DB;">全选</a>
							<a id="reverse1" class="blue" href="javascript:void(0);" style="margin-left: 5px;color: #0097DB;">反选</a>
							<a id="cancelALL1" class="blue" href="javascript:void(0);" style="margin-left: 5px;color: #0097DB;">全取消</a>
						</dt>
						<dd style="margin-top:30px;">
							<ul class="listpay">
								<li class="li-list li-title">
									  <div style="float:left;width:30px;margin-top:5px;margin-left:5px;">
										   <p>&nbsp;</p>
									  </div>
									  <div style="float:left;width:150px;">
										   <p class="p1">单号</p>
									  </div>
									  <div style="float:left;width:300px;">
										   <p class="p1">商品</p>
									  </div>
									  <div style="float:left;width:80px;">
										   <p class="p1">重量（磅）</p>
									  </div>
									  <div style="float:left;width:70px;">
										   <p class="p1">单价</p>
									  </div>
									  <div style="float:left;width:70px;">
										  <p class="p1">运费</p>
									  </div>
									  <div style="float:left;width:80px;">
										  <p class="p1">增值费用</p>
									  </div>
									  <div style="float:left;width:70px;">
										  <p class="p1">包裹合计</p>
									  </div>
								</li>
								<c:forEach var="pkg"  items="${payTransportCostList}">
								 <li class="li-list">
									  <div style="float:left;width:30px;margin-top:2px;margin-left:5px;">
										   <input type="checkbox" class="checkbox1" name="select1" id="${pkg.logistics_code}" value="${pkg.transport_cost}" checked>
									  </div>
									  <div style="float:left;width:150px;">
										   <p class="p1">${pkg.logistics_code}</p>
									  </div>
									  <div style="float:left;width:300px;">
										  <c:forEach var="goods"  items="${pkg.goods}">
											<p class="p1">${goods.goods_name}*${goods.quantity}</p>
										  </c:forEach>
									  </div>
									  <div style="float:left;width:80px;">
										  <p class="p1">${pkg.weight}</p>
									  </div>
									  <div style="float:left;width:70px;">
										  <p class="p1"><fmt:formatNumber value="${pkg.price+0.00001}" type="currency" pattern="$#,###.##"/></p>
									  </div>
									  <div style="float:left;width:70px;">
										  <p class="p1"><fmt:formatNumber value="${pkg.freight+0.00001}" type="currency" pattern="$#,###.##"/></p>
									  </div>
									  <div style="float:left;width:80px;">
										  <p class="p1"><fmt:formatNumber value="${pkg.attach_service_price+0.00001}" type="currency" pattern="$#,###.##"/></p>
									  </div>
									  <div style="float:left;width:70px;">
										  <p class="p1"><fmt:formatNumber value="${pkg.transport_cost+0.00001}" type="currency" pattern="$#,###.##"/></p>
									  </div>
									  
								 </li>
							   </c:forEach>
								 <li class="li-total">
								   <div style="float:left;width:600px;">
									   <p class="p1">已选择包裹数：
										  <span class="orange size18" id="count1">${fn:length(payTransportCostList)}</span>
									   </p>
									   <p class="p1">总计费用：
										  <span class="orange size18" id ="money1">
											<fmt:formatNumber value="${transportCostTotal+0.00001}" type="currency" pattern="$#,###.##"/>
										  </span>
									   </p>
								   </div>
								 </li>
							</ul>
						</dd>
					</dl>
					</c:if>
					<c:if test="${payTransportCostList==null || fn:length(payTransportCostList)==0}">
                    <dl class="error-tip" style="display:none;">
                        <dt><img src="../images/error.jpg"> </dt>
                        <dd>
                            <p class="strong">您还没有要支付运费的包裹！</p>
                            <p class="link"><a href="#" class="col_blue">新增包裹</a>试试吧！ </p>
                        </dd>
                    </dl>
					</c:if>
                </div>
                <div class="tab-pane" id="tab02">
					<dl>
						<dt style="margin-top:30px;">
							<a id="selectAll2" class="blue" href="javascript:void(0);" style="margin-left: 5px;">全选</a>
							<a id="reverse2" class="blue" href="javascript:void(0);" style="margin-left: 5px;">反选</a>
							<a id="cancelALL2" class="blue" href="javascript:void(0);" style="margin-left: 5px;">全取消</a>
						</dt>
						<dd style="margin-top:30px;">
							<ul class="listpay">
								<li class="li-list li-title">
									  <div style="float:left;width:30px;margin-top:5px;margin-left:5px;">
										   <p>&nbsp;</p>
									  </div>
									  <div style="float:left;width:150px;">
										   <p class="p1">单号</p>
									  </div>
									  <div style="float:left;width:320px;">
										   <p class="p1">商品</p>
									  </div>

									  <div style="float:left;width:90px;">
										  <p class="p1">关税</p>
									  </div>
								</li>
							  <c:forEach var="pkg"  items="${payTaxtList}">
								 <li class="li-list">
									  <div style="float:left;width:30px;margin-top:5px;margin-left:5px;">
										   <input type="checkbox" class="checkbox2" name="select2" id="${pkg.logistics_code}" value="${pkg.customs_cost}" checked>
									  </div>
									  <div style="float:left;width:150px;">
										   <p class="p1">${pkg.logistics_code}</p>
									  </div>
									  <div style="float:left;width:320px;">
										  <c:forEach var="goods"  items="${pkg.goods}">
											<p class="p1">${goods.goods_name}*${goods.quantity}</p>
										  </c:forEach>
									  </div>

									  <div style="float:left;width:90px;">
										  <p class="p1"><fmt:formatNumber value="${pkg.customs_cost+0.00001}" type="currency" pattern="$#,###.##"/></p>
									  </div>
									  
								 </li>
							   </c:forEach>
								 <li class="li-total">
								   <div style="float:left;width:600px;">
									   <p class="p1">已选择包裹数：
										  <span class="orange size18" id="count2">${fn:length(payTaxtList)}</span>
									   </p>
									   <p class="p1">总计费用：
										  <span class="orange size18" id ="money2">
											<fmt:formatNumber value="${payTaxtTotal+0.00001}" type="currency" pattern="$#,###.##"/>
										  </span>
									   </p>
								   </div>
								 </li>
							 </ul>
							  <div class="PushButton">
								   <a id="cancel2" class="button" style="cursor:pointer;">取　消</a>
								   <a id="toSettlementTax" class="button" style="cursor:pointer;">去结算</a>
							  </div>
						</dd>
					</dl>
                    <dl class="error-tip" style="display:none;">
                        <dt><img src="../images/error.jpg"> </dt>
                        <dd>
                            <p class="strong">您还没有要支付关税的包裹！</p>
                            <p class="link"><a href="#" class="col_blue">新增包裹</a>试试吧！ </p>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>

    </div>
    <!--右侧内容结束-->

</div>
<!--中间内容结束-->

<!--底部开始-->
<div class="footer">
    粤ICP备14073551-2号，Copyright © 2016 www.su8exp.com Inc.All Rights Reserved.07693329337
</div>
<!--底部结束-->
<script>
$(function(){
	// 选择框选中
  $(".checkbox1").click(function() {
	 sum();
	  });
  
  // 全选
  $("#selectAll1").click(function() {
	  $("input[name='select1']").prop("checked",true);
	  sum();
	  });
  // 反选
  $("#reverse1").click(function() {
	  $("input[name='select1']").each(function(){
		  $(this).prop("checked",!this.checked);
		   });
	  sum();
	  });
  
  // 全取消
  $("#cancelALL1").click(function() {
	  $("input[name='select1']").prop("checked",false);
	  sum();
	  });
	 function sum(){
		  var summoney =0;
		  var id_array=new Array(); 
		  $("input[name='select1']:checked").each(function () {
			  summoney += Number($(this).val());
			  id_array.push($(this).attr('id'));
		  });
		  $("#money1").html("$"+Math.round(summoney*100)/100);
		  
		  var count1=$("input[name='select1']:checked").length;
		  $("#count1").html(count1);
	  }
	// 结算运费
      $("#toSettlement").click(function() {
          var count1=$("input[name='select1']:checked").length;
          if(count1==0){
               alert("包裹未选择！");
               return;
          }
          var id_array=new Array(); 
          $("input[name='select1']:checked").each(function () {
              id_array.push($(this).attr('id'));
          });
		  var logisticsCodes=id_array.join(',');
		  console.log(logisticsCodes);
		  if(logisticsCodes != null && logisticsCodes != ""){
			window.location.href = "${mainServer}/payment/paymentList?logisticsCodes=" + logisticsCodes;
		  }
          // 关税支付
          //parent.$('#logisticsCodes').val(id_array.join(','));
          // 运费支付
          //parent.$('#payFlag').val("1");
          // 关闭窗口
          //parent.layer.close(index);
       });
	//==============================运费关税分割========================================
	//==============================运费关税分割========================================
      // 选择框选中
      $(".checkbox2").click(function() {
         sum2();
          });
      // 全选
      $("#selectAll2").click(function() {
          $("input[name='select2']").prop("checked",true);
          sum2();
          });
      
      // 反选
      $("#reverse2").click(function() {
          $("input[name='select2']").each(function(){
              $(this).prop("checked",!this.checked);
               });
          sum2();
          });
      
      // 全取消
      $("#cancelALL2").click(function() {
          $("input[name='select2']").prop("checked",false);
          sum2();
          });
      
      function sum2(){
          var summoney =0;
          var id_array=new Array(); 
          $("input[name='select2']:checked").each(function () {
              summoney += Number($(this).val());
              id_array.push($(this).attr('id'));
          });
          $("#money2").html("$"+Math.round(summoney*100)/100);
          
          var count2=$("input[name='select2']:checked").length;
          $("#count2").html(count2);
      }

    
      // 结算关税
      $("#toSettlementTax").click(function() {  
          var count2=$("input[name='select2']:checked").length;
          if(count2==0){
               alert("包裹未选择！");
               return;
          }
          var id_array=new Array(); 
          $("input[name='select2']:checked").each(function () {
              id_array.push($(this).attr('id'));
          });
		  var logisticsCodes=id_array.join(',');
		  console.log(logisticsCodes);
		  if(logisticsCodes != null && logisticsCodes != ""){
			window.location.href = "${mainServer}/payment/taxList?logisticsCodes=" + logisticsCodes;
		  }
          // 关税支付
          //parent.$('#logisticsCodes').val(id_array.join(','));
          // 运费支付
         // parent.$('#payFlag').val("2");
          // 关闭窗口
          //parent.layer.close(index);
       });
     
});
</script>

</body>
</html>