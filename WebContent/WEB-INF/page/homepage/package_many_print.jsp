<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="overflow: hidden;">
<%
String path=request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
	<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
	<link href="${resource_path}/css/sydialog.css" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
	<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
	<script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script>	
	<script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>
	<script src="${backJsFile}/LodopFuncs.js"></script>
	<script src="${backJsFile}/My97DatePicker/WdatePicker.js"></script>
<style type="text/css">
.many_print_inst{padding-left:25px;width: 600px;font-size: 15px;}
.logistics_code_context{padding-left:25px;width: 600px;font-size: 15px;}
.logistics_codes{  
	margin-top:0px;
	height:250px;
	overflow-y: scroll;
	border:1px solid #ccc;
	width:700px; 
}
.logistics_codes li{
	float: left;
	list-style: none;
	margin:5px;
	/*margin-right:50px;*/
	padding: 3px 5px;
	text-align: center;
	min-width: 10px;
	border: 1px solid #ccc;
}
 #logistics_codes li{float: left;font-size: 15px;}
 
 .logistics_codes_all{  
	margin-top:0px;
	height:250px;
	overflow-y: scroll;
	border:1px solid #ccc;
	width:700px; 
}
.logistics_codes_all li{
	float: left;
	list-style: none;
	margin:5px;
	/*margin-right:50px;*/
	padding: 3px 5px;
	text-align: center;
	min-width: 10px;
	border: 1px solid #ccc;
}
 #logistics_codes_all li{float: left;font-size: 15px;}
 
 .class_logistics_code{}
 .class_original_num{}
  .class_print{
  color: red;
  }
 </style>
</head>
<body>    
<div style="padding-top:20px;">
	<input type="hidden" value="${package_ids}" id="package_ids"/>
	<div id="msg" style="color:#F92312;clear:both;text-align: center;">
		
	</div>
	<div class="Whole" id="addressid" style="margin-bottom:10px;clear: both;">
    	<div class="many_print_inst"><span style="color:#F00; font-size:16px; font-weight:bold;">批量打印功能需要安装控件，<a href="${mainServer}/help?item=60" target="_blank" style="color:#00F; text-decoration:none; font-size:16px; font-weight:bold;">点我查看使用说明</a></span></div>
		<div class="logistics_code_context">
        <span>已选批量打印包裹:</span>
			<ul id="logistics_codes" class="logistics_codes">
				
			</ul>
		</div>
		<div class="clear"></div>
		<div class="logistics_code_context"><span>选择包裹进行批量打印:</span>
	<div>
		<ul>
			<li style="float:left;margin-left: 20px;">运单号或关联单号:<input type="text"  style="width:200px;height:18px;" id="inputpkg" />
			打印状态:
						<select class="input" id="print_flag"
							name="print_flag" >
							<option value="-1">---选择打印状态---</option>
							<option value="0">未打印</option>
							<option value="1">已打印</option>
						</select>
			</li>
			<li style="float:left;margin-left: 20px;">包裹创建时间:<input type="text" class="input" id="fromDate" name="fromDate"
						style="width: 200px"
						onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
						onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>~
						<input type="text" class="input" id="toDate" name="toDate"
						style="width: 200px"
						onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
						onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
						
			<input type="button" value="查询" style="width:64px;height:36px;" id="searchpkg"/></li>
		</ul>
	</div>		
			<ul id="logistics_codes_all" class="logistics_codes">
				
			</ul>
		</div>
		<div class="PushButton" style="margin-top: 40px;">
				<a class="Block input14 white" href="javascript:save()" style="margin-right:30px;" id="saveButton_id">打印</a> 
				<a style="text-decoration: none;
					cursor: pointer; color: #666; width: 70px; font-family: &#39;微软雅黑&#39;; background-image: url(http://img.cdn.birdex.cn/images/input36.png);
					text-align: center;" class="button" id="cancelNew" href="javascript:close()">取 消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	var logistics_codes='${logistics_codes}';
	var original_nums='${original_nums}';
	var package_ids='${package_ids}';
	var printFlags='${printFlags}';
	if(logistics_codes!=""){
		var packageArry=new Array();
		packageArry=package_ids.split(',');
		var printFlagArry=new Array();
		printFlagArry=printFlags.split(',');
		var original_numArry=new Array();
		original_numArry=original_nums.split(',');
		var array=logistics_codes.split(',');
		$.each(array,function(i,str){
			var printStr="未打印";
			if(printFlagArry[i]=='1')
			{
				printStr="已打印";
			}
			var ul=$("#logistics_codes");
			var li=$("<li id=\"pakg"+packageArry[i]+"\"></li>");
			li.append("<span>运单号:<label class=\"class_logistics_code\">"+str+"</label></span>");
			li.append("<span style=\"margin-left:10px;\">关联单号:<label class=\"class_original_num\">"+original_numArry[i]+"</label></span>");
			li.append("<span style=\"margin-left:10px;\"><label class=\"class_print\">"+printStr+"</label></span>");
			li.append("<a href=\"javascript:;\" class=\"del\" style=\"cursor:pointer;margin-left:10px;\">删除</a>");
			ul.append(li);
		});
	}
	
	var all_logistics_codes='${all_logistics_codes}';
	var all_original_nums='${all_original_nums}';
	var all_package_ids='${all_package_ids}';
	var all_printFlags='${all_printFlags}';
	if(all_logistics_codes!=""){
		var all_packageArry=new Array();
		all_packageArry=all_package_ids.split(',');
		var all_printFlagArry=new Array();
		all_printFlagArry=all_printFlags.split(',');
		var all_original_numArry=new Array();
		all_original_numArry=all_original_nums.split(',');
		var all_array=all_logistics_codes.split(',');
		$.each(all_array,function(i,logistics_code){
			var printStr="未打印";
			if(all_printFlagArry[i]=='1')
			{
				printStr="已打印";
			}
			var isOnlyPkg=true;
			var $lis=$('#logistics_codes').find('li');
			$.each($lis,function(nh,li){
				var id=$(li).attr('id').replace("pakg","");
				if(all_packageArry[i]==id){
					isOnlyPkg=false;
				}
			});
			if(isOnlyPkg){
				var tempon="";
				if(all_original_numArry[i]!='null')
				{
					tempon=all_original_numArry[i];
				}
				var ul=$("#logistics_codes_all");
				var li=$("<li id=\"pakg_all"+all_packageArry[i]+"\"></li>");
				li.append("<span>运单号:<label class=\"class_logistics_code\">"+logistics_code+"</label></span>");
				li.append("<span style=\"margin-left:10px;\">关联单号:<label class=\"class_original_num\">"+tempon+"</label></span>");				
				li.append("<span style=\"margin-left:10px;\"><label class=\"class_print\">"+printStr+"</label></span>");
				li.append("<a href=\"javascript:;\" class=\"add\" style=\"cursor:pointer;margin-left:10px;\">添加</a>");
				ul.append(li);
			}
		});
	}
	
	$("#searchpkg").click(function(){
		var logistics_code=$.trim($("#inputpkg").val());
		var print_flag = ($('#print_flag')
				.find("option:selected"))[0].value;		
		var fromDate=$.trim($("#fromDate").val());
		var toDate=$.trim($("#toDate").val());
		var url = "${mainServer}/homepkg/searchpkgBylogisticsAndPackageCreateTimeRangeAndPrintStatus";
		$.ajax({
			url:url,
			type:'post',
			dataType:'json',
			async:false,
			data:{'logistics_code':logistics_code,'fromDate':fromDate,'toDate':toDate,'printFlag':print_flag},
			success:function(data){
				$("#msg").html("");
				var msg=data['msg'];
				if(msg=="2"){
					window.parent.location.href="${mainServer}/login";
				}else{
					if(msg=="Ok"){
						var all_logistics_codes=data['all_logistics_codes'];
						var all_original_nums=data['all_original_nums'];
						var all_package_ids=data['all_package_ids'];
						var all_printFlags=data['all_printFlags'];
						if(all_logistics_codes!=""){
							var all_packageArry=new Array();
							all_packageArry=all_package_ids.split(',');
							var all_printFlagArry=new Array();
							all_printFlagArry=all_printFlags.split(',');
							var all_original_numArry=new Array();
							all_original_numArry=all_original_nums.split(',');
							var all_array=all_logistics_codes.split(',');
							$("#logistics_codes_all").find("li").remove(); 
							$.each(all_array,function(i,logistics_code){
								var printStr="未打印";
								if(all_printFlagArry[i]=='1')
								{
									printStr="已打印";
								}
								var isOnlyPkg=true;
								var $lis=$('#logistics_codes').find('li');
								$.each($lis,function(nh,li){
									var id=$(li).attr('id').replace("pakg","");
									if(all_packageArry[i]==id){
										isOnlyPkg=false;
										//alert(id);
									}
								});
								if(isOnlyPkg){
									var tempon="";
									if(all_original_numArry[i]!='null')
									{
										tempon=all_original_numArry[i];
									}
									var ul=$("#logistics_codes_all");
									var li=$("<li id=\"pakg_all"+all_packageArry[i]+"\"></li>");
									li.append("<span>运单号:<label class=\"class_logistics_code\">"+logistics_code+"</label></span>");
									li.append("<span style=\"margin-left:10px;\">关联单号:<label class=\"class_original_num\">"+tempon+"</label></span>");				
									li.append("<span style=\"margin-left:10px;\"><label class=\"class_print\">"+printStr+"</label></span>");
									li.append("<a href=\"javascript:;\" class=\"add\" style=\"cursor:pointer;margin-left:10px;\">添加</a>");
									ul.append(li);
								}
							});
						}
					}else{
						$("#msg").html(msg);
					}
				}
			}
		});
	});
})

//提交
function save(){
	var package_ids=[];
	var $lis=$('#logistics_codes').find('li');
	$.each($lis,function(i,li){
		var id=$(li).attr('id').replace("pakg","");
		package_ids.push(id);		
	});
	if(package_ids.length==0){
		layer.msg("请搜索需要打印的包裹", 2, 5);
		return;
	}
	if(package_ids.length==1){
		layer.msg("批量打印包裹至少需要两个", 2, 5);
		return;
	}
	
	$('#saveButton_id').attr("href","#");
	LODOP=getLodop();
	$.each(package_ids,function(i,package_id){
    	$.ajax({
			url:'${mainServer}/homepkg/updateFrontPrint',
			type:'post',
			data:{package_id:package_id},
			dataType:"json",
			async:false,
			success:function(data){
			}			
		});
    	LODOP.NewPage();
    	LODOP.PRINT_INIT("打印面单");
		//LODOP.SET_PRINT_PAGESIZE(1,1016,1524," ");
		LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","73%");
    	LODOP.ADD_PRINT_URL(0,0,'100%','100%','<%=basePath%>backprint?package_id='+package_id);
    	LODOP.PRINT();//直接打印
    	//LODOP.PRINTA();//选择打印机打印
    	//LODOP.PREVIEW();//打印预览	 
    	
   /*      $.layer({
            title :'打印面单',
            type: 2,
            shadeClose: true,
            shade: 0.8,
            offset: ['10px', '400px'],
            area: ['590px', '800px'],
            iframe: { src: '${mainServer}/homepage/print?package_id='+package_id }
        }); */
       /*  window.open('${mainServer}/homepage/print?package_id='+package_id ) ; //打开窗口 */
	});
	
	  //刷新父窗口的页面
	 window.parent.location.href=window.parent.location.href;		
}
function close(){
	window.parent.layer.closeAll();
}

/*var $lis=$('#logistics_codes').find('li');
$.each($lis,function(i,li){
	var id=$(li).attr('id');
	
	
});*/
//删除包裹
$('.del').live('click',function(){
	var li=$(this).parent();
	li.remove();
	var id=$(li).attr('id').replace("pakg","");
    var logistics_code= $.trim(li.find('.class_logistics_code').text());
    var original_num= $.trim(li.find('.class_original_num').text());
    var printStr= $.trim(li.find('.class_print').text());
	var ul=$("#logistics_codes_all");
	var dli=$("<li id=\"pakg_all"+id+"\"></li>");
	dli.append("<span>运单号:<label class=\"class_logistics_code\">"+logistics_code+"</label></span>");
	dli.append("<span style=\"margin-left:10px;\">关联单号:<label class=\"class_original_num\">"+original_num+"</label></span>");
	dli.append("<span style=\"margin-left:10px;\"><label class=\"class_print\">"+printStr+"</label></span>");
	dli.append("<a href=\"javascript:;\" class=\"add\" style=\"cursor:pointer;margin-left:10px;\">添加</a>");
	ul.append(dli);
	li.remove();
});

//添加包裹
$('.add').live('click',function(){
	var li=$(this).parent();
	var id=$(li).attr('id').replace("pakg_all","");
    var logistics_code= $.trim(li.find('.class_logistics_code').text());
    var original_num= $.trim(li.find('.class_original_num').text());
    var printStr= $.trim(li.find('.class_print').text());
	var ul=$("#logistics_codes");
	var dli=$("<li id=\"pakg"+id+"\"></li>");
	dli.append("<span>运单号:<label class=\"class_logistics_code\">"+logistics_code+"</label></span>");
	dli.append("<span style=\"margin-left:10px;\">关联单号:<label class=\"class_original_num\">"+original_num+"</label></span>");
	dli.append("<span style=\"margin-left:10px;\"><label class=\"class_print\">"+printStr+"</label></span>");
	dli.append("<a href=\"javascript:;\" class=\"del\" style=\"cursor:pointer;margin-left:10px;\">删除</a>");
	ul.append(dli);
	li.remove();
});
</script>
</body>
</html>