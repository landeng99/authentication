<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>

<style type="text/css">


.size18 {
    font-size: 18px;
}

.size16 {
    font-size: 16px;
}

p {
    color: #666666;
    font-size: 12px;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}

.blue, a.blue {
    color: #0097DB;
}

.orange, a.orange {
    color: #FF8B00;
}

.balance span {
display:-moz-inline-box;
display:inline-block;
width:150px; 
}


.pd10 {
    padding-bottom: 0;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 0;
}


.balance a:hover, .balance a.current {
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input21.png");
    background-origin: padding-box;
    background-position: center top;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
}

.balance a {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input20.png");
    background-origin: padding-box;
    background-position: center top;
    background-repeat: no-repeat;
    background-size: auto auto;
    float: left;
    font-size: 16px;
    height: 32px;
    line-height: 32px;
    margin-bottom: 0;
    margin-left: 5px;
    margin-right: 5px;
    margin-top: 0;
    overflow-x: hidden;
    overflow-y: hidden;
    padding-bottom: 9px;
    text-align: center;
    width: 99px;
}

.balance {
    overflow-x: hidden;
    overflow-y: hidden;
    padding-top: 10px;
    width: 100%;
}
.balance .pull-left {
    width: 100%;
}

.List-input {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.List-input li {
    height: 52px;
    line-height: 32px;
    list-style-type:none
}
.List-input li span.pull-left {
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: right;
    width: 63px;
}
.List-input li input {
    margin-right: 4px;
}
.List-input li .text-short, .List-input li .text-long {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 178px;
}
.List-input li .text-long {
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input727.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    width: 726px;
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button1.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 119px;
}

.span4 .label{
     width:80px;
     float: left;
}

.line1 {
    margin-left: 0;
    margin-right: 0;
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
</style>
<body>
<div style="background-image:none;border-right:none;width:897px;padding-top:5px;" id="content">
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
    <input type="hidden" id="able_balance" value="${frontUser.able_balance}">
    <input type="hidden" id="treasure">
        <div >
            <div class="balance" >
                <div style="margin-left:8px;">
                    <p><strong>账户余额：<strong>
                      <span class="orange size18">
                        <fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#,###.##"/>
                      </span><strong>冻结余额：<strong>
                      <span class="orange size18">
                       <fmt:formatNumber value="${frontUser.frozen_balance}" type="currency" pattern="$#,###.##"/>
                    </span><strong>可用余额：<strong>
                    <span class="orange size18">
                       <fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#,###.##"/>
                    </span>
                   </p>
               </div>
                <div Style="margin-top:20px; margin-left:300px;">
                   <a item="alipay" class="control current" style="cursor: pointer;">支付宝</a>
                   <a item="bank" class="control" style="cursor:pointer;">网　银</a>
                </div>
            </div>
            <div class="line1"></div>
            <div id="alipay">
                 <ul class="List-input">
                      <li class="span4"><p><span class="label">可退余额：</span><span class="orange size18"><fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#,###.##"/></span></p></li>
                      <li class="span4"><p><span class="label">提现金额：</span><input type="text" class="text-short" id="amount" name="amount" style="padding-left: 5px;"><span style="color:Red;">*</span></p></li>
                      <li class="span4">
                          <p>
                            <label class="label" for="alipayName">支付宝姓名：</label>
                            <input type="text" class="text-short" id="alipayName" value="${frontUser.alipayName}">
                            <span style="color:Red;">*</span>
                            <span id ="message0" style="color: red;font-size: 13px;" class="message"></span>
                          </p>
                      </li>
                      <li class="span4">
                          <p>
                             <span class="label">支付宝账号：</span>
                             <span id ="pay_treasure">${frontUser.pay_treasure}</span>
                             <!-- <span style="margin-left:10px;"><a href="javascript:void(0);" class="blue size16" id="payBind">[修改]</a></span>  -->
                          </p>
                      </li>
                      <li class="span4"><p><span class="label">提现备注：</span><input type="text" class="text-long" id="remark" name="remark" style="padding-left: 5px;"></li>
                      <li class="span4"><p><span class="label">&nbsp;</span></li>
                  </ul>
                  
          
                  <div class="PushButton">
                       <p style="padding-left:1px;">注：以上信息请仔细核对</p>
                  </div>
                  <div class="PushButton">
                       <a id="resetAlipay" class="button" style="cursor:pointer;">清　空</a>
                       <a id="toAlipay" class="button" style="cursor:pointer;">申请提现</a>
                  </div>
          </div>

          <div style="display: none;" id="bank">
                 <ul class="List-input">
                      <li class="span4"><p><span class="label">可退余额：</span><span class="orange size18"><fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#,###.##"/></span></p></li>
                      <li class="span4"><p><span class="label">提现金额：</span><input type="text" class="text-short" id="amount_bank" name="amount_bank" style="padding-left: 5px;"><span style="color:Red;">*</span></p></li>
                      <li class="span4">
                          <p>
                              <span class="label">开户姓名：</span>
                              <!-- <span style="margin-left:10px;"><a href="javascript:void(0);" class="blue size16" id="bankBind">[修改]</a></span> -->
                          </p>
                       </li>
                      <li class="span4">
                           <p>
                              <span class="label">开户银行：</span>
                              <span id ="bank_account">${frontUser.bank_account}</span>
                           </p>
                      </li>
                      <li class="span4">
                           <p>
                              <span class="label">银行卡号：</span>
                              <span id ="bank_card">${frontUser.bank_card}</span>
                           </p>
                      </li>
                      <li class="span4"><p><span class="label">提现备注：</span><input type="text" class="text-long" id="remark_bank" name="remark_bank" style="padding-left: 5px;"></li>
                  </ul>
                  <div class="PushButton">
                       <p style="padding-left:1px;">注：以上信息请仔细核对</p>
                  </div>
                  <div class="PushButton">
                       <a id="resetBank" class="button" style="cursor:pointer;">清　空</a>
                       <a id="toBank" class="button" style="cursor:pointer;">申请提现</a>
                  </div>
          </div>
    </div>
    
</div>

    <script type="text/javascript">

      $(".control").click(function() {
          $(".control").removeClass("current");
          $(this).addClass("current");
          var item = $(this).attr("item"); //当前操作的内容
          if (item == "alipay") {
              $("#alipay").show();
              $("#bank").hide();
            } else {
              $("#alipay").hide();
              $("#bank").show();
            }
          });
       //支付宝提现
      $("#toAlipay").click(function () {
          
          //获取窗口索引
          var index = parent.layer.getFrameIndex(window.name);

          var able_balance =$('#able_balance').val();
          var amount =$('#amount').val();
          var remark =$('#remark').val();
          var alipayName = $("#alipayName").val();
          
          if(amount==null||amount.trim()==""){
              alert("金额不能为空！");
              $('#amount').focus();
              return;
          }
          if(alipayName==null||alipayName.trim()==""){
        	  $("#message0").html("提示!支付宝姓名不能为空！");
              //alert("提示!支付宝姓名不能为空！");
              $('#alipayName').focus();
              return;
          }  
          
          var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
          if(!exp.test(amount)){ 
              alert("金额格式错误！");
              $('#amount').focus();
              return;
            }
          
          if(parseFloat(amount) < parseFloat(0.1)){
              alert("最小金额为0.1！");
              $('#amount').focus();
              return;
          }
          
          if(parseFloat(amount) > parseFloat(able_balance)){
              alert("账户可用余额不足！");
              $('#amount').focus();
              return;
          }

          var url="${mainServer}/member/withdrawal";
          $.ajax({
              url:url,
              data:{
                  "amount":amount,
                  "remark":remark,
                  "alipayName":alipayName,
                  "flag":"1"},
              type:'post',
              dataType:'json',
              success:function(data){
                  alert(data.message);
                  if(data.result){
                      // 关闭窗口
                      parent.layer.close(index);  
                  }
              }
          });
      }); 
       
       
      //网银提现
      $("#toBank").click(function () {
          
          //获取窗口索引
          var index = parent.layer.getFrameIndex(window.name);

          var able_balance =$('#able_balance').val();
          var amount_bank =$('#amount_bank').val();
          var remark_bank =$('#remark_bank').val();
          
          if(amount_bank==null||amount_bank.trim()==""){
              alert("金额不能为空！");
              $('#amount_bank').focus();
              return;
          }

          var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
          if(!exp.test(amount_bank)){ 
              alert("金额格式错误！");
              $('#amount_bank').focus();
              return;
            }

          if(parseFloat(amount_bank) < parseFloat(0.1)){
              alert("最小金额为0.1！");
              $('#amount_bank').focus();
              return;
          }

          if(parseFloat(amount_bank) > parseFloat(able_balance)){
              alert("账户可用余额不足！");
              $('#amount_bank').focus();
              return;
          }

          var url="${mainServer}/member/withdrawal";
          $.ajax({
              url:url,
              data:{
                  "amount":amount_bank,
                  "remark":remark_bank,
                   "flag":"2"},
              type:'post',
              dataType:'json',
              success:function(data){
                  alert(data.message);
                  if(data.result){
                      // 关闭窗口
                      parent.layer.close(index);  
                  }
              }
          });
      }); 
       

      $("#resetBank").click(function () {

           $("#amount_bank").val("");
           $("#remark_bank").val("");
           $("#rechargeAmount").focus();
      }); 
      
      $("#resetAlipay").click(function () {
          
          $("#amount").val("");
          $("#remark").val("");
          $("#amount").focus();
      }); 
      
      $("#payBind").click(function () {

          layer.open({
              title :'绑定支付宝',
              type: 2,
              fix: true,
              shade: [0.5, '#ccc', true],
              border: [0, 0.3, '#666', true],
              area: ['400px', '330px',],
              content:'${mainServer}/account/payBindInit',
          });
      });
      $("#bankBind").click(function () {
          layer.open({
              title :'绑定银行卡',
              type: 2,
              fix: true,
              shade: [0.5, '#ccc', true],
              border: [0, 0.3, '#666', true],
              area: ['400px', '360px',],
              content:'${mainServer}/account/bankBindInit',
          });
      });
        </script>
</body>
</html>