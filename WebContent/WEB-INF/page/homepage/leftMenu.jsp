<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">
    $(function () {
        $("ul.sideaav li").each(function () {
            if (document.location.href.toLowerCase().indexOf($(this).attr("item").toLowerCase()) > -1) {
                $(this).addClass("current");
                $(this).find("i").css("background-image", "url('" + $(this).attr("img") + "')");
                $("ul.nav>li").removeClass("current");
                $("#headAccount").addClass("current");
            }
        });

        
        
        // 充值
        $("#charge").click(function () {
            layer.open({
                title : '充值',
                type : 2,
                shadeClose : true,
                shade: [0.5, '#ccc', true],
                area : [ '1000px', '570px' ],
                border: [1, 0.3, '#666', true],
                content : '${mainServer}/member/rechargeInit',
                end : function(index) {
                    // 点击保存的时候 刷新页面
                 //   if ($('#returnCode').val() == 1) {
                //        alert("修改成功！");
            //            window.location.href = "${mainServer}/express/expressInit";
              //      }
                }
            });

        })
    });
</script>

<div class="left" style="margin-bottom:250px;">
          <h3 class="size14"></h3>
          <p><span style="float:right;">
                <c:if test="${frontUser.status==0}">未激活</c:if>
                <c:if test="${frontUser.status==1}">已激活</c:if>
                <c:if test="${frontUser.status==2}">禁用</c:if>
             </span>${frontUser.account}
          </p>
          <p></p>
          <ul class="recharge">
              <li class="span5 pull-right text-center">
                  <i class="icon06"></i>
                  <h3 class="size14">${countAll}张</h3>
                  <span style="font-size:13px;"><a href="${mainServer}/account/accountInit" class="blue">优惠劵</a></span>
                  
              </li>
              <li class="span5 pull-left text-center" style="width:auto;">
                  <a title="钱包余额" style="text-decoration:none;cursor:pointer;"><i class="icon05"></i>
                  <h3 class="size14"><span style="padding-left:1px;"><fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#,###.00"/></span></h3></a>
                  <a style="cursor:pointer;font-size:13px;" class="blue" id="charge">充值</a>
              </li>
          </ul>
          <ul class="modification">
              <li class="span6"><a style="cursor:pointer;" class="blue icon07" id="editPass">修改密码</a></li>
              <li class="span6" style="display:none;"><a href="" class="blue icon08">修改订阅</a></li>
              <li class="span6" style="display:none;"><a href="" class="blue icon09">使用习惯</a></li>
          </ul>
           <ul class="sideaav">
              <li class="span12" item="transport" img="${mainServer}/resource/img/icon10_fff.png"><a href="${mainServer}/homepage/transport" class="size16"><i class="icon10"></i>我的包裹管理</a></li>
              <li class="span12" item="warehouse" img="${mainServer}/resource/img/icon11_fff.png"><a href="${mainServer}/warehouse" class="size16"><i class="icon11"></i>海外仓库地址</a></li>
              <li class="span12" item="destination" img="${mainServer}/resource/img/icon12_fff.png"><a href="${mainServer}/destination" class="size16"><i class="icon12"></i>收货地址管理</a></li>
              <li class="span12" item="record" img="${mainServer}/resource/img/icon13_fff.png"><a href="${mainServer}/member/record" class="size16"><i class="icon13"></i>账户记录</a></li>
              <li class="span12" item="account" img="${mainServer}/resource/img/icon14_fff.png"><a href=${mainServer}/account/accountInit class="size16"><i class="icon14" style="background-image: url(${mainServer}/resource/img/icon14.png);"></i>账户设置</a></li>
          </ul>
</div>