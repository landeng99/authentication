<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">

p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 18px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}


.text1 {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.text1 li {
    height: 40px;
    list-style-type:none
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 375px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button1.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 119px;
}

.span4 .label{
     width: 100%;
     float: left;
}

</style>
<body>
<div style="background-image:none;border-right:none;width:450px;padding-top:5px;"/>
<input type="hidden" id="coupon_id" name="coupon_id" value="${coupon_id}">
<input type="hidden" id="couponLogistics_code" name="couponLogistics_code" value="${couponLogistics_code}">
            <div>
                <ul class="text1">
                      <li class="span4">
                         <p>
                            <span class="label">您正在进行网上支付...</span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label">支付完成后，请根据支付情况点击下面按钮确认结果。</span>
                         </p>
                      </li>
                  </ul>
                  <div class="PushButton">
                       <a id="no" class="button" style="cursor:pointer;">支付失败</a>
                   <a id="yes" class="button" style="cursor:pointer;">支付成功</a>
                  </div>
           </div>
  <script type="text/javascript">

    //获取窗口索引
    var index = parent.layer.getFrameIndex(window.name);
    $("#no").click(function () {
        // 关闭窗口
        parent.layer.close(index);
    }); 
    $("#yes").click(function () {
        // 关闭窗口
        
        var couponLogistics_code =$('#couponLogistics_code').val();
        var coupon_id=$('#coupon_id').val();
           var url="${mainServer}/payment/payConfirmCouponUsed";
        $.ajax({
            url:url,
            data:{
                "coupon_id":coupon_id,
                "couponLogistics_code":couponLogistics_code},
            type:'post',
            async:false,
            dataType:'json',
            success:function(data){
            }
        });
        parent.layer.close(index);
    }); 

  </script>
</body>
</html>