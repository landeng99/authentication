<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >

<!-- 头部 -->
<div class="wrap p-header">
    <div class="wrap-box">
        <header class=" row">
            <div class="col-sm-4">
                <div class="name">
                    HI，
                    <a href="${mainServer}/account/accountInit" class="col_orange">${frontUser.user_name}</a>
                </div>
                <c:if test="${null != frontUser}">
	                <div class="quit">
	                    <a href="${mainServer}/exit">退出</a>
	                </div>
                </c:if>
            </div>
            <div class="col-sm-8">
                <ul class="p-header-ul"> 
                    <li class="item"><a href="${mainServer}">返回首页</a></li>
                    <li class="item"><a href="${mainServer}/free">资费说明</a></li>
                    <li class="item"><a href="${mainServer}/prohibition">禁运物品</a></li>
                    <li class="item"><a href="${mainServer}/help">用户指南</a></li>
                    <li class="item"><a href="#">客服中心</a></li>
                </ul>
            </div>
        </header>
    </div>
</div>

