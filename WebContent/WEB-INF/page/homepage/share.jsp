<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<link href="${resource_path}/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/share.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="${resource_path}/js/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/jquery.nicescroll.min.js" charset="utf-8"></script>
<script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
<script type="text/javascript">
	var mainServer = '${mainServer}';
	var backServer = '${backServer}';
	var jsFileServer = '${backJsFile}';
	var cssFileServer = '${backCssFile}';
	var imgFileServer = '${backImgFile}';
	var uploadPath = '${uploadPath}';
</script>
<style>
.ke-upload-area{
	position: relative;
	overflow: hidden;
	margin: 0;
	padding: 0;
	*height: 25px;
}
.uploadify-queue{
	display:none;
}
.uploadify{
	position: absolute;
	font-size: 60px;
	top: 0;
	right: 0;
	padding: 0;
	margin: 0;
	z-index: 811212;
	border: 0 none;
	opacity: 0;
	filter: alpha(opacity=0);
	width:81px;
	height:50px;
}
</style>
</head>
<body style="width:600px !important;">
<form class="form-horizontal"  style="max-width: 540px;">
<fieldset>
  <div id="legend" class="">
        <a style="position:absolute;left:560px;top:5px;border:none;background-color:none;background-image:none;box-shadow:none;" name="close" id="close" class="btn btn-default" role="button" href="javascript:Close()"> X </a>
        <legend>晒单<c:if test="${approval_status ==2}">【已审核】</c:if></legend>
  </div>
  <c:if test="${approval_status ==3}">
    <div class="form-group">
    <label class="col-xs-3 control-label" style="color: red;"><b>拒绝原因：</b></label>
    <div class="col-xs-6">
      <p class="form-control-static" id="OrderCodereard" style="color: red;"><b>${reason}</b></p>
    </div>
  </div>
  </c:if>
  
  <div class="form-group">
    <label class="col-xs-3 control-label">转运单号：</label>
    <div class="col-xs-6">
      <p class="form-control-static" id="OrderCode">${logistics_code}</p>
    </div>
  </div>
  <div class="form-group" id="urlDiv">
    <label for="inputPassword" class="col-xs-3 control-label">晒单链接：</label>
    <div class="col-xs-9">
	    <c:if test="${approval_status !=2}">
	      <input type="text" class="form-control" id="share_link" name="share_link" value="${share_link}" placeholder="晒单链接" value=""/>
	    </c:if>
	    <c:if test="${approval_status ==2}">	      
	      <p class="form-control-static" id="OrderCode">${share_link}</p>
	    </c:if>
    </div>
    <input type="hidden" name="package_id" value="${package_id}"/>
  </div>
    <div class="form-group">
    <label for="inputPassword" class="col-xs-3 control-label">晒单图片：</label>
    <c:if test="${approval_status !=2}">
    <div style="background-color: #fff; margin-left: 0px;padding-top: 0px; overflow: hidden;float:left;">
            <div style="width: 81px; height: 50px; text-align: center;display: block;">
                <a href="javascript:;" style="text-align:left;" class="notSafari">
					<div class="ke-inline-block ">
						<div class="ke-upload-area">
						<div class="ke-button-common" style="width: 81px; height: 50px; cursor: pointer; background-image: url(${resource_path}/img/share.png);">
							<input type="button" class="ke-button-common ke-button" value="" style="margin-right: 0px; width:81px; height: 50px; cursor: pointer; background-image: url(${resource_path}/img/share.png); background-position: 0% 50%;"/></div>
						<input type="file" class="uploadify" id="ke-upload-file" name="imgFile" />
						</div>
					</div>	
				</a>               
            </div>
     </div>
             
    <div class="col-xs-6">
        <p class="form-control-static tip">点击左边上传图片，最多可上传三张。</p>
        <p class="form-control-static tip">点击已上传图片可删除重新上传。</p>
    </div>
    </c:if>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-3 col-xs-10" id="show">
        <div style="width:450px; margin-left: 45px;height: 0px;" id="divOrderImg" class="orderImg">
        	<c:forEach var="shopImg"  items="${shopImgList}">
        	<div class="divImgOrder" val="${shopImg.img_id}" style="width: 120px; height: 120px; margin-top:5px;margin-left:8px;float:left;border:1px solid #E1E1E1;" id="divOrderImg${shopImg.img_id}">
				<c:if test="${approval_status !=2}">
				<div id="divDel${shopImg.img_id}" style="margin:5px 0px 0px 105px;width:15px;position:absolute;">
					<img alt="删除" title="删除" style="cursor: pointer; display: none;" id="delOrderImg${shopImg.img_id}" onclick="delOrderImg(${shopImg.img_id})" src="${resource_path}/img/close-x.png"/>
				</div>
				</c:if>
				<div style="width: 120px;height:120px;display:table-cell;vertical-align: middle;text-align: center;">
					<img  alt="" src="${resource_path}${shopImg.img_path}" id="imgOrder${shopImg.img_id}" style="cursor:pointer;width:120px;"/>
					<input type="hidden" id="hdOrderImg${shopImg.img_id}" value="20150618154724_872.png"/>
				</div>
			</div>
			</c:forEach>
        </div>
    </div>
  </div>
  <c:if test="${approval_status !=2}">
  <div class="form-group">
    <div class="col-xs-offset-4 col-xs-8">
      <button type="button" class="btn btn-success" name="confirm" id="confirm">确   定</button>
      <button type="button" class="btn btn-default" name="cancel" id="cancel">取   消</button>
    </div>
  </div>
  </c:if>

<c:if test="${approval_status ==2}">
	<table style="color: green;">
	<tr>
	<td>奖励优惠券编号：</td>
	<td>${userCoupon.coupon_code}</td>
	</tr>
	<tr>
	<td>奖励优惠券名称：</td>
	<td>${userCoupon.coupon_name}</td>
	</tr>
	<tr>
	<td>奖励优惠券数量：</td>
	<td>${userCoupon.quantity}</td>
	</tr>
	</table>  
 </c:if>
  <div class="form-group">
    <div class="col-xs-12">
      <p class="form-control-static">速8快递注册会员，收货后成功晒单，即可获得每晒一单20元优惠劵奖励。</p>
    </div>
  </div>
  <div>
        <legend id="insingleRule">晒单规则：</legend>
  </div>
    <div class="form-group" style="margin-bottom:0;">
        <div class="col-xs-12" id="insingleDesc" style="height:200px;">
          <p class="form-control-static">1.每晒单一次可得到10元优惠劵，优惠劵只可抵消运费，不可提现</p>
          <p class="form-control-static">2.同一跨境转运订单号在只能晒单一次，将视为无效</p>
          <p class="form-control-static">3.与海淘无关网站或论坛的晒单视为无效</p>
		  <p class="form-control-static">4.晒单帖子如在2天内被删除，将视为无效</p>
		  <p class="form-control-static">5.优惠券有效期60天，每个订单仅限使用一张优惠券</p>
		  <p class="form-control-static">6.晒单订单需在2个月内参与活动则有效</p>
		  <p class="form-control-static">7.工作人员审核通过后的3个工作日内审核，会把优惠券赠送到您的账户中（节假日顺延）</p>
		  <p class="form-control-static">8.分享微信朋友圈，需24小时以上方可有效</p>

        </div>
  </div>
  </fieldset>
</form>

<script type="text/javascript">
    var index = parent.layer.getFrameIndex(window.name);
    var package_id=${package_id};
    $(function () {
        var AvailHeight = window.screen.availHeight;
        if (AvailHeight < 930) {
            $("html").niceScroll({ styler: "fb", cursorcolor: "#000", cursoropacitymin: 1, cursoropacitymax: 1, autohidemode: false });
            $("#insingleDesc").niceScroll({
                autohidemode: false,
                cursorwidth: '6px'
            });
        }
        $("#confirm").click(function(){
       	 if(!validateLogin(1)){
    		 return ;
    	 }
        	var url = '${mainServer}/homepkg/share';        	
        	var share_link=$("#share_link").val();
        	//alert(share_link);
        	if(share_link==""){
        		layer.msg("请输入包裹关联单号", 2, 500);
        		alert("链接地址不能为空!")
        		return;
        	}
        	//alert($('.orderImg').text());
        	if($('.orderImg').text()==""){        		
        		alert("请上传晒单图片！")
        		return;
        	}
    		$.ajax({
    			url:url,
    			type:'post',
    			dataType:'json',
    			async:false,
    			data:{package_id:package_id,share_link:share_link},
    			success:function(data){
    				var msg=data['msg'];
    				if(msg=="2"){
    					window.parent.location.href="${mainServer}/login";
    				}else{
    					Close();
    				}
    			}
    		});
        });
        $("#cancel").click(function(){
        	parent.layer.close(index);
        });
    });
    $(".divImgOrder").hover(function () {
        $("#delOrderImg" + $(this).attr("val")).show();
    }, function () {
        $("#delOrderImg" + $(this).attr("val")).hide();
    });
    
    $(document).ready(function() { 
	    $("#ke-upload-file").uploadify({
	        'height'        : 50,
	        'swf'           : '${resource_path}/js/uploadify.swf',
	        'uploader'      : '${mainServer}/homepkg/sharepic?package_id='+package_id,
	        'width'         : 81,
	        'fileObjName'   : 'the_files',
	        'fileSizeLimit':'1MB',
	        'fileTypeDesc':'Image Files',
	        'fileTypeExts': '*.jpg;*.png',
	        'onUploadSuccess':function(file, data, response) {
	            if(data !=""){
	            	initOrderImg(data);
	            }else{
	            	alert("晒单图片不能超过3张");
	            }
	            
	        }
	    });
	});
    function Close() {
        parent.layer.close(index);
    }
    function initOrderImg(data) {
        var strs= new Array();
        strs = data.split("^");
        //alert(strs);
        if (strs != "") {
                 var htmlStr = "<div class='divImgOrder' val='" + strs[2] + "' style='width: 120px; height: 120px; margin-top:5px;margin-left:8px;float:left;border:1px solid #E1E1E1;' id='divOrderImg" + strs[2] + "'>";
                 htmlStr += "<div id='divDel" + strs[2] + "' style='margin:5px 0px 0px 105px;width:15px;position:absolute;'>";
                 htmlStr += "<img alt='删除' title='删除' style='cursor:pointer;display:none;' id='delOrderImg" + strs[2] + "' onclick='delOrderImg(" + strs[2] + ")' src='${resource_path}/img/close-x.png' /></div>";
                	htmlStr += "<div style='width: 120px;height:120px;display:table-cell;vertical-align: middle;text-align: center;'>";
                 htmlStr += "<img alt='' src='" + '${resource_path}'+strs[0] + "' id='imgOrder" + strs[2] + "' style='cursor:pointer;width:120px;' />";
                 //htmlStr += "<img onclick='showImg(this)' onload=\"javascript:DrawImage(this,'120','120')\" alt='' src='" + imgUrl + "' id='imgOrder" + i + "' style='cursor:pointer;' />";
                 htmlStr += "<input type='hidden' id='hdOrderImg" + strs[2] + "' value='" + strs[0] + "' /></div>";
                 htmlStr += "</div>";
                 $("#divOrderImg").append(htmlStr);
           }
        $(".divImgOrder").hover(function () {
            $("#delOrderImg" + $(this).attr("val")).show();
        }, function () {
            $("#delOrderImg" + $(this).attr("val")).hide();
        });
     }
    function delOrderImg(img_id) {
		var url = "${mainServer}/homepkg/deleteSharePic";
		$.ajax({
			url:url,
			type:'post',
			dataType:'json',
			async:false,
			data:{img_id:img_id},
			success:function(data){
				if(data){
					var img_id=data['img_id'];
					$("#divOrderImg"+img_id).empty();
					$("#divOrderImg"+img_id).remove();
				}
			}
		});
	}
</script>
</body>
</html>