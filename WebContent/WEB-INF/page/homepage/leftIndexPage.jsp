<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- 禁用物品 -->
<div class="s-content hidden" id="li-jywp" name="hiddenDiv">
	<div class="s-con-banner">
		<img id="topImg" src="${resource_path}/images/sub-jy-banner.png" />
	</div>
	<div class="s-embargo">
		<div class="clearfix pt20">
			<div class="s-e-tit tit col-sm-2">禁运物品</div>
		</div>
		<ul class="s-embargo-ul">
			<c:forEach items="${prohibitionLists}" var="prohPojo">
				<li>
					<dl class="s-e-dl">
						<dt>${prohPojo.class_name}</dt>
						<dd title="${prohPojo.description }">${prohPojo.description }
						</dd>
					</dl> 
					<div class="icon" style="padding-top:17px;"><img alt="禁运物品" src="${resource_path}/${prohPojo.imageurl}"></div>
				</li>
			</c:forEach>
		</ul>
	</div>
</div>

<!-- 费用相关 -->
<div class="s-content hidden" id="li-fyxg" name="hiddenDiv">
	<div class="s-con-banner">
		<img src="${resource_path}/images/s-expense.png">
	</div>
	<h4 class="tit" style="margin-bottom: 20px;">电商清关模式</h4>
	<table class="table table-bordered table-set">
		<colgroup>
			<col width="180">
		</colgroup>
		<tbody>
			<tr>
				<td>渠道</td>
				<td>F渠道（不做关税补贴）</td>
			</tr>
			<tr>
				<td>收费标准</td>
				<td>首3.0$/磅，续0.1磅$/0.3磅</td>
			</tr>
			<tr>
				<td>产品特性</td>
				<td>征收11.9%关税（无50元免征额），税费=申报货值*税率（如）税费=500（申报货值）*11.9%
					（税率）=59.5 元</td>
			</tr>
			<tr>
				<td>身份证</td>
				<td>需提供国内收件人身份证号码</td>
			</tr>
			<tr>
				<td>注：</td>
				<td>单个包裹价值小于等于2000元（RMB），不区分包裹可拆分或不可拆分；个人年度消费额小于等
					于20000元（RMB）；由速8快递收代缴。凡货值超过2000rmb的包裹都会被退运或者罚没。</td>
			</tr>

		</tbody>
	</table>
	<h4 class="tit" style="margin-bottom: 20px;">增值服务价格</h4>
	<table class="table table-bordered table-set">
		<colgroup>
			<col width="180" style="background-color: #fff;">
		</colgroup>
		<thead>
			<tr class="bg-gray">
				<th></th>
				<th>普通会员</th>
				<th>大客户</th>
				<th>备注说明</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>加固</td>
				<td>$1.00</td>
				<td>$1.00</td>
				<td>为包裹做加固操作</td>
			</tr>
			<tr>
				<td>拍照</td>
				<td>$1.00</td>
				<td>$1.00</td>
				<td>为包裹内件进行拍照</td>
			</tr>
			<tr>
				<td>去鞋盒</td>
				<td>免费</td>
				<td>免费</td>
				<td>取出包裹中的鞋盒</td>
			</tr>
			<tr>
				<td>取小票</td>
				<td>免费</td>
				<td>免费</td>
				<td>取出包裹中的小票</td>
			</tr>
			<tr>
				<td>清点</td>
				<td>$1.20</td>
				<td>$1.20</td>
				<td>清点包裹内件</td>
			</tr>
			<tr>
				<td>分箱</td>
				<td>免费</td>
				<td>免费</td>
				<td>按分箱后每个箱子收费，仅提供一次分箱服务</td>
			</tr>
			<tr>
				<td>合箱</td>
				<td>$1.20</td>
				<td>$1.20</td>
				<td>按合箱后的箱子收费，最多3票合1，提供一次合箱服务</td>
			</tr>
			<tr>
				<td>保价</td>
				<td colspan="3">需交保费=保价额*1%</td>
			</tr>
		</tbody>
	</table>
</div>

<!-- 动态模式下的菜单及内容(用户指南、海淘教程、加盟合作) -->
<c:forEach items="${artList}" var="art" varStatus="srtStatus">
	<div class="s-content" id="li-${art.classify_id}" name="hiddenDiv">
		<div class="s-con-banner">
			<img src="${resource_path}/images/sub-banner.png">
		</div>
		<ul class="s-con-ul">
			<li class="s-con-li selected">
				<div class="sub-title ">
					<p class="p">${art.title}</p>
				</div>
				<div class="sub-text">
					<div class="img"></div>
					<div class="txt" style="line-height: 26px;">${art.content}</div>
				</div>
			</li>

		</ul>
	</div>
</c:forEach>

<!-- 侧边栏 -->
<div class="s-sidebar">
	<ul>
		<li id="jywp" name="menu"><a href="#">禁运物品</a></li>
		<li id="fyxg" name="menu"><a href="#">费用相关</a></li>
		<c:forEach items="${artClassListParent}" var="aclParent">
			<li id="${aclParent.id}" name="menu" parentfalg="00"><a href="#">${aclParent.name}<i
					class="caret"></i></a>
				<ul class="s-sidebar-sub ">
					<c:forEach items="${artClassList}" var="acl">
						<c:if test="${acl.parent_id==aclParent.id}">
							<li id="${acl.id}" name="menu" data-parent="${acl.parent_id}"><a
								href="#">${acl.name}</a></li>
						</c:if>
					</c:forEach>
				</ul></li>
		</c:forEach>
	</ul>
</div>

<script type="text/javascript">
	$(function() {
		$("li[name=menu]").click(function() {
			var item = $(this).prop("id");
			$("li[name=menu]").removeClass("selected");
			$(this).addClass("selected");
			if ($(this).data('parent')) {
				$('#' + $(this).data('parent')).addClass("selected");
			}

			$("div[name='hiddenDiv']").addClass('hidden');

			//需要展示的内容
			var parentFlag = $(this).attr("parentfalg");
			if (parentFlag == "00") { //若是父级元素，则判断是否有子元素
				var ls = $(this).children("ul").children("li");
				$.each(ls, function() {
					var subItem = $(this).prop("id");
					var obj = $("#li-" + subItem);
					if (obj.length > 0) {
						$(this).addClass("selected");
						obj.removeClass('hidden');
						return false;
					}
				});
			} else {
				$("#li-" + item).removeClass('hidden');
			}
			return false;
		});
		var t_item = $("#requestItem").val();
		if (null != t_item && "" != t_item) {
			$('#' + t_item).click();
		} else {
			var curPath = window.document.location.href;       //获取访问当前页的目录，如：  http://localhost:8080/test/index.jsp 
			//因这三项不是动态配置的
			if(curPath.indexOf("/help") > -1){ //用户中心
				$("li[name='menu'][parentfalg='00']").get(0).click();
			} else if(curPath.indexOf("/prohibition") > -1){
				$('#jywp').click();
			} else if(curPath.indexOf("/free") > -1){
				$('#fyxg').click();
			} else {
				$("li[name='menu'][parentfalg='00']").get(0).click();
			}
		}
	});
</script>
