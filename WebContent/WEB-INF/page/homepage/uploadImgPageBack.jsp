<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0109)http://www.birdex.cn/Transport/UploadImgPage.aspx?ID=0&type=0&fileNameVal=/2015/05/20/20150520120502kg9c0.png -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="/lmp/resource/css/common.css"/>
    <link href="/lmp/resource/css/syfault.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/lmp/resource/js/jquery-1.8.3.all.js"></script>
    <script type="text/javascript" src="/lmp/resource/js/layer.min.js"></script>
    <script type="text/javascript" src="/lmp/resource/js/common.js"></script>
    <script type="text/javascript" src="/lmp/resource/js/kindeditor-min.js"></script>
    <script type="text/javascript" src="/lmp/resource/js/jquery.uploadify.v2.1.0.js"></script>
</head>
<body>
    <form method="post" action="http://www.birdex.cn/Transport/UploadImgPage.aspx?ID=0&type=0&fileNameVal=%2f2015%2f05%2f20%2f20150520120502kg9c0.png" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTQxMzA5NzQ1N2RkE9/WBzobqE6A5zNCtaaaEqR2mCjXq3mWhkIzWM0PLyg=">
</div>

    <div class="modal-dialog" id="uploadOrderImgDiv" style="width: 750px; height: 650px; border: 0px; padding: 0px; overflow-x: hidden;">
        <div class="disass" style="width: 700px; height: auto; padding-left: 15px;">
            <div class="Whole" style="width: 700px; margin-left: 13px; height: 11px; padding: 17px;">
                <h1 class="blue" style="float: left; font-size: 16px; color:#0097db;">
                    上传购物图片</h1>
                    <br>
                    <h1 class="blue" style="font-size: 13px; color:#666;">
                    温馨提示:上传的购物图需带上购物网址及购物单价</h1>
            </div>
            <div style="width: 655px; margin-left: 30px; padding-top: 18px;">
                <img src="/lmp/resource/img/images-2014092401-1.jpg" style="border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;">
            </div>
        </div>
        <div style="width: 750px; height: 70px; background-color: #fff; margin-left: 0px;
            padding-top: 0px; overflow: hidden;">
            <div style="width: 250px; height: 40px; text-align: center; margin: 18px 0px 0px 250px;display: block;">
                <a href="javascript:;" style="text-align:left;" class="notSafari">
					<div class="ke-inline-block ">
						<form class="ke-upload-area ke-form" method="post" enctype="multipart/form-data" target="kindeditor_upload_iframe_1432094543711" action="http://upload.birdex.cn//UploadHandlerForEditor.ashx?FileType=Image&Directory=OrderImg" style="width: 250px;">
							<span class="ke-button-common" style="width: 250px; height: 40px; cursor: pointer; background-image: url(http://img.cdn.birdex.cn/images/uploagorderimg.png);">
							<input type="button" class="ke-button-common ke-button" value="" style="margin-right: 0px; width: 250px; height: 40px; cursor: pointer; background-image: url(http://img.cdn.birdex.cn/images/uploagorderimg.png); background-position: 0% 50%;"></span>
							<input type="file" class="ke-upload-file" name="imgFile" tabindex="-1"></form>
					</div>
					<input type="file" name="fileOrderImg" id="fileOrderImg" style="display: none;">
				</a>
                <a href="javascript:;" class="isSafari" style="display: none;"><input type="file" id="safarifileOrderImg" name="safarifileOrderImg"></a>
            </div>
            <div style="width: 250px; height: 40px; background-color: #F88800; text-align: center;
                line-height: 38px; margin: 20px 0px 0px 250px; display: none;">
                <a href="javascript:;" class="" id="uploadOrderImg" style="color: White; font-size: 18px;">
                    点击上传购物图片</a>
            </div>
        </div>
        <div style="width: 655px; margin-left: 45px;height: 0px;" id="divOrderImg"><div class="divImgOrder" val="1" style="width: 120px; height: 120px; margin-top:5px;margin-left:8px;float:left;border:1px solid #E1E1E1;" id="divOrderImg1"><div id="divDel1" style="margin:5px 0px 0px 105px;width:15px;position:absolute;"><img alt="删除" title="删除" style="cursor: pointer; display: none;" id="delOrderImg1" onclick="delOrderImg(1)" src="close-x.png"></div><div style="width: 120px;height:120px;display:table-cell;vertical-align: middle;text-align: center;"><img onclick="showImg(this)" alt="" src="/lmp/resource/img/20150520120502kg9c0.png" id="imgOrder1" style="cursor:pointer;width:120px;"><input type="hidden" id="hdOrderImg1" value="/2015/05/20/20150520120502kg9c0.png"></div></div></div>
        <div style="padding: 0px 0px 0px 22px; float: left; width: 646px;">
            <a href="javascript:Close()" class="lbAction modal-dialog-title-close close" rel="deactivate" style="background: transparent url(&#39;http://img.cdn.birdex.cn//images/close-x.png&#39;) no-repeat scroll;
                background-position: 50% 50%; position: fixed; right: 10px;"></a>
        </div>
    </div>
    <div id="layer_showImg" style="width:660px;height:660px;overflow:hidden;margin:0 auto;display:none;">
        <div style="width:660px; height:650px;overflow:hidden;margin:10px auto 0;display:table-cell;vertical-align: middle;text-align: center;">
            <img style="vertical-align:middle;" src="" name="layerImg" id="layerImg">
        </div>
        <div style="width: 660px;">
            <a class="lbAction modal-dialog-title-close close" rel="deactivate" style="background: transparent url(&#39;http://img.cdn.birdex.cn//images/close-x.png&#39;) no-repeat scroll;
                background-position: 50% 50%; position: absolute; right: 10px;"></a>
        </div>
    </div>
    </form>
    <script type="text/javascript">
        var fileDirectory = "OrderImg";
       // document.domain = domainUrl;
        var UserID = "74915";
        var path = "http://img.cdn.birdex.cn/images/uploagorderimg.png";
        var fileUrl = fileDirectory;
        var index = parent.layer.getFrameIndex(window.name);
        var fileNameVal = getUrlParam("fileNameVal");
		//alert(fileNameVal);
        var type = getUrlParam("type");
        var Sys = {};

        $(document).ready(function () {
            //ImgUploadConfig($("#safarifileOrderImg"));
            //ImgUploadKindEditor();

            var ua = navigator.userAgent.toLowerCase();
            var s;
            (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
            if (Sys.safari) {
                ImgUploadConfig($("#safarifileOrderImg"));
                $(".notSafari").hide();
                $(".isSafari").show();
            } else {
                ImgUploadKindEditor();
                $(".notSafari").show();
                $(".isSafari").hide();
            }

            GetOrderImg();
        });

        //-----------------------------上传事件控件绑定---------------------
        function ImgUploadConfig(fileID) {
            if (fileID == undefined) {
                return;
            }
            fileID.uploadify({
                'auto': true, //选定文件后是否自动上传，默认false
                'fileDesc': '*.jpg;*.png;*.gif', //出现在上传对话框中的文件类型描述
                'fileExt': '*.jpg;*.gif;*.png', //控制可上传文件的扩展名，启用本项时需同时声明fileDesc
                'height': 42, // The height of the flash button
                'width': 360, // The width of the flash button
                'uploader': '/Common/uploadifyp.swf',
                'fileDataName': 'imgFile',
                'buttonImg': path,
                'isonSelect': false, // The height of the flash button
                'sizeLimit': 1572864, //控制上传文件的大小，单位byte common.js中定义
                'onSelect': function (e, queueId, fileObj) {
                    var size = fileObj.size;
                    if (size > 1572864) {
                        $.Beyond.Warn("请将图片尺寸控制在1.5MB以下");
                    }
                    $.BDEX.Wait();
                },
                'scriptData': { 'FileType': 'Image', 'F': fileUrl },
                'onComplete': function (e, queueID, fileObj, response, data) {
                    $.BDEX.Close();
                    if (response != undefined && response.indexOf("|") > -1) {
                        var arr = response.split("|");
                        //                        $("#hdOrderImg1").val(arr[1]);
                        //                        $("#imgOrder1").attr("src", arr[2]);
                        //alert(arr[2]);
                        UpdateUploadOrderImg(arr[1]);
                    }
                    else
                        $.Beyond.Warn(response);
                }
            });
        };

        function UpdateUploadOrderImg(imgPath) {
            function onResponseSucess(result) {
                if (!result.BoolOut) {
                    $.BDEX.Error(result.ErrMsg);
                }
                else {
                    $.BDEX.Success("上传成功", 1);
                    initOrderImg(result.StrOut, result.ObjectOut);
                }
            }

            function sparam(param) {
                param.LongIn = getUrlParam("ID");
                param.IntIn = type;
                param.StrIn = imgPath;
                param.StrIn2 = fileNameVal + "^" + imgPath;
                return param;
            }

            //初始化数据
            var param = {
                AssemblyName: "Beyond.Birdex.Business.dll",
                ClassName: "Beyond.Birdex.Business.Transport.Tst_TransportOrderBLL",
                MethodName: "UpdateUploadOrderImg",
                ParamModelName: "Beyond.WebFrame.Param.Common.Params",
                ParamAssemblyName: "Beyond.WebFrame.Param.dll",
                onResponse: onResponseSucess,
                onRequest: sparam
            }
            $.ajaxRequest(param);
        }

        function initOrderImg(orderImgStr, orderImgStr1) {
            //return;
            fileNameVal = orderImgStr1;
            window.parent.setFileName(orderImgStr1);

            var orderImgArr = orderImgStr.split("^");
            var orderImgArr1 = orderImgStr1.split("^");

            var imgLen = 0;
            for (var i = 0; i < orderImgArr.length; i++) {
                if (orderImgArr[i].trim() != "")
                    imgLen++;
            }
            setHeight(imgLen);

            $("#divOrderImg").html("");
            for (var i = 1; i <= orderImgArr.length; i++) {
                var imgUrl = "";
                var imgPath = "";
                if (i > orderImgArr.length) {
                    imgUrl = "";
                    imgPath = "";
                }
                else {
                    imgUrl = orderImgArr[i - 1];
                    imgPath = orderImgArr1[i - 1];
                }
                if (imgUrl != "") {
                    var htmlStr = "<div class='divImgOrder' val='" + i + "' style='width: 120px; height: 120px; margin-top:5px;margin-left:8px;float:left;border:1px solid #E1E1E1;' id='divOrderImg" + i + "'>";
                    if (imgPath != "") {
                        htmlStr += "<div id='divDel" + i + "' style='margin:5px 0px 0px 105px;width:15px;position:absolute;'>";
                        htmlStr += "<img alt='删除' title='删除' style='cursor:pointer;display:none;' id='delOrderImg" + i + "' onclick='delOrderImg(" + i + ")' src='http://img.cdn.birdex.cn//images/close-x.png' /></div>";
                    }
                    htmlStr += "<div style='width: 120px;height:120px;display:table-cell;vertical-align: middle;text-align: center;'>";
                    htmlStr += "<img onclick='showImg(this)' alt='' src='" + imgUrl + "' id='imgOrder" + i + "' style='cursor:pointer;width:120px;' />";
                    //htmlStr += "<img onclick='showImg(this)' onload=\"javascript:DrawImage(this,'120','120')\" alt='' src='" + imgUrl + "' id='imgOrder" + i + "' style='cursor:pointer;' />";
                    htmlStr += "<input type='hidden' id='hdOrderImg" + i + "' value='" + imgPath + "' /></div>";
                    htmlStr += "</div>";
                    $("#divOrderImg").append(htmlStr);
                }
            }

            $(".divImgOrder").hover(function () {
                $("#delOrderImg" + $(this).attr("val")).show();
            }, function () {
                $("#delOrderImg" + $(this).attr("val")).hide();
            });
        }

        function setHeight(imgLen) {
            var heiVal = 500;
            if ((imgLen % 5) == 0)
                heiVal += (imgLen / 5) * 150;
            else
                heiVal += (parseInt(imgLen / 5) + 1) * 150;

            if (parent.layer.index > 0) {
                window.parent.setHeight(index, heiVal, imgLen);
            }

            $("#uploadOrderImgDiv").css("height", heiVal);
        }

        function GetOrderImg() {
            function onResponseSucess(result) {
                if (!result.BoolOut) {
                    $.BDEX.Error(result.ErrMsg);
                }
                else {
                    initOrderImg(result.StrOut, result.ObjectOut);
                }
            }

            function sparam(param) {
                var id = getUrlParam("ID");
                if (id <= 0) {
                    param.StrIn2 = fileNameVal;
                } else {
                    param.LongIn = id;
                    param.IntIn = type;
                }
                return param;
            }

            //初始化数据
            var param = {
                AssemblyName: "Beyond.Birdex.Business.dll",
                ClassName: "Beyond.Birdex.Business.Transport.Tst_TransportOrderBLL",
                MethodName: "GetOrderImg",
                ParamModelName: "Beyond.WebFrame.Param.Common.Params",
                ParamAssemblyName: "Beyond.WebFrame.Param.dll",
                onResponse: onResponseSucess,
                onRequest: sparam
            }
            $.ajaxRequest(param);
        }

        function delOrderImg(id) {
            function onResponseSucess(result) {
                if (!result.BoolOut) {
                    $.BDEX.Error(result.ErrMsg);
                }
                else {
                    initOrderImg(result.StrOut, result.ObjectOut)
                }
            }

            function sparam(param) {
                param.LongIn = getUrlParam("ID");
                param.IntIn = type;
                param.IntIn2 = 1;
                param.StrIn = $("#hdOrderImg" + id).val();
                param.StrIn2 = fileNameVal;
                return param;
            }

            //初始化数据
            var param = {
                AssemblyName: "Beyond.Birdex.Business.dll",
                ClassName: "Beyond.Birdex.Business.Transport.Tst_TransportOrderBLL",
                MethodName: "DelOrderImg",
                ParamModelName: "Beyond.WebFrame.Param.Common.Params",
                ParamAssemblyName: "Beyond.WebFrame.Param.dll",
                onResponse: onResponseSucess,
                onRequest: sparam
            }
            $.ajaxRequest(param);
        }

        function showImg(obj) {
            var imgUrl = obj.src.replace('-w80h50', '');
            $("#layer_showImg #layerImg").attr("src", imgUrl);

            var cleindex = $.layer({
                shade: [0.5, '#ccc', true],
                shadeClose: true,
                fadeIn: 350,
                closeBtn: [0, false], //显示关闭按钮
                type: 1,
                offset: [25, ''],
                area: ['auto', 'auto'],
                title: false,
                border: [2, 0.5, '#666', true],
                page: { dom: '#layer_showImg' },
                close: function (index) {
                    layer.close(index);
                }
            });

            var new_image = new Image();
            new_image.src = imgUrl;
            if (new_image.width > 650)
                $("#layer_showImg #layerImg").css("width", 650);
            else if (new_image.width > 0)
                $("#layer_showImg #layerImg").css("width", new_image.width);
            //alert(imgUrl + "-" + new_image.width);

            $("#layer_showImg .close").click(function () {
                layer.close(cleindex);
            });
        }

        function getUrlParam(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
			//alert(reg);
			var r = window.location.search.substr(1).match(reg);  //匹配目标参数
			//alert(r);
            if (r != null) return unescape(r[2]); return null; //返回参数值
        }

        function Close() {
            parent.layer.close(index);
        }

        function ImgUploadKindEditor() {
            KindEditor.ready(function (K) {
                var uploadbutton = K.uploadbutton({
                    button: K('#fileOrderImg')[0],
                    fieldName: 'imgFile',
                    url: UploadUrl + '/UploadHandlerForEditor.ashx?FileType=Image&Directory=' + fileDirectory,
                    afterUpload: function (data) {
                        if (data.error === 0) {
                            var url = K.formatUrl(data.url, 'absolute');
                            var urlGroup = url.split(fileDirectory);
                            //alert(urlGroup[1].replace("-w80h50",""));
                            UpdateUploadOrderImg(urlGroup[1].replace("-w80h50", ""));
                        } else {
                            alert(data.message);
                        }
                    },
                    afterError: function (str) {
                        alert('自定义错误信息: ' + str);
                    }
                });
                uploadbutton.fileBox.change(function (e) {
                    uploadbutton.submit();
                });

                $("input.ke-button").css("margin-right", 0).css("background-position-y", 0);
                $("form.ke-upload-area").css("width", "250px");
                $("span.ke-button-common").each(function (index, item) {
                    if (index == 0) {
                        $(this).css("background-image", "url(" + path + ")").css("width", "250px").css("height", "40px").css("cursor", "pointer");
                    } else if (index == 1) {
                        $(this).css("background-image", "url(" + path + ")").css("width", "250px").css("height", "40px").css("cursor", "pointer");
                    }
                });
                $("input.ke-button-common").each(function (index, item) {
                    if (index == 0) {
                        $(this).css("background-image", "url(" + path + ")").css("width", "250px").css("height", "40px").css("cursor", "pointer").css("background-position", "left center");
                    } else if (index == 1) {
                        $(this).css("background-image", "url(" + path + ")").css("width", "250px").css("height", "40px").css("cursor", "pointer").css("background-position", "left center");
                    }
                });
            });
        }
    </script>
</body>
</html>