<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0021)http://www.birdex.cn/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syembgo.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${resource_path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
</head>
<body>

<jsp:include page="headNotice.jsp"></jsp:include>
<jsp:include page="headNavigation.jsp"></jsp:include>

<div style="border-top:3px solid #2473ba;text-align: center;">
	<!-- <div class="embargo"> -->
            	<c:forEach var="packageImg" items="${packageImgList}">
	            		<img id="${packageImg.img_id}" path="${packageImg.img_path}" src="${resource_path}/${packageImg.img_path}"/>
	            		<br></br>
            	</c:forEach>
	<!-- </div> -->
</div>
 
 <jsp:include page="headNotice3.jsp"></jsp:include>
 <jsp:include page="footer.jsp"></jsp:include>

</body>
</html>