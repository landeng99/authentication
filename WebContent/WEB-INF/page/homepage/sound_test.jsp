<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>播放声音测试</title>
<script type="text/javascript">
	var audio = document.getElementById("snd_html5");
	audio.loop = false;
	audio.autoplay = false;
	function playSound(src) {
		var explorerType= getExplorerInfo();
		if(explorerType=="IE")
		{
			var ie_s = document.getElementById('snd_ie');
			if(src!='' && typeof src!=undefined){
				ie_s.src = src;
			}
		}else
		{
			var _s = document.getElementById('snd_html5');
			_s.play();
		}
	}

	function getExplorerInfo() {
		var explorer = window.navigator.userAgent.toLowerCase();
		//ie 
		if (explorer.indexOf("msie") >= 0||explorer.indexOf("net") >= 0) {
			return "IE";
		}
		//firefox 
		else if (explorer.indexOf("firefox") >= 0) {
			return  "Firefox";
		}
		//Chrome
		else if (explorer.indexOf("chrome") >= 0) {
			return  "Chrome";
		}
		//Opera
		else if (explorer.indexOf("opera") >= 0) {
			return  "Opera";
		}
		//Safari
		else if (explorer.indexOf("Safari") >= 0) {
			return  "Safari";
		}
	}
</script>
</head>
<body>
	<bgsound id="snd_ie" loop="0" src="">
	<audio id="snd_html5" hidden="true" controls="true"> <source
		id="snd" src="${resource_path}/sound/nonfind.wav" type="audio/mpeg" /></audio>

	<input type="button" value="声音"
		onclick="playSound('${resource_path}/sound/nonfind.wav')">
</body>
</html>