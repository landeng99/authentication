<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
	<link href="${resource_path}/css/sydialog.css" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
	<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
	<script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script>	
	<script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>
	<script type="text/javascript" src="${resource_path}/js/json2.js"></script>
</head>
<body>
<form method="post" action="${mainServer}/homepkg/addpkg" id="pkgGoods" name="pkgGoods">
		<div class="leightbox modal-dialog" id="newTransport" style="">
			<a href="javascript:;" class="lbAction modal-dialog-title-close close" rel="deactivate" style="background: transparent url(${resource_path}/img/close-x.png) no-repeat scroll;background-position: 50% 50%;"  id="closeDiv"></a>
			<div class="Whole" style="width: 100%;">
				
				<h1 class="blue"><span style="float:left;">包裹物流追踪号： </span>
				<span class="overTip" style="margin-top:3px;" tiphtml="&#39;包裹物流追踪号&#39;需输入您在购物网站购买商品后，商家发货后的物流号。"></span></h1>
				<div class="Search">
					<input type="text" value="请输入包裹追踪号..."  onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;"  name="original_num" id="txtDeliveryCode" class="Block input_none_02 pull-left"  style="padding-left: 5px; width: 243px; overflow: auto; height: 32px; line-height: normal;">
					<span style="padding-left: 5px; color: Red;" id="deliveryCodeMst"></span>
				</div>
				<h1 class="blue">
					新包裹 - 商品申报：</h1> 
				 <h1 style="background-color:#FF9900;color:#FFFFFF;padding-left:5px;padding-bottom:3px;font-size:12px;font-weight:normal;display:none;">温馨提示：请如实填写购买商品名称及数量，错误申报会造成海关扣件或退件！<span style="float: right; margin-right: 10px; font-size: 14px;cursor:pointer;" id="tipSpan">x</span></h1> 
			</div>
			<ul class="List01_bt">
				<li class="span7" style="width:58.333%;">商品名称</li>
				<li class="span2">申报类别</li>
				<li class="span1">单价($)</li>
				<li class="span1">数量</li>
				<li class="span1">操作</li>
			</ul>
			<ul class="List01" id="tabProduct">
				<li id="tr127" class="new">
					<ol>
						<li class="span7" style="width:58.333%;">
							<input type="text" name="goods_name" value="" placeholder="建议直接复制商家英文商品名称" class="Block2 input_none_09 radius3 mgtIE" style="width:420px;" id="product127">
						</li>
						<li class="span2">
							<input class="Block2 input_none_05 radius3 mgtIE showCatagory" style="background-color:#fff" id="catalog127" onClick="ShowCataDialog(127);" >
							<input type="hidden" id="taxs127">
							<input type="hidden" id="catalogKey127" value="3" name="goods_type_id">
						</li>
						<li class="span1">
							<input type="text" name="price" class="Block2 radius3 mgtIE" value="" id="price127" onKeyUp="GetChargeMsg();" placeholder="$0.00">
						</li>
						<li class="span1">
							<input type="text" name="quantity" class="Block2 input_none_06 pull-left radius3" value="1" id="count127" onKeyUp="GetChargeMsg();">
							<div class="PM"><a class="icon33" href="javascript:ModifyNum(1,127)"></a><a class="icon34" href="javascript:ModifyNum(-1,127)"></a></div></li>
						<li class="span1">
							<a href="javascript:delProduct(127)">删除</a>
						</li>
					</ol>
				</li>
				
				<li id="tr722" class="new" style="background-color:#f3f3f3;">
				<ol>
				<li class="span7" style="width:58.333%;">
				<input type="text" name="goods_name" value="" placeholder="建议直接复制商家英文商品名称" class="Block2 input_none_09 radius3 mgtIE" style="width:420px;" id="product722"></li>
				<li class="span2"><input class="Block2 input_none_05 radius3 mgtIE showCatagory" style="background-color:#fff" id="catalog722" onClick="ShowCataDialog(722);">
				<input type="hidden" id="taxs722"><input type="hidden"  name="goods_type_id" value="3" id="catalogKey722"></li>
				<li class="span1">
				<input type="text"  name="price" class="Block2 radius3 mgtIE" value="" id="price722" onKeyUp="GetChargeMsg();" placeholder="$0.00"></li>
				<li class="span1">
				<input type="text" name="quantity" class="Block2 input_none_06 pull-left radius3" value="1" id="count722"  onkeyup="GetChargeMsg();"><div class="PM"><a class="icon33" href="javascript:ModifyNum(1,722)"></a><a class="icon34" href="javascript:ModifyNum(-1,722)"></a></div></li>
				<li class="span1"><a href="javascript:delProduct(722)">删除</a></li>
				</ol></li>
				<li class="background" id="BtnAdd" style="margin-top: 8px; margin-bottom: 8px; line-height: 40px;">
					<ol>
						<li class="span7" style="width:18%;">
							<a class="icon49" href="javascript:AddProduct(true)">添加新商品</a>
						</li>
						<li class="span7" style="width:40.333%;">
							<a title="包裹清关时需提供购物图片,上传购物图片可提高清关速度。图片需携带购物网址及购买单价！" class="icon101" href="#">上传购物小票（<span style="padding:0px;" id="orderImgCount" class="orange">0张</span>）</a>
							<input type="hidden" id="hdOrderImg">
						</li>
						<li style="text-align: center;">申报总价：<span style="padding-left: 0px;" class="orange size18"><input type="hidden" name="total_worth" value="30"/><span style="padding-left: 0px;">$</span><span style="padding-left: 0px;" id="lblTotalPrice">0.00</span></span></li>
					</ol>
				</li>
				<li class="background" style="margin-top: 4px; margin-bottom: 8px; line-height: 40px; padding-left: 0;background-color:White;">
					<h1 class="blue" style="float: left; width: 70px; height: 30px;">
						<a class="blue" href="javascript:ShowRemark()" style="color:#999999;line-height:16px;font-weight:normal;font-size:13px;">备注信息：</a>
					</h1>
					<div style="height:30px;line-height:40px;vertical-align:middle;width:300px;margin-left:70px;">
						<input type="text" name="remark" id="remark" value="如有其它要求请在这填写" style="border: 1px solid rgb(204, 204, 204); color: rgb(153, 153, 153); width: 140px; overflow: auto; height: 14px; line-height: normal;">
						<textarea id="txtUserRemark" style="resize: none; margin: 0 0 10px 0; float: left;display: none; width: 365px;border:1px solid #ccc;" rows="3"></textarea>
					</div>
				</li>
			</ul>
			<div class="Whole" id="Div1" style="margin-bottom:10px;width:820px;">
				<h1 class="blue">转运费用：</h1>
				<ul class="List02">
					<li class="span2 red" style="width:250px;"><input type="radio" value="1" name="pay_type" id="autoPay" title="入库后可快速出库"><label for="autoPay" title="入库后可快速出库">快速出库（账户余额自动支付）</label></li>
					<li class="span2" style="width:300px;"><input type="radio" value="2" name="pay_type" id="noAutoPay" title="入库后需等待支付再出库" style="margin-left:20px;"><label for="noAutoPay" title="入库后需等待支付再出库">网上在线支付</label></li>
				</ul>
			</div>
			<div class="Whole" id="addressid" style="margin-bottom:10px;width:820px;">
				<h1 class="blue">收货地址：</h1>
				<div>
					<select id="address_id" style="border:1px solid #ccc;height:32px;">
						<option value="">-------------------------选择收货地址-------------------------</option>
						<c:forEach var="address" items="${addressList}">
							<option value="address.address_id">${address.addressStr}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="Whole" id="osaddrid" style="margin-bottom:10px;width:820px;">
				<h1 class="blue">海外仓库地址：</h1>
				<div>
					<select style="border:1px solid #ccc;height:32px;" name="osaddr_id">
						<option value="-1" selected="selected">---------------------请选择海外收货仓库----------------------</option>
					  	 <c:forEach var="overSeaaddress" items="${frontOverSeadAddressList}">
							<option value="${overSeaaddress.id}">${overSeaaddress.country} ${overSeaaddress.state}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="Whole" id="service" style="width:820px;">
				<h1 class="blue">
					增值服务：</h1>
				<ul class="List02">
				
				<c:forEach items="${attachServiceList}" var="asPojo" varStatus="asSta">
					<li class="span2">
						<input type="checkbox" value="${asPojo.attach_id}" name="chkServices" 
							onclick="clickboxService(this)">
							<a class="icon${asSta.count + 50}" href="javascript:;" 
								onclick="clickService(this,${asSta.count});">${asPojo.service_name}</a></li>
				</c:forEach>
					
				</ul>
				<li style="text-align: left; padding-left: 560px;">
					<input name="hidService" type="hidden" id="hidService" value="${TOOL_PRICE}">
					增值服务费：<span style="color: Orange; font-size: 18px;">¥</span> <span style="color: Orange;
						font-size: 18px;" id="serviceAmount">0.00</span></li>
			</div>
			<div class="hr mg18">
			</div>
			<div class="PushButton" style="width: 776px;">
				<a class="Block input14 white" href="javascript:save()">保存</a> 
				<a style="text-decoration: none;
					cursor: pointer; color: #666; width: 70px; font-family: &#39;微软雅黑&#39;; background-image: url(http://img.cdn.birdex.cn/images/input36.png);
					text-align: center;" class="button" id="cancelNew" href="javascript:CloseNewTransport(1)">取 消</a>
			</div>
		</div>
		</form>
<script type="text/javascript">
//类别选择页
function ShowCataDialog(ID) {
	//alert(ID);
	var off = ($(window).height() - 320) / 2;
	$.layer({
		bgcolor: '#fff',
		type: 2,
		title: false,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [1, 0.3, '#666', true],
		offset: [off, ''],
		area: ['850px', '370px'],
		closeBtn: [0, false], //显示关闭按钮
		iframe: { src: '${mainServer}/catalog/add?ID='+ID},
		});

	}
//新增页面新增产品
function AddProduct(isClick) {
	var key = new Date().getMilliseconds();
	while (2 > 1) {
		if ($("#tr" + key).html() == undefined && $("#catalog" + key).html() == undefined)
			break;
		key = new Date().getMilliseconds();
	}

//    if ($("li[id^=tr]").length == 7) {
//        $.BDEX.Warn("一个预报最多只能添加7个商品");
//        return;
//    }

	//var table1 = $('#tabProduct');

	var row;
	//var row = $("<li id=\"tr" + key + "\" class='new'></li>");

	if ((($('#tabProduct li.new').length + 1) % 2) == 0) {
		row = $("<li id=\"tr" + key + "\" class='new' style='background-color:#f3f3f3;'></li>");
	} else {
		row = $("<li id=\"tr" + key + "\" class='new'></li>");
	}

	var ol = $("<ol></ol>");

	var td = $("<li class=\"span7\" style=\"width:58.333%;\"></li>");
	td.append($("<input type='text' name='goods_name' value='' placeholder='建议直接复制商家英文商品名称' class=\"Block2 input_none_09 radius3 mgtIE\" style=\"width:420px;\" id=\"product" + key + "\" />"));
	ol.append(td);

	td = $("<li class=\"span2\"></li>");

	td.append($("<input class=\"Block2 input_none_05 radius3 mgtIE showCatagory\"  style=\"background-color:#fff\" id=\"catalog" + key + "\" onclick=\"ShowCataDialog(" + key + ");\"  /><input type='hidden' id=\"taxs" + key + "\"><input type='hidden' name='goods_type_id' value='3' id=\"catalogKey" + key + "\">"));

	ol.append(td);

	td = $("<li class=\"span1\"></li>");
	td.append($("<input type='text' name='price' class=\"Block2 radius3 mgtIE\" value='' id=\"price" + key + "\" onkeyup=\"GetChargeMsg();\" placeholder='$0.00'>"));
	ol.append(td);


	td = $("<li class=\"span1\"></li>");
	td.append($("<input type='text' name='quantity' class=\"Block2 input_none_06 pull-left radius3\" value='1' id=\"count" + key + "\"'  onkeyup=\"GetChargeMsg();\"><div class=\"PM\"><a class=\"icon33\" href=\"javascript:ModifyNum(1," + key + ")\"></a><a class=\"icon34\" href=\"javascript:ModifyNum(-1," + key + ")\"></a></div>"));
	ol.append(td);


	td = $("<li class=\"span1\"></li>");
	td.append($("<a href=\"javascript:delProduct(" + key + ")\">删除</a>"));
	ol.append(td);

	row.append(ol);

	row.insertBefore($("#BtnAdd"));

	if (isClick) {
		//$("#xubox_layer" + AddIndex).css("height", $("#xubox_layer" + AddIndex).height() + 75);
		$("#xubox_border" + AddIndex).css("height", $("#xubox_border" + AddIndex).height() + 75);
	}
}
//删除产品
function delProduct(key) {
	if ($("li[id^=tr]").length == 1) {
		layer.msg("必须最少有一个商品", 5, 5);
		return;
	}

	var k = layer.confirm('确定删除该产品？', function () {
		$("#tr" + key).remove();
		GetChargeMsg();
		layer.close(k);
	});
}
//新增页面修改产品数量
function ModifyNum(num, id) {
	if ($("#count" + id).val() == "" || isNaN($("#count" + id).val())) {
		$("#count" + id).val(1);
	}

	if ($("#count" + id).val() == "1" && num < 0) {
		return;
	}
	$("#count" + id).val(parseInt($("#count" + id).val()) + num);
	GetChargeMsg();
}
//获取收费信息
function GetChargeMsg() {
	var price = 0;
	var taxs = 0;
	$("#tabProduct li").each(function () {
		//存在数据的才进行统计
		if ($(this).attr("ID") != undefined) {
			var id = $(this).attr("ID").replace("tr", "");
			if (RegRule.Number.test($("#count" + id).val()) && RegRule.Decimal.test($("#price" + id).val()) && RegRule.Decimal.test($("#taxs" + id).val())) {
				price = price + (parseFloat($("#price" + id).val()) * parseFloat($("#count" + id).val()))
				//alert(price);
				taxs = taxs + (parseFloat($("#price" + id).val()) * parseFloat($("#count" + id).val()) * parseFloat($("#taxs" + id).val()));
			}
		}

	});
	$("#lblTotalPrice").html(price.toFixed(2));
	//$("#lblTaxs").html(taxs.toFixed(2));
}
//提交
function save(){
$("#pkgGoods").submit();
}
</script>
</body>
</html>