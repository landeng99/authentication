<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<script type="text/javascript"
	src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<style type="text/css">
p {
	color: #666666;
	font-family: "微软雅黑";
	font-size: 12px;
	font-weight: normal;
	list-style-type: none;
	margin-bottom: 0;
	margin-left: 0;
	margin-right: 0;
	margin-top: 0;
	padding-bottom: 0;
	padding-left: 0;
	padding-right: 0;
	padding-top: 0;
}

.mg18 {
	margin-bottom: 18px;
	margin-left: 0;
	margin-right: 0;
	margin-top: 18px;
}

.hr {
	background-color: #EBEBEB;
	height: 1px;
	overflow-x: hidden;
	overflow-y: hidden;
	width: 100%;
}

.List06 {
	overflow-x: hidden;
	overflow-y: hidden;
	width: 100%;
}

.List06 li {
	height: 52px;
	list-style-type: none
}

.List06 li span.pull-left {
	overflow-x: hidden;
	overflow-y: hidden;
	text-align: right;
	width: 63px;
}

.List06 li input {
	margin-right: 4px;
}

.List06 li .text08 {
	-moz-border-bottom-colors: none;
	-moz-border-left-colors: none;
	-moz-border-right-colors: none;
	-moz-border-top-colors: none;
	background-attachment: scroll;
	background-clip: border-box;
	background-color: rgba(0, 0, 0, 0);
	background-image: url("${resource_path}/img/input24.png");
	background-origin: padding-box;
	background-position: 0 0;
	background-repeat: no-repeat;
	background-size: auto auto;
	border-bottom-color: -moz-use-text-color;
	border-bottom-style: none;
	border-bottom-width: medium;
	border-image-outset: 0 0 0 0;
	border-image-repeat: stretch stretch;
	border-image-slice: 100% 100% 100% 100%;
	border-image-source: none;
	border-image-width: 1 1 1 1;
	border-left-color-ltr-source: physical;
	border-left-color-rtl-source: physical;
	border-left-color-value: -moz-use-text-color;
	border-left-style-ltr-source: physical;
	border-left-style-rtl-source: physical;
	border-left-style-value: none;
	border-left-width-ltr-source: physical;
	border-left-width-rtl-source: physical;
	border-left-width-value: medium;
	border-right-color-ltr-source: physical;
	border-right-color-rtl-source: physical;
	border-right-color-value: -moz-use-text-color;
	border-right-style-ltr-source: physical;
	border-right-style-rtl-source: physical;
	border-right-style-value: none;
	border-right-width-ltr-source: physical;
	border-right-width-rtl-source: physical;
	border-right-width-value: medium;
	border-top-color: -moz-use-text-color;
	border-top-style: none;
	border-top-width: medium;
	height: 32px;
	line-height: 32px;
	overflow-x: hidden;
	overflow-y: hidden;
	width: 178px;
}

.PushButton {
	overflow-x: hidden;
	overflow-y: hidden;
	width: 250px;
}

.PushButton a {
	float: right;
	font-size: 16px;
	line-height: 32px;
	margin-left: 16px;
}

.PushButton a.button {
	-moz-text-blink: none;
	-moz-text-decoration-color: -moz-use-text-color;
	-moz-text-decoration-line: none;
	-moz-text-decoration-style: solid;
	background-attachment: scroll;
	background-clip: border-box;
	background-color: rgba(0, 0, 0, 0);
	background-image: url("${resource_path}/img/button2.png");
	background-origin: padding-box;
	background-position: 0 0;
	background-repeat: no-repeat;
	background-size: auto auto;
	color: #FFFFFF;
	height: 32px;
	overflow-x: hidden;
	overflow-y: hidden;
	text-align: center;
	width: 72px;
}

.span4 .label {
	width: 70px;
	float: left;
}
</style>
<body>
	<div
		style="background-image: none; border-right: none; width: 350px; padding-top: 5px;" />
	<div class="left"></div>
	<div style="margin-left: 5px;" class="right">
		<div>
			<div style="display: block;" id="withdrawal">
				<input type="hidden" id="package_id" name="package_id"
					value="${package_id}">
				<ul class="List06">
					<li class="span4">
						<p>
							<span style="margin-top: 5px;">运单号：</span> <span>${logistics_code}&nbsp;&nbsp;&nbsp;</span>
							<span style="margin-top: 5px;">关联单号：</span> <span>${original_num}</span>
						</p>
					</li>
					<li class="span4">
						<p>
							<span style="margin-top: 5px;">请输入保价金额(元)：</span> <input
								type="text" class="text08" id="keep_price" name="keep_price"
								value="${keep_price}" onblur="checkKeep_price()" style="padding-left: 5px;"
								>
						</p>
					</li>
					<li class="span4">
						<p>
							<span style="margin-top: 5px;">保费(元)：</span> <span
								id="needToPayKeepPrice">${sevice_price}</span>
						</p>
					</li>
				</ul>
				<div class="PushButton">
					<a id="no" class="button" style="cursor: pointer;">取消</a> <a
						id="yes" class="button" style="cursor: pointer;">确定</a>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function checkKeep_price() {
			//
			var reg =  /^\d+(\.\d+)?$/;
			var keep_price = $('#keep_price').val();
			if (keep_price == null || keep_price.trim() == ""
					|| !reg.test(keep_price)) {
				alert("保价金额输入有误");
				return;
			}
			if (keep_price > 3000) {
				alert("保价金额最高为RMB 3000元！");
				return;
			}
			var needpay = keep_price * 0.01;
			$('#needToPayKeepPrice').html(needpay);
		}
		//获取窗口索引
		var index = parent.layer.getFrameIndex(window.name);
		$("#no").click(function() {
			// 关闭窗口
			/* var keep_price = $('#keep_price').val();
			if (keep_price > 3000) {
				alert("保价金额最高为RMB 3000元！");
			} else {
				parent.layer.close(index);
			} */
			parent.layer.close(index);
		});

		$("#yes")
				.click(
						function() {
							var package_id = $('#package_id').val();
							var keep_price = $('#keep_price').val();
							if (keep_price > 3000) {
								alert("保价金额最高为RMB 3000元！");
							} else {
								var needpay = keep_price * 0.01;
								var url = '${mainServer}/homepkg/setKeepPriceService';
								$
										.ajax({
											url : url,
											data : {
												"package_id" : package_id,
												"attach_id" : 32,
												"service_price" : needpay
											},
											type : 'post',
											async : false,
											dataType : 'json',
											success : function(data) {
												var status = data['status'];
												if ("1" == status) {
													alert("您已成功选择[保价]服务");
												} else if ("2" == status) {
													alert("您已更新[保价]服务");
												} else if ("3" == status) {
													alert("您已取消[保价]服务");
												} else {
													alert("输入保价金额不正确");
												}
												//刷新父窗口的页面
												window.parent.location.href = window.parent.location.href;
												// 关闭窗口
												//parent.layer.close(index);
											}
										});
							}
						});

		//清空所有的提示信息
		function clearPrompt() {
			$('#message1').html("");
			$('#message2').html("");
			$('#message3').html("");

		}
	</script>
</body>
</html>