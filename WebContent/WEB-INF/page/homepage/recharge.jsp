<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">


.size18 {
    font-size: 18px;
}

p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}

.orange, a.orange {
    color: #FF8B00;
}


.mg18 {
    margin-bottom: 18px;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
}
.hr {
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.balance span {
display:-moz-inline-box;
display:inline-block;
width:150px; 
}


.pd10 {
    padding-bottom: 0;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 0;
}


.balance a:hover, .balance a.current {
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input21.png");
    background-origin: padding-box;
    background-position: center top;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
}

.balance a {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input20.png");
    background-origin: padding-box;
    background-position: center top;
    background-repeat: no-repeat;
    background-size: auto auto;
    float: left;
    font-size: 16px;
    height: 32px;
    line-height: 32px;
    margin-bottom: 0;
    margin-left: 5px;
    margin-right: 5px;
    margin-top: 0;
    overflow-x: hidden;
    overflow-y: hidden;
    padding-bottom: 9px;
    text-align: center;
    width: 99px;
}

.balance {
    overflow-x: hidden;
    overflow-y: hidden;
    padding-top: 10px;
    width: 100%;
}
.balance .pull-left {
    width: 100%;
}
.How {
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    padding-bottom: 0;
    width: 100%;
}
.How .much {
    float: left;
    margin-left:100px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: right;
    width: 218px;
}
.How input {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input19.png");
    background-origin: padding-box;
    background-position: center top;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;
    float: left;
    height: 32px;
    line-height: 32px;
    margin-right: 6px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 208px;
}
.List-bank {
    overflow-x: hidden;
    overflow-y: hidden;
    padding-top: 0;
    width: 100%;
    margin-left:-50px;

}
.List-bank li {
    height: 50px;
    list-style:none;
    float:left;
    margin-left: 40px;
}
.List-bank li input {
    float: left;
    margin-top: 16px;
}
.List-bank li img {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-bottom-color: #CECECE;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: #CECECE;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: solid;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: 1px;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: #CECECE;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: solid;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: 1px;
    border-top-color: #CECECE;
    border-top-style: solid;
    border-top-width: 1px;
    float: left;
    height: 39px;
    margin-left: 8px;
    width: 149px;
}



.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button1.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 119px;
}

.span4 .label{
     width:80px;
     float: left;
}
</style>
<body>
<div style="background-image:none;border-right:none;width:897px;padding-top:5px;" id="content">
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
    <input type="hidden" id="able_balance" value="${frontUser.able_balance}">
        <div >
            <div class="balance" >
                <p>信用卡充值的账户余额不可提现</p></br>
                <div style="margin-left:8px;">
                    <p><strong>账户余额：<strong>
                      <span class="orange size18">
                        <fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#,###.##"/>
                      </span><strong>冻结余额：<strong>
                      <span class="orange size18">
                       <fmt:formatNumber value="${frontUser.frozen_balance}" type="currency" pattern="$#,###.##"/>
                    </span><strong>可用余额：<strong>
                    <span class="orange size18">
                       <fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#,###.##"/>
                       </br>
                       <fmt:formatNumber value="${frontUser.able_balance_yuan}" type="currency" pattern="￥#,###.##"/>
                    </span>
                   </p>
               </div>

            </div>
            <div class="hr mg18"></div>
            <div id="recharge" style="display: block;">
                <div class="How">
                   <p><span class="much"><strong>充值金额：<strong></span>
                      <input type="text" style="padding-left:10px;" id="rechargeAmount" name="rechargeAmount">元
                   </p>
                </div>
                <jsp:include page="bankList.jsp"></jsp:include>
                <div class="PushButton">
                   <a id="resetCharge" class="button" style="cursor:pointer;">清　空</a>
                   <a id="submitCharge" class="button" style="cursor:pointer;">立即充值</a>
                </div> 
           </div>
    </div>
    
</div>

    <script type="text/javascript">

      $(".control").click(function() {
          $(".control").removeClass("current");
          $(this).addClass("current");
          var item = $(this).attr("item"); //当前操作的内容
          if (item == "recharge") {
              $("#recharge").show();
              $("#withdrawal").hide();
              $("#rechargeAmount").focus()
            } else {
              $("#recharge").hide();
              $("#withdrawal").show();
              $("#chargeMoney").val("");
              $("#amount").focus();
            }
          });
      
      $("#submitCharge").click(function () {
          
          //获取窗口索引
          var index = parent.layer.getFrameIndex(window.name);

          var rechargeAmount =$('#rechargeAmount').val();
          var bankCode =$("input[name='bankCode']:checked").val();

          if(rechargeAmount==null||rechargeAmount.trim()==""){
              alert("金额不能为空！");
              $('#rechargeAmount').focus();
              return;
          }
          var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
          if(!exp.test(rechargeAmount)){ 
              alert("金额格式错误！");
              $('#rechargeAmount').focus();
              return;
            }
          if(parseFloat(rechargeAmount) < parseFloat(0.1)){
              alert("最小金额为0.1！");
              $('#rechargeAmount').focus();
              return;
          }

          var url="${mainServer}/member/recharge?rechargeAmount="+rechargeAmount+"&bankCode="+bankCode;

          window.open(url);
       // 关闭窗口
          parent.layer.close(index);
      }); 

      $("#resetCharge").click(function () {

           $("#rechargeAmount").val("");
           $("#rechargeAmount").focus();
      }); 
 
        </script>
</body>
</html>