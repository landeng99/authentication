<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style type="text/css">
	.notice1{background:#4d4d4d;color:#ffffff;height:200px;text-align:center;}
    .notice2{background:#4d4d4d;color:#ffffff; float:left; margin-left:10%}
	.notice-fi2{text-align:middle;font-size:24px;margin-top:7px;padding-top:7px;color:#0F9;}
	.li1{border:1px;border-style:dotted;text-align:left;line-height:28px;margin-left:auto;margin-right:auto;width:420px;font-size:13px;margin-top:5px;color:#ffffff;}
</style>

<div class="notice1">

<div class="notice2">
		  <div class="notice-fi2">身份证上传通知</div>
		 	 <li class="li1">为适应中国新的海关通关政策需要，为了不影响包裹正常通关，即日起必须提供收件人身份证正反面<b style="color:#6F0">图片</b>方可进行通关。&nbsp&nbsp&nbsp&nbsp <b style="color:#0F9">-2016.04.29</b><br /><br />
             <b style="color:#0F9; font-size:17px;">身份证上传入口： <a href="${mainServer}/id" target="_blank" style="color:#6F0; font-size:18px;text-decoration:none;">点击进入</a></b>
			 </li1>
			 <a href="${mainServer}/noticeInfo?noticeId=39" style="color:#CFC;text-decoration:none;float:right;" target="_blank" >查看详情</a>

</div>

<div class="notice2">
		  <div class="notice-fi2">跨境价格变更通知</div>
		 	 <li class="li1">根据海关政策的变动及公司运营的需要，自2016年8月1日开始跨境物流转运收费计价变更，包裹费用按照4.5美金一磅收取，按照实磅计重，首重一磅，不含税（附件一里的包裹享受关税补贴），凡自北京时间2016年8月1日起下单的包裹按照新价格收取，在此之前下单的包裹按照旧价格结算费用。<b style="color:#0F9">-2016.07.30</b></li1>
			 <a href="${mainServer}/noticeInfo?noticeId=46" style="color:#CFC;text-decoration:none;float:right;" target="_blank" >查看详情</a>
            

</div>


</div>