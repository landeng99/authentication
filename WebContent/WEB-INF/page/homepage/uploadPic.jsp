<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>跨境物流 专业的跨境转运</title>
<meta name="keywords" content="跨境物流，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="跨境物流，拥有美国仓库及专业物流配送体系"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<script src="${mainServer}/back/js/jquery.js"></script>
<script src="<%=basePath %>back/js/pintuer.js"></script>
<script src="<%=basePath %>back/js/respond.js"></script>
<script src="<%=basePath %>back/js/admin.js"></script>
<script src="${resource_path}/js/jquery-1.8.3.all.js" type="text/javascript"></script>
<script src="${resource_path}/js/jquery.Jcrop.js" type="text/javascript"></script>
<link rel="stylesheet" href="${resource_path}/css/jquery.Jcrop.css" type="text/css" />	
<style type="text/css">

a{text-decoration:none;outline:none; color:#666666;}
a:hover{text-decoration:none}
img{border:0}
ul{list-style:none;margin:0;}
h2{
	color:#6CBD45;
	font-size:14px;
	font-weight:bold;
	padding-bottom:0.5em;
	margin:0;
}

h3{
	font-size:13px;
	font-weight:bold;
	
	
}
#show{background:url(${mainServer}/resource/${path})}
#meun1{color:#fff; padding-left:10px;}
#meun1 img{ float:left;}
#submeun1{ margin-left:70px; float:left;}
#submeun1 li{ text-align:center;  margin-right:10px; float:left;  display:inline;}
#submeun1 li a{ color:#fff;height:50px; line-height:50px;  font-size:14px; font-weight:bold; text-align:center;  padding-left:15px; padding-right:15px;display:block;}
#submeun1 li.cur{ text-align:center; background:#82ce18; margin-right:10px;float:left;  display:inline;}
#top1{
	background-color:#000;
	margin: 0em 0 10px 0em;
	border-style:solid; border-width:1px; border-color:#E5E5E5;
	height:50px;
	line-height:50px;
}
div.subtitle{
	font-size:13px;
	float:right;
	color:#6CBD45;
	margin:0 10px;
	text-align:right;
}

h1.title{
	height:50px;    
    font-size:12px;
	background:url(logo.png) no-repeat;
	
}
h1.title a:link,h1.title a:visited,h1.title a:hover{
	color:#000;
	text-decoration:none;
}

</style>	
<script type="text/javascript">
	jQuery(function($){
		$('#target').Jcrop({
			onChange:   showCoords,
			onSelect:   showCoords,
			onRelease:  clearCoords,
			boxWidth:200,
			boxHeight:200,
			//minSelect:[100,100]
			//minSize: [164, 164],
			setSelect: [0, 0, 100, 100] 
		});
		/*$('#uploadP').click(function(){
			var url="${mainServer}/upload/pic";
			var x1 = $('#txtX1').val(); 
			var y1 = $('#txtY1').val(); 
			var x2 = $('#txtX2').val(); 
			var y2 = $('#txtY2').val(); 
			var w = $('#txtW').val(); 
			var h = $('#txtH').val();
			$.ajax({
				url:url,
				type:"POST",
				data:{"x1":x1,"y1":y1,"x2":x2,"y2":y2,"w":w,"h":h},
				success:function(data){
				
				}
			});
		});*/
	});  	
	function showCoords(c){
		$('#txtX1').val(c.x);
		$('#txtY1').val(c.y);
		$('#txtX2').val(c.x2);
		$('#txtY2').val(c.y2);
		$('#txtW').val(c.w);
		$('#txtH').val(c.h);   
		
		$('#show').css({"backgroundPosition":"-"+c.x+"px -"+c.y+"px","width":c.w,"height":c.h}); 
	};
	
	function clearCoords(){
		$('#coords input').val('');
		$('#h').css({color:'red'});
		window.setTimeout(function(){
			$('#h').css({color:'inherit'});
		},500);
	};
</script>
</head>

<body>
<center>
<div id="content">
	
<br><br><br>
<!--DEMO start-->
<div >
<img src="${mainServer}/resource${path}" id="target"/>
<input type="hidden" value="${path}" id="picPath"/>
</div>
<input type="hidden" id="txtX1" />
<input type="hidden" id="txtY1" />
<input type="hidden" id="txtX2" />
<input type="hidden" id="txtY2" />
<input type="hidden" id="txtW" />
<input type="hidden" id="txtH" />

<div id="show"></div>
<div style="margin-top:50px;">
<input type="button" value="剪切图片" id="uploadP" onclick="uploadPic()"/>
</div>
<div style="float:right;margin:0 0 5px 5px; display:inline; overflow:hidden;">
<!--DEMO end-->

</div>
</center>
<script type="text/javascript">
function uploadPic(){
	var url= " ${mainServer}/upload/pic";
	var x1 = $('#txtX1').val(); 
	var y1 = $('#txtY1').val(); 
	var x2 = $('#txtX2').val(); 
	var y2 = $('#txtY2').val(); 
	var w = $('#txtW').val(); 
	var h = $('#txtH').val();
	var pic = $('#picPath').val();
	$.ajax({
		url:url,
		type:"POST",
		dataType:'json',
		async:false,
		data:{x1:x1,y1:y1,x2:x2,y2:y2,w:w,h:h,"pic":pic},
		success:function(data){
			if(data){
				var imgUrl=data['imgpath'];
				window.parent.showIdcard(imgUrl);
				window.parent.layer.closeAll();
			}
		}
	});
}
</script>
</body>
</html>