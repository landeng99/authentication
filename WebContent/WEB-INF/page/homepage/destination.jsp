<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>9577收货地址</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/common.js"></script>
    <script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
	
	<!-- 新增的css -->
	<jsp:include page="../common/commonCss.jsp"></jsp:include>
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css"> <!-- 前店公共框架样式 头部 -->
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/dropify/dist/css/dropify.min.css"> <!-- 图片上传控件样式 --> 
	<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/p-pay.css"> <!-- 前店公共框架样式 头部 -->
	
	<script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script>
</head>
<body>
	<!-- 头部页面 -->
	<jsp:include page="topPage.jsp"></jsp:include>
	
	<!-- banner -->
    <div class="min-banner">
        <div class="bgImg"></div>
        <div class="logo"></div>
    </div>
    
    <!-- 内容 -->
     <div class="wrap bg-gray">
        <div class="wrap-box pb20 pt20 clearfix">
            <div class="p-content">
                <!-- 我的收货地址列表 -->
                <div id="addressListDiv"> <!-- class="hidden"  -->
                    <div class="paybox pt20">
                        <h4 class=" tit  ml20">我的收货地址</h4>
                         <hr>
                        <form class="form-horizontal">
                           <div class="form-group" style="margin-bottom: 15px;">
                               <label for="" class="col-sm-2 control-label">姓名：</label>
                               <div class="col-sm-3">
                                  <input class="form-control " placeholder="" type="text" id="q_userName" name="q_userName">
                               </div>
                               <label for="" class="col-sm-2 control-label">手机号码：</label>
                               <div class="col-sm-3">
                                 <input class="form-control " placeholder="" type="text" name="q_moblieNum" id="q_moblieNum">
                               </div>
                               <div class="col-sm-2">
                                 <a class="btn btn-primary2" id="query_button_id" onclick="query_button_click();">查询</a>
                               </div>
                           </div>
                        </form>
                        <hr>
                       <div class="listbox">
                           <div class="mb20 ml20"><a href="javascript:addAndQueryAddrChange('01')" class="btn btn-primary btn-radius btn-sm" ><i class="glyphicon glyphicon-plus"></i>新增收货地址</a></div>
                           <table class="table table-list table-hover">
                           		<colgroup>
	                                <col width="100">
	                                <col width="15%">
	                                <col width="30%">
	                                <col width="80">
	                                <col >
	                                <col width="80">
	                                <col width="80">
                              </colgroup>
                               <thead>
                                    <tr>
                                       <th >收件人</th>
                                       <th>地区</th>
                                       <th width="260">详细地址</th>
                                       <th >邮编</th>
                                       <th>电话/手机</th>
                                       <th>状态</th>
                                       <th>操作</th>
                                    </tr>
                               </thead>
                               <tbody>
	                               <c:if test="${null == addressList}">
	                               		<tr>
											<td>暂无收货地址</td>
										</tr>
									</c:if>
									<c:forEach var="address" items="${addressList}">
										 <tr>
	                                       <td>
	                                       		${address.receiver}
	                                       		<input type="hidden" name="address_id" value="${address.address_id}"/>
												<input type="hidden" name="receiver"  value="${address.receiver}"/>
	                                       </td>
	                                       <td>${address.addressStr}<input type="hidden" name="region"  value='${address.region}'/></td>
	                                       <td>${address.street}<input type="hidden" name="street"  value="${address.street}"/></td>
	                                       <td>${address.postal_code}<input type="hidden" name="postal_code"  value="${address.postal_code}"/></td>
	                                       <td>
	                                           ${address.mobile}<input type="hidden" name="mobile"  value="${address.mobile}"/>
	                                       </td>
	                                       <td>
	                                            <c:if test="${address.idcard_status==0}"><span class="label label-default">未审核</span></c:if>
												<c:if test="${address.idcard_status==1}"><span class="label label-info">审核通过</span></c:if>
												<c:if test="${address.idcard_status==2}"><span class="label label-default">审核拒绝</span></c:if>
												<input type="hidden" name="idcard_status" value="${address.idcard_status}"/>
												<input type="hidden" name="front_img"  value="${address.front_img}"/>
												<input type="hidden" name="back_img"  value="${address.back_img}"/>
												<input type="hidden" name="isdefault"  value="${address.isdefault}"/>
	                                       </td>
	                                       <td>
	                                           <a href="#" title="修改" class="pr10 edit">
	                                               <i class=" glyphicon glyphicon-pencil" ></i>
	                                           </a>
	                                           <a href="javascript:deleteAddress(${address.address_id})" title="删除">
	                                               <i class=" glyphicon glyphicon-remove text-danger" ></i>
	                                           </a>
	                                           <input type="hidden" name="idcard" value="${address.idcard}"/>
	                                       </td>
	                                    </tr>
									</c:forEach>
                               </tbody>
                           </table>
                       </div>
                    </div>
                    <div class="page-list">
						<jsp:include page="paper.jsp"></jsp:include>
					    <script type="text/javascript"> 
						    function pagerClick(index){
						    	//业务查询
						    	window.location.href="${mainServer}/useraddr/destination?status="+status+"&pageIndex="+index;
						    }
					    </script>
					</div>
                </div>
                
                <!-- 新增/编辑收货地址 class="hidden" -->
                <div class="hidden" id="addAddressDiv">
                    <ul class="breadcrumb">
                        <li><a><i class="iconfont icon-dingwei"></i>收货地址</a></li>
                        <li class="active"><span>添加/编辑</span></li>
                    </ul>
                    <div class="paybox pl20 pr20 ">
                        <h4 class=" tit ">新增收货地址</h4>
                        <form method="post" id="destinationFrom" action="${mainServer}/useraddr/addAddress" class="form-horizontal ">
                           <div class="form-group">
                               <label for="" class="col-sm-2 control-label"><span class="text-danger">*</span>所在地区：</label>
                               <div class="col-sm-3">
                                  <select class="form-control" placeholder="选择省市" name="modifyProvince" id="modifyProvince" onchange="SelectModifyChange(this,2);">
                                      <option value="0" disabled selected style='display:none;'>选择省市</option>
                                      <option value="110000" p="6">北京市</option>
									<option value="120000" p="6">天津市</option>
									<option value="130000" p="6">河北省</option>
									<option value="140000" p="6">山西省</option>
									<option value="150000" p="6">内蒙古自治区</option>
									<option value="210000" p="6">辽宁省</option>
									<option value="220000" p="6">吉林省</option>
									<option value="230000" p="6">黑龙江省</option>
									<option value="310000" p="6">上海市</option>
									<option value="320000" p="6">江苏省</option>
									<option value="330000" p="6">浙江省</option>
									<option value="340000" p="6">安徽省</option>
									<option value="350000" p="6">福建省</option>
									<option value="360000" p="6">江西省</option>
									<option value="370000" p="6">山东省</option>
									<option value="410000" p="6">河南省</option>
									<option value="420000" p="6">湖北省</option>
									<option value="430000" p="6">湖南省</option>
									<option value="440000" p="6">广东省</option>
									<option value="450000" p="6">广西壮族自治区</option>
									<option value="460000" p="6">海南省</option>
									<option value="500000" p="6">重庆市</option>
									<option value="510000" p="6">四川省</option>
									<option value="520000" p="6">贵州省</option>
									<option value="530000" p="6">云南省</option>
									<option value="540000" p="6">西藏自治区</option>
									<option value="610000" p="6">陕西省</option>
									<option value="620000" p="6">甘肃省</option>
									<option value="630000" p="6">青海省</option>
									<option value="640000" p="6">宁夏回族自治区</option>
									<option value="650000" p="6">新疆维吾尔自治区</option>
									<option value="710000" p="6">台湾省</option>
									<option value="810000" p="6">香港特别行政区</option>
									<option value="820000" p="6">澳门特别行政区</option>
                                  </select>
                                   <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请选择省份。</span>
                               </div>
                               <div class="col-sm-3">
                                  <select class="form-control" placeholder="选择城市"  name="modifyCity" id="modifyCity" onchange="SelectModifyChange(this,3);" >
                                      <option value="0">请选择城市...</option>
                                  </select>
                                   <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请填写此字段。</span>
                               </div>
                               <div class="col-sm-3">
                                  <select class="form-control" placeholder="选择区县"  name="modifyQzone" id="modifyQzone">
                                      <option value="0">请选择区/县...</option>
                                  </select>
                                   <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i><i class="glyphicon glyphicon-info-sign pr5"></i>请填写此字段。</span>
                               </div>
                           </div>
                           <div class="form-group">
                               <label for="" class="col-sm-2 control-label"><!-- <span class="text-danger">*</span> -->邮编(Zip)：</label>
                               <div class="col-sm-4">
                                   <input class="form-control " placeholder="请输入邮编" type="text" name="postal_code" id="PostCode" >
                                   <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请填写邮编。</span>
                               </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label"><span class="text-danger">*</span>地址(Address)：</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" rows="3" name="street" id="Address" ></textarea>
                                  <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请填写此字段。</span>
                                </div>
                             </div>
                             <div class="form-group">
                                 <label for="" class="col-sm-2 control-label"><span class="text-danger">*</span>收货人姓名：</label>
                                 <div class="col-sm-4">
                                       <input class="form-control " placeholder="长度不超过25个字符" type="text" name="receiver" id="TrueName" >
                                     <span class="help-block hidden">请填写此字段。</span>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="" class="col-sm-2 control-label"><span class="text-danger">*</span>手机号码：</label>
                                 <div class="col-sm-4">
                                     <input class="form-control " placeholder="手机、电话号码必填一项" type="text" name="mobile" id="Mobile">
                                     <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign"></i>请填写此字段。</span>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="" class="col-sm-2 control-label"><!-- <span class="text-danger">*</span> -->电话号码：</label>
                                 <div class="col-sm-9">
                                     <input name="Code" id="Code"  class="form-control form-control-inline form-control-xs" placeholder="区号" type="text" >
                                     <input name="Tel" id="Tel" class="form-control form-control-inline form-control-sm" placeholder="电话号码" type="text" width="150">
                                     <input name="Extension" id="Extension" class="form-control form-control-inline form-control-xs" placeholder="分机" type="text" width="50">
                                 </div>
                              </div>
                              <div class="form-group">
                       		  		<label class="control-label col-sm-2 ">设为默认地址：</label>
                              		<div class="col-sm-7 ">
                                 		<div class="mt5">
                                  		<input type="checkbox" name="isdefault" value="0" id="IsDefault"/>
                                 		设置为默认地址
                                 		</div>
                               		</div>
                              </div>
                              <hr>
                              <div class="form-group">
                                 <label for="" class="col-sm-2 control-label"><!-- <span class="text-danger">*</span> -->证件号码：</label>
                                 <div class="col-sm-4">
                                     <input class="form-control " placeholder="长度不超过25个字符" type="text"  name="idcard" id="Identity" >
                                     <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请填写此字段。</span>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="" class="col-sm-2 control-label"><!-- <span class="text-danger">*</span> -->证件照片：</label>
                                 <div class="col-sm-4">
								   <input id="frontFace" class="dropify" type="button" name="imgFile">
                                   <span class="help-block "><i class="glyphicon glyphicon-info-sign pr5"></i>身份证正面照</span>
                                   <input type="hidden" name="front_img"  id="front_img" value="${resource_path}/img/Iden.jpg"/>
                                 </div>
                                 <div class="col-sm-4">
                                    <input id="backFace" class="dropify" type="button" name="imgFile">
                                    <span class="help-block "><i class="glyphicon glyphicon-info-sign pr5"></i>身份证反面照</span>
									<input type="hidden" name="back_img" id="back_img" value="${resource_path}/img/IdenEx.jpg"/>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="" class="col-sm-2 control-label">证件示例：</label>
                                 <div class="col-sm-9" role="alert">
                                    <div class="alert  alert-warning " style="min-height: 270px;">
                                       	 身份证上传要求：<br>  
                                        1.身份证人名要与收件人名一致； <br>
                                        2.正面与反面要单独分开上传； <br>
                                        3.若自己添加水印，请不要遮挡住身份证里的信息及头像；<br> 
                                        4.身份证四角边框要齐全，清晰可见，不得有缺损，反光；<br>
                                        5.不得有其他背景图片在身份证照片当中；<br> 
                                        6.图片格式为JPG格式，大小在200KB以内； <br>
                                        7.身份证要在有效期内 
                                        <div class="ex-identity">
                                            <img src="${resource_path}/images/ex-id.png">
                                        </div>
                                    </div>
                                  </div>
                              </div>
                              <input type="hidden" id="address_id" name="address_id" value="-1"/>
	               			  <input type="hidden" name="region" id="region" value=""/>
	                          <input type="hidden" name="tel"  id="tel" value=""/>
                        </form>
                        <div class="text-center pb20">
                            <button class="btn btn-primary btn-p-radius" onclick="checkFormValue()" >保存</button>
<!--                             <button class="btn btn-default btn-d-radius" onclick="cancel()">取消</button>
 -->                            <button class="btn btn-default btn-d-radius" onclick="addAndQueryAddrChange('')">返回列表</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 左侧页面 -->
			<jsp:include page="leftPage.jsp"></jsp:include>
        </div>
    </div>
    
    <!-- 底部页面 -->
	<jsp:include page="bottomPage.jsp"></jsp:include>
	
</body>

<script type="text/javascript" src="${resource_path}/js/kindeditor-min.js"></script>
<script type="text/javascript" src="${resource_path}/js/jquery.uploadify.v2.1.0.js"></script>
<script type="text/javascript" src="${resource_path}/font/iconfont.js"></script>
<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="${resource_path}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>


<!-- 图片上传 -->
<script src="${resource_path}/new_css/dropify/dist/js/dropify.js"></script>
<script>
    $(document).ready(function(){
        // Basic
        var imgEvent = $('.dropify').dropify();
    });
</script>

<script src="${resource_path}/js/upfile.js" type="text/javascript"></script>

<script type="text/javascript">
   var domUrl='${resource_path}';
   var faceType=1;
    $('#frontFace').click(function(){
    	faceType=1;
    });
    
    $('#backFace').click(function(){
    	faceType=2;
    });
    
	var element=document.getElementById("frontFace");
	UpfileObject.initialize({
		elements:[element],
		baseURL:"${mainServer}/upload/initUploadIdCard",
		complete: function(response, index){				
		}
	});
	
	var element=document.getElementById("backFace");
	UpfileObject.initialize({
		elements:[element],
		baseURL:"${mainServer}/upload/initUploadIdCard",
		complete: function(response, index){
			
		}
	});
		
		
    function loadImg(path){
    	var mainServer='${mainServer}';
   		var newSrc=mainServer+"/resource"+path;
   		if(faceType == 1){
   			var drDestroy = $('#frontFace').dropify();
   			drDestroy = drDestroy.data('dropify');
   			drDestroy.settings.defaultFile = newSrc;
   			drDestroy.destroy(); 
   			drDestroy.init(); 
   			$('#front_img').val(path);
   		} else {
   			var drDestroy = $('#backFace').dropify();
   			drDestroy = drDestroy.data('dropify');
   			drDestroy.settings.defaultFile = newSrc;
   			drDestroy.destroy(); 
   			drDestroy.init(); 
   			$("#back_img").val(path);
   		}
   		
    }

    //初始化地区数据
    function configRegion(array){
    	var province='';
    	var city='';
    	var area='';
    	
		for(i=0;i<array.length;i++){
			var item=array[i];
			
			if(item.level==1){
				 province=item.id;
			}
			if(item.level==2){
				city=item.id;
			}
			if(item.level==3){
				area=item.id;	
			}
		}
		if(province!=''){
			$('#modifyProvince').val(province);
			var modifyProvince=$('#modifyProvince')[0];
			//初始化下拉菜单
			SelectModifyChange(modifyProvince,2);
			if(city!=''){
				$('#modifyCity').val(city);
				var modifyCity=$('#modifyCity')[0];
				SelectModifyChange(modifyCity,3);
				if(area!=''){
					$('#modifyQzone').val(area);
				}
			}
		}
    }
	
	$('#IsDefault').click(function(){
		if($(this).is(":checked")){
			$(this).val(1);

		}else{
			$(this).val(0);
		}
	});
	
	var provinceString='${provinceString}';
	var cityString='${cityString}';
	var areaString='${areaString}';	
	var province = $.parseJSON(provinceString);
	var city = $.parseJSON(cityString);
	var area = $.parseJSON(areaString);
	
</script>
<script type="text/javascript" src="${resource_path}/js/destinationBusiness.js"></script>
</html>