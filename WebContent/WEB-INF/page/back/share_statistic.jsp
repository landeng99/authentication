<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 .field-group .filter_timefield{
 	line-height: 40px;
 	height: 40px;
 }
 .field-group .filter_type{
 	line-height: 40px;
 	height: 40px;
 }

 </style>
 
<body>
	<div class="admin">
	<div class="admin_context">
	<form action="${backServer}/statistic/userlist">
		<div class="field-group">
			<div class="filter_timefield">
				<select name="year" id="year">
					<option>2014</option>
					<option>2015</option>
				 </select>年
				 <select name="month"  id="month" >
					<option>1</option>
					<option>2</option>
					<option>3</option>
				 </select>月
				 <select name="day" id="day">
				 	<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
				</select>日
 				<input type="submit" id="search" class="button bg-main icon-search" value="查询">
			</div>
		 		
			<div class="filter_type">
			
				  <!--当年每月的统计  -->
			 	  <label><input type="radio" id="yearModel" value="year" name="queryModel"/>按年统计</label>
				
    			   <!--当月每日的统计  -->
			 	  <label><input type="radio" id="monthModel" value="month" name="queryModel"/>按月统计</label>
				
					<!--选择某天，当天所在周的统计  -->
			 		<label><input type="radio" id="weekModel"  value="week" name="queryModel"/>按周统计</label>
				
					<!-- 当日24小时的统计 -->
			 		 <label><input type="radio" id="dayModel" value="day" name="queryModel"/>按日统计</label>
			 	 
			</div>
		</div>
		</form>
		
		<div class="xm11">
			<div class="panel admin-panel pickorder-list">
				<div class="panel-head"><strong>统计查询</strong></div>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>时间</th>
							<th>晒单数</th>
							<th>百分比</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="statistic" items="${statisticList}">
							<tr>
							<td>${statistic.description}</td>
							<td>${statistic.count}</td>
							<td>${statistic.percent}</td>
							</tr>
					 </c:forEach>
					</tbody>
				</table>	
			</div>
		</div>
	</div>
</div>	
<script type="text/javascript">
	 
	 //初始化各表单状态
	 function init(){
		 	var statisticModel="${statisticModel}";
			
		 	var year='${year}';
		 	var month='${month}';
		 	var day='${day}';
		 	
		 	if('year'==statisticModel){
		 		$('#month').attr('disabled',"disabled");
		 		$('#day').attr('disabled',"disabled");
		 		$('#yearModel').attr('checked','checked');
		 			
		 	}else if('month'==statisticModel){
		 		$('#day').attr('disabled',"disabled");
		 		$('#monthModel').attr('checked','checked');
		 	}else if('week'==statisticModel){
		 		
		 		$('#weekModel').attr('checked','checked');
		 	} else if("day"==statisticModel){
		 		$('#dayModel').attr('checked','checked');
		 	}
		 	if(year!=''){
			 	$('#year').val(year);
		 	}
		 	if(month!=''){
		 		$('#month').val(month);
		 	}
		 	if(day!=''){
		 		$('#day').val(day);
		 	}
		 	
		 	
	 }
	 init();
	 
	 //根据查询类型，设置时间粒度
	 $('#yearModel').click(function(){
	 		$('#month').attr('disabled',"disabled");
	 		$('#day').attr('disabled',"disabled");
		 
	 });
	 $('#monthModel').click(function(){
		 $('#month').removeAttr('disabled');
		 $('#day').attr('disabled',"disabled");
	 });

	 $('#weekModel').click(function(){
		 $('#month').removeAttr('disabled');
		 $('#day').removeAttr('disabled');
	 });

	 
	 $('#dayModel').click(function(){
		 $('#month').removeAttr('disabled');
		 $('#day').removeAttr('disabled');
	 });
		 
</script>



</body>
</html>