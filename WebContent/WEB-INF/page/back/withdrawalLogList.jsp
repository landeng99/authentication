<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .div-title{
        font-weight: bold;
        float:left;
        width:90px;
        margin-top:5px;
        margin-left:10px;
    }
        .div-input{
        float:left;
        width:220px;
    }
    
    .input-width{
        width:200px;
    }

</style>
<div class="admin">
    <div class="panel admin-panel">
         <input type="hidden" id="status_bk" value="${params.status}">
         <input type="hidden" id="bank_name_bk" value="${params.bank_name}">
        <div class="panel-head"><strong>提现查询</strong></div>
        </br>
          <form action="${backServer}/withdrawalLog/search" method="post" id="myform" name="myform">

             <div style="height: 40px;">
                  <div class="div-title">申请人:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="user_name" name="user_name"
                             value="${params.user_name}"/>
                  </div>
                  <div class="div-title">账户:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="account" name="account"
                             value="${params.account}"/>
                  </div>
                  <div class="div-title">状态:</div>
                  <div class="div-input">
                        <select id="status" name="status" class="input input-width" >
                            <option value="">请选择</option>
                            <option value=1>申请中</option>
                            <option value=2>审核通过</option>
                            <option value=3>审核拒绝</option>
                            <option value=4>取消</option>
                        </select>
                  </div>
                  
             </div>

             <div style="height: 40px;">
                  <div class="div-title">提现方式:</div>
                  <div class="div-input">
                      <select id="bank_name" name="bank_name" class="input input-width" >
                            <option value="">请选择</option>
                            <option value="支付宝">支付宝</option>
                            <option value="中国工商银行">中国工商银行</option>
                            <option value="中国建设银行">中国建设银行</option>
                            <option value="中国农业银行">中国农业银行</option>
                            <option value="中国银行">中国银行</option>
                            <option value="交通银行">交通银行</option>
                            <option value="招商银行">招商银行</option>
                            <option value="中国邮政储蓄">中国邮政储蓄</option>
                            <option value="中国光大银行">中国光大银行</option>
                            <option value="广发银行">广发银行</option>
                            <option value="上海浦东发展银行">上海浦东发展银行</option>
                            <option value="北京银行">北京银行</option>
                            <option value="中国民生银行">中国民生银行</option>
                            <option value="平安银行">平安银行</option>
                            <option value="中信银行">中信银行</option>
                        </select>
                  </div>
                  <div class="div-title">起始时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="timeStart" name="timeStart" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'timeEnd\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.timeStart}"/>
                  </div>
                  <div class="div-title">终止时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="timeEnd" name="timeEnd" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'timeStart\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
                            value="${params.timeEnd}"/>
                  </div>
             </div>
          </form>
             <div class="padding border-bottom">
                  <input type="button" class="button bg-main" onclick="search()" value="查询" />
             </div>
             <table class="table">
                  <tr>
                    <th width="15%">申请人</th>
                    <th width="15%">账户</th>
                    <th width="10%">状态</th>
                    <th width="10%">金额</th>
                    <th width="15%">提现方式</th>
                    <th width="15%">时间</th>
                    <th width="20%">操作</th>
                  </tr>    
             </table>
             <table class="table table-hover">
               <c:forEach var="withdrawalLog"  items="${withdrawalLogList}">
                   <tr> 
                      <td width="15%">${withdrawalLog.real_name}</td>
                      <td width="15%">${withdrawalLog.account}</td>
                      <td width="10%">
                          <c:if test="${withdrawalLog.status==1}">申请中</c:if>
                          <c:if test="${withdrawalLog.status==2}">审核通过</c:if>
                          <c:if test="${withdrawalLog.status==3}">审核拒绝</c:if>
                          <c:if test="${withdrawalLog.status==4}">取消</c:if>
                      </td>
                      <td width="10%"><fmt:formatNumber value="${withdrawalLog.amount}" type="currency" pattern="$#0.00#"/></td>
                      <td width="15%">${withdrawalLog.bank_name}</td>
                      <td width="15%"><fmt:formatDate value="${withdrawalLog.time}" type="both"/></td>
                      <td width="20%">
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="detail('${withdrawalLog.log_id}')">详情</a>
                        <c:if test="${withdrawalLog.status==1}">
                          <a class="button border-blue button-little" href="javascript:void(0);" onclick="update('${withdrawalLog.log_id}')">审核</a>
                        </c:if>
                      </td>
                  </tr>
               </c:forEach>
             </table>
                  <div class="panel-foot text-center">
                   <jsp:include page="webfenye.jsp"></jsp:include>
                 </div>
             </div>
</div>
</body>
<script type="text/javascript">

//选择框选中
$("#status").val($("#status_bk").val());
//选择框选中
$("#bank_name").val($("#bank_name_bk").val());

//初始化焦点
$('#user_name').focus();

function search(){ 
    document.getElementById('myform').submit();
}

function detail(log_id){
    var url = "${backServer}/withdrawalLog/detail?log_id="+log_id;
    window.location.href = url;
}
function update(log_id){
    var url = "${backServer}/withdrawalLog/edit?log_id="+log_id;
    window.location.href = url;
}

//回车事件
      $(function(){
        document.onkeydown = function(e){
            var ev = document.all ? window.event : e;
            if(ev.keyCode==13) {
                search();
             }
        }
      });

</script>

</html>