<%@ page language="java"  pageEncoding="utf-8"%>
<%@ include file="header.jsp" %>
<script src="${backJsFile}/LodopFuncs.js"></script>
<script src="${backJsFile}/angular-1.2.5.js"></script>
<%
String path=request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		margin-right: 10px;
	}
	.field-group .field-item .field{
		float:left;
	}
	.pkg-list{
		margin-top: 20px;
	}
	
	.pkg-list .store_tips {
	    font-size: 30px;
	    text-align: center;
	 
	}
	.pkg-list .store_tips .tips_text{
		margin: 50px auto;
		line-height: 50px;
	}
	.pkg-list .store_tips .info{
		margin-top: 20px;
	}
	.pkg-list .notice {
    	text-align: center;
    	font-size: 20px;
	}
	
	.text_item {
	padding: 30px 20px;
}

.text_item span {
	margin-left: 10px;
}

.div_input_text {
	width: 60px;
	margin: 10px 0px 10px 6px;
}

.buttonList {
	clear:both;
	margin-top: 30px;
}
</style>
<script type="text/javascript">
	var audio = document.getElementById("snd_html5");
	audio.loop = false;
	audio.autoplay = false;
	function playSound(src) {
		var explorerType= getExplorerInfo();
		if(explorerType=="IE")
		{
			var ie_s = document.getElementById('snd_ie');
			if(src!='' && typeof src!=undefined){
				ie_s.src = src;
			}
		}else
		{
			var _s = document.getElementById('snd_html5');
			_s.src = src;
			_s.play();
		}
	}

	function getExplorerInfo() {
		var explorer = window.navigator.userAgent.toLowerCase();
		//ie 
		if (explorer.indexOf("msie") >= 0||explorer.indexOf("net") >= 0) {
			return "IE";
		}
		//firefox 
		else if (explorer.indexOf("firefox") >= 0) {
			return  "Firefox";
		}
		//Chrome
		else if (explorer.indexOf("chrome") >= 0) {
			return  "Chrome";
		}
		//Opera
		else if (explorer.indexOf("opera") >= 0) {
			return  "Opera";
		}
		//Safari
		else if (explorer.indexOf("Safari") >= 0) {
			return  "Safari";
		}
	}
</script>
<body  onkeydown="doKeyDown()">
<!-- 1、扫描入库：弹出层->提示信息：请扫描查询包裹或输入lastname查询包裹，
			注意：扫描包裹，需将鼠标指针置于包裹运单号输入框内。
			扫描完成后：
			如果包裹找到，展示包裹的信息：增值服务等，打印包裹条形码；
			如果包裹未找到，提示，包裹未找到，请新增该包裹信息。
			
2、手动入库： -->
<div class="admin">
  
<div class="admin_context">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
         <li class="active"><a href="#tab-scan" >扫描入库</a></li>
          <li ><a href="#tab-manual">手动入库</a></li>
         <li ><a href="#tab-scan-log" onclick="scan_log_click()">扫描日志</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />

        <div class="tab-panel" id="tab-scan-log">
			  <div class="field-group">
				 <div class="field-item">
					  <div class="label"><label for="originalNum">时间:</label></div>
                    <div class="field">
                     <input type="text" class="input" id="scanDate" name="scanDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd'})"
                            value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >                    </div>
				 </div>
                <div class="field-item">
                	 <div class="label"><label for="pkgNo">单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="pkgNo" name="pkgNo" style="width:200px" onKeyUp="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>
                 <div class="form-button">
                     	<a href="javascript:void(0);" class="button bg-main icon-search" id="scan_log_search" >查询</a>
				  </div>
             </div>
				             
              <div class="panel admin-panel pkg-list">
			 		  <div style="min-height: 300px;">
			 		  	 <div class="panel-head"><strong>查询列表</strong></div>
			 		  	   <div class="store_tips" id="scan_logmanual_error_info" style="display:none;">
						    	 <div class="info" style="font-size:13px;color: red"></div>
						    </div> 
			 				<div class="store_tips" id="scan_logmanualTips" style="display: none">
			 					<div class="info">查询扫描日志不存在......<br/>
			 			 		 </div>
			 		    	</div>
							<div id="scan_logmanualPkgWrager" style="display: none" >	
		     		    	 <table class="table table-hover" id="scanLogPkgList">
					      		 <thead>
					      			<tr>
						      			<th id="log_time" width="300">时间</th>
						      			<th id="operate1" width="100">操作1</th>
						      			<th id="operate2" width="130">操作2</th>
				      				</tr>
				      			</thead>
				        	</table>
				        </div>
				    </div>
             </div>
             </div>

        <div class="tab-panel" id="tab-manual">
		<div class="field-group">
				 <div class="field-item">
					  <div class="label"><label for="originalNum">包裹单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="manulOriginalNum" placeholder="公司运单号或关联单号或物流单号"  name="manulOriginalNum" style="width:250px"  />
                    </div>
				 </div>
                <div class="field-item">
                	 <div class="label"><label for="lastname">LAST NAME:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="lastName" name="lastName" style="width:200px" onKeyUp="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>
                 <div class="form-button">
                     	<a href="javascript:void(0);" class="button bg-main icon-search" id="search" >查询</a>
				  </div>
             </div>
				             
              <div class="panel admin-panel pkg-list">
			 		  <div style="min-height: 300px;">
			 		  	 <div class="panel-head"><strong>查询列表</strong></div>
			 		  	   <div class="store_tips" id="manual_error_info" style="display:none;">
						    	 <div class="info" style="font-size:13px;color: red"></div>
						    </div> 
			 				<div class="store_tips" id="manualTips" style="display: none">
			 					<div class="info">查询的包裹不存在......<br/>
			 						<a href="${backServer}/pkg/initadd?original_num=${original_num}" onClick="scan_manualTips_click(this);" style="font-size: 12px;text-decoration:underline;">单击，创建一个包裹？</a>
			 			 		 </div>
			 		    	</div>
							<div id="manualPkgWrager" style="display: none" >	
		     		    	 <table class="table table-hover" id="manualPkgList">
					      		 <thead>
					      			<tr>
						      			<th id="logistics_code" width="45">公司运单号</th>
						      			<th id="userName" width="100">客户名称</th>
						      			<th id="original_num" width="130">包裹关联单号</th>
						      			<th id="weight" width="130">重量(单位：磅)</th>
						      			<th id="pkgAttacheServicesStr" width="130">增值服务</th>
						      			<th id="createTimeStr" width="130" >创建时间</th>
						      			<th id="status" width="130" formatter="fmtStatus">状态</th>
						      			<th id="opt" width="200" formatter="opts">操作</th>
				      				</tr>
				      			</thead>
				        	</table>
				        </div>
				    </div>
             </div>
             </div>
			 <div class="tab-panel active" id="tab-scan">
			 	 <div class="panel admin-panel pkg-list" >
						<div class="store_tips">
								<div class="tips_text">等待扫描包裹.....	<br/>
									<div align="center"><input type="text" class="input" id="scan_originalNum" placeholder="公司运单号或关联单号或物流单号"  name="originalNum" style="width:500px; height:60px; font-size:23px; "   /></div>
							   </div>
						</div>
			 		
						<div style="mix-height: 300px;">
						 	<div class="store_tips" id="scan_error_info" style="display:none;">
						    	 <div class="info" style="font-size:13px;color: red"></div>
						    </div> 
			 			 	<div class="store_tips" id="scanTips" style="display: none">
			 					<div class="info">查询的包裹不存在......<br/>
			 						<a href="${backServer}/pkg/initadd?original_num=${original_num}" onClick="scan_manualTips_click(this);" style="font-size: 12px;text-decoration:underline;">单击，创建一个包裹？</a>
			 			 		 </div>
			 		    	</div>
							<div id="pkgWrager" style="display: none" >
							 <div class="panel-head"><strong>查询列表</strong></div>
							     <table class="table table-hover" id="pkgList">
						      		 <thead>
						      			<tr>
							      			<th id="logistics_code" width="45">公司运单号</th>
							      			<th id="userName" width="100">客户名称</th>
							      			<th id="original_num" width="130">包裹关联单号</th>
							      			<th id="weight" width="130">重量(单位：磅)</th>
							      			<th id="pkgAttacheServicesStr" width="130">增值服务</th>
							      			<th id="createTimeStr" width="130" >创建时间</th>
							      			<th id="status" width="130" formatter="fmtStatus">状态</th>
							      			<th id="opt" width="200" formatter="opts">操作</th>
					      				</tr>
					      			</thead>
					           	</table>	
				           	</div>
			           </div>
			 	 </div>
		 	        <div class="note">
						 <div class="notice" style="color:red"> 注意：扫描包裹，需将鼠标指针置于包裹运单号输入框内。</div>
				  </div>
				  <br>
				  <div>
				  <table id="todyScanTableId">
				  <tr></tr>
				  </table>
				  </div>
			 </div> 
        </div>
        </div>
        <div id="add" style="display: none;width:800px;height:400px;">
        <div class="input-field"><input type="text" name="weight" value=""/></div>
        	 <div><a class="button bg-main" href="javascript:;">保存</a></div>
        </div>
    </div>
   </div> 

   <div id="weightdialog" data-ng-App="">
   	<div class="text_item"><!-- value=value.replace(/[^\d.]/g,'') -->
		<span >公司单号:<label class="logistics_code"></label></span> 
		<span >首重:<label class="first_weight" id="first_weight"></label></span> 
		<span >进位:<label class="carry" id="carry"></label></span> 
		<span>状态:<label class="status" id="status_packet">1212</label></span> <!--.replace(/^[1-9]\d*$/g,'')  -->
		<br> 
		<span>重量:</span><input type="text" name="actual_weight"  onkeyup="if(value.length-value.indexOf('.')>3&&value.indexOf('.')>0){value=parseInt(value)+'.'+value.substring(value.indexOf('.')+1,value.indexOf('.')+4)}" class="div_input_text" />磅<span class="conn">连接电子称...</span>
		<a class="button button-small border-red" href="javascript:;" onclick="overWeightToSplit()" href="#">超重分箱</a>
		<br> 
		<span>长度:</span><input type="text" id="length" class="div_input_text" onKeyUp="value=value.replace(/[^\d.]/g,'')"/> 
		<span>宽度:</span><input type="text" id="width" class="div_input_text" onKeyUp="value=value.replace(/[^\d.]/g,'')"/> 
		<span>高度:</span><input type="text" id="height" class="div_input_text" onKeyUp="value=value.replace(/[^\d.]/g,'')"/> 
		<br> 
		<!--
		<span>运费:</span><span id="yunfei"><label class="freight"></label>&nbsp;元</span><br>
		<span>收费重量:</span><span id="packet_yuan"><label class="packet_yuan">{{aa}}</label>&nbsp;磅</span>
		-->
		<br> 
		<div class="buttonList">
		<input type="button" id="sub" class="button bg-main"  onclick ="openServer()" value="开始计重" />
		<input type="button" id="saveWeight" class="button bg-main" value="完成"/>
		</div>
		<div style="font-size: xx-large;color: red;padding-top: 10px; font-weight:bold;" id="waitting_merge_flag_id"></div>
	</div>
	<input type="hidden" name="oldRequestScanOriginalNum" id="oldRequestScanOriginalNum" value="${logistics_code}"/>
	<input type="hidden" name="package_id" id="package_id"/>
	<input type="hidden" name="user_id" id="user_id" />
	<input type="hidden" name="quota_id" id="quota_id" />
	<input type="hidden" name="freight" id="freight" />
	<input type="hidden" name="editWeightFlag" id="editWeightFlag" />
	<input type="hidden" name="userType" id="userType" />
	<input type="hidden" name="first_weight" id="first_weight" value="${first_weight}"/>
	<input type="hidden" name="carry" id="carry" value="${carry}"/>
	<input type="hidden" name="packet_yuan" id="packet_yuan" value="${packet_yuan}"/>
	<div style="display:none" id="statusGroup_packet"></div>
	<%-- <input type="hidden" name="status" id="status" value="${status}"/> --%>
   </div>
   	<div style="width: 800px; height: 300px; position: absolute; z-index: 100;left: -300px; top: -300px">
	    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="socket" width="100%" height="100%"
	         codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
	         <param name="movie" value="${backJsFile}/socket.swf" />
	         <param name="quality" value="high" />
	         <param name="bgcolor" value="#869ca7" />
	         <param name="allowScriptAccess" value="sameDomain" />
	         <embed src="${backJsFile}/socket.swf" quality="high" bgcolor="#869ca7"
	             width="100%" height="100%" name="socket" align="middle"
	             play="true"
	             loop="false"
	             quality="high"
	             allowScriptAccess="sameDomain"
	             type="application/x-shockwave-flash"
	             pluginspage="http://www.adobe.com/go/getflashplayer">
	         </embed>
	     </object>
	</div>
   
    <script type="text/javascript">
    //存放状态json集合
    var statusGroup={};
    
    //状态展示
    function fmtStatus(val,rowData){
    	$("#statusGroup_packet").html(statusGroup[val]);
    	$("#status_packet").html(statusGroup[val]);
    	return statusGroup[val];
    }
    
    //当前查询过单号
    var originalNum='';
    
    //当前数据集对象
	var  datagrid='';
    
    
    var isWeightDialogOpend=false;
    
    //最近的一次过磅值
    var last_weight=0;
    
    function opts(val,rowData){
    	var id=rowData.package_id;
    	var status=rowData.status;
    	var  $html=$('<div></div>');
    	if(status==0){
    		var  $store=$('<a class="button button-small border-yellow store" href="javascript:;" id="'+id+'">入库</a>');
	    	$html.append($store);
	    	$store.bind('click',function(){
	        	var self=$(this);
	        	   			    //div->td->tr	
	        	var parent=self.parent().parent().parent();
	        	var self_row_index= parent.index();
	        	var dataRow=datagrid.datagrid('getData',self_row_index);
	        	var pkgAttacheServicesStatus=dataRow['pkgAttacheServicesStatus'];
	        	var id=self.attr('id');
	        	if(pkgAttacheServicesStatus==2){
		        	weightReader(id);
	        	}else if(pkgAttacheServicesStatus==1){
	        		attachAtler(id);
	        	}
	    	});
    	}
    	if(rowData['arrive_status']==0){
    		
    		 $html.append(' <a class="button button-small border-yellow" onclick="arrive('+id+')" href="#">到库确认</a>');
    	}
    	
	    if(rowData['pkgAttacheServicesStatus']==1){
	    	 $html.append(' <a class="button button-small border-yellow" onclick="attachAtler('+id+')" href="#">增值服务</a>');
	    }
    	 $html.append(' <a class="button button-small border-green" href="${backServer}/pkg/detail?package_id='+id+'">包裹详情</a>');
    	 $html.append(' <a class="button button-small border-green" href="javascript:;" onclick="ShowCataDialog('+id+')" >预览打印</a>');
    	 $html.append(' <a class="button button-small border-green" href="javascript:;" onclick="ShowCataDialog_noreview('+id+')" >打印</a>');
    	
    	 var canModifyWeigth=true;
    	 //普通客户（user_type=1）包裹付款后入库扫描处不能再编辑包裹的重量
    	if(rowData['userType']==1&&rowData['pay_status']>1){
    		canModifyWeigth=false;
    	 }
    	if(canModifyWeigth)
    	{  
    		var statusGroup_packet=$("#statusGroup_packet").html();
    	   /* alert(statusGroup_packet)  */
         	if((statusGroup_packet)==="待入库"||(statusGroup_packet)==="已入库"&&(rowData['memberbill_import_flag']==null||rowData['transport_cost']==0)){
       		 	 $html.append(' <a class="button button-small border-red" href="javascript:;" onclick="weightReaderForEditWeight('+id+')" href="#">编辑重量</a>');
       		 	 return $html;
       		
       		}else{
       		  $html.append(''); 
       		  return $html;
       		}		
    	}/* &&(statusGroup_packet)=="待入库"&&(statusGroup_packet)=="已入库" */
    	 /* return $html; (rowData['memberbill_import_flag']==null||rowData['transport_cost']==0&&*/
    }
    
    function operate(package_id, logistics_code) {
        var url = "${backServer}/pkg/operate?package_id=" + package_id
                + "&logistics_code=" + logistics_code;
        window.location.href = url;

    }

	function ShowCataDialog(package_id) {
	    layer.open({
	        title :'打印面单',
	        type: 2,
	        shadeClose: true,
	        shade: 0.8,
	        offset: ['10px', '300px'],
	        area: ['590px', '650px'],
	        content:'${backServer}/pkg/print?package_id='+package_id
	    }); 
	}
	
	function ShowCataDialog_noreview(package_id) {
		LODOP=getLodop();  
		LODOP.PRINT_INIT("打印面单");
		LODOP.SET_PRINT_PAGESIZE(1,1016,1524," ");
		LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","73%");
		LODOP.ADD_PRINT_URL(0,0,'100%','100%','<%=basePath%>backprint?package_id='+package_id);
		LODOP.PRINT();//直接打印
		//LODOP.PRINTA();//选择打印机打印
		//LODOP.PREVIEW();//打印预览
	}
	
	//增值服务操作
	function  attachAtler(package_id){
	    layer.open({
	        title :'待增值服务',
	        type:2,
	        shadeClose: true,
	        shade: 0.8,
	        offset: ['10px', '100px'],
	        area: ['820px', '500px'],
	        content:'${backServer}/store/pkgAttachServicelist?package_id='+package_id
	    }); 
	}
	
	//到库确认
	function  arrive(package_id){
		 layer.confirm('包裹是否已到库？', {
			    btn: ['已到库','未到库'], //按钮
			    shade: false //不显示遮罩
			}, function(){
				$.ajax({
					url:'${backServer}/store/updateArriveStatus?package_id='+package_id,
					type:'post',
					dataType:'json',
				    async:false,
				    success:function(data){
				    	if(data['result']){
						    layer.msg('包裹已到库', {icon: 1});
				    	}else{
				    		
				    	}
				    }
				});
			}, function(){
			});
		
	}
	
    $(function(){
    		$('#scan_originalNum').focus();
    		
    		var oldRequestScanOriginalNum=$('#oldRequestScanOriginalNum').val();
    		if(oldRequestScanOriginalNum!=null&&oldRequestScanOriginalNum!='')
    		{
    			$('#scan_originalNum').val(oldRequestScanOriginalNum);
    			init($('#pkgList'),oldRequestScanOriginalNum);
    			request_scan();
    			$('#scan_originalNum').val('');
    		}
    		
    		$('#search').click(function(){
    			search();
    		});
    		
    		$('#scan_log_search').click(function(){
    			scan_log_search();
    		});	
    		$('#scan_originalNum').change(function(){
    			init($('#pkgList'),$(this).val());
    			request_scan();
    			$(this).val('');
    		
    		});
    	});
    function init($datagrid,code){
    	originalNum=code;
		//初始化表格对象
		datagrid=$datagrid;
    	
    }
    function scan_manualTips_click(a_elem)
    {
    	a_elem.href="${backServer}/pkg/initadd?original_num="+originalNum;
    	a_elem.click();
    }
    function search(){
			init($('#manualPkgList'),$('#manulOriginalNum').val());
			var lastname=$('#lastName').val();
			if(lastname==''&&originalNum==''){
				alert("请输入包裹单号或用户last name再查询！");
			}else{
	   			var url='${backServer}/store/search';
	   			var params={};
	   			params['originalNum']=originalNum;
	   			params['lastname']=lastname;
    			$.ajax({
    				url:url,
    				data:params,
					type:'post',
					dataType:'json',
					async:false,
					success:function(data){
						statusGroup=data.statusGroup;
						if(data.row.length==0){
							$('#manualTips').show();
							$('#manualPkgWrager').hide();
						}else{
							$('#manualPkgWrager').show();
							datagrid.datagrid({
								data : data.row
							});
							$('#manualTips').hide();
							//alert(JSON.stringify(data.row));
							 //包裹未到库，则提示用户进行操作
/* 							 if(data.row[0]['arrive_status']==0){
								 layer.confirm('包裹是否已到库？', {
									    btn: ['已到库','未到库'], //按钮
									    shade: false //不显示遮罩
									}, function(){
										$.ajax({
											url:'${backServer}/store/updateArriveStatus?package_id='+data.row[0]['package_id'],
											type:'post',
											dataType:'json',
										    async:false,
										    success:function(data){
										    	if(data['result']){
												    layer.msg('包裹已到库', {icon: 1});
										    	}else{
										    		
										    		
										    	}
										    }
										});
									}, function(){
									});
							 } */
							 if(data.row[0]['express_num']!=null){
								 layer.msg('包裹需要拆箱', {icon: 1}); 
							 }
						}
					}
    			});
			}
    	
    }
    
    
    //扫描查询请求
    function request_scan(){
		$('#scanTips').hide();
		var params={};
		params['originalNum']=originalNum;
		var url='${backServer}/store/scanadd';
		$.ajax({
			url:url,
			data:params,
			type:'post',
			dataType:'json',
			async:false,
			success:function(data){
				 if(data['result']==false){
					 $('#scan_error_info').show();
					 $('#scan_error_info').find('.info').text(data['msg']);
					 return;
				 }
				 statusGroup=data.statusGroup;
				/*  alert(data); */
				/*  for(var i=6;i<10;i++){
					 $("#statusGroup_packet").html(i);
				 } */
				 
				 
				 if(data.row.length==0){
					 $('#scanTips').show();
					 $('#pkgWrager').hide();
					 playSound('${resource_path}/sound/nonfind.wav');
				 }else {
						$('#pkgWrager').show();
						//alert(JSON.stringify(data.row));
						datagrid.datagrid({
							data : data.row
						});
						if (data.needToDisplayAfter12NotFound == 'Y') {
							$('#scanTips').show();
							playSound('${resource_path}/sound/nonfind.wav')
						} else {
							$('#scanTips').hide();
							playSound('${resource_path}/sound/find.wav')
							/* if(data.needToDisplayExpressPackageWarning == 'Y')
							{
								alert('快件包裹！');
						         layer.open({
						            title :'过磅',
						            type: 1,
						            shadeClose: true,
						            shade: 0.8,
						            area: ['100px', '100px'],
						            content:'快件包裹！'
						        }); 
							} */
							
							if (data.needToDisplayExpressNumWarning == 'Y') {
								layer.msg('包裹需要拆箱', {
									icon : 1
								});
							} else {
								if (data.row.length == 1) {
									var pkg = data.row[0];
									if (pkg.pkgAttacheServicesStatus == 1) {
										attachAtler(pkg.package_id);
									}

									//是否为待入库状态包裹状态，且增值服务状态已完成则可以入库
									if (pkg.status == 0
											&& pkg.pkgAttacheServicesStatus == 2) {
										weightReader(pkg.package_id);
									}
								}
							}
						}
				 
				 }
			}
		});
		queryTodyScanLog();
    }
    
    //刷新列表
   function reflashList(){
		var params={};
		params['originalNum']=originalNum;
		params['noNeedLog']="Y";
		var url='${backServer}/store/scanadd';
		$.ajax({
			url:url,
			data:params,
			type:'post',
			dataType:'json',
			async:false,
			success:function(data){
				 statusGroup=data.statusGroup;
				 if(data.row.length==0){
				 }else{
					datagrid.datagrid({
						data:data.row
					});	
				 }
			}
		});
   }
   function doKeyDown(event) {
		 var e=event || window.event || arguments.callee.caller.arguments[0];
	    if (e.keyCode == 13) {
	    	if(isWeightDialogOpend==true){
	    		var isValid=validate();
		    	if(isValid){
		    		updateWeitht();
		    	}
		    	return;
	    	}
	    	if($('#tab-manual').hasClass('active')){
		    	search();
	    	}
	    }
	}
    //关闭 过磅窗口后，扫描输入框重获焦点
    function focus(){
    	$(document).ready(function(){$("#scan_originalNum").focus();});
    }
    
    //过磅输入对话框弹出
    function weightReader(package_id) {
    	loadWeightInfo(package_id);
    	showWeightDialog();
    }
    
    function weightReaderForEditWeight(package_id) {
    	$('#editWeightFlag').val("Y");
    	loadWeightInfo(package_id);
    	showWeightDialog();
    }
    
    //加载过磅信息
    function loadWeightInfo(package_id){
    	$.ajax({url:'${backServer}/store/weight?package_id='+package_id,
    			type:'post',
    			dataType:'json',
    			async:false,
    			cache:false,
    			success:function(data){
	    			 var pkg=data['pkg'];
	    			 var quota=data['quota'];
	    			 $('#package_id').val(pkg['package_id']);
	    			/*  $('#weightdialog').find('.status').text(pkg['status']); *//* 包裹状态后台传回来的值 */
	    			 $('#weightdialog').find('.logistics_code').text(pkg['logistics_code']);/* 公司运单号后台传回来的值 */
	    			 $('#weightdialog').find('.packet_yuan').text(pkg['weight']);/* 公司运单号后台传回来的值 */
	    			 $('#weightdialog').find('.first_weight').text(quota['first_weight']);/* 首重后台传回来的值 */
	    			 $('#weightdialog').find('.carry').text(quota['carry']);/* 进位后台传回来的值 */
	    			 $('#weightdialog').find("input[name='actual_weight']").val(pkg['actual_weight']);/* 输入重量后台传回来的值 */
	    			 $('#length').val(pkg['length']);
	    			 $('#width').val(pkg['width']);
	    			 $('#height').val(pkg['height']);
	    			 $('#weightdialog').find('.freight').text(pkg['freight']);/* 运费返回值 pkg['freight']*/
	    			 $('#user_id').val(pkg['user_id']);
	    			 $('#freight').val(pkg['freight']);
	    			 $('#userType').val(pkg['userType']);
	    			 $('#first_weight').val(quota['first_weight']);/* 首重 */
	    			 $('#carry').val(quota['carry']);/* 进位 */
	    			 $('#quota_id').val(quota['quota_id']);
	    			 var waitting_merge_flag_text="";
	    			 if(pkg['waitting_merge_flag']=='Y')
	    			 {
	    				 waitting_merge_flag_text="该包裹需要等待合箱";
	    			 }
	    			 if(pkg['exception_package_flag']=='Y')
	    			 {
	    				 waitting_merge_flag_text="该包裹为异常件，客户未补充信息不可入库";
	    			 }
	    			 if(data['needToDisplayExpressPackageWarning']=='Y')
	    			 {
	    				 waitting_merge_flag_text="快件包裹！";
	    			 }
	    			 $('#waitting_merge_flag_id').html(waitting_merge_flag_text);
	    			 if(pkg['userType']==1&&pkg['pay_status']>1)
	    			 {
	    				 $('#width').attr("disabled","disabled");//再改成disabled  
	    			 }else
	    			 {
	    				 $('#width').removeAttr("disabled");//要变成Enable
	    			 }
    			}
    	});
    	
    }
    
    //显示过磅对话框
    function  showWeightDialog(){
    	//电子称对话框是否打开标志位，设置为打开
    	isWeightDialogOpend=true;
        layer.open({
            title :'过磅',
            type: 1,
            shadeClose: true,
            shade: 0.8,
            area: ['500px', '350px'],
            content:$('#weightdialog'),
            end:function(){
            	reflashList();
            	focus();
            	isWeightDialogOpend=false;
            	last_weight=0;
            }
        });
      $('#weightdialog').find("input[name='actual_weight']").focus();
        
        //打开对话框后读取电子称
    	start();
    }
    
   
	var conn=null;
    function openServer() {
        	 conn =thisMovie('socket');
        	 try {
        		 //服务连接等待
         		if(conn.ConncetServer){
         			conn.ConncetServer();
         			$('.conn').hide();
         			stopOpen();
         		}
			} catch (e) {
			}
			/* alert($('#first_weight').html()); */
    }
    
    //
    var openInterval;
    
    //开始建立连接定时器
    function startOpen(){
    	openInterval=self.setInterval('openServer()',100);
    }
    
   
    //关闭建立连接定时器 
    function stopOpen(){
    	  window.clearInterval(openInterval);
    }
    
    //网页加载完成后，建立连接
    window.onload=startOpen;
    
    function thisMovie(movieName) {  
        if (navigator.appName.indexOf("Microsoft") != -1) {
            return window[movieName];
        }  
        else {
            return document[movieName];
        }  
    }

    var interval;
    function start(){
        interval=self.setInterval('send()',500);
    }
    function stop(){
        window.clearInterval(interval);
    }

    function send(){
        if(conn!=null&&isWeightDialogOpend==true){
            conn.Send();
            conn.getResut();
        }
    }

    
    
    
    
    //获取收费重量部分一共三个方法
   /*  function getfeeWeightOld(actual_weight){
		
    	 /* //小数位比较数
       var contrast=0.1;
    	
    	//2磅起运
	  var min_weight=2;
	  var str=actual_weight.toString();
 
      var result=0;
      
      //如果整数则直接返回
      if(str.indexOf(".")<=0){
    	var number=parseInt(actual_weight);
    	result=(number<min_weight)?min_weight:number;
        return result;
      }
     
	    //整数部分
	    var int_part=parseInt(str.substring(0,str.indexOf(".")));

	    //小数部分
		var decimal_part='0.'+str.substring(str.indexOf(".")+1,str.length);

     	var decimal=parseFloat(decimal_part);

        var number=0;
      //小数大于等于差数的数字则进位，反之则省略。
	    if(contrast<=decimal){
	    	 number= int_part+1;
	    }else{
	         number=int_part;
	    }
      //不足2磅，算2磅。
      result=(number<min_weight)?min_weight:number;
      return result;  
    }    */
    
    //获取收费重量
    function getfeeWeight(actual_weight,userType){
        var result=0.0;
       //这里是将字符串转换成数字供后面计算
       var FirstWeight=Number($('#first_weight').html());
       var JinWei=Number($('#carry').html()); 
             /*   alert(typeof JinWei); Number($('#first_weight').html())*/
             /* alert(JinWei);  */
             //包裹重量整数部分Number($('#carry').html())Number
        var int_part=parseInt(actual_weight);
     
		/* alert(actual_weight);  */
        //包裹重量小数部分
        var decimal_part=actual_weight-int_part;
        var center_part;
        var up_int;
        if(decimal_part/JinWei==0){
        	up_int=0;
        }else if(decimal_part/JinWei>0&&decimal_part/JinWei<=1){
        	up_int=1;
        }else if(decimal_part/JinWei>1&&decimal_part/JinWei>parseInt(decimal_part/JinWei)+0.001){
        	up_int=parseInt(decimal_part/JinWei)+1;
        }else if(decimal_part/JinWei>1&&decimal_part/JinWei<parseInt(decimal_part/JinWei)+0.001){
        	up_int=decimal_part/JinWei;
        } 
        /* alert(up_int); */
        center_part=JinWei*up_int;
        /* alert(Math.ceil(decimal_part/JinWei));  */
        //不足1磅的按照1磅计算，超过1磅以后按照实磅计算
         //不足1磅，算1磅。
         //result=1.0;
         if(actual_weight>0)
         {
			/* result=actual_weight; */			
			if(FirstWeight==0){
			/* 计算当首重等于0时收费重量 */
				result=actual_weight;
				return result;
			}else if(FirstWeight>=actual_weight){
				/* 计算当首重大于包裹重量时收费重量 */
				result=FirstWeight;
				return result;
			}else if(FirstWeight<actual_weight){
				/* 计算当首重下于包裹重量时收费重量 */
				if(JinWei==0){
					/* 进位为0时 */					
					/* 计算当首重大于包裹重量时收费重量 */
						result=actual_weight;
						return result;
					
				}else if(JinWei!=0){
					if(center_part<=1){
						result=int_part+center_part;
						return result;
					}else if(center_part>1){
						result=int_part+1;
						return result;
					}
				}
			}
         }else if(actual_weight<=0){
        	 actual_weight=0;
        	 result=actual_weight;
        	 return result;
         }
        
     
       
	}
    
    function setFreight(){
        var actual_weight =$.trim($('#weightdialog').find("input[name='actual_weight']").val());
        if(actual_weight==''){
        	actual_weight=0;
        }
        //如果上次记录的重量与当前重量不同则重新计算
        if(last_weight!=actual_weight){
        	last_weight=actual_weight;
        }else{
        	return;
        } 
        
        var user_id =$('#user_id').val();
        var userType =$('#userType').val();
        var quota_id =$('#quota_id').val();
        //计算收费重量
        var weight= getfeeWeight(actual_weight,userType);
        /* var weight=weight.toPrecision(4); */
        var packet_yuan =$('.packet_yuan').html(weight);
      	//传到后台的首重
        var FirstWeight=$('#first_weight').html();
        
        var url = "${backServer}/pkg/calculation";
        
        $.ajax({
        	url:url,
            data:{"weight":weight,
            	  "FirstWeight":FirstWeight,
            	  "quota_id":quota_id,
                  "user_id":user_id},
            type:'post',
            dataType:'json',
            async:false,
            success:function(data){
            	if(!data.result){
                	layer.alert(data.message);
                }else{
                     $('#yunfei label').html(data.freight);
                     $('#freight').val(data.freight);
                }
            }
        });
    }
    
    
    
    
    
    
    //保存重量，并入库
    $('#saveWeight').click(function(){
    	updateWeitht();
    });
    function updateWeitht(){
    	if(validate()){
    		//实际重量
	       var actual_weight =$('#weightdialog').find("input[name='actual_weight']").val();
	        if(actual_weight == 0){
	        	alert("重量不能为0,请正确输入重量");
	        	return;
	        }
	        
	        var userType =$('#userType').val();
	        setFreight();
	        var weight=getfeeWeight(actual_weight,userType);
	        var freight =$('#freight').val();
	        var length =$('#length').val();
	        var width =$('#width').val();
	        var height =$('#height').val();
	        var package_id =$('#package_id').val();
	        var edit_weight_flag=$('#editWeightFlag').val();
	        var params={freight:freight,
	                   length:length,
	                   width:width,
	                   height:height,
	                   weight:weight,
	                   actual_weight:actual_weight,
	                   package_id:package_id,
	                   editWeightFlag:edit_weight_flag};
	        
	        var url = "${backServer}/pkg/updateWeight";
	        
	        $('#editWeightFlag').val("");
	        
 	      	$.ajax({
	        	url:url,
	            data:params,
	            type:'post',
	            dataType:'text',
	            async:false,
	            success:function(data){
	            	if(data == "0"){
	            		//完成计重后，关闭读取电子称定时器
	            		stop();
	            		layer.closeAll();
	            	}else if(data =="show")
	            	{
	            		alert("预扣款成功，包裹可直接出库！");
	            		//完成计重后，关闭读取电子称定时器
	            		stop();
	            		layer.closeAll();
	            	}
	            }
	        });  
    	}
    }
    
    
    function  showResult(str){
    	//console.log(str);
    	if(str==undefined||str==''){
    		str='0';
    	}
    	
    	str=$.trim(str.replace('+',''));
    	
    	//转换为磅
        var Kg_to_LB=2.2046226;
        var g_to_LB=0.0022046;
        
        //精确度
        var precision=3;
        
        var index=str.indexOf('LB');
        if(index>0){
            var str=str.substring(0,index);
            var val=str;
            $('#weightdialog').find("input[name='actual_weight']").val(val);
            setFreight();
            return ;
        }
        var index=str.indexOf('kg');
        if(index>0){
            var str=str.substring(0,index);
            var val=ForDight(str,Kg_to_LB,precision);
            $('#weightdialog').find("input[name='actual_weight']").val(val);
            setFreight();
            return ;
        }
        
        var index=str.indexOf('g');
        if(index>0){
            var str=str.substring(0,index); 
            var val=ForDight(str,g_to_LB,precision);
            $('#weightdialog').find("input[name='actual_weight']").val(val);
            setFreight();
            return ;
        }
    }
	//（乘数，系数，精确度）
    function ForDight(str,factor,precision){
		
    	//小数点后面位数
    	var digit=str.length-str.indexOf(".")-1;
    	
    	//乘数
    	var number=str;
    	
    	//需要扩大的倍数
    	var multiple=Math.pow(10,digit);
    	var val=(number*(factor*multiple)/multiple).toFixed(precision);
    	return val;
    }
    
    
     $('#weightdialog').find("input[name='actual_weight']").keyup(function(){
    	setFreight();
	});
	 
	function validate(){
	        var weight =$('#weightdialog').find("input[name='actual_weight']").val();
	        var freight =$('#freight').val();
	        var length =$('#length').val();
	        var width =$('#width').val();
	        var height =$('#height').val();

	        if(isNaN(length)||length<0){
	            layer.alert("长度必须为非负数");
	            return false;
	        }
	        if(isNaN(width)||width<0){
	            layer.alert("宽度必须为非负数");
	            return false;
	        }
	            
	        if(isNaN(height)||height<0){
	            layer.alert("高度必须为非负数");
	            return false;
	        }
	        if(isNaN(weight)||weight<0){
	            layer.alert("重量必须为非负数");
	            return false;
	        }
	        if(isNaN(freight)||freight<0){
	            layer.alert("运费必须为非负数");
	            return false;
	         }
		
		return true;
	}
	
	
	//超重分箱
	function overWeightToSplit()
	{
		var package_id =$('#package_id').val();
		$.ajax({
			url:'${backServer}/pkg/packageMergeSplitCheck',
			type:'post',
			data:{packageIds:package_id},
			dataType:"json",
			async:false,
			success:function(data){
				if("F" == data.result){
					alert("该包裹已进行分箱｜合箱服务，不能再次分箱!");
				}else
				{
				    layer.open({
				        title :'分箱',
				        type:2,
				        shadeClose: true,
				        shade: 0.8,
				        offset: ['10px', '100px'],
				        area: ['1000px', '500px'],
				        content:'${backServer}/pkg/unboxBackpkg?pkgid='+package_id
				    }); 
				}
			}			
		});

	}
	
	//扫描日志搜索
	function scan_log_search()
	{
	var url='${backServer}/inputScanLog/queryScanLog';
   	var params={};
    var scanDate =$('#scanDate').val();
    var pkg_no =$('#pkgNo').val();
   	params['scan_date']=scanDate;
   	params['pkg_no']=pkg_no;
			$.ajax({
				url:url,
				data:params,
				type:'post',
				dataType:'json',
				async:false,
				success:function(data){
					data.dateCountList;
					if(data.dateCountList.length==0){
						$('#scan_logmanualTips').show();
						$('#scan_logmanualPkgWrager').hide();
					}else{
						$('#scan_logmanualTips').hide();
						$('#scan_logmanualPkgWrager').show();
						$("#scanLogPkgList tr").empty();
						for(i=0;i<data.dateCountList.length;i++)
						{
							var newRow = "<tr><td width=\"300\">"+data.dateCountList[i].days+"</td><td width=\"100\"><a href=\"${backServer}/inputScanLog/search?scan_date="+data.dateCountList[i].days+"&pkg_no="+pkg_no+"\">查看详情</a></td>";
							newRow+="<td width=\"130\"><a href=\"${backServer}/inputScanLog/exportInputScanLog?scan_date="+data.dateCountList[i].days+"&pkg_no="+pkg_no+"\">导出</a></td></tr>";
							
							$("#scanLogPkgList tr:last").after(newRow);
						}
					}
				}
			});
	}
	
	//今天扫描日志
	function queryTodyScanLog()
	{
	var url='${backServer}/inputScanLog/queryTodyScanLog';
   	var params={};
			$.ajax({
				url:url,
				data:params,
				type:'post',
				dataType:'json',
				async:false,
				success:function(data){
					if(data.todayInputScanLogList.length==0){
					}else{
						$("#todyScanTableId tr").empty();
						for(i=0;i<data.todayInputScanLogList.length;i++)
						{
							var newRow = "<tr ";
							if(data.todayInputScanLogList[i].need_mark_flag==1)
							{
								newRow+="style=\"color:red\"";
							}
							
							newRow+="><td>"+data.todayInputScanLogList[i].scan_dateStr;
							newRow+="，"+data.todayInputScanLogList[i].pkg_no;
							newRow+="，"+data.todayInputScanLogList[i].pkg_status;
							newRow+="，"+data.todayInputScanLogList[i].scan_user_name;
							newRow+="，扫描次数："+data.todayInputScanLogList[i].scan_count;
							newRow+="</td></tr>";
							
							$("#todyScanTableId tr:last").after(newRow);
						}
					}
				}
			});
	}
	queryTodyScanLog();
	
	function scan_log_click()
	{
		scan_log_search();
	}
    </script>
    
	<bgsound id="snd_ie" loop="0" src="">
	<audio id="snd_html5" hidden="true" controls="true"> <source
		id="snd" src="${resource_path}/sound/nonfind.wav" type="audio/mpeg" /></audio>
</body>
</html>