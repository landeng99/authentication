<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>
<style type="text/css">
	.form-group{
		width:200px;
	}

</style>

<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">异常包裹编辑</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br/>
        <div class="tab-panel active" id="tab-set">
        
        		<form action="${backServer}/unusualPkg/unusualPkgEdt" id="fom" method="post">
          		 <input type="hidden" id="unusual_pkg_id" name="unusual_pkg_id" value="${unusualPkg.unusual_pkg_id}"/>
          		 <div class="form-group">
                  <div class="label"><label for="original_num">关联单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" name="original_num" id="original_num" value="${unusualPkg.original_num}" disabled="disabled"/>
                    </div>
                </div>

       			 <div class="form-group">
                  <div class="label"><label for="actual_weight">包裹重量:</label></div>
                    <div class="field">
                      <input type="text" class="input" id="actual_weight" name="actual_weight" value="${unusualPkg.actual_weight}" placeholder="包裹重量" onkeyup="value=value.replace(/[^\d.]/g,'')" data-validate="required:请填重量,compare#>0:重量必须大于0" />
                    </div>
                </div>
                <div class="form-group">
                  <div class="label"><label for="account">用户账号:</label></div>
                    <div class="field">
                      <input type="text" class="input" id="account" name="account" value="${unusualPkg.account}" placeholder="用户账号" />
                    </div>
                </div>
                
       			 <div class="form-group">
                  <div class="label"><label for="last_name">Last name:</label></div>
                    <div class="field">
                      <input type="text" class="input" id="last_name" name="last_name" value="${unusualPkg.last_name}" placeholder="Last name"/>
                    </div>
                </div>                
                
                  <div class="form-group">
                  <div class="label"><label for="osaddr_id">当前海外仓库地址:</label></div>
                    <div class="field">
                    	 <select id="osaddr" name="osaddr_id" class="input" data-validate="required:请填海外仓库">
		                  	<option value=9>----选择地址----</option>
		                     <c:forEach var="overseasAddress" items="${overseasAddressList}">
		                     	<option value="${overseasAddress.id}" <c:if test="${overseasAddress.id==unusualPkg.osaddr_id}">selected="selected"</c:if> >${overseasAddress.warehouse}</option>
		                     </c:forEach>
		                 </select>
                    </div>
                </div>
                <div class="form-group">
                  <div class="label"><label for="remark">备注:</label></div>
                    <div class="field">
                      <input type="text" class="input" id="remark" name="remark" placeholder="备注" value="${unusualPkg.remark}"/>
                    </div>
                </div>
       			 <div class="form-button" ><input type="button" id="save" class="button bg-main"  value="保存">
                    <a href="javascript:history.go(-1)" class="button bg-main">取消</a> 
                </div>
                </form>
             </div>
             </div>
        </div>
        </div>
	<script type="text/javascript">

	var isExist=false;
	var isSamePersion=false;
	var isAccount=false;
	var isLastName=false;
      $('#original_num').blur(function(){
/*     	  var original_num=$(this).val();
    	  var reg = /^[0-9a-zA-Z]+$/;
    	  if(!reg.test(original_num))
		  {
    		    isExist=true;
				$('#original_num_note').text('关联单号只能是数字或英文');
				$('#original_num_note').css('color','red');
		  }else
		  {
	    	  var url='${backServer}/unusualPkg/validateExist'
	        	  $.ajax({
	        		  url:url,
	        		  data:{'original_num':original_num},
	        		  type:'post',
	        		  dataType:'json',
	        		  async:false,
	        		  success:function(data){
	    					if(data.result){
	    						isExist=true;
	    						$('#original_num_note').text('关联单号已经存在');
	    						$('#original_num_note').css('color','red');
	    					}else{
	    						isExist=false;
	    						$('#original_num_note').text('关联单号可以使用');
	    						$('#original_num_note').css('color','#2c7');
	    					}    			  
	        		  }
	        	  })			  
		  } */
		  check();
      });
      	
      	$('#save').click(function(){
      		//check();
      		checkAccount();
      		checkLastName();
      		checkSamePerson();
      		//alert('isExist='+isExist+';isAccount='+isAccount+';isLastName='+isLastName+';isSamePersion='+isSamePersion);
			if(!isExist&&!isAccount&&!isLastName&&!isSamePersion){
				$('#fom').submit();				
			}		
		});
      function check()
      {
    	  var original_num=$('#original_num').val();
    	  var reg = /^[0-9a-zA-Z]+$/;
    	  if(!reg.test(original_num))
		  {
    		    isExist=true;
				$('#original_num_note').text('关联单号只能是数字或英文');
				$('#original_num_note').css('color','red');
		  }else
		  {
	    	  var url='${backServer}/unusualPkg/validateExist'
	        	  $.ajax({
	        		  url:url,
	        		  data:{'original_num':original_num},
	        		  type:'post',
	        		  dataType:'json',
	        		  async:false,
	        		  success:function(data){
	    					if(data.result){
	    						isExist=true;
	    						$('#original_num_note').text('关联单号已经存在');
	    						$('#original_num_note').css('color','red');
	    					}else{
	    						isExist=false;
	    						$('#original_num_note').text('关联单号可以使用');
	    						$('#original_num_note').css('color','#2c7');
	    					}    			  
	        		  }
	        	  })			  
		  }
      }
    function checkSamePerson()
    {
    	var account=$('#account').val();
    	var last_name=$('#last_name').val();
    	if(account!=null&&account!=''&&last_name!=null&&last_name!='')
    	{
	    	  var url='${backServer}/unusualPkg/validateSamePerson';
	        	  $.ajax({
	        		  url:url,
	        		  data:{'account':account,'lastName':last_name},
	        		  type:'post',
	        		  dataType:'json',
	        		  async:false,
	        		  success:function(data){
	    					if(!data.result){
	    						isSamePersion=true;
	    						alert("用户账号与last name不是同一个人");
	    					}else
	    					{
	    						isSamePersion=false;
	    					}
	        		  }
	        	  });
    	}else
      	{
    		isSamePersion=false;
      	}
    }
    
    function checkAccount()
    {
    	var account=$('#account').val();
    	if(account!=null&&account!='')
    	{
	    	  var url='${backServer}/unusualPkg/validateAccount';
	        	  $.ajax({
	        		  url:url,
	        		  data:{'account':account},
	        		  type:'post',
	        		  dataType:'json',
	        		  async:false,
	        		  success:function(data){
	    					if(!data.result){
	    						isAccount=true;
	    						alert("该用户账号不存在");
	    					}else
	    					{
	    						isAccount=false;
	    					}
	        		  }
	        	  });
    	}else
      	{
    		isAccount=false;
      	}
    }
    function checkLastName()
    {
    	var last_name=$('#last_name').val();
    	if(account!=null&&account!=''&&last_name!=null&&last_name!='')
    	{
	    	  var url='${backServer}/unusualPkg/validateLastName';
	        	  $.ajax({
	        		  url:url,
	        		  data:{'lastName':last_name},
	        		  type:'post',
	        		  dataType:'json',
	        		  async:false,
	        		  success:function(data){
	    					if(!data.result){
	    						isLastName=true;
	    						alert("该last name不存在");
	    					}else
	    					{
	    						isLastName=false;
	    					} 			  
	        		  }
	        	  });
    	}else
      	{
    		isLastName=false;
      	}
    }
 </script>
</body>
</html>