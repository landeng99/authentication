<%@ page language="java"  pageEncoding="utf-8"%>
<script src="${resource_path}/js/jquery-1.9.0.js"></script>
<script src="${resource_path}/js/jquery.PrintArea.js"></script>
<%-- <script src="${resource_path}/js/jquery-1.7.2.min.js"></script> --%>
<%-- <script src="${resource_path}/js/jquery.jqprint.js"></script> --%>
<script src="${resource_path}/js/jquery-barcode.js"></script>
<link rel="stylesheet" href="${backCssFile}/print.css" media="print" />
<%-- <link rel="stylesheet" href="${backCssFile}/screen.css"/> --%>
<body>
<style>
</style>
    <div class="admin">
        <div >
            <div style="float:left;">
              <input type="button" id="print"  value="打印"/>
              <input type="hidden" id="code"  value="${pkgDetail.logistics_code}"/>
            </div>
            <br/>
                  <div style="height:750px;margin-top:10px ">	
                        <div class ="my_show">
                            <div class="top">
                            	 <table class="part_1">
                            		<tr>
                            			<td class="logo_title"><H1><strong>跨境</strong></H1></td>
                            			<td><div id="128Code1"></div></td>
                            		</tr>
                            	 </table>
                            	<table class="part_2">
                            		<tr>
                            			<td> 
                            				<div class="title_black" ><span>寄件人(from)：</span></div>
                            			</td>
                                        <td>
                                       	 <div class="text" >
                                          <span>${frontUser.user_name}</span>
                                        </div>
                                        </td>
                                        <td>
	                                       <div class="title_black"><span>电话/Tel：</span></div>
	                                      </td>
	                                      <td>
	                                       	<div  class="text">
	                                           <Span>${frontUser.mobile}</Span>
	                                        </div>
                                       	 </td>
                                       </tr>
                                       <tr>
                                       <td >
                                       <div class="title_black" ><span >地址/Address：</span></div>
                                       <td colspan="3">
                                       <div class="text text_addr">
                                         <span>${osaddr}</span>
                                       </div>
                                       </td>
                                     </div>
                                     </tr>
                            	</table>
                            	<table class="part_3">
                            		<tr>
                            		<td>
                                       <div class="title_black" >
                                      	 <span>收件人/To：</span></div>
                                       </td>
                                       <td>
                                        <div class="text" >
                                          <span>${userAddress.receiver}</span>
                                        </div>
                                        </td>
                                        <td>
                                        <div class="title_black">
                                        	<span >电话/Tel：</span></div>
                                        </td>
                                         <td>
                                        <div class="text">
                                           <span>${userAddress.mobile}</span>
                                        </div>
                                        </td>
                                     </tr>
                                     <tr>
                                     <td><div class="title_addr">
                                        <span class ="title_black">地址/Address：</span></div></td>
                                     <td colspan="3">
                                          <div class="text text_addr">
                                           <Span>${address}</Span>
                                        </div>
                                     </td>
                                     </tr>
                                     <tr>
                                      <td></td>
                                      <td colspan="2" align="right"><div class ="title_black"><span>邮编/Post Code：</span></div></td>
                                       <td>
                                        <div class="text">
                                          <span>${userAddress.postal_code}</span>
                                        </div>
                                        </td>
                                	 </tr>
                            	</table>
                             </div>
                             <div class="middle">
                             	<table class="part_1">
                                     <tr><td><span>实际重量：</span></td><td>${pkgDetail.weight}</td><td>磅</td></tr>
                                     <tr><td><span>体积重量：</span></td><td>${pkgDetail.getBulkfactor()}</td><td>磅</td></tr>
                                     <tr><td><span>长/L:${pkgDetail.length}cm</span></td><td>宽/W:${pkgDetail.width}cm</td>
                                     <td>高/H:${pkgDetail.height}cm</td></tr>
                             	</table>
                             <table class="part_2">
                             	<tr><td>收件人签名：</td><td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
                             	<td>签收时间： &nbsp&nbsp&nbsp&nbsp年&nbsp&nbsp&nbsp&nbsp月 &nbsp&nbsp&nbsp日 &nbsp&nbsp&nbsp时</td></tr>
                             </table>	
                             <table class="part_3">
	                              <tr>
	                             	<td ><span class="title_black">寄件人(from)：</span></td><td>${frontUser.user_name}</td>
	                             	<td><span class="title_black">电话/Tel：</span></td><td>${frontUser.mobile}</td>
	                             </tr>
	                             <tr><td><span class="title_black">地址/Address:</span></td><td colspan="3"><div class="text_addr">${osaddr}</div></td></tr>
                             </table>
                             </div>
                             
                             <div class="buttom">
                             	<table class="part_1">
                             	  <tr>
	                             	  <td><span class ="title_black">收件人/To：</span></td><td>${userAddress.receiver}</td>
	                             	  <td><span class ="title_black">电话/Tel：</span></td><td>${userAddress.mobile}</td>
                             	  </tr>
                             	  <tr>
                             		  <td><span class ="title_black">地址/Address：</span></td>
                             		  <td colspan="3"><div class="text_addr">${address}</div></td>	
                             	  </tr>
                             	  </table>
                             	  <table class="part_2">
	                             	  <tr>
	                             	  	<td>
	                             	        <div class="code2">
		                                          <div id="128Code2"></div>
		                                          <div style="float:left;margin-left:120px;margin-top:10px;clear: both;"><span>原寄地：${country}</span></div>
	                                          </div> 		
	                             	  	</td>
	                             	  	<td>
	                             	  	      <div class="qr_code" >
	                                       <%--    	<img src="${resource_path}/img/site.png" />
	                                          	<div class="site">www.su8exp.com</div> --%>
	                                          </div>
	                             	  	</td>
	                             	  </tr>
                             	</table>
                             </div>
                        </div>
                     </div>
                     
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function() {
        var code =$('#code').val();
        $('#128Code1').barcode(code, "code128",{barWidth:2,fontSize:20});
        $('#128Code2').barcode(code, "code128",{barWidth:2,fontSize:20});
         $('#print').click(function() {
     /*        $('.my_show').jqprint({
            	debug:true,
            	importCSS:true,
            }); */
            $('.my_show').printArea();
        }); 
    });
</script>
</body>
</html>