<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
</style>
<body>
<div class="admin">
	<div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">晒单信息详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
		 
			 <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>用户账号:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                     <span>${sharePojo.account}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong>用户名:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${sharePojo.user_name}</span>
                   </div>
              </div>
              <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>包裹单号:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                     <span>${sharePojo.logistics_code}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong>晒单时间:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${sharePojo.share_time}</span>
                   </div>
              </div>
              <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>审核状态:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${sharePojo.approval_status==1?'未审核':sharePojo.approval_status==2?'通过':'拒绝'}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>审核原因:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${sharePojo.reason }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
              <br>

             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>优惠券编号:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${userCoupon.coupon_code }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>优惠券名称:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${userCoupon.coupon_name }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>优惠券数量:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${userCoupon.quantity }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>是否已使用:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>
                      <c:if test="${userCoupon.coupon_name !=null}">
                      ${userCoupon.haveUsedCount==0?'否':'是' }
                      </c:if>
                      </span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
              
              <br>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>晒单链接:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${sharePojo.share_link }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>晒单图片:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span></span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>                            
                    <c:forEach var="shopImg"  items="${pkgImgList}">
        	              <div class="divImgOrder" val="${shopImg.img_id}" style="width: 120px; height: 400px; margin-top:5px;margin-left:8px;float:left;border:1px solid #E1E1E1;" id="divOrderImg${shopImg.img_id}">
						  	<div style="width: 120px;height:120px;display:table-cell;vertical-align: middle;text-align: center;">
					      	<a href="${resource_path}${shopImg.img_path}" target="_blank"><img  alt="" src="${resource_path}${shopImg.img_path}" id="imgOrder${shopImg.img_id}" style="cursor:pointer;width:120px;"/></a>
				         	</div>
						  </div>
			</c:forEach>
        
         <div class="form-button" style="float:left;margin-left:100px;margin-top:10px; clear: both;">
      			<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;
      	 </div>		
      	 
        </div>
      </div>
	</div>
</div>
</body>
<script type="text/javascript"> 
	function inShare(url){
		window.location.href = url;
		
	}
	function save(){
		var type = $('input[name="approvalStatus"][type="radio"]:checked').val();
		if("refusing" == type){
			updateShare($('#shareId').val(),3);
		}else if("isrefusing" == type){
			updateShare($('#shareId').val(),2);
		}else{
			alert("请选择操作状态");
		}
	}
	function updateShare(shareId,approvalStatus){
		var userCouponId = "";
		var userCouponIdVal=$('#userCouponId').val();
		if(userCouponIdVal != ""){
			userCouponId = "&userCouponId="+$('#userCouponId').val();
		}
		var url = "${backServer}/share/updateShare?shareId="+shareId+"&approvalStatus="+approvalStatus;
		window.location.href = url;
	 }
	
</script>
</html>