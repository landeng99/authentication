<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
   <div class="panel admin-panel">
    		<table class="table table-hover" id="list" name="list">
                       <tr>
                        <th width="15%">待操作包裹</th>
                        <th width="18%">待操作包裹关联单号</th>
                        <th width="15%">新包裹单号</th>
                        <th width="10%">增值服务</th>
                        <th width="15%">描述信息</th>
                        <th width="10%">状态</th>
                        <th width="8%">操作</th>
                    </tr>
                    <c:forEach var="pkgAttachServiceGroup" items="${pkgAttachServiceGroupList}">
                        <tr>
                            <td >
	                            <div style="width:120px;word-wrap:break-word;">
	                            	${pkgAttachServiceGroup.og_logistics_codes}
	                            </div>
                            </td>
                            <td><div style="width:120px;word-wrap:break-word;">${pkgAttachServiceGroup.og_original_nums}</div></td>
                            <td >
                           	 <div style="width:120px;word-wrap:break-word;">${pkgAttachServiceGroup.tgt_logistics_codes}</div>
                            </td>
                            <td >
                         	 <div style="width:75px;word-wrap:break-word;margin:auto;" class="service_names"> ${pkgAttachServiceGroup.service_names}</div>
                            </td>
                               <td> <div style="width:150px;word-wrap:break-word;<c:if test='${pkgAttachServiceGroup.serviceable==false}'>color:red</c:if><c:if test='${pkgAttachServiceGroup.serviceable==true}'>color:green</c:if>">${pkgAttachServiceGroup.serviceableDesc}</div></td>
                            <td >
                        	   <c:if test="${pkgAttachServiceGroup.status==1}">未完成</c:if>
                        	   <c:if test="${pkgAttachServiceGroup.status==2}">已完成</c:if>
                            </td>
                            <td >
                                <a class="button border-blue button-little operate"  <c:if test='${pkgAttachServiceGroup.serviceable==false}'> disabled="disabled"  </c:if> href="javascript:void(0);">操作
                                	<input type="hidden" name="og_package_id_group" value="${pkgAttachServiceGroup.og_package_id_group}">
                                	<input type="hidden" name="tgt_package_id_group" value="${pkgAttachServiceGroup.tgt_package_id_group}">
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
       </div>
        <div style="color:red;margin-left: 10px;margin-top: 10px;">
        	注意：
        	  <ul><li>1、请先完成增值服务再入库操作</li>
        	     <li>2、包裹到库之后才能进行增值服务</li>
        	  </ul>
        </div>
        <script type="text/javascript">
 	   $('.operate').click(function(){
		   var text=$(this).parent().parent().find('td').eq(3).find('.service_names').text();
		   if(text.indexOf('分箱')>0&&text.length<=3){
			   var input=$(this).find("input[name='og_package_id_group']")[0];
			   var logistics_code=$(this).parent().parent().find('td').eq(0).find('div').text();
			   var package_id=input.value;
			   var url = "${backServer}/pkg/initSplit?package_id=" + package_id+"&logistics_code="+logistics_code;
			   window.parent.location.href = url;
		   }else if(text.indexOf('合箱')>0){
			
			   var input=$(this).find("input[name='tgt_package_id_group']")[0];
			   var logistics_code=$(this).parent().parent().find('td').eq(1).find('div').text();
			   var package_id=input.value;
			   var url = "${backServer}/pkg/initMerge?package_id=" + package_id+"&logistics_code="+logistics_code;
			   window.parent.location.href = url;
		   }else{
			   	var input=$(this).find("input[name='og_package_id_group']")[0];
			  	var logistics_code=$(this).parent().parent().find('td').eq(0).find('div').text();
			   	var package_id=input.value;
			    var url = "${backServer}/pkg/operate?package_id=" + package_id
                + "&logistics_code=" + logistics_code;
       			 window.parent.location.href = url;
		   }
		  
	   });
        </script>
</body>
</html>