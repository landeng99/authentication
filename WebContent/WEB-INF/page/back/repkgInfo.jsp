<%@ page import="com.xiangrui.lmp.business.admin.claim.vo.Claim"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="admin">
	<div class="tab">
		<div class="tab-head">
			<ul class="tab-nav">
				<li class="active"><a href="#tab-set">退货信息详情</a></li>
			</ul>
		</div>
		<div class="tab-body">
			<div class="tab-panel active" id="tab-set">
				<div class="form-group">
					<form id="myform" name="myform">
						<input type="hidden" value="${2}"/>
						<div style="height: 40px;">
							<div style="float:left;">
								<span for="userName"><strong>申请人名:</strong></span></div>
							<div style="float:left;margin-left:40px;width:400px;">
								<span>${repkgPojo.user_name}</span></div>
							
							<div style="float:left;">
								<span for="createTime"><strong>申请状态:</strong></span></div>
							<div style="float:left;margin-left:10px;">
								<span>${repkgPojo.approve_status==0?'申请':
									repkgPojo.approve_status==1?'拒绝':'通过'}</span></div>
						</div>
						
						<div style="height: 40px;">
							<div style="float:left;">
								<span for="userName"><strong>申请时间:</strong></span></div>
							<div style="float:left;margin-left:40px;width:400px;">
								<span>${repkgPojo.create_time}</span></div>
								
							<div style="float:left;">
								<span for="createTime"><strong>审批时间:</strong></span></div>
							<div style="float:left;margin-left:10px;">
								<span>${repkgPojo.approve_time}</span></div>
						</div>
						
						<div style="height: 40px;">
							<div style="float:left;">
								<span for="userName"><strong>包裹编号:</strong></span></div>
							<div style="float:left;margin-left:40px;width:400px;">
								<span>${repkgPojo.logistics_code}</span></div>
								
							<div style="float:left;">
								<span for="createTime"><strong>退货金额:</strong></span></div>
							<div style="float:left;margin-left:10px;">
								<span><fmt:formatNumber value="${repkgPojo.return_cost}" type="currency" pattern="$#0.00#"/></span></div>
						</div>
						
						<div style="height:180px; ">
							<div style="float:left;">
								<span for="goods"><strong>商品:</strong></span></div>
							<div style="float:left;margin-left:68px;" >
								<div class="panel admin-panel" >
									<table class="table">
										<tr><td width ="300">商品名称</td>
										<td width ="100">商品申报类别</td>
										<td width ="100" align="right">价格</td>
										<td width ="100" align="right">数量</td>
										<td width ="100" align="right">关税费用</td>
										</tr>
									</table>
								</div>
								<div style="height:123px;overflow:auto;" class="panel admin-panel">
									<table class="table">
								<c:forEach var="good"  items="${pkgGoodsList}">
										<tr><td width ="300">${good.goods_name}</td>
										<td width ="100">${good.goods_type_id}</td>
										<td width ="100" align="right"><fmt:formatNumber value="${good.price}" type="currency" pattern="$#0.00#"/></td>
										<td width ="100" align="right">${good.quantity}</td>
										<td width ="110" align="right"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
										</tr>
								</c:forEach>
									</table>
								</div>
							</div>
						</div>
					
						<div style="height: 40px;">
							<div style="float:left;">
								<span for="address"><strong>退货图片:</strong></span></div>
							<div style="float:left;margin-left:40px;">
						<c:if test="${img_path != '无图片' && img_path != ''}">
								<img id="img_path" name="img_path" src="${img_path }"/>
						</c:if>
							</div>
						</div>
					
						<div style="height: 40px; clear: left;">
							<div style="float:left;">
								<span for="address"><strong>申请退货原因:</strong></span></div>
							<div style="float:left;margin-left:40px;">
								<Span>${repkgPojo.reason }</Span></div>
						</div>
					 <c:if test="${CIF_TRUE == 'bgin'}">
					 
					 	<div style="height: 40px;">
		                   <div style="float:left;"><span for="createTime"><strong>选择操作:</strong></span></div>
		                   <div style="float:left;margin-left:40px;">
		                   
		           		<shiro:hasPermission name="repkg:application">
						<c:if test="${repkgPojo.approve_status==0 }">
		                   		<input name="approve_status" value="through" type="radio"/>审核
		            	</c:if>
						</shiro:hasPermission><shiro:hasPermission name="repkg:refusing">
						<c:if test="${repkgPojo.approve_status == 0 }">
		                   		<input name="approve_status" value="refusing" type="radio"/>拒绝
		            	</c:if>
						</shiro:hasPermission>
		            
		                     <input type="hidden" value="${repkgPojo.return_id}" 
		                     	name="return_id" id="return_id"/>
		                   </div>
		              	</div>
		              	
						<div style="height: 40px;">
							<div style="float:left;">
								<span for="address"><strong>处理操作原因:</strong></span></div>
							<div style="float:left;margin-left:40px;">
								<textarea rows="3" cols="50" 
									id="approve_reason" name="approve_reason">  ${repkgPojo.approve_reason }</textarea>
							</div>
						</div>
					</c:if> <c:if test="${CIF_TRUE != 'bgin'}">
					
					
					
              
						<div style="height: 40px; clear: left;">
							<div style="float:left;">
								<span for="address"><strong>处理操作原因:</strong></span></div>
							<div style="float:left;margin-left:40px;">
								<Span>${repkgPojo.approve_reason }</Span></div>
						</div>
					</c:if>
					</form>
				</div>
				<div class="form-button" style="float:left;margin-left:100px; clear: both;">
		<c:if test="${CIF_TRUE == 'bgin'}">
				<a class="button bg-main" href="javascript:void(0);" 
						onclick="save('${repkgPojo.return_id}')">保存</a>&nbsp;&nbsp;
		</c:if>
					<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
function save(return_id){
	var type = $('input[name="approve_status"][type="radio"]:checked').val();
	if("through" == type){
		through(return_id);
	}else if("refusing" == type){
		refusing(return_id);
	}else{
		alert("请选择审核状态/您没有权限,请申请好权限在来");
	}
}
function through(return_id){
	var url = "${backServer}/repkg/updateRepkgThrough?return_id="+return_id+
		"&approve_reason="+$('#approve_reason').val();
	window.location.href = url;
}
function refusing(return_id){
	var url = "${backServer}/repkg/updateRepkgRefusing?return_id="+return_id+
			"&approve_reason="+$('#approve_reason').val();
	window.location.href = url;
}
	function updateRepkg(return_id,typeString){
		/* alert($('#approve_reason').val());
		if(confirm("确认拒绝审核吗？")){
			var url = "${backServer}repkg/updateRepkg"+typeString+"?return_id="+return_id+
					"&approve_reason="+$('#approve_reason').val();//+
					//"&operater="+$('#operater').val();
					
			window.location.href = url；
		} */
	}

</script>
</body>