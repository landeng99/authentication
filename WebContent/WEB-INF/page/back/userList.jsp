<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<body>
<div class="admin">
    <div class="panel admin-panel">
    	<div class="panel-head"><strong>用户列表</strong></div>
      <div class="padding border-bottom">
      <shiro:hasPermission name="user:add">
      		<input type="button" class="button button-small border-green" onclick="add()" value="添加用户" />
      </shiro:hasPermission>
      </div>
      <input type="hidden" value="${back_user.user_id}" id="back_user_id" name="back_user_id"/>
      <table class="table table-hover">
      		<tr><th width="45">用户编号</th><th width="100">用户名称</th><th width="100">操作</th></tr>	
      		<c:forEach var="list"  items="${userLists}">
      		<tr><th width="45">${list.user_id }</th><th width="120">${list.username }</th>
      		<th width="100">
      		<shiro:hasPermission name="user:select">
      			<a class="button border-blue button-little" href="javascript:void(0);" onclick="query('${list.user_id }','${list.username }')">用户详情</a>&nbsp;&nbsp;
      		</shiro:hasPermission>
      		<shiro:hasPermission name="user:edit">
      			<a class="button border-blue button-little" href="javascript:void(0);" onclick="update('${list.user_id }','${list.username }','${list.password }')">编辑</a>
      		</shiro:hasPermission>
      		<shiro:hasPermission name="user:delete">
      			
      			&nbsp;&nbsp;<a class="button border-blue button-little" href="javascript:void(0);" onclick="del('${list.user_id }')">删除</a>
      		</shiro:hasPermission>
      		</th>
      		</tr>	
      		</c:forEach>
      </table>
   		<div class="panel-foot text-center">
      		<jsp:include page="webfenye.jsp"></jsp:include>
      	</div>
      </div>
</div>

</body>
<script type="text/javascript">
	function add(){
		var url = "${backServer}/user/toAddUser";
		window.location.href = url;
	}
	
	function del(user_id){
		if(confirm("确认删除吗？")){
			var id = $("#back_user_id").val();
			if(id == user_id){
				alert("无法删除自身的账号！");
				return;
			}
			var url = "${backServer}/user/delUser?user_id="+user_id;
			$.ajax({
				url:url,
				type:'GET',
				success:function(data){
					 alert("删除成功！");
					 window.location.href = "${backServer}/user/queryAll";
				}
			});
		}
	}
	
	function query(user_id,username){
		var url = "${backServer}/user/userPage?user_id="+user_id+"&username="+username;
		window.location.href = url;
	}
	
	function update(user_id,username,password){
		var url ="${backServer}/user/updateUser?user_id="+user_id+"&username="+username+"&password="+password;
		window.location.href = url;
	}
</script>

</html>