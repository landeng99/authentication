<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
	#tplSelect ul{
		display: inline-block;
		margin:15px 0px
	}
	#tplSelect {margin:30px auto;width:100%;text-align: center;display: inline-block;}
	#tplSelect label{float:left;margin-left:10px;width:150px;text-align: left;line-height: 32px;}
	#tplSelect ul li{
		float: left;
		list-style: none;
		margin: 0px 10px;
	}
	</style>
<body>
	<div id="tplSelect">
		 <div style="float:left">
	 	 <label >导出模板类型:</label>
			<select id="exportConfig" >
				<option value="">--请选择模板--</option> 
			    <c:forEach var="exportConfig" items="${exportConfigList}">
				    <option value="${exportConfig.export_id}">${exportConfig.template_name}</option> 
			    </c:forEach>
			</select>
			<input type="hidden" id="pick_id" value="${pick_id}"/>
			<input type="hidden" id="pick_code" value="${pick_code}"/>
			
		</div>
	 	<div class="clear"></div>
		 <ul class="fileType">
			 <li><input type="radio" name="fileType" checked="checked" value="xls"/>xls</li>
			 <li> <input type="radio" name="fileType" value="xlsx"/>xlsx </li>
		 </ul>
		 <div><input type="button" value="导出" id="exportBtn" class="button bg-main"></div>
	</div>
	<script type="text/javascript">
	
	$('#exportBtn').click(function(){
		
		var export_id=$('#exportConfig').val();
		var $array=$('#tplSelect input:checked');
		var fileType='';
		
		$.each($array,function(i,input){
			fileType=$(input).val();
		});
		
		var pick_id=$('#pick_id').val();
		var pick_code=$('#pick_code').val();
		
 		if(export_id!=''){
			window.open("${backServer}/pickorder/export?export_id="+export_id+"&pick_id="+pick_id+"&pick_code="+pick_code+"&fileType="+fileType);
			window.parent.layer.closeAll();
		}else{
			alert('请选择模板');
		} 
	});
	
	</script>
</body>
</html>