<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<style type="text/css">
	.div_label{ float: left;width: 40%;margin-left: 10px; height: 36px;}
	.label_val{ margin-left: 10px;width: 80px;float: left;}
	.label_test{margin-left: 2px;width: 120px;float: left;}
	.div_label_menu{ float: left;width: 100%;margin-left: 10px; height: 20px;}
	.div_label_table{float: left;width: 90%;margin-top: 10px;clear: both;}
</style>

<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">用户记录信息详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="myform" name="myform">
              <div class="div_label">
                   <div class="label_val"><span><strong>用户编号&nbsp;</strong></span></div>
                   <div class="label_test">
                     <span>${accountBalancePojo.user_id == 0 ? '所有用户':accountBalancePojo.user_id}</span>
                   </div>
              </div>
              <div class="div_label">
                   <div class="label_val"><span><strong>用户名称&nbsp;</strong></span></div>
                   <div class="label_test">
                     <span>${accountBalancePojo.user_id == 0 ? '所有用户':accountBalancePojo.user_name}</span>
                   </div>
              </div>
              <div class="div_label">
                   <div class="label_val"><span><strong>处理时间&nbsp;:</strong></span></div>
                   <div class="label_test">
                     <span>${accountBalancePojo.timeStart}</span>
                   </div>
               </div>
              <div class="div_label">
                   <div class="label_val"><span><strong>上期余额&nbsp;:</strong></span></div>
                   <div class="label_test">
                     <span><fmt:formatNumber value="${accountBalancePojo.last_balance}" type="currency" pattern="$#0.00#"/></span>
                   </div>
               </div>
              <div class="div_label">
                   <div class="label_val"><span><strong>本期余额&nbsp;</strong></span></div>
                   <div class="label_test">
                     <span><fmt:formatNumber value="${accountBalancePojo.curr_balance}" type="currency" pattern="$#0.00#"/></span>
                   </div>
              </div>
              <div class="div_label">
                   <div class="label_val"><span><strong>本期差额&nbsp;</strong></span></div>
                   <div class="label_test">
                     <span><fmt:formatNumber value="${accountBalancePojo.difference}" type="currency" pattern="$#0.00#"/></span>
                   </div>
              </div>
               <div class="div_label">
                    <div class="label_val"><span><strong>充值总额&nbsp;</strong></span></div>
                    <div class="label_test">
                      <Span><fmt:formatNumber value="${accountBalancePojo.total_recharge }" type="currency" pattern="$#0.00#"/></Span>
                    </div>
               </div>               
               <div class="div_label">
                    <div class="label_val"><span><strong>索赔总额&nbsp;</strong></span></div>
                    <div class="label_test">
                      <Span><fmt:formatNumber value="${accountBalancePojo.total_withdraw }" type="currency" pattern="$#0.00#"/></Span>
                    </div>
               </div>               
               <div class="div_label">
                    <div class="label_val"><span><strong>消费总额&nbsp;</strong></span></div>
                    <div class="label_test">
                      <Span><fmt:formatNumber value="${accountBalancePojo.total_expend }" type="currency" pattern="$#0.00#"/></Span>
                    </div>
               </div>               
               <div class="div_label">
                    <div class="label_val"><span><strong>提现总额&nbsp;</strong></span></div>
                    <div class="label_test">
                      <Span><fmt:formatNumber value="${accountBalancePojo.total_claim_cost }" type="currency" pattern="$#0.00#"/></Span>
                    </div>
               </div> 
             </form>
        </div>
        
        <div class="div_label_menu">
              <div class="label_val"><span for="goods">
              	<strong>详细记录:</strong></span></div>
             <div class="div_label_table"><div class="panel admin-panel" >  
                 
              <table class="table" >
           <c:if test="${null != accountLogLists}">
             	<tr><th>订单号</th>
				<th>用户</th>
				<th>类型</th>
				<th>金额</th>
				<th>时间</th>
				<th>描述信息</th></tr>
			</c:if>
			<c:forEach var="accountLog" items="${accountLogLists}">
				<tr><td>${accountLog.order_id }</td>
				<td>${accountLog.user_name }</td>
				<td><c:if test="${accountLog.account_type==1 }">充值</c:if>
					<c:if test="${accountLog.account_type==2 }">消费</c:if>
					<c:if test="${accountLog.account_type==3 }">提现</c:if>
					<c:if test="${accountLog.account_type==4 }">索赔</c:if></td>
				<td><fmt:formatNumber value="${accountLog.amount }" type="currency" pattern="$#0.00#"/></td>
				<td>${accountLog.opr_time }</td>
				<td>${accountLog.description }</td></tr>	
			</c:forEach>
			
			<c:if test="${null != accountBalanceLists}">
             	<tr><th>用户编号</th>
				<th>用户名</th>
				<th>充值金额</th>
				<th>索赔金额</th>
				<th>消费金额</th>
				<th>提现金额</th>
				<th>上期结余</th>
				<th>本期结余</th>
				<th>差额</th><th>操作</th></tr>
			</c:if>
			<c:forEach var="accountBalance" items="${accountBalanceLists}">
				<tr><td>${accountBalance.user_id == 0 ? '所有用户':accountBalance.user_id}</td>
				<td>${accountBalance.user_name }</td>
				<td><fmt:formatNumber value="${accountBalance.total_recharge }" type="currency" pattern="$#0.00#"/></td>
				<td><fmt:formatNumber value="${accountBalance.total_claim_cost }" type="currency" pattern="$#0.00#"/></td>
				<td><fmt:formatNumber value="${accountBalance.total_expend }" type="currency" pattern="$#0.00#"/></td>
				<td><fmt:formatNumber value="${accountBalance.total_withdraw }" type="currency" pattern="$#0.00#"/></td>
				<td><fmt:formatNumber value="${accountBalance.last_balance }" type="currency" pattern="$#0.00#"/></td>
				<td><fmt:formatNumber value="${accountBalance.curr_balance }" type="currency" pattern="$#0.00#"/></td>
				<td><fmt:formatNumber value="${accountBalance.difference }" type="currency" pattern="$#0.00#"/></td>
				<td>
				<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="finkAccountBalance('${accountBalance.balance_id }','log')">再次查看</a>
						</td></tr>
			</c:forEach>
			</table>
                
             </div></div>
         </div>
         
        <!--  <div class="form-button" >
        		<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;
      	 </div>	 -->	
      	 
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
function finkAccountBalance(logId,type){
	var url = "${backServer}/accountBalance/toFinkAccountBalance?balance_id="+logId;
	window.location.href = url;
}
</script>
</body>