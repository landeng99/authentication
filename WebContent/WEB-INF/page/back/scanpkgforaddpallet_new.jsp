<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp"%>
 <script src="${backJsFile}/LodopFuncs.js"></script>
<%
String path=request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
 <script type="text/javascript">
	var audio = document.getElementById("snd_html5");
	audio.loop = false;
	audio.autoplay = false;
	function playSound(src) {
		var explorerType= getExplorerInfo();
		if(explorerType=="IE")
		{
			var ie_s = document.getElementById('snd_ie');
			if(src!='' && typeof src!=undefined){
				ie_s.src = src;
			}
		}else
		{
			var _s = document.getElementById('snd_html5');
			_s.src = src;
			_s.play();
		}
	}

	function getExplorerInfo() {
		var explorer = window.navigator.userAgent.toLowerCase();
		//ie 
		if (explorer.indexOf("msie") >= 0||explorer.indexOf("net") >= 0) {
			return "IE";
		}
		//firefox 
		else if (explorer.indexOf("firefox") >= 0) {
			return  "Firefox";
		}
		//Chrome
		else if (explorer.indexOf("chrome") >= 0) {
			return  "Chrome";
		}
		//Opera
		else if (explorer.indexOf("opera") >= 0) {
			return  "Opera";
		}
		//Safari
		else if (explorer.indexOf("Safari") >= 0) {
			return  "Safari";
		}
	}
</script>
<body>
	<table style="width:100%;">
	<tr>
	<td>
		<div style="margin-left: 10px;">
			数量统计：<br>
			<span id="seaport_count_id">
			</span>
		</div>
	</td>
	<td>

	 <div style="width:500px;margin: auto;margin-top:35px">	
		 <div style="width:200px;margin: auto;"><label>包裹运单号(or关联单号)：</label><input type="text" class="input" id="scanInput" size="30" style="width:220px; height:40px; font-size:18px;"/></div>
		 <div id="note" style="color:red;text-align: center;font-size:20px; font-weight:bold"></div>
	 </div>
	 <div style="text-align:center;margin: auto;margin-top:20px;color:red">
	 	   注意：包裹扫描之前，请将鼠标放入文本框中。
	 </div>
	 </td>
	 	<td>
	 		<div style="color: red;font-size: xx-large;margin-top: 25px;">发货口岸：<span id="seaport_name">--</span></div>
	 	</td>
	</tr>
	</table>
	 <div id="todyScanDivId" style="height:98px; overflow:auto">
		<table id="todyScanTableId">
			<tr></tr>
		</table>
	</div>
				   
	 <div class="panel admin-panel">
	 	<div class="panel-head"><strong>已放入提单包裹</strong></div>
	 	   <div style="min-height:200px;" >
	         	 <table class="table table-hover " id="pkgList">
	 	         	  <thead>
	         	     	<tr>
	         	     		<th id="logistics_code" width="20%">公司运单号</th>
	         	     		<th id="declaration_code" width="20%">报关单号</th>
	         	     		<th id="pick_code" width="20%">加入提单号</th>
	         	     		<th id="seaport_name" width="10%">口岸</th>
	         	     		<th width="8%" formatter="fmtOpt1">操作1</th>
	         	     		<th width="8%" formatter="fmtOpt2">操作2</th>
	         	     		<th width="8%" formatter="fmtOpt3">操作3</th>
	         	     		<th width="8%" formatter="fmtOpt4">操作4</th>
	         	     	</tr>
	         	      </thead>
	         	</table>
	         </div>
	    </div> 
	    <div class="buttonList" style="text-align: center;padding-top:20px;">
	         <button  class="button bg-main" id="close">返回</button>
	    </div>
	    <input type="hidden" id="output_id" value="${output_id}"/>
	 
	 <script type="text/javascript">
	 
	 var datagrid=[];
	 var $datagrid=$('#pkgList').datagrid({data:datagrid});
	 function fmtOpt1(val,rowData){
			var url="${backServer}/seaport/printOutput?package_id="+rowData.package_id+"&seaport_id="+rowData.seaport_id;
			var $opt=$('<a class="button button-small border-green" href="javascript:;">预览打印</a>');
			$opt.bind('click',function(){
				var self=$(this);
				 $table=self.parent().parent().parent();
				 $tr=self.parent().parent();
				 ShowCataDialog(rowData.package_id,rowData.seaport_id);
			});
			
			return $opt;
	 };
	 
	 function fmtOpt2(val,rowData){
			var url="${backServer}/seaport/printOutput?package_id="+rowData.package_id+"&seaport_id="+rowData.seaport_id;
			var $opt=$('<a class="button button-small border-green" href="javascript:;">打印</a>');
			$opt.bind('click',function(){
				var self=$(this);
				 $table=self.parent().parent().parent();
				 $tr=self.parent().parent();
				 autoPrint(rowData.package_id,rowData.seaport_id);
			});
			
			return $opt;
	 };
	 
	 function fmtOpt3(val,rowData){
			var url="${backServer}/pkg/detail?package_id="+rowData.package_id;
			var $opt=$('<a class="button button-small border-green" href="javascript:;">详情</a>');
			$opt.bind('click',function(){
				var self=$(this);
				 $table=self.parent().parent().parent();
				 $tr=self.parent().parent();
				  window.location.href=url;	
			});
			
			return $opt;
	 };
	 
	 function fmtOpt4(val,rowData){
			var url="${backServer}/pallet/delPkg_new?package_id="+rowData.package_id+"&seaport_id="+rowData.seaport_id+"&output_id="+$('#output_id').val();
			var $opt=$('<a class="button button-small border-green" href="javascript:;">删除</a>');
			$opt.bind('click',function(){
				var self=$(this);
				 $table=self.parent().parent().parent();
				 $tr=self.parent().parent();
				 var bool = window.confirm("确定删除?");
				 if(bool){
					 $.ajax({
						 url:url,
						 dataType:'json',
						 type:'post',
						 async:false,
						 success:function(data){
							 alert("删除成功");
							 var index=$('#pkgList tbody tr').index($tr);
							 $datagrid.datagrid('remove',index);
						 }
					 })
				 }
			});
			
			return $opt;
	 };
	 
	 $('#scanInput').change(function(){
			var logistics_code= $(this).val();
/* 	  		var hasAddData =$datagrid.datagrid('getAllData');
	  		for(i=0;i<hasAddData.length;i++){
	  			var temp=hasAddData[i];
	  			if(temp['logistics_code']==logistics_code){
	  				$('#note').text("包裹已经加入托盘，请继续扫描... ...");
	  				return;
	  			}
	  		} */
		  	var output_id=$('#output_id').val();
		  	//var pick_id=$('#pick_id').val();
		  	//var pallet_id=$('#pallet_id').val();
			$.ajax({
				url:'${backServer}/pallet/scanForAddPallet_new',
				data:{logistics_code:logistics_code,'output_id':output_id},
				type:'post',
				dataType:'json',
				async:false,
				success:function(data){
					var pkgList=data.pkgList;
					if(data.seaport_name!=null&&data.seaport_name!='')
					{
						$("#seaport_name").html('');
						$("#seaport_name").html(data.seaport_name);
					}
					if(pkgList){
						if(data.need_check_out_flag=='Y')
						{
							$('#note').text("【失败】该包裹为异常件，需要取出不可出库！");
							playSound('${resource_path}/sound/nonfind.wav');
						}else
						{
							if(data.needLeaveWarning=='Y')
							{
								alert("口岸报关号码过少，请上传新增报关号码~");
							}
							playSound('${resource_path}/sound/find.wav');
							for(i=0;i<pkgList.length;i++){
		  						//$datagrid.datagrid('add',pkgList[i]);
		  						$datagrid.datagrid('addFirst',pkgList[i]);
								//自动打印
								if(data.auto_print==1)
								{
									//alert("自动打印");
									autoPrint(pkgList[i].package_id,pkgList[i].seaport_id);
								}
		  					}
							$('#note').text("【成功】包裹加入托盘成功，请继续扫描... ...");
							//刷新口岸统计显示
							seaportCountReload();
						}
					}else{
						playSound('${resource_path}/sound/nonfind.wav');
						$('#note').text("【失败】"+data.msg);
					}
				}
			});
			queryTodyScanLog();
	  		$(this).val('');
	  	});
	 $('#close').click(function(){
		 var output_id=$('#output_id').val();
		 var url="${backServer}/output/outputDetail?output_sn=&output_id="+output_id;
		 window.location.href=url;
	 });
	 
		//今天扫描日志
function queryTodyScanLog()
{
	var output_id=$('#output_id').val();
		var url='${backServer}/outputScanLog/queryAllScanLog';
	   	var params={};
				$.ajax({
					url:url,
					data:{"pick_id":"out"+output_id},
					type:'post',
					dataType:'json',
					async:false,
					success:function(data){
						if(data.outputScanLogList.length==0){
							$("#todyScanDivId").css("height","20px");
							var newRow = "<tr style=\"color:red\"><td>尚未有扫描日志记录</td></tr>";
							$("#todyScanTableId tr:last").after(newRow);
						}else{
							$("#todyScanDivId").css("height","98px");
							$("#todyScanTableId tr").empty();
							for(i=0;i<data.outputScanLogList.length;i++)
							{
								var newRow = "<tr ";
								if(data.outputScanLogList[i].add_pallet_success==1)
								{
									newRow+="style=\"color:green\"";
								}else
								{
									newRow+="style=\"color:red\"";
								}
								if(i==0)
								{
									newRow+="><td style=\"font-size: larger;\">";
								}else
								{
									newRow+="><td>";
								}
								newRow+=data.outputScanLogList[i].scan_dateStr;
								newRow+="，"+data.outputScanLogList[i].pkg_no;
								newRow+="，"+data.outputScanLogList[i].pkg_status;
								if(data.outputScanLogList[i].add_pallet_success==0)
								{
									newRow+="，"+data.outputScanLogList[i].add_fail_reson;
								}
								newRow+="，"+data.outputScanLogList[i].scan_user_name;
								newRow+="，扫描次数："+data.outputScanLogList[i].scan_count;
								
 								if(data.outputScanLogList[i].add_pallet_success==1)
								{
									newRow+="&nbsp;&nbsp; √";
								} 
								newRow+="</td></tr>";
								
								$("#todyScanTableId tr:last").after(newRow);
							}
						}
					}
				});
		}
		queryTodyScanLog();
		seaportCountReload();
		function seaportCountReload()
		{
			var output_id=$('#output_id').val();
            var url = "${backServer}/output/queryPickOrderByOutputId"
                $.ajax({
                 url:url,
                 data:{
                     "output_id":output_id},
                 type:'post',
                 dataType:'json',
                 success:function(data){
                	 $("#seaport_count_id").html('');
                	 var count_html="";
					$.each(data.pickOrderList,function(n,value)
					{
						count_html+=value.sname+":"+value.pkgCnt+"<br>";
					});
					$("#seaport_count_id").html(count_html);
                 }
                });
		}
	
	//自动打印
	function autoPrint(package_id,seaport_id)
	{
		var url="<%=basePath%>printOutput?package_id="+package_id+"&seaport_id="+seaport_id;
		LODOP=getLodop();  
		LODOP.PRINT_INIT("打印面单");
		LODOP.SET_PRINT_PAGESIZE(1,1016,1524," ");
		LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","73%");
		LODOP.ADD_PRINT_URL(0,0,'100%','100%',url);
		LODOP.PRINT();//直接打印
		//LODOP.PRINTA();//选择打印机打印
		//LODOP.PREVIEW();//打印预览

	}
	//预览打印
	function ShowCataDialog(package_id,seaport_id) {
	    layer.open({
	        title :'打印面单',
	        type: 2,
	        shadeClose: true,
	        shade: 0.8,
	        offset: ['10px', '300px'],
	        area: ['590px', '650px'],
	        content:"${backServer}/seaport/printOutput?package_id="+package_id+"&seaport_id="+seaport_id
	    }); 
	}
	
	existDataLoad();
	function existDataLoad()
	{
		var output_id=$('#output_id').val();
        var url = "${backServer}/pallet/initScanForAddPallet_newLoadData"
            $.ajax({
             url:url,
             data:{
                 "output_id":output_id},
             type:'post',
             dataType:'json',
             success:function(data){
            	 var pkgList=data.pkgList;
					for(i=0;i<pkgList.length;i++){
  						$datagrid.datagrid('add',pkgList[i]);
  					}
             }
            });
	}
	 </script>
	<bgsound id="snd_ie" loop="0" src="">
	<audio id="snd_html5" hidden="true" controls="true"> <source
		id="snd" src="${resource_path}/sound/nonfind.wav" type="audio/mpeg" /></audio>	 
</body>
</html>