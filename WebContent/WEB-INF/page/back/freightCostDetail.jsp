<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>
<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>会员费用详情</strong></div>
      <div class="tab-body">
        <br/>
                  <input type="hidden" id=checkRateIdResult value="1">
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>等级名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;">${rate_name}</div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span><fmt:formatNumber value="${unit_price}" type="currency" pattern="$#0.00#"/>(元/磅)</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>首重单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span><fmt:formatNumber value="${first_weight_price}" type="currency" pattern="$#0.00#"/>(元/磅)</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>续重单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span><fmt:formatNumber value="${go_weight_price}" type="currency" pattern="$#0.00#"/>(元/磅)</span>
                    </div>
                 </div>
                  <div class="padding border-bottom" >
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
                 </div>
      </div>
    </div>
</div>
    <script type="text/javascript">

    </script>
</body>
</html>