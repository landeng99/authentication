<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>
<%
String path=request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
     <script src="${backJsFile}/LodopFuncs.js"></script>
    <style type="text/css">
    .field-group .field-item{
        float:left;
        margin-right: 20px;
        width: 1000px;
        line-height: 35px;
    }
    .field-group .field-item .label{
        float:left;
        line-height: 33px;
        margin-right: 10px;
    }
    .field-group .field-item .field{
        float:left;
    }
    
    .pallet-list{
        margin-top: 20px;
    }
    .imglist{
    	border:1px dashed #ccc;
    	width: 880px;
    	display:inline-block;
    	min-height: 250px;
    	
    }
    .divX
	{
	    z-index:100;
	    border-style:solid;
	    border-width:1px;
	    border-color:#0ae;
	    background-color:#ffffff;
	    display:none;
	    line-height:14px;
	    text-align:center;
	    font-weight:bold;
	    cursor:pointer;
	    font-size:14px;
	    color:#0ae;
	    right: 0;
	    height: 14px;
	    width: 14px;
	    position: absolute; 
	}
    .imglist li{display:block;float: left;margin:5px;position: relative;}
    .imglist li img{width: 200px;height:200px}
</style>
<body>
  <div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">增值服务详情</a>
          </li>
        </ul>
      </div>
      <div class="tab-body">
        <div class="admin_context">
          <div class="field-group">
            <input type="hidden" id="package_id" value="${pkg.package_id}" />
            <div class="field-item">
              <div style="float: left; width: 210px;"><span><strong>公司运单号：</strong>${pkg.logistics_code}</span></div>
              <div style="float: left; width: 250px;"><span><strong>关联单号：</strong>${pkg.original_num}</span></div>
              <div style="float: left; width: 120px;"><span><strong>包裹状态：</strong>${pkg.statusDesc}</span></div>
            </div>
            <div class="field-item">
              <div style="float: left; width: 210px;">
                         <strong> 是否到库：</strong><c:if test="${pkg.arrive_status==0}">未到库</c:if>
                                <c:if test="${pkg.arrive_status==1}">已到库</c:if>
                                <input type="hidden" id="arrive_status"  value="${pkg.arrive_status}" name="arrive_status">
              </div>
              <div style="float: left;"><span><strong>到库时间：</strong><fmt:formatDate value="${pkg.arrive_time}" pattern="yyyy/MM/dd ，E ，HH:mm"/></span></div>
            </div>
            <div class="field-item">
              <div style="float: left; width: 1000px;">&nbsp</div>
            </div>
            <div class="field-item" id="pkgAttachList">
              <div style="float: left; width: 100px;"><span><strong>增值服务内容：</strong></span>
              </div>
              <c:forEach var="pkgAttachService" items="${pkgAttachServiceList}">
                <div style="float: left; width: 20px; margin-top: 1px; margin-left: 10px;">
                  <input class="checkbox" name="attach_id"  type="checkbox"
                    value="${pkgAttachService.attach_id}"
                    <c:if test="${pkgAttachService.status==2}">checked="checked"</c:if> />
                </div>
                <div style="float: left; width: 120px;"><span>${pkgAttachService.service_name}</span>
                <c:if test="${pkgAttachService.attach_id==32}"> &nbsp;&nbsp;保价额为：<fmt:formatNumber value="${pkgAttachService.service_price*100}" type="currency" pattern="$#0.00#"/></c:if>
                </div>
              </c:forEach>
            </div>
               <div style="clear: both"></div>
            <div class="panel admin-panel" >
                  	<div class="panel-head"><strong>包裹明细</strong></div>
                  	  <div class="padding border-bottom">
                  	   	<a class="button border-blue button-little" href="javascript:void(0);"
                           onclick="ShowCataDialog('${package_id}')">预览打印面单</a>
                  	   	<a class="button border-blue button-little" href="javascript:void(0);"
                           onclick="ShowCataDialog_noreview('${package_id}')">打印面单</a>                        
                  	  </div>
                      <table class="table table-hover">
                        <tr>
                          <td width="100" align="center">商品名称</td>
                          <td width="100" align="center">品牌</td>
                          <td width="100" align="center">商品申报类别</td>
                          <td width="100" align="center">价格(元)</td>
                          <td width="100" align="center">数量</td>
                          <td width="100" align="center">关税费用(元)</td>
                        </tr>
                        <c:forEach var="good" items="${pkg.goodsList}">
                          <tr>
                            <td width="100" align="center">${good.goods_name}</td>
                            <td width="100" align="center">${good.brand}</td>
                            <td width="100" align="center">${good.name_specs}</td>
                            <td width="100" align="center"><fmt:formatNumber value="${good.price}" type="currency" pattern="$#0.00#"/></td>
                            <td width="100" align="center">${good.quantity}</td>
                            <td width="100" align="center"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                          </tr>
                        </c:forEach>
                      </table> 
            </div>
          </div>
          <div class="field-item">
            <div style="float: left; width: 1000px;" id="upload_panel">
            	<div id="pic_upload">
            		 <!-- <input type="button" value="上传图片"  name="uploadfile" id="uploadfile"/>  -->
            		
            		<input type="file" name="uploadify" id="uploadify"/>
            	</div>
            	<ul class="imglist">
            	<c:forEach var="packageImg" items="${pkg.packageImgList}">
	            	<li>
	            		<div class="divX">X</div>
	            		<img id="${packageImg.img_id}" style="width: 200px;height:200px" path="${packageImg.img_path}" src="${resource_path}/${packageImg.img_path}"/>
	            	</li>
            	</c:forEach>
            	<li class="note" style="color:red;display: none">请上传图片！！</li>
            	</ul>
            </div>
          </div>
    	  <div class="buttonList" style="clear: both; padding-top: 20px;">
            <input type="button" class="button bg-main" onclick="finish()"value="提交" />
            <input type="button" class="button bg-main" onclick="goback()" value="取消" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="${backJsFile}/upfile.js"></script>
  <script type="text/javascript">
/*  	var element=document.getElementById("uploadfile");
 	UpfileObject.initialize({
 		elements:[element],
 		baseURL:'${backServer}/pkgimg/initUploadAttachPic?package_id=${pkg.package_id}&logistics_code=${pkg.logistics_code}',
 		complete: function(response, index){
 		}
 	});  */ 
    function ShowCataDialog(package_id) {
      layer.open({
        title : '打印面单',
        type : 2,
        shadeClose : true,
        shade : 0.8,
        area : [ '630px', '800px' ],
        content : '${backServer}/pkg/print?package_id=' + package_id
      });
    }
	function ShowCataDialog_noreview(package_id) {
		LODOP=getLodop();  
		LODOP.PRINT_INIT("打印面单");
		LODOP.SET_PRINT_PAGESIZE(1,1016,1524," ");
		LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","73%");
		LODOP.ADD_PRINT_URL(0,0,'100%','100%','<%=basePath%>backprint?package_id='+package_id);
		LODOP.PRINT();//直接打印
		//LODOP.PRINTA();//选择打印机打印
		//LODOP.PREVIEW();//打印预览
	}
	
    $(function(){
    
    	//增值服务id
    	var pictureAttachId=9;
    	var inputs=$('#pkgAttachList').find("input[name='attach_id']");
    	$.each(inputs,function(i,input){
    		if(pictureAttachId==input.value){
    			$('#upload_panel').append("");
    		}
    	});
    });
	function bindEvent(){
		$('.imglist').find('li').unbind('mouseenter').bind('mouseenter',function(){
			$(this).find('.divX').show();
		});
		$('.imglist').find('li').unbind('mouseleave').bind('mouseleave',function(){
			$(this).find('.divX').hide();
		});
		$('.imglist').find('.divX').unbind('click').bind('click',function(){
			var $img=$(this).parent().find('img');
			var img_id=$img.attr('id');
			layer.confirm('确定删除？', {
			    btn: ['删除','取消'],
			    shade: false //不显示遮罩
			}, function(){
				$.ajax({url:'${backServer}/pkgimg/delImg',
					data:{'img_id':img_id,"imgPath":$img.attr('path')},
					type:'post',
					dataType:'json',
					async:false,
					success:function(data){
						if(data['result']){
							$img.parent().remove();
							layer.alert("删除成功");
						}else{
							alert(data['msg']);
						}
					}
					});
			});
		});
	}
	bindEvent();
	
    function loadImg(path,img_id){
    	 var img=$('<img/>');
         img.attr('src','${resource_path}'+'/'+path);
         img.attr('path',path);
         img.attr('id',img_id);
         var $li=$('<li><div class="divX">X</div></li>');
         $li.append(img);
         $('.imglist').append($li);
         $('.imglist').find('.note').hide();
         $('.imglist').css('border-color','#ccc');
         //绑定事件
         bindEvent();
    }
    
    function finish() {
        var arrive_status= $('#arrive_status').val();
      	if(arrive_status==0){
      	    layer.alert("包裹未到库，不能操作该服务");
      	    return ;
      	}
      // checkbox未选择
      var isNotChecked = false;
	  var  attach_ids={};
	  var picMust=false;
      $("input[name='attach_id']").each(function() {
    	  var data={};
    	  
    	  if(this.checked){
    		  attach_ids[$(this).val()]=2;
    		  
    		  //增值服务为9表示拍照服务：存在该服务必须判断是否有图片
    		  if($(this).val()==18){
    			  var $img=$('.imglist').find('img');
    			  var imgCnt=$img.length;
    			  if(imgCnt==0){
    				  picMust=true;
    			  }
    		  }
    	  }else{
    		  attach_ids[$(this).val()]=1;
    	  }
      });
      
      if (attach_ids.length==0) {
    	layer.alert("请选择增值服务");
        return;
      }
      if(picMust){
    	  layer.alert("请上传图片");
    	  $('.imglist').css('border-color','red');
    	  $('.imglist').find('.note').show();
    	  return;
      }
      var url = "${backServer}/pkg/finish";
      var package_id = $('#package_id').val();
 
      $.ajax({
        url : url,
        data : {
          "package_id" : package_id,
          'attach_ids':JSON.stringify(attach_ids)
        },
        type : 'post',
        dataType : 'json',
        async : false,
        success : function(data) {
          if(data['result']==true){
        	    if(data['result']==true){
    	            layer.alert("操作成功",function(){
    		            window.location.href = "${backServer}/breadcrumb/back";
    	            });
    	        }
          }else{
        	  alert(data['msg']);
          }
        }
      });
    }
    function goback(){
    	window.location.href='${backServer}/breadcrumb/back';
    }
    
    $(document).ready(function(){
    	var package_id=${pkg.package_id};
    	var logistics_code=${pkg.logistics_code};
        $("#uploadify").uploadify({
             'uploader': '${backServer}/pkgimg/attachPictrueMore;jsessionid=${pageContext.session.id}',
             'formData':{"package_id":package_id,"logistics_code":logistics_code},
             'swf':"${backJsFile}/uploadify.swf",
             'cancelImg': '${backImgFile}/uploadify-cancel.png',
             'queueID': 'fileQueue',
             'method':'post',
             'auto': true,
             'buttonText': '上传图片',
             'multi': true,
             'uploadLimit':10, 
             'progressData':'speed',
             'queueSizeLimit' : 10,
             'fileTypeDesc':'Image Files',
              'fileSizeLimit':'1MB', 
             'fileTypeExts': '*.jpg;*.png',
             'onUploadSuccess':function(file,data,response){            	 
            	 if(data !=""){
            		var imgIdArray=data.split("-");
            		loadImg(imgIdArray[0],imgIdArray[1]);
            	 }
             },             
             'onError': function(event, queueID, fileObj) {
                 alert("文件:" + fileObj.name + "上传失败");
             }
         });
    });
  </script>
</body>
</html>