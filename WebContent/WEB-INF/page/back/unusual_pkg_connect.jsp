<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">

p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}


.mg18 {
    margin-bottom: 18px;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
}
.hr {
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.List06 {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.List06 li {
    height: 52px;
    list-style-type:none
}
.List06 li span.pull-left {
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: right;
    width: 63px;
}
.List06 li input {
    margin-right: 4px;
}
.List06 li .text08 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 178px;
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 250px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button2.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 72px;
}

.span4 .label{
     width:70px;
     float: left;
}

</style>
<body>
<div style="background-image:none;border-right:none;width:350px;padding-top:5px;"/>
    <div class="left"></div>
    <div style="margin-left:5px;" class="right">
        <div>
            <div style="display: block;" id="withdrawal">
            	<input type="hidden" id="unusual_pkg_id" name="unusual_pkg_id" value="${unusualPkg.unusual_pkg_id}"/>
                <ul class="List06">
                      <li class="span4">
                         <p>
                            <span style="margin-top:5px;">关联单号：</span>
                            <span>${unusualPkg.original_num}</span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span style="margin-top:5px;">请输入用户账号:</span>
                            <input type="text" class="text08" id="account" name="account" style="padding-left: 5px;" >
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span style="margin-top:5px;">请输入Last name：</span>
                            <input type="text" class="text08" id="last_name" name="last_name" style="padding-left: 5px;" >
                         </p>
                      </li>
                  </ul>
                  <div class="PushButton">
                       <a id="no"  class="button" style="cursor:pointer;">取消</a>
                       <a id="yes" class="button" style="cursor:pointer;">确定</a>
                  </div>
           </div>
      </div>
   </div>

    <script type="text/javascript">
    //获取窗口索引
    var index = parent.layer.getFrameIndex(window.name);
    $("#no").click(function () {
        // 关闭窗口
        parent.layer.close(index);
    }); 
    isExist=false;
      $("#yes").click(function () {
    	  checkSamePerson();
    	if(!isExist)
    	{
          var unusual_pkg_id =$('#unusual_pkg_id').val();
          var account =$('#account').val();
          var last_name =$('#last_name').val();
          var url='${backServer}/unusualPkg/connectUnusualPackage';
          $.ajax({
              url:url,
              data:{
                  "unusual_pkg_id":unusual_pkg_id,
                  "account":account,
                  "lastName":last_name},
              type:'post',
              async:false,
              dataType:'json',
              success:function(data){
            	  var status=data['status'];
            	  if(data.result){
						alert("您已成功关联该包裹");
						  //刷新父窗口的页面
						  window.parent.location.href=window.parent.location.href;	
	                      // 关闭窗口
	                      //parent.layer.close(index);
				  }else
				  {
					  alert("关联失败，请重试其它用户名或LASTNAME");
				  }

              }
          });
      	}    		  
	  });
      function checkSamePerson()
      {
      	var account=$('#account').val();
      	var last_name=$('#last_name').val();
      	if(account!=null&&account!=''&&last_name!=null&&last_name!='')
      	{
  	    	  var url='${backServer}/unusualPkg/validateSamePerson';
  	        	  $.ajax({
  	        		  url:url,
  	        		  data:{'account':account,'lastName':last_name},
  	        		  type:'post',
  	        		  dataType:'json',
  	        		  async:false,
  	        		  success:function(data){
  	    					if(!data.result){
  	    						isExist=true;
  	    						alert("用户账号与last name不是同一个人");
  	    					} 			  
  	        		  }
  	        	  });
      	}else
      	{
      		isExist=false;
      	}
      	
      }
        </script>
</body>
</html>