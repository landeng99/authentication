<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
    <div class="panel admin-panel">
    	<div class="panel-head"><strong>文章列表</strong></div>
      <div class="padding border-bottom">
      <shiro:hasPermission name="article:add">
      		<input type="button" class="button button-small border-green" onclick="add()" value="添加文章" />
      	</shiro:hasPermission>
      </div>
      <table class="table table-hover">
      	<tr>
      		<th>文章类别</th>
      		<th>文章标题</th>
      		<th>文章作者</th>
      		<th>创建时间</th>
      		<th>操作</th>
      	</tr>	
      	<c:forEach var="list"  items="${articleLists}">
      	<tr>
      		<td>${list.articleClassify.name }</td>
      		<td>${list.title }</td>
      		<td>${list.author }</td>
      		<td>${list.create_time }</td>
      		<td>
      		<shiro:hasPermission name="article:edit">
				<a class="button border-blue button-little" href="javascript:void(0);" onclick="update('${list.id }')">编辑</a>&nbsp;&nbsp;
			</shiro:hasPermission><shiro:hasPermission name="article:delete">
				<a class="button border-blue button-little" href="javascript:void(0);" onclick="dele('${list.id }')">删除</a>
			</shiro:hasPermission>
      		</td>
      	</tr>	
      	</c:forEach>
      </table>
   		<div class="panel-foot text-center">
				<jsp:include page="webfenye.jsp"></jsp:include>
		</div>
      </div>
</div>

</body>
<script type="text/javascript">
	function add(){
		var url = "${backServer}/article/add";
		window.location.href = url;
	}
	function update(id){
		var url = "${backServer}/article/update?id="+id;
		window.location.href = url;
	}
	function dele(id){
		var k = layer.confirm('确定删除该文章？', function () {
			var url = "${backServer}/article/deleArticle?id="+id;
			window.location.href = url;
			layer.close(k);
		});			
	}
</script>
</html>