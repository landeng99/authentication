<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>短信模板设置</strong>
			</div>
			<div class="padding border-bottom">
				<input type="button" class="button button-small border-green"
					onclick="addOrEdit(-1)" value="添加模板" />
			&nbsp;&nbsp;&nbsp;允许发送短信：		
			<select id="enableSms" name="enableSms" onchange="enableSmsChange();">
				<option value="N" <c:if test="${enableSms == 'N'}">selected="selected"</c:if>>禁用</option>
				<option value="Y" <c:if test="${enableSms == 'Y'}">selected="selected"</c:if>>启用</option>
			</select>
			&nbsp;&nbsp;&nbsp;允许发送短信时间段：<input type="text" id="smsTimeRange" name="smsTimeRange" value="${smsTimeRange}" onchange="setSmsTimeRange();"/>&nbsp;输入样例：08:00~18:00
			</div>
			<table class="table table-hover">
				<tr>
					<th>模板名称</th>
					<th>创建时间</th>
					<th>状态</th>
					<th>操作</th>
				</tr>

				<c:forEach var="list" items="${warningSmsTemplateList}">
					<tr>
						<td>${list.temp_name}</td>
						<td><fmt:formatDate value="${list.create_time}" pattern="yyyy-MM-dd hh:mm"/></td>
						<td><c:if test="${list.enable=='N'}">禁用</c:if> <c:if
								test="${list.enable=='Y'}">启用</c:if></td>
						<td><a class="button border-blue button-little"
							href="javascript:void(0);"
							onclick="show('${list.seq_id }')">查看详情</a>&nbsp;&nbsp;
							<a class="button border-blue button-little"
							href="javascript:void(0);"
							onclick="addOrEdit('${list.seq_id }')">编辑</a>&nbsp;&nbsp;
							<a class="button border-blue button-little"
							href="javascript:void(0);" onclick="del('${list.seq_id }')">删除</a>
						</td>
					</tr>
				</c:forEach>
			</table>
			<div class="panel-foot text-center">
				<jsp:include page="webfenye.jsp"></jsp:include>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
	function addOrEdit(seq_id) {
		var url = "${backServer}/warningSmsTemplate/editInit?seq_id=" + seq_id;
		window.location.href = url;
	}
	function show(seq_id) {
		var url = "${backServer}/warningSmsTemplate/show?seq_id=" + seq_id;
		window.location.href = url;
	}

	function del(seq_id) {
		if (confirm("确认删除吗？")) {
			var url = "${backServer}/warningSmsTemplate/delete?seq_id=" + seq_id;
			$.ajax({
				url : url,
				type : 'GET',
				success : function(data) {
					alert("删除成功！");
					window.location.href = "${backServer}/warningSmsTemplate/queryAllOne";
				}
			});
		}
	}
	
	function enableSmsChange() {
			var enableSms=$('#enableSms').val();
			var url = "${backServer}/warningSmsTemplate/enableSmsHandle?enableSms=" + enableSms;
			$.ajax({
				url : url,
				type : 'GET',
				success : function(data) {
					alert("设置成功！");
					window.location.href = "${backServer}/warningSmsTemplate/queryAllOne";
				}
			});
	}
 function setSmsTimeRange() {
		var smsTimeRange=$('#smsTimeRange').val();
		var regTimeRange=/^[0-9][0-9]:[0-9][0-9]~[0-9][0-9]:[0-9][0-9]$/;
	    if(!(regTimeRange.test(smsTimeRange))){ 
	        alert("允许发送短信时间段有误，请重填");  
	        return false; 
	    }
		var url = "${backServer}/warningSmsTemplate/setSmsTimeRange?smsTimeRange=" + smsTimeRange;
		$.ajax({
			url : url,
			type : 'GET',
			success : function(data) {
				alert("设置成功！");
				window.location.href = "${backServer}/warningSmsTemplate/queryAllOne";
			}
		});
}
</script>
</html>