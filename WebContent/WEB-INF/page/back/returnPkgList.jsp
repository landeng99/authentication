<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>
<body>

<div class="admin">
    <div class="panel admin-panel">
    <input type="hidden" id="retpay_status_bk" value="${params.retpay_status}">
        <div class="panel-head"><strong>境外退运包裹</strong></div>
        </br>
        <form action="${backServer}/pkg/searchRtnPkg" method="post" id="myform" name="myform">
             <div style="height: 40px;">
                  <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>退货支付状态:</strong></span></div>
                  <div style="float:left;margin-left:20px;width:230px;">
                        <select id="retpay_status" name="retpay_status" class="input" style="width:200px">
                            <option value="">请选择</option>
                            <option value=1>未支付</option>
                            <option value=2>已支付</option>
                        </select>
                  </div>
                  <div style="float:left;margin-top:5px;"><span><strong>包裹用户名称:</strong></span></div>
                  <div style="float:left;margin-left:20px;width:230px;">
                      <input type="text" class="input" id="user_name" name="user_name" style="width:200px"
                             value="${params.user_name}" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                  </div>
                  <div style="float:left;margin-top:5px;"><span><strong>公司运单号:</strong></span></div>
                  <div style="float:left;margin-left:20px;">
                      <input type="text" class="input" id="logistics_code" name="logistics_code" style="width:200px"
                             onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                              value="${params.logistics_code}"/>
                  </div>
             </div>
             <div style="height: 40px;">
                  <div style="float:left;margin-top:5px;margin-left:10px"><span><strong>起始时间:</strong></span></div>
                  <div style="float:left;margin-left:48px;width:230px;">
                     <input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
                  <div style="float:left;margin-top:5px;"><span><strong>终止时间:</strong></span></div>
                  <div style="float:left;margin-left:48px;">
                     <input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"  
                            value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
             </div>
          </form>
             <div class="padding border-bottom">
                  <input type="button" class="button bg-main" onclick="search()" value="查询" />
             </div>
             <table class="table">
                  <tr><th width="25%">公司运单号</th>
                      <th width="25%">用户名称</th>
                      <th width="25%">退货支付状态</th>
                      <th width="25%">包裹详情</th>
                  </tr>    
             </table>
             <table class="table table-hover" id ="list" name ="list">
             <c:forEach var="returnPkg"  items="${returnPkgList}">
                  <tr><td width="25%">${returnPkg.logistics_code}</td>
                      <td width="25%">${returnPkg.user_name}</td>
                      <td width="25%">
                        <c:if test="${returnPkg.retpay_status==1}">未支付</c:if>
                        <c:if test="${returnPkg.retpay_status==2}">已支付</c:if>
                      </td>
                      <td width="25%">
                      <shiro:hasPermission name="pkgreturn:select">
                      <a class="button border-blue button-little" href="javascript:void(0);"
                             onclick="detail('${returnPkg.package_id}')">详情</a>
                      </shiro:hasPermission>
                      </td>
                  </tr>
             </c:forEach>
             </table>
                  <div class="panel-foot text-center">
                   <jsp:include page="webfenye.jsp"></jsp:include>
                 </div>
             </div>
</div>
</body>
<script type="text/javascript">
//选择框选中
$("#retpay_status").val($("#retpay_status_bk").val());
//初始化焦点
$('#retpay_status').focus();

function search(){

    document.getElementById("myform").submit();
}

function detail(package_id){
    var url = "${backServer}/pkg/detail?package_id="+package_id;
    window.location.href = url;
}

//回车事件
$(function(){
  document.onkeydown = function(e){
      var ev = document.all ? window.event : e;
      if(ev.keyCode==13) {

          search();
       }
  }
});
</script>

</html>