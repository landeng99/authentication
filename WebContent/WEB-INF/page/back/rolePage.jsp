<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>  
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@	taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>翔锐物流-后台管理</title>
    <link rel="stylesheet" href="${backCssFile}/admin.css">
    <link rel="stylesheet" href="${backJsFile}/layer/skin/layer.css">
    
    <link rel="stylesheet" href="${resource_path}/zTree_v3/css/zTreeStyle/zTreeStyle.css">
    <link rel="stylesheet" href="${resource_path}/zTree_v3/css/zTreePage.css">
    <script src="${resource_path}/zTree_v3/js/jquery-1.4.4.min.js"></script>
	<script src="${resource_path}/zTree_v3/js/jquery.ztree.core-3.5.js"></script>
	<script src="${resource_path}/zTree_v3/js/jquery.ztree.excheck-3.5.js"></script>
 
</head>
<style type="text/css">
.warehouse{width: 800px;}
.warehouse li{
	float: left;
	margin-right: 10px;
}
</style>
<body>
<div class="admin">

    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">角色详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="roleform">
                <div class="form-group">
                	<input type="hidden" id="addrole_check" value="0">
                	<input type="hidden" id="id" value="${role.id }">
                    <div class="label" style="float: left;"><label for="role_name">角色名称</label></div>
                    <div class="field" style="float: left;">
                    	<input type="text" class="input" id="rolename" name="rolename" value="${rolename }" style="width:200px" readonly="true" >
                   		<span id="rolenameNote"></span>
                    </div>
                
                    <div class="label" style="float: left; margin-left: 100px;"><label for="desc">角色描述</label></div>
                    <div class="field" style="float: left;">
                    	<input type="text" class="input" id="desc" name="desc" value="${desc }" style="width:300px" readonly="true" >
                    </div>
                </div>
			<div style="clear: both;">
				<ul id="treeDemo" class="ztree">
			</div>
			<div style="clear: both;" ></div>
				<div class="warehouse"><span>仓库权限：</span><ul>
					<c:forEach var="roleOverseasAddress" items="${roleOverseasAddressList}">
						<li><input type="checkbox" disabled="disabled" id="${roleOverseasAddress.id}" <c:if test="${roleOverseasAddress.role_id!=0}">checked="checked"</c:if>/>${roleOverseasAddress.warehouse}</li>
					</c:forEach>
				</ul></div>

			<div style="clear: both;" ></div>
				<div class="warehouse"><span>业务权限：</span><ul>
					<c:forEach var="businessName" items="${businessNameList}">
						<li><input type="checkbox" disabled="disabled" id="${businessName.business_name}" <c:if test="${businessName.role_id!=0}">checked="checked"</c:if>/>${businessName.business_name}</li>
					</c:forEach>
				</ul></div>
				
				<div class="form-button" style="margin-left: 10px;margin-top: 80px; clear: both; margin-bottom: 50px;">
				    <a href="javascript:history.go(-1)" class="button bg-main transitionCss">返回</a> 
        		</div>
             </form>
             </div>
             </div>
</body> 
<script type="text/javascript">
	var setting = {
        view: {
            addHoverDom: false,
            removeHoverDom: false,
            selectedMulti: false
        },
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        edit: {
            enable: false
        }
    };
	
	$(document).ready(function(){
		$.fn.zTree.init($("#treeDemo"), setting, <%=request.getAttribute("zNodess")%>);
    });
</script>
</html>