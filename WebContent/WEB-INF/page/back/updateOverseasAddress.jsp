<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>
<link href="${resource_path}/css/easyui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${resource_path}/js/jquery.easyui.min.js"></script>
<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>海外地址编辑</strong></div>
      <div class="tab-body">
        <br/>
               <input type="hidden" id="id" name ="id" value="${overseasAddress.id}">
               <input type="hidden" id="is_tax_free_init" name ="is_tax_free_init" value="${overseasAddress.is_tax_free}">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>国家:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
                       <input type="text" class="input" id="country" name="country" style="width:150px" value="${overseasAddress.country}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="countryTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>县/郡:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="county" name="county" style="width:150px" value="${overseasAddress.county}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="countyTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>州:</strong></span></div>
                    <div style="float:left;margin-left:30px;">
                       <input type="text" class="input" id="state" name="state" style="width:150px" value="${overseasAddress.state}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="stateTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>城市:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
                       <input type="text" class="input" id="city" name="city" style="width:150px" value="${overseasAddress.city}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="cityTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>仓库:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
                       <input type="text" class="input" id="warehouse" name="warehouse" style="width:150px" value="${overseasAddress.warehouse}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="warehouseTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>地址:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="address_first" name="address_first" style="width:500px" value="${overseasAddress.address_first}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="firstTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>邮编:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
                       <input type="text" class="input" id="postcode" name="postcode" style="width:100px" value="${overseasAddress.postcode}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="postcodeTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>电话:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
                       <input type="text" class="input" id="tell" name="tell" style="width:100px" value="${overseasAddress.tell}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="tellTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>免税:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
                       <select id="is_tax_free" name="is_tax_free" class="input" style="width:100px" >
                            <option value="">请选择</option>
                            <option value=1>免税</option>
                            <option value=2>不免税</option>
                         </select>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="taxTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>图标:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
                       <input type="file" name="uploadify" id="uploadify"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="flagIcon"></span>
                    </div>
                    <div id="fileQueue" style="display:none;"></div>                                     
                 </div>
                 <div style="height: 40px;">
                    <div style="clear:both;margin-left: 10px;">
                    	<img id="flagPath" src="${resource_path}${overseasAddress.flag_path}" name="flag_path"/ width="20px" height="20px">
                    	<input type="hidden" value="${overseasAddress.flag_path}" id="hiddFlag"/>
                    </div> 
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>只对以下客户可见:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
						<textarea rows="3" cols="50" readonly="readonly"  onclick="addGroupMember()" id="authAccount" name="authAccount">${authAccount}</textarea>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="taxTextArea"></span>
                    </div>
                 </div>                 
                  <div class="padding border-bottom" >
                    <input type="button" class="button bg-main" onclick="save()" value="保存" />
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
                 </div>
      </div>
    </div>
</div>

<div id="member_div" style="display:none">
    <input class="easyui-searchbox" data-options="prompt:'请输入可见用户帐号',searcher:doGroupMemberSearch" style="width:180px"></input>
	<br/>
	<table align="center"  width="394" height="282"  border="0" cellpadding="0" cellspacing="0" style=" table-layout: fixed;">
    <tr>
        <td colspan="4">
            <select name="member_from" id="member_from" multiple="multiple" size="10" style="width:100%;  height: 100%;  background: #eeeeee;  color: #a0a0a0;">
            </select>
        </td>
        <td valign="middle" align="center"  width="30">
            <input type="button" id="member_addAll" value=" >> "  class="lgb" style="width:30px;" /><br />
            <input type="button" id="member_addOne" value=" > "  class="lgb" style="width:30px;" /><br />
            <input type="button" id="member_removeOne" value="&lt;"  class="lgb" style="width:30px;" /><br />
            <input type="button" id="member_removeAll" value="&lt;&lt;"  class="lgb" style="width:30px;" /><br />
        </td>
        <td colspan="4">
            <select name="member_to" id="member_to" multiple="multiple" size="10" style="width:100%;  height: 100%;  background: #eeeeee;  color: #a0a0a0;">
            </select>
        </td>
    </tr>
</table>
      <button type="button"  class="lgb" style="margin-left: 175px;  margin-top: 6px;color: #000" onclick="addGroupMemberSubmit()">确定</button>
</div>

    <script type="text/javascript">
    $(function(){is_tax_free_init
        $('#country').focus();
    $('#is_tax_free').val($('#is_tax_free_init').val());
    });
    
    $(document).ready(function(){
        $("#uploadify").uploadify({
             'uploader': '${backServer}/overSea/uploadFlag',
             'swf':"${backJsFile}/uploadify.swf",
             'cancelImg': '${backImgFile}/uploadify-cancel.png',
             'queueID': 'fileQueue',
             'method':'post',
             'auto': true,
             'buttonText': '上传图标',
             'multi': true,
             'uploadLimit':10, 
             'progressData':'speed',
             'queueSizeLimit' : 10,
             'fileTypeDesc':'Image Files',
              'fileSizeLimit':'1MB', 
             'fileTypeExts': '*.jpg;*.png',
             'onUploadSuccess':function(file,data,response){            	 
            	 if(data !=""){
            		 $("#flagPath").attr("src","${resource_path}/"+data);
            		 $("#hiddFlag").val(data);
            	 }
             },             
             'onError': function(event, queueID, fileObj) {
                 alert("文件:" + fileObj.name + "上传失败");
             }
         });
    });
    
    $(document).ready(function(){
        $("#uploadify").uploadify({
             'uploader': '${backServer}/overSea/uploadFlag;jsessionid=${pageContext.session.id}',
             'swf':"${backJsFile}/uploadify.swf",
             'cancelImg': '${backImgFile}/uploadify-cancel.png',
             'queueID': 'fileQueue',
             'method':'post',
             'auto': true,
             'buttonText': '上传图标',
             'multi': true,
             'uploadLimit':10, 
             'progressData':'speed',
             'queueSizeLimit' : 10,
             'fileTypeDesc':'Image Files',
              'fileSizeLimit':'1MB', 
             'fileTypeExts': '*.jpg;*.png',
             'onUploadSuccess':function(file,data,response){            	 
            	 if(data !=""){
            		 $("#flagPath").attr("src","${resource_path}/"+data);
            		 $("#hiddFlag").val(data);
            	 }
             },             
             'onError': function(event, queueID, fileObj) {
                 alert("文件:" + fileObj.name + "上传失败");
             }
         });
    });
    
    function save(){
        
        $('#countryTextArea').empty();
        $('#countyTextArea').empty();
        $('#stateTextArea').empty();
        $('#cityTextArea').empty();
        $('#firstTextArea').empty();
        $('#postcodeTextArea').empty();
        $('#tellTextArea').empty();
        $('#taxTextArea').empty();
        $('#warehouse').empty();
        $('#flagIcon').empty();
        
        var url = "${backServer}/overSea/updateOverseasAddress";
        var id =$('#id').val();
        var country =$('#country').val();
        var county =$('#county').val();
        var state =$('#state').val();
        var city =$('#city').val();
        var address_first =$('#address_first').val();
        var postcode =$('#postcode').val();
        var tell =$('#tell').val();
        var is_tax_free =$('#is_tax_free').val();
        var warehouse =$('#warehouse').val();
        var flag_path =$('#hiddFlag').val();
        
        var checkResult ="0"
        if(country==null||country.trim()==""){
            $('#countryTextArea').text('国家不能为空!');
            $('#countryTextArea').css('color','red');
            checkResult ="1";
        }

        if(county==null||county.trim()==""){
            $('#countyTextArea').text('县/郡不能为空!');
            $('#countyTextArea').css('color','red');
            checkResult ="1";
        }
        if(state==null||state.trim()==""){
            $('#stateTextArea').text('州不能为空!');
            $('#stateTextArea').css('color','red');
            checkResult ="1";
        }
        if(city==null||city.trim()==""){
            $('#cityTextArea').text('城市不能为空!');
            $('#cityTextArea').css('color','red');
            checkResult ="1";
        }
        if(warehouse==null||warehouse.trim()==""){
            $('#warehouseTextArea').text('仓库不能为空!');
            $('#warehouseTextArea').css('color','red');
            checkResult ="1";
        }
        if(address_first==null||address_first.trim()==""){
            $('#firstTextArea').text('地址1不能为空!');
            $('#firstTextArea').css('color','red');
            checkResult ="1";
        }
        if(postcode==null||postcode.trim()==""){
            $('#postcodeTextArea').text('邮编不能为空!');
            $('#postcodeTextArea').css('color','red');
            checkResult ="1";
        }
        if(tell==null||tell.trim()==""){
            $('#tellTextArea').text('电话不能为空!');
            $('#tellTextArea').css('color','red');
            checkResult ="1";
        }
        if(is_tax_free==null||is_tax_free.trim()==""){
            $('#taxTextArea').text('免税区分不能为空!');
            $('#taxTextArea').css('color','red');
            checkResult ="1";
        }
        if(flag_path==null||flag_path.trim()==""){
            $('#flagIcon').text('请上传图标!');
            $('#flagIcon').css('color','red');
            checkResult ="1";
        }
        if(checkResult =="1"){
           return;
        }
        $.ajax({
            url:url,
            data:{"id":id,
                "country":country,
                "county":county,
                "state":state,
                "city":city,
                "warehouse":warehouse,
                "address_first":address_first,
                "postcode":postcode,
                "tell":tell,
                "is_tax_free":is_tax_free,
                "flag_path":flag_path,
                "authAccount":$('#authAccount').val()},
            type:'post',
            dataType:'text',
            async:false,
            success:function(data){
               alert("保存成功");
               window.location.href = "${backServer}/overSea/overseasAddressInit";
            }
        });
    }
    
	function addGroupMember()
	{
		var existMember=$("#authAccount").val();
		$("#member_to").empty();
		if(existMember!=null&&existMember!="")
		{
			var existMemberArr=existMember.split(",");
			for(var i=0;i<existMemberArr.length-1;i++)
			{
				$("#member_to").append("<option >"+existMemberArr[i]+"</option>");
			}
		}
		$('#member_div').dialog({
	    title: '选择可见用户',
	    top:25,
	    width: 400,
	    height: 400,
	    closed: false,
	    cache: false,
	    onClose: function(){
	   
	    },
	    modal: true
		});
		$('#member_div').css("display","block");
		$('#member_div').dialog('refresh');
		//doGroupMemberSearch("%");
	}
	function addGroupMemberSubmit()
	{
		  var memberUser="";
		     $.each($("#member_to option"),function(i,val)
		 		{
		 			memberUser+=val.text+",";
		 		}
		 	);
		 	$("#authAccount").val(memberUser);
	 	$("#member_div").parent().find(".panel-tool-close").click();
	}  
	function doGroupMemberSearch(value)
	{
	    $.post("${backServer}/frontUser/queryFrontUserLikeByAccount",{"account":value}, function(data){  
	 	$("#member_from").empty();
		$.each(data.frontUserList,function(i,val)
	 	{
	 		$("#member_from").append("<option >"+val.account+"</option>");
	 	});
	  },"json");
	}
	
	$(function() {
		//member
		//选择一项
		$("#member_addOne").click(function() {
			addOne($("#member_from option:selected"),"#member_to");
		});

		//选择全部
		$("#member_addAll").click(function() {
			addOne($("#member_from option"),"#member_to");
		});

		//移除一项
		$("#member_removeOne").click(function() {
			addOne($("#member_to option:selected"),"#member_from");
		});

		//移除全部
		$("#member_removeAll").click(function() {
			addOne($("#member_to option"),"#member_from");
		});
	});
	
	function addOne(source,addTo)
	{
		$.each(source,function(i,val)
 		{
 			var temp=addTo+" option";
			var needAdd=-1;
			$.each($(temp),function(j,val1)
 			{
 				if(val.text==val1.text)
 				{
 					needAdd=1;
 				}
 			});
 			if(needAdd==-1)
 			{
 				$(addTo).append("<option >"+val.text+"</option>");
 			}
 		});
 		source.remove();
	}        
    </script>
</body>
</html>