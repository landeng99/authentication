<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style>
	.div_tr{float:left;height: 40px; margin-left: 10px; clear: both;}
	.div_tr div{float: left;width: 350px;}
	.div_tr_td_label{float:left;line-height: 30px;text-align: center;width: 60px;}
	.div_tr_td_text{width:200px;margin-left: 10px;float:left;}
</style>
<div class="admin">
    <div class="tab">
    	<div class="tab-head">
    		<ul class="tab-nav">
          		<li class="active"><a href="#tab-set">优惠券</a></li>
        	</ul>
      	</div>
      	<div class="tab-body">
        	<br />
        	<div class="tab-panel active" id="tab-set">
        		<input type="hidden" id="couponId" value="${couponPojo.couponId }">
				<div class="form-group">
					<form id="myform" name="myform">
						<input type="hidden" id="messageHidden" name="messageHidden" value="${coupon.couponId}"></input>
						<div class="padding" id="findDisplayDiv" style="width: 90%;">
							<div class="div_tr">
								<div>
									<label for="name" class="div_tr_td_label">编号</label>
									<input type="hidden"  
										id="old_couponCode" name="old_couponCode" value="${couponPojo.couponCode}"/>
									<input type="text" class="input div_tr_td_text"  
										id="couponCode" name="couponCode"

										value="${couponPojo.couponCode}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="couponCodeNote"></span>
								</div><div>
									<label for="name" class="div_tr_td_label">名称</label>
									<input type="text" class="input div_tr_td_text"  
										id="coupon_name" name="coupon_name"  
										value="${couponPojo.coupon_name}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="resourceNote"></span>
								</div>
							 </div><div class="div_tr">
							 	<div>
								 	<label for="name" class="div_tr_td_label">状态</label>
									<select id="status" name="status" class="input div_tr_td_text" >
										<c:if test="${couponPojo.status==2}">
										<option value="2" selected="selected">禁用</option>
										<option value="1">启用</option>
										</c:if><c:if test="${couponPojo.status==1}">
										<option value="2">禁用</option>
										<option value="1" selected="selected">启用</option>
										</c:if><c:if test="${couponPojo.status !=2 && couponPojo.status !=1 }">
										<option value="2">禁用</option>
										<option value="1">启用</option>
										</c:if>
									</select>
								</div><div>
									<label for="name" class="div_tr_td_label">是否叠加</label>
									<select id="isRepeatUse" name="isRepeatUse" class="input div_tr_td_text" >
										
										
										<option value="1" 
										<c:if test="${couponPojo.isRepeatUse==1}">
										selected="selected"
										</c:if>
										>不可叠加</option>
										<option value="2"
										<c:if test="${couponPojo.isRepeatUse==2}">
										selected="selected"
										</c:if>
										>可以叠加</option>
										
										
									</select>
								</div>
							</div><div class="div_tr">	
								<div>
									<label for="name" class="div_tr_td_label">面值</label>
									<input type="text" class="input div_tr_td_text" 
										id="denomination" name="denomination"  
										value="${couponPojo.denomination }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="denominationNote"></span>
								</div>
								<div>
									<label for="name" class="div_tr_td_label">数目</label>
									<input type="hidden"  id="left_count" name="left_count" value="${couponPojo.left_count}"/>
									<input type="text" class="input div_tr_td_text"
										id="total_count" name="total_count"  
										value="${couponPojo.total_count }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="total_countNote"></span>
									-1代表可无限发放
								</div>
							</div><div class="div_tr">	
								<div>
									<label for="name" class="div_tr_td_label">开始时间</label>
									<input type="text" class="input div_tr_td_text"
										id="effectTime" name="effectTime" 
										onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'expirationTime\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
										value="${couponPojo.effectTime}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="effectTimeNote"></span>
								</div><div>
									<label for="name" class="div_tr_td_label">截止时间</label>
									<input type="text" class="input div_tr_td_text"
										id="expirationTime" name="expirationTime"  
										onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'effectTime\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
										value="${couponPojo.expirationTime}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="expirationTimeNote"></span>
								</div>
							</div><div class="div_tr">	
								<div>
									<label for="name" class="div_tr_td_label">有效天数</label>
									<input type="text" class="input div_tr_td_text"
										id="validDays" name="validDays"  
										value="${couponPojo.validDays }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="validDaysNote"></span>
									-1表无使用期限
								</div>
							</div>
							<div class="div_tr">	
								<div>
									<label for="name" class="div_tr_td_label">自动发送张数</label>
									<input type="text" class="input div_tr_td_text"
										id="quantity" name="quantity"  
										value="${couponPojo.quantity }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="quantityNote"></span>
								</div>
							</div>
							<div class="form-button" style="float:left;margin-left:100px; clear: both;">
					 			<a href="javascript:void(0);" class="button bg-main" onclick="save()">确定</a>
								<a href="javascript:history.go(-1)" class="button bg-main">取消</a>
							 </div>
						</div>
					</form>
        		</div>
        	</div>
      	</div>
	</div>
</div>

<script type="text/javascript">
    var canCouponCodeSubmit=true;
    var canDenominationSubmit=true;
    var canTotal_countNoteSubmit=true;
	function save(){
		var couponId = $('#couponId').val();
		if("" == couponId){
			couponId=-1;
		}
		checkCouponCode();
		checkDenomination();
		checkTotal_countNote();
		
		if(canCouponCodeSubmit&&canDenominationSubmit&&canTotal_countNoteSubmit)
		{
		var url = "${backServer}/coupon/updateOrAddCoupon";
		$.ajax({
			url:url,
			type:"POST",
			data:{"couponId":couponId,
				"couponCode":$('#couponCode').val(),
				"resource":$('#resource').val(),
				"status":$('#status').val(),
				"isRepeatUse":$('#isRepeatUse').val(),
				"denomination":$('#denomination').val(),
				"isRepeatUse":$('#isRepeatUse').val(),
				"quantity":$('#quantity').val(),
				"validDays":$('#validDays').val(),
				"effectTime":$('#effectTime').val(),
				"coupon_name":$('#coupon_name').val(),
				"total_count":$('#total_count').val(),
				"left_count":$('#left_count').val(),
				"expirationTime":$('#expirationTime').val()},
			success:function(data){
				if(data){
		            window.location.href = "${backServer}/coupon/queryAllOne";
				}
			}
		});
		}
	}
	$(function(){
		$("#couponCode").blur(function(){
			checkCouponCode();
		});
	});
	function checkCouponCode()
	{
		var taxRate = $("#couponCode").val();
		var len = $("#couponCode").val().length;
		 if(len==0||taxRate==null){
			$("#couponCodeNote").html("编号不能为空");
			$("#myform").attr("onclick","return false");
			canCouponCodeSubmit=false;
		}else{
			canCouponCodeSubmit=true;
			$("#couponCodeNote").html("");
			$("#myform").attr("onclick","return true");
			var old_couponCode = $("#old_couponCode").val();
			var couponCode = $("#couponCode").val();
			if(old_couponCode==null||old_couponCode!=couponCode)
			{
				alert("check");
				var url = "${backServer}/coupon/checkCouponCodeValid";
				$.ajax({
					url:url,
					type:"get",
					async:false,
					data:{
						"couponCode":couponCode
						},
					success:function(data){
						if(data.isValid=='N'){
							$("#couponCodeNote").html("编号已经存在");
							canCouponCodeSubmit=false;
						}else
						{
							$("#couponCodeNote").html("编号可用");
						}
						
					}
				});				
			}			
		}
	}
	 $(function(){
		$("#resource").blur(function(){
			var unit = $("#resource").val();
			var len = $("#resource").val().length;
			 if(len==0||unit==null){
				$("#resourceNote").html("来源不能为空");
				$("#myform").attr("onclick","return false");
				canSubmit=false;
			}else{
				$("#myform").attr("onclick","return true");
				canSubmit=true;
			}
		});
	});
	 $(function(){
		$("#denomination").blur(function(){
			checkDenomination();
		});
	});
	 
	 function checkDenomination()
	 {
			var unit = $("#denomination").val();
			var len = $("#denomination").val().length;
			 if(len==0||unit==null){
				$("#denominationNote").html("面值不能为空");
				$("#myform").attr("onclick","return false");
				canDenominationSubmit=false;
			}else if(unit<=0){
				$("#denominationNote").html("面值必须大于0");
				$("#myform").attr("onclick","return false");
				canDenominationSubmit=false;
			}else {
				$("#myform").attr("onclick","return true");
				$("#denominationNote").html("");
				canDenominationSubmit=true;
			}
	 }
	 
	 $(function(){
			$("#quantity").blur(function(){
				checkTotal_countNote();
			});
		});
	 function checkTotal_countNote()
	 {
			var unit = $("#total_count").val();
			var len = $("#total_count").val().length;
			 if(len==0||unit==null){
				$("#total_countNote").html("数量不能为空");
				$("#myform").attr("onclick","return false");
				canTotal_countNoteSubmit=false;
			}else if(unit<=0&&unit!=-1){
				$("#total_countNote").html("数量必须大于0或者等于-1");
				$("#myform").attr("onclick","return false");
				canTotal_countNoteSubmit=false;
			}else{
				$("#myform").attr("onclick","return true");
				$("#total_countNote").html("");
				canTotal_countNoteSubmit=true;
			}
	 }
	 /* $(function(){
			$("#validDays").blur(function(){
				var unit = $("#validDays").val();
				var len = $("#validDays").val().length;
				 if(len==0||unit==null){
					$("#validDaysNote").html("可用天数不能为空");
					$("#myform").attr("onclick","return false");
				}else{
					$("#myform").attr("onclick","return true");
				}
			});
		}); 
	 $(function(){
			$("#effectTime").blur(function(){
				var unit = $("#effectTime").val();
				var len = $("#effectTime").val().length;
				 if(len==0||unit==null){
					$("#effectTimeNote").html("开始日期不能空");
					$("#myform").attr("onclick","return false");
				}else{
					$("#myform").attr("onclick","return true");
				}
			});
		});
	 $(function(){
			$("#expirationTime").blur(function(){
				var unit = $("#expirationTime").val();
				var len = $("#expirationTime").val().length;
				 if(len==0||unit==null){
					$("#expirationTimeNote").html("截止日期能空");
					$("#myform").attr("onclick","return false");
				}else{
					$("#myform").attr("onclick","return true");
				}
			});
		});*/
</script>
</body>