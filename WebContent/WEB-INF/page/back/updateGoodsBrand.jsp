<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
	<div class="tab">
	<div class="form-group">
	
<form id="myform" name="myform" method="post" action="${backServer}/goodsBrand/saveOrUpdateGoodsBrand">
	<input type="hidden" value="${2}"/>
	<div style="height: 60px;">
		<div style="float:left;">
		<c:if test="${goodsBrandPojo.goodsTypeId != 0}">
			<span for="userName" style=" line-height: 30px;"><strong>公告标题:</strong></span></c:if></div>
		<div style="float:left;margin-left:10px;width:300px;">
		<c:if test="${goodsBrandPojo.goodsTypeId != 0}">
			<input type="text" 
				id="goodsTypeId" name="goodsTypeId" style="width: 170px;" readonly="readonly"
				value="${goodsBrandPojo.goodsTypeId}" class="input"/></c:if>
		<c:if test="${goodsBrandPojo.goodsTypeId == 0}">
			<input type="hidden" 
				id="goodsTypeId" name="goodsTypeId" style="width: 170px;"
				value="0" class="input"/></c:if></div>
	</div>
	<div style="height: 60px;">
		<div style="float:left;">
			<span for="userName" style=" line-height: 30px;"><strong>商品名称:</strong></span></div>
		<div style="float:left;margin-left:10px;width:300px;">
			<input type="text" 
				id="brandName" name="brandName" style="width: 170px;"
				value="${goodsBrandPojo.brandName}" class="input"/></div>
	</div>
	<div style="height: 40px; float: left; clear: both; margin-top:10px; margin-left: 40px;">
		<div style="float:left;">
	<c:if test="${goodsBrandPojo.goodsTypeId != 0}">
			<input type="hidden" name="noticeId" value="${noticePojo.noticeId}"/>
			<a class="button bg-main" href="javascript:void(0);"
				onclick="sub()">更新</a>
	</c:if><c:if test="${goodsBrandPojo.goodsTypeId == 0}">
			<a class="button bg-main" href="javascript:void(0);"
				onclick="sub()">保存</a>
	</c:if>
			<a class="button bg-main" href="javascript:history.go(-1);">返回</a>
			</div>
	</div>
</form> 
		 
	</div>
	</div>
</div>
</body>
<script type="text/javascript">
	function sub(){
		document.forms["myform"].submit();
	}
</script>
</html>