<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>

<body>
    <div class="admin">
        <div class="panel admin-panel">
            <div class="panel-head">
                <strong>增值服务</strong>
            </div>
            <div class="tab-body">
                <br />
                <form action="${backServer}/pkg/attachServiceSearch" method="post"
                    id="myform" name="myform">
                    <input type="hidden" id="status_bk" value="${params.status}">
                    <input type="hidden" id="attach_id_bk" value="${params.attach_id}">
                    <input type="hidden" id="auto_split_bk" value="${params.auto_split}">
                    <div style="height: 100px;">
                        <div style="float: left; margin-top: 5px; margin-left: 10px;width: 100px;"class="label">
                            <span><strong>包裹单号:</strong></span>
                        </div>
                        <div style="float: left; margin-left: 10px; width: 200px">
                            <input type="text" class="input" id="logistics_code"
                                name="logistics_code" style="width: 200px"
                                value="${params.logistics_code}"
                                onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" placeholder="公司运单号或关联单号"/>
                        </div>
                        <div style="float: left; margin-top: 5px; margin-left: 10px;width: 100px;"class="label">
                            <span><strong>增值服务状态:</strong></span>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <select id="status" name="status" class="input" style="width: 150px">
                                <option value="">---请选择状态---</option>
                                <option value=2>已完成</option>
                                <option value=1>未完成</option>
                            </select>
                        </div>
                       <div style="float: left; margin-top: 5px; margin-left: 10px;width: 100px;"class="label">
                            <span><strong>服务类型:</strong></span>
                        </div>
                       <div style="float: left; margin-left: 10px;">
                            <select id="attach_id" name="attach_id" class="input" style="width: 150px">
                                <option value="">---请选择服务---</option>
                          <c:forEach var="attachService" items="${attachServiceList}">
                           	<option value="${attachService.attach_id}">${attachService.service_name}</option>
                          </c:forEach>
                            </select>
                        </div>
                        
                  <div style="display:inline-block; margin-top: 10px;">
                	<div style="float: left; margin-top: 5px; margin-left: 10px;width: 100px;"class="label"><span><strong>创建时间:</strong></span></div>
                    <div style="float: left; margin-left: 10px;">
                  			<input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                  			onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                   	<div class="interval_label" style="float: left"><label for ="interval">~</label></div>
                   	<div style="float: left">
                   		<input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                   		onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                   	</div>
                   	
                       <div style="float: left; margin-top: 5px; margin-left: 10px;width: 100px;"class="label">
                            <span><strong>自动分箱:</strong></span>
                        </div>
                       <div style="float: left; margin-left: 10px;">
                            <select id="auto_split" name="auto_split" class="input" style="width: 150px">
                                <option value="">---请选择自动分箱---</option>
                           	    <option value="Y">是</option>
                            </select>
                        </div>                   	
                </div>
                    </div>
	                <div style="margin-left: 10px;">
	                    <input type="button" class="button bg-main" onclick="search()" value="查询" />
	                </div> 
                </form>
             
                <table class="table table-hover" id="list" name="list">
                       <tr>
                        <th width="12%">待操作包裹</th>
                        <th width="20%">待操作包裹关联单号</th>
                        <th width="12%">新包裹单号</th>
                        <th width="10%">增值服务</th>
                        <th width="10%">状态</th>
                        <th width="18%">创建时间</th>
                        <th width="10%">操作</th>
                    </tr>
                    <c:forEach var="pkgAttachServiceGroup" items="${pkgAttachServiceGroupList}">
                        <tr>
                            <td >
	                            <div style="width:120px;word-wrap:break-word;">
	                            	${pkgAttachServiceGroup.og_logistics_codes}
	                            </div>
                            </td>
                           <td >
                           	 <div style="width:200px;word-wrap:break-word;">${pkgAttachServiceGroup.og_original_nums}</div>
                            </td>
                            <td >
                           	 <div style="width:120px;word-wrap:break-word;">${pkgAttachServiceGroup.tgt_logistics_codes}</div>
                            </td>
                            <td >
                         	 <div style="width:75px;word-wrap:break-word;margin:auto;" class="service_names"> ${pkgAttachServiceGroup.service_names}</div>
                            </td>
                            <td >
                        	   <c:if test="${pkgAttachServiceGroup.status==1}">未完成</c:if>
                        	   <c:if test="${pkgAttachServiceGroup.status==2}">已完成</c:if>
                            </td>
                            <td>${pkgAttachServiceGroup.createTime}</td>
                            <td >
                            <shiro:hasPermission name="pkgAttachServiceList:caozuo">
                                <a class="button border-blue button-little operate" href="javascript:void(0);">操作
                                	<input type="hidden" name="og_package_id_group" value="${pkgAttachServiceGroup.og_package_id_group}">
                                	<input type="hidden" name="tgt_package_id_group" value="${pkgAttachServiceGroup.tgt_package_id_group}">
                                </a>
                                </shiro:hasPermission>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <div class="panel-foot text-center">
                    <jsp:include page="webfenye.jsp"></jsp:include>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $('#logistics_code').focus();

            //选择框选中
            $('#status').val($('#status_bk').val());
          //选择框选中
            $('#attach_id').val($('#attach_id_bk').val());
            $('#auto_split').val($('#auto_split_bk').val());
        });

        function search() {
            document.getElementById('myform').submit();
        }

        
	   $('.operate').click(function(){
		   var text=$(this).parent().parent().find('td').eq(3).find('.service_names').text();
		   if(text.indexOf('分箱')>0){
			   var input=$(this).find("input[name='og_package_id_group']")[0];
			   var logistics_code=$(this).parent().parent().find('td').eq(0).find('div').text();
			   var package_id=input.value;
			   var url = "${backServer}/pkg/initSplit?package_id=" + package_id+"&logistics_code="+logistics_code;
			   window.location.href = url;
		   }else if(text.indexOf('合箱')>0){
			
			   var input=$(this).find("input[name='tgt_package_id_group']")[0];
			   var logistics_code=$(this).parent().parent().find('td').eq(1).find('div').text();
			   var package_id=input.value;
			   var url = "${backServer}/pkg/initMerge?package_id=" + package_id+"&logistics_code="+logistics_code;
			   window.location.href = url;
		   }else{
			   	var input=$(this).find("input[name='og_package_id_group']")[0];
			  	var logistics_code=$(this).parent().parent().find('td').eq(0).find('div').text();
			   	var package_id=input.value;
			    var url = "${backServer}/pkg/operate?package_id=" + package_id
                + "&logistics_code=" + logistics_code;
       			 window.location.href = url;
		   }
		  
	   });
/*         function operate(package_id) {

            var url = "${backServer}/pkg/operate?package_id=" + package_id
                    + "&logistics_code=" + logistics_code;
            window.location.href = url;

        } */
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search(); 
               }
          }
        });
    </script>
</body>
</html>