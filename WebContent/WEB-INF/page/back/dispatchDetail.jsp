<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<% String path = request.getContextPath(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=path%>/resource/css/dispatchDetail.css">
	<script src="http://apps.bdimg.com/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<%=path%>/resource/css/fileinput.min.css">
	<script src="<%=path%>/resource/js/fileinput.min.js"></script>
	<script src="<%=path%>/resource/js/fileinput_locale_zh.js"></script>
</head>
<body class="container-fluid">
	<!-- 口岸列表详情、导入包裹   -->
	<div class="row">
		<div class="col-lg-2">
			<label>口岸:</label>
			<span>${dispatch.name}</span>
		</div>
		<div class="col-lg-2">
			<label>口岸列表:</label>
			<span>${dispatch.sname}</span>
		</div>
		<div class="col-lg-2">
			<label>创建时间:</label>
			<span><fmt:formatDate value="${dispatch.createTime}" pattern="yyyy-MM-dd"/></span>
		</div> 
		<div class="col-lg-2">
			<label>备注:</label>
			<span>${dispatch.description}</span>
		</div>
		<div class="col-lg-2">
			<form id="importDeclarationForm"  enctype="multipart/form-data" target="importDeclarationIFrame" method="post" action="${backServer}/dispatch/importPackage">
       			<input type="hidden" value="${id}" name="id">
	       		<input type="hidden" value="${sid}" name="sid">
	       		<input type="file" title="请选择EXCEL" id="importFile" name="importFile"/>
	          	<button type="submit" class="btn btn-info btn-large btn-primary">导入</button>
	             <%-- <a id="downTemplate" target="_blank" href="${resource_path}/download/seaport_declaration_template.xlsx">下载导入模板模板</a>  --%>
		      	<input type="hidden" name="cacheReImportId" id="cacheReImportId">
		       <iframe id="importDeclarationIFrame" name="importDeclarationIFrame" style="display: none;" onload="importOnloadDeclaration(this)"></iframe> 
			</form>
		</div>
		<!-- <button type="button" class="btn btn-info return" onclick="window.history.back(-1);"><i class="glyphicon glyphicon-share-alt">返回</i></button>  -->
	</div>
	
	<!-- 口岸列表包裹详情   -->
	<table class="table table-striped table-bordered table-hover">
		<caption></caption>
		<thead>
			<tr class="success">
				<th>公司单号</th>
				<th>关联单号</th>
				<th>商品明细</th>
				<th>入库日期</th>
				<th>实际重量</th>
				<th>身份证号</th>
				<th>身份证图片</th>
				<th>渠道</th>
				<th>状态</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="li" items="${list}">
			<tr class="active">
				<td>${li.logistics_code}</td>
				<td>${li.original_num}</td>
				<td>${li.goods_name}</td>
				<td><fmt:formatDate value="${li.input_time}" pattern="yyyy-MM-dd HH:mm"/></td>
				<td>${li.actual_weight}</td>
				<td>${li.idcard}</td>
				<td>
					<c:if test="${li.idcardImg == null }">无</c:if>
				</td>
				<td>
					<c:if test="${li.express_package == 0 }">默认</c:if>
					<c:if test="${li.express_package == 1 }">A</c:if>
					<c:if test="${li.express_package == 2 }">B</c:if>
					<c:if test="${li.express_package == 3 }">C</c:if>
					<c:if test="${li.express_package == 4 }">D</c:if>
					<c:if test="${li.express_package == 5 }">E</c:if>
				</td>
				<td>
					<c:if test="${li.status == -1 }">异常</c:if>
					<c:if test="${li.status == 0 }">待入库</c:if>
					<c:if test="${li.status == 1 }">已入库</c:if>
					<c:if test="${li.status == 2 }">待发货</c:if>
					<c:if test="${li.status == 3 }">已出库</c:if>
					<c:if test="${li.status == 4 }">空运中</c:if>
					<c:if test="${li.status == 5 }">待清关</c:if>
					<c:if test="${li.status == 7 }">已清关派件中</c:if>
					<c:if test="${li.status == 9 }">已收货</c:if>
					<c:if test="${li.status == 20 }">废弃</c:if>
					<c:if test="${li.status == 21 }">退货</c:if>
				</td> 
				<td>
					<button type="button" class="btn btn-info btn-large btn-primary" onclick="del(${li.package_id},this)"><i class="glyphicon glyphicon-trash">删除</i></button>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="panel-foot text-center">
       <jsp:include page="webfenye.jsp"></jsp:include>
    </div>
<div>
</div>
<script type="text/javascript">

	/* 详情删除  */
	function del(package_id, obj){
		var url = "${backServer}/dispatch/delPackage";
		if(confirm("确定删除")){
			$(obj).parent().parent().remove();
			$.ajax({
				url:url,
				data:{
					"package_id":package_id,
				},
				type:"post",
				dataType:"text",
				success:function(data){
					alert("提示！删除成功!");
				}
			});
		}
		else{
			alert("讨厌！！！人家都等不及了,删了吧！！！");
			return false;
		} 
	}
	
	/* Excel文件导入   */
	 function importOnloadDeclaration(dom){
    	var text = $("#importDeclarationIFrame").contents().find("body").text();
    	if(text == ""){return;}
   		data = JSON.parse(text);
   		console.log(data)
   		if(data.flag=='S')
   		{
   			window.location.href="${backServer}/dispatch/init";
   		}
	 } 
</script>	
</body>
</html>