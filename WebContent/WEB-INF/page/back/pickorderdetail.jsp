<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
		width: 800px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
		width: 150px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
	/* .upload{float: left;width: 429px;line-height: 50px;height:50px;} */
	#pick_code_url{
		display: block;
		background: url(${resource_path}/img/pdf.png) no-repeat;
		height:16px;
		line-height:16px;
		padding-left: 20px;
	    margin: 15px 159px;
	}
</style>
 
<body>
	 <div class="admin">
	  <div class="field-group">
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="pick_code">提单号</label>
		 		</div>
		 		<div class="field">
		 			 ${pickOrder.pick_code}
		 		</div>
		 	</div>
	 		<div class="field-item">
		 		<div class="label">
		 			<label for="flight_num">空运单号</label>
		 		</div>
		 		<div class="field">
		 		 	${pickOrder.flight_num}
		 		</div>
		 	</div>
		 	
		 	 <div class="field-item">
		 	<div class="label">
		 	<label for="pick_code_url">空运单号文件</label>
		 	</div>
		 		<c:if test="${isPicPdfFileExists}">	
					<a id="pick_code_url"  target=“_blank” 
						href="${resource_path}/upload/${pickOrder.pick_code}.pdf">
					<span>下载空运单号</span></a>
				</c:if>
		 	</div>
		 	
		 	
		 	  <div class="field-item">
		 		<div class="label">
		 			<label for="seaport_id">口岸</label>
		 		</div>
		 		<div class="field">
		 			${pickOrder.sname}
		 		</div>
		 	</div>
		  	 <div class="field-item">
		 		<div class="label">
		 			<label for="seaport_id">仓库</label>
		 		</div>
		 		<div class="field">
		 			${pickOrder.warehouse}
		 		</div>
		 	</div>
		 	
		   <div class="field-item">
		 		<div class="label">
		 			<label >描述信息</label>
		 		</div>
		 		<div class="text-field">
		 			 ${pickOrder.description}
		 		</div>
		 	</div>
	 	</div>
	 	
   		 <div style="clear: both"></div>
	  	<div class="panel admin-panel pickorder-list">
	  	      	  	 <div class="panel-head"><strong>托盘列表</strong>
	  	      	  	 <input type="button"  onclick="showScanLog(${pickOrder.pick_id})" value="扫描日志" />
	  	      	  	 <input type="button"  onclick="batchExport(${pickOrder.pick_id})" value="下载日志" />	  	      	  	 
	  	      	  	 </div>
	         	     <div class="padding border-bottom">
			        </div>
			        <div style="min-height: 200px;" >
	         	     <table class="table table-hover " id="palletPkgList">
	         	     <thead>
	         	     	<tr>
	         	     		<th width="45">托盘单号</th>
	         	     		<th width="45">包裹数</th>
	         	     		<th width="45">状态</th>
	         	     		<th width="45">描述信息</th>
	         	     		<th width="45">创建时间</th>
	         	     		<th width="45">操作</th>
	         	     	</tr>
	         	      </thead>
	         	     	<tbody>
	         	     		<c:forEach var="pallet"  items="${pickOrder.pallets}">
	         	     		<tr>
		         	     		<td>${pallet.pallet_code}</td>
		         	     		<td>${pallet.pkgCnt}</td>
		         	     		 <td>
			         	     		<c:if test="${pallet.status==0}">未出库</c:if>
			         	     		<c:if test="${pallet.status==1}">已出库</c:if>
			         	     		<c:if test="${pallet.status==2}">空运中</c:if>
			         	     		<c:if test="${pallet.status==3}">已清关</c:if>
		         	     		</td>
		         	     		<td>${pallet.description}</td>
		         	     		<td>${pallet.create_time}</td>
		         	     		<td><a class="button button-small border-green" href="${backServer}/pallet/detail?pallet_id=${pallet.pallet_id}">详情</a></td>
	         	     		</tr>
	         	     		</c:forEach>
	         	     	</tbody> 
	         	     </table>
	         	   	<div class="panel-foot text-center">
	      				<jsp:include page="webfenye.jsp"></jsp:include>
	      			</div>	
	         	     </div>
	  	   </div>
	  	  <div style="clear: both"></div>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<a class="button bg-main" href="javascript:;" onclick='back()'>返回</a>
	  	   </div>
	  	   </div>
	</div>
	<script type="text/javascript">
	
	 function back(){
		 window.location.href="${backServer}/breadcrumb/back";
	 }
		function showScanLog(pick_id)
		{
		    layer.open({
		        title :'扫描日志',
		        type:2,
		        shadeClose: true,
		        shade: 0.8,
		        offset: ['10px', '100px'],
		        area: ['900px', '800px'],
		        content:'${backServer}/outputScanLog/showAllScanLog?pick_id='+pick_id
		    }); 
		}
		function batchExport(pick_id) {
			window.location.href = "${backServer}/outputScanLog/exportOutputScanLog?pick_id="
					+ pick_id
					;
		}	 
	</script>
</body>
</html>