<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
</style>
 
<body>
	 <div class="admin">
	<form action="${backServer}/pkgcategory/edit" method="post">
	  <div class="field-group">
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="category_name">分类名称</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="hidden"  name="id" value="${pkgcate.id}"/>
		 			<input  class="input" type="hidden"  name="pframework" value="${pkgcate.id_framework}"/>
		 			
		 			<input  class="input" type="text"  style="width:200px"  name="category_name" value="${pkgcate.category_name}"/>
		 		</div>
		 	</div>
		 	  <div class="field-item">
		 		<div class="label">
		 			<label for="parent_id">父分类</label>
		 		</div>
		 		<div class="field">
		 			<input type="hidden" id="id_framework" name="id_framework"/>
		 			<select  class="input" style="width:200px" id="parent_id"  name="parent_id" >
		 				<option value="0" id_framework="0">--选择父分类--</option>
		 				<c:forEach var="pkgCategory" items="${pkgCategoryList}">
				 			<option value="${pkgCategory.id}" id_framework="${pkgCategory.id_framework}">${pkgCategory.category_name}</option>
		 				</c:forEach>
		 			</select>
		 		</div>
		 	</div>
	 	</div>
	 		<div style="clear: both"></div>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<input type="submit" class="button bg-main" value="保存">
	  	   		<a class="button bg-main" href="javascript:;" onclick="back()">取消</a>
	  	   </div>
	  	   </form>
	</div>
	
	 <script type="text/javascript">
		 $('#parent_id').change(function(){
			 var id_framework=$(this).find("option:selected").attr('id_framework');
			 $('#id_framework').val(id_framework);
		 });
	 
		 function back(){
			 window.history.go(-1);		 
		 }
	 </script>
	
</body>
</html>