<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style>
	.div_tr{float:left;height: 40px; margin-left: 10px; clear: both;}
	.div_tr div{float: left;width: 350px;}
	.div_tr_td_label{float:left;line-height: 30px;text-align: center;width: 60px;}
	.div_tr_td_text{width:200px;margin-left: 10px;float:left;}
</style>
<div class="admin">
    <div class="tab">
    	<div class="tab-head">
    		<ul class="tab-nav">
          		<li class="active"><a href="#tab-set">下发优惠券</a></li>
        	</ul>
      	</div>
      	<div class="tab-body">
        	<br />
        	<div class="tab-panel active" id="tab-set">
        		<input type="hidden" id="couponId" value="${couponPojo.couponId }">
				<div class="form-group">
					<form id="myform" name="myform">
						<input type="hidden" id="messageHidden" name="messageHidden" value="${coupon.couponId}"></input>
						<div class="padding" id="findDisplayDiv" style="width: 90%;">
							<div class="div_tr">
								<div>
									<label for="name" class="div_tr_td_label">编号</label>
									<input type="text" class="input div_tr_td_text"  
										id="couponCode" name="couponCode" disabled="disabled"

										value="${couponPojo.couponCode}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="couponCodeNote"></span>
								</div><div>
									<label for="name" class="div_tr_td_label">名称</label>
									<input type="text" class="input div_tr_td_text"  
										id="coupon_name" name="coupon_name"  
										value="${couponPojo.coupon_name}" disabled="disabled" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
								</div>
								
								<div>
									<label for="name" class="div_tr_td_label">面值</label>
									<input type="text" class="input div_tr_td_text" 
										id="denomination" name="denomination"  
										value="${couponPojo.denomination }" disabled="disabled" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="denominationNote"></span>
								</div>
								</div><div class="div_tr">
								<div>
									<label for="name" class="div_tr_td_label">发放用户帐号</label>
									<input type="text" class="input div_tr_td_text"
										id="userName" name="userName"  />
									<span style="color: red;" id="userNameNote"></span>
								</div>
								<div>
									<label for="name" class="div_tr_td_label">数目</label>
									<input type="text" class="input div_tr_td_text"
										id="pushQuantity" name="pushQuantity"  
										 onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="pushQuantityNote"></span>
								</div>
							</div>
							
							<div class="form-button" style="float:left;margin-left:100px; clear: both;">
					 			<a href="javascript:void(0);" class="button bg-main" onclick="save()">确定</a>
								<a href="javascript:history.go(-1)" class="button bg-main">取消</a>
							 </div>
						</div>
					</form>
        		</div>
        	</div>
      	</div>
	</div>
</div>

<script type="text/javascript">
	var canUserNameSubmit=true;
    var canQuantitySubmit=true;
	function save(){
		var couponId = $('#couponId').val();
		if("" == couponId){
			couponId=-1;
		}
		checkUserName();
		checkQuantity();
		if(canUserNameSubmit&&canQuantitySubmit)
		{
		var url = "${backServer}/coupon/pushCouponToUser";
		$.ajax({
			url:url,
			type:"POST",
			data:{"couponId":couponId,
				"userName":$('#userName').val(),
				"pushQuantity":$('#pushQuantity').val(),
				},
			success:function(data){
				if(data.isPushed=='Y'){
		            window.location.href = "${backServer}/coupon/queryAllOne";
				}else
				{
					alert(data.errorMsg);
				}
			}
		});
		}
	}
	$(function(){
		$("#userName").blur(function(){
			checkUserName();
		});
	});
	
	function checkUserName()
	{
		var taxRate = $("#userName").val();
		var len = $("#userName").val().length;
		 if(len==0||taxRate==null){
			$("#userNameNote").html("发放用户名不能为空");
			$("#myform").attr("onclick","return false");
			canUserNameSubmit=false;
		}else{
			$("#userNameNote").html("");
			canUserNameSubmit=true;
		}
	}
	 $(function(){
			$("#pushQuantity").blur(function(){
				checkQuantity();
			});
	 });
	 function checkQuantity()
	 {
			var unit = $("#pushQuantity").val();
			var len = $("#pushQuantity").val().length;
			 if(len==0||unit==null){
				$("#pushQuantityNote").html("数量不能为空");
				$("#myform").attr("onclick","return false");
				canQuantitySubmit=false;
			}else if(unit<=0){
				$("#pushQuantityNote").html("数量必须大于0");
				$("#myform").attr("onclick","return false");
				canQuantitySubmit=false;
			}else{
				$("#myform").attr("onclick","return true");
				$("#pushQuantityNote").html("");
				canQuantitySubmit=true;
			}
	 }
</script>
</body>