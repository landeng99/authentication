<%@ page import="com.xiangrui.lmp.business.admin.claim.vo.Claim"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">索赔信息详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
		
			<form id="myform" name="myform">
			 <div style="height: 40px;">
                   <div style="float:left;"><span for="userName"><strong>申请人名:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                     <span>${claimPojo.userName}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong>申请类型:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${retypay_status}</span>
                   </div>
              </div>
              <div style="height: 40px;">
                   <div style="float:left;"><span for="userName"><strong>申请时间:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                     <span>${claimPojo.applyTime}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong>申请状态:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${claimPojo.status==1?'申请':claimPojo.status==2?'拒绝'
                     		:claimPojo.status==3?'审批':claimPojo.status==4?'退款中':'完成'}</span>
                   </div>
              </div>
              <div style="height: 40px;">
                   <div style="float:left;"><span for="userName"><strong>包裹编号:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                     <span>${claimPojo.logistics_code}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong>理赔金额:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${claimPojo.amount}</span>
                   </div>
              </div>
              
              <div style="height:180px; ">
                      <div style="float:left;"><span for="goods"><strong>商品:</strong></span></div>
                      <div style="float:left;margin-left:68px;" >
                        <div class="panel admin-panel" >
                          <table class="table">
                            <tr>
                              <td width ="300">商品名称</td>
                              <td width ="100">商品申报类别</td>
                              <td width ="100" align="right">价格</td>
                              <td width ="100" align="right">数量</td>
                              <td width ="100" align="right">关税费用</td>
                            </tr>
                          </table>
                        </div>
                        <div style="height:123px;overflow:auto;" class="panel admin-panel">
                          <table class="table">
                          <c:forEach var="good"  items="${pkgGoodsList}">
                            <tr>
                              <td width ="300">${good.goods_name}</td>
                              <td width ="100">${good.goods_type_id}</td>
                              <td width ="100" align="right"><fmt:formatNumber value="${good.price}" type="currency" pattern="$#0.00#"/></td>
                              <td width ="100" align="right">${good.quantity}</td>
                              <td width ="110" align="right"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                            </tr>
                          </c:forEach>
                          </table>
                        </div>
                      </div>
                 </div>               
                 <div style="height: 40px;">
                     <div style="float:left;"><span for="address"><strong>退货图片:</strong></span></div>
                     <div style="float:left;margin-left:40px;">
                       <c:if test="${img_path != '无图片' && img_path != ''}">
               	    	<img 
               	    		id="img_path" name="img_path" 
               	 			src="${img_path }"/>
               	 		</c:if>
                     </div>
                </div>
                
              <div style="height: 40px; clear: left;">
                     <div style="float:left;"><span for="address"><strong>申请原因:</strong></span></div>
                     <div style="float:left;margin-left:40px;">
                       <Span>${claimPojo.reason }</Span>
                     </div>
                </div>
            <c:if test="${CIF_TRUE == 'bgin'}">
            	<div style="height: 40px;">
                   <div style="float:left;"><span for="createTime"><strong>选择操作:</strong></span></div>
                   <div style="float:left;margin-left:40px;">
            <shiro:hasPermission name="claim:application">
      		<c:if test="${claimPojo.status == 1}">
                   		<input name="status" value="through" type="radio"/>审核
            </c:if></shiro:hasPermission><shiro:hasPermission name="claim:refusing">	
      		<c:if test="${claimPojo.status == 1 }">
                   		<input name="status" value="refusing" type="radio"/>拒绝
            </c:if></shiro:hasPermission><shiro:hasPermission name="claim:through">	
      		<c:if test="${claimPojo.status == 3 }"> 
                   		<input name="status" value="processing" type="radio"/>财务审查
            </c:if></shiro:hasPermission><shiro:hasPermission name="claim:complete">	
      		<c:if test="${claimPojo.status == 4 }">
                   		<input name="status" value="complete" type="radio"/>财务付款
            </c:if></shiro:hasPermission>
                     <input type="hidden" value="${claimPojo.claimId}" 
                     	name="claimId" id="claimId"/>
                   </div>
              </div>
                <div style="height: 40px;">
                     <div style="float:left;"><span for="address"><strong>退货原因:</strong></span></div>
                     <div style="float:left;margin-left:40px;">
                       <textarea rows="3" cols="50" 
               	    	id="reason" name="reason">
               	    	这个理由是新增的,操作的原因</textarea>
                     </div>
                </div>
             </c:if>   
             </form>
        </div>
        
         <div class="form-button" style="float:left;margin-left:100px;margin-top:10px; clear: both;">
        		<a class="button bg-main" href="javascript:void(0);" 
      				onclick="save('${claimPojo.claimId }')">保存</a>&nbsp;&nbsp;
      			<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;
      	 </div>		
      	 
        </div>
      </div>
	</div>
</div>
<script type="text/javascript">

	function back(){
		window.location.href = "${backServer}/cliam/queryAll";
	}
	
	function save(){ 
		//
		var statusType = $('input[type="radio"][name="status"]:checked').val();
		var claimId = $('#claimId').val();
		if("through"==statusType){
			through(claimId);
		}else if("refusing"==statusType){
			refusing(claimId);
		}else if("processing"==statusType){
			processing(claimId);
		}else if("complete"==statusType){
			complete(claimId);
		}else{
			alert("请选择操作状态");
		}
	}
	function through(claimId){
		if("" == reason || "这个理由是新增的,操作的原因" == reason){
			alert("请填写同意索赔的原因");
			return false;
		}
		if(confirm("确认同意本包裹的索赔申请？")){
			url = "${backServer}/claim/updateClaimThrough?claimId="+claimId;
			window.location.href = url;
		}
	}
	function refusing(claimId){
		if("" == reason || "这个理由是新增的,操作的原因" == reason){
			alert("请填写拒绝索赔的原因");
			return false;
		}
		if(confirm("确认同意本包裹的索赔申请？")){
			url = "${backServer}/claim/updateClaimRefusing?claimId="+claimId;
			window.location.href = url;
		}
	}
	function processing(claimId){
		if("" == reason || "这个理由是新增的,操作的原因" == reason){
			alert("请填写条件包裹的索赔的原因");
			return false;
		}
		if(confirm("确定提交本包裹财务信息？")){
			url = "${backServer}/claim/updateClaimProcessing?claimId="+claimId;
			window.location.href = url;
		}
	}
	function complete(claimId){
		if("" == reason || "这个理由是新增的,操作的原因" == reason){
			alert("请填写支付的原因");
			return false;
		}
		if(confirm("确定索赔支付吗？")){
			url = "${backServer}/claim/updateClaimComplete?claimId="+claimId;
			window.location.href = url;
		}
	}	
</script>
</body>