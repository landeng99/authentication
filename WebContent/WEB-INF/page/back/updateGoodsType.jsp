<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
</style>
 
<body>
	 <div class="admin">
	<form action="${backServer}/goodsType/saveOrUpdateGoodsType" method="post">
	  <div class="field-group">
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="category_name">申报类名</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="hidden"  name="goodsTypeId" value="${goodsTypePojo.goodsTypeId}"/>
		 			<input  class="input" type="text"  style="width:200px"  name="nameSpecs" value="${goodsTypePojo.nameSpecs}"/>
		 		</div>
		 	</div>
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="category_name">单位</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="text"  style="width:200px"  name="unit" value="${goodsTypePojo.unit}"/>
		 		</div>
		 	</div>
		 	

<c:if test="${goodsTypePojo.goodsTypeId == 0}">
		 	  <div class="field-item">
		 		<div class="label">
		 			<label for="parent_id">父分类</label>
		 		</div>
		 		<div class="field">
		 			<select  class="input" style="width:200px" name="parentId">
		 				<option value="0">--选择父分类--</option>
		 				<c:forEach var="cate" items="${goodsTypeLists}">
					 			<option value="${cate.goodsTypeId}" <c:if test="${cate.goodsTypeId==goodsTypePojo.parentId}"> selected="selected" </c:if>>${cate.nameSpecs}</option>
		 				</c:forEach>
		 			</select>
		 		</div>
		 	</div>
</c:if>
	 	</div>
	 		<div style="clear: both"></div>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<input type="submit" class="button bg-main" value="保存">
	  	   		<a class="button bg-main" href="javascript:;" onclick="back()">取消</a>
	  	   </div>
	  	   </form>
	</div>
	
	 <script type="text/javascript">
		 function back(){
			 window.history.go(-1);		 
		 }
	 </script>
	
</body>
</html>