<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>跨境物流后台-用户登录</title>
 <script src="${backJsFile}/jquery.js"></script>
 <script>
//如果页面是在框架中打开则跳转至，将父框架重新定位至首页。
    function isInFrame(){
	    if( window.top == window.self ){
	
	     }else{
	    	   window.parent.location.href="${backServer}/admin/login";
	     }
	}
 isInFrame();
// html5本地存储,保存账号与密码
 $(function(){
    $("#btnLogin").click(function(){
                var usename=$("#txtUserName").val();
                var password=$("#txtPassword").val();
                 window.localStorage.setItem("use",usename);
                 window.localStorage.setItem("pass",password);
                 // window.localStorage.clear();
            });
         var usename=window.localStorage.getItem("use");
         var password=window.localStorage.getItem("pass",password);
         // alert(usename);
          if($("#rem").is(':checked')){                
               $("#txtUserName").val(usename);
               $("#txtPassword").val(password);
            }else{
                
            }     
 });
</script>
 
 
 
</head>
 <style type="text/css">
   body,div, dl, dt, dd, h1
        {
            margin: 0 auto;
            padding: 0;
        }
        
        .login_context{
        	width:100%;
        	margin:150px 0px;
        	position: relative;
        }
        .login
        {
            width: 950px;
            margin: 0px auto;
            
        }
        .login h1
        {
            padding: 20px 0 20px 30px;
        }
        .login-frm
        {
            background: url(${backImgFile}/login.png) no-repeat;
            width: 390px;
            height: 405px;
            float: center;
        }
        .login-frm dl
        {
            padding: 10px 0 0 40px;
        }
        .login-frm dt
        {
            font: 26px \5fae\8f6f\96c5\9ed1,\9ed1\4f53;
            color: #0187CB;
        }
        .login-frm dt span
        {
            padding-left: 5px;
            font: 14px arial;
            color: #c1c1c1;
        }
        .login-frm dd
        {
            padding: 5px 0;
        }
        .slide
        {
            float: left;
        }
        .verification-code
        {
            vertical-align: middle;
            height: 33px;
        }
        .txt, .txtF
        {
            border: 1px solid #C1CCD0;
            font-size: 14px;
            height:35px;
            line-height:35px;
            vertical-align: middle;
            color: #B6B6B6;
        }
        .txtF
        {
            border-color: #A0AEB4;
            color: #000;
        }
        .txtErr
        {
            font-size: 14px;
            color: Red;
        }
        .submit
        {
            background-color: #018FD5;
            padding: 8px 25px;
            color: #fff;
            font: 16px \5fae\8f6f\96c5\9ed1,\9ed1\4f53;
            border: 0;
            cursor: pointer;
        }
        .footer{margin: auto;  text-align:  center; background-color: #fff; border-top: 1px solid #f0f0f0; margin-top:200px;  overflow: hidden; width: 100%;line-height: 30px;font-size:12px;color:gray;}
    </style>


<body>

<body onkeydown="doKeyDown()">
    <!-- login -->
    <div class="login_context">
    <div id="divmain" class="login">
        <!--
		<div class="slide">
            <img src="${backImgFile}/slide.jpg" alt="" />
        </div>
		-->
        <form action="${backServer}/login" method="post" id="myform" name="myform">
        <div class="login-frm">
            <h1>
                <img src="${backImgFile}/logo.png" alt="" /></h1>
            <dl>
                <dt>系统用户登录<span>User Login</span></dt>
                <dd>
                    <input id="txtUserName" type="text" name="username" class="txt" style="width: 245px; font-size:20px" value="请输入用户名"
                        onfocus="OnFocus('txtUserName','请输入用户名');" onBlur="LostFocus('txtUserName','请输入用户名');LostFocus_UserName();" />
                    <br />
                    <label id="errUserName" class="txtErr">
                    </label>
                </dd>
                <dd>
                 <input id="txtPassword1" type="text" class="txt" style="width: 245px; font-size:20px" value="请输入密码"
                        onfocus="txtPassword_OnFocus();" />
                   <input id="txtPassword" type="password" name="password" class="txtF" style="width: 245px;" value=""
                        onblur="txtPassword_LostFocus();" />
                    <br />
                    <label id="errPassword" class="txtErr">
                    </label>
                </dd>
                <dd>
                    <input id="txtCode" type="text" class="txt" size="15" maxlength="4" name="txtCode" value="请输入验证码"  onblur="LostFocus('txtCode','请输入验证码');LostFocus_Code();"/>
                    <img id="imgVC" src="${mainServer}/authImg" alt="看不清？点击刷新。" width="120" height="36" hspace="2" onClick="refresh()"
                        style="cursor: pointer;" class="verification-code" />
                    <br />
                    <label id="errCode" class="txtErr">
                    ${txtCodeErr}
                    </label>
                </dd>
               <input type="checkBox" name="rem" id="rem" checked="">记住密码<br>
                <dd>
                    <input type="button" id="btnLogin" value="登录" class="submit" /></dd>

           
            </dl>
             <div style="margin-top:10px;">
				   &nbsp;&nbsp;<span>${fail}</span>
		    </div>
        </div>
        </form>
    </div>
    </div>
 	<div style="clear: both;"></div>
</body>
<script type="text/javascript">
$().ready(function () {
    $("#btnLogin").click(Login);
    $("#txtUserName").focus(function(){
    
    	if($(this).val()=='请输入用户名'){
    		$(this).val('');
    	}
    	
    });
    
    $("#txtCode").focus(function(){
        
    	if($(this).val()=='请输入验证码'){
    		$(this).val('');
    	}
    	
    });
    
    

    $("#txtPassword").hide();
    $("#txtPassword1").show();

    $("#errUserName").hide();
    $("#errPassword").hide();
    
    var textCodeError='${txtCodeErr}';
    if(textCodeError!=''){
	    $("#errCode").show();
    }else{
	    $("#errCode").hide();
    }
});

/* $().ready(function () {
    $("#btnLogin").click(Login);
    $("#txtUserName").focus();

    $("#txtPassword").hide();
    $("#txtPassword1").show();

    $("#errUserName").hide();
    $("#errPassword").hide();
    
    $("#errCode").hide();
}); */

function Login() {

    $("#errUserName").hide();
    $("#errPassword").hide();
    $("#errCode").hide();
    var username = $("#txtUserName").val();
    if (username == "" || username == "请输入用户名") {
        $("#errUserName").html("用户名不能为空!");
        $("#errUserName").show();
        $("#txtUserName").focus();
        return;
    }
    var password = $("#txtPassword").val();
    if (password == "" || password == "请输入密码") {
        $("#errPassword").html("密码不能为空!");
        $("#errPassword").show();
        $("#txtPassword").focus();
        $("#txtPassword1").hide();
        $("#txtPassword").show();
        return;
    }
    var code = $("#txtCode").val();
    if (code == "" || code == "请输入验证码") {
        $("#errCode").html("验证码不能为空!");
        $("#errCode").show();
        $("#txtCode").focus();
        return;
    }
	$('#myform').submit()
}
function doKeyDown(event) {
	 var e=event || window.event || arguments.callee.caller.arguments[0];
 
    if (e.keyCode == 13) {
        Login();
    }
}

function OnFocus(id, dval) {
    var obj = $("#txtUserName");
    if (id == "txtCode")
        obj = $("#txtCode");

    obj.attr("class", "txtF");
    if (obj.val() == dval) {
        //obj.val('');
        obj.select();
    }
}
function LostFocus(id, dval) {
    var obj = $("#txtUserName");
    if (id == "txtCode")
        obj = $("#txtCode");

    if (obj.val() == "" || obj.val() == dval) {
        obj.attr("class", "txt");
        obj.val(dval);
    } else {
        obj.attr("class", "txtF");
    }
}
function LostFocus_UserName() {
    var username = $("#txtUserName").val();
    if (username == "" || username == "请输入用户名") {
        $("#errUserName").html("用户名不能为空!");
        $("#errUserName").show();
    } else {
        $("#errUserName").hide();
    }
}
function LostFocus_Code() {
    var code = $("#txtCode").val();
    if (code == "" || code == "请输入验证码") {
        $("#errCode").html("验证码不能为空!");
        $("#errCode").show();
    } else {
        $("#errCode").hide();
    }
}

function txtPassword_OnFocus() {
    $("#txtPassword1").hide();
    $("#txtPassword").show();
    $("#txtPassword").focus();
}
function txtPassword_LostFocus() {
    var password = $("#txtPassword").val();
    if (password == "") {
        $("#txtPassword").hide();
        $("#txtPassword1").show();
        $("#errPassword").html("密码不能为空!");
        $("#errPassword").show();
    } else {
        $("#errPassword").hide();
    }
}
function refresh(){
	document.getElementById("imgVC").src="${mainServer}/authImg?change="+(new Date()).getTime();
}	
</script>

</html>
