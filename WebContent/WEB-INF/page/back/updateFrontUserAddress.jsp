<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:80px;
    }
    .div-front{
        float:left;
        width:300px;
    }
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">账号审核</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br/>
        <div class="tab-panel active" id="tab-set">
        <div class="field-group ">
        <input type="hidden" id="address_id" name ="address_id" value="${receiveAddress.address_id}">
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">用户账号:</span>
                           <span>${frontUser.account}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户姓名:</span>
                           <span>${frontUser.user_name}</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">手机号码:</span>
                           <span>${frontUser.mobile}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户邮箱:</span>
                           <span>${frontUser.email}</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">注册时间:</span>
                           <span><fmt:formatDate value="${frontUser.register_time}" type="both"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户类型:</span>
                           <span>
                             <c:if test="${frontUser.user_type==1}">普通用户</c:if>
                             <c:if test="${frontUser.user_type==2}">同行用户</c:if>
                           </span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">first_name:</span>
                           <span>${frontUser.first_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">last_name:</span>
                           <span> ${frontUser.last_name}</span>
                      </div>
                </div>
                 <div style="height: 40px;">
                      <div style="float:left;"><span><strong>用户地址:</strong></span></div>
                      <div style="float:left;margin-left:30px;">
                       <Span>${address}</Span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;"><span><strong>身份证号:</strong></span></div>
                      <div style="float:left;margin-left:30px;">
                       <Span>${receiveAddress.idcard}</Span>
                      </div>
                 </div>

                 <div style="height: 250px;">
                      <div style="float:left;"><span><strong>身份证照片:</strong></span></div>
                      <div style="float:left;margin-left:12px;">
                        <img src="${resource_path}${receiveAddress.front_img}" id="picArea"  name ="picArea" width="320" height="240"/>
                      </div>
                      <div style="float:left;margin-left:40px;">
                        <img src="${resource_path}${receiveAddress.back_img}" id="picArea"  name ="picArea" width="320" height="240"/>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;"><span><strong>审核结果:</strong></span></div>
                      <div style="float:left;margin-left:40px;">
                        <input type="radio"  id="yes" name="idcard_status" style="width:30px" value = 1 
                        <c:if test="${receiveAddress.idcard_status==1}">checked="checked"</c:if>/>
                      </div>
                      <div style="float:left;">
                      <Span>同意</Span>
                      </div>
                      <div style="float:left;margin-left:40px;">
                        <input type="radio"  id="no" name="idcard_status" style="width:30px" value = 2 
                         <c:if test="${receiveAddress.idcard_status!=1}">checked="checked"</c:if> />
                      </div>
                      <div style="float:left;">
                      <Span>拒绝</Span>
                      </div>
                 </div>

                 <div style="height: 40px;">
                      <div style="float:left;margin-top:5px;"><span><strong>拒绝理由:</strong></span></div>
                      <div style="float:left;margin-left:40px;width:800px;">
                        <input type="text" class="input" id="refuse_reason" name="refuse_reason" style="width:600px"
                        value ="${receiveAddress.refuse_reason}"/>
                      </div>
                 </div>
         </div>
        <div class="form-button" style="float:left;margin-left:100px;"><a href="javascript:void(0);" class="button bg-main" onclick="save()" >保存</a>
            <a href="javascript:history.go(-1)" class="button bg-main">取消</a> 
        </div>
             </div>
             </div>
        </div>
        </div>
    </div>
<script type="text/javascript">

    function save(){
        var url = "${backServer}/frontUser/frontUserVerifyUpdate";

        var address_id=$('#address_id').val();
        var idcard_status =$("input[name='idcard_status']:checked").val();
        var refuse_reason =$('#refuse_reason').val();
        if(2==idcard_status &&( refuse_reason==null||refuse_reason.trim()=="")){
            alert("拒绝理由不能为空");
            $('#refuse_reason').focus();
            return
        }
        
        $.ajax({
            url:url,
            data:{"address_id":address_id,
                  "idcard_status":idcard_status,
                  "refuse_reason":refuse_reason},
            type:'post',
            dataType:'text',
            async:false,
            success:function(data){
               alert("保存成功");
               window.location.href = "${backServer}/frontUser/frontUserAddressInit";
            }
        });
    }
    
    $('#yes').click(function(){
        $('#refuse_reason').attr('disabled',"disabled");
    });
    $('#no').click(function(){
        $('#refuse_reason').removeAttr('disabled');
    });

    </script>
</body>
</html>