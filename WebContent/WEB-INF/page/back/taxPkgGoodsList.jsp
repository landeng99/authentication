<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div class="admin">

	<div class="padding border-bottom" style="height: 100px;"
		id="findDisplayDiv">
		<!-- 提供登录人用户id userId 信息 -->
		<div
			style="float: left; height: 32px; margin-left: 10px; margin-top: 8px; width: 900px;">
			<label for="name"
				style="float: left; line-height: 30px; text-align: center;">包裹编号&nbsp;</label>
			<input type="text" class="input" style="width: 200px; float: left;"
				id="logistics_code" name="logistics_code"
				value="${objectParams.logistics_code}"
				onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" /> <label
				for="name"
				style="float: left; line-height: 30px; text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;包裹状态&nbsp;</label>
			<select id="status" name="status" class="input"
				style="width: 200px; float: left;">
				<option value="">请选择</option>
				<option value=-1>异常</option>
				<option value=0>待入库</option>
				<option value=1>已入库</option>
				<option value=2>待发货</option>
				<option value=3>已出库</option>
				<option value=4>空运中</option>
				<option value=5>待清关</option>
				<option value=7>已清关派件中</option>
				<option value=9>已收货</option>
				<option value=20>废弃</option>
				<option value=21>退货</option>
			</select> <label for="name"
				style="float: left; line-height: 30px; text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;支付状态&nbsp;</label>
			<select id="pay_status_custom" name="pay_status_custom" class="input"
				style="width: 200px; float: left;">
				<option value="">请选择</option>
				<option value=3>关税未支付</option>
				<option value=4>关税已支付</option>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="javascript:void(0);" class="button bg-main"
					onclick="queryAll()">查询</a>
			</div>
			<%-- 
            <label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;税号&nbsp;&nbsp;&nbsp;</label>
      		<input type="text" class="input" style="width:200px;float:left;"
      			id="tax_number" name="tax_number"  
            	value="${objectParams.tax_number}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
           	
           	<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;提单号&nbsp;&nbsp;&nbsp;</label>
      		<input type="text" class="input"  style="width:200px;float:left;"
      			id="pick_code" name="pick_code"  
            	value="${objectParams.pick_code}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
            </div>
           	<div style="float:left;height: 32px; margin-left: 10px;margin-top:8px;width:900px;">
           
            
            <div style="float: left;width: 300px;">
           	<label for="name" style="float:left;line-height: 30px;text-align: center;">入库时间</label>
      		<input type="text" class="input" style="width:100px;float:left;margin-left: 10px;"
      			id="time_start" name="time_start"  
      			onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'time_end\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
            	value="${objectParams.time_start}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
            <span style="line-height: 30px; float:left; text-align: center; margin-left: 8px;">到</span>
            <input type="text" class="input"  style="width:100px;float:left;margin-left: 8px"
            	id="time_end" name="time_end"  
            	onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'time_start\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="${objectParams.time_end}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
            </div>
            --%>
		
	</div>

	<div class="panel admin-panel">
		<div class="panel-head" style="height: 50px;">
			<strong style="float: left; text-align: center; margin-left: 10px;">关税清单列表</strong>
		</div>
		<div class="padding border-bottom" style="height: 50px;">
			<shiro:hasPermission name="taxPkgGoods:input">
				<input type="button" class="button button-small border-green"
					onclick="add()" value="导入" />
				<input type="button" class="button button-small border-green"
					onclick="exportPackage()" value="导出" />	
			</shiro:hasPermission>
		</div>
		<table class="table table-hover">
			<tr>
				<th>公司运单号</th>
				<th>包裹状态</th>
				<th>税金支付状态</th>
				<th>税金支付金额</th>
				<%--<th>税单单号</th>
      		<th>物品名称及规格</th>
      		<th>申报数量</th>
      		<th>单位</th>
      		<th>税款金额</th>
      		<th>完税总额</th>
      		<th>税率</th>
      		<th>审核状态</th>--%>
				<th>操作</th>
			</tr>

			<c:forEach var="list" items="${taxPkgGoodsLists}">
				<tr>
					<td>${list.logistics_code }</td>
					<td><c:if test="${list.status=='-2'}">已删除</c:if> <c:if
							test="${list.status=='-1'}">异常</c:if> <c:if
							test="${list.status=='0'}">待入库</c:if> <c:if
							test="${list.status=='1'}">已入库</c:if> <c:if
							test="${list.status=='2'}">待发货</c:if> <c:if
							test="${list.status=='3'}">已出库</c:if> <c:if
							test="${list.status=='4'}">空运中</c:if> <c:if
							test="${list.status=='5'}">待清关</c:if> <c:if
							test="${list.status=='6'}">清关中</c:if> <c:if
							test="${list.status=='7'}">已清关</c:if> <c:if
							test="${list.status=='8'}">派件中</c:if> <c:if
							test="${list.status=='9'}">已收货</c:if> <c:if
							test="${list.status=='20'}">废弃</c:if> <c:if
							test="${list.status=='21'}">退货</c:if></td>
					<td><%-- <c:if test="${list.pay_status_freight=='1' || list.pay_status_freight=='0'}">未支付</c:if> <c:if
							test="${list.pay_status_freight=='2'}">已支付</c:if>  --%>
							<c:if test="${list.pay_status_custom=='0'}"></c:if><c:if
							test="${list.pay_status_custom=='3'}">关税未支付</c:if> <c:if
							test="${list.pay_status_custom=='4'}">关税已支付</c:if></td>
					<td>${list.taxTotal}</td>
					<%-- 
      		<td>${list.tax_number }</td>
      		<td>${list.goods_name }</td>
      		<td>${list.goods_tax_number }</td>
      		<td>${list.unit }</td>
      		<td><fmt:formatNumber value="${list.paid+list.customs_cost}" type="currency" pattern="$#0.00#"/>
      		</td>
      		<td><fmt:formatNumber value="${list.tax_payment_total }" type="currency" pattern="$#0.00#"/>
      		</td>
      		<td><fmt:formatNumber value="${list.tax_rate }" type="percent" pattern="#0.00#%"/></td>
      		<td>${list.tax_status }</td>--%>
					<td><shiro:hasPermission name="taxPkgGoods:edit">
							<a class="button border-blue button-little"
								href="javascript:void(0);" onclick="upd('${list.logistics_code}','${list.package_id}')">编辑</a>&nbsp;&nbsp;
      		</shiro:hasPermission> <%-- <shiro:hasPermission name="taxPkgGoods:delete">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      				onclick="del('${list.goods_id }')">删除</a>
      		</shiro:hasPermission> --%></td>
				</tr>
			</c:forEach>
		</table>
		<div class="panel-foot text-center">
			<jsp:include page="webfenye.jsp"></jsp:include>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">
	var status = "${objectParams.status}";
	if(status){
		 $("#status").val(status);
	}
	var pay_status = "${objectParams.pay_status}";
	if(pay_status){
		 $("#pay_status").val(pay_status);
	}
	var pay_status_custom = "${objectParams.pay_status_custom}";
	if(pay_status_custom){
		 $("#pay_status_custom").val(pay_status_custom);
	}
	function add() {
		var url = "${backServer}/taxPkgGoods/toAddTaxPkgGoods";
		window.location.href = url;
	}

	function upd(goods_id,packid) {
		if(goods_id){
			var url = "${backServer}/taxPkgGoods/toUpdateTaxPkgGoods?goods_id="
				+ goods_id + "&taxPage=${taxPage}&packageId="+packid;
			window.location.href = url;
		}
	}

	function queryAll() {
		var status = $("#status").val();
		//console.log(status);
		//return;
		var pay_status_custom = $("#pay_status_custom").val();
		var logistics_code = $('#logistics_code').val();
		//var tax_number = $('#tax_number').val();
		//var pick_code = $('#pick_code').val();
		//var time_start = $('#time_start').val();
		//var time_end = $('#time_end').val();

		var url = "${backServer}/taxPkgGoods/queryAll?logistics_code="
				+ logistics_code + "&status=" + status + "&pay_status_custom="
				+ pay_status_custom +"&taxPage=${taxPage}&isTaxPage=false";
				//"&time_end=" + time_end + "&pick_code="
				//+ pick_code + 
		window.location.href = url;
	}
	function exportPackage(){
		var status = $("#status").val();
		//console.log(status);
		//return;
		var pay_status = $("#pay_status_custom").val();
		var logistics_code = $('#logistics_code').val();
		var url = "${backServer}/taxPkgGoods/exportPackage?logistics_code="
			+ logistics_code + "&status=" + status + "&pay_status="
			+ pay_status;
		window.location.href = url;
	}
	//回车事件
	$(function() {
		document.onkeydown = function(e) {
			var ev = document.all ? window.event : e;
			if (ev.keyCode == 13) {

				queryAll();
			}
		}
	});

	/* function del(goodsTypeId){
		if(confirm("确认删除吗？")){
			var url = "${backServer}/taxPkgGoods/deleteTaxPkgGoods";
			$.ajax({
				url:url,
				type:'post',
				data:{'goods_id':goodsTypeId},
				success:function(data){
					 alert("删除成功！");
					 window.location.href = "${backServer}/taxPkgGoods/queryAll";
				}
			});
		}
	}	 */
</script>

</html>