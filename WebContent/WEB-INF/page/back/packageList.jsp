<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>
<%
String path=request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    <script src="${backJsFile}/LodopFuncs.js"></script>
<style type="text/css">
    .div-title{
        font-weight: bold;
        float:left;
        width:90px;
        margin-top:5px;
        margin-left:10px;
    }
        .div-input{
        float:left;
        width:220px;
    }
    
    .input-width{
        width:200px;
    }

</style>
<div class="admin">

    <div class="panel admin-panel">
         <input type="hidden" id="status_bk" value="${params.status}">
         <input type="hidden" id="pay_status_bk" value="${params.pay_status}">
         <input type="hidden" id="arrive_status_bk" value="${params.arrive_status}">
         <input type="hidden" id="need_check_out_flag_bk" value="${params.need_check_out_flag}">
        <div class="panel-head"><strong>包裹查询</strong></div>
        </br>
          <form action="${backServer}/pkg/search" method="post" id="myform" name="myform">
             <div style="height: 40px;">
                  <div class="div-title">公司运单号:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="logistics_code" name="logistics_code"
                             onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                             value="${params.logistics_code}"/>
                  </div>
                  <div class="div-title">关联单号:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="original_num" name="original_num"
                             onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                             value="${params.original_num}"/>
                  </div>
                  <div class="div-title">派送单号:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="ems_code" name="ems_code"
                             onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                             value="${params.ems_code}"/>
                  </div>
             </div>
             <div style="height: 40px;">
                  <div class="div-title">用户账号:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="user_name" name="user_name"
                             value="${params.user_name}"/>
                  </div>
                  <div class="div-title">包裹状态:</div>
                  <div class="div-input">
                        <select id="status" name="status" class="input input-width" >
                            <option value="">请选择</option>
                            <option value=-1>异常</option>
                            <option value=0>待入库</option>
                            <option value=1>已入库</option>
                            <option value=2>待发货</option>
                            <option value=3>已出库</option>
                            <option value=4>空运中</option>
                            <option value=5>待清关</option>
                            <option value=7>已清关派件中</option>
                            <option value=9>已收货</option>
                            <option value=20>废弃</option>
                            <option value=21>退货</option>
                        </select>
                  </div>
                  
                  <div class="div-title">支付状态:</div>
                  <div class="div-input">
                      <select id="pay_status" name="pay_status" class="input input-width" >
                            <option value="">请选择</option>
                            <option value=1>运费待支付</option>
                            <option value=2>运费已支付</option>
                            <option value=3>关税待支付</option>
                            <option value=4>关税已支付</option>
                      </select>
                  </div>
             </div>

             <div style="height: 40px;">
                  <div class="div-title">提单号:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="pick_code" name="pick_code"
                             onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                             value="${params.pick_code}"/>
                  </div>
                  <div class="div-title">起始时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
                  <div class="div-title">终止时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
                            value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
             </div>
             <div style="height: 40px;">
                <div class="div-title">收件人:</div>
                <div class="div-input">
                      <input type="text" class="input input-width" id="receiver" name="receiver"
                             value="${params.receiver}"/>
                </div>
                <div class="div-title">空运单号:</div>
                <div class="div-input">
                      <input type="text" class="input input-width" id="flight_num" name="flight_num"
                             value="${params.flight_num}"/>
                </div>
               <div class="div-title">到库状态:</div>
                  <div class="div-input">
                      <select id="arrive_status" name="arrive_status" class="input input-width" >
                            <option value="">请选择</option>
                            <option value=0>未到库</option>
                            <option value=1>已到库</option>
                      </select>
                  </div>
             </div>

             <div style="height: 40px;">
                <div class="div-title">仓库:</div>
                <div class="div-input">
                        	<select  class="input" id="overseasAddress_id" name="overseasAddress_id" style="width:200px" >
                   		  <option value="">---------选择仓库---------</option>
                    		<c:forEach var="userOverseasAddress" items="${overseasAddressList}">
                    			<option value="${userOverseasAddress.id}">${userOverseasAddress.warehouse}</option>
                    		</c:forEach>
                   		</select>
                </div>
                <div class="div-title">业务员:</div>
                <div class="div-input">
                        	<select  class="input" id="business_name" name="business_name" style="width:200px" >
                   		  <option value="">---------选择业务员---------</option>
                    		<c:forEach var="businessName" items="${businessNameList}">
                    			<option value="${businessName.business_name}">${businessName.business_name}</option>
                    		</c:forEach>
                   		</select>
                </div>
<!--                 <div class="div-title">物流单号:</div>
                <div class="div-input">
                      <input type="text" class="input input-width" id="express_num" name="express_num"
                             value="${params.express_num}"/>
                </div> -->
                <div class="div-title">包裹渠道:</div>
                <div class="div-input">
                          <select  class="input" id="express_package" name="express_package" style="width:200px" >
                   		  <option value="-1">---------查询渠道---------</option>
						  <option value="0">默认</option>
						  <option value="1">A</option>
						  <option value="2">B</option>
						  <option value="3">C</option>
						  <option value="4">D</option>
						  <option value="5">E</option>
                   		</select>
                </div>
             </div> 
			 <div class="div-title">需要取出:</div>
                <div class="div-input">
                          <select  class="input" id="need_check_out_flag" name="need_check_out_flag" style="width:200px" >
                   		  <option value="">---------是否需取出---------</option>
						  <option value="Y">是</option>
						  <option value="N">否</option>
                   		</select>
                </div>
          </form>
             <div class="padding border-bottom">
                  <input type="button" class="button bg-main" onclick="search()" value="查询" />
             </div>
             <table class="table">
                  <tr>
                    <th width="12%">公司运单号</th>
                    <th width="10%">关联单号</th>
                    <th width="12%">派送单号</th>
                    <th width="16%">用户账号</th>
                    <th width="8%">支付状态</th>
                    <th width="10%">包裹状态</th>
					<th width="10%">渠道</th>
					<th width="12%">收件人</th>
                    <th width="18%">包裹操作</th>
                  </tr>    
             </table>
             <table class="table table-hover">
               <c:forEach var="pkg"  items="${pkgList}">
                   <tr> 
                      <td width="12%">${pkg.logistics_code}</td>
                      <td width="10%"><div style="width:180px;word-wrap:break-word;">${pkg.original_num}</div></td>
                      <td width="12%">${pkg.ems_code}</td>
                      <td width="16%">${pkg.user_name}</td>
                      <td width="8%">
                         <c:if test="${pkg.pay_status==1}">运费待支付</c:if>
                         <c:if test="${pkg.pay_status==2}">运费已支付</c:if>
                         <c:if test="${pkg.pay_status==3}">关税待支付</c:if>
                         <c:if test="${pkg.pay_status==4}">关税已支付</c:if>
                      </td>
                      <td width="10%">
                         <c:if test="${pkg.status==-1}">异常</c:if>
                         <c:if test="${pkg.status==0}">待入库</c:if>
                         <c:if test="${pkg.status==1}">已入库</c:if>
                         <c:if test="${pkg.status==2}">待发货</c:if>
                         <c:if test="${pkg.status==3}">已出库</c:if>
                         <c:if test="${pkg.status==4}">空运中</c:if>
                         <c:if test="${pkg.status==5}">待清关</c:if>
                         <c:if test="${pkg.status==7}">已清关派件中</c:if>
                         <c:if test="${pkg.status==9}">已收货</c:if>
                         <c:if test="${pkg.status==20}">废弃</c:if>
                         <c:if test="${pkg.status==21}">退货</c:if>
                      </td>
					  <td width="10%">
                         <c:if test="${pkg.express_package==0}">默认</c:if>
                         <c:if test="${pkg.express_package==1}">A</c:if>
                         <c:if test="${pkg.express_package==2}">B</c:if>
                         <c:if test="${pkg.express_package==3}">C</c:if>
						 <c:if test="${pkg.express_package==4}">D</c:if>
						 <c:if test="${pkg.express_package==5}">E</c:if>
                      </td>
                      <td width="12%">${pkg.receiver}
                      <p>
                      ${pkg.mobile}
                      </td>
                      <td width="18%">
                      <shiro:hasPermission name="pkgquery:select">
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="detail('${pkg.package_id}')">详情</a>
                      </shiro:hasPermission><shiro:hasPermission name="pkgquery:edit">
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="update('${pkg.package_id}')">编辑</a>
                      </shiro:hasPermission><shiro:hasPermission name="pkgquery:print">
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="ShowCataDialog('${pkg.package_id}')">预览打印</a>
                      </shiro:hasPermission>
					  <!--
                      <shiro:hasPermission name="pkgquery:print">
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="printPackage('${pkg.package_id}')">打印</a>
                      </shiro:hasPermission>
					  -->
                      </td>
                  </tr>
               </c:forEach>
             </table>
                  <div class="panel-foot text-center">
                   <jsp:include page="webfenye.jsp"></jsp:include>
                 </div>
             </div>
</div>
</body>
<script type="text/javascript">
function initSearchField(){
	$('#overseasAddress_id').val('${params.overseasAddress_id}');
	$('#business_name').val('${params.business_name}');
	$('#express_package').val('${params.express_package}');
	$('#need_check_out_flag').val('${params.need_check_out_flag}');
}
initSearchField(); 
//选择框选中
$("#status").val($("#status_bk").val());
//选择框选中
$("#pay_status").val($("#pay_status_bk").val());

$("#arrive_status").val($("#arrive_status_bk").val());

//初始化焦点
$('#logistics_code').focus();

function search(){

    document.getElementById('myform').submit();
}

function detail(package_id){
    var url = "${backServer}/pkg/detail?package_id="+package_id;
    window.location.href = url;
}
function update(package_id){
    var url = "${backServer}/pkg/update?package_id="+package_id;
    window.location.href = url;
}

function ShowCataDialog(package_id) {
    layer.open({
        title :'打印面单',
        type: 2,
        shadeClose: true,
        shade: 0.8,
        offset: ['10px', '300px'],
        area: ['590px', '650px'],
        content:'${backServer}/pkg/print?package_id='+package_id 
    }); 
}

//回车事件
      $(function(){
        document.onkeydown = function(e){
            var ev = document.all ? window.event : e;
            if(ev.keyCode==13) {

                search();
             }
        }
      });

function printPackage(package_id) {
	LODOP=getLodop();  
	LODOP.PRINT_INIT("打印面单");
	LODOP.SET_PRINT_PAGESIZE(1,1016,1524," ");
	LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","73%");
	LODOP.ADD_PRINT_URL(0,0,'100%','100%','<%=basePath%>backprint?package_id='+package_id);
	LODOP.PRINT();//直接打印
	//LODOP.PRINTA();//选择打印机打印
	//LODOP.PREVIEW();//打印预览	
}
</script>

</html>