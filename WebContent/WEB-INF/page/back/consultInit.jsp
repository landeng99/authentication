<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
 <link rel="stylesheet" href="${backJsFile}/jquery-foxibox/style/jquery-foxibox-0.2.css">
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
	
</style>
<body>
	<div class="admin">
		<div class="admin_context">
		<div class="panel admin-panel">
			<div class="panel-head" style="height: 50px;">
				<strong style="float:left;text-align: center;margin-left: 10px;">海淘咨询信息列表</strong>
			</div>
			<div class="padding border-bottom" style="height: 50px;">
				<input type="button"  
				class="button button-small border-green"
				onclick="uploadBanner()" value="新建" />
			</div>
			
			<table class="table table-hover">
				<tr><th>图片</th>
				<th>标题</th>
				<th>状态</th>
				<th>操作</th></tr>
			<c:forEach var="consult" items="${consultInfoLists}">
				<tr><td>
					<a href="${consult.forward_url}"  rel="[gall1]">
						<img src="${resource_path}/${consult.display_image}" height="40"/>
					</a>
				</td>
				<td>${consult.consult_title}</td>
				<td><c:if test="${consult.status==1 }">启用</c:if>
					<c:if test="${consult.status==2 }">禁用</c:if></td>
				<td>
				<c:if test="${consult.status==2 }">
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="updateBanner('${consult.seq_id }')">启用</a>&nbsp;&nbsp;
				</c:if>
				<c:if test="${consult.status==1 }">
					<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateBanner('${consult.seq_id }')">禁用</a>&nbsp;&nbsp;
      			</c:if>
      				<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="del('${consult.seq_id }')">删除</a>&nbsp;&nbsp;
				</td></tr>	
			</c:forEach>
			</table>
			
			<div class="panel-foot text-center">
	      		<jsp:include page="webfenye.jsp"></jsp:include>
	      	</div>
		</div>
		</div>
	</div>
</body>
<script src="${backJsFile}/jquery-foxibox/script/jquery-foxibox-0.2.min.js"></script>
<script src="${backJsFile}/jquery-migrate-1.2.1.min.js"></script>


<script type="text/javascript">
	
	//图片播放
	$(document).ready(function(){
	  $('a[rel]').foxibox();
	});

	function uploadBanner(){
		var url = "${backServer}/consult/toConsultInfo";
		window.location.href = url;
	}
	
	//更新状态,是否启用
	function updateBanner(seq_id){
		var url = "${backServer}/consult/updateConsult?seq_id="+seq_id;
		$.ajax({
			url:url,
			type:'GET',
			success:function(data){
				 window.location.href = "${backServer}/consult/queryAll";
			}
		});
	}
	
	function del(seq_id){
		if(confirm("确认删除吗？")){
			var url = "${backServer}/consult/deleteConsultInfo?seq_id="+seq_id;
			$.ajax({
				url:url,
				type:'GET',
				success:function(data){
					 alert("删除成功！");
					 window.location.href = "${backServer}/consult/queryAll";
				}
			});
		}
	}
</script>
</html>