<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">修改关税</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
        <div class="form-group">
			<form id="myform" name="myform">
                <input type="hidden" id="goods_id" name="goods_id" value="${taxPkgGoodsPojo.goods_id}">
				<div class="form-group">
                    <div class="label"><label for="name">税号[不可改]</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="tax_number" name="tax_number" style="width:200px" 
                    		value="${taxPkgGoodsPojo.tax_number }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" readonly="true"/>
                   		<span style="color: red;" id="tax_numberNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="name">物品名称及规格[不可改]</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="goods_name" name="goods_name" style="width:200px" 
                    		value="${taxPkgGoodsPojo.goods_name }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" readonly="true"/>
                   		<span style="color: red;" id="goods_nameNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="name">税率</label></div>
                    <div class="field">
                    <fmt:formatNumber value="${taxPkgGoodsPojo.tax_rate}" var="tax_rate_fmt"  type="currency" pattern="#0.0000#"/>
                    	<input type="text" class="input" id="tax_rate" name="tax_rate" style="width:200px" 
                    		value="${tax_rate_fmt}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" />
                   		<span style="color: red;" id="tax_rateNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="name">计量单位</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="unit" name="unit" style="width:200px" 
                    		value="${taxPkgGoodsPojo.unit }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
                   		<span style="color: red;" id="unitNote"></span>
                    </div>
                </div>
                <div class="form-button"><a href="javascript:void(0);" class="button bg-main" onclick="save()">提交</a>
                	<a href="javascript:history.go(-1);" class="button bg-main">返回</a></div>
             </form>
        </div>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
	function save(){
		var taxRate = $('#tax_rate').val();
		var unit = $('#unit').val(); 
		var goods_id = $('#goods_id').val();  
		if(taxRate==""){
			alert("税率不能为空");
			document.myform.name.focus();
			return false;
		}
		if(taxRate > 1){
			alert("税率不能大于100%");
			document.myform.name.focus();
			return false;
		}
		if(unit==""){
			alert("计量单位不能为空");
			document.myform.link.focus();
			return false;
		}
		var url = "${backServer}/taxPkgGoods/updateTaxPkgGoodsSubmit?tax_rate="+taxRate+
				"&unit="+unit+"&goods_id="+goods_id+"&taxPage=${taxPage}&isTaxPage=true";
		/* $.ajax({
			url:url,
			type:"POST",
			data:{"tax_rate":taxRate,"unit":unit,"goods_id":goods_id},
			success:function(data){
				if(data){
		            alert("修改成功"); */
		            window.location.href = url;
			/* 	}
			}
		}); */
	}
	 $(function(){
			$("#tax_rate").blur(function(){
					var taxRate = $("#tax_rate").val();
					var len = $("#tax_rate").val().length;
					 if(len==0||taxRate==null){
							$("#tax_rateNote").html("税率不能为空！");
							$("#myform").attr("onclick","return false");
							}else{
							$("#myform").attr("onclick","return true");
								}
						});
				});
	 $(function(){
			$("#unit").blur(function(){
					var unit = $("#unit").val();
					var len = $("#unit").val().length;
					 if(len==0||unit==null){
							$("#unitNote").html("单位不能为空！");
							$("#myform").attr("onclick","return false");
							}else{
							$("#myform").attr("onclick","return true");
								}
						});
				});
	
</script>
</body>