<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
	.field-item{
		float:left;
		margin-right: 20px;
	}
	.field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 80px;
	}
	
	.field-item .interval_label{
		width: 10px;
		float:left;
		margin: auto 10px;
	}
	
	.field-item .field{
		float:left;
		width: 200px;
	}
    .div-title{
        font-weight: bold;
        float:left;
        width:90px;
        margin-top:5px;
        margin-left:10px;
    }
        .div-input{
        float:left;
        width:220px;
    }
    
    .input-width{
        width:200px;
    }
    
    .even {
	background-color: #C1CDCD;
	}
.td1{border:solid #c00; border-width:0px 1px 1px 0px; padding-left:10px;}
.table1{border:solid #c00; border-width:1px 0px 0px 1px;}
}

</style>
<div class="admin">
    <div class="panel admin-panel">
        <div class="panel-head"><strong>短信发送记录</strong></div>
        </br>
          <form action="${backServer}/warningSmsLog/search" method="post" id="myform" name="myform">

             <div style="height: 40px;">
                  <div class="div-title">手机号:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="receriver_moblie" name="receriver_moblie"
                             value="${params.receriver_moblie}" onkeyup="value=$.trim(value)"/>
                  </div>
                  <div class="field-item">
                	 <div class="label"><label for="status">发送状态:</label></div>
                    <div class="field">
                        	<select  class="input" id="status" name="status" style="width:150px" >
                   		  <option value="">---选择发送状态---</option>
                    	  <option value="0">成功</option>
                    	  <option value="1">失败</option>
                    	  <option value="-1">待发送</option>
                   		</select>
                    </div>
                </div>               
             
                  <div class="div-title">发送起始时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
                  <div class="div-title">发送终止时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
                            value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
             </div>
          </form>
             <div class="padding border-bottom">
                  <input type="button" class="button bg-main" onclick="search()" value="查询" />
             </div>
             <table class="table">
                    <tr>
                    <th class="td" width="25%">手机号</th>
                    <th class="td" width="25%">发送时间</th>
                    <th class="td" width="25%">发送状态</th>
                    <th class="td" width="25%">操作</th>
                  </tr>
               <c:forEach var="warningSms"  items="${warningSmsLogList}" varStatus="status">
                   <tr class="${status.count % 2 == 0 ? 'odd' : 'even'}"> 
                      <td class="td" width="25%">${warningSms.receriver_moblie}</td>
                      <td class="td" width="25%"><fmt:formatDate value="${warningSms.send_time}" pattern="yyyy-MM-dd HH:mm"/></td>
                      <td class="td" width="25%"><c:if test="${warningSms.status==0}">成功</c:if><c:if test="${warningSms.status==1}">失败</c:if><c:if test="${warningSms.status==-1}">待发送</c:if></td>
                      <td class="td" width="25%"><a class="button border-blue button-little"
							href="javascript:void(0);" onclick="reSend('${warningSms.seq_id }')">重新发送</a></td>
                  </tr>
               </c:forEach>
             </table>
                  <div class="panel-foot text-center">
                   <jsp:include page="webfenye.jsp"></jsp:include>
                 </div>
             </div>
</div>
</body>
<script type="text/javascript">
function initSearchField(){
	$('#status').val('${params.status}');
}
initSearchField(); 

function search(){

    document.getElementById('myform').submit();
}

	//回车事件
	$(function() {
		document.onkeydown = function(e) {
			var ev = document.all ? window.event : e;
			if (ev.keyCode == 13) {

				search();
			}
		}
	});

	function reSend(seq_id) {
		var url = "${backServer}/warningSmsLog/reSendSms?seq_id=" + seq_id;
		$.ajax({
			url : url,
			type : 'GET',
			success : function(data) {
				if(data.flag=='Y')
				{
					alert("发送成功！");
				}else
				{
					alert("发送失败！");
				}
				window.location.href = "${backServer}/warningSmsLog/search";
			}
		});
}
</script>

</html>