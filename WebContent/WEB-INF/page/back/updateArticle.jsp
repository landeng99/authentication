<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
	<title>修改文章</title>
	<link rel="stylesheet" href="<%=basePath %>resource/css/default2.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>resource/css/prettify.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>back/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>back/css/admin.css">
	<script charset="utf-8" src="<%=basePath %>resource/js/kindeditor.js"></script>
	<script charset="utf-8" src="<%=basePath %>resource/js/zh_CN.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="content"]', {
				uploadJson : '<%=basePath %>kindeditor/uploadImg',
				fileManagerJson : '<%=basePath %>kindeditor/fileMgr',
				allowFileManager : true,
				afterBlur: function(){this.sync();}
			});
		});
	</script>
</head>
<body>
<div class="admin">
	<div class="tab">
	<div class="form-group">
		<form method="post" action="${backServer}/article/updataArticle" onsubmit="return check()">
			<input type="hidden" name="id" value="${id}">
			<div style="display: block;">
				<div class="form-group" style="height:40px">
					<div style="float:left;">文章类别:</div>
					<div style="float:left;margin-left:20px;">
						<select name="classify_id" id="classify_id">
							<c:if test="${id==0}">
								<option value="0">--请选择--</option>
							</c:if>
							<c:forEach items="${artClaList}" var="list">
								<c:if test="${classify_id == list.id}">
									<option value="${list.id}" selected="selected">${list.name}</option>
								</c:if><c:if test="${classify_id != list.id}">
									<option value="${list.id}">${list.name}</option>
								</c:if>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group" style="height:40px">	
					<div style="float:left;">标题:</div>
					<div style="float:left;margin-left:46px;"><input type="text" name="title" value="${title}" id="title"/></div>
				</div>
				<div class="form-group" style="height:40px">	
					<div style="float:left;">作者:</div>
					<div style="float:left;margin-left:46px;"><input type="text" name="author" value="${author}" readonly="readonly" id="author"/></div>
				</div>
				<div class="form-group" style="height:430px;overflow: auto;">
					<div >内容:</div>
					<div style="margin-left: 80px;"><textarea name="content" id="content1" cols="100" rows="8" style="width:900px;height:400px;" >${content}</textarea> </div>
				</div>
				<div class="form-group" style="margin-left: 80px;">
					<input type="submit" name="button" value="保存" />
					<input type="button" name="button" value="返回" onclick="returnPage()"/>
				</div>
			</div>	
		</form>
	</div>
	</div>
</div>
<script type="text/javascript">
	function returnPage(){
		history.go(-1);
	}
	function check(){
		   var name = document.getElementById("classify_id").value;
		   var title = document.getElementById("title").value;
		   var content = document.getElementById("content1").value;
		   if(name == 0){
		        alert("请选择文章类别");
		        return false;
		   }
		   if(title == null || title ==''){
		        alert("请输入标题");
		        return false;
		   }
		   if(content==""){
		        alert("请输入文章内容");
		        return false;
		   }
		   return true;
	}
</script>
</body>
</html>