<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
<body>
	<div class="admin">
		<form id="fom" action="${backServer}/pallet/initadd" method="post">
		<input type="hidden" name="pick_id" value="${pick_id}"/>
		<input type="hidden" name="seaport_id" value="${seaport_id}"/>
		<input type="hidden" name="pick_code" value="${pick_code}"/>
		
		<div style="height:500px;margin-top:200px;">
			<div style="color:#86be1f;font-size:30px;text-align: center;">托盘创建成功！<img alt="托盘创建成功！" src="${backImgFile}/ok.jpg"></div>
	 		<div class="form-button" style="text-align: center;margin-top:20px;">
		 		<input type="submit"  class="button bg-main" value="继续创建托盘">
	  	   		<a class="button bg-main" href="${backServer}/pickorder/initedit?pick_id=${pick_id}">返回提单</a>
		 </div>	
		</div>
		</form>
	</div>
</body>
</html>