<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style>
	.div_tr{float:left;height: 40px; margin-left: 10px; clear: both;}
	.div_tr div{float: left;width: 350px;}
	.div_tr_td_label{float:left;line-height: 30px;text-align: center;width: 60px;}
	.div_tr_td_text{width:200px;margin-left: 10px;float:left;}
</style>
<div class="admin">
    <div class="tab">
    	<div class="tab-head">
    		<ul class="tab-nav">
          		<li class="active"><a href="#tab-set">优惠券</a></li>
        	</ul>
      	</div>
      	<div class="tab-body">
        	<br />
        	<form action="${backServer}/coupon/couponDetail" method="post" id="myform" name="myform">
        	<div class="tab-panel active" id="tab-set">
        		<input type="hidden" id="couponId" name="couponId" value="${couponPojo.couponId }">
				<div class="form-group">
						<div class="padding" id="findDisplayDiv" style="width: 90%;">
							<div class="div_tr">
								<div>
									<label for="name" class="div_tr_td_label">编号</label>
									<input type="text" disabled="disabled" class="input div_tr_td_text"  
										id="couponCode" name="couponCode"
										value="${couponPojo.couponCode}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
								</div><div>
									<label for="name" class="div_tr_td_label">名称</label>
									<input type="text"  disabled="disabled" class="input div_tr_td_text"  
										id="coupon_name" name="coupon_name"  
										value="${couponPojo.coupon_name}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
								</div>
							 
							 	<div>
								 	<label for="name" class="div_tr_td_label">状态</label>
									<select id="status"  disabled="disabled" name="status" class="input div_tr_td_text" >
										<c:if test="${couponPojo.status==2}">
										<option value="2" selected="selected">禁用</option>
										<option value="1">启用</option>
										</c:if><c:if test="${couponPojo.status==1}">
										<option value="2">禁用</option>
										<option value="1" selected="selected">启用</option>
										</c:if><c:if test="${couponPojo.status !=2 && couponPojo.status !=1 }">
										<option value="2">禁用</option>
										<option value="1">启用</option>
										</c:if>
									</select>
								</div>
								</div><div class="div_tr">	
								<div>
									<label for="name" class="div_tr_td_label">是否叠加</label>
									<select id="isRepeatUse"  disabled="disabled" name="isRepeatUse" class="input div_tr_td_text" >
										
										
										<option value="1" 
										<c:if test="${couponPojo.isRepeatUse==1}">
										selected="selected"
										</c:if>
										>不可叠加</option>
										<option value="2"
										<c:if test="${couponPojo.isRepeatUse==2}">
										selected="selected"
										</c:if>
										>可以叠加</option>
										
										
									</select>
								</div>
							
								<div>
									<label for="name" class="div_tr_td_label">面值</label>
									<input type="text"  disabled="disabled" class="input div_tr_td_text" 
										id="denomination" name="denomination"  
										value="${couponPojo.denomination }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="denominationNote"></span>
								</div>
								<div>
									<label for="name" class="div_tr_td_label">数目</label>
									<input type="text"  disabled="disabled" class="input div_tr_td_text"
										id="quantity" name="quantity"  
										value="${couponPojo.quantity }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="quantityNote"></span>
								</div>
							</div><div class="div_tr">	
								<div>
									<label for="name" class="div_tr_td_label">开始时间</label>
									<input type="text"  disabled="disabled" class="input div_tr_td_text"
										id="effectTime" name="effectTime" 
										onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'expirationTime\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
										value="${couponPojo.effectTime}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="effectTimeNote"></span>
								</div><div>
									<label for="name" class="div_tr_td_label">截止时间</label>
									<input type="text"  disabled="disabled" class="input div_tr_td_text"
										id="expirationTime" name="expirationTime"  
										onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'effectTime\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
										value="${couponPojo.expirationTime}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="expirationTimeNote"></span>
								</div>
							
								<div>
									<label for="name" class="div_tr_td_label">有效天数</label>
									<input type="text"  disabled="disabled" class="input div_tr_td_text"
										id="validDays" name="validDays"  
										value="${couponPojo.validDays }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
									<span style="color: red;" id="validDaysNote"></span>
								</div>
							</div>
							<div class="div_tr">	
								<div>
									<label for="name" class="div_tr_td_label">客户账号</label>
									<input type="text"  class="input div_tr_td_text"
										id="account" name="account"  
										value="${params.account}" />
								</div>
							</div>							
						</div>
							 <div class="form-button" style="float:left;margin-left:100px; clear: both;">
							 <input type="button" class="button bg-main" onclick="search()" value="查询" />
								<a href="javascript:history.go(-1)" class="button bg-main">返回</a>
							 </div>
        		</div>
        	</div>
        	</form>
      	</div>

		<table class="table table-hover">
			<tr><th>客户账号</th>
			<th>已发放优惠券名称</th>
				<th>已发放优惠券面值</th>
				<th>已发放优惠券数量</th>
				<th>是否使用|(使用数量/下发数量)</th>
				<th>发放时间</th>
			</tr>

			<c:forEach var="list"  items="${userCouponList}">
			<tr><td>${list.account }</td>
			<td>${list.coupon_name }</td>
				<td><fmt:formatNumber value="${list.denomination }" type="currency" pattern="$#0.00#"/>
				</td>
				<td>
					${list.quantity }
				</td>
				<td>
					<c:if test="${list.haveUsedCount==list.quantity }">已使用</c:if>
					<c:if test="${list.haveUsedCount<list.quantity }">${list.haveUsedCount }/${list.quantity }</c:if></td>
				
				<td>
					${list.create_time }
				</td>
			</tr>	
			</c:forEach>
		</table>
		<div class="panel-foot text-center">
			<jsp:include page="webfenye.jsp"></jsp:include>
		</div>
	</div>	
</div>
<script type="text/javascript">
function search(){

    document.getElementById('myform').submit();
}
</script>
</body>