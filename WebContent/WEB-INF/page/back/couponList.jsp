<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 80px;
	}
	
	.field-group .field-item .interval_label{
		width: 10px;
		float:left;
		margin: auto 10px;
	}
	
	.field-group .field-item .field{
		float:left;
		width: 200px;
	}
	
</style>
<body>
<div class="admin">
	 <div class="padding border-bottom" id="findDisplayDiv" style="height: 200px;">
		<div class="field-group">
			<div class="field-item">
				<div class="label"><label  for="name" >编号:</label></div>
				 <div class="field">
				<input type="text" class="input"  id="couponCode" name="couponCode" style="width:200px;" 
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
					</div>
			</div>
			<div class="field-item">
				<div class="label"><label for="name">名称:</label></div>
				<div  class="field">
				<input type="text" class="input"   id="coupon_name" name="coupon_name" style="width:200px;" 
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
			   </div>
			</div>
			
			<div class="field-item">
				<div class="label"><label for="name" >状态：</label></div>
				<div class="field">
				<select id="status" name="status" class="input" style="width:80px">
				<option value="-1" selected="selected">请选择</option>
					<option value="2">禁用</option>
					<option value="1">启用</option>
				</select>
				</div>
			</div>
			<div class="field-item">
				<div class="label"><label for="name" >是否叠加：</label></div>
				<div class="field">
				<select id="isRepeatUse" name="isRepeatUse" class="input" style="width:80px">
					<option value="-1" selected="selected">请选择</option>
					<option value="1">不可叠加</option>
					<option value="2">可以叠加</option>
				</select>
				</div>
			</div>
			
		   <div class="field-item">
				<div class="label"><label for="name" >面值:</label></div>
				<div class="field">
				<input type="text" class="input" style="width:100%;" id="denominationStrat" name="denominationStrat"  
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
				 </div>
				<div class="interval_label"><label for ="interval">~</label></div>
				<div class="field">
				<input type="text" class="input"  style="width:100%;" id="denominationEnd" name="denominationEnd"  
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
				</div>	
			</div>
			<div class="field-item">
		     	<div class="label"> 
		     		<label for="name" >是否过期:</label>
				</div>
				<div class="field">
					<select id="guoq" name="guoq" class="input" style="width:80px;">
						<option value="-1" selected="selected">请选择</option>
						<option value="1">过期</option>
						<option value="2">未过期</option>
					</select>
				</div>
			 </div>
				<div class="field-item">
					<div class="label">
						<label for="name">有效期:</label>
					</div>
					<div class="field">
					<input type="text" class="input" style="width:100%" id="effectTime" name="effectTime"value="" 
						onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'expirationTime\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
					</div>
					<div class="interval_label"><label for ="interval">~</label></div>
					<div class="field">
					 	<input type="text" class="input"  style="width:100%;" id="expirationTime" name="expirationTime"  value="" 
						onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'effectTime\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
					</div>
				</div>
			
		 </div>
		  <div style="clear: both"></div>
			<div class="form-button">
				<a href="javascript:void(0);" class="button bg-main icon-search" onclick="queryAll()">查询</a>
			</div>
	</div>
 	<div class="panel admin-panel">
		<div class="panel-head" style="height: 50px;">
			<strong style="float:left;text-align: center;margin-left: 10px;">优惠券列表</strong>
		</div>
		<div class="padding border-bottom" style="height: 50px;">
<shiro:hasPermission name="coupon:add">			
			<input type="button"  
			class="button button-small border-green"
			onclick="add()" value="添加" />
</shiro:hasPermission>
		</div>
		<table class="table table-hover">
			<tr><th>编号</th>
			<th>名称</th>
				<th>面值</th>
				<th>总数量</th>
				<th>剩余数量</th>
				<th>有效时间</th>
				<th>状态</th>
				<th>操作</th>
			</tr>

			<c:forEach var="list"  items="${couponLists}">
			<tr><td>${list.couponCode }</td>
			<td>${list.coupon_name }</td>
				<td><fmt:formatNumber value="${list.denomination }" type="currency" pattern="$#0.00#"/>
				</td>
				<td>
				   <c:if test="${list.total_count==-1}">无限</c:if>
				   <c:if test="${list.total_count>-1}">${list.total_count }</c:if>
				</td>
				<td>
				   <c:if test="${list.left_count==-1}">无限</c:if>
				   <c:if test="${list.left_count>-1}">${list.left_count }</c:if>
				</td>
				<td>
					<c:if test="${ null != list.expirationTime && list.expirationTime != '' }">
						${list.expirationTime }
					</c:if><c:if  test="${ null == list.expirationTime || list.expirationTime == '' }">
						<c:if  test="${list.validDays==-1}">
						无限期
					   </c:if>
						<c:if  test="${list.validDays>0}">
						领取时的<font color="red">${list.validDays }</font>天内
					   </c:if>
					</c:if></td>
				<td>
					<c:if test="${list.status==2 }">禁用</c:if>
					<c:if test="${list.status==1 }">启用</c:if></td>
				<td>
				<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="detail('${list.couponId }')">查看</a>&nbsp;&nbsp;
				<shiro:hasPermission name="coupon:edit">
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="upd('${list.couponId }')">编辑</a>&nbsp;&nbsp;
				</shiro:hasPermission>
				 <c:if test="${list.can_remove==0}">
				<shiro:hasPermission name="coupon:delete">
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="del('${list.couponId }')">删除</a>&nbsp;&nbsp;
				</shiro:hasPermission>
				</c:if>
				<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="pushToUser('${list.couponId }')">下发</a>&nbsp;&nbsp;
						</td>
			</tr>	
			</c:forEach>
		</table>
		<div class="panel-foot text-center">
			<jsp:include page="webfenye.jsp"></jsp:include>
		</div>
	</div>
</div>
</body>
<script type="text/javascript"> 
	
	function add(){
		var url = "${backServer}/coupon/toAddCoupon";
		window.location.href = url;
	}

	function upd(couponId){	
		var url = "${backServer}/coupon/touUpdateCoupon?couponId="+couponId;
		window.location.href = url;
	}
	
	function detail(couponId){	
		var url = "${backServer}/coupon/couponDetail?couponId="+couponId;
		window.location.href = url;
	}
	
	function pushToUser(couponId){	
		var url = "${backServer}/coupon/prePushCouponToUser?couponId="+couponId;
		window.location.href = url;
	}
	function queryAll(){
		var couponCode = $("#couponCode").val();
		var coupon_name = $("#coupon_name").val();
		var status = $("#status").val(); // $("#ddlregtype ").val(); 
		var isRepeatUse = $("#isRepeatUse").val();
		var guoq = $("#guoq").val(); 
		var effectTime = $("#effectTime").val();
		var expirationTime = $("#expirationTime").val();
		var denominationStrat = $("#denominationStrat").val();
		if("" == denominationStrat){
			denominationStrat = -1;
		}
		var denominationEnd = $("#denominationEnd").val();
		if("" == denominationEnd){
			denominationEnd = -1;
		} 
		var url = "${backServer}/coupon/queryAll?couponCode="+couponCode+
				"&coupon_name="+coupon_name+
				"&status="+status+
				"&isRepeatUse="+isRepeatUse+
				"&guoq="+guoq+
				"&effectTime="+effectTime+
				"&expirationTime="+expirationTime+
				"&denominationStrat="+denominationStrat+
				"&denominationEnd="+denominationEnd; 
		window.location.href = url;
	}
	function del(couponId){
		if(confirm("确认删除吗？")){
			var url = "${backServer}/coupon/deleteCoupon?couponId="+couponId;
			$.ajax({
				url:url,
				type:'GET',
				success:function(data){
					 alert("删除成功！");
					 window.location.href = "${backServer}/coupon/queryAllOne";
				}
			});
		}
	}
	
    //回车事件
    $(function(){
      document.onkeydown = function(e){
          var ev = document.all ? window.event : e;
          if(ev.keyCode==13) {

              queryAll();
           }
      }
    });
</script>
</html>