<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">提现申请{前台做吧}</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
	        <input type="hidden" id="userId" value="${WITHDRAWALLOGGET_USER_ID }">
			<div class="form-group" style="clear: both;">
				<div class="label" style="float: left;"><label for="name">账户余额</label></div>
		         <div class="field"style="float: left; margin-left: 15px;"><font color="red" size="4">${WITHDRAWALLOGGET_ACCOUNTLOG_AMOUNT_TOOL }</font>
		         </div>
			</div>
			<div class="form-group" style="clear: both;">
				<form id="myform" name="myform">
	                <div class="form-group" style="clear: both;">
	                    <div class="label" style="float: left;"><label for="name">提现金额</label></div>
	                    <div class="field"style="float: left; margin-left: 15px;">
	                    	<input type="text" class="input" id="amount" name="amount" style="width:200px" 
	                    		value="${amount }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
	                   		<span style="color: red;" id="amountNote"></span>
	                    </div>
	                </div>
	                <div class="form-group" style="clear:both; height: 34px;">
		                <div class="form-button" style="float: left; margin-left: 15px;"><a href="javascript:void(0);" class="button bg-main" onclick="save()">提现</a></div>
		                <div class="form-button" style="float: left; margin-left: 40px;"><a href="javascript:void(0);" class="button bg-main" onclick="back()">返回</a></div>
	            	</div>
                 </form>
	        </div>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
function back(){
	window.location.href = "${backServer}/withdrawalLog/queryAll";
}
	function save(){
		var userId = $('#userId').val();
		var amount = $('#amount').val();
		if(amount==""){
			alert("提现金额不能为空");
			document.myform.name.focus();
			return false;
		}
		var url = "${backServer}/withdrawalLog/addWithdrawalLog";
		$.ajax({
			url:url,
			type:"POST",
			data:{"amount":amount,"userId":userId},
			success:function(data){
				if(data){
		            alert("提现申请成功,请等待审核");
		            window.location.href = "${backServer}/withdrawalLog/queryAll";
				}
			}
		});
	}
	$(function(){
		$("#amount").blur(function(){
				var tax_code = $("#amount").val();
				var len = $("#amount").val().length;
				 if(len==0||tax_code==null){
						$("#amountNote").html("税号不能为空！");
						$("#myform").attr("onclick","return false");
						}else{
						$("#myform").attr("onclick","return true");
							}
					});
			});
</script>
</body>