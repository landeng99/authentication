<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<style type="text/css">
.label-class {
	font: bold;
}
</style>
<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>现金充值</strong>
			</div>
			<div class="tab-body">
				<br />
<%-- 				<form action="${backServer}/output/saveOutput" id="form" --%>
<!-- 					method="post"> -->
				<div class="field-group">
					<div>
						<div class="label" style="margin-left: 10px;">
							<label for="c_user">收银员:</label> <input type="hidden"
								id="cur_user" name="cur_user" value="${curUser}" /> ${curUser}
						</div>
					</div>
					<div class="field-item">
						<div class="label"><label for="pickorder">输入账号：</label></div>
	                    <div class="field">
	                    	<input type="text" class="input" id="login_account" name="login_account" value="" style="width:200px"/>
	                    </div>
					 </div>
					 <div class="field-item">
						<div class="label"><label for="pickorder">注册手机号：</label></div>
	                    <div class="field">
	                    	<input type="text" class="input" id="phone_num" name="phone_num" value="" style="width:200px"/>
	                    </div>
					 </div>
					<div class="form-button" style="margin-top: 20px;">
						<a class="button bg-main" onclick="checkUser()" id="checkUser">确定</a>
					</div>
				</div>
				<div class="panel admin-panel pickorder-list">
					<div class="panel-head">
						<strong></strong>
					</div>
					<div class="padding border-bottom">
						<shiro:hasPermission name="pickorder:add">
							<input type="button" id="add" value="添加"
								class="button button-small border-green" />
						</shiro:hasPermission>
					</div>
					<table class="table table-hover" id="pkgList">
						<tr>
							<th>充值账号</th>
							<th>注册手机号</th>
							<th>用户名</th>
							<th>可用余额（$）</th>
							<th>充值金额（$）</th>
						</tr>
					</table>
				</div>
			</div>
			<div style="clear: both"></div>
			<div class="form-button" style="margin-top: 20px;">
				<a class="button bg-main" onclick="confirmPay()" id="confirm">提交</a>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('#finish').click(function() {
			var seaport_id = '';
			$("input[name='seaport_id']:checked").each(
			function()
			{
				seaport_id+=this.value+",";
			}
			);
			if (seaport_id == '') {
				alert('请选择口岸');
			} else {
				$("#seaport_id_list").val(seaport_id);
				$('#form').submit();
			}
		});

		$('#back').click(function() {
			window.location.href = "${backServer}/output/queryAll";
		});
		
// 		$("#original_num").keydown(function(e) {
// 			alert(e.keyCode);
// 	           if (e.keyCode == 13) {
// // 	        	   addNumbers();
// 	        	   if( isOriginalExist() ){
// 	        		   alert("此单号已加入下方列表，请不要重复加入！");
// 	        	   }
// 	        	   else{
// 	        		   addNumbers();
// 	        	   }
// 	        	   clearOriginaNum();
// 	           }  
// 	    });
		
//         function addNumbers(){
//         	var url = "${backServer}/pkg/getPkgByOriginalNumUnpaid";
        	
//         	var original_num=$('#original_num').val();
//             if( ""==original_num||original_num==null){
//                 alert("单号不能为空");
//                 $('#original_num').focus();
//                 return
//             }
//         	$.ajax({
//                 url:url,
//                 data:{"originalNum":original_num},
//                 type:'post',
//                 success:function(data){
//                     if(data.result != true){
//                     	alert(data.msg);
//                     	return
//                     }else{
//                     	var tmp = "<tr>"
// 	                    	+ "<td>" + data.pkg.original_num + "</td>"
// 	                    	+ "<td>" + data.pkg.logistics_code + "</td>"
// 	                    	+ "<td>" + data.pkg.account + "</td>"
// 	                    	+ "<td>" + data.pkg.freight + "</td>"
// // 	                    	+ "<td>" + data.pkg.customs_cost + "</td>"
//  	                    	+ "<td>"
//  	                    		+ "<div class=\"field\">"
//  	                    			+ "<input type=\"text\" oninput=\"OnInput (event)\" onpropertychange=\"OnPropChanged (event)\" class=\"input\" value=\""
//  	                    			+ data.pkg.customs_cost
//  	                    			+ "\" style=\"width:200px\"/>"
//  	                    		+ "</div>"
//  	                    	+ "</td>"
// // 	                    	+ "<td style=\"display:none;\">" + data.pkg.logistics_code + "</td>"
// 	                    	+ "<td>"
// 	                    		+ "<a class=\"button border-blue button-little\" href=\"javascript:void(0);\" onclick=\"detail('"
// 	                    			+ data.pkg.package_id
// 	                    			+ "')\">详情</a>"
// 	                    	+ "</td>"
// 	                    	+ "<td>"
//                     			+ "<a class=\"button border-blue button-little\" href=\"javascript:void(0);\" onclick=\"deleteTr(this)\">删除</a>"
//                     		+ "</td>"
//                     	+ "</tr>";
//                         $('#pkgList').append(tmp);
//                     }
                    
//                 }
//             });
//         }
        
        function detail(package_id){
            var url = "${backServer}/pkg/detail?package_id="+package_id;
            window.location.href = url;
        }
        
        function deleteTr(obj) {
       	 	$(obj).closest('tr').remove();
       	}
        function addUser(user) {
        	var ttt = "<tr>"
					+"<th>充值账号</th>"
					+"<th>注册手机号</th>"
					+"<th>用户名</th>"
					+"<th>可用余额（$）</th>"
					+"<th>充值金额（$）</th>"
				+"</tr>";
        	$('#pkgList').html(ttt);// 清空，再插入
        	var tmp = "<tr>"
            	+ "<td>" + user.account + "</td>"
            	+ "<td>" + user.mobile + "</td>"
            	+ "<td>" + user.user_name + "</td>"
            	+ "<td>" + user.able_balance + "</td>"
             	+ "<td>"
             		+ "<div class=\"field\">"
             			+ "<input type=\"text\" id=\"chargeMoney\" name=\"chargeMoney\" class=\"input\" value=\""
             			+ 100
             			+ "\" style=\"width:200px\"/>"
             		+ "</div>"
             	+ "</td>"
        	+ "</tr>";
            $('#pkgList').append(tmp);
       	}
        function checkUser() {
        	var login_account=$('#login_account').val();
        	var phone_num=$('#phone_num').val();
        	var url = "${backServer}/frontUser/checkAccountAndPhone";
        	$.ajax({
			    url:url,
			    data:{"login_account":login_account,
			    	"phone_num":phone_num},
		    	type:'post',
                async:false,
			    success:function(data){
			        // 账户正常
			        if(data.result==false){
			        	alert('请输入正确的用户账号和手机号');
			        	return false;
			        }
			        else{
			        	addUser(data.frontUser);
			        	return true;
			        }
			    }
			});
        }
        
//         function isOriginalExist() {
//         	var inputOriginalNum=$('#original_num').val();
//         	var isExist = false;
//         	$("#pkgList").find("tr").each(function(){
//         	    var tdArr = $(this).children();
//         	    var tmpOriginal = tdArr.eq(0).text();
// //         	    alert('tmpOriginal:'+tmpOriginal+',inputOriginalNum:'+inputOriginalNum);
//         	    if( inputOriginalNum==tmpOriginal ){
//         	    	isExist = true;
//         	    	return false;	
//         	    }
//         	    else{
//         	    	tmpOriginal = tdArr.eq(1).text(); // 两个单号都要对比
//         	    	if( inputOriginalNum==tmpOriginal ){
//             	    	isExist = true;
//             	    	return false;	
//             	    }
//         	    }
//         	  });
        	
// //         	alert('isExist:'+isExist);
//         	return isExist;
//         }
        
//         function confirmPhone(){
//         	var url = "${backServer}/admin/frontUser/getFrontUserByAccountAndMobile";
        	
//         	$.ajax({
// 			    url:url,
// 			    data:{"logisticsCodes":logisticsCodes,
// 			    	"transportCost":cost,
// 			    	"taxes":taxes},
// 			    	type:'post',
// 	                async:false,
// 			    success:function(data){
// 			        // 账户正常
// 			        if(data.result==true){
// 			        	alert('支付成功');
// 			        	var url = "${backServer}/pkg/cashCollectionInit?curUser="+$("#cur_user").val();
// 			            window.location.href = url;
// 			        }
// 			        else{
// 			        	alert('支付失败');
// 			        }
// 			    }
// 			});
//         }

		function pay(chargeMoney,login_account){
			var url = "${backServer}/pkg/rechargeByCash";
			$.ajax({
			    url:url,
			    data:{"chargeMoney":chargeMoney,
			    	"login_account":login_account},
			    	type:'post',
	                async:false,
			    success:function(data){
			        // 账户正常
			        if(data.result==true){
			        	alert('充值成功');
			        	var url = "${backServer}/pkg/cashCollectionInit?curUser="+$("#cur_user").val();
			            window.location.href = url;
			        }
			        else{
			        	alert('充值失败');
			        }
			    }
			});
		}
        
    	function confirmPay() {
			var chargeMoney = $("#chargeMoney").val();
			var login_account=$('#login_account').val();
			if(chargeMoney<=0){
	        	alert('请输入正确的充值金额');
	        	return false;
	        }
			var yuan = chargeMoney*8;
			if(confirm("确定充值："+chargeMoney+" 美元（对应人民币："+yuan+" 元）吗？")){
	             pay(chargeMoney,login_account);
	         }
// 			$.ajax({
// 			    url:url,
// 			    data:{"chargeMoney":chargeMoney,
// 			    	"login_account":login_account},
// 			    	type:'post',
// 	                async:false,
// 			    success:function(data){
// 			        // 账户正常
// 			        if(data.result==true){
// 			        	alert('充值成功');
// 			        	var url = "${backServer}/pkg/cashCollectionInit?curUser="+$("#cur_user").val();
// 			            window.location.href = url;
// 			        }
// 			        else{
// 			        	alert('充值失败');
// 			        }
// 			    }
// 			});
     	}
    	
//     	function clearOriginaNum(){
//     		$("#original_num").val("");
//     		$('#original_num').focus();
//     	}
    	
    	// Firefox, Google Chrome, Opera, Safari, Internet Explorer from version 9
        function OnInput (event) {
//             alert ("The new content: " + event.target.value);
        }
    // Internet Explorer
        function OnPropChanged (event) {
            if (event.propertyName.toLowerCase () == "value") {
//                 alert ("The new content: " + event.srcElement.value);
            }
        }
	</script>
</body>
</html>