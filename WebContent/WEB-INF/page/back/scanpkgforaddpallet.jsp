<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp"%>
 <script type="text/javascript">
	var audio = document.getElementById("snd_html5");
	audio.loop = false;
	audio.autoplay = false;
	function playSound(src) {
		var explorerType= getExplorerInfo();
		if(explorerType=="IE")
		{
			var ie_s = document.getElementById('snd_ie');
			if(src!='' && typeof src!=undefined){
				ie_s.src = src;
			}
		}else
		{
			var _s = document.getElementById('snd_html5');
			_s.src = src;
			_s.play();
		}
	}

	function getExplorerInfo() {
		var explorer = window.navigator.userAgent.toLowerCase();
		//ie 
		if (explorer.indexOf("msie") >= 0||explorer.indexOf("net") >= 0) {
			return "IE";
		}
		//firefox 
		else if (explorer.indexOf("firefox") >= 0) {
			return  "Firefox";
		}
		//Chrome
		else if (explorer.indexOf("chrome") >= 0) {
			return  "Chrome";
		}
		//Opera
		else if (explorer.indexOf("opera") >= 0) {
			return  "Opera";
		}
		//Safari
		else if (explorer.indexOf("Safari") >= 0) {
			return  "Safari";
		}
	}
</script>
<body>
	 <div style="width:500px;margin: auto;margin-top:35px">	
		 <div style="width:200px;margin: auto;"><label>包裹运单号(or关联单号)：</label><input type="text" class="input" id="scanInput" size="30" style="width:220px; height:40px; font-size:18px;"/></div>
		 <div id="note" style="color:red;text-align: center;font-size:20px; font-weight:bold"></div>
	 </div>
	 <div style="text-align:center;margin: auto;margin-top:20px;color:red">
	 	   注意：包裹扫描之前，请将鼠标放入文本框中。
	 </div>
	 
	 <div id="todyScanDivId" style="height:98px; overflow:auto">
		<table id="todyScanTableId">
			<tr></tr>
		</table>
	</div>
				   
	 <div class="panel admin-panel">
	 	<div class="panel-head"><strong>已放入托盘包裹</strong></div>
	 	   <div style="min-height:200px;" >
	         	 <table class="table table-hover " id="pkgList">
	 	         	  <thead>
	         	     	<tr>
	         	     		<th id="logistics_code" width="10%">公司运单号</th>
	         	     		<th id="original_num" width="10%">关联单号</th>
	         	     		<th id="email" width="10%">用户账号</th>
	         	     		<th id="receiver" width="15%">收件人</th>
	         	     		<th id="idcard" width="15%">身份证号</th>
	         	     		<th id="mobile" width="10%">联系方式</th>
	         	     		<th id="address" width="20%">收件地址</th>
	         	     		<th width="10%" formatter="fmtOpt">操作</th>
	         	     	</tr>
	         	      </thead>
	         	</table>
	         </div>
	    </div> 
	    <div class="buttonList" style="text-align: center;padding-top:20px;">
	         <button  class="button bg-main" id="close">返回</button>
	    </div>
	    <input type="hidden" id="seaport_id" value="${seaport_id}"/>
	    <input type="hidden" id="pick_id" value="${pick_id}"/>
	    <input type="hidden" id="pallet_id" value="${pallet_id}"/>
	 
	 <script type="text/javascript">
	 
	 var datagrid=[];
	 var $datagrid=$('#pkgList').datagrid({data:datagrid});
	 function fmtOpt(val,rowData){
			var url="${backServer}/pallet/delpkg?package_id="+rowData.package_id+"&pallet_id="+$('#pallet_id').val();
			var $opt=$('<a class="button button-small border-green" href="javascript:;">删除</a>');
			$opt.bind('click',function(){
				var self=$(this);
				 $table=self.parent().parent().parent();
				 $tr=self.parent().parent();
				 var bool = window.confirm("确定删除?");
				 if(bool){
					 $.ajax({
						 url:url,
						 dataType:'json',
						 type:'post',
						 async:false,
						 success:function(data){
							 alert("删除成功");
							 var index=$('#pkgList tbody tr').index($tr);
							 $datagrid.datagrid('remove',index);
						 }
					 })
				 }
			});
			
			return $opt;
	 };
	 
	 $('#scanInput').change(function(){
			var logistics_code= $(this).val();
/* 	  		var hasAddData =$datagrid.datagrid('getAllData');
	  		for(i=0;i<hasAddData.length;i++){
	  			var temp=hasAddData[i];
	  			if(temp['logistics_code']==logistics_code){
	  				$('#note').text("包裹已经加入托盘，请继续扫描... ...");
	  				return;
	  			}
	  		} */
		  	var seaport_id=$('#seaport_id').val();
		  	var pick_id=$('#pick_id').val();
		  	var pallet_id=$('#pallet_id').val();
			$.ajax({
				url:'${backServer}/pallet/scanforaddpallet',
				data:{logistics_code:logistics_code,'seaport_id':seaport_id,'pick_id':pick_id,'pallet_id':pallet_id},
				type:'post',
				dataType:'json',
				async:false,
				success:function(data){
					var pkgList=data.pkgList;
					if(pkgList){
						if(data.need_check_out_flag=='Y')
						{
							playSound('${resource_path}/sound/nonfind.wav');
							$('#note').text("该包裹为异常件，需要取出不可出库！");
						}else
						{
						playSound('${resource_path}/sound/find.wav');
						for(i=0;i<pkgList.length;i++){
		  					$datagrid.datagrid('add',pkgList[i]);
		  				}
						$('#note').text("包裹加入托盘成功，请继续扫描... ...");
						}
					}else{
						playSound('${resource_path}/sound/nonfind.wav');
						$('#note').text(data.msg);
					}
				}
			});
			queryTodyScanLog();
	  		$(this).val('');
	  	});
	 $('#close').click(function(){
	 
		 var pallet_id=$('#pallet_id').val();
		 var url="${backServer}/pallet/initedit?pallet_id="+pallet_id;
		 window.location.href=url;
	 });
	 
		//今天扫描日志
function queryTodyScanLog()
{
	var pick_id=$('#pick_id').val();
		var url='${backServer}/outputScanLog/queryAllScanLog';
	   	var params={};
				$.ajax({
					url:url,
					data:{"pick_id":pick_id},
					type:'post',
					dataType:'json',
					async:false,
					success:function(data){
						if(data.outputScanLogList.length==0){
							$("#todyScanDivId").css("height","20px");
							var newRow = "<tr style=\"color:red\"><td>尚未有扫描日志记录</td></tr>";
							$("#todyScanTableId tr:last").after(newRow);
						}else{
							$("#todyScanDivId").css("height","98px");
							$("#todyScanTableId tr").empty();
							for(i=0;i<data.outputScanLogList.length;i++)
							{
								var newRow = "<tr ";
								if(data.outputScanLogList[i].add_pallet_success==1)
								{
									newRow+="style=\"color:green\"";
								}else
								{
									newRow+="style=\"color:red\"";
								}
								
								newRow+="><td>"+data.outputScanLogList[i].scan_dateStr;
								newRow+="，"+data.outputScanLogList[i].pkg_no;
								newRow+="，"+data.outputScanLogList[i].pkg_status;
								if(data.outputScanLogList[i].add_pallet_success==0)
								{
									newRow+="，"+data.outputScanLogList[i].add_fail_reson;
								}
								newRow+="，"+data.outputScanLogList[i].scan_user_name;
								newRow+="，扫描次数："+data.outputScanLogList[i].scan_count;
								
								if(data.outputScanLogList[i].add_pallet_success==1)
								{
									newRow+="&nbsp;&nbsp; √";
								}
								newRow+="</td></tr>";
								
								$("#todyScanTableId tr:last").after(newRow);
							}
						}
					}
				});
		}
		queryTodyScanLog();
	 </script>
	<bgsound id="snd_ie" loop="0" src="">
	<audio id="snd_html5" hidden="true" controls="true"> <source
		id="snd" src="${resource_path}/sound/nonfind.wav" type="audio/mpeg" /></audio>	 
</body>
</html>