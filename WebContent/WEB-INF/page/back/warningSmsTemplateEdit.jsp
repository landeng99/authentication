<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
	<title>修改文章</title>
	<link rel="stylesheet" href="<%=basePath %>resource/css/default2.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>resource/css/prettify.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>back/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>back/css/admin.css">
	<script charset="utf-8" src="<%=basePath %>resource/js/kindeditor.js"></script>
	<script charset="utf-8" src="<%=basePath %>resource/js/zh_CN.js"></script>
    <script src="<%=basePath %>back/js/My97DatePicker/WdatePicker.js"></script>
	<script>
		<%-- KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="temp_conent"]', {
				uploadJson : '<%=basePath %>kindeditor/uploadImg',
				fileManagerJson : '<%=basePath %>kindeditor/fileMgr',
				allowFileManager : true
			});
		}); --%>
	</script>
</head>
<body>
<div class="admin">
	<div class="tab">
	<div class="form-group">
	
<form method="post" action="${backServer}/warningSmsTemplate/saveOrupdate">
	<div style="height: 60px;">
		<div style="float:left;">
			<span for="userName" style=" line-height: 30px;"><strong>模板名称:</strong></span></div>
		<div style="float:left;margin-left:10px;width:300px;">
			<input type="text" 
				id="temp_title" name="temp_name" style="width: 170px;"
				value="${warningSmsTemplate.temp_name}" class="input"/></div>
		
		<div style="float:left;">
			<span for="createTime" style=" line-height: 30px;"><strong>模板状态:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<select id="enable" name="enable" class="input">
				<option value="N" <c:if test="${warningSmsTemplate.enable == 'N'}">selected="selected"</c:if>>禁用</option>
				<option value="Y" <c:if test="${warningSmsTemplate.enable == 'Y'}">selected="selected"</c:if>>启用</option>
			</select></div>
	</div>
	
	<div>
		<div style="float:left;">
			<span for="address" style=" line-height: 30px;"><strong>模板内容:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<textarea rows="10" cols="80"
				id="temp_conent" name="temp_conent">${warningSmsTemplate.temp_conent}</textarea>
		</div>
	</div>
	
	<div style="height: 40px; float: left; clear: both; margin-top:10px; margin-left: 40px;">
	<span style="font: bold;color: red;">注意：目前只支持如下标签</span><br>
	<table border="1">
	<thead><tr ><th width="10%">标签</th><th width="10%">含义</th></tr></thead>
	<tbody>
	<tr><td width="10%">[logisticsCodeList]</td><td width="10%">公司单号列表</td></tr>
	</tbody>
	</table>
	</div>
	<div style="height: 40px; float: left; clear: both; margin-top:80px; margin-left: 40px;">
		<div style="float:left;">
		<input type="hidden" name="seq_id" value="${seq_id}" />
	<c:if test="${seq_id != -1}">
		<input type="submit" name="button" value="更新" />
	</c:if><c:if test="${seq_id == -1}">
		<input type="submit" name="button" value="增加" />
	</c:if>
		<input type="button" name="button" value="返回" onclick="returnPage()"/>	
			</div>
	</div>
</form> 
		 
	</div>
	</div>
</div>
</body>
<script type="text/javascript">
	function returnPage(){
		history.go(-1);
	}
</script>
</html>