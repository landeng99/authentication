<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>会员费用信息</strong></div>
      <div class="tab-body">
        <br />
<!--                  <div class="padding border-bottom"> -->
<!--                     <input type="button" class="button button-small border-green" onclick="add()" value="添加 "/> -->
<!--                  </div> -->
                 <table class="table">
                    <tr><th width="20%">等级名称</th>
                        <th width="20%">单价（元/磅）</th>
                        <th width="20%">首重单价（元/磅）</th>
                        <th width="20%">续重单价（元/磅）</th>
                        <th width="20%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover">
                  <c:forEach var="freightCost"  items="${freightCostList}">
                     <tr>
                         <td width="20%">${freightCost.memberRate.rate_name}</td>
                         <td width="20%"><fmt:formatNumber value="${freightCost.unit_price}" type="currency" pattern="$#0.00#"/></td>
                         <td width="20%"><fmt:formatNumber value="${freightCost.first_weight_price}" type="currency" pattern="$#0.00#"/></td>
                         <td width="20%"><fmt:formatNumber value="${freightCost.go_weight_price}" type="currency" pattern="$#0.00#"/></td>
                         <td width="20%">
                           <a class="button border-blue button-little" href="javascript:void(0);"onclick="detail('${freightCost.freight_id}','${freightCost.memberRate.rate_name}','${freightCost.unit_price}','${freightCost.first_weight_price}','${freightCost.go_weight_price}')">详情</a>
                           <a class="button border-blue button-little" href="javascript:void(0);"onclick="modify('${freightCost.freight_id}','${freightCost.memberRate.rate_name}','${freightCost.unit_price}','${freightCost.first_weight_price}','${freightCost.go_weight_price}','${freightCost.memberRate.rate_id}')">修改</a>
                         </td>
                     </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
            $('#rate_id').focus();
        });
        
        function modify(freight_id,rate_name,unit_price,first_weight_price,go_weight_price,rate_id){
            var url = "${backServer}/freightCost/modify?freight_id="+freight_id+"&rate_name="+rate_name+"&unit_price="+unit_price+"&first_weight_price="+first_weight_price+"&go_weight_price="+go_weight_price+"&rate_id="+rate_id;
            window.location.href = url;
        }
        
        function detail(freight_id,rate_name,unit_price,first_weight_price,go_weight_price){
            var url = "${backServer}/freightCost/detail?freight_id="+freight_id+"&rate_name="+rate_name+"&unit_price="+unit_price+"&first_weight_price="+first_weight_price+"&go_weight_price="+go_weight_price;
            window.location.href = url;
        }

        function add(){
            var url = "${backServer}/freightCost/add";
            window.location.href = url;
        }
    </script>
</body>
</html>