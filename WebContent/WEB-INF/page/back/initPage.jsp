<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<% String path = request.getContextPath(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- 分页 -->
	<link rel="stylesheet" href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css"> 
	<link type="text/css" rel="stylesheet" href="<%=path%>/resource/css/pushLink.css"> 
	<script src="http://apps.bdimg.com/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<%=path%>/resource/js/jquery.zclip.min.js"></script>
	<script type="text/javascript" src="<%=path%>/resource/js/jquery-1.8.0.js"></script>
	
	<%-- <link href="<%=path%>/resource/css/myPage.css" rel="stylesheet" type="text/css" />
    <script src="<%=path%>/resource/js/jqPaginator.min.js" type="text/javascript"></script>
    <script src="<%=path%>/resource/js/myPage.js" type="text/javascript"></script> --%>
   
	<!-- <script type="text/javascript">
	
        function loadData(num) {
            $("#PageCount").val("89");
        }
    </script> -->
	<!--  -->
</head>
<body>
<div class="tabbable"> 
  <ul class="nav nav-tabs nav-pills" >
    <li id="initPush" ><a href="#push" data-toggle="tab" style="width:750px;font-size: medium"><i class="glyphicon glyphicon glyphicon-leaf">推广链接</i></a></li>
    <li data-toggle="modal" data-target="#add" style="width:750px;font-size: medium"><a href="#add" data-toggle="tab" style="width:750px;"><i class="glyphicon glyphicon-refresh">新增</i></a></li>
    <!-- <li id="initCount"><a href="#count" data-toggle="tab"><i class="glyphicon glyphicon glyphicon-signal">推广统计</i></a></li> -->
  </ul>
  <div class="tab-content">
  	<!-- 推广链接 -->
    <div class="tab-pane fade active" id="push">
      <table class="table table-striped table-bordered table-hover table-responsive"> 
	    <caption></caption> 
	    <thead> 
	        <tr class="success"> 
	            <th>业务员</th> 
	            <th>推广链接</th> 
	            <th>操作</th> 
	        </tr> 
	    </thead> 
	    <tbody>
	    <c:forEach var="link" items="${linkList}">
	        <tr> 
	        	<!-- 业务名    -->
	            <td align="center" style="vertical-align:middle;">${link.businessName}</td>
	            <!-- 推广链接    --> 
	            <td align="center" style="vertical-align:middle;">${link.pushLink}</td>
	            <!-- 查看二维码    -->	           
	            <td align="center" style="vertical-align:middle;">
	           	   <!-- <button href="javascript:void(0)" id ="copyBtn" class="btn btn-info btn-large btn-primary copyit">复制链接</button> -->
	           		<button class="btn btn-info btn-large btn-primary" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-send">查看二维码</i></button>
           			<!-- 查看二维码模态框   -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
			       		<div class="modal-content">
				          <div class="modal-header form-horizontal">
						    <div class="control-group info pull-left"> 
				             	<span class="modal-title glyphicon glyphicon-qrcode" id="myModalLabel" >推广二维码</span>
							</div>
				          </div>
				          <div class="modal-body">
							 <img alt="" src="${link.pushCode}">
						  </div>
				          <div class="modal-footer">
				              <button type="button" class="btn btn-info btn-large btn-primary" data-dismiss="modal">关闭</button>
				          </div>
				       </div>
				    </div>
				    </div>
				    <!-- 删除	 -->
	           		<button type="button" onclick="dele(${link.id},this)" class="btn btn-info btn-large btn-primary"><i class="glyphicon glyphicon-trash">删除</i></button>
	            </td> 
	        </tr> 
	    </c:forEach>
	    </tbody> 
	  </table>
	  <!-- 推广链接分页  -->
	  <!-- <form id="form1" runat="server">
	    <div>
	    </div>
	    <div>
	        <ul class="pagination" id="pagination">
	        </ul>
	        <input type="hidden" id="PageCount" runat="server" />
	        <input type="hidden" id="PageSize" runat="server" value="8" />
	        <input type="hidden" id="countindex" runat="server" value="10"/>
	        设置最多显示的页码数 可以手动设置 默认为7
	        <input type="hidden" id="visiblePages" runat="server" value="7" />
	    </div>
      </form> -->
<!-- 	 <div class="pagination pagination-lg">
	    	<ul class="pager">
			  <li class="previous"><a href="#">&laquo;</a></li> 
			  <li><a href="#">上一页</a></li>
		      <li class="active"><a href="#">1</a></li> 
		      <li><a href="#">2</a></li> 
		      <li><a href="#">3</a></li> 
		      <li><a href="#">4</a></li> 
		      <li><a href="#">5</a></li> 
		      <li><a href="#">下一页</a></li>
		      <li><a href="#">&raquo;</a></li> 
			</ul>
	    </div>   -->
	    <div class="panel-foot text-center">
           <jsp:include page="webfenye.jsp"></jsp:include>
        </div> 
    </div>
    
    <!-- 新增 -->
    <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog ">
			<div class="modal-content" >
				<div class="modal-header form-horizontal">
					<div class="control-group info pull-left"> 
						<span class="modal-title glyphicon glyphicon-pencil">新增</span>	
					</div>		
				</div>
				<div class="modal-body form-horizontal">
					<input id="checkBusiness" type="hidden" value="0">
				    <!-- <div id="success" class="alert alert-success"></div>
				    <div id="info" class="alert alert-info"></div> -->
					<div class="control-group info pull-left"> 
				        <label class="col-sm-4 control-label" for="inputIn">业务名：</label> 
				        <div class="col-sm-8 controls"> 
				            <input type="text" id="businessName" class="form-control"  placeholder="请输入...">
				            <span id="usernameNote"></span>
				        </div> 
				    </div> 
				</div>
				<div class="modal-footer ">
					<button type="button" class="btn btn-info btn-large btn-primary" data-dismiss="modal" onclick="submit()" id="btn12"><span class="glyphicon glyphicon-ok">提交</span></button>
					<button type="button" class="btn btn-info btn-large btn-primary" data-dismiss="modal"><span class="glyphicon glyphicon-remove">关闭</span></button>
				</div>
		   </div>
	    </div>
	 </div> 
	 
	 
  </div>
</div>
<!-- JavaScript -->
<script type="text/javascript">


	/* 新增业务员   */
	function submit(){
		//输入的业务员名字
		var businessName = $("#businessName").val();
		if(businessName == null || businessName == ""){
			alert("业务员不能为空！");
			//return;
		} else{
			
			var url = "${backServer}/pushLink/addBusiness";
 			$.ajax({
 				url:url,
 				data:{
 					 "businessName":businessName, 
 				},
				type:'post',
 				datatype:'text',
 				async:true,
 				success:function(data){
 					if(data){
	 					alert("新增业务名成功!");
 					}
 					else{
 						alert("提示！业务名已存在!");
 					}
				}
 			});
		}
	}
	
	 /* 新增业务员后自动清空模态框的数据  */
	 $(function(){
		 $("#btn12").click(function(){
		 	$("#businessName").val("");
		 });
	 });
	 
	 /* 推广链接删除  */
	 function dele(id,obj){
		 $(obj).parent().parent().remove();
		 var url = "${backServer}/pushLink/deleteBusiness";
			$.ajax({
				url:url,
				data:{
					"id":id,
				},
				type:'post',
				datatype:'json',
				success:function(data){
					alert("删除成功");
				}
			}); 
	 }
	
	
	
	
</script>
</body>
</html>