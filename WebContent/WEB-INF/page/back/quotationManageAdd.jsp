<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="${resource_path}/css/easyui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${resource_path}/js/jquery.easyui.min.js"></script>
<style>
    .span1{
        display:-moz-inline-box;
        display:inline-block;
        width:20px;
        height:25px;
    }
	.hided{display: none;}
	.hided1{display: block;}
</style>
<script type="text/javascript">

/* 点击特殊适用 */
 function OverseasChange(){
	if($("#account_type").find("option:selected").attr("grade")==3){
		$("#see").removeClass().addClass("hided");
	}else if($("#account_type").find("option:selected").attr("grade")==4){
		$("#see").removeClass();
	}
}
 
  function gradeChange(){
	  //创建数组，可以不指定大小   
	     var loadList = new Array( );  
	     //为数组赋值。每个单元格可以是数组。JavaScript不支持二维数组
/* 	     loadList[-1]=['--请选择--']; */
	     loadList[0]=['默认','A渠道', 'B渠道', 'C渠道', 'D渠道', 'E渠道'];  
	     loadList[1]=['默认','A渠道', 'B渠道', 'C渠道', 'D渠道', 'E渠道'];   
	     //获得选项的索引号    
	     var pIndex=document.myform.user_type.selectedIndex-1; 
	     var newOption1;  
	     document.myform.express_package.options.length=1;  
	     for (var j in loadList[pIndex]) {  
	         newOption1=new Option(loadList[pIndex][j],j);   
	         document.myform.express_package.options.add(newOption1);  
	     }  
    }
  
  function addGroupMember(){
		var existMember=$("#authAccount").val();
		$("#member_to").empty();
		if(existMember!=null&&existMember!=""){
			var existMemberArr=existMember.split(",");
			for(var i=0;i<existMemberArr.length-1;i++){
				$("#member_to").append("<option >"+existMemberArr[i]+"</option>");
			}
		}
		$('#member_div').dialog({
	    title: '选择适用用户',
	    top:25,
	    width: 400,
	    height: 400,
	    closed: false,
	    cache: false,
	    onClose: function(){
	   
	    },
	    modal: true
		});
		$('#member_div').css("display","block");
		$('#member_div').dialog('refresh');
		//doGroupMemberSearch("%");
	}
  function addGroupMemberSubmit(){
		  var memberUser="";
		     $.each($("#member_to option"),function(i,val){
		 			memberUser+=val.text+",";
		 		}
		 	);
		 	$("#authAccount").val(memberUser);
	 	$("#member_div").parent().find(".panel-tool-close").click();
	}  
	function doGroupMemberSearch(value){
		/* 分开特殊报价单中同行用户与普通用户 */
		var number;
		if($("#user_type").find("option:selected").attr("grade")==1){
			if($("#account_type").find("option:selected").attr("grade")==4){
				number=1;
			}
		}else if($("#user_type").find("option:selected").attr("grade")==2){
			if($("#account_type").find("option:selected").attr("grade")==4){
				number=2;
			}
		}
	    $.post("${backServer}/frontUser/queryFrontUserLikeByAccountType",{"account":value,"user_type":number}, function(data){  
	 	$("#member_from").empty();
		$.each(data.frontUserList,function(i,val){
	 		$("#member_from").append("<option >"+val.account+"</option>");
	 	});
	  },"json");
	}
	
	$(function() {
		//member
		//选择一项
		$("#member_addOne").click(function() {
			addOne($("#member_from option:selected"),"#member_to");
		});

		//选择全部
		$("#member_addAll").click(function() {
			addOne($("#member_from option"),"#member_to");
		});

		//移除一项
		$("#member_removeOne").click(function() {
			addOne($("#member_to option:selected"),"#member_from");
		});

		//移除全部
		$("#member_removeAll").click(function() {
			addOne($("#member_to option"),"#member_from");
		});
	});
	
	function addOne(source,addTo){
		$.each(source,function(i,val){
			var temp=addTo+" option";
			var needAdd=-1;
			$.each($(temp),function(j,val1){
				if(val.text==val1.text){
					needAdd=1;
				}
			});
			if(needAdd==-1){
				$(addTo).append("<option >"+val.text+"</option>");
			}
		});
		source.remove();
	}    
</script>
<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>报价单添加</strong>
			</div>
			<div class="tab-body"> <form name="myform" id="myform">
				<br />
				<input type="hidden" id="adduser_check" value="0">
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>报价单名称:</strong> </span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<input type="text" class="input" id="quota_name"
							name="quota_name" style="width: 200px; margin-left: 30px;" placeholder="名称不能为空" onblur="checkUser()" />
					</div>
					<div
						style="float: left; margin-top: 5px; margin-left: 30px; width: 200px;">
						<span id="serviceNameTextArea"></span>
					</div>
				</div>
				
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>仓库:</strong> </span>
					</div>
					<div style="float: left; margin-left: 80px;">
						<select  class="input" id="osaddr_id" name="osaddr_id" style="width:150px" >
	             		  <option value="">--选择仓库--</option>
		              		<c:forEach var="userOverseasAddress" items="${overseasAddressList}">
		              			<option value="${userOverseasAddress.id}">${userOverseasAddress.warehouse}</option>
		              		</c:forEach>
	             		</select>
					</div>
			
					<div style="float: left; margin-top: 5px; margin-left: 180px" class="label">
						<span><strong>用户类型:</strong> </span>
					</div>
					<div style="float: left; margin-left: 20px;">
						 <select name="user_type" class="input" style="width:150px" id="user_type"  onclick="gradeChange()">
	                         <option value="">--请选择--</option>
	                         <option value="1" grade="1">普通用户</option>
	                         <option value="2" grade="2" >同行用户</option>
	                    </select>
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 180px" class="label" >
						<span><strong>选择渠道:</strong> </span>
					</div>
					<div style="float: left; margin-left: 30px;" class="" id="choose_same">
						 <select name="express_package" id = "express_package" class="input" style="width:150px">
                              <option value="">--请选择--</option>
                         </select>
					</div>
				</div>
			
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>报价类型:</strong> </span>
					</div>
					<div style="float: left; margin-left:50px;">
						<select name="account_type" id="account_type"  class="input" style="width:150px" onchange="OverseasChange()">
	                         <option value="">--请选择--</option>
	                         <option value="0" grade="3">默认报价</option>
	                         <option value="1" grade="4">特殊报价</option>
	                    </select>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">
						<span><strong>计量单位:</strong> </span>
					</div>
					<div style="float: left; margin-left: 20px;">
						 <select name="unit" id = "unit" class="input" style="width:150px">
	                         <option value="">--请选择--</option>
	                         <option value="1">g(克)</option>
	                         <option value="2">kg/(千克)</option>
	                         <option value="3">lb(磅)</option>
	                    </select>
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">
						<span><strong>是否含税:</strong> </span>
					</div>
					<div style="float: left; margin-left: 30px;">
						<select id="isHaveTax" name="isHaveTax" class="input" style="width:150px">
                            <option value="">--请选择--</option>
                            <option value="1">含税</option>
                            <option value="0">不含税</option>
                     	</select>
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>首重:</strong> </span>
					</div>
					<div style="float: left; margin-left: 50px;">
						<input type="number" class="input" id="first_weight"
							name="first_weight" style="width: 150px; margin-left: 30px;" placeholder="必须填数字"/>
					</div>
					<div
						style="margin-top: 5px; margin-left: 30px; width: 200px;position: absolute;left:320px">
						<span id="serviceNameTextArea1"></span>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">
						<span><strong>进位:</strong> </span>
					</div>
					<div style="float: left; margin-left: 15px;">
						<input type="number" class="input" id="carry"
							name="carry" style="width: 150px; margin-left: 30px;" placeholder="必须是0-1的数字" />
					</div>
					<div
						style="float: left; margin-top: 5px; margin-left: 30px; width: 200px;">
						<span id="serviceNameTextArea2"></span>
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>首重单价:</strong> </span>
					</div>
					<div style="float: left; margin-left: 50px;">
						<input type="number" class="input" id="first_weight_price" name="first_weight_price" style="width: 150px; margin-left: 30px;" placeholder="必须填数字"/>
							
					</div>
					<div
						style="margin-top: 5px; margin-left: 30px; width: 200px;position: absolute;left:320px">
						<span id="first_weight_priceTextArea"></span>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">
						<span><strong>续重单价:</strong> </span>
					</div>
					<div style="float: left; margin-left: 15px;">
						<input type="number" class="input" id="go_weight_price"
							name="go_weight_price" style="width: 150px; margin-left: 30px;" placeholder="必须填数字" />
					</div>
					<div
						style="float: left; margin-top: 5px; margin-left: 30px; width: 200px;">
						<span id="go_weight_priceTextArea"></span>
					</div>
				</div>

				<div style="height: 100px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>描述信息:</strong> </span>
					</div>
					<div style="float: left; margin-left: 50px; width: 250px">
						<textarea class="input" rows="3" cols="50" id="description"
							name="description"></textarea>
					</div>
				</div>
				<!-- 只对以下客户适用 -->
				<div style="height: 40px;margin-bottom:10px" class="hided" id="see">
                    <div style="float:left;margin-top:5px; margin-left:30px" class="label"><span><strong>只对以下客户适用:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
						<textarea rows="3" cols="50" readonly="readonly"  onclick="addGroupMember()" id="authAccount" name="authAccount"></textarea>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="taxTextArea"></span>
                    </div>
                    <span id="serviceNameTextArea3"></span>
                 </div>
                 
 <div id="member_div" style="display:none">
    <input class="easyui-searchbox" data-options="prompt:'请输入适用用户帐号',searcher:doGroupMemberSearch" style="width:180px"></input>
	<br/>
	<table align="center"  width="394" height="282"  border="0" cellpadding="0" cellspacing="0" style=" table-layout: fixed;">
	    <tr>
	        <td colspan="4">
	            <select name="member_from" id="member_from" multiple="multiple" size="10" style="width:100%;  height: 100%;  background: #eeeeee;  color: #a0a0a0;">
	            </select>
	        </td>
	        <td valign="middle" align="center"  width="30">
	            <input type="button" id="member_addAll" value=" >> "  class="lgb" style="width:30px;" /><br />
	            <input type="button" id="member_addOne" value=" > "  class="lgb" style="width:30px;" /><br />
	            <input type="button" id="member_removeOne" value="&lt;"  class="lgb" style="width:30px;" /><br />
	            <input type="button" id="member_removeAll" value="&lt;&lt;"  class="lgb" style="width:30px;" /><br />
	        </td>
	        <td colspan="4">
	            <select name="member_to" id="member_to" multiple="multiple" size="10" style="width:100%;  height: 100%;  background: #eeeeee;  color: #a0a0a0;">
	            </select>
	        </td>
	    </tr>
	</table>
    <button type="button"  class="lgb" style="margin-left: 175px;  margin-top: 6px;color: #000" onclick="addGroupMemberSubmit()">确定</button>
</div>
                
</form>	
				<div class="padding border-bottom" style="margin-left:20px;clear:both">
							<input type="button" class="button bg-main" onclick="add()" value="保存" /> 
							<input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#quota_name').focus();
		});

		function add() {
			var quota_name = $('#quota_name').val();
			var osaddr_id = $('#osaddr_id').val();
			var user_type = $('#user_type').val();
			var account_type = $('#account_type').val();
			var unit = $('#unit').val();
			var isHaveTax = $('#isHaveTax').val();
			var express_package= $('#express_package').val();
			var first_weight = $('#first_weight').val();
			var first_weight_price = $('#first_weight_price').val();
			var go_weight_price = $('#go_weight_price').val();
			var carry = $('#carry').val();
			var description = $('#description').val();
			var authAccount = $('#authAccount').val();
			
			var usercheck = $('#adduser_check').val();
			
			var checkResult = "0"
			if (quota_name == null || quota_name.trim() == "") {
				$('#serviceNameTextArea').text('报价单名称不能为空!');
				$('#serviceNameTextArea').css('color', 'red');
				checkResult = "1";
			}
			if (first_weight == null || first_weight.trim() == "") {
				$('#serviceNameTextArea1').text('不能为空且必须是数字!');
				$('#serviceNameTextArea1').css('color', 'red');
				checkResult = "1";
			}
			if (first_weight_price == null || first_weight_price.trim() == "") {
				$('#first_weight_priceTextArea').text('不能为空且必须是数字!');
				$('#first_weight_priceTextArea').css('color', 'red');
				checkResult = "1";
			}
			if (go_weight_price == null || go_weight_price.trim() == "") {
				$('#go_weight_priceTextArea').text('不能为空且必须是数字!');
				$('#go_weight_priceTextArea').css('color', 'red');
				checkResult = "1";
			}
			if ((account_type == 1 && authAccount == null) || (account_type == 1 && authAccount.trim() == "")) {
				$('#serviceNameTextArea3').text('特殊报价单下用户不能为空!');
				$('#serviceNameTextArea3').css('color', 'red');
				checkResult = "1";
			}
			
			/* 正则认证必须是0-1的数字,最多保留两位! */
		    var regexp=/^([0-1](\.\d{1,2})?|1)$/;
			if(!regexp.test(carry)){
				$('#serviceNameTextArea2').text('必须是0-1的数字,最多保留两位!');
				$('#serviceNameTextArea2').css('color', 'red');
				checkResult = "1";
			}
			if (checkResult == "1") {
				return;
			}
			
			if(usercheck==1){
				var url = "${backServer}/quotationManage/add";
				if(account_type == 0){
					alert("正在保存请稍等，在弹出'添加成功'之前，请勿重复点击保存！");
				}
				$.ajax({
						url : url,
						data : {
							"quota_name" : quota_name,
							"osaddr_id" : osaddr_id,
							"user_type" : user_type,
							"account_type" : account_type,
							"unit" : unit,
							"isHaveTax" : isHaveTax,
							"express_package" : express_package,
							"first_weight" : first_weight,
							"first_weight_price" : first_weight_price,
							"go_weight_price" : go_weight_price,
							"carry" : carry,
							"description" : description,
							"authAccount":$('#authAccount').val()
						},
						type : 'post',
						dataType : 'text',
						success : function(data) {
							alert("添加成功！");
							window.location.href = "${backServer}/quotationManage/queryAllOne";
						}
					});
			}else{
				alert("报价单名称已存在！");
			}
			
			
// 			//判断，报价单里面是否已经同时添加了：同一仓库、同一用户类型、同一渠道、同一报价单类型，如果存在，则不让添加
// 			$.ajax({
// 					url:'${backServer}/quotationManage/checkIsHave',
// 					type:'post',
// 					data:{
// 						"osaddr_id" : osaddr_id,
// 						"user_type" : user_type,
// 						"account_type" : account_type,
// 						"express_package" : express_package},
// 					success:function(data){
// 						if(data['result']==true){
// 							alert("已存在该仓库下 该用户类型 选择该渠道 该报价类型的报价单，不能再添加！");
// 							return;
// 						}else if(data['result']==false){
// 							if(usercheck==1){
// 								var url = "${backServer}/quotationManage/add";
// 								if(account_type == 0){
// 									alert("正在保存请稍等，在弹出'添加成功'之前，请勿重复点击保存！");
// 								}
// 								$.ajax({
// 										url : url,
// 										data : {
// 											"quota_name" : quota_name,
// 											"osaddr_id" : osaddr_id,
// 											"user_type" : user_type,
// 											"account_type" : account_type,
// 											"unit" : unit,
// 											"isHaveTax" : isHaveTax,
// 											"express_package" : express_package,
// 											"first_weight" : first_weight,
// 											"first_weight_price" : first_weight_price,
// 											"go_weight_price" : go_weight_price,
// 											"carry" : carry,
// 											"description" : description,
// 											"authAccount":$('#authAccount').val()
// 										},
// 										type : 'post',
// 										dataType : 'text',
// 										success : function(data) {
// 											alert("添加成功！");
// 											window.location.href = "${backServer}/quotationManage/queryAllOne";
// 										}
// 									});
// 							}else{
// 								alert("报价单名称已重复");
// 							}
// 						}
// 					}
// 				});
			
			
			
			function addGroupMember() {
				var existMember=$("#authAccount").val();
				$("#member_to").empty();
				if(existMember!=null&&existMember!=""){
					var existMemberArr=existMember.split(",");
					for(var i=0;i<existMemberArr.length-1;i++){
						$("#member_to").append("<option >"+existMemberArr[i]+"</option>");
					}
				}
				$('#member_div').dialog({
			    title: '选择适用用户',
			    top:25,
			    width: 400,
			    height: 400,
			    closed: false,
			    cache: false,
			    onClose: function(){
			   
			    },
			    modal: true
				});
				$('#member_div').css("display","block");
				$('#member_div').dialog('refresh');
				//doGroupMemberSearch("%");
			}
			function addGroupMemberSubmit(){
				  var memberUser="";
				     $.each($("#member_to option"),function(i,val){
				 			memberUser+=val.text+",";
				 		}
				 	);
				 	$("#authAccount").val(memberUser);
			 	$("#member_div").parent().find(".panel-tool-close").click();
			}  
			function doGroupMemberSearch(value){
				/* 分开特殊报价单中同行用户与普通用户 */
				var number;
				if($("#user_type").find("option:selected").attr("grade")==1){
					if($("#account_type").find("option:selected").attr("grade")==4){
						number=1;
					}
				}else if($("#user_type").find("option:selected").attr("grade")==2){
					if($("#account_type").find("option:selected").attr("grade")==4){
						number=2;
					}
				}
			    $.post("${backServer}/frontUser/queryFrontUserLikeByAccountType",{"account":value,"user_type":number}, function(data){  
			 	$("#member_from").empty();
				$.each(data.frontUserList,function(i,val){
			 		$("#member_from").append("<option >"+val.account+"</option>");
			 	});
			  },"json");
			}
			
			$(function() {
				//member
				//选择一项
				$("#member_addOne").click(function() {
					addOne($("#member_from option:selected"),"#member_to");
				});

				//选择全部
				$("#member_addAll").click(function() {
					addOne($("#member_from option"),"#member_to");
				});

				//移除一项
				$("#member_removeOne").click(function() {
					addOne($("#member_to option:selected"),"#member_from");
				});

				//移除全部
				$("#member_removeAll").click(function() {
					addOne($("#member_to option"),"#member_from");
				});
			});
			
			function addOne(source,addTo) {
				$.each(source,function(i,val) {
		 			var temp=addTo+" option";
					var needAdd=-1;
					$.each($(temp),function(j,val1){
		 				if(val.text==val1.text){
		 					needAdd=1;
		 				}
		 			});
		 			if(needAdd==-1){
		 				$(addTo).append("<option >"+val.text+"</option>");
		 			}
		 		});
		 		source.remove();
			} 
		}
		
		//报价单名文本框鼠标失去焦点，校验报价单名是否已经存在
		function checkUser(){
			var quota_name = $("#quota_name").val();
			if(quota_name==""){
				$('#serviceNameTextArea').text('报价单名称不能为空!');
				$('#serviceNameTextArea').css('color','red');
			}if(quota_name!=""){
				$.ajax({
					url:'${backServer}/quotationManage/checkQM',
					type:'post',
					data:{quota_name:quota_name},
					success:function(data){
						if(data['result']==true){
							$('#adduser_check').val("1");
							$('#serviceNameTextArea').text('该报价单名称可以使用!');
							$('#serviceNameTextArea').css('color','green');
						}else{
							$('#adduser_check').val("0");
							$('#serviceNameTextArea').text('该报价单名称已经存在!');
							$('#serviceNameTextArea').css('color','red');
						}
					}
				});
			}
		}
		
	</script>
</body>
</html>