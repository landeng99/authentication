<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <input type="hidden" id="rate_type_bk" value="${params.rate_type}">
      <div class="panel-head"><strong>会员等级查询</strong></div>
      <div class="tab-body">
        <br/>
        <form action="${backServer}/memberRate/search" method="post" id="myform" name="myform">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户类型:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:240px">
                         <select id="rate_type" name ="rate_type" class="input" style="width:200px">
                            <option value="">请选择</option>
                            <option value=1>普通用户</option>
                            <option value=2>同行用户</option>
                         </select>
                    </div>
                  </div>
            </form>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" id="search" value="查询" />
                    <input type="button" class="button button-small border-green" id="add" value="添加 "/>
                 </div>
                 <table class="table">
                    <tr><th width="25%">会员类型</th>
                        <th width="25%">等级名称</th>
                        <th width="25%">积分下限</th>
                        <th width="25%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list">
                  <c:forEach var="memberRate"  items="${memberRateList}">
                    <tr> 
                         <td width="25%">
                             <c:if test="${memberRate.rate_type==1}">普通用户</c:if>
                             <c:if test="${memberRate.rate_type==2}">同行用户</c:if>
                         </td>
                         <td width="25%">${memberRate.rate_name}</td>
                         <td width="25%">${memberRate.integral_min}</td>
                         <td width="25%">
                           <a class="button border-blue button-little" href="javascript:void(0);" onclick="modify('${memberRate.rate_id}')">编辑</a>
                           <c:if test="${memberRate.integral_min != 0}">
                           		<a class="button border-blue button-little" href="javascript:void(0);" onclick="del('${memberRate.rate_id}')">删除</a>
                           </c:if>
                         </td>
                     </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
       
        $(function() {
           
            //选择框选中
            $("#rate_type").val($("#rate_type_bk").val());
            
            $('#add').click(function() {
                var url = "${backServer}/memberRate/add";
                window.location.href = url;
            });

            $('#search').click(function() {

                document.getElementById('myform').submit();
            });
        });
        function modify(rate_id) {
            
            var url = "${backServer}/memberRate/modify?rate_id=" + rate_id;
            window.location.href = url;
        }

        function del(rate_id){
            
            if(confirm("是否要删除该条记录？")){
                 var url = "${backServer}/memberRate/del"
                 $.ajax({
                  url:url,
                  data:{
                      "rate_id":rate_id},
                  type:'post',
                  dataType:'text',
                  success:function(data){
                	  if(data == "1"){
                     alert("删除成功");
                     window.location.href = "${backServer}/memberRate/memberRateList";
                	  }else{
                		  alert("这个基础会员,不允许删除本会员信息");
                	  }
                  }
                 });
            }
          }
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  document.getElementById('myform').submit();
               }
          }
        });
    </script>
</body>

</html>