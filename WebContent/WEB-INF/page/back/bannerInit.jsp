<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
 <link rel="stylesheet" href="${backJsFile}/jquery-foxibox/style/jquery-foxibox-0.2.css">
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
	
</style>
<body>
	<div class="admin">
		<div class="admin_context">
		<div class="panel admin-panel">
			<div class="panel-head" style="height: 50px;">
				<strong style="float:left;text-align: center;margin-left: 10px;">轮播图列表</strong>
			</div>
			<div class="padding border-bottom" style="height: 50px;">
		  <shiro:hasPermission name="banner:upload">
				<input type="button"  
				class="button button-small border-green"
				onclick="uploadBanner()" value="上传" />
		  </shiro:hasPermission>
			</div>
			
			<table class="table table-hover">
				<tr><th>图片</th>
				<th>关键字说明</th>
				<th>状态</th>
				<th>操作</th></tr>
			<c:forEach var="banner" items="${bannerLists}">
				<tr><td>
					<c:if test="${banner.status==1 }">
					<a href="${resource_path}/${banner.imageurl}"  rel="[gall1]"></c:if>
						<img src="${resource_path}/${banner.imageurl}" height="40"/>
					<c:if test="${banner.status==2 }"><a></c:if>
					</a>
				</td>
				<td>${banner.keyword}</td>
				<td><c:if test="${banner.status==1 }">启用</c:if>
					<c:if test="${banner.status==2 }">禁用</c:if></td>
				<td>
				<shiro:hasPermission name="banner:enable">
				<c:if test="${banner.status==2 }">
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="updateBanner('${banner.banner_id }')">启用</a>&nbsp;&nbsp;
				</c:if>
				<c:if test="${banner.status==1 }">
					<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateBanner('${banner.banner_id }')">禁用</a>&nbsp;&nbsp;
      			</c:if>
      			</shiro:hasPermission><shiro:hasPermission name="banner:delete">
      				<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="del('${banner.banner_id }')">删除</a>&nbsp;&nbsp;
				</shiro:hasPermission>
				</td></tr>	
			</c:forEach>
			</table>
			
			<div class="panel-foot text-center">
	      		<jsp:include page="webfenye.jsp"></jsp:include>
	      	</div>
		</div>
		</div>
	</div>
</body>
<script src="${backJsFile}/jquery-foxibox/script/jquery-foxibox-0.2.min.js"></script>
<script src="${backJsFile}/jquery-migrate-1.2.1.min.js"></script>


<script type="text/javascript">
	
	//图片播放
	$(document).ready(function(){
	  $('a[rel]').foxibox();
	});

	function uploadBanner(){
		var url = "${backServer}/banner/toBannerUpload";
		window.location.href = url;
	}
	
	//更新状态,是否启用
	function updateBanner(banner_id){
		var url = "${backServer}/banner/updateBanner?banner_id="+banner_id;
		$.ajax({
			url:url,
			type:'GET',
			success:function(data){
				 window.location.href = "${backServer}/banner/queryAll";
			}
		});
	}
	
	function del(banner_id){
		if(confirm("确认删除吗？")){
			var url = "${backServer}/banner/deleteBanner?banner_id="+banner_id;
			$.ajax({
				url:url,
				type:'GET',
				success:function(data){
					 alert("删除成功！");
					 window.location.href = "${backServer}/banner/queryAll";
				}
			});
		}
	}
</script>
</html>