<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
  <style type="text/css">
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 80px;
	}
	
	.field-group .field-item .interval_label{
		float: left;
	}
	.field-group .field-item .field{
		float:left;
		width: 203px;
	}
	
	.pickorder-list{
		margin-top: 20px;
	}
</style>
<body>
 	<div class="admin">
 			<div class="admin_context">
 			    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li><a href="#" onclick="memberbill()">待合计</a></li>
          <li  class="active"><a href="#batchlist" >已合计</a></li>
        </ul>
      </div>
 			   <div class="tab-body">
 			   	<div class="tab-panel active" id="batchlist">
 			   	  <div class="field-group">
				<form id="form" action="${backServer}/pickorder/search">
				 <div class="field-item">
					  <div class="label"><label for="pick_code">账号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="pick_code" name="pick_code" style="width:200px"  />
                    </div>
				 </div>
                <div class="field-item">
                	 <div class="label"><label for="flight_num">用户名:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="flight_num" name="flight_num" style="width:200px">
                    </div>
                </div>
               <div class="field-item">
                	 <div class="label"><label for="pick_code">运单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="logistics_code" name="logistics_code" style="width:200px"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>
                <div class="field-item">
                	<div class="label"><label for="lastname">创建时间:</label></div>
                    <div class="field">
                 	<input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                  			onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                   	<div class="interval_label"><label for ="interval">~</label></div>
                   	<div class="field">
                	<input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                   		onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                   	</div>
                </div>
                   <div style="clear: both"></div>
                 <div class="form-button">
                     	<input type="submit" class="button bg-main icon-search" id="search" value="查询"/>
				  </div>
				 </form> 
             </div>
 			   	       <div class="panel admin-panel pickorder-list">
	         	     <div class="panel-head"><strong>操作记录</strong></div>
	         	     <div class="padding border-bottom">
	         	    <shiro:hasPermission name="pickorder:export">
			            <input type="button" id="export" value="导出" class="button button-small border-green">
			        </shiro:hasPermission>   
			        <shiro:hasPermission name="pickorder:import">
			            <input type="button" id="import" value="导出" class="button button-small border-green">
			        </shiro:hasPermission>
			        </div>
	         	     <table class="table table-hover" id="batchlist">
	         	     	<tr>
	         	     		<th>批次</th>
	         	     		<th>操作人员</th>
	         	     		<th>时间</th>
	         	     		<th>操作</th>
	         	     	</tr>
		        	</table>
		        	<div class="panel-foot text-center">
	      				<jsp:include page="webfenye.jsp"></jsp:include>
	      			</div>
	         		</div>
 			   	</div>
 			   </div>
 			</div>
 	</div>
 	<script type="text/javascript">
 	
 	function memberbill(){
 		window.location.href="${backServer}/memberbill/list";
 	}
 	</script>
</body>
</html>