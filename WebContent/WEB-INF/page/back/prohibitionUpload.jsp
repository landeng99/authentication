<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">禁运物品上传</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br /><div class="tab-panel active form-x" id="tab-set">
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="myform" name="myform" action="${backServer}/prohibition/addProhibition" method="post">
			
			<input type="hidden" id="prohibition_id" name="prohibition_id" 
				value="${prohibitionPojo.prohibition_id }"/>
			
			<input type="hidden" id="imageurl" name="imageurl" 
				value="${prohibitionPojo.imageurl }"/>
            
                <div class="form-group" style="clear:left; float:left; width: 80%;">
                    <div class="label" style="float: left;padding-top:8px;"><label for="name">类型名称:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float: left; width: 80%;">
                    	<input type="text" class="input" 
                    		id="class_name" name="class_name" 
                    	 	value="${prohibitionPojo.class_name }" 
                    		onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
                    		placeholder="请填写物品类型名称" 
                    		data-validate="required:请填写你的物品类型名称"/>
                  		<span style="color: red;" id="class_nameNote"></span>
                    </div>
                  </div>
                
                <div class="form-group" style="clear:left; width: 80%; float:left; margin-top: 10px; margin-bottom: 10px;">
                    <div class="label" style="float: left;padding-top:8px;"><label for="name">&nbsp;类别名称&nbsp;&nbsp;:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float: left; width: 80%;">
                    	<input type="text" class="input" 
                    		id="child_cla_group" name="child_cla_group" 
                    		value="${prohibitionPojo.child_cla_group }" 
                    		onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
                    		placeholder="请填写1-3个小类别名称" 
                    		data-validate="required:请填写你的1-3个小类别名称"/>
                   		<span style="color: red;" id="child_cla_groupNote"></span>
                    </div>
                </div>
                
                <div class="form-group" style="clear:left; float:left; width: 80%;">
                    <div class="label" style="float: left;padding-top:8px;"><label for="name">&nbsp;描述详情&nbsp;&nbsp;:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float: left; width: 80%;">
                    	<input type="text" class="input" 
                    		id="description" name="description" 
                    		value="${prohibitionPojo.description }" 
                    		onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
                    		placeholder="请填写物品介绍描述" 
                    		data-validate="required:请填写你的物品介绍描述"/>
                   		<span style="color: red;" id="descriptionNote"></span>
                    </div>
                </div>
                
               <div class="form-group" style="clear: both; float:left; margin-top: 20px; margin-bottom: 20px; width: 80%;">
                     <div class="label" style="float:left;margin-top:5px; width: 14%;""><label for="name">上传轮播图:</label></div>
                     <div style="float:left;margin-top:5px;margin-left:40px;width:100px">
                       <%-- <shiro:hasPermission name="pkgreturn:upload"> --%>
                       <input class=" input-file" type="file"  id="prohibitionPic" name="prohibitionPic" 
                       		onchange="prohibitionPicChange()"/>
                       <%-- </shiro:hasPermission> --%>
                       <span style="color: red;" id="prohibitionPicNote"></span>
                     </div>
                     <div style="clear: both;margin-left:40px;">
                     <c:if test="${prohibitionPojo.imageurl != ''}">
                     	<img src="${resource_path}/${prohibitionPojo.imageurl}" id="urlArea"  name ="urlArea" width="320" height="240"/>
                     </c:if><c:if test="${prohibitionPojo.imageurl == ''}">
                     	<img src="" id="urlArea"  name ="urlArea" width="320" height="240"/>
                     </c:if>
                       
                     </div>
                </div>
                <div class="form-button" style="clear:left; float:left;">
                	<a href="javascript:void(0);" class="button bg-main" onclick="save()">保存</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
             </form>
        </div>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
document.getElementById('class_name').focus();

var img_suffix_ary = new Array();
img_suffix_ary[0] = ".bmp";
img_suffix_ary[1] = ".jpg";
img_suffix_ary[2] = ".jpeg";
img_suffix_ary[3] = ".png";
img_suffix_ary[4] = ".gif";
img_suffix_ary[5] = ".pcx";
img_suffix_ary[6] = ".raw";
img_suffix_ary[7] = ".tiff";
img_suffix_ary[8] = ".tga";
img_suffix_ary[9] = ".exif";
img_suffix_ary[10] = ".fpx";
img_suffix_ary[11] = ".svg";
img_suffix_ary[12] = ".psd";
img_suffix_ary[13] = ".cdr";
img_suffix_ary[14] = ".pcd";
img_suffix_ary[15] = ".dxf";
img_suffix_ary[16] = ".ufo";
img_suffix_ary[17] = ".eps";
img_suffix_ary[18] = ".hdri";
img_suffix_ary[19] = ".ai";
    //获取文件后缀
function test(file_name){
	var result =/\.[^\.]+/.exec(file_name);
	return result;
}

var imgurlVal = $("#urlArea").val();
function prohibitionPicChange(){
	//文件名
	var file_name = $("#prohibitionPic").val();
	//后缀
	var result = test(file_name);
	//后缀是否存在于图片格式中
	var isImgSuffix = false;
	
	var x;
	for (x in img_suffix_ary)
	{	
		if(result == img_suffix_ary[x]){
			//存在
			isImgSuffix = true;
			break;
		}
	}
	//
	if(isImgSuffix == false){
		 $("#prohibitionPicNote").html("文件["+file_name+"]不是图片格式,请选择图片上传");
		 $("#prohibitionPicNote").show();
		return;
	}else{
		$("#prohibitionPicNote").hide();
	}
	
	 
     var url = "${backServer}/prohibition/uploadProhibition";
      $.ajaxFileUpload({
          url : url,
          data:{"imgText":imgurlVal,"prohibition_id":$('#prohibition_id').val()},
          type : 'post',
          secureuri : false,
          fileElementId : 'prohibitionPic',
          dataType : 'text/html',
          success : function(data ) {
          	$('#urlArea').attr("src","${resource_path}/"+data);
          	imgurlVal = data;
          },
          error : function(data, status, e) {
              alert("error" + e);
          }
      }); 
  }
  
function LostFocus_Code(text,ereIdName,idName) {
    var code = $("#"+idName).val();
    if (code == "" || code == "请输入"+text) {
        $("#"+ereIdName).html(text+"不能为空!");
        $("#"+ereIdName).show();
        return false;
    } else {
        $("#"+ereIdName).hide();
        return true;
    }
}

function save(){

	if(imgurlVal == ""){
		$("#prohibitionPicNote").html("请上传图片");
		$("#prohibitionPicNote").show();
        return;
	}else{
		$("#prohibitionPicNote").hide();
	}
	
	var class_name = $("#class_name").val();
	if(class_name == "" || class_name == "请填写物品类型名称"){
        return;
    }
	
	var child_cla_group = $("#child_cla_group").val();
	if(child_cla_group == "" || child_cla_group == "请填写1-3个小类别名称"){
        return;
    }
    
	var description = $("#description").val();
	if(description == "" || description == "请填写物品介绍描述"){
        return;
    }
	
	//补充图片信息
	$("#imageurl").val(imgurlVal);
	
    myform.submit();
  }
  
  
</script>
</body>