<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
 <link rel="stylesheet" href="${backCssFile}/jquery.treetable.css">
 <link rel="stylesheet" href="${backCssFile}/jquery.treetable.theme.modified.css">
<script type="text/javascript" src="${backJsFile}/jquery.treetable.js"></script>
 
	
<body>
	<div class="admin">
		<div class="admin_context">
	    <div class="panel admin-panel">
		  <div class="padding border-bottom">
		  <shiro:hasPermission name="goodsType:add">
	      		<a href="${backServer}/goodsType/inGoodsTypeInfo?goodsTypeId=0" class="button button-small border-green" />添加申报类型</a>
	      </shiro:hasPermission>
	      </div>
		<table id="pkg_category" class="table table-hover">
		 <thead>
			<tr>
				<th>申报类名</th><th>单位</th><th>操作</th>
			</tr>
		 </thead>
		 
		 <c:forEach var="goodsType" items="${goodsTypeLists}">
			<tr data-tt-id="${goodsType.goodsTypeId}" data-tt-parent-id="${goodsType.parentId}">
	 			<td>${goodsType.nameSpecs}</td>
	 			<td>${goodsType.unit}</td>
	 			<td>
	 			<shiro:hasPermission name="goodsType:edit">
	 			<a class="button border-blue button-little" href="${backServer}/goodsType/inGoodsTypeInfo?goodsTypeId=${goodsType.goodsTypeId}">编辑</a>
	 			</shiro:hasPermission>
	 			<shiro:hasPermission name="goodsType:delete">
	 			<a class="button border-blue button-little" href="${backServer}/goodsType/deleteGoodsType?goodsTypeId=${goodsType.goodsTypeId}">删除</a>
	 			</shiro:hasPermission>
	 			</td>
		   </tr>
		   </c:forEach> 
		 </table>	
		</div>
	</div>
	</div>
<script type="text/javascript">
	$("#pkg_category").treetable({expandable: true,initialState:"expanded" });
</script>
</body>
</html>