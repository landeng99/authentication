<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@include file="header.jsp" %>
  <style type="text/css">
	.pkg_field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.pkg_field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
	.pkg_field-group .field-item .field{
		float:left;
	}
	
	.pkg-list{
		margin-top: 20px;
	}
</style>
 
 
<body>
	<div class="admin">
	    <div class="pkg_field-group">
				<form id="form" action="${backServer}/pallet/searchforaddpallet">
				 <div class="field-item">
					  <div class="label"><label for="logistics_code">包裹单号(or关联单号):</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="logistics_code" name="logistics_code" style="width:200px"  />
                    </div>
				 </div>
                <div class="field-item">
                	<div class="label"><label for="fromDate">创建时间</label></div>
                    <div class="field">
                  	<input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                  			onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                   	<div class="label"><label for ="interval">~</label></div>
                   	<div class="field">
                   	<input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                   		onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                   	</div>
                </div>
				<input type="hidden" id="seaport_id" name="seaport_id" value="${seaport_id}"/>
			    <input type="hidden" id="pick_id" name="pick_id" value="${pick_id}"/>
			    <input type="hidden" id="pallet_id" name="pallet_id" value="${pallet_id}"/>
			    <input type="hidden" id="overseas_address_id" name="overseas_address_id" value="${overseas_address_id}"/>
			    
                 <div class="form-button">
                     	<a href="javascript:void(0);" class="button bg-main icon-search" id="search" >查询</a>
				  </div>
			 </form>
             </div>
    	<div class="panel admin-panel pkg-list">
    	<form method="post">
        <div class="panel-head"><strong>包裹查询</strong></div>
         	<div class="padding border-bottom">
	            <input type="button" value="全选" checkfor="pkg_id" name="checkall" class="button button-small checkall">
        	</div>
	     <table class="table" id="pkgList">
                <tr>
                 <th>选择</th>
                 <th>公司运单号</th>
                 <th>关联单号</th>
                 <th>用户账号</th>
                 <th>收件人</th>
                 <th>身份证号</th>
                 <th>联系方式</th>
                 <th>收件地址</th>
                 <th>支付状态</th>
                 <th>包裹状态</th>
                 <th>操作</th>
               </tr> 
                <c:forEach var="pkg" items="${pkgList}">
               <tr>
               <td>
              	 <input type="checkbox" name="pkg_id"  value="${pkg.package_id }"/>
               </td>
               <td >${pkg.logistics_code}</td>
               <td>${pkg.original_num}</td>
               <td>${pkg.email}</td>
               <td>${pkg.receiver}</td>
               <td>${pkg.idcard}</td>
               <td>${pkg.mobile}</td>
               <td>${pkg.address}</td>
               <td>
				    	<c:if test="${pkg.pay_status==1}">运费待支付</c:if>
                        <c:if test="${pkg.pay_status==2}">运费已支付</c:if>
                        <c:if test="${pkg.pay_status==3}">关税待支付</c:if>
                        <c:if test="${pkg.pay_status==4}">关税已支付</c:if>
               </td>
               <td>待发货</td>
               <td><a href="javascript:;">详情</a></td>
               </tr>   
               </c:forEach>
         </table>    
         <div class="panel-foot text-center">
      		 <jsp:include page="webfenye.jsp"></jsp:include>
      	</div>
      	</form>	
    </div>
 	  <div class="form-button" style="margin-top: 20px;text-align: center;">
  	   	  <a id="transmit" class="button bg-main" href="javascript:;" >放入托盘</a>
  	   	  <a id="close" href="javascript:;" class="button bg-main">返回</a>
	  </div>		
 </div>
</body>
<script type="text/javascript">
$('#transmit').bind('click', function(){
		
	//获取选中的行
  	var $checkbox=$('#pkgList').find('input:checked');
  	
	//存放被选中包裹
  	var addData=[];
	var  bool=-1;	  		
  	$.each($checkbox,function(i,checkbox){
  		var $checkbox=$(checkbox);
  		var package_id=$checkbox.val();
  		var logistics_code=$checkbox.parent().next().text();
  		var data={'package_id':package_id,'logistics_code':logistics_code};
  		addData.push(data);
  	});

  	//将数据放入托盘中
  	if(addData.length>0){
  		var url='';
	  	var seaport_id=$('#seaport_id').val();
	  	var pick_id=$('#pick_id').val();
	  	var pallet_id=$('#pallet_id').val();
	  	 
   		$.ajax({
  			url:"${backServer}/pallet/addpkgbatch",
  			data:{package_str:JSON.stringify(addData),'seaport_id':seaport_id,'pick_id':pick_id,'pallet_id':pallet_id},
  			type:'post',
  			dataType:'json',
  			success:function(data){
  				if(data.result){
  					alert("放入托盘成功");
  					window.location.href=window.location.href
  				}else{
  					
  					alert(data['msg']);
  				}
  			}
  		}); 
  	}
  });
  $('#close').click(function(){
		 var pallet_id=$('#pallet_id').val();
		 var url="${backServer}/pallet/initedit?pallet_id="+pallet_id;
		 window.location.href=url;
		window.location.href=url;
  });
  
  $('#search').click(function(){
	  $('#form').submit();
  });
</script>
</html>