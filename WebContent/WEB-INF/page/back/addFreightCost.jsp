<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>
<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>会员费用添加</strong></div>
      <div class="tab-body">
        <br/>
                  <input type="hidden" id=checkRateIdResult value="1">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>等级名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <select id="rate_id" class="input" style="width:100px;" onchange="checkRateId()">
                            <option value="">请选择</option>
                            <c:forEach var="memberRate"  items="${memberRateList}">
                            <option value="${memberRate.rate_id}">${memberRate.rate_name}</option>
                            </c:forEach>
                       </select>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="rateIdTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <input type="text" class="input" id="unit_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/千克</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="unitPriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>首重单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <input type="text" class="input" id="first_weight_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/千克</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="firstPriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>续重单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <input type="text" class="input" id="go_weight_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/千克</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="goPriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>合箱费用:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                           <input type="text" class="input" id="merge_price" style="width:100px"
                                  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/票</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="mergePriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>分箱费用:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                           <input type="text" class="input" id="unpack_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/票</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="unpackPriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>清点费用:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                           <input type="text" class="input" id="check_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/票</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="checkPriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>加固费用:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                           <input type="text" class="input" id="reinforce_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/票</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="reinforcePriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>退货费用:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                           <input type="text" class="input" id="return_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/票</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="returnPriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>取发票费:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                           <input type="text" class="input" id="invoice_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/票</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="invoicePriceTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>免仓租期:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                           <input type="text" class="input" id="rent_days" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>天</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="rentDaysTextArea"></span>
                    </div>
                 </div>

        
                  <div class="padding border-bottom" >
                    <input type="button" class="button bg-main" onclick="add()" value="保存" />
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
                 </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
    $(function(){
        $('#rate_id').focus();
    });
    
    //资费名称文本框鼠标失去焦点，校验资费名称是否已经存在
    function checkRateId(){
        $('#rateIdTextArea').empty();
        var url = "${backServer}/freightCost/check";
        var rate_id =$('#rate_id').val();

        if(rate_id==null || rate_id.trim()==""){
            return;  
        }
        $.ajax({
            url:url,
            type:'post',
            data:{"rate_id":rate_id},
            success:function(data){
                $('#checkRateIdResult').val(data);
                if(data!="1"){
                    $('#rateIdTextArea').text('会员等级已经存在!');
                    $('#rateIdTextArea').css('color','red');
                }
            }
            
        });

    }


        function add() {

        $('#rateIdTextArea').empty();
        $('#unitPriceTextArea').empty();
        $('#firstPriceTextArea').empty();
        $('#goPriceTextArea').empty();
        $('#mergePriceTextArea').empty();
        $('#unpackPriceTextArea').empty();
        $('#checkPriceTextArea').empty();
        $('#reinforcePriceTextArea').empty();
        $('#returnPriceTextArea').empty();
        $('#invoicePriceTextArea').empty();
        $('#rentDaysTextArea').empty();

        var url = "${backServer}/freightCost/insert";

        var rate_id = $('#rate_id').val();
        var unit_price = $('#unit_price').val();
        var first_weight_price = $('#first_weight_price').val();
        var go_weight_price = $('#go_weight_price').val();
        var merge_price = $('#merge_price').val();
        var unpack_price = $('#unpack_price').val();
        var check_price = $('#check_price').val();
        var reinforce_price = $('#reinforce_price').val();
        var return_price = $('#return_price').val();
        var invoice_price = $('#invoice_price').val();
        var rent_days = $('#rent_days').val();

        var checkResult = "0"
        // 金额检验
        var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
        // 天数检验
        var daysexp = /^[0-9]*[1-9][0-9]*$/;

        if (rate_id == null || rate_id.trim() == "") {
            $('#rateIdTextArea').text('会员等级不能为空!');
            $('#rateIdTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (unit_price == null || unit_price.trim() == "") {
            $('#unitPriceTextArea').text('单价不能为空!');
            $('#unitPriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(unit_price)) {
            $('#unitPriceTextArea').text('金额格式错误！');
            $('#unitPriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (first_weight_price == null || first_weight_price.trim() == "") {
            $('#firstPriceTextArea').text('首重单价不能为空!');
            $('#firstPriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(first_weight_price)) {
            $('#firstPriceTextArea').text('金额格式错误！');
            $('#firstPriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (go_weight_price == null || go_weight_price.trim() == "") {
            $('#goPriceTextArea').text('续重单价不能为空!');
            $('#goPriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(go_weight_price)) {
            $('#goPriceTextArea').text('金额格式错误！');
            $('#goPriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (merge_price == null || merge_price.trim() == "") {
            $('#mergePriceTextArea').text('合箱费用不能为空!');
            $('#mergePriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(merge_price)) {
            $('#mergePriceTextArea').text('金额格式错误！');
            $('#mergePriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (unpack_price == null || unpack_price.trim() == "") {
            $('#unpackPriceTextArea').text('分箱费用不能为空!');
            $('#unpackPriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(unpack_price)) {
            $('#unpackPriceTextArea').text('金额格式错误！');
            $('#unpackPriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (check_price == null || check_price.trim() == "") {
            $('#checkPriceTextArea').text('清点费用不能为空!');
            $('#checkPriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(check_price)) {
            $('#checkPriceTextArea').text('金额格式错误！');
            $('#checkPriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (reinforce_price == null
        || reinforce_price.trim() == "") {
            $('#reinforcePriceTextArea').text('加固费用不能为空!');
            $('#reinforcePriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(reinforce_price)) {
            $('#reinforcePriceTextArea').text('金额格式错误！');
            $('#reinforcePriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (return_price == null || return_price.trim() == "") {
            $('#returnPriceTextArea').text('退货费用不能为空!');
            $('#returnPriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(return_price)) {
            $('#returnPriceTextArea').text('金额格式错误！');
            $('#returnPriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (invoice_price == null || invoice_price.trim() == "") {
            $('#invoicePriceTextArea').text('取发票费不能为空!');
            $('#invoicePriceTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!exp.test(invoice_price)) {
            $('#invoicePriceTextArea').text('金额格式错误！');
            $('#invoicePriceTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (rent_days == null || rent_days.trim() == "") {
            $('#rentDaysTextArea').text('免仓租期不能为空!');
            $('#rentDaysTextArea').css('color', 'red');
            checkResult = "1";
        } else if (!daysexp.test(rent_days)) {
            $('#rentDaysTextArea').text('天数格式错误！');
            $('#rentDaysTextArea').css('color', 'red');
            checkResult = "1";
        }

        if (checkResult == "1") {
            return;
        }

        if ($('#checkRateIdResult').val() != "1") {
            alert("会员等级已经存在!");
            return;
        }
        $
        .ajax({
            url : url,
            data : {
                "rate_id" : rate_id,
                "unit_price" : unit_price,
                "first_weight_price" : first_weight_price,
                "go_weight_price" : go_weight_price,
                "merge_price" : merge_price,
                "unpack_price" : unpack_price,
                "check_price" : check_price,
                "reinforce_price" : reinforce_price,
                "return_price" : return_price,
                "invoice_price" : invoice_price,
                "rent_days" : rent_days},
            type : 'post',
            dataType : 'text',
            success : function(data) {
                alert("添加成功");
                window.location.href = "${backServer}/freightCost/freightCostList";
            }
        });
        }
    </script>
</body>
</html>