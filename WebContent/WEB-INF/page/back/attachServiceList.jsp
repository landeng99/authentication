<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>
<style type="text/css">
    .span1{
        display:-moz-inline-box;
        display:inline-block;
        width:25px;
        height:20px;
    }
</style>
<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>增值服务查询</strong></div>
      <div class="tab-body">
        <br />
        <form action="${backServer}/attachService/attachServiceSearch" method="post" id="myform" name="myform">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>增值服务名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:250px">
                         <select id="service_name" name="service_name" class="input" style="width:200px">
                            <option value="">请选择</option>
                            <c:forEach var="name"  items="${attachServiceName}">
                               <option value="${name}">${name}</option>
                            </c:forEach>
                         </select>
                    </div>
                 </div> 
        </form>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" onclick="search()" value="查询" />
                 <shiro:hasPermission name="attachService:add">
                    <input type="button" class="button button-small border-green" onclick="add()" value="添加 "/>
                 </shiro:hasPermission>
                 
                 
                 <div style="height:30px;margin-bottom: 10px; width: 200px; float: right;">
                     <span style="background:blue;" class="span1">&nbsp</span>
                     <span id="excelSpan">同行会员</span>
                  </div>
                 <div style="height:30px;margin-bottom: 10px; width: 200px; float: right;">
                     <span style="background:red;" class="span1">&nbsp</span>
                     <span id="excelSpan">普通会员</span>
                  </div>
                  </div>
                  
                 <table class="table">
                    <tr><th width="9%">增值服务名称</th>
                        
                        	<c:forEach items="${memberRates}" var="memberRate">
                        <th width="${70/lengSize}%">
                        <c:if test="${memberRate.rate_type == 1}">
                        	<font color="red">${memberRate.rate_name}</font>
                        </c:if>
                        <c:if test="${memberRate.rate_type == 2}">
                        	<font color="blue">${memberRate.rate_name}</font>
                        </c:if>	
                        </th>
                        	</c:forEach>
                        
                        <th width="10%">描述信息</th>
                        <th width="9%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="attachService"  items="${attachServiceList}" varStatus="asvs">
                     <tr><td width="9%">${attachService.service_name}</td>
                         
                         <c:forEach items="${servicePriceList}" var="servicePrice" begin="${asvs.count*lengSize - lengSize}" end="${asvs.count*lengSize - 1}">
                         	<td width="${70/lengSize}%">
                         			<fmt:formatNumber value="${servicePrice}" type="currency" pattern="$#0.00#"/>
                        	 </td>
                         </c:forEach>
                         	<%-- <c:forEach items="${attachList}" var="attach">
                         		<c:if test="${attach.attachService.attach_id == attachService.attach_id}">
                         <td width="10%">
                         			<fmt:formatNumber value="${attach.service_price}" type="currency" pattern="$#0.00#"/>
                         </td>
                         		</c:if>
                         	</c:forEach> --%>
                         
                         <td width="10%">${attachService.description}</td>
                         <td width="9%">
                         <shiro:hasPermission name="attachService:update">
                           <a class="button border-blue button-little" href="javascript:void(0);"onclick="modify('${attachService.attach_id}')">修改</a>
                         </shiro:hasPermission><shiro:hasPermission name="attachService:delete">
                         	<c:if test="${attachService.service_type != 1}">
                           <a class="button border-blue button-little" href="javascript:void(0);"onclick="deleteAttachService('${attachService.attach_id}','${attachService.service_name}')">删除</a>
                           </c:if>
                         </shiro:hasPermission>
                         </td>
                         </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
            $('#service_name').focus();
        });
        
        function modify(attach_id){
            var url = "${backServer}/attachService/modifyAttachService?attach_id="+attach_id;
            window.location.href = url;
        }
        function deleteAttachService(attach_id,attach_name){
          if("分箱"==attach_name || "合箱"==attach_name){
        	  confirm(attach_name+"是不能删除的？");
        	  return false;
          }
          if(confirm("是否要删除该条记录？")){
               var url = "${backServer}/attachService/deleteAttachService"
               $.ajax({
                url:url,
                data:{
                    "attach_id":attach_id},
                type:'post',
                dataType:'text',
                success:function(data){
                   alert("删除成功");
                   window.location.href = "${backServer}/attachService/attachServiceInit";
                }
               });
          }
        }
        
        function search(){ 
          document.getElementById('myform').submit();
        }
        
        function add(){
            var url = "${backServer}/attachService/addAttachService";
            window.location.href = url;
        }
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search();
               }
          }
        });
    </script>
</body>
</html>