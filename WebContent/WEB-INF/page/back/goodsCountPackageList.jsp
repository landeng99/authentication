<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .div-title{
        font-weight: bold;
        float:left;
        width:90px;
        margin-top:5px;
        margin-left:10px;
    }
        .div-input{
        float:left;
        width:220px;
    }
    
    .input-width{
        width:200px;
    }

</style>
<div class="admin">
    <div class="panel admin-panel">
        <div class="panel-head"><strong>货量统计查询</strong></div>
        </br>
          <form action="${backServer}/pkg/goodsCountSearch" method="post" id="myform" name="myform">

             <div style="height: 40px;">
                  <div class="div-title">起始时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
                  <div class="div-title">终止时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
                            value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
             </div>
          </form>
             <div class="padding border-bottom">
                  <input type="button" class="button bg-main" onclick="search()" value="查询" />
                  <input type="button" class="button bg-main" onclick="batchExport()" value="批量导出" />                     
             </div>
             <!-- <div style="float:left;width:800px;">
                  <a class="button border-blue button-little" href="javascript:void(0);" id ="selectAll">全选</a>
                  <a class="button border-blue button-little" href="javascript:void(0);" id ="reverse">反选</a>
               </div>  -->            
             <table class="table">
                  <tr>
                    <!-- <th width="3%"></th>  -->                 
                    <th width="12%">公司运单号</th>
                    <th width="20%">关联单号</th>
                    <th width="12%">派送单号</th>
                    <th width="18%">用户名称</th>
                    <th width="10%">支付状态</th>
                    <th width="12%">包裹状态</th>
                    <th width="18%">包裹操作</th>
                  </tr>    
             </table>
             <table class="table table-hover">
               <c:forEach var="pkg"  items="${pkgList}">
                   <tr> 
                      <%-- <td width="3%">
                           <input type="checkbox" class="checkbox" name="select" id="${pkg.package_id}" value="${pkg.package_id}">
                      </td>   --%>                     
                      <td width="12%">${pkg.logistics_code}</td>
                      <td width="20%"><div style="width:180px;word-wrap:break-word;">${pkg.original_num}</div></td>
                      <td width="12%">${pkg.ems_code}</td>
                      <td width="18%">${pkg.user_name}</td>
                      <td width="10%">
                         <c:if test="${pkg.pay_status==1}">运费待支付</c:if>
                         <c:if test="${pkg.pay_status==2}">运费已支付</c:if>
                         <c:if test="${pkg.pay_status==3}">关税待支付</c:if>
                         <c:if test="${pkg.pay_status==4}">关税已支付</c:if>
                      </td>
                      <td width="12%">
                         <c:if test="${pkg.status==-1}">异常</c:if>
                         <c:if test="${pkg.status==0}">待入库</c:if>
                         <c:if test="${pkg.status==1}">已入库</c:if>
                         <c:if test="${pkg.status==2}">待发货</c:if>
                         <c:if test="${pkg.status==3}">已出库</c:if>
                         <c:if test="${pkg.status==4}">空运中</c:if>
                         <c:if test="${pkg.status==5}">待清关</c:if>
                         <c:if test="${pkg.status==7}">已清关派件中</c:if>
                         <c:if test="${pkg.status==9}">已收货</c:if>
                         <c:if test="${pkg.status==20}">废弃</c:if>
                         <c:if test="${pkg.status==21}">退货</c:if>
                      </td>
                      <td width="18%">
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="detail('${pkg.package_id}')">详情</a>
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="update('${pkg.package_id}')">编辑</a>
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="ShowCataDialog('${pkg.package_id}')">打印面单</a>
                      </td>
                  </tr>
               </c:forEach>
             </table>
                  <div class="panel-foot text-center">
                   <jsp:include page="webfenye.jsp"></jsp:include>
                 </div>
             </div>
</div>
</body>
<script type="text/javascript">
//全选
$("#selectAll").click(function() {
    $("input[name='select']").prop("checked",true);
    });
// 反选
$("#reverse").click(function() {
    $("input[name='select']").each(function(){
        $(this).prop("checked",!this.checked);
         });
    });


//选择框选中
$("#status").val($("#status_bk").val());
//选择框选中
$("#pay_status").val($("#pay_status_bk").val());

//初始化焦点
$('#logistics_code').focus();

function search(){

    document.getElementById('myform').submit();
}

function detail(package_id){
    var url = "${backServer}/pkg/detail?package_id="+package_id;
    window.location.href = url;
}
function update(package_id){
    var url = "${backServer}/pkg/update?package_id="+package_id;
    window.location.href = url;
}

function ShowCataDialog(package_id) {
    layer.open({
        title :'打印面单',
        type: 2,
        shadeClose: true,
        shade: 0.8,
        offset: ['10px', '300px'],
        area: ['590px', '650px'],
        content:'${backServer}/pkg/print?package_id='+package_id 
    }); 
}

function batchExport(){
    var fromDate=$("#fromDate").val();
    var toDate=$("#toDate").val();
    window.location.href="${backServer}/pkg/goodsCountExportList?fromDate="+fromDate+"&toDate="+toDate;
}
//回车事件
      $(function(){
        document.onkeydown = function(e){
            var ev = document.all ? window.event : e;
            if(ev.keyCode==13) {

                search();
             }
        }
      });

</script>

</html>