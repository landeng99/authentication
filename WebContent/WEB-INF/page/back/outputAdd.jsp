<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<style type="text/css">
.label-class {
	font: bold;
}
</style>
<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>新增出库</strong>
			</div>
			<div class="tab-body">
				<br />
				<form action="${backServer}/output/saveOutput" id="form"
					method="post">
					<div class="field-group">
						<div>
							<div class="label" style="margin-left: 10px;">
								<label for="output_sn">出库编号:</label> <input type="hidden"
									name="output_sn" value="${output_sn}" /> ${output_sn}
							</div>
						</div>
						<div class="field-item">
							<div class="label" style="margin-left: 10px;">
								<label>选择出库口岸:</label>

							</div>
						</div>

						<div class="field-item">
							<div class="field" style="margin-left: 10px;">
								<table>
									<tr>
										<td style="text-align: right;"><input type="hidden" name="seaport_id_list"
											id="seaport_id_list" />全选：</td>
										<td><input type="checkbox" id="select_all_id"></td>
									</tr>
									<c:forEach var="seaport" items="${seaportList}">
										<tr>
											<td style="text-align: right;">${seaport.sname}：</td>
											<td><input name="seaport_id" type="checkbox" value="${seaport.sid}"></td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
						<div class="field-item">
							<div class="label" style="margin-left: 10px;">
								<label for="osaddr_id">选择仓库:</label> <select
									class="input" id="osaddr_id"
									name="osaddr_id"
									style="width: 200px; display: -webkit-inline-box;">
									<c:forEach var="userOverseasAddress"
										items="${overseasAddressList}">
										<option value="${userOverseasAddress.id}">${userOverseasAddress.warehouse}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="field-item">
							<div class="label" style="margin-left: 10px;">
								<label>备注信息:</label>
							</div>
							<div class="text-field" style="margin-left: 10px;">
								<textarea rows="3" cols="50" name="remark"></textarea>
							</div>
						</div>

					</div>
					<div style="clear: both"></div>
					<div class="form-button" style="margin-top: 20px;">
						<input type="button" class="button bg-main" id="finish"
							value="完成"> <a class="button bg-main"
							href="javascript:;" id="back">取消</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('#finish').click(function() {
			var seaport_id = '';
			$("input[name='seaport_id']:checked").each(
			function()
			{
				seaport_id+=this.value+",";
			}
			);
			if (seaport_id == '') {
				alert('请选择口岸');
			} else {
				$("#seaport_id_list").val(seaport_id);
				$('#form').submit();
			}
		});

		$('#back').click(function() {
			window.location.href = "${backServer}/output/queryAll";
		});
		
        $("#select_all_id").click(function () {//全选  
        	if($('#select_all_id').get(0).checked)
        	{
        		$("input[name='seaport_id']").attr("checked", true);  
        	}else
        	{
        		$("input[name='seaport_id']").attr("checked", false);  
        	}
            
        });  
	</script>
</body>
</html>