<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>
<%
String path=request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    <script src="${backJsFile}/LodopFuncs.js"></script>
    <style type="text/css">
    .field-group .field-item{
        float:left;
        margin-right: 20px;
        width: 1000px;
    }
    .field-group .field-item .label{
        float:left;
        line-height: 33px;
        margin-right: 10px;
    }
    .field-group .field-item .field{
        float:left;
    }
    .pallet-list{
        margin-top: 20px;
    }

</style>
<body>
  <div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">分箱服务详情</a>
          </li>
        </ul>
      </div>
      <div class="tab-body">
        <div class="admin_context">
          <div class="field-group" style="margin-top:10px;">
            <input type="hidden" id="package_id" value="${pkg.package_id}" />
            <input type="hidden" id="abandon_packages" value="${abandon_packages}" />
    
            <div class="field-item">
              <div style="float: left; width: 250px;"><span><strong>待分箱包裹运单号：</strong>${pkg.logistics_code}</span></div>
              <div style="float: left; width: 250px;"><span><strong>关联单号：</strong>${pkg.original_num}</span></div>
              <div style="float: left; width: 120px; margin-left: 10px;"><span>包裹状态：${pkg.statusDesc}</span></div>
            </div>
            <div class="field-item">
              <div style="float: left; width: 250px;">
                         <strong> 是否到库：</strong><c:if test="${pkg.arrive_status==0}">未到库</c:if>
                                <c:if test="${pkg.arrive_status==1}">已到库</c:if>
                                <input type="hidden" id="arrive_status"  value="${pkg.arrive_status}" name="arrive_status">
              </div>
              <div style="float: left;"><span><strong>到库时间：</strong><fmt:formatDate value="${pkg.arrive_time}" pattern="yyyy/MM/dd ，E ，HH:mm"/></span></div>
            </div>
                  <div class="field-item">
                  <a class="button border-blue button-little" href="javascript:void(0);"
                           onclick="ShowCataDialog('${package_id}')">预览打印面单</a>
                  <a class="button border-blue button-little" href="javascript:void(0);"
                           onclick="ShowCataDialog_noreview('${package_id}')">打印面单</a>                           
                    <table border="1" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="300" align="center">商品名称</td>
                          <td width="300" align="center">品牌</td>
                          <td width="100" align="center">商品申报类别</td>
                          <td width="100" align="center">价格(元)</td>
                          <td width="100" align="center">数量</td>
                          <td width="100" align="center">关税费用(元)</td>
                        </tr>
                        <c:forEach var="packageGoods" items="${pkg.goodsList}">
                          <tr>
                            <td width="300" align="center">${packageGoods.goods_name}</td>
                            <td width="300" align="center">${packageGoods.brand}</td>
                            <td width="100" align="center">${packageGoods.name_specs}</td>
                            <td width="100" align="center">${packageGoods.price}</td>
                            <td width="100" align="center">${packageGoods.quantity}</td>
                            <td width="100" align="center"><fmt:formatNumber value="${packageGoods.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                          </tr>
                        </c:forEach>
                      </table>
             </div> 
                     
            <div class="field-item">
               <div style="float: left; width: 1000px;margin-top: 10px;"></div>
            </div>
              <div style="clear: both"></div>
              <div class="panel admin-panel" id="packageList">
	         	     <div class="panel-head"><strong>分箱后包裹</strong></div>
	         	     <div class="padding border-bottom">
                  	   	<input class="checkbox" id="selectAll"  type="checkbox">
                  	   	全选 
                  	 </div> 
             	 <c:forEach var="packageGoods" items="${packageGoodsList}">
                <table border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td colspan="2">&nbsp</td>
                  </tr>
                  <tr>
                    <td width=300>分箱后子包裹运单号：${packageGoods.pkg.logistics_code}</td>
                    <td><a class="button border-blue button-little" href="javascript:void(0);"
                           onclick="ShowCataDialog('${packageGoods.pkg.package_id}')">预览打印面单</a>
                           <a class="button border-blue button-little" href="javascript:void(0);"
                           onclick="ShowCataDialog_noreview('${packageGoods.pkg.package_id}')">打印面单</a>
                    </td>
                    <td width=300>包裹状态：${packageGoods.pkg.statusDesc}</td>
                    <td>服务状态：
                      <c:forEach var="pkgAttachService"  items="${packageGoods.pkg.pkgAttachServiceList}">
                          <c:if test="${pkgAttachService.status==2}">
		                    <input type="radio" name="${packageGoods.pkg.package_id}" checked="checked" value="2">已完成  &nbsp &nbsp
		                     <input type="radio" name="${packageGoods.pkg.package_id}" value=1>未完成</td>
                          </c:if>
                           <c:if test="${pkgAttachService.status==1}">
                          		  <input type="radio" name="${packageGoods.pkg.package_id}"  value=2>已完成  &nbsp &nbsp
                          		   <input type="radio" name="${packageGoods.pkg.package_id}"  checked="checked"  value=1>未完成</td>
                           </c:if>
                      </c:forEach>
                  </tr>
     <!--              <tr><td colspan="4">
                     	 <ul>
                     	 	<li><span>子包裹相关增值服务：</span></li>
                     	 	<li><input class="checkbox" name="attach_id"  type="checkbox">拍照</li>
                     	 	<li><input class="checkbox" name="attach_id"  type="checkbox">拍照</li>
                     	 	<li><input class="checkbox" name="attach_id"  type="checkbox">拍照</li>
                     	 	<li><input class="checkbox" name="attach_id"  type="checkbox">拍照</li>
                     	 </ul>
                      </td></tr> -->
                  <tr>
                    <td colspan="4">
                      <table border="1" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="300" align="center">商品名称</td>
                          <td width="300" align="center">品牌</td>
                          <td width="100" align="center">商品申报类别</td>
                          <td width="100" align="center">价格(元)</td>
                          <td width="100" align="center">数量</td>
                          <td width="100" align="center">关税费用(元)</td>
                        </tr>
                        <c:forEach var="good" items="${packageGoods.goodList}">
                          <tr>
                            <td width="300" align="center">${good.goods_name}</td>
                            <td width="300" align="center">${good.brand}</td>
                            <td width="100" align="center">${good.name_specs}</td>
                            <td width="100" align="center">${good.price}</td>
                            <td width="100" align="center">${good.quantity}</td>
                            <td width="100" align="center"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                          </tr>
                        </c:forEach>
                      </table></td>
                  </tr>
                </table>
              </c:forEach>
            </div>
          </div>
    
          <div class="buttonList" style="clear: both; padding-top: 20px;">
            <input type="button" class="button bg-main" onclick="finish()"value="提交" />
            <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function ShowCataDialog(package_id) {
      layer.open({
        title : '打印面单',
        type : 2,
        shadeClose : true,
        shade : 0.8,
        offset: ['10px', '300px'],
        area: ['590px', '650px'],
        content : '${backServer}/pkg/print?package_id=' + package_id
      });
    }
	function ShowCataDialog_noreview(package_id) {
		LODOP=getLodop();  
		LODOP.PRINT_INIT("打印面单");
		LODOP.SET_PRINT_PAGESIZE(1,1016,1524," ");
		LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","73%");
		LODOP.ADD_PRINT_URL(0,0,'100%','100%','<%=basePath%>backprint?package_id='+package_id);
		LODOP.PRINT();//直接打印
		//LODOP.PRINTA();//选择打印机打印
		//LODOP.PREVIEW();//打印预览
	}
	
    $('#selectAll').click(function(){
    	
    	 $('#packageList table').find('input:radio').prop("checked",false); 
     
    	 if($(this).prop('checked')){
	    	  $('#packageList').find("input[type='radio'][value='2']").prop("checked",true); 
    		 
    	 }else{
    		 $('#packageList').find("input[type='radio'][value='1']").prop("checked",true); 
    	 }
     });
    function finish(package_id) {

      var arrive_status= $('#arrive_status').val();	
    	if(arrive_status==0){
    	    layer.alert("包裹未到库，不能操作该服务");
    	    return ;
    	}
      // checkbox未选择
      var isNotChecked = false;

     var $inputs=$('#packageList').find('input:checked');
     var package_ids={};
      $('#packageList table').find('input:checked').each(function() {
    	  var status=$(this).val();
    	  package_ids[$(this).attr('name')]=status;
      });

      if (package_ids.length==0) {
    	alert("请勾选已分箱的包裹");
        return;
      }
		
      var url = "${backServer}/pkg/split";
 
      //原包裹废弃
      var abandon_packages = $('#abandon_packages').val();
      $.ajax({
        url : url,
        data : {
          "package_id" : JSON.stringify(package_ids),
          "abandon_packages" : abandon_packages
        },
        type : 'post',
        dataType : 'json',
        async : false,
        success : function(data) {
	        if(data['result']==true){
	            layer.alert("操作成功",function(){
		            window.location.href = "${backServer}/breadcrumb/back";
	            });
	        }	
        }
      });
    }
  </script>
</body>
</html>