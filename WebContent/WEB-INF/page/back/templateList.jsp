<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>导出模板列表</strong></div>
      <div class="tab-body">
        <br />
            <form action="${backServer}/template/templateSearch" method="post" id="myform" name="myform">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>模板名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:180px">
                         <select id="template_name" name="template_name" class="input" style="width:150px">
                            <option value="">请选择</option>
                            <c:forEach var="templateName"  items="${templateNameList}">
                               <option value="${templateName}">${templateName}</option>
                            </c:forEach>
                         </select>
                    </div>
                 </div>
            </form>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" onclick="search()" value="查询" />
                    <shiro:hasPermission name="template:add">
                    <input type="button" class="button button-small border-green" onclick="add()" value="添加 "/>
                    </shiro:hasPermission>
                 </div>
                 <table class="table">
                    <tr><th width="10%">模板名称</th>
                        <th width="50%">描述</th>
                        <th width="15%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover">
                  <c:forEach var="exportConfig"  items="${exportConfigList}">
                     <tr><td width="10%">${exportConfig.template_name}</td>
                         <td width="50%">${exportConfig.description}</td>
                         <td width="15%">
                         <shiro:hasPermission name="template:update">
                           <a class="button border-blue button-little" href="javascript:void(0);"onclick="modify('${exportConfig.export_id}')">修改</a>
                         </shiro:hasPermission>
                           <a class="button border-blue button-little" href="javascript:void(0);"onclick="del('${exportConfig.export_id}')">删除</a>
                         </td>
                         </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
            $('#template_name').focus();
        });
        
        function modify(export_id){
            var url = "${backServer}/template/modifyTemplate?export_id="+export_id;
            window.location.href = url;
        }

        function search(){

            document.getElementById('myform').submit();
        }
        function add(){
            var url = "${backServer}/template/addTemplate";
            window.location.href = url;
        }
        
        function del(export_id){
            
            if(confirm("是否要删除该条记录？")){
                 var url = "${backServer}/template/delete";
                 $.ajax({
                  url:url,
                  data:{
                      "export_id":export_id},
                  type:'post',
                  dataType:'text',
                  success:function(data){
                     alert("删除成功");
                     window.location.href = "${backServer}/template/templateInit";
                  }
                 });
            }
          }
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search(); 
               }
          }
        });
    </script>
</body>
</html>