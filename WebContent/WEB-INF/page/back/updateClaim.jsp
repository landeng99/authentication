<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">索赔金额计算</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
        <input type="hidden" id="goodsTypeId" value="${goodsTypeId }">
		<div class="form-group">
			<form id="myform" name="myform">
                <div class="form-group">
                    <div class="label"><label for="name">税号[不可改]</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="taxCode" name="taxCode" style="width:200px" 
                    		value="${claimPojo.taxCode }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" readonly="true"/>
                   		<span style="color: red;" id="taxCodeNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="name">物品名称及规格[不可改]</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="nameSpecs" name="nameSpecs" style="width:200px" 
                    		value="${claimPojo.nameSpecs }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" readonly="true"/>
                   		<span style="color: red;" id="nameSpecsNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="name">税率</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="taxRate" name="taxRate" style="width:200px" 
                    		value="${claimPojo.taxRate }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" />
                   		<span style="color: red;" id="taxRateNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="name">计量单位</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="unit" name="unit" style="width:200px" 
                    		value="${claimPojo.unit }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" />
                   		<span style="color: red;" id="unitNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="name">完税价格</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="taxesPrice" name="taxesPrice" style="width:200px" 
                    		value="${claimPojo.taxesPrice }" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" />
                   		<span style="color: red;" id="taxesPriceNote"></span>
                    </div>
                </div>
                <div class="form-button"><a href="javascript:void(0);" class="button bg-main" onclick="save()">提交</a></div>
             </form>
        </div>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
	function save(){
		var taxRate = $('#taxRate').val();
		var unit = $('#unit').val();
		var taxesPrice = $('#taxesPrice').val();
		if(taxRate==""){
			alert("税率不能为空");
			document.myform.name.focus();
			return false;
		}
		if(unit==""){
			alert("计量单位不能为空");
			document.myform.link.focus();
			return false;
		}
		if(taxesPrice==""){
			alert("完税价格不能为空");
			document.myform.name.focus();
			return false;
		}
		var url = "${backServer}/goodsType/updateGoodsTypeSubmit";
		$.ajax({
			url:url,
			type:"POST",
			data:{"taxRate":taxRate,"unit":unit,"taxesPrice":taxesPrice,
				"goodsTypeId":$('#goodsTypeId').val(),
				"taxCode":$('#taxCode').val(),
				"nameSpecs":$('#nameSpecs').val()},
			success:function(data){
				if(data){
		            alert("修改成功");
		            window.location.href = "${backServer}/goodsType/queryAll";
				}
			}
		});
	}
	 $(function(){
			$("#taxRate").blur(function(){
					var taxRate = $("#taxRate").val();
					var len = $("#taxRate").val().length;
					 if(len==0||taxRate==null){
							$("#taxRateNote").html("税率不能为空！");
							$("#myform").attr("onclick","return false");
							}else{
							$("#myform").attr("onclick","return true");
								}
						});
				});
	 $(function(){
			$("#unit").blur(function(){
					var unit = $("#unit").val();
					var len = $("#unit").val().length;
					 if(len==0||unit==null){
							$("#unitNote").html("单位不能为空！");
							$("#myform").attr("onclick","return false");
							}else{
							$("#myform").attr("onclick","return true");
								}
						});
				});
	 $(function(){
			$("#taxesPrice").blur(function(){
					var taxesPrice = $("#taxesPrice").val();
					var len = $("#taxesPrice").val().length;
					 if(len==0||taxesPrice==null){
							$("#taxesPriceNote").html("链接不能为空！");
							$("#myform").attr("onclick","return false");
							}else{
							$("#myform").attr("onclick","return true");
							}
						});
				});
		  	
</script>
</body>