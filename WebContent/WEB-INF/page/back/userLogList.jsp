<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
</style>
<body>
	<div class="admin">
		<input type="hidden" id="messageHidden" name="messageHidden" value="${MESSAGE_INFO}"></input>
		<div class="padding border-bottom" id="findDisplayDiv" style="width: 90%; height: 100px;">
			<!-- 提供登录人用户id userId 信息 -->
			<div style="float:left;height: 32px; margin-left: 10px;">
				<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;用户名&nbsp;</label>
					<input type="text" class="input"id="user_name" name="user_name" style="width:200px;margin-left: 10px;float:left;" 
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
			
				<div style="float: left;width: 480px;">
					<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;记录时间&nbsp;</label>
					<input type="text" class="input" style="width:140px;float:left;margin-left: 10px;"
						id="start_time" name="start_time" 
						onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'end_time\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
					<span style="line-height: 30px; float:left; text-align: center; margin-left: 8px;">到</span>
					<input type="text" class="input"style="width:140px;float:left;margin-left: 8px"
						id="end_time" name="end_time" 
						onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'start_time\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
				</div>
				<div class="form-button" style="float:left;"><a href="javascript:void(0);" class="button bg-main" onclick="queryAll()">查询</a></div>
			</div>
		</div>
		<div class="panel admin-panel">
			<div class="panel-head" style="height: 50px;">
				<strong style="float:left;text-align: center;margin-left: 10px;">操作记录列表</strong>
			</div>
			<table class="table table-hover">
				<tr><th>用户名</th>
				<th>用户ip地址</th>
				<th>操作日期</th>
				<th>操作内容</th>
				<th>操作</th></tr>
			<c:forEach var="userLog" items="${userLogLists}">
				<tr><td>${userLog.user_name }</td>
				<td>${userLog.ip_values }</td>
				<td>${userLog.create_time }</td>
				<td>${userLog.contentDesc }</td>
				<td>
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="finkUserLog('${userLog.log_id }','log')">查看详情</a>&nbsp;&nbsp;
					<%-- <a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="finkUserLog('${userLog.userId }','user')">查看用户详情</a>&nbsp;&nbsp; --%>
			 	</td></tr>	
			</c:forEach>
			</table>
			<div class="panel-foot text-center">
				<jsp:include page="webfenye.jsp"></jsp:include>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	function finkUserLog(log_id){
		var url = "${backServer}/userLog/toFinkUserLog?log_id="+log_id;
		window.location.href = url;
	}
	
	function queryAll(){
		var user_name = $('#user_name').val(); 
		var start_time = $('#start_time').val();
		var end_time = $('#end_time').val();
		var url = "${backServer}/userLog/queryAll?user_name="+user_name+
				"&start_time="+start_time+
				"&end_time="+end_time;
		window.location.href = url;
	}
	
    //回车事件
    $(function(){
      document.onkeydown = function(e){
          var ev = document.all ? window.event : e;
          if(ev.keyCode==13) {

        	  queryAll(); 
           }
      }
    });
</script>
</html>