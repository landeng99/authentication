<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
 <link type="image/x-icon" href="/favicon.ico" rel="shortcut icon" />
    <link href="/favicon.ico" rel="bookmark icon" />
</head>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">用户详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<input type="hidden" id="id" value="${user_id }">
                <div class="form-group">
                    <div class="label"><label for="username">用户名</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="username" name="username" style="width:200px" value="${username }" readonly="true" />
                    </div>
                </div>
               	 用户详情：
               	 <div id="choic">
               	 	<ul>
                	<c:forEach var="role"  items="${roleLists}">
                		<li style="list-style: outside none none;float: left;width:300px;" > 
                		<input type="checkbox" disabled="disabled" class="role_${role.id }" value="${role.id }">&nbsp;&nbsp;${role.rolename }&nbsp;&nbsp;
                		</li>
                	</c:forEach>
                	</ul>
              	 </div>
              	 
              	 <div class="form-button" style="margin-left: 10px;margin-top: 150px;">
              		<a href="javascript:history.go(-1)" class="button bg-main">返回</a> 
              </div>
        </div>
        </div>
      </div>
	</div>
</div>
</body>
<script type="text/javascript">
$(function(){
	var id = $("#id").val();
	$.ajax({
		url:"${backServer}/user/queryUserRole",
		type:"POST",
		data:{"user_id":id},
		success:function(data){
			if(data){
				$.each(data,function(i,ele){
					$(".role_"+ele.role_id).attr('checked', 'checked');
				});
			}
		}
	});
});
	

</script>
</html>