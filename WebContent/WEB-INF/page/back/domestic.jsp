<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<style type="text/css">
    .span1{
        display:-moz-inline-box;
        display:inline-block;
        width:25px;
        height:25px;
    }
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-import">运单上传</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <div class="tab-panel active" id="tab-import">
           <div class="field-group">
                <form id="form" action="${backServer}/store/search">
                </form>
                   <div style="height: 70px;">
                    <div class="label"><label>导入EXCEL文件:</label></div>
                    <div class="field">
                        <input type="file"  id="excel" name="excel"  onchange="excelChange()"/>
                    </div>
                   </div>
                 <div class="form-button" style="height: 60px;">
                 <shiro:hasPermission name="pkgdomestic:input">
                      <a href="javascript:void(0);" class="button bg-main" id="import" >导入</a>
                 </shiro:hasPermission>
                  </div>
                  <div id="download"  style="color:#FF0000"></div>
                  <div id="textArea" style="display: none;margin-top:10px;">
                     <div style="height:30px;">
                        <span>错误信息说明：</span>
                     </div>
                     <div style="height:30px;">
                        <span style="background:red;" class="span1">&nbsp</span>
                        <span>系统中不存在该公司运单号</span>
                     </div>
                     <div style="height:30px;">
                        <span style="background:maroon;" class="span1">&nbsp</span>
                        <span>导入清单内该公司运单号重复</span>
                     </div>
                     <div style="height:30px;">
                        <span style="background:yellow;" class="span1">&nbsp</span>
                        <span>系统中已存在该派送单号</span>
                     </div>
                     <div style="height:30px;">
                        <span style="background:olive;" class="span1">&nbsp</span>
                        <span>导入清单内该派送单号重复</span>
                     </div>
                  </div>
          </div>
		  <table cellspacing="0" cellpadding="1" border="1" style="font-size:15px">
		  <col width=150px />
		  <col width=200px />
		  <tr>
		  <td>快递公司</td>
		  <td>快递公司编码</td>
		  </tr>
		  <tr>
		  <td>ems</td>
		  <td>ems</td>
		  </tr>
		  <tr>
		  <td>中通</td>
		  <td>zhongtong</td>
		  </tr>
		  <tr>
		  <td>申通</td>
		  <td>shentong</td>
		  </tr>
		  <tr>
		  <td>顺丰</td>
		  <td>shunfeng</td>
		  </tr>
		  <tr>
		  <td>百世快递</td>
		  <td>huitongkuaidi</td>
		  </tr>
		  <tr>
		  <td>德邦物流</td>
		  <td>debangwuliu</td>
		  </tr>
		  <tr>
		  <td>韵达</td>
		  <td>yunda</td>
		  </tr>
		  <tr>
		  <td>圆通</td>
		  <td>yuantong</td>
		  </tr>
		  <tr>
		  <td>优寄快递</td>
		  <td>youjikuaidi</td>
		  </tr>
		  <tr>
		  <td>宅急送</td>
		  <td>zhaijisong</td>
		  </tr>
		  <tr>
		  <td>中邮物流</td>
		  <td>zhongyouwuliu</td>
		  </tr>
		  </table>
		  <br />
		  <a target="_blank" href="${resource_path}/upload/ems.xlsx">点击下载导入模板</a>
        </div>
      </div> 
    </div>
</div>
<script type="text/javascript">

    function excelChange() {
        $('#download').empty();
        $('#textArea').hide();
    }
    
    $(function() {
        $('#excel').focus();
        $('#import').click(function() {
            var excel = $('#excel').val();
            if (excel == null || excel == "") {
                alert("请选择一个文件！");
                return;
            }
            var url = "${backServer}/pkg/excel";
            $.ajaxFileUpload({
                url : url,
                type : 'post',
                fileElementId : 'excel',
                dataType : 'content',
                async:false,
                success : function(data) {
                    //去掉<pre></pre>标签
                    var start = data.indexOf(">");
                    if (start != -1) {
                        var end = data.indexOf("<", start + 1);
                        if (end != -1) {
                            data = data.substring(start + 1, end);
                        }
                    }
                    //json格式
                    var result = $.parseJSON(data);
                    //错误订单号
                    if (result.flag == "3") {
                        $('#download').empty();
                        var a = "<a style='color:blue;' href='"+"${resource_path}"+result.path+"'>点击此处查看错误信息</a>";
                        $('#download').append(a);
                        $('#textArea').show();

                    }
                    alert(result.message);
                },
                error : function(data, status, e) {

                    alert(data.flag);
                }
            });
        });
    });
</script>
</body>
</html>