<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
    <div class="panel admin-panel">
    	<div class="panel-head"><strong>公告列表</strong></div>
      <div class="padding border-bottom">
      <shiro:hasPermission name="notice:add">
      		<input type="button" class="button button-small border-green" onclick="updateNotice(0)" value="公告添加" />
      </shiro:hasPermission>
      </div>
      <table class="table table-hover">
      		<tr><th>标题</th>
      		<!-- <th>内容简介</th> -->
      		<th>生效时间</th>
      		<th>失效时间</th>
      		<th>状态</th>
      		<th>操作</th></tr>
      			
      		<c:forEach var="list"  items="${noticeLists}">
      		<tr><td>${list.title}</td>
      		<%-- <td>${list.contextProfile }</td> --%>
      		<td>${list.effectTime}</td>
      		<td>${list.timeOut}</td>
      		<td><c:if test="${list.status==0}">禁用</c:if>
      			<c:if test="${list.status==1}">启用</c:if></td>
      		<td>
      		<shiro:hasPermission name="notice:select">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateNotice('${list.noticeId }')">查看详情</a>&nbsp;&nbsp;
      		</shiro:hasPermission><shiro:hasPermission name="notice:edit">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateNoticeOne('${list.noticeId }')">编辑</a>&nbsp;&nbsp;
      		</shiro:hasPermission><shiro:hasPermission name="notice:enable">
      				<c:if test="${list.status==0 }">
      						<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="updNotice('${list.noticeId }')">启用</a>&nbsp;&nbsp;
					</c:if>
					<c:if test="${list.status==1 }">
      						<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="updNotice('${list.noticeId }')">禁用</a>&nbsp;&nbsp;
					</c:if>
			</shiro:hasPermission><shiro:hasPermission name="notice:delete">
      						<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="del('${list.noticeId }')">删除</a>
			</shiro:hasPermission>
						</td></tr>	
      		</c:forEach>
      </table>
   		<div class="panel-foot text-center">
      		<jsp:include page="webfenye.jsp"></jsp:include>
      	</div>
      </div>
</div>
</body>

<script type="text/javascript">
	function updateNotice(noticeId){
		var url = "${backServer}/notice/toUpdateNotice?noticeId="+noticeId;
		window.location.href = url;
	}
	function updateNoticeOne(noticeId){
		var url = "${backServer}/notice/toUpdateNoticeOne?noticeId="+noticeId;
		window.location.href = url;
	}
	function updNotice(noticeId){
		var url = "${backServer}/notice/updNotice?noticeId="+noticeId;
		window.location.href = url;
	}
	
	function del(noticeId){
		if(confirm("确认删除吗？")){
		var url = "${backServer}/notice/deleteNotice?noticeId="+noticeId;
		$.ajax({
			url:url,
			type:'GET',
			success:function(data){
				 alert("删除成功！");
				 window.location.href = "${backServer}/notice/queryAll";
			}
		});
		}
	}	
</script>
</html>