<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
    <div class="panel admin-panel">
    	<div class="panel-head"><strong>角色列表</strong></div>
      <div class="padding border-bottom">
      <shiro:hasPermission name="role:add">
      		<input type="button" class="button button-small border-green" onclick="add()" value="添加角色" />
      </shiro:hasPermission>
      </div>
      <table class="table table-hover">
      		<tr><th>编号</th><th>角色名称</th><th>角色详情</th><th>操作</th></tr>	
      		<c:forEach var="list"  items="${roleLists}">
      		<tr>
      			<td>${list.id }</td>
      			<td>${list.rolename }</td>
      			<td>${list.desc }</td>
      			<td>
      		<shiro:hasPermission name="role:select">
      			<a class="button border-blue button-little" href="javascript:void(0);" onclick="viewxq('${list.id }','${list.rolename }','${list.desc }')">角色详情</a>&nbsp;&nbsp;
      		</shiro:hasPermission><shiro:hasPermission name="role:edit">
      			<a class="button border-blue button-little" href="javascript:void(0);" onclick="update('${list.id }','${list.rolename }','${list.desc }')">编辑</a>&nbsp;&nbsp;
      		</shiro:hasPermission><shiro:hasPermission name="role:delete">
      			<a class="button border-blue button-little" href="javascript:void(0);" onclick="delRole('${list.id }','${back_user.user_id}')">删除</a>
      		</shiro:hasPermission>	
      			</td></tr>
      		</c:forEach>
      </table>
      <div class="panel-foot text-center">
			<jsp:include page="webfenye.jsp"></jsp:include>
		</div>
</div>

</body>
<script type="text/javascript">
	function update(id,rolename,desc){
		var url = "${backServer}/role/updateRole?id="+id+"&rolename="+encodeURIComponent(encodeURIComponent(rolename))+"&desc="+encodeURIComponent(encodeURIComponent(desc));
		window.location.href = url;
	}
	function viewxq(id,rolename,desc){
		var url = "${backServer}/role/rolePage?id="+id+"&rolename="+encodeURIComponent(encodeURIComponent(rolename))+"&desc="+encodeURIComponent(encodeURIComponent(desc));
		window.location.href = url;
	}
	
	function add(){
		var url = "${backServer}/role/toAddRole";
		window.location.href = url;
	}
	
	function delRole(id,user_id){
		if(confirm("确认删除吗？")){
			var url = "${backServer}/role/deleteRole";
			$.ajax({
				url:url,
				data:{"id":id,"user_id":user_id},
				type:'post',
				success:function(data){
					if(data == "1"){
						layer.alert("删除成功!", function(index){
							window.location.href = "${backServer}/role/queryAll";
						});
					
					}else{
						layer.alert("当前用户属于该角色,不可删除!");
					}
				}
			});
			}
	}
</script>

</html>