<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
	<style type="text/css">
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 80px;
	}
	
	.field-group .field-item .interval_label{
		width: 10px;
		float:left;
		margin: auto 10px;
	}
	
	.field-group .field-item .field{
		float:left;
		width: 200px;
	}
	
	.pallet-list{
		margin-top: 20px;
	}
</style>

<body>
	<div class="admin">
	<div class="admin_context">
		    <div class="field-group">
				<form id="form" action="${backServer}/pallet/search" method="post">
				
				 <div class="field-item">
					  <div class="label"><label for="pickorder">托盘号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="pallet_code" name="pallet_code" value="${params.pallet_code}" style="width:200px"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
				 </div>
                <div class="field-item">
                	 <div class="label"><label for="pick_code">提单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="pick_code" name="pick_code" style="width:200px" value="${params.pick_code}"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>
                <div class="field-item">
                	 <div class="label"><label for="pick_code">运单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="logistics_code" name="logistics_code" value="${params.logistics_code }" style="width:200px"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>
                <div class="field-item">
                	 <div class="label"><label for="status">状态:</label></div>
                    <div class="field">
                        <select  class="input" id="status" name="status" style="width:200px" >
                        	<option value="">---------选择状态---------</option>
                    		<option value="1">已出库</option>
                    		<option value="2">空运中</option>
                    		<option value="3">待清关</option>
                    	</select>
                    </div>
                </div>
              <div class="field-item">
                	 <div class="label"><label for="status">仓库:</label></div>
                    <div class="field">
                        	<select  class="input" id="overseas_address_id" name="overseas_address_id" style="width:200px" >
                   		  <option value="">---------选择仓库---------</option>
                    		<c:forEach var="userOverseasAddress" items="${userOverseasAddressList}">
                    			<option value="${userOverseasAddress.id}">${userOverseasAddress.warehouse}</option>
                    		</c:forEach>
                   		</select>
                    </div>
                </div>
                
                
              <div class="field-item">
                	<div class="label"><label for="fromDate">创建时间</label></div>
                    <div class="field">
                  			<input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                  			onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                   	<div class="interval_label"><label for ="interval">~</label></div>
                   	<div class="field">
                   		<input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                   		onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                   	</div>
                </div>
				 <div style="clear: both"></div>
                 <div class="form-button">
                     	<a href="javascript:void(0);" class="button bg-main icon-search" id="search" >查询</a>
				  </div>
				</form>
             </div>
             <div style="clear: both"></div>
	         <div class="panel admin-panel pallet-list">
	         	     <div class="panel-head"><strong>查询列表</strong></div>
	         	     <table class="table table-hover">
	         	     	<tr>
	         	     		<th>托盘号</th>
	         	     		<th>提单号</th>
	         	     		<th>包裹数</th>
	         	     		<th>仓库</th>
	         	     		<th>状态</th>
	         	     		<th>描述信息</th>
	         	     		<th>创建时间</th>
	         	     		<th>操作</th>
	         	     	</tr>
	         	     	<c:forEach var="pallet"  items="${palletList}">
	         	     	<tr>
		         	     	<td>${pallet.pallet_code}</td>
		         	     	<td>${pallet.pick_code}</td>
		         	     	<td>${pallet.pkgCnt}</td>
		         	     	<td>${pallet.warehouse}</td>
		         	     	<td>
		         	     		<c:if test="${pallet.status==1}">已出库</c:if>
		         	     		<c:if test="${pallet.status==2}">空运中</c:if>
		         	     		<c:if test="${pallet.status==3}">待清关</c:if>
		         	     	</td>
		         	     	<td>${pallet.description}</td>
		         	     	<td><fmt:formatDate value="${pallet.create_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		         	     	<td>
		         	     	<shiro:hasPermission name="pallet:edit">
			         	     	<c:if test="${pallet.status==1}">
			         	     	<a href="${backServer}/pallet/initedit?pallet_id=${pallet.pallet_id}" class="button button-small border-green">编辑</a>
			         	     	</c:if>
			         	     </shiro:hasPermission><shiro:hasPermission name="pallet:select">
			         	     	<a href="${backServer}/pallet/detail?pallet_id=${pallet.pallet_id}" class="button button-small border-green">详情</a>
			         	     </shiro:hasPermission><shiro:hasPermission name="pallet:delete">
			         	     	<c:if test="${pallet.status==1}">
			         	     		<a class="button button-small border-yellow del" href="javascript:;">
			         	     		<input type="hidden" name="pkgCnt" value="${pallet.pkgCnt}"/>
			         	     		<input type="hidden" name="pallet_id" value="${pallet.pallet_id}"/>删除</a>
			         	     	</c:if>
			         	     </shiro:hasPermission>
		         	     	</td>
	         	     	</tr>
	         	     	</c:forEach>
	         	     </table>
		        	<div class="panel-foot text-center">
	      				<jsp:include page="pager.jsp"></jsp:include>
	      			</div>	
	         </div>	
	  	</div>
	</div>
	<script type="text/javascript">
	
    function initSearchField(){
    	$('#seaport_id').val('${params.seaport_id}');
    	$('#overseas_address_id').val('${params.overseas_address_id}');
    	$('#status').val('${params.status}');
    }
    initSearchField(); 
	
	 $('#search').click(function(){
		$('#form').submit(); 
	 })
	 
	 $('.del').click(function(){
		 var pkgCnt=$(this).find('input')[0].value;
		 var pallet_id=$(this).find('input')[1].value;
			if(pkgCnt>0){
	 			msg="包裹数量大于0，确定删除？";
	 		}
	 		else{
	 			msg="确定删除？";
	 		}
		 
		 if(window.confirm(msg)){
			 var url='${backServer}/pallet/del?pallet_id='+pallet_id;
			 window.location.href=url;
		 }
	 });
	 
     //回车事件
     $(function(){
       document.onkeydown = function(e){
           var ev = document.all ? window.event : e;
           if(ev.keyCode==13) {

               $('#form').submit(); 
            }
       }
     });
	</script>
</body>
</html>