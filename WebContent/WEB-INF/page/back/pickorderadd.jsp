<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
</style>
 
<body>
	 <div class="admin">
	<form action="${backServer}/pickorder/add" id="form" method="post">
	  <div class="field-group">
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="pick_code">提单号</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="text"  style="width:200px"  name="pick_code" value="${pick_code}" readonly="readonly"/>
		 		</div>
		 	</div>
		 	  <div class="field-item">
		 		<div class="label">
		 			<label for="seaport_id">口岸</label>
		 		</div>
		 		<div class="field">
		 			 
	                  	<select  class="input" id="seaport_id" name="seaport_id" style="width:200px" >
                    		  <option value="">---------选择口岸---------</option>
	                    		<c:forEach var="seaport" items="${seaportList}">
	                    			<option value="${seaport.sid}">${seaport.sname}</option>
	                    		</c:forEach>
                    	</select>
		 			 
		 		</div>
		 	</div>
		 	
		 	 <div class="field-item">
		 		<div class="label">
		 			<label for="seaport_id">仓库</label>
		 		</div>
		 		<div class="field">
                  	<select  class="input" id="overseas_address_id" name="overseas_address_id" style="width:200px" >
                   		  <option value="">---------选择仓库---------</option>
                    		<c:forEach var="userOverseasAddress" items="${userOverseasAddressList}">
                    			<option value="${userOverseasAddress.id}">${userOverseasAddress.warehouse}</option>
                    		</c:forEach>
                   	</select>
		 		</div>
		 	</div>
		 	
		   <div class="field-item">
		 		<div class="label">
		 			<label >描述信息</label>
		 		</div>
		 		<div class="text-field">
		 			 <textarea rows="3" cols="50" name="description"></textarea>
		 		</div>
		 	</div>
					 	
	 	</div>
	 		<div style="clear: both"></div>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<input type="button" class="button bg-main" id="nextStep" value="下一步">
	  	   		<a class="button bg-main" href="javascript:;" id="back">返回</a>
	  	   </div>
	  	   </form>
	</div>
	<script type="text/javascript">
	     $('#nextStep').click(function(){
	    	 var seaport_id=$('#seaport_id').val();
	    	 if(seaport_id==''){
	    		 alert('请选择口岸');
	    	 }else{
		    	 $('#form').submit();
	    	 }
	     });
	
	     $('#back').click(function(){
			 window.location.href="${backServer}/breadcrumb/back";
	     });
	</script>
</body>
</html>