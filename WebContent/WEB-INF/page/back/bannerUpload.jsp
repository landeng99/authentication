<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">轮播图上传</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br /><div class="tab-panel active form-x" id="tab-set">
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="myform" name="myform">
                <div class="form-group" style="clear:left; float:left; width: 80%;">
                    <div class="label" style="float: left;padding-top:8px;"><label for="name">关键字说明:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float: left; width: 80%;">
                    	<input type="text" class="input" id="keyword" name="keyword"
                    		onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
                    		placeholder="请填写关键字" data-validate="required:请填写你的关键字"/>
                   		<span style="color: red;" id="keywordNote"></span>
                    </div>
                  </div>
                
                <div class="form-group" style="clear:left; width: 80%; float:left; margin-top: 10px; margin-bottom: 10px;">
                    <div class="label" style="float: left;padding-top:8px;"><label for="name">&nbsp;图片链接&nbsp;&nbsp;:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float: left; width: 80%;">
                    	<input type="text" class="input" id="urlName" name="urlName" 
                    		onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
                    		placeholder="请填写图片链接" data-validate="required:请填写你的图片链接"/>
                   		<span style="color: red;" id="urlNameNote"></span>
                    </div>
                </div>
                
                <div class="form-group" style="clear:left; float:left; width: 80%;">
                    <div class="label" style="float: left;padding-top:8px; width: 14%;"><label for="name">&nbsp;&nbsp;状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态&nbsp;&nbsp;:&nbsp;</label></div>
                    <div class="field" style="float: left;">
                    	<select id="status" name="status" class="input" style="width:80px;float:left;margin-left: 10px;">
							<option value="1" selected="selected">启用</option>
							<option value="2">禁用</option>
						</select>
                    </div>
                </div>
                
               <div class="form-group" style="clear: both; float:left; margin-top: 20px; margin-bottom: 20px; width: 80%;">
                     <div class="label" style="float:left;margin-top:5px; width: 14%;""><label for="name">上传轮播图:</label></div>
                     <div style="float:left;margin-top:5px;margin-left:40px;width:100px">
                       <%-- <shiro:hasPermission name="pkgreturn:upload"> --%>
                       <input class=" input-file" type="file"  id="bannerPic" name="bannerPic" 
                       		onchange="bannerPicChange()"/>
                       <%-- </shiro:hasPermission> --%>
                       <span style="color: red;" id="bannerPicNote"></span>
                     </div>
                     <div style="clear: both;margin-left:40px;">
                       <img src="" id="urlArea"  name ="urlArea" width="320" height="240"/>
                     </div>
                </div>
                <div class="form-button" style="clear:left; float:left;">
                	<a href="javascript:void(0);" class="button bg-main" onclick="save()">保存</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
             </form>
        </div>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
document.getElementById('keyword').focus();

var img_suffix_ary = new Array();
img_suffix_ary[0] = ".bmp";
img_suffix_ary[1] = ".jpg";
img_suffix_ary[2] = ".jpeg";
img_suffix_ary[3] = ".png";
img_suffix_ary[4] = ".gif";
img_suffix_ary[5] = ".pcx";
img_suffix_ary[6] = ".raw";
img_suffix_ary[7] = ".tiff";
img_suffix_ary[8] = ".tga";
img_suffix_ary[9] = ".exif";
img_suffix_ary[10] = ".fpx";
img_suffix_ary[11] = ".svg";
img_suffix_ary[12] = ".psd";
img_suffix_ary[13] = ".cdr";
img_suffix_ary[14] = ".pcd";
img_suffix_ary[15] = ".dxf";
img_suffix_ary[16] = ".ufo";
img_suffix_ary[17] = ".eps";
img_suffix_ary[18] = ".hdri";
img_suffix_ary[19] = ".ai";
    //获取文件后缀
function test(file_name){
	var result =/\.[^\.]+/.exec(file_name);
	return result;
}

var imgurlVal = "";
function bannerPicChange(){
	//文件名
	var file_name = $("#bannerPic").val();
	//后缀
	var result = test(file_name);
	//后缀是否存在于图片格式中
	var isImgSuffix = false;
	
	var x;
	for (x in img_suffix_ary)
	{	
		if(result == img_suffix_ary[x]){
			//存在
			isImgSuffix = true;
			break;
		}
	}
	//
	if(isImgSuffix == false){
		 $("#bannerPicNote").html("文件["+file_name+"]不是图片格式,请选择图片上传");
		 $("#bannerPicNote").show();
		return;
	}else{
		$("#bannerPicNote").hide();
	}
	
    var url = "${backServer}/banner/uploadBanner";
      $.ajaxFileUpload({
          url : url,
          data:{"imgText":imgurlVal},
          type : 'post',
          secureuri : false,
          fileElementId : 'bannerPic',
          dataType : 'text/html',
          success : function(data) {
          	$('#urlArea').attr("src","${resource_path}/"+data);
          	imgurlVal = data;
          },
          error : function(data, status, e) {
              alert("error" + e);
          }
      });
  }
  
function LostFocus_Code(text,ereIdName,idName) {
    var code = $("#"+idName).val();
    if (code == "" || code == "请输入"+text) {
        $("#"+ereIdName).html(text+"不能为空!");
        $("#"+ereIdName).show();
        return false;
    } else {
        $("#"+ereIdName).hide();
        return true;
    }
}

function save(){

	if(imgurlVal == ""){
		$("#bannerPicNote").html("请上传图片");
		$("#bannerPicNote").show();
        return;
	}else{
		$("#bannerPicNote").hide();
	}
	
	var urlName = $("#urlName").val();
	if(urlName == "" || urlName == "请填写图片链接"){
        return;
    }
	
	var keyword = $("#keyword").val();
	if(keyword == "" || keyword == "请填写关键字"){
        return;
    }
    
	var status = $("#status").val();
	
	var url = "${backServer}/banner/addBanner?status="+status+"&keyword="+keyword+"&imageurl="+imgurlVal+"&url="+urlName;
    window.location.href = url;
  }
  
  
</script>
</body>