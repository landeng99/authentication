<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:100px;
    }
    .div-front{
        float:left;
        width:400px;
    }
    .div-front-List{
        float:left;
    }
    
     .th-title{
        height:30px;
        vertical-align:middle;
    }
    
        .td-List-left{
        height:30px;
        vertical-align:middle;
        text-align:left;
        padding-left:5px;
    }
        .td-List-right{
        height:30px;
        vertical-align:middle;
        text-align:right;
        padding-right:5px;
        
    }
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">包裹详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <div class="tab-panel active" id="tab-set">
        <div class="field-group ">
        <input type="hidden" id="package_id" name ="package_id" value="${pkgDetail.package_id}">
        <input type="hidden" id="user_id" name ="user_id" value="${pkgDetail.user_id}">
        <input type="hidden" id="status_bk" name ="status_bk" value="${pkgDetail.status}">
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户名称:</span>
                           <span>${frontUser.user_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">手机号:</span>
                           <span>${frontUser.mobile}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户账户:</span>
                           <span>${frontUser.account}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">包裹创建时间:</span>
                           <span><fmt:formatDate value="${pkgDetail.createTime}" type="both"/></span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                          <span class="span-title">关联单号:</span>
                          <span>${pkgDetail.original_num}</span>
                      </div>
                      <div style="float:left;">
                          <span class="span-title">包裹状态:</span>
                          <span>
                              <c:if test="${pkgDetail.status==-1}">异常</c:if>
                              <c:if test="${pkgDetail.status==0}">待入库</c:if>
                              <c:if test="${pkgDetail.status==1}">已入库</c:if>
                              <c:if test="${pkgDetail.status==2}">待发货</c:if>
                              <c:if test="${pkgDetail.status==3}">已出库</c:if>
                              <c:if test="${pkgDetail.status==4}">空运中</c:if>
                              <c:if test="${pkgDetail.status==5}">待清关</c:if>
                              <c:if test="${pkgDetail.status==7}">已清关派件中</c:if>
                              <c:if test="${pkgDetail.status==9}">已收货</c:if>
                              <c:if test="${pkgDetail.status==20}">废弃</c:if>
                              <c:if test="${pkgDetail.status==21}">退货</c:if>
                          </span>
                      </div>
                 </div>
				 <div style="height: 40px;">
                      <div class ="div-front">
                          <span class="span-title">包裹渠道:</span>
                          <span>
							  <c:if test="${pkgDetail.express_package==0}">默认</c:if>
                              <c:if test="${pkgDetail.express_package==1}">A渠道</c:if>
                              <c:if test="${pkgDetail.express_package==2}">B渠道</c:if>
                              <c:if test="${pkgDetail.express_package==3}">C渠道</c:if>
                              <c:if test="${pkgDetail.express_package==4}">D渠道</c:if>
                              <c:if test="${pkgDetail.express_package==5}">E渠道</c:if>
						  </span>
                      </div>
                      <div style="float:left;">
                          <span class="span-title">包裹口岸:</span>
                          <span>
                              <c:if test="${pkgDetail.seaport_id==16}">梧州</c:if>
                              <c:if test="${pkgDetail.seaport_id==17}">广州</c:if>
                              <c:if test="${pkgDetail.seaport_id==21}">香港</c:if>
                              <c:if test="${pkgDetail.seaport_id==22}">梧州-不需身份证</c:if>
                              <c:if test="${pkgDetail.seaport_id==23}">广州-不需身份证</c:if>
                              <c:if test="${pkgDetail.seaport_id==24}">贵阳</c:if>
                              <c:if test="${pkgDetail.seaport_id==25}">快件口岸</c:if>
                              <c:if test="${pkgDetail.seaport_id==26}">西安</c:if>
                          </span>
                      </div>
                 </div>
				 <div style="height: 40px;">
                      <div class ="div-front">
                          <span class="span-title">到库状态:</span>
                          <span>
							  <c:if test="${pkgDetail.arrive_status==0}">未到库</c:if>
                              <c:if test="${pkgDetail.arrive_status==1}">已到库</c:if>
						  </span>
                      </div>
                      <div style="float:left;">
                          <span class="span-title">到库时间:</span>
                          <span>${pkgDetail.arrive_time}</span>
                      </div>
                 </div>
				 <div style="height: 40px;">
                      <div class ="div-front">
                          <span class="span-title">需等待合箱:</span>
                          <span>
							  <c:if test="${pkgDetail.waitting_merge_flag==null}">否</c:if>
                              <c:if test="${pkgDetail.waitting_merge_flag=='N'}">否</c:if>
							  <c:if test="${pkgDetail.waitting_merge_flag=='Y'}">是</c:if>
						  </span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                            <span class="span-title">派送单号:</span>
                            <span>${pkgDetail.ems_code}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">收件人:</span>
                           <span>${userAddress.receiver}</span>
                      </div>
                 </div>
				 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">公司运单号:</span>
                           <span>${pkgDetail.logistics_code}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">收件人身份证:</span>
                           <span>${userAddress.idcard}</span>
                      </div>
                 </div>	
				<div style="height: 40px;">
                      <div style="float:left;">
                           <span class="span-title">收件人电话:</span>
                           <span>${userAddress.mobile}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;">
                           <span class="span-title">包裹地址:
                           </span>
						   <span>
								<input id="addrInput" type="text" class="input" value="${address}" style="width:600px"/>
						   </span>
                      </div>
                 </div>
                 <p>
                 <p>
                 <div style="height: 40px;">
                      <div style="float:left;">
                           <span class="span-title">海外仓库地址:</span>
                           <span>${osaddr}</span>
                      </div>
                 </div>
                 
                 <div>
                      <div class="div-front-List"><span class="span-title">商品:</span></div>
                      <span>
                           <table border="1" cellpadding="0" cellspacing="0" id="goodsTable">
                             <tr>
                               <th width ="300" class="th-title">商品名称</th>
                               <th width ="100" class="th-title">品牌</th>
                               <th width ="100" class="th-title">商品申报类别</th>
                               <th width ="100" class="th-title">单价</th>
                               <th width ="50" class="th-title">数量</th>
                               <th width ="100" class="th-title">规格</th>
                               <th width ="100" class="th-title">关税费用</th>
                             </tr>
                             <c:forEach var="good"  items="${pkgGoodsList}">
                                <tr>
                                  <td width ="300" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.goods_name}"/>
                                  	</div>
                                  </td>
                                  <td width ="100" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.brand}"/>
                                  	</div>
                                  </td>
                                  <td width ="100" class="td-List-left">${good.name_specs}</td>
<%--                                   <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.price}" type="currency" pattern="$#0.00#"/></td> --%>
								  <td width ="100" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.price}"/>
                                  	</div>
                                  </td>
<%--                                   <td width ="50" class="td-List-right">${good.quantity}</td> --%>
								  <td width ="100" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.quantity}"/>
                                  	</div>
                                  </td>
                                  <td width ="100" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.spec}"/>
                                  	</div>
                                  </td>
                                  <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                                  <td style="display:none;">${good.goods_id}</td>
                                </tr>
                             </c:forEach>
                           </table>
                      </span>
                 </div>

                 <div>&nbsp;</div>
                 
                 <div style="height: 40px;">
                      <div style="float:left;"><span class="span-title">包裹体积:</span></div>
                      <div style="float:left;width:100px;">
                           <span>长:</span>
                           <span>${pkgDetail.length}CM</span>
                      </div>
                      <div style="float:left;width:100px;">
                           <span>宽:</span>
                           <span>${pkgDetail.width}CM</span></div>
                      <div style="float:left;width:100px;">
                           <span>高:</span>
                           <span>${pkgDetail.height}CM</span>
                      </div>
                 </div>

                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">包裹实际重量:</span>
                           <span>${pkgDetail.actual_weight}磅</span></div>
                      <div style="float:left;">
                           <span class="span-title">运费:</span>
                           <span><fmt:formatNumber value="${pkgDetail.freight}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">收费重量:</span>
                           <span>${pkgDetail.weight}磅</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">申报总价值:</span>
                           <span><fmt:formatNumber value="${total_worth}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">税金:</span>
                           <span><fmt:formatNumber value="${pkgDetail.customs_cost}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">清关审核:</span>
                           <span>${pkgDetail.tax_status}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">转运总费用:</span>
                           <span><fmt:formatNumber value="${pkgDetail.transport_cost}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">支付方式:</span>
                           <span>
                               <c:if test="${pkgDetail.pay_type==1}">快速出库</c:if>
                               <c:if test="${pkgDetail.pay_type==2}">网上在线支付</c:if>
                           </span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">退货单号:</span>
                           <span>${pkgDetail.return_code}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">退货费用:</span>
                           <span><fmt:formatNumber value="${pkgDetail.return_cost}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">包裹支付状态:</span>
                           <span>
                              <c:if test="${pkgDetail.pay_status==1}">运费未支付</c:if>
                              <c:if test="${pkgDetail.pay_status==2}">运费已支付</c:if>
                              <c:if test="${pkgDetail.pay_status==3}">关税未支付</c:if>
                              <c:if test="${pkgDetail.pay_status==4}">关税已支付</c:if>
                              <c:if test="${pkgDetail.pay_status==5}">已支付（预扣）</c:if>
                              <c:if test="${pkgDetail.pay_status==6}">预扣失败</c:if>
                           </span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">退货支付状态:</span>
                           <span>
                              <c:if test="${pkgDetail.retpay_status==0}">未支付</c:if>
                              <c:if test="${pkgDetail.retpay_status==1}">已支付</c:if>
                           </span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">物流单号:</span>
                           <span>
                              ${pkgDetail.express_num}
                           </span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">需要取出:</span>
                           <span>
                              <select>
                              <option value="N" <c:if test="${pkgDetail.need_check_out_flag=='N'}">selected="selected"</c:if>>否</option>
                              <option value="Y" <c:if test="${pkgDetail.need_check_out_flag=='Y'}">selected="selected"</c:if>>是</option>
                              </select>
                           </span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">报关单号:</span>
                           <span>
                              ${pkgDetail.declaration_code}
                           </span>
                      </div>
                      <div style="float:left;">
                      </div>
                 </div>                  
         </div>
        <div class="form-button" style="margin-top: 20px;">
				<a class="button bg-main" href="javascript:history.back()" >返回</a>
				<a class="button bg-main" onclick="save()" id="save">保存</a>
		</div>
             </div>
             </div>
        </div>
        </div>
<script type="text/javascript">

	function combineRow(id, name, brand, spec, price, quantity) {
		return "{" + "\"goods_id\":\"" + id + "\","
				   + "\"goods_name\":\"" + name + "\","
				   + "\"brand\":\"" + brand + "\","
				   + "\"price\":\"" + price + "\","
				   + "\"quantity\":\"" + quantity + "\","
				   + "\"name_specs\":\"" + spec + "\""
			+ "}"
	}
	
	function save() {
		var url = "${backServer}/pkg/updateDetail";
		var jsonGoods = "[";
		var addr = $("#addrInput").val();
		var address_id = ${userAddress.address_id};
		var i=0;
		$("#goodsTable").find("tr").each(function(){
		    var tdArr = $(this).children();
		    var tmpName = tdArr.eq(0).find("input").val();
		    var tmpBrand = tdArr.eq(1).find("input").val();
		    var tmpPrice = tdArr.eq(3).find("input").val();
		    var tmpQuantity = tdArr.eq(4).find("input").val();
		    var tmpSpec = tdArr.eq(5).find("input").val();
		    var tmpId = tdArr.eq(7).text();
			if(i>0){// 第一次拿到的是标题，不是列表中的内容
				if( i>1 ){
					jsonGoods += ","; // 用逗号分隔多个 code
				}
				jsonGoods += combineRow(tmpId, tmpName, tmpBrand, tmpSpec, tmpPrice, tmpQuantity);
			}
		    i++;
		  });
		if( 1==i ){
			alert('没有要支付的包裹');
			return false;
		}
		jsonGoods += "]";
// 		alert(jsonGoods);
		$.ajax({
		    url:url,
		    data:{"address_id":address_id,
		    	"jsonGoods":jsonGoods,
		    	"address":addr},
		    	type:'post',
	            async:false,
		    success:function(data){
		        // 账户正常
		        if(data.result==true){
		        	alert('保存成功');
		        }
		        else{
		        	alert('保存失败');
		        }
		    }
		});
	}
</script>
</body>
</html>