<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
</style>
<body>
<div class="admin">
  <div class="panel admin-panel">
    	<div class="panel-head" style="height: 50px;">
    	<strong style="float:left;text-align: center;margin-left: 10px;">禁运物品</strong>
     	</div>
     	<div class="padding border-bottom" style="height: 50px;">
		  <shiro:hasPermission name="prohibition:upload">
				<input type="button"  
				class="button button-small border-green"
				onclick="upload()" value="上传" />
		  </shiro:hasPermission>
		</div>
      <table class="table table-hover">
      		<tr><th>物品图片</th>
      		<th>物品类型</th>
      		<th>物品主要子类</th>
      		<th>物品描述</th>
      		<th>操作</th></tr>
      			
      		<c:forEach var="list"  items="${prohibitionLists}">
      		<tr><td>
      			<img src="${resource_path}/${list.imageurl}" height="40"/>
      		</td>
      		<td>${list.class_name }</td>
      		<td>
      			<c:forEach var="chiidName"  items="${list.chiidClaseName}">
      			&nbsp;${chiidName}&nbsp;
      			</c:forEach>
      		</td>
      		<td><span title="${list.description}">${list.newDesccription}</span></td>
      		<td>
      		<shiro:hasPermission name="prohibition:update">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      				onclick="update('${list.prohibition_id }')">修改</a>&nbsp;&nbsp;
      		</shiro:hasPermission><shiro:hasPermission name="prohibition:delete">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      				onclick="del('${list.prohibition_id }')">删除</a>&nbsp;&nbsp;
      		</shiro:hasPermission>
      						</td></tr>	
      		</c:forEach>
      </table>
   		<div class="panel-foot text-center">
      		<jsp:include page="webfenye.jsp"></jsp:include>
      	</div>
      </div>
</div>
</body>
<script type="text/javascript">
function upload(){
	var url = "${backServer}/prohibition/toProhibitionUpload?prohibition_id=0";
	window.location.href = url;
}
function update(prohibition_id){
	var url = "${backServer}/prohibition/toProhibitionUpload?prohibition_id="+prohibition_id;
	window.location.href = url;
}
function del(prohibition_id){
	if(confirm("确认删除吗？")){
		var url = "${backServer}/prohibition/deleteProhibition?prohibition_id="+prohibition_id;
		$.ajax({
			url:url,
			type:'GET',
			success:function(data){
				 alert("删除成功！");
				 window.location.href = "${backServer}/prohibition/queryAll";
			}
		});
	}
}
</script>
</html>