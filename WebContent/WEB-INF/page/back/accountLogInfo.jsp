<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:100px;
    }
    .div-front{
        float:left;
        width:400px;
    }
    .div-front-List{
        float:left;
    }
    
     .th-title{
        height:30px;
        vertical-align:middle;
    }
    
        .td-List-left{
        height:30px;
        vertical-align:middle;
        text-align:left;
        padding-left:5px;
    }
        .td-List-right{
        height:30px;
        vertical-align:middle;
        text-align:right;
        padding-right:5px;
        
    }
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">账户记录详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <div class="tab-panel active" id="tab-set">
        <div class="field-group ">
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户姓名:</span>
                           <span>${frontUser.user_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">手机号:</span>
                           <span>${frontUser.mobile}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户账户:</span>
                           <span>${frontUser.account}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">账户余额:</span>
                           <span><fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">可用余额:</span>
                           <span><fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">冻结余额:</span>
                           <span><fmt:formatNumber value="${frontUser.frozen_balance}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
                 

                 <div style="height: 50px;">
                      <div style="float:left;"><span class="span-title">公司运单号:</span></div>

                      <span>
                        <table>
                          <tr>
                            <c:if test="${null !=accountLog.logistics_code and ''!=accountLog.logistics_code}">
                              <td>${accountLog.logistics_code}</td>
                            </c:if>
                            <c:if test="${null ==accountLog.logistics_code or ''==accountLog.logistics_code}">
                              <td>&nbsp;</td>
                            </c:if>
                          </tr>
                        </table>
                      </span>
					 <div style="float: left;margin-left: 400px;margin-top: -20px;">
                          <span class="span-title">支付宝姓名:</span>
                          <span>${accountLog.alipayName}</span>
                     </div>
                 </div>
                  <c:if test="${null !=accountLog.logistics_code and ''!=accountLog.logistics_code}">
                 <div>
                      <div style="float:left;"><span class="span-title">运单详情:</span></div>

                        <span>
                           <table border="1" cellpadding="0" cellspacing="0">
                             <tr>
                               <th width ="300" class="th-title">公司运单号</th>
                               <th width ="100" class="th-title">单价</th>
                               <th width ="100" class="th-title">运费</th>
                               <th width ="300" class="th-title">增值服务总费用</th>
                               <th width ="150" class="th-title">合计</th>
                               <th width ="100" class="th-title">税金</th>
                             </tr>
                             <c:forEach var="packq"  items="${packageList}">
                                <tr>
                                  <td width ="300" class="td-List-left">${packq.logistics_code}</td>
                                  <td width ="100" class="td-List-left"><fmt:formatNumber type="number" value="${packq.price}" maxFractionDigits="2"/></td>
                                  <td width ="100" class="td-List-left"><fmt:formatNumber type="number" value="${packq.freight}" maxFractionDigits="2"/></td>
                                  <td width ="300" class="td-List-left"><fmt:formatNumber type="number" value="${packq.totalServicePrice}" maxFractionDigits="2"/></td>
                                  <td width ="150" class="td-List-left"><fmt:formatNumber type="number" value="${packq.transport_cost}" maxFractionDigits="2"/></td>
                                  <td width ="100" class="td-List-left"><fmt:formatNumber type="number" value="${packq.customs_cost}" maxFractionDigits="2"/></td>
                                </tr>
                             </c:forEach>
                           </table>
                      </span>
                 </div>
                 </c:if>
                 <div>&nbsp;</div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">支付宝账号:</span>
                           <c:if test="${null ==accountLog.bank_seq_no or ''==accountLog.bank_seq_no}">
                               <span>${accountLog.alipay_no}</span>
                           </c:if>
                           <c:if test="${null !=accountLog.bank_seq_no and ''!=accountLog.bank_seq_no}">
                               <span>无</span>
                           </c:if>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">订单号:</span>
                           <span>${accountLog.order_id}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">支付宝流水号:</span>
                           <span>${accountLog.trade_no}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">银行流水号:</span>
                           <span>${accountLog.bank_seq_no}</span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">金额:</span>
                           <span><fmt:formatNumber value="${actualPay}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">类型:</span>
                           <span>
                              <c:if test="${accountLog.account_type==1}">充值</c:if>
                              <c:if test="${accountLog.account_type==2}">消费</c:if>
                              <c:if test="${accountLog.account_type==3}">提现</c:if>
                              <c:if test="${accountLog.account_type==4}">索赔</c:if>
                           </span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">状态:</span>
                           <span>
                              <c:if test="${accountLog.status==1}">申请中</c:if>
                              <c:if test="${accountLog.status==2}">成功</c:if>
                              <c:if test="${accountLog.status==3}">失败</c:if>
                              <c:if test="${accountLog.status==4}">取消</c:if>
                           </span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">交易时间:</span>
                           <span><fmt:formatDate value="${accountLog.opr_time}" type="both"/></span>
                      </div>
                 </div>

                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">描述:</span>
                           <span>${accountLog.description}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">实际支付金额:</span>
                           <span><fmt:formatNumber value="${accountLog.amount}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>

                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">优惠券编号:</span>
                           <span>${couponUsed.coupon_code}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">优惠券名称:</span>
                           <span>${couponUsed.coupon_name}</span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">优惠券面值:</span>
                           <span><fmt:formatNumber value="${couponUsed.denomination}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">返利流水号:</span>
                           <span>${accountLog.rebatesms_serial_no}</span>
                      </div>
                 </div>                 
         </div>
        <div class="form-button" >
                    <a href="javascript:history.go(-1)" class="button bg-main">返回</a> 
        </div>
             </div>
             </div>
        </div>
        </div>
<script type="text/javascript">
</script>
</body>
</html>