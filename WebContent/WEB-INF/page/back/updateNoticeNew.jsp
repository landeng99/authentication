<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
	<div class="tab">
	<div class="form-group">
	
<form id="myform" name="myform" method="post" action="${backServer}/notice/saveNotice">
	<input type="hidden" value="${2}" name=""/>
	<div style="height: 60px;">
		<div style="float:left;">
			<span for="userName" style=" line-height: 20px;"><strong>公告标题:</strong></span></div>
		<div style="float:left;margin-left:10px;width:300px;">
			<span>${noticePojo.title}</span></div>
		
		<div style="float:left;">
			<span for="createTime" style=" line-height: 20px;"><strong>公告状态:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<span>${noticePojo.status==0?'禁用':'启用'}</span></div>
	</div>
	
	<div style="height: 60px;">
		<div style="float:left;">
			<span for="userName" style=" line-height: 20px;"><strong>生效时间:</strong></span></div>
		<div style="float:left;margin-left:10px;width:300px;">
			<span>${noticePojo.effectTime}</span></div>
			
		<div style="float:left;">
			<span for="createTime" style=" line-height: 20px;"><strong>失效时间:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<span>${noticePojo.timeOut}</span></div>
	</div>
	
	<div>
		<div style="float:left;">
			<span for="tt_profile" style=" line-height: 20px;"><strong>公告简介:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<span>${noticePojo.tt_profile}</span>
		</div>
	</div>
	
	<div>
		<div style="float:left;">
			<span for="address" style=" line-height: 20px;"><strong>公告内容:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<span>${noticePojo.context}</span>
		</div>
	</div>
	
	<div style="height: 40px; float: left; clear: both; margin-top:10px; margin-left: 40px;">
		<div style="float:left;">
			<a class="button bg-main" href="javascript:history.go(-1);">返回</a>
		</div>
	</div>
</form> 
		 
	</div>
	</div>
</div>
</body>
<script type="text/javascript">
	
</script>
</html>