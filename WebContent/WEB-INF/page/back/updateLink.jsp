<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">添加链接</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="myform" name="myform" action="${backServer}/friendlylink/updateLink" method="post">
				<input type="hidden" id="id" name="id" value="${friendlyLinkPojo.id }">
                <div class="form-group" style="clear: both; height: 50px;">
                    <div class="label" style="float:left;"><label for="name">&nbsp;&nbsp;&nbsp;&nbsp;标&nbsp;&nbsp;题&nbsp;&nbsp;:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float:left;">
                    	<input type="text" class="input"
                    		value="${friendlyLinkPojo.name }" 
                    		id="name" name="name" 
                    		style="width:200px" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" />
                   		<span style="color: red;" id="nameNote"></span>
                    </div>
                </div>
                <div class="form-group" style="clear: both; height: 50px;">
                    <div class="label" style="float:left;"><label for="link">&nbsp;&nbsp;&nbsp;&nbsp;链&nbsp;&nbsp;接&nbsp;&nbsp;:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float:left;">
                    	<input type="text" class="input"
                    		value="${friendlyLinkPojo.link }" 
                    		id="link" name="link" 
                    		style="width:400px" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')" />
                    	<span style="color: red;" id="linkNote"></span>
                    </div>
                </div>
                <div class="form-group" style="clear: both; height: 50px;">
                	<div class="label" style="float:left;">
			 			<label for="parent_id">&nbsp;&nbsp;&nbsp;&nbsp;状&nbsp;&nbsp;态&nbsp;&nbsp;:&nbsp;&nbsp;</label>
			 		</div>
			 		<div class="field" style="float:left;">
			 			<select  class="input" name="status" style="width: 200px;">
			 			<c:if test="${friendlyLinkPojo.status==1}">
			 				<option value="1" selected="selected">启用</option>
			 				<option value="2">禁用</option>
			 			</c:if><c:if test="${friendlyLinkPojo.status!=1}">
			 				<option value="1">启用</option>
			 				<option value="2" selected="selected">禁用</option>
			 			</c:if>
			 			</select>
		 			</div>
                </div>
                <div class="form-group" style="clear: both; height: 50px;">
                	<div class="label" style="float:left;">
			 			<label for="opentype">打开方式&nbsp;:&nbsp;&nbsp;</label>
			 		</div>
			 		<div class="field" style="float:left;">
			 			<select  class="input" name="opentype" style="width: 200px;">			 			
			 				<option value="_self" 
			 					<c:if test="${friendlyLinkPojo.opentype == '_self'}"> 
			 					 	selected="selected"
			 					</c:if>
			 				>启用</option>
                    		<option value="_blank"
                    			<c:if test="${friendlyLinkPojo.opentype == '_blank'}"> 
			 					 	selected="selected"
			 					</c:if>
                    		>新窗口打开</option>
			 			</select>
		 			</div>
                </div>
                <div class="form-button" style="clear: both; height: 50px;">
                	<a href="javascript:void(0);" class="button bg-main" onclick="save()">提交</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
             </form>
        </div>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
	function save(){
		var name = $('#name').val();
		var link = $('#link').val();
		if(name==""){
			alert("标题不能为空");
			document.myform.name.focus();
			return false;
		}
		if(link==""){
			alert("链接不能为空");
			document.myform.link.focus();
			return false;
		}
		
		myform.submit();
	}
	 $(function(){
			$("#name").blur(function(){
					var uname = $("#name").val();
					var len = $("#name").val().length;
					 if(len==0||uname==null){
							$("#nameNote").html("标题不能为空！");
							$("#myform").attr("onclick","return false");
							}else{
							$("#myform").attr("onclick","return true");
								}
						});
				});
	 $(function(){
			$("#link").blur(function(){
					var link = $("#link").val();
					var len = $("#link").val().length;
					 if(len==0||link==null){
							$("#linkNote").html("链接不能为空！");
							$("#myform").attr("onclick","return false");
							}else{
							$("#myform").attr("onclick","return true");
							}
						});
				});
		  	
</script>
</body>