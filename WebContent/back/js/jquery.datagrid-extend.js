
(function($){
		
		 var buildGrid=function(target,opts){
			 target.options= $.extend({},opts);
			 var  $target=$(target);
			 methods.init($target,target.options);
			 return $target;
		 };
		 var  methods=(function(){
			 
			 var dataTemp={};
			 
			 //被选中的行
			 var selectedRow=undefined;
			 var selectedIndex=undefined;
			 var seletedColor="#D1EEEE";
			 var header={};
			 var cols=[];
			 var $self;
			 
			 var createRows=function(datalist){
					$.each(datalist,function(index,data){
						 $tr=$('<tr></tr>');
						 for(var i=0;i<cols.length;i++){
							 var id=cols[i].id;
							 var formatterName=$(cols[i]).attr('formatter');
							 var cellIndex=i;
							 header[id]={'cellIndex':cellIndex,'formatter':formatterName};
							 text=(data[id]==undefined)?'':data[id];
							 if(formatterName){
								var formatter= window[formatterName];
								var val=formatter(text,data)||"";
								var $td=$('<td></td>');
								$td.append(val);
								$tr.append($td);
							 }else{
								 var $td=$('<td></td>');
								 $td.append(text);
								 $tr.append($td);
							 }
						
						 }
						 $self.find('tbody').append($tr);
						  // render(target);渲染行颜色，因为冲突此处暂时去掉
					 
				   });
				 
			 };
			 
			 var createRowsNew=function(datalist){
					$.each(datalist,function(index,data){
						 $tr=$('<tr></tr>');
						 for(var i=0;i<cols.length;i++){
							 var id=cols[i].id;
							 var formatterName=$(cols[i]).attr('formatter');
							 var cellIndex=i;
							 header[id]={'cellIndex':cellIndex,'formatter':formatterName};
							 text=(data[id]==undefined)?'':data[id];
							 if(formatterName){
								var formatter= window[formatterName];
								var val=formatter(text,data)||"";
								var $td=$('<td></td>');
								$td.append(val);
								$tr.append($td);
							 }else{
								 var $td=$('<td></td>');
								 $td.append(text);
								 $tr.append($td);
							 }
						
						 }
						 //$self.find('tbody').append($tr);
						 if($self.find('tbody tr:eq(0)').length>0)
						 {
							 $self.find('tbody tr:eq(0)').before($tr);
						 }else
						{
							 $self.find('tbody').append($tr);
						}
						 
						  // render(target);渲染行颜色，因为冲突此处暂时去掉
					 
				   });
				 
			 };
			 
			 var render=function(){
				 $self.find('tbody tr').each(function(i,tr){
					if(selectedIndex!=i){
						 var color=((i%2)==0)?'#F7F8FA':'#FBFBFD';
						 	$(tr).css('background-color',color);
					}
				 });
				 
			 };
			 return {
					init:function($target,options){
						dataTemp = options;
						var text='',$tr;
						$self=$target;
						$self.find('tbody').remove();
						$self.append('<tbody></tbody>');
						cols=$self.find("thead th");
						createRows(dataTemp.data);
					   
		/*				$self.find('tbody tr').hover(
						 function(){
							  $(this).css('background-color',seletedColor);
						  },
						  function(){
							  render($self);
						  }   
					   );*/
					  
						$self.find('tbody tr').unbind('click').bind('click',function(){
						   var self=this;
						   selectedIndex=$(self).parent().find('tr').index(self);
						   $(this).css('background-color',seletedColor);
						   
						  // render(target);渲染行颜色，因为冲突此处暂时去掉
						   var rowData=options.data[selectedIndex];
						   selectedRow=rowData;
						   if(options.onClickRow){
							  options.onClickRow(selectedIndex,rowData);
						   }
					   });
					   
					},	 
					add:function(data){
						dataTemp.data.push(data);
						var temp=[];
						temp.push(data);
						createRows(temp);
					 },
					addFirst:function(data){
							dataTemp.data.push(data);
							var temp=[];
							temp.push(data);
							createRowsNew(temp);
					},
					 remove:function(index){
						 	var row=$self.find('tbody tr').eq(index);
						 	//删除数组中元素
							dataTemp.data.splice(index,1);
							$(row).remove();
					},
				 
					getOptions:function(){
						return dataTemp;
					},
					getAllData:function(){
						return dataTemp.data;
					},
					getData:function(index){
						if(index>=0&&dataTemp.data.length>index){
							return dataTemp.data[index];
						}else{
							return [];
						}
					},
					getSelected:function(){
						return selectedRow;
					},
					getSelectedIndex:function(){
						return selectedIndex;
					},
					update:function(params){
						 
						var index=params.index;
						var dataRow=dataTemp.data[index];
						var fields=params.fields;
						var tr=$self.find('tbody tr').eq(index);
						
						//列索引
						var cellIndex=0;
						for(var key in fields){
							cellIndex=header[key].cellIndex;
							var formatterName=header[key].formatter;
							dataRow[key]=fields[key];
							var td=tr.find('td').eq(cellIndex);
							if(formatterName){
								var formatter = window[formatterName];
								var val=formatter(fields[key],dataRow)||"";
								td.empty().append(val);
							}else{
								td.empty().append(fields[key]);
							}
						}
						 
					}	
					 
				 };
		 })();
		 $.fn.datagrid=function(options,param){
			 if (typeof options == 'string'){
					var method = methods[options];
					return method(param);
				}else{
					 options = options || {};
					 return buildGrid(this,options);
					 
				}
		 };
	 
	
})(jQuery);
