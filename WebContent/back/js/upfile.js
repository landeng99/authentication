var UpfileObject = (function(){
	var setter = {
	
	};
	var change = function(element, callback){			
		element.click();
		if(document.all){
			element.focus();
			element.onblur = callback;
			window.focus();
		}
		else{
			element.onchange = callback;
		};
	};
	
	var click = function(index){
		 
		var contentWindow = document.getElementById('uploadIframe').contentWindow,
		contentDocument = contentWindow.document,
		file = contentDocument.getElementById('fileData');
		contentDocument.getElementById('eventTarget').value = index;
		change(file , function(){
			if(file.value){
				var allowSubmit=true;
				if(typeof(setter.sendBefore) === 'function'){
					allowSubmit= setter.sendBefore(file.value);
				}
				if(allowSubmit){
					contentDocument.getElementById('submit').click();
				}
			}
		});
	};
	
	var complete = function(response, index){
		if(typeof(setter.complete) === 'function'){
			setter.complete(response, index);
		}
	};
	
	var initIframe = function(baseURL){
		var container = document.createElement('div');
		container.style.cssText='width:1px;height:1px;overflow:hidden;position:absolute;left:-9999px;top:0px;';
		container.innerHTML = '<iframe id="uploadIframe" src="'+baseURL+'" frameborder="0" width="1" height="1"></iframe>';
		document.body.appendChild(container);
	};
	
	var initialize = function( options ){
		if( ! options.elements ){
			return;
		};
		var size = options.elements.length;
		setter = options;
		while(size--){
			(function(index , element){
				element.onclick = function(){
					click(index);
				};
			})(size, options.elements[ size ]);
		};
		initIframe(options.baseURL);
	};
	
	return {
		initialize : initialize,
		complete   : complete
	};
	
})();
