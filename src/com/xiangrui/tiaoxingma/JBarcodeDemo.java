package com.xiangrui.tiaoxingma;

import java.awt.FontFormatException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jbarcode.JBarcode;
import org.jbarcode.encode.Code128Encoder;
import org.jbarcode.encode.InvalidAtributeException;
import org.jbarcode.paint.EAN8TextPainter;
import org.jbarcode.paint.WidthCodedPainter;

 

public class JBarcodeDemo
{

    public static void main(String[] args) throws FontFormatException,
            IOException, InvalidAtributeException
    {
 
        FileInputStream fi = new FileInputStream("d://excel//test.xls");
        HSSFWorkbook workbook = new HSSFWorkbook(fi);
        HSSFSheet sheet = workbook.getSheet("运单打印");

        // hssfSheet.getRow(0).getCell(2).setCellValue("BE293510260AU");
       
        JBarcode localJBarcode = new JBarcode(Code128Encoder.getInstance(),WidthCodedPainter.getInstance(),EAN8TextPainter.getInstance());  
        localJBarcode.setShowText(false);
        localJBarcode.setShowCheckDigit(false);
        localJBarcode.setBarHeight(11);
        String str = "BE293510260AU";
      
        try
        {
           
            BufferedImage localBufferedImage = localJBarcode.createBarcode(str); 
            
            HSSFPatriarch patri = sheet.createDrawingPatriarch();

            // XSSFClientAnchor(int dx1, int dy1, int dx2, int dy2, int col1,
            // int row1, int col2, int row2)
            /*
             * dx1 - the x coordinate within the first cell. 
             * dy1 - the y coordinate within the first cell. 
             * dx2 - the x coordinate within the second cell. 
             * dy2 - the y coordinate within the second cell.
             * col1 - the column (0 based) of the first cell.结束列
             * row1 - the row (0 based) of the first cell.距离top高度
             * col2 - the column (0 based) of the second cell.从第几列开始
             * row2 - the row (0 based) of the second cell.占据的行高
             */
/*            HSSFClientAnchor anchor = new HSSFClientAnchor(5, 5, 5, 5,
                    (short) 6, 0, (short) 2, 1);*/
            
            //偏移左边390，偏移top50
            HSSFClientAnchor anchor = new HSSFClientAnchor(390, 50,700, 0,
                    (short) 2, 0, (short) 6, 1);
            
            anchor.setAnchorType(2);
            
            String name = System.currentTimeMillis()+"";
            ByteArrayOutputStream outs=new ByteArrayOutputStream(); 
            
            ImageIO.write(localBufferedImage, "JPEG", outs);

            byte[] bt=outs.toByteArray();
             
            //
            HSSFPicture pic= patri.createPicture(anchor, workbook.addPicture(bt, HSSFWorkbook.PICTURE_TYPE_JPEG));
            
            // 图片缩放0.8倍
            pic.resize();

            String excelName=name+ ".xls";

            workbook.write(new FileOutputStream("d://excel//" + excelName));

        } catch (Exception localException)
        {
            localException.printStackTrace();
        }

    }

    public static int getAnchorX(int px, int colWidth)
    {
        return (int) Math.round(((double) 701 * 16000.0 / 301)
                * ((double) 1 / colWidth) * px);
    }

    public static int getAnchorY(int px, int rowHeight)
    {
        return (int) Math.round(((double) 144 * 8000 / 301)
                * ((double) 1 / rowHeight) * px);
    }

    public static int getRowHeight(int px)
    {
        return (int) Math.round(((double) 4480 / 300) * px);
    }

    public static int getColWidth(int px)
    {
        return (int) Math.round(((double) 10971 / 300) * px);
    }
 

}
