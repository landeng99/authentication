package com.xiangrui.lmp.pojo;

import java.io.Serializable;

/**
 * 分页通用Bean
 * @author administrator
 */
@SuppressWarnings("serial")
public class Pagination implements Serializable{

	/**
	 * 上一页
	 */
	public static final String PREVIOUS = "previous";

	/**
	 * 下一页
	 */
	public static final String NEXT = "next";

	/**
	 * 首页
	 */
	public static final String FIRST = "first";

	/**
	 * 尾页
	 */
	public static final String LAST = "last";

	/**
	 * 最大记录数
	 */
	private int maxinum; 

	/**
	 * 每页多少条记录
	 */
	private int perRecord = 15;

	/**
	 * 一共多少页
	 */
	private int pageCount;

	/**
	 * 当前第几页
	 */
	private int pageCurrent = 1;

	/**
	 * 操作类型
	 */
	private String operate;
	
	/**
	 * 页.
	 */
	private Integer page;
	/**
	 * 行数.
	 */
	private Integer rows = 15;
	/**
	 * 起始数.
	 */
	private Integer begNum;
	/**
	 * 结束数.
	 */
	private Integer endNum;
	
	/**
	 * 分页查询请求地址
	 */
	private String targetUrl;
	
	
	public void setPaginationData() {
		this.begNum = (page - 1) * rows;
		this.endNum = rows;
	}
	public int getBegNum() {
		return begNum;
	}
	public void setBegNum(int begNum) {
		this.begNum = begNum;
	}
	public int getEndNum() {
		return endNum;
	}
	public void setEndNum(int endNum) {
		this.endNum = endNum;
	}

	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getRows() {
		return rows;
	}
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	public void setBegNum(Integer begNum) {
		this.begNum = begNum;
	}
	public void setEndNum(Integer endNum) {
		this.endNum = endNum;
	}
	/**
	 * 计算一共多少页并为当前多少页赋值
	 * @param maxinum	最大记录数
	 */
	public void countPageCurrent(Integer maxinum) {
		this.maxinum = maxinum;
		// 计算一共多少页
		pageCount = maxinum % perRecord == 0 ? maxinum / perRecord : maxinum / perRecord + 1;
		// 为当前页数赋值
		if (PREVIOUS.equals(operate)) {
			pageCurrent--;
		} else if (NEXT.equals(operate)) {
			pageCurrent++;
		} else if (FIRST.equals(operate)) {
			pageCurrent = 1;
		} else if (LAST.equals(operate)) {
			pageCurrent = pageCount;
		}
		if (pageCurrent < 1)
			pageCurrent = 1;
		if (pageCurrent > this.pageCount)
			pageCurrent = this.pageCount;
	}

	/**
	 * 获得操作类型
	 * @return 操作类型符号
	 */
	public String getOperate() {
		return operate;
	}

	/**
	 * 设置操作类型
	 * @param operate	操作类型符号
	 */
	public void setOperate(String operate) {
		this.operate = operate;
	}

	/**
	 * 获得最大记录数
	 * @return
	 */
	public int getMaxinum() {
		return maxinum;
	}
	
	/**
	 * 获得当前页码
	 * @return
	 */
	public int getPageCurrent() {
		return pageCurrent;
	}

	/**
	 * 获得一共多少页
	 * @return 总页数
	 */
	public int getPageCount() {
		return pageCount;
	}

	/**
	 * 获取每页记录条数
	 * @return  每页记录条数
	 */
	public int getPerRecord() {
		return perRecord;
	}

	/**
	 * 设置最大记录数
	 * @param maxinum	最大记录数
	 */
	public void setMaxinum(int maxinum) {
		this.maxinum = maxinum;
	}

	/**
	 * 设置当前第几页
	 * @param pageCurrent	当前第几页
	 */
	public void setPageCurrent(int pageCurrent) {
		this.pageCurrent = pageCurrent;
	}

	/**
	 * 设置一共多少页
	 * @param pageCount	页数
	 */
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	/**
	 * 设置每页多少条记录
	 * @param perRecord	每页多少条记录
	 */
	public void setPerRecord(int perRecord) {
		this.perRecord = perRecord;
	}
	public String getTargetUrl() {
		return targetUrl;
	}
	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}
	
}
