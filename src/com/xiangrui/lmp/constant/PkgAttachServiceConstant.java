package com.xiangrui.lmp.constant;

/**
 * @author Administrator
 *增值服务状态。（1：未完成，2：已完成）
 */
public interface PkgAttachServiceConstant
{
    /**
     * 增值服务状态
     */
    int SERVICE_UNFINSH=1;
    
    int SERVICE_FINSH=2;
    
}
