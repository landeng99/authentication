package com.xiangrui.lmp.constant;

import java.util.ResourceBundle;

import com.xiangrui.lmp.util.StringUtil;

/**
 * 系统常量
 * 
 * @author Administrator
 * 
 */
public interface SYSConstant
{

	/**
	 * 资源配置信息.
	 */
	ResourceBundle config = ResourceBundle.getBundle("com/res/config/sys-constants");

	// 短信网关用户id
	String SMS_USERID = config.getString("sms.userid");

	// 短信网关账号
	String SMS_ACCOUNT = config.getString("sms.account");

	// 短信网关密码
	String SMS_PASSWORD = config.getString("sms.password");

	/**
	 * 短信验证码网关
	 */
	String SMS_URL = config.getString("sms.url");

	// 邮箱服务器
	String EMAIL_HOST = config.getString("email.host");

	// 邮箱账号
	String EMAIL_ACCOUNT = config.getString("email.account");
	// 发件人
	String EMAIL_SENDER = StringUtil.getFromResourceBundle(config, "email.sender") == null ? EMAIL_ACCOUNT : StringUtil.getFromResourceBundle(config, "email.sender");

	// 邮件密码
	String EMAIL_PASSWORD = config.getString("email.password");
	
	// 阿里身份证验证 appcode
    String IDCARD_APPCODE = config.getString("idcard.appcode");

	/**
	 * 当前会话用户
	 */
	String SESSION_USER = "sessionUser";

	/**
	 * 用户操作日志保留多少天数据
	 */
	int USER_LOG_DEL_DAYS = Integer.parseInt(config.getString("userLogDel.days"));

	/**
	 * 图片上传的数据库路基
	 */
	String UPLOAD_IMG_PATH_DB = config.getString("uploadimgpath.db");

	/**
	 * mysqldump命令
	 */
	String MYSQLDUMP_CMD = config.getString("mysqldump.cmd");

	/**
	 * 快递100 订阅开关
	 */
	boolean KUAIDI100_OPEN = Boolean.parseBoolean(config.getString("kuaidi100.open"));

	/**
	 * 向快递100发送请求地址
	 */
	public static final String POST_URL = config.getString("post_url");

	/**
	 * 快递100回传地址
	 */
	public static final String CALLBACK_URL = config.getString("callback_url");

	/**
	 * 快递100授权码
	 */
	public static final String KUAIDI100_KEY = config.getString("kuaidi100_key");

	/**
	 * 宝支付支付服务器异步通知页面路径
	 */
	public static final String NOTIFY_URL_PAY = config.getString("notify.url.pay");

	/**
	 * 宝支付支付页面跳转同步通知页面路径
	 */
	public static final String RETURN_URL_PAY = config.getString("return.url.pay");

	/**
	 * 支付付充值宝服务器异步通知页面路径
	 */
	public static final String NOTIFY_URL_RECHARGE = config.getString("notify.url.recharge");

	/**
	 * 支付付充值宝页面跳转同步通知页面路径
	 */
	public static final String RETURN_URL_RECHARGE = config.getString("return.url.recharge");

	/**
	 * 网银支付服务器异步通知页面路径
	 */
	public static final String NOTIFY_URL_PAY_BANK = config.getString("notify.url.pay.bank");

	/**
	 * 网银支付页面跳转同步通知页面路径
	 */
	public static final String RETURN_URL_PAY_BANK = config.getString("return.url.pay.bank");

	/**
	 * 网银充值服务器异步通知页面路径
	 */
	public static final String NOTIFY_URL_RECHARGE_BANK = config.getString("notify.url.recharge.bank");

	/**
	 * 网银充值页面跳转同步通知页面路径
	 */
	public static final String RETURN_URL_RECHARGE_BANK = config.getString("return.url.recharge.bank");

	/**
	 * 宝支付支付服务器异步通知页面路径
	 */
	public static final String NOTIFY_URL_PAY_TAX = config.getString("notify.url.pay.tax");

	/**
	 * 宝支付支付页面跳转同步通知页面路径
	 */
	public static final String RETURN_URL_PAY_TAX = config.getString("return.url.pay.tax");

	/**
	 * 网银支付服务器异步通知页面路径
	 */
	public static final String NOTIFY_URL_PAY_BANK_TAX = config.getString("notify.url.pay.bank.tax");

	/**
	 * 网银支付页面跳转同步通知页面路径
	 */
	public static final String RETURN_URL_PAY_BANK_TAX = config.getString("return.url.pay.bank.tax");
}