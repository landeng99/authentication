package com.xiangrui.lmp.constant;

/**
 * 包裹物流状态，-1：异常，0：待入库，1：已入库，2：待发货，3：已出库，4：空运中，5：待清关，6：清关中，7：已清关，8：派件中，9：已收货；
 * 20：废弃（分箱、合箱之后原包裹被废弃），21：退货
 * 
 * 
 * 包裹支付状态：1，未支付，2：已支付
 * 
 * 退货支付状态：1：未支付、2:已支付
 * 
 * 订阅状态: 1未订阅、2、已订阅，3取消订阅
 * 
 * @author Administrator
 * 
 */
public interface PkgConstant
{

    /**
     * 物流状态：0待入库
     */
    int LOGISTICS_STORE_WAITING = 0;

    /**
     * 物流状态:-1包裹异常，待入库包裹 在仓库中查询不到对应的数据
     */
    int LOGISTICS_UNUSUALLY = -1;

    /**
     * 物流状态：已入库
     */
    int LOGISTICS_STORAGED = 1;

    /**
     * 物流状态：待发货
     */
    int LOGISTICS_SEND_WAITING = 2;

    /**
     * 物流状态：已出库
     */
    int LOGISTICS_SENT_ALREADY = 3;

    /**
     * 物流状态：已空运
     */
    int LOGISTICS_AIRLIFT_ALREADY = 4;

    /**
     * 物流状态：待清关
     */
    int LOGISTICS_CUSTOMS_WAITING = 5;

    /**
     * 物流状态：清关中
     */
    int LOGISTICS_CUSTOMS_UNDERWAY = 6;

    /**
     * 物流状态：已清关
     */
    int LOGISTICS_CUSTOMS_ALREADY = 7;

    /**
     * 物流状态：派件中
     */
    int LOGISTICS_DELIVERED_ALREADY = 8;

    /**
     * 物流状态：已签收
     */
    int LOGISTICS_SIGN_IN = 9;

    /**
     * 物流状态：废弃
     */
    int LOGISTICS_DISCARD = 20;

    /**
     * 物流状态：退货
     */
    int LOGISTICS_RETURN = 21;

    /**
     * 待支付
     */
    int PAYMENT_UNPAID = 1;

    /**
     * 已支付
     */
    int PAYMENT_PAID = 2;

    /**
     * 退货：待支付
     */
    int RET_PAYMENT_UNPAID = 1;

    /**
     * 退货：已支付
     */
    int RET_PAYMENT_PAID = 2;

    /**
     * 订阅未提交
     */
    int SUBSCRIBE_UNSUBMIT = 1;

    /**
     * 订阅状态：已订阅
     */
    int SUBSCRIBE_SUBMIT = 2;

    /**
     * 订阅：取消
     */
    int SUBSCRIBE_CANCEL = 3;
}
