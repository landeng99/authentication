package com.xiangrui.lmp.constant;


import java.util.ResourceBundle;

/**
 * 
 * class Name [WebConstants]. description [Web常量类1]
 * 
 * @author  林太平
 * @version 1.0
 * @since JDK 1.7
 */
/**
 * @author Administrator
 *
 */
public class WebConstants {

    public WebConstants() {
        super();
    }

    /**
     * 资源配置信息.
     */
    public static ResourceBundle config = ResourceBundle
            .getBundle("com/res/config/web-constants_loc");

    /**
     * 项目启动检查是否liunx系统 flase 不是liunx 系统 true 是liunx系统.
     */
    public static final boolean SYSTEM_ISLIUNX = false;

    /**
     * 文件上传路径.
     */
    public static final String UPLOAD_FILE_PATH = "";

    /**
     * 错误文件存放路径.
     */
    public static final String ERROR_LOG_PATH = "";

    /**
     * 生成数据库导入SQL文件地址.
     */
    public static final String IMPORT_SQL_PATH = "";

    /**
     * 用户登录key.
     */
    public static final String LOGINUSER = "loginUser";

    /**
     * 主server名，即首页.
     */
    public static final String MAIN_SERVER = config.getString("com.lmp.main.server");

    /**
     * CSS文件server名.
     */
    public static final String CSS_FILE_SERVER = config
            .getString("com.lmp.css.file.server");

    /**
     * JS文件server名.
     */
    public static final String JS_FILE_SERVER = config
            .getString("com.lmp.js.file.server");

    /**
     * 图片文件server名.
     */
    public static final String IMG_FILE_SERVER = config
            .getString("com.lmp.img.file.server");

    /**
     * 服务器图片存放地址.
     */
    public static final String IMG_STORAGE_SERVER = config
            .getString("server.images.path");
    
    
    
    /**
     * 服务器图片存放地址.
     */
    public static final String UPLOAD_PATH = config
            .getString("com.lmp.upload");
    
    /**
     * 前端页面资源路径
     */
    public static final String RESOURCE_PATH = config
            .getString("com.lmp.resource_path");
    
    
    
    
    /**
     * 后台请求地址
     */
    public static final String BACK_SERVER = config
            .getString("com.lmp.back.server");
    
    
    /**
     * 后台图片请求地址
     */
    public static final String BACK_IMG_FILE_SERVER = config
            .getString("com.lmp.back.images");
    
    
    /**
     * 后台js文件请求地址
     */
    public static final String BACK_JS_FILE_SERVER = config
            .getString("com.lmp.back.js");
    
    /**
     * 后台样式请求地址
     */
    public static final String BACK_CSS_FILE_SERVER = config
            .getString("com.lmp.back.css");
    
    /**
     * APP下载路径.
     */
    public static final String DOWNLOAD_APP_PATH = "";

    /**
     * 资源文件夹路径.
     */
    public static final String CREATE_RESOURCE_PATH = config
            .getString("create_resource_path");
    
    
    /**
     * 生成品类文件夹路径.
     */
    public static final String CREATE_CATEGORYFILE_PATH = config
            .getString("create_category_path");
    
    /**
     * 海外退运上传图片存放地址.
     */
    public static final String UPLOAD_PATH_IMAGES = config
            .getString("upload_path_images");
    
    /**
     * 前台请求前缀
     */
    public static final String FRONT_URL_PREFIX = config
            .getString("com.lmp.weixin.api.url.prefix");

    
    /**
     * 返利接口校验site code
     */
    public static final String REBATES_ME_SITE_CODE = config
            .getString("com.lmp.rebatesMeApi.siteCode");
    /**
     * 返利接口校验site key
     */
    public static final String REBATES_ME_SITE_KEY = config
            .getString("com.lmp.rebatesMeApi.siteKey");
    
    /**
     * 微信推送请求发送地址
     */
    public static final String WEIXIN_PUSH_REQUEST_URL = config
            .getString("com.lmp.weixin.push.request.url");
    

}
