package com.xiangrui.lmp.business.api.controller;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;
import com.xiangrui.lmp.business.admin.freightcost.service.FreightCostService;
import com.xiangrui.lmp.business.admin.freightcost.vo.FreightCost;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgAttachServiceService;
import com.xiangrui.lmp.business.admin.pkg.service.UnusualPkgService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
import com.xiangrui.lmp.business.admin.quotationManage.service.QuotationManageService;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.store.mapper.StoreMapper;
import com.xiangrui.lmp.business.admin.store.vo.StorePackage;
import com.xiangrui.lmp.business.api.bean.APIPackageInfo;
import com.xiangrui.lmp.business.api.bean.PaymentInfo;
import com.xiangrui.lmp.business.api.bean.SplitBoxGoodInfo;
import com.xiangrui.lmp.business.api.bean.SplitBoxInfo;
import com.xiangrui.lmp.business.api.service.PackageApiService;
import com.xiangrui.lmp.business.api.util.WeiXinImageUtil;
import com.xiangrui.lmp.business.api.vo.ExpressContent;
import com.xiangrui.lmp.business.api.vo.ExpressDetail;
import com.xiangrui.lmp.business.api.vo.FilterCondition;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.homepage.controller.HomePkgGoodsController;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgGoodsMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontUserAddressMapper;
import com.xiangrui.lmp.business.homepage.service.FrontOverSeaAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontReceiveAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontShareService;
import com.xiangrui.lmp.business.homepage.service.FrontUserAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.service.PkgImgService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontReceiveAddress;
import com.xiangrui.lmp.business.homepage.vo.FrontShare;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;
import com.xiangrui.lmp.business.homepage.vo.PackageImg;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.util.IdentifierCreator;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 包裹相关接口
 * 
 * @author ljc
 */
@Controller
@RequestMapping(value = "/api")
public class PackageApiController
{
	private static final Logger logger = Logger.getLogger(PackageApiController.class);
	/**
	 * 新增增值服务的初试状态,服务开启,
	 */
	private final static int ATTACH_SERVICE_STATUS_INIT = 1;
	private static final int PKG_ENABLE = 1;// 包裹状态：可用
	@Autowired
	private FrontUserService ffrontUserService; // 前台用户
	@Autowired
	private PackageApiService packageApiService;
	@Autowired
	private PkgService frontPkgService;
	@Autowired
	private PackageService packageService;
	@Autowired
	private PkgAttachServiceService pkgAttachServiceService;// 增值服务与包裹间的关联表
	@Autowired
	private FrontOverSeaAddressService frontOverSeaAddressService;
	@Autowired
	private PkgAttachServiceMapper pkgAttachServiceMapper;
	/**
	 * 增值服务数据查询
	 */
	@Autowired
	private AttachServiceService attachServiceService;
	@Autowired
	private PkgGoodsService frontPkgGoodsService;
	@Autowired
	private QuotationManageService quotationManageService;
	@Autowired
	private StoreMapper storeMapper;
	@Autowired
	private FreightCostService freightCostService;
	@Autowired
	private PackageMapper packageMapper;
	// 异常包裹服务
	@Autowired
	private UnusualPkgService unusualPkgService;
	@Autowired
	private FrontPkgGoodsMapper frontPkgGoodsMapper;

	@Autowired
	private FrontUserAddressService frontUserAddressService;
	@Autowired
	private PkgImgService frontPkgImgService;

	@Autowired
	private FrontShareService frontShareService;
	
	@Autowired
	private FrontUserAddressMapper frontUserAddressMapper;
	
	@Autowired
	private FrontReceiveAddressService frontReceiveAddressService;

	/**
	 * 运单查询接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/query_order_by_no", method = RequestMethod.GET)
	public Map<String, Object> queryOrderByNo(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String order_no = request.getParameter("order_no");
		String openId=StringUtils.trimToNull(request.getParameter("openId"));
		logger.info("运单查询接口  order_no=" + order_no);
		if (order_no == null || "".equals(order_no))
		{
			result.put("msg", "非法参数,order_no不能为空");
			result.put("result", 1101);
			return result;
		}
		if (openId == null)
		{
			result.put("msg", "非法参数,open_id不能为空");
			result.put("result", 1101);
			return result;
		}		
		try
		{
			List<ExpressDetail> list = packageApiService.queryOrderByNo(openId, order_no);
			if (list == null || list.size() == 0)
			{
				result.put("msg", "不存在的单号");
				result.put("result", 1102);
				return result;
			}
			else
			{
				result.put("list", list);
				result.put("msg", "请求成功");
				result.put("result", 0);
				result.put("isSuccess", Boolean.TRUE);
			}

		} catch (Exception e)
		{
			logger.error("运单查询接口！  order_no=" + order_no);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 根据运单号查询物流信息追踪接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_logistics_info_by_order_no", method = RequestMethod.GET)
	public Map<String, Object> getLogisticsInfoByOrderNo(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String order_no = request.getParameter("order_no");
		String phone = request.getParameter("phone");
		logger.info("根据运单号查询物流信息追踪接口  order_no=" + order_no);
		logger.info("根据运单号查询物流信息追踪接口  phone=" + phone);
		if (order_no == null || "".equals(order_no))
		{
			result.put("msg", "非法参数");
			result.put("result", 1101);
			return result;
		}
		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户");
				result.put("result", 1101);
				return result;
			}
			List<ExpressContent> list = packageApiService.getLogisticsInfoByOrderNo(order_no);
			if (list == null || list.size() == 0)
			{
				result.put("msg", "不存在的单号");
				result.put("result", 1102);
				return result;
			}
			else
			{
				result.put("list", list);
				result.put("msg", "请求成功");
				result.put("result", 0);
				result.put("isSuccess", Boolean.TRUE);
			}
		} catch (Exception e)
		{
			logger.error("根据运单号查询物流信息追踪接口！  order_no=" + order_no);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取所有包裹列表接口
	 * 
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_all_package", method = RequestMethod.GET)
	public Map<String, Object> getAllPackage(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String phone = request.getParameter("phone");
		logger.info("获取所有包裹列表接口  phone=" + phone);

		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户");
				result.put("result", 1101);
				return result;
			}
			List<HashMap<String, Object>> list = packageApiService.queryAllPackage(frontUser.getUser_id());
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取所有包裹列表接口 出错！  phone=" + phone);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 按照包裹收件人的手机号查询到属于这个收件人的包裹列表接口
	 * 
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/phone_get_all_package", method = RequestMethod.GET)
	public Map<String, Object> phoneGetAllPackage(HttpServletRequest request, String phone, String code)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		logger.info("按照包裹收件人的手机号查询到属于这个收件人的包裹列表接口  phone=" + phone);

		try
		{
			String smsCode = BaseApiController.getSmsCodeByPhone(phone);
			if (smsCode == null || !smsCode.equals(code))
			{
				result.put("msg", "验证码错误！");
				result.put("result", 1102);
				return result;
			}
			List<HashMap<String, Object>> list = packageApiService.queryAllPackageByReceiverMoblie(phone);
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("按照包裹收件人的手机号查询到属于这个收件人的包裹列表接口 出错！  phone=" + phone);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 获取查询历史接口
	 * 
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/query_history", method = RequestMethod.GET)
	public Map<String, Object> queryHistory(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String phone = request.getParameter("phone");
//		String smsVerificationCode = request.getParameter("smsVerificationCode");

		String openId=StringUtils.trimToNull(request.getParameter("openId"));
		logger.info("获取查询历史接口  openId=" + openId);
		if (openId == null)
		{
			result.put("msg", "非法参数,open_id不能为空");
			result.put("result", 1101);
			return result;
		}	
//		logger.info("获取查询历史接口  phone=" + phone);

		try
		{
//			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
//			if (frontUser == null)
//			{
//				result.put("msg", "不存在该手机用户!");
//				result.put("result", 1101);
//				return result;
//			}
//			String smsCode = BaseApiController.getSmsCodeByPhone(phone);
//			if (smsCode == null || !smsCode.equals(smsVerificationCode))
//			{
//				result.put("msg", "验证码错误！");
//				result.put("result", 1102);
//				// return result;
//			}

			List<Map<String, Object>> list = packageApiService.queryHistory(openId);
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取查询历史接口  出错！  phone=" + phone);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取指定包裹详情接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_package_detail", method = RequestMethod.GET)
	public Map<String, Object> getPackageDetail(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String idStr = request.getParameter("id");
		logger.info("获取指定包裹详情接口  id=" + idStr);
		if ("".equals(idStr))
		{
			result.put("msg", "非法参数");
			result.put("result", 1101);
			return result;
		}
		int id = 0;
		try
		{
			id = Integer.parseInt(idStr);
			Map<String, Object> packageDetail = packageApiService.queryPackageByPackageId(id);
			result.put("packageDetail", packageDetail);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取指定包裹详情接口 出错！  id=" + id);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 指定条件查询包裹接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_all_package_condition", method = RequestMethod.GET)
	public Map<String, Object> getAllPackageByCondition(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String phone = request.getParameter("phone");
		String condition = request.getParameter("condition");
		logger.info("指定条件查询包裹接口 phone=" + phone);
		logger.info("指定条件查询包裹接口 condition=" + condition);

		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户");
				result.put("result", 1101);
				return result;
			}
			List<Map<String, Object>> list = packageApiService.getAllPackageByCondition(frontUser.getUser_id(), condition);
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取指定包裹详情接口 出错！  =");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 公司单号/关联单号/收件人姓名/收件人手机号查询包裹
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_packages", method = RequestMethod.GET)
	public Map<String, Object> getPackages(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String phone = request.getParameter("phone");
		// String orderNo = request.getParameter("orderNo");// 公司单号
		// String originalNum = request.getParameter("relativeNo");// 关联单号
		// String receiver = request.getParameter("receiver");// 收件人姓名
		// String receiverPhone = request.getParameter("receiverPhone");//
		// 收件人手机号查询包裹

		String condition = request.getParameter("condition");
		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户");
				result.put("result", 1101);
				return result;
			}
			// if (receiver != null)
			// {
			// receiver = new String(receiver.getBytes("ISO-8859-1"), "UTF-8");
			// }
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("userId", frontUser.getUser_id());
			// paramMap.put("orderNo", orderNo);
			// paramMap.put("originalNum", originalNum);
			// paramMap.put("receiver", receiver);
			// paramMap.put("receiverPhone", receiverPhone);

			if (condition != null)
			{
				condition = new String(condition.getBytes("ISO-8859-1"), "UTF-8");
			}
			else
			{
				result.put("msg", "查询条件不能为空");
				result.put("result", 1101);
				return result;
			}
			paramMap.put("condition", condition);
			List<Map<String, Object>> list = packageApiService.getPackages(paramMap);
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("查询包裹接口 出错！  =");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取可合并包裹接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_package_merge", method = RequestMethod.GET)
	public Map<String, Object> getPackageMerge(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String phone = request.getParameter("phone");
		logger.info("获取可合并包裹接口  phone=" + phone);
		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户");
				result.put("result", 1101);
				return result;
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("user_id", frontUser.getUser_id());
			List<Map<String, Object>> availableMergeList = packageApiService.queryAvailableMergePackageList(paramMap);
			result.put("availableMergeList", availableMergeList);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取可合并包裹接口  出错！  phone=" + phone);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 根据运单号查询可合并的包裹
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_package_merge_by_no", method = RequestMethod.GET)
	public Map<String, Object> getPackageMergeByNo(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String phone = request.getParameter("phone");
		String order_no = request.getParameter("order_no");
		logger.info("根据运单号查询可合并的包裹  phone=" + phone);
		logger.info("根据运单号查询可合并的包裹  order_no=" + order_no);
		if ("".equals(phone) || "".equals(order_no))
		{
			result.put("msg", "非法参数");
			result.put("result", 1101);
			return result;
		}
		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户");
				result.put("result", 1101);
				return result;
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("user_id", frontUser.getUser_id());
			paramMap.put("order_no", order_no);
			List<Map<String, Object>> availableMergeList = packageApiService.queryAvailableMergePackageList(paramMap);
			result.put("availableMergeList", availableMergeList);

			/*
			 * Map<String, Object> params = new HashMap<String, Object>();
			 * params.put("user_id", frontUser.getUser_id());
			 * params.put("enable", PKG_ENABLE); params.put("logistics_code",
			 * order_no);
			 * 
			 * Pkg pkg = frontPkgService.queryPkgBylogistics(params); if (pkg !=
			 * null) { if (pkg.getStatus() > 0) { result.put("msg",
			 * "该包裹不能参与合箱!"); } else { result.put("msg", "Ok"); } if
			 * (!checkCanMergeSplitPackage("" + pkg.getPackage_id())) {
			 * result.put("msg", "该包裹不能参与合箱!"); } // 查询海外仓库地址
			 * FrontOverSeaAddress frontOverSeaAddress =
			 * frontOverSeaAddressService.queryAllById(pkg.getOsaddr_id()); //
			 * 获取仓库名称 String warehouse =
			 * frontOverSeaAddress.getWarehouseIndex(); result.put("package_id",
			 * String.valueOf(pkg.getPackage_id()));
			 * result.put("logistics_code", pkg.getLogistics_code());
			 * result.put("warehouse", warehouse); result.put("osaddr_id",
			 * String.valueOf(pkg.getOsaddr_id())); result.put("original_num",
			 * pkg.getOriginal_num());
			 * com.xiangrui.lmp.business.admin.pkg.vo.Pkg tpkg =
			 * packageService.queryPackageById(pkg.getPackage_id()); if
			 * ("Y".equals(tpkg.getException_package_flag())) {
			 * result.put("msg", "包裹信息不完整，无法进行分箱合箱"); } }else{ result.put("msg",
			 * "该包裹不能参与合箱!"); }
			 */
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("根据运单号查询可合并的包裹  出错！  phone=" + phone);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取包裹筛选条件接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_package_filter_condition", method = RequestMethod.GET)
	public Map<String, Object> getPackageFilterCondition(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		try
		{
			List<FilterCondition> list = packageApiService.getPackageFilterCondition();
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取包裹筛选条件接口  出错！");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 根据各种条件筛选包裹的接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_package_by_filter", method = RequestMethod.POST)
	public Map<String, Object> getPackageByFilter(@RequestBody JSONObject jsonObject)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String phone = jsonObject.has("phone") ? jsonObject.getString("phone") : "";
		String filters = jsonObject.has("filters") ? jsonObject.getString("filters") : "";

		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户");
				result.put("result", 1101);
				return result;
			}
			List<Map<String, Object>> list = packageApiService.getPackageByFilter(frontUser.getUser_id(), filters);
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("根据各种条件筛选包裹的接口  出错！  phone=" + phone);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	private boolean checkCanMergeSplitPackage(String packageId)
	{
		boolean canMergeSplit = true;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("package_id", packageId);
		List<PkgAttachServiceGroup> pkgAttachServiceGroupList = pkgAttachServiceService.queryPkgAttachServiceGroup(params);
		if (pkgAttachServiceGroupList != null && pkgAttachServiceGroupList.size() > 0)
		{
			for (PkgAttachServiceGroup pkgAttachServiceGroup : pkgAttachServiceGroupList)
			{
				if (pkgAttachServiceGroup.getAttach_ids().contains("" + BaseAttachService.MERGE_PKG_ID) || pkgAttachServiceGroup.getAttach_ids().contains("" + BaseAttachService.SPLIT_PKG_ID))
				{
					canMergeSplit = false;
				}
			}
		}
		return canMergeSplit;
	}

	/**
	 * 添加包裹接口
	 * 
	 * @param req
	 * @param jsonObject
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/do_add_package", method = RequestMethod.POST)
	public Map<String, Object> do_add_package(HttpServletRequest request, String oversea_warehouse, String receiver_info, String user_id)
	{
		Pkg pkg = new Pkg();
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String package_infoStr = request.getParameter("package_info");
		Gson gson = new Gson();
		APIPackageInfo package_info = gson.fromJson(package_infoStr, APIPackageInfo.class);
		logger.info("添加包裹接口  oversea_warehouse=" + oversea_warehouse);
		if (oversea_warehouse == null || "".equals(oversea_warehouse))
		{
			result.put("msg", "仓库不能为空！");
			result.put("result", 1104);
			return result;
		}
		try
		{
			pkg.setOsaddr_id(Integer.parseInt(oversea_warehouse));
		} catch (Exception e)
		{
			result.put("msg", "仓库有误！");
			result.put("result", 1104);
			return result;
		}
		if (receiver_info == null || "".equals(receiver_info))
		{
			result.put("msg", "收件地址不能为空！");
			result.put("result", 1104);
			return result;
		}
		try
		{
			pkg.setAddress_id(Integer.parseInt(receiver_info));
		} catch (Exception e)
		{
			result.put("msg", "收件地址有误！");
			result.put("result", 1104);
			return result;
		}

		if (user_id == null || "".equals(user_id))
		{
			result.put("msg", "用户user_id不能为空！");
			result.put("result", 1112);
			return result;
		}
		if (package_info == null)
		{
			result.put("msg", "包裹信息不能为空！");
			result.put("result", 1113);
			return result;
		}
		String original_num = StringUtils.trimToNull(package_info.getRef_orderno());
		if (package_info.getRef_orderno() == null)
		{
			result.put("msg", "关联单号不能为空！");
			result.put("result", 1114);
			return result;
		}
		if (!checkOriginalnumDeal(original_num))
		{
			result.put("msg", "关联单号不可用");
			result.put("result", 1115);
			return result;
		}

		FrontUser frontUser = ffrontUserService.queryFrontUserByUserId(Integer.parseInt(user_id));
		if (frontUser == null)
		{
			result.put("msg", "未识别当前身份，请先登录系统！");
			result.put("result", 1111);
			return result;
		}
		List<PkgGoods> pkgGoodsList = new ArrayList<PkgGoods>();

		if (package_info.getGoods() == null)
		{
			result.put("msg", "包裹物品信息不能为空！");
			result.put("result", 1113);
			return result;
		}
		try
		{
			for (int i = 0; i < package_info.getGoods().size(); i++)
			{
				PkgGoods pkgGoods = new PkgGoods();
				pkgGoods.setGoods_type_id(Integer.parseInt(package_info.getGoods().get(i).getCategory()));
				pkgGoods.setGoods_name(package_info.getGoods().get(i).getName());
				pkgGoods.setPrice(Float.parseFloat(package_info.getGoods().get(i).getUnit_price()));
				pkgGoods.setQuantity(Integer.parseInt(package_info.getGoods().get(i).getTotal()));
				pkgGoods.setBrand(package_info.getGoods().get(i).getBrand());
				pkgGoodsList.add(pkgGoods);
			}
		} catch (Exception e)
		{
			result.put("msg", "包裹物品信息有误！");
			result.put("result", 1113);
			return result;
		}

		// 获取物流单号、物流跟踪号、用户标识
		Timestamp creatTime = new Timestamp(System.currentTimeMillis());
		String logistics_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PACKAGE);

		String keepPrice = StringUtils.trimToNull(package_info.getSave_price());
		float keepPriceServiceFee = 0;
		if (keepPrice != null)
		{
			keepPriceServiceFee = Float.parseFloat(keepPrice);
		}

		pkg.setStatus(Pkg.LOGISTICS_STORE_WAITING);
		pkg.setLogistics_code(logistics_code);
		pkg.setCreateTime(creatTime);
		pkg.setUser_id(frontUser.getUser_id());

		pkg.setOriginal_num(original_num);

		int pay_type = 1;
		try
		{
			pay_type = Integer.parseInt(StringUtils.trimToNull(package_info.getPay_type()));
		} catch (Exception e)
		{
			// TODO: handle exception
		}
		pkg.setPay_type(pay_type);

		if (frontUser.getUser_type() == 1)
		{
			// 资费费率
			Map<String, Object> ttparmas = new HashMap<String, Object>();
			ttparmas.put("integral_min", frontUser.getIntegral());
			ttparmas.put("rate_type", frontUser.getUser_type());

			FreightCost freightCost = freightCostService.queryFreightCostByIntegralMin(ttparmas);
			if (freightCost != null)
			{
				float price = freightCost.getUnit_price();
				pkg.setPrice(price);
			}
		}
		else
		{
			pkg.setPrice(0);
		}
		
		// 002)此处查询报价单，将报价单id做set包裹处理
		int express_package = 0;
		QuotationManage qm = quotationManageService.getQuotaByPkgInfo(Integer.parseInt(oversea_warehouse),
				express_package, frontUser.getUser_type(),frontUser.getUser_id());
		if (qm != null) {
			pkg.setQuota_id(qm.getQuota_id());
		}

		frontPkgGoodsService.addPkg(pkg);

		int package_id = pkg.getPackage_id();

		String waitting_merge_flag = package_info.getNeed_merge() == null ? "N" : package_info.getNeed_merge();
		setWaittingMergePkgHandle(package_id, waitting_merge_flag);

		List<PkgAttachService> attachServices = new ArrayList<PkgAttachService>();
		if (null != package_info.getExtend_values())
		{
			float total_transport_cost = 0;
			for (int i = 0; i < package_info.getExtend_values().size(); i++)
			{
				boolean canAdd = true;
				int attach_id = Integer.parseInt(package_info.getExtend_values().get(i));
				PkgAttachService attachService = new PkgAttachService();
				attachService.setAttach_id(attach_id);
				attachService.setPackage_id(package_id);
				attachService.setStatus(ATTACH_SERVICE_STATUS_INIT);
				float attachServicePrice = getServicePrice(frontUser.getUser_id(), attach_id);
				if (attach_id == BaseAttachService.KEEP_PRICE_ID)
				{
					if (keepPriceServiceFee == 0)
					{
						canAdd = false;
					}
					float ddddd = 0.01f;
					attachServicePrice = keepPriceServiceFee * ddddd;
					attachService.setStatus(2);// 完成
				}
				// 将增值服务插入PkgAttachService
				attachService.setService_price(attachServicePrice);

				// 包裹创建，原始包裹package_group默认为当前包裹id
				attachService.setPackage_group(String.valueOf(attachService.getPackage_id()));
				if (canAdd)
				{
					attachServices.add(attachService);
					total_transport_cost += attachServicePrice;
				}

			}
			if (!waitting_merge_flag.equals("Y"))
			{
				/*
				 * // 资费费率 Map<String, Object> parmas = new HashMap<String,
				 * Object>(); parmas.put("integral_min",
				 * frontUser.getIntegral()); parmas.put("rate_type",
				 * frontUser.getUser_type());
				 * 
				 * FreightCost freightCost =
				 * freightCostService.queryFreightCostByIntegralMin(parmas);
				 * BigDecimal totalTransportCostBigDecimal = new
				 * BigDecimal(freightCost.getUnit_price()).multiply(new
				 * BigDecimal(total_transport_cost)); float totalTransportCost =
				 * totalTransportCostBigDecimal.floatValue(); // 转运总费用
				 * pkg.setTransport_cost(totalTransportCost);
				 */
				com.xiangrui.lmp.business.admin.pkg.vo.Pkg backPkg = new com.xiangrui.lmp.business.admin.pkg.vo.Pkg();
				backPkg.setTransport_cost(total_transport_cost);
				backPkg.setPackage_id(package_id);
				packageMapper.update(backPkg);
				this.pkgAttachServiceService.addPkgAttachServiceOnList(attachServices);
			}
		}

		for (PkgGoods pkgGoods : pkgGoodsList)
		{
			pkgGoods.setPackage_id(package_id);
		}
		frontPkgGoodsService.addPkgGoods(pkgGoodsList);
		result.put("result", true);
		result.put("msg", "新建包裹完成");
		result.put("isSuccess", Boolean.TRUE);
		return result;
	}

	/**
	 * 合并包裹接口
	 * 
	 * @param req
	 * @param jsonObject
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/do_merge_packages", method = RequestMethod.POST)
	public Map<String, Object> do_merge_packages(HttpServletRequest request, String receiver_info, String oversea_warehouse, String user_id)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String packagesStr = request.getParameter("packages");
		List<String> packages = JSONTool.getDTOStringList(packagesStr);

		logger.info("合并包裹接口  receiver_info=" + receiver_info);
		if (oversea_warehouse == null || "".equals(oversea_warehouse))
		{
			result.put("msg", "仓库不能为空！");
			result.put("result", 1104);
			return result;
		}
		if (receiver_info == null || "".equals(receiver_info))
		{
			result.put("msg", "收件地址不能为空！");
			result.put("result", 1104);
			return result;
		}
		if (user_id == null || "".equals(user_id))
		{
			result.put("msg", "用户user_id不能为空！");
			result.put("result", 1112);
			return result;
		}
		int express_pacakge_count=0;
		boolean submitAgainst = false;
		boolean haveNotAllowMergePackage=false;
		List<Integer> package_ids = new ArrayList<Integer>();
		for (int i = 0; i < packages.size(); i++)
		{
			int package_id = Integer.parseInt(packages.get(i));
			package_ids.add(package_id);
			Map<String, Object> tparams = new HashMap<String, Object>();
			tparams.put("package_id", package_id);
			// 查询包裹是否有合箱服务
			tparams.put("attach_id", AttachService.MERGE_PKG_ID);
			List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(tparams);
			if (mergelist != null && mergelist.size() > 0)
			{
				submitAgainst = true;
			}
			if(!packageApiService.checkPackageAllReady(package_id))
			{
				haveNotAllowMergePackage=true;
			}
			Pkg tempPkg = frontPkgService.queryPackageByPackageId(package_id);
			if(tempPkg!=null)
			{
				express_pacakge_count +=tempPkg.getExpress_package();
			}
		}
		if (haveNotAllowMergePackage)
		{
			result.put("msg", "合箱失败，所选包裹可能包含了异常包裹，请检查~");
			result.put("result", 1112);
			return result;
		}
		if (!(express_pacakge_count == 0 || express_pacakge_count == packages.size()))
		{
			result.put("msg", "合箱失败，所选合箱包裹不全部在同一渠道（只有渠道相同的包裹才能合箱），请检查~");
			result.put("result", 1112);
			return result;
		}

		if (!submitAgainst)
		{
			int int_user_id = Integer.parseInt(user_id);
			Map<String, Object> params = new HashMap<String, Object>();

			params.put("address_id", Integer.parseInt(receiver_info));
			params.put("package_ids", package_ids);
			params.put("osaddr_id", Integer.parseInt(oversea_warehouse));
			params.put("package_group", packages.toArray());

			// 获取合箱费用
			float service_price = getServicePrice(int_user_id, AttachService.MERGE_PKG_ID);

			// 增加合箱包裹
			int cnt = frontPkgGoodsService.insertMergePkg(params, service_price,express_pacakge_count==0?0:1);

			if (cnt > 0)
			{
				boolean isContainExceptionPKG = false;
				Integer targetpackage_id_int = null;
				int exceptionPkgCount = 0;
				// 清除异常包裹，不让其在前台待处理栏上显示
				for (Integer tempPackageId : package_ids)
				{
					Pkg tempPkg = frontPkgService.queryPackageByPackageId(tempPackageId);
					if ("Y".equals(StringUtils.trimToEmpty(tempPkg.getException_package_flag())))
					{
						FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(tempPkg.getAddress_id());
						// 客户在编辑包裹时包裹内件数大于等于1，且包裹有选择收货地址，点击完成，那么包裹从前台页面的“待处理包裹”下消失
						// 包裹的商品
						List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(tempPkg.getPackage_id());
						// 客户在编辑包裹时包裹内件数大于等于1，且包裹有选择收货地址，点击完成，那么包裹从前台页面的“待处理包裹”下消失
						if (frontUserAddress != null && gooldList != null && gooldList.size() > 0)
						{
							packageService.updateExceptionPackageFlag(tempPkg.getAddress_id(), "N");
						}
					}
					if (StringUtils.trimToNull(tempPkg.getException_package_flag()) != null)
					{
						isContainExceptionPKG = true;
						exceptionPkgCount++;
						Map<String, Object> tparams = new HashMap<String, Object>();
						tparams.put("package_id", tempPackageId);
						// 查询包裹是否有合箱服务
						tparams.put("attach_id", AttachService.MERGE_PKG_ID);
						List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(tparams);
						if (mergelist != null && mergelist.size() > 0)
						{

							PkgAttachServiceGroup serviceGroup = mergelist.get(0);
							String targetpackage_id = serviceGroup.getTgt_package_id_group();
							targetpackage_id_int = Integer.parseInt(targetpackage_id);
						}
					}
				}
				// 异常包裹合箱后需要在后台到库列表和待处理列表中显示
				if (isContainExceptionPKG)
				{
					StorePackage childPkg = new StorePackage();
					// 全部是异常包裹合箱的话需要在待处理列表中显示，因为异常包裹默认是到库的
					if (exceptionPkgCount == package_ids.size())
					{
						childPkg.setPackage_id(targetpackage_id_int);
						childPkg.setNeed_handle_flag(StorePackage.NEED_HANLDE);
						storeMapper.updateNeedHandleFlag(childPkg);
					}
					childPkg.setPackage_id(targetpackage_id_int);
					childPkg.setArrived_package_display_flag(StorePackage.NEED_HANLDE);
					storeMapper.updateArrivedPackageDisplayFlag(childPkg);
				}
				result.put("result", true);
				result.put("msg", "合箱完成");
				result.put("isSuccess", Boolean.TRUE);
			}
			else
			{
				result.put("result", false);
				result.put("msg", "失败");
			}
		}
		else
		{
			result.put("msg", "合箱请求重复");
		}
		return result;
	}

	/**
	 * 查询增值服务费用
	 * 
	 * @param req
	 * @param attach_id
	 * @return
	 */
	private float getServicePrice(int user_id, int attach_id)
	{
		// 数据库的增值服务信息
		List<AttachService> attachServiceList = attachServiceService.selectAttachService(user_id);
		float service_price = 0;
		if (!attachServiceList.isEmpty())
		{
			for (AttachService attachService : attachServiceList)
			{
				if (attach_id == attachService.getAttach_id())
				{
					service_price = attachService.getService_price();
					break;
				}
			}
		}
		return service_price;
	}

	/**
	 * 关联单号唯一性检查接口
	 * 
	 * @param request
	 * @param ref_order_no
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/check_original_num_valid", method = RequestMethod.GET)
	public Map<String, Object> checkOriginalnumValid(HttpServletRequest request, String original_num)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		logger.info("关联单号唯一性检查接口  original_num=" + original_num);
		if (original_num == null || "".equals(original_num))
		{
			result.put("msg", "非法参数");
			result.put("result", 1101);
			return result;
		}
		try
		{
			result.put("msg", "0");
			if (checkOriginalnumDeal(original_num))
			{
				isSuccess = Boolean.TRUE;
				result.put("msg", "关联单号可用");
				result.put("result", 0);
				result.put("isSuccess", isSuccess);
			}
			else
			{
				isSuccess = Boolean.FALSE;
				result.put("msg", "关联单号已存在");
				result.put("result", 1101);
			}
		} catch (Exception e)
		{
			logger.error("关联单号唯一性检查接口！  original_num=" + original_num);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	private boolean checkOriginalnumDeal(String original_num)
	{
		boolean isValid = true;
		String regex = "^[0-9A-Za-z]+$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(original_num);
		if (!matcher.find())
		{
			isValid = false;
		}
		else
		{
			List<Pkg> pkgList = frontPkgService.queryPackageByOriginalNum(original_num);
			// 增加异常包裹关联单号的检查
			UnusualPkg unusualPkg = unusualPkgService.queryUnusualPkgByOriginal_num(original_num);
			if ((null != pkgList && pkgList.size() > 0) || unusualPkg != null||HomePkgGoodsController.checkOriginalNumExist(original_num))
			{
				isValid = false;
			}
		}
		return isValid;
	}

	/**
	 * 包裹分箱接口
	 * 
	 * @param req
	 * @param jsonObject
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/package_box", method = RequestMethod.POST)
	public Map<String, Object> package_box(HttpServletRequest request, String package_id, String order_no, String ref_order_no, String osaddr_id, String user_id)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String boxesStr = request.getParameter("boxes");

		logger.info("包裹分箱接口  package_id=" + package_id);
		if (package_id == null || "".equals(package_id))
		{
			result.put("msg", "原包裹ID不能为空！");
			result.put("result", 1101);
			return result;
		}
		if (order_no == null || "".equals(order_no))
		{
			result.put("msg", "运单号不能为空！");
			result.put("result", 1102);
			return result;
		}
		if (osaddr_id == null || "".equals(osaddr_id))
		{
			result.put("msg", "海外仓ID不能为空！");
			result.put("result", 1102);
			return result;
		}
		if (user_id == null || "".equals(user_id))
		{
			result.put("msg", "用户user_id不能为空！");
			result.put("result", 1112);
			return result;
		}
		if (boxesStr == null || "".equals(boxesStr))
		{
			result.put("msg", "分箱信息不能为空！");
			result.put("result", 1113);
			return result;
		}
		Gson gson = new Gson();
		List<SplitBoxInfo> splitBoxList = gson.fromJson(boxesStr, new TypeToken<List<SplitBoxInfo>>() {
		}.getType());
		int package_id_int = 0;
		try
		{
			package_id_int = Integer.parseInt(package_id);
		} catch (Exception e)
		{
			logger.error("转换包裹id错误：", e);
			result.put("result", false);
			result.put("msg", "原包裹ID有误");
			return result;
		}

		Timestamp createTime = new Timestamp(System.currentTimeMillis());
		// 新包裹集合
		List<Pkg> newPkgList = new ArrayList<Pkg>();
		if (splitBoxList != null && splitBoxList.size() > 1)
		{
			for (SplitBoxInfo splitBoxInfo : splitBoxList)
			{
				// 新包裹
				Pkg pkg = new Pkg();

				// 新的运单号
				String logistics_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PACKAGE);
				pkg.setLogistics_code(logistics_code);
				pkg.setCreateTime(createTime);
				List<SplitBoxGoodInfo> splitBoxGoodInfoList = splitBoxInfo.getGoods_list();
				if (splitBoxGoodInfoList != null && splitBoxGoodInfoList.size() > 0)
				{
					List<PkgGoods> pkgGoodsList = new ArrayList<PkgGoods>();
					for (SplitBoxGoodInfo splitBoxGoodInfo : splitBoxGoodInfoList)
					{
						Integer goodId = Integer.parseInt(splitBoxGoodInfo.getGoods_id());
						int afterSplitgoodQuantity = Integer.parseInt(splitBoxGoodInfo.getTotal());
						PkgGoods existPkgGoods = frontPkgGoodsMapper.queryPkgGoodsByGoodId(goodId);
						existPkgGoods.setQuantity(afterSplitgoodQuantity);
						pkgGoodsList.add(existPkgGoods);
					}
					pkg.setGoods(pkgGoodsList);
				}
				newPkgList.add(pkg);
			}
		}
		else
		{
			result.put("msg", "分箱信息有误！");
			result.put("result", 1113);
			return result;
		}
		int int_user_id = Integer.parseInt(user_id);

		Map<Integer, List<Pkg>> params = new HashMap<Integer, List<Pkg>>();
		params.put(package_id_int, newPkgList);

		float service_price = getServicePrice(int_user_id, AttachService.SPLIT_PKG_ID);

		// 将分箱之后的包裹保存
		frontPkgGoodsService.insertNewDisassemblePkg(params, service_price);
		result.put("result", true);
		result.put("msg", "分箱完成");
		result.put("isSuccess", Boolean.TRUE);

		return result;
	}

	/**
	 * 包裹前去支付接口
	 * 
	 * @param req
	 * @param jsonObject
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/payment", method = RequestMethod.POST)
	public Map<String, Object> payment(HttpServletRequest request, String packages, String phone, String total)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		int coupon_id = -1;
		String coupon_idStr = StringUtils.trimToNull(request.getParameter("coupon_id"));
		if (coupon_idStr != null)
		{
			coupon_id = Integer.parseInt(coupon_idStr);
		}

		logger.info("包裹前去支付接口  packages=" + packages);
		if (packages == null || "".equals(packages))
		{
			result.put("msg", "前去支付包裹信息不能为空！");
			result.put("result", 1101);
			return result;
		}
		if (phone == null || "".equals(phone))
		{
			result.put("msg", "手机号码不能为空！");
			result.put("result", 1103);
			return result;
		}
		FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
		if (frontUser == null)
		{
			result.put("msg", "用户不存在！");
			result.put("result", 1102);
			return result;
		}
		if (total == null || "".equals(total))
		{
			result.put("msg", "总金额不能为空！");
			result.put("result", 1104);
			return result;
		}
		/*
		 * if (code == null || "".equals(code)) { result.put("msg", "验证码不能为空！");
		 * result.put("result", 1115); return result; }
		 */
		Gson gson = new Gson();
		List<PaymentInfo> payPackageInfoList = gson.fromJson(packages, new TypeToken<List<PaymentInfo>>() {
		}.getType());
		BigDecimal total_float = null;
		try
		{
			// 支付金额
			total_float = new BigDecimal(total);
		} catch (Exception e)
		{
			logger.error("转换总金额错误：", e);
			result.put("result", false);
			result.put("msg", "总金额有误");
			return result;
		}

		if (FrontUser.FRONT_USER_STATUS_NO_ACTIVATION == frontUser.getStatus())
		{
			result.put("message", "该账户未激活！");
			result.put("result", "4");
			return result;

		}
		else if (FrontUser.FRONT_USER_STATUS_DISABLE == frontUser.getStatus())
		{
			result.put("message", "该账户已被禁用！");
			result.put("result", "5");
			return result;
		}

		if (total_float.compareTo(new BigDecimal("0.0001")) < 0)
		{
			result.put("message", "没有要支付的项目！");
			result.put("result", "6");
			return result;
		}

		String logisticsListStr = "";
		if (payPackageInfoList != null && payPackageInfoList.size() > 0)
		{
			for (PaymentInfo paymentInfo : payPackageInfoList)
			{
				Pkg pkg = frontPkgGoodsService.queryPkgByPkgid(Integer.parseInt(paymentInfo.getPackage_id()));
				if (pkg != null)
				{
					logisticsListStr = pkg.getLogistics_code() + "," + logisticsListStr;
				}
				else
				{
					result.put("msg", "前去支付包裹信息有误(支付包裹不存在)！");
					result.put("result", 1113);
					return result;
				}
			}
			logisticsListStr = StringUtils.substringBeforeLast(logisticsListStr, ",");
			String errorCode = "7";
			String payStatus = ffrontUserService.updatePayment(total, frontUser.getUser_id(), logisticsListStr, coupon_id, errorCode);
			if (payStatus != null)
			{
				result.put("message", payStatus);
				result.put("result", errorCode);
				return result;
			}
		}
		else
		{
			result.put("msg", "前去支付包裹信息有误！");
			result.put("result", 1113);
			return result;
		}
		result.put("result", 0);
		result.put("msg", "支付完成");
		result.put("isSuccess", true);

		return result;
	}

	/**
	 * 根据运单号查询收件人手机号码接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_phone", method = RequestMethod.GET)
	public Map<String, Object> getPhone(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String order_no = request.getParameter("order_no");
		logger.info(" 根据运单号查询收件人手机号码接口   order_no=" + order_no);
		if (order_no == null || "".equals(order_no))
		{
			result.put("msg", "运单号不能为空！");
			result.put("result", 1101);
			return result;
		}
		try
		{
			String phone = packageApiService.getPhone(order_no);
			if (StringUtil.isEmpty(phone))
			{
				result.put("msg", "不存在该运单号对应的手机号");
				result.put("result", 1102);
				result.put("isSuccess", Boolean.TRUE);
				return result;
			}
			else
			{
				result.put("phone", phone);
				result.put("msg", "请求成功");
				result.put("result", 0);
				result.put("isSuccess", Boolean.TRUE);
			}
		} catch (Exception e)
		{
			logger.error("根据运单号查询收件人手机号码接口 出错！  order_no=" + order_no);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 删除指定包裹接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/remove_package", method = RequestMethod.GET)
	public Map<String, Object> remove_package(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String idStr = request.getParameter("id");
		logger.info("删除指定包裹接口  id=" + idStr);
		if ("".equals(idStr))
		{
			result.put("msg", "非法参数");
			result.put("result", 1101);
			return result;
		}
		int id = 0;
		try
		{
			id = Integer.parseInt(idStr);
			if (packageApiService.removePackage(id))
			{
				isSuccess = Boolean.TRUE;
				msg = "删除成功";
			}
			else
			{
				msg = "包裹不可以删除";
			}
			result.put("msg", msg);
			result.put("result", 0);
			result.put("isSuccess", isSuccess);
		} catch (Exception e)
		{
			logger.error("删除指定包裹接口 出错！  id=" + id);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 编辑包裹接口
	 * 
	 * @param req
	 * @param jsonObject
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/edit_package", method = RequestMethod.POST)
	public Map<String, Object> edit_package(HttpServletRequest request, String oversea_warehouse, String receiver_info, String user_id)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		int oversea_warehouse_int = 0;
		int receiver_info_int = 0;
		String package_infoStr = request.getParameter("package_info");
		Gson gson = new Gson();
		APIPackageInfo package_info = gson.fromJson(package_infoStr, APIPackageInfo.class);
		logger.info("编辑包裹接口  oversea_warehouse=" + oversea_warehouse);
		if (oversea_warehouse == null || "".equals(oversea_warehouse))
		{
			result.put("msg", "仓库不能为空！");
			result.put("result", 1104);
			return result;
		}
		try
		{
			oversea_warehouse_int = Integer.parseInt(oversea_warehouse);
		} catch (Exception e)
		{
			result.put("msg", "仓库有误！");
			result.put("result", 1104);
			return result;
		}
		if (receiver_info == null || "".equals(receiver_info))
		{
			result.put("msg", "收件地址不能为空！");
			result.put("result", 1104);
			return result;
		}
		try
		{
			receiver_info_int = Integer.parseInt(receiver_info);
		} catch (Exception e)
		{
			result.put("msg", "收件地址有误！");
			result.put("result", 1104);
			return result;
		}
		if (user_id == null || "".equals(user_id))
		{
			result.put("msg", "用户user_id不能为空！");
			result.put("result", 1112);
			return result;
		}
		if (package_info == null)
		{
			result.put("msg", "包裹信息不能为空！");
			result.put("result", 1113);
			return result;
		}
		String original_num = StringUtils.trimToNull(package_info.getRef_orderno());
		if (package_info.getRef_orderno() == null)
		{
			result.put("msg", "关联单号不能为空！");
			result.put("result", 1114);
			return result;
		}

		FrontUser frontUser = ffrontUserService.queryFrontUserByUserId(Integer.parseInt(user_id));
		if (frontUser == null)
		{
			result.put("msg", "未识别当前身份，请先登录系统！");
			result.put("result", 1111);
			return result;
		}

		List<PkgGoods> pkgGoodsList = new ArrayList<PkgGoods>();
		if (package_info.getGoods() == null)
		{
			result.put("msg", "包裹物品信息不能为空！");
			result.put("result", 1113);
			return result;
		}
		try
		{
			for (int i = 0; i < package_info.getGoods().size(); i++)
			{
				PkgGoods pkgGoods = new PkgGoods();
				pkgGoods.setGoods_type_id(Integer.parseInt(package_info.getGoods().get(i).getCategory()));
				pkgGoods.setGoods_name(package_info.getGoods().get(i).getName());
				pkgGoods.setPrice(Float.parseFloat(package_info.getGoods().get(i).getUnit_price()));
				pkgGoods.setQuantity(Integer.parseInt(package_info.getGoods().get(i).getTotal()));
				pkgGoods.setBrand(package_info.getGoods().get(i).getBrand());
				pkgGoodsList.add(pkgGoods);
			}
		} catch (Exception e)
		{
			result.put("msg", "包裹物品信息有误！");
			result.put("result", 1113);
			return result;
		}

		int package_id = -1;
		String packagIdStr = StringUtils.trimToNull(package_info.getPackage_id());
		try
		{
			package_id = Integer.parseInt(packagIdStr);
		} catch (Exception e)
		{
			result.put("msg", "包裹ID有误！");
			result.put("result", 1113);
			return result;
		}
		com.xiangrui.lmp.business.admin.pkg.vo.Pkg pkg = packageService.queryPackageById(package_id);

		if (pkg == null)
		{
			result.put("msg", "原包裹不存在（请检查pakcage_id）！");
			result.put("result", 1113);
			return result;
		}

		if (!packageApiService.checkPackageCanEdit(package_id))
		{
			result.put("msg", "包裹不可以被编辑！");
			result.put("result", 1113);
			return result;
		}

		if ((!original_num.equals(pkg.getOriginal_num())) && !checkOriginalnumDeal(original_num))
		{
			result.put("msg", "关联单号不可用");
			result.put("result", 1115);
			return result;
		}
		


		pkg.setOriginal_num(original_num);
		String keepPrice = StringUtils.trimToNull(package_info.getSave_price());
		float keepPriceServiceFee = 0;
		if (keepPrice != null)
		{
			keepPriceServiceFee = Float.parseFloat(keepPrice);
		}
		if (pkg.getAddress_id() != receiver_info_int)
		{
			FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(pkg.getAddress_id());
			boolean needInsert = false;
			if (frontUserAddress == null)
			{
				frontUserAddress = new FrontUserAddress();
				needInsert = true;
			}
			
			// 根据address_id查询用户收货地址
			FrontReceiveAddress frontReceiveAddress = frontReceiveAddressService.queryReceiveAddressByAddressId(receiver_info_int);

			if (frontReceiveAddress == null)
			{
				result.put("msg", "收件地址有误！");
				result.put("result", 1104);
				return result;
			}

			// 更新包裹地址
			frontUserAddress.setUser_id(frontUser.getUser_id());
			frontUserAddress.setRegion(frontReceiveAddress.getRegion());
			frontUserAddress.setStreet(frontReceiveAddress.getStreet());
			frontUserAddress.setPostal_code(frontReceiveAddress.getPostal_code());
			frontUserAddress.setReceiver(frontReceiveAddress.getReceiver());
			frontUserAddress.setMobile(frontReceiveAddress.getMobile());
			frontUserAddress.setTel(frontReceiveAddress.getTel());
			frontUserAddress.setIsdefault(frontReceiveAddress.getIsdefault());
			frontUserAddress.setIdcard(frontReceiveAddress.getIdcard());
			frontUserAddress.setFront_img(frontReceiveAddress.getFront_img());
			frontUserAddress.setBack_img(frontReceiveAddress.getBack_img());
			frontUserAddress.setIdcard_status(frontReceiveAddress.getIdcard_status());
			frontUserAddress.setRefuse_reason(frontReceiveAddress.getRefuse_reason());

			if (needInsert)
			{
				frontUserAddressMapper.insertFrontUserAddress(frontUserAddress);
				pkg.setAddress_id(frontUserAddress.getAddress_id());
			}
			else
			{
				frontUserAddressService.updateUserAddressByReceiveAddress(frontUserAddress);
			}
		}
		
		
		pkg.setOsaddr_id(oversea_warehouse_int);

		if (frontUser.getUser_type() == 1)
		{
			// 资费费率
			Map<String, Object> ttparmas = new HashMap<String, Object>();
			ttparmas.put("integral_min", frontUser.getIntegral());
			ttparmas.put("rate_type", frontUser.getUser_type());

			FreightCost freightCost = freightCostService.queryFreightCostByIntegralMin(ttparmas);
			if (freightCost != null)
			{
				float price = freightCost.getUnit_price();
				pkg.setPrice(price);
			}
		}
		else
		{
			pkg.setPrice(0);
		}

		int pay_type = 1;
		try
		{
			pay_type = Integer.parseInt(StringUtils.trimToNull(package_info.getPay_type()));
		} catch (Exception e)
		{
			// TODO: handle exception
		}
		pkg.setPay_type(pay_type);
		String waitting_merge_flag = package_info.getNeed_merge() == null ? "N" : package_info.getNeed_merge();
		setWaittingMergePkgHandle(package_id, waitting_merge_flag);

		List<PkgAttachService> attachServices = new ArrayList<PkgAttachService>();
		if (null != package_info.getExtend_values())
		{
			float total_transport_cost = 0;
			for (int i = 0; i < package_info.getExtend_values().size(); i++)
			{
				boolean canAdd = true;
				int attach_id = Integer.parseInt(package_info.getExtend_values().get(i));
				PkgAttachService attachService = new PkgAttachService();
				attachService.setAttach_id(attach_id);
				attachService.setPackage_id(package_id);
				attachService.setStatus(ATTACH_SERVICE_STATUS_INIT);
				float attachServicePrice = getServicePrice(frontUser.getUser_id(), attach_id);
				if (attach_id == BaseAttachService.KEEP_PRICE_ID)
				{
					if (keepPriceServiceFee == 0)
					{
						canAdd = false;
					}
					float ddddd = 0.01f;
					attachServicePrice = keepPriceServiceFee * ddddd;
					attachService.setStatus(2);// 完成
				}
				// 将增值服务插入PkgAttachService
				attachService.setService_price(attachServicePrice);

				// 包裹创建，原始包裹package_group默认为当前包裹id
				attachService.setPackage_group(String.valueOf(attachService.getPackage_id()));
				if (canAdd)
				{
					attachServices.add(attachService);
					total_transport_cost += attachServicePrice;
				}

			}
			if (!waitting_merge_flag.equals("Y"))
			{
				/*
				 * // 资费费率 Map<String, Object> parmas = new HashMap<String,
				 * Object>(); parmas.put("integral_min",
				 * frontUser.getIntegral()); parmas.put("rate_type",
				 * frontUser.getUser_type());
				 * 
				 * FreightCost freightCost =
				 * freightCostService.queryFreightCostByIntegralMin(parmas);
				 * BigDecimal totalTransportCostBigDecimal = new
				 * BigDecimal(freightCost.getUnit_price()).multiply(new
				 * BigDecimal(total_transport_cost)); float totalTransportCost =
				 * totalTransportCostBigDecimal.floatValue(); // 转运总费用
				 * pkg.setTransport_cost(totalTransportCost);
				 */
				this.pkgAttachServiceService.addPkgAttachServiceOnList(attachServices);
			}
			pkg.setTransport_cost(total_transport_cost);
		}
		// 更新包裹信息
		packageMapper.update(pkg);

		for (PkgGoods pkgGoods : pkgGoodsList)
		{
			pkgGoods.setPackage_id(package_id);
		}

		// 清除原来物品
		frontPkgGoodsService.delPkgGoods(package_id);

		frontPkgGoodsService.addPkgGoods(pkgGoodsList);

		// 异常包裹没有选择待合箱服务时，编辑包裹时要让其在前台待处理栏上消失
		if ((!"Y".equals(StringUtils.trimToEmpty(pkg.getWaitting_merge_flag()))) && "Y".equals(StringUtils.trimToEmpty(pkg.getException_package_flag())))
		{
			// 根据address_id查询包裹地址
			if (StringUtils.trimToNull("" + pkg.getAddress_id()) != null)
			{
				FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(pkg.getAddress_id());
				// 客户在编辑包裹时没有勾选需等待合箱，且包裹内件数大于等于1，且包裹有选择收货地址，点击完成，那么包裹从前台页面的“待处理包裹”下消失
				if (frontUserAddress != null && pkgGoodsList.size() > 0)
				{
					packageService.updateExceptionPackageFlag(package_id, "N");
				}
			}
		}

		result.put("result", true);
		result.put("msg", "编辑包裹完成");
		result.put("isSuccess", Boolean.TRUE);
		return result;
	}

	private void setWaittingMergePkgHandle(int package_id, String waitting_merge_flag)
	{
		// 清除原来增值服务
		List<PkgAttachService> attachServiceList = pkgAttachServiceService.queryPkgAttachServiceByPackage(package_id);
		if (attachServiceList != null && attachServiceList.size() > 0)
		{
			for (PkgAttachService pkgAttachService : attachServiceList)
			{
				pkgAttachServiceService.pkgAttachServiceClear(pkgAttachService);
			}
		}
		if (waitting_merge_flag != null)
		{
			Pkg pkg = frontPkgService.queryPackageByPackageId(package_id);
			if (waitting_merge_flag.equals("Y"))
			{
				// 异常包裹选择待合箱服务时，要其在前台待处理栏上显示
				if ("N".equals(StringUtils.trimToEmpty(pkg.getException_package_flag())))
				{
					packageService.updateExceptionPackageFlag(package_id, "Y");
				}
			}
			packageService.updateWaittingMergePkg(package_id, waitting_merge_flag);
		}
	}

	/**
	 * 查看晒单详情
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/query_share_order", method = RequestMethod.GET)
	public Map<String, Object> query_share_order(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String idStr = request.getParameter("package_id");
		logger.info("查看晒单详情接口  id=" + idStr);
		int package_id = 0;
		try
		{
			package_id = Integer.parseInt(idStr);
		} catch (Exception e)
		{
			result.put("msg", "非法参数");
			result.put("result", 1101);
			return result;
		}

		try
		{
			PackageImg packageImg = new PackageImg();
			packageImg.setPackage_id(package_id);
			packageImg.setImg_type(PackageImg.IMG_SHARE_TYPE);
			List<PackageImg> shopImgList = frontPkgImgService.queryPkgImg(packageImg);

			Pkg pkg = frontPkgService.queryPackageByPackageId(package_id);

			FrontShare frontShare = frontShareService.queryPkgShare(package_id);
			String approvaled = "N";
			String title = "";
			String content = "";
			String share_link = "";
			if (null != frontShare)
			{
				title = StringUtils.trimToEmpty(frontShare.getTitle());
				content = StringUtils.trimToEmpty(frontShare.getContent());
				share_link = StringUtils.trimToEmpty(frontShare.getShare_link());
				if (frontShare.getApproval_status() == 2)
				{
					approvaled = "Y";
				}
			}
			List<HashMap<String, Object>> share_img_list = new ArrayList<HashMap<String, Object>>();
			if (shopImgList != null && shopImgList.size() > 0)
			{
				for (PackageImg tpackageImg : shopImgList)
				{
					HashMap<String, Object> map = new HashMap<String, Object>();
					map.put("img_id", tpackageImg.getImg_id());
					map.put("img_path", tpackageImg.getImg_path());
					share_img_list.add(map);
				}
			}
			result.put("title", title);
			result.put("content", content);
			result.put("share_link", share_link);
			result.put("approvaled", approvaled);
			result.put("share_img_list", share_img_list);
			result.put("logistics_code", pkg.getLogistics_code());

			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("查看晒单详情接口 出错！  id=" + idStr);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 生成/修改晒单
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add_or_update_share_order", method = RequestMethod.POST)
	public Map<String, Object> add_or_update_share_order(HttpServletRequest request, String title, String content, String share_link, String access_token)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String idStr = request.getParameter("package_id");
		String imgListStr = request.getParameter("img_list");
		List<String> imgList = new ArrayList<String>();
		logger.info("查看晒单详情接口  id=" + idStr);
		int package_id = 0;
		try
		{
			package_id = Integer.parseInt(idStr);
			imgList = JSONTool.getDTOStringList(imgListStr);
		} catch (Exception e)
		{
			result.put("msg", "非法参数");
			result.put("result", 1101);
			return result;
		}
		try
		{
			PackageImg packageImg = new PackageImg();
			packageImg.setPackage_id(package_id);
			packageImg.setImg_type(PackageImg.IMG_SHARE_TYPE);
			List<PackageImg> shopImgList = frontPkgImgService.queryPkgImg(packageImg);

			Pkg pkg = frontPkgService.queryPackageByPackageId(package_id);
			if (pkg == null)
			{
				result.put("msg", "包裹不存在，不允许晒单");
				result.put("result", 1101);
				return result;
			}

			FrontShare frontShare = frontShareService.queryPkgShare(package_id);
			if (null != frontShare)
			{
				if (frontShare.getApproval_status() == 2)
				{
					result.put("msg", "晒单已审核，不允许编辑");
					result.put("result", 1101);
					return result;
				}
				else
				{
					frontShare.setTitle(title);
					frontShare.setContent(content);
					frontShare.setShare_link(share_link);
					frontShareService.updatePkgShare(frontShare);
				}
			}
			else
			{
				// 获取系统当前时间
				frontShare = new FrontShare();
				Timestamp creatTime = new Timestamp(System.currentTimeMillis());
				frontShare.setUser_id(pkg.getUser_id());
				frontShare.setShare_time(creatTime);
				frontShare.setTitle(title);
				frontShare.setContent(content);
				frontShare.setShare_link(share_link);
				frontShare.setPackage_id(package_id);
				frontShare.setApproval_status(FrontShare.APPROVE_PROCESSING);
				frontShare.setStatus(FrontShare.SHARE_STATUS_NOGIVE);
				frontShareService.addShare(frontShare);
			}
			List<String> noNeedUploadList = new ArrayList<String>();
			List<PackageImg> noNeedDeleteShareImgList = new ArrayList<PackageImg>();
			if (imgList != null && imgList.size() > 0)
			{
				for (String imgUrl : imgList)
				{
					if (shopImgList != null && shopImgList.size() > 0)
					{
						for (PackageImg tpackageImg : shopImgList)
						{
							if (imgUrl.equals(tpackageImg.getImg_path()))
							{
								noNeedUploadList.add(imgUrl);
								noNeedDeleteShareImgList.add(tpackageImg);
							}
						}
					}
				}
			}
			// delete exist image
			if (shopImgList != null && shopImgList.size() > 0)
			{
				for (PackageImg tpackageImg : shopImgList)
				{
					if (!noNeedDeleteShareImgList.contains(tpackageImg))
					{
						frontPkgImgService.deletePkgImg(tpackageImg.getImg_id());
					}
				}
			}

			// upload new image
			if (imgList != null && imgList.size() > 0)
			{
				for (String imgUrl : imgList)
				{
					if (!noNeedUploadList.contains(imgUrl))
					{
						String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource";
						String img_pathrr = "/" + "upload" + "/";
						SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
						// 图片名称
						String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000);
						int flag = 0;
						String frontImage = null;
						if (imgUrl != null)
						{
							frontImage = WeiXinImageUtil.getWeiXinImageToLocal(ctxPath, img_pathrr + newFileName, access_token, imgUrl);
							if (frontImage != null)
							{
								// 数据库存储路径
								flag++;
								PackageImg tpackageImg = new PackageImg();
								tpackageImg.setImg_type(PackageImg.IMG_SHARE_TYPE);
								tpackageImg.setPackage_id(package_id);
								tpackageImg.setImg_path(frontImage);
								frontPkgImgService.insertPkgImg(tpackageImg);
							}
						}

						if (flag == 1)
						{
							result.put("msg", "添加成功！");
							result.put("result", 0);
							result.put("isSuccess", Boolean.TRUE);
						}
						else
						{
							result.put("msg", "从微信服务器中获取不到图片！");
							result.put("result", 1104);
							return result;
						}
					}
				}
			}
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("查看晒单详情接口 出错！  id=" + idStr);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}
}
