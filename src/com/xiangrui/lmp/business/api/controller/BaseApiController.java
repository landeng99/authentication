package com.xiangrui.lmp.business.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.article.service.ArticleService;
import com.xiangrui.lmp.business.admin.article.vo.Article;
import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;
import com.xiangrui.lmp.business.api.service.BaseApiService;
import com.xiangrui.lmp.business.api.service.PackageApiService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.constant.WebConstants;

/**
 * 其他公共基础相关接口
 * 
 * @author ljc
 */
@Controller
@RequestMapping(value = "/api")
public class BaseApiController
{
	private static final Logger logger = Logger.getLogger(BaseApiController.class);
	@Autowired
	private ArticleService articleService;
	@Autowired
	private PackageApiService packageApiService;
	@Autowired
	private FrontUserService ffrontUserService; // 前台用户
	@Autowired
	private BaseApiService baseApiService;

	private static Map<String, String> API_PHONE_SMSCODE_MAP = new ConcurrentHashMap<String, String>();

	public static String getSmsCodeByPhone(String phone)
	{
		String smsCode = null;
		if (API_PHONE_SMSCODE_MAP.containsKey(phone))
		{
			smsCode = API_PHONE_SMSCODE_MAP.get(phone);
		}
		return smsCode;
	}

	/**
	 * 获取验证码接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_sms_verification_code", method = RequestMethod.GET)
	public Map<String, Object> getSmsVerificationCode(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		String phone = request.getParameter("phone");
		logger.info("获取验证码接口  phone=" + phone);
		try
		{
			/*
			 * FrontUser frontUser =
			 * ffrontUserService.queryFrontUserByMobile(phone);
			 * if(frontUser!=null){ result.put("msg", "手机号码已注册");
			 * result.put("result", 1101); return result; }
			 */
			String smsCode = baseApiService.getSmsVerificationCode(phone);
			if (null != smsCode && !"".equals(smsCode))
			{
				HttpSession session = request.getSession();
				session.setAttribute("smsCode", smsCode);
				result.put("smsCode", smsCode);
				API_PHONE_SMSCODE_MAP.put(phone, smsCode);
				result.put("msg", "请求成功");
				result.put("result", 0);
				result.put("isSuccess", Boolean.TRUE);
			}
		} catch (Exception e)
		{
			logger.error("获取验证码接口  出错！phone=" + phone);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取添加包裹时申报类别接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_all_category", method = RequestMethod.GET)
	public Map<String, Object> getAllCategory(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		try
		{
			List<Map<String, Object>> list = baseApiService.getAllCategory();
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取添加包裹时申报类别接口  出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取添加包裹时增值服务列表接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_all_extend_values", method = RequestMethod.GET)
	public Map<String, Object> getAllExtendValues(HttpServletRequest request, String phone)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户");
				result.put("result", 1101);
				return result;
			}
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("user_type", frontUser.getUser_type());
			params.put("integral_min", frontUser.getIntegral());
			List<MemberRate> member_rateList = baseApiService.getMemberRate(params);
			if (member_rateList != null && member_rateList.size() > 0)
			{
				List<HashMap<String, Object>> list = baseApiService.getAllExtendValues(member_rateList.get(0).getRate_id());
				result.put("list", list);
				result.put("msg", "请求成功");
				result.put("result", 0);
				result.put("isSuccess", Boolean.TRUE);
			}
			else
			{
				result.put("msg", "会员信息为空");
				result.put("result", 0);
			}
		} catch (Exception e)
		{
			logger.error("获取添加包裹时申报类别接口  出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取添加包裹时保价与保费比例接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_cost_ratio", method = RequestMethod.GET)
	public Map<String, Object> getCostRatio(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		try
		{
			// Todo
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取添加包裹时保价与保费比例接口  出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取省份城市列表接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_all_citys", method = RequestMethod.GET)
	public Map<String, Object> getAllCitys(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		try
		{
			List<Map<String, Object>> list = baseApiService.getAllCitys();
			// System.out.println(JSONObject.toJSONString(list));
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取省份城市列表接口  出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取用户指南列表接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_user_document", method = RequestMethod.GET)
	public Map<String, Object> getUserDocument(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);

		try
		{
			List<HashMap<String, Object>> list = baseApiService.getUserDocument();
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取用户指南列表接口  出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取用户指南明细接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_user_document_detail", method = RequestMethod.GET)
	public Map<String, Object> getUserDocumentDetail(HttpServletRequest request)
	{

		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);
		String id = request.getParameter("id");
		try
		{
			Article article = articleService.queryArticle(Integer.parseInt(id));
			if (article != null)
			{
				String content = StringUtils.trimToNull(article.getContent());
				if (content != null)
				{
					content = StringUtils.replace(content, "src=\"", "src=\"" + WebConstants.FRONT_URL_PREFIX);
				}
				article.setContent(content);
				
				String title = StringUtils.trimToNull(article.getTitle());
				if (title != null)
				{
					title = StringUtils.replace(title, "src=\"", "src=\"" + WebConstants.FRONT_URL_PREFIX);
				}
				article.setTitle(title);
			}
			result.put("article", article);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取用户指南明细接口  出错！id=" + id);
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取全部海外仓地信息接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_all_overseas_warehouses", method = RequestMethod.GET)
	public Map<String, Object> getAllOverseasWarehouses(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);
		String phone = request.getParameter("phone");
		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户!");
				result.put("result", 1101);
				return result;
			}
			List<HashMap<String, Object>> list = baseApiService.getAllOverseasWarehouses(frontUser.getUser_id());
			result.put("list", list);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取全部海外仓地信息接口  出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取海外仓地信息明细接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/get_overseas_warehouses_detail", method = RequestMethod.GET)
	public Map<String, Object> getOverseasDeatil(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);
		String phone = request.getParameter("phone");
		String id = request.getParameter("id");
		try
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
			if (frontUser == null)
			{
				result.put("msg", "不存在该手机用户!");
				result.put("result", 1101);
				return result;
			}
			Map<String, Object> detail = baseApiService.getOverseasDeatil(Integer.parseInt(id));
			detail.put("firstName", frontUser.getFirst_name());
			detail.put("lastName", frontUser.getLast_name());
			result.put("detail", detail);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取海外仓地信息明细接口  出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取所有通知接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/notification_list", method = RequestMethod.GET)
	public Map<String, Object> getnotification_list(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);
		String phone = request.getParameter("phone");
		try
		{
//			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(phone);
//			if (frontUser == null)
//			{
//				result.put("msg", "不存在该手机用户!");
//				result.put("result", 1101);
//				return result;
//			}
			List<Map<String, Object>> noticeList = baseApiService.getAllNotice();
			result.put("noticeList", noticeList);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取所有通知接口  出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取通知明细接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/notification_details", method = RequestMethod.GET)
	public Map<String, Object> notification_details(HttpServletRequest request)
	{
		String msg = "请求失败";
		int resultCode = -1;
		Boolean isSuccess = Boolean.FALSE;
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("msg", msg);
		result.put("result", resultCode);
		result.put("isSuccess", isSuccess);
		String id = request.getParameter("id");
		try
		{
			Map<String, Object> detail = baseApiService.getNoticeDeatil(Integer.parseInt(id));
			result.put("detail", detail);
			result.put("msg", "请求成功");
			result.put("result", 0);
			result.put("isSuccess", Boolean.TRUE);
		} catch (Exception e)
		{
			logger.error("获取通知明细接口 出错！  ");
			result.put("msg", "数据库读取失败");
			result.put("result", 1202);
			e.printStackTrace();
		}
		return result;
	}
}
