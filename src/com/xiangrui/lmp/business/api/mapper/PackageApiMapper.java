package com.xiangrui.lmp.business.api.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.api.vo.QueryHistroy;

public interface PackageApiMapper {

	List<HashMap<String,Object>> queryAllPackage(int userId);
	
	List<Map<String,Object>> queryPackageAddr(Map<String,Object> map);
	
	List<Map<String,Object>> getPackageByFilter(Map<String,Object> map);

	List<Map<String, Object>> getAllPackageByCondition(Map<String, Object> paramMap);
	
	int insertHistroy(QueryHistroy queryHistroy);

	List<Map<String, Object>> queryHistory(String open_id);
	
	QueryHistroy  queryHistroyByNo (Map<String,Object> map);
	
	int updateHistroy(int id);
	
	List<Map<String, Object>> queryPkgGoods(int packageId);
	
	List<Map<String, Object>> queryPkgAttachService(int packageId);
	
	Map<String, Object> queryPkgReceiveAddress(int packageId);
	/**
	 * 查询可合并包裹
	 * @param map
	 * @return
	 */
	List<Map<String,Object>> queryAvailableMergePackageList(Map<String,Object> paramMap);
	/**
	 * 按照包裹收件人的手机号查询到属于这个收件人的包裹列表
	 * @param moblie
	 * @return
	 */
	List<HashMap<String,Object>> queryAllPackageByReceiverMoblie(String mobile);

	/**
	 * 根据运单号查询收件人手机号码接口
	 * @param order_no
	 * @return
	 */
	String getPhone(String order_no);

	/**
	 * 公司单号/关联单号/收件人姓名/收件人手机号查询包裹
	 * @param paramMap
	 * @return
	 */
	List<Map<String, Object>> getPackages(Map<String, Object> paramMap);
}
