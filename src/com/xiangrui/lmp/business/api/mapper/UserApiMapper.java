package com.xiangrui.lmp.business.api.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.frontuser.vo.ReceiveAddress;

public interface UserApiMapper {

	List<HashMap<String,Object>> queryAllShare(int userId);
	
	List<HashMap<String,Object>> getConsumeDetail(int userId);
	
	List<HashMap<String,Object>> getRechargeDetail(int userId);

	List<HashMap<String, Object>> getAllAddresses(int userId);
	
	HashMap<String, Object> getAddressDetail(Map<String, Object> map);

	int insertAddress(ReceiveAddress address);
	
	int updateAddress(ReceiveAddress address);

	int deleteUserAddress(int addressId);

	List<HashMap<String, Object>> getAllCoupons(int userId);

	List<Map<String, Object>> getAddressBy(Map<String, Object> paramMap);

	int updateVirtualPayFlag(Map<String, Object> paramMap);
	
	int updateDefaultPayType(Map<String, Object> paramMap);

	ReceiveAddress getAddressByAddressId(int addressId);

	int reSetDefaultAddress(Map<String, Object> paramMap);

	int updateDefaultAddress(Map<String, Object> paramMap);
}
