package com.xiangrui.lmp.business.api.bean;

import java.util.List;

public class SplitBoxInfo
{
	private List<SplitBoxGoodInfo> goods_list;
	private String receiver_info;
	public List<SplitBoxGoodInfo> getGoods_list()
	{
		return goods_list;
	}
	public void setGoods_list(List<SplitBoxGoodInfo> goods_list)
	{
		this.goods_list = goods_list;
	}
	public String getReceiver_info()
	{
		return receiver_info;
	}
	public void setReceiver_info(String receiver_info)
	{
		this.receiver_info = receiver_info;
	}
	
}
