package com.xiangrui.lmp.business.api.bean;

import java.util.List;

public class APIPackageInfo
{
	private String package_id;
	private String ref_orderno;
	private List<APIGoods> goods;
	private List<String> extend_values;
	private String save_price;
	private String save_cost;
	private String need_merge;
	private String pay_type;
	private String virtual_pay_flag;

	public String getPackage_id()
	{
		return package_id;
	}

	public void setPackage_id(String package_id)
	{
		this.package_id = package_id;
	}

	public String getRef_orderno()
	{
		return ref_orderno;
	}

	public void setRef_orderno(String ref_orderno)
	{
		this.ref_orderno = ref_orderno;
	}

	public List<APIGoods> getGoods()
	{
		return goods;
	}

	public void setGoods(List<APIGoods> goods)
	{
		this.goods = goods;
	}

	public List<String> getExtend_values()
	{
		return extend_values;
	}

	public void setExtend_values(List<String> extend_values)
	{
		this.extend_values = extend_values;
	}

	public String getSave_price()
	{
		return save_price;
	}

	public void setSave_price(String save_price)
	{
		this.save_price = save_price;
	}

	public String getSave_cost()
	{
		return save_cost;
	}

	public void setSave_cost(String save_cost)
	{
		this.save_cost = save_cost;
	}

	public String getNeed_merge()
	{
		return need_merge;
	}

	public void setNeed_merge(String need_merge)
	{
		this.need_merge = need_merge;
	}

	public String getPay_type()
	{
		return pay_type;
	}

	public void setPay_type(String pay_type)
	{
		this.pay_type = pay_type;
	}

	public String getVirtual_pay_flag()
	{
		return virtual_pay_flag;
	}

	public void setVirtual_pay_flag(String virtual_pay_flag)
	{
		this.virtual_pay_flag = virtual_pay_flag;
	}
}
