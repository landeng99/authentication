package com.xiangrui.lmp.business.api.bean;

public class SplitBoxGoodInfo
{
	private String goods_id;
	private String total;

	public String getGoods_id()
	{
		return goods_id;
	}

	public void setGoods_id(String goods_id)
	{
		this.goods_id = goods_id;
	}

	public String getTotal()
	{
		return total;
	}

	public void setTotal(String total)
	{
		this.total = total;
	}

}
