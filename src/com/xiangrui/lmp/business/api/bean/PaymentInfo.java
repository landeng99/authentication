package com.xiangrui.lmp.business.api.bean;

public class PaymentInfo
{
	private String package_id;
	private String package_money;

	public String getPackage_id()
	{
		return package_id;
	}

	public void setPackage_id(String package_id)
	{
		this.package_id = package_id;
	}

	public String getPackage_money()
	{
		return package_money;
	}

	public void setPackage_money(String package_money)
	{
		this.package_money = package_money;
	}

}
