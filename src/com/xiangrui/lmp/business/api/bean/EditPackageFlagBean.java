package com.xiangrui.lmp.business.api.bean;

public class EditPackageFlagBean
{
	private String is_handle;
	private String is_edit;

	public String getIs_handle()
	{
		return is_handle;
	}

	public void setIs_handle(String is_handle)
	{
		this.is_handle = is_handle;
	}

	public String getIs_edit()
	{
		return is_edit;
	}

	public void setIs_edit(String is_edit)
	{
		this.is_edit = is_edit;
	}

}
