package com.xiangrui.lmp.business.api.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.admin.pkglog.service.PkgLogService;
import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;
import com.xiangrui.lmp.business.api.bean.EditPackageFlagBean;
import com.xiangrui.lmp.business.api.mapper.PackageApiMapper;
import com.xiangrui.lmp.business.api.service.PackageApiService;
import com.xiangrui.lmp.business.api.vo.ExpressContent;
import com.xiangrui.lmp.business.api.vo.ExpressDetail;
import com.xiangrui.lmp.business.api.vo.FilterCondition;
import com.xiangrui.lmp.business.api.vo.FilterItems;
import com.xiangrui.lmp.business.api.vo.Filters;
import com.xiangrui.lmp.business.api.vo.QueryHistroy;
import com.xiangrui.lmp.business.api.vo.Status;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgMapper;
import com.xiangrui.lmp.business.homepage.service.FrontUserAddressService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.business.kuaidi100.service.ExpressInfoService;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.bean.PkgLogExpress;

/**
 * 包裹相关的接口
 * 
 * @author liangjincai
 *
 */
@Service(value = "packageApiService")
public class PackageApiServiceImp implements PackageApiService
{

	@Autowired
	private PkgLogService pkgLogService;// 包裹日志
	@Autowired
	private PkgService frontPkgService; // 前台包裹
	@Autowired
	private ExpressInfoService expressInfoService;
	@Autowired
	private PackageApiMapper packageApiMapper;
	@Autowired
	private FrontPkgMapper frontPkgMapper;
	@Autowired
	private PackageMapper packageMapper;

	@Autowired
	private PkgAttachServiceMapper pkgAttachServiceMapper;
	@Autowired
	private FrontUserAddressService frontUserAddressService;
	@Autowired
	private PkgGoodsService frontPkgGoodsService;

	/**
	 * 获取所有包裹列表
	 */
	@Override
	public List<HashMap<String, Object>> queryAllPackage(int userId)
	{
		List<HashMap<String, Object>> list = packageApiMapper.queryAllPackage(userId);
		if (list != null && list.size() > 0)
		{
			for (HashMap<String, Object> obj : list)
			{
				Integer status = (Integer) obj.get("status");
				obj.put("status", Status.getName(status));
				Integer package_id = (Integer) obj.get("package_id");
				String canRemove = "N";
				if (checkPackageCanRemove(package_id))
				{
					canRemove = "Y";
				}
				obj.put("canRemove", canRemove);
			}
		}
		return list;
	}

	/**
	 * 根据package_id查询包裹
	 * 
	 * @param package_id
	 * @return List
	 */
	@Override
	public Map<String, Object> queryPackageByPackageId(int package_id)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		Pkg pkg = frontPkgMapper.queryPackageByPackageId(package_id);
		List<Map<String, Object>> goods = packageApiMapper.queryPkgGoods(package_id);
		List<Map<String, Object>> attachServices = packageApiMapper.queryPkgAttachService(package_id);
		Map<String, Object> receiveInfo = packageApiMapper.queryPkgReceiveAddress(package_id);
		if (receiveInfo != null)
		{
			String street = (String) receiveInfo.get("street");
			String region = (String) receiveInfo.get("region");
			String area = "";
			if (StringUtils.trimToNull(region) != null)
			{
				JSONArray regionArray = JSONArray.fromObject(region);
				if (regionArray != null)
				{
					Object[] regionObjs = (Object[]) regionArray.toArray();
					for (Object reg : regionObjs)
					{
						JSONObject regMap = (JSONObject) reg;
						area = area + regMap.get("name");
					}
				}
				receiveInfo.put("address", area + street);
				receiveInfo.remove("region");
				receiveInfo.remove("street");
				String frontImg = "" + receiveInfo.get("idcardImg");
				if ("0".equals(frontImg))
				{
					receiveInfo.put("idcardImg", "否");
				}
				else
				{
					receiveInfo.put("idcardImg", "是");
				}
			}
		}
		if (pkg != null)
		{
			Map<String, Object> subparams = new HashMap<String, Object>();
			subparams.put("package_id", pkg.getPackage_id());
			String spec = ",";

			// 查询包裹是否有分箱服务
			subparams.put("attach_id", AttachService.SPLIT_PKG_ID);
			List<PkgAttachServiceGroup> splitlist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(subparams);
			String old_original_num = "";
			if (splitlist != null && splitlist.size() > 0)
			{
				for (PkgAttachServiceGroup serviceGroup : splitlist)
				{
					String package_id_group = serviceGroup.getOg_package_id_group();
					com.xiangrui.lmp.business.admin.pkg.vo.Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(package_id_group));
					old_original_num = childpkg.getOriginal_num();
				}
			}

			if (pkg.getArrive_time() != null)
			{
				pkg.setArrive_time_formatStr(DateUtil.getStringTemestamp(pkg.getArrive_time(), "yyyy-MM-dd HH:mm:ss"));
			}

			// 查询包裹是否有合箱服务
			subparams.put("attach_id", AttachService.MERGE_PKG_ID);
			List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(subparams);
			if (mergelist != null && mergelist.size() > 0)
			{
				for (PkgAttachServiceGroup serviceGroup : mergelist)
				{
					String package_id_group = serviceGroup.getOg_package_id_group();
					String[] package_ids = package_id_group.split(spec);
					for (String id : package_ids)
					{
						com.xiangrui.lmp.business.admin.pkg.vo.Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(id));
						old_original_num += childpkg.getOriginal_num() + ",";
					}
				}
			}
			pkg.setOld_original_num(old_original_num);
		}
		BigDecimal totalAttachServiceFee = new BigDecimal(0);
		String canSlipt = "N";
		String canMerge = "N";
		if (pkg!=null&&pkg.getStatus() == 0)
		{
			canSlipt = "Y";
			canMerge = "Y";
		}
		if (attachServices != null && attachServices.size() > 0)
		{
			for (Map<String, Object> map : attachServices)
			{
				String str = "" + map.get("price");
				totalAttachServiceFee = totalAttachServiceFee.add(new BigDecimal(str));
				String attach_idStr = "" + map.get("attach_id");
				String statusStr = "" + map.get("status");
				int attach_id = Integer.parseInt(attach_idStr);
				int status = Integer.parseInt(statusStr);
				String description = (String) map.get("description");
				// 产生分箱,合箱增值服务
				if (attach_id == BaseAttachService.MERGE_PKG_ID || attach_id == BaseAttachService.SPLIT_PKG_ID)
				{
					canSlipt = "N";
					canMerge = "N";
					String service_status = null;
					if (status == 1)
					{
						service_status = "待" + description;
					}
					else
					{
						service_status = "已" + description;
					}
					pkg.setService_status(service_status);
				}
			}
		}
		if (goods == null)
		{
			canSlipt = "N";
		}
		else
		{
			int goodCount = 0;
			for (Map<String, Object> good : goods)
			{
				String quantity = "" + good.get("quantity");
				goodCount += Integer.parseInt(quantity);
			}
			if (goodCount < 1)
			{
				canSlipt = "N";
			}
		}

		String canRemove = "N";
		String is_edit = "否";
		if (checkPackageCanRemove(package_id))
		{
			canRemove = "Y";
		}
		if (checkPackageCanEdit(package_id))
		{
			is_edit = "是";
		}
		//a.arrive_status=1 and a.exception_package_flag='Y'
		String is_handle="否";
		if(pkg!=null)
		{
			if(pkg.getEnable()==1&&pkg.getArrive_status()==1&&"Y".equals(pkg.getException_package_flag()))
			{
				is_handle="是";
			}
		}
		EditPackageFlagBean edit_package =new EditPackageFlagBean();
		edit_package.setIs_edit(is_edit);
		edit_package.setIs_handle(is_handle);
		result.put("edit_package", edit_package);
		
		result.put("pkg", pkg);
		result.put("goods", goods);
		result.put("attachServices", attachServices);
		result.put("receiveInfo", receiveInfo);
		result.put("totalAttachServiceFee", NumberUtils.scaleMoneyDataFloat(totalAttachServiceFee.floatValue()));
		result.put("canSlipt", canSlipt);
		result.put("canMerge", canMerge);
		result.put("canRemove", canRemove);
		return result;
	}

	public boolean checkPackageCanRemove(int package_id)
	{
		boolean canRemove = false;
		Pkg pkg = frontPkgMapper.queryPackageByPackageId(package_id);
		if (pkg != null)
		{
			if (pkg.getStatus() == BasePackage.LOGISTICS_STORE_WAITING&&pkg.getArrive_status()==0)
			{
				List<Map<String, Object>> attachServices = packageApiMapper.queryPkgAttachService(package_id);
				if (attachServices != null && attachServices.size() > 0)
				{
					canRemove = true;
					for (Map<String, Object> map : attachServices)
					{
						String statusStr = "" + map.get("status");
						String attach_id=""+map.get("attach_id");
						if ((!attach_id.equals("32"))&&"2".equals(statusStr))
						{
							canRemove = false;
						}
					}
				}
				else
				{
					canRemove = true;
				}
			}
		}
		return canRemove;
	}

	public boolean checkPackageCanEdit(int package_id)
	{
		boolean canEdit = false;
		Pkg pkg = frontPkgMapper.queryPackageByPackageId(package_id);
		if (pkg != null)
		{
			if (pkg.getStatus() == BasePackage.LOGISTICS_STORE_WAITING)
			{
				List<Map<String, Object>> attachServices = packageApiMapper.queryPkgAttachService(package_id);
				if (attachServices != null && attachServices.size() > 0)
				{
					canEdit = true;
					for (Map<String, Object> map : attachServices)
					{
						String statusStr = "" + map.get("status");
						String attach_id=""+map.get("attach_id");
						if ((!attach_id.equals("32"))&&"2".equals(statusStr))
						{
							canEdit = false;
						}
					}
				}
				else
				{
					canEdit = true;
				}
			}
		}
		return canEdit;
	}
	
	/**
	 * 运单查询接口
	 */
	@Override
	public List<ExpressDetail> queryOrderByNo(String openId, String order_no)
	{

		List<ExpressDetail> list = new ArrayList<ExpressDetail>();
		String logistics_code = order_no.trim();
		Pkg pkgObj = new Pkg();
		List<PkgLog> pkgLogs = new ArrayList<PkgLog>();
		// 通过运单号匹配包裹
		List<Pkg> pkgs = new ArrayList<Pkg>();
		// 公司运单号
		pkgs = this.frontPkgService.queryPackageByLogisticsCode(logistics_code);
		if (null != pkgs && pkgs.size() > 0)
		{
			pkgObj = pkgs.get(0);
			// 读取包裹的操作日志
			pkgLogs = this.pkgLogService.queryPkgLogByPackageId(pkgObj.getPackage_id());
		}
		// 匹配快件发送返回包裹状态信息,查询是id倒序
		List<ExpressInfo> expressInfos = expressInfoService.queryExpressInfoByCode(pkgObj.getLogistics_code(), pkgObj.getEms_code());
		if (null != expressInfos && expressInfos.size() > 0)
		{
			// 仅仅获取最近的一次发送返回信息,集合第一条
			ExpressInfo expressInfo = expressInfos.get(0);
			// 快递100报文数据
			String data = expressInfo.getData();
			if (null != data && !"".equals(data))
			{
				Object[] pkgLogExpresses = JSONTool.getDTOArray(data, PkgLogExpress.class, null);
				int i = 0;
				for (Object pkgLogExpress : pkgLogExpresses)
				{
					PkgLogExpress express = (PkgLogExpress) pkgLogExpress;
					ExpressDetail detailInfo = new ExpressDetail(express.getFtime(), express.getContext());
					// 主动创建一个包裹已签收的显示数据,仅仅一条
					if (i == 0)
					{
						// 已签收
						if (ExpressInfo.ISCHECK_YES.equals(expressInfo.getIscheck()))
						{
							ExpressDetail detailInfo2 = new ExpressDetail(express.getFtime(), "包裹已签收");
							list.add(detailInfo2);
						}
					}
					i++;
					list.add(detailInfo);
				}
			}
		}
		// 是否有快递信息
		boolean isHasExpressInfo = (list.size() > 0);
		// 拼接包裹操作记录的页面显示数据
		for (PkgLog pkgLog : pkgLogs)
		{
			// 判定某个状态是否为废弃状态
			boolean isDiscard = (PackageLogUtil.STATUS_DESC_NO.equals(pkgLog.getDescription()));
			// 废弃则跳出
			if (isDiscard)
			{
				continue;
			}
			// (有快递信息)
			if (isHasExpressInfo)
			{
				// (签收信息不为已废弃&&不为签收状态)
				if (pkgLog.getOperated_status() != BasePackage.LOGISTICS_SIGN_IN)
				{
					String time = pkgLog.getOperate_time_toStr();
					String context = pkgLog.getDescription();
					ExpressDetail pkgLogExprEntity = new ExpressDetail(time, context);
					list.add(pkgLogExprEntity);
				}
			}
			else
			{
				String time = pkgLog.getOperate_time_toStr();
				String context = pkgLog.getDescription();
				ExpressDetail pkgLogExprEntity = new ExpressDetail(time, context);
				list.add(pkgLogExprEntity);
			}
		}
		// 保存查单历史
		if (openId!=null)
		{
			saveHistory(openId, order_no);
		}
		return list;
	}

	/**
	 * 根据运单号查询物流信息追踪接口
	 * 
	 * @param order_no
	 * @return
	 */
	public List<ExpressContent> getLogisticsInfoByOrderNo(String order_no)
	{
		List<ExpressContent> list = new ArrayList<ExpressContent>();
		String logistics_code = order_no.trim();
		Pkg pkgObj = new Pkg();
		List<PkgLog> pkgLogs = new ArrayList<PkgLog>();
		// 通过运单号匹配包裹
		List<Pkg> pkgs = new ArrayList<Pkg>();
		// 公司运单号
		pkgs = this.frontPkgService.queryPackageByLogisticsCode(logistics_code);
		if (null != pkgs && pkgs.size() > 0)
		{
			pkgObj = pkgs.get(0);
			// 读取包裹的操作日志
			pkgLogs = this.pkgLogService.queryPkgLogByPackageId(pkgObj.getPackage_id());
		}
		// 匹配快件发送返回包裹状态信息,查询是id倒序
		List<ExpressInfo> expressInfos = expressInfoService.queryExpressInfoByCode(pkgObj.getLogistics_code(), pkgObj.getEms_code());
		if (null != expressInfos && expressInfos.size() > 0)
		{
			// 仅仅获取最近的一次发送返回信息,集合第一条
			ExpressInfo expressInfo = expressInfos.get(0);
			// 快递100报文数据
			String data = expressInfo.getData();
			if (null != data && !"".equals(data))
			{
				Object[] pkgLogExpresses = JSONTool.getDTOArray(data, PkgLogExpress.class, null);
				int i = 0;
				for (Object pkgLogExpress : pkgLogExpresses)
				{
					PkgLogExpress express = (PkgLogExpress) pkgLogExpress;
					ExpressContent detailInfo = new ExpressContent(express.getFtime(), express.getContext());
					// 主动创建一个包裹已签收的显示数据,仅仅一条
					if (i == 0)
					{
						// 已签收
						if (ExpressInfo.ISCHECK_YES.equals(expressInfo.getIscheck()))
						{
							ExpressContent detailInfo2 = new ExpressContent(express.getFtime(), "包裹已签收");
							list.add(detailInfo2);
						}
					}
					i++;
					list.add(detailInfo);
				}
			}
		}
		// 是否有快递信息
		boolean isHasExpressInfo = (list.size() > 0);
		// 拼接包裹操作记录的页面显示数据
		for (PkgLog pkgLog : pkgLogs)
		{
			// 判定某个状态是否为废弃状态
			boolean isDiscard = (PackageLogUtil.STATUS_DESC_NO.equals(pkgLog.getDescription()));
			// 废弃则跳出
			if (isDiscard)
			{
				continue;
			}
			// (有快递信息)
			if (isHasExpressInfo)
			{
				// (签收信息不为已废弃&&不为签收状态)
				if (pkgLog.getOperated_status() != BasePackage.LOGISTICS_SIGN_IN)
				{
					String time = pkgLog.getOperate_time_toStr();
					String context = pkgLog.getDescription();
					ExpressContent pkgLogExprEntity = new ExpressContent(time, context);
					list.add(pkgLogExprEntity);
				}
			}
			else
			{
				String time = pkgLog.getOperate_time_toStr();
				String context = pkgLog.getDescription();
				ExpressContent pkgLogExprEntity = new ExpressContent(time, context);
				list.add(pkgLogExprEntity);
			}
		}
		return list;
	}

	/**
	 * 查询历史
	 * 
	 * @param userId
	 * @return
	 */
	public void saveHistory(String openId, String order_no)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("open_id", openId);
		paramMap.put("orderNo", order_no);
		// 先查询是否存在，如果存在再更新查询时间，否则添加查询历史
		QueryHistroy historyTemp = packageApiMapper.queryHistroyByNo(paramMap);
		if (historyTemp != null)
		{
			packageApiMapper.updateHistroy(historyTemp.getId());
		}
		else
		{
			List<Map<String, Object>> list = packageApiMapper.queryPackageAddr(paramMap);
			if (list != null && list.size() > 0)
			{
				for (Map<String, Object> obj : list)
				{
					String receiveAddress = (String) obj.get("receive_address");
					String orderNo = (String) obj.get("order_no");
					String sendAddress = (String) obj.get("send_address");
					JSONArray regionArray = JSONArray.fromObject(receiveAddress);
					if (regionArray != null)
					{
						Object[] regionObjs = (Object[]) regionArray.toArray();
						if (regionObjs.length >= 2)
						{
							JSONObject regMap = (JSONObject) regionObjs[1];// 获取市级
							receiveAddress = (String) regMap.get("name");
							QueryHistroy history = new QueryHistroy();
							history.setOpen_id(openId);
							history.setOrder_no(orderNo);
							history.setQuery_time(new Date());
							history.setReceive_address(receiveAddress);
							history.setSend_address(sendAddress);
							packageApiMapper.insertHistroy(history);
						}
					}
				}
			}

		}
	}

	/**
	 * 获取包裹筛选条件接口
	 */
	@Override
	public List<FilterCondition> getPackageFilterCondition()
	{
		List<FilterCondition> list = new ArrayList<FilterCondition>();
		FilterCondition status = new FilterCondition();
		status.setFilterName("包裹状态");
		status.setFilterKey("1");
		List<FilterItems> statusList = new ArrayList<FilterItems>();
		statusList.add(new FilterItems("-1", "异常"));
		statusList.add(new FilterItems("0", "待入库"));
		statusList.add(new FilterItems("1", "已入库"));
		statusList.add(new FilterItems("2", "待发货"));
		statusList.add(new FilterItems("3", "已出库"));
		statusList.add(new FilterItems("4", "空运中"));
		statusList.add(new FilterItems("5", "待清关"));
		// statusList.add(new FilterItems("6","清关中"));
		statusList.add(new FilterItems("7", "已清关派件中"));
		// statusList.add(new FilterItems("8", "派件中"));
		statusList.add(new FilterItems("9", "已收货"));
		statusList.add(new FilterItems("20", "废弃"));
		statusList.add(new FilterItems("21", "退货"));
		status.setFilterItems(statusList);
		list.add(status);

		FilterCondition pay = new FilterCondition();
		pay.setFilterName("支付状态");
		pay.setFilterKey("2");
		List<FilterItems> payList = new ArrayList<FilterItems>();
		payList.add(new FilterItems("1", "未支付"));
		payList.add(new FilterItems("2", "已支付"));
		payList.add(new FilterItems("3", "关税未支付"));
		payList.add(new FilterItems("4", "关税已支付"));
		payList.add(new FilterItems("5", "已支付（预扣）"));
		pay.setFilterItems(payList);
		list.add(pay);

		FilterCondition time = new FilterCondition();
		time.setFilterName("时间查询");
		time.setFilterKey("3");
		List<FilterItems> timeList = new ArrayList<FilterItems>();
		timeList.add(new FilterItems("7", "最近一周"));
		timeList.add(new FilterItems("30", "最近一个月"));
		timeList.add(new FilterItems("", "全部时间"));
		time.setFilterItems(timeList);
		list.add(time);

		FilterCondition pkgArriveStatus = new FilterCondition();
		pkgArriveStatus.setFilterName("包裹到库状态");
		pkgArriveStatus.setFilterKey("4");
		List<FilterItems> pkgArriveStatusList = new ArrayList<FilterItems>();
		pkgArriveStatusList.add(new FilterItems("0", "未到库"));
		pkgArriveStatusList.add(new FilterItems("1", "已到库"));
		pkgArriveStatus.setFilterItems(pkgArriveStatusList);
		list.add(pkgArriveStatus);

		return list;
	}

	/**
	 * 根据各种条件筛选包裹的接口
	 */
	@Override
	public List<Map<String, Object>> getPackageByFilter(int userId, String filters)
	{

		JSONArray filterArray = JSONArray.fromObject(filters);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		if (filterArray != null)
		{
			Filters[] FiltersArray = (Filters[]) JSONArray.toArray(filterArray, Filters.class);
			for (Filters filter : FiltersArray)
			{
				// 根据包裹状态查询
				if ("1".equals(filter.getFilterKey()))
				{
					String status = "";
					String statusValue = filter.getValues();
					String[] statusTemp = statusValue.split(",");
					if (statusTemp != null && statusTemp.length > 0)
					{
						for (String s : statusTemp)
						{
							status = status + ",'" + s + "'";
						}
						status = status.substring(1);
					}
					paramMap.put("status", status);
				}
				// 根据支付状态查询
				if ("2".equals(filter.getFilterKey()))
				{
					String payStatus = "";
					String payStatusValue = filter.getValues();
					String[] payStatusTemp = payStatusValue.split(",");
					if (payStatusTemp != null && payStatusTemp.length > 0)
					{
						for (String s : payStatusTemp)
						{
							payStatus = payStatus + ",'" + s + "'";
						}
						payStatus = payStatus.substring(1);
					}
					paramMap.put("payStatus", payStatus);
				}
				// 根据时间查询
				if ("3".equals(filter.getFilterKey()))
				{
					String days = filter.getValues();
					if (days != null && !"".equals(days))
					{
						paramMap.put("days", Integer.parseInt(days));
					}
				}
				// 根据包裹到库状态查询
				if ("4".equals(filter.getFilterKey()))
				{
					String pkgArriveStatus = filter.getValues();
					if (pkgArriveStatus != null && !"".equals(pkgArriveStatus))
					{
						paramMap.put("pkgArriveStatus", pkgArriveStatus);
					}
				}
			}
		}
		List<Map<String, Object>> list = packageApiMapper.getPackageByFilter(paramMap);
		if (list != null && list.size() > 0)
		{
			for (Map<String, Object> obj : list)
			{
				Integer status = (Integer) obj.get("status");
				obj.put("status", Status.getName(status));
				Integer package_id = (Integer) obj.get("package_id");
				String canRemove = "N";
				if (checkPackageCanRemove(package_id))
				{
					canRemove = "Y";
				}
				obj.put("canRemove", canRemove);
			}
		}
		return list;
	}

	/**
	 * 指定条件查询包裹接口
	 */
	@Override
	public List<Map<String, Object>> getAllPackageByCondition(int userId, String condition)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("condition", condition);
		List<Map<String, Object>> list = packageApiMapper.getAllPackageByCondition(paramMap);
		return list;
	}

	/**
	 * 查询历史
	 */
	@Override
	public List<Map<String, Object>> queryHistory(String open_id)
	{
		return packageApiMapper.queryHistory(open_id);
	}

	/**
	 * 查询可合并包裹
	 * 
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> queryAvailableMergePackageList(Map<String, Object> map)
	{
		List<Map<String, Object>> list= packageApiMapper.queryAvailableMergePackageList(map);
		List<Map<String, Object>> returnList = new ArrayList<Map<String,Object>>();
		if(list!=null&&list.size()>0)
		{
			for(Map<String, Object> tmap:list)
			{
				String package_id=""+tmap.get("package_id");
				int package_id_int=Integer.parseInt(package_id);
				if(checkPackageAllReady(package_id_int))
				{
					returnList.add(tmap);
				}
			}
		}
		return returnList;
	}

	/**
	 * 检查包裹是否可以合箱
	 * @param packageId
	 * @return
	 */
	public boolean checkPackageAllReady(int packageId)
	{
		boolean isReady=false;
		Pkg tempPkg = frontPkgService.queryPackageByPackageId(packageId);
		if (tempPkg != null)
		{
			FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(tempPkg.getAddress_id());
			// 客户在编辑包裹时包裹内件数大于等于1，且包裹有选择收货地址，点击完成，那么包裹从前台页面的“待处理包裹”下消失
			// 包裹的商品
			List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(tempPkg.getPackage_id());
			// 客户在编辑包裹时包裹内件数大于等于1，且包裹有选择收货地址，点击完成，那么包裹从前台页面的“待处理包裹”下消失
			if (frontUserAddress != null && gooldList != null && gooldList.size() > 0)
			{
				isReady = true;
			}
		}
		return isReady;
	}
	/**
	 * 按照包裹收件人的手机号查询到属于这个收件人的包裹列表
	 * 
	 * @param moblie
	 * @return
	 */
	public List<HashMap<String, Object>> queryAllPackageByReceiverMoblie(String mobile)
	{
		List<HashMap<String, Object>> list = packageApiMapper.queryAllPackageByReceiverMoblie(mobile);
		if (list != null && list.size() > 0)
		{
			for (HashMap<String, Object> obj : list)
			{
				Integer status = (Integer) obj.get("status");
				obj.put("status", Status.getName(status));
				Integer package_id = (Integer) obj.get("package_id");
				String canRemove = "N";
				if (checkPackageCanRemove(package_id))
				{
					canRemove = "Y";
				}
				obj.put("canRemove", canRemove);
			}
		}
		return list;
	}

	/**
	 * 根据运单号查询收件人手机号码接口
	 */
	@Override
	public String getPhone(String order_no)
	{
		return packageApiMapper.getPhone(order_no);
	}

	/**
	 * 公司单号/关联单号/收件人姓名/收件人手机号查询包裹
	 */
	@Override
	public List<Map<String, Object>> getPackages(Map<String, Object> paramMap)
	{
		List<Map<String, Object>> list = packageApiMapper.getPackages(paramMap);
		if (list != null && list.size() > 0)
		{
			for (Map<String, Object> obj : list)
			{
				Integer status = (Integer) obj.get("status");
				obj.put("status", Status.getName(status));
				Integer package_id = (Integer) obj.get("package_id");
				String canRemove = "N";
				if (checkPackageCanRemove(package_id))
				{
					canRemove = "Y";
				}
				obj.put("canRemove", canRemove);
			}
		}
		return list;
	}

	/**
	 * 删除包裹
	 * 
	 * @param package_id
	 * @return
	 */
	public boolean removePackage(int package_id)
	{
		boolean isRemove = false;
		if (checkPackageCanRemove(package_id))
		{
			frontPkgService.deleteAllPkgByPackageId(package_id, false);
			isRemove = true;
		}
		return isRemove;
	}
}
