package com.xiangrui.lmp.business.api.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;
import com.xiangrui.lmp.business.api.mapper.BaseApiMapper;
import com.xiangrui.lmp.business.api.service.BaseApiService;
import com.xiangrui.lmp.business.homepage.service.FrontGoodsTypeService;
import com.xiangrui.lmp.business.homepage.vo.FrontGoodsType;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.constant.WebConstants;
import com.xiangrui.lmp.util.CommonUtil;
import com.xiangrui.lmp.util.HttpRequest;

@Service(value = "baseApiService")
public class BaseApiServiceImp implements BaseApiService
{
	public final static String SERVER_PREFIX="112.74.114.88:8080";
	@Autowired
	private FrontGoodsTypeService frontGoodsTypeService;
	@Autowired
	private BaseApiMapper baseApiMapper;
	private static final Logger logger = Logger.getLogger(BaseApiServiceImp.class);
	/**
	 * 通过积分下限查询用户的等级
	 * 
	 * @param params
	 * @return
	 */
	public List<MemberRate> getMemberRate(Map<String, Object> params)
	{
		return baseApiMapper.getMemberRate(params);
	}
	/**
	 * 获取添加包裹时增值服务列表接口
	 */
	@Override
	public List<HashMap<String, Object>> getAllExtendValues(int rate_id)
	{
		return baseApiMapper.getAllExtendValues(rate_id);
	}

	/**
	 * 获取用户指南列表接口
	 * 
	 * @return
	 */
	@Override
	public List<HashMap<String, Object>> getUserDocument()
	{
		List<HashMap<String, Object>> list= baseApiMapper.getUserDocument();
		if(list!=null&&list.size()>0)
		{
			for(HashMap<String, Object> obj:list)
			{
				String content=(String) obj.get("content");
				content=StringUtils.replace(content, "src=\"", "src=\""+WebConstants.FRONT_URL_PREFIX);
				obj.put("content", content);
			}
		}
		return list;
	}

	@Override
	public List<HashMap<String, Object>> getAllOverseasWarehouses(int user_id)
	{
		List<HashMap<String, Object>> list = baseApiMapper.getAllCountryOverseas(user_id);
		if (list != null && list.size() > 0)
		{
			for (HashMap<String, Object> obj : list)
			{
				String country = obj.get("country").toString();
				List<HashMap<String, Object>> warehouses = baseApiMapper.getOverseasByCountry(country);
				obj.put("warehouses", warehouses);
			}
		}
		return list;
	}

	/**
	 * 获取海外仓库明细
	 */
	@Override
	public Map<String, Object> getOverseasDeatil(int id)
	{
		return baseApiMapper.getOverseasDeatil(id);
	}

	/**
	 * 获取添加包裹时申报类别接口
	 */
	@Override
	public List<Map<String, Object>> getAllCategory()
	{
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		// 查询父类
		List<FrontGoodsType> parentGoodsTypeList = frontGoodsTypeService.queryAllGoodsType(0);
		// 查询子类
		List<FrontGoodsType> childGoodsTypeList = frontGoodsTypeService.queryChildGoodsType();
		if (parentGoodsTypeList != null && parentGoodsTypeList.size() > 0)
		{
			for (FrontGoodsType type : parentGoodsTypeList)
			{
				List<HashMap<String, Object>> children = new ArrayList<HashMap<String, Object>>();
				int id = type.getGoodsTypeId();
				if (childGoodsTypeList != null && childGoodsTypeList.size() > 0)
				{
					for (FrontGoodsType typeChild : childGoodsTypeList)
					{
						if (typeChild.getParentId() == id)
						{
							HashMap<String, Object> map = new HashMap<String, Object>();
							map.put("id", typeChild.getGoodsTypeId());
							map.put("name", typeChild.getNameSpecs());
							children.add(map);
						}
					}
				}
				Map<String, Object> resultMap = new HashMap<String, Object>();
				resultMap.put("id", id);
				resultMap.put("name", type.getNameSpecs());
				resultMap.put("children", children);
				list.add(resultMap);
			}
		}
		return list;
	}

	/**
	 * 获取手机验证码
	 */
	@Override
	public String getSmsVerificationCode(String mobile)
	{
		// 短信验证码
		String smsCode = CommonUtil.radomCode();
		Map<String, String> params = new HashMap<String, String>();
		params.put("UserID", SYSConstant.SMS_USERID);
		params.put("Account", SYSConstant.SMS_ACCOUNT);
		params.put("Password", SYSConstant.SMS_PASSWORD);
		params.put("Phones", mobile);
		String str = "您的验证码是" + smsCode + "【9577跨境物流】";
		String content = "";
		try
		{
			content = URLEncoder.encode(str, "utf-8");
		} catch (UnsupportedEncodingException e1)
		{
			e1.printStackTrace();
		}
		params.put("Content", content);
		params.put("SendTime", "");
		params.put("SendType", "1");
		params.put("PostFixNumber", "1");
		// 发送短信
		String sendResult = HttpRequest.postData(SYSConstant.SMS_URL, params, "UTF-8", HttpRequest.SMS_SEND_INTERFACE);
		try
		{
			if (sendResult != null)
			{
				// 解析返回结果
				Document doc = DocumentHelper.parseText(sendResult);
				Element root = doc.getRootElement();
				Element retE = root.element("RetCode");
				String retCode = retE.getText();
				if (null != retCode && "Sucess".equals(retCode.trim()))
				{
				}
				else
				{
					smsCode = ""; // 发送失败，验证码置空
				}
			}
		} catch (DocumentException e)
		{
			logger.error("短信通知返回内容解析错误：" + e.getMessage());
			e.printStackTrace();
		}
		return smsCode;
	}

	/**
	 * 查询地区名字
	 * 
	 * @param country
	 * @return
	 */
	public String getAreaName(String id)
	{
		return baseApiMapper.getAreaName(id);
	}

	/**
	 * 获取省份城市列表接口
	 */
	@Override
	public List<Map<String, Object>> getAllCitys()
	{
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> provinceList = baseApiMapper.getAllProvince();
		List<Map<String, Object>> cityList = baseApiMapper.getAllCitys();
		List<Map<String, Object>> areaList = baseApiMapper.getAllArea();
		// 遍历省份
		for (Map<String, Object> map : provinceList)
		{
			Map<String, Object> resultMap = new HashMap<String, Object>();
			String provinceName = map.get("city_name").toString();
			String provinceId = map.get("city_id").toString();
			List<Map<String, Object>> citys = new ArrayList<Map<String, Object>>();
			// 遍历市级
			for (Map<String, Object> cityMap : cityList)
			{
				String father_id = cityMap.get("father_id").toString();
				if (provinceId.equals(father_id))
				{
					String city_id = cityMap.get("city_id").toString();
					String city_name = cityMap.get("city_name").toString();
					// 遍历区级
					List<Map<String, Object>> areas = new ArrayList<Map<String, Object>>();
					for (Map<String, Object> areaMap : areaList)
					{
						String fatherId = areaMap.get("father_id").toString();
						if (city_id.equals(fatherId))
						{
							Map<String, Object> areaMapTemp = new HashMap<String, Object>();
							String area_id = areaMap.get("city_id").toString();
							String area_name = areaMap.get("city_name").toString();
							areaMapTemp.put("id", area_id);
							areaMapTemp.put("name", area_name);
							areas.add(areaMapTemp);
						}
					}
					Map<String, Object> cityMapTemp = new HashMap<String, Object>();
					cityMapTemp.put("id", city_id);
					cityMapTemp.put("name", city_name);
					cityMapTemp.put("areas", areas);
					citys.add(cityMapTemp);
				}
			}
			resultMap.put("id", provinceId);
			resultMap.put("province", provinceName);
			resultMap.put("citys", citys);
			resultList.add(resultMap);
		}
		return resultList;
	}

	/**
	 * 获取所有通知
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getAllNotice()
	{
		return baseApiMapper.getAllNotice();
	}

	/**
	 * 根据notice_id通知详情
	 * 
	 * @param notice_id
	 * @return
	 */
	public Map<String, Object> getNoticeDeatil(int notice_id)
	{
		Map<String, Object> map= baseApiMapper.getNoticeDeatil(notice_id);
		String context=(String) map.get("context");
		context=StringUtils.replace(context, "src=\"", "src=\""+WebConstants.FRONT_URL_PREFIX);
		map.put("context", context);
		return map;
	}
}
