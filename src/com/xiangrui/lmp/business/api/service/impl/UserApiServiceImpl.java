package com.xiangrui.lmp.business.api.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.CouponUsed;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;
import com.xiangrui.lmp.business.admin.frontuser.vo.ReceiveAddress;
import com.xiangrui.lmp.business.api.mapper.UserApiMapper;
import com.xiangrui.lmp.business.api.service.UserApiService;
import com.xiangrui.lmp.util.NumberUtils;

@Service(value = "userApiService")
public class UserApiServiceImpl implements UserApiService
{

	@Autowired
	private UserApiMapper userApiMapper;
	/**
	 * 优惠券处理服务
	 */
	@Autowired
	private CouponService couponService;
	/**
	 * 获取用户晒单列表接口
	 */
	@Override
	public List<HashMap<String, Object>> queryAllShare(int userId)
	{
		List<HashMap<String, Object>> list = userApiMapper.queryAllShare(userId);
		if (list != null && list.size() > 0)
		{
			for (HashMap<String, Object> obj : list)
			{
				Integer status = (Integer) obj.get("approval_status");
				// 审核状态，1：未审核，2：审核通过，3：审核拒绝
				if (status == 1)
				{
					obj.put("approval_status", "未审核");
				}
				else if (status == 2)
				{
					obj.put("approval_status", "审核通过");
				}
				else if (status == 3)
				{
					obj.put("approval_status", "审核拒绝");
				}
			}
		}
		return list;
	}

	/**
	 * 客户消费明细接口
	 * 
	 * @param userId
	 * @return
	 */
	public List<HashMap<String, Object>> getConsumeDetail(int userId)
	{
		return userApiMapper.getConsumeDetail(userId);
	}

	/**
	 * 客户充值明细接口
	 * 
	 * @param userId
	 * @return
	 */
	public List<HashMap<String, Object>> getRechargeDetail(int userId)
	{
		return userApiMapper.getRechargeDetail(userId);
	}

	/**
	 * 获取客户地址列表接口
	 */
	@Override
	public List<HashMap<String, Object>> getAllAddresses(int userId)
	{
		List<HashMap<String, Object>> list = userApiMapper.getAllAddresses(userId);
		if (list != null && list.size() > 0)
		{
			for (HashMap<String, Object> obj : list)
			{
				String region = (String) obj.get("region");
				String street = (String) obj.get("street");
				String area = "";
				JSONArray regionArray = JSONArray.fromObject(region);
				if (regionArray != null)
				{
					Object[] regionObjs = (Object[]) regionArray.toArray();
					for (Object reg : regionObjs)
					{
						JSONObject regMap = (JSONObject) reg;
						area = area + regMap.get("name");
					}
				}
				obj.put("address", area + street);
				obj.remove("region");
				obj.remove("street");
			}
		}
		return list;
	}

	/**
	 * 查看客户地址明细接口
	 */
	@Override
	public HashMap<String, Object> getAddressDetail(int userId, int addressId)
	{
		Map<String, Object> paraMap = new HashMap<String, Object>();
		paraMap.put("userId", userId);
		paraMap.put("addressId", addressId);

		HashMap<String, Object> addressDetail = userApiMapper.getAddressDetail(paraMap);
		if (addressDetail != null)
		{
			String area = "";
			String city = (String) addressDetail.get("city");
			JSONArray regionArray = JSONArray.fromObject(city);
			if (regionArray != null)
			{
				Object[] regionObjs = (Object[]) regionArray.toArray();
				for (Object reg : regionObjs)
				{
					JSONObject regMap = (JSONObject) reg;
					area = area + regMap.get("name");
					String srt = "" + regMap.get("level");
					int level = Integer.parseInt(srt);
					if (level == 1)
					{
						addressDetail.put("provinceCode", regMap.get("id"));
					}
					else if (level == 2)
					{
						addressDetail.put("cityCode", regMap.get("id"));
					}
				}
			}
			addressDetail.put("region", area);
		}
		return addressDetail;
	}

	/**
	 * 添加地址
	 */
	@Override
	public int insertAddress(ReceiveAddress address)
	{
		return userApiMapper.insertAddress(address);
	}

	/**
	 * 更新地址
	 */
	@Override
	public int updateAddress(ReceiveAddress address)
	{
		return userApiMapper.updateAddress(address);
	}

	/**
	 * 删除地址接口
	 */
	@Override
	public int deleteUserAddress(int addressId)
	{
		return userApiMapper.deleteUserAddress(addressId);
	}

	/**
	 * 获取可用优惠券接口
	 */
	@Override
	public List<HashMap<String, Object>> getAvliableCoupons(int userId)
	{
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("user_id", userId);
		List<UserCoupon> avliableCouponList=couponService.queryAvliableCouponByUserId(params);
		
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String,Object>>();
		if (avliableCouponList != null && avliableCouponList.size() > 0)
		{
//			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			for ( UserCoupon userCoupon: avliableCouponList)
			{
				HashMap<String, Object> obj = new HashMap<String, Object>();
				obj.put("coupon_id", userCoupon.getCoupon_id());
				obj.put("coupon_code", userCoupon.getCoupon_code());
				obj.put("valid_date", userCoupon.getValid_date_toStr());
				obj.put("coupon_name", userCoupon.getCoupon_name());
				float denomination=(float)userCoupon.getDenomination();
				obj.put("denomination", NumberUtils.scaleMoneyData(denomination));
				obj.put("valid_count", userCoupon.getQuantity()-userCoupon.getHaveUsedCount());
				list.add(obj);
			}
		}
		return list;
	}
	
	/**
	 * 获取已使用优惠券接口
	 */
	@Override
	public List<HashMap<String, Object>> getUsedCoupons(int userId)
	{
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("user_id", userId);
		List<CouponUsed> usedCouponList=couponService.queryUsedCouponByUserId(params);
		
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String,Object>>();
		if (usedCouponList != null && usedCouponList.size() > 0)
		{
//			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			for ( CouponUsed userCoupon: usedCouponList)
			{
				HashMap<String, Object> obj = new HashMap<String, Object>();
				obj.put("coupon_id", userCoupon.getActual_coupon_id());
				obj.put("coupon_code", userCoupon.getCoupon_code());
				
				obj.put("coupon_name", userCoupon.getCoupon_name());
				float denomination=(float)userCoupon.getDenomination();
				obj.put("denomination", NumberUtils.scaleMoneyData(denomination));
				obj.put("used_quantity", userCoupon.getQuantity());
				obj.put("use_date", userCoupon.getUse_date());
				obj.put("order_code", userCoupon.getOrder_code());
				list.add(obj);
			}
		}
		return list;
	}
	
	/**
	 * 获取过期优惠券接口
	 */
	@Override
	public List<HashMap<String, Object>> getExpiredCoupons(int userId)
	{
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("user_id", userId);
		List<UserCoupon> expiredCouponList=couponService.queryExpiredCouponByUserId(params);
		
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String,Object>>();
		if (expiredCouponList != null && expiredCouponList.size() > 0)
		{
//			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			for ( UserCoupon userCoupon: expiredCouponList)
			{
				HashMap<String, Object> obj = new HashMap<String, Object>();
				obj.put("coupon_id", userCoupon.getActual_coupon_id());
				obj.put("coupon_code", userCoupon.getCoupon_code());
				obj.put("valid_date", userCoupon.getValid_date_toStr());
				obj.put("coupon_name", userCoupon.getCoupon_name());
				float denomination=(float)userCoupon.getDenomination();
				obj.put("denomination", NumberUtils.scaleMoneyData(denomination));
				obj.put("expired_quantity", userCoupon.getQuantity());
				list.add(obj);
			}
		}
		return list;
	}

	/**
	 * 根据客户名称，手机号等搜索地址接口
	 */
	@Override
	public List<Map<String, Object>> getAddressBy(Map<String, Object> paramMap)
	{
		List<Map<String, Object>> list = userApiMapper.getAddressBy(paramMap);
		if (list != null && list.size() > 0)
		{
			for (Map<String, Object> obj : list)
			{
				String region = (String) obj.get("region");
				String street = (String) obj.get("street");
				String address = "";
				if (region.startsWith("["))
				{
					JSONArray regionArray = JSONArray.fromObject(region);
					if (regionArray != null)
					{
						Object[] regionObjs = (Object[]) regionArray.toArray();
						for (Object reg : regionObjs)
						{
							JSONObject regMap = (JSONObject) reg;
							String level = regMap.get("level").toString();
							String name = regMap.get("name").toString();
							switch (level)
							{
								case "1":
									// obj.put("province", name);
									address = address + name;
									break;
								case "2":
									// obj.put("city", name);
									address = address + name;
									break;
								case "3":
									// obj.put("county", name);
									address = address + name;
									break;
							}
						}
					}
				}
				obj.remove("region");
				obj.put("address", address + street);
			}
		}
		return list;
	}

	/**
	 * 客户修改支付方式接口
	 */
	@Override
	public int updatePayType(int user_id, String pay_type, String virtual_pay_flag)
	{
		// 其中若virtual_pay_flag设置为Y时，default_pay_type固定为1且不可变更，若virtual_pay_flag设置为N时，default_pay_type可以设置为1或者2

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", user_id);
		int result = 0;
		if ("Y".equals(virtual_pay_flag))
		{
			pay_type = "1";
		}
		if (virtual_pay_flag != null && !"".equals(virtual_pay_flag))
		{
			paramMap.put("virtualPayFlag", virtual_pay_flag);
			result = userApiMapper.updateVirtualPayFlag(paramMap);
		}
		if (pay_type != null && !"".equals(pay_type))
		{
			paramMap.put("payType", pay_type);
			result = userApiMapper.updateDefaultPayType(paramMap);
		}
		return result;
	}

	public ReceiveAddress getAddressByAddressId(int addressId)
	{
		return userApiMapper.getAddressByAddressId(addressId);
	}

	/**
	 * 设置默认地址
	 */
	@Override
	public int updateDefaultAddress(int user_id, String type, String address_id)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", user_id);
		paramMap.put("type", type);
		paramMap.put("addressId", address_id);
		// 如果设置默认地址，先把旧的默认地址更新为非默认地址
		if ("1".equals(type))
		{
			userApiMapper.reSetDefaultAddress(paramMap);
		}
		return userApiMapper.updateDefaultAddress(paramMap);
	}

}
