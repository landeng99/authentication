package com.xiangrui.lmp.business.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;

public interface BaseApiService {
 
	/**
	 * 通过积分下限查询用户的等级
	 * 
	 * @param params
	 * @return
	 */
	List<MemberRate> getMemberRate(Map<String, Object> params);
    /**
     * 获取添加包裹时增值服务列表接口
     * @return
     */
	List<HashMap<String, Object>> getAllExtendValues(int rate_id);
	
    /**
     * 获取用户指南列表接口
     * @return
     */
	List<HashMap<String, Object>> getUserDocument();
 

	/**
	 * 获取海外仓地信息接口
	 * @return
	 */
	List<HashMap<String, Object>> getAllOverseasWarehouses(int user_id);
	
	/**
	 * 获取海外仓库明细
	 * @param id
	 * @return
	 */
	Map<String, Object> getOverseasDeatil(int id);

	/**
	 * 获取添加包裹时申报类别接口
	 * @param result
	 * @return
	 */
	List<Map<String, Object>> getAllCategory();

	/**
	 * 获取手机验证码
	 * @param mobile
	 * @return
	 */
	String getSmsVerificationCode(String mobile);
	
	/**
	 * 查询地区名字
	 * @param country
	 * @return
	 */
	String getAreaName( String id);

	/**
	 * 获取省份城市列表接口 
	 * @return
	 */
	List<Map<String, Object>> getAllCitys();

	/**
	 * 获取所有通知
	 * 
	 * @return
	 */
	List<Map<String, Object>> getAllNotice();

	/**
	 * 根据notice_id通知详情
	 * 
	 * @param notice_id
	 * @return
	 */
	Map<String, Object> getNoticeDeatil(int notice_id);
}
