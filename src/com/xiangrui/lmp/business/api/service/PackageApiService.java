package com.xiangrui.lmp.business.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.api.vo.ExpressContent;
import com.xiangrui.lmp.business.api.vo.ExpressDetail;
import com.xiangrui.lmp.business.api.vo.FilterCondition;
import com.xiangrui.lmp.business.homepage.vo.Pkg;

public interface PackageApiService {
	/**
	 * 获取所有包裹列表
	 * @param mobile
	 * @return
	 */
	List<HashMap<String,Object>> queryAllPackage(int userId);
	
	/**
     * 根据package_id
     * @param package_id
     * @return List
     */
	Map<String, Object> queryPackageByPackageId(int package_id);

	/**
	 * 运单查询接口
	 * @param openId 
	 * @param order_no
	 * @return
	 */
	List<ExpressDetail> queryOrderByNo(String openId, String order_no);
	
 
	/**
	 * 根据运单号查询物流信息追踪接口
	 * @param order_no
	 * @return
	 */
	List<ExpressContent> getLogisticsInfoByOrderNo(String order_no);
	
	/**
	 * 查询历史
	 * @param open_id
	 * @return
	 */
	List<Map<String,Object>> queryHistory(String open_id);
	
	/**
	 * 根据各种条件筛选包裹的接口
	 * @param userId
	 * @return
	 */
	List<Map<String,Object>> getPackageByFilter(int userId,String filters);

	/**
	 * 获取包裹筛选条件接口
	 * @return
	 */
	List<FilterCondition> getPackageFilterCondition();
	
	
	/**
	 * 指定条件查询包裹接口
	 * @param userId
	 * @return
	 */
	List<Map<String,Object>> getAllPackageByCondition(int userId,String condition);
	
	/**
	 * 查询可合并包裹
	 * @param map
	 * @return
	 */
	List<Map<String,Object>> queryAvailableMergePackageList(Map<String,Object> map);
	
	/**
	 * 检查包裹是否可以合箱
	 * @param packageId
	 * @return
	 */
	boolean checkPackageAllReady(int packageId);
	/**
	 * 按照包裹收件人的手机号查询到属于这个收件人的包裹列表
	 * @param moblie
	 * @return
	 */
	List<HashMap<String,Object>> queryAllPackageByReceiverMoblie(String moblie);

	/**
	 * 根据运单号查询收件人手机号码接口
	 * @param order_no
	 * @return
	 */
	String getPhone(String order_no);

	/**
	 * 公司单号/关联单号/收件人姓名/收件人手机号查询包裹
	 * @param paramMap
	 * @return
	 */
	List<Map<String, Object>> getPackages(Map<String, Object> paramMap);
	
	/**
	 * 删除包裹
	 * @param package_id
	 * @return
	 */
	boolean removePackage(int package_id);
	
	boolean checkPackageCanRemove(int package_id);
	
	boolean checkPackageCanEdit(int package_id);
}
