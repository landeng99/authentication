package com.xiangrui.lmp.business.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.frontuser.vo.ReceiveAddress;

public interface UserApiService {
	/**
	 * 获取用户晒单列表接口
	 * @param userId
	 * @return
	 */
	List<HashMap<String, Object>> queryAllShare(int userId);

	/**
	 * 客户消费明细接口
	 * @param userId
	 * @return
	 */
	List<HashMap<String, Object>> getConsumeDetail(int userId);
	
	/**
	 * 客户充值明细接口
	 * @param userId
	 * @return
	 */
	List<HashMap<String, Object>> getRechargeDetail(int userId);

	/**
	 * 获取客户地址列表接口
	 * @param user_id
	 * @return
	 */
	List<HashMap<String, Object>> getAllAddresses(int userId);

	/**
	 * 查看客户地址明细接口
	 * @param user_id
	 * @param addressId
	 * @return
	 */
	HashMap<String, Object> getAddressDetail(int userId,int addressId);
	
	ReceiveAddress getAddressByAddressId(int addressId);

	/**
	 * 添加客户地址
	 * @param address
	 */
	int insertAddress(ReceiveAddress address);

	/**
	 * 更新地址
	 * @param address
	 * @return
	 */
	int updateAddress(ReceiveAddress address);
	
	/**
	 *修改删除地址接口
	 * @param addressId
	 * @return
	 */
	int deleteUserAddress(int addressId);

//	/**
//	 * 获取优惠券接口
//	 * @param user_id
//	 * @return
//	 */
//	List<HashMap<String, Object>> getAllCoupons(int userId);

	/**
	 * 根据客户名称，手机号等搜索地址接口
	 * @param paramMap
	 * @return
	 */
	List<Map<String, Object>> getAddressBy(Map<String, Object> paramMap);

	/**
	 * 客户修改支付方式接口
	 * @param user_id
	 * @param pay_type
	 * @param virtual_pay_flag
	 * @return
	 */
	int updatePayType(int user_id, String pay_type, String virtual_pay_flag);

	/**
	 * 设置默认地址
	 * @param user_id
	 * @param type
	 * @param address_id
	 * @return
	 */
	int updateDefaultAddress(int user_id, String type, String address_id);

	/**
	 * 获取过期优惠券接口
	 */
	List<HashMap<String, Object>> getExpiredCoupons(int userId);
	/**
	 * 获取已使用优惠券接口
	 */
	List<HashMap<String, Object>> getUsedCoupons(int userId);
	/**
	 * 获取可用优惠券接口
	 */
	List<HashMap<String, Object>> getAvliableCoupons(int userId);
}
