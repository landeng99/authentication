package com.xiangrui.lmp.business.api.util;

import java.io.File;

import javax.imageio.stream.FileImageOutputStream;

import org.apache.commons.lang.StringUtils;

import weixin.popular.api.MediaAPI;
import weixin.popular.bean.media.MediaGetResult;

public class WeiXinImageUtil
{
	public static String getWeiXinImageToLocal(String basePath,String filePath, String access_token, String imageId)
	{
		String reFileName = null;
		MediaGetResult result = MediaAPI.mediaGet(access_token, imageId, true);
		if (result != null)
		{
			String fileName = result.getFilename();
			if (fileName != null)
			{
				String subfix = StringUtils.substringAfterLast(fileName, ".");
				FileImageOutputStream imageOutput;
				try
				{
					imageOutput = new FileImageOutputStream(new File(basePath+filePath + "." + subfix));
					imageOutput.write(result.getBytes(), 0, result.getBytes().length);
					imageOutput.close();
					reFileName=filePath + "." + subfix;
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}

		}
		return reFileName;
	}
}
