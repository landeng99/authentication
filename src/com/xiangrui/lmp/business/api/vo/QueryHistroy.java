package com.xiangrui.lmp.business.api.vo;

import java.io.Serializable;
import java.util.Date;

public class QueryHistroy implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private int id;
    /**
     * 用户id
     */
    private int user_id;

    /**
     * 运单号
     */
    private String order_no;

    /**
     * 查询时间
     */
    private Date query_time;

    /**
     * 收货地址
     */
    private String receive_address;

    /**
     * 发货地址
     */
    private String send_address;
    
    private String open_id;

	public String getOpen_id()
	{
		return open_id;
	}

	public void setOpen_id(String open_id)
	{
		this.open_id = open_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public Date getQuery_time() {
		return query_time;
	}

	public void setQuery_time(Date query_time) {
		this.query_time = query_time;
	}

	public String getReceive_address() {
		return receive_address;
	}

	public void setReceive_address(String receive_address) {
		this.receive_address = receive_address;
	}

	public String getSend_address() {
		return send_address;
	}

	public void setSend_address(String send_address) {
		this.send_address = send_address;
	}

	public QueryHistroy() {
		super();
	}

	public QueryHistroy(int id, int user_id, String order_no, Date query_time,
			String receive_address, String send_address) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.order_no = order_no;
		this.query_time = query_time;
		this.receive_address = receive_address;
		this.send_address = send_address;
	}
 
}
