package com.xiangrui.lmp.business.api.vo;

public enum Status {
	exception("异常", -1), waitIN("待入库",0), in("已入库", 1), waitOut("待发货",2),
	out("已出库",3), ship("空运中",4), waitClear("待清关",5),clearing("清关中",6), 
	clear("已清关",7), send("派件中",8), receive("已收货",9), noUse("废弃",20), back("退货",21);
	
     // 成员变量
     private String name;
     private int index;

     // 构造方法
     private Status(String name, int index) {
         this.name = name;
         this.index = index;
     }

     // 普通方法
     public static String getName(int index) {
         for (Status c : Status.values()) {
             if (c.getIndex() == index) {
                 return c.name;
             }
         }
         return null;
     }

     // get set 方法
     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

     public int getIndex() {
         return index;
     }

     public void setIndex(int index) {
         this.index = index;
     }
}
