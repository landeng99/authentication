package com.xiangrui.lmp.business.api.vo;

/**
 * 运单查询接口返回信息
 * <p>@author <b>ljc</b></p>
 * <p>2015-7-10 上午10:54:04</p>
 */
public class ExpressDetail
{
    private String date_time;
    private String logistic_info;
    
	public String getDate_time() {
		return date_time;
	}
	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}
	public String getLogistic_info() {
		return logistic_info;
	}
	public void setLogistic_info(String logistic_info) {
		this.logistic_info = logistic_info;
	}
	public ExpressDetail(String date_time, String logistic_info) {
		super();
		this.date_time = date_time;
		this.logistic_info = logistic_info;
	}
	public ExpressDetail() {
		super();
	}
    
    
    
}
