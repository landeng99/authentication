package com.xiangrui.lmp.business.api.vo;

import java.util.List;

public class FilterCondition {
	private String filterName;
    private String filterKey;
    List<FilterItems> filterItems;
    
	public String getFilterName() {
		return filterName;
	}
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	public String getFilterKey() {
		return filterKey;
	}
	public void setFilterKey(String filterKey) {
		this.filterKey = filterKey;
	}
	public List<FilterItems> getFilterItems() {
		return filterItems;
	}
	public void setFilterItems(List<FilterItems> filterItems) {
		this.filterItems = filterItems;
	}
    
    
}
