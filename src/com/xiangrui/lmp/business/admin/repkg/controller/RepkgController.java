package com.xiangrui.lmp.business.admin.repkg.controller;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.pkg.service.PackageImgService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgGoodsService;
import com.xiangrui.lmp.business.admin.pkg.vo.PackageImg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods;
import com.xiangrui.lmp.business.admin.repkg.service.RepkgService;
import com.xiangrui.lmp.business.admin.repkg.vo.Repkg;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.PageView;

/**
 * 包裹退货界面
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-25 下午8:02:24
 * </p>
 */
@Controller
@RequestMapping("/admin/repkg")
public class RepkgController
{

    /**
     * 包裹退货操作
     */
    @Autowired
    private RepkgService repkgService;

    /**
     * 包裹品牌操作
     */
    @Autowired
    private PkgGoodsService pkgGoodsService;

    /**
     * 包裹图片操作
     */
    @Autowired
    PackageImgService packageImgService;

    /**
     * 审核通过
     */
    private static final int APPROVE_THROUGH = 1;

    /**
     * 审核拒绝
     */
    private static final int APPROVE_REFUSING = 2;

    /**
     * 首次查询
     * 
     * @param request
     * @return
     */
    @RequestMapping("/queryAllOne")
    public String indexFink(HttpServletRequest request)
    {
        return queryAll(request, null);
    }

    /**
     * 查询所有退货包裹 及其退货信息
     * 
     * @param request
     * @param repkg
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request, Repkg repkg)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);
        
        // 首次查询 当没有有request范围内的参数aAll的设置时,不是首次查询
        if (null != repkg)
        {
            params.put("USER_NAME", repkg.getUser_name());
            params.put("APPROVE_STATUS", repkg.getApprove_status());
            params.put("CREATETIMEEND", repkg.getCreateTimeEnd());
            params.put("LOGISTICS_CODE", repkg.getLogistics_code());
            params.put("CREATETIMESTART", repkg.getCreateTimeStart());
        }

        request.setAttribute("pageView", pageView);
        request.setAttribute("repkgLists", repkgService.queryAll(params));
        return "back/repkgList";
    }

    /**
     * 进入退货包裹的详细信息
     * 
     * @param request
     * @param repkg
     * @return
     */
    @RequestMapping("/toFinkRepkgOne")
    public String toFinkRepkgOne(HttpServletRequest request, Repkg repkg)
    {
        // 匹配包裹退货信息
        repkg = repkgService.queryAllId(repkg.getReturn_id());
        
        // 匹配包裹商品信息
        List<PkgGoods> pkgGoodsList = pkgGoodsService
                .selectPkgGoodsByPackageId(repkg.getPkg_id());
        
        // 匹配包裹图片信息
        // TODO 原来的方法一个包裹id可能对应多条记录
        PackageImg packageImg = new PackageImg();
        packageImg.setPackage_id(repkg.getPkg_id());
        
        // TODO 设置图片类型
        // packageImg.setImg_type(img_type);
        packageImg = packageImgService.queryPackageImg(packageImg);
        if (null == packageImg || null == packageImg.getImg_path())
        {
            request.setAttribute("img_path", "无图片");
        } else
        {
            request.setAttribute("img_path", packageImg.getImg_path());
        }
        request.setAttribute("CIF_TRUE", "bgin");
        request.setAttribute("repkgPojo", repkg);
        request.setAttribute("pkgGoodsList", pkgGoodsList);
        return "back/repkgInfo";
    }

    /**
     * 进入退货包裹的详细信息
     * 
     * @param request
     * @param repkg
     * @return
     */
    @RequestMapping("/toFinkRepkg")
    public String toFinkRepkg(HttpServletRequest request, Repkg repkg)
    {
        return toFinkRepkgOne(request, repkg);
    }

    /**
     * 审核
     * 
     * @param request
     * @param repkg
     * @return
     */
    @RequestMapping("/updateRepkgThrough")
    public String updateRepkgThrough(HttpServletRequest request, Repkg repkg)
    {
        approve(request, repkg, APPROVE_THROUGH);
        return queryAll(request, null);
    }

    /**
     * 拒绝
     * 
     * @param request
     * @param repkg
     * @return
     */
    @RequestMapping("/updateRepkgRefusing")
    public String updateRepkgRefusing(HttpServletRequest request, Repkg repkg)
    {
        approve(request, repkg, APPROVE_REFUSING);
        return queryAll(request, null);
    }

    /**
     * 审核,拒绝等的共有处理方式
     * 
     * @param request
     * @param repkg
     * @param approve_status
     *            审核,拒绝的状态吗
     */
    private void approve(HttpServletRequest request, Repkg repkg,
            int approve_status)
    {
        // 审核理由
        String approveReason = "";
        try
        {
            approveReason = new String(repkg.getApprove_reason().getBytes(
                    "ISO8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        User back_user = (User) request.getSession().getAttribute("back_user");
        
        // 登陆用户已经失去连接,请重新登陆
        if (null == back_user)
        {
            return;
        }
        int operater = back_user.getUser_id();// 审核人id
        
        // 审核时间
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        // 查询数据库
        repkg = repkgService.queryAllId(repkg.getReturn_id());
        
        // 申请状态 拒绝
        repkg.setApprove_status(approve_status);
        repkg.setApprove_reason(approveReason);
        repkg.setOperater(operater);
        repkg.setApprove_time(df.format(date));
        
        // 修改
        repkgService.updateRepkg(repkg);
    }
}
