package com.xiangrui.lmp.business.admin.repkg.vo;

import java.io.Serializable;

/**
 * 包裹退货
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午4:29:47
 * </p>
 */
public class Repkg implements Serializable
{

    private static final long serialVersionUID = 1L;

    /**
     * 查询退货结束时间
     */
    private String createTimeEnd;
    /**
     * 查询退货开始事假
     */
    private String createTimeStart;

    /**
     * 查询退货金额
     */
    private double return_cost;
    /**
     * 包裹退货申请ID
     */
    private int return_id;
    /**
     * 退货包裹单号
     */
    private String logistics_code;
    /**
     * 退货人id
     */
    private int user_id;
    /**
     * 退货人
     */
    private String user_name;
    /**
     * 包裹ID
     */
    private int pkg_id;
    /**
     * 退货理由
     */
    private String reason;
    /**
     * 审批状态
     */
    private int approve_status;
    /**
     * 拒绝理由
     */
    private String approve_reason;
    /**
     * 审批人id
     */
    private int operater;
    /**
     * 申请人名
     */
    private String operater_name;
    /**
     * 申请时间
     */
    private String create_time;
    /**
     * 审批时间
     */
    private String approve_time;

    public int getReturn_id()
    {
        return return_id;
    }

    public void setReturn_id(int return_id)
    {
        this.return_id = return_id;
    }

    public String getLogistics_code()
    {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public String getUser_name()
    {
        return user_name;
    }

    public void setUser_name(String user_name)
    {
        this.user_name = user_name;
    }

    public int getPkg_id()
    {
        return pkg_id;
    }

    public void setPkg_id(int pkg_id)
    {
        this.pkg_id = pkg_id;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public int getApprove_status()
    {
        return approve_status;
    }

    public void setApprove_status(int approve_status)
    {
        this.approve_status = approve_status;
    }

    public String getApprove_reason()
    {
        return approve_reason;
    }

    public void setApprove_reason(String approve_reason)
    {
        this.approve_reason = approve_reason;
    }

    public int getOperater()
    {
        return operater;
    }

    public void setOperater(int operater)
    {
        this.operater = operater;
    }

    public String getCreate_time()
    {
        return create_time;
    }

    public void setCreate_time(String create_time)
    {
        this.create_time = create_time;
    }

    public String getApprove_time()
    {
        return approve_time;
    }

    public void setApprove_time(String approve_time)
    {
        this.approve_time = approve_time;
    }

    public String getOperater_name()
    {
        return operater_name;
    }

    public void setOperater_name(String operater_name)
    {
        this.operater_name = operater_name;
    }

    public double getReturn_cost()
    {
        return return_cost;
    }

    public void setReturn_cost(double return_cost)
    {
        this.return_cost = return_cost;
    }

    public String getCreateTimeEnd()
    {
        return createTimeEnd;
    }

    public void setCreateTimeEnd(String createTimeEnd)
    {
        this.createTimeEnd = createTimeEnd;
    }

    public String getCreateTimeStart()
    {
        return createTimeStart;
    }

    public void setCreateTimeStart(String createTimeStart)
    {
        this.createTimeStart = createTimeStart;
    }

}
