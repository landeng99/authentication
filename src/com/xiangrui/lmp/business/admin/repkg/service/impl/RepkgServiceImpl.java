package com.xiangrui.lmp.business.admin.repkg.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.repkg.mapper.RepkgMapper;
import com.xiangrui.lmp.business.admin.repkg.service.RepkgService;
import com.xiangrui.lmp.business.admin.repkg.vo.Repkg;

/**
 * 包裹退货
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午4:28:38
 * </p>
 */
@Service("repkgService")
public class RepkgServiceImpl implements RepkgService
{

    /**
     * 包裹数据
     */
    @Autowired
    private RepkgMapper repkgMapper;

    /**
     * 查询所有
     */
    @Override
    public List<Repkg> queryAll(Map<String, Object> params)
    {
        return repkgMapper.queryAll(params);
    }

    /**
     * 查询具体
     */
    @Override
    public Repkg queryAllId(int id)
    {
        return repkgMapper.queryAllId(id);
    }

    /**
     * 修改退货信息,主要是状态的变动
     */
    @SystemServiceLog(description = "修改退货信息")
    @Override
    public void updateRepkg(Repkg repkg)
    {
        repkgMapper.updateRepkg(repkg);
    }
}
