package com.xiangrui.lmp.business.admin.repkg.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.repkg.vo.Repkg;

/**
 * 包裹退货
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午4:28:27
 * </p>
 */
public interface RepkgService
{

    /**
     * 查询所有
     * 
     * @param parems
     * @return
     */
    List<Repkg> queryAll(Map<String, Object> params);

    /**
     * 查询所有
     * 
     * @param parems
     * @return
     */
    Repkg queryAllId(int id);

    /**
     * 查询所有
     * 
     * @param parems
     * @return
     */
    void updateRepkg(Repkg repkg);
}
