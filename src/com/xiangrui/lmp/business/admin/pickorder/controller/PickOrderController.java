package com.xiangrui.lmp.business.admin.pickorder.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xiangrui.lmp.business.admin.export.service.ExportService;
import com.xiangrui.lmp.business.admin.export.vo.PkgExport;
import com.xiangrui.lmp.business.admin.output.service.OutputService;
import com.xiangrui.lmp.business.admin.output.vo.Output;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.pallet.service.PalletService;
import com.xiangrui.lmp.business.admin.pallet.vo.Pallet;
import com.xiangrui.lmp.business.admin.pickorder.service.PickOrderService;
import com.xiangrui.lmp.business.admin.pickorder.vo.PickOrder;
import com.xiangrui.lmp.business.admin.pkg.service.UserAddressService;
import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;
import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.template.service.ExportConfigService;
import com.xiangrui.lmp.business.admin.template.vo.ExportConfig;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.constant.WebConstants;
import com.xiangrui.lmp.init.AppServerUtil;
import com.xiangrui.lmp.init.SystemParamUtil;
import com.xiangrui.lmp.util.BreadcrumbUtil;
import com.xiangrui.lmp.util.FileToZipUtil;
import com.xiangrui.lmp.util.IdentifierCreator;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.newExcel.ExcelWriteUtil;

@Controller
@RequestMapping("/admin/pickorder")
public class PickOrderController
{

    Logger logger = Logger.getLogger(PickOrderController.class);

    @Autowired
    private PickOrderService pickOrderService;

    @Autowired
    private PalletService palletService;

    @Autowired
    private SeaportService seaportService;

    @Autowired
    private ExportConfigService exportConfigService;

    @Autowired
    private ExportService exportService;

    @Autowired
    private UserAddressService userAddressService;

    @Autowired
    private FrontUserService frontUserService;

    @Autowired
    private OverseasAddressService overseasAddressService;
	@Autowired
	private OutputService outputService;
    /**
     * 上传图片在服务器存放路径resource/upload/
     */
    private static final String UPLOAD_IMG_PATH = AppServerUtil.getWebRoot()+"resource"+File.separator+"upload"+File.separator;
           
    /**
     * 上传文件的后缀 pdf格式
     */
    private static final String UPLOAD_SUFFIX = ".pdf";

    /**
     * 
     * 查询提单
     * 
     * @param req
     * @param resp
     * @return
     */
    @RequestMapping("/list")
    public String list(HttpServletRequest req, HttpServletResponse resp)
    {

        String pageIndexStr = req.getParameter("pageIndex");
        PageView pageView = null;

        if (null != pageIndexStr && !"".equals(pageIndexStr))
        {
            int pageIndex = Integer.valueOf(pageIndexStr);
            pageView = new PageView(pageIndex);
        } else
        {
            pageView = new PageView(1);
        }

       
        Map<String,Object> params=new HashMap<String,Object>();
        params.put("pageView", pageView);
        List<Seaport> seaportList = seaportService.queryAll();

        Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = (Map<Integer, List<RoleOverseasAddress>>) SystemParamUtil
                .getAuthority("userOverseasAddressMap");

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("back_user");

        // 获取用户仓库权限
        List<RoleOverseasAddress> userOverseasAddressList = userOverseasAddressMap
                .get(user.getUser_id());
        if(userOverseasAddressList.isEmpty()){
            List<RoleOverseasAddress> tempList=new ArrayList<RoleOverseasAddress>();
            RoleOverseasAddress roleOverseasAddress=new RoleOverseasAddress();
            roleOverseasAddress.setId(9999);
            tempList.add(roleOverseasAddress);
            params.put("userOverseasAddressList", tempList);
        }else{
            params.put("userOverseasAddressList", userOverseasAddressList);
        }
        
        List<PickOrder> pickOrderList = pickOrderService.queryByMap(params);
        

        req.setAttribute("userOverseasAddressList", userOverseasAddressList);
        req.setAttribute("seaportList", seaportList);
        req.setAttribute("pickOrderList", pickOrderList);
        req.setAttribute("pageView", pageView);
        BreadcrumbUtil.push(req);
        return "back/pickorderlist";
    }

    /**
     * 进入提单创建页面
     * 
     * @param req
     * @param resp
     * @return
     */
    @RequestMapping("/initadd")
    public String initAdd(HttpServletRequest req, HttpServletResponse resp)
    {

        String pick_code = IdentifierCreator
                .createIdentifier(IdentifierCreator.TYPE_PICKORDER);
        req.setAttribute("pick_code", pick_code);
        List<Seaport> seaportList = seaportService.queryAll();
        req.setAttribute("seaportList", seaportList);

        Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = (Map<Integer, List<RoleOverseasAddress>>) SystemParamUtil
                .getAuthority("userOverseasAddressMap");

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("back_user");

        // 获取用户仓库权限
        List<RoleOverseasAddress> userOverseasAddressList = userOverseasAddressMap
                .get(user.getUser_id());

        req.setAttribute("userOverseasAddressList", userOverseasAddressList);
        
        BreadcrumbUtil.push(req);
        
        return "back/pickorderadd";
    }

    @RequestMapping("/add")
    public ModelAndView add(HttpServletRequest req, HttpServletResponse resp,
            PickOrder pickOrder)
    {
        Timestamp createTime = new Timestamp(System.currentTimeMillis());
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("back_user");
        pickOrder.setCreate_user(user.getUser_id());
        pickOrder.setCreate_time(createTime);
        pickOrder.setStatus(PickOrder.PICKORDER_SENT_ALREADY);
        int count = pickOrderService.save(pickOrder);

        ModelMap modelMap = new ModelMap();

        // 创建提单完成之后， 进入提单页面
        if (count > 0)
        {
            modelMap.put("pick_id", pickOrder.getPick_id());

            return new ModelAndView("redirect:/admin/pickorder/initedit",
                    modelMap);

        } else
        {
            modelMap.put("fail", "保存提单失败，请联系管理员");
            return new ModelAndView("back/pickorderadd", modelMap);
        }
    }

    @RequestMapping("/exportIdcard")
    public void exportIdcard(HttpServletRequest req, HttpServletResponse resp)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pick_id", req.getParameter("pick_id"));

        List<UserAddress> userAddressList = userAddressService
                .queryPkgUserAddrByPickId(params);

        StringBuffer buffer = new StringBuffer();
        String ctxPath = req.getSession().getServletContext().getRealPath("/")
                + "resource";
        String pick_code = req.getParameter("pick_code");

        String zipFileName = pick_code + ".zip";

        String zipFilePath = ctxPath + File.separator + zipFileName;

        // 创建压缩包
        ZipOutputStream zos = FileToZipUtil.createZip(zipFilePath);

        PrintWriter out = null;

        if (zos == null)
        {
            try
            {
                out = resp.getWriter();

                out.print("<div>系统错误，无法创建压缩包....</div>");
                return;
            } catch (IOException e)
            {
                e.printStackTrace();
                logger.error("输出错误：", e);
                return;
            }

        }
        List<String> existFileList = new ArrayList<String>();
        for (UserAddress userAddr : userAddressList)
        {
            String idcard = userAddr.getIdcard();
            FrontUser frontUser = frontUserService.queryFrontUserById(userAddr
                    .getUser_id());
            if (idcard == null || "".equals(idcard))
            {
                buffer.append("mobile:" + frontUser.getMobile());
                buffer.append(",pkg:" + userAddr.getLogistics_code());
                buffer.append(",error:无身份证号码");
                buffer.append("\n");
                continue;
            }

            // 正面照片
            String front_img = userAddr.getFront_img();

            if (front_img == null || "".equals(front_img))
            {
                buffer.append("用户手机号:" + frontUser.getMobile());
                buffer.append(",包裹运单号:" + userAddr.getLogistics_code());
                buffer.append(",错误信息:" + idcard + "无正面照");
                buffer.append("\n");
            }
            // (不为空&&不等空格符&&未被写入)
            if (front_img != null && !"".equals(front_img)
                    && !existFileList.contains(front_img))
            {
                // 如果文件已经被写入压缩包，则跳出循环

                existFileList.add(front_img);

                String imgPath = front_img.replace("/", File.separator);

                String front_sourceFilePath = ctxPath + imgPath;

                // 文件打包到压缩包中
                boolean isExist = FileToZipUtil.fileToZip(front_sourceFilePath,
                        zos);

                if (!isExist)
                {
                    buffer.append("用户手机号:" + frontUser.getMobile());
                    buffer.append(",包裹运单号:" + userAddr.getLogistics_code());
                    buffer.append(",错误信息: " + idcard + "无正面照");
                    buffer.append("\n");
                }
            }

            String back_img = userAddr.getBack_img();

            if (back_img == null || "".equals(back_img))
            {
                buffer.append("用户手机号:" + frontUser.getMobile());
                buffer.append(",包裹运单号:" + userAddr.getLogistics_code());
                buffer.append(",错误信息:" + idcard + "无反面照");
                buffer.append("\n");
            }
            // (不为空&&不等空格符&&未被写入)
            if (back_img != null && !"".equals(back_img)
                    && !existFileList.contains(back_img))
            {
                // 替换图片路径
                String imgPath = back_img.replace("/", File.separator);

                String back_sourceFilePath = ctxPath + imgPath;
                // 文件是否存在
                boolean isExist = FileToZipUtil.fileToZip(back_sourceFilePath,
                        zos);

                if (!isExist)
                {
                    buffer.append("用户手机号:" + frontUser.getMobile());
                    buffer.append(",包裹运单号:" + userAddr.getLogistics_code());
                    buffer.append(",错误信息:" + idcard + "无反面照");
                    buffer.append("\n");
                }
            }
        }

        String txtFilePath = ctxPath + File.separator + pick_code + ".txt";

        File txtFile = new File(txtFilePath);
        if (txtFile.exists())
        {
            txtFile.delete();
        }

        FileOutputStream fos;
        try
        {
            fos = new FileOutputStream(txtFile);
            fos.write(buffer.toString().getBytes());
            FileToZipUtil.fileToZip(txtFilePath, zos);
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {

            e.printStackTrace();
        } finally
        {

            try
            {
                zos.close();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        // 输出压缩包
        print(req, resp, zipFilePath, zipFileName);
    }

    private void print(HttpServletRequest req, HttpServletResponse resp,
            String zipFilePath, String zipFileName)
    {
        String fileType = "application/octet-stream";
        resp.setHeader("Content-disposition", "attachment;filename=\""
                + zipFileName + "\"");
        resp.setContentType(fileType);

        BufferedInputStream bis;
        try
        {
            bis = new BufferedInputStream(new FileInputStream(zipFilePath));

            ServletOutputStream out = resp.getOutputStream();

            byte[] bf = new byte[1024];
            int length = 0;

            while ((length = bis.read(bf)) != -1)
            {
                out.write(bf, 0, length);
            }
            out.flush();
        } catch (Exception e)
        {
            logger.error("文件输出失败", e);
        }
    }

    @RequestMapping("/detail")
    public String detail(HttpServletRequest req, HttpServletResponse resp)
    {
        String pick_idStr = req.getParameter("pick_id");

        Integer pick_id = Integer.valueOf(pick_idStr);
        Map<String, Integer> param = new HashMap<String, Integer>();
        param.put("pick_id", pick_id);

        String pageIndexStr = req.getParameter("pageIndex");

        PageView pageView = null;

        if (null != pageIndexStr && !"".equals(pageIndexStr))
        {
            int pageIndex = Integer.valueOf(pageIndexStr);
            pageView = new PageView(pageIndex);
        } else
        {
            pageView = new PageView(1);
        }
        PickOrder pickOrder = pickOrderService.queryDetail(param, pageView);
        int overseas_address_id =pickOrder.getOverseas_address_id();
        
        Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = (Map<Integer, List<RoleOverseasAddress>>) SystemParamUtil
                .getAuthority("userOverseasAddressMap");

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("back_user");

        // 获取用户仓库权限
        List<RoleOverseasAddress> userOverseasAddressList = userOverseasAddressMap
                .get(user.getUser_id());

       for(RoleOverseasAddress userOverseasAddress: userOverseasAddressList){
           
           if(userOverseasAddress.getId()==overseas_address_id){
               pickOrder.setWarehouse(userOverseasAddress.getWarehouse());
               break;
           }
       }
        
        // 判断是否有上传的提单pdf文件,可提供下载的代码
        // 文件名称
        String pdfFile = pickOrder.getPick_code();
        // 文件完整名称 前面路径+文件名+后缀
        String pdfFileFull = UPLOAD_IMG_PATH + pdfFile + UPLOAD_SUFFIX;
        File fileTemp = new File(pdfFileFull);
        // 判断文件是否存在
        boolean isPicPdfFileExists = fileTemp.exists();
        req.setAttribute("isPicPdfFileExists", isPicPdfFileExists);

        req.setAttribute("pickOrder", pickOrder);

        req.setAttribute("pageView", pageView);
        BreadcrumbUtil.push(req);
        return "back/pickorderdetail";
    }

    @RequestMapping("/delpickorder")
    public String delPickOrder(HttpServletRequest req,
            HttpServletResponse resp, int pick_id)
    {
        PickOrder pickOrder = new PickOrder();
        pickOrder.setPick_id(pick_id);
        pickOrderService.delPickOrder(pickOrder);

        return "redirect:/admin/pickorder/list";
    }
    
    @RequestMapping("/delPickOrderAjax")
	@ResponseBody
    public String delPickOrderAjax(HttpServletRequest req,
            HttpServletResponse resp, int pick_id, Integer seq_id)
    {
    	PickOrder pickOrder = pickOrderService.queryByPickId(pick_id);
		Output output = outputService.queryOutputBySeqId(seq_id);
		if (output != null)
		{
			String existSeaportIdList = output.getSeaport_id_list();
			String[] array=existSeaportIdList.split(",");
			String result="";
			String temp=""+pickOrder.getSeaport_id();
			if(array!=null&&array.length>0)
			{
				for(String ssstr:array)
				{
					if(!ssstr.equals(temp))
					{
						result+=ssstr+",";
					}
				}
			}
			output.setSeaport_id_list(result);
			outputService.updateOutput(output);
		}
        
        pickOrder.setPick_id(pick_id);
        pickOrderService.delPickOrder(pickOrder);

        return null;
    }

    @RequestMapping("/initedit")
    public String initEdit(HttpServletRequest req, HttpServletResponse resp)
    {
        String pick_idStr = req.getParameter("pick_id");
        Integer pick_id = Integer.valueOf(pick_idStr);
        Map<String, Integer> param = new HashMap<String, Integer>();
        param.put("pick_id", pick_id);
        String pageIndexStr = req.getParameter("pageIndex");

        PageView pageView = null;

        if (null != pageIndexStr && !"".equals(pageIndexStr))
        {
            int pageIndex = Integer.valueOf(pageIndexStr);
            pageView = new PageView(pageIndex);
        } else
        {
            pageView = new PageView(1);
        }
        PickOrder pickOrder = pickOrderService.queryDetail(param, pageView);

       int overseas_address_id =pickOrder.getOverseas_address_id();
        
        Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = (Map<Integer, List<RoleOverseasAddress>>) SystemParamUtil
                .getAuthority("userOverseasAddressMap");

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("back_user");

        // 获取用户仓库权限
        List<RoleOverseasAddress> userOverseasAddressList = userOverseasAddressMap
                .get(user.getUser_id());

       for(RoleOverseasAddress userOverseasAddress: userOverseasAddressList){
           
           if(userOverseasAddress.getId()==overseas_address_id){
               pickOrder.setWarehouse(userOverseasAddress.getWarehouse());
               break;
           }
       }
        
        // 判断是否有上传的提单pdf文件,可提供下载的代码
        // 文件名称
        String pdfFile = pickOrder.getPick_code();
        // 文件完整名称 前面路径+文件名+后缀
        String pdfFileFull = UPLOAD_IMG_PATH + pdfFile + UPLOAD_SUFFIX;
        File fileTemp = new File(pdfFileFull);
        // 判断文件是否存在
        boolean isPicPdfFileExists = fileTemp.exists();
        req.setAttribute("isPicPdfFileExists", isPicPdfFileExists);

        req.setAttribute("pickOrder", pickOrder);
        req.setAttribute("pageView", pageView);
        BreadcrumbUtil.push(req);
        return "back/pickorderedit";
    }

    @RequestMapping("/edit")
    @ResponseBody
    public Map<String, Object> edit(HttpServletRequest req,
            HttpServletResponse resp, PickOrder pickOrder)
    {
        Map<String, Object> result = new HashMap<String, Object>();

        String flight_num = pickOrder.getFlight_num();

        // 如果包裹处于已出库状态变更为空运中。
        if (flight_num != null && !"".equals(flight_num.trim())
                && pickOrder.getStatus() == PickOrder.PICKORDER_SENT_ALREADY)
        {
            pickOrder.setStatus(PickOrder.PICKORDER_AIRLIFT_ALREADY);
        }

        int updateCnt = pickOrderService.updatePickOrder(pickOrder);

        if (updateCnt > 0)
        {
            result.put("result", true);
        } else
        {

            result.put("result", false);
        }
        return result;
    }

    @RequestMapping("/delpickorderbatch")
    public String delPickOrderBatch(HttpServletRequest req,
            HttpServletResponse resp)
    {

        String pick_idsStr = req.getParameter("pick_ids");
        String[] pick_ids = pick_idsStr.split(",");
        for (String str : pick_ids)
        {
            int pick_id = Integer.valueOf(str);
            PickOrder pickOrder = new PickOrder();
            pickOrder.setPick_id(pick_id);
            pickOrderService.delPickOrder(pickOrder);
        }
        return "redirect:/admin/pickorder/list";
    }

    @RequestMapping("/delpallet")
    public ModelAndView delpallet(HttpServletRequest req,
            HttpServletResponse resp, RedirectAttributes attr, Pallet pallet)
    {

        int delCnt = palletService.delPallet(pallet);

        if (delCnt > 0)
        {
            req.setAttribute("resultMsg", "删除成功");
        } else
        {
            req.setAttribute("resultMsg", "删除失败");
        }
        attr.addAttribute("pick_id", pallet.getPick_id());
        ModelMap mlMap = new ModelMap();
        mlMap.put("pick_id", pallet.getPick_id());
        //当前请求放入面包切中
        BreadcrumbUtil.push(req);
        return new ModelAndView("redirect:/admin/pickorder/initedit", mlMap);
    }

    @RequestMapping("/search")
    public String search(HttpServletRequest req, HttpServletResponse resp)
    {
        String pageIndexStr = req.getParameter("pageIndex");
        PageView pageView = null;

        if (null != pageIndexStr && !"".equals(pageIndexStr))
        {
            int pageIndex = Integer.valueOf(pageIndexStr);
            pageView = new PageView(pageIndex);
        } else
        {
            pageView = new PageView(1);
        }

        String pick_code = req.getParameter("pick_code");
        if (pick_code != null)
        {
            pick_code = pick_code.trim();
        }
        String flight_num = req.getParameter("flight_num");

        if (flight_num != null)
        {
            flight_num = flight_num.trim();
        }

        String seaport_id = req.getParameter("seaport_id");
        String status = req.getParameter("status");
        String logistics_code = req.getParameter("logistics_code");
        
        String overseas_address_id=req.getParameter("overseas_address_id");

        if (logistics_code != null)
        {
            logistics_code = logistics_code.trim();
        }

        String fromDate = req.getParameter("fromDate");

        String toDate = req.getParameter("toDate");

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pick_code", pick_code);
        params.put("logistics_code", logistics_code);
        params.put("flight_num", flight_num);
        params.put("seaport_id", seaport_id);
        params.put("overseas_address_id", overseas_address_id);
        params.put("status", status);

        // 起始时间
        params.put("fromDate", fromDate);

        // 终止时间
        params.put("toDate", toDate);
        params.put("pageView", pageView);
        
        //当前请求放入面包切中
        BreadcrumbUtil.push(req);
        
        // 查询口岸
        List<Seaport> seaportList = seaportService.queryAll();

        Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = (Map<Integer, List<RoleOverseasAddress>>) SystemParamUtil
                .getAuthority("userOverseasAddressMap");

        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("back_user");

        // 获取用户仓库权限
        List<RoleOverseasAddress> userOverseasAddressList = userOverseasAddressMap
                .get(user.getUser_id());
        if(userOverseasAddressList.isEmpty()){
            List<RoleOverseasAddress> tempList=new ArrayList<RoleOverseasAddress>();
            RoleOverseasAddress roleOverseasAddress=new RoleOverseasAddress();
            roleOverseasAddress.setId(9999);
            tempList.add(roleOverseasAddress);
            params.put("userOverseasAddressList", tempList);
        }else{
            params.put("userOverseasAddressList", userOverseasAddressList);
        }
        
        // 查收提单
        List<PickOrder> pickOrderList = pickOrderService.queryByMap(params);
        
        
        req.setAttribute("params", params);
        req.setAttribute("userOverseasAddressList", userOverseasAddressList);
        
        req.setAttribute("seaportList", seaportList);
        req.setAttribute("pickOrderList", pickOrderList);
        req.setAttribute("pageView", pageView);
        return "back/pickorderlist";
    }

    @RequestMapping("/initexport")
    public String initExport(HttpServletRequest req, HttpServletResponse resp)
    {
        List<ExportConfig> list = exportConfigService
                .queryAllExportConfig(new HashMap<String, Object>());

        String pick_id = req.getParameter("pick_id");
        String pick_code = req.getParameter("pick_code");
        req.setAttribute("exportConfigList", list);
        req.setAttribute("pick_id", pick_id);
        req.setAttribute("pick_code", pick_code);
        return "back/pickorder_export";
    }

    /**
     * @param req
     * @param resp
     * @param export_id
     * @param pick_id
     * @param fileType
     * @throws Exception
     */
    @RequestMapping("/export")
    public void export(HttpServletRequest req, HttpServletResponse resp,
            String export_id, String pick_id, String pick_code, String fileType)
            throws Exception
    {
        // 时间戳命名文件
        String name = String.valueOf(System.currentTimeMillis());

        // 查询配置表，获取列名
        ExportConfig exportConfig = exportConfigService
                .selectExportConfigByExportId(Integer.parseInt(export_id));

        String itemsStr = exportConfig.getItems();

        List<Map<String, String>> items = new ArrayList<Map<String, String>>();

        // 字符转为对象
        items = JSONUtil.jsonToBean(itemsStr, items.getClass());

        // 查询的数据
        List<PkgExport> exportList = exportService.pkgExportByPick(Integer
                .parseInt(pick_id));

        // 1、查询数据库查询获取key,及列名
        // 2、查询数据库获取数据
        // 3、key要匹配数据库的字段
        Map<String, String> colNameMap = new LinkedHashMap<String, String>();

        for (Map<String, String> map : items)
        {
            String col_key = map.get("col_key");
            String col_name = map.get("col_name");
            colNameMap.put(col_key, col_name);
        }

        // 创建excel文档
        Workbook workbook = ExcelWriteUtil.createExcel(fileType, name,
                colNameMap, exportList);

        String filedisplay = pick_code + "." + fileType;

        // 输出文件
        resp.setContentType("application/ms-excel");
        try
        {
            filedisplay = URLEncoder.encode(filedisplay, "UTF-8");
        } catch (UnsupportedEncodingException e1)
        {
            e1.printStackTrace();
        }

        resp.addHeader("Content-Disposition", "attachment;filename="
                + filedisplay);
        try
        {
            OutputStream out = resp.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 上传提单pdf文件
     * 
     * @param request
     * @param code
     *            提单单号,作为提单的pdf文件名
     * @return
     */
    @RequestMapping("/upload")
    @ResponseBody
    public String upload(HttpServletRequest request, String code)
    {
        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        MultipartFile file = req.getFile("flight_num_upload");

        // 获取文件后缀
        String suffix = file.getOriginalFilename()
                .substring(file.getOriginalFilename().lastIndexOf("."))
                .toLowerCase();

        // 不是pdf格式,结束上传操作
        if (!UPLOAD_SUFFIX.equals(suffix))
        {
            return "";
        }

        // 使用提单单号作为文件名称,补全pdf文件格式
        String fileName = code + suffix;
        // 2017 07 17 10 10 10
        // 上传保存到服务器物理路径去
        try
        {
            FileUtils.copyInputStreamToFile(file.getInputStream(), new File(
                    getUploadImgPath(UPLOAD_IMG_PATH + fileName)));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return UPLOAD_IMG_PATH + fileName;
    }

    private String getUploadImgPath(String uploadImgPath)
    {
        String separator = File.separator;
        return uploadImgPath.replace("\\", separator);
    }
}
