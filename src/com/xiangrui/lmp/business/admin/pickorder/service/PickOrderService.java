package com.xiangrui.lmp.business.admin.pickorder.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pickorder.vo.PickOrder;
import com.xiangrui.lmp.util.PageView;

public interface PickOrderService {

	
	/**
	 * 查询提单列表
	 * @param pickOrder
	 * @return
	 */
	List<PickOrder> query(PickOrder pickOrder);
	
	
	/**
	 * 根据参数查询提单
	 * @param params
	 * @return
	 */
	List<PickOrder> queryByMap(Map<String,Object> params);
	
	/**
	 * 保存提单
	 * @param pickOrder
	 * @return
	 */
	int save(PickOrder pickOrder);
	
	/**
	 * 查询提单详情
	 * @param params
	 * @return
	 */
	PickOrder queryDetail(Map<String,Integer> params,PageView pageView);
	
	/**
	 * 删除提单
	 * @param pickOrder
	 * @return
	 */
	int delPickOrder(PickOrder pickOrder);
	
	
	/**
	 * 更新提单
	 * @param pickOrder
	 * @return
	 */
	int updatePickOrder(PickOrder pickOrder);
	
	/**
	 * 根据参数查询提单(出库详情)
	 * @param params
	 * @return
	 */
	List<PickOrder> queryByMapOutput(Map<String,Object> params);
	
	PickOrder queryByPickCode(String pick_code);
	
	PickOrder queryByPickId(int pick_id);
}
