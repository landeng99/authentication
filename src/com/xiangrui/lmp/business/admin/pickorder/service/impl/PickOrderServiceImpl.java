package com.xiangrui.lmp.business.admin.pickorder.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.pallet.mapper.PalletMapper;
import com.xiangrui.lmp.business.admin.pallet.vo.Pallet;
import com.xiangrui.lmp.business.admin.pickorder.mapper.PickOrderMapper;
import com.xiangrui.lmp.business.admin.pickorder.service.PickOrderService;
import com.xiangrui.lmp.business.admin.pickorder.vo.PickOrder;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.content.SysContent;

@Service(value = "pickOrderService")
public class PickOrderServiceImpl implements PickOrderService
{

    @Autowired
    private PickOrderMapper pickOrderMapper;

    @Autowired
    private PalletMapper palletMapper;

    @Autowired
    private PackageMapper packageMapper;

    @Override
    public List<PickOrder> query(PickOrder pickOrder)
    {
        List<PickOrder> list = pickOrderMapper.query(pickOrder);

        return list;
    }

    @Override
    public List<PickOrder> queryByMap(Map<String, Object> params)
    {

        return pickOrderMapper.queryByMap(params);
    }

    @SystemServiceLog(description = "保存提单信息")
    @Override
    public int save(PickOrder pickOrder)
    {
        return pickOrderMapper.insertPickOrder(pickOrder);
    }

    @Override
    public PickOrder queryDetail(Map<String, Integer> params, PageView pageView)
    {
        PickOrder pickOrder = pickOrderMapper.queryDetail(params);
        Pallet pallet = new Pallet();
        pallet.setPick_id(pickOrder.getPick_id());
        pallet.setPageView(pageView);

        // 查询提单下面所有的托盘
        List<Pallet> pallets = palletMapper.query(pallet);
        pickOrder.setPallets(pallets);
        return pickOrder;
    }

    @SystemServiceLog(description = "删除提单信息")
    @Override
    public int delPickOrder(PickOrder pickOrder)
    {

        int pick_id = pickOrder.getPick_id();

        Map<String, Object> param = new HashMap<String, Object>();

        param.put("pick_id", pick_id);

        // 提单删除更新为状态为待出库
        param.put("status", Pkg.LOGISTICS_SEND_WAITING);

        packageMapper.updateStatusByPickId(param);

        // 删除托盘包裹
        palletMapper.delPalletPkgByPickId(pick_id);

        // 删除托盘
        palletMapper.delPalletByPickId(pick_id);

        // 删除托盘包裹
        int count = pickOrderMapper.delPickOrder(pickOrder);

        return count;
    }

    @SystemServiceLog(description = "修改提单信息")
    @Override
    public int updatePickOrder(PickOrder pickOrder)
    {

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pick_id", pickOrder.getPick_id());

        switch (pickOrder.getStatus())
        {

        // 已出库
        case PickOrder.PICKORDER_SENT_ALREADY:
            params.put("status", Pallet.PALLET_SENT_ALREADY);
            break;
        // 空运
        case PickOrder.PICKORDER_AIRLIFT_ALREADY:
            params.put("status", Pallet.PALLET_AIRLIFT_ALREADY);
            break;
        // 待清关
        case PickOrder.PICKORDER_CUSTOMS_ALREADY:
            params.put("status", Pallet.PALLET_CUSTOMS_ALREADY);
            break;

        default:
            break;
        }

        // 同步托盘状态
        int delPkgCnt = palletMapper.updatePalletStatusByPickId(params);

        params.remove("status");

        switch (pickOrder.getStatus())
        {

        // 已出库
        case PickOrder.PICKORDER_SENT_ALREADY:
            params.put("status", Pkg.LOGISTICS_SENT_ALREADY);
            break;
        // 空运
        case PickOrder.PICKORDER_AIRLIFT_ALREADY:
            params.put("status", Pkg.LOGISTICS_AIRLIFT_ALREADY);
            break;
        // 待清关
        case PickOrder.PICKORDER_CUSTOMS_ALREADY:
            params.put("status", Pkg.LOGISTICS_CUSTOMS_WAITING);
            break;

        default:
            break;
        }

        // 同步包裹状态
        packageMapper.updateStatusByPickId(params);

        // 同步包裹日志记录
        User user = (User) SysContent.getSession().getAttribute("back_user");
        List<Pkg> pkgs = packageMapper.queryPackageByPick_id(pickOrder
                .getPick_id());
        for (Pkg pkg : pkgs)
        {
            // 在本出的更新日志中数据库的包裹状态还没变动
            PackageLogUtil.log(pkg, user);
        }

        return pickOrderMapper.updatePickOrder(pickOrder);
    }
    
	/**
	 * 根据参数查询提单(出库详情)
	 * @param params
	 * @return
	 */
	public List<PickOrder> queryByMapOutput(Map<String,Object> params)
	{
		return pickOrderMapper.queryByMapOutput(params);
	}
	
	public PickOrder queryByPickCode(String pick_code)
	{
		return pickOrderMapper.queryByPickCode(pick_code);
	}
	
	public PickOrder queryByPickId(int pick_id)
	{
		return pickOrderMapper.queryByPickId(pick_id);
	}
}
