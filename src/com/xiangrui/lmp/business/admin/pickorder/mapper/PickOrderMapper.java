package com.xiangrui.lmp.business.admin.pickorder.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pickorder.vo.PickOrder;

public interface PickOrderMapper {
	List<PickOrder> query(PickOrder pickOrder);
	
	List<PickOrder> queryByMap(Map<String, Object> params);
	
	int insertPickOrder(PickOrder pickOrder);
	
	PickOrder queryDetail(Map<String, Integer> params);
	
	int delPickOrder(PickOrder pickOrder);
	
	int updatePickOrder(PickOrder pickOrder);
	
	List<PickOrder> queryByMapOutput(Map<String,Object> params);
	
	PickOrder queryByPickCode(String pick_code);
	
	PickOrder queryByPickId(int pick_id);
}
