package com.xiangrui.lmp.business.admin.pickorder.vo;

import java.util.List;

import com.xiangrui.lmp.business.admin.pallet.vo.Pallet;
import com.xiangrui.lmp.business.base.BasePickOrder;
import com.xiangrui.lmp.util.PageView;

public class PickOrder extends BasePickOrder {

	
    
    
    
	private PageView pageView;
	
	private List<Pallet> pallets;
	
	/**
	 * 托盘数
	 */
	private int palletCnt;
	
	/**
	 * 包裹数
	 */
	private int pkgCnt;
	
	/**
	 * 口岸名称
	 */
	private String sname;
	
	/**
	 * 包裹仓库名称
	 */
	private String warehouse;
	
	
	public PageView getPageView() {
		return pageView;
	}

	public void setPageView(PageView pageView) {
		this.pageView = pageView;
	}

	public List<Pallet> getPallets() {
		return pallets;
	}

	public void setPallets(List<Pallet> pallets) {
		this.pallets = pallets;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

    public int getPalletCnt()
    {
        return palletCnt;
    }

    public void setPalletCnt(int palletCnt)
    {
        this.palletCnt = palletCnt;
    }

    public int getPkgCnt()
    {
        return pkgCnt;
    }

    public void setPkgCnt(int pkgCnt)
    {
        this.pkgCnt = pkgCnt;
    }

    public String getWarehouse()
    {
        return warehouse;
    }

    public void setWarehouse(String warehouse)
    {
        this.warehouse = warehouse;
    }
	
}
