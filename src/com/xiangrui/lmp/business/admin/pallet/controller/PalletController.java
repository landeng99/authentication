package com.xiangrui.lmp.business.admin.pallet.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.xiangrui.lmp.business.admin.output.service.OutputService;
import com.xiangrui.lmp.business.admin.output.vo.Output;
import com.xiangrui.lmp.business.admin.pallet.service.PalletService;
import com.xiangrui.lmp.business.admin.pallet.vo.Pallet;
import com.xiangrui.lmp.business.admin.pickorder.service.PickOrderService;
import com.xiangrui.lmp.business.admin.pickorder.vo.PickOrder;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;
import com.xiangrui.lmp.business.admin.scanLog.service.OutputScanLogService;
import com.xiangrui.lmp.business.admin.scanLog.vo.OutputScanLog;
import com.xiangrui.lmp.business.admin.seaport.mapper.SeaportMapper;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportDeclarationService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.business.admin.seaport.vo.SeaportDeclaration;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.base.Region;
import com.xiangrui.lmp.business.homepage.mapper.FrontReceiveAddressMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontUserAddressMapper;
import com.xiangrui.lmp.business.homepage.vo.FrontReceiveAddress;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;
import com.xiangrui.lmp.constant.PkgConstant;
import com.xiangrui.lmp.init.SystemParamUtil;
import com.xiangrui.lmp.util.BreadcrumbUtil;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.IdentifierCreator;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.PageView;

/**
 * @author Administrator
 * 
 */

@Controller
@RequestMapping("/admin/pallet")
public class PalletController
{

	@Autowired
	private PalletService palletService;

	@Autowired
	private PackageService packageService;
	@Autowired
	private FrontUserAddressMapper frontUserAddressMapper;

	@Autowired
	private FrontReceiveAddressMapper frontReceiveAddressMapper;

	@Autowired
	private SeaportMapper seaportMapper;
	
	@Autowired
	private OutputScanLogService outputScanLogService;
	
	@Autowired
	private PickOrderService pickOrderService;
	
	@Autowired
	private OutputService outputService;
	
    @Autowired
    private SeaportDeclarationService seaportDeclarationService;
	/**
	 * 查看托盘信息列表
	 * 
	 * @param req
	 * @param resp
	 * @param pallet
	 * @return
	 */
	@RequestMapping("/list")
	public String list(HttpServletRequest req, HttpServletResponse resp, Pallet pallet)
	{

		String pageIndexStr = req.getParameter("pageIndex");
		PageView pageView = null;

		if (null != pageIndexStr && !"".equals(pageIndexStr))
		{
			int pageIndex = Integer.valueOf(pageIndexStr);
			pageView = new PageView(pageIndex);
		}
		else
		{
			pageView = new PageView(1);
		}

		pallet.setPageView(pageView);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);
		Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = (Map<Integer, List<RoleOverseasAddress>>) SystemParamUtil.getAuthority("userOverseasAddressMap");

		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("back_user");

		// 获取用户仓库权限
		List<RoleOverseasAddress> userOverseasAddressList = userOverseasAddressMap.get(user.getUser_id());
		if (userOverseasAddressList.isEmpty())
		{
			List<RoleOverseasAddress> tempList = new ArrayList<RoleOverseasAddress>();
			RoleOverseasAddress roleOverseasAddress = new RoleOverseasAddress();
			roleOverseasAddress.setId(9999);
			tempList.add(roleOverseasAddress);
			params.put("userOverseasAddressList", tempList);
		}
		else
		{
			params.put("userOverseasAddressList", userOverseasAddressList);
		}

		List<Pallet> palletList = palletService.queryAllByMap(params);
		req.setAttribute("palletList", palletList);
		req.setAttribute("pageView", pageView);
		req.setAttribute("userOverseasAddressList", userOverseasAddressList);

		BreadcrumbUtil.push(req);
		return "back/palletlist";
	}

	/**
	 * 增加托盘
	 * 
	 * @param req
	 * @param resp
	 * @param pallet
	 * @return
	 */
	@RequestMapping("/add")
	public String add(HttpServletRequest req, HttpServletResponse resp, Pallet pallet)
	{
		try
		{
			User user = (User) req.getSession().getAttribute("back_user");

			pallet.setCreate_user(user.getUser_id());
			String pallet_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PALLET);

			pallet.setPallet_code(pallet_code);

			// 出库
			pallet.setStatus(Pallet.PALLET_SENT_ALREADY);
			pallet.setCreate_time(new Timestamp(System.currentTimeMillis()));
			palletService.insert(pallet);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		String pick_id = req.getParameter("pick_id");
		String seaport_id = req.getParameter("seaport_id");
		String pick_code = req.getParameter("pick_code");
		String overseas_address_id = req.getParameter("overseas_address_id");

		pallet.setPick_code(pick_code);
		pallet.setSeaport_id(Integer.parseInt(seaport_id));
		pallet.setPick_id(Integer.parseInt(pick_id));

		req.setAttribute("pallet", pallet);
		req.setAttribute("pick_id", pick_id);
		req.setAttribute("seaport_id", seaport_id);
		req.setAttribute("pick_code", pick_code);
		req.setAttribute("overseas_address_id", overseas_address_id);
		return "back/palletedit";

	}

	/**
	 * 初始化增加页面
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("/initadd")
	public String initAdd(HttpServletRequest req, HttpServletResponse resp)
	{
		String pick_id = req.getParameter("pick_id");
		String seaport_id = req.getParameter("seaport_id");
		String pick_code = req.getParameter("pick_code");
		String pallet_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PALLET);

		req.setAttribute("pallet_code", pallet_code);
		req.setAttribute("pick_id", pick_id);
		req.setAttribute("seaport_id", seaport_id);
		req.setAttribute("pick_code", pick_code);
		BreadcrumbUtil.push(req);
		return "back/palletedit";
	}

	/**
	 * 初始化编辑页面
	 * 
	 * @param req
	 * @param resp
	 * @param pallet_id
	 * @return
	 */
	@RequestMapping("/initedit")
	public String initEdit(HttpServletRequest req, HttpServletResponse resp, int pallet_id)
	{
		Pallet param = new Pallet();
		param.setPallet_id(pallet_id);
		String pageIndexStr = req.getParameter("pageIndex");

		PageView pageView = null;

		if (null != pageIndexStr && !"".equals(pageIndexStr))
		{
			int pageIndex = Integer.valueOf(pageIndexStr);
			pageView = new PageView(pageIndex);
		}
		else
		{
			pageView = new PageView(1);
		}
		BreadcrumbUtil.push(req);
		Pallet pallet = palletService.queryPalletDetail(param, pageView);
		req.setAttribute("pallet", pallet);
		req.setAttribute("pageView", pageView);
		return "back/palletedit";
	}

	/**
	 * 编辑操作
	 * 
	 * @param req
	 * @param resp
	 * @param pallet
	 * @return
	 */
	@RequestMapping("/edit")
	public String edit(HttpServletRequest req, HttpServletResponse resp, Pallet pallet)
	{

		req.setAttribute("pallet_id", pallet.getPallet_id());
		req.setAttribute("pick_id", pallet.getPick_id());
		palletService.updatePallet(pallet);

		return "/back/savePalletSuccess";
	}

	@RequestMapping("initscanforaddpallet")
	public String initScanForAddPallet(HttpServletRequest req, HttpServletResponse resp)
	{
		String seaport_id = req.getParameter("seaport_id");
		String pick_id = req.getParameter("pick_id");
		String pallet_id = req.getParameter("pallet_id");
		req.setAttribute("seaport_id", seaport_id);
		req.setAttribute("pick_id", pick_id);
		req.setAttribute("pallet_id", pallet_id);

		return "back/scanpkgforaddpallet";
	}

	/**
	 * 扫描包裹后，查询数据库获取包裹信息， 并校验包裹是否可以加入托盘，校验通过则直接插入托盘
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("scanforaddpallet")
	@ResponseBody
	public Map<String, Object> scanForAddPallet(HttpServletRequest req, HttpServletResponse resp)
	{
		OutputScanLog outputScanLog = new OutputScanLog();
		User user = (User) req.getSession().getAttribute("back_user");
		String logistics_code = req.getParameter("logistics_code");
		String seaport_id = req.getParameter("seaport_id");
		String pick_id = req.getParameter("pick_id");

		String pallet_id = req.getParameter("pallet_id");

		int scanCount=0;
		outputScanLog.setScan_user_name(user.getUsername());
		outputScanLog.setPkg_no(logistics_code);
		outputScanLog.setAdd_pallet_success(0);
		outputScanLog.setPick_id(pick_id);
		Map<String, Object> params = new HashMap<String, Object>();

		// 包裹物流单号
		params.put("logistics_code", logistics_code);

		// 获取包裹状态为待发货的包裹
		params.put("status", Pkg.LOGISTICS_SEND_WAITING);

		params.put("seaport_id", seaport_id);
		params.put("pick_id", pick_id);
		params.put("pallet_id", pallet_id);

		Map<String, Object> result = new HashMap<String, Object>();

		// 查询包裹
		List<Pkg> pkgList = packageService.queryForScanAddPallet(params);

		if (pkgList.isEmpty())
		{
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPkgNo(logistics_code,pick_id);
			result.put("msg", "查无此号，请扫描正确的单号");
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status("无此单号");
			outputScanLog.setAdd_fail_reson("无此单号");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}

		Pkg pkg = pkgList.get(0);
		outputScanLog.setPkg_id(pkg.getPackage_id());
		
		String need_check_out_flag="N";
		if("Y".equals(pkg.getNeed_check_out_flag())){
			need_check_out_flag="Y";
			result.put("need_check_out_flag", need_check_out_flag);
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),pick_id);
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("该包裹为异常件，需要取出不可出库");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);			
			return result;
		}


		// 校验包裹是否已经被放入其他托盘
		params.put("package_id", pkg.getPackage_id());
		boolean isPkgExist = palletService.isPkgInfoExist(params);
		if (isPkgExist)
		{
			result.put("msg", "包裹已经加入托盘，请继续扫描");
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),pick_id);
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("包裹已经加入托盘，请继续扫描");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}
		
		// 如果包裹非待入库状态，则提示用户为
		if (pkg.getStatus() != Pkg.LOGISTICS_SEND_WAITING)
		{
			result.put("msg", "包裹非待发货状态，请扫描其他包裹");
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),pick_id);
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("包裹非待发货状态，请扫描其他包裹");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}
		
		// 校验用户信息是否超过规定数量
		boolean isUserInfoExist = palletService.isPkgUserInfoExist(params, pkgList);

		if (isUserInfoExist)
		{
			result.put("msg", "包裹用户信息超过规定数量");
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),pick_id);
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("包裹用户信息超过规定数量");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}

		Seaport seaport = seaportMapper.querySeaportBySid(Integer.parseInt(seaport_id));

		if (seaport.getIscheck_idcard_img() == 1)//需要检查身份证照片
		{
			// 根据用户地址创建包裹地址
			FrontUserAddress frontUserAddress = frontUserAddressMapper.queryUseraddress(pkg.getAddress_id());

			// 身份证不存在，需要用收件人和手机号码去系统查检是否已经填写过身份证号码
			boolean needToWardIdcardMsg = false;
			if (StringUtils.trimToNull(frontUserAddress.getFront_img()) == null || StringUtils.trimToNull(frontUserAddress.getBack_img()) == null)
			{
				needToWardIdcardMsg = true;
				Map<String, Object> tparams = new HashMap<String, Object>();
				tparams.put("mobile", frontUserAddress.getMobile());
				tparams.put("receiver", frontUserAddress.getReceiver());
				List<FrontUserAddress> addresslist = frontUserAddressMapper.queryAddressByMobileAndReceiver(tparams);
				if (addresslist != null && addresslist.size() > 0)
				{
					for (FrontUserAddress tfrontUserAddress : addresslist)
					{
						if (StringUtils.trimToNull(tfrontUserAddress.getFront_img()) != null && StringUtils.trimToNull(tfrontUserAddress.getBack_img()) != null)
						{
							needToWardIdcardMsg = false;
						}
					}
				}
				if (!needToWardIdcardMsg)
				{
					List<FrontReceiveAddress> receiveAddresslist = frontReceiveAddressMapper.queryReceiveAddressByMobileAndReceiver(tparams);
					if (receiveAddresslist != null && receiveAddresslist.size() > 0)
					{
						for (FrontReceiveAddress frontReceiveAddress : receiveAddresslist)
						{
							if (StringUtils.trimToNull(frontReceiveAddress.getFront_img()) != null && StringUtils.trimToNull(frontReceiveAddress.getBack_img()) != null)
							{
								needToWardIdcardMsg = false;
							}
						}
					}
				}
			}
			if (needToWardIdcardMsg)
			{
				result.put("msg", "包裹无身份证图片！");
				scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),pick_id);
				outputScanLog.setAdd_pallet_success(0);
				outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
				outputScanLog.setAdd_fail_reson("包裹无身份证图片！");
				outputScanLog.setScan_count(++scanCount);
				outputScanLogService.insertOutputScanLog(outputScanLog);
				return result;
			}

		}

		List<Map<String, Object>> pkgParams = new ArrayList<Map<String, Object>>();

		for (Pkg pkgTemp : pkgList)
		{
			Map<String, Object> pkgParam = new HashMap<String, Object>();
			int package_id = pkgTemp.getPackage_id();
			pkgParam.put("package_id", package_id);
			pkgParam.put("pallet_id", pallet_id);
			pkgParams.add(pkgParam);

			List<Region> regionlist = (List<Region>) JSONTool.getDTOList(pkg.getRegion(), Region.class);

			StringBuffer buffer = new StringBuffer();

			// 拼接地址信息
			for (Region region : regionlist)
			{
				buffer.append(region.getName());
				buffer.append(",");
			}
			String street = pkgTemp.getStreet();
			buffer.append(street == null ? "" : street);
			pkgTemp.setAddress(buffer.toString());
			
//			if(seaport.getExpress_seaport()==1)
//			{
//				if(pkgTemp.getExpress_package()!=1)
//				{
//					result.put("msg", "该包裹不是快件包裹，不能加入此提单中！");
//					scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),pick_id);
//					outputScanLog.setAdd_pallet_success(0);
//					outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
//					outputScanLog.setAdd_fail_reson("该包裹不是快件包裹，不能加入此提单中！");
//					outputScanLog.setScan_count(++scanCount);
//					outputScanLogService.insertOutputScanLog(outputScanLog);
//					return result;
//				}
//			}
//			if(seaport.getExpress_seaport()==0)
//			{
//				if(pkgTemp.getExpress_package()!=0)
//				{
//					result.put("msg", "该包裹是快件包裹，不能加入此提单中！");
//					scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),pick_id);
//					outputScanLog.setAdd_pallet_success(0);
//					outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
//					outputScanLog.setAdd_fail_reson("该包裹是快件包裹，不能加入此提单中！");
//					outputScanLog.setScan_count(++scanCount);
//					outputScanLogService.insertOutputScanLog(outputScanLog);
//					return result;
//				}
//			}
		}
		

		// 满足入库条件，将包裹加入托盘中
		palletService.insertPalletPkg(pkgParams, user);
		result.put("pkgList", pkgList);
		scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),pick_id);
		outputScanLog.setAdd_pallet_success(1);
		outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
		outputScanLog.setScan_count(++scanCount);
		outputScanLog.setPkg_id(pkg.getPackage_id());
		outputScanLogService.insertOutputScanLog(outputScanLog);
		return result;
	}

	/**
	 * 用于托盘扫描功能
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/delpkg")
	@ResponseBody
	public Map<String, Object> delPkg(HttpServletRequest request)
	{

		Map<String, Object> result = new HashMap<String, Object>();

		Map<String, Object> pkgParam = new HashMap<String, Object>();

		pkgParam.put("package_id", request.getParameter("package_id"));
		pkgParam.put("pallet_id", request.getParameter("pallet_id"));
		User user = (User) request.getSession().getAttribute("back_user");
		int cnt = palletService.delPkg(pkgParam, user);

		if (cnt >= 0)
		{
			result.put("result", true);
		}
		else
		{
			result.put("result", false);
		}
		return result;
	}

	/**
	 * 托盘列表中的删除
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/delPalletPkg")
	public ModelAndView delPalletPkg(HttpServletRequest request)
	{

		Map<String, Object> pkgParam = new HashMap<String, Object>();

		String package_id = request.getParameter("package_id");
		String pallet_id = request.getParameter("pallet_id");
		String pick_id = request.getParameter("pick_id");
		pkgParam.put("package_id", package_id);
		pkgParam.put("pallet_id", pallet_id);

		User user = (User) request.getSession().getAttribute("back_user");
		int cnt = palletService.delPkg(pkgParam, user);
		ModelMap mlMap = new ModelMap();
		mlMap.put("pallet_id", pallet_id);
		mlMap.put("pick_id", pick_id);
		return new ModelAndView("redirect:/admin/pallet/initedit", mlMap);

	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryforaddpallet")
	public String queryForAddPallet(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);

		// 包裹支付状态为已付款的
		params.put("pay_status", PkgConstant.PAYMENT_PAID);

		// 获取包裹状态为待发货的包裹
		params.put("status", PkgConstant.LOGISTICS_SEND_WAITING);

		// 根据提单仓库查询包裹
		String overseas_address_id = request.getParameter("overseas_address_id");
		params.put("overseas_address_id", overseas_address_id);

		List<Pkg> pkgList = packageService.queryForAddPallet(params);

		for (Pkg pkgTemp : pkgList)
		{
			Map<String, Object> pkgParam = new HashMap<String, Object>();
			int package_id = pkgTemp.getPackage_id();
			pkgParam.put("package_id", package_id);
			List<Region> regionlist = null;
			regionlist = (List<Region>) JSONTool.getDTOList(pkgTemp.getRegion(), Region.class);

			StringBuffer buffer = new StringBuffer();

			buffer.append("");

			// 拼接地址信息
			for (Region region : regionlist)
			{
				buffer.append(region.getName());
				buffer.append(",");
			}
			String street = pkgTemp.getStreet();
			buffer.append(street == null ? "" : street);
			pkgTemp.setAddress(buffer.toString());
		}

		String seaport_id = request.getParameter("seaport_id");
		String pick_id = request.getParameter("pick_id");
		String pallet_id = request.getParameter("pallet_id");
		request.setAttribute("seaport_id", seaport_id);
		request.setAttribute("pick_id", pick_id);
		request.setAttribute("pallet_id", pallet_id);
		request.setAttribute("pkgList", pkgList);
		request.setAttribute("pageView", pageView);
		request.setAttribute("overseas_address_id", overseas_address_id);
		BreadcrumbUtil.push(request);
		return "back/pkglistforaddpallet";
	}

	/**
	 * 批量增加包裹到托盘
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("/addpkgbatch")
	@ResponseBody
	public Map<String, Object> addpkgbatch(HttpServletRequest req, HttpServletResponse resp)
	{

		String package_str = req.getParameter("package_str");
		String seaport_id = req.getParameter("seaport_id");
		String pick_id = req.getParameter("pick_id");
		String pallet_id = req.getParameter("pallet_id");

		Map<String, Object> params = new HashMap<String, Object>();
		// 获取包裹状态为待发货的包裹
		params.put("status", Pkg.LOGISTICS_SEND_WAITING);

		params.put("seaport_id", seaport_id);
		params.put("pick_id", pick_id);
		params.put("pallet_id", pallet_id);

		Map<String, Object> result = new HashMap<String, Object>();

		result.put("result", true);
		result.put("msg", "包裹可放入托盘");

		boolean isPkgExist = false;
		boolean isUserInfoExist = false;

		params.put("seaport_id", seaport_id);
		params.put("pick_id", pick_id);
		params.put("pallet_id", pallet_id);

		List<Pkg> pkgs = JSONTool.getDTOList(package_str, Pkg.class);

		// 检查包裹是否已经存在
		for (Pkg pkg : pkgs)
		{
			params.put("package_id", pkg.getPackage_id());
			isPkgExist = palletService.isPkgInfoExist(params);
			if (isPkgExist)
			{
				result.put("result", false);
				result.put("msg", "编号为" + pkg.getLogistics_code() + "的包裹已经被放入托盘,请选择其他包裹");
				return result;
			}
		}

		isUserInfoExist = palletService.isPkgUserInfoExist(params, pkgs);
		if (isUserInfoExist)
		{
			result.put("result", false);
			result.put("msg", "包裹用户信息超过口岸规定数量");
			return result;
		}

		List<Map<String, Object>> pkgParams = new ArrayList<Map<String, Object>>();
		for (Pkg pkg : pkgs)
		{
			Map<String, Object> pkgParam = new HashMap<String, Object>();
			int package_id = pkg.getPackage_id();
			pkgParam.put("package_id", package_id);
			pkgParam.put("pallet_id", pallet_id);
			pkgParams.add(pkgParam);
		}
		User user = (User) req.getSession().getAttribute("back_user");
		palletService.insertPalletPkg(pkgParams, user);
		return result;

	}

	/**
	 * 
	 * @param req
	 * @param resp
	 * @param pkg
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/searchforaddpallet")
	public String searchForAddPallet(HttpServletRequest req, HttpServletResponse resp, Pkg pkg, String fromDate, String toDate)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = req.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);

		// 包裹物料单号
		params.put("logistics_code", pkg.getLogistics_code());

		if (!"".equals(fromDate))
		{

			// 起始时间
			params.put("fromDate", fromDate);
		}
		else
		{

			params.put("fromDate", null);
		}

		if (!"".equals(toDate))
		{
			// 终止时间
			params.put("toDate", toDate);
		}
		else
		{
			params.put("toDate", null);
		}

		// 获取包裹状态为待发货的包裹
		params.put("status", PkgConstant.LOGISTICS_SEND_WAITING);

		// 根据提单仓库查询包裹
		String overseas_address_id = req.getParameter("overseas_address_id");

		params.put("overseas_address_id", overseas_address_id);

		List<Pkg> pkgList = packageService.queryForAddPallet(params);

		for (Pkg pkgTemp : pkgList)
		{
			Map<String, Object> pkgParam = new HashMap<String, Object>();
			int package_id = pkgTemp.getPackage_id();
			pkgParam.put("package_id", package_id);

			List<Region> regionlist = null;
			regionlist = (List<Region>) JSONTool.getDTOList(pkgTemp.getRegion(), Region.class);
			StringBuffer buffer = new StringBuffer();

			buffer.append("");

			// 拼接地址信息
			for (Region region : regionlist)
			{
				buffer.append(region.getName());
				buffer.append(",");
			}
			String street = pkgTemp.getStreet();
			buffer.append(street == null ? "" : street);
			pkgTemp.setAddress(buffer.toString());
		}
		String seaport_id = req.getParameter("seaport_id");
		String pick_id = req.getParameter("pick_id");
		String pallet_id = req.getParameter("pallet_id");
		req.setAttribute("seaport_id", seaport_id);
		req.setAttribute("pick_id", pick_id);
		req.setAttribute("pallet_id", pallet_id);
		req.setAttribute("pageView", pageView);
		req.setAttribute("overseas_address_id", overseas_address_id);
		req.setAttribute("pkgList", pkgList);
		BreadcrumbUtil.push(req);
		return "back/pkglistforaddpallet";
	}

	/**
	 * 
	 * 将包裹放入托盘之前先判断包裹信息：手机号码、身份证、地址
	 * 
	 * @param req
	 * @param resp
	 * @param pkgs
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/validatepkg")
	public Map<String, Object> validatePkg(HttpServletRequest req, HttpServletResponse resp)
	{
		String packages_str = req.getParameter("packages_str");
		String seaport_id = req.getParameter("seaport_id");
		String pick_id = req.getParameter("pick_id");
		String pallet_id = req.getParameter("pallet_id");
		Pkg[] pkgs = null;
		try
		{
			pkgs = JSONUtil.jsonToBean(packages_str, Pkg[].class);

		} catch (Exception e)
		{

			e.printStackTrace();
		}

		Map<String, Object> result = new HashMap<String, Object>();

		result.put("result", true);
		result.put("msg", "包裹可放入托盘");

		boolean isPkgExist = false;
		boolean isUserInfoExist = false;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("seaport_id", seaport_id);
		params.put("pick_id", pick_id);
		params.put("pallet_id", pallet_id);

		// 检查包裹是否已经存在
		for (Pkg pkg : pkgs)
		{
			params.put("package_id", pkg.getPackage_id());
			isPkgExist = palletService.isPkgInfoExist(params);
			if (isPkgExist)
			{
				result.put("result", false);
				result.put("msg", "编号为" + pkg.getLogistics_code() + "的包裹已经被放入托盘,请选择其他包裹");
				return result;
			}

		}
		List<Pkg> list = Arrays.asList(pkgs);
		isUserInfoExist = palletService.isPkgUserInfoExist(params, list);
		if (isUserInfoExist)
		{
			result.put("result", false);
			result.put("msg", "包裹用户信息超过口岸规定数量");
			return result;
		}
		return result;
	}

	/**
	 * 托盘详情
	 * 
	 * @param req
	 * @param resp
	 * @param pallet_id
	 * @return
	 */
	@RequestMapping("/detail")
	public String detail(HttpServletRequest req, HttpServletResponse resp, int pallet_id)
	{
		Pallet param = new Pallet();
		param.setPallet_id(pallet_id);
		String pageIndexStr = req.getParameter("pageIndex");
		PageView pageView = null;

		if (null != pageIndexStr && !"".equals(pageIndexStr))
		{
			int pageIndex = Integer.valueOf(pageIndexStr);
			pageView = new PageView(pageIndex);
		}
		else
		{
			pageView = new PageView(1);
		}

		Pallet pallet = palletService.queryPalletDetail(param, pageView);
		req.setAttribute("pallet", pallet);
		req.setAttribute("pageView", pageView);
		return "back/palletdetail";
	}

	@RequestMapping("/search")
	public String search(HttpServletRequest req, HttpServletResponse resp)
	{

		String pageIndexStr = req.getParameter("pageIndex");
		PageView pageView = null;

		if (null != pageIndexStr && !"".equals(pageIndexStr))
		{
			int pageIndex = Integer.valueOf(pageIndexStr);
			pageView = new PageView(pageIndex);
		}
		else
		{
			pageView = new PageView(1);
		}
		String pick_code = req.getParameter("pick_code");
		if (pick_code != null)
		{
			pick_code = pick_code.trim();
		}

		String pallet_code = req.getParameter("pallet_code");

		if (pallet_code != null)
		{
			pallet_code = pallet_code.trim();
		}

		String seaport_id = req.getParameter("seaport_id");
		String logistics_code = req.getParameter("logistics_code");

		if (logistics_code != null)
		{
			logistics_code = logistics_code.trim();
		}
		String status = req.getParameter("status");

		String fromDateStr = req.getParameter("fromDate");

		String toDateStr = req.getParameter("toDate");
		String overseas_address_id = req.getParameter("overseas_address_id");
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pick_code", pick_code);
		params.put("pallet_code", pallet_code);
		params.put("logistics_code", logistics_code);
		params.put("seaport_id", seaport_id);
		params.put("status", status);
		params.put("overseas_address_id", overseas_address_id);
		params.put("pageView", pageView);

		// 当前请求放入面包切中
		BreadcrumbUtil.push(req);

		Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = (Map<Integer, List<RoleOverseasAddress>>) SystemParamUtil.getAuthority("userOverseasAddressMap");

		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("back_user");

		// 获取用户仓库权限
		List<RoleOverseasAddress> userOverseasAddressList = userOverseasAddressMap.get(user.getUser_id());
		if (userOverseasAddressList.isEmpty())
		{
			List<RoleOverseasAddress> tempList = new ArrayList<RoleOverseasAddress>();
			RoleOverseasAddress roleOverseasAddress = new RoleOverseasAddress();
			roleOverseasAddress.setId(9999);
			tempList.add(roleOverseasAddress);
			params.put("userOverseasAddressList", tempList);
		}
		else
		{
			params.put("userOverseasAddressList", userOverseasAddressList);
		}

		if (fromDateStr != null && !"".equals(fromDateStr))
		{
			Date fromDate = DateUtil.getDateFromString(fromDateStr, "yyyy-MM-dd HH:mm:ss");
			params.put("fromDate", fromDate);
		}
		if (toDateStr != null && !"".equals(toDateStr))
		{
			Date toDate = DateUtil.getDateFromString(toDateStr, "yyyy-MM-dd HH:mm:ss");
			params.put("toDate", toDate);
		}
		List<Pallet> palletList = palletService.queryAllByMap(params);

		req.setAttribute("params", params);
		req.setAttribute("palletList", palletList);
		req.setAttribute("pageView", pageView);
		req.setAttribute("userOverseasAddressList", userOverseasAddressList);
		return "back/palletlist";
	}

	@RequestMapping("/del")
	public String del(HttpServletRequest req, HttpServletResponse resp, Pallet pallet)
	{

		palletService.delPallet(pallet);
		return "redirect:/admin/pallet/list";
	}

	/**
	 * 新版出库扫描初始化
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("initscanforaddpallet_new")
	public String initScanForAddPallet_new(HttpServletRequest req, HttpServletResponse resp)
	{
		String output_id = req.getParameter("output_id");
		req.setAttribute("output_id", output_id);
		return "back/scanpkgforaddpallet_new";
	}
	
	/**
	 * 新版删除托盘包裹功能
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/delPkg_new")
	@ResponseBody
	public Map<String, Object> delPkg_new(HttpServletRequest request)
	{

		Map<String, Object> result = new HashMap<String, Object>();

		Map<String, Object> pkgParam = new HashMap<String, Object>();
		String seaport_id = request.getParameter("seaport_id");
		String output_id = request.getParameter("output_id");
		Pallet pallet = palletService.queryPalletBySeaportIdAndOutputId(seaport_id, output_id);
		String palletId = "-1";
		if (pallet != null)
		{
			palletId = "" + pallet.getPallet_id();

			pkgParam.put("package_id", request.getParameter("package_id"));
			pkgParam.put("pallet_id", palletId);
			User user = (User) request.getSession().getAttribute("back_user");
			int cnt = palletService.delPkg(pkgParam, user);

			if (cnt >= 0)
			{
				result.put("result", true);
			}
			else
			{
				result.put("result", false);
			}
		}
		else
		{
			result.put("result", false);
		}
		return result;
	}
	
	/**
	 * 新版出库扫描
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("scanForAddPallet_new")
	@ResponseBody
	public Map<String, Object> scanForAddPallet_new(HttpServletRequest req, HttpServletResponse resp)
	{
		OutputScanLog outputScanLog = new OutputScanLog();
		User user = (User) req.getSession().getAttribute("back_user");
		String logistics_code = req.getParameter("logistics_code");
		String output_id = req.getParameter("output_id");
		String spPickId="out"+output_id;
		
		Output output=outputService.queryOutputBySeqId(Integer.parseInt(output_id));
		
		int scanCount=0;
		outputScanLog.setScan_user_name(user.getUsername());
		outputScanLog.setPkg_no(logistics_code);
		outputScanLog.setAdd_pallet_success(0);
		outputScanLog.setPick_id(spPickId);
		Map<String, Object> params = new HashMap<String, Object>();

		// 包裹物流单号
		params.put("logistics_code", logistics_code);

		// 获取包裹状态为待发货的包裹
		params.put("status", Pkg.LOGISTICS_SEND_WAITING);

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("seaport_name", "");

		// 查询包裹
		List<Pkg> pkgList = packageService.queryForScanAddPallet(params);

		if (pkgList.isEmpty())
		{
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPkgNo(logistics_code,spPickId);
			result.put("msg", "查无此号，请扫描正确的单号");
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status("无此单号");
			outputScanLog.setAdd_fail_reson("无此单号");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}

		Pkg pkg = pkgList.get(0);
		outputScanLog.setPkg_id(pkg.getPackage_id());
		
		int seaport_id=pkg.getSeaport_id();//包裹所属口岸ID
		params.put("seaport_id", ""+seaport_id);
		
		String outputSeaportIdListStr=StringUtils.trimToEmpty(output.getSeaport_id_list());
		String[] outputSeaportIdArray=outputSeaportIdListStr.split(",");
		List<String> outputSeaportIdList=Arrays.asList(outputSeaportIdArray);
		
		Seaport seaport = seaportMapper.querySeaportBySid(seaport_id);
		//口岸名称
		result.put("seaport_name", seaport.getSname());
		
		//出库口岸检查
		if(!outputSeaportIdList.contains(""+seaport_id))
		{
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPkgNo(logistics_code,spPickId);
			result.put("msg", "包裹非本次出库口岸，无法出库！");
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("包裹非本次出库口岸，无法出库！");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}
		
		Pallet outputPallet=palletService.queryPalletBySeaportIdAndOutputId(""+seaport_id, output_id);
		String pallet_id=""+outputPallet.getPallet_id();
		params.put("pick_id", ""+outputPallet.getPick_id());
		params.put("pallet_id", pallet_id);
		
		String need_check_out_flag="N";
		if("Y".equals(pkg.getNeed_check_out_flag())){
			need_check_out_flag="Y";
			result.put("need_check_out_flag", need_check_out_flag);
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("该包裹为异常件，需要取出不可出库");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			result.put("pkgList", pkgList);
			return result;
		}


		// 校验包裹是否已经被放入其他托盘
		params.put("package_id", pkg.getPackage_id());
		boolean isPkgExist = palletService.isPkgInfoExist(params);
		if (isPkgExist)
		{
			result.put("msg", "包裹已经加入托盘，请继续扫描");
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("包裹已经加入托盘，请继续扫描");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}
		
		// 如果包裹非待入库状态，则提示用户为
		if (pkg.getStatus() != Pkg.LOGISTICS_SEND_WAITING)
		{
			result.put("msg", "包裹非待发货状态，请扫描其他包裹");
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("包裹非待发货状态，请扫描其他包裹");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}
		
		// 校验用户信息是否超过规定数量
		boolean isUserInfoExist = palletService.isPkgUserInfoExist(params, pkgList);

		if (isUserInfoExist)
		{
			result.put("msg", "包裹用户信息超过规定数量");
			scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
			outputScanLog.setAdd_pallet_success(0);
			outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
			outputScanLog.setAdd_fail_reson("包裹用户信息超过规定数量");
			outputScanLog.setScan_count(++scanCount);
			outputScanLogService.insertOutputScanLog(outputScanLog);
			return result;
		}



		if (seaport.getIscheck_idcard_img() == 1)//需要检查身份证照片
		{
			// 根据用户地址创建包裹地址
			FrontUserAddress frontUserAddress = frontUserAddressMapper.queryUseraddress(pkg.getAddress_id());

			// 身份证不存在，需要用收件人和手机号码去系统查检是否已经填写过身份证号码
			boolean needToWardIdcardMsg = false;
			if (StringUtils.trimToNull(frontUserAddress.getFront_img()) == null || StringUtils.trimToNull(frontUserAddress.getBack_img()) == null)
			{
				needToWardIdcardMsg = true;
				Map<String, Object> tparams = new HashMap<String, Object>();
				tparams.put("mobile", frontUserAddress.getMobile());
				tparams.put("receiver", frontUserAddress.getReceiver());
				List<FrontUserAddress> addresslist = frontUserAddressMapper.queryAddressByMobileAndReceiver(tparams);
				if (addresslist != null && addresslist.size() > 0)
				{
					for (FrontUserAddress tfrontUserAddress : addresslist)
					{
						if (StringUtils.trimToNull(tfrontUserAddress.getFront_img()) != null && StringUtils.trimToNull(tfrontUserAddress.getBack_img()) != null)
						{
							needToWardIdcardMsg = false;
						}
					}
				}
				if (!needToWardIdcardMsg)
				{
					List<FrontReceiveAddress> receiveAddresslist = frontReceiveAddressMapper.queryReceiveAddressByMobileAndReceiver(tparams);
					if (receiveAddresslist != null && receiveAddresslist.size() > 0)
					{
						for (FrontReceiveAddress frontReceiveAddress : receiveAddresslist)
						{
							if (StringUtils.trimToNull(frontReceiveAddress.getFront_img()) != null && StringUtils.trimToNull(frontReceiveAddress.getBack_img()) != null)
							{
								needToWardIdcardMsg = false;
							}
						}
					}
				}
			}
			if (needToWardIdcardMsg)
			{
				result.put("msg", "包裹无身份证图片！");
				scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
				outputScanLog.setAdd_pallet_success(0);
				outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
				outputScanLog.setAdd_fail_reson("包裹无身份证图片！");
				outputScanLog.setScan_count(++scanCount);
				outputScanLogService.insertOutputScanLog(outputScanLog);
				return result;
			}

		}

		List<Map<String, Object>> pkgParams = new ArrayList<Map<String, Object>>();

		for (Pkg pkgTemp : pkgList)
		{
			Map<String, Object> pkgParam = new HashMap<String, Object>();
			int package_id = pkgTemp.getPackage_id();
			pkgParam.put("package_id", package_id);
			pkgParam.put("pallet_id", pallet_id);
			pkgParams.add(pkgParam);

			List<Region> regionlist = (List<Region>) JSONTool.getDTOList(pkg.getRegion(), Region.class);

			StringBuffer buffer = new StringBuffer();

			// 拼接地址信息
			for (Region region : regionlist)
			{
				buffer.append(region.getName());
				buffer.append(",");
			}
			String street = pkgTemp.getStreet();
			buffer.append(street == null ? "" : street);
			pkgTemp.setAddress(buffer.toString());
			
/*			if(seaport.getExpress_seaport()==1)
			{
				if(pkgTemp.getExpress_package()!=1)
				{
					result.put("msg", "该包裹不是快件包裹，不能加入此提单中！");
					scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
					outputScanLog.setAdd_pallet_success(0);
					outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
					outputScanLog.setAdd_fail_reson("该包裹不是快件包裹，不能加入此提单中！");
					outputScanLog.setScan_count(++scanCount);
					outputScanLogService.insertOutputScanLog(outputScanLog);
					return result;
				}
			}
			if(seaport.getExpress_seaport()==0)
			{
				if(pkgTemp.getExpress_package()!=0)
				{
					result.put("msg", "该包裹是快件包裹，不能加入此提单中！");
					scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
					outputScanLog.setAdd_pallet_success(0);
					outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
					outputScanLog.setAdd_fail_reson("该包裹是快件包裹，不能加入此提单中！");
					outputScanLog.setScan_count(++scanCount);
					outputScanLogService.insertOutputScanLog(outputScanLog);
					return result;
				}
			}*/
			
//			if (pkgTemp.getExpress_package() == 1)
//			{
//				if (seaport.getExpress_seaport() != 1)
//				{
//					result.put("msg", "该包裹是快件包裹，不能加入此提单中！");
//					scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
//					outputScanLog.setAdd_pallet_success(0);
//					outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
//					outputScanLog.setAdd_fail_reson("该包裹是快件包裹，不能加入此提单中！");
//					outputScanLog.setScan_count(++scanCount);
//					outputScanLogService.insertOutputScanLog(outputScanLog);
//					return result;
//				}
//			}
		}
		SeaportDeclaration existSeaportDeclaration=seaportDeclarationService.queryDeclarationBySeaportIdAndPackageId(seaport_id, pkg.getPackage_id());
		if (existSeaportDeclaration == null)
		{
			int seaportDeclarationLeave = seaportDeclarationService.countLeaveSeaportDeclarationBySeaportId(seaport.getSid());
			if (seaportDeclarationLeave <= 0)
			{
				result.put("msg", "报关号码已分配完！");
				scanCount = outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(), spPickId);
				outputScanLog.setAdd_pallet_success(0);
				outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
				outputScanLog.setAdd_fail_reson("报关号码已分配完！");
				outputScanLog.setScan_count(++scanCount);
				outputScanLogService.insertOutputScanLog(outputScanLog);
				return result;
			}

			String needLeaveWarning = "N";// 报关号码剩余提醒
			if (seaportDeclarationLeave - 1 <= seaport.getLeave_warning_count())
			{
				needLeaveWarning = "Y";
			}
			result.put("needLeaveWarning", needLeaveWarning);
		}
		//是否自动打印
		result.put("auto_print", output.getAuto_print());
		
		// 满足入库条件，将包裹加入托盘中
		palletService.insertPalletPkg(pkgParams, user);
		result.put("pkgList", pkgList);
		scanCount=outputScanLogService.countCurrentDateOutputScanLogByPacakgeId(pkg.getPackage_id(),spPickId);
		outputScanLog.setAdd_pallet_success(1);
		outputScanLog.setPkg_status(PackageLogUtil.getDescription(pkg.getStatus()));
		outputScanLog.setScan_count(++scanCount);
		outputScanLog.setPkg_id(pkg.getPackage_id());
		outputScanLogService.insertOutputScanLog(outputScanLog);
		
		PickOrder pickOrder=pickOrderService.queryByPickId(outputPallet.getPick_id());
		for (Pkg pkgTemp : pkgList)
		{
			pkgTemp.setPick_code(pickOrder.getPick_code());
			
			if(existSeaportDeclaration==null)
			{
				SeaportDeclaration seaportDeclaration=seaportDeclarationService.queryCurrentUseDeclarationBySeaportId(seaport_id);
				seaportDeclaration.setPackage_id(pkgTemp.getPackage_id());
				pkgTemp.setDeclaration_code(seaportDeclaration.getDeclaration_code());
				//自动分配报关单号
				seaportDeclarationService.useSeaportDeclaration(seaportDeclaration);				
			}else
			{
				pkgTemp.setDeclaration_code(existSeaportDeclaration.getDeclaration_code());
			}
		}
		return result;
	}
	
	@RequestMapping("/initScanForAddPallet_newLoadData")
	@ResponseBody
	public Map<String, Object> initScanForAddPallet_newLoadData(HttpServletRequest request)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		String output_id = request.getParameter("output_id");
		List<Pkg> pkgList = packageService.queryForScanAddPalletNewInit(Integer.parseInt(output_id));
		result.put("pkgList", pkgList);
		return result;
	}
}
