package com.xiangrui.lmp.business.admin.pallet.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pallet.vo.Pallet;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.PageView;

public interface PalletService
{

    /**
     * 查询托盘
     * 
     * @param pallet
     * @return
     */
    List<Pallet> queryAll(Pallet pallet);

    List<Pallet> queryAllByMap(Map<String, Object> params);

    /**
     * 创建托盘
     * 
     * @param pallet
     * @return
     */
    int insert(Pallet pallet);

    /**
     * 检查包裹及包裹是否已经放入其他托盘
     * 
     * @param pkg
     * @return
     */
    boolean isPkgInfoExist(Map<String, Object> params);

    /**
     * 检查包裹信息是否重复
     * 
     * @param params
     * @return
     */
    boolean isPkgUserInfoExist(Map<String, Object> params, List<Pkg> list);

    /**
     * 
     * 删除托盘
     * 
     * @param pallet
     * @return
     */
    int delPallet(Pallet pallet);

    /**
     * 查询托盘详情
     * 
     * @param pallet
     * @return
     */
    Pallet queryPalletDetail(Pallet pallet, PageView pageView);

    /**
     * 编辑托盘
     * 
     * @param pallet
     * @return
     */
    boolean updatePallet(Pallet pallet);

    /**
     * 增加托盘包裹
     * 
     * @param pkg
     * @param user
     *            同步包裹日志的操作人
     * @return
     */
    int insertPalletPkg(List<Map<String, Object>> pkgParams, User user);

    /**
     * 删除托盘中的某些包裹
     * 
     * @param pkgParam
     * @param user
     *            同步包裹日志的操作人
     * @return
     */
    int delPkg(Map<String, Object> pkgParam, User user);
    
    Pallet queryPalletBySeaportIdAndOutputId(String seaport_id,String output_id);
}
