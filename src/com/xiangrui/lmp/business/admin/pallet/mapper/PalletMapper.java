package com.xiangrui.lmp.business.admin.pallet.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pallet.vo.Pallet;

public interface PalletMapper
{
    /**
     * 查询托盘
     * 
     * @param pallet
     * @return
     */
    List<Pallet> query(Pallet pallet);

    /**
     * 查询托盘
     * 
     * @param pallet
     * @return
     */
    List<Pallet> queryAll(Pallet pallet);

    /**
     * 创建托盘
     * 
     * @param pallet
     * @return
     */
    int insertPallet(Pallet pallet);

    int isPkgExist(Map<String, Object> params);

    int isUserInfoExist(Map<String, Object> params);

    /**
     * 创建保存托盘包裹
     * 
     * @param param
     * @return
     */
    int insertPalletPkg(Map<String, Object> param);

    /**
     * 删除托盘
     * 
     * @param pallet
     * @return
     */
    int delPallet(Pallet pallet);

    /**
     * 托盘包裹删除
     * 
     * @param params
     * @return
     */
    int delPalletPkg(Pallet pallet);

    /**
     * 根据提单删除托盘包裹
     * 
     * @param pick_id
     * @return
     */
    int delPalletPkgByPickId(int pick_id);

    /**
     * 根据提单删除托盘
     * 
     * @param pick_id
     * @return
     */
    int delPalletByPickId(int pick_id);

    /**
     * 查询托盘详情
     * 
     * @param pallet
     * @return
     */
    Pallet queryPalletDetail(Pallet pallet);

    /**
     * 修改托盘信息
     * 
     * @param pallet
     * @return
     */
    int updatePallet(Pallet pallet);

    /**
     * 更新同一提单下所有托盘的物流状态
     * 
     * @param params
     * @return
     */
    int updatePalletStatusByPickId(Map<String, Object>params);
    
  /*  
    *//**根据参数查询提单
     * @param params
     * @return
     */
    List<Pallet> queryAllByMap(Map<String, Object> params);
    
    /**删除托盘某个包裹
     * @param pkgParam
     * @return
     */
    int delPkg(Map<String, Object> pkgParam);
    
    List<Pallet> queryPalletBySeaportIdAndOutputId(Map<String, Object> pkgParam);

}
