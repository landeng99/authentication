package com.xiangrui.lmp.business.admin.pallet.vo;

import java.util.List;

import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.base.BasePallet;
import com.xiangrui.lmp.util.PageView;

public class Pallet extends BasePallet {
	
	private List<Pkg> pkgs;
	
	private String pick_code;
	
	private int seaport_id;
	
	private PageView pageView ;
	
	/**
	 *包裹数量
	 */
	private int pkgCnt;
	
	private String pkgsData;
	
	private int overseas_address_id;
	   
    /**
     * 包裹仓库名称
     */
    private String warehouse;

	public List<Pkg> getPkgs() {
		return pkgs;
	}

	public void setPkgs(List<Pkg> pkgs) {
		this.pkgs = pkgs;
	}

	public String getPick_code() {
		return pick_code;
	}

	public void setPick_code(String pick_code) {
		this.pick_code = pick_code;
	}

	public PageView getPageView() {
		return pageView;
	}

	public void setPageView(PageView pageView) {
		this.pageView = pageView;
	}
	
	public int getPkgCnt()
    {
        return pkgCnt;
    }

    public void setPkgCnt(int pkgCnt)
    {
        this.pkgCnt = pkgCnt;
    }

    public String getPkgsData() {
		return pkgsData;
	}

	public void setPkgsData(String pkgsData) {
		this.pkgsData = pkgsData;
	}

	public int getSeaport_id() {
		return seaport_id;
	}

	public void setSeaport_id(int seaport_id) {
		this.seaport_id = seaport_id;
	}
	
    public int getOverseas_address_id()
    {
        return overseas_address_id;
    }

    public void setOverseas_address_id(int overseas_address_id)
    {
        this.overseas_address_id = overseas_address_id;
    }

    public String getWarehouse()
    {
        return warehouse;
    }

    public void setWarehouse(String warehouse)
    {
        this.warehouse = warehouse;
    }
	
}
