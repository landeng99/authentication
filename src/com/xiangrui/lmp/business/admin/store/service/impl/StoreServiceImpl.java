package com.xiangrui.lmp.business.admin.store.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.admin.store.mapper.StoreMapper;
import com.xiangrui.lmp.business.admin.store.mapper.StorePkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.store.service.StoreService;
import com.xiangrui.lmp.business.admin.store.vo.StorePackage;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.util.IntegerUtil;
import com.xiangrui.lmp.util.PackageLogUtil;

@Service(value = "storeService")
public class StoreServiceImpl implements StoreService
{

	@Autowired
	private StoreMapper storeMapper;

	@Autowired
	private PackageMapper packageMapper;

	@Autowired
	private StorePkgAttachServiceMapper storePkgAttachServiceMapper;

	@Autowired
	private PkgAttachServiceMapper pkgAttachServiceMapper;

	@Override
	public List<StorePackage> queryPkgs(StorePackage pkg, boolean needLike)
	{
		List<StorePackage> pkgList = null;
		if (needLike)
		{
			pkgList = storeMapper.queryPkgsLike(pkg);
		}
		else
		{
			pkgList = storeMapper.queryPkgs(pkg);
		}
		for (StorePackage tempPkg : pkgList)
		{
			int pkgId = tempPkg.getPackage_id();

			// 查询包裹增值服务
			List<PkgAttachServiceGroup> allPkgAttachServiceList = new ArrayList<PkgAttachServiceGroup>();
			List<PkgAttachServiceGroup> pkgAttachServiceList = storePkgAttachServiceMapper.queryPkgAttach(pkgId);
			allPkgAttachServiceList.addAll(pkgAttachServiceList);
			if (pkgAttachServiceList != null && pkgAttachServiceList.size() == 1)
			{
				PkgAttachServiceGroup tempPkgAttachServiceGroup = pkgAttachServiceList.get(0);
				if (tempPkgAttachServiceGroup.getAttach_ids().equals("" + BaseAttachService.SPLIT_PKG_ID) || tempPkgAttachServiceGroup.getAttach_ids().equals("" + BaseAttachService.MERGE_PKG_ID))
				{
					String tgtPackageIdGroup = tempPkgAttachServiceGroup.getTgt_package_id_group();
					if (!tgtPackageIdGroup.contains("" + pkgId))
					{
						String[] needQueryPkgAttachServicePackageIds = tgtPackageIdGroup.split(",");
						if (needQueryPkgAttachServicePackageIds != null && needQueryPkgAttachServicePackageIds.length > 0)
						{
							for (String needQueryPkgAttachServicePackageId : needQueryPkgAttachServicePackageIds)
							{
								List<PkgAttachServiceGroup> partPkgAttachServiceList = storePkgAttachServiceMapper.queryPkgAttach(Integer.parseInt(needQueryPkgAttachServicePackageId));
								if (partPkgAttachServiceList != null && partPkgAttachServiceList.size() > 0)
								{
									for (PkgAttachServiceGroup tempPartPkgAttachServiceGroup : partPkgAttachServiceList)
									{
										if (!isPkgAttachServiceGroupListContains(allPkgAttachServiceList, tempPartPkgAttachServiceGroup))
										{
											allPkgAttachServiceList.add(tempPartPkgAttachServiceGroup);
										}
									}
								}
							}
						}
					}
				}
			}

			// 分隔符
			String separator = ", ";

			for (PkgAttachServiceGroup serviceGroup : allPkgAttachServiceList)
			{

				String package_id_group = serviceGroup.getOg_package_id_group();

				// 分隔符
				StringBuffer buffer = new StringBuffer();
				if (null != package_id_group && !package_id_group.trim().equals(""))
				{
					String[] strs = package_id_group.split(",");
					for (String str : strs)
					{
						if (!str.trim().equals(""))
						{
							int package_id = Integer.parseInt(str.trim());
							Pkg temp = packageMapper.queryPackageById(package_id);
							buffer.append(temp.getLogistics_code());
							buffer.append(separator);
						}
					}

					String opt_status_str = serviceGroup.getOpt_status_str();

					String status = String.valueOf(PkgAttachService.ATTACH_UNFINISH);

					// 只要包含有未完成状态的，则标记为未完成
					if ((null != opt_status_str && opt_status_str.contains(status)) || null == opt_status_str)
					{
						serviceGroup.setStatus(PkgAttachService.ATTACH_UNFINISH);
					}
					else
					{
						serviceGroup.setStatus(PkgAttachService.ATTACH_FINISH);
					}
					int length = buffer.length();

					if (length > 0)
					{
						// 删除最后一个分隔符
						buffer.deleteCharAt(length - separator.length());
					}
					serviceGroup.setOg_logistics_codes(buffer.toString());
				}
			}

			tempPkg.setPkgAttachServiceList(allPkgAttachServiceList);
		}
		return pkgList;
	}

	public int storePkg(StorePackage pkg, User user)
	{
		int retInt = storeMapper.updatePkgForStore(pkg);

		// 操作成功,同步包裹日志记录
		if (retInt > 0)
		{
			BasePackage basePackage = packageMapper.queryPackageById(pkg.getPackage_id());
			PackageLogUtil.log(basePackage, user);
		}
		return retInt;
	}

	@Override
	public int updateStoreArriveStatus(StorePackage pkg)
	{
		int package_id = pkg.getPackage_id();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("package_id", package_id);

		// 查询包裹是否有分箱服务
		params.put("attach_id", AttachService.SPLIT_PKG_ID);
		List<PkgAttachServiceGroup> splitlist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);
		String spec = ",";

		// 查询包裹是否有合箱服务
		params.put("attach_id", AttachService.MERGE_PKG_ID);
		List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);

		int arrive_status = StorePackage.STORE_PACKAGE_ARRIVED;
		if (splitlist != null && splitlist.size() > 0)
		{
			for (PkgAttachServiceGroup serviceGroup : splitlist)
			{
				// 扫描到原分箱的包裹，需要显示到待处理列表
				if (IntegerUtil.isIntegerEqual(package_id, Integer.parseInt(serviceGroup.getOg_package_id_group())))
				{
					String package_id_group = serviceGroup.getTgt_package_id_group();
					String[] package_ids = package_id_group.split(spec);
					for (String id : package_ids)
					{
						StorePackage childPkg = new StorePackage();
						childPkg.setPackage_id(Integer.parseInt(id));
						childPkg.setNeed_handle_flag(StorePackage.NEED_HANLDE);
						storeMapper.updateNeedHandleFlag(childPkg);
						childPkg.setArrived_package_display_flag(StorePackage.NEED_HANLDE);
						storeMapper.updateArrivedPackageDisplayFlag(childPkg);
					}
				}
			}
		}
		else if (mergelist != null && mergelist.size() > 0)
		{

			for (PkgAttachServiceGroup serviceGroup : mergelist)
			{
				String package_id_group = serviceGroup.getOg_package_id_group();
				String targetpackage_id = serviceGroup.getTgt_package_id_group();
				Integer targetpackage_id_int = Integer.parseInt(targetpackage_id);
				String[] package_ids = package_id_group.split(spec);
				// 扫描到非合箱后的包裹，需要显示到待处理列表
				if (!IntegerUtil.isIntegerEqual(package_id,targetpackage_id_int))
				{
					boolean isDisplay = true;
					for (String id : package_ids)
					{
						Integer integerId=Integer.parseInt(id);
						Pkg childpkg = packageMapper.queryPackageById(integerId);
						//本身包裹已经到库，检查其它非合箱后的包裹即可
						if (!IntegerUtil.isIntegerEqual(childpkg.getPackage_id(),targetpackage_id_int)&&!IntegerUtil.isIntegerEqual(childpkg.getPackage_id(),package_id))
						{
							if (childpkg.getArrive_status() != StorePackage.STORE_PACKAGE_ARRIVED)
							{
								isDisplay = false;
							}
						}
					}
					if (isDisplay)
					{
						StorePackage childPkg = new StorePackage();
						childPkg.setPackage_id(targetpackage_id_int);
						childPkg.setNeed_handle_flag(StorePackage.NEED_HANLDE);
						storeMapper.updateNeedHandleFlag(childPkg);
					}
					
					StorePackage childPkg = new StorePackage();
					childPkg.setPackage_id(targetpackage_id_int);
					childPkg.setArrived_package_display_flag(StorePackage.NEED_HANLDE);
					storeMapper.updateArrivedPackageDisplayFlag(childPkg);
				}
			}
		}
		pkg.setArrive_status(arrive_status);
		return storeMapper.updateStoreArriveStatus(pkg);
	}

	public static boolean isPkgAttachServiceGroupListContains(List<PkgAttachServiceGroup> allPkgAttachServiceList, PkgAttachServiceGroup tempPartPkgAttachServiceGroup)
	{
		boolean isContains = false;
		if (allPkgAttachServiceList != null && allPkgAttachServiceList.size() > 0)
		{
			for (PkgAttachServiceGroup pkgAttachServiceGroup : allPkgAttachServiceList)
			{
				if (pkgAttachServiceGroup.getService_names().equals(tempPartPkgAttachServiceGroup.getService_names()))
				{
					isContains = true;
				}
			}
		}
		return isContains;
	}

	public void dealToNeedHandleFlag(Integer package_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("package_id", package_id);
		// 查询包裹是否有分箱服务
		params.put("attach_id", AttachService.SPLIT_PKG_ID);
		List<PkgAttachServiceGroup> splitlist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);
		String spec = ",";

		// 查询包裹是否有合箱服务
		params.put("attach_id", AttachService.MERGE_PKG_ID);
		List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);

		if (splitlist != null && splitlist.size() > 0)
		{
			for (PkgAttachServiceGroup serviceGroup : splitlist)
			{
				// 扫描到原分箱的包裹，需要显示到待处理列表
				Integer og_package_id_group_int=Integer.parseInt(serviceGroup.getOg_package_id_group());
				if (!IntegerUtil.isIntegerEqual(package_id, og_package_id_group_int))
				{
					String package_id_group = serviceGroup.getTgt_package_id_group();
					String[] package_ids = package_id_group.split(spec);
					boolean isDisplay = true;
					for (String id : package_ids)
					{
						Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(id));
						if (!IntegerUtil.isIntegerEqual(childpkg.getPackage_id(), og_package_id_group_int))
						{
							if (childpkg.getStatus() != StorePackage.LOGISTICS_STORAGED)
							{
								isDisplay = false;
							}
						}
					}
					if(isDisplay)
					{
						for (String id : package_ids)
						{
							Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(id));
							if (!IntegerUtil.isIntegerEqual(childpkg.getPackage_id(),og_package_id_group_int))
							{
								StorePackage childPkg = new StorePackage();
								childPkg.setPackage_id(childpkg.getPackage_id());
								childPkg.setNeed_handle_flag(StorePackage.NO_NEED_HANLDE);
								storeMapper.updateNeedHandleFlag(childPkg);
								childPkg.setArrived_package_display_flag(StorePackage.NO_NEED_HANLDE);
								storeMapper.updateArrivedPackageDisplayFlag(childPkg);
							}
						}
					}
				}
			}
		}
		else if (mergelist != null && mergelist.size() > 0)
		{

			for (PkgAttachServiceGroup serviceGroup : mergelist)
			{
				String package_id_group = serviceGroup.getOg_package_id_group();
				String targetpackage_id = serviceGroup.getTgt_package_id_group();
				Integer targetpackage_id_int = Integer.parseInt(targetpackage_id);
				String[] package_ids = package_id_group.split(spec);
				// 扫描到合箱后的包裹，需要取消待处理列表中的显示
				if (IntegerUtil.isIntegerEqual(package_id, targetpackage_id_int))
				{
					StorePackage childPkg = new StorePackage();
					childPkg.setPackage_id(targetpackage_id_int);
					childPkg.setNeed_handle_flag(StorePackage.NO_NEED_HANLDE);
					storeMapper.updateNeedHandleFlag(childPkg);
					childPkg.setArrived_package_display_flag(StorePackage.NO_NEED_HANLDE);
					storeMapper.updateArrivedPackageDisplayFlag(childPkg);
				}
				/*// 扫描到非合箱后的包裹，需要显示到待处理列表
				if (!IntegerUtil.isIntegerEqual(package_id, targetpackage_id_int))
				{
					boolean isDisplay = true;
					for (String id : package_ids)
					{
						Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(id));
						if (childpkg.getPackage_id() != targetpackage_id_int)
						{
							if (childpkg.getStatus() != StorePackage.STORE_PACKAGE_ARRIVED)
							{
								isDisplay = false;
							}
						}
					}
					if (isDisplay)
					{
						StorePackage childPkg = new StorePackage();
						childPkg.setPackage_id(targetpackage_id_int);
						childPkg.setNeed_handle_flag(StorePackage.NEED_HANLDE);
						storeMapper.updateNeedHandleFlag(childPkg);
					}
				}*/
			}
		}
	}
	
	public void setPkgAttacheServicesStatusDisplay(StorePackage pkg)
	{
		int package_id = pkg.getPackage_id();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("package_id", package_id);

		// 查询包裹是否有分箱服务
		params.put("attach_id", AttachService.SPLIT_PKG_ID);
		List<PkgAttachServiceGroup> splitlist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);
		String spec = ",";

		// 查询包裹是否有合箱服务
		params.put("attach_id", AttachService.MERGE_PKG_ID);
		List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);

		if (splitlist != null && splitlist.size() > 0)
		{
			for (PkgAttachServiceGroup serviceGroup : splitlist)
			{
				if (!IntegerUtil.isIntegerEqual(package_id,Integer.parseInt(serviceGroup.getOg_package_id_group())))
				{
					// 不弹出弹出增值服务窗口
					pkg.setPkgAttacheServicesStatus(2);
				}
			}
		}
		else if (mergelist != null && mergelist.size() > 0)
		{

//			for (PkgAttachServiceGroup serviceGroup : mergelist)
//			{
//				String package_id_group = serviceGroup.getOg_package_id_group();
//				String targetpackage_id = serviceGroup.getTgt_package_id_group();
//				Integer targetpackage_id_int = Integer.parseInt(targetpackage_id);
//				String[] package_ids = package_id_group.split(spec);
//				// 扫描到非合箱后的包裹，需要显示到待处理列表
//				if (!IntegerUtil.isIntegerEqual(package_id,targetpackage_id_int))
//				{
//					boolean isDisplay = true;
//					for (String id : package_ids)
//					{
//						Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(id));
//						if (!IntegerUtil.isIntegerEqual(childpkg.getPackage_id(),targetpackage_id_int))
//						{
//							if (childpkg.getStatus() != StorePackage.LOGISTICS_STORAGED)
//							{
//								isDisplay = false;
//							}
//						}
//					}
//					if (!isDisplay)
//					{
//						// 不弹出弹出增值服务窗口
//						pkg.setPkgAttacheServicesStatus(2);
//					}
//				}
//			}
		}
	}
	
	@Override
	public void dealNeedHandleFlag(int package_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("package_id", package_id);

		// 查询包裹是否有分箱服务
		params.put("attach_id", AttachService.SPLIT_PKG_ID);
		List<PkgAttachServiceGroup> splitlist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);
		String spec = ",";

		// 查询包裹是否有合箱服务
		params.put("attach_id", AttachService.MERGE_PKG_ID);
		List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);

		int arrive_status = StorePackage.STORE_PACKAGE_ARRIVED;
		if (splitlist != null && splitlist.size() > 0)
		{
			for (PkgAttachServiceGroup serviceGroup : splitlist)
			{
				// 扫描到原分箱的包裹，需要显示到待处理列表
				if (IntegerUtil.isIntegerEqual(package_id, Integer.parseInt(serviceGroup.getOg_package_id_group())))
				{
					String package_id_group = serviceGroup.getTgt_package_id_group();
					String[] package_ids = package_id_group.split(spec);
					for (String id : package_ids)
					{
						StorePackage childPkg = new StorePackage();
						childPkg.setPackage_id(Integer.parseInt(id));
						childPkg.setNeed_handle_flag(StorePackage.NEED_HANLDE);
						storeMapper.updateNeedHandleFlag(childPkg);
						childPkg.setArrived_package_display_flag(StorePackage.NEED_HANLDE);
						storeMapper.updateArrivedPackageDisplayFlag(childPkg);
					}
				}
			}
		}
		else if (mergelist != null && mergelist.size() > 0)
		{

			for (PkgAttachServiceGroup serviceGroup : mergelist)
			{
				String package_id_group = serviceGroup.getOg_package_id_group();
				String targetpackage_id = serviceGroup.getTgt_package_id_group();
				Integer targetpackage_id_int = Integer.parseInt(targetpackage_id);
				String[] package_ids = package_id_group.split(spec);
				// 扫描到非合箱后的包裹，需要显示到待处理列表
				if (!IntegerUtil.isIntegerEqual(package_id,targetpackage_id_int))
				{
					boolean isDisplay = true;
					for (String id : package_ids)
					{
						Integer integerId=Integer.parseInt(id);
						Pkg childpkg = packageMapper.queryPackageById(integerId);
						//本身包裹已经到库，检查其它非合箱后的包裹即可
						if (!IntegerUtil.isIntegerEqual(childpkg.getPackage_id(),targetpackage_id_int)&&!IntegerUtil.isIntegerEqual(childpkg.getPackage_id(),package_id))
						{
							if (childpkg.getArrive_status() != StorePackage.STORE_PACKAGE_ARRIVED)
							{
								isDisplay = false;
							}
						}
					}
					if (isDisplay)
					{
						StorePackage childPkg = new StorePackage();
						childPkg.setPackage_id(targetpackage_id_int);
						childPkg.setNeed_handle_flag(StorePackage.NEED_HANLDE);
						storeMapper.updateNeedHandleFlag(childPkg);
					}
					
					StorePackage childPkg = new StorePackage();
					childPkg.setPackage_id(targetpackage_id_int);
					childPkg.setArrived_package_display_flag(StorePackage.NEED_HANLDE);
					storeMapper.updateArrivedPackageDisplayFlag(childPkg);
				}
			}
		}
	}
}
