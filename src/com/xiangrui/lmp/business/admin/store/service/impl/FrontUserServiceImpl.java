package com.xiangrui.lmp.business.admin.store.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.frontuser.vo.FrontUserAddress;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.mapper.FfrontUserMapper;
import com.xiangrui.lmp.util.StringUtil;

@Service(value = "frontUserService")
public class FrontUserServiceImpl implements FrontUserService {
    
    @Autowired
    private FrontUserMapper frontUserMapper;
    
    @Autowired
    private FfrontUserMapper fFrontUserMapper;
    
    @Override
    public FrontUser queryFrontUserById(int user_id) {
        
        return frontUserMapper.queryFrontUserById(user_id);
    }
    
    /**
     * 前台用户信息
     * 
     * @param user_name
     * @return
     */
    @Override
    public FrontUser queryFrontUserByName(String user_name) {
        
        return frontUserMapper.queryFrontUserByName(user_name);
    }
    
    /**
     * 前台用户信息
     * 
     * @param params
     * @return
     */
    @Override
    public List<FrontUser> queryFrontUser(Map<String, Object> params) {
        return frontUserMapper.queryFrontUser(params);
    }
    
    @Override
    public List<FrontUser> queryFrontUserByAccountAndMobie(String account, String mobie){
        Map<String, Object> params = new HashMap<>();
        params.put("mobile", mobie);
        params.put("account", account);
        return frontUserMapper.queryFrontUser(params);
    }
    
    /**
     * 前台用户信息
     * @param params
     * @return
     */
    @Override
    public List<FrontUserAddress> queryFrontUserAddress(Map<String, Object> params){
        
        return frontUserMapper.queryFrontUserAddress(params);
    }
    
    /**
     * 分配同行账号
     * 
     * @param frontUser
     * @return
     */
    @SystemServiceLog(description="为同行分配前台账号")
    @Override
    public void insertFrontUser(FrontUser frontUser) {
        
        // 是否存在，默认值是存在
        boolean isExist = true;
        while (isExist)
        {

            // 6位随机字符串
            String last_name = StringUtil.radomString();
            Map<String, String> param = new HashMap<String, String>();

            param.put("last_name", last_name);
            int cnt = fFrontUserMapper.isLastNameExist(param);
            if (cnt == 0)
            {
                isExist = false;
                frontUser.setLast_name(last_name);
            } else
            {
                isExist = true;
            }
        }
        frontUser.setFirst_name(FrontUser.FIRST_NAME_INIT);
        frontUserMapper.insertFrontUser(frontUser);
    }
    
    /**
     * 更新账户状态
     * @param frontUser
     * @return
     */
    @SystemServiceLog(description="更新前台账号状态")
    @Override
    public  void updatefrontUserStatus(FrontUser frontUser){
        frontUserMapper.updatefrontUserStatus(frontUser);
    }

	@Override
	public int queryFrontUserMaxUserId() {
		return frontUserMapper.queryFrontUserMaxUserId();
	}

    @Override
    public int updatefrontUserIsFreeze(FrontUser frontUser)
    {
        return frontUserMapper.updatefrontUserIsFreeze(frontUser);
    }
    /**
     * Account Like查询
     * @param account
     * @return
     */
    public List<FrontUser> queryFrontUserLikeByAccount(Map<String, Object> params)
    {
    	return frontUserMapper.queryFrontUserLikeByAccount(params);
    }
    
    /**
     * 修改用户的业务名和快件渠道
     * @param frontUser
     * @return
     */
    public void updateFrontUserBussinessNameAndSelectExpressChannel(FrontUser frontUser)
    {
    	frontUserMapper.updateFrontUserBussinessNameAndSelectExpressChannel(frontUser);
    }
    
    /**
     * 查询有冻结金额的用户
     * @return
     */
    public List<FrontUser> queryFrozenBalanceFrontUser(){
    	return frontUserMapper.queryFrozenBalanceFrontUser();
    }
    /**
     * Account Like查询
     * @param account
     * @return
     */
    public List<FrontUser> queryFrontUserLikeByAccountType(Map<String, Object> params)
    {
    	return frontUserMapper.queryFrontUserLikeByAccountType(params);
    }

    /**
     * 提现申请前更新alipayName
     */
	@Override
	public void updateAlipayName(Map<String, Object> param)
	{
		this.frontUserMapper.updateAlipayName(param);
	}

	@Override
	public void updatefrontUserBalance(FrontUser frontUser) {
		this.frontUserMapper.updatefrontUserBalance(frontUser);
	}

}
