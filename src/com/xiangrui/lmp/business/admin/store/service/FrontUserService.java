package com.xiangrui.lmp.business.admin.store.service;



import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.frontuser.vo.FrontUserAddress;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

/**
 * 前台用户
 * @author Administrator
 *
 */
public interface FrontUserService {

	/**
    * 前台用户信息
    * @param user_id
    * @return
    */
    FrontUser queryFrontUserById(int user_id);
    
	/**
    * 查询用户的最大id
    * @param user_id
    * @return
    */
    int queryFrontUserMaxUserId();
    
    /**
     * 前台用户信息
     * @param user_name
     * @return
     */
    FrontUser queryFrontUserByName(String user_name);
    
    /**
     * 前台用户信息
     * @param params
     * @return
     */
    List<FrontUser> queryFrontUser(Map<String, Object> params);
    
    List<FrontUser> queryFrontUserByAccountAndMobie(String account, String mobie);
    
    /**
     * @param params
     * @return
     */
    List<FrontUserAddress> queryFrontUserAddress(Map<String, Object> params);
    
    /**
     * 分配同行账号
     * @param frontUser
     * @return
     */
    void insertFrontUser(FrontUser frontUser);
    
    /**
     * 更新账户状态
     * @param frontUser
     * @return
     */
    void updatefrontUserStatus(FrontUser frontUser);
    
    /**
     * 更新账户余额
     * @param frontUser
     * @return
     */
    void updatefrontUserBalance(FrontUser frontUser);
    
    /**
     * 更新账户是否冻结状态
     * @param frontUser
     * @return
     */
    int updatefrontUserIsFreeze(FrontUser frontUser);
    
    /**
     * Account Like查询
     * @param account
     * @return
     */
    List<FrontUser> queryFrontUserLikeByAccount(Map<String, Object> params);
    
    
    /**
     * 修改用户的业务名和快件渠道
     * @param frontUser
     * @return
     */
    void updateFrontUserBussinessNameAndSelectExpressChannel(FrontUser frontUser);
    
    /**
     * 查询有冻结金额的用户
     * @return
     */
    List<FrontUser> queryFrozenBalanceFrontUser();
    


	List<FrontUser> queryFrontUserLikeByAccountType(Map<String, Object> params);

	/**
	 * 更新alipayName
	 * @param param
	 */
	void updateAlipayName(Map<String, Object> param);

}
