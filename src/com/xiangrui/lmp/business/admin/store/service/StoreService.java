package com.xiangrui.lmp.business.admin.store.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.store.vo.StorePackage;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.base.BasePackage;

/**
 * 查询包裹
 * 
 * @author Administrator
 * 
 */
public interface StoreService
{

	/**
	 * 根据参数查询包裹信息
	 * 
	 * @param params
	 * @return
	 */
	List<StorePackage> queryPkgs(StorePackage pkg, boolean needLike);

	/**
	 * 
	 * @param pkg
	 * @param user
	 *            包裹日志操作人
	 * @return
	 */
	int storePkg(StorePackage pkg, User user);

	/**
	 * 更新包裹的到库状态
	 * 
	 * @param pkg
	 * @return
	 */
	int updateStoreArriveStatus(StorePackage pkg);

	/**
	 * 处理待处理标示
	 * 
	 * @param package_id
	 */
	void dealToNeedHandleFlag(Integer package_id);

	/**
	 * 检查是否需求弹出增值服务处理
	 * @param pkg
	 */
	void setPkgAttacheServicesStatusDisplay(StorePackage pkg);
	
	void dealNeedHandleFlag(int package_id);
}
