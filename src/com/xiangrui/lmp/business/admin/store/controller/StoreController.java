package com.xiangrui.lmp.business.admin.store.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgAttachServiceService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.admin.quotationManage.service.QuotationManageService;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.scanLog.service.InputScanLogService;
import com.xiangrui.lmp.business.admin.scanLog.vo.InputScanLog;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.service.StoreService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.store.vo.StorePackage;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.homepage.service.FrontUserAddressService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.util.BreadcrumbUtil;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PackageLogUtil;

/**
 * 包裹管理控制器
 * 
 * @author Administrator
 */
@Controller
@RequestMapping("/admin/store")
public class StoreController {

	@Autowired
	private StoreService storeService;

	@Autowired
	private QuotationManageService quotationManageService;

	@Autowired
	private PackageService packageService;

	@Autowired
	private PkgAttachServiceService pkgAttachServiceService;

	@Autowired
	private AttachServiceService attachServiceService;
	@Autowired
	private FrontUserAddressService frontUserAddressService;
	/**
	 * 包裹商品
	 */
	@Autowired
	private PkgGoodsService frontPkgGoodsService;
	@Autowired
	private FrontUserService frontUserService;

	@Autowired
	private InputScanLogService inputScanLogService;

	/**
	 * 初始化手动新增包裹操作页面
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("/initadd")
	public String initAdd(HttpServletRequest req, HttpServletResponse resp) {

		String logistics_code = (String) req.getParameter("query_logistics_code");
		if (StringUtils.trimToNull(logistics_code) == null) {
			logistics_code = "";
		}
		req.setAttribute("logistics_code", logistics_code);
		BreadcrumbUtil.push(req);
		return "back/storeinitadd";
	}

	/**
	 * 初始化新增包裹操作页面
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("/initscanadd")
	public String initScanAdd(HttpServletRequest req, HttpServletResponse resp) {
		return "back/storeinitscanadd";
	}

	/**
	 * 查询包裹
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/search")
	public Map<String, Object> search(HttpServletRequest req, HttpServletResponse resp) {
		Map<String, Object> result = new HashMap<String, Object>();
		String lastName = StringUtils.trimToNull(req.getParameter("lastname"));
		
		String originalNum = req.getParameter("originalNum");
		if (originalNum != null) {
			originalNum = originalNum.trim();
		}

		// 入库状态：0表示待入库
		int status = -100;
		StorePackage pkg = new StorePackage();
		if (lastName != null) {
			status = 0;
		}
		pkg.setStatus(status);
		pkg.setOriginal_num(originalNum);
		pkg.setLastName(lastName);

		List<StorePackage> pkgList = storeService.queryPkgs(pkg, true);
		for (StorePackage temp : pkgList) {
			Timestamp createTime = temp.getCreateTime();
			String format = "yyyy-MM-dd HH:mm:ss";
			String date = DateUtil.getStringTemestamp(createTime, format);
			temp.setCreateTimeStr(date);
			FrontUser frontUser = frontUserService.queryFrontUserById(temp.getUser_id());
			temp.setUserType(frontUser.getUser_type());
		}
		result.put("row", pkgList);
		Map<Integer, String> statusGroup = new HashMap<Integer, String>();
		statusGroup.put(StorePackage.LOGISTICS_STORE_WAITING, "待入库");
		statusGroup.put(StorePackage.LOGISTICS_UNUSUALLY, "包裹异常");
		statusGroup.put(StorePackage.LOGISTICS_STORAGED, "已入库");
		statusGroup.put(StorePackage.LOGISTICS_SEND_WAITING, "待发货");
		statusGroup.put(StorePackage.LOGISTICS_SENT_ALREADY, "已出库");
		statusGroup.put(StorePackage.LOGISTICS_AIRLIFT_ALREADY, "已空运");
		statusGroup.put(StorePackage.LOGISTICS_CUSTOMS_WAITING, "待清关");
		statusGroup.put(StorePackage.LOGISTICS_CUSTOMS_ALREADY, "已清关并已派件中");
		statusGroup.put(StorePackage.LOGISTICS_SIGN_IN, "已签收");
		statusGroup.put(StorePackage.LOGISTICS_DISCARD, "废弃");
		statusGroup.put(StorePackage.LOGISTICS_RETURN, "退货");
		result.put("statusGroup", statusGroup);

		return result;
	}

	/**
	 * 包裹到库状态更新
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/updateArriveStatus")
	public Map<String, Object> updateArriveStatus(HttpServletRequest req, HttpServletResponse resp, StorePackage pkg) {
		// 到库时间
		pkg.setArrive_time(new Timestamp(System.currentTimeMillis()));
		int cnt = storeService.updateStoreArriveStatus(pkg);
		Map<String, Object> result = new HashMap<String, Object>();
		if (cnt > 0) {
			result.put("result", true);
		} else {
			result.put("result", false);
			result.put("msg", "到库失败");
		}
		return result;
	}

	/**
	 * 手动入库
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public Map<String, Object> add(HttpServletRequest req, HttpServletResponse resp) {

		String package_id = req.getParameter("package_id");

		StorePackage pkg = new StorePackage();
		pkg.setPackage_id(Integer.valueOf(package_id));
		pkg.setStatus(StorePackage.LOGISTICS_STORAGED);
		pkg.setAccounting_status(StorePackage.ACCOUNTING_UNDO);
		User back_user = (User) req.getSession().getAttribute("back_user");
		int cnt = storeService.storePkg(pkg, back_user);
		Map<String, Object> map = new HashMap<String, Object>();
		// (cnt>0)表示 更新成功，反之则更新失败
		if (cnt > 0) {
			map.put("result", true);
		} else {
			map.put("result", false);
		}
		return map;
	}

	/**
	 * 扫描入库，点击enter调用的接口 
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/scanadd")
	public Map<String, Object> scanadd(HttpServletRequest req, HttpServletResponse resp) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		String lastName = StringUtils.trimToNull(req.getParameter("lastname"));
		if (lastName != "" && lastName != null) {
			String originalNum = req.getParameter("originalNum");
			if (originalNum != null) {
				originalNum = originalNum.trim();
			}
			// 入库状态：0表示待入库
			int status = 0;
			StorePackage pkg = new StorePackage();
			pkg.setStatus(status);
			pkg.setOriginal_num(originalNum);
			pkg.setLastName(lastName);
			List<StorePackage> pkgList = storeService.queryPkgs(pkg, true);
			for (StorePackage temp : pkgList) {
				Timestamp createTime = temp.getCreateTime();
				String format = "yyyy-MM-dd HH:mm:ss";
				String date = DateUtil.getStringTemestamp(createTime, format);
				temp.setCreateTimeStr(date);
			}
			result.put("row", pkgList);
			Map<Integer, String> statusGroup = new HashMap<Integer, String>();
			statusGroup.put(StorePackage.LOGISTICS_STORE_WAITING, "待入库");
			statusGroup.put(StorePackage.LOGISTICS_UNUSUALLY, "包裹异常");
			statusGroup.put(StorePackage.LOGISTICS_STORAGED, "已入库");
			statusGroup.put(StorePackage.LOGISTICS_SEND_WAITING, "待发货");
			statusGroup.put(StorePackage.LOGISTICS_SENT_ALREADY, "已出库");
			statusGroup.put(StorePackage.LOGISTICS_AIRLIFT_ALREADY, "已空运");
			statusGroup.put(StorePackage.LOGISTICS_CUSTOMS_WAITING, "待清关");
			statusGroup.put(StorePackage.LOGISTICS_CUSTOMS_ALREADY, "已清关并已派件中");
			statusGroup.put(StorePackage.LOGISTICS_SIGN_IN, "已签收");
			statusGroup.put(StorePackage.LOGISTICS_DISCARD, "废弃");
			statusGroup.put(StorePackage.LOGISTICS_RETURN, "退货");
			result.put("statusGroup", statusGroup);
			return result;
		} else {

			String originalNum = StringUtils.trimToNull(req.getParameter("originalNum").trim());
			String logOriginalNum=originalNum;
			String noNeedLog = StringUtils.trimToNull(req.getParameter("noNeedLog"));
			if (originalNum == null) {
				result.put("result", false);
				result.put("msg", "单号不能为空!");
				return result;
			}

			// 匹配后12位后提示找不到包裹
			String needToDisplayAfter12NotFound = "N";
			// 入库状态：0表示待入库
			int status = -100;
			StorePackage param = new StorePackage();
			param.setOriginal_num(originalNum);
			param.setStatus(status);
			List<StorePackage> pkgList = storeService.queryPkgs(param, false);
			if (pkgList == null || pkgList.size() == 0) {
				int length = originalNum.length();
				if (length >= 12) {
					originalNum = StringUtils.substring(originalNum, length - 12, length);
					param.setOriginal_num(originalNum);
					pkgList = storeService.queryPkgs(param, true);
					if (pkgList != null && pkgList.size() > 0) {
						needToDisplayAfter12NotFound = "Y";

					}
				}
			}
			if (pkgList.size() == 1) {
				// 扫描包裹之后自动更新包裹状态
				for (StorePackage pkg : pkgList) {

					Timestamp createTime = pkg.getCreateTime();
					String format = "yyyy-MM-dd HH:mm:ss";
					String date = DateUtil.getStringTemestamp(createTime, format);

					pkg.setCreateTimeStr(date);

					// （未到库）： 则更新包裹到库状态为：已到库
					if (pkg.getArrive_status() == StorePackage.STORE_PACKAGE_NOT_ARRIVED) {
						pkg.setArrive_time(new Timestamp(System.currentTimeMillis()));
						storeService.updateStoreArriveStatus(pkg);
					}
					// storeService.setPkgAttacheServicesStatusDisplay(pkg);
				}
			}
			InputScanLog inputScanLog = new InputScanLog();
			inputScanLog.setPkg_no(logOriginalNum);
			int scan_count = 0;
			// 物流单号查询提示
			String needToDisplayExpressNumWarning = "N";
			String needToDisplayExpressPackageWarning = "N";
			if (pkgList != null && pkgList.size() > 0) {
				for (StorePackage pkg : pkgList) {
					if (StringUtils.trimToNull(pkg.getExpress_num()) != null) {
						needToDisplayExpressNumWarning = "Y";
					}
					if (pkg.getExpress_package() == 1) {
						needToDisplayExpressPackageWarning = "Y";
					}
					FrontUser frontUser = frontUserService.queryFrontUserById(pkg.getUser_id());
					pkg.setUserType(frontUser.getUser_type());
				}
				inputScanLog.setPkg_id(pkgList.get(0).getPackage_id());
				scan_count = inputScanLogService.countCurrentDateInputScanLogByPacakgeId(pkgList.get(0).getPackage_id());
				inputScanLog.setPkg_status(PackageLogUtil.getDescription(pkgList.get(0).getStatus()));
				if ("Y".equals(needToDisplayExpressNumWarning)) {
					inputScanLog.setPkg_status("物流单号需要拆箱");
				}
			} else {
				inputScanLog.setPkg_status("无此单号");
				inputScanLog.setNeed_mark_flag(1);
				scan_count = inputScanLogService.countCurrentDateInputScanLogByPkgNo(logOriginalNum);
			}

			HttpSession session = req.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				inputScanLog.setScan_user_name(user.getUsername());
			}
			if (noNeedLog == null)
			{
				inputScanLog.setScan_count(++scan_count);
				inputScanLogService.insertInputScanLog(inputScanLog);
			}

			result.put("row", pkgList);
			Map<Integer, String> statusGroup = new HashMap<Integer, String>();
			statusGroup.put(StorePackage.LOGISTICS_STORE_WAITING, "待入库");
			statusGroup.put(StorePackage.LOGISTICS_UNUSUALLY, "包裹异常");
			statusGroup.put(StorePackage.LOGISTICS_STORAGED, "已入库");
			statusGroup.put(StorePackage.LOGISTICS_SEND_WAITING, "待发货");
			statusGroup.put(StorePackage.LOGISTICS_SENT_ALREADY, "已出库");
			statusGroup.put(StorePackage.LOGISTICS_AIRLIFT_ALREADY, "已空运");
			statusGroup.put(StorePackage.LOGISTICS_CUSTOMS_WAITING, "待清关");
			statusGroup.put(StorePackage.LOGISTICS_CUSTOMS_ALREADY, "已清关派件中");
			statusGroup.put(StorePackage.LOGISTICS_SIGN_IN, "已签收");
			statusGroup.put(StorePackage.LOGISTICS_DISCARD, "废弃");
			statusGroup.put(StorePackage.LOGISTICS_RETURN, "退货");
			result.put("statusGroup", statusGroup);
			result.put("needToDisplayExpressNumWarning", needToDisplayExpressNumWarning);
			result.put("needToDisplayAfter12NotFound", needToDisplayAfter12NotFound);
			result.put("needToDisplayExpressPackageWarning", needToDisplayExpressPackageWarning);
			return result;
		}

	}
	
	public static void main(String[] args) {
		System.out.println("00000");
	}

	/**
	 * 包裹入库提醒用户进行增值服务
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/pkgAttachServicelist")
	public String pkgAttachServicelist(HttpServletRequest request) {
		try {
			Map<String, Object> attachServiceParams = new HashMap<String, Object>();
			attachServiceParams.put("isdeleted", AttachService.ISDELETED_NO);
			List<AttachService> attachServiceList = attachServiceService.queryAllAttachService(attachServiceParams);

			int pkgId = -1;
			String packageId = request.getParameter("package_id");
			pkgId = Integer.parseInt(packageId);

			// 增值服务查询
			List<PkgAttachServiceGroup> pkgAttachServiceGroupList = pkgAttachServiceService
					.queryPkgAttachServiceGroupForScanDisplay(pkgId);

			request.setAttribute("attachServiceList", attachServiceList);
			request.setAttribute("pkgAttachServiceGroupList", pkgAttachServiceGroupList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "back/attachservicenotify";
	}

	/**
	 * 入库操作
	 * 
	 * @param request
	 * @param freight
	 *            运费
	 * @param length
	 *            长度
	 * @param width
	 *            宽度
	 * @param height
	 *            高度
	 * @param package_id
	 *            包裹id
	 * @param weight
	 *            重量
	 * @return
	 */
	@RequestMapping("/storage")
	@ResponseBody
	public String storage(HttpServletRequest request, String freight, String length, String width, String height,
			int package_id, String weight, String actual_weight) {

		Pkg pkg = new Pkg();
		// 运费
		pkg.setFreight(Float.parseFloat(freight));
		// 长度
		pkg.setLength(Float.parseFloat(length));
		// 宽度
		pkg.setWidth(Float.parseFloat(width));
		// 高度
		pkg.setHeight(Float.parseFloat(height));
		// 包裹id
		pkg.setPackage_id(package_id);
		// 重量
		pkg.setWeight(Float.parseFloat(weight));
		// 支付状态为 待支付
		pkg.setPay_status(BasePackage.PAYMENT_UNPAID);
		pkg.setPay_status_freight(BasePackage.PAYMENT_FREIGHT_UNPAID);
		// 包裹状态为 已入库
		pkg.setStatus(BasePackage.LOGISTICS_STORAGED);
		pkg.setAccounting_status(Pkg.ACCOUNTING_UNDO);
		pkg.setActual_weight(Float.parseFloat(actual_weight));

		// 入库修改包裹的操作人,即后台登陆人员
		User user = (User) request.getSession().getAttribute("back_user");

		// 当前数据库存储的包裹
		Pkg pkgTemp = packageService.queryPackageById(package_id);

		float currTransportCost = pkgTemp.getTransport_cost();

		float totalTransportCost = (float) NumberUtils.add(currTransportCost, Float.parseFloat(freight));

		// 转运总费用
		pkg.setTransport_cost(totalTransportCost);

		this.packageService.update(pkg, user);
		this.storeService.dealToNeedHandleFlag(package_id);
		return "0";
	}

	@RequestMapping("/weight")
	@ResponseBody
	public Map<String, Object> weightInit(HttpServletRequest request, int package_id) {
		Pkg pkg = packageService.queryPackageById(package_id);
		String needToDisplayExpressPackageWarning = "N";
		Map<String, Object> map = new HashMap<String, Object>();
		if (pkg != null) {
			String exception_package_flag = pkg.getException_package_flag();
			if (null != exception_package_flag) {
				// 根据address_id查询包裹地址
				if (StringUtils.trimToNull("" + pkg.getAddress_id()) != null) {
					List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());
					FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(pkg.getAddress_id());
					// 客户在编辑包裹时没有勾选需等待合箱，且包裹内件数大于等于1，且包裹有选择收货地址，点击完成，那么包裹从前台页面的“待处理包裹”下消失
					if (frontUserAddress != null && gooldList.size() > 0) {
						exception_package_flag = "N";
					}
				}
			}
			pkg.setException_package_flag(exception_package_flag);

			FrontUser frontUser = frontUserService.queryFrontUserById(pkg.getUser_id());
			pkg.setUserType(frontUser.getUser_type());

			if (pkg.getExpress_package() == 1) {
				needToDisplayExpressPackageWarning = "Y";
			}
			// 根据公司运单号查询客户适合的报价单，然后返回首重和进位
			QuotationManage quota = quotationManageService.getQuotaByOriginum(pkg.getLogistics_code());
			map.put("quota", quota);
		}
		map.put("pkg", pkg);
		map.put("needToDisplayExpressPackageWarning", needToDisplayExpressPackageWarning);
		return map;
	}

}
