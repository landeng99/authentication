package com.xiangrui.lmp.business.admin.store.vo;

import com.xiangrui.lmp.business.base.BasePkgAttachService;

public class StorePkgAttachService extends BasePkgAttachService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 增值服务名称
	 */
	private String service_name;

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

}
