package com.xiangrui.lmp.business.admin.store.vo;

import com.xiangrui.lmp.business.base.BaseFrontUser;


public class FrontUser extends BaseFrontUser {

    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    //业务姓名
    private String businessName;
    
    public String getBusinessName()
	{
		return businessName;
	}

	public void setBusinessName(String businessName)
	{
		this.businessName = businessName;
	}

	/**
     * 包裹数量统计
     */
    private int package_count;

    public int getPackage_count()
    {
        return package_count;
    }

    public void setPackage_count(int package_count)
    {
        this.package_count = package_count;
    }


}
