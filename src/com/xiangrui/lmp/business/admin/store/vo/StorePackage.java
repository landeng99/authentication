package com.xiangrui.lmp.business.admin.store.vo;

import java.util.List;

import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.base.BasePackage;

/**
 * 
 * 包裹信息表
 * 
 * @author Administrator
 * 
 */
public class StorePackage extends BasePackage
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//包裹需要处理标志
	public static final String NEED_HANLDE="Y";
	//包裹不需要处理标志
	public static final String NO_NEED_HANLDE="N";
	
	/**
	 * 英文名的 last name
	 */
	private String lastName;

	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 增值服务
	 */
	private List<PkgAttachServiceGroup> pkgAttachServiceList;

	private String createTimeStr;

	private String pkgAttacheServicesStr;

	/**
	 * 增值服务状态
	 */
	private int pkgAttacheServicesStatus;

	/**
	 * 需要处理标志
	 */
	private String need_handle_flag;
    /**
     * 包裹所属用户类型
     */
    private int userType;
    
    //用户帐号（Email）
    private String account;
    //包裹出库时间
    private String outputTime;

	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}

	public String getOutputTime()
	{
		if (null != outputTime && outputTime.lastIndexOf(".0") == 19)
		{
			return outputTime.substring(0, 19);
		}
		return outputTime;
	}

	public void setOutputTime(String outputTime)
	{
		this.outputTime = outputTime;
	}

	public int getUserType()
	{
		return userType;
	}

	public void setUserType(int userType)
	{
		this.userType = userType;
	}
	public String getNeed_handle_flag()
	{
		return need_handle_flag;
	}

	public void setNeed_handle_flag(String need_handle_flag)
	{
		this.need_handle_flag = need_handle_flag;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public List<PkgAttachServiceGroup> getPkgAttachServiceList()
	{
		return pkgAttachServiceList;
	}

	public void setPkgAttachServiceList(List<PkgAttachServiceGroup> pkgAttachServiceList)
	{
		this.pkgAttachServiceList = pkgAttachServiceList;
		StringBuffer buffer = new StringBuffer();
		String separator = "、";
		pkgAttacheServicesStatus = 2;
		for (PkgAttachServiceGroup pkgattachService : pkgAttachServiceList)
		{
			buffer.append(pkgattachService.getService_names());
			buffer.append(separator);
			if (PkgAttachService.ATTACH_UNFINISH == pkgattachService.getStatus())
			{
				pkgAttacheServicesStatus = PkgAttachService.ATTACH_UNFINISH;
			}
		}

		int length = buffer.length();
		if (length > 0)
		{
			// 删除最后一个分隔符
			buffer.deleteCharAt(length - separator.length());
		}
		pkgAttacheServicesStr = buffer.toString();

	}

	public String getPkgAttacheServicesStr()
	{
		return pkgAttacheServicesStr;
	}

	public void setPkgAttacheServicesStr(String pkgAttacheServicesStr)
	{
		this.pkgAttacheServicesStr = pkgAttacheServicesStr;
	}

	public String getCreateTimeStr()
	{
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr)
	{
		this.createTimeStr = createTimeStr;
	}

	public int getPkgAttacheServicesStatus()
	{
		return pkgAttacheServicesStatus;
	}

	public void setPkgAttacheServicesStatus(int pkgAttacheServicesStatus)
	{
		this.pkgAttacheServicesStatus = pkgAttacheServicesStatus;
	}

}
