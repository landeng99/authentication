package com.xiangrui.lmp.business.admin.store.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.cost.vo.MemberBillModel;
import com.xiangrui.lmp.business.admin.frontuser.vo.FrontUserAddress;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

public interface FrontUserMapper {

    /**
     * 前台用户信息
     * @param user_id
     * @return
     */
    FrontUser queryFrontUserById(int user_id);

    /**
     * 前台用户信息
     * @param user_name
     * @return
     */
    FrontUser queryFrontUserByName(String user_name);
    
    /**
     * 前台用户信息
     * @param params
     * @return
     */
    List<FrontUser> queryFrontUser(Map<String, Object> params);
    
    /**
     * 前台用户信息
     * @param params
     * @return
     */
    List<FrontUserAddress> queryFrontUserAddress(Map<String, Object> params);
    
    /**
     * 分配同行账号
     * @param frontUser
     * @return
     */
    void insertFrontUser(FrontUser frontUser);
    
    /**
     * 更新账户状态
     * @param frontUser
     * @return
     */
    void updatefrontUserStatus(FrontUser frontUser);

	int queryFrontUserMaxUserId();

    /**
     * 更新账户是否冻结状态
     * @param frontUser
     * @return
     */
    int updatefrontUserIsFreeze(FrontUser frontUser);
    
    /**
     * 更新余额
     * @param frontUser
     * @return
     */
    void updateBalance(FrontUser frontUser);

    /**
     * 同行用户待合计账单查询
     * @param frontUser
     * @return
     */
    List<FrontUser> queryFrontUserBill(Map<String, Object> params);
    
    /**
     * 多用户账单明细
     * @param userIds
     * @return
     */
    List<MemberBillModel> queryFrontUserBillByuserIds(List<String> userIds);
    
    MemberBillModel queryFrontUserBillByPackageId(int package_id);
    
    MemberBillModel queryMemberBillModelByPackageId(int package_id);
    
    List<FrontUser> queryFrontUserByAccount(Map<String, Object> params);
    List<FrontUser> queryFrontUserByLastName(Map<String, Object> params);
    
    List<FrontUser> queryFrontUserLikeByAccount(Map<String, Object> params);
    /**
     * 修改用户的业务名和快件渠道
     * @param frontUser
     * @return
     */
    void updateFrontUserBussinessNameAndSelectExpressChannel(FrontUser frontUser);
    
    /**
     * 查询有冻结金额的用户
     * @return
     */
    List<FrontUser> queryFrozenBalanceFrontUser();

	List<FrontUser> queryFrontUserByUserType(Map<String, Object> fparams);

	List<FrontUser> queryFrontUserLikeByAccountType(Map<String, Object> params);

	void updateAuthQuotaByUserId(int user_id);

	void updateAuthQuotaOKByUserId(int user_id);

	List<FrontUser> queryFrontUserByNotAble(Map<String, Object> fparams);

	void deleteAuthQuotaTeshuByUserId(int user_id);

	List<FrontUser> queryAuthQuota(int user_id);

	void updateAuthQuotaByUserId2(Map<String, Object> fparams);

	List<FrontUser> selectAuthAccountByInfo(Map<String, Object> map);

	void updateAuthQuotaByUserId1(Map<String, Object> map);

	List<FrontUser> selectAuthAccountByDelete(Map<String, Object> map);

	/**
	 * 提现申请前更新alipayName
	 * @param param
	 */
	void updateAlipayName(Map<String, Object> param);


	void updatefrontUserBalance(FrontUser frontUser);
}
