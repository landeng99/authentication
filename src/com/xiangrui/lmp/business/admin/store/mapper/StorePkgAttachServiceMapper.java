package com.xiangrui.lmp.business.admin.store.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;

public interface StorePkgAttachServiceMapper {
	
	/**
	 * 
	 * 根据用户包裹标识查询包裹的增值服务
	 * @param pkgId
	 * @return
	 */
	List<PkgAttachServiceGroup> queryPkgAttach(int pkgId);
	
}
