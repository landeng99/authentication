package com.xiangrui.lmp.business.admin.store.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.store.vo.StorePackage;

public interface StoreMapper {
	/**
	 * 根据参数查询包裹信息
	 * @param params
	 * @return
	 */
	List<StorePackage> queryPkgs(StorePackage pkg);
	
	List<StorePackage> queryPkgsLike(StorePackage pkg);
	
	
	/**
	 * 更新包裹状态
	 * @param pkg
	 * @return
	 */
	int updatePkgForStore(StorePackage pkg);
	
	/**
	 * 更新包裹的到库状态
	 * @param pkg
	 * @return
	 */
	int updateStoreArriveStatus(StorePackage pkg);
	
	/**
	 * 更新包裹的需要处理标志
	 * @param pkg
	 * @return
	 */
	int updateNeedHandleFlag(StorePackage pkg);
	
	/**
	 * 更新包裹的到库显示标志
	 * @param pkg
	 * @return
	 */
	int updateArrivedPackageDisplayFlag(StorePackage pkg);
}
