package com.xiangrui.lmp.business.admin.seaport.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgGoodsService;
import com.xiangrui.lmp.business.admin.pkg.service.UserAddressService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods;
import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportDeclarationService;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.business.admin.seaport.vo.SeaportDeclaration;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.newExcel.ExcelReadUtil;
import com.xiangrui.lmp.util.newExcel.SheetConfig;

@Controller
@RequestMapping("/admin/seaport")
public class SeaportController
{

    @Autowired
    private SeaportService seaportService;
    @Autowired
    private SeaportDeclarationService seaportDeclarationService;
	@Autowired
	private PackageService packageService;
	@Autowired
	private FrontUserService frontUserService;
	@Autowired
	private UserAddressService userAddressService;
	@Autowired
	private OverseasAddressService overseasAddressService;
	@Autowired
	private PkgGoodsService pkgGoodsService;
    private static Map<String,List<String>> SEAPORT_DECLARATION_REIMPORT_MAP = new ConcurrentHashMap<String, List<String>>();
    /**
     * 口岸名称1：不存在
     */
    private static final String SNAME_NOTEXIST = "1";

    /**
     * 口岸名称0：存在
     */
    private static final String SNAME_EXIST = "0";

    /**
     * 口岸编码1：不存在
     */
    private static final String SCODE_NOTEXIST = "1";

    /**
     * 口岸编码0：存在
     */
    private static final String SCODE_EXIST = "0";

    /**
     * 逻辑删除0：未删除
     */
    private static final int ISDELETED_NO = 0;

    /**
     * 逻辑删除1：删除
     */
    private static final int ISDELETED_YES = 1;

    
    /**
     * 口岸列表初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/seaportInit")
    public String seaportInit(HttpServletRequest request)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);
        params.put("isdeleted", ISDELETED_NO);
        // 口岸列表
        List<Seaport> seaportList = seaportService.queryAllSeaport(params);
        // 口岸类型
        List<String> stypeList = seaportService.selectStype();

        if(seaportList!=null&&seaportList.size()>0)
        {
			for (Seaport seaport : seaportList)
			{
				int totalCount = seaportDeclarationService.countSeaportDeclarationBySeaportId(seaport.getSid());
				int leaveCount = seaportDeclarationService.countLeaveSeaportDeclarationBySeaportId(seaport.getSid());
				seaport.setTotalCount(totalCount);
				seaport.setUsedCount(totalCount-leaveCount);
			}
        }
        request.setAttribute("stypeList", stypeList);
        request.setAttribute("seaportList", seaportList);
        request.setAttribute("pageView", pageView);

        return "back/seaportList";
    }

    /**
     * 口岸列表查询
     * 
     * @param request
     * @param stype
     * @param sname
     * @param scode
     * @return
     */
    @RequestMapping("/seaportSearch")
    public String seaportSearch(HttpServletRequest request, String stype,
            String sname, String scode)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);
        // 口岸名称
        params.put("sname", sname);
        // 口岸编码
        params.put("scode", scode);
        // 口岸类型
        params.put("stype", stype);
        params.put("isdeleted", ISDELETED_NO);
        // 口岸列表
        List<Seaport> seaportList = seaportService.queryAllSeaport(params);

        // 口岸类型
        List<String> stypeList = seaportService.selectStype();
        if(seaportList!=null&&seaportList.size()>0)
        {
			for (Seaport seaport : seaportList)
			{
				int totalCount = seaportDeclarationService.countSeaportDeclarationBySeaportId(seaport.getSid());
				int leaveCount = seaportDeclarationService.countLeaveSeaportDeclarationBySeaportId(seaport.getSid());
				seaport.setTotalCount(totalCount);
				seaport.setUsedCount(totalCount-leaveCount);
			}
        }
        request.setAttribute("stypeList", stypeList);
        request.setAttribute("seaportList", seaportList);
        request.setAttribute("pageView", pageView);

        return "back/seaportList";
    }

    /**
     * 口岸添加初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/addSeaport")
    public String addSeaport(HttpServletRequest request)
    {

        return "back/addSeaport";
    }

    /**
     * 口岸名称重复检查
     * 
     * @param request
     * @param scode
     * @return
     */
    @RequestMapping("/checkSname")
    @ResponseBody
    public String checkSname(HttpServletRequest request, String sname)
    {

        String checkResult = SNAME_EXIST;
        Map<String, Object> params = new HashMap<String, Object>();
        // 口岸名称
        params.put("sname", sname);

        List<Seaport> seaportList = seaportService.checkSeaport(params);
        // 口岸名称不存在
        if (seaportList == null || seaportList.size() == 0)
        {
            checkResult = SNAME_NOTEXIST;
        }

        return checkResult;
    }

    /**
     * 口岸编码重复检查
     * 
     * @param request
     * @param scode
     * @return
     */
    @RequestMapping("/checkScode")
    @ResponseBody
    public String checkScode(HttpServletRequest request, String scode)
    {

        String checkResult = SCODE_EXIST;
        Map<String, Object> params = new HashMap<String, Object>();
        // 口岸编码
        params.put("scode", scode);

        List<Seaport> seaportList = seaportService.checkSeaport(params);
        // 口岸编码不存在
        if (seaportList == null || seaportList.size() == 0)
        {
            checkResult = SCODE_NOTEXIST;
        }
        return checkResult;
    }

    /**
     * 添加口岸
     * 
     * @param request
     * @param seaport
     * @return
     */
    @RequestMapping("/insertSeaport")
    @ResponseBody
    public String insertSeaport(HttpServletRequest request, Seaport seaport)
    {
    	String template_path=StringUtils.trimToNull(request.getParameter("template_path"));
        if(template_path!=null)
    	{
        	seaport.setOutput_print_template(template_path);
    	}
    	String leave_warning_count=StringUtils.trimToNull(request.getParameter("leave_warning_count"));
        if(leave_warning_count!=null)
    	{
        	seaport.setLeave_warning_count(Integer.parseInt(leave_warning_count));
    	}
        seaportService.insertSeaport(seaport);
        String cacheReImportId=StringUtils.trimToNull(request.getParameter("cacheReImportId"));
        if(cacheReImportId!=null)
        {
        	Seaport newSeaport=seaportService.querySeaportBySname(seaport.getSname());
			if (newSeaport != null)
			{
				HttpSession session = request.getSession();
				User user = (User) session.getAttribute("back_user");
				List<String> list = SEAPORT_DECLARATION_REIMPORT_MAP.get(cacheReImportId);
				if (list != null && list.size() > 0)
				{
					for (String declarationCode : list)
					{
						SeaportDeclaration seaportDeclaration = new SeaportDeclaration();
						seaportDeclaration.setDeclaration_code(declarationCode);
						seaportDeclaration.setSeaport_id(newSeaport.getSid());
						seaportDeclaration.setUse_flag(0);
						seaportDeclaration.setCreate_user_id(user.getUser_id());
						seaportDeclarationService.insertSeaportDeclaration(seaportDeclaration);
					}
				}
			}
        }
        return null;
    }

    /**
     * 口岸列表详情初始化
     * 
     * @param request
     * @param sid
     * @return
     */
    @RequestMapping("/seaportDetail")
    public String seaportDetail(HttpServletRequest request, String sid)
    {

        Seaport seaport = new Seaport();

        seaport.setSid(Integer.parseInt(sid));
        // 口岸详情
        List<Seaport> seaportList = seaportService.querySeaport(seaport);
        request.setAttribute("seaport", seaportList.get(0));
        request.setAttribute("totalCount", seaportDeclarationService.countSeaportDeclarationBySeaportId(seaport.getSid()));
        request.setAttribute("leaveCount", seaportDeclarationService.countLeaveSeaportDeclarationBySeaportId(seaport.getSid()));
        SeaportDeclaration currentSeaportDeclaration=seaportDeclarationService.queryCurrentUseDeclarationBySeaportId(seaport.getSid());
        String currentCode="";
        if(currentSeaportDeclaration!=null)
        {
        	currentCode=currentSeaportDeclaration.getDeclaration_code();
        }
        request.setAttribute("currentCode", currentCode);

        return "back/seaportDetail";
    }

    /**
     * 更新口岸
     * 
     * @param request
     * @param seaport
     * 
     * @return
     */
    @RequestMapping("/updateSeaport")
    @ResponseBody
    public String updateSeaport(HttpServletRequest request, Seaport seaport)
    {
    	String template_path=StringUtils.trimToNull(request.getParameter("template_path"));
        if(template_path!=null)
    	{
        	seaport.setOutput_print_template(template_path);
    	}
    	String leave_warning_count=StringUtils.trimToNull(request.getParameter("leave_warning_count"));
        if(leave_warning_count!=null)
    	{
        	seaport.setLeave_warning_count(Integer.parseInt(leave_warning_count));
    	}
        seaportService.updateSeaport(seaport);
        String cacheReImportId=StringUtils.trimToNull(request.getParameter("cacheReImportId"));
		if (cacheReImportId != null)
		{
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			List<String> list = SEAPORT_DECLARATION_REIMPORT_MAP.get(cacheReImportId);
			if (list != null && list.size() > 0)
			{
				for (String declarationCode : list)
				{
					SeaportDeclaration seaportDeclaration = new SeaportDeclaration();
					seaportDeclaration.setDeclaration_code(declarationCode);
					seaportDeclaration.setSeaport_id(seaport.getSid());
					seaportDeclaration.setUse_flag(0);
					seaportDeclaration.setCreate_user_id(user.getUser_id());
					seaportDeclarationService.insertSeaportDeclaration(seaportDeclaration);
				}
			}
		}
        return null;
    }

    /**
     * 逻辑删除口岸
     * 
     * @param request
     * @param seaport
     * 
     * @return
     */
    @RequestMapping("/deleteSeaport")
    @ResponseBody
    public String deleteSeaport(HttpServletRequest request, Seaport seaport)
    {
        seaport.setIsdeleted(ISDELETED_YES);
        seaportService.updateSeaportIsdeleted(seaport);

        return null;
    }

    /**
     * 导入 报关号码
     * @param request
     * @return
     */
    @RequestMapping("/importDeclaration")
    @ResponseBody
    public Map<String, String> importDeclaration(HttpServletRequest request)
    {
    	Map<String, String> result = new HashMap<String, String>();
    	String flag="F";//默认导入失败
    	String message="导入失败";
    	
		MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
		//String seaportId=StringUtils.trimToNull(req.getParameter("osaddr_id"));

		MultipartFile excelFile = req.getFile("import_data_file");
		// 文件名
		String fileName = excelFile.getOriginalFilename();
		// 后缀名
		String exName = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
		try
		{
			Workbook workbook = null;
			if ("xls".equals(exName))
			{
				workbook = new HSSFWorkbook(excelFile.getInputStream());
			}
			else
			{
				workbook = new XSSFWorkbook(excelFile.getInputStream());
			}
			Map<String, String> colMap = new HashMap<String, String>();
			colMap.put("declarationNum", "口岸号码(报关号码)");
			List<Map<String, Object>> list = ExcelReadUtil.parseExcel(workbook, colMap, new SheetConfig());
			if(list!=null&&list.size()>0)
			{
				List<String> canImportDeclarationList = new ArrayList<String>();
				for(Map<String,Object> map:list)
				{
					String declarationNum=(String) map.get("declarationNum");
					if(seaportDeclarationService.countSeaportDeclarationByDeclarationCode(declarationNum)==0)
					{
						canImportDeclarationList.add(declarationNum);
					}
				}
				if(canImportDeclarationList.size()==list.size())
				{
					flag="S";
					message="预导入成功";
				}else if(canImportDeclarationList.size()>=0&&canImportDeclarationList.size()>list.size())
				{
					flag="S";
					message="预导入成功,但上传报关号码有重复，系统已经自动去重";
				}else
				{
					flag="F";
					message="预导入失败,上传报关号码在系统中已经存在，请重新确认后再上传";
				}
				if("S".equalsIgnoreCase(flag))
				{
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
					String cacheReImportId=simpleDateFormat.format(new Date())+RandomStringUtils.randomAlphabetic(5);
					SEAPORT_DECLARATION_REIMPORT_MAP.put(cacheReImportId, canImportDeclarationList);
					result.put("cacheReImportId", cacheReImportId);
				}
			}
		} catch (IOException e)
		{
		}
		result.put("flag", flag);
		result.put("message", message);
        return result;
    }
    
    /**
     * 上传口岸打印出库面单模板
     * @param request
     * @return
     */
    @RequestMapping("/uploadSeaportPrint")
    @ResponseBody
    public Map<String, String> uploadSeaportPrint(HttpServletRequest request)
    {
    	Map<String, String> result = new HashMap<String, String>();
    	String flag="F";//默认上传失败
    	String message="上传失败";
    	
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();

		String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "WEB-INF" + File.separator + "page" + File.separator + "output_template" + File.separator;
		File file = new File(ctxPath);

		if (!file.exists())
		{
			file.mkdirs();
		}
		// 上传文件名
		String fileName = null;
		// 返回前台预览模板路径
		String template_path = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet())
		{
			MultipartFile mf = entity.getValue();
			fileName = mf.getOriginalFilename();
			String newFilename = "";
			template_path = "output_template/";
			try
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				String newName = format.format(new Date());
				newName += RandomStringUtils.randomAlphanumeric(5);
				String subfix = StringUtils.substringAfterLast(fileName, ".");
				template_path = template_path + newName;
				newName += "." + subfix;
				newFilename = newName;
				// 将文件输出
				String path = ctxPath + newFilename;
				File uploadFile = new File(path);
				FileCopyUtils.copy(mf.getBytes(), uploadFile);

				template_path=template_path.replaceAll("/", "-");
				result.put("template_path", template_path);
		    	flag="S";//默认上传失败
		    	message="上传成功";
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
    	
		result.put("flag", flag);
		result.put("message", message);
        return result;
    }
    
    /**
     * 预览出库打印模板
     * @param request
     * @return
     */
    @RequestMapping("/reviewOutputTemplateDemo")
    public String reviewOutputTemplateDemo(HttpServletRequest request)
    {
    	String template_path=StringUtils.trimToNull(request.getParameter("template_path"));
		if (template_path != null)
		{
			template_path = template_path.replaceAll("-", "/");
			int id = seaportDeclarationService.queryReviewDemoPackageId();
			// 包裹
			Pkg pkgDetail = packageService.queryPackageById(id);

			// 客户信息
			FrontUser frontUser = frontUserService.queryFrontUserById(pkgDetail.getUser_id());

			String address = "";
			// 包裹地址
			UserAddress userAddress = userAddressService.queryAddressById(pkgDetail.getAddress_id());
			if (userAddress != null)
			{
				// 地址拼接
				address = addressInfo(userAddress.getRegion()) + userAddress.getStreet();
			}

			String osaddr = "";

			// 海外仓库地址
			OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
			if (overseasAddress != null)
			{
				// 海外地址拼接
				osaddr = joinOverseasAddress(overseasAddress);
			}
			// 商品
			List<PkgGoods> pkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(id);

			// 计算总价值
			BigDecimal total_worth = new BigDecimal("0");
			for (PkgGoods pkgGoods : pkgGoodsList)
			{
				total_worth = total_worth.add(new BigDecimal(pkgGoods.getPrice()).multiply(new BigDecimal(pkgGoods.getQuantity())));
			}

			// 页面显示信息
			request.setAttribute("pkgDetail", pkgDetail);
			request.setAttribute("frontUser", frontUser);
			request.setAttribute("userAddress", userAddress);
			request.setAttribute("address", address);
			request.setAttribute("osaddr", osaddr);
			request.setAttribute("pkgGoodsList", pkgGoodsList);
			request.setAttribute("total_worth", total_worth);
			request.setAttribute("express_logo", "/theme/images/demo_express_logo.png");// 快递logo
			request.setAttribute("declaration_code", "BE20160914100");// 报关单号
			if( -1!=address.indexOf("北京") ){
                request.setAttribute("channel_code", "");// 渠道号
                request.setAttribute("channel_num", 1);// 渠道数字
            }
            else if( -1!=address.indexOf("河北") //河北省、宁夏回族自治区、青海省、天津市、西藏自治区、新疆维吾尔自治区、甘肃省
                || -1!=address.indexOf("宁夏")
                || -1!=address.indexOf("青海")
                || -1!=address.indexOf("天津")
                || -1!=address.indexOf("西藏")
                || -1!=address.indexOf("新疆")
                || -1!=address.indexOf("甘肃") ){
                request.setAttribute("channel_code", "B");// 渠道号    
                request.setAttribute("channel_num", 2);// 渠道数字
            }
            else {
                request.setAttribute("channel_code", "J");// 渠道号
                request.setAttribute("channel_num", 3);// 渠道数字
            }
		}
        return template_path;
    }
    
	/**
	 * 海外地址拼接
	 * 
	 * @param osa
	 * @return String
	 */
	private String joinOverseasAddress(OverseasAddress osa)
	{
		String osaddr = osa.getAddress_first() + osa.getCity() + osa.getState() + osa.getCounty() + osa.getCountry();
		return osaddr;
	};

	/**
	 * 解析省市县
	 * 
	 * @param request
	 * @param jsonString
	 * @return
	 */
	private String addressInfo(String jsonString)
	{
		if (StringUtil.isEmpty(jsonString))
		{
			return "";
		}
		List<String> aList = new ArrayList<String>();
		try
		{
			aList = JSONUtil.readValueFromJson(jsonString, "name");
		} catch (Exception e)
		{
			e.printStackTrace();
			return "";
		}
		StringBuffer address = new StringBuffer("");
		for (String city : aList)
		{
			address.append(city);
		}

		return address.toString().replaceAll("\"", "");
	}
	
    @RequestMapping("/clearUnuseDeclaration")
    @ResponseBody
    public String clearUnuseDeclaration(HttpServletRequest request)
    {
    	String sid=StringUtils.trimToNull(request.getParameter("sid"));
        if(sid!=null)
    	{
        	int seaport_id =Integer.parseInt(sid);
        	seaportDeclarationService.delSeaportDeclarationBySeaportId(seaport_id);
    	}
    	return null;
    }
    
    /**
     * 
     * @param request
     * @return
     */
    @RequestMapping("/printOutput")
    public String printOutput(HttpServletRequest request,int package_id)
    {
		String seaport_idstr = StringUtils.trimToNull(request.getParameter("seaport_id"));
		String template_path = null;
		if (seaport_idstr != null)
		{
			Seaport seaport = seaportService.querySeaportBySid(Integer.parseInt(seaport_idstr));
			template_path = seaport.getOutput_print_template();
			if (template_path != null)
			{

				template_path = template_path.replaceAll("-", "/");
				// 包裹
				Pkg pkgDetail = packageService.queryPackageById(package_id);

				// 客户信息
				FrontUser frontUser = frontUserService.queryFrontUserById(pkgDetail.getUser_id());

				String address = "";
				// 包裹地址
				UserAddress userAddress = userAddressService.queryAddressById(pkgDetail.getAddress_id());
				if (userAddress != null)
				{
					// 地址拼接
					address = addressInfo(userAddress.getRegion()) + userAddress.getStreet();
				}

				String osaddr = "";

				// 海外仓库地址
				OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
				if (overseasAddress != null)
				{
					// 海外地址拼接
					osaddr = joinOverseasAddress(overseasAddress);
					request.setAttribute("country", overseasAddress.getCountry());
				}
				// 商品
				List<PkgGoods> pkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(package_id);

				// 计算总价值
				BigDecimal total_worth = new BigDecimal("0");
				for (PkgGoods pkgGoods : pkgGoodsList)
				{
					total_worth = total_worth.add(new BigDecimal(pkgGoods.getPrice()).multiply(new BigDecimal(pkgGoods.getQuantity())));
				}
				
				SeaportDeclaration seaportDeclaration=seaportDeclarationService.queryDeclarationBySeaportIdAndPackageId(seaport.getSid(), pkgDetail.getPackage_id());
				String declaration_code="";
				if(seaportDeclaration!=null)
				{
					declaration_code=seaportDeclaration.getDeclaration_code();
				}
				// 页面显示信息
				request.setAttribute("pkgDetail", pkgDetail);
				request.setAttribute("frontUser", frontUser);
				request.setAttribute("userAddress", userAddress);
				request.setAttribute("address", address);
				request.setAttribute("osaddr", osaddr);
				request.setAttribute("pkgGoodsList", pkgGoodsList);
				request.setAttribute("total_worth", total_worth);
				request.setAttribute("express_logo", "/theme/images/demo_express_logo.png");// 快递logo
				request.setAttribute("declaration_code", declaration_code);// 报关单号
				if( -1!=address.indexOf("北京") ){
				    request.setAttribute("channel_code", "");// 渠道号
				    request.setAttribute("channel_num", 1);// 渠道数字
				}
				else if( -1!=address.indexOf("河北") //河北省、宁夏回族自治区、青海省、天津市、西藏自治区、新疆维吾尔自治区、甘肃省
				    || -1!=address.indexOf("宁夏")
				    || -1!=address.indexOf("青海")
				    || -1!=address.indexOf("天津")
				    || -1!=address.indexOf("西藏")
				    || -1!=address.indexOf("新疆")
				    || -1!=address.indexOf("甘肃") ){
				    request.setAttribute("channel_code", "B");// 渠道号    
				    request.setAttribute("channel_num", 2);// 渠道数字
				}
				else {
				    request.setAttribute("channel_code", "J");// 渠道号
				    request.setAttribute("channel_num", 3);// 渠道数字
                }
			}else
			{
				template_path="back/error_output_print";
			}
		}
		return template_path;
    }
}
