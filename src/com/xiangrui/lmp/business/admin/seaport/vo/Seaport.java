package com.xiangrui.lmp.business.admin.seaport.vo;

import com.xiangrui.lmp.business.base.BaseSeaport;

public class Seaport extends BaseSeaport
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private int usedCount;
    
    private int totalCount;

	public int getUsedCount()
	{
		return usedCount;
	}

	public void setUsedCount(int usedCount)
	{
		this.usedCount = usedCount;
	}

	public int getTotalCount()
	{
		return totalCount;
	}

	public void setTotalCount(int totalCount)
	{
		this.totalCount = totalCount;
	}
    

}
