package com.xiangrui.lmp.business.admin.seaport.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.seaport.mapper.SeaportMapper;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;

@Service(value = "seaportService")
public class SeaportServiceImpl implements SeaportService
{

    @Autowired
    private SeaportMapper seaportMapper;

    /**
     * 根据 参数查询口岸
     * 
     * @param params
     * @return
     */
    @Override
    public List<Seaport> querySeaport(Seaport seaport)
    {
        return seaportMapper.querySeaport(seaport);
    }

    /**
     * 分页查询口岸
     * 
     * @param params
     * @return
     */
    @Override
    public List<Seaport> queryAllSeaport(Map<String, Object> params)
    {
        return seaportMapper.queryAllSeaport(params);
    }

    /**
     * 口岸查重
     * 
     * @param params
     * @return
     */
    @Override
    public List<Seaport> checkSeaport(Map<String, Object> params)
    {
        return seaportMapper.checkSeaport(params);
    }

    /**
     * 新增口岸
     * 
     * @param seaport
     * @return
     */
    @SystemServiceLog(description = "新增口岸信息")
    @Override
    public void insertSeaport(Seaport seaport)
    {
        seaportMapper.insertSeaport(seaport);
    }

    /**
     * 口岸更新
     * 
     * @param seaport
     * @return
     */
    @SystemServiceLog(description = "更新口岸信息")
    @Override
    public void updateSeaport(Seaport seaport)
    {
        seaportMapper.updateSeaport(seaport);
    }

    /**
     * 查询所有的口岸
     * 
     * @return
     */
    @Override
    public List<Seaport> queryAll()
    {
        return seaportMapper.queryAll();
    }

    /**
     * 口岸逻辑删除
     * 
     * @param seaport
     * @return
     */
    @SystemServiceLog(description = "删除口岸信息")
    @Override
    public void updateSeaportIsdeleted(Seaport seaport)
    {
        seaportMapper.updateSeaportIsdeleted(seaport);
    }

    /**
     * 口岸类型
     * 
     * @return
     */
    @Override
    public List<String> selectStype()
    {
        return seaportMapper.selectStype();

    }
    /**
     * 根据sname 查询
     * @param sname
     * @return
     */
    public Seaport querySeaportBySname(String sname)
    {
    	return seaportMapper.querySeaportBySname(sname);
    }
    
    /**
     * 根据sid 查询
     * @param sid
     * @return
     */
    public Seaport querySeaportBySid(Integer sid)
    {
    	return seaportMapper.querySeaportBySid(sid);
    }
}
