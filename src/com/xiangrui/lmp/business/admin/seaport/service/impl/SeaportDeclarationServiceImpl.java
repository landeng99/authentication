package com.xiangrui.lmp.business.admin.seaport.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.seaport.mapper.SeaportDeclarationMapper;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportDeclarationService;
import com.xiangrui.lmp.business.admin.seaport.vo.SeaportDeclaration;

@Service(value = "seaportDeclarationService")
public class SeaportDeclarationServiceImpl implements SeaportDeclarationService
{
	@Autowired
	private SeaportDeclarationMapper seaportDeclarationMapper;

	@Override
	public void insertSeaportDeclaration(SeaportDeclaration seaportDeclaration)
	{
		seaportDeclarationMapper.insertSeaportDeclaration(seaportDeclaration);
	}

	@Override
	public void useSeaportDeclaration(SeaportDeclaration seaportDeclaration)
	{
		seaportDeclarationMapper.useSeaportDeclaration(seaportDeclaration);
	}

	@Override
	public int countSeaportDeclarationBySeaportId(Integer seaport_id)
	{
		return seaportDeclarationMapper.countSeaportDeclarationBySeaportId(seaport_id);
	}

	@Override
	public int countLeaveSeaportDeclarationBySeaportId(Integer seaport_id)
	{
		return seaportDeclarationMapper.countLeaveSeaportDeclarationBySeaportId(seaport_id);
	}

	@Override
	public int countSeaportDeclarationByDeclarationCode(String declaration_code)
	{
		return seaportDeclarationMapper.countSeaportDeclarationByDeclarationCode(declaration_code);
	}

	@Override
	public void delSeaportDeclarationBySeaportId(Integer seaport_id)
	{
		seaportDeclarationMapper.delSeaportDeclarationBySeaportId(seaport_id);
	}
	
	public int queryReviewDemoPackageId()
	{
		return seaportDeclarationMapper.queryReviewDemoPackageId();
	}
	
	public SeaportDeclaration queryCurrentUseDeclarationBySeaportId(Integer seaport_id)
	{
		return seaportDeclarationMapper.queryCurrentUseDeclarationBySeaportId(seaport_id);
	}
	
	/**
     * 通过包裹ID和口岸ID来查询报关号码
     * @param seaport_id
     * @param package_id
     * @return
     */
	public SeaportDeclaration queryDeclarationBySeaportIdAndPackageId(Integer seaport_id,Integer package_id)
    {
		SeaportDeclaration seaportDeclaration=null;
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("package_id", package_id);
		params.put("seaport_id", seaport_id);
		List<SeaportDeclaration> list =seaportDeclarationMapper.queryDeclarationBySeaportIdAndPackageId(params);
		if(list!=null&&list.size()>0)
		{
			seaportDeclaration=list.get(0);
		}
		return seaportDeclaration;
    }
	
	public boolean checkPackageHaveUsedSeaportDeclarationByPackage_id(int package_id)
	{
		boolean used=false;
		if(seaportDeclarationMapper.countSeaportDeclarationByPackage_id(package_id)>0)
		{
			used=true;
		}
		return used;
	}
}
