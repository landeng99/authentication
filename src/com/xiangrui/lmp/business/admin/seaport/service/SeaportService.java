package com.xiangrui.lmp.business.admin.seaport.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;

public interface SeaportService
{
    List<Seaport> querySeaport(Seaport seaport);

    /**
     * 分页查询口岸
     * 
     * @param params
     * @return
     */
    List<Seaport> queryAllSeaport(Map<String, Object> params);

    /**
     * 口岸查重
     * 
     * @param params
     * @return
     */

    List<Seaport> checkSeaport(Map<String, Object> params);

    /**
     * 新增口岸
     * 
     * @param seaport
     * @return
     */
    void insertSeaport(Seaport seaport);

    /**
     * 口岸更新
     * 
     * @param seaport
     * @return
     */
    void updateSeaport(Seaport seaport);

    /**
     * 查询所有的口岸
     * 
     * @return
     */
    List<Seaport> queryAll();

    /**
     * 口岸逻辑删除
     * 
     * @param seaport
     * @return
     */
    void updateSeaportIsdeleted(Seaport seaport);

    /**
     * 口岸类型
     * 
     * @return
     */
    List<String> selectStype();
    /**
     * 根据sname 查询
     * @param sname
     * @return
     */
    Seaport querySeaportBySname(String sname);
    
    Seaport querySeaportBySid(Integer sid);
}
