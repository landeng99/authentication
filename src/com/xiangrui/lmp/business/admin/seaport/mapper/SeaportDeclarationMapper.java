package com.xiangrui.lmp.business.admin.seaport.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.seaport.vo.SeaportDeclaration;

public interface SeaportDeclarationMapper
{
	/**
	 * 新增报关号码
	 * 
	 * @param seaport
	 * @return
	 */
	void insertSeaportDeclaration(SeaportDeclaration seaportDeclaration);

	/**
	 * 使用报关号码
	 * 
	 * @param seaport
	 * @return
	 */
	void useSeaportDeclaration(SeaportDeclaration seaportDeclaration);

	/**
	 * 统计口岸的所有报关号码
	 * 
	 * @param seaport_id
	 * @return
	 */
	int countSeaportDeclarationBySeaportId(Integer seaport_id);

	/**
	 * 统计口岸的未使用的报关号码数
	 * 
	 * @param seaport_id
	 * @return
	 */
	int countLeaveSeaportDeclarationBySeaportId(Integer seaport_id);

	/**
	 * 检查报关号码是否在系统中存在
	 * 
	 * @param seaport_id
	 * @return
	 */
	int countSeaportDeclarationByDeclarationCode(String declaration_code);

	/**
	 * 清除口岸中未使用的报关号码
	 * 
	 * @param seaport_id
	 */
	void delSeaportDeclarationBySeaportId(Integer seaport_id);

	int queryReviewDemoPackageId();

	SeaportDeclaration queryCurrentUseDeclarationBySeaportId(Integer seaport_id);

	/**
	 * 通过包裹ID和口岸ID来查询报关号码
	 * 
	 * @param seaport_id
	 * @param package_id
	 * @return
	 */
	List<SeaportDeclaration> queryDeclarationBySeaportIdAndPackageId(Map<String, Object> params);

	/**
	 * 检查包裹是否已经分配有报关号码是
	 * @param package_id
	 * @return
	 */
	int countSeaportDeclarationByPackage_id(int package_id);
}
