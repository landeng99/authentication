package com.xiangrui.lmp.business.admin.pkglog.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;

/**
 * 包裹状态记录逻辑
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-7-2 下午2:44:45
 * </p>
 */
public interface PkgLogService
{
    /**
     * 添加包裹操作记录
     * @param pkgLog
     */
    void pkgLogCreate(PkgLog pkgLog);

    /**
     * 修改包裹日记记录  主要用户修改包裹日志的时间
     * @param pkgLog
     */

    void pkgLogUpdate(PkgLog pkgLog);
    /**
     * 根据包裹id获取日志记录
     * @param package_id 包裹id
     * @return
     */
    List<PkgLog> queryPkgLogByPackageId(int package_id);
    
    /**
     * 根据包裹id和包裹状态获取日志记录
     * @param package_id
     * @param operated_status
     * @return
     */
    PkgLog queryPkgLogByPackageIdAndStatus(int package_id,int operated_status);
    
    /**
     * 批量插入 日志
     * @param pkgLogList
     */
    void pkgLogBatchCreate(List<PkgLog> pkgLogList);

	/**
	 * 通过packageID和清关日期区间来查询包裹是否存在
	 * 
	 * @param packageId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	List<PkgLog> queryPkgLogByPackageId(Integer packageId,String fromDate, String toDate);
}
