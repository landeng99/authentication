package com.xiangrui.lmp.business.admin.pkglog.vo;

import com.xiangrui.lmp.business.base.BasePkgLog;

/**
 * 包裹记录实体
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-7-2 下午2:39:53
 * </p>
 */
public class PkgLog extends BasePkgLog
{

    private static final long serialVersionUID = 1L;

}
