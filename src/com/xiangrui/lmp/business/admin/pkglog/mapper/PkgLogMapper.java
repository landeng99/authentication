package com.xiangrui.lmp.business.admin.pkglog.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;

/**
 * 包裹状态记录数据逻辑,实现
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-7-2 下午2:44:56
 * </p>
 */
public interface PkgLogMapper
{

    /**
     * 添加包裹操作记录
     * @param pkgLog
     */
    void pkgLogCreate(PkgLog pkgLog);

    /**
     * 根据包裹id获取日志记录
     * @param package_id 包裹id
     * @return
     */
    List<PkgLog> queryPkgLogByPackageId(int package_id);

    /**
     * 修改包裹日记记录  主要用户修改包裹日志的时间
     * @param pkgLog
     */
    void pkgLogUpdate(PkgLog pkgLog);

    /**
     * 根据包裹id和包裹状态获取日志记录
     * @param pkgLog
     * @return
     */
    PkgLog queryPkgLog(PkgLog pkgLog);
    
    
    /**
     * 批量记录日志
     * @param pkgLogList
     */
    void pkgLogBatchCreate(List<PkgLog> pkgLogList);
    
    List<PkgLog> queryPkgLogByPackageIdAndDateRange(Map<String, Object> params);
}
