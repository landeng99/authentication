package com.xiangrui.lmp.business.admin.pushLink.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pushLink.vo.PushLink;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;

public interface PushLinkService {

	List<PushLink> queryAll(Map<String, Object> params);

	PushLink createPush(PushLink pushLink, String ctxPath) throws Exception;

	void updateBusinessById(Integer id);

	PushLink insertPush(PushLink pushLink);

	List<FrontUser> queryAllCount(Map<String, Object> params);

	String validateBusinessName(String businessName);


}
