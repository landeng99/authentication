package com.xiangrui.lmp.business.admin.pushLink.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.pushLink.service.PushLinkService;
import com.xiangrui.lmp.business.admin.pushLink.vo.PushLink;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 业务员推广
 * 
 * @author Windows10
 */
@Controller
@RequestMapping("/admin/pushLink")
public class PushLinkController
{

	// 业务推广
	@Autowired
	private PushLinkService pushLinkService;
	
	//检索条件
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";


	
	/**
	 * 推广链接初始化页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/initPage")
	public String initPage(HttpServletRequest request, HttpServletResponse response)
	{

		// 分页
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCount = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCount, 20, 0);
		} else
		{
			pageView = new PageView(1, 20, 0);
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);

		// 推广链接
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{

			params.put("back_user_id", user.getUser_id());
			List<PushLink> linkList = this.pushLinkService.queryAll(params);
			request.setAttribute("linkList", linkList);
		}

		request.setAttribute("params", params);
		request.setAttribute("pageView", pageView);
		return "/back/initPage";
	}
	
	
	/**
	 * 推广链接新增
	 * @param request
	 * @param response
	 * @param pushLink
	 * @return
	 * @throws IOException 
	 * @throws Exception
	 */
	@RequestMapping("/addBusiness")
	public Map<String, Object> addBusiness(HttpServletRequest request,HttpServletResponse response, PushLink pushLink) 
	{
		//校验业务名
		String businessName = request.getParameter("businessName");
		boolean flag = false;
		Map<String,Object> result = new HashMap<String, Object>();
		String validateName = this.pushLinkService.validateBusinessName(businessName);
		if (validateName != null || StringUtils.isNotEmpty(validateName))
		{
			 result.put("flag", flag);
		} else
		{ 
			//更新推广链接业务名、创建时间和删除状态
			pushLink.setId(pushLink.getId());
			pushLink.setBusinessName(businessName);
			pushLink.setCreateTime(new Timestamp(System.currentTimeMillis()));
			pushLink.setDeleteStatus(0);
			pushLink = this.pushLinkService.insertPush(pushLink);
			
			//生成推广链接和推广二维码
			String code = StringUtils.trimToNull(pushLink.getPushCode());
			String link = StringUtils.trimToNull(pushLink.getPushLink());
			if ((code == null) || (link == null))
			{
				String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource" + File.separator +"upload" + File.separator +"pushCode" + File.separator;
				try
				{
					pushLink = this.pushLinkService.createPush(pushLink,ctxPath);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			} 
			flag = true;
			request.setAttribute("pushLink", pushLink);
		}
		result.put("flag", flag);
		return result;
	}
	
	/**
	 * 推广链接删除
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteBusiness")
	public String deleteBusiness(HttpServletRequest request, HttpServletResponse response, Integer id)
	{
		this.pushLinkService.updateBusinessById(id);
		return "/back/initPage";
	}
	
	
	/**
	 * 推广统计初始化
	 * @param 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/initCount")
	public String initCount(HttpServletRequest request, HttpServletResponse response)
	{

		// 分页
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCount = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCount, 10, 0);
		} else
		{
			pageView = new PageView(1, 10, 0);
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);

		// 推广链接
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			params.put("back_user_id", user.getUser_id());
			List<FrontUser> countList = this.pushLinkService.queryAllCount(params);	
			request.setAttribute("countList", countList);
		}
		
		request.setAttribute("params", params);
		request.setAttribute("pageView", pageView);
		return "/back/initCount";
	}
	
	
	/**
	 * 推广统计查询
	 * @param request
	 * @param response
	 * @param account
	 * @param user_name
	 * @param businessName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/search")
	public String search(HttpServletRequest request, HttpServletResponse response, String account, String user_name, String businessName)
	{
		
		//分页
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		Map<String, Object> params = new HashMap<String, Object>();
		if (StringUtils.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView  = new PageView(pageCurrent, 10, 0);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else
		{
			pageView = new PageView(1, 10, 0);
			//客户账号
			params.put("account", account);
			//客户名
			params.put("user_name", user_name);
			//业务名
			params.put("businessName", businessName);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}
		
		params.put("pageView", pageView);
		List<FrontUser> countList = this.pushLinkService.queryAllCount(params);
		request.setAttribute("params", params);
		request.setAttribute("pageView", pageView);
		request.setAttribute("countList", countList);
		
		return "/back/initCount";
	}
}
