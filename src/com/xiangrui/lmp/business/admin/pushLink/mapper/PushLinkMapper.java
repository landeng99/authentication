package com.xiangrui.lmp.business.admin.pushLink.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pushLink.vo.PushLink;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;

public interface PushLinkMapper {
	
	/**
	 * 推广链接业务员信息
	 * @param params
	 * @return
	 */
	List<PushLink> queryAll(Map<String, Object> params);

	/**
	 * 新增业务员信息
	 * @author Windows10
	 * @param pushLink
	 * @return
	 */
	void createPush(PushLink pushLink);
	
	/**
	 * 通过Id删除推广信息
	 * @param id
	 */
	void updateBusinessById(Integer id);

	void insertPush(PushLink pushLink);
	
	/**
	 * 推广统计初始化查询
	 * @param params
	 * @return
	 */
	List<FrontUser> queryAllCount(Map<String, Object> params);

	/**
	 * 新增业务名校验
	 * @param businessName 
	 * @return
	 */
	String validateBusinessName(String businessName);

}
