package com.xiangrui.lmp.business.admin.banner.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.banner.service.BannerService;
import com.xiangrui.lmp.business.admin.banner.vo.Banner;
import com.xiangrui.lmp.business.base.BaseBanner;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.constant.WebConstants;
import com.xiangrui.lmp.init.AppServerUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 轮播图
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-8 上午10:08:24
 *         </p>
 */
@Controller
@RequestMapping("/admin/banner")
public class BannerController
{
    /**
     * 图片后缀
     */
    private static final String[] IMG_SUFFIX_ARY = new String[] { ".bmp",
            ".jpg", ".jpeg", ".png", ".gif", ".pcx", ".tiff", ".tga", ".exif",
            ".fpx", ".svg", ".psd", ".cdr", ".pcd", ".dxf", ".ufo", ".eps",
            ".hdri", ".ai", ".raw" };

    private static final String EXCEL_SUFFIX = ".xls";
   
    private static final String EXCEL_SUFFIX_XLSX = ".xlsx";
 
    private static final String EXCEL_FILE = "resource"+File.separator+"upload"+File.separator+"template_001.xls";
    
    private static final String EXCEL_FILE_XLSX = "resource"+File.separator+"upload"+File.separator+"template_001.xlsx";
    /**
     * 上传图片在服务器存放路径
     */
    private static final String UPLOAD_IMG_PATH = AppServerUtil.getWebRoot()
            + WebConstants.UPLOAD_PATH_IMAGES;

    /**
     * 操作轮播图
     */
    @Autowired
    private BannerService bannerService;

    /**
     * 
     * @return
     */
    @RequestMapping("/uploadTemplateInit")
    public String uploadTemplateInit(){
        return "back/uploadTemplateInit";
    }
    
    @RequestMapping("/uploadTemplate")
    @ResponseBody
    public String uploadTempLate(HttpServletRequest request){
        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        MultipartFile excelFile = req.getFile("excelFile");

        // 获取文件后缀
        String excelSuffix = excelFile.getOriginalFilename()
                .substring(excelFile.getOriginalFilename().lastIndexOf("."))
                .toLowerCase();
        
        String fileNameString ="";

        // 不是execl中的xls格式,结束上传操作
        if (EXCEL_SUFFIX.equals(excelSuffix))
        {
            fileNameString = EXCEL_FILE;
        } else if (EXCEL_SUFFIX_XLSX.equals(excelSuffix))
        {
            fileNameString = EXCEL_FILE_XLSX;
        } else
        {
            return "";
        }

        try
        {
            File file = new File(getUploadImgPath(AppServerUtil.getWebRoot() + fileNameString));
            if(file.exists()){
                file.delete();
            }
            // 上传保存到服务器物理路径去
            FileUtils.copyInputStreamToFile(excelFile.getInputStream(),new File(getUploadImgPath(AppServerUtil.getWebRoot() + fileNameString)));
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // 返回到前台页面显示
        return SYSConstant.UPLOAD_IMG_PATH_DB + fileNameString;
    }
    /**
     * 查询所有轮播图
     * template_001
     * @param request
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);
        request.setAttribute("pageView", pageView);
        List<Banner> bannerLists = bannerService.queryAll(params);

        request.setAttribute("bannerLists", bannerLists);
        return "back/bannerInit";
    }

    /**
     * 启用或者禁用
     * 
     * @return
     */
    @RequestMapping("/updateBanner")
    public String updateBanner(HttpServletRequest request, Banner banner)
    {
        // 匹配数据库
        banner = bannerService.queryALlId(banner.getBanner_id());

        // 更新状态
        if (banner.getStatus() == BaseBanner.BANNER_STATUS_DISABLE)
        {
            banner.setStatus(BaseBanner.BANNER_STATUS_ENABLE);
        } else if (banner.getStatus() == BaseBanner.BANNER_STATUS_ENABLE)
        {
            banner.setStatus(BaseBanner.BANNER_STATUS_DISABLE);
        }

        // 更新数据库
        bannerService.updateBannerStatus(banner);

        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("bannerLists", null);
        return queryAll(request);
    }

    /**
     * 删除轮播图
     * 
     * @param banner
     * @return
     */
    @RequestMapping("/deleteBanner")
    public String deleteBanner(HttpServletRequest request, Banner banner)
    {
        // 匹配数据库
        banner = bannerService.queryALlId(banner.getBanner_id());

        // 图片数据库位置
        String imageurlOld = banner.getImageurl();

        // 删除图片数据库信息
        bannerService.deleteBanner(banner);

        // 删除原位置的图片
        if (!"".equals(imageurlOld))
        {
            /*
             * // 获取原图片位置的物理位置 // 上传图片在服务器存放路径+文件名称 // 文件名称是,数据库数据去掉数据库开头位置
             * imageurlOld = UPLOAD_IMG_PATH +
             * imageurlOld.substring(UPLOAD_IMG_PATH_DB.length());
             */
            // 用物理存储文件夹替换数据库存储文件夹 ==> 最终的物理存储文件
            imageurlOld = imageurlOld.replace(SYSConstant.UPLOAD_IMG_PATH_DB,
                    UPLOAD_IMG_PATH);
            // 删除文件
            File fileTemp = new File(imageurlOld);
            if (fileTemp.exists())
            {
                fileTemp.delete();
            }
        }

        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("bannerLists", null);
        return queryAll(request);
    }

    /**
     * 进入上传界面
     * 
     * @return
     */
    @RequestMapping("/toBannerUpload")
    public String toBanner()
    {
        return "back/bannerUpload";
    }

    /**
     * 上传轮播图图片
     * 
     * @return
     */
    @RequestMapping("/uploadBanner")
    @ResponseBody
    public String uploadBanner(HttpServletRequest request, String imgText)
    {
        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        MultipartFile imgFile1 = req.getFile("bannerPic");

        // 获取文件后缀
        String imgSuffix = imgFile1.getOriginalFilename()
                .substring(imgFile1.getOriginalFilename().lastIndexOf("."))
                .toLowerCase();

        // imgSuffix是否是图片格式
        boolean isImgSuffix = false;
        for (String suffix : IMG_SUFFIX_ARY)
        {
            if (suffix.equals(imgSuffix))
            {
                isImgSuffix = true;
                break;
            }
        }

        // 不是图片格式,结束上传操作
        if (isImgSuffix == false)
        {
            return "";
        }

        // 获取随机名称,补全图片格式
        String fileName = String.valueOf(System.currentTimeMillis())
                + imgSuffix;

        // 文件路径,数据库路径,前台显示路径
        String rtnPath = SYSConstant.UPLOAD_IMG_PATH_DB + fileName;
        try
        {
            // 上传保存到服务器物理路径去
            FileUtils.copyInputStreamToFile(imgFile1.getInputStream(),
                    new File(getUploadImgPath(UPLOAD_IMG_PATH + fileName)));

            // 如果存在上次上传图片,并且没有保存到数据库中,清除上次上传没有保存数据库的图片文件信息
            if (null != imgText && !"".equals(imgText))
            {
                File fileTemp = new File(getUploadImgPath(imgText.replace(
                        SYSConstant.UPLOAD_IMG_PATH_DB, UPLOAD_IMG_PATH)));
                if (fileTemp.exists())
                {
                    fileTemp.delete();
                }
            }

            // 添加临时图片到数据库中
            Banner tempBanner = new Banner();
            tempBanner.setStatus(BaseBanner.BANNER_STATUS_DELETE);
            tempBanner
                    .setImageurl(getUploadImgPath(SYSConstant.UPLOAD_IMG_PATH_DB
                            + fileName));
            bannerService.tempBannerUploadAdd(tempBanner);

            ServletContext application = request.getSession()
                    .getServletContext();
            application.setAttribute("bannerLists", null);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // 返回到前台页面显示
        return rtnPath;
    }

    /**
     * 上传添加
     * 
     * @param request
     * @param banner
     * @return
     */
    @RequestMapping("/addBanner")
    public String addBanner(HttpServletRequest request, Banner banner)
    {
        // 乱码处理
        String keyword = "";
        String url = "";
        try
        {
            keyword = new String(banner.getKeyword().getBytes("ISO-8859-1"),
                    "UTF-8");
            url = new String(banner.getUrl().getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        banner.setKeyword(keyword);
        banner.setUrl(url);

        // 检查临时表中是否存在本图的上传信息
        Banner tempBanner = bannerService.queryAllTemp(banner.getImageurl());
        if (null != tempBanner)
        {
            // 获取临时信息的id更新数据即可
            banner.setBanner_id(tempBanner.getBanner_id());
            bannerService.tempBannerUpdate(banner);
        } else
        {
            // 全新增加
            bannerService.addBanner(banner);
        }

        // 清空临时存储数据
        List<Banner> bannerLists = bannerService.queryAllTempList();
        for (Banner temp : bannerLists)
        {
            File fileTemp = new File(getUploadImgPath(temp.getImageurl()
                    .replace(SYSConstant.UPLOAD_IMG_PATH_DB, UPLOAD_IMG_PATH)));
            if (fileTemp.exists())
            {
                fileTemp.delete();
            }
        }
        bannerService.tempBannerDelete();

        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("bannerLists", null);
        return queryAll(request);
    }

    private String getUploadImgPath(String uploadImgPath)
    {
        String separator = File.separator;
        return uploadImgPath.replace("\\", separator);
    }
}
