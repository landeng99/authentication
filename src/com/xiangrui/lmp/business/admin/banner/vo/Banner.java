package com.xiangrui.lmp.business.admin.banner.vo;

import com.xiangrui.lmp.business.base.BaseBanner;

/**
 * 轮播图
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-8 上午10:06:08
 *         </p>
 */
public class Banner extends BaseBanner
{

    private static final long serialVersionUID = 1L;

}
