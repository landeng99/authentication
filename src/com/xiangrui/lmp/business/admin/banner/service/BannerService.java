package com.xiangrui.lmp.business.admin.banner.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.banner.vo.Banner;

/**
 * 轮播图
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-8 上午10:07:34
 * </p>
 */
public interface BannerService
{

    /**
     * 查询所有轮播图
     * 
     * @param params
     * @return
     */
    List<Banner> queryAll(Map<String, Object> params);

    /**
     * 查询某个轮播图信息
     * 
     * @param banner_id
     * @return
     */
    Banner queryALlId(int banner_id);

    /**
     * 跟新轮播图状态
     * 
     * @param banner
     */
    void updateBannerStatus(Banner banner);

    /**
     * 删除轮播图
     * 
     * @param banner
     */
    void deleteBanner(Banner banner);

    /**
     * 按照状态查询
     * 
     * @param status
     * @return
     */
    List<Banner> queryALlStatus(int status);

    /**
     * 添加轮播图信息
     * 
     * @param banner
     */
    void addBanner(Banner banner);

    /**
     * 添加轮播图信息
     * 
     * @param banner
     */
    void tempBannerUploadAdd(Banner tempBanner);

    /**
     * 查询临时表中的某一条
     * @param banner
     * @return
     */
    Banner queryAllTemp(String imageurl);

    /**
     * 更新临时的轮播图
     * @param tempBanner
     */
    void tempBannerUpdate(Banner tempBanner);

    /**
     * 查询临时表中的所有
     * @return
     */
    List<Banner> queryAllTempList();

    /**
     * 删除临时
     */
    void tempBannerDelete();

}
