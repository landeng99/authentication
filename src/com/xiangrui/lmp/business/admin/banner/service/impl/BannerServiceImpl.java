package com.xiangrui.lmp.business.admin.banner.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.banner.mapper.BannerMapper;
import com.xiangrui.lmp.business.admin.banner.service.BannerService;
import com.xiangrui.lmp.business.admin.banner.vo.Banner;

/**
 * 轮播图操作实现
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-8 上午10:13:27
 * </p>
 */
@Service("bannerService")
public class BannerServiceImpl implements BannerService
{

    /**
     * 轮播图数据对象
     */
    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public List<Banner> queryAll(Map<String, Object> params)
    {
        return bannerMapper.queryAll(params);
    }

    @Override
    public Banner queryALlId(int banner_id)
    {
        return bannerMapper.queryALlId(banner_id);
    }

    @SystemServiceLog(description = "更新轮播图状态")
    @Override
    public void updateBannerStatus(Banner banner)
    {
        bannerMapper.updateBannerStatus(banner);
    }

    @SystemServiceLog(description = "删除轮播图")
    @Override
    public void deleteBanner(Banner banner)
    {
        bannerMapper.deleteBanner(banner);
    }

    @Override
    public List<Banner> queryALlStatus(int status)
    {
        return bannerMapper.queryALlStatus(status);
    }

    @SystemServiceLog(description = "添加轮播图")
    @Override
    public void addBanner(Banner banner)
    {
        bannerMapper.addBanner(banner);
    }

    @Override
    public void tempBannerUploadAdd(Banner tempBanner)
    {
        bannerMapper.addBanner(tempBanner);
    }

    @Override
    public Banner queryAllTemp(String imageurl)
    {
        return bannerMapper.queryAllTemp(imageurl);
    }

    @SystemServiceLog(description = "更新轮播图的临时数据")
    @Override
    public void tempBannerUpdate(Banner tempBanner)
    {
        bannerMapper.updateBanner(tempBanner);
    }

    @Override
    public List<Banner> queryAllTempList()
    {
        return bannerMapper.queryAllTempList();
    }

    @Override
    public void tempBannerDelete()
    {
        bannerMapper.deleteBannerTemp();
    }
}
