package com.xiangrui.lmp.business.admin.pkg.controller;

import java.util.List;

import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.kuaidi100.service.ExpressInfoService;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.init.SpringContextHolder;
import com.xiangrui.lmp.util.kuaidiUtil.PostToKuaidi100;
import com.xiangrui.lmp.util.kuaidiUtil.pojo.TaskResponse;

/**
 * 快递信息订阅线程
 * 
 * @author hyh
 * 
 */
public class ExpressOrderThread extends Thread
{
    private static Logger logger = Logger.getLogger(PostToKuaidi100.class);

    /**
     * 快递公司
     */
    // TODO 当前只有EMS吗
//    private static final String COMPANY = "ems";

    /**
     * 重复订阅代号
     */
    private static final String REPEAT_SUBSCRIPTION = "501";

    private List<ExpressInfo> expressInfoList;

    public ExpressOrderThread(List<ExpressInfo> expressInfoList)
    {
        this.expressInfoList = expressInfoList;
    }

    @Override
    public void run()
    {
        int index = 0;
        for (ExpressInfo expressInfo : this.expressInfoList)
        {
            // 向快递100发送请求
            TaskResponse resp = PostToKuaidi100.sendJson(expressInfo.getCom(),
                    expressInfo.getNu(), "", expressInfo.getRegion());

            // 订阅成功 重复订阅
            if (resp.getResult()
                    || REPEAT_SUBSCRIPTION.equals(resp.getReturnCode()))
            {
                expressInfo
                        .setSubscribe_status(ExpressInfo.SUBSCRIBE_STATUS_SUCCESS);

            } else

            {
                expressInfo
                        .setSubscribe_status(ExpressInfo.SUBSCRIBE_STATUS_FAIL);
            }
            index++;

            if (index % 50 == 0)
            {
                try
                {
                    logger.info("睡觉 10秒");
                    Thread.sleep(10000);

                } catch (InterruptedException e)
                {

                    e.printStackTrace();
                }
            }
        }

        if (this.expressInfoList != null && !this.expressInfoList.isEmpty())
        {
            // 获取数据处理service
            ExpressInfoService expressInfoService = SpringContextHolder
                    .getBean("expressInfoService");

            expressInfoService.batchUpdatesubscribeStatus(this.expressInfoList);
        }

    }
}
