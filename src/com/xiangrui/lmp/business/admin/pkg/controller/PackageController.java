package com.xiangrui.lmp.business.admin.pkg.controller;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.accountlog.service.AccountLogService;
import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;
import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.businessModel.service.RoleBusinessNameService;
import com.xiangrui.lmp.business.admin.businessModel.vo.RoleBusinessName;
import com.xiangrui.lmp.business.admin.cost.service.MemberBillService;
import com.xiangrui.lmp.business.admin.cost.vo.MemberBillModel;
import com.xiangrui.lmp.business.admin.message.service.MessageSendService;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pallet.service.PalletService;
import com.xiangrui.lmp.business.admin.pkg.service.PackageImgService;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgAttachServiceService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgGoodsService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgReturnService;
import com.xiangrui.lmp.business.admin.pkg.service.UnusualPkgService;
import com.xiangrui.lmp.business.admin.pkg.service.UserAddressService;
import com.xiangrui.lmp.business.admin.pkg.vo.PackageGoods;
import com.xiangrui.lmp.business.admin.pkg.vo.PackageImg;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgReturn;
import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;
import com.xiangrui.lmp.business.admin.pkglog.service.PkgLogService;
import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;
import com.xiangrui.lmp.business.admin.quotationManage.service.QuotationManageService;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.service.StoreService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.user.service.UserService;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.api.vo.Status;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.constant.PkgConstant;
import com.xiangrui.lmp.init.AppServerUtil;
import com.xiangrui.lmp.util.BreadcrumbUtil;
import com.xiangrui.lmp.util.IdentifierCreator;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.newExcel.ExcelReadUtil;

import net.sf.json.JSONArray;

@Controller
@RequestMapping("/admin/pkg")
public class PackageController {

	private static final String RELATIVELY_PATH = "resource" + File.separator + "upload" + File.separator;
	/**
	 * 退运包裹默认图片（背景）；
	 */
	private static final String INIT_RETURN_IMG = "/img/img03.jpg";
	/**
	 * 上传图片在服务器存放路径
	 */
	private static final String UPLOAD_IMG_PATH = AppServerUtil.getWebRoot() + RELATIVELY_PATH;
	/**
	 * 上传图片DB存放路径信息
	 */
	private static final String UPLOAD_IMG_PATH_DB = "/upload/";

	/**
	 * Excel导入国内运单信息
	 */
	private static final String SHEET_NAME = "清单";

	/**
	 * Excel导入国内运单信息 翔锐物流单号 列名
	 */
	private static final String LOGISTICSCODE_COLNAME = "公司运单号";

	/**
	 * Excel导入国内运单信息 EMS物流单号 列名
	 */
	private static final String EMSCODE_COLNAME = "EMS派送单号";
	
	/**
	 * Excel导入国内运单信息 快递公司
	 */
	private static final String COM_COLNAME = "快递公司";

	/**
	 * Excel列番号初始化 ：一个不存在的列番号
	 */
	private static final int COL_NUMBER_INIT = 9999999;

	/**
	 * Excel读取内容格式错误
	 */
	private static final String EXCEL_CONTENT_ERROR = "1";

	/**
	 * 不是Excel文件
	 */
	private static final String IS_NOT_EXCEL = "2";

	/**
	 * 订单号不存在
	 */
	private static final String LOGISTICSCODE_NOT_EXIST = "3";

	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

	/**
	 * 图片类型 2：退货图片
	 */
	private static final int IMG_TYPE_RETURN = 2;

	/**
	 * 时间格式化
	 */
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	@Autowired
	private StoreService storeService;
	@Autowired
	private PackageService packageService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserAddressService userAddressService;

	@Autowired
	private PkgGoodsService pkgGoodsService;

	@Autowired
	private OverseasAddressService overseasAddressService;

	@Autowired
	private PkgReturnService pkgReturnService;

	@Autowired
	private PackageImgService packageImgService;

	@Autowired
	private FrontUserService frontUserService;

	@Autowired
	private PkgAttachServiceService pkgAttachServiceService;

	@Autowired
	private QuotationManageService quotationManageService;

	@Autowired
	private PalletService palletService;

	@Autowired
	private AttachServiceService attachServiceService;

	/**
	 * 同行帐单详情管理
	 */
	@Autowired
	private MemberBillService memberBillService;

	/**
	 * 消费记录
	 */
	@Autowired
	private AccountLogService accountLogService;

	@Autowired
	private PkgLogService pkgLogService;

	// 发送Email｜SMS服务
	@Autowired
	private MessageSendService messageSendService;

	// 异常包裹服务
	@Autowired
	private UnusualPkgService unusualPkgService;
	// 前台包裹服务
	@Autowired
	private com.xiangrui.lmp.business.homepage.service.PkgGoodsService frontPkgGoodsService;
	// 业务员授权相关
	@Autowired
	private RoleBusinessNameService roleBusinessNameService;
	@Autowired
	private FrontUserMapper frontUserMapper;
	
	//口岸
	@Autowired
	private SeaportService seaportService;

	/**
	 * 包裹列表 初始化
	 * 
	 * @request request
	 * @return
	 */
	@RequestMapping("/query")
	public String getPackageinit(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<String, Object>();
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);

		}
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			params.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
			List<RoleBusinessName> businessNameList = roleBusinessNameService
					.queryRoleBusinessNameByBackUserId(user.getUser_id());
			request.setAttribute("businessNameList", businessNameList);
		}
		// 快件包裹
		params.put("express_package", "-1");
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		// 包裹列表
		List<Pkg> pkgList = packageService.queryAll(params);
		request.setAttribute("pkgList", pkgList);

		return "back/packageList";
	}

	/**
	 * 包裹列表 包裹查询
	 * 
	 * @param request
	 * @param status
	 * @param pay_status
	 * @param user_name
	 * @param logistics_code
	 * @param original_num
	 * @param ems_code
	 * @param pick_code
	 * @param fromDate
	 * @param toDate
	 * @param receiver
	 * @param flight_num
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/search")
	public String getPackageList(HttpServletRequest request, String status, String pay_status, String user_name,
			String logistics_code, String original_num, String ems_code, String pick_code, String fromDate,
			String toDate, String receiver, String flight_num, String arrive_status, String express_num,
			String overseasAddress_id, String business_name, String express_package, String need_check_out_flag) {
		// 分页查询
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			params.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
			List<RoleBusinessName> businessNameList = roleBusinessNameService
					.queryRoleBusinessNameByBackUserId(user.getUser_id());
			request.setAttribute("businessNameList", businessNameList);
		}

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);

		} else {
			pageView = new PageView(1);

			// 包裹状态
			params.put("status", status);
			// 支付状态
			params.put("pay_status", pay_status);
			// 用户名称
			params.put("user_name", user_name);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// 派送单号
			params.put("ems_code", ems_code);
			// 提单号
			params.put("pick_code", pick_code);
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 收件人
			params.put("receiver", receiver);
			// 空运单号
			params.put("flight_num", flight_num);

			params.put("arrive_status", arrive_status);

			// 物流单号
			params.put("express_num", express_num);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);

			// 业务员
			params.put("business_name", business_name);
			// 快件包裹
			params.put("express_package", express_package);
			// 需要取出包裹
			params.put("need_check_out_flag", need_check_out_flag);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);

		}

		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<Pkg> pkgList = packageService.queryAll(params);
		request.setAttribute("pkgList", pkgList);

		request.setAttribute("params", params);

		return "back/packageList";
	}

	/**
	 * 包裹列表 包裹详情
	 * 
	 * @param request
	 * @param package_id
	 * @return String
	 */
	@RequestMapping("/detail")
	public String getDetail(HttpServletRequest request, String package_id) {
		int id = Integer.parseInt(package_id);
		// 包裹
		Pkg pkgDetail = packageService.queryPackageById(id);

		// 客户信息
		FrontUser frontUser = frontUserService.queryFrontUserById(pkgDetail.getUser_id());

		String address = "";
		// 包裹地址
		UserAddress userAddress = userAddressService.queryAddressById(pkgDetail.getAddress_id());
		if (userAddress != null) {
			// 地址拼接
			address = addressInfo(userAddress.getRegion()) + userAddress.getStreet();
		}

		String osaddr = "";

		// 海外仓库地址
		OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
		if (overseasAddress != null) {
			// 海外地址拼接
			osaddr = joinOverseasAddress(overseasAddress);
		}
		// 商品
		List<PkgGoods> pkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(id);

		// 计算总价值
		BigDecimal total_worth = new BigDecimal("0");
		for (PkgGoods pkgGoods : pkgGoodsList) {
			total_worth = total_worth
					.add(new BigDecimal(pkgGoods.getPrice()).multiply(new BigDecimal(pkgGoods.getQuantity())));
		}

		// 页面显示信息
		request.setAttribute("pkgDetail", pkgDetail);
		request.setAttribute("frontUser", frontUser);
		request.setAttribute("userAddress", userAddress);
		request.setAttribute("address", address);
		request.setAttribute("osaddr", osaddr);
		request.setAttribute("pkgGoodsList", pkgGoodsList);
		request.setAttribute("total_worth", total_worth);

		return "back/packageDetail";
	}
	
	@RequestMapping("/updateDetail")
	@ResponseBody
	public Map<String, Object> updateDetail(HttpServletRequest request, String address_id, String jsonGoods, String address) {
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		
		// 包裹地址
		UserAddress userAddress = userAddressService.queryAddressById(Integer.parseInt(address_id));
		if (userAddress != null) {
			userAddress.setStreet(address);
			userAddressService.updateStreet(userAddress);
		}
		
		PkgGoods[] gArray;
		try {
			gArray = JSONUtil.jsonToBean(jsonGoods, PkgGoods[].class);
		} catch (Exception e) {
			e.printStackTrace();
			rtnMap.put("result", false);
			return rtnMap;
		}
		List<PkgGoods> gList = Arrays.asList(gArray);
		if( 0>=pkgGoodsService.batchUpdatePkgGoods(gList) ){
			rtnMap.put("result", false);
			return rtnMap;
		}
		
		
		
		rtnMap.put("result", true);
		return rtnMap;
	}

	/**
	 * 包裹列表 包裹修改
	 * 
	 * @param request
	 * @param package_id
	 * @return String
	 */
	@RequestMapping("/update")
	public String update(HttpServletRequest request, String package_id) {
		int id = Integer.parseInt(package_id);
		// 包裹
		Pkg pkgDetail = packageService.queryPackageById(id);

		// 客户信息
		FrontUser frontUser = frontUserService.queryFrontUserById(pkgDetail.getUser_id());

		String address = "";
		// 包裹地址
		UserAddress userAddress = userAddressService.queryAddressById(pkgDetail.getAddress_id());
		if (userAddress != null) {
			// 地址拼接
			address = addressInfo(userAddress.getRegion()) + userAddress.getStreet();
		}
		
		String osaddr = "";
		
		//海外地址列表
		User user = (User) request.getSession().getAttribute("back_user");
		List<OverseasAddress> seasAddressList = overseasAddressService.queryOverseasAddressByUserId(user.getUser_id());
		
		Integer sid = pkgDetail.getSeaport_id();
		//查询包裹所属口岸
		Seaport seaport = this.seaportService.querySeaportBySid(sid);
		
		// 海外仓库地址
		OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
		if (overseasAddress != null) {
			// 海外地址拼接
			osaddr = joinOverseasAddress(overseasAddress);
		}
		// 商品
		List<PkgGoods> pkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(id);

		// 计算总价值
		BigDecimal total_worth = new BigDecimal("0");
		for (PkgGoods pkgGoods : pkgGoodsList) {
			total_worth = total_worth
					.add(new BigDecimal(pkgGoods.getPrice()).multiply(new BigDecimal(pkgGoods.getQuantity())));
		}

		// 页面显示信息
		request.setAttribute("pkgDetail", pkgDetail);
		request.setAttribute("frontUser", frontUser);
		request.setAttribute("userAddress", userAddress);
		request.setAttribute("overseasAddress", overseasAddress);
		request.setAttribute("address", address);
		request.setAttribute("osaddr", osaddr);
		request.setAttribute("pkgGoodsList", pkgGoodsList);
		request.setAttribute("total_worth", total_worth);
		request.setAttribute("seasAddressList", seasAddressList);
		request.setAttribute("seaport", seaport);

		return "back/updatePackage";
	}

	/**
	 * 包裹列表 打印面单
	 * 
	 * @param request
	 * @param package_id
	 * @return String
	 */
	@RequestMapping("/print")
	public String print(HttpServletRequest request, String package_id) {

		// 包裹信息
		Pkg pkgDetail = packageService.queryPackageById(Integer.parseInt(package_id));

		FrontUser frontUser = frontUserService.queryFrontUserById(pkgDetail.getUser_id());

		String address = "";
		// 包裹地址
		UserAddress userAddress = userAddressService.queryAddressById(pkgDetail.getAddress_id());
		if (userAddress != null) {
			// 地址拼接
			address = addressInfo(userAddress.getRegion()) + userAddress.getStreet();
		}

		String osaddr = "";

		// 海外仓库地址
		OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
		if (overseasAddress != null) {
			// 海外地址拼接
			osaddr = joinOverseasAddress(overseasAddress);
			request.setAttribute("country", overseasAddress.getCountry());
		}

		// 面单信息
		request.setAttribute("frontUser", frontUser);
		request.setAttribute("osaddr", osaddr);
		request.setAttribute("pkgDetail", pkgDetail);
		request.setAttribute("userAddress", userAddress);
		request.setAttribute("address", address);

		return "back/printExpress";
	}

	/**
	 * 包裹详情 包裹在提单中时 包裹状态不能变更
	 * 
	 * @param request
	 * @param package_id
	 * @return
	 */
	@RequestMapping("/checkPackageInPallet")
	@ResponseBody
	public Map<String, Object> checkPackageInPallet(HttpServletRequest request, Integer package_id, Integer status) {

		Map<String, Object> rtnMap = new HashMap<String, Object>();

		Map<String, Object> parmas = new HashMap<String, Object>();
		parmas.put("package_id", package_id);
		parmas.put("status", status);
		User user = (User) request.getSession().getAttribute("back_user");
		// 修改前包裹信息
		Pkg pkgDetail = packageService.queryPackageById(package_id);

		if (status.compareTo(pkgDetail.getStatus()) < 0) {
			if (palletService.isPkgInfoExist(parmas)) {
				rtnMap.put("result", false);
				rtnMap.put("message", "包裹已被放入托盘，请先从托盘中删除再修改。");
				return rtnMap;
			}
			else{
				pkgDetail.setStatus(status);
				this.packageService.updateStatus(pkgDetail,user);
				rtnMap.put("result", true);
				rtnMap.put("message", "提示！保存成功!");
				return rtnMap;
			}
		}
		else{
			pkgDetail.setStatus(status);
			this.packageService.updateStatus(pkgDetail,user);
			rtnMap.put("result", true);
			rtnMap.put("message", "提示！保存成功!");
			return rtnMap;
		}
	}

	/**
	 * 点击完成触发的接口
	 * 
	 * @param request
	 * @param freight
	 *            运费
	 * @param length
	 *            长度
	 * @param width
	 *            宽度
	 * @param height
	 *            高度
	 * @param package_id
	 *            包裹id
	 * @param weight
	 *            收费重量
	 * @param actual_weight
	 *            实际重量
	 * @return
	 */
	@RequestMapping("/updateWeight")
	@ResponseBody
	public String updateWeight(HttpServletRequest request, String freight, String length, String width, String height,
			int package_id, String weight, String actual_weight, String editWeightFlag) {
		String returnStr = "0";
		Pkg pkg = new Pkg();

		// 运费
		pkg.setFreight(Float.parseFloat(freight));
		// 长度
		pkg.setLength(Float.parseFloat(length));
		// 宽度
		pkg.setWidth(Float.parseFloat(width));
		// 高度
		pkg.setHeight(Float.parseFloat(height));
		// 包裹id
		pkg.setPackage_id(package_id);
		// 收费重量
		pkg.setWeight(Float.parseFloat(weight));
		// 实际重量
		pkg.setActual_weight(Float.parseFloat(actual_weight));

		// 当前数据库存储的包裹
		Pkg pkgTemp = packageService.queryPackageById(package_id);
		// 前端客户
		FrontUser frontUser = frontUserService.queryFrontUserById(pkgTemp.getUser_id());
		// 用户类型
		int userType = frontUser.getUser_type();

		pkg.setStatus(pkgTemp.getStatus());

		boolean canPay = true;
		// 前台已经过滤了导入帐单的包裹是不可以编辑重量的
		if (StringUtils.trimToNull(editWeightFlag) == null) {
			float service_price = 0;
			// 算出增值服务费
			// 算出增值服务费
			List<AttachService> attachServices = attachServiceService.selectAttachServiceByOnlyPackageId(package_id);
			if (attachServices != null && attachServices.size() > 0) {
				for (AttachService attachService : attachServices) {
					if (attachService != null) {
						service_price += attachService.getService_price();
					}
				}
			}

			float totalTransportCost = Float.parseFloat(freight) + service_price;
			// 转运总费用=实际运费+增值服务费
			pkg.setTransport_cost(totalTransportCost);

			boolean needToSendEmail = false;
			// 不需要等待合箱服务时直接入库，或者不是后台创建了已认领的异常包裹后，若前台用户未点击编辑包裹，未补充完整包裹内件和收货地址
			// 否则，还需要保持原来待入库状态
			if (("Y".equals(pkgTemp.getWaitting_merge_flag())) || ("Y".equals(pkgTemp.getException_package_flag()))) {
				// 保持原来待入库状态
				canPay = false;
			} else {
				// 包裹状态为 已入库
				pkg.setStatus(BasePackage.LOGISTICS_STORAGED);
				// 支付状态为 待支付
				pkg.setPay_status(BasePackage.PAYMENT_UNPAID);
				pkg.setPay_status_freight(BasePackage.PAYMENT_FREIGHT_UNPAID);
				needToSendEmail = true;
			}
			pkg.setAccounting_status(Pkg.ACCOUNTING_UNDO);

			// 提醒客户进行付款--Email发送
			if (needToSendEmail) {
				if (FrontUser.USER_TYPE_OWN == userType) {
					boolean wantSendEmail = true;
					if (wantSendEmail) {
						String needPayMenoy = NumberUtils.scaleMoneyData(totalTransportCost);
						String emailAddress = frontUser.getAccount();
						messageSendService.sendPayWarnEmail(frontUser.getUser_name(), emailAddress,
								frontUser.getAccount(), 1, needPayMenoy);
					}
				}
			}
		} else {
			// 支付状态为 待支付
			pkg.setPay_status(BasePackage.PAYMENT_UNPAID);
			pkg.setPay_status_freight(BasePackage.PAYMENT_FREIGHT_UNPAID);
			float service_price = 0;
			// 算出增值服务费
			List<AttachService> attachServices = attachServiceService.selectAttachServiceByOnlyPackageId(package_id);
			if (attachServices != null && attachServices.size() > 0) {
				for (AttachService attachService : attachServices) {
					if (attachService != null) {
						service_price += attachService.getService_price();
					}
				}
			}

			// 转运总费用=实际运费+增值服务费
			pkg.setTransport_cost(Float.parseFloat(freight) + service_price);
		}

		// 入库修改包裹的操作人,即后台登陆人员
		User user = (User) request.getSession().getAttribute("back_user");
		this.packageService.update(pkg, user);
		this.storeService.dealToNeedHandleFlag(package_id);

		// 同行用户，判断是否需要做虚拟预付款处理
		if (StringUtils.trimToNull(editWeightFlag) == null && FrontUser.USER_TYPE_PEER == userType && canPay) {
			String virtualPayFlag = StringUtils.trimToNull(frontUser.getVirtual_pay_flag());
			if ("Y".equals(virtualPayFlag)) {
				float currTransportCost = pkgTemp.getTransport_cost();// 增值服务费用
				// 直接设定虚拟扣款的固定单价是39.6元/磅
				BigDecimal virtualPayGood = new BigDecimal(39.6)
						.multiply(new BigDecimal(calculateFeeWeith(userType, Float.parseFloat(actual_weight))));
				float virtualPay = (float) NumberUtils.add(currTransportCost, virtualPayGood.floatValue());
				frontUser = frontUserService.queryFrontUserById(pkgTemp.getUser_id());
				if (virtualPay > 0.00001f && frontUser.getAble_balance() > virtualPay) {
					// 冻结扣款金额
					frontUser.setAble_balance(frontUser.getAble_balance() - virtualPay);
					frontUser.setFrozen_balance(frontUser.getFrozen_balance() + virtualPay);
					frontUserMapper.updateBalance(frontUser);
					// 包裹支付状态已支付（预扣）
					pkg.setPay_status(Pkg.PAYMENT_VIRTUAL_PAID);
					// 物流状态:待发货
					pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING);
					pkg.setVirtual_pay(virtualPay);
					this.packageService.update(pkg, user);
					returnStr = "show";
				}
			}
		}
		return returnStr;
	}

	/**
	 * 包裹详情 计算运费
	 * 
	 * @param request
	 * @param weight
	 *            收费重量
	 * @param FirstWeight
	 *            首重
	 * @param user_id
	 * @param quota_id
	 * @return
	 */
	@RequestMapping("/calculation")
	@ResponseBody
	public Map<String, Object> calculation(HttpServletRequest request, String weight, String FirstWeight,
			String quota_id, String user_id) {

		Map<String, Object> rtnMap = new HashMap<String, Object>();
		// FrontUser frontUser =
		// frontUserService.queryFrontUserById(Integer.parseInt(user_id));
		// 资费费率
		// Map<String, Object> parmas = new HashMap<String, Object>();
		// parmas.put("integral_min", frontUser.getIntegral());
		// parmas.put("rate_type", frontUser.getUser_type());
		// FreightCost freightCost =
		// freightCostService.queryFreightCostByIntegralMin(parmas);
		QuotationManage qm = quotationManageService.selectQuotationManageByLogId(Integer.valueOf(quota_id));

		if (qm == null) {
			rtnMap.put("result", false);
			rtnMap.put("message", "报价单信息不存在，请与管理员联系!");
			return rtnMap;
		}
		if (weight != null && !"".equals(weight.trim()) && !"0".equals(weight.trim())) {
			weight = weight.trim();
			// 首重
			BigDecimal first_weight = new BigDecimal(FirstWeight);
			// 首重单价
			BigDecimal first_weight_price = new BigDecimal(qm.getFirst_weight_price()).setScale(2,
					BigDecimal.ROUND_HALF_UP);
			// 续重单价
			BigDecimal go_weight_price = new BigDecimal(qm.getGo_weight_price());
			// 计算包裹的运费
			BigDecimal freight = first_weight.multiply(first_weight_price)
					.add((new BigDecimal(weight).subtract(first_weight)).multiply(go_weight_price));

			rtnMap.put("freight", freight.setScale(2, BigDecimal.ROUND_HALF_UP));

		} else {
			weight = "0";
			rtnMap.put("freight", 0);
		}

		rtnMap.put("result", true);
		return rtnMap;
	}
	/**
	 * 最初的方法
	 * 
	 * @param request
	 * @param weight
	 * @param user_id
	 * @return
	 */
	// @RequestMapping("/calculation")
	// @ResponseBody
	// public Map<String, Object> calculation(HttpServletRequest request, String
	// weight, String user_id) {
	//
	// Map<String, Object> rtnMap = new HashMap<String, Object>();
	// FrontUser frontUser =
	// frontUserService.queryFrontUserById(Integer.parseInt(user_id));
	// // 资费费率
	// Map<String, Object> parmas = new HashMap<String, Object>();
	// parmas.put("integral_min", frontUser.getIntegral());
	// parmas.put("rate_type", frontUser.getUser_type());
	//
	// FreightCost freightCost =
	// freightCostService.queryFreightCostByIntegralMin(parmas);
	//
	// if (freightCost == null) {
	// rtnMap.put("result", false);
	// rtnMap.put("message", "用户费用信息不存在，请与管理员联系!");
	// return rtnMap;
	// }
	// if (weight != null && !"".equals(weight.trim())) {
	// weight = weight.trim();
	// } else {
	// weight = "0";
	// }
	// BigDecimal freight = new
	// BigDecimal(freightCost.getUnit_price()).multiply(new BigDecimal(weight));
	//
	// rtnMap.put("freight", freight.setScale(2, BigDecimal.ROUND_HALF_UP));
	//
	// rtnMap.put("result", true);
	// return rtnMap;
	// }
	
	private Logger logger =Logger.getLogger(PackageController.class);

	/**
	 * 包裹列表 更新包裹
	 * 
	 * @param request
	 * @param package_id
	 * @param status
	 * @param weight
	 * @param freight
	 * @param width
	 * @param width
	 * @param height
	 * @return
	 * 
	 */
	@RequestMapping("/editPackage")
	@ResponseBody
	public Map<String, Object> save(HttpServletRequest request, String package_id, String freight,
			String status, String length, String width, String original_num, String height,
			int pay_type, int express_package, int osaddr_id, String receiver, String idcard, 
			String mobile, int address_id, String address, String jsonGoods) {
		Map<String, Object> rtnMap = new HashMap<String, Object>();

		Pkg updatePkg = packageService.queryPackageById(Integer.parseInt(package_id));
		// 包裹由非待入库状态改成待入库状态时需要重置原有的运费和总运费
		if (updatePkg.getStatus() > 0) {
			if (Integer.parseInt(status) == 0) {
				float totalTransportCost = (float) NumberUtils.subtract(updatePkg.getTransport_cost(),
						updatePkg.getFreight());
				if (totalTransportCost < 0) {
					totalTransportCost = 0;
				}
				updatePkg.setTransport_cost(totalTransportCost);
				// updatePkg.setPay_status(1);
				updatePkg.setPay_status_freight(Pkg.PAYMENT_FREIGHT_UNPAID);
			}
		}
		if (updatePkg.getStatus() >= 9) {
			// 返还冻结扣款金额
			putBackVirtualPay(updatePkg.getUser_id(), updatePkg.getPackage_id());
		}
		// 更新条件：包裹id
		updatePkg.setPackage_id(Integer.parseInt(package_id));
		// 更新包裹状态
		//updatePkg.setStatus(Integer.parseInt(status));

		// 包裹运费
		updatePkg.setFreight(Float.parseFloat(freight));
		// 长度
		updatePkg.setLength(Float.parseFloat(length));
		// 宽度
		updatePkg.setWidth(Float.parseFloat(width));
		// 高度
		updatePkg.setHeight(Float.parseFloat(height));
		// 关联单号
		updatePkg.setOriginal_num(original_num);
		//支付方式
		updatePkg.setPay_type(pay_type);
		//渠道
		updatePkg.setExpress_package(express_package);
		//海外仓库
		updatePkg.setOsaddr_id(osaddr_id);
		//收件人
		updatePkg.setReceiver(receiver);
		//收件人手机
		updatePkg.setMobile(mobile);
		//收件人身份证
		updatePkg.setIdcard(idcard);
		
		// 更新包裹
		User user = (User) request.getSession().getAttribute("back_user");
		packageService.updatePackage(updatePkg, user);
		
		// 包裹地址
		UserAddress userAddress = userAddressService.queryAddressById(address_id);
		if (userAddress != null) {
			// 修改全都加到 street 中。
			userAddress.setRegion("");
			userAddress.setStreet(address);
		} else {
			logger.error("address_id no exist: " + address_id);
		}
		
		PkgGoods[] gArray;
		try {
			gArray = JSONUtil.jsonToBean(jsonGoods, PkgGoods[].class);
		} catch (Exception e) {
			e.printStackTrace();
			rtnMap.put("result", false);
			logger.error("jsonToBean err: " + jsonGoods);
			return rtnMap;
		}
		List<PkgGoods> gList = Arrays.asList(gArray);
		if( 0>=pkgGoodsService.batchUpdatePkgGoods(gList) ){
			rtnMap.put("result", false);
			logger.error("batchUpdatePkgGoods err: " + jsonGoods);
			return rtnMap;
		}
		
		rtnMap.put("result", true);
		return rtnMap;
	}

	/**
	 * 增值服务初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/attachService")
	public String pkgAttachServiceInit(HttpServletRequest request) {

		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		// 分页查询参数
		params.put("pageView", pageView);

		try {
			Map<String, Object> attachServiceParams = new HashMap<String, Object>();
			attachServiceParams.put("isdeleted", AttachService.ISDELETED_NO);
			List<AttachService> attachServiceList = attachServiceService.queryAllAttachService(attachServiceParams);

			// 增值服务查询
			List<PkgAttachServiceGroup> pkgAttachServiceGroupList = pkgAttachServiceService
					.queryPkgAttachServiceGroup(params);

			request.setAttribute("pageView", pageView);
			request.setAttribute("attachServiceList", attachServiceList);
			request.setAttribute("pkgAttachServiceGroupList", pkgAttachServiceGroupList);

		} catch (Exception e) {
			e.printStackTrace();
		}

		// 当前请求放入面包切中
		BreadcrumbUtil.push(request);
		return "back/pkgAttachServiceList";
	}

	/**
	 * 增值服务查询
	 * 
	 * @param request
	 * @param logistics_code
	 * @param status
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/attachServiceSearch")
	public String pkgAttachServiceSearch(HttpServletRequest request, String logistics_code, String status,
			String attach_id, String auto_split) {
		// 分页查询
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			pageView = new PageView(1);

			// 物流运单号或关联单号
			params.put("logistics_code", logistics_code);

			// 增值服务状态
			params.put("status", status);

			params.put("attach_id", attach_id);
			// 自动分箱查询
			params.put("auto_split", auto_split);

			String fromDate = request.getParameter("fromDate");

			String toDate = request.getParameter("toDate");

			params.put("fromDate", fromDate);

			params.put("toDate", toDate);

			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}

		// 分页查询参数
		params.put("pageView", pageView);

		List<PkgAttachServiceGroup> pkgAttachServiceGroupList = new ArrayList<PkgAttachServiceGroup>();

		List<Pkg> pkgs = null;
		if (logistics_code != null && !"".equals(logistics_code.trim())) {
			Map<String, Object> codeParams = new HashMap<String, Object>();
			codeParams.put("code", logistics_code);

			pkgs = packageService.queryPackageByCode(codeParams);
		}
		if (pkgs == null || pkgs.isEmpty()) {
			// 如果查询单号不为空，且没有包裹数据，则以设置包裹id的值查询条件为不符合包裹id的字符串
			if (logistics_code != null && !"".equals(logistics_code.trim())) {
				params.put("package_id", "not pkg data");
			}

			pkgAttachServiceGroupList.addAll(pkgAttachServiceService.queryPkgAttachServiceGroup(params));
		} else {
			// 增值服务查询
			for (Pkg pkg : pkgs) {
				params.put("package_id", pkg.getPackage_id());
				pkgAttachServiceGroupList.addAll(pkgAttachServiceService.queryPkgAttachServiceGroup(params));
			}
		}

		request.setAttribute("pageView", pageView);

		Map<String, Object> attachServiceParams = new HashMap<String, Object>();
		attachServiceParams.put("isdeleted", AttachService.ISDELETED_NO);
		List<AttachService> attachServiceList = attachServiceService.queryAllAttachService(attachServiceParams);

		// 页面值保持
		request.setAttribute("logistics_code", logistics_code);
		request.setAttribute("attachServiceList", attachServiceList);
		request.setAttribute("pkgAttachServiceGroupList", pkgAttachServiceGroupList);

		request.setAttribute("params", params);

		return "back/pkgAttachServiceList";
	}

	/**
	 * 非分箱、合箱的增值服务操作初始化
	 * 
	 * @param request
	 * @param attach_id
	 * @param package_id
	 * @return String
	 */
	@RequestMapping("/operate")
	public String operate(HttpServletRequest request, String package_id, String logistics_code) {
		// 包裹增值服务列表
		List<PkgAttachService> pkgAttachServiceList = pkgAttachServiceService
				.queryPkgAttachServiceBypackageId(Integer.parseInt(package_id));

		// 待服务操作
		List<PkgAttachService> list = new ArrayList<PkgAttachService>();
		for (PkgAttachService pkgAttachService : pkgAttachServiceList) {
			int attach_id = pkgAttachService.getAttach_id();
			if (attach_id == AttachService.MERGE_PKG_ID || attach_id == AttachService.SPLIT_PKG_ID) {
				continue;
			}
			list.add(pkgAttachService);
		}
		Pkg pkg = packageService.queryPackageById(Integer.parseInt(package_id));
		// 包裹明细
		List<PkgGoods> allPkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(Integer.parseInt(package_id));

		pkg.setGoodsList(allPkgGoodsList);

		PackageImg packageImg = new PackageImg();

		packageImg.setImg_type(PackageImg.IMG_PICTRUE_TYPE);
		packageImg.setPackage_id(pkg.getPackage_id());

		List<PackageImg> packageImgList = packageImgService.queryPackageImgList(packageImg);
		pkg.setPackageImgList(packageImgList);
		request.setAttribute("pkg", pkg);
		request.setAttribute("pkgAttachServiceList", list);
		request.setAttribute("package_id", package_id);
		request.setAttribute("logistics_code", logistics_code);
		// 当前请求放入面包切中
		BreadcrumbUtil.push(request);
		return "back/pkgAttachServiceDetail";
	}

	@RequestMapping("/initSplit")
	public String initSplit(HttpServletRequest request, String package_id, String logistics_code) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("package_id", package_id);

		// 待废弃的包裹
		String abandon_packages = package_id;

		// 子包裹包裹增值服务列表
		List<PkgAttachService> pkgAttachServiceList = pkgAttachServiceService.queryForSplit(param);

		Map<Integer, PkgAttachService> pkgAttachServiceMap = new HashMap<Integer, PkgAttachService>();
		List<String> packageIds = new ArrayList<String>();

		for (PkgAttachService pkgAttachService : pkgAttachServiceList) {

			packageIds.add(String.valueOf(pkgAttachService.getPackage_id()));
			pkgAttachServiceMap.put(pkgAttachService.getPackage_id(), pkgAttachService);
		}

		List<Pkg> packageList = packageService.queryPackageByPackageIds(packageIds);

		// 待分箱包裹明细
		List<PkgGoods> allPkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(Integer.parseInt(package_id));

		// 分箱包裹明细
		List<PackageGoods> packageGoodsList = new ArrayList<PackageGoods>();

		for (Pkg pkg : packageList) {

			List<PkgAttachService> pkgAttachServices = new ArrayList<PkgAttachService>();
			pkgAttachServices.add(pkgAttachServiceMap.get(pkg.getPackage_id()));
			pkg.setPkgAttachServiceList(pkgAttachServices);
			PackageGoods packageGoods = new PackageGoods();
			packageGoods.setPkg(pkg);
			List<PkgGoods> pkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(pkg.getPackage_id());
			packageGoods.setGoodList(pkgGoodsList);
			packageGoodsList.add(packageGoods);
		}
		Pkg pkg = packageService.queryPackageById(Integer.parseInt(package_id));
		pkg.setGoodsList(allPkgGoodsList);
		request.setAttribute("pkg", pkg);
		request.setAttribute("pkgAttachServiceList", pkgAttachServiceList);
		request.setAttribute("packageGoodsList", packageGoodsList);
		request.setAttribute("abandon_packages", abandon_packages);
		request.setAttribute("package_id", package_id);
		request.setAttribute("logistics_code", logistics_code);
		// 当前请求放入面包切中
		BreadcrumbUtil.push(request);
		return "back/pkg_split";
	}

	@RequestMapping("/split")
	@ResponseBody
	public Map<String, Object> split(HttpServletRequest request, String package_id, String abandon_packages) {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("package_ids", package_id);

		// 更新包裹的分箱增值服务
		params.put("attach_id", AttachService.SPLIT_PKG_ID);

		// 分箱后把原来的包裹废弃
		params.put("abandon_packages", abandon_packages);

		// 更新增值服务表和包裹表
		User user = (User) request.getSession().getAttribute("back_user");
		int cnt = pkgAttachServiceService.updatePkgAttachServiceAndPackage(params, user);
		Map<String, Object> result = new HashMap<String, Object>();
		if (cnt > 0) {
			result.put("result", true);
		} else {
			result.put("result", false);
			result.put("msg", "保存错误");
		}
		return result;

	}

	@RequestMapping("/initMerge")
	public String initMerge(HttpServletRequest request, String package_id, String logistics_code) {

		// 待合箱包裹明细
		List<PkgGoods> allPkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(Integer.parseInt(package_id));

		PkgAttachService pkgAttachService = pkgAttachServiceService
				.queryAllPkgAttachServiceById(Integer.parseInt(package_id), AttachService.MERGE_PKG_ID);
		String package_idStr = pkgAttachService.getPackage_group();

		// 待废弃包裹
		String abandon_packages = package_idStr;

		List<String> package_ids = new ArrayList<String>();
		if (null != package_idStr) {
			String[] package_idArray = package_idStr.split(",");
			for (String str : package_idArray) {

				if (!"".equals(str.trim())) {
					package_ids.add(str.trim());
				}
			}
		}
		List<Pkg> packageList = null;
		if (!package_ids.isEmpty()) {
			packageList = packageService.queryPackageByPackageIds(package_ids);
		} else {
			packageList = new ArrayList<Pkg>();
		}
		// 合箱包裹明细
		List<PackageGoods> packageGoodsList = new ArrayList<PackageGoods>();

		for (Pkg pkg : packageList) {
			List<PkgAttachService> pkgAttachServices = new ArrayList<PkgAttachService>();
			pkgAttachServices.add(pkgAttachService);
			PackageGoods packageGoods = new PackageGoods();
			packageGoods.setPkg(pkg);
			List<PkgGoods> pkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(pkg.getPackage_id());
			packageGoods.setGoodList(pkgGoodsList);

			packageGoodsList.add(packageGoods);
		}

		Pkg pkg = packageService.queryPackageById(Integer.parseInt(package_id));
		pkg.setGoodsList(allPkgGoodsList);
		request.setAttribute("pkg", pkg);
		request.setAttribute("packageGoodsList", packageGoodsList);
		request.setAttribute("package_id", package_id);
		request.setAttribute("pkgAttachService", pkgAttachService);
		request.setAttribute("abandon_packages", abandon_packages);
		request.setAttribute("logistics_code", logistics_code);
		// 当前请求放入面包切中
		BreadcrumbUtil.push(request);

		return "back/pkg_merge";
	}

	/**
	 * 合箱
	 * 
	 * @param request
	 * @param package_id
	 * @param abandon_packages
	 * @return
	 */
	@RequestMapping("/merge")
	@ResponseBody
	public Map<String, Object> merge(HttpServletRequest request, String package_id, String abandon_packages) {
		Map<String, Object> params = new HashMap<String, Object>();

		params.put("package_ids", package_id);
		params.put("attach_id", AttachService.MERGE_PKG_ID);

		// 分箱后把原来的包裹废弃
		params.put("abandon_packages", abandon_packages);

		// 更新增值服务表和包裹表
		User user = (User) request.getSession().getAttribute("back_user");
		int cnt = pkgAttachServiceService.updatePkgAttachServiceAndPackage(params, user);
		Map<String, Object> result = new HashMap<String, Object>();
		if (cnt > 0) {
			result.put("result", true);
		} else {
			result.put("result", false);
			result.put("msg", "保存错误");
		}
		return result;

	}

	/**
	 * 增值服务完成
	 * 
	 * @param request
	 * 
	 * @param package_id
	 * @return String
	 */
	@RequestMapping("/finish")
	@ResponseBody
	public Map<String, Object> finish(HttpServletRequest request, String package_id, String attach_ids) {
		Map<String, Object> params = new HashMap<String, Object>();
		User user = (User) request.getSession().getAttribute("back_user");
		// 增值服务已经完成的包裹
		params.put("package_id", Integer.parseInt(package_id));
		params.put("attach_ids", attach_ids);

		pkgAttachServiceService.updatePkgAttachService(params, user);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", true);
		return result;
	}

	/**
	 * 海外退运包裹 初始化
	 * 
	 * @param request
	 * @return String
	 */
	@RequestMapping("/return")
	public String returnPkg(HttpServletRequest request) {

		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		// 分页查询参数
		params.put("pageView", pageView);

		params.put("status", Pkg.LOGISTICS_RETURN);
		// 退运包裹查询
		List<Pkg> returnPkgList = packageService.queryAllReturn(params);

		request.setAttribute("pageView", pageView);
		request.setAttribute("returnPkgList", returnPkgList);

		return "back/returnPkgList";
	}

	/**
	 * 海外退运包裹 包裹查询
	 * 
	 * @param request
	 * @param retpay_status
	 * @param userName
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/searchRtnPkg")
	public String getreturnPackage(HttpServletRequest request, String retpay_status, String user_name,
			String logistics_code, String fromDate, String toDate) {
		// 分页查询
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			pageView = new PageView(1);

			// 退运包裹
			params.put("status", PkgConstant.LOGISTICS_RETURN);
			// 退运支付状态
			params.put("retpay_status", retpay_status);
			// 用户名称
			params.put("user_name", user_name);
			// 物流跟踪号
			params.put("logistics_code", logistics_code);
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);

			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}

		// 分页查询参数
		params.put("pageView", pageView);
		// 海外退运包裹
		List<Pkg> returnPkgList = packageService.queryAllReturn(params);

		request.setAttribute("pageView", pageView);
		request.setAttribute("returnPkgList", returnPkgList);
		// 查询条件保持

		request.setAttribute("params", params);

		return "back/returnPkgList";
	}

	/**
	 * 海外退运包裹 包裹详情
	 * 
	 * @param request
	 * @param package_id
	 * @return String
	 */
	@RequestMapping("/returnDetail")
	public String getReturnDetail(HttpServletRequest request, String package_id) {
		int pkg_id = Integer.parseInt(package_id);
		// 包裹
		Pkg pkgDetail = packageService.queryPackageById(pkg_id);

		// 客户信息
		FrontUser frontUser = frontUserService.queryFrontUserById(pkgDetail.getUser_id());

		String address = "";
		// 包裹地址
		UserAddress userAddress = userAddressService.queryAddressById(pkgDetail.getAddress_id());
		if (userAddress != null) {
			// 地址拼接
			address = addressInfo(userAddress.getRegion()) + userAddress.getStreet();
		}

		String osaddr = "";

		// 海外仓库地址
		OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
		if (overseasAddress != null) {
			// 海外地址拼接
			osaddr = joinOverseasAddress(overseasAddress);
		}

		// 商品
		List<PkgGoods> pkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(pkg_id);
		try {
			// 包裹退货申请表
			PkgReturn pkgReturn = pkgReturnService.queryPkgReturnByPkgId(pkg_id);
			// 页面显示信息
			request.setAttribute("pkgDetail", pkgDetail);
			request.setAttribute("frontUser", frontUser);
			request.setAttribute("address", address);
			request.setAttribute("osaddr", osaddr);
			request.setAttribute("pkgGoodsList", pkgGoodsList);
			request.setAttribute("pkgReturn", pkgReturn);
			// 退货支付状态：已支付
			if (PkgConstant.RET_PAYMENT_PAID == pkgDetail.getRetpay_status()) {
				User user = userService.getUserById(pkgReturn.getOperater());
				request.setAttribute("operater", user.getUsername());
				// 默认图片路径
				String path = INIT_RETURN_IMG;

				PackageImg pkgImg = new PackageImg();
				pkgImg.setPackage_id(Integer.parseInt(package_id));
				pkgImg.setImg_type(IMG_TYPE_RETURN);
				pkgImg = packageImgService.queryPackageImg(pkgImg);

				if (pkgImg != null && StringUtil.isNotEmpty(pkgImg.getImg_path())) {
					path = pkgImg.getImg_path();
				}
				request.setAttribute("path", path);
				return "back/returnPkgPay";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "back/returnPkgDetail";
	}

	/**
	 * 海外退运包裹 退运审批
	 * 
	 * @param request
	 * @param package_id
	 * @param return_cost
	 * @param approve_status
	 * @param approve_reason
	 * @return
	 */
	@RequestMapping("/approve")
	@ResponseBody
	public String approve(HttpServletRequest request, String package_id, String return_cost, String approve_status,
			String approve_reason) {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");

		Pkg pkg = new Pkg();
		// 包裹ID
		pkg.setPackage_id(Integer.parseInt(package_id));
		// 退货费用
		pkg.setReturn_cost(Float.parseFloat(return_cost));

		PkgReturn pkgReturn = new PkgReturn();
		// 包裹ID
		pkgReturn.setPkg_id(Integer.parseInt(package_id));
		// 审核状态
		pkgReturn.setApprove_status(Integer.parseInt(approve_status));
		// 拒绝理由
		pkgReturn.setApprove_reason(approve_reason);
		// 审批人员
		pkgReturn.setOperater(user.getUser_id());
		// 更新时间
		pkgReturn.setApprove_time(new Timestamp(System.currentTimeMillis()));

		// 更新
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("pkg", pkg);
		paramMap.put("pkgReturn", pkgReturn);

		// 同时更新t_package和t_pkg_return
		packageService.updateReturnCost(paramMap);
		return null;
	}

	/**
	 * 海外退运包裹 图片上传
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/picUp")
	@ResponseBody
	public String getupfile(HttpServletRequest request) {
		MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
		MultipartFile imgFile1 = req.getFile("pkgPic");
		// TODO 需要清除
		String fileName = "temp" + String.valueOf(System.currentTimeMillis()) + ".jpg";
		String rtnPath = UPLOAD_IMG_PATH_DB + fileName;
		try {
			FileUtils.copyInputStreamToFile(imgFile1.getInputStream(), new File(UPLOAD_IMG_PATH + fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rtnPath;

	}

	/**
	 * 海外退运包裹 图片保存
	 * 
	 * @param request
	 * @param packageImg
	 * @return
	 */
	@RequestMapping("/savePic")
	@ResponseBody
	public String savePic(HttpServletRequest request, PackageImg packageImg) {
		MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
		MultipartFile imgFile1 = req.getFile("pkgPic");

		// 文件名
		String fileName = "return" + String.valueOf(System.currentTimeMillis()) + ".jpg";
		try {
			FileUtils.copyInputStreamToFile(imgFile1.getInputStream(), new File(UPLOAD_IMG_PATH + fileName));

			String rtnPath = UPLOAD_IMG_PATH_DB + fileName;

			packageImg.setImg_type(IMG_TYPE_RETURN);

			PackageImg packageImgOld = packageImgService.queryPackageImg(packageImg);
			if (packageImgOld != null) {
				// 更新图片路径
				packageImgOld.setImg_path(rtnPath);
				packageImgService.updateImgPath(packageImgOld);
			} else {

				// 图片路径
				packageImg.setImg_path(rtnPath);
				// 图片类型 2：退货图片
				packageImg.setImg_type(IMG_TYPE_RETURN);
				// 插入记录
				packageImgService.insertPackageImg(packageImg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 异常包裹 查询
	 * 
	 * @param request
	 * @param original_num
	 * @param logistics_code
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/searchAbnormalPkg")
	public String searchAbnormalPkg(HttpServletRequest request, String original_num, String logistics_code,
			String fromDate, String toDate) {
		// 分页查询
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			pageView = new PageView(1);

			// 异常包裹
			params.put("status", PkgConstant.LOGISTICS_UNUSUALLY);
			// 关联单号
			params.put("original_num", original_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);

			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}
		// 分页查询参数
		params.put("pageView", pageView);
		// 异常包裹查询
		List<Pkg> abnormalPkgList = packageService.queryAllAbnormalPkg(params);

		// 页面显示信息
		request.setAttribute("abnormalPkgList", abnormalPkgList);
		request.setAttribute("pageView", pageView);
		// 查询条件保持

		request.setAttribute("params", params);

		return "back/abnormalPkgList";
	}

	/**
	 * 异常包裹 关联
	 * 
	 * @param request
	 * @param package_id
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/associate")
	public String associate(HttpServletRequest request, String package_id) {
		int pkg_id = Integer.parseInt(package_id);

		// 异常包裹包裹信息
		Pkg pkgDetail = packageService.queryPackageById(pkg_id);
		// 商品
		List<PkgGoods> pkgGoodsList = pkgGoodsService.selectPkgGoodsByPackageId(pkg_id);
		// 计算总价值
		BigDecimal total_worth = new BigDecimal("0");
		for (PkgGoods pkgGoods : pkgGoodsList) {
			total_worth = total_worth
					.add(new BigDecimal(pkgGoods.getPrice()).multiply(new BigDecimal(pkgGoods.getQuantity())));
		}

		String osaddr = "";

		// 海外仓库地址
		OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
		if (overseasAddress != null) {
			// 海外地址拼接
			osaddr = joinOverseasAddress(overseasAddress);
		}

		request.setAttribute("pkgDetail", pkgDetail);
		request.setAttribute("pkgGoodsList", pkgGoodsList);
		request.setAttribute("osaddr", osaddr);
		request.setAttribute("total_worth", total_worth);

		return "back/abnormalPkgDetail";
	}

	/**
	 * 异常包裹 客户信息查询
	 * 
	 * @param request
	 * @param package_id
	 * @param account
	 * @param user_name
	 * @param mobile
	 * @return email
	 */
	@RequestMapping("/frontUserInfo")
	@ResponseBody
	public Map<String, Object> getFrontUserInfo(HttpServletRequest request, String account, String user_name,
			String mobile, String email) {
		// 查询条件
		Map<String, Object> params = new HashMap<String, Object>();

		// 退运包裹
		params.put("account", account);

		// 起始时间
		params.put("user_name", user_name);
		// 终止时间
		params.put("mobile", mobile);

		// 终止时间
		params.put("email", email);
		// 用户信息查询
		List<FrontUser> userInfo = frontUserService.queryFrontUser(params);
		// 回传数据
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		// 根据条件检索不到用户
		if (null == userInfo || 0 == userInfo.size()) {
			rtnMap.put("result", false);
		} else {
			rtnMap.put("result", true);
			rtnMap.put("userInfo", userInfo.get(0));
		}

		return rtnMap;
	}

	/**
	 * 异常包裹 用户信息关联
	 * 
	 * @param request
	 * @param pkg
	 * 
	 * @return
	 */
	@RequestMapping("/associateFrontUser")
	@ResponseBody
	public String associateFrontUser(HttpServletRequest request, Pkg pkg) {
		// 更新包裹状态：已入库
		pkg.setStatus(PkgConstant.LOGISTICS_STORAGED);
		// 更新
		User user = (User) request.getSession().getAttribute("back_user");
		packageService.updateUserId(pkg, user);

		return "back/abnormalPkg";
	}

	/**
	 * 国内运单管理
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/domestic")
	public String domestic(HttpServletRequest request) {

		return "back/domestic";
	}

	/**
	 * 国内运单管理 EXCEL上传
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/excel")
	@ResponseBody
	public Map<String, Object> getExcel(HttpServletRequest request) {
		MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
		MultipartFile excelFile = req.getFile("excel");
		// 文件名
		String fileName = excelFile.getOriginalFilename();
		// 后缀名
		String exName = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());

		Map<String, Object> rtnMap = new HashMap<String, Object>();

		List<Pkg> pkgList = new ArrayList<Pkg>();

		Map<String, String> colNameMap = new HashMap<String, String>();

		colNameMap.put("logistics_code", LOGISTICSCODE_COLNAME);
		colNameMap.put("ems_code", EMSCODE_COLNAME);
		colNameMap.put("com", COM_COLNAME);

		Workbook workbook = null;
		String errorFileName = "";

		try {

			// Excel版本 version = 2003
			if ("xls".endsWith(exName)) {

				errorFileName = "error.xls";

				workbook = new HSSFWorkbook(excelFile.getInputStream());

				// Excel版本 version = 2007
			} else if ("xlsx".endsWith(exName)) {
				errorFileName = "error.xlsx";
				workbook = new XSSFWorkbook(excelFile.getInputStream());

			} else {
				rtnMap.put("flag", IS_NOT_EXCEL);
				rtnMap.put("message", "文件类型错误！请选择Excel文件");
				return rtnMap;
			}
			pkgList = ExcelReadUtil.excelToBean(workbook, SHEET_NAME, colNameMap, Pkg.class);
		} catch (Exception e) {
			e.printStackTrace();
			rtnMap.put("flag", EXCEL_CONTENT_ERROR);
			rtnMap.put("message", "无法识别文件内容！");
			return rtnMap;
		}
		// 读取不到数据
		if (pkgList == null || pkgList.size() == 0) {
			rtnMap.put("flag", EXCEL_CONTENT_ERROR);
			rtnMap.put("message", "没有清单信息！");
			return rtnMap;
		}

		// 发送单号用
		List<Pkg> updateList = new ArrayList<Pkg>();

		// 公司运单号存在检查 去除已派送单号经存在并且没有变化的数据
		List<String> logisticsCodeList = unMatchLogisticsCode(pkgList, updateList);

		// 重复单号检查
		Map<String, String> sameCodeMap = sameCode(pkgList);

		// 已经存在的派送单号
		List<String> emsCodeList = sameEmsCode(pkgList);

		if ((logisticsCodeList == null || logisticsCodeList.isEmpty()) && (sameCodeMap == null || sameCodeMap.isEmpty())
				&& (emsCodeList == null || emsCodeList.isEmpty())) {
			if (!updateList.isEmpty()) {
				// 更新EMS物流单号
				User user = (User) request.getSession().getAttribute("back_user");
				// 需要登陆的快递单号
				List<ExpressInfo> insertList = new ArrayList<ExpressInfo>();

				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				for (Pkg pkg : updateList) {

					// 登陆快递信息情况
					ExpressInfo expressInfo = new ExpressInfo();
					expressInfo.setNu(pkg.getEms_code());
					expressInfo.setRegion(pkg.getRegion());
					expressInfo.setCom(pkg.getCom());
					expressInfo.setLogistics_code(pkg.getLogistics_code());
					expressInfo.setSubscribe_status(ExpressInfo.SUBSCRIBE_STATUS_SUBSCRIBE);
					expressInfo.setReceive_time(timestamp);
					expressInfo.setAble(ExpressInfo.ABLE_YES);

					insertList.add(expressInfo);

				}

				packageService.batchUpdatePkg(updateList, insertList, user);
				// 订阅线程
				ExpressOrderThread expressOrderThread = new ExpressOrderThread(insertList);
				expressOrderThread.start();

			}
		} else {
			try {
				// 错误订单号标识
				ceateDownExcel(workbook, logisticsCodeList, sameCodeMap, emsCodeList);

				File file = new File(UPLOAD_IMG_PATH + errorFileName);
				if (file.exists()) {
					file.delete();
				}
				FileOutputStream fs = new FileOutputStream(file);
				workbook.write(fs);
				fs.close();

				String path = "/upload/" + errorFileName;

				rtnMap.put("flag", LOGISTICSCODE_NOT_EXIST);
				rtnMap.put("message", "导入清单信息有误！");
				rtnMap.put("path", path);

				return rtnMap;
			}

			catch (Exception e) {
				e.printStackTrace();
				rtnMap.put("flag", EXCEL_CONTENT_ERROR);
				rtnMap.put("message", "生成错误报告失败，请于管理员联系！");
				return rtnMap;
			}
		}
		rtnMap.put("message", "导入成功！");

		return rtnMap;
	}

	/**
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("/initadd")
	public String initAdd(HttpServletRequest req, HttpServletResponse resp, String original_num) {
		String logistics_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PACKAGE);
		Map<String, Object> params = new HashMap<String, Object>();

		// 获取未删除地址
		params.put("isdeleted", OverseasAddress.ISDELETE_UNDELETED);
		/*
		 * List<OverseasAddress> overseasAddressList =
		 * overseasAddressService.queryAllOverseasAddress(params); String
		 * derictShowOrverseasAddress="N";
		 * if(overseasAddressList!=null&&overseasAddressList.size()==1) {
		 * derictShowOrverseasAddress="Y"; }
		 * req.setAttribute("overseasAddressList", overseasAddressList);
		 * req.setAttribute("logistics_code", logistics_code);
		 * req.setAttribute("original_num", original_num);
		 * req.setAttribute("derictShowOrverseasAddress",
		 * derictShowOrverseasAddress);
		 */

		Map<String, Object> paramsO = new HashMap<String, Object>();
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			paramsO.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			req.setAttribute("overseasAddressList", overseasAddressList);
			String derictShowOrverseasAddress = "N";
			if (overseasAddressList != null && overseasAddressList.size() == 1) {
				derictShowOrverseasAddress = "Y";
			}
			req.setAttribute("derictShowOrverseasAddress", derictShowOrverseasAddress);

		}
		req.setAttribute("logistics_code", logistics_code);
		req.setAttribute("original_num", original_num);

		return "back/pkgadd";
	}

	@RequestMapping("/addpkg")
	public String addPkg(HttpServletRequest req, HttpServletResponse resp, Pkg pkg) {
		Timestamp creatTime = new Timestamp(System.currentTimeMillis());
		pkg.setCreateTime(creatTime);

		FrontUser frontUser = frontUserService.queryFrontUserById(pkg.getUser_id());
		if (frontUser != null) {
			// 此处查询报价单，将报价单id做set包裹处理
			QuotationManage qm = quotationManageService.getQuotaByPkgInfo(pkg.getOsaddr_id(), pkg.getExpress_package(),
					frontUser.getUser_type(), frontUser.getUser_id());
			if (qm != null) {
				pkg.setQuota_id(qm.getQuota_id());
			}

		}

		User user = (User) req.getSession().getAttribute("back_user");
		int package_id = packageService.insertPkg(pkg, user);
		return associate(req, package_id + "");
		// return "back/pkgaddsuccess";
	}

	/**
	 * 公司运单号存在检查 去除已派送单号经存在并且没有变化的数据
	 * 
	 * @param list
	 *            导入的excel数据
	 * @param updateList
	 *            需要更新并且向快递100发送请求的数据
	 * @return 公司运单号不存在
	 */
	private List<String> unMatchLogisticsCode(List<Pkg> list, List<Pkg> updateList) {
		List<String> rtnList = new ArrayList<String>();
		List<Pkg> pkgList = packageService.queryPackageBylogisticsCodes(list);

		String logisticsCode = "";
		String emsCode = "";

		for (Pkg pkgA : list) {

			logisticsCode = pkgA.getLogistics_code();
			emsCode = pkgA.getEms_code();

			boolean matchFlag = false;

			for (Pkg pkgB : pkgList) {
				// 导入的 公司运单号 和派送单号 与原来相同
				if (logisticsCode.equals(pkgB.getLogistics_code()) && emsCode.equals(pkgB.getEms_code())) {
					matchFlag = true;
					break;

				} else if (logisticsCode.equals(pkgB.getLogistics_code())) {
					// 向快递100发送请求用的 省市
					pkgA.setRegion(city(pkgB.getRegion()));

					// 更新包裹状态为:已清关派件中
					pkgA.setStatus(Pkg.LOGISTICS_CUSTOMS_ALREADY);
					updateList.add(pkgA);
					matchFlag = true;
					break;
				}
			}

			// 包裹表中不存在的订单
			if (!matchFlag) {
				rtnList.add(logisticsCode);
			}
		}

		return rtnList;
	}

	/**
	 * 重复公司运单号，派送单号
	 * 
	 * @param list
	 * @return
	 */
	private Map<String, String> sameCode(List<Pkg> list) {
		Map<String, String> rtnMap = new HashMap<String, String>();

		for (Pkg pkgA : list) {
			String logisticsCode = pkgA.getLogistics_code();
			String emsCode = pkgA.getEms_code();

			int logisticsCodeIndex = 0;
			int emsCodeIndex = 0;

			for (Pkg pkgB : list) {

				if (logisticsCode.equals(pkgB.getLogistics_code())) {
					logisticsCodeIndex++;
					if (logisticsCodeIndex == 2) {

						rtnMap.put(logisticsCode, "1");
					}
				}

				if (emsCode.equals(pkgB.getEms_code())) {
					emsCodeIndex++;
					if (emsCodeIndex == 2) {

						rtnMap.put(emsCode, "2");
					}
				}

				if (logisticsCodeIndex == 2 && emsCodeIndex == 2) {
					break;
				}

			}

		}

		return rtnMap;
	}

	/**
	 * 派送单号唯一
	 * 
	 * @param list
	 * @return
	 */
	private List<String> sameEmsCode(List<Pkg> list) {
		List<String> rtnList = new ArrayList<String>();

		List<String> emsCodeList = packageService.queryAllEmsCode(list);

		for (Pkg pkgA : list) {

			String emsCode = pkgA.getEms_code();
			if (emsCodeList.contains(emsCode)) {

				rtnList.add(emsCode);
			}
		}

		return rtnList;
	}

	/**
	 * 错误的翔锐物流单号 订单号涂颜色,返回错误订单号文件路径
	 * 
	 * @param excelFile
	 * @param list
	 * @return
	 * @throws Exception
	 */
	private void ceateDownExcel(Workbook workBook, List<String> logisticsCodeList, Map<String, String> sameCodeMap,
			List<String> emsCodeList) throws Exception {

		int logisticsCodeColNumber = COL_NUMBER_INIT;
		int emsCodeColNumber = COL_NUMBER_INIT;

		// 取得订单sheet
		Sheet orderSheet = workBook.getSheet(SHEET_NAME);
		// 总行数
		int rows = orderSheet.getLastRowNum();
		// 第一行：列名
		Row firstHssfRow = orderSheet.getRow(0);
		// 总列数
		int cols = firstHssfRow.getPhysicalNumberOfCells();
		for (int n = 0; n < cols; n++) {
			String colName = firstHssfRow.getCell(n).getStringCellValue();
			// 公司运单号
			if (LOGISTICSCODE_COLNAME.equals(colName)) {
				logisticsCodeColNumber = n;

			}

			// 派送单号
			if (EMSCODE_COLNAME.equals(colName)) {
				emsCodeColNumber = n;

			}
		}
		// 不存在公司运单号单元格格式
		CellStyle cellStyle1 = workBook.createCellStyle();
		cellStyle1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle1.setFillForegroundColor(IndexedColors.RED.getIndex());

		// 重复公司运单号单元格格式
		CellStyle cellStyle2 = workBook.createCellStyle();
		cellStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle2.setFillForegroundColor(IndexedColors.DARK_RED.getIndex());

		// 重复派送单号单元格格式
		CellStyle cellStyle3 = workBook.createCellStyle();
		cellStyle3.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle3.setFillForegroundColor(IndexedColors.DARK_YELLOW.getIndex());

		// 已存在派送单号单元格格式
		CellStyle cellStyle4 = workBook.createCellStyle();
		cellStyle4.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle4.setFillForegroundColor(IndexedColors.YELLOW.getIndex());

		for (int i = 1; i <= rows; i++) {

			Row hssfRow = orderSheet.getRow(i);
			// 空行检验
			if (hssfRow == null || ExcelReadUtil.isBlankRow(hssfRow)) {
				continue;
			}

			// 翔锐物流单号 订单号

			String logisticsCode = orderSheet.getRow(i).getCell(logisticsCodeColNumber).getStringCellValue().trim();
			String emsCode = orderSheet.getRow(i).getCell(emsCodeColNumber).getStringCellValue().trim();

			if (logisticsCodeList.contains(logisticsCode)) {
				// 不存在公司运单号涂颜色
				orderSheet.getRow(i).getCell(logisticsCodeColNumber).setCellStyle(cellStyle1);
			}

			if ("1".equals(sameCodeMap.get(logisticsCode))) {
				// 重复公司运单号涂颜色
				orderSheet.getRow(i).getCell(logisticsCodeColNumber).setCellStyle(cellStyle2);
			}
			String vString = sameCodeMap.get(emsCode);
			if ("2".equals(vString)) {
				// 重复派送单号涂颜色
				orderSheet.getRow(i).getCell(emsCodeColNumber).setCellStyle(cellStyle3);
			}

			if (emsCodeList.contains(emsCode)) {

				// 已存在派送单号涂颜色
				orderSheet.getRow(i).getCell(emsCodeColNumber).setCellStyle(cellStyle4);
			}
		}

	}

	/**
	 * 海外地址拼接
	 * 
	 * @param osa
	 * @return String
	 */
	private String joinOverseasAddress(OverseasAddress osa) {
		String osaddr = osa.getAddress_first() + osa.getCity() + osa.getState() + osa.getCounty() + osa.getCountry();
		return osaddr;
	};

	/**
	 * 解析省市县
	 * 
	 * @param request
	 * @param jsonString
	 * @return
	 */
	private String addressInfo(String jsonString) {
		if (StringUtil.isEmpty(jsonString)) {
			return "";
		}
		List<String> aList = new ArrayList<String>();
		try {
			aList = JSONUtil.readValueFromJson(jsonString, "name");
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		StringBuffer address = new StringBuffer("");
		for (String city : aList) {
			address.append(city);
		}

		return address.toString().replaceAll("\"", "");
	}

	/**
	 * 解析省市
	 * 
	 * @param request
	 * @param jsonString
	 * @return
	 */
	private static String city(String jsonString) {
		if (StringUtil.isEmpty(jsonString)) {
			return "";
		}

		List<String> aList = new ArrayList<String>();
		try {
			aList = JSONUtil.readValueFromJson(jsonString, "name");
		} catch (Exception e) {
			e.printStackTrace();

			return "";
		}
		StringBuffer address = new StringBuffer("");

		int index = 0;

		for (String city : aList) {
			// 只要省和市
			if (index > 1) {
				break;
			}
			address.append(city);
			index++;
		}

		return address.toString().replaceAll("\"", "");
	}

	/**
	 * 货量统计初始化查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/goodsCountquery")
	public String getGoodsCountPackageinit(HttpServletRequest request) {

		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		// 包裹列表
		List<Pkg> pkgList = packageService.queryGoodsCountAll(params);
		request.setAttribute("pkgList", pkgList);

		return "back/goodsCountPackageList";
	}

	/**
	 * 货量统计搜索
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/goodsCountSearch")
	public String getGoodsCountPackageList(HttpServletRequest request, String fromDate, String toDate) {
		// 分页查询
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);

		} else {
			pageView = new PageView(1);

			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);

		}

		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<Pkg> pkgList = packageService.queryGoodsCountAll(params);
		request.setAttribute("pkgList", pkgList);

		request.setAttribute("params", params);

		return "back/goodsCountPackageList";
	}

	/**
	 * 导出货量统计
	 * 
	 * @param request
	 * @param response
	 * @param packageIds
	 */

	@RequestMapping("/goodsCountExportList")
	public void goodsCountExportList(HttpServletRequest request, HttpServletResponse response, String fromDate,
			String toDate) {
		try {
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "货量统计";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition",
					"attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");
			Map<String, Object> params = new HashMap<String, Object>();

			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			List<Pkg> packageList = packageService.queryGoodsCountAll(params);

			@SuppressWarnings("resource")
			XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

			// 新建sheet
			XSSFSheet xssfSheet = xssfWorkbook.createSheet("货量统计");
			// 第一列固定
			xssfSheet.createFreezePane(0, 1, 0, 1);

			// 颜色黄色
			XSSFColor yellowColor = new XSSFColor(Color.YELLOW);

			// 样式黄色居中
			XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
			style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			style2.setFillForegroundColor(yellowColor);
			style2.setWrapText(true);

			// 列宽
			xssfSheet.setColumnWidth(0, 1500);
			xssfSheet.setColumnWidth(1, 2000);
			xssfSheet.setColumnWidth(2, 5000);
			xssfSheet.setColumnWidth(3, 4000);
			xssfSheet.setColumnWidth(4, 4000);
			xssfSheet.setColumnWidth(5, 4000);
			xssfSheet.setColumnWidth(6, 4000);
			xssfSheet.setColumnWidth(7, 5000);
			xssfSheet.setColumnWidth(8, 4000);
			xssfSheet.setColumnWidth(9, 6000);
			xssfSheet.setColumnWidth(10, 5000);
			xssfSheet.setColumnWidth(11, 4000);
			xssfSheet.setColumnWidth(12, 5000);
			xssfSheet.setColumnWidth(13, 5000);
			xssfSheet.setColumnWidth(14, 5000);
			xssfSheet.setColumnWidth(15, 4000);
			xssfSheet.setColumnWidth(16, 4000);

			// 增值服务
			List<String> serviceNameList = attachServiceService.queryAttachServiceName();
			int serviceCount = 0;
			if (serviceNameList != null) {
				serviceCount = serviceNameList.size();
			}
			xssfSheet.setColumnWidth(17 + serviceCount, 5000);
			xssfSheet.setColumnWidth(18 + serviceCount, 6000);
			xssfSheet.setColumnWidth(19 + serviceCount, 8000);
			xssfSheet.setColumnWidth(20 + serviceCount, 5000);
			xssfSheet.setColumnWidth(21 + serviceCount, 4000);
			xssfSheet.setColumnWidth(22 + serviceCount, 4000);
			xssfSheet.setColumnWidth(23 + serviceCount, 1000);
			xssfSheet.setColumnWidth(24 + serviceCount, 1500);
			xssfSheet.setColumnWidth(25 + serviceCount, 5000);
			xssfSheet.setColumnWidth(26 + serviceCount, 5000);
			xssfSheet.setColumnWidth(27 + serviceCount, 5000);
			xssfSheet.setColumnWidth(28 + serviceCount, 5000);
			xssfSheet.setColumnWidth(29 + serviceCount, 5000);

			// 表头列
			XSSFRow firstXSSFRow = xssfSheet.createRow(0);

			List<String> columnsList = new ArrayList<String>();
			columnsList.add("业务");
			columnsList.add("客户姓名");
			columnsList.add("客户账号");
			columnsList.add("客户手机号");
			columnsList.add("账户余额");
			columnsList.add("可用余额");
			columnsList.add("冻结余额");
			columnsList.add("公司运单号");
			columnsList.add("所属仓库");
			columnsList.add("品名");
			columnsList.add("品牌");
			columnsList.add("申报类别");
			columnsList.add("申报总价值(RMB)");
			columnsList.add("重量(实际/磅)");
			columnsList.add("重量(收费/磅)");
			columnsList.add("单价(RMB)");
			columnsList.add("运费(RMB)");

			// 增值服务
			columnsList.addAll(serviceNameList);

			columnsList.add("合计(RMB)");
			columnsList.add("支付宝号");
			columnsList.add("订单号");
			columnsList.add("支付宝流水号");
			columnsList.add("银行流水号");
			columnsList.add("金额");
			columnsList.add("类型");
			columnsList.add("状态");
			columnsList.add("交易时间");
			columnsList.add("包裹入库时间");
			columnsList.add("包裹清关时间");
			columnsList.add("包裹创建日期");
			columnsList.add("描述");

			for (int i = 0; i < columnsList.size(); i++) {
				XSSFCell cell = firstXSSFRow.createCell(i);
				cell.setCellType(XSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(columnsList.get(i));
				cell.setCellStyle(style2);
			}

			List<Integer> haveExportedPackageList = new ArrayList<Integer>();
			Pkg pkgDetail = null;
			int rowCount = 1;
			for (int j = 0; j < packageList.size(); j++) {
				pkgDetail = packageList.get(j);
				if (haveExportedPackageList.contains(new Integer(pkgDetail.getPackage_id()))) {
					continue;
				}
				XSSFRow row = null;
				// 消费记录
				AccountLog accountLog = null;
				// 同行帐单信息
				MemberBillModel memberBillModel = null;
				FrontUser frontUser = null;
				List<Pkg> recountPackageList = new ArrayList<Pkg>();
				if (packageList.get(j).getLogistics_code() != null) {
					accountLog = accountLogService
							.selectAccountLogByLogistics_code(packageList.get(j).getLogistics_code());
					if (accountLog != null) {
						String logisticsStr = accountLog.getLogistics_code();
						String[] logisticsArray = logisticsStr.split("<br>");
						List<String> logistics_codeList = new ArrayList<String>();
						if (logisticsArray != null && logisticsArray.length > 1) {
							for (String tempLogisticsCode : logisticsArray) {
								logistics_codeList.add(tempLogisticsCode);
							}
							List<Pkg> readyCountPackageList = packageService
									.queryPackageByLogistics_codeList(logistics_codeList);
							if (readyCountPackageList != null && readyCountPackageList.size() > 0) {
								for (Pkg tempPkg : readyCountPackageList) {
									List<PkgLog> packageLogList = pkgLogService
											.queryPkgLogByPackageId(tempPkg.getPackage_id(), fromDate, toDate);
									if (packageLogList != null && packageLogList.size() > 0 && !haveExportedPackageList
											.contains(new Integer(tempPkg.getPackage_id()))) {
										recountPackageList.add(tempPkg);
									}
								}
							}
						}
					}
				}
				int packageListSize = recountPackageList.size();
				if (packageListSize > 0) {
					CellRangeAddress cra18 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							18 + serviceCount, 18 + serviceCount);
					// 在sheet里增加合并单元格
					xssfSheet.addMergedRegion(cra18);
					CellRangeAddress cra19 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							19 + serviceCount, 19 + serviceCount);
					xssfSheet.addMergedRegion(cra19);
					CellRangeAddress cra20 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							20 + serviceCount, 20 + serviceCount);
					xssfSheet.addMergedRegion(cra20);
					CellRangeAddress cra21 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							21 + serviceCount, 21 + serviceCount);
					xssfSheet.addMergedRegion(cra21);
					CellRangeAddress cra22 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							22 + serviceCount, 22 + serviceCount);
					xssfSheet.addMergedRegion(cra22);
					CellRangeAddress cra23 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							23 + serviceCount, 23 + serviceCount);
					xssfSheet.addMergedRegion(cra23);
					CellRangeAddress cra24 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							24 + serviceCount, 24 + serviceCount);
					xssfSheet.addMergedRegion(cra24);
					CellRangeAddress cra25 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							25 + serviceCount, 25 + serviceCount);
					xssfSheet.addMergedRegion(cra25);
					CellRangeAddress cra29 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1,
							29 + serviceCount, 29 + serviceCount);
					xssfSheet.addMergedRegion(cra29);
					for (int i = 0; i < recountPackageList.size(); i++) {
						row = xssfSheet.createRow(rowCount++);
						pkgDetail = recountPackageList.get(i);
						memberBillModel = memberBillService.queryMemberBillModelByPackageId(pkgDetail.getPackage_id());
						frontUser = frontUserService.queryFrontUserById(pkgDetail.getUser_id());
						if (i == 0) {
							addRow(row, frontUser, accountLog, memberBillModel, pkgDetail, serviceNameList,
									serviceCount, true);
						} else {
							addRow(row, frontUser, accountLog, memberBillModel, pkgDetail, serviceNameList,
									serviceCount, false);
						}
						haveExportedPackageList.add(new Integer(pkgDetail.getPackage_id()));
					}
				} else {
					row = xssfSheet.createRow(rowCount++);
					memberBillModel = memberBillService
							.queryMemberBillModelByPackageId(packageList.get(j).getPackage_id());
					frontUser = frontUserService.queryFrontUserById(packageList.get(j).getUser_id());
					addRow(row, frontUser, accountLog, memberBillModel, pkgDetail, serviceNameList, serviceCount, true);
					haveExportedPackageList.add(new Integer(pkgDetail.getPackage_id()));
				}
			}
			OutputStream oStream = response.getOutputStream();

			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	private void addRow(XSSFRow row, FrontUser frontUser, AccountLog accountLog, MemberBillModel memberBillModel,
			Pkg pkgDetail, List<String> serviceNameList, int serviceCount, boolean needAddAccountLogCell) {
		// 业务
		XSSFCell cell0 = row.createCell(0);
		cell0.setCellValue(frontUser != null ? frontUser.getBusiness_name() : "");

		// 客户姓名
		XSSFCell cell1 = row.createCell(1);
		cell1.setCellValue(frontUser != null ? frontUser.getUser_name() : "");

		// 客户账号
		XSSFCell cell2 = row.createCell(2);
		cell2.setCellValue(frontUser != null ? frontUser.getAccount() : "");

		// 客户手机号
		XSSFCell cell3 = row.createCell(3);
		cell3.setCellValue(frontUser != null ? StringUtils.trimToEmpty(frontUser.getMobile()) : "");

		// 账户余额
		XSSFCell cell4 = row.createCell(4);
		cell4.setCellValue(frontUser != null ? NumberUtils.scaleMoneyData(frontUser.getBalance()) : "");

		// 可用余额
		XSSFCell cell5 = row.createCell(5);
		cell5.setCellValue(frontUser != null ? NumberUtils.scaleMoneyData(frontUser.getAble_balance()) : "");

		// 冻结余额
		XSSFCell cell6 = row.createCell(6);
		cell6.setCellValue(frontUser != null ? NumberUtils.scaleMoneyData(frontUser.getFrozen_balance()) : "");

		// 公司运单号
		XSSFCell cell7 = row.createCell(7);
		cell7.setCellValue(pkgDetail.getLogistics_code());

		// 所属仓库
		XSSFCell cell8 = row.createCell(8);
		cell8.setCellValue(memberBillModel != null ? memberBillModel.getWarehouse() : "");

		// 品名
		XSSFCell cell9 = row.createCell(9);
		cell9.setCellValue(memberBillModel != null ? memberBillModel.getGoods_name() : "");

		// 品牌
		XSSFCell cell10 = row.createCell(10);
		cell10.setCellValue(memberBillModel != null ? memberBillModel.getBrand() : "");

		// 申报类别
		XSSFCell cell11 = row.createCell(11);
		cell11.setCellValue(memberBillModel != null ? memberBillModel.getGoods_type() : "");

		// 申报总价值(RMB)
		XSSFCell cell12 = row.createCell(12);
		cell12.setCellValue(
				memberBillModel != null ? NumberUtils.scaleMoneyData(memberBillModel.getTotal_worth()) : "");

		// 重量(实际/磅)
		XSSFCell cell13 = row.createCell(13);
		cell13.setCellValue("" + pkgDetail.getActual_weight());

		// 重量(收费/磅)
		XSSFCell cell14 = row.createCell(14);
		cell14.setCellValue("" + pkgDetail.getWeight());

		// 单价(RMB)
		XSSFCell cell15 = row.createCell(15);
		cell15.setCellValue(NumberUtils.scaleMoneyData(pkgDetail.getPrice()));

		// 运费(RMB)
		XSSFCell cell16 = row.createCell(16);
		cell16.setCellValue(NumberUtils.scaleMoneyData(pkgDetail.getFreight()));

		// 增值服务
		for (int k = 0; k < serviceNameList.size(); k++) {

			XSSFCell attachServiceCell = row.createCell(17 + k);
			if (memberBillModel != null) {
				Map<String, Float> map = memberBillModel.getAttach_service_map();
				if (map != null && !map.isEmpty()) {
					Float price = map.get(serviceNameList.get(k));
					if (price != null && price.compareTo(0.000001f) > 0) {
						attachServiceCell.setCellValue(Double.parseDouble(price.toString()));
					}
				}
			} else {
				attachServiceCell.setCellValue("");
			}
		}
		// 合计(RMB)
		XSSFCell cell17 = row.createCell(17 + serviceCount);
		cell17.setCellValue(
				memberBillModel != null ? NumberUtils.scaleMoneyData(memberBillModel.getTransport_cost()) : "");
		if (needAddAccountLogCell) {
			// 支付宝号
			XSSFCell cell18 = row.createCell(18 + serviceCount);
			cell18.setCellValue(accountLog != null ? StringUtils.trimToEmpty(accountLog.getAlipay_no()) : "");

			// 订单号
			XSSFCell cell19 = row.createCell(19 + serviceCount);
			cell19.setCellValue(accountLog != null ? StringUtils.trimToEmpty(accountLog.getOrder_id()) : "");

			// 支付宝流水号
			XSSFCell cell20 = row.createCell(20 + serviceCount);
			cell20.setCellValue(accountLog != null ? StringUtils.trimToEmpty(accountLog.getTrade_no()) : "");

			// 银行流水号
			XSSFCell cell21 = row.createCell(21 + serviceCount);
			cell21.setCellValue(accountLog != null ? StringUtils.trimToEmpty(accountLog.getBank_seq_no()) : "");

			// 金额
			XSSFCell cell22 = row.createCell(22 + serviceCount);
			cell22.setCellValue(accountLog != null ? NumberUtils.scaleMoneyData(accountLog.getAmount()) : "");

			// 类型
			XSSFCell cell23 = row.createCell(23 + serviceCount);
			int type = (accountLog != null ? accountLog.getAccount_type() : -100);
			String accountType = "";
			if (type == 1) {
				accountType = "充值";
			} else if (type == 2) {
				accountType = "消费";
			} else if (type == 3) {
				accountType = "提现";
			} else if (type == 4) {
				accountType = "索赔";
			}
			cell23.setCellValue(accountType);

			// 状态
			XSSFCell cell24 = row.createCell(24 + serviceCount);
			int status = (accountLog != null ? accountLog.getStatus() : -100);
			String accountStatus = "";
			if (status == 1) {
				accountStatus = "申请中";
			} else if (status == 2) {
				accountStatus = "成功";
			} else if (status == 3) {
				accountStatus = "失败";
			} else if (status == 4) {
				accountStatus = "取消";
			}
			cell24.setCellValue(accountStatus);

			// 交易时间
			XSSFCell cell25 = row.createCell(25 + serviceCount);
			cell25.setCellValue(accountLog != null ? StringUtils.trimToEmpty(accountLog.getOpr_time_string()) : "");
		}
		// 包裹入库日期
		XSSFCell cell26 = row.createCell(26 + serviceCount);
		String packageCreateTime = pkgDetail.getCreateTime() != null
				? simpleDateFormat.format(pkgDetail.getCreateTime()) : "";
		String packageArriveTime = pkgDetail.getArrive_time() != null
				? simpleDateFormat.format(pkgDetail.getArrive_time()) : "";
		if (packageArriveTime.equals("")) {
			packageArriveTime = packageCreateTime;
		}
		cell26.setCellValue(packageArriveTime);

		// 包裹清关时间
		XSSFCell cell27 = row.createCell(27 + serviceCount);
		String operateTimeStr = "";
		if (memberBillModel != null) {
			if (memberBillModel.getOperate_time() != null) {
				operateTimeStr = simpleDateFormat.format(memberBillModel.getOperate_time());
			}
		}
		cell27.setCellValue(operateTimeStr);

		// 包裹创建日期
		XSSFCell cell28 = row.createCell(28 + serviceCount);
		cell28.setCellValue(packageCreateTime);

		// 描述
		XSSFCell cell29 = row.createCell(29 + serviceCount);
		cell29.setCellValue(accountLog != null ? StringUtils.trimToEmpty(accountLog.getDescription()) : "");
	}

	/**
	 * 待发货包裹初始化查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/readyDeliveryPackageQuery")
	public String getReadyDeliveryPackageinit(HttpServletRequest request) {

		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			params.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		// 包裹列表
		List<PkgOperate> pkgOperateList = packageService.queryReadyDeliveryPackage(params);
		request.setAttribute("pkgOperateList", pkgOperateList);

		return "back/pkg_readyDeliveryPackageList";
	}

	/**
	 * 待发货包裹搜索
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/readyDeliveryPackageSearch")
	public String getReadyDeliveryPackageList(HttpServletRequest request, String fromDate, String toDate,
			String express_num, String logistics_code, String original_num, String last_name, String user_name,
			String overseasAddress_id, String express_package) {
		// 分页查询
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
				List<OverseasAddress> overseasAddressList = overseasAddressService
						.queryOverseasAddressByUserId(user.getUser_id());
				request.setAttribute("overseasAddressList", overseasAddressList);
			}
			pageView = new PageView(1);
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 物流单号
			params.put("express_num", express_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// LastName
			params.put("last_name", last_name);
			// 用户名
			params.put("user_name", user_name);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);
			// 快件包裹
			params.put("express_package", express_package);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);

		}

		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<PkgOperate> pkgOperateList = packageService.queryReadyDeliveryPackage(params);
		request.setAttribute("pkgOperateList", pkgOperateList);

		request.setAttribute("params", params);

		return "back/pkg_readyDeliveryPackageList";
	}

	/**
	 * 待发货包裹导出
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@RequestMapping("/exportReadyDeliveryPackage")
	public void exportReadyDeliveryPackageList(HttpServletRequest request, HttpServletResponse response,
			String fromDate, String toDate, String express_num, String logistics_code, String original_num,
			String last_name, String user_name, String overseasAddress_id, String express_package) {
		try {
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "待发货包裹";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition",
					"attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");
			Map<String, Object> params = new HashMap<String, Object>();
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
			}
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 物流单号
			params.put("express_num", express_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// LastName
			params.put("last_name", last_name);
			// 用户名
			params.put("user_name", user_name);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);
			// 快件包裹
			params.put("express_package", express_package);
			List<PkgOperate> pkgOperateList = packageService.queryReadyDeliveryPackageExport(params);
			XSSFWorkbook xssfWorkbook = packageService.createReadyDelivery_HaveInputed(fileName, pkgOperateList);
			OutputStream oStream = response.getOutputStream();
			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	/**
	 * 已入库包裹初始化查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/haveInputedPackageQuery")
	public String getHaveInputedPackageinit(HttpServletRequest request) {

		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			params.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		// 包裹列表
		List<PkgOperate> pkgOperateList = packageService.queryHaveInputedPackageInit(params);
		request.setAttribute("pkgOperateList", pkgOperateList);

		return "back/pkg_haveInputedPackageList";
	}
	
	/**
	 * 现金收款
	 * @param request
	 * @return
	 */
	@RequestMapping("/cashCollectionInit")
	public String cashCollectionInit(HttpServletRequest request) {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		request.setAttribute("curUser", user.getUsername());
		return "back/cashCollection";
	}

	/**
	 * 已入库包裹搜索
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/haveInputedPackageSearch")
	public String getHaveInputedPackageList(HttpServletRequest request, String fromDate, String toDate,
			String express_num, String logistics_code, String original_num, String last_name, String user_name,
			String overseasAddress_id, String express_package) {
		// 分页查询
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
				List<OverseasAddress> overseasAddressList = overseasAddressService
						.queryOverseasAddressByUserId(user.getUser_id());
				request.setAttribute("overseasAddressList", overseasAddressList);
			}
			pageView = new PageView(1);
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 物流单号
			params.put("express_num", express_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// LastName
			params.put("last_name", last_name);
			// 用户名
			params.put("user_name", user_name);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);
			// 快件包裹
			params.put("express_package", express_package);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);

		}

		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<PkgOperate> pkgOperateList = packageService.queryHaveInputedPackageInit(params);
		request.setAttribute("pkgOperateList", pkgOperateList);

		request.setAttribute("params", params);

		return "back/pkg_haveInputedPackageList";
	}

	/**
	 * 已入库包裹导出
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@RequestMapping("/exportHaveInputedPackage")
	public void exportHaveInputedPackageList(HttpServletRequest request, HttpServletResponse response, String fromDate,
			String toDate, String express_num, String logistics_code, String original_num, String last_name,
			String user_name, String overseasAddress_id, String express_package) {
		try {
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "已入库包裹";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");
			Map<String, Object> params = new HashMap<String, Object>();
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
			}
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 物流单号
			params.put("express_num", express_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// LastName
			params.put("last_name", last_name);
			// 用户名
			params.put("user_name", user_name);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);
			// 快件包裹
			params.put("express_package", express_package);
			List<PkgOperate> pkgOperateList = packageService.queryHaveInputedPackage(params);
			XSSFWorkbook xssfWorkbook = packageService.createReadyDelivery_HaveInputed(fileName, pkgOperateList);
			OutputStream oStream = response.getOutputStream();
			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	/**
	 * 待处理包裹初始化查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/readyHandlePackageQuery")
	public String getReadyHandlePackageinit(HttpServletRequest request) {

		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			params.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		// 包裹列表
		List<PkgOperate> pkgOperateList = packageService.queryReadyHandlePackage(params);
		request.setAttribute("pkgOperateList", pkgOperateList);

		return "back/pkg_readyHandlePackageList";
	}

	/**
	 * 待处理包裹搜索
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/readyHandlePackageSearch")
	public String getReadyHandlePackageList(HttpServletRequest request, String fromDate, String toDate,
			String express_num, String logistics_code, String original_num, String last_name, String user_name,
			String overseasAddress_id, String express_package) {
		// 分页查询
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
				List<OverseasAddress> overseasAddressList = overseasAddressService
						.queryOverseasAddressByUserId(user.getUser_id());
				request.setAttribute("overseasAddressList", overseasAddressList);
			}
			pageView = new PageView(1);
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 物流单号
			params.put("express_num", express_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// LastName
			params.put("last_name", last_name);
			// 用户名
			params.put("user_name", user_name);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);
			// 快件包裹
			params.put("express_package", express_package);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);

		}

		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<PkgOperate> pkgOperateList = packageService.queryReadyHandlePackage(params);
		request.setAttribute("pkgOperateList", pkgOperateList);

		request.setAttribute("params", params);

		return "back/pkg_readyHandlePackageList";
	}

	/**
	 * 待处理包裹导出
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@RequestMapping("/exportReadyHandlePackage")
	public void exportReadyHandlePackageList(HttpServletRequest request, HttpServletResponse response, String fromDate,
			String toDate, String express_num, String logistics_code, String original_num, String last_name,
			String user_name, String overseasAddress_id, String express_package) {
		try {
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "待处理包裹";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition",
					"attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");
			Map<String, Object> params = new HashMap<String, Object>();
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
			}
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 物流单号
			params.put("express_num", express_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// LastName
			params.put("last_name", last_name);
			// 用户名
			params.put("user_name", user_name);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);
			// 快件包裹
			params.put("express_package", express_package);
			List<PkgOperate> pkgOperateList = packageService.queryReadyHandlePackage(params);
			XSSFWorkbook xssfWorkbook = packageService.createReadyHandle(fileName, pkgOperateList, false);
			OutputStream oStream = response.getOutputStream();
			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	/**
	 * 已到库包裹初始化查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/arrivedPackageQuery")
	public String getArrivedPackageinit(HttpServletRequest request) {

		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			params.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		// 包裹列表
		List<PkgOperate> pkgOperateList = packageService.queryArrivedPackage(params);
		request.setAttribute("pkgOperateList", pkgOperateList);

		return "back/pkg_arrivedPackageList";
	}

	/**
	 * 已到库包裹搜索
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/arrivedPackageSearch")
	public String getArrivedPackageList(HttpServletRequest request, String fromDate, String toDate, String express_num,
			String logistics_code, String original_num, String last_name, String user_name, String overseasAddress_id,
			String express_package) {
		// 分页查询
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
				List<OverseasAddress> overseasAddressList = overseasAddressService
						.queryOverseasAddressByUserId(user.getUser_id());
				request.setAttribute("overseasAddressList", overseasAddressList);
			}
			pageView = new PageView(1);
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 物流单号
			params.put("express_num", express_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// LastName
			params.put("last_name", last_name);
			// 用户名
			params.put("user_name", user_name);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);
			// 快件包裹
			params.put("express_package", express_package);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);

		}

		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<PkgOperate> pkgOperateList = packageService.queryArrivedPackage(params);
		request.setAttribute("pkgOperateList", pkgOperateList);

		request.setAttribute("params", params);

		return "back/pkg_arrivedPackageList";
	}

	/**
	 * 已到库包裹导出
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@RequestMapping("/exportArrivedPackage")
	public void exportArrivedPackageList(HttpServletRequest request, HttpServletResponse response, String fromDate,
			String toDate, String express_num, String logistics_code, String original_num, String last_name,
			String user_name, String overseasAddress_id, String express_package) {
		try {
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "到库包裹";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition",
					"attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");
			Map<String, Object> params = new HashMap<String, Object>();
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
			}
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 物流单号
			params.put("express_num", express_num);
			// 公司运单号
			params.put("logistics_code", logistics_code);
			// 关联单号
			params.put("original_num", original_num);
			// LastName
			params.put("last_name", last_name);
			// 用户名
			params.put("user_name", user_name);
			// 仓库ID
			params.put("overseasAddress_id", overseasAddress_id);
			// 快件包裹
			params.put("express_package", express_package);
			List<PkgOperate> pkgOperateList = packageService.queryArrivedPackage(params);
			XSSFWorkbook xssfWorkbook = packageService.createReadyHandle(fileName, pkgOperateList, true);
			OutputStream oStream = response.getOutputStream();
			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	/**
	 * 校验包裹关联单号
	 */
	@RequestMapping("/checkOriginalnum")
	@ResponseBody
	public Map<String, Object> checkOriginalnum(HttpServletRequest req, String original_num) {
		Map<String, Object> result = new HashMap<String, Object>();
		String msg = "0";
		Pkg pkg = packageService.queryPackageByOriginal_num(original_num);
		if (pkg == null) {
			msg = "1";
			// 增加异常包裹关联单号的检查
			UnusualPkg unusualPkg = unusualPkgService.queryUnusualPkgByOriginal_num(original_num);
			if (unusualPkg != null) {
				msg = "0";
			}
		}

		result.put("msg", msg);
		return result;
	}

	/**
	 * 跳转到包裹拆分
	 */
	@RequestMapping("/unboxBackpkg")
	public String unboxBackpkg(String pkgid, HttpServletRequest request) {
		int pkgId = Integer.parseInt(pkgid);

		// 查询包裹
		Pkg pkg = packageService.queryPackageById(pkgId);

		// 查询包裹中的商品

		List<PkgGoods> goodsList = pkgGoodsService.selectPkgGoodsByPackageId(pkg.getPackage_id());

		request.setAttribute("original_num", pkg.getOriginal_num());
		request.setAttribute("logistics_code", pkg.getLogistics_code());
		request.setAttribute("goodsList", goodsList);
		request.setAttribute("package_id", pkgId);
		return "/back/disassemble";
	}

	/**
	 * 拆分包裹
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping("/splitBackkg")
	@ResponseBody
	public Map<String, Object> splitBackkg(HttpServletRequest req, HttpServletResponse resp) {
		Map<String, Object> result = new HashMap<String, Object>();
		// // 没有登录用户
		// if (null == req.getSession().getAttribute("frontUser"))
		// {
		// result.put("msg", "2");
		// return result;
		// }
		// 待拆分的包裹id
		String package_idStr = req.getParameter("package_id");

		if (package_idStr == null || "".equals(package_idStr.trim())) {
			result.put("result", false);
			result.put("msg", "3");
			return result;
		}

		int package_id = 0;
		try {
			package_id = Integer.parseInt(package_idStr);
		} catch (Exception e) {
			// logger.error("转换包裹id错误：", e);
			result.put("result", false);
			result.put("msg", "3");
			return result;
		}

		Pkg originalPkg = packageService.queryPackageById(package_id);
		String newPkgs = req.getParameter("newPkgs");

		JSONArray jsonArray = JSONArray.fromObject(newPkgs);
		Iterator it = jsonArray.iterator();

		Timestamp createTime = new Timestamp(System.currentTimeMillis());

		// 新包裹集合
		List<com.xiangrui.lmp.business.homepage.vo.Pkg> newPkgList = new ArrayList<com.xiangrui.lmp.business.homepage.vo.Pkg>();

		while (it.hasNext()) {
			String jsonString = it.next().toString();

			// 新包裹
			com.xiangrui.lmp.business.homepage.vo.Pkg pkg = new com.xiangrui.lmp.business.homepage.vo.Pkg();

			// 新的运单号
			String logistics_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PACKAGE);
			pkg.setLogistics_code(logistics_code);

			pkg.setCreateTime(createTime);

			// json字符串解析获取重新分配后的包裹物品
			List<com.xiangrui.lmp.business.homepage.vo.PkgGoods> pkgGoodsList = JSONTool.getDTOList(jsonString,
					com.xiangrui.lmp.business.homepage.vo.PkgGoods.class);

			pkg.setGoods(pkgGoodsList);
			newPkgList.add(pkg);
		}

		Map<Integer, List<com.xiangrui.lmp.business.homepage.vo.Pkg>> params = new HashMap<Integer, List<com.xiangrui.lmp.business.homepage.vo.Pkg>>();

		params.put(package_id, newPkgList);

		float service_price = getServicePrice(req, AttachService.SPLIT_PKG_ID, originalPkg.getUser_id());

		// 将分箱之后的包裹保存
		frontPkgGoodsService.insertNewDisassemblePkgBack(params, service_price);

		storeService.dealNeedHandleFlag(package_id);
		// 返还冻结扣款金额
		putBackVirtualPay(originalPkg.getUser_id(), originalPkg.getPackage_id());
		result.put("msg", "1");
		return result;
	}

	/**
	 * 查询增值服务费用
	 * 
	 * @param req
	 * @param attach_id
	 * @return
	 */
	private float getServicePrice(HttpServletRequest req, int attach_id, Integer user_id) {

		// 数据库的增值服务信息
		List<AttachService> attachServiceList = attachServiceService.selectAttachService(user_id);
		float service_price = 0;
		if (!attachServiceList.isEmpty()) {
			for (AttachService attachService : attachServiceList) {
				if (attach_id == attachService.getAttach_id()) {
					service_price = attachService.getService_price();
					break;
				}
			}
		}
		return service_price;
	}

	/**
	 * ajax 校验包裹是否能合箱或分箱
	 * 
	 * @param request
	 * @param packageIds
	 * @return
	 */
	@RequestMapping("/packageMergeSplitCheck")
	@ResponseBody
	public Map<String, Object> packageMergeSplitCheck(HttpServletRequest request, String packageIds) {
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		String avaliableFlag = "S";
		if (packageIds != null) {
			String[] packageIdArray = packageIds.split(",");
			if (packageIdArray != null && packageIdArray.length > 0) {
				for (String packageId : packageIdArray) {
					if (!checkCanMergeSplitPackage(packageId)) {
						avaliableFlag = "F";
					}
				}
			}
		}
		rtnMap.put("result", avaliableFlag);
		return rtnMap;
	}

	private boolean checkCanMergeSplitPackage(String packageId) {
		boolean canMergeSplit = true;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("package_id", packageId);
		List<PkgAttachServiceGroup> pkgAttachServiceGroupList = pkgAttachServiceService
				.queryPkgAttachServiceGroup(params);
		if (pkgAttachServiceGroupList != null && pkgAttachServiceGroupList.size() > 0) {
			for (PkgAttachServiceGroup pkgAttachServiceGroup : pkgAttachServiceGroupList) {
				if (pkgAttachServiceGroup.getAttach_ids().contains("" + BaseAttachService.MERGE_PKG_ID)
						|| pkgAttachServiceGroup.getAttach_ids().contains("" + BaseAttachService.SPLIT_PKG_ID)) {
					canMergeSplit = false;
				}
			}
		}
		Pkg pkg = packageService.queryPackageById(Integer.parseInt(packageId));
		if (pkg != null) {
			if ("Y".equals(pkg.getWaitting_merge_flag())) {
				canMergeSplit = false;
			}
		}
		return canMergeSplit;
	}

	/**
	 * 计算收费重量
	 * 
	 * @param userType
	 * @param actualWeight
	 * @return
	 */
	public static float calculateFeeWeith(int userType, float actualWeight) {
		float result = 0;
		if (userType == 1) {
			result = 1;
			if (actualWeight > 1) {
				BigDecimal bigDecimal = new BigDecimal(actualWeight);
				bigDecimal = bigDecimal.divide(new BigDecimal(1), 1, BigDecimal.ROUND_UP);
				result = bigDecimal.floatValue();
			}
		} else if (userType == 2) {
			result = 2;
			if (actualWeight >= 2.1f) {
				int baseInt = (int) actualWeight;
				float base_count = (float) baseInt;
				if (actualWeight < (base_count + 0.1f)) {
					result = base_count;
				} else if (actualWeight >= (base_count + 0.1f) && actualWeight < (base_count + 0.6f)) {
					result = base_count + (float) 0.5;
				} else if (actualWeight >= (base_count + 0.6f) && actualWeight < (base_count + 1.1f)) {
					result = base_count + (float) 1.0;
				}
			}
		}
		return result;
	}

	/**
	 * 返还冻结扣款金额
	 * 
	 * @param user_id
	 * @param package_id
	 */
	private void putBackVirtualPay(Integer user_id, Integer package_id) {
		FrontUser frontUser = frontUserService.queryFrontUserById(user_id);
		Pkg pkgTemp = packageService.queryPackageById(package_id);
		if (pkgTemp.getVirtual_pay() > 0.00001f) {
			// 返还冻结扣款金额
			frontUser.setAble_balance(frontUser.getAble_balance() + pkgTemp.getVirtual_pay());
			frontUser.setFrozen_balance(frontUser.getFrozen_balance() - pkgTemp.getVirtual_pay());
			frontUserMapper.updateBalance(frontUser);
			pkgTemp.setVirtual_pay(-1);
			this.packageService.update(pkgTemp, null);
		}
	}

	/**
	 * ajax 校验包裹是否能合箱或分箱
	 * 
	 * @param request
	 * @param packageIds
	 * @return
	 */
	@RequestMapping("/updateNeedCheckOutFlag")
	@ResponseBody
	public Map<String, Object> updateNeedCheckOutFlag(HttpServletRequest request, String need_check_out_flag,
			Integer package_id) {
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		String avaliableFlag = "S";
		packageService.updateNeedCheckOutFlag(need_check_out_flag, package_id);
		rtnMap.put("result", avaliableFlag);
		return rtnMap;
	}
	
	/**
	 * 取未支付的包裹
	 * @param request
	 * @param originalNum
	 * @return
	 */
	@RequestMapping("/getPkgByOriginalNumUnpaid")
	@ResponseBody
	public Map<String, Object> getPkgByOriginalNumUnpaid(HttpServletRequest request, String originalNum) {
		Map<String, Object> rtnMap = new HashMap<String, Object>();
//		Pkg pkg = packageService.queryPackageByOriginal_numEx(originalNum);
		List<Pkg> pList = packageService.queryPackageByCodeUser(originalNum);
		if( null==pList || pList.size()==0 ){
			rtnMap.put("msg", "单号 "+originalNum+" 不存在");
			rtnMap.put("result", false);
			return rtnMap;
		}
		Pkg pkg = pList.get(0);
		if( null==pkg ){
			rtnMap.put("msg", "单号 "+originalNum+" 不存在");
		}
		else if( 0!=pkg.getExpress_package() ){
			rtnMap.put("msg", "只有默认渠道才能现金支付！");
		}
//		else if( Pkg.PAYMENT_UNPAID==pkg.getPay_status() ){// 未支付
		else if( Pkg.PAYMENT_FREIGHT_UNPAID==pkg.getPay_status_freight() ){// 未支付
			rtnMap.put("pkg", pkg);
			rtnMap.put("result", true);
			return rtnMap;
		}
//		else if( Pkg.PAYMENT_CUSTOM_UNPAID==pkg.getPay_status() ){// 关税未支付
		if( Pkg.PAYMENT_CUSTOM_UNPAID==pkg.getPay_status_custom() ){// 关税未支付
			rtnMap.put("msg", "包裹状态为：关税未支付，不能现金支付！");
		}
//		else if( Pkg.PAYMENT_PAID==pkg.getPay_status() ){// 已支付
		else if( Pkg.PAYMENT_FREIGHT_PAID==pkg.getPay_status_freight() ){// 已支付
			rtnMap.put("msg", "包裹状态为：已支付，不需要再支付！");
		}
		else {
			if( Status.waitIN.getIndex()==pkg.getStatus() ){
				rtnMap.put("msg", "包裹还未入库，请先入库！");
			}
			else
				rtnMap.put("msg", "包裹状态为：关税已支付，不需要再支付！");
		}
		rtnMap.put("result", false);
		return rtnMap;
	}
	
	/**
	 * 现金支付
	 * @param request
	 * @param logisticsCodes 以逗号分隔的多个 logistics_code
	 * @return
	 */
	@RequestMapping("/payByCash")
	@ResponseBody
	public Map<String, Object> payByCash(HttpServletRequest request, String logisticsCodes, String transportCost, String taxes) {
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		
		rtnMap.put("result", packageService.updatePayment(transportCost, logisticsCodes, taxes));
		return rtnMap;
	}
	
	/**
	 * 现金充值
	 * @param request
	 * @return
	 */
	@RequestMapping("/rechargeByCash")
	@ResponseBody
	public Map<String, Object> rechargeByCash(HttpServletRequest request, String chargeMoney, String login_account){
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		Integer uId = 0;
		if( null!=user )
			uId = user.getUser_id();
		
		rtnMap.put("result", packageService.cashRecharge(chargeMoney,login_account, uId));
		return rtnMap;
	}
}
