package com.xiangrui.lmp.business.admin.pkg.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.pkg.service.PackageImgService;
import com.xiangrui.lmp.business.admin.pkg.vo.PackageImg;

@Controller
@RequestMapping("/admin/pkgimg")
public class PackageImgController
{

	@Autowired
	PackageImgService packageImgService;

	/**
	 * 删除图片
	 * 
	 * @return
	 */
	@RequestMapping("/delImg")
	@ResponseBody
	public Map<String, Object> delImg(HttpServletRequest request)
	{
		PackageImg img = new PackageImg();

		img.setImg_id(Integer.parseInt(request.getParameter("img_id")));
		String imgPath = request.getParameter("imgPath");
		int cnt = packageImgService.delImg(img);
		Map<String, Object> msgMap = new HashMap<String, Object>();
		if (cnt > 0)
		{
			String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource";
			String filePath = ctxPath + imgPath.replace("/", File.separator);
			File file = new File(filePath);

			// 如果文件存在则删除
			if (file.exists() && file.isFile())
			{
				file.delete();
			}
			msgMap.put("result", true);
			return msgMap;
		}
		else
		{
			msgMap.put("msg", "删除失败");
			return msgMap;
		}
	}

	/**
	 * 初始化上传拍照
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/initUploadAttachPic")
	public String initUploadAttachPic(HttpServletRequest request)
	{

		request.setAttribute("package_id", request.getParameter("package_id"));
		request.setAttribute("logistics_code", request.getParameter("logistics_code"));
		return "back/upfile";
	}

	/**
	 * 上传拍照
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/uploadAttachPic")
	public String attachPictrue(HttpServletRequest request, HttpServletResponse response)
	{

		// String responseStr = "";
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

		// 获取前台传值
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();

		// 存储路径
		String configPath = File.separator + "upload" + File.separator + "pictrue" + File.separator;

		// 图片路径
		String imgPath = "/upload/pictrue/";

		String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource";

		// 图片存储路径
		ctxPath += configPath;
		// 获取包裹ID
		String package_idStr = request.getParameter("package_id");
		String logistics_code = request.getParameter("logistics_code");

		int package_id = Integer.parseInt(package_idStr);

		// 创建文件夹
		File file = new File(ctxPath);
		if (!file.exists())
		{
			file.mkdirs();
		}

		String fileName = null;

		String imagePathArrayStr = "";
		String imageIdArrayStr = "";
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet())
		{

			MultipartFile mf = entity.getValue();

			fileName = mf.getOriginalFilename();
			// 图片格式
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

			String newFileName = logistics_code + "_" + System.currentTimeMillis() + "." + fileExt;

			File uploadFile = new File(ctxPath + newFileName);
			try
			{
				FileCopyUtils.copy(mf.getBytes(), uploadFile);
			} catch (IOException e)
			{
				imgPath = "上传失败";
				e.printStackTrace();
			}
			imgPath = imgPath + newFileName;

			PackageImg packageImg = new PackageImg();
			packageImg.setPackage_id(package_id);
			packageImg.setImg_type(PackageImg.IMG_PICTRUE_TYPE);
			packageImg.setImg_path(imgPath);
			packageImgService.insertPackageImg(packageImg);
			imagePathArrayStr += imgPath + ",";
			imageIdArrayStr += packageImg.getImg_id() + ",";
		}
		imagePathArrayStr=StringUtils.substringBeforeLast(imagePathArrayStr, ",");
		imageIdArrayStr=StringUtils.substringBeforeLast(imageIdArrayStr, ",");
		request.setAttribute("path", imagePathArrayStr);
		request.setAttribute("img_id", imageIdArrayStr);
		request.setAttribute("package_id", package_idStr);
		request.setAttribute("logistics_code", logistics_code);
		return "back/upfile";
	}
	
	
	/**
	 * 上传拍照
	 * 
	 * @param request
	 * @return
	 * @throws IOException 
	 */
    @RequestMapping(value="/attachPictrueMore", method=RequestMethod.POST)
	public void attachPictrueMore(HttpServletRequest request, HttpServletResponse response) throws IOException
	{

		String responseStr = "";
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

		// 获取前台传值
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();

		// 存储路径
		String configPath = File.separator + "upload" + File.separator + "pictrue" + File.separator;

		// 图片路径
		String imgPath = "/upload/pictrue/";

		String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource";

		// 图片存储路径
		ctxPath += configPath;
		// 获取包裹ID
		String package_idStr = request.getParameter("package_id");
		String logistics_code = request.getParameter("logistics_code");

		int package_id = Integer.parseInt(package_idStr);

		// 创建文件夹
		File file = new File(ctxPath);
		if (!file.exists())
		{
			file.mkdirs();
		}

		String fileName = null;

		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet())
		{

			MultipartFile mf = entity.getValue();

			fileName = mf.getOriginalFilename();
			// 图片格式
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

			String newFileName = logistics_code + "_" + System.currentTimeMillis() + "." + fileExt;

			File uploadFile = new File(ctxPath + newFileName);
			try
			{
				FileCopyUtils.copy(mf.getBytes(), uploadFile);
			} catch (IOException e)
			{
				imgPath = "上传失败";
				e.printStackTrace();
			}
			imgPath = imgPath + newFileName;

			PackageImg packageImg = new PackageImg();
			packageImg.setPackage_id(package_id);
			packageImg.setImg_type(PackageImg.IMG_PICTRUE_TYPE);
			packageImg.setImg_path(imgPath);
			packageImgService.insertPackageImg(packageImg);
			responseStr += imgPath +"-"+packageImg.getImg_id()+ ",";
		}
		responseStr=StringUtils.substringBeforeLast(responseStr, ",");
		
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        
        // 这句话的意思，是告诉servlet用UTF-8转码，而不是用默认的ISO8859
        response.setCharacterEncoding("UTF-8");
        response.getWriter().append(responseStr);
	}
}
