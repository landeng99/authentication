package com.xiangrui.lmp.business.admin.pkg.vo;

import java.util.List;

public class PackageGoods
{

    /**
     * 包裹
     */
    private Pkg pkg;

    /**
     * 商品列表
     */
    private List<PkgGoods> goodList;

    public Pkg getPkg()
    {
        return pkg;
    }

    public void setPkg(Pkg pkg)
    {
        this.pkg = pkg;
    }

    public List<PkgGoods> getGoodList()
    {
        return goodList;
    }

    public void setGoodList(List<PkgGoods> goodList)
    {
        this.goodList = goodList;
    }

}
