package com.xiangrui.lmp.business.admin.pkg.vo;

public class PkgAttachServiceGroup
{

    /**
     * 原始包裹的包裹id
     */
    private String og_package_id_group;
    /**
     * 原始包裹
     */
    private String og_logistics_codes;
    
    /**
     * 原包裹关联单号
     */
    private String og_original_nums;

    /**
     * 目标包裹
     */
    private String tgt_logistics_codes;

    /**
     * 增值服务名称
     */
    private String service_names;
    
    /**
     * 增值服务ID
     */
    private String attach_ids;

    /**
     * 包裹id
     */
    private String tgt_package_id_group;
    
    /**
     * 操作状态
     */
    private String opt_status_str;
    
    /**
     * 状态
     */
    private int status;
    
    /**
     * 新包裹创建时间
     */
    private String createTime;
    
    /**
     * 可增值服务信息描述
     */
    private String serviceableDesc;
    
    private  boolean serviceable;
    
    //包裹增值服务描述
    private String pkgDescription;
    

	public String getPkgDescription()
	{
		return pkgDescription;
	}

	public void setPkgDescription(String pkgDescription)
	{
		this.pkgDescription = pkgDescription;
	}

	public String getOg_package_id_group()
    {
        return og_package_id_group;
    }

    public void setOg_package_id_group(String og_package_id_group)
    {
        this.og_package_id_group = og_package_id_group;
    }

    public String getOg_logistics_codes()
    {
        return og_logistics_codes;
    }

    public void setOg_logistics_codes(String og_logistics_codes)
    {
        this.og_logistics_codes = og_logistics_codes;
    }

    public String getOg_original_nums()
    {
        return og_original_nums;
    }

    public void setOg_original_nums(String og_original_nums)
    {
        this.og_original_nums = og_original_nums;
    }

    public String getTgt_logistics_codes()
    {
        return tgt_logistics_codes;
    }

    public void setTgt_logistics_codes(String tgt_logistics_codes)
    {
        this.tgt_logistics_codes = tgt_logistics_codes;
    }

    public String getService_names()
    {
        return service_names;
    }

    public void setService_names(String service_names)
    {
        this.service_names = service_names;
    }

    public String getTgt_package_id_group()
    {
        return tgt_package_id_group;
    }

    public void setTgt_package_id_group(String tgt_package_id_group)
    {
        this.tgt_package_id_group = tgt_package_id_group;
    }

    public String getOpt_status_str()
    {
        return opt_status_str;
    }

    public void setOpt_status_str(String opt_status_str)
    {
        this.opt_status_str = opt_status_str;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }

    public String getServiceableDesc()
    {
        return serviceableDesc;
    }

    public void setServiceableDesc(String serviceableDesc)
    {
        this.serviceableDesc = serviceableDesc;
    }

    public boolean isServiceable()
    {
        return serviceable;
    }

    public void setServiceable(boolean serviceable)
    {
        this.serviceable = serviceable;
    }

	public String getAttach_ids()
	{
		return attach_ids;
	}

	public void setAttach_ids(String attach_ids)
	{
		this.attach_ids = attach_ids;
	}
    
}
