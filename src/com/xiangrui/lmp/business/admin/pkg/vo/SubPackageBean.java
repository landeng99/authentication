package com.xiangrui.lmp.business.admin.pkg.vo;

public class SubPackageBean
{

	/**
	 * 原关联单号
	 */
	private String oldOriginal_num;

	/**
	 * 包裹到库状态
	 */
	private int arriveStatus;

	public String getOldOriginal_num()
	{
		return oldOriginal_num;
	}

	public void setOldOriginal_num(String oldOriginal_num)
	{
		this.oldOriginal_num = oldOriginal_num;
	}

	public int getArriveStatus()
	{
		return arriveStatus;
	}

	public void setArriveStatus(int arriveStatus)
	{
		this.arriveStatus = arriveStatus;
	}

}
