package com.xiangrui.lmp.business.admin.pkg.vo;

import  com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;
import  com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
public class PackPrint 
{

    /**
     * 批量打印面单 VO
     */ 
    //包裹的商品信息
    private String goodsname;
    //包裹的商品数量
    private int goodscount;
    //包裹详情： 关联单号：
    private Pkg pkgDetail; 
	//收件人 信息
    private UserAddress userAddress;
    //收件人地址
    private String address;
    //序列
    private int pageorder;
 
    
    
   
	public int getPageorder() {
		return pageorder;
	}
	public void setPageorder(int pageorder) {
		this.pageorder = pageorder;
	}
	public String getGoodsname() {
		return goodsname;
	}
	public void setGoodsname(String goodsname) {
		this.goodsname = goodsname;
	}
	public int getGoodscount() {
		return goodscount;
	}
	public void setGoodscount(int goodscount) {
		this.goodscount = goodscount;
	}
 
	public UserAddress getUserAddress() {
		return userAddress;
	}
	public void setUserAddress(UserAddress userAddress) {
		this.userAddress = userAddress;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Pkg getPkgDetail() {
		return pkgDetail;
	}
	public void setPkgDetail(Pkg pkgDetail) {
		this.pkgDetail = pkgDetail;
	}
  
    
}
