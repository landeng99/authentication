package com.xiangrui.lmp.business.admin.pkg.vo;

import java.sql.Timestamp;

import com.xiangrui.lmp.business.base.BasePackage;

public class PkgOperate extends BasePackage
{
	private static final long serialVersionUID = 1L;
	
	private String last_name;
	/**
	 * 内件明细
	 */
	private String goods_name;
	/**
	 * 增值服务
	 */
	private String attach_service;
	/**
	 * 增值服务ID
	 */
	private String attach_id;
	/**
	 * 物流单号
	 */
	private String express_num;
	/**
	 * 收件人
	 */
	private String receiver;

	/**
	 * 入库时间
	 */
	private Timestamp input_time;
	/**
	 * 分箱数
	 */
	private int splitCount;
	/**
	 * 合箱数
	 */
	private int mergeCount;

	/**
	 * 合箱后的packageId
	 */
	private String afterMergeLogistics_code;

	/**
	 * 显示行color
	 */
	private int displayColor;

	private int expressNumCount;

	/**
	 * 身份证
	 */
	private String idcard;
	/**
	 * 身份证照片
	 */
	private String idcardImg;
	/**
	 * 分箱前的package Id
	 */
	private int splitPackage_id;
	
	/**
	 * 分箱包裹中有没有入库的需要用红包标注
	 */
	private String showSplitNotInputRed;
	
	/**
	 * 渠道字符串
	 */
	private String express_package_str;
	/**
	 * 包裹状态字符串
	 */
	private String status_str;
	
	//省市字符串
	private String region;
	
	//详细地址
	private String street;
	
	//邮政编码
	private String postal_code;
	
	//收件人手机号码
	private String receiver_mobile;
	
	//发送人
	private String user_name;
	
	//发送人手机号码
	private String send_mobile;
	
	//业务名称
	private String business_name;
	
	//商品详情
	private String goods_detail;
	
	//商品数量
	private String quantity;
	
	//商品单位
	private String unit;
	
	//商品品牌
	private String brand;
	
	//商品价格
	private float price;
	
	//规格
	private String spec;
	
	//商品名称
	private String goods_names;
	
	//收件人地址
	private String send_address;
	
	//报关单号
	private String declaration_code;
	
	//保费
	private float keep_price;
	
	//审核状态
	private int idcardStatus;
	
	//省份
	private String province;
	
	//城市
	private String city;
	
	//申报类别
	private String name_specs;
	
	public String getExpress_package_str()
	{
		return express_package_str;
	}


	public void setExpress_package_str(String express_package_str)
	{
		this.express_package_str = express_package_str;
	}


	public String getStatus_str()
	{
		return status_str;
	}


	public void setStatus_str(String status_str)
	{
		this.status_str = status_str;
	}


	@Override
	public String toString()
	{
		return "BasePackage [package_id=" + super.getPackage_id() + ", user_id=" + super.getUser_id() + ", original_num=" + super.getOriginal_num() + ", input_time=" + input_time + ", last_name=" + last_name + ", logistics_code=" + super.getLogistics_code() + ", status=" + super.getStatus() + ", goods_name=" + goods_name + ", actual_weight=" + super.getActual_weight() + ", receiver=" + receiver + ", idcard=" + idcard + ", idcardImg=" + idcardImg
				+ ", express_package=" + super.getExpress_package()+ "]";
	}
	public String getShowSplitNotInputRed()
	{
		return showSplitNotInputRed;
	}

	public void setShowSplitNotInputRed(String showSplitNotInputRed)
	{
		this.showSplitNotInputRed = showSplitNotInputRed;
	}

	public int getSplitPackage_id()
	{
		return splitPackage_id;
	}

	public void setSplitPackage_id(int splitPackage_id)
	{
		this.splitPackage_id = splitPackage_id;
	}

	public String getIdcard()
	{
		return idcard;
	}

	public void setIdcard(String idcard)
	{
		this.idcard = idcard;
	}

	public String getIdcardImg()
	{
		return idcardImg;
	}

	public void setIdcardImg(String idcardImg)
	{
		this.idcardImg = idcardImg;
	}

	public int getExpressNumCount()
	{
		return expressNumCount;
	}

	public void setExpressNumCount(int expressNumCount)
	{
		this.expressNumCount = expressNumCount;
	}

	public int getSplitCount()
	{
		return splitCount;
	}

	public void setSplitCount(int splitCount)
	{
		this.splitCount = splitCount;
	}

	public int getMergeCount()
	{
		return mergeCount;
	}

	public void setMergeCount(int mergeCount)
	{
		this.mergeCount = mergeCount;
	}

	public Timestamp getInput_time()
	{
		return input_time;
	}

	public void setInput_time(Timestamp input_time)
	{
		this.input_time = input_time;
	}

	public String getLast_name()
	{
		return last_name;
	}

	public void setLast_name(String last_name)
	{
		this.last_name = last_name;
	}

	public String getGoods_name()
	{
		return goods_name;
	}

	public void setGoods_name(String goods_name)
	{
		this.goods_name = goods_name;
	}

	public String getAttach_service()
	{
		return attach_service;
	}

	public void setAttach_service(String attach_service)
	{
		this.attach_service = attach_service;
	}

	public String getAttach_id()
	{
		return attach_id;
	}

	public void setAttach_id(String attach_id)
	{
		this.attach_id = attach_id;
	}

	public String getExpress_num()
	{
		return express_num;
	}

	public void setExpress_num(String express_num)
	{
		this.express_num = express_num;
	}

	public String getReceiver()
	{
		return receiver;
	}

	public void setReceiver(String receiver)
	{
		this.receiver = receiver;
	}

	public String getAfterMergeLogistics_code()
	{
		return afterMergeLogistics_code;
	}

	public void setAfterMergeLogistics_code(String afterMergeLogistics_code)
	{
		this.afterMergeLogistics_code = afterMergeLogistics_code;
	}

	public int getDisplayColor()
	{
		return displayColor;
	}

	public void setDisplayColor(int displayColor)
	{
		this.displayColor = displayColor;
	}


	public String getRegion()
	{
		return region;
	}


	public void setRegion(String region)
	{
		this.region = region;
	}


	public String getStreet()
	{
		return street;
	}


	public void setStreet(String street)
	{
		this.street = street;
	}


	public String getPostal_code()
	{
		return postal_code;
	}


	public void setPostal_code(String postal_code)
	{
		this.postal_code = postal_code;
	}


	public String getReceiver_mobile()
	{
		return receiver_mobile;
	}


	public void setReceiver_mobile(String receiver_mobile)
	{
		this.receiver_mobile = receiver_mobile;
	}


	public String getUser_name()
	{
		return user_name;
	}


	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}


	public String getSend_mobile()
	{
		return send_mobile;
	}


	public void setSend_mobile(String send_mobile)
	{
		this.send_mobile = send_mobile;
	}


	public String getBusiness_name()
	{
		return business_name;
	}


	public void setBusiness_name(String business_name)
	{
		this.business_name = business_name;
	}


	public String getGoods_detail()
	{
		return goods_detail;
	}


	public void setGoods_detail(String goods_detail)
	{
		this.goods_detail = goods_detail;
	}


	public String getQuantity()
	{
		return quantity;
	}


	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}


	public String getUnit()
	{
		return unit;
	}


	public void setUnit(String unit)
	{
		this.unit = unit;
	}


	public String getBrand()
	{
		return brand;
	}


	public void setBrand(String brand)
	{
		this.brand = brand;
	}


	public float getPrice()
	{
		return price;
	}


	public void setPrice(float price)
	{
		this.price = price;
	}


	public String getSpec()
	{
		return spec;
	}


	public void setSpec(String spec)
	{
		this.spec = spec;
	}


	public String getGoods_names()
	{
		return goods_names;
	}


	public void setGoods_names(String goods_names)
	{
		this.goods_names = goods_names;
	}


	public String getSend_address()
	{
		return send_address;
	}


	public void setSend_address(String send_address)
	{
		this.send_address = send_address;
	}


	public String getDeclaration_code()
	{
		return declaration_code;
	}


	public void setDeclaration_code(String declaration_code)
	{
		this.declaration_code = declaration_code;
	}


	public float getKeep_price()
	{
		return keep_price;
	}


	public void setKeep_price(float keep_price)
	{
		this.keep_price = keep_price;
	}


	public int getIdcardStatus()
	{
		return idcardStatus;
	}


	public void setIdcardStatus(int idcardStatus)
	{
		this.idcardStatus = idcardStatus;
	}


	public String getProvince()
	{
		return province;
	}


	public void setProvince(String province)
	{
		this.province = province;
	}


	public String getCity()
	{
		return city;
	}


	public void setCity(String city)
	{
		this.city = city;
	}


	public String getName_specs()
	{
		return name_specs;
	}


	public void setName_specs(String name_specs)
	{
		this.name_specs = name_specs;
	}
	

}
