package com.xiangrui.lmp.business.admin.pkg.vo;

import java.sql.Timestamp;
import java.util.List;

import com.xiangrui.lmp.business.base.BasePackage;

public class Pkg extends BasePackage
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 包裹创建用户
     */
    private String user_name;

    /**
     * 收件人
     */
    private String receiver;

    /**
     * 海外地址的城市名称
     */
    private String city;

    /**
     * 海外仓库名
     */
    private String warehouse;
    
    /**
     * 地址
     */
    private String street;

    private String mobile;

    private String idcard;

    private String region;

    private String address;

    private String email;

    /**
     * 状态描述信息
     */
    private String statusDesc;
    
    
    private List<PkgGoods> goodsList;

    private List<PkgAttachService> pkgAttachServiceList;
    /**
     * 总增值服务费
     */
    private float totalServicePrice;
    /**
     * 包裹图片信息
     */
    List<PackageImg> packageImgList;
    
    /**
   	 * 快递公司编码
   	 */
       private String com;
    

	/**
     * 包裹所属用户类型
     */
    private int userType;

    /**
     * 报关号码
     */
    private String declaration_code;
    
    /**
     * 提单编号
     */
    private String pick_code;
	/**
	 * 入库时间
	 */
	private Timestamp input_time;
	
	/**
	 * 口岸名称
	 */
	private String seaport_name;
	
	/**
	 * 包裹所示帐户账号（同行客户包裹付款邮件需求用到）
	 */
	private String account;
	/**
	 * 包裹个数统计（同行客户包裹付款邮件需求用到）
	 */
	private int package_count;
	/**
	 * 包裹所需支付统计（同行客户包裹付款邮件需求用到）
	 */
	private float need_pay_money;
	
	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}

	public int getPackage_count()
	{
		return package_count;
	}

	public void setPackage_count(int package_count)
	{
		this.package_count = package_count;
	}

	public float getNeed_pay_money()
	{
		return need_pay_money;
	}

	public void setNeed_pay_money(float need_pay_money)
	{
		this.need_pay_money = need_pay_money;
	}
  public String getCom() {
		return com;
	}

	public void setCom(String com) {
		this.com = com;
	}

	public String getSeaport_name()
	{
		return seaport_name;
	}

	public void setSeaport_name(String seaport_name)
	{
		this.seaport_name = seaport_name;
	}

	public Timestamp getInput_time()
	{
		return input_time;
	}

	public void setInput_time(Timestamp input_time)
	{
		this.input_time = input_time;
	}

	public String getDeclaration_code()
	{
		return declaration_code;
	}

	public void setDeclaration_code(String declaration_code)
	{
		this.declaration_code = declaration_code;
	}

	public String getPick_code()
	{
		return pick_code;
	}

	public void setPick_code(String pick_code)
	{
		this.pick_code = pick_code;
	}

	public int getUserType()
	{
		return userType;
	}

	public void setUserType(int userType)
	{
		this.userType = userType;
	}

	public String getWarehouse()
    {
        return warehouse;
    }

    public void setWarehouse(String warehouse)
    {
        this.warehouse = warehouse;
    }



    public String getUser_name()
    {
        return user_name;
    }

    public void setUser_name(String user_name)
    {
        this.user_name = user_name;
    }

    public String getReceiver()
    {
        return receiver;
    }

    public void setReceiver(String receiver)
    {
        this.receiver = receiver;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getIdcard()
    {
        return idcard;
    }

    public void setIdcard(String idcard)
    {
        this.idcard = idcard;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }
    
  

    public List<PkgGoods> getGoodsList()
    {
        return goodsList;
    }

    public void setGoodsList(List<PkgGoods> goodsList)
    {
        this.goodsList = goodsList;
    }

    public List<PkgAttachService> getPkgAttachServiceList()
    {
        return pkgAttachServiceList;
    }

    public void setPkgAttachServiceList(
            List<PkgAttachService> pkgAttachServiceList)
    {
        this.pkgAttachServiceList = pkgAttachServiceList;
    }

    public String getStatusDesc()
    {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc)
    {
        this.statusDesc = statusDesc;
    }

    public float getTotalServicePrice()
	{
		return totalServicePrice;
	}

	public void setTotalServicePrice(float totalServicePrice)
	{
		this.totalServicePrice = totalServicePrice;
	}

	public List<PackageImg> getPackageImgList()
    {
        return packageImgList;
    }

    public void setPackageImgList(List<PackageImg> packageImgList)
    {
        this.packageImgList = packageImgList;
    }

    public void setStatus(int status)
    {
        switch (status)
        {
        case LOGISTICS_STORE_WAITING:
            this.statusDesc = "待入库";break;
        case LOGISTICS_UNUSUALLY:
            this.statusDesc = "包裹异常";break;
        case LOGISTICS_STORAGED:
            this.statusDesc = "已入库";break;
        case LOGISTICS_SENT_ALREADY:
            this.statusDesc="已出库";break;    
        case LOGISTICS_SEND_WAITING:
            this.statusDesc = "待发货";break;
        case LOGISTICS_AIRLIFT_ALREADY:
            this.statusDesc = "已空运";break;
        case LOGISTICS_CUSTOMS_WAITING:
            this.statusDesc = "待清关";break;
        case LOGISTICS_CUSTOMS_ALREADY:
            this.statusDesc = "已清关派件中";break;
        case LOGISTICS_SIGN_IN:
            this.statusDesc = "已签收";break;
        case LOGISTICS_DISCARD:
            this.statusDesc = "废弃";break;
        case LOGISTICS_RETURN:
            this.statusDesc = "退货";break;
        }

        super.setStatus(status);
    }
}
