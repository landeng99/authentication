package com.xiangrui.lmp.business.admin.pkg.vo;

import com.xiangrui.lmp.business.base.BaseUserAddress;

public class UserAddress extends BaseUserAddress
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String logistics_code;

    public String getLogistics_code()
    {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }
    
}
