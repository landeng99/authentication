package com.xiangrui.lmp.business.admin.pkg.vo;

import com.xiangrui.lmp.business.base.BasePkgAttachService;

public class PkgAttachService extends BasePkgAttachService
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 物流跟踪号
     */
    private String logistics_code;

    /**
     * 增值服务名称
     */
    private String service_name;


    public String getLogistics_code()
    {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }

    public String getService_name()
    {
        return service_name;
    }

    public void setService_name(String service_name)
    {
        this.service_name = service_name;
    }


}