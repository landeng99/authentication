package com.xiangrui.lmp.business.admin.pkg.vo;

import com.xiangrui.lmp.business.base.BaseUnusualPkg;

public class UnusualPkg extends BaseUnusualPkg
{
	private String username;
	private String exception_package_flag;
	private String doneOrder;

	public String getDoneOrder()
	{
		return doneOrder;
	}

	public void setDoneOrder(String doneOrder)
	{
		this.doneOrder = doneOrder;
	}

	public String getException_package_flag()
	{
		return exception_package_flag;
	}

	public void setException_package_flag(String exception_package_flag)
	{
		this.exception_package_flag = exception_package_flag;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

}
