package com.xiangrui.lmp.business.admin.pkg.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;

public interface PackageService
{

    /**
     * 分页查询包裹列表
     * 
     * @param params
     * @return
     */
    List<Pkg> queryAll(Map<String, Object> params);
    
    /**
     * 货量统计查询
     * 
     * @param params
     * @return
     */
    List<Pkg> queryGoodsCountAll(Map<String, Object> params);
    
    
    /**
     * 货量统计导出查询
     * 
     * @param packageIdList
     * @return
     */
    List<Pkg> queryPackageByIdList(List<String> packageIdList);

    /**
     * 海外退运包裹 分页查询包裹列表
     * 
     * @param params
     * @return
     */
    List<Pkg> queryAllReturn(Map<String, Object> params);

    /**
     * 查询总记录数
     * 
     * @param role
     * @return
     */
    int queryCount();

    /**
     * 通过ID查询包裹
     * 
     * @param package_id
     * @return
     */
    Pkg queryPackageById(int package_id);

    List<Pkg> queryPackageByIdString(String groupPackageId);
    /**
     * 查询异常包裹
     * 
     * @param params
     * @return
     */
    List<Pkg> queryAllAbnormalPkg(Map<String, Object> params);

    /**
     * 仓库管理入库管理扫描入库更新包裹
     * 
     * @param pkg
     * @return
     */
    void update(Pkg pkg, User user);
    

    /**
     * 关联包裹用户
     * 
     * @param pkg
     * @return
     */
    void updateUserId(Pkg pkg, User user);

    /**
     * 退运包裹 审批
     * 
     * @param pkg
     * @return
     */
    void updateReturnCost(Map<String, Object> paramMap);

    /**
     * 创建包裹
     * 
     * @param pkg
     * @return
     */
    int insertPkg(Pkg pkg, User user);

    /**
     * Excel导入国内运单信息
     * 
     * @param list
     * @return
     */
    void batchUpdatePkg(List<Pkg> list,List<ExpressInfo> expressInfoList, User user);

    /**
     * 国内运单查询
     * 
     * @param params
     * @return
     */
    List<Pkg> queryAlllogisticsCode(Map<String, Object> params);

    /**
     * 
     * 创建托盘包裹查询
     * 
     * @param params
     * @return
     */
    List<Pkg> queryForAddPallet(Map<String, Object> params);

    /**
     * 用户下所有包裹
     * 
     * @param user_id
     * @return
     */
    List<Pkg> queryPackageAddressByUserId(int user_id);

    /**
     * 多个包裹ID
     * 
     * @param packageIds
     * @return
     */
    List<Pkg> queryPackageByPackageIds(List<String> packageIds);

    /**
     * 通过提单号,找到对应的包裹
     * 
     * @param pick_id
     * @return
     */
    List<Pkg> queryPackageByPick_id(int pick_id);

    /**
     * 按照logistiics_cod查询对应的包裹
     * 
     * @param logistics_code
     * @return
     */
    List<Pkg> queryPackageByLogistics_code(String logistics_code);
    Pkg queryPackageByLogistics_codeOne(String logistics_code);
    
    /**
     * 按照logistiics_cod列表查询对应的包裹
     * 
     * @param logistics_code
     * @return
     */
    List<Pkg> queryPackageByLogistics_codeList(List<String> logistics_codeList);

    /**
     * 按照ems_code查询对应的包裹
     * 
     * @param ems_code
     * @return
     */
    List<Pkg> queryPackageByEms_code(String ems_code);

    /**
     * 快递单号
     * 
     * @param ems_code
     * @return
     */
    Pkg queryPackageByEmsCode(String ems_code);

    /**
     * 更新快递单号
     * 
     * @param params
     * @return
     */
    void updateEmsCode(Map<String, Object> params,User user);

    /**
     * 导入运单查询包裹信息
     * 
     * @param packageList
     * @return
     */
    List<Pkg> queryPackageBylogisticsCodes(List<Pkg> packageList);
    
    
    List<Pkg>  queryForScanAddPallet(Map<String,Object>param);
    
    /**
     * 本次导入公司运单号以外 的 所有 派送单号
     * @param packageList
     * @return
     */
    List<String> queryAllEmsCode(List<Pkg> packageList);

    /**
     * 根据包裹单号查询包裹
     * 
     * @param params
     * @return
     */
    List<Pkg> queryPackageByCode(Map<String, Object> params);
    
    /**
     * 根据自建单号（original_num）或平台单号（logistics_code）查包裹
     * 结果包含 user_name account 等用户信息
     * @param code 单号要完全匹配
     * @return
     */
    List<Pkg> queryPackageByCodeUser(String code);

    /**
     * 快速支付查询
     * 
     * @param params
     * @return
     */
    List<Pkg> selectPackageForFastPayment(Map<String, Object> params);
    
    /**
     * 快速支付
     * 
     * @param pkg
     * @param frontUser
     * @param frontAccountLog
     * @param pkgGoodsList
     * @return
     */
    int taskFastPayment(Pkg pkg,FrontUser frontUser,FrontAccountLog frontAccountLog,List<PkgGoods> pkgGoodsList);
    
    Pkg queryPackageByOriginal_num(String original_num);
    Pkg queryPackageByOriginal_numEx(String original_num);
    
    /**
     * 待处理包裹
     * @param <user_id>
     * @param <express_num>
     * @param <fromDate>
     * @param <toDate>
     * @param <logistics_code>
     * @param <original_num>
     * @param <last_name>
     * @param <user_name>
     * @param params
     * @return
     */
    List<PkgOperate> queryReadyHandlePackage(Map<String, Object> params);
    /**
     * 已入库包裹
     * @param <user_id>
     * @param <express_num>
     * @param <fromDate>
     * @param <toDate>
     * @param <logistics_code>
     * @param <original_num>
     * @param <last_name>
     * @param <user_name>
     * @param params
     * @return
     */
    List<PkgOperate> queryHaveInputedPackage(Map<String, Object> params);
    /**
     * 待发货包裹
     * @param <user_id>
     * @param <express_num>
     * @param <fromDate>
     * @param <toDate>
     * @param <logistics_code>
     * @param <original_num>
     * @param <last_name>
     * @param <user_name>
     * @param params
     * @return
     */
    List<PkgOperate> queryReadyDeliveryPackage(Map<String, Object> params);
    /**
     * 通过包裹ID来查询包裹处理信息
     * @param pacakgeIds
     * @return
     */
    List<PkgOperate> queryPackageHandleInfoByPackageIds(String pacakgeIds);
    
    /**
     * 生成待发货包裹或已入库包裹导出excel
     * @param fileName
     * @param pkgOperateList
     * @return
     */
    XSSFWorkbook createReadyDelivery_HaveInputed(String fileName,List<PkgOperate> pkgOperateList);
    
    /**
     * 生成待处理包裹导出excel
     * @param fileName
     * @param pkgOperateList
     * @return
     */
    XSSFWorkbook createReadyHandle(String fileName,List<PkgOperate> pkgOperateList,boolean needOutPutArriveStatus);
    /**
     * 根据package_id来更新需待合箱处理标志(waitting_merge_flag)
     * @param <package_id>
     * @param <waitting_merge_flag>
     * @return
     */
    int updateWaittingMergePkg(Integer package_id,String waitting_merge_flag);
    
    /**
     * 已到库包裹
     * @param <user_id>
     * @param <express_num>
     * @param <fromDate>
     * @param <toDate>
     * @param <logistics_code>
     * @param <original_num>
     * @param <last_name>
     * @param <user_name>
     * @param params
     * @return
     */
    List<PkgOperate> queryArrivedPackage(Map<String, Object> params);
    
    /**
     * 根据package_id来更新异常包裹标志(exception_package_flag)
     * @param <package_id>
     * @param <exception_package_flag>
     * @return
     */
    int updateExceptionPackageFlag(Integer package_id,String exception_package_flag);
    
    /**
     * 更新优惠券支付金额
     * @param coupan_pay
     * @param logistics_code
     * @return
     */
    int updateCoupan_pay(String logistics_code,float coupan_pay);
    /**
     * 更新包裹需要取出标志
     * @param need_check_out_flag
     * @param package_id
     * @return
     */
    int updateNeedCheckOutFlag(String need_check_out_flag,int package_id);
    
    /**
     * 根据用户ID统计需要预捐款的包裹
     * @param front_user_id
     * @return
     */
    int countNeedVirtualPayPackageByFrontUserId(int front_user_id);
    
    /**
     * 更新余额
     * @param frontUser
     * @return
     */
    void updateBalance(FrontUser frontUser);
    
    List<Pkg>  queryForScanAddPalletNewInit(int output_id);
    
    /**
     * 查询当天需要邮件提醒付款的包裹统计
     * @return
     */
    List<Pkg> queryCurrentDayEmailWarnToPayRecord();


	void operateExecutive(Map<String, Object> map);

	/**
	 * 更新包裹状态
	 * @param parmas
	 */
	void updateStatus(Pkg pkgDetail, User user);

	/**
	 * 已入库包裹初始化查询
	 * @param params
	 * @return
	 */
	List<PkgOperate> queryHaveInputedPackageInit(Map<String, Object> params);

	/**
	 * 待发货包裹导出
	 * @param params
	 * @return
	 */
	List<PkgOperate> queryReadyDeliveryPackageExport(Map<String, Object> params);

	/**
	 * 渠道编辑
	 * @param package_id
	 * @param express_package
	 */
	void updateExpressPackage(Map<String, Object> param);

	/**
	 * 包裹管理包裹列表更新包裹
	 * @param pkg
	 * @param user
	 */
	void updatePackage(Pkg pkg, User user);
	void updatePackage(Pkg pkg, Integer userId);
	
	int updateTax(Pkg pkg, Integer userId);
	

	/**
	 * 现金支付时，更新支付状态
	 * @param transport_cost
	 * @param logisticsCodes
	 * @return 是否成功
	 */
	boolean updatePayment(String transport_cost, String logisticsCodes, String taxes);
	
	boolean cashRecharge(String chargeMoney, String login_account, Integer uId);
	
	/**
	 * 在包裹详细页面更新
	 * @param request
	 * @param address_id 
	 * @param jsonGoods
	 * @param address
	 * @return
	 */
	boolean updateDetail(String address_id, String jsonGoods, String address);

}
