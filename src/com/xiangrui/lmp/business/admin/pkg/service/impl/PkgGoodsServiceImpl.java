package com.xiangrui.lmp.business.admin.pkg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.pkg.mapper.PkgGoodsMapper;
import com.xiangrui.lmp.business.admin.pkg.service.PkgGoodsService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods;




@Service(value = "pkgGoodsService")
public class PkgGoodsServiceImpl implements PkgGoodsService {
    
    @Autowired
    private PkgGoodsMapper pkgGoodsMapper;
    
    /**
     * 查询同一包裹下的所有商品
     * 
     * @param package_id
     * @return List
     */
    @Override
    public List<PkgGoods> selectPkgGoodsByPackageId(int package_id) {
        
        return pkgGoodsMapper.selectPkgGoodsByPackageId(package_id);
    }

	@Override
	public int batchUpdatePkgGoods(List<PkgGoods> list) {
		return pkgGoodsMapper.batchUpdatePkgGoods(list);
	}
    
}
