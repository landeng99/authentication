package com.xiangrui.lmp.business.admin.pkg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.pkg.mapper.CityMapper;
import com.xiangrui.lmp.business.admin.pkg.service.CityService;
import com.xiangrui.lmp.business.admin.pkg.vo.City;

@Service(value = "cityService")
public class CityServiceImpl implements CityService {
    
    @Autowired
    private CityMapper cityMapper;
    
    /**
     * 查询省级城市
     * 
     * @return
     */
    @Override
    public List<City> queryCities() {
        
        return cityMapper.queryCities();
    }
    
    /**
     * 通过城市ID查询城市
     * 
     * @param city_id
     * @return City
     */
    @Override
    public City queryCityByCityId(int city_id) {
        
        return cityMapper.queryCityByCityId(city_id);
    }
    
    /**
     * 查询父级城市下所有的子城市
     * 
     * @param father_id
     * @return List
     */
    @Override
    public List<City> queryCitiesByFatherCity(int father_id) {
        
        return cityMapper.queryCitiesByFatherCity(father_id);
    }
    
}
