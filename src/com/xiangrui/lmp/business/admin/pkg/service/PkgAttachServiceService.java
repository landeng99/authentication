package com.xiangrui.lmp.business.admin.pkg.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.admin.user.vo.User;

public interface PkgAttachServiceService
{

    /**
     * 增值服务
     * 
     * @param params
     * @return
     */
    List<PkgAttachService> queryAllPkgAttachService(Map<String, Object> params);

    /**
     * 更新
     * 
     * @param package_id
     * @return
     */
    int updatePkgAttachServiceAndPackage(Map<String, Object> params,User user);

    /**
     * 包裹所有的增值服务
     * 
     * @param package_id
     * @return
     */
    List<PkgAttachService> queryPkgAttachServiceBypackageId(int package_id);

    /**
     * 查询或者是验证某个指定的包裹,指定的增值服务 信息
     * @param package_id 包裹id
     * @param attach_id 增值服务id
     * @return
     */
    PkgAttachService queryAllPkgAttachServiceById(int package_id,int attach_id);
    
    /**
     * 为包裹添加增值服务
     * @param pkgAttachService
     */
    void addPkgAttachService(PkgAttachService pkgAttachService);
    
    /**
     * 为包裹添加增值服务 添加量的数据
     * @param list
     */
    void addPkgAttachServiceOnList(List<PkgAttachService> list);

    void pkgAttachServiceClear(PkgAttachService pkgAttachService);
    
    /**
     * 计算总增值服务费用用
     * 
     * @param package_id
     * @return
     */
    List<PkgAttachService> queryPkgAttachServiceByPackage(
            int package_id);
    
    List<PkgAttachServiceGroup> queryPkgAttachServiceGroup(
            Map<String, Object> params);

    List<PkgAttachServiceGroup> queryPkgAttachServiceGroupForScanDisplay(
    		int pkgId);
    /**
     * 根据参数查询待分箱包裹的子包裹的增值服务
     * @param params
     * @return
     */
    List<PkgAttachService> queryForSplit(Map<String, Object> params);
    
    /**
     * 更新包裹增值服务
     * @param params
     * @return
     */
    int updatePkgAttachService (Map<String, Object> params, User user);
    
    /**
     * 更新包裹增值服务price
     * @param params
     */
    void updatePkgAttachServicePrice(Map<String, Object> params);
    
    List<PkgAttachService> queryAllPkgAttachServiceByPackageGroup(int package_group, int attach_id);
    
    
}
