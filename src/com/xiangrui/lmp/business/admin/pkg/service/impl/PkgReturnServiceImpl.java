package com.xiangrui.lmp.business.admin.pkg.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgReturnMapper;
import com.xiangrui.lmp.business.admin.pkg.service.PkgReturnService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgReturn;

@Service(value = "pkgReturnService")
public class PkgReturnServiceImpl implements PkgReturnService {
    
    @Autowired
    private PkgReturnMapper pkgReturnMapper;
    
    /**
     * 退运包裹
     * 
     * @return
     */
    @Override
    public PkgReturn queryPkgReturnByPkgId(int pkg_id) {
        
        return pkgReturnMapper.queryPkgReturnByPkgId(pkg_id);
    }
    
    /**
     * 退运包裹 审批
     * 
     * @return
     */
    @SystemServiceLog(description="退运包裹审批")
    public void updateApprove(PkgReturn PkgReturn) {
        
        pkgReturnMapper.updateApprove(PkgReturn);
        
    }
}
