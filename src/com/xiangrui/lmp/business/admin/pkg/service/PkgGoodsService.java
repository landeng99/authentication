package com.xiangrui.lmp.business.admin.pkg.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods;

;

public interface PkgGoodsService
{

    /**
     * 查询同一包裹下的所有商品
     * 
     * @param package_id
     * @return List
     */
    List<PkgGoods> selectPkgGoodsByPackageId(int package_id);

    int batchUpdatePkgGoods(List<PkgGoods> list);
}
