package com.xiangrui.lmp.business.admin.pkg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageImgMapper;
import com.xiangrui.lmp.business.admin.pkg.service.PackageImgService;
import com.xiangrui.lmp.business.admin.pkg.vo.PackageImg;

@Service(value = "packageImgService")
public class PackageImgServiceImpl implements PackageImgService {
    
    @Autowired
    private PackageImgMapper packageImgMapper;
    
    /**
     * 查询包裹的图片
     * @param packageImg
     * @return
     */
    @Override
    public List<PackageImg> queryPackageImgList(PackageImg packageImg)
    {
        return packageImgMapper.queryPackageImgList(packageImg);
    }
    

    /**
     * 图片查询 (包裹ID 和图片类型)
     * 
     * @return
     */
    @Override
    public PackageImg queryPackageImg(PackageImg packageImg) {
        
        return packageImgMapper.queryPackageImg(packageImg);
    }
    
    /**
     * 图片插入
     * 
     * @return
     */
    @SystemServiceLog(description="新增图片")
    @Override
    public int insertPackageImg(PackageImg packageImg) {
        
       return packageImgMapper.insertPackageImg(packageImg);
    }
    
    /**
     * 更新图片地址
     * 
     * @return
     */
    @SystemServiceLog(description="修改图片")
    @Override
    public void updateImgPath(PackageImg packageImg) {
        
        packageImgMapper.updateImgPath(packageImg);
    }
    /**
     * 删除图片
     * @param packageImg
     * @return
     */
    @Override
    public int delImg(PackageImg packageImg)
    {
        
        
        int cnt= packageImgMapper.delImg(packageImg);
        return cnt;
    }
}
