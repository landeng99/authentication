package com.xiangrui.lmp.business.admin.pkg.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;

public interface UserAddressService
{

    /**
     * 通过地址ID查询地址
     * 
     * @param address_id
     * @return UserAddress
     */
    UserAddress queryAddressById(int address_id);

    /**
     * 通过用户ID查询地址
     * 
     * @param userAddress
     * @return
     */
    void updateIdcardStatus(UserAddress userAddress);
    
    /**
     * 根据提单id查询身份证信息
     * @param param
     * @return
     */
    List<UserAddress> queryPkgUserAddrByPickId(Map<String,Object> param);
    
 
    void updateStreet(UserAddress userAddress);
}
