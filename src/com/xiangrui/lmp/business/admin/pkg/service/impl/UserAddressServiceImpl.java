package com.xiangrui.lmp.business.admin.pkg.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.pkg.mapper.UserAddressMapper;
import com.xiangrui.lmp.business.admin.pkg.service.UserAddressService;
import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;

@Service(value = "userAddressService")
public class UserAddressServiceImpl implements UserAddressService
{

    @Autowired
    private UserAddressMapper userAddressMapper;

    /**
     * 通过地址ID查询地址
     * 
     * @param address_id
     * @return UserAddress
     */
    @Override
    public UserAddress queryAddressById(int address_id)
    {
        return userAddressMapper.queryAddressById(address_id);
    }

    /**
     * 通过用户ID查询地址
     * 
     * @param user_id
     * @return
     */
    @Override
    public void updateIdcardStatus(UserAddress userAddress)
    {

        userAddressMapper.updateIdcardStatus(userAddress);
    }

    public List<UserAddress> queryPkgUserAddrByPickId(Map<String, Object> params)
    {

        return userAddressMapper.queryPkgUserAddrByPickId(params);
    }

	@Override
	public void updateStreet(UserAddress userAddress) {
		userAddressMapper.updateStreet(userAddress);
	}

}
