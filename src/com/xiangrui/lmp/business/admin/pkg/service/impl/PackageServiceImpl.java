package com.xiangrui.lmp.business.admin.pkg.service.impl;

import java.awt.Color;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgReturnMapper;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgReturn;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.base.BaseAccountLog;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.homepage.mapper.FrontAccountLogMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgGoodsMapper;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.business.kuaidi100.mapper.ExpressInfoMapper;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.util.CommonUtil;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.ServletContainer;
import com.xiangrui.lmp.util.StringUtil;

@Service(value = "packageService")
public class PackageServiceImpl implements PackageService
{
	static Logger logger = Logger.getLogger(PackageServiceImpl.class);
	
	private static final Integer PROVINCE_LEVEL=1;
    
    private static final Integer CITY_LEVEL=2;
    
    private static final Integer AREA_LEVEL=3;
    
	@Autowired
	private PackageMapper packageMapper;
	
	@Autowired
	private PkgReturnMapper pkgReturnMapper;

	@Autowired
	private ExpressInfoMapper expressInfoMapper;

	@Autowired
	private FrontUserMapper frontUserMapper;
	
	@Autowired
	private FrontAccountLogMapper frontAccountLogMapper;

	@Autowired
	private FrontPkgGoodsMapper frontPkgGoodsMapper;
	@Autowired
	private PkgAttachServiceMapper pkgAttachServiceMapper;

	/**
	 * 分页查询包裹列表
	 * 
	 * @param params
	 * @return List
	 */
	@Override
	public List<Pkg> queryAll(Map<String, Object> params)
	{

		return packageMapper.queryAll(params);
	}

	/**
	 * 货量统计查询
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<Pkg> queryGoodsCountAll(Map<String, Object> params)
	{
		return packageMapper.goodsCountQueryAll(params);
	}

	/**
	 * 货量统计导出查询
	 * 
	 * @param packageIdList
	 * @return
	 */
	@Override
	public List<Pkg> queryPackageByIdList(List<String> packageIdList)
	{
		return packageMapper.queryPackageByIdList(packageIdList);
	}

	/**
	 * 海外退运包裹 分页查询包裹列表
	 * 
	 * @param params
	 * @return List
	 */
	@Override
	public List<Pkg> queryAllReturn(Map<String, Object> params)
	{

		return packageMapper.queryAllReturn(params);
	}

	/**
	 * 查询总记录数
	 * 
	 * @param role
	 * @return int
	 */
	@Override
	public int queryCount()
	{

		return packageMapper.queryCount();
	}

	/**
	 * 通过ID查询包裹
	 * 
	 * @param package_id
	 * @return Pkg
	 */
	@Override
	public Pkg queryPackageById(int package_id)
	{

		return packageMapper.queryPackageById(package_id);
	}

	/**
	 * 查询异常包裹
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<Pkg> queryAllAbnormalPkg(Map<String, Object> params)
	{

		return packageMapper.queryAllAbnormalPkg(params);
	}

	/**
	 * 仓库管理入库管理扫描入库更新包裹
	 * 
	 * @param pkg
	 * @return
	 */
	@SystemServiceLog(description = "修改包裹信息")
	@Override
	public void update(Pkg pkg, User user)
	{
		int retInt = packageMapper.update(pkg);

		// 操作成功,同步包裹日志记录
		if (retInt > 0 && user != null)
		{
			Pkg basePackage = packageMapper.queryPackageById(pkg.getPackage_id());
			PackageLogUtil.log(basePackage, user);
		}
	};

	/**
	 * 包裹管理包裹列表更新包裹
	 */
	@SystemServiceLog(description = "修改包裹信息")
	@Override
	public void updatePackage(Pkg pkg, User user)
	{
		int retInt = packageMapper.updatePackage(pkg);

		// 操作成功,同步包裹日志记录
		if (retInt > 0 && user != null)
		{
			Pkg basePackage = packageMapper.queryPackageById(pkg.getPackage_id());
			PackageLogUtil.log(basePackage, user);
		}
	};
	/**
	 * 包裹管理包裹列表更新包裹2
	 */
	@SystemServiceLog(description = "修改包裹信息2")
	@Override
	public
	void updatePackage(Pkg pkg, Integer userId){
		int retInt = packageMapper.updatePackage(pkg);

		// 操作成功,同步包裹日志记录
		if (retInt > 0)
		{
			Pkg basePackage = packageMapper.queryPackageById(pkg.getPackage_id());
			PackageLogUtil.log(basePackage, userId);
		}
	}
	
	@SystemServiceLog(description = "修改包裹信息2")
	@Override
	public
	int updateTax(Pkg pkg, Integer userId){
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customs_cost", pkg.getCustoms_cost());
		params.put("package_id", pkg.getPackage_id());
		int retInt = packageMapper.updateTax(params);

		// 操作成功,同步包裹日志记录
		if (retInt > 0)
		{
			Pkg basePackage = packageMapper.queryPackageById(pkg.getPackage_id());
			PackageLogUtil.log(basePackage, userId);
		}
		return retInt;
	}
	
	/**
	 * 关联包裹用户
	 * 
	 * @param pkg
	 * @return
	 */
	@SystemServiceLog(description = "关联包裹用户信息")
	@Override
	public void updateUserId(Pkg pkg, User user)
	{
		int retInt = packageMapper.updateUserId(pkg);

		// 操作成功,同步日志记录
		if (retInt > 0)
		{
			Pkg basePackage = packageMapper.queryPackageById(pkg.getPackage_id());
			PackageLogUtil.log(basePackage, user);
		}
	}

	/**
	 * 退运包裹 审批
	 * 
	 * @param pkg
	 * @return
	 */
	@SystemServiceLog(description = "退运包裹审批")
	@Override
	public void updateReturnCost(Map<String, Object> paramMap)
	{
		packageMapper.updateReturnCost((Pkg) paramMap.get("pkg"));
		pkgReturnMapper.updateApprove((PkgReturn) paramMap.get("pkgReturn"));
	}

	@SystemServiceLog(description = "创建包裹信息")
	@Override
	public int insertPkg(Pkg pkg, User user)
	{
		int retInt = packageMapper.insertPkg(pkg);

		// 操作成功,同步包裹日志记录
		if (retInt > 0)
		{
			List<Pkg> packageList = new ArrayList<Pkg>();
			packageList.add(pkg);
			packageList = packageMapper.queryPackageBylogisticsCodes(packageList);

			for (Pkg basePkg : packageList)
			{
				retInt = basePkg.getPackage_id();
				PackageLogUtil.log(basePkg, user);
			}
		}
		return retInt;
	}

	/**
	 * Excel导入国内运单信息
	 * 
	 * @param list
	 * @param user
	 *            同步包裹日志的操作人
	 * @return
	 */
	@Override
	public void batchUpdatePkg(List<Pkg> list, List<ExpressInfo> expressInfoList, User user)
	{
		int retInt = packageMapper.batchUpdatePkg(list);

		List<String> logistics_codes = new ArrayList<String>();

		// 操作成功,同步包裹日志
		if (retInt > 0)
		{
			for (Pkg pkg : list)
			{
				List<Pkg> pkgs = packageMapper.queryPackageByEms_code(pkg.getEms_code());
				for (Pkg pkg2 : pkgs)
				{
					PackageLogUtil.log(pkg2, user);
				}
				logistics_codes.add(pkg.getLogistics_code());
			}

		}

		expressInfoMapper.batchUpdateAble(logistics_codes);
		expressInfoMapper.batchInsertExpressInfo(expressInfoList);

	}

	/**
	 * 国内运单查询
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<Pkg> queryAlllogisticsCode(Map<String, Object> params)
	{

		return packageMapper.queryAlllogisticsCode(params);
	}

	public List<Pkg> queryForAddPallet(Map<String, Object> params)
	{

		return packageMapper.queryForAddPallet(params);
	}

	/**
	 * 用户下所有包裹信息
	 * 
	 * @param user_id
	 * @return
	 */
	public List<Pkg> queryPackageAddressByUserId(int user_id)
	{

		return packageMapper.queryPackageAddressByUserId(user_id);
	}

	/**
	 * 多个包裹ID
	 * 
	 * @param packageIds
	 * @return
	 */
	@Override
	public List<Pkg> queryPackageByPackageIds(List<String> packageIds)
	{

		return packageMapper.queryPackageByPackageIds(packageIds);
	}

	/**
	 * 快递单号
	 * 
	 * @param ems_code
	 * @return
	 */
	@Override
	public Pkg queryPackageByEmsCode(String ems_code)
	{
		return packageMapper.queryPackageByEmsCode(ems_code);
	}

	/**
	 * 更新快递单号
	 * 
	 * @param params
	 * @return
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateEmsCode(Map<String, Object> params, User user)
	{

		List<Pkg> pkgList = queryPackageByLogistics_code((String) params.get("logistics_code"));
		if (pkgList != null && pkgList.size() != 0)
		{
			Pkg pkg = pkgList.get(0);
			pkg.setEms_code((String) params.get("new_ems_code"));
			pkg.setCom((String) params.get("com"));
			// 更新包裹表
			packageMapper.updateEmsCode(pkg);

			PackageLogUtil.log(pkg, user);
		}
		// 更新快递信息表
		ExpressInfo expressInfo = new ExpressInfo();

		String express_info_id = (String) params.get("express_info_id");
		expressInfo.setExpress_info_id(Integer.parseInt(express_info_id));
		expressInfo.setNu((String) params.get("new_ems_code"));
		expressInfo.setCom((String) params.get("com"));
		// 未订阅
		expressInfo.setSubscribe_status(ExpressInfo.SUBSCRIBE_STATUS_INIT);

		expressInfo.setReceive_time(new Timestamp(System.currentTimeMillis()));
		expressInfoMapper.updateEmsCode(expressInfo);
	}

	/**
	 * 导入运单查询包裹信息
	 * 
	 * @param packageList
	 * @return
	 */
	@Override
	public List<Pkg> queryPackageBylogisticsCodes(List<Pkg> packageList)
	{

		return packageMapper.queryPackageBylogisticsCodes(packageList);
	}

	@Override
	public List<Pkg> queryPackageByPick_id(int pick_id)
	{
		return packageMapper.queryPackageAddressByUserId(pick_id);
	}

	@Override
	public List<Pkg> queryPackageByLogistics_code(String logistics_code)
	{
		return packageMapper.queryPackageByLogistics_code(logistics_code);
	}
	
	@Override
	public Pkg queryPackageByLogistics_codeOne(String logistics_code)
	{
		List<Pkg> pList = packageMapper.queryPackageByLogistics_code(logistics_code);
		if( null==pList ) return null;
		
		return pList.get(0);
	}

	@Override
	public List<Pkg> queryPackageByLogistics_codeList(List<String> logistics_codeList)
	{
		return packageMapper.queryPackageBylogisticsCodeList(logistics_codeList);
	}
	
	public List<Pkg> queryPackageByLogistics_codeListPayment(List<PaymentData> pdList){
		List<String> sList = new LinkedList<String>();
		for(PaymentData pd: pdList)
			sList.add(pd.getLogistics_code());
		
		List<Pkg> pList = this.queryPackageByLogistics_codeList(sList);
		for(Pkg p: pList){
			for(PaymentData pd: pdList){
				if( p.getLogistics_code().equals(pd.getLogistics_code()) ){
					p.setCustoms_cost(Float.parseFloat(pd.getTax()));
					break;
				}
			}
		}
			
		return pList;
	}

	@Override
	public List<Pkg> queryPackageByEms_code(String ems_code)
	{
		return packageMapper.queryPackageByEms_code(ems_code);
	}

	@Override
	public List<Pkg> queryPackageByIdString(String pacakge_id)
	{
		return packageMapper.queryPackageByIdString(pacakge_id);
	}

	@Override
	public List<Pkg> queryForScanAddPallet(Map<String, Object> param)
	{

		return packageMapper.queryForScanAddPallet(param);
	}

	/**
	 * 本次导入公司运单号以外 的 所有 派送单号
	 * 
	 * @param packageList
	 * @return
	 */
	@Override
	public List<String> queryAllEmsCode(List<Pkg> packageList)
	{
		return packageMapper.queryAllEmsCode(packageList);
	}

	@Override
	public List<Pkg> queryPackageByCode(Map<String, Object> params)
	{
		return packageMapper.queryPackageByCode(params);
	}
	
	@Override
	public
	List<Pkg> queryPackageByCodeUser(String code){
		return packageMapper.queryPackageByCodeUser(code);
	}

	/**
	 * 快速支付查询
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<Pkg> selectPackageForFastPayment(Map<String, Object> params)
	{

		return packageMapper.selectPackageForFastPayment(params);
	}

	/**
	 * 快速支付
	 * 
	 * @param pkg
	 * @param frontUser
	 * @param frontAccountLog
	 * @return
	 */
	@SystemServiceLog(description = "快速支付")
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public int taskFastPayment(Pkg pkg, FrontUser frontUser, FrontAccountLog frontAccountLog, List<PkgGoods> pkgGoodsList)
	{

		frontAccountLogMapper.insertFrontAccountLog(frontAccountLog);

		// 用户积分
		int integral = (int) frontAccountLogMapper.sumAmount(frontUser.getUser_id());

		frontUser.setIntegral(integral);

		frontUserMapper.updateBalance(frontUser);

		int count = packageMapper.updatePayStatusByPackageId(pkg);

		if (pkgGoodsList != null && !pkgGoodsList.isEmpty())
		{
			frontPkgGoodsMapper.updateTax(pkgGoodsList);
		}

		PackageLogUtil.log(pkg, null);

		return count;
	}

	@Override
	public Pkg queryPackageByOriginal_num(String original_num)
	{

		return packageMapper.queryPackageByOriginal_num(original_num);
	}
	
	@Override
	public Pkg queryPackageByOriginal_numEx(String original_num)
	{
		return packageMapper.queryPackageByOriginal_numEx(original_num);
	}

	/**
	 * 待处理包裹
	 * 
	 * @param <user_id>
	 * @param <express_num>
	 * @param <fromDate>
	 * @param <toDate>
	 * @param <logistics_code>
	 * @param <original_num>
	 * @param <last_name>
	 * @param <user_name>
	 * @param params
	 * @return
	 */
	public List<PkgOperate> queryReadyHandlePackage(Map<String, Object> params)
	{
		List<PkgOperate> reList = new ArrayList<PkgOperate>();
		List<PkgOperate> queryList = packageMapper.queryReadyHandlePackage(params);
		if (queryList != null && queryList.size() > 0)
		{
			String splitPackageGroup = "";
			String splitOriginal_num = "";
			int splitPackage_id=-1;
			Timestamp splitArrive_time = null;
			int needSetColor = 0;
			for (PkgOperate pkgOperate : queryList)
			{
				String attach_id = StringUtils.trimToNull(pkgOperate.getAttach_id());
				if (attach_id != null)
				{
					// 分箱
					if (attach_id.contains("" + BaseAttachService.SPLIT_PKG_ID))
					{
						List<PkgAttachService> pkgAttachServices = pkgAttachServiceMapper.queryPkgAttachServiceForSplitPackageDisplay("" + pkgOperate.getPackage_id());
						if (pkgAttachServices != null && pkgAttachServices.size() > 0)
						{
							PkgAttachService pkgAttachService = pkgAttachServices.get(0);
							String tempSplitPackageGroup = pkgAttachService.getPackage_group();
							if (!tempSplitPackageGroup.equals(splitPackageGroup))
							{
								List<String> splitPackageIdsList = new ArrayList<String>();
								for (PkgAttachService tempPkgAttachService : pkgAttachServices)
								{
									splitPackageIdsList.add("" + tempPkgAttachService.getPackage_id());
								}
								List<String> tempSplitPackageIdList = new ArrayList<String>();
								tempSplitPackageIdList.add(tempSplitPackageGroup);
								List<PkgOperate> beforeSplitList = packageMapper.queryMergePackageHandleInfoByPackageIdsNoStatus(tempSplitPackageIdList);
								if (beforeSplitList != null && beforeSplitList.size() > 0)
								{
									PkgOperate beforeSplitPkgOperate = beforeSplitList.get(0);
									splitOriginal_num = beforeSplitPkgOperate.getOriginal_num();
									splitPackage_id=beforeSplitPkgOperate.getPackage_id();
									splitArrive_time = beforeSplitPkgOperate.getArrive_time();
								}
								List<PkgOperate> splitList = packageMapper.querySplitPackageHandleInfoByPackageIds(splitPackageIdsList);
								if (splitList != null && splitList.size() > 0)
								{
									boolean addSplit = true;
									int ddSetColor = needSetColor++ % 2 == 0 ? 0 : 1;
									for (PkgOperate temppkgOperate : splitList)
									{
										if (addSplit)
										{
											temppkgOperate.setSplitCount(splitList.size());
											addSplit = false;
										}
										else
										{
											temppkgOperate.setSplitCount(-1);
										}
										temppkgOperate.setOriginal_num(splitOriginal_num);
										temppkgOperate.setArrive_time(splitArrive_time);
										temppkgOperate.setDisplayColor(ddSetColor);
										temppkgOperate.setSplitPackage_id(splitPackage_id);
										//temppkgOperate.setPackage_id(splitPackage_id);
										temppkgOperate.setAttach_id("" + BaseAttachService.SPLIT_PKG_ID);
										if(temppkgOperate.getStatus()<1)
										{
											temppkgOperate.setShowSplitNotInputRed("Y");
										}
										addPkgOperate(reList, temppkgOperate);
									}
								}
							}
							splitPackageGroup = tempSplitPackageGroup;
						}
					}
					// 合箱
					else if (attach_id.contains("" + BaseAttachService.MERGE_PKG_ID))
					{
						List<PkgAttachService> pkgAttachServices = pkgAttachServiceMapper.queryPkgAttachServiceForMergePackageDisplay("" + pkgOperate.getPackage_id());
						if (pkgAttachServices != null && pkgAttachServices.size() > 0)
						{
							PkgAttachService pkgAttachService = pkgAttachServices.get(0);
							String mergePackageGroup = pkgAttachService.getPackage_group();
							List<String> mergePackageIdsList = new ArrayList<String>();
							if (mergePackageGroup != null)
							{
								String[] mergePackageGroupArray = mergePackageGroup.split(",");
								for (String tempMergePackageGroup : mergePackageGroupArray)
								{
									mergePackageIdsList.add("" + tempMergePackageGroup);
								}
							}
							List<PkgOperate> mergeList = packageMapper.queryMergePackageHandleInfoByPackageIdsNoStatus(mergePackageIdsList);
							if (mergeList != null && mergeList.size() > 0)
							{
								boolean addMerge = true;
								int ddSetColor = needSetColor++ % 2 == 0 ? 0 : 1;
								for (PkgOperate temppkgOperate : mergeList)
								{
									if (addMerge)
									{
										temppkgOperate.setMergeCount(mergeList.size());
										addMerge = false;
									}
									else
									{
										temppkgOperate.setMergeCount(-1);
									}
									temppkgOperate.setAfterMergeLogistics_code(pkgOperate.getLogistics_code());
									temppkgOperate.setAttach_service(pkgOperate.getAttach_service());
									temppkgOperate.setDisplayColor(ddSetColor);
									temppkgOperate.setAttach_id("" + BaseAttachService.MERGE_PKG_ID);
									addPkgOperate(reList, temppkgOperate);
								}
							}
						}
					}
					else
					{
						pkgOperate.setDisplayColor(needSetColor++ % 2 == 0 ? 0 : 1);
						addPkgOperate(reList, pkgOperate);
					}
				}
				else
				{
					pkgOperate.setDisplayColor(needSetColor++ % 2 == 0 ? 0 : 1);
					addPkgOperate(reList, pkgOperate);
				}
			}
			/*
			 * // 拆箱 if (reList != null && reList.size() > 0) { String
			 * express_num = ""; int shitDisplayColor = 0; for (PkgOperate
			 * pkgOperate : reList) { String tempExpressNum =
			 * StringUtils.trimToNull(pkgOperate.getExpress_num()); if
			 * (tempExpressNum != null) { pkgOperate.setAttach_id("-100");
			 * pkgOperate.setAttach_service("拆箱"); int expressCount = -1; if
			 * (!tempExpressNum.equals(express_num)) { int actExpressCount =
			 * expressNumCount(reList, tempExpressNum); shitDisplayColor =
			 * pkgOperate.getDisplayColor(); if (actExpressCount > 1) {
			 * expressCount = actExpressCount; } } express_num = tempExpressNum;
			 * pkgOperate.setDisplayColor(shitDisplayColor);
			 * pkgOperate.setExpressNumCount(expressCount); } } }
			 */
		}
		return reList;
	}

	@SuppressWarnings("unused")
	private int expressNumCount(List<PkgOperate> pkgOperateList, String expressNum)
	{
		int count = 0;
		if (pkgOperateList != null && pkgOperateList.size() > 0)
		{
			for (PkgOperate tempkgOperate : pkgOperateList)
			{
				String tempExpressNum = StringUtils.trimToNull(tempkgOperate.getExpress_num());
				if (tempExpressNum != null && tempExpressNum.equals(expressNum))
				{
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * 防止重复性添加pkgOperate
	 * 
	 * @param pkgOperateList
	 * @param pkgOperate
	 * @return
	 */
	private List<PkgOperate> addPkgOperate(List<PkgOperate> pkgOperateList, PkgOperate pkgOperate)
	{
		if (checkPklExist(pkgOperateList, pkgOperate))
		{
			String expressNum = StringUtils.trimToEmpty(pkgOperate.getExpress_num());
			expressNum = expressNum.replaceAll(",", "<br>");
			pkgOperateList.add(pkgOperate);
		}
		return pkgOperateList;
	}

	private boolean checkPklExist(List<PkgOperate> pkgOperateList, PkgOperate pkgOperate)
	{
		boolean canAdd = true;
		if (pkgOperateList != null && pkgOperateList.size() > 0)
		{
			for (PkgOperate tempkgOperate : pkgOperateList)
			{
				if (tempkgOperate.getPackage_id() == pkgOperate.getPackage_id())
				{
					canAdd = false;
					break;
				}
			}
		}
		return canAdd;
	}

	/**
	 * 已入库包裹
	 * 
	 * @param <user_id>
	 * @param <express_num>
	 * @param <fromDate>
	 * @param <toDate>
	 * @param <logistics_code>
	 * @param <original_num>
	 * @param <last_name>
	 * @param <user_name>
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<PkgOperate> queryHaveInputedPackage(Map<String, Object> params)
	{
		List<PkgOperate> list = packageMapper.queryHaveInputedPackage(params);
		for (PkgOperate pkgOperate : list)
		{
			String region = pkgOperate.getRegion();
			List<Map<String, Object>> items = new ArrayList<>();
			try
			{
				items = JSONUtil.jsonToBean(region, items.getClass());
				for (Map<String, Object> item : items)
				{
					Object obj = item.get("level");
					Integer level=0;
                    if(Integer.class.isAssignableFrom(obj.getClass())){
                        level=(Integer)obj;
                    }
                    else if(String.class.isAssignableFrom(obj.getClass()))
                    {
                        level=Integer.parseInt((String)obj);
                    }
                    
					if (PROVINCE_LEVEL.equals(level))
					{
						pkgOperate.setProvince((String) item.get("name"));
					}
					if (CITY_LEVEL.equals(level))
					{
						pkgOperate.setCity((String) item.get("name"));
					}
					if (AREA_LEVEL.equals(level))
					{
						pkgOperate.setStreet((String) item.get("name") + pkgOperate.getStreet());
					}
				}
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * 待发货包裹
	 * 
	 * @param <user_id>
	 * @param <express_num>
	 * @param <fromDate>
	 * @param <toDate>
	 * @param <logistics_code>
	 * @param <original_num>
	 * @param <last_name>
	 * @param <user_name>
	 * @param params
	 * @return
	 */
	public List<PkgOperate> queryReadyDeliveryPackage(Map<String, Object> params)
	{
		return packageMapper.queryReadyDeliveryPackage(params);
	}

	/**
	 * 通过包裹ID来查询包裹处理信息
	 * 
	 * @param pacakgeIds
	 * @return
	 */
	public List<PkgOperate> queryPackageHandleInfoByPackageIds(String pacakgeIds)
	{
		// return packageMapper.queryPackageHandleInfoByPackageIds(pacakgeIds);
		return null;
	}

	/**
	 * 生成待发货包裹或已入库包裹导出excel
	 * 
	 * @param fileName
	 * @param pkgOperateList
	 * @return
	 */
	public XSSFWorkbook createReadyDelivery_HaveInputed(String fileName, List<PkgOperate> pkgOperateList)
	{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		// 新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
		// 第一列固定
		xssfSheet.createFreezePane(0, 1, 0, 1);

		// 颜色蓝色
		XSSFColor blueColor = new XSSFColor(new Color(185, 211, 238));
		// 白色
		XSSFColor whiteColor = new XSSFColor(Color.WHITE);

		// 样式白色居中
		XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
		style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setFillForegroundColor(whiteColor);
		style1.setWrapText(true);

		// 样式蓝色居中
		XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style2.setFillForegroundColor(blueColor);
		style2.setWrapText(true);

		// 列宽
		xssfSheet.setColumnWidth(0, 3500);
		xssfSheet.setColumnWidth(1, 4000);
		xssfSheet.setColumnWidth(2, 1800);
		xssfSheet.setColumnWidth(3, 3000);
		xssfSheet.setColumnWidth(4, 5000);
		xssfSheet.setColumnWidth(5, 1800);
		xssfSheet.setColumnWidth(6, 1800);
		xssfSheet.setColumnWidth(7, 10000);
		xssfSheet.setColumnWidth(8, 5000);
		xssfSheet.setColumnWidth(9, 5000);
		xssfSheet.setColumnWidth(10, 5000);
		xssfSheet.setColumnWidth(11, 1500);
		xssfSheet.setColumnWidth(12, 2200);
		xssfSheet.setColumnWidth(13, 1800);
		xssfSheet.setColumnWidth(14, 1500);
		xssfSheet.setColumnWidth(15, 2800);
		xssfSheet.setColumnWidth(16, 1500);
		xssfSheet.setColumnWidth(17, 2800);
		xssfSheet.setColumnWidth(18, 2800);
		xssfSheet.setColumnWidth(19, 2500);
		xssfSheet.setColumnWidth(20, 2500);
		xssfSheet.setColumnWidth(21, 2800);
		xssfSheet.setColumnWidth(22, 3500);
		xssfSheet.setColumnWidth(23, 2500);
		xssfSheet.setColumnWidth(24, 2500);
		xssfSheet.setColumnWidth(25, 1500);
		xssfSheet.setColumnWidth(26, 1500);
		xssfSheet.setColumnWidth(27, 1800);
		xssfSheet.setColumnWidth(28, 3000);
		
		// 表头列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		List<String> columnsList = new ArrayList<String>();
		columnsList.add("公司单号");
		columnsList.add("关联单号");
		columnsList.add("收件人");
		columnsList.add("收件电话");
		columnsList.add("身份证");
		columnsList.add("省份");
		columnsList.add("城市");
		columnsList.add("详细地址");
		columnsList.add("商品名称");
		columnsList.add("内件明细");
		columnsList.add("品牌");
		columnsList.add("数量");
		columnsList.add("实际重量");
		columnsList.add("价格");
		columnsList.add("单位");
		columnsList.add("入库日期");
		columnsList.add("渠道");
		columnsList.add("LASTNAME");
		columnsList.add("身份证图片");
		columnsList.add("审核状态");
		columnsList.add("邮政编码");
		columnsList.add("发件人");
		columnsList.add("发件电话");
		columnsList.add("存放位置");
		columnsList.add("海外仓库");
		columnsList.add("规格");
		columnsList.add("保费");
		columnsList.add("业务");
		columnsList.add("申报类别");

		for (int i = 0; i < columnsList.size(); i++)
		{
			XSSFCell cell = firstXSSFRow.createCell(i);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(columnsList.get(i));
			cell.setCellStyle(style1);
		}

		PkgOperate pkgOperate = null;
		int rowCount = 1;
		for (int j = 0; j < pkgOperateList.size(); j++)
		{
			pkgOperate = pkgOperateList.get(j);
			XSSFRow row = xssfSheet.createRow(rowCount++);
			// 公司单号
			XSSFCell cell0 = row.createCell(0);
			cell0.setCellValue(StringUtils.trimToEmpty(pkgOperate.getLogistics_code()));
			cell0.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 关联单号
			XSSFCell cell1 = row.createCell(1);
			cell1.setCellValue(StringUtils.trimToEmpty(pkgOperate.getOriginal_num()));
			cell1.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 收件人
			XSSFCell cell2 = row.createCell(2);
			cell2.setCellValue(StringUtils.trimToEmpty(pkgOperate.getReceiver()));
			cell2.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//收件电话
			XSSFCell cell3 = row.createCell(3);
			cell3.setCellValue(StringUtils.trimToEmpty(pkgOperate.getReceiver_mobile()));
			cell3.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			// 身份证
			XSSFCell cell4 = row.createCell(4);
			cell4.setCellValue(StringUtils.trimToEmpty(pkgOperate.getIdcard()));
			cell4.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//省份
			XSSFCell cell5 = row.createCell(5);
			cell5.setCellValue(StringUtils.trimToEmpty(pkgOperate.getProvince()));
			cell5.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//城市
			XSSFCell cell6 = row.createCell(6);
			cell6.setCellValue(StringUtils.trimToEmpty(pkgOperate.getCity()));
			cell6.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//详细地址
			XSSFCell cell7 = row.createCell(7);
			cell7.setCellValue(StringUtils.trimToEmpty(pkgOperate.getStreet()));
			cell7.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//商品名称
			XSSFCell cell8 = row.createCell(8);
			cell8.setCellValue(StringUtils.trimToEmpty(pkgOperate.getGoods_detail()));
			cell8.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			// 内件明细
			XSSFCell cell9 = row.createCell(9);
			cell9.setCellValue(StringUtils.trimToEmpty(pkgOperate.getGoods_names()));
			cell9.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//品牌
			XSSFCell cell10 = row.createCell(10);
			cell10.setCellValue(StringUtils.trimToEmpty(pkgOperate.getBrand()));
			cell10.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//数量
			XSSFCell cell11 = row.createCell(11);
			cell11.setCellValue(StringUtils.trimToEmpty(pkgOperate.getQuantity()));
			cell11.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			// 实际重量
			XSSFCell cell12 = row.createCell(12);
			cell12.setCellValue(NumberUtils.scaleMoneyData3(pkgOperate.getActual_weight()));
			cell12.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//价格
			XSSFCell cell13 = row.createCell(13);
			cell13.setCellValue(NumberUtils.scaleMoneyData(pkgOperate.getPrice()));
			cell13.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//单位
			XSSFCell cell14 = row.createCell(14);
			cell14.setCellValue(StringUtils.trimToEmpty(pkgOperate.getUnit()));
			cell14.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			// 入库日期
			XSSFCell cell15 = row.createCell(15);
			cell15.setCellValue(DateUtil.getDateFormatSting(pkgOperate.getInput_time(), "yyyy/MM/dd"));
			cell15.setCellStyle(j % 2 == 0 ? style2 : style1);

			//渠道
			XSSFCell cell16 = row.createCell(16);
			switch (pkgOperate.getExpress_package()) 
			{
				case 1:
					cell16.setCellValue("A");
					break;
				case 2:
					cell16.setCellValue("B");
					break;	
				case 3:
					cell16.setCellValue("C");
					break;
				case 4:
					cell16.setCellValue("D");
					break;
				case 5:
					cell16.setCellValue("E");
					break;
				default:
					cell16.setCellValue("默认");
					break;
			}
			cell16.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			// LASTNAME
			XSSFCell cell17 = row.createCell(17);
			cell17.setCellValue(StringUtils.trimToEmpty(pkgOperate.getLast_name()));
			cell17.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 身份证图片
			XSSFCell cell18 = row.createCell(18);
			cell18.setCellValue(StringUtils.trimToEmpty(pkgOperate.getIdcardImg()).equals("")?"无":"");
			cell18.setCellStyle(j % 2 == 0 ? style2 : style1);

			//审核状态
			XSSFCell cell19 = row.createCell(19);
			switch (pkgOperate.getIdcardStatus())
			{
				case 1 :
					cell19.setCellValue("审核通过");
					break;
				case 2 :
					cell19.setCellValue("审核拒绝");
					break;
				default :
					cell19.setCellValue("未审核");
					break;
			}
			cell19.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//邮政编码
			XSSFCell cell20 = row.createCell(20);
			cell20.setCellValue(StringUtils.trimToEmpty(pkgOperate.getEms_code()));
			cell20.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//发件人
			XSSFCell cell21 = row.createCell(21);
			cell21.setCellValue(StringUtils.trimToEmpty(pkgOperate.getUser_name()));
			cell21.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//发件电话
			XSSFCell cell22 = row.createCell(22);
			cell22.setCellValue(StringUtils.trimToEmpty(pkgOperate.getSend_mobile()));
			cell22.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			// 存放位置
			XSSFCell cell23 = row.createCell(23);
			cell23.setCellValue("");
			cell23.setCellStyle(j % 2 == 0 ? style2 : style1);

			//海外仓库
			XSSFCell cell24 = row.createCell(24);
			switch (pkgOperate.getOsaddr_id())
			{
				case 17 :
					cell24.setCellValue("纽约仓库");
					break;
				case 27 :
					cell24.setCellValue("香港仓库");
					break;
				case 28 :
					cell24.setCellValue("韩国仓库");
					break;
				case 29 :
					cell24.setCellValue("加拿大仓库");
					break;
				case 31 :
					cell24.setCellValue("田那西仓库");
					break;
				case 32 :
					cell24.setCellValue("俄勒冈仓库");
					break;
			}
			cell24.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//规格
			XSSFCell cell25 = row.createCell(25);
			cell25.setCellValue(StringUtils.trimToEmpty(pkgOperate.getSpec()));
			cell25.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//保费
			XSSFCell cell26 = row.createCell(26);
			cell26.setCellValue(NumberUtils.scaleMoneyData3(pkgOperate.getKeep_price()));
			cell26.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//业务
			XSSFCell cell27 = row.createCell(27);
			cell27.setCellValue(StringUtils.trimToEmpty(pkgOperate.getBusiness_name()));
			cell27.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			//申报类别
			XSSFCell cell28 = row.createCell(28);
			cell28.setCellValue(StringUtils.trimToEmpty(pkgOperate.getName_specs()));
			cell28.setCellStyle(j % 2 == 0 ? style2 : style1);
		}
		return xssfWorkbook;
	}

	/**
	 * 生成待处理包裹导出excel
	 * 
	 * @param fileName
	 * @param pkgOperateList
	 * @return
	 */
	public XSSFWorkbook createReadyHandle(String fileName, List<PkgOperate> pkgOperateList, boolean needOutPutArriveStatus)
	{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		// 新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
		// 第一列固定
		xssfSheet.createFreezePane(0, 1, 0, 1);

		// 颜色蓝色
		XSSFColor yellowColor = new XSSFColor(new Color(185, 211, 238));
		// 白色
		XSSFColor whiteColor = new XSSFColor(Color.WHITE);

		// 样式白色居中
		XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
		style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setFillForegroundColor(whiteColor);
		style1.setWrapText(true);

		// 样式蓝色居中
		XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style2.setFillForegroundColor(yellowColor);
		style2.setWrapText(true);

		// 列宽
		xssfSheet.setColumnWidth(0, 4000);
		xssfSheet.setColumnWidth(1, 4000);
		xssfSheet.setColumnWidth(2, 4000);
		xssfSheet.setColumnWidth(3, 4000);
		xssfSheet.setColumnWidth(4, 4000);
		xssfSheet.setColumnWidth(5, 8000);
		xssfSheet.setColumnWidth(6, 4000);
		xssfSheet.setColumnWidth(7, 4000);
		xssfSheet.setColumnWidth(8, 4000);
		if (needOutPutArriveStatus)
		{
			xssfSheet.setColumnWidth(9, 4000);
		}

		// 表头列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		List<String> columnsList = new ArrayList<String>();
		columnsList.add("公司运单号");
		columnsList.add("关联单号");
		columnsList.add("物流单号");
		columnsList.add("到库日期");
		columnsList.add("LASTNAME");
		columnsList.add("内件明细");
		columnsList.add("实际重量");
		columnsList.add("增值服务");
		columnsList.add("存放位置");
		if (needOutPutArriveStatus)
		{
			columnsList.add("包裹状态");
		}

		for (int i = 0; i < columnsList.size(); i++)
		{
			XSSFCell cell = firstXSSFRow.createCell(i);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(columnsList.get(i));
			cell.setCellStyle(style1);
		}

		PkgOperate pkgOperate = null;
		int rowCount = 1;
		for (int j = 0; j < pkgOperateList.size(); j++)
		{
			pkgOperate = pkgOperateList.get(j);
			XSSFRow row = xssfSheet.createRow(rowCount++);

			if (pkgOperate.getMergeCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkgOperate.getMergeCount() - 2, 0, 0);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			// 公司运单号
			XSSFCell cell0 = row.createCell(0);
			if (pkgOperate.getMergeCount() != -1)
			{
				cell0.setCellValue(pkgOperate.getMergeCount() == 0 ? StringUtils.trimToEmpty(pkgOperate.getLogistics_code()) : StringUtils.trimToEmpty(pkgOperate.getAfterMergeLogistics_code()));
			}
			cell0.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);

			if (pkgOperate.getSplitCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkgOperate.getSplitCount() - 2, 1, 1);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			// 关联单号
			XSSFCell cell1 = row.createCell(1);
			if (pkgOperate.getSplitCount() != -1)
			{
				cell1.setCellValue(StringUtils.trimToEmpty(pkgOperate.getOriginal_num()));
			}
			cell1.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);
			// 物流单号
			if (pkgOperate.getExpressNumCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkgOperate.getExpressNumCount() - 2, 2, 2);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			XSSFCell cell2 = row.createCell(2);
			String expressNum = StringUtils.trimToEmpty(pkgOperate.getExpress_num());
			expressNum = expressNum.replaceAll("<br>", ",");
			cell2.setCellValue(expressNum);
			cell2.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 到库日期
			if (pkgOperate.getSplitCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkgOperate.getSplitCount() - 2, 3, 3);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			else if (pkgOperate.getExpressNumCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkgOperate.getExpressNumCount() - 2, 3, 3);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			XSSFCell cell3 = row.createCell(3);
			if (needOutPutArriveStatus)
			{
				cell3.setCellValue(DateUtil.getDateFormatSting(pkgOperate.getArrive_time(), "yyyy/MM/dd"));
			}
			else
			{

				if (pkgOperate.getAttach_id() != null)
				{
					if (pkgOperate.getAttach_id().contains("4"))
					{
						if (pkgOperate.getSplitCount() != -1)
						{
							cell3.setCellValue(DateUtil.getDateFormatSting(pkgOperate.getArrive_time(), "yyyy/MM/dd"));
						}
					}
					else if (pkgOperate.getAttach_id().contains("-100"))
					{
						if (pkgOperate.getExpressNumCount() != -1)
						{
							cell3.setCellValue(DateUtil.getDateFormatSting(pkgOperate.getArrive_time(), "yyyy/MM/dd"));
						}
					}
					else
					{
						cell3.setCellValue(DateUtil.getDateFormatSting(pkgOperate.getArrive_time(), "yyyy/MM/dd"));
					}
				}
				else
				{
					cell3.setCellValue(DateUtil.getDateFormatSting(pkgOperate.getArrive_time(), "yyyy/MM/dd"));
				}
			}
			cell3.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);
			// LASTNAME
			XSSFCell cell4 = row.createCell(4);
			cell4.setCellValue(StringUtils.trimToEmpty(pkgOperate.getLast_name()));
			cell4.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 内件明细
			XSSFCell cell5 = row.createCell(5);
			cell5.setCellValue(StringUtils.trimToEmpty(pkgOperate.getGoods_name()));
			cell5.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 实际重量
			XSSFCell cell6 = row.createCell(6);
			cell6.setCellValue(NumberUtils.scaleMoneyData3(pkgOperate.getActual_weight()));
			cell6.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 增值服务
			if (pkgOperate.getSplitCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkgOperate.getSplitCount() - 2, 7, 7);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			else if (pkgOperate.getMergeCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkgOperate.getMergeCount() - 2, 7, 7);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			else if (pkgOperate.getExpressNumCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkgOperate.getExpressNumCount() - 2, 7, 7);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			XSSFCell cell7 = row.createCell(7);
			if (pkgOperate.getAttach_id() != null)
			{
				if (pkgOperate.getAttach_id().contains("4"))
				{
					if (pkgOperate.getSplitCount() != -1)
					{
						cell7.setCellValue(StringUtils.trimToEmpty(pkgOperate.getAttach_service()));
					}
				}
				else if (pkgOperate.getAttach_id().contains("7"))
				{
					if (pkgOperate.getMergeCount() != -1)
					{
						cell7.setCellValue(StringUtils.trimToEmpty(pkgOperate.getAttach_service()));
					}
				}
				else if (pkgOperate.getAttach_id().contains("-100"))
				{
					if (pkgOperate.getExpressNumCount() != -1)
					{
						cell7.setCellValue(StringUtils.trimToEmpty(pkgOperate.getAttach_service()));
					}
				}
				else
				{
					cell7.setCellValue(StringUtils.trimToEmpty(pkgOperate.getAttach_service()));
				}
			}
			else
			{
				cell7.setCellValue(StringUtils.trimToEmpty(pkgOperate.getAttach_service()));
			}
			cell7.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);
			// 存放位置
			XSSFCell cell8 = row.createCell(8);
			String exceptionPackageFlag=StringUtils.trimToNull(pkgOperate.getException_package_flag());
			String location="";
			if(exceptionPackageFlag!=null)
			{
				location="异常区";
				if(exceptionPackageFlag.equals("N"))
				{
					location="异常区(已下单)";
				}
			}
			cell8.setCellValue(location);
			cell8.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);
			if (needOutPutArriveStatus)
			{
				// 包裹状态
				XSSFCell cell9 = row.createCell(9);
				cell9.setCellValue(pkgOperate.getArrive_status() == 1 ? "已到库" : "未到库");
				cell9.setCellStyle(pkgOperate.getDisplayColor() % 2 == 0 ? style2 : style1);
			}

		}
		return xssfWorkbook;
	}

	/**
	 * 根据package_id来更新需待合箱处理标志(waitting_merge_flag)
	 * 
	 * @param <package_id>
	 * @param <waitting_merge_flag>
	 * @return
	 */
	public int updateWaittingMergePkg(Integer package_id, String waitting_merge_flag)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("package_id", package_id);
		params.put("waitting_merge_flag", waitting_merge_flag);
		return packageMapper.updateWaittingMergePkg(params);
	}

	/**
	 * 已到库包裹
	 * 
	 * @param <user_id>
	 * @param <express_num>
	 * @param <fromDate>
	 * @param <toDate>
	 * @param <logistics_code>
	 * @param <original_num>
	 * @param <last_name>
	 * @param <user_name>
	 * @param params
	 * @return
	 */
	public List<PkgOperate> queryArrivedPackage(Map<String, Object> params)
	{
		List<PkgOperate> reList = new ArrayList<PkgOperate>();
		List<PkgOperate> queryList = packageMapper.queryArrivedPackage(params);
		if (queryList != null && queryList.size() > 0)
		{
			String splitPackageGroup = "";
			String splitOriginal_num = "";
			Timestamp splitArrive_time = null;
			int needSetColor = 0;
			int splitPackage_id=-1;
			for (PkgOperate pkgOperate : queryList)
			{
				if (checkPklExist(reList, pkgOperate))
				{
					String attach_id = StringUtils.trimToNull(pkgOperate.getAttach_id());
					if (attach_id != null)
					{
						// 分箱
						if (attach_id.contains("" + BaseAttachService.SPLIT_PKG_ID))
						{
							List<PkgAttachService> pkgAttachServices = pkgAttachServiceMapper.queryPkgAttachServiceForSplitPackageDisplay("" + pkgOperate.getPackage_id());
							if (pkgAttachServices != null && pkgAttachServices.size() > 0)
							{
								PkgAttachService pkgAttachService = pkgAttachServices.get(0);
								String tempSplitPackageGroup = pkgAttachService.getPackage_group();
								if (!tempSplitPackageGroup.equals(splitPackageGroup))
								{
									List<String> splitPackageIdsList = new ArrayList<String>();
									for (PkgAttachService tempPkgAttachService : pkgAttachServices)
									{
										splitPackageIdsList.add("" + tempPkgAttachService.getPackage_id());
									}
									List<String> tempSplitPackageIdList = new ArrayList<String>();
									tempSplitPackageIdList.add(tempSplitPackageGroup);
									List<PkgOperate> beforeSplitList = packageMapper.queryAllPackageHandleInfoByPackageIds(tempSplitPackageIdList);
									if (beforeSplitList != null && beforeSplitList.size() > 0)
									{
										PkgOperate beforeSplitPkgOperate = beforeSplitList.get(0);
										splitOriginal_num = beforeSplitPkgOperate.getOriginal_num();
										splitPackage_id=beforeSplitPkgOperate.getPackage_id();
										splitArrive_time = beforeSplitPkgOperate.getArrive_time();
									}
									List<PkgOperate> splitList = packageMapper.queryAllPackageHandleInfoByPackageIds(splitPackageIdsList);
									if (splitList != null && splitList.size() > 0)
									{
										boolean addSplit = true;
										int ddSetColor = needSetColor++ % 2 == 0 ? 0 : 1;
										for (PkgOperate temppkgOperate : splitList)
										{
											if (addSplit)
											{
												temppkgOperate.setSplitCount(splitList.size());
												addSplit = false;
											}
											else
											{
												temppkgOperate.setSplitCount(-1);
											}
											temppkgOperate.setOriginal_num(splitOriginal_num);
											temppkgOperate.setDisplayColor(ddSetColor);
											temppkgOperate.setSplitPackage_id(splitPackage_id);
											//temppkgOperate.setPackage_id(splitPackage_id);
											temppkgOperate.setAttach_id("" + BaseAttachService.SPLIT_PKG_ID);
											temppkgOperate.setArrive_time(splitArrive_time);
											addPkgOperate(reList, temppkgOperate);
										}
									}
								}
								splitPackageGroup = tempSplitPackageGroup;
							}
						}
						// 合箱
						else if (attach_id.contains("" + BaseAttachService.MERGE_PKG_ID))
						{
							List<PkgAttachService> pkgAttachServices = pkgAttachServiceMapper.queryPkgAttachServiceForMergePackageDisplay("" + pkgOperate.getPackage_id());
							if (pkgAttachServices != null && pkgAttachServices.size() > 0)
							{
								PkgAttachService pkgAttachService = pkgAttachServices.get(0);
								String mergePackageGroup = pkgAttachService.getPackage_group();
								List<String> mergePackageIdsList = new ArrayList<String>();
								if (mergePackageGroup != null)
								{
									String[] mergePackageGroupArray = mergePackageGroup.split(",");
									for (String tempMergePackageGroup : mergePackageGroupArray)
									{
										mergePackageIdsList.add("" + tempMergePackageGroup);
									}
								}
								List<PkgOperate> mergeList = packageMapper.queryAllPackageHandleInfoByPackageIds(mergePackageIdsList);
								if (mergeList != null && mergeList.size() > 0)
								{
									boolean addMerge = true;
									int ddSetColor = needSetColor++ % 2 == 0 ? 0 : 1;
									for (PkgOperate temppkgOperate : mergeList)
									{
										if (addMerge)
										{
											temppkgOperate.setMergeCount(mergeList.size());
											addMerge = false;
										}
										else
										{
											temppkgOperate.setMergeCount(-1);
										}
										temppkgOperate.setAfterMergeLogistics_code(pkgOperate.getLogistics_code());
										temppkgOperate.setAttach_service(pkgOperate.getAttach_service());
										temppkgOperate.setDisplayColor(ddSetColor);
										temppkgOperate.setAttach_id("" + BaseAttachService.MERGE_PKG_ID);
										addPkgOperate(reList, temppkgOperate);
									}
								}
							}
						}
						else
						{
							pkgOperate.setDisplayColor(needSetColor++ % 2 == 0 ? 0 : 1);
							addPkgOperate(reList, pkgOperate);
						}
					}
					else
					{
						pkgOperate.setDisplayColor(needSetColor++ % 2 == 0 ? 0 : 1);
						addPkgOperate(reList, pkgOperate);
					}
				}
			}
		}
		return reList;
	}
	
    /**
     * 根据package_id来更新异常包裹标志(exception_package_flag)
     * @param <package_id>
     * @param <exception_package_flag>
     * @return
     */
    public int updateExceptionPackageFlag(Integer package_id,String exception_package_flag)
    {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("package_id", package_id);
		params.put("exception_package_flag", exception_package_flag);
		return packageMapper.updateExceptionPackageFlag(params);
    }
    
    /**
     * 更新优惠券支付金额
     * @param coupan_pay
     * @param logistics_code
     * @return
     */
    public int updateCoupan_pay(String logistics_code,float coupan_pay)
    {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("coupan_pay", coupan_pay);
		params.put("logistics_code", logistics_code);
		return packageMapper.updateCoupan_pay(params);
    }
    /**
     * 更新包裹需要取出标志
     * @param need_check_out_flag
     * @param package_id
     * @return
     */
    public int updateNeedCheckOutFlag(String need_check_out_flag,int package_id)
    {
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("need_check_out_flag", need_check_out_flag);
    	params.put("package_id", package_id);
    	return packageMapper.updateNeedCheckOutFlag(params);
    }
    
    /**
     * 根据用户ID统计需要预捐款的包裹
     * @param front_user_id
     * @return
     */
    public int countNeedVirtualPayPackageByFrontUserId(int front_user_id)
    {
    	return packageMapper.countNeedVirtualPayPackageByFrontUserId(front_user_id);
    }
    
    /**
     * 更新余额
     * @param frontUser
     * @return
     */
    public void updateBalance(FrontUser frontUser)
    {
    	frontUserMapper.updateBalance(frontUser);
    }
    
    public List<Pkg>  queryForScanAddPalletNewInit(int output_id){
    	return packageMapper.queryForScanAddPalletNewInit(output_id);
    }
    
    /**
     * 查询当天需要邮件提醒付款的包裹统计
     * @return
     */
    public List<Pkg> queryCurrentDayEmailWarnToPayRecord(){
    	return packageMapper.queryCurrentDayEmailWarnToPayRecord();
    }

	@Override
	public void operateExecutive(Map<String, Object> map) {
		packageMapper.operateExecutive(map);
	}

	/**
	 * 更新包裹状态
	 */
	@Override
	public void updateStatus(Pkg pkgDetail, User user) {
		int retInt = this.packageMapper.updateStatus(pkgDetail);

		// 操作成功,同步包裹日志记录
		if (retInt > 0 && user != null)
		{
			Pkg basePackage = packageMapper.queryPackageById(pkgDetail.getPackage_id());
			PackageLogUtil.log(basePackage, user);
		}
		
	}

	/**
	 * 已入库包裹初始化查询
	 */
	@Override
	public List<PkgOperate> queryHaveInputedPackageInit(Map<String, Object> params)
	{
		return this.packageMapper.queryHaveInputedPackageInit(params);
	}

	/**
	 * 待发货包裹Excel导出
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<PkgOperate> queryReadyDeliveryPackageExport(Map<String, Object> params)
	{
		List<PkgOperate> list = packageMapper.queryReadyDeliveryPackageExport(params);
		for (PkgOperate pkgOperate : list)
		{
			String region = pkgOperate.getRegion();
			List<Map<String, Object>> items = new ArrayList<>();
			try
			{
				items = JSONUtil.jsonToBean(region, items.getClass());
				for (Map<String, Object> item : items)
				{
					Object obj = item.get("level");
					Integer level=0;
                    if(Integer.class.isAssignableFrom(obj.getClass())){
                        level=(Integer)obj;
                    }
                    else if(String.class.isAssignableFrom(obj.getClass()))
                    {
                        level=Integer.parseInt((String)obj);
                    }
                    
					if (PROVINCE_LEVEL.equals(level))
					{
						pkgOperate.setProvince((String) item.get("name"));
					}
					if (CITY_LEVEL.equals(level))
					{
						pkgOperate.setCity((String) item.get("name"));
					}
					if (AREA_LEVEL.equals(level))
					{
						pkgOperate.setStreet((String) item.get("name") + pkgOperate.getStreet());
					}
				}
			} 
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * 渠道编辑
	 */
	@Override
	public void updateExpressPackage(Map<String, Object> param)
	{
		this.packageMapper.updateExpressPackage(param);
	}
	
	/**
	 * 更新单个用户的包裹，即 logisticsCodes 必须全都属于 userId
	 * @param userId
	 * @param logisticsCodes
	 * @return 总共支付的金额
	 */
	private float updatePayment(int userId, List<PaymentData> pdList, boolean isPayTransportCost){
		
		List<Pkg> pkgList = this.queryPackageByLogistics_codeListPayment(pdList);
		
		float amount = 0;
		float tax = 0;
//		String couponLogistics_code = null;
		for (Pkg pkg : pkgList)
		{
//			if (Pkg.PAYMENT_PAID == pkg.getPay_status())
			if (Pkg.isPaid(pkg))
			{
				logger.error("package already pay: "+pkg.getPackage_id());
				return -1;
			}
			// 支付状态
//			Pkg.setPaid(pkg);
//			pkg.setPay_status(Pkg.PAYMENT_PAID);
			pkg.setPay_status(Pkg.PAYMENT_PAID);
	        pkg.setPay_status_custom(Pkg.PAYMENT_CUSTOM_PAID);
	        if( isPayTransportCost )
	            pkg.setPay_status_freight(Pkg.PAYMENT_FREIGHT_PAID);
			// 支付方式
			pkg.setPay_type(Pkg.PAY_TYPE_CASH);
			// 物流状态:待发货
			if (Pkg.isPaid(pkg))
			    pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING);
			// 总价为运费+税金
			amount += pkg.getTransport_cost();
			tax += pkg.getCustoms_cost();

			// 标志虚拟扣款已结算处理
			pkg.setVirtual_pay(-1);
		}
		
		// 登陆账户记录
		FrontAccountLog frontAccountLog = new FrontAccountLog();
		// 必填 商户订单号
		String out_trade_no = CommonUtil.getCurrentDateNum();

		String tmp = "";
		for(PaymentData pd: pdList){
			if( StringUtil.isNotEmpty(tmp) )
				tmp += "<br>";
			tmp += pd.getLogistics_code();
		}
		// 公司运单号
		frontAccountLog.setLogistics_code(tmp);

		frontAccountLog.setOrder_id(out_trade_no);
		// 交易金额
		frontAccountLog.setAmount(amount+tax);
		// 税金
		frontAccountLog.setCustoms_cost(tax);
		
		User backUser = ServletContainer.getCurBackUser();
		frontAccountLog.setBackUserId(backUser.getUser_id());
		frontAccountLog.setBackUserName(backUser.getUsername());
		
		// 时间
		frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
		// 用户
		frontAccountLog.setUser_id(userId);
		// 交易类型
		frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_CONSUME);
		// 交易成功
		frontAccountLog.setStatus(FrontAccountLog.SUCCESS);
		// 描述
		frontAccountLog.setDescription("现金支付");
		// 网上支付部分
		frontAccountLog.setAlipay_amount(0);
		// 账务余额支付部分
		frontAccountLog.setBalance_amount(amount);
		
		this.payment(pkgList, frontAccountLog, userId);
		return amount;
	}
	
	/**
	 * 将属于多个 userId 的 logisticsCodes 分类
	 * isPayTransportCost: 是否支付了运费
	 * @return 所有 logisticsCodes 总价,出错返回 <0
	 */
	private float classifyLogisticsCodes(String logisticsCodes, String taxes, boolean isPayTransportCost){
		List<String> codeList = StringUtil.toStringList(logisticsCodes, ",");
		if( null==codeList || codeList.size()==0){
			logger.error("classifyLogisticsCodes err: "+logisticsCodes);
			return -1;	
		}
		List<PaymentData> pdList = new LinkedList<PaymentData>();
		List<String> taxList = StringUtil.toStringList(taxes, ",");
		Iterator<String> iTax = taxList.iterator();
		for(String c: codeList){
			pdList.add(new PaymentData(c,iTax.next()));
		}
		
		List<Pkg> pkgList = new LinkedList<Pkg>();
		Map<Integer, List<PaymentData>> map = new HashMap<Integer, List<PaymentData>>();
		for(PaymentData pd: pdList){
			Pkg pkg = queryPackageByLogistics_codeOne(pd.getLogistics_code());
			if( null!=pkg ){
				List<PaymentData> tList = map.get(pkg.getUser_id());
				if( null==tList ){
					tList = new LinkedList<PaymentData>();
					tList.add(pd);
					map.put(pkg.getUser_id(), tList);
				}
				else{
					tList.add(pd);
				}
				// 设置修改后的税金
				pkg.setCustoms_cost(Float.parseFloat(pd.getTax()));
				pkgList.add(pkg);
			}
		}
		
		float total = 0;
		for(Entry<Integer, List<PaymentData>> ent : map.entrySet()){
			float tmp = updatePayment(ent.getKey(),ent.getValue(),isPayTransportCost);
			if( tmp < -0.5 )
				return tmp;
			total += tmp;
		}
	
		return total;
	}
	
	private class PaymentData{
		String logistics_code;
		String tax;
		public String getLogistics_code() {
			return logistics_code;
		}
//		public void setLogistics_code(String logistics_code) {
//			this.logistics_code = logistics_code;
//		}
		public String getTax() {
			return tax;
		}
//		public void setTax(String tax) {
//			this.tax = tax;
//		}
		public PaymentData(String logistics_code, String tax) {
			super();
			this.logistics_code = logistics_code;
			this.tax = tax;
		}
		
	}
	
	@Override
	public boolean updatePayment(String transport_cost, String logisticsCodes, String taxes)
	{
		// 更新数据库用
//		Map<String, Object> params = new HashMap<String, Object>();

		List<String> taxList = StringUtil.toStringList(taxes, ",");
		float amount = Float.parseFloat(transport_cost);
		for( String tax: taxList ){
			amount -= Float.parseFloat(tax);
		}

		// 更新用户账户信息
//		FrontUser frontUser = this.queryFrontUserByUserId(user_id);

		float actualTransport_cost = classifyLogisticsCodes(logisticsCodes,taxes, amount>=0.01);
		
		actualTransport_cost = NumberUtils.scaleMoneyDataFloat(actualTransport_cost);
		if( Math.abs(actualTransport_cost - amount) > 0.00001 )
//		if ((actualTransport_cost - amount) != 0)
		{
			logger.error("cost not equal, actualTransport_cost:" + actualTransport_cost + " total:" + amount);
			return false;
		}
	
		return true;
	}

	private void payment(List<Pkg> pkglist, FrontAccountLog log, int userId)
	{
		frontAccountLogMapper.insertFrontAccountLog(log);
//		@SuppressWarnings("unchecked")
//		List<Pkg> pkglist = (List<Pkg>) params.get("pkgList");
//
//		FrontUser frontUser = (FrontUser) params.get("frontUser");
		// 用户积分
//		int integral = (int) frontAccountLogMapper.sumAmount(frontUser.getUser_id());
//
//		frontUser.setIntegral(integral);
//
//		frontUserMapper.updateBalanceForpayment(frontUser);

		packageMapper.updatePackageForPayment(pkglist);
		FrontUser fu = frontUserMapper.queryFrontUserById(userId);
		String frontUserName = "unknowName";
		if( null!=fu ){
			frontUserName = fu.getUser_name();
			if( StringUtil.isEmpty(frontUserName) )
				frontUserName = fu.getAccount();
		}
			
		for (Pkg pkg : pkglist)
		{
			PackageLogUtil.log(pkg, null);
			logger.info("CashPay(现金支付) --- 用户：" + frontUserName + "      包裹：" + pkg.getLogistics_code() + "     运费：" + pkg.getTransport_cost() + "     预付款运费：" + pkg.getVirtual_pay() );
		}
	}

	@Override
	public boolean updateDetail(String address_id, String jsonGoods, String address) {
		
		return false;
	}

	@Override
	public boolean cashRecharge(String chargeMoney, String login_account, Integer uId) {

        // 登陆账户记录
        FrontAccountLog frontAccountLog = new FrontAccountLog();

        // 时间
        frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
        // 交易成功
        frontAccountLog.setStatus(FrontAccountLog.SUCCESS);
        frontAccountLog.setAmount(Float.parseFloat(chargeMoney));
        frontAccountLog.setAccount_type(BaseAccountLog.ACCOUNT_TYPE_RECHARGE);
        frontAccountLog.setDescription("现金充值");
        frontAccountLog.setBackUserId(uId);
        
        // 支付宝号
//        frontAccountLog.setAlipay_no(alipay_no);
        // 支付宝流水号
//        frontAccountLog.setTrade_no(trade_no);
        // 银行流水号
//        frontAccountLog.setBank_seq_no(bank_seq_no);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("account", login_account);
        // 更新用户账户信息
        List<FrontUser> users = frontUserMapper.queryFrontUser(params);
        FrontUser frontUser = users.get(0);
        frontAccountLog.setUser_id(frontUser.getUser_id());
        // 账户余额
        frontUser.setBalance(frontUser.getBalance()
                + frontAccountLog.getAmount());
        // 可用余额
        frontUser.setAble_balance(frontUser.getAble_balance()
                + frontAccountLog.getAmount());

        frontUserMapper.updatefrontUserBalance(frontUser);
		frontAccountLogMapper.insertFrontAccountLog(frontAccountLog);
        logger.debug("充值后更新业务表正常");
		return true;
	}
}
