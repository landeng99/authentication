package com.xiangrui.lmp.business.admin.pkg.service;

import java.util.List;
import com.xiangrui.lmp.business.admin.pkg.vo.City;

public interface CityService
{

    /**
     * 查询省级城市
     * 
     * @return
     */
    List<City> queryCities();

    /**
     * 通过城市ID查询城市
     * 
     * @param city_id
     * @return City
     */
    City queryCityByCityId(int city_id);

    /**
     * 查询父级城市下所有的子城市
     * 
     * @param father_id
     * @return List
     */
    List<City> queryCitiesByFatherCity(int father_id);

}
