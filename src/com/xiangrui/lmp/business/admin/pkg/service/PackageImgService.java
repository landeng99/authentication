package com.xiangrui.lmp.business.admin.pkg.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.pkg.vo.PackageImg;

public interface PackageImgService
{

    /**
     * 查询包裹的图片
     * @param packageImg
     * @return
     */
    List<PackageImg> queryPackageImgList(PackageImg packageImg);
    
    
    /**
     * 图片查询 (包裹ID 和图片类型)
     * 
     * @return
     */
    PackageImg queryPackageImg(PackageImg packageImg);

    /**
     * 图片插入
     * 
     * @return
     */
    int insertPackageImg(PackageImg packageImg);

    /**
     * 更新图片地址
     * 
     * @return
     */
    void updateImgPath(PackageImg packageImg);
    
    
    /**
     * 删除图片
     * @param packageImg
     * @return
     */
    int delImg(PackageImg packageImg);

}
