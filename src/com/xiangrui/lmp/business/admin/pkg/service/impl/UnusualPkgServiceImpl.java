package com.xiangrui.lmp.business.admin.pkg.service.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.pkg.mapper.UnusualPkgMapper;
import com.xiangrui.lmp.business.admin.pkg.service.UnusualPkgService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;
import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.NumberUtils;

@Service("unusualPkgService")
public class UnusualPkgServiceImpl implements UnusualPkgService
{

	@Autowired
	private UnusualPkgMapper unusualPkgMapper;
	@Autowired
	private PkgService frontPkgService;
	@Autowired
	private FrontUserMapper frontUserMapper;

	@Override
	public int insertPkg(UnusualPkg unusualPkg)
	{
		return unusualPkgMapper.insertUnusualPkg(unusualPkg);

	}

	@Override
	public List<UnusualPkg> queryList(Map<String, Object> params)
	{
		return unusualPkgMapper.queryUnusualPkg(params);
	}

	@Override
	public UnusualPkg queryUnusualPkgByOriginal_num(String original_num)
	{

		return unusualPkgMapper.queryUnusualPkgByoriginalNum(original_num);
	}

	/**
	 * 关联异常包裹
	 * 
	 * @param unusualPkg
	 * @return
	 */
	public boolean connectPackage(UnusualPkg unusualPkg)
	{
		boolean isSuccess = false;
		String account = StringUtils.trimToNull(unusualPkg.getAccount());
		String last_name = StringUtils.trimToNull(unusualPkg.getLast_name());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("account", account);
		params.put("last_name", last_name);
		List<FrontUser> accountFrontUserList = frontUserMapper.queryFrontUserByAccount(params);
		FrontUser frontUser = null;
		if (accountFrontUserList != null && accountFrontUserList.size() > 0)
		{
			frontUser = accountFrontUserList.get(0);
		}
		else
		{
			List<FrontUser> lastNameFrontUserList = frontUserMapper.queryFrontUserByLastName(params);
			if (lastNameFrontUserList != null && lastNameFrontUserList.size() > 0)
			{
				frontUser = lastNameFrontUserList.get(0);
			}
		}
		if (frontUser != null)
		{
			isSuccess = true;
			Integer user_id = frontUser.getUser_id();
			float actualWeight = Float.parseFloat(unusualPkg.getActual_weight());
			// 新建包裹
			Integer packageId = frontPkgService.addPkg(user_id, unusualPkg.getOriginal_num(), actualWeight, unusualPkg.getOsaddr_id(), "Y",frontUser.getDefault_pay_type());
			// 关联包裹
			unusualPkg.setUnusual_status(UnusualPkg.STATUS_NORMAL);
			unusualPkg.setConnect_package_id(packageId);
			unusualPkg.setAccount(frontUser.getAccount());
			unusualPkg.setLast_name(frontUser.getLast_name());
			unusualPkgMapper.updateUnusualPkgStatusAndConnectPackageId(unusualPkg);
		}
		return isSuccess;
	}

	/**
	 * 取消关联异常包裹
	 * 
	 * @param unusualPkgId
	 * @return
	 */
	public boolean disConnectPackage(Integer unusualPkgId)
	{
		boolean isSuccess = false;
		UnusualPkg unusualPkg = unusualPkgMapper.queryUnusualPkgByUnusualPkgId(unusualPkgId);
		if (unusualPkg != null)
		{
			isSuccess = true;
			unusualPkg.setUnusual_status(UnusualPkg.STATUS_UNUSUAL);
			Integer connectPackageId = unusualPkg.getConnect_package_id();
			unusualPkg.setConnect_package_id(-1);
			unusualPkg.setAccount(null);
			unusualPkg.setLast_name(null);
			// 删除关联包裹
			frontPkgService.deleteAllPkgByPackageId(connectPackageId, true);
			unusualPkgMapper.updateUnusualPkgStatusAndConnectPackageId(unusualPkg);
		}
		return isSuccess;
	}

	/**
	 * 根据异常包裹ID查询异常包裹
	 * 
	 * @param unusual_pkg_id
	 * @return
	 */
	public UnusualPkg queryUnusualPkgByUnusual_pkg_id(int unusual_pkg_id)
	{
		return unusualPkgMapper.queryUnusualPkgByUnusualPkgId(unusual_pkg_id);
	}

	/**
	 * 更新异常包裹
	 * 
	 * @param pkg
	 * @return
	 */
	public int updateUnusualPkg(UnusualPkg pkg)
	{
		return unusualPkgMapper.updateUnusualPkg(pkg);
	}

	/**
	 * 生成异常包裹导出excel
	 * 
	 * @param fileName
	 * @param pkgOperateList
	 * @return
	 */
	public XSSFWorkbook createUnusualPkgWorkBook(String fileName, List<UnusualPkg> unusualPkgList)
	{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		// 新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
		// 第一列固定
		xssfSheet.createFreezePane(0, 1, 0, 1);

		// 颜色蓝色
		XSSFColor yellowColor = new XSSFColor(new Color(185, 211, 238));
		// 白色
		XSSFColor whiteColor = new XSSFColor(Color.WHITE);

		// 样式白色居中
		XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
		style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setFillForegroundColor(whiteColor);
		style1.setWrapText(true);

		// 样式蓝色居中
		XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style2.setFillForegroundColor(yellowColor);
		style2.setWrapText(true);

		// 列宽
		xssfSheet.setColumnWidth(0, 4000);
		xssfSheet.setColumnWidth(1, 4000);
		xssfSheet.setColumnWidth(2, 8000);
		xssfSheet.setColumnWidth(3, 4000);
		xssfSheet.setColumnWidth(4, 4000);
		xssfSheet.setColumnWidth(5, 4000);
		xssfSheet.setColumnWidth(6, 4000);
		xssfSheet.setColumnWidth(7, 4000);
		xssfSheet.setColumnWidth(8, 4000);

		// 表头列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		List<String> columnsList = new ArrayList<String>();
		columnsList.add("关联单号");
		columnsList.add("包裹重量（磅）");
		columnsList.add("创建时间");
		columnsList.add("状态");
		columnsList.add("LASTNAME");
		columnsList.add("创建人");
		columnsList.add("存放位置");
		columnsList.add("下单状态 ");
		columnsList.add("备注");

		for (int i = 0; i < columnsList.size(); i++)
		{
			XSSFCell cell = firstXSSFRow.createCell(i);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(columnsList.get(i));
			cell.setCellStyle(style1);
		}

		UnusualPkg unusualPkg = null;
		int rowCount = 1;
		for (int j = 0; j < unusualPkgList.size(); j++)
		{
			unusualPkg = unusualPkgList.get(j);
			XSSFRow row = xssfSheet.createRow(rowCount++);
			// 关联单号
			XSSFCell cell0 = row.createCell(0);
			cell0.setCellValue(StringUtils.trimToEmpty(unusualPkg.getOriginal_num()));
			cell0.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 包裹重量（磅）
			XSSFCell cell1 = row.createCell(1);
			if (StringUtils.trimToNull(unusualPkg.getActual_weight()) != null)
			{
				float weight = Float.parseFloat(unusualPkg.getActual_weight());
				cell1.setCellValue(NumberUtils.scaleMoneyData3(weight));
			}
			else
			{
				cell1.setCellValue("");
			}
			cell1.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 创建时间
			XSSFCell cell2 = row.createCell(2);
			cell2.setCellValue(DateUtil.getDateFormatSting(unusualPkg.getCreate_time(), "yyyy/MM/dd HH:mm:ss"));
			cell2.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 状态
			XSSFCell cell3 = row.createCell(3);
			int status = unusualPkg.getUnusual_status();
			cell3.setCellValue(status == UnusualPkg.STATUS_NORMAL ? "已认领" : "未认领");
			cell3.setCellStyle(j % 2 == 0 ? style2 : style1);

			// LASTNAME
			XSSFCell cell4 = row.createCell(4);
			cell4.setCellValue(StringUtils.trimToEmpty(unusualPkg.getLast_name()));
			cell4.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 创建人
			XSSFCell cell5 = row.createCell(5);
			cell5.setCellValue(unusualPkg.getUsername());
			cell5.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 存放位置
			XSSFCell cell6 = row.createCell(6);
			cell6.setCellValue("");
			cell6.setCellStyle(j % 2 == 0 ? style2 : style1);

			// 下单状态 ‘已下单’的意思是：认领的异常包裹，客户在前台已经补充了内件信息和收货地址信息
			XSSFCell cell7 = row.createCell(7);
			String exception_package_flag =StringUtils.trimToEmpty(unusualPkg.getException_package_flag());
			String downMessage="未下单";
			if(exception_package_flag!=null&&"N".equals(exception_package_flag))
			{
				downMessage="已下单";
			}
			cell7.setCellValue(downMessage);
			cell7.setCellStyle(j % 2 == 0 ? style2 : style1);
			
			// 备注
			XSSFCell cell8 = row.createCell(8);
			cell8.setCellValue(StringUtils.trimToEmpty(unusualPkg.getRemark()));
			cell8.setCellStyle(j % 2 == 0 ? style2 : style1);
		}
		return xssfWorkbook;
	}

	/**
	 * 检查是不是同一个人
	 * 
	 * @param account
	 * @param lastName
	 * @return
	 */
	public boolean validateSamePerson(String account, String lastName)
	{
		boolean isSame = false;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("account", account);
		params.put("last_name", lastName);
		List<FrontUser> accountFrontUserList = frontUserMapper.queryFrontUserByAccount(params);
		FrontUser acocuntFrontUser = null;
		FrontUser lastNameFrontUser = null;
		if (accountFrontUserList != null && accountFrontUserList.size() > 0)
		{
			acocuntFrontUser = accountFrontUserList.get(0);
		}
		List<FrontUser> lastNameFrontUserList = frontUserMapper.queryFrontUserByLastName(params);
		if (lastNameFrontUserList != null && lastNameFrontUserList.size() > 0)
		{
			lastNameFrontUser = lastNameFrontUserList.get(0);
		}
		if (acocuntFrontUser != null && lastNameFrontUser != null)
		{
			if (acocuntFrontUser.getUser_id() == lastNameFrontUser.getUser_id())
			{
				isSame = true;
			}
		}
		return isSame;
	}

	/**
	 * 检查account是不是存在
	 * 
	 * @param account
	 * @return
	 */
	public boolean validateAccount(String account)
	{
		boolean isSame = false;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("account", account);
		List<FrontUser> accountFrontUserList = frontUserMapper.queryFrontUserByAccount(params);
		if (accountFrontUserList != null && accountFrontUserList.size() > 0)
		{
			isSame = true;
		}
		return isSame;
	}

	/**
	 * 检查lastName是不是存在
	 * 
	 * @param lastName
	 * @return
	 */
	public boolean validateLastName(String lastName)
	{
		boolean isSame = false;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("last_name", lastName);
		List<FrontUser> lastNameFrontUserList = frontUserMapper.queryFrontUserByLastName(params);
		if (lastNameFrontUserList != null && lastNameFrontUserList.size() > 0)
		{
			isSame = true;
		}
		return isSame;
	}
}
