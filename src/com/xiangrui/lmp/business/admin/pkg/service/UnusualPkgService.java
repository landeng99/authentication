package com.xiangrui.lmp.business.admin.pkg.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
 

public interface UnusualPkgService
{
    int insertPkg(UnusualPkg unusualPkg);
    
    List<UnusualPkg> queryList(Map<String,Object> params);
    
    UnusualPkg queryUnusualPkgByOriginal_num(String original_num);
    
    /**
     * 关联异常包裹
     * @param unusualPkg
     * @return
     */
    boolean connectPackage(UnusualPkg unusualPkg);
    
   /**
    * 取消关联异常包裹
    * @param unusualPkgId
    * @return
    */
    boolean disConnectPackage(Integer unusualPkgId);
    
    /**
     * 根据异常包裹ID查询异常包裹
     * @param unusual_pkg_id
     * @return
     */
    UnusualPkg queryUnusualPkgByUnusual_pkg_id(int unusual_pkg_id);
    
    /**
     * 更新异常包裹
     * @param pkg
     * @return
     */
    int updateUnusualPkg(UnusualPkg pkg);
    
    /**
     * 生成异常包裹导出excel
     * @param fileName
     * @param pkgOperateList
     * @return
     */
    XSSFWorkbook createUnusualPkgWorkBook(String fileName,List<UnusualPkg> unusualPkgList);
    
    /**
     * 检查是不是同一个人
     * @param account
     * @param lastName
     * @return
     */
    boolean validateSamePerson(String account,String lastName);
    
    /**
     * 检查account是不是存在
     * @param account
     * @return
     */
    boolean validateAccount(String account);
    
    /**
     * 检查lastName是不是存在
     * @param lastName
     * @return
     */
    boolean validateLastName(String lastName);
}
