package com.xiangrui.lmp.business.admin.pkg.mapper;

import com.xiangrui.lmp.business.admin.pkg.vo.PkgReturn;

public interface PkgReturnMapper
{

    /**
     * 退运包裹
     * 
     * @return
     */
    PkgReturn queryPkgReturnByPkgId(int pkg_id);

    /**
     * 退运包裹 审批
     * 
     * @return
     */
    void updateApprove(PkgReturn PkgReturn);

}
