package com.xiangrui.lmp.business.admin.pkg.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;

public interface UnusualPkgMapper
{
    /**
     * 查询异常包裹
     * @param params
     * @return
     */
    List<UnusualPkg> queryUnusualPkg(Map<String,Object> params);
    
    
    /**
     * 根据关联单号查询异常包裹
     * @param original_num
     * @return
     */
    UnusualPkg queryUnusualPkgByoriginalNum(String original_num);
    
    /**
     * 根据关联单号查询异常包裹
     * more record
     * @param original_num
     * @return
     */
    List<UnusualPkg> queryUnusualPkgsByoriginalNum(String original_num);
    
    /**
     * 更新异常包裹状态
     * @param unusualPkg
     * @return
     */
    int updateUnusualPkgStatus(UnusualPkg unusualPkg);
    
    /**
     * 新增异常包裹
     * @param unusualPkg
     * @return
     */
    int insertUnusualPkg(UnusualPkg unusualPkg);
    
    /**
     * 根据异常包裹ID查询异常包裹
     * @param original_num
     * @return
     */
    UnusualPkg queryUnusualPkgByUnusualPkgId(Integer unusual_pkg_id);
    
    /**
     * 更新异常包裹状态和关联包裹ID
     * @param unusualPkg<unusual_pkg_id>
     * @param unusualPkg<connect_package_id>
     * @param unusualPkg<unusual_status>
     * @return
     */
    int updateUnusualPkgStatusAndConnectPackageId(UnusualPkg unusualPkg);
    
    /**
     * 更新异常包裹
     * @return
     */
    int updateUnusualPkg(UnusualPkg unusualPkg);
    
    /**
     * 通过关联ID删除异常包裹
     * @param connect_package_id
     * @return
     */
    int deleteUnusualPkgByConnectPackageId(Integer connect_package_id);
    
}
