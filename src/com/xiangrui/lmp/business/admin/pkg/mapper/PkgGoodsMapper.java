package com.xiangrui.lmp.business.admin.pkg.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods;

;

public interface PkgGoodsMapper
{
    /**
     * 查询同一包裹下的所有商品
     * 
     * @param package_id
     * @return List
     */
    List<PkgGoods> selectPkgGoodsByPackageId(int package_id);

    int batchUpdatePkgGoods(List<PkgGoods> list);
}
