package com.xiangrui.lmp.business.admin.pkg.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;

public interface PackageMapper
{

    /**
     * 分页查询包裹列表
     * 
     * @param params
     * @return
     */
    List<Pkg> queryAll(Map<String, Object> params);
    
    /**
     * 货量统计查询
     * 
     * @param params
     * @return
     */
    List<Pkg> goodsCountQueryAll(Map<String, Object> params);

    /**
     * 货量统计导出查询
     * 
     * @param packageIdList
     * @return
     */
    List<Pkg> queryPackageByIdList(List<String> packageIdList);
    /**
     * 海外退运包裹 分页查询包裹列表
     * 
     * @param params
     * @return
     */
    List<Pkg> queryAllReturn(Map<String, Object> params);

    /**
     * 按照logistiics_cod查询对应的包裹
     * 
     * @param logistics_code
     * @return
     */
    List<Pkg> queryPackageByLogistics_code(String logistics_code);

    /**
     * 按照ems_code查询对应的包裹
     * 
     * @param ems_code
     * @return
     */
    List<Pkg> queryPackageByEms_code(String ems_code);

    /**
     * 查询总记录数
     * 
     * @param role
     * @return
     */
    int queryCount();
    
    int queryCountByUserId(Integer userId);
    int queryCountByUserIdStatus(Map<String, Object> paramsCount);
    

    /**
     * 通过ID查询包裹
     * 
     * @param package_id
     * @return
     */
    Pkg queryPackageById(int package_id);

    /**
     * 查询异常包裹
     * 
     * @param params
     * @return
     */
    List<Pkg> queryAllAbnormalPkg(Map<String, Object> params);

    /**
     * 包裹管理包裹列表更新包裹
     * @param pkg
     * @return
     */
    int updatePackage(Pkg pkg);
    
    /**
     * 更新税金
     * @param pkg
     * @return
     */
    int updateTax(Map<String, Object> params);

    /**
     * 关联包裹用户
     * 
     * @param pkg
     * @return
     */
    int updateUserId(Pkg pkg);

    /**
     * 退运包裹 审批
     * 
     * @param pkg
     * @return
     */
    void updateReturnCost(Pkg pkg);

    /**
     * 包裹创建
     * 
     * @param pkg
     * @return
     */
    int insertPkg(Pkg pkg);

    /**
     * Excel导入国内运单信息
     * 
     * @param list
     * @return
     */
    int batchUpdatePkg(List<Pkg> list);
    
    /**
     * 现金支付时更新包裹状态
     * @param list
     * @return
     */
    int updatePackageForPayment(List<Pkg> list);

    /**
     * 国内运单查询
     * 
     * @param params
     * @return
     */
    List<Pkg> queryAlllogisticsCode(Map<String, Object> params);

    /**
     * 托盘创建包裹查询
     * 
     * @param params
     * @return
     */
    List<Pkg> queryForAddPallet(Map<String, Object> params);

    /**
     * 用户下所有包裹信息
     * 
     * @param user_id
     * @return
     */
    List<Pkg> queryPackageAddressByUserId(int user_id);

    /**
     * 查询托盘包裹
     * 
     * @param pallet_id
     * @return
     */
    List<Pkg> queryPalletPkgs(int pallet_id);

    /**
     * 根据参数查询包裹信息
     * 
     * @param param
     * @return
     */
    List<Pkg> queryPalletPkgsByParam(Map<String, Object> param);

    /**
     * 多个包裹ID
     * 
     * @param packageIds
     * @return
     */
    List<Pkg> queryPackageByPackageIds(List<String> packageIds);

    /**
     * 快递单号
     * 
     * @param ems_code
     * @return
     */
    Pkg queryPackageByEmsCode(String ems_code);

    /**
     * 更新快递单号
     * 
     * @param pkg
     * @return
     */
    void updateEmsCode(Pkg pkg);

    /**
     * 包裹状态 (多条)
     * 
     * @param packageIds
     * @return
     */
    int batchUpdateStatus(List<Pkg> packageList);

    /**
     * 同一提单的包裹
     * 
     * @param params
     * @return
     */
    int updateStatusByPickId(Map<String, Object> params);
    
    /**更新托盘下面的包裹状态
     * @param params
     * @return
     */
    int updateStatusByPalletId(Map<String, Object> params);

    /**
     * 导入运单查询包裹信息
     * 
     * @param packageList
     * @return
     */
    List<Pkg> queryPackageBylogisticsCodes(List<Pkg> packageList);
    
    /**
     * 导入运单查询包裹信息
     * 
     * @param logisticsCodeList
     * @return
     */
    List<Pkg> queryPackageBylogisticsCodeList(List<String> logisticsCodeList);

    /**
     * 通过提单号,找到对应的包裹
     * 
     * @param pick_id
     * @return
     */
    List<Pkg> queryPackageByPick_id(int pick_id);

    /**
     * 通过ems 单号更新 包裹状态
     * 
     * @param params
     * @return
     */
    int updateStatusByEmsCode(Map<String, Object> params);
    
    
    int updateStatusByPkg(Pkg pkg);

    List<Pkg> queryPackageByIdString(String pacakge_id);
    
    
    /**
     * 包裹扫描加入托盘查询
     * @param params
     * @return
     */
    List<Pkg> queryForScanAddPallet(Map<String, Object> params);
    
    /**
     * 本次导入公司运单号以外 的 所有 派送单号 
     * @param packageList
     * @return
     */
    List<String> queryAllEmsCode(List<Pkg> packageList);
    
    /**
     * 根据包裹单号查询包裹
     * @param params
     * @return
     */
    List<Pkg> queryPackageByCode(Map<String, Object> params);
    
    /**
     * 根据自建单号或平台单号查包裹，完全匹配
     * @param code
     * @return
     */
    List<Pkg> queryPackageByCodeUser(String code);

    /**
     * 同行账单导入查询
     * 
     * @param list
     * @return
     */
    List<Pkg> selectPackageBylogisticsCodes(List<String> list);

    /**
     * 同行账单导入
     * 
     * @param list
     * @return
     */
    int batchUpdatePayStatus(List<Pkg> list);

    /**
     * 快速支付查询
     * 
     * @param params
     * @return
     */
    List<Pkg> selectPackageForFastPayment(Map<String, Object> params);
    
    /**
     * 快速支付
     * 
     * @param pkg
     * @return
     */
    int updatePayStatusByPackageId(Pkg pkg);
    
    Pkg queryPackageByOriginal_num(String original_num);
    Pkg queryPackageByOriginal_numEx(String original_num);
    /**
     * 待处理包裹
     * @param <user_id>
     * @param <express_num>
     * @param <fromDate>
     * @param <toDate>
     * @param <logistics_code>
     * @param <original_num>
     * @param <last_name>
     * @param <user_name>
     * @param params
     * @return
     */
    List<PkgOperate> queryReadyHandlePackage(Map<String, Object> params);
    /**
     * 已入库包裹
     * @param <user_id>
     * @param <express_num>
     * @param <fromDate>
     * @param <toDate>
     * @param <logistics_code>
     * @param <original_num>
     * @param <last_name>
     * @param <user_name>
     * @param params
     * @return
     */
    List<PkgOperate> queryHaveInputedPackage(Map<String, Object> params);
    /**
     * 待发货包裹
     * @param <user_id>
     * @param <express_num>
     * @param <fromDate>
     * @param <toDate>
     * @param <logistics_code>
     * @param <original_num>
     * @param <last_name>
     * @param <user_name>
     * @param params
     * @return
     */
    List<PkgOperate> queryReadyDeliveryPackage(Map<String, Object> params);
    /**
     * 通过包裹ID来查询包裹处理信息
     * @param pacakgeIds
     * @return
     */
    List<PkgOperate> querySplitPackageHandleInfoByPackageIds(List<String> pacakgeIds);
    
    /**
     * 通过包裹ID来查询包裹处理信息
     * @param pacakgeIds
     * @return
     */
    List<PkgOperate> queryMergePackageHandleInfoByPackageIds(List<String> pacakgeIds);
    
    List<PkgOperate> queryMergePackageHandleInfoByPackageIdsNoStatus(List<String> pacakgeIds);
    
    /**
     * 根据package_id来更新需待合箱处理标志(waitting_merge_flag)
     * @param <package_id>
     * @param <waitting_merge_flag>
     * @return
     */
    int updateWaittingMergePkg(Map<String, Object> params);
    
    /**
     * 已到库包裹
     * @param <user_id>
     * @param <express_num>
     * @param <fromDate>
     * @param <toDate>
     * @param <logistics_code>
     * @param <original_num>
     * @param <last_name>
     * @param <user_name>
     * @param params
     * @return
     */
    List<PkgOperate> queryArrivedPackage(Map<String, Object> params);
    /**
     * 通过包裹ID来查询包裹[包含增值服务等]信息
     * @param pacakgeIds
     * @return
     */
    List<PkgOperate> queryAllPackageHandleInfoByPackageIds(List<String> pacakgeIds);
    
    /**
     * 根据package_id来更新异常包裹标志(exception_package_flag)
     * @param <package_id>
     * @param <exception_package_flag>
     * @return
     */
    int updateExceptionPackageFlag(Map<String, Object> params);
    /**
     * 更新优惠券支付金额
     * logistics_code
     * @param coupan_pay
     * @param logistics_code
     * @return
     */
    int updateCoupan_pay(Map<String, Object> params);
    
    /**
     * 更新包裹需要取出标志
     * @param need_check_out_flag
     * @param package_id
     * @return
     */
    int updateNeedCheckOutFlag(Map<String, Object> params);
    
    /**
     * 根据用户ID统计需要预捐款的包裹
     * @param front_user_id
     * @return
     */
    int countNeedVirtualPayPackageByFrontUserId(int front_user_id);
    
    
    List<Pkg>  queryForScanAddPalletNewInit(int output_id);
    /**
     * 查询当天需要邮件提醒付款的包裹统计
     * @return
     */
    List<Pkg> queryCurrentDayEmailWarnToPayRecord();

	void operateExecutive(Map<String, Object> map);

	/**
	 * 更新包裹状态
	 * @param pkgDetail
	 * @return 
	 */
	int updateStatus(Pkg pkgDetail);

	/**
	 * 已入库包裹初始化查询
	 * @param params
	 * @return
	 */
	List<PkgOperate> queryHaveInputedPackageInit(Map<String, Object> params);

	/**
	 * 待发货包裹Excel导出
	 * @param params
	 * @return
	 */
	List<PkgOperate> queryReadyDeliveryPackageExport(Map<String, Object> params);

	/**
	 * 渠道编辑
	 * @param package_id
	 * @param express_package
	 */
	void updateExpressPackage(Map<String, Object> param);

	/**
	 * 仓库管理入库管理扫描入库更新包裹
	 * @param pkg
	 * @return
	 */
	int update(Pkg pkg);

}
