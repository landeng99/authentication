package com.xiangrui.lmp.business.admin.pkg.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;

public interface UserAddressMapper
{

    /**
     * 通过地址ID查询地址
     * 
     * @param address_id
     * @return
     */
    UserAddress queryAddressById(int address_id);

    /**
     * 查询包裹用户信息
     * 
     * @param params
     * @return
     */
    List<UserAddress> queryUserAddrByPkg(Map<String, Object> params);

    /**
     * 查询提单包裹的用户信息
     * 
     * @param params
     * @return
     */
    List<UserAddress> queryUserAddrByPickOrder(Map<String, Object> params);

    /**
     * 更新身份证审核状态
     * 
     * @param userAddress
     * @return
     */
    void updateIdcardStatus(UserAddress userAddress);
    
    
    /**根据提单号查询包裹地址信息
     * @param params
     * @return
     */
    List<UserAddress>queryPkgUserAddrByPickId(Map<String,Object>params);

    void updateStreet(UserAddress userAddress);
}
