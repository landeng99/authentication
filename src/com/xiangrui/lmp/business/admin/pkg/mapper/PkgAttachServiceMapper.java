package com.xiangrui.lmp.business.admin.pkg.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;

public interface PkgAttachServiceMapper
{

    /**
     * 增值服务
     * 
     * @param params
     * @return
     */
    List<PkgAttachService> queryAllPkgAttachService(Map<String, Object> params);

    /**
     * 包裹所有的增值服务
     * 
     * @param package_id
     * @return
     */
    List<PkgAttachService> queryPkgAttachServiceBypackageId(int package_id);

    /**
     * 查询或者是验证某个指定的包裹,指定的增值服务 信息
     * 
     * @param package_id
     *            包裹id
     * @param attach_id
     *            增值服务id
     * @param attachService
     *            仅仅是查询其中的包裹id和服务id
     * @return
     */
    PkgAttachService queryAllPkgAttachServiceById(PkgAttachService attachService);
    
    List<PkgAttachService> queryAllPkgAttachServiceByPackageGroup(PkgAttachService attachService);

    /**
     * 为包裹添加增值服务
     * 
     * @param pkgAttachService
     */
    void addPkgAttachService(PkgAttachService pkgAttachService);

    void delPkgAttachService(PkgAttachService pkgAttachService);

    /**
     * 计算总增值服务费用用
     * 
     * @param package_id
     * @return
     */
    List<PkgAttachService> queryPkgAttachServiceByPackage(
            int package_id);

    /**
     * 查询包裹增值服务
     * @param params
     * @return
     */
    List<PkgAttachServiceGroup> queryPkgAttachServiceGroup(
            Map<String, Object> params);
    
    /**
     * 根据参数查询待分箱包裹的子包裹的增值服务
     * @param params
     * @return
     */
    List<PkgAttachService> queryForSplit(Map<String, Object> params);
    
    /**
     * 更新包裹增值服务
     * @param params
     * @return
     */
    int updatePkgAttachService (Map<String, Object> params);
    
    /**
     * 更新包裹增值服务
     * @param params
     * @return
     */
    int updatePkgAttachServicePrice (Map<String, Object> params);
    
    /**
     * 分箱，用package_group查出分箱前包裹信息，用于并装分箱后的显示
     * @param params
     * @return
     */
    List<PkgAttachService> queryPkgAttachServiceForSplitPackageDisplay(String package_id);
    
    /**
     * 合箱，用package_group查出合箱前包裹信息，用于并装合箱后的显示 
     * @param params
     * @return
     */
    List<PkgAttachService> queryPkgAttachServiceForMergePackageDisplay(String package_id);
}
