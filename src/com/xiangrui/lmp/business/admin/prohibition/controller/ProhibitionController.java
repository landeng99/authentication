package com.xiangrui.lmp.business.admin.prohibition.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.banner.vo.Banner;
import com.xiangrui.lmp.business.admin.prohibition.service.ProhibitionService;
import com.xiangrui.lmp.business.admin.prohibition.vo.Prohibition;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.constant.WebConstants;
import com.xiangrui.lmp.init.AppServerUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 禁运物品
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-11 上午11:06:59
 *         </p>
 */
@Controller
@RequestMapping("/admin/prohibition")
public class ProhibitionController
{

    /**
     * 图片后缀
     */
    private static final String[] IMG_SUFFIX_ARY = new String[] { ".bmp",
            ".jpg", ".jpeg", ".png", ".gif", ".pcx", ".tiff", ".tga", ".exif",
            ".fpx", ".svg", ".psd", ".cdr", ".pcd", ".dxf", ".ufo", ".eps",
            ".hdri", ".ai", ".raw" };

    /**
     * 上传图片在服务器存放路径
     */
    private static final String UPLOAD_IMG_PATH = AppServerUtil.getWebRoot()
            + WebConstants.UPLOAD_PATH_IMAGES;

    /**
     * 禁运物品
     */
    @Autowired
    private ProhibitionService prohibitionService;

    // 首次登陆,上传,删除,修改
    /**
     * 查询所有轮播图
     * 
     * @param request
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);
        request.setAttribute("pageView", pageView);
        List<Prohibition> prohibitionLists = prohibitionService
                .queryAll(params);

        request.setAttribute("prohibitionLists", prohibitionLists);
        return "back/prohibitionList";
    }

    /**
     * 删除
     * 
     * @param prohibition
     * @return
     */
    @RequestMapping("/deleteProhibition")
    public String deleteProhibition(HttpServletRequest request,
            Prohibition prohibition)
    {
        // 匹配数据库
        prohibition = prohibitionService.queryALlId(prohibition
                .getProhibition_id());

        // 删除数据
        prohibitionService.deleteProhibition(prohibition.getProhibition_id());

        // 清除图片
        // 图片数据库位置
        String imageurlOld = prohibition.getImageurl();
        if (!"".equals(imageurlOld))
        {
            // 用物理存储文件夹替换数据库存储文件夹 ==> 最终的物理存储文件
            imageurlOld = imageurlOld.replace(SYSConstant.UPLOAD_IMG_PATH_DB,
                    UPLOAD_IMG_PATH);
            imageurlOld = getUploadImgPath(imageurlOld);
            // 删除文件
            File fileTemp = new File(imageurlOld);
            if (fileTemp.exists())
            {
                fileTemp.delete();
            }
        }

        // 前台禁用物品的application刷新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("prohibitionLists", null);

        return "back/prohibitionList";
    }

    /**
     * 进入上传或者修改界面
     * 
     * @param request
     * @param prohibition
     * @return
     */
    @RequestMapping("/toProhibitionUpload")
    public String toProhibitionUpload(HttpServletRequest request,
            Prohibition prohibition)
    {
        if (prohibition.getProhibition_id() > 0)
        {
            prohibition = prohibitionService.queryALlId(prohibition
                    .getProhibition_id());
            request.setAttribute("prohibitionPojo", prohibition);
        } else
        {
            request.setAttribute("prohibitionPojo", new Prohibition());
        }
        return "back/prohibitionUpload";
    }

    /**
     * 上传图片
     * 
     * @param request
     * @param imgText
     *            图片遗留值
     * @param prohibition_id
     *            禁运物品id,区分是新增上传,还是修改图片的上传
     * @return
     */
    @RequestMapping("/uploadProhibition")
    @ResponseBody
    public String uploadProhibition(HttpServletRequest request, String imgText,
            int prohibition_id)
    {
        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        MultipartFile imgFile1 = req.getFile("prohibitionPic");

        // 获取文件后缀
        String imgSuffix = imgFile1.getOriginalFilename()
                .substring(imgFile1.getOriginalFilename().lastIndexOf("."))
                .toLowerCase();

        // imgSuffix是否是图片格式
        boolean isImgSuffix = false;
        for (String suffix : IMG_SUFFIX_ARY)
        {
            if (suffix.equals(imgSuffix))
            {
                isImgSuffix = true;
                break;
            }
        }

        // 不是图片格式,结束上传操作
        if (isImgSuffix == false)
        {
            return "";
        }

        // 获取随机名称,补全图片格式
        String fileName = String.valueOf(System.currentTimeMillis())
                + imgSuffix;

        // 文件路径,数据库路径,前台显示路径
        String rtnPath = SYSConstant.UPLOAD_IMG_PATH_DB + fileName;
        try
        {
            String imgPathNew = UPLOAD_IMG_PATH + fileName;
            imgPathNew = getUploadImgPath(imgPathNew);
            // 上传保存到服务器物理路径去
            FileUtils.copyInputStreamToFile(imgFile1.getInputStream(),
                    new File(imgPathNew));

            // 新增上传,修改上传时,不需要删除信息,防止用户不保存而离开操作界面,把本来存在的数据删除
            if (prohibition_id == 0)
            {
                // 如果存在上次上传图片,并且没有保存到数据库中,清除上次上传没有保存数据库的图片文件信息
                if (null != imgText && !"".equals(imgText))
                {
                    String imgPathOld = imgText.replace(
                            SYSConstant.UPLOAD_IMG_PATH_DB, UPLOAD_IMG_PATH);
                    imgPathOld = getUploadImgPath(imgPathOld);
                    File fileTemp = new File(imgPathOld);
                    if (fileTemp.exists())
                    {
                        fileTemp.delete();
                    }
                }
            }

            // rtnPath = getUploadImgPath(rtnPath);
            // 添加临时图片到数据库中
            Prohibition tempProhibition = new Prohibition();
            tempProhibition.setImageurl(rtnPath);
            prohibitionService.tempProhibitionAdd(tempProhibition);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // 前台禁用物品的application刷新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("prohibitionLists", null);

        // 返回到前台页面显示
        return rtnPath;
    }

    /**
     * 保存禁运物品
     * 
     * @param request
     * @param prohibition
     * @return
     */
    @RequestMapping("/addProhibition")
    public String addProhibition(HttpServletRequest request,
            Prohibition prohibition)
    {

        // 是否为修改
        if (prohibition.getProhibition_id() > 0)
        {
            // 原始图片路径
            String imageOld = prohibitionService.queryALlId(
                    prohibition.getProhibition_id()).getImageurl();
            // 修改
            prohibitionService.updateProhibition(prohibition);
            // 图片有变动
            if (!imageOld.equals(prohibition.getImageurl()))
            {
                // 原始图片添加的临时数据中,方便清空图片信息
                Prohibition tempProhibition = new Prohibition();
                tempProhibition.setImageurl(imageOld);
                prohibitionService.tempProhibitionAdd(tempProhibition);
            }

            // 清除上传图片时的临时数据,避免后面清空图片是把本图片个清除了
            Prohibition tempPro = prohibitionService
                    .queryAllTempImageurl(prohibition.getImageurl());
            if (null != tempPro)
            {
                prohibitionService.tempProhibitionDelete(tempPro
                        .getProhibition_id());
            }
        } else
        {
            // 增加操作中
            // 临时数据中是否存在本图片信息
            Prohibition prohibitionTemp = prohibitionService
                    .queryAllTempImageurl(prohibition.getImageurl());

            // 存在本图片的临时数据
            if (null != prohibitionTemp)
            {
                // 获取主键id,修改操作即可
                prohibition.setProhibition_id(prohibitionTemp
                        .getProhibition_id());
                prohibitionService.updateProhibition(prohibition);
            } else
            {
                prohibitionService.addProhibition(prohibition);
            }
        }

        // 清空临时数据和临时图片
        List<Prohibition> prohibitionTemps = prohibitionService
                .queryAllTempList();
        for (Prohibition temp : prohibitionTemps)
        {
            if (null != temp.getImageurl() && !"".equals(temp.getImageurl()))
            {
                String imgPathOld = temp.getImageurl().replace(
                        SYSConstant.UPLOAD_IMG_PATH_DB, UPLOAD_IMG_PATH);
                imgPathOld = getUploadImgPath(imgPathOld);
                // 删除文件
                File fileTemp = new File(imgPathOld);
                if (fileTemp.exists())
                {
                    fileTemp.delete();
                }
            }
        }
        // 删除数据
        prohibitionService.tempProhibitionDelete();

        // 前台禁用物品的application刷新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("prohibitionLists", null);

        return queryAll(request);
    }

    private String getUploadImgPath(String uploadImgPath)
    {
        String separator = File.separator;
        return uploadImgPath.replace("\\", separator);
    }

}
