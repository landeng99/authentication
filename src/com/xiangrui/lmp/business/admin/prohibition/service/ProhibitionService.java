package com.xiangrui.lmp.business.admin.prohibition.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.prohibition.vo.Prohibition;

/**
 * 禁运物品
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-11 上午11:05:07</p>
 */
public interface ProhibitionService
{
    /**
     * 查询所有禁运物品
     * 
     * @param parmes
     *            仅为了分页
     * @return
     */
    List<Prohibition> queryAll(Map<String, Object> parmes);

    /**
     * 查询某个禁运物品
     * 
     * @param prohibition_id
     * @return
     */
    Prohibition queryALlId(int prohibition_id);

    /**
     * 查询某个禁运物品的临时信息
     * 
     * @param imageurl
     * @return
     */
    Prohibition queryAllTempImageurl(String imageurl);

    /**
     * 查询所有禁运物品的临时信息
     * 
     * @return
     */
    List<Prohibition> queryAllTempList();

    /**
     * 更新禁运物品信息
     * 
     * @param prohibition
     */
    void updateProhibition(Prohibition prohibition);

    /**
     * 新增禁运物品
     * 
     * @param prohibition
     */
    void addProhibition(Prohibition prohibition);

    /**
     * 新增禁运物品临时信息
     * 
     * @param prohibition
     */
    void tempProhibitionAdd(Prohibition tempProhibition);

    /**
     * 删除禁运物品
     * 
     * @param prohibition_id
     */
    void deleteProhibition(int prohibition_id);

    /**
     * 删除所有的禁运物品临时信息
     */
    void tempProhibitionDelete();

    /**
     * 删除某个临时数据
     */
    void tempProhibitionDelete(int prohibition_id);

}
