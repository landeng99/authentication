package com.xiangrui.lmp.business.admin.prohibition.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.prohibition.mapper.ProhibitionMapper;
import com.xiangrui.lmp.business.admin.prohibition.service.ProhibitionService;
import com.xiangrui.lmp.business.admin.prohibition.vo.Prohibition;

/**
 * 禁运物品
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-11 上午11:06:04</p>
 */
@Service("prohibitionService")
public class ProhibitionServiceImpl implements ProhibitionService
{

    /**
     * 禁运物品
     */
    @Autowired
    private ProhibitionMapper prohibitionMapper;

    @Override
    public List<Prohibition> queryAll(Map<String, Object> parmes)
    {
        return prohibitionMapper.queryAll(parmes);
    }

    @Override
    public Prohibition queryALlId(int prohibition_id)
    {
        return prohibitionMapper.queryALlId(prohibition_id);
    }

    @Override
    public Prohibition queryAllTempImageurl(String imageurl)
    {
        return prohibitionMapper.queryAllTempImageurl(imageurl);
    }

    @Override
    public List<Prohibition> queryAllTempList()
    {
        return prohibitionMapper.queryAllTempList();
    }

    @SystemServiceLog(description="更新禁运物品")
    @Override
    public void updateProhibition(Prohibition prohibition)
    {
        prohibitionMapper.updateProhibition(prohibition);
    }

    @SystemServiceLog(description="新增禁运物品")
    @Override
    public void addProhibition(Prohibition prohibition)
    {
        prohibitionMapper.addProhibition(prohibition);
    }

    @SystemServiceLog(description="删除禁运物品")
    @Override
    public void deleteProhibition(int prohibition_id)
    {
        prohibitionMapper.deleteProhibition(prohibition_id);
    }

    @Override
    public void tempProhibitionDelete()
    {
        prohibitionMapper.deleteProhibitionTemp();
    }

    @Override
    public void tempProhibitionAdd(Prohibition tempProhibition)
    {
        prohibitionMapper.addProhibition(tempProhibition);
    }

    @Override
    public void tempProhibitionDelete(int prohibition_id)
    {
        prohibitionMapper.deleteProhibition(prohibition_id);
    }
}
