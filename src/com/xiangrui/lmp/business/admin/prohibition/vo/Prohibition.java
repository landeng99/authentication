package com.xiangrui.lmp.business.admin.prohibition.vo;

import com.xiangrui.lmp.business.base.BaseProhibition;

/**
 * 禁运物品
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-11 上午10:59:23</p>
 */
public class Prohibition extends BaseProhibition
{

    private static final long serialVersionUID = 1L;

    /**
     * 解析chiid_cla_group成页面样式
     * @return
     */
    public String[] getChiidClaseName(){
        return this.getChild_cla_group().split(";");
    }
    
    /**
     * 截取简介描述文字的前面一段文字
     * @return
     */
    public String getNewDesccription(){
        if(this.getDescription().length() > 20){
            return this.getDescription().substring(0,20)+"...";
        }else{
            return this.getDescription();
        }
    }
}
