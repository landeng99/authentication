package com.xiangrui.lmp.business.admin.weixunPush.vo;

public class WeiXunPushRequestBean
{
	// 用户微信的openid
	private String openid;
	// 推送内容
	private String msg;

	public String getOpenid()
	{
		return openid;
	}

	public void setOpenid(String openid)
	{
		this.openid = openid;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

}
