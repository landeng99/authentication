package com.xiangrui.lmp.business.admin.weixunPush.vo;

import com.xiangrui.lmp.business.base.BaseWeiXunPushSetting;

public class WeiXunPushSetting extends BaseWeiXunPushSetting
{
	private static final long serialVersionUID = 1L;

	public boolean checkCanPushWeiXunMessage(int package_status)
	{
		boolean canPush = false;
		// 包裹状态，-1：异常，0：待入库，1：已入库，2：待发货，3：已出库，4：空运中，5：待清关，6：清关中，7已清关并已派件中，9：已收货；
		// 20：废弃，21：退货

		if ("Y".equalsIgnoreCase(this.getAllow_input()) && package_status == 1)
		{
			canPush = true;
		}
		if ("Y".equalsIgnoreCase(this.getAllow_output()) && package_status == 3)
		{
			canPush = true;
		}
		if ("Y".equalsIgnoreCase(this.getAllow_delivery()) && package_status == 7)
		{
			canPush = true;
		}
		if ("Y".equalsIgnoreCase(this.getAllow_sign()) && package_status == 9)
		{
			canPush = true;
		}
		return canPush;
	}
}
