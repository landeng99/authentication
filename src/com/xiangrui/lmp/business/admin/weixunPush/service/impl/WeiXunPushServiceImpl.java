package com.xiangrui.lmp.business.admin.weixunPush.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.weixunPush.mapper.WeiXunPushLogMapper;
import com.xiangrui.lmp.business.admin.weixunPush.mapper.WeiXunPushSettingMapper;
import com.xiangrui.lmp.business.admin.weixunPush.service.WeiXunPushService;
import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushLog;
import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushSetting;
@Service(value = "weiXunPushService")
public class WeiXunPushServiceImpl implements WeiXunPushService
{
	@Autowired
	WeiXunPushLogMapper weiXunPushLogMapper;
	@Autowired
	WeiXunPushSettingMapper weiXunPushSettingMapper;

	@Override
	public void saveOrUpdateWeiXunPushSetting(WeiXunPushSetting weiXunPushSetting)
	{
		WeiXunPushSetting existWeiXunPushSetting = weiXunPushSettingMapper.queryByOpenId(weiXunPushSetting.getOpen_id());
		if (existWeiXunPushSetting != null)
		{
			String allow_delivery = StringUtils.trimToNull(weiXunPushSetting.getAllow_delivery());
			if (allow_delivery != null)
			{
				existWeiXunPushSetting.setAllow_delivery(allow_delivery);
			}
			String allow_input = StringUtils.trimToNull(weiXunPushSetting.getAllow_input());
			if (allow_input != null)
			{
				existWeiXunPushSetting.setAllow_input(allow_input);
			}
			String allow_output = StringUtils.trimToNull(weiXunPushSetting.getAllow_output());
			if (allow_output != null)
			{
				existWeiXunPushSetting.setAllow_output(allow_output);
			}
			String allow_sign = StringUtils.trimToNull(weiXunPushSetting.getAllow_sign());
			if (allow_delivery != null)
			{
				existWeiXunPushSetting.setAllow_sign(allow_sign);
			}
			weiXunPushSettingMapper.updateByOpenId(existWeiXunPushSetting);
		}
		else
		{
			weiXunPushSettingMapper.add(weiXunPushSetting);
		}

	}

	@Override
	public int saveWeiXunPushLog(WeiXunPushLog weiXunPushLog)
	{
		return weiXunPushLogMapper.add(weiXunPushLog);
	}

	/**
	 * 根据open_id查看微信推送设置
	 * 
	 * @param open_id
	 * @return
	 */
	public WeiXunPushSetting queryWeiXunPushSettingByOpenId(String open_id)
	{
		return weiXunPushSettingMapper.queryByOpenId(open_id);
	}

}
