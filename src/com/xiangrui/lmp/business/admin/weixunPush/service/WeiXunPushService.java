package com.xiangrui.lmp.business.admin.weixunPush.service;

import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushLog;
import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushSetting;

public interface WeiXunPushService
{
	/**
	 * 添加｜更新个人微信推送设置
	 * 
	 * @param weiXunPushSetting
	 */
	public void saveOrUpdateWeiXunPushSetting(WeiXunPushSetting weiXunPushSetting);

	/**
	 * 保存微信推送记录
	 * 
	 * @param weiXunPushLog
	 * @return
	 */
	public int saveWeiXunPushLog(WeiXunPushLog weiXunPushLog);

	/**
	 * 根据open_id查看微信推送设置
	 * 
	 * @param open_id
	 * @return
	 */
	public WeiXunPushSetting queryWeiXunPushSettingByOpenId(String open_id);
}
