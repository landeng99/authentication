package com.xiangrui.lmp.business.admin.weixunPush.mapper;

import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushSetting;

public interface WeiXunPushSettingMapper
{

	public WeiXunPushSetting queryByOpenId(String open_id);

	public int add(WeiXunPushSetting weiXunPushSetting);

	public int updateByOpenId(WeiXunPushSetting weiXunPushSetting);
}
