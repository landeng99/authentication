package com.xiangrui.lmp.business.admin.weixunPush.mapper;

import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushLog;

public interface WeiXunPushLogMapper
{
	public int add(WeiXunPushLog weiXunPushLog);
}
