package com.xiangrui.lmp.business.admin.notice.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.notice.mapper.NoticeMapper;
import com.xiangrui.lmp.business.admin.notice.service.NoticeService;
import com.xiangrui.lmp.business.admin.notice.vo.Notice;

/**
 * 公告操作实现
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午4:11:18
 * </p>
 */
@Service("noticeService")
public class NoticeServiceImpl implements NoticeService
{

    /**
     * 公告数据
     */
    @Autowired
    private NoticeMapper noticeMapper;

    /**
     * 查询公告
     */
    @Override
    public List<Notice> queryAll(Map<String, Object> params)
    {
        return noticeMapper.queryAll(params);
    }

    /**
     * 新增
     */
    @SystemServiceLog(description = "新增文章")
    @Override
    public void addNotice(Notice notice)
    {
        noticeMapper.addNotice(notice);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改文章")
    @Override
    public void updateNotice(Notice notice)
    {
        noticeMapper.updateNotice(notice);
    }

    /**
     * 删除
     */
    @SystemServiceLog(description = "删除文章")
    @Override
    public void deleteNotice(int id)
    {
        noticeMapper.deleteNotice(id);
    }

    /**
     * 统计公告总数据数目
     */
    @Override
    public int queryCount()
    {
        return noticeMapper.queryCount();
    }

    /**
     * 查询具体某条公告,id
     */
    @Override
    public Notice queryAllId(int id)
    {
        return noticeMapper.queryAllId(id);
    }

    @Override
    public List<Notice> queryAllHomepage()
    {
        return noticeMapper.queryAllHomepage();
    }
    
    @Override
    public Notice queryLastNotice()
    {
    	Notice lastNotice=null;
    	List<Notice> list=noticeMapper.queryAllHomepage();
    	if(list!=null&&list.size()>0)
    	{
    		lastNotice=list.get(0);
    	}
    	return lastNotice;
    }

    public List<Notice> queryAllHomepage5()
    {
    	return noticeMapper.queryAllHomepage5();
    }

    public List<Notice> queryAllHomepage6()
    {
    	return noticeMapper.queryAllHomepage6();
    }
}
