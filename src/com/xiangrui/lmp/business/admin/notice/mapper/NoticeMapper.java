package com.xiangrui.lmp.business.admin.notice.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.notice.vo.Notice;

/**
 * 公告数据
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午4:09:32
 *         </p>
 */
public interface NoticeMapper
{

    /**
     * 查询所有公告
     * 
     * @param params
     * @return
     */
    List<Notice> queryAll(Map<String, Object> params);

    /**
     * 查询所有没时禁用的公告
     * 
     * @return
     */
    List<Notice> queryAllHomepage();

    /**
     * 查询单个公告,id编号
     * 
     * @param id
     * @return
     */
    Notice queryAllId(int id);

    /**
     * 新增公告
     * 
     * @param Notice
     */
    void addNotice(Notice notice);

    /**
     * 修改公告
     * 
     * @param Notice
     */
    void updateNotice(Notice notice);

    /**
     * 删除公告
     * 
     * @param id
     */
    void deleteNotice(int id);

    /**
     * 公告总数
     * 
     * @return
     */
    int queryCount();

    List<Notice> queryAllHomepage5();

    List<Notice> queryAllHomepage6();
}
