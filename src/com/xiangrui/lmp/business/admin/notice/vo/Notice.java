package com.xiangrui.lmp.business.admin.notice.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.xiangrui.lmp.business.base.BaseNotice;
import com.xiangrui.lmp.util.ZTEHtmlUtil;

/**
 * 公告实体
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午4:10:13
 * </p>
 */
public class Notice extends BaseNotice
{

    private static final long serialVersionUID = 1L;

    private String effectShortYear;
    
    private String effectShortDate;
    
    private String ttshortContent;
    
    
    public String getTtshortContent()
	{
    	String longContent=StringEscapeUtils.escapeHtml(getContext());
    	longContent=ZTEHtmlUtil.getPureText(longContent);
    	if(longContent!=null&&longContent.length()>300)
    	{
    		longContent=longContent.substring(0, 300);
    		longContent+="......";
    	}
		return longContent;
	}


	public String getEffectShortYear()
	{
    	String str=null;
    	String tempeffectTime=super.getEffectTime();
    	if(tempeffectTime!=null)
    	{
    		SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		SimpleDateFormat simpleDateFormat1= new SimpleDateFormat("yyyy");
    		try
			{
				Date dd=simpleDateFormat.parse(tempeffectTime);
				str=simpleDateFormat1.format(dd);
			} catch (ParseException e)
			{
				e.printStackTrace();
			}
    	}
		return str;
	}


	public void setEffectShortYear(String effectShortYear)
	{
		this.effectShortYear = effectShortYear;
	}


	public String getEffectShortDate()
	{
    	String str=null;
    	String tempeffectTime=super.getEffectTime();
    	if(tempeffectTime!=null)
    	{
    		SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		SimpleDateFormat simpleDateFormat1= new SimpleDateFormat("MM-dd");
    		try
			{
				Date dd=simpleDateFormat.parse(tempeffectTime);
				str=simpleDateFormat1.format(dd);
			} catch (ParseException e)
			{
				e.printStackTrace();
			}
    	}
		return str;
	}


	public void setEffectShortDate(String effectShortDate)
	{
		this.effectShortDate = effectShortDate;
	}


	/**
     * 仅仅读取公告内容的前面20个字符串内容
     * 
     * @return
     */
    public String getContextProfile()
    {
        if (this.getContext().length() >= 20)
        {
            return this.getContext().substring(0, 20);
        } else
        {
            return this.getContext();
        }
    }
}