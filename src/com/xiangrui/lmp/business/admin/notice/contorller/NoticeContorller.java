package com.xiangrui.lmp.business.admin.notice.contorller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.notice.service.NoticeService;
import com.xiangrui.lmp.business.admin.notice.vo.Notice;
import com.xiangrui.lmp.util.PageView;

/**
 * 公告处理
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午4:06:17
 *         </p>
 */
@Controller
@RequestMapping("/admin/notice")
public class NoticeContorller
{

    /**
     * 公告处理
     */
    @Autowired
    private NoticeService noticeService;

    /**
     * 首次查询
     * 
     * @return
     */
    @RequestMapping("/queryAllOne")
    public String queryAllOne(HttpServletRequest request)
    {
        return noticePage(request, null);
    }

    /**
     * 分页查询所有公告链接
     * 
     * @return
     */
    @RequestMapping("/queryAll")
    public String noticePage(HttpServletRequest request, Notice notice)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);

        List<Notice> list = noticeService.queryAll(params);
        request.setAttribute("pageView", pageView);
        request.setAttribute("noticeLists", list);
        return "back/noticeList";
    }

    /**
     * 更新公告 仅仅是公告的启用和禁用状态的转换
     * 
     * @param notice
     * @return
     */
    @RequestMapping("/updNotice")
    public String updNotice(HttpServletRequest request, Notice notice)
    {
        notice = noticeService.queryAllId(notice.getNoticeId());

        // 状态0和1的转换 (0+1)%2=1,(1+1)%2=0;
        notice.setStatus((notice.getStatus() + 1) % 2);
        noticeService.updateNotice(notice);

        // 更新前台公告先的application
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("noticeLists",
                noticeService.queryAllHomepage());

        return noticePage(request, null);
    }

    /**
     * 进入更新或者新增公告界面,也是详细界面
     * 
     * @param request
     * @param notice
     * @return
     */
    @RequestMapping("/toUpdateNoticeOne")
    public String toUpdateNoticeOne(HttpServletRequest request, Notice notice)
    {
        // 有id是,查到对应的公告信息显示到界面
        notice = noticeService.queryAllId(notice.getNoticeId());
        request.setAttribute("noticePojo", notice);
        return "back/updateNotice";
    }

    /**
     * 进入更新或者新增公告界面,也是详细界面
     * 
     * @param request
     * @param notice
     * @return
     */
    @RequestMapping("/toUpdateNotice")
    public String toUpdateNotice(HttpServletRequest request, Notice notice)
    {
        // 当没有id时,直接进入添加公告的方式
        if (0 == notice.getNoticeId())
        {
            request.setAttribute("noticePojo", new Notice());
            return "back/updateNotice";
        } else
        {

            // 有id是,查到对应的公告信息显示到界面
            notice = noticeService.queryAllId(notice.getNoticeId());
            request.setAttribute("noticePojo", notice);
            return "back/updateNoticeNew";
        }
    }

    /**
     * 更新公告 新增公告
     * 
     * @param notice
     *            有id为更新
     * @return
     */
    @RequestMapping("/saveNotice")
    public String saveNotice(HttpServletRequest request, Notice notice)
    {
        // 除去前后位置多余的空格,公告文本输入空格问题
        notice.setContext(notice.getContext().trim());

        // 是否有id,是则修改,否则新增
        if (0 < notice.getNoticeId())
        {
            noticeService.updateNotice(notice);
        } else
        {
            // 创建日期
            notice.setCreate_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .format(new Date()));

            // 添加
            noticeService.addNotice(notice);
        }

        // 更新前台公告先的application
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("noticeLists",
                noticeService.queryAllHomepage());

        return noticePage(request, null);
    }

    /**
     * 删除公告
     * 
     * @param notice
     *            有id为更新
     * @return
     */
    @RequestMapping("/deleteNotice")
    public String deleteNotice(HttpServletRequest request, Notice notice)
    {
        noticeService.deleteNotice(notice.getNoticeId());

        // 更新前台公告先的application
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("noticeLists",
                noticeService.queryAllHomepage());

        return "back/noticeList";
    }
}
