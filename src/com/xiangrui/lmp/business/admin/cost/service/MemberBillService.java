package com.xiangrui.lmp.business.admin.cost.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.cost.vo.MemberBillModel;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

public interface MemberBillService
{

    /**
     * 同行用户待合计账单查询
     * @param frontUser
     * @return
     */
    List<FrontUser> queryFrontUserBill(Map<String, Object> params);
    
    /**
     * 多用户账单明细
     * @param userIds
     * @return
     */
    List<MemberBillModel> queryFrontUserBillByuserIds(List<String> userIds);
    
    MemberBillModel queryFrontUserBillByPackageId(Integer package_id);
    MemberBillModel queryMemberBillModelByPackageId(Integer package_id);
    /**
     *  同行账单导入查询
     * @param list
     * @return
     */
    List<Pkg> selectPackageBylogisticsCodes(List<String> list);

}
