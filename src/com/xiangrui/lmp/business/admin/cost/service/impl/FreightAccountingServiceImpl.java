package com.xiangrui.lmp.business.admin.cost.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xiangrui.lmp.business.admin.cost.mapper.FreightAccountingDetailMapper;
import com.xiangrui.lmp.business.admin.cost.mapper.FreightAccountingMapper;
import com.xiangrui.lmp.business.admin.cost.service.FreightAccountingService;
import com.xiangrui.lmp.business.admin.cost.vo.FreightAccounting;
import com.xiangrui.lmp.business.admin.cost.vo.FreightAccountingDetail;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.user.vo.User;

@Service(value = "freightAccountingService")

public class FreightAccountingServiceImpl implements FreightAccountingService
{

    @Autowired
    private FreightAccountingMapper freightAccountingMapper;

    @Autowired
    private FreightAccountingDetailMapper freightAccountingDetailMapper;

    @Autowired
    private PackageMapper packageMapper;

    /**
     * 同行账单导入
     * 
     * @param freightAccounting
     * @param freightAccountingDetailList
     * @param pkgList
     * @param user
     * @return
     * @throws Exception 
     */
    @Override
    @Transactional
    public int insertFreightAccounting(FreightAccounting freightAccounting,
            List<FreightAccountingDetail> freightAccountingDetailList,
            List<Pkg> pkgList, User user)
    {
        int countA = freightAccountingMapper
                .insertFreightAccounting(freightAccounting);
        
        for(FreightAccountingDetail freightAccountingDetail:freightAccountingDetailList){
            
            freightAccountingDetail.setBatch_id(freightAccounting.getBatch_id());
        }

        int countB = freightAccountingDetailMapper
                .batchInsertFreightAccountingDetail(freightAccountingDetailList);
             
        int countC = packageMapper.batchUpdatePayStatus(pkgList);
//        for (Pkg pkg : pkgList)
//        {
//
//            PackageLogUtil.log(pkg, user);
//        }

        if (countA < 1 || countB < 1 || countC < 1)
        {
            return 0;
        }

        return countA + countB + countC;
    }

}
