package com.xiangrui.lmp.business.admin.cost.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.cost.service.MemberBillService;
import com.xiangrui.lmp.business.admin.cost.vo.MemberBillModel;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

@Service(value = "memberBillService")
public class MemberBillServiceImpl implements MemberBillService
{

    @Autowired
    private FrontUserMapper frontUserMapper;
    
    @Autowired
    private PackageMapper packageMapper;

    /**
     * 同行用户待合计账单查询
     * 
     * @param frontUser
     * @return
     */
    @Override
    public List<FrontUser> queryFrontUserBill(Map<String, Object> params)
    {
        return frontUserMapper.queryFrontUserBill(params);
    }

    /**
     * 多用户账单明细
     * 
     * @param userIds
     * @return
     */
    @Override
    public List<MemberBillModel> queryFrontUserBillByuserIds(
            List<String> userIds)
    {

        return frontUserMapper.queryFrontUserBillByuserIds(userIds);
    }
    public MemberBillModel queryFrontUserBillByPackageId(Integer package_id){
    	return frontUserMapper.queryFrontUserBillByPackageId(package_id);
    }
    
    public MemberBillModel queryMemberBillModelByPackageId(Integer package_id){
    	return frontUserMapper.queryMemberBillModelByPackageId(package_id);
    }

    /**
     *  同行账单导入查询
     * @param list
     * @return
     */
    @Override
    public List<Pkg> selectPackageBylogisticsCodes(List<String> list)
    {

        return packageMapper.selectPackageBylogisticsCodes(list);
    }

}
