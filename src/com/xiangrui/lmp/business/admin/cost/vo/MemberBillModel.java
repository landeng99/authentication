package com.xiangrui.lmp.business.admin.cost.vo;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.util.StringUtil;

/**
 * 同行账单导出
 * 
 * @author hyh
 * 
 */

public class MemberBillModel
{

    /**
     * 创建包裹时间
     */
    private Timestamp createTime;

    /**
     * 公司运单号
     */
    private String logistics_code;

    /**
     * 包裹创建用户名称
     */
    private String user_name;

    /**
     * 商品名称以及数量
     */
    private String goods_name;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 包裹重量
     */
    private float actual_weight;

    /**
     * 重量(收费/磅)
     */
    private float weight;

    /**
     * 单价
     */
    private float price;

    /**
     * 运费
     */
    private float freight;

    /**
     * 税费
     */
    private float customs_cost;

    /**
     * 所有增值服务
     */
    private String attach_service;

    /**
     * 合计
     */
    private float transport_cost;
    
    /**
     * 支付状态
     */
    private int pay_status;
    private int pay_status_freight;
    private int pay_status_custom;

    /**
     * 增值服务Map<名称，价格>
     */
    private Map<String, Float> attach_service_map;

    /**
     * 商品申报类别
     */
    private String goods_type;
    
    /**
     * 申报总价值
     */
    private float total_worth;
    
    /**
     * 海外仓库名
     */
    private String warehouse;
    /**
     * 业务名
     */
    private String business_name;
    /**
     * 包裹清关时间
     */
    private Timestamp operate_time;
    
    /**
     * 快件包裹 0：不是 1：是
     */
    private int express_package;
    
    public int getExpress_package()
	{
		return express_package;
	}

	public void setExpress_package(int express_package)
	{
		this.express_package = express_package;
	}

	public Timestamp getOperate_time()
	{
		return operate_time;
	}

	public void setOperate_time(Timestamp operate_time)
	{
		this.operate_time = operate_time;
	}
	public String getBusiness_name()
	{
		return business_name;
	}

	public void setBusiness_name(String business_name)
	{
		this.business_name = business_name;
	}

	public float getTotal_worth()
	{
		return total_worth;
	}

	public void setTotal_worth(float total_worth)
	{
		this.total_worth = total_worth;
	}

	public String getWarehouse()
	{
		return warehouse;
	}

	public void setWarehouse(String warehouse)
	{
		this.warehouse = warehouse;
	}

	public Timestamp getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime)
    {
        this.createTime = createTime;
    }

    public String getLogistics_code()
    {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }

    public String getUser_name()
    {
        return user_name;
    }

    public void setUser_name(String user_name)
    {
        this.user_name = user_name;
    }

    public String getGoods_name()
    {
        return goods_name;
    }

    public void setGoods_name(String goods_name)
    {
        this.goods_name = goods_name;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public float getWeight()
    {
        return weight;
    }

    public void setWeight(float weight)
    {
        this.weight = weight;
    }


    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public float getFreight()
    {
        return freight;
    }

    public void setFreight(float freight)
    {
        this.freight = freight;
    }

    public float getCustoms_cost()
    {
        return customs_cost;
    }

    public void setCustoms_cost(float customs_cost)
    {
        this.customs_cost = customs_cost;
    }

    public String getAttach_service()
    {
        return attach_service;
    }

    public void setAttach_service(String attach_service)
    {
        this.attach_service = attach_service;
        // 字符串装成Map
        setAttach_service_map(attach_service);

    }

    public Map<String, Float> getAttach_service_map()
    {
        return attach_service_map;
    }

    /**
     * 字符串装成Map
     * 
     * @param attach_service
     */
    private void setAttach_service_map(String attach_service)
    {
        List<String> attachList = StringUtil.toStringList(attach_service, ",");

        Map<String, Float> map = new HashMap<String, Float>();

        for (String attach : attachList)
        {

            if (StringUtil.isEmpty(attach))
            {
                continue;
            }
            List<String> attachArray = StringUtil.toStringList(attach, "|");

            if (attachArray.size() > 1)
            {
                map.put(attachArray.get(0),
                        Float.parseFloat(attachArray.get(1)));
            } else
            {
                map.put(attachArray.get(0), 0f);
            }

        }
        this.attach_service_map = map;
    }

    public float getTransport_cost()
    {
        return transport_cost;
    }

    public void setTransport_cost(float transport_cost)
    {
        this.transport_cost = transport_cost;
    }

    public float getActual_weight()
    {
        return actual_weight;
    }

    public void setActual_weight(float actual_weight)
    {
        this.actual_weight = actual_weight;
    }

    public int getPay_status()
    {
        return pay_status;
    }

    public void setPay_status(int pay_status)
    {
        this.pay_status = pay_status;
    }

	public String getGoods_type()
	{
		return goods_type;
	}

	public void setGoods_type(String goods_type)
	{
		this.goods_type = goods_type;
	}

	public int getPay_status_freight() {
		return pay_status_freight;
	}

	public void setPay_status_freight(int pay_status_freight) {
		this.pay_status_freight = pay_status_freight;
	}

	public int getPay_status_custom() {
		return pay_status_custom;
	}

	public void setPay_status_custom(int pay_status_custom) {
		this.pay_status_custom = pay_status_custom;
	}

}
