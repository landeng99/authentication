package com.xiangrui.lmp.business.admin.cost.vo;

import com.xiangrui.lmp.business.base.BaseFreightAccountingDetail;

/**
 * 同行账单导入导出
 * 
 * @author hyh
 * 
 */

public class FreightAccountingDetail extends BaseFreightAccountingDetail
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
