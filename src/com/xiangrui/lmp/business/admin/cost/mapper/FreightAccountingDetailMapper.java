package com.xiangrui.lmp.business.admin.cost.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.cost.vo.FreightAccountingDetail;

public interface FreightAccountingDetailMapper
{

    /**
     * 同行账单导入详情
     */
    int batchInsertFreightAccountingDetail(List<FreightAccountingDetail> list);
}
