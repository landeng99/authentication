package com.xiangrui.lmp.business.admin.cost.mapper;

import com.xiangrui.lmp.business.admin.cost.vo.FreightAccounting;

public interface FreightAccountingMapper
{
    /**
     * 同行账单导入
     */
    int insertFreightAccounting(FreightAccounting freightAccounting);

}
