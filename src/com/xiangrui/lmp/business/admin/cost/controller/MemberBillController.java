package com.xiangrui.lmp.business.admin.cost.controller;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.cost.service.FreightAccountingService;
import com.xiangrui.lmp.business.admin.cost.service.MemberBillService;
import com.xiangrui.lmp.business.admin.cost.vo.FreightAccounting;
import com.xiangrui.lmp.business.admin.cost.vo.FreightAccountingDetail;
import com.xiangrui.lmp.business.admin.cost.vo.MemberBillModel;
import com.xiangrui.lmp.business.admin.message.service.MessageSendService;
import com.xiangrui.lmp.business.admin.message.vo.UserSendEmailBean;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.init.AppServerUtil;
import com.xiangrui.lmp.util.IdentifierCreator;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.newExcel.ExcelReadUtil;

/**
 * 包裹同行账单
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/admin/memberbill")
public class MemberBillController
{

	Logger logger = Logger.getLogger(MemberBillController.class);

	@Autowired
	private MemberBillService memberBillService;

	@Autowired
	private AttachServiceService attachServiceService;

	@Autowired
	private FreightAccountingService freightAccountingService;

	// 发送Email｜SMS服务
	@Autowired
	private MessageSendService messageSendService;
	@Autowired
	private PackageService packageService;
	@Autowired
	private FrontUserService frontUserService;

	private static final String RELATIVELY_PATH = "resource" + File.separator + "upload" + File.separator;

	/**
	 * 上传图片在服务器存放路径
	 */
	private static final String UPLOAD_IMG_PATH = AppServerUtil.getWebRoot() + RELATIVELY_PATH;

	/**
	 * 时间格式化
	 */
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Excel导入信息
	 */
	private static final String SHEET_NAME = "同行账单";

	/**
	 * Excel 列名 包裹创建日期
	 */
	private static final String COLNAME_CREATE_TIME = "包裹创建日期";

	/**
	 * Excel列名 公司运单号
	 */
	private static final String COLNAME_LOGISTICSCODE = "公司运单号";

	/**
	 * Excel 列名 用户名称
	 */
	private static final String COLNAME_USER_NAME = "用户名称";

	/**
	 * Excel 列名 品名
	 */
	private static final String COLNAME_GOODS_NAME = "品名";

	/**
	 * Excel 列名 品牌
	 */
	private static final String COLNAME_BRAND = "品牌";

	/**
	 * Excel 列名 重量(磅)
	 */
	private static final String COLNAME_ACTUAL_WEIGHT = "重量(磅)";

	/**
	 * Excel 列名 重量(收费/磅)
	 */
	private static final String COLNAME_WEIGHT = "重量(收费/磅)";

	/**
	 * Excel 列名 单价(RMB)
	 */
	private static final String COLNAME_PRICE = "单价(RMB)";

	/**
	 * Excel 列名 运费(RMB)
	 */
	private static final String COLNAME_FREIGHT = "运费(RMB)";

	/**
	 * Excel 列名 合计(RMB)
	 */
	private static final String COLNAME_TRANSPORT_COST = "合计(RMB)";

	/**
	 * Excel列番号初始化 ：一个不存在的列番号
	 */
	private static final int COL_NUMBER_INIT = 9999999;

	/**
	 * 增值服务起始列
	 */
	private static final int ATTACHSERVICE_START_INDEX = 13;

	/**
	 * 导入账单运单号有误
	 */
	private static final String LOGISTICSCODE_ERROR = "LOGISTICSCODE_ERROR";

	/**
	 * 导入账单 有负数
	 */
	private static final String MINUS_ERROR = "MINUS_ERROR";

	/**
	 * Excel读取内容格式错误
	 */
	private static final String EXCEL_CONTENT_ERROR = "1";

	/**
	 * 不是Excel文件
	 */
	private static final String IS_NOT_EXCEL = "2";

	/**
	 * 订单号有误
	 */
	private static final String LIST_ERROR = "3";

	/**
	 * 订单号正常
	 */
	private static final String LOGISTICSCODE_NORMAL = "0";

	/**
	 * 订单号重复
	 */
	private static final String LOGISTICSCODE_REPEAT = "1";

	/**
	 * 订单号已支付
	 */
	private static final String LOGISTICSCODE_PAID = "2";

	/**
	 * 订单号不存在
	 */
	private static final String LOGISTICSCODE_NOT_EXIST = "3";

	/**
	 * 包裹还没到库
	 */
	private static final String LOGISTICSCODE_NOT_ARRIVED = "4";

	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

	/**
	 * 导入导出排他锁
	 */
	private static boolean import_export_lock = false;

	/**
	 * 导入导出排他等待时间 毫秒
	 */
	private static int WAITING_MILLISECOND = 2000;

	/**
	 * 初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/list")
	public String memberbilllist(HttpServletRequest request)
	{

		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);

		// params.put("accounting_status", Pkg.ACCOUNTING_UNDO);

		List<FrontUser> billList = memberBillService.queryFrontUserBill(params);

		request.setAttribute("billList", billList);

		request.setAttribute("pageView", pageView);

		return "/back/memberbilllist";
	}

	/**
	 * 查询
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/search")
	public String search(HttpServletRequest request, String account, String user_name)
	{

		// 分页查询
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		}
		else
		{
			pageView = new PageView(1);

			params.put("account", account);
			try
			{
				params.put("user_name", new String(user_name.getBytes("ISO-8859-1"), "UTF-8"));
			} catch (UnsupportedEncodingException e)
			{

				e.printStackTrace();
			}
			/* params.put("accounting_status", Pkg.ACCOUNTING_UNDO); */

			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}

		params.put("pageView", pageView);

		List<FrontUser> billList = memberBillService.queryFrontUserBill(params);

		request.setAttribute("billList", billList);
		request.setAttribute("pageView", pageView);
		request.setAttribute("params", params);

		return "/back/memberbilllist";
	}

	/**
	 * 详情
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/detail")
	public String batchlist(HttpServletRequest request)
	{
		return "/back/batchlist";
	}

	/**
	 * 导出同行账单
	 * 
	 * @param request
	 * @param response
	 * @param userIds
	 */

	@RequestMapping("/exportList")
	public void exportList(HttpServletRequest request, HttpServletResponse response, String userIds)
	{
		waiting();

		try
		{
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "同行账单";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");

			List<MemberBillModel> billList = memberBillService.queryFrontUserBillByuserIds(StringUtil.toStringList(userIds, ","));

			XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

			// 新建sheet
			XSSFSheet xssfSheet = xssfWorkbook.createSheet(SHEET_NAME);
			// 第一列固定
			xssfSheet.createFreezePane(0, 1, 0, 1);

			// 颜色黄色
			XSSFColor yellowColor = new XSSFColor(Color.YELLOW);

			// 样式居中
			XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
			style1.setAlignment(XSSFCellStyle.ALIGN_CENTER);

			// 样式黄色居中
			XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
			style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			style2.setFillForegroundColor(yellowColor);
			style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			style2.setWrapText(true);

			// 列宽
			xssfSheet.setColumnWidth(0, 3500);
			xssfSheet.setColumnWidth(1, 4500);
			xssfSheet.setColumnWidth(2, 4500);
			xssfSheet.setColumnWidth(3, 4500);
			xssfSheet.setColumnWidth(4, 4500);
			xssfSheet.setColumnWidth(5, 8000);
			xssfSheet.setColumnWidth(6, 8000);
			xssfSheet.setColumnWidth(7, 3500);
			xssfSheet.setColumnWidth(8, 3500);
			xssfSheet.setColumnWidth(9, 3500);

			// 增值服务
			List<String> serviceNameList = attachServiceService.queryAttachServiceName();

			// 表头列
			XSSFRow firstXSSFRow = xssfSheet.createRow(0);

			List<String> columnsList = new ArrayList<String>();
			columnsList.add(COLNAME_CREATE_TIME);
			columnsList.add(COLNAME_LOGISTICSCODE);
			columnsList.add("所属仓库");
			columnsList.add(COLNAME_USER_NAME);
			columnsList.add("业务");
			columnsList.add(COLNAME_GOODS_NAME);
			columnsList.add(COLNAME_BRAND);
			columnsList.add("申报类别");
			columnsList.add("申报总价值(RMB)");
			columnsList.add(COLNAME_ACTUAL_WEIGHT);
			columnsList.add(COLNAME_WEIGHT);
			columnsList.add(COLNAME_PRICE);
			columnsList.add(COLNAME_FREIGHT);
			// 增值服务
			columnsList.addAll(serviceNameList);
			columnsList.add(COLNAME_TRANSPORT_COST);
			columnsList.add("是否快件包裹");

			for (int i = 0; i < columnsList.size(); i++)
			{
				XSSFCell cell = firstXSSFRow.createCell(i);
				cell.setCellType(XSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(columnsList.get(i));
				if ((i > 9 && i < ATTACHSERVICE_START_INDEX) || i == columnsList.size() - 1)
				{
					cell.setCellStyle(style1);
				}
				else
				{
					cell.setCellStyle(style2);
				}

			}

			for (int j = 0; j < billList.size(); j++)
			{
				XSSFRow row = xssfSheet.createRow(j + 1);

				// 包裹创建日期
				XSSFCell cell0 = row.createCell(0);
				cell0.setCellValue(simpleDateFormat.format(billList.get(j).getCreateTime()));
				cell0.setCellStyle(style2);

				// 公司运单号
				XSSFCell cell1 = row.createCell(1);
				cell1.setCellValue(billList.get(j).getLogistics_code());
				cell1.setCellStyle(style2);

				// 所属仓库
				XSSFCell cell2 = row.createCell(2);
				cell2.setCellValue(billList.get(j).getWarehouse());
				cell2.setCellStyle(style2);

				// 用户名称
				XSSFCell cell3 = row.createCell(3);
				cell3.setCellValue(billList.get(j).getUser_name());
				cell3.setCellStyle(style2);

				// 业务
				XSSFCell cell4 = row.createCell(4);
				cell4.setCellValue(billList.get(j).getBusiness_name());
				cell4.setCellStyle(style2);

				// 品名
				XSSFCell cell5 = row.createCell(5);
				cell5.setCellValue(billList.get(j).getGoods_name());
				cell5.setCellStyle(style2);

				// 品牌
				XSSFCell cell6 = row.createCell(6);
				cell6.setCellValue(billList.get(j).getBrand());
				cell6.setCellStyle(style2);

				// 申报类别
				XSSFCell cell7 = row.createCell(7);
				cell7.setCellValue(billList.get(j).getGoods_type());
				cell7.setCellStyle(style2);

				// 申报总价值(RMB)
				XSSFCell cell8 = row.createCell(8);
				cell8.setCellValue(NumberUtils.scaleMoneyData(billList.get(j).getTotal_worth()));
				cell8.setCellStyle(style2);

				// 重量(磅)
				XSSFCell cell9 = row.createCell(9);
				cell9.setCellValue(Double.parseDouble(String.valueOf(billList.get(j).getActual_weight())));
				cell9.setCellStyle(style2);

				// 重量(收费/磅)
				XSSFCell cell10 = row.createCell(10);
				cell10.setCellValue(Double.parseDouble(String.valueOf(billList.get(j).getWeight())));
				cell10.setCellStyle(style1);

				// 单价(RMB)
				XSSFCell cell11 = row.createCell(11);
				cell11.setCellValue(NumberUtils.scaleMoneyData(billList.get(j).getPrice()));
				cell11.setCellStyle(style1);

				// 运费(RMB)
				XSSFCell cell12 = row.createCell(12);
				cell12.setCellValue(NumberUtils.scaleMoneyData(billList.get(j).getFreight()));
				cell12.setCellStyle(style1);

				// 增值服务
				for (int k = 0; k < serviceNameList.size(); k++)
				{

					XSSFCell attachServiceCell = row.createCell(ATTACHSERVICE_START_INDEX + k);
					attachServiceCell.setCellStyle(style2);

					Map<String, Float> map = billList.get(j).getAttach_service_map();
					if (map != null && !map.isEmpty())
					{
						Float price = map.get(serviceNameList.get(k));
						if (price != null && price.compareTo(0.000001f) > 0)
						{
							attachServiceCell.setCellValue(Double.parseDouble(price.toString()));
						}
					}

				}
				// 合计(RMB)
				XSSFCell lastCell = row.createCell(columnsList.size() - 2);
				// 第二次导出
//				if (Pkg.PAYMENT_UNPAID == billList.get(j).getPay_status())
				if (Pkg.PAYMENT_FREIGHT_UNPAID == billList.get(j).getPay_status_freight())
				{
					lastCell.setCellValue(NumberUtils.scaleMoneyData(billList.get(j).getTransport_cost()));
				}
				else
				{
					lastCell.setCellValue(Double.parseDouble(String.valueOf(0)));
				}

				lastCell.setCellStyle(style1);
				
				//是否快件包裹
				XSSFCell lastTCell = row.createCell(columnsList.size() - 1);
				//包裹渠道
				int i = billList.get(j).getExpress_package();
				switch(i){
					case 1:
						lastTCell.setCellValue("A");
						break;
					case 2:
						lastTCell.setCellValue("B");
						break;
					case 3:
						lastTCell.setCellValue("C");
						break;
					case 4:
						lastTCell.setCellValue("D");
						break;
					case 5:
						lastTCell.setCellValue("E");
						break;
					default:
						lastTCell.setCellValue("否");
						break;
				}
				//lastTCell.setCellValue(billList.get(j).getExpress_package()==1?"是":"否");
				lastTCell.setCellStyle(style1);

			}

			OutputStream oStream = response.getOutputStream();

			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
			import_export_lock = false;
		} catch (IOException e)
		{
			import_export_lock = false;
			e.printStackTrace();

		}
	}

	@RequestMapping("/importInit")
	public String importInit(HttpServletRequest request)
	{

		return "/back/importBillList";

	}

	@RequestMapping("/importList")
	@ResponseBody
	public Map<String, Object> importList(HttpServletRequest request)
	{

		waiting();

		List<Pkg> needSendEmailPkgList = new ArrayList<Pkg>();
		MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
		MultipartFile excelFile = req.getFile("excel");
		// 文件名
		String fileName = excelFile.getOriginalFilename();
		// 后缀名
		String exName = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());

		Map<String, Object> rtnMap = new HashMap<String, Object>();

		List<FreightAccountingDetail> freightAccountingDetailList = new ArrayList<FreightAccountingDetail>();

		Map<String, String> colNameMap = new HashMap<String, String>();

		colNameMap.put("logistics_code", COLNAME_LOGISTICSCODE);
		colNameMap.put("weight", COLNAME_WEIGHT);
		colNameMap.put("price", COLNAME_PRICE);
		colNameMap.put("freight", COLNAME_FREIGHT);
		colNameMap.put("transport_cost", COLNAME_TRANSPORT_COST);
		try
		{
			Workbook workbook = null;
			// Excel版本 version = 2003
			if ("xls".endsWith(exName))
			{
				System.out.println("Excel版本 version = 2003");

				workbook = new HSSFWorkbook(excelFile.getInputStream());

				// Excel版本 version = 2007
			}
			else if ("xlsx".endsWith(exName))
			{
				workbook = new XSSFWorkbook(excelFile.getInputStream());

			}
			else
			{
				rtnMap.put("flag", IS_NOT_EXCEL);
				rtnMap.put("message", "文件类型错误！请选择Excel文件");
				import_export_lock = false;
				return rtnMap;
			}
			freightAccountingDetailList = ExcelReadUtil.excelToBean(workbook, SHEET_NAME, colNameMap, FreightAccountingDetail.class);

			// 读取不到数据
			if (freightAccountingDetailList == null || freightAccountingDetailList.size() == 0)
			{
				rtnMap.put("flag", EXCEL_CONTENT_ERROR);
				rtnMap.put("message", "没有账单信息！");
				import_export_lock = false;
				return rtnMap;
			}

			List<String> logisticsCodes = new ArrayList<String>();

			for (FreightAccountingDetail freightAccountingDetail : freightAccountingDetailList)
			{
				logisticsCodes.add(freightAccountingDetail.getLogistics_code());
			}
			List<Pkg> pkgList = memberBillService.selectPackageBylogisticsCodes(logisticsCodes);

			// 校验：包裹状态为，非支付状态 单号重复 单号不存在
			Map<String, String> logisticsCodeMap = unMatchLogisticsCode(freightAccountingDetailList, pkgList);

			String logisticsCodeError = logisticsCodeMap.get(LOGISTICSCODE_ERROR);
			String minusError = logisticsCodeMap.get(MINUS_ERROR);
			// 通过校验
			if (StringUtil.isEmpty(logisticsCodeError) && StringUtil.isEmpty(minusError))
			{
				User user = (User) request.getSession().getAttribute("back_user");
				FreightAccounting freightAccounting = new FreightAccounting();

				// 当前时间
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				// 批次号
				String batch_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PARTNER);

				// 批次管理
				freightAccounting.setUser_id(user.getUser_id());
				freightAccounting.setBatch_code(batch_code);
				freightAccounting.setCreatetime(timestamp);
				if(pkgList!=null)
				{
					for(Pkg pkg:pkgList)
					{
						pkg.setMemberbill_import_flag("Y");
					}
				}

				int count = freightAccountingService.insertFreightAccounting(freightAccounting, freightAccountingDetailList, pkgList, user);

				needSendEmailPkgList = pkgList;

				if (count < 1)
				{

					rtnMap.put("flag", "7");
					rtnMap.put("message", "更新数据失败！");
					import_export_lock = false;
					return rtnMap;
				}

			}
			else

			{
				Workbook errorWorkbook = null;
				String error = "";
				if ("xls".endsWith(exName))
				{

					errorWorkbook = new HSSFWorkbook(excelFile.getInputStream());
					error = "errorBill.xls";
				}
				else
				{
					errorWorkbook = new XSSFWorkbook(excelFile.getInputStream());
					error = "errorBill.xlsx";
				}

				// 错误订单号标识
				ceateDownExcel(errorWorkbook, logisticsCodeMap);

				File file = new File(UPLOAD_IMG_PATH + error);
				if (file.exists())
				{
					file.delete();
				}
				FileOutputStream fs = new FileOutputStream(file);
				errorWorkbook.write(fs);
				fs.close();

				String path = "/upload/" + error;

				rtnMap.put("flag", LIST_ERROR);
				rtnMap.put("message", "导入清单信息有误！");
				rtnMap.put("path", path);
				import_export_lock = false;
				return rtnMap;
			}
		} catch (Exception e)
		{
			logger.error("导入账单异常异常：" + e.toString());
			e.printStackTrace();
			rtnMap.put("flag", EXCEL_CONTENT_ERROR);
			rtnMap.put("message", "导入失败！");
			import_export_lock = false;
			return rtnMap;
		}
		rtnMap.put("message", "导入成功！");
		import_export_lock = false;

		// 提醒客户进行付款--Email发送
		if (needSendEmailPkgList != null && needSendEmailPkgList.size() > 0)
		{
			Map<Integer, UserSendEmailBean> waitingSendMap = new HashMap<Integer, UserSendEmailBean>();
			for (int i = 0; i < needSendEmailPkgList.size(); i++)
			{
				UserSendEmailBean userSendEmailBean = null;
				float transportCost = 0;
				int packageCount = 0;
				Pkg tempPkg = needSendEmailPkgList.get(i);
				Pkg updatedPkg = packageService.queryPackageById(tempPkg.getPackage_id());
				if (updatedPkg.getTransport_cost() > 0)// 费用为零时不需要计算
				{
					int userId = updatedPkg.getUser_id();
					if (waitingSendMap.containsKey(userId))
					{
						userSendEmailBean = waitingSendMap.get(userId);
						transportCost = userSendEmailBean.getTransportCost() + updatedPkg.getTransport_cost();
						packageCount = userSendEmailBean.getPackageCount() + 1;
					}
					else
					{
						userSendEmailBean = new UserSendEmailBean();
						transportCost = updatedPkg.getTransport_cost();
						packageCount = 1;
					}
					userSendEmailBean.setPackageCount(packageCount);
					userSendEmailBean.setTransportCost(transportCost);
					userSendEmailBean.setUserId(userId);
					waitingSendMap.put(userId, userSendEmailBean);
				}
			}

			boolean wantSendEmail = true;// todo, need to check system setting
			if (wantSendEmail)
			{
				Iterator<Integer> iterator = waitingSendMap.keySet().iterator();
				while (iterator.hasNext())
				{
					Integer key = iterator.next();
					UserSendEmailBean userSendEmailBean = waitingSendMap.get(key);
					FrontUser frontUser = frontUserService.queryFrontUserById(userSendEmailBean.getUserId());
					String needPayMenoy = NumberUtils.scaleMoneyData(userSendEmailBean.getTransportCost());
					String emailAddress = frontUser.getAccount();
					messageSendService.sendPayWarnEmail(frontUser.getUser_name(), emailAddress, frontUser.getAccount(), userSendEmailBean.getPackageCount(), needPayMenoy);
				}
			}
		}
		return rtnMap;
	}

	/**
	 * 导入账单校验 检验正确后更新新包裹支付状态,收费重量,单价,运费,总运费
	 * 
	 * @param freightAccountingDetailList
	 *            导入的账单
	 * @param pkgList
	 *            运单号对应的包裹
	 * @return
	 */

	private Map<String, String> unMatchLogisticsCode(List<FreightAccountingDetail> freightAccountingDetailList, List<Pkg> pkgList)
	{

		Map<String, String> rtnMap = new HashMap<String, String>();

		for (FreightAccountingDetail freightAccountingDetail : freightAccountingDetailList)
		{

			if (minus(freightAccountingDetail.getWeight()) || minus(freightAccountingDetail.getPrice()) || minus(freightAccountingDetail.getFreight()) || minus(freightAccountingDetail.getTransport_cost()))
			{
				rtnMap.put(MINUS_ERROR, MINUS_ERROR);
			}
			String logisticsCode = freightAccountingDetail.getLogistics_code();
			// 重复校验
			int size = rtnMap.size();
			rtnMap.put(logisticsCode, LOGISTICSCODE_NORMAL);

			if (rtnMap.size() == size)
			{

				rtnMap.put(logisticsCode, LOGISTICSCODE_REPEAT);
				rtnMap.put(LOGISTICSCODE_ERROR, LOGISTICSCODE_ERROR);
			}
			// 运单号存在校验
			boolean matchFlag = false;
			for (Pkg pkg : pkgList)
			{
				if (logisticsCode.equals(pkg.getLogistics_code()))
				{
					matchFlag = true;
					if (Pkg.LOGISTICS_STORAGED <= pkg.getStatus())
					{
						// if (Pkg.PAYMENT_PAID <= pkg.getPay_status() ||
						// Pkg.LOGISTICS_SIGN_IN <= pkg.getStatus())
						if ((Pkg.PAYMENT_PAID <= pkg.getPay_status()&&pkg.getPay_status()<Pkg.PAYMENT_VIRTUAL_PAID)||(Pkg.PAYMENT_VIRTUAL_PAID_FAILURE< pkg.getPay_status())||(Pkg.PAYMENT_VIRTUAL_PAID== pkg.getPay_status()&&pkg.getVirtual_pay()<0))
						{
							rtnMap.put(logisticsCode, LOGISTICSCODE_PAID);
							rtnMap.put(LOGISTICSCODE_ERROR, LOGISTICSCODE_ERROR);
						}
						// 更新包裹用
						else
						{

/*							//已支付（预扣）时，需要将包裹状态置为处理。。
							if(Pkg.PAYMENT_VIRTUAL_PAID== pkg.getPay_status()&&pkg.getVirtual_pay()>0)
							{
								pkg.setPay_status(Pkg.PAYMENT_VIRTUAL_PAID_HANDLE);
							}else
							{
								// 跟新包裹状态
								pkg.setPay_status(Pkg.PAYMENT_UNPAID);
							}*/
							// 跟新包裹状态
							pkg.setPay_status(Pkg.PAYMENT_UNPAID);
							pkg.setPay_status_freight(Pkg.PAYMENT_FREIGHT_UNPAID);
							// 收费重量
							pkg.setWeight(freightAccountingDetail.getWeight());
							// 单价
							pkg.setPrice(freightAccountingDetail.getPrice());
							// 运费
							pkg.setFreight(roundHalfUp(freightAccountingDetail.getFreight()));
							// 总运费
							pkg.setTransport_cost(roundHalfUp(freightAccountingDetail.getTransport_cost()));
						}
					}
					else
					{
						rtnMap.put(logisticsCode, LOGISTICSCODE_NOT_ARRIVED);
						rtnMap.put(LOGISTICSCODE_ERROR, LOGISTICSCODE_ERROR);
					}

					break;
				}
			}

			if (!matchFlag)
			{
				rtnMap.put(logisticsCode, LOGISTICSCODE_NOT_EXIST);
				rtnMap.put(LOGISTICSCODE_ERROR, LOGISTICSCODE_ERROR);
			}

		}

		return rtnMap;
	}

	/**
	 * 四舍五入
	 * 
	 * @param val
	 * @return
	 */
	private float roundHalfUp(float val)
	{
		BigDecimal temp = new BigDecimal(String.valueOf(val));
		String vString = temp.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		return Float.parseFloat(vString);
	}

	/**
	 * 错误的单号 订单号涂颜色,返回错误订单号文件
	 * 
	 * @param workBook
	 * @param logisticsCodeMap
	 * @return
	 * @throws Exception
	 */
	private void ceateDownExcel(Workbook workBook, Map<String, String> logisticsCodeMap) throws Exception
	{
		// 公司运单号列番号
		int logisticsCodeColNumber = COL_NUMBER_INIT;
		// 收费重量列番号
		int weightColNumber = COL_NUMBER_INIT;
		// 单价列番号
		int priceColNumber = COL_NUMBER_INIT;
		// 运费番号
		int freightColNumber = COL_NUMBER_INIT;
		// 总费用番号
		int transportCostColNumber = COL_NUMBER_INIT;

		// 取得订单sheet
		Sheet orderSheet = workBook.getSheet(SHEET_NAME);
		// 总行数
		int rows = orderSheet.getLastRowNum();
		// 第一行：列名
		Row firstHssfRow = orderSheet.getRow(0);
		// 总列数
		int cols = firstHssfRow.getPhysicalNumberOfCells();
		for (int n = 0; n < cols; n++)
		{
			String colName = firstHssfRow.getCell(n).getStringCellValue();
			// 公司运单号
			if (COLNAME_LOGISTICSCODE.equals(colName))
			{
				logisticsCodeColNumber = n;
			}
			// 收费重量
			if (COLNAME_WEIGHT.equals(colName))
			{
				weightColNumber = n;
			}
			// 单价列
			if (COLNAME_PRICE.equals(colName))
			{
				priceColNumber = n;
			}
			// 运费
			if (COLNAME_FREIGHT.equals(colName))
			{
				freightColNumber = n;
			}
			// 总费用
			if (COLNAME_TRANSPORT_COST.equals(colName))
			{
				transportCostColNumber = n;
			}

		}

		// 重复公司运单号单元格格式
		CellStyle cellStyle1 = workBook.createCellStyle();
		cellStyle1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle1.setFillForegroundColor(IndexedColors.DARK_RED.getIndex());
		cellStyle1.setAlignment(CellStyle.ALIGN_CENTER);

		// 单号已支付单元格格式
		CellStyle cellStyle2 = workBook.createCellStyle();
		cellStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle2.setFillForegroundColor(IndexedColors.DARK_YELLOW.getIndex());
		cellStyle2.setAlignment(CellStyle.ALIGN_CENTER);

		// 不存在公司运单号单元格格式
		CellStyle cellStyle3 = workBook.createCellStyle();
		cellStyle3.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle3.setFillForegroundColor(IndexedColors.RED.getIndex());
		cellStyle3.setAlignment(CellStyle.ALIGN_CENTER);

		// 不存在公司运单号单元格格式
		CellStyle cellStyle4 = workBook.createCellStyle();
		cellStyle4.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle4.setFillForegroundColor(IndexedColors.GREEN.getIndex());
		cellStyle4.setAlignment(CellStyle.ALIGN_CENTER);

		// 包裹还没到库
		CellStyle cellStyle5 = workBook.createCellStyle();
		cellStyle5.setFillPattern(CellStyle.SOLID_FOREGROUND);
		cellStyle5.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		cellStyle5.setAlignment(CellStyle.ALIGN_CENTER);

		for (int i = 1; i <= rows; i++)
		{

			Row row = orderSheet.getRow(i);

			// 空行检验
			if (ExcelReadUtil.isBlankRow(row))
			{
				continue;
			}

			// 运单号
			String logisticsCode = row.getCell(logisticsCodeColNumber).getStringCellValue().trim();

			Cell logisticsCodeCell = row.getCell(logisticsCodeColNumber);

			String errorLogisticsCode = logisticsCodeMap.get(logisticsCode);

			// 运单重复
			if (LOGISTICSCODE_REPEAT.equals(errorLogisticsCode))
			{
				logisticsCodeCell.setCellStyle(cellStyle1);
				// 运单已支付
			}
			else if (LOGISTICSCODE_PAID.equals(errorLogisticsCode))
			{
				logisticsCodeCell.setCellStyle(cellStyle2);
				// 运单不存在
			}
			else if (LOGISTICSCODE_NOT_EXIST.equals(errorLogisticsCode))
			{
				logisticsCodeCell.setCellStyle(cellStyle4);
			}// 包裹还没到库
			else if (LOGISTICSCODE_NOT_ARRIVED.equals(errorLogisticsCode))
			{
				logisticsCodeCell.setCellStyle(cellStyle5);
			}

			// 收费重量
			Cell weightCell = row.getCell(weightColNumber);
			setMinusCellStyle(weightCell, cellStyle4);

			// 单价重量
			Cell priceCell = row.getCell(priceColNumber);
			setMinusCellStyle(priceCell, cellStyle4);

			// 运费重量
			Cell freightCell = row.getCell(freightColNumber);
			setMinusCellStyle(freightCell, cellStyle4);

			// 总费用重量
			Cell transportCostCell = row.getCell(transportCostColNumber);
			setMinusCellStyle(transportCostCell, cellStyle4);

		}
	}

	/**
	 * 负数单元格格式
	 * 
	 * @param cell
	 * @param cellStyle
	 */
	private void setMinusCellStyle(Cell cell, CellStyle cellStyle)
	{
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		String stringVaule = cell.getStringCellValue();
		float floatvalue = 0;
		if (stringVaule != null)
		{
			floatvalue = Float.parseFloat(stringVaule);
		}
		if (minus(floatvalue))
		{
			cell.setCellStyle(cellStyle);
		}
	}

	/**
	 * 负数校验
	 * 
	 * @param value
	 * @return
	 */

	private boolean minus(float value)
	{

		if (value < -0.00001f)
		{
			return true;
		}

		return false;
	}

	/**
	 * 导入导出排他
	 */
	private void waiting()
	{
		try
		{
			while (true)
			{

				if (lock())
				{
					break;

				}
				else
				{
					System.out.println("等待其他用户操作中 ...");
					Thread.sleep(WAITING_MILLISECOND);

				}

			}
		} catch (InterruptedException e1)
		{

			e1.printStackTrace();

		}
	}

	/**
	 * 导入导出排他锁
	 */
	private static synchronized boolean lock()
	{
		if (!import_export_lock)
		{
			import_export_lock = true;
			return true;
		}

		return false;
	}

	/**
	 * 查询详情
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/showDetail")
	public String showDetail(HttpServletRequest request, String user_id)
	{
		List<MemberBillModel> billList = memberBillService.queryFrontUserBillByuserIds(StringUtil.toStringList(user_id, ","));
		request.setAttribute("billList", billList);
		return "/back/memberbilldetail";
	}
}
