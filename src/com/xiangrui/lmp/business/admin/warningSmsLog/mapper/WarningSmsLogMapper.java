package com.xiangrui.lmp.business.admin.warningSmsLog.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.warningSmsLog.vo.WarningSmsLog;

public interface WarningSmsLogMapper
{
	List<WarningSmsLog> queryWarningSmsLog(Map<String, Object> paremt);

	WarningSmsLog queryWarningSmsLogBySeqId(int seq_id);

	void updateWarningSmsLogResentCountBySeqId(Map<String, Object> paremt);

	void insertWarningSmsLog(WarningSmsLog warningSmsLog);

	void updateWarningSmsLogSendTimeBySeqId(Map<String, Object> paremt);
}
