package com.xiangrui.lmp.business.admin.warningSmsLog.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.warningSmsLog.vo.WarningSmsLog;

public interface WarningSmsLogService
{
	List<WarningSmsLog> queryWarningSmsLog(Map<String, Object> paremt);

	WarningSmsLog queryWarningSmsLogBySeqId(int seq_id);

	void updateWarningSmsLogResentCountBySeqId(Map<String, Object> paremt);

	void insertWarningSmsLog(WarningSmsLog warningSmsLog);
}
