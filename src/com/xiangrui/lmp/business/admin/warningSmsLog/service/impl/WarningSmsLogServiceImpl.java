package com.xiangrui.lmp.business.admin.warningSmsLog.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.warningSmsLog.mapper.WarningSmsLogMapper;
import com.xiangrui.lmp.business.admin.warningSmsLog.service.WarningSmsLogService;
import com.xiangrui.lmp.business.admin.warningSmsLog.vo.WarningSmsLog;

@Service("warningSmsLogService")
public class WarningSmsLogServiceImpl implements WarningSmsLogService
{
	@Autowired
	WarningSmsLogMapper warningSmsLogMapper;

	@Override
	public List<WarningSmsLog> queryWarningSmsLog(Map<String, Object> paremt)
	{
		return warningSmsLogMapper.queryWarningSmsLog(paremt);
	}

	@Override
	public WarningSmsLog queryWarningSmsLogBySeqId(int seq_id)
	{
		return warningSmsLogMapper.queryWarningSmsLogBySeqId(seq_id);
	}

	@Override
	public void updateWarningSmsLogResentCountBySeqId(Map<String, Object> paremt)
	{
		warningSmsLogMapper.updateWarningSmsLogResentCountBySeqId(paremt);
	}

	@Override
	public void insertWarningSmsLog(WarningSmsLog warningSmsLog)
	{
		warningSmsLogMapper.insertWarningSmsLog(warningSmsLog);
	}

}
