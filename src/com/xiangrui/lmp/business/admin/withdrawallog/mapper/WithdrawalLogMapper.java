package com.xiangrui.lmp.business.admin.withdrawallog.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.withdrawallog.vo.WithdrawalLog;

/**
 * 提现数据接口
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午8:36:19
 * </p>
 */
public interface WithdrawalLogMapper
{

    /**
     * 分页查询所有提现现象
     * 
     * @param params
     * @return
     */
    List<WithdrawalLog> queryAll(Map<String, Object> params);

    /**
     * 根据id查找
     * 
     * @param log_id
     */
    WithdrawalLog selectWithdrawalLogByLogId(int log_id);

    /**
     * 审核
     * 
     * @param withdrawalLog
     */
    void updateStatusByLogId(WithdrawalLog withdrawalLog);

}
