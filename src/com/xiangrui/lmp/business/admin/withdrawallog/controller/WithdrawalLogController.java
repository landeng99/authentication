package com.xiangrui.lmp.business.admin.withdrawallog.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.accountlog.service.AccountLogService;
import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.admin.withdrawallog.service.WithdrawalLogService;
import com.xiangrui.lmp.business.admin.withdrawallog.vo.WithdrawalLog;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 提现界面操作
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午8:25:07
 *         </p>
 */
@Controller
@RequestMapping("/admin/withdrawalLog")
public class WithdrawalLogController
{

    /**
     * 提现操作
     */
    @Autowired
    private WithdrawalLogService withdrawalLogService;

    /**
     * 申请人的信息管理
     */
    @Autowired
    private FrontUserService frontUserService;

    /**
     * 提现操作记录操作
     */
    @Autowired
    private AccountLogService accountLogService;

    /**
     * 检索条件 记录
     */
    private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

    /**
     * 提现审核初始化
     * 
     * @return
     */
    @RequestMapping("/queryAllOne")
    public String queryAll(HttpServletRequest request)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);

        List<WithdrawalLog> withdrawalLogList = withdrawalLogService
                .queryAll(params);
        request.setAttribute("pageView", pageView);
        request.setAttribute("withdrawalLogList", withdrawalLogList);

        return "back/withdrawalLogList";
    }

    /**
     * 分页查询所有提现信息
     * 
     * @param request
     * @param user_name
     * @param status
     * @param timeStart
     * @param timeEnd
     * @param bank_name
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping("/search")
    public String search(HttpServletRequest request, String user_name,
            String account, String status, String timeStart, String timeEnd,
            String bank_name)
    {
        // 分页查询
        PageView pageView = null;

        Map<String, Object> params = new HashMap<String, Object>();

        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);

            params = (Map<String, Object>) request.getSession().getAttribute(
                    SESSION_KEY_SEARCH_CONDITION);
        } else
        {
            pageView = new PageView(1);

            params.put("user_name", user_name);
            params.put("account", account);
            params.put("status", status);
            params.put("timeStart", timeStart);
            params.put("timeEnd", timeEnd);
            params.put("bank_name", bank_name);

            request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION,
                    params);
        }

        params.put("pageView", pageView);

        List<WithdrawalLog> withdrawalLogList = withdrawalLogService
                .queryAll(params);
        request.setAttribute("pageView", pageView);
        // 页面显示用
        request.setAttribute("params", params);
        request.setAttribute("withdrawalLogList", withdrawalLogList);

        return "back/withdrawalLogList";
    }

    /**
     * 提现详情初始化
     * 
     * @param request
     * @param log_id
     * @return
     */
    @RequestMapping("/detail")
    public String detail(HttpServletRequest request, Integer log_id)
    {
        WithdrawalLog withdrawalLog = withdrawalLogService
                .selectWithdrawalLogByLogId(log_id);
        FrontUser frontUser = frontUserService.queryFrontUserById(withdrawalLog
                .getUser_id());

        request.setAttribute("withdrawalLog", withdrawalLog);
        request.setAttribute("frontUser", frontUser);

        return "back/withdrawalLogInfo";
    }

    /**
     * 提现审核初始化
     * 
     * @param request
     * @param log_id
     * @return
     */
    @RequestMapping("/edit")
    public String edit(HttpServletRequest request, Integer log_id)
    {
        WithdrawalLog withdrawalLog = withdrawalLogService
                .selectWithdrawalLogByLogId(log_id);

        FrontUser frontUser = frontUserService.queryFrontUserById(withdrawalLog
                .getUser_id());

        request.setAttribute("withdrawalLog", withdrawalLog);
        request.setAttribute("frontUser", frontUser);

        return "back/updateWithdrawalLog";
    }

    /**
     * 保存
     * 
     * @param request
     * @param withdrawalLog
     * @return
     */
    @RequestMapping("/update")
    public String update(HttpServletRequest request, Integer log_id,
            Integer status, String remark)
    {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("back_user");
        WithdrawalLog withdrawalLog = withdrawalLogService
                .selectWithdrawalLogByLogId(log_id);

        withdrawalLog.setStatus(status);
        withdrawalLog.setRemark(remark);
        withdrawalLog.setOperator(user.getUser_id());
        withdrawalLog
                .setOperate_time(new Timestamp(System.currentTimeMillis()));

        AccountLog accountLog = accountLogService
                .selectAccountLogByOrderId(withdrawalLog.getTrans_id());
        if (status.equals(AccountLog.FAIL))
        {
            accountLog.setDescription(remark);
        }

        accountLog.setStatus(status);
        accountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));

        // 客户信息
        FrontUser frontUser = frontUserService.queryFrontUserById(withdrawalLog
                .getUser_id());
        float amount = withdrawalLog.getAmount();
        // 审核通过
        if (status.equals(WithdrawalLog.SUCCESS))
        {

            frontUser.setBalance(frontUser.getBalance() - amount);
            frontUser.setFrozen_balance(frontUser.getFrozen_balance() - amount);
        } else
        {
            frontUser.setAble_balance(frontUser.getAble_balance() + amount);
            frontUser.setFrozen_balance(frontUser.getFrozen_balance() - amount);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("withdrawalLog", withdrawalLog);
        params.put("frontUser", frontUser);
        params.put("accountLog", accountLog);

        withdrawalLogService.update(params);

        return "back/withdrawalLogList";
    }

}
