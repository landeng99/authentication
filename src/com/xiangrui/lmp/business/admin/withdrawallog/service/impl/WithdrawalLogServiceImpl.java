package com.xiangrui.lmp.business.admin.withdrawallog.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.accountlog.mapper.AccountLogMapper;
import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.withdrawallog.mapper.WithdrawalLogMapper;
import com.xiangrui.lmp.business.admin.withdrawallog.service.WithdrawalLogService;
import com.xiangrui.lmp.business.admin.withdrawallog.vo.WithdrawalLog;

/**
 * 提现操作实现
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午8:36:56
 *         </p>
 */
@Service("withdrawalLogService")
public class WithdrawalLogServiceImpl implements WithdrawalLogService
{

    /**
     * 提现操作实现
     */
    @Autowired
    private WithdrawalLogMapper withdrawalLogMapper;
    
    @Autowired
    private FrontUserMapper frontUserMapper;
    
    @Autowired
    private  AccountLogMapper accountLogMapper;

    /**
     * 分页查询所有提现现象
     * 
     * @param params
     * @return
     */
    @Override
    public List<WithdrawalLog> queryAll(Map<String, Object> params)
    {

        return withdrawalLogMapper.queryAll(params);
    }

    /**
     * 根据id查找
     * 
     * @param log_id
     */
    @Override
    public WithdrawalLog selectWithdrawalLogByLogId(int log_id)
    {

        return withdrawalLogMapper.selectWithdrawalLogByLogId(log_id);
    }

    /**
     * 审核
     * 
     * @param withdrawalLog
     */
    @Override
    public void update(Map<String, Object> params)
    {

        withdrawalLogMapper.updateStatusByLogId((WithdrawalLog) params.get("withdrawalLog"));
        
       frontUserMapper.updateBalance((FrontUser) params.get("frontUser"));
       accountLogMapper.updateStatus((AccountLog) params.get("accountLog"));
    }

}
