package com.xiangrui.lmp.business.admin.withdrawallog.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.withdrawallog.vo.WithdrawalLog;

/**
 * 提现操作
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午8:36:38
 *         </p>
 */
public interface WithdrawalLogService
{
    /**
     * 分页查询所有提现现象
     * 
     * @param params
     * @return
     */
    List<WithdrawalLog> queryAll(Map<String, Object> params);

    /**
     * 根据id查找
     * 
     * @param log_id
     */
    WithdrawalLog selectWithdrawalLogByLogId(int log_id);

    /**
     * 审核
     * 
     * @param withdrawalLog
     */
    void update(Map<String, Object> params);
}
