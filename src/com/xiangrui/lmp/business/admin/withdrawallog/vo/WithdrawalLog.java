package com.xiangrui.lmp.business.admin.withdrawallog.vo;

import com.xiangrui.lmp.business.base.BaseWithdrawalLog;

/**
 * 提现实体
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午8:39:53
 *         </p>
 */
public class WithdrawalLog extends BaseWithdrawalLog
{

    private static final long serialVersionUID = 1L;
    
    /**
     * 账户注册是的姓名，非 提现申请是填写的姓名
     */
   private String  real_name;
   
   /**
    * 账户
    */
  private String  account;
   
   /**
    * 审核人员
    */
  private String  opreate_name;


public String getOpreate_name()
{
    return opreate_name;
}


public void setOpreate_name(String opreate_name)
{
    this.opreate_name = opreate_name;
}


public String getReal_name()
{
    return real_name;
}


public void setReal_name(String real_name)
{
    this.real_name = real_name;
}


public String getAccount()
{
    return account;
}


public void setAccount(String account)
{
    this.account = account;
}

}
