package com.xiangrui.lmp.business.admin.goodstype.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.goodstype.vo.GoodsType;
import com.xiangrui.lmp.business.admin.goodstype.mapper.GoodsTypeMapper;
import com.xiangrui.lmp.business.admin.goodstype.service.GoodsTypeService;

/**
 * 申报类型
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:38:34
 * </p>
 */
@Service("goodsTypeService")
public class GoodsTypeServiceImpl implements GoodsTypeService
{

    /**
     * 申报类型
     */
    @Autowired
    private GoodsTypeMapper goodsTypeMapper;

    /**
     * 查询申报类型
     */
    @Override
    public List<GoodsType> queryAll(Map<String, Object> parmse)
    {
        return goodsTypeMapper.queryAll(parmse);
    }

    /**
     * 查询申报类型,更具id
     */
    @Override
    public GoodsType queryAllId(int id)
    {
        return goodsTypeMapper.queryAllId(id);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改申报类型")
    @Override
    public void updateGoodsType(GoodsType goodsType)
    {
        goodsTypeMapper.updateGoodsType(goodsType);
    }

    /**
     * 删除
     */
    @SystemServiceLog(description = "删除申报类型")
    @Override
    public void delGoodsType(int goodsTypeId)
    {
        goodsTypeMapper.delGoodsType(goodsTypeId);
    }

    /**
     * 新增
     */
    @SystemServiceLog(description = "新增申报类型")
    @Override
    public void addGoodsType(GoodsType goodsType)
    {
        goodsTypeMapper.addGoodsType(goodsType);
    }

    @Override
    public List<GoodsType> queryInParentIdClertGid(Map<String, Object> inParetnId)
    {
        return goodsTypeMapper.queryInParentIdClertGid(inParetnId);
    }

    @SystemServiceLog(description = "删除一系列申报类型")
    @Override
    public void deleteGoodsTypeInGid(Map<String, Object> inGid)
    {
        goodsTypeMapper.deleteGoodsTypeInGid(inGid);
    }
}
