package com.xiangrui.lmp.business.admin.goodstype.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.goodstype.service.GoodsTypeService;
import com.xiangrui.lmp.business.admin.goodstype.vo.GoodsType;

/**
 * 申报类别
 * <p>
 * 
 * @author hsjing
 *         </p>
 *         <p>
 *         2015-5-25 下午7:56:57
 *         </p>
 */
@Controller
@RequestMapping("/admin/goodsType")
public class GoodsTypeController
{

    /**
     * 申报类别
     */
    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 首次进入
     * 
     * @return
     */
    @RequestMapping("queryAllOne")
    public String queryAllOne(HttpServletRequest request)
    {
        return queryAll(request, null);
    }

    /**
     * 查询所有申报类别
     * 
     * @return
     */
    @RequestMapping("queryAll")
    public String queryAll(HttpServletRequest request, GoodsType goodsType)
    {
        // 无需分类,空条件查询
        Map<String, Object> params = new HashMap<String, Object>();
        List<GoodsType> list = goodsTypeService.queryAll(params);
        
        Comparator<GoodsType> comparator = new Comparator<GoodsType>() {  
            public int compare(GoodsType p1, GoodsType p2) {//return必须是int，而str.age是double,所以不能直接return (p1.age-p2.age)  
              
                String[] oifw = p2.getIdFramework().split("_");
                String[] ifw = p1.getIdFramework().split("_");
                int size = oifw.length > ifw.length ? ifw.length : oifw.length;
                boolean isEq = false;
                for (int i = 0; i < size; i++)
                {
                    int oifwInt = Integer.parseInt(oifw[i]);
                    int ifwInt = Integer.parseInt(ifw[i]);
                    if (oifwInt > ifwInt)
                    {
                        return -1;
                    }
                    isEq = (oifwInt == ifwInt);
                }
                if (isEq)
                {
                    if (oifw.length > ifw.length)
                    {
                        return -1;
                    } else if (oifw.length == ifw.length)
                    {
                        return 0;
                    }
                }
                Integer o1=new Integer(p1.getParentId());
                Integer o2=new Integer(p2.getParentId());
                return p1.getOrderId().compareTo(p2.getOrderId());// 正确的方式  
               // return 1;
            }  
        };  
        System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
        //Collections.sort(list,comparator);
        request.setAttribute("goodsTypeLists", list);
        return "back/goodsTypeList";
    }

    /**
     * 进入修改,或者增加
     * 
     * @return
     */
    @RequestMapping("saveOrUpdateGoodsType")
    public String saveOrUpdateGoodsType(HttpServletRequest request,
            GoodsType goodsType)
    {
        // 是否存在id 有则修改,否则新增
        if (goodsType.getGoodsTypeId() > 0)
        {
            String name = goodsType.getNameSpecs();
            String unit = goodsType.getUnit();

            // 匹配数据库
            goodsType = this.goodsTypeService.queryAllId(goodsType
                    .getGoodsTypeId());
            goodsType.setNameSpecs(name);
            goodsType.setUnit(unit);

            this.goodsTypeService.updateGoodsType(goodsType);
        } else
        {
            // 查询父级 找到IdFramework
            GoodsType goodsTypeParent = this.goodsTypeService
                    .queryAllId(goodsType.getParentId());

            if (null == goodsTypeParent)
            {
                goodsType.setIdFramework("0");
            } else
            {
                // 配置好IdFramework
                goodsType.setIdFramework(goodsTypeParent.getIdFramework());
            }
            this.goodsTypeService.addGoodsType(goodsType);
        }
        return queryAll(request, null);
    }

    /**
     * 进入增加或者修改
     * 
     * @return
     */
    @RequestMapping("inGoodsTypeInfo")
    public String inGoodsTypeInfo(HttpServletRequest request,
            GoodsType goodsType)
    {
        // 默认参数为非空指针的空GoodsType对象
        request.setAttribute("goodsTypePojo", new GoodsType());

        // 是否存在id, 存在则匹配好对象, 用在前台页面是用于做修改或者新增的判断
        if (goodsType.getGoodsTypeId() > 0)
        {
            goodsType = this.goodsTypeService.queryAllId(goodsType
                    .getGoodsTypeId());
            request.setAttribute("goodsTypePojo", goodsType);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        List<GoodsType> list = goodsTypeService.queryAll(params);
        request.setAttribute("goodsTypeLists", list);
        return "back/updateGoodsType";
    }

    /**
     * 删除
     * 
     * @return
     */
    @RequestMapping("deleteGoodsType")
    public String deleteGoodsType(HttpServletRequest request,GoodsType goodsType)
    {

        String parmas = goodsType.getGoodsTypeId() + "";
        StringBuffer inGid = new StringBuffer(parmas);
        boolean isDoWhile = true;
        do
        {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("inParetnId", parmas);
            List<GoodsType> goodsTypes = this.goodsTypeService
                    .queryInParentIdClertGid(map);
            StringBuffer inParetnId = new StringBuffer();
            for (GoodsType gtObj : goodsTypes)
            {
                inParetnId.append(",");
                inParetnId.append(gtObj.getGoodsTypeId());
            }
            if (goodsTypes.size() > 0)
            {
                parmas = inParetnId.substring(1);
                inGid.append(inParetnId);
            } else
            {
                isDoWhile = false;
            }
        } while (isDoWhile);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("inGid", inGid);
        this.goodsTypeService.deleteGoodsTypeInGid(map);
        
        return queryAll(request, null);
    }

}
