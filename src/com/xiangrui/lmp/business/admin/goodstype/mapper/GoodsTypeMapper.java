package com.xiangrui.lmp.business.admin.goodstype.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.goodstype.vo.GoodsType;

/**
 * 申报类别
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:34:26
 * </p>
 */
public interface GoodsTypeMapper
{

    /**
     * 查询所有申报类别,带参数的,其实申报类别无需分页,参数可以为空
     * 
     * @param parmse
     * @return
     */
    List<GoodsType> queryAll(Map<String, Object> parmse);

    /**
     * 查询单个申报类别
     * 
     * @param id
     * @return
     */
    GoodsType queryAllId(int id);

    /**
     * 修改申报类别
     * 
     * @param goodsType
     */
    void updateGoodsType(GoodsType goodsType);

    /**
     * 删除申报类别
     * 
     * @param id
     */
    void delGoodsType(int goodsTypeId);

    /**
     * 增加申报类别
     * 
     * @param goodsType
     */
    void addGoodsType(GoodsType goodsType);

    List<GoodsType> queryInParentIdClertGid(Map<String, Object> inParetnId);

    void deleteGoodsTypeInGid(Map<String, Object> inGid);

}
