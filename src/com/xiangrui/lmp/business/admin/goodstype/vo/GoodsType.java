package com.xiangrui.lmp.business.admin.goodstype.vo;

import com.xiangrui.lmp.business.base.BaseGoodsType;

/**
 * 申报类新
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-25 上午11:18:06
 * </p>
 */
public class GoodsType extends BaseGoodsType
{

    private static final long serialVersionUID = 1L;
    

    /**
     * 提供最新的排序值
     * 
     * @return
     */
    public String getOrderId()
    {
        return this.getIdFramework() + "_" + this.getGoodsTypeId();
    }
    
    /**
     * 分解 类似 [0_1_2_3],[0_3],[0_48_25_3_15]等类型的方法
     * <p>
     * [0_1_2_3 ==> 1_2]; [0_1_2 ==> 0_1]; [0_3 ==> 0_0]; [0 ==> 0_0]
     * </p>
     * <p>
     * 这个方法的结合 数据库的数据来看<br>
     * 在数据库中,idFramework=0_1_2_3,则有id=3,parent_id=2<br>
     * 通过getOneId(),可以读取到[2_3]<br>
     * 在办法中getTwoId(),可以读到[1_2]<br>
     * 在页面中就可以充分化解了:
     * </p>
     * <p>
     * data-tt-id="${goodsType.oneId}" data-tt-parent-id="${goodsType.twoId}"
     * </p>
     * 
     * @return
     */
    public String getTwoId()
    {
        String[] ary = this.getIdFramework().split("_");

        if (ary.length < 2)
            return "0_0";
        else if (ary.length == 2)
            return "0_" + ary[0];
        else
        {
            return ary[ary.length - 3] + "_" + ary[ary.length - 2];
        }
    }

}
