package com.xiangrui.lmp.business.admin.coupon.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.coupon.vo.Coupon;

/**
 * 优惠券
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午3:11:39
 *         </p>
 */
public interface CouponMapper
{

	/**
	 * 查询优惠券
	 * 
	 * @param params
	 * @return
	 */
	List<Coupon> queryAll(Map<String, Object> params);

	/**
	 * 优惠券来源分类统计
	 * 
	 * @return
	 */
	List<Coupon> queryAllType();

	/**
	 * 查询优惠券 单个的
	 * 
	 * @param id
	 * @return
	 */
	Coupon queryAllId(int id);

	/**
	 * 修改
	 * 
	 * @param coupon
	 */
	void updateCoupon(Coupon coupon);

	/**
	 * 修改
	 * 
	 * @param coupon
	 */
	void addCoupon(Coupon coupon);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void deleteCoupon(int id);

	/**
	 * 通过优惠券编辑查看详细信息
	 * 
	 * @param couponCode
	 * @return
	 */
	List<Coupon> queryByCouponCode(String couponCode);

	/**
	 * 检查优惠券编号是否重复
	 * 
	 * @param couponCode
	 * @return
	 */
	Integer checkCouponCode(String couponCode);
}
