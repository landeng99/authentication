package com.xiangrui.lmp.business.admin.coupon.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.coupon.vo.CouponUsed;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;

/**
 * 
 * @author Will
 *
 */
public interface UserCouponMapper
{

	/**
	 * 通过优惠券编码查看发放用户列表
	 * 
	 * @param params
	 * @return
	 */
	List<UserCoupon> queryByCoupon_code(Map<String, Object> params);

	/**
	 * 下发优惠券给用户
	 * 
	 * @param coupon
	 */
	int pushCouponToUser(UserCoupon userCoupon);

	/**
	 * 对已下发的优惠券两次下发时只需要增加数量即可
	 * 
	 * @param coupon
	 */
	void updateUserCouponQuantity(Map<String, Object> params);

	/**
	 * 标志下发优惠券已使用
	 * 
	 * @param coupon
	 */
	void setUsedUserCoupon(Map<String, Object> params);

	/**
	 * 根据用户ID来查询该用户可使用的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	List<UserCoupon> queryAvliableCouponByUserId(Map<String, Object> params);

	/**
	 * 根据用户ID来查询该用户已经使用的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	List<CouponUsed> queryUsedCouponByUserId(Map<String, Object> params);

	/**
	 * 根据用户ID来查询该用户已过期的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	List<UserCoupon> queryExpiredCouponByUserId(Map<String, Object> params);

	/**
	 * 根据coupon_code来查询下发给用户的优惠券
	 * 
	 * @param params
	 * @return
	 */
	List<UserCoupon> queryUserCouponBycoupon_code(Map<String, Object> params);

	/**
	 * 定入已使用优惠券
	 * 
	 * @param rontCouponUsed
	 */
	void insertCouponUsed(CouponUsed couponUsed);

	/**
	 * 用户优惠券数量统计
	 * 
	 * @param user_id
	 * @return
	 */
	int sumQuantityByUserId(Integer user_id);

	/**
	 * 用户可用优惠券数量统计
	 * 
	 * @param user_id
	 * @return
	 */
	int sumAvaliableQuantityByUserId(Integer user_id);
	
	/**
	 * 查看下发给用户的某个优惠券
	 * @return
	 */
	UserCoupon queryUserCouponBycouponId(Integer couponId);
	
	/**
	 * 根据订单号查看已经使用过的优惠券
	 * @return
	 */
	CouponUsed queryCouponUsedByOrder_code(String order_code);
	
	/**
	 * 删除下发给用户的某个优惠券
	 * @return
	 */
	void deleteUserCoupon(int couponId);
	
	/**
	 * 获取下发给用户优惠券ID
	 * 
	 * @param coupon
	 */
	int getPushCouponToUserCoupon_id();
}
