package com.xiangrui.lmp.business.admin.coupon.vo;

import com.xiangrui.lmp.business.base.BaseCouponUsed;

public class CouponUsed extends BaseCouponUsed
{
	private int actual_coupon_id;

	public int getActual_coupon_id()
	{
		return actual_coupon_id;
	}

	public void setActual_coupon_id(int actual_coupon_id)
	{
		this.actual_coupon_id = actual_coupon_id;
	}
}
