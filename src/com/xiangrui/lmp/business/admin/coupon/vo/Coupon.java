package com.xiangrui.lmp.business.admin.coupon.vo;

import com.xiangrui.lmp.business.base.BaseCoupon;

/**
 * 优惠券
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:16:55
 * </p>
 */
public class Coupon extends BaseCoupon
{

    private static final long serialVersionUID = 1L;

    /**
     * 是否过期
     */
    private int guoq;

    /**
     * 开始日期
     */
    private int denominationStrat;

    /**
     * 结束日期
     */
    private int denominationEnd;

    public int getGuoq()
    {
        return guoq;
    }

    public void setGuoq(int guoq)
    {
        this.guoq = guoq;
    }

    public int getDenominationStrat()
    {
        return denominationStrat;
    }

    public void setDenominationStrat(int denominationStrat)
    {
        this.denominationStrat = denominationStrat;
    }

    public int getDenominationEnd()
    {
        return denominationEnd;
    }

    public void setDenominationEnd(int denominationEnd)
    {
        this.denominationEnd = denominationEnd;
    }

}
