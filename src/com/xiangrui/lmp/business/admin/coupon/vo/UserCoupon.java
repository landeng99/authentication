package com.xiangrui.lmp.business.admin.coupon.vo;

import com.xiangrui.lmp.business.base.BaseUserCoupon;

public class UserCoupon extends BaseUserCoupon
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userName;
	private int haveUsedCount;
	private int avalidCount;
	private int actual_coupon_id;
	private String account;

	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}

	public int getActual_coupon_id()
	{
		return actual_coupon_id;
	}

	public void setActual_coupon_id(int actual_coupon_id)
	{
		this.actual_coupon_id = actual_coupon_id;
	}

	public int getAvalidCount()
	{
		return avalidCount;
	}

	public void setAvalidCount(int avalidCount)
	{
		this.avalidCount = avalidCount;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public int getHaveUsedCount()
	{
		return haveUsedCount;
	}

	public void setHaveUsedCount(int haveUsedCount)
	{
		this.haveUsedCount = haveUsedCount;
	}

}
