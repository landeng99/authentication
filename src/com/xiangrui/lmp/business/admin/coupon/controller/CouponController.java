package com.xiangrui.lmp.business.admin.coupon.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.coupon.mapper.UserCouponMapper;
import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.Coupon;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.util.DateFormatUtils;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 优惠券
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午3:06:49
 *         </p>
 */
@Controller
@RequestMapping("/admin/coupon")
public class CouponController
{
	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";
	/**
	 * 优惠券
	 */
	@Autowired
	private CouponService couponService;
	@Autowired
	private FrontUserService frontUserService;
	@Autowired
	private UserCouponMapper userCouponMapper;

	/**
	 * 首次查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryAllOne")
	public String indexFink(HttpServletRequest request)
	{
		return queryAll(request, null);
	}

	/**
	 * 初试化分页查询所有优惠券
	 * 
	 * @return
	 */
	@RequestMapping("/queryAll")
	public String queryAll(HttpServletRequest request, Coupon coupon)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);

		// 首次查询 当没有有request范围内的参数aAll的设置时,不是首次查询
		if (null != coupon)
		{
			params.put("COUPONCODE", coupon.getCouponCode());
			params.put("DENOMINATIONSTRAT", coupon.getDenominationStrat());
			params.put("COUPON_NAME", coupon.getCoupon_name());
			params.put("STATUS", coupon.getStatus());
			params.put("EFFECTTIME", coupon.getEffectTime());
			params.put("EXPIRATIONTIME", coupon.getExpirationTime());
			params.put("ISREPEATUSE", coupon.getIsRepeatUse());
			params.put("DENOMINATIONEND", coupon.getDenominationEnd());
			params.put("GUOQ", coupon.getGuoq());
			try
			{
				if (null != coupon.getResource())
					coupon.setCoupon_name(new String(coupon.getCoupon_name().getBytes("ISO-8859-1"), "UTF-8"));
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
			params.put("RESOURCE", coupon.getResource());
		}

		List<Coupon> list = couponService.queryAll(params);
		request.setAttribute("pageView", pageView);
		request.setAttribute("couponLists", list);
		return "back/couponList";
	}

	/**
	 * 计入修改 优惠券
	 * 
	 * @param request
	 * @param coupon
	 * @return
	 */
	@RequestMapping("/touUpdateCoupon")
	public String toUpdateCoupon(HttpServletRequest request, Coupon coupon)
	{
		coupon = this.couponService.queryAllId(coupon.getCouponId());
		request.setAttribute("couponPojo", coupon);
		return "back/updateCoupon";
	}

	/**
	 * 计入新增 优惠券
	 * 
	 * @param request
	 * @param coupon
	 * @return
	 */
	@RequestMapping("/toAddCoupon")
	public String toAddCoupon(HttpServletRequest request)
	{
		request.setAttribute("couponPojo", new Coupon());
		return "back/updateCoupon";
	}

	/**
	 * 修改或者新增 优惠券
	 * 
	 * @param request
	 * @param coupon
	 * @return
	 */
	@RequestMapping("updateOrAddCoupon")
	public String updateOrAddCoupon(HttpServletRequest request, Coupon coupon)
	{
		// 没有日期参数输入时,也就是优惠券选择的是有用几天
		if (coupon.getEffectTime() == "")
		{
			coupon.setEffectTime(null);
		}
		if (coupon.getExpirationTime() == "")
		{
			coupon.setExpirationTime(null);
		}

		// 是否有id 是则增加 ,否则修改
		if (coupon.getCouponId() <= 0)
		{
			coupon.setLeft_count(coupon.getTotal_count());
			this.couponService.addCoupon(coupon);
		}
		else
		{
			this.couponService.updateCoupon(coupon);
		}
		return queryAll(request, null);
	}

	/**
	 * 删除优惠券
	 * 
	 * @param coupon
	 * @return
	 */
	@RequestMapping("deleteCoupon")
	public String deleteCoupon(Coupon coupon)
	{
		this.couponService.deleteCoupon(coupon.getCouponId());
		return "back/couponList";
	}

	/**
	 * 查看优惠券详情
	 * 
	 * @param coupon
	 * @return
	 */
	@RequestMapping("couponDetail")
	public String couponDetail(HttpServletRequest request, Coupon coupon,String account)
	{
		coupon = this.couponService.queryAllId(coupon.getCouponId());
		request.setAttribute("couponPojo", coupon);

		Map<String, Object> params = new HashMap<String, Object>();
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);

		}
		else
		{
			pageView = new PageView(1);
			if(account!=null)
			{
			params.put("account", account);
			}
			params.put("coupon_code", coupon.getCouponCode());
		}
		params.put("pageView", pageView);
		request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		request.setAttribute("pageView", pageView);

		List<UserCoupon> userCouponList = couponService.queryByCoupon_code(params);
		request.setAttribute("userCouponList", userCouponList);
		request.setAttribute("params", params);
		return "back/couponDetail";
	}

	/**
	 * 检查优惠券编码是否唯一
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/checkCouponCodeValid", method = RequestMethod.GET)
	public Map<String, Object> checkCouponCodeValid(HttpServletRequest request, String couponCode)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		String isValid = "Y";
		int count = couponService.checkCouponCode(couponCode);
		if (count > 0)
		{
			isValid = "N";
		}
		result.put("isValid", isValid);
		return result;
	}

	/**
	 * 进入发放优惠券
	 * 
	 * @param coupon
	 * @return
	 */
	@RequestMapping("prePushCouponToUser")
	public String prePushCouponToUser(HttpServletRequest request, Coupon coupon)
	{
		coupon = this.couponService.queryAllId(coupon.getCouponId());
		request.setAttribute("couponPojo", coupon);
		return "back/CouponPushToUser";
	}

	/**
	 * 发放优惠券
	 * 
	 * @param coupon
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/pushCouponToUser", method = RequestMethod.POST)
	public Map<String, Object> pushCouponToUser(HttpServletRequest request, Coupon coupon, int pushQuantity, String userName)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		String isPushed = "Y";
		String errorMsg = "";
		coupon = this.couponService.queryAllId(coupon.getCouponId());
		Map<String, Object> params = new HashMap<String, Object>();
		 params.put("account", userName);
		List<FrontUser> frontUserList = frontUserService.queryFrontUser(params);
		if (coupon == null)
		{
			isPushed = "N";
			errorMsg = "优惠券不存在";
			result.put("isPushed", isPushed);
			result.put("errorMsg", errorMsg);
			return result;
		}
		if (coupon.getStatus() == 2)
		{
			isPushed = "N";
			errorMsg = "优惠券未启用";
			result.put("isPushed", isPushed);
			result.put("errorMsg", errorMsg);
			return result;
		}
		if (frontUserList == null)
		{
			isPushed = "N";
			errorMsg = "下发用户不存在";
			result.put("isPushed", isPushed);
			result.put("errorMsg", errorMsg);
			return result;
		}
		if (coupon.getTotal_count() != -1 && coupon.getLeft_count() < pushQuantity)
		{
			isPushed = "N";
			errorMsg = "已超出可发放限额";
			result.put("isPushed", isPushed);
			result.put("errorMsg", errorMsg);
			return result;
		}
		/*
		 * Map<String, Object> params = new HashMap<String, Object>();
		 * params.put("user_id", frontUser.getUser_id());
		 * params.put("coupon_code", coupon.getCouponCode()); List<UserCoupon>
		 * userCouponList =
		 * userCouponMapper.queryUserCouponBycoupon_code(params); if
		 * (userCouponList != null && userCouponList.size() > 0) { UserCoupon
		 * userCoupon = userCouponList.get(0); Map<String, Object> paramsdd =
		 * new HashMap<String, Object>(); paramsdd.put("coupon_id",
		 * userCoupon.getCoupon_id()); paramsdd.put("quantity",
		 * userCoupon.getQuantity() + pushQuantity);
		 * userCouponMapper.updateUserCouponQuantity(paramsdd); } else {
		 */
		FrontUser frontUser=frontUserList.get(0);
		UserCoupon userCoupon = new UserCoupon();
		userCoupon.setCoupon_id(coupon.getCouponId());
		userCoupon.setCoupon_code(coupon.getCouponCode());
		userCoupon.setCoupon_name(coupon.getCoupon_name());
		userCoupon.setIs_repeat_use(coupon.getIsRepeatUse());
		userCoupon.setStatus(1);
		userCoupon.setDenomination(coupon.getDenomination());
		userCoupon.setQuantity(pushQuantity);
		userCoupon.setUser_id(frontUser.getUser_id());
		String valid_date = null;
		int validDays = coupon.getValidDays();
		if (validDays > 0 || validDays == -1)
		{
			if (validDays == -1)
			{
				validDays = 365 * 100;
			}
			valid_date = DateFormatUtils.getNextDayString(validDays);
		}
		else
		{
			valid_date = coupon.getExpirationTime();
		}
		userCoupon.setValid_date(valid_date);
		couponService.pushCouponToUser(userCoupon);
		// }
		if (coupon.getTotal_count() != -1)
		{
			coupon.setLeft_count(coupon.getLeft_count() - pushQuantity);
		}
		couponService.updateCoupon(coupon);
		result.put("isPushed", isPushed);
		result.put("errorMsg", errorMsg);
		return result;
	}
}
