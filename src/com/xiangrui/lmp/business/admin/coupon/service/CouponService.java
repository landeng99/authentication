package com.xiangrui.lmp.business.admin.coupon.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.coupon.vo.Coupon;
import com.xiangrui.lmp.business.admin.coupon.vo.CouponUsed;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;

/**
 * 优惠券
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午3:12:02
 *         </p>
 */
public interface CouponService
{

	/**
	 * 查询优惠券
	 * 
	 * @param params
	 * @return
	 */
	List<Coupon> queryAll(Map<String, Object> params);

	/**
	 * 优惠券来源分类统计
	 * 
	 * @return
	 */
	List<Coupon> queryAllType();

	/**
	 * 查询优惠券 单个的
	 * 
	 * @param id
	 * @return
	 */
	Coupon queryAllId(int id);

	/**
	 * 修改
	 * 
	 * @param coupon
	 */
	void updateCoupon(Coupon coupon);

	/**
	 * 修改
	 * 
	 * @param coupon
	 */
	void addCoupon(Coupon coupon);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void deleteCoupon(int id);

	/**
	 * 通过优惠券编辑查看详细信息
	 * 
	 * @param couponCode
	 * @return
	 */
	List<Coupon> queryByCouponCode(String couponCode);

	/**
	 * 通过优惠券编码查看发放用户列表
	 * 
	 * @param params
	 * @return
	 */
	List<UserCoupon> queryByCoupon_code(Map<String, Object> params);

	/**
	 * 下发优惠券给用户
	 * 
	 * @param coupon
	 */
	int pushCouponToUser(UserCoupon userCoupon);

	/**
	 * 根据用户ID来查询该用户可使用的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	public List<UserCoupon> queryAvliableCouponByUserId(Map<String, Object> params);

	/**
	 * 根据用户ID和需要支付金额查询可使用的优惠券（包裹支付页面用）
	 * 
	 * @param userId
	 * @return
	 */
	List<UserCoupon> queryAvliableCouponByUserIdAndNeedPay(Integer userId, float needPay);

	/**
	 * 根据用户ID来查询该用户已经使用的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	List<CouponUsed> queryUsedCouponByUserId(Map<String, Object> params);

	/**
	 * 根据用户ID来查询该用户已过期的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	List<UserCoupon> queryExpiredCouponByUserId(Map<String, Object> params);

	/**
	 * 下发注册优惠券
	 * 
	 * @param userId
	 * @return
	 */
	String pushRegistCoupon(Integer userId);

	/**
	 * 下发晒单优惠券
	 * 
	 * @param userId
	 * @return
	 */
	String pushShowOrderCoupon(Integer userId);

	/**
	 * 下发推荐优惠券
	 * 
	 * @param userId
	 * @return
	 */
	String pushRecommandCoupon(Integer userId,String source_friend);

	/**
	 * 使用优惠券
	 * 
	 * @param userId 使用者ID
	 * @param couponId 所使用优惠券ID
	 * @param packageId 所使用订单ID
	 * @param useQuantity 使用张数
	 * @return
	 */
	String useCoupon(Integer userId, Integer couponId,String logistics_code, int useQuantity);
	
	/**
	 * 用户优惠券数量统计
	 * 
	 * @param user_id
	 * @return
	 */
	int sumQuantityByUserId(Integer user_id);

	/**
	 * 用户可用优惠券数量统计
	 * 
	 * @param user_id
	 * @return
	 */
	int sumAvaliableQuantityByUserId(Integer user_id);
	
	/**
	 * 检查优惠券编号是否重复
	 * 
	 * @param couponCode
	 * @return
	 */
	Integer checkCouponCode(String couponCode);
	
	/**
	 * 查看下发给用户的某个优惠券
	 * @return
	 */
	UserCoupon queryUserCouponBycouponId(Integer couponId);
	
	/**
	 * 根据订单号查看已经使用过的优惠券
	 * @return
	 */
	CouponUsed queryCouponUsedByOrder_code(String order_code);
	
	/**
	 * 删除
	 */
    void deleteUserCoupon(int couponId);
    
	/**
	 * 下发优惠券
	 * @param userId
	 * @param coupon
	 * @return
	 */
	int pushUserCoupon(Integer userId,Coupon coupon);
	
	/**
	 * 根据用户ID来查询该用户获得的推荐优惠券
	 * 
	 * @param userId
	 * @return
	 */
	List<UserCoupon> queryUserRecommandUserCoupon(Integer userId);
}
