package com.xiangrui.lmp.business.admin.coupon.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.coupon.mapper.CouponMapper;
import com.xiangrui.lmp.business.admin.coupon.mapper.UserCouponMapper;
import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.Coupon;
import com.xiangrui.lmp.business.admin.coupon.vo.CouponUsed;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.util.DateFormatUtils;

/**
 * 优惠券
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午3:12:39
 *         </p>
 */
@Service("couponService")
public class CouponServiceImpl implements CouponService
{
	public final static int REGISTER_COUPON_ID = 1;
	public final static int SHOW_ORDER_COUPON_ID = 2;
	public final static int RECOMMAND_COUPON_ID = 3;
	/**
	 * 优惠券
	 */
	@Autowired
	private CouponMapper couponMapper;

	@Autowired
	private UserCouponMapper userCouponMapper;
	
	@Autowired
	private PackageService packageService;

	/**
	 * 查询,带参数
	 */
	@Override
	public List<Coupon> queryAll(Map<String, Object> params)
	{
		return couponMapper.queryAll(params);
	}

	/**
	 * 修改
	 */
	@SystemServiceLog(description = "修改优惠券")
	@Override
	public void updateCoupon(Coupon coupon)
	{
		couponMapper.updateCoupon(coupon);
	}

	/**
	 * 增加
	 */
	@SystemServiceLog(description = "增加优惠券")
	@Override
	public void addCoupon(Coupon coupon)
	{
		couponMapper.addCoupon(coupon);
	}
	/**
	 * 删除
	 */
	@SystemServiceLog(description = "删除优惠券")
	@Override
	public void deleteCoupon(int id)
	{
		couponMapper.deleteCoupon(id);
	}

	/**
	 * 查询优惠券,条件为优惠券id
	 */
	@Override
	public Coupon queryAllId(int id)
	{
		return couponMapper.queryAllId(id);
	}

	/**
	 * 查询所有
	 */
	@Override
	public List<Coupon> queryAllType()
	{
		return couponMapper.queryAllType();
	}

	/**
	 * 通过优惠券编辑查看详细信息
	 * 
	 * @param couponCode
	 * @return
	 */
	public List<Coupon> queryByCouponCode(String couponCode)
	{
		return couponMapper.queryByCouponCode(couponCode);
	}

	/**
	 * 通过优惠券编码查看发放用户列表
	 * 
	 * @param params
	 * @return
	 */
	public List<UserCoupon> queryByCoupon_code(Map<String, Object> params)
	{
		return userCouponMapper.queryByCoupon_code(params);
	}

	/**
	 * 下发优惠券给用户
	 * 
	 * @param coupon
	 */
	public int pushCouponToUser(UserCoupon userCoupon)
	{
		return userCouponMapper.pushCouponToUser(userCoupon);
	}
	
	/**
	 * 获取下发给用户优惠券ID
	 * 
	 * @param coupon
	 */
	public int getPushCouponToUserCoupon_id()
	{
		return userCouponMapper.getPushCouponToUserCoupon_id();
	}

	/**
	 * 根据用户ID来查询该用户可使用的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	public List<UserCoupon> queryAvliableCouponByUserId(Map<String, Object> params)
	{
		List<UserCoupon> list= userCouponMapper.queryAvliableCouponByUserId(params);
		if(list!=null&&list.size()>0)
		{
			for(UserCoupon userCoupon:list)
			{
				userCoupon.setAvalidCount(userCoupon.getQuantity()-userCoupon.getHaveUsedCount());
			}
		}
		return list;
	}

	/**
	 * 根据用户ID和需要支付金额查询可使用的优惠券（包裹支付页面用）
	 * 
	 * @param userId
	 * @return
	 */
	public List<UserCoupon> queryAvliableCouponByUserIdAndNeedPay(Integer userId, float needPay)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", userId);
		params.put("needpay", needPay);
		List<UserCoupon> list= userCouponMapper.queryAvliableCouponByUserId(params);
		if(list!=null&&list.size()>0)
		{
			for(UserCoupon userCoupon:list)
			{
				userCoupon.setAvalidCount(userCoupon.getQuantity()-userCoupon.getHaveUsedCount());
			}
		}
		return list;
	}

	/**
	 * 根据用户ID来查询该用户已经使用的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	public List<CouponUsed> queryUsedCouponByUserId(Map<String, Object> params)
	{
		return userCouponMapper.queryUsedCouponByUserId(params);
	}

	/**
	 * 根据用户ID来查询该用户已过期的优惠券
	 * 
	 * @param userId
	 * @return
	 */
	public List<UserCoupon> queryExpiredCouponByUserId(Map<String, Object> params)
	{
		return userCouponMapper.queryExpiredCouponByUserId(params);
	}

	/**
	 * 下发注册优惠券
	 * 
	 * @param userId
	 * @return
	 */
	public String pushRegistCoupon(Integer userId)
	{
		String msg = null;
		Coupon coupon = couponMapper.queryAllId(REGISTER_COUPON_ID);
		if (coupon.getStatus() == 2)
		{
			msg = "优惠券未启用";
		}
		else
		{
			UserCoupon userCoupon = new UserCoupon();
			userCoupon.setCoupon_id(coupon.getCouponId());
			userCoupon.setCoupon_code(coupon.getCouponCode());
			userCoupon.setCoupon_name(coupon.getCoupon_name());
			userCoupon.setIs_repeat_use(coupon.getIsRepeatUse());
			userCoupon.setDenomination(coupon.getDenomination());
			userCoupon.setStatus(1);
			userCoupon.setQuantity(coupon.getQuantity());
			userCoupon.setUser_id(userId);
			String valid_date = null;
			int validDays = coupon.getValidDays();
			if (validDays > 0 || validDays == -1)
			{
				if (validDays == -1)
				{
					validDays = 365 * 100;
				}
				valid_date = DateFormatUtils.getNextDayString(validDays);
			}
			else
			{
				valid_date = coupon.getExpirationTime();
			}
			userCoupon.setValid_date(valid_date);
			this.pushCouponToUser(userCoupon);
		}
		return msg;
	}

	/**
	 * 下发晒单优惠券
	 * 
	 * @param userId
	 * @return
	 */
	public String pushShowOrderCoupon(Integer userId)
	{
		String msg = null;
		Coupon coupon = couponMapper.queryAllId(SHOW_ORDER_COUPON_ID);
		if (coupon.getStatus() == 2)
		{
			msg = "优惠券未启用";
		}
		else
		{
			UserCoupon userCoupon = new UserCoupon();
			userCoupon.setCoupon_id(coupon.getCouponId());
			userCoupon.setCoupon_code(coupon.getCouponCode());
			userCoupon.setCoupon_name(coupon.getCoupon_name());
			userCoupon.setIs_repeat_use(coupon.getIsRepeatUse());
			userCoupon.setStatus(1);
			userCoupon.setQuantity(coupon.getQuantity());
			userCoupon.setDenomination(coupon.getDenomination());
			userCoupon.setUser_id(userId);
			String valid_date = null;
			int validDays = coupon.getValidDays();
			if (validDays > 0 || validDays == -1)
			{
				if (validDays == -1)
				{
					validDays = 365 * 100;
				}
				valid_date = DateFormatUtils.getNextDayString(validDays);
			}
			else
			{
				valid_date = coupon.getExpirationTime();
			}
			userCoupon.setValid_date(valid_date);
			this.pushCouponToUser(userCoupon);
		}
		return msg;
	}

	/**
	 * 下发推荐优惠券
	 * 
	 * @param userId
	 * @return
	 */
	public String pushRecommandCoupon(Integer userId,String source_friend)
	{
		String msg = null;
		Coupon coupon = couponMapper.queryAllId(RECOMMAND_COUPON_ID);
		if (coupon.getStatus() == 2)
		{
			msg = "优惠券未启用";
		}
		else
		{
			UserCoupon userCoupon = new UserCoupon();
			userCoupon.setCoupon_id(coupon.getCouponId());
			userCoupon.setCoupon_code(coupon.getCouponCode());
			userCoupon.setCoupon_name(coupon.getCoupon_name());
			userCoupon.setIs_repeat_use(coupon.getIsRepeatUse());
			userCoupon.setStatus(1);
			userCoupon.setQuantity(coupon.getQuantity());
			userCoupon.setDenomination(coupon.getDenomination());
			userCoupon.setUser_id(userId);
			userCoupon.setSource_friend(source_friend);
			String valid_date = null;
			int validDays = coupon.getValidDays();
			if (validDays > 0 || validDays == -1)
			{
				if (validDays == -1)
				{
					validDays = 365 * 100;
				}
				valid_date = DateFormatUtils.getNextDayString(validDays);
			}
			else
			{
				valid_date = coupon.getExpirationTime();
			}
			userCoupon.setValid_date(valid_date);
			this.pushCouponToUser(userCoupon);
		}
		return msg;
	}

	/**
	 * 使用优惠券
	 * 
	 * @param userId
	 *            使用者ID
	 * @param couponId
	 *            所使用优惠券ID
	 * @param logistics_code
	 *            所使用订单号
	 * @param useQuantity
	 *            使用张数
	 * @return
	 */
	public String useCoupon(Integer userId, Integer couponId, String logistics_code, int useQuantity)
	{
		String msg = null;
		UserCoupon userCoupon = userCouponMapper.queryUserCouponBycouponId(couponId);
		if (userCoupon != null)
		{
			if (userCoupon.getQuantity() >= (userCoupon.getHaveUsedCount() - useQuantity))
			{
				CouponUsed couponUsed = new CouponUsed();
				couponUsed.setCoupon_id(userCoupon.getCoupon_id());
				couponUsed.setCoupon_code(userCoupon.getCoupon_code());
				couponUsed.setCoupon_name(userCoupon.getCoupon_name());
				couponUsed.setDenomination((float) userCoupon.getDenomination());
				couponUsed.setQuantity(useQuantity);
				couponUsed.setUser_id(userId);
				couponUsed.setOrder_code(logistics_code);
				couponUsed.setReal_user_coupon_id(couponId);
				userCouponMapper.insertCouponUsed(couponUsed);
				packageService.updateCoupan_pay(logistics_code, (float)userCoupon.getDenomination());
			}
			else
			{
				msg = "优惠券不可用";
			}
		}
		else
		{
			msg = "优惠券尚没有下发给该用户";
		}

		return msg;
	}

	/**
	 * 用户优惠券数量统计
	 * 
	 * @param user_id
	 * @return
	 */
	public int sumQuantityByUserId(Integer user_id)
	{
		return userCouponMapper.sumQuantityByUserId(user_id);
	}

	/**
	 * 用户可用优惠券数量统计
	 * 
	 * @param user_id
	 * @return
	 */
	public int sumAvaliableQuantityByUserId(Integer user_id)
	{
		return userCouponMapper.sumAvaliableQuantityByUserId(user_id);
	}

	/**
	 * 检查优惠券编号是否重复
	 * 
	 * @param couponCode
	 * @return
	 */
	public Integer checkCouponCode(String couponCode)
	{
		return couponMapper.checkCouponCode(couponCode);
	}
	
	/**
	 * 查看下发给用户的某个优惠券
	 * @return
	 */
	public UserCoupon queryUserCouponBycouponId(Integer couponId)
	{
		return userCouponMapper.queryUserCouponBycouponId(couponId);
	}
	
	/**
	 * 根据订单号查看已经使用过的优惠券
	 * @return
	 */
	public CouponUsed queryCouponUsedByOrder_code(String order_code)
	{
		return userCouponMapper.queryCouponUsedByOrder_code(order_code);
	}
	
	
	/**
	 * 删除
	 */
	@SystemServiceLog(description = "删除优惠券")
	@Override
	public void deleteUserCoupon(int couponId)
	{
		userCouponMapper.deleteUserCoupon(couponId);
	}
	
	/**
	 * 下发优惠券(用于推荐赠送)
	 * @param userId
	 * @param coupon
	 * @return
	 */
	public int pushUserCoupon(Integer userId,Coupon coupon)
	{
		int userCount_id=-1;
		String msg = null;
		int pushQuantity=coupon.getQuantity();
		if (coupon.getTotal_count() != -1 && coupon.getLeft_count() < pushQuantity)
		{
			msg = "已超出可发放限额";
			return userCount_id;
		}
		if (coupon.getStatus() == 2)
		{
			msg = "优惠券未启用";
		}
		else
		{
			UserCoupon userCoupon = new UserCoupon();
			userCoupon.setCoupon_id(coupon.getCouponId());
			userCoupon.setCoupon_code(coupon.getCouponCode());
			userCoupon.setCoupon_name(coupon.getCoupon_name());
			userCoupon.setIs_repeat_use(coupon.getIsRepeatUse());
			userCoupon.setStatus(1);
			userCoupon.setQuantity(pushQuantity);
			userCoupon.setDenomination(coupon.getDenomination());
			userCoupon.setUser_id(userId);
			String valid_date = null;
			int validDays = coupon.getValidDays();
			if (validDays > 0 || validDays == -1)
			{
				if (validDays == -1)
				{
					validDays = 365 * 100;
				}
				valid_date = DateFormatUtils.getNextDayString(validDays);
			}
			else
			{
				valid_date = coupon.getExpirationTime();
			}
			userCoupon.setValid_date(valid_date);
			this.pushCouponToUser(userCoupon);
			userCount_id=getPushCouponToUserCoupon_id();
			if (coupon.getTotal_count() != -1)
			{
				coupon.setLeft_count(coupon.getLeft_count() - pushQuantity);
			}
			couponMapper.updateCoupon(coupon);
		}
		return userCount_id;
	}
	
	/**
	 * 根据用户ID来查询该用户获得的推荐优惠券
	 * 
	 * @param userId
	 * @return
	 */
	public List<UserCoupon> queryUserRecommandUserCoupon(Integer userId)
	{
		Coupon coupon = couponMapper.queryAllId(RECOMMAND_COUPON_ID);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", userId);
		params.put("coupon_code", coupon.getCouponCode());
		params.put("checksource_friend", "Y");
		return userCouponMapper.queryUserCouponBycoupon_code(params);
	}
}
