package com.xiangrui.lmp.business.admin.identifier.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.TransactionIsolationLevel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.xiangrui.lmp.business.admin.identifier.mapper.IdentifierMapper;
import com.xiangrui.lmp.business.admin.identifier.service.IdentifierService;
import com.xiangrui.lmp.business.admin.identifier.vo.Identifier;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.IdentifierCreator;

@Service(value = "identifierService")
public class IdentifierServiceImpl implements IdentifierService {
	private final static Logger logger = Logger.getLogger(IdentifierServiceImpl.class);
	/**
	 * 包裹类型
	 */
	public final static int  TYPE_PACKAGE=1;
	
	/**
	 * 提单编号类型
	 */
	public final static int TYPE_PICKORDER=2;
	
	
	/**
	 * 托盘类型
	 */
	public final static int TYPE_PALLET=3;

    /**
     * 同行账户导入批次
     */
    public final static int TYPE_PARTNER = 4;
	
	@Autowired
	private IdentifierMapper  identifierMapper;
	
	@Transactional
	public String getCode(Map<String, Object> params) {
		
		Identifier identifier =identifierMapper.queryIdentifier(params);
		
		int identifier_id=(Integer) params.get("identifier_id");
		
		if(identifier_id==TYPE_PICKORDER){
			return getPickOrderCode(identifier);
		}else if(identifier_id==TYPE_PACKAGE){
			return getPkgCode(identifier,params);
		}else if(identifier_id==TYPE_PALLET){
			
			return getPalletCode(identifier);
        } else if (identifier_id == TYPE_PARTNER)
        {
            return getBatchCode(identifier);
        }
		
		return null;
	}
	
	/**
	 * 以日期+编号（三位，不足则补零如20150509+001）格式输出字符串
	 * 业务实现思路：读取当前时间是与记录的时间一致，
	 * 如果不一致则，更新为当前时间，last_nums更新为1;
	 * 一致，则累加
	 * @return
	 */
	private  String getPickOrderCode(Identifier  identifier){
		String dateString=identifier.getPrefix();
		BigDecimal last_num=null;
		String currentDate=DateUtil.getFormatDateTime(new Date(), "yyyyMMdd");
	 
		Map<String,Object> params=new HashMap<String,Object>();
		
		
		if(currentDate.equals(dateString)){
			last_num=identifier.getLast_num();
			BigDecimal add_num=new BigDecimal(1);
			last_num=last_num.add(add_num);
			
		}else{
			
			//时间不等于当前时间，则重新累计
			last_num= new BigDecimal(1);
		
		}
		
		int num=last_num.intValue();
		String prefix=currentDate;
		String index= String.format("%03d", num);
		String code= prefix+index;
		params.put("identifier_id", identifier.getIdentifier_id());
		params.put("prefix", prefix);
		params.put("last_num", last_num);
	
		identifierMapper.updateIndentifier(params);
		return code;
	}
	
	/**
	 * 以日期+编号（三位，不足则补零如20150509+001）格式输出字符串
	 * 业务实现思路：读取当前时间是与记录的时间一致，
	 * 如果不一致则，更新为当前时间，last_nums更新为1;
	 * 一致，则累加
	 * @return
	 */
	private  String getPalletCode(Identifier  identifier){
		String dateString=identifier.getPrefix();
		BigDecimal last_num=null;
		String currentDate=DateUtil.getFormatDateTime(new Date(), "yyyyMMdd");
	 
		Map<String,Object> params=new HashMap<String,Object>();
		
		
		if(currentDate.equals(dateString)){
			last_num=identifier.getLast_num();
			BigDecimal add_num=new BigDecimal(1);
			last_num=last_num.add(add_num);
			
		}else{
			
			//时间不等于当前时间，则重新累计
			last_num= new BigDecimal(1);
		
		}
		
		int num=last_num.intValue();
		String prefix=currentDate;
		String index= String.format("%03d", num);
		String code= prefix+index;
		params.put("identifier_id", identifier.getIdentifier_id());
		params.put("prefix", prefix);
		params.put("last_num", last_num);
	
		identifierMapper.updateIndentifier(params);
		return code;
	}
	
	
	/**
	 * 业务实现思路：读取last_num, 并累加last_num
	 * 一致，则累加
	 * @return
	 */
	@Transactional(isolation=Isolation.SERIALIZABLE)
	private synchronized String getPkgCode(Identifier identifier,Map<String, Object> tparams){
		String code = null;
		BigDecimal last_num =null;
		Identifier liveIdentifier = identifierMapper.queryIdentifier(tparams);
		last_num = liveIdentifier.getLast_num();
		do
		{
			BigDecimal add_num = new BigDecimal(1);
			last_num = add_num.add(last_num);
			code = last_num.toString();
//			logger.info("------------getLogistics_code="+code);
		} while (IdentifierCreator.checkLogistics_code_listExist(code));
		IdentifierCreator.addLogistics_code_list(code);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("identifier_id", identifier.getIdentifier_id());
		params.put("last_num", last_num);
		identifierMapper.updateIndentifier(params);
		

		return code;
	}

    @Override
    public List<Identifier> queryAllByName(Map<String, Object> parmas)
    {
        return identifierMapper.queryAllByName(parmas);
    }

    @Override
    public void updateIndentifier(Map<String, Object> params)
    {
        identifierMapper.updateIndentifier(params);
    }
    
    
    /**
     * 以日期+编号（三位，不足则补零如20150509+001）格式输出字符串
     * 业务实现思路：读取当前时间是与记录的时间一致，
     * 如果不一致则，更新为当前时间，last_nums更新为1;
     * 一致，则累加
     * @return
     */
    private  String getBatchCode(Identifier  identifier){
        String dateString=identifier.getPrefix();
        BigDecimal last_num=null;
        String currentDate=DateUtil.getFormatDateTime(new Date(), "yyyyMMdd");
     
        Map<String,Object> params=new HashMap<String,Object>();
        
        
        if(currentDate.equals(dateString)){
            last_num=identifier.getLast_num();
            BigDecimal add_num=new BigDecimal(1);
            last_num=last_num.add(add_num);
            
        }else{
            
            //时间不等于当前时间，则重新累计
            last_num= new BigDecimal(1);
        
        }
        
        int num=last_num.intValue();
        String prefix=currentDate;
        String index= String.format("%03d", num);
        String code= prefix+index;
        params.put("identifier_id", identifier.getIdentifier_id());
        params.put("prefix", prefix);
        params.put("last_num", last_num);
    
        identifierMapper.updateIndentifier(params);
        return code;
    }
}
