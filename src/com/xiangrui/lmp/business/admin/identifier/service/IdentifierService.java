package com.xiangrui.lmp.business.admin.identifier.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.identifier.vo.Identifier;

public interface IdentifierService {
	
	
	/**获取提单编号
	 * @param params
	 * @return
	 */
	String getCode(Map<String,Object>params);

    List<Identifier> queryAllByName(Map<String, Object> parmas);
    void updateIndentifier(Map<String,Object> params);
}
