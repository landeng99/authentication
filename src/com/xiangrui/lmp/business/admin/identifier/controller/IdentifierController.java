package com.xiangrui.lmp.business.admin.identifier.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.identifier.service.IdentifierService;
import com.xiangrui.lmp.business.admin.identifier.vo.Identifier;
import com.xiangrui.lmp.business.base.BaseIdentifier;

@Controller
@RequestMapping("/admin/identifier")
public class IdentifierController
{

    @Autowired
    private IdentifierService identifierService;
    
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request)
    {
        Map<String, Object> parmas = new HashMap<String, Object>();
        parmas.put("identifier_id", BaseIdentifier.IDENTIFIER_ID_PACKAGE);
        List<Identifier> identifiers = identifierService.queryAllByName(parmas);
        if (identifiers.size() == 0)
        {
            request.setAttribute("identifier_Last_num", null);
        } else
        {
            request.setAttribute("identifier_Last_num", identifiers.get(0).getLast_num());
        }
        return "back/identifierInfo";
    }
    
    @RequestMapping("/updateIdentifier")
    public String updateSiteInfo(HttpServletRequest request,String last_num)
    {
        // 乱码处理
         String ident_last_num = "";
        try
        {
             ident_last_num = new String(last_num.getBytes("ISO-8859-1"),
                    "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
         //ident_last_num
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("prefix", null);
        params.put("last_num", ident_last_num);
        params.put("identifier_id", BaseIdentifier.IDENTIFIER_ID_PACKAGE);
        identifierService.updateIndentifier(params);

        return queryAll(request);
    }
}
