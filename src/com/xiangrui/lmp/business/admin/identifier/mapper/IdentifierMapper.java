package com.xiangrui.lmp.business.admin.identifier.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.identifier.vo.Identifier;

public interface IdentifierMapper {

	 Identifier queryIdentifier(Map<String,Object> params);
	
	 void updateIndentifier(Map<String,Object> params);

    List<Identifier> queryAllByName(Map<String, Object> parmas);
	
	
}
