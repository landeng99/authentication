package com.xiangrui.lmp.business.admin.menu.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.menu.vo.Menu;

public interface MenuMapper
{
    /**
     * 查询所有菜单信息
     * 
     * @return
     */
    List<Menu> queryAll();

    /**
     * 通过主键查找菜单信息
     * 
     * @param id
     * @return
     */
    Menu queryById(int id);

    /**
     * 更新菜单信息
     * 
     * @param menu
     */
    void update(Menu menu);

    /**
     * 根据主键删除菜单信息
     * 
     * @param id
     */
    void delete(int id);

    /**
     * 添加菜单
     * 
     * @param menu
     */
    void insert(Menu menu);

    /**
     * 通过父节点查询子节点信息
     * 
     * @param parent_id
     * @return
     */
    List<Menu> queryChildrenByParentId(String parent_id);

    /**
     * 查询含有子节点的菜单
     * 
     * @return
     */
    List<Menu> queryMenu();

    /**
     * 查询所有子节点
     * 
     * @return
     */
    List<Menu> queryChildren(String parent_id);

    /**
     * 查询
     * 
     * @param menu
     * @return
     */
    List<Menu> queryByUserIdAndPermission(Menu menu);

    /**
     * 查询
     * 
     * @param menu
     * @return
     */
    List<Menu> queryByUserIdAndPermissionByUserId(Menu menu);

    /**
     * 查询
     * 
     * @param par
     * @return
     */
    List<Menu> queryByUserIdAndPermissionAndParentId(Menu par);

}
