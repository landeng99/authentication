package com.xiangrui.lmp.business.admin.menu.vo;


import java.util.List;
import java.util.Set;

public class Menu implements Comparable{
	int id;
	String name;		//菜单名称
	String link;		//链接
	String parent_id;	//父id
	String permission;  //页面
	int orderType;      //排序
	
	private List<Menu> menus;
	
	public int getOrderType()
    {
        return orderType;
    }
    public void setOrderType(int orderType)
    {
        this.orderType = orderType;
    }
    public String getPermission()
    {
        return permission;
    }
    public void setPermission(String permission)
    {
        this.permission = permission;
    }
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	
	public List<Menu> getMenus()
    {
        return menus;
    }
    public void setMenus(List<Menu> menus)
    {
        this.menus = menus;
    }
    public Menu(int id, String name, String link, String parent_id) {
		super();
		this.id = id;
		this.name = name;
		this.link = link;
		this.parent_id = parent_id;
	}
	
	
	
	public Menu(String name, String link, String parent_id) {
		super();
		this.name = name;
		this.link = link;
		this.parent_id = parent_id;
	}
	public Menu() {
		super();
		 
	}
    @Override
    public int compareTo(Object o)
    {
        Menu menu=(Menu)o;
        return this.orderType-menu.getOrderType();
    }
}
