package com.xiangrui.lmp.business.admin.menu.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.menu.vo.Menu;

public interface MenuService
{
    /**
     * 查询所有菜单信息
     * 
     * @return
     */
    List<Menu> queryAll();

    /**
     * 通过主键查找菜单信息
     * 
     * @param id
     * @return
     */
    Menu queryById(int id);

    /**
     * 更新菜单信息
     * 
     * @param menu
     */
    void update(Menu menu);

    /**
     * 根据主键删除菜单信息
     * 
     * @param id
     */
    void delete(int id);

    /**
     * 添加菜单
     * 
     * @param menu
     */
    void insert(Menu menu);

    /**
     * 通过父节点查询子节点信息
     * 
     * @param parent_id
     *            = 0 是主菜单
     * @return
     */
    List<Menu> queryChildrenByParentId(String parent_id);

    /**
     * 查询权限
     * <p>
     * <strong>id=user_id</strong>用户id,跳过角色查询
     * </p>
     * <p>
     * <strong>link=permission</strong>link其实是permission的参数保存, 另外保存的是jsp页面名称
     * </p>
     * 
     * @param menu
     * @return
     */
    List<Menu> queryByUserIdAndPermission(Menu menu);

    /**
     * 查询权限
     * <p>
     * <strong>id=user_id</strong>用户id,跳过角色查询
     * </p>
     * 
     * @param menu
     * @return
     */
    List<Menu> queryByUserIdAndPermissionByUserId(Menu menu);

    /**
     * 查询含有子节点的菜单
     * 
     * @return
     */
    List<Menu> queryMenu();

    /**
     * 查询所有子节点
     * 
     * @return
     */
    List<Menu> queryChildren(String parent_id);

    List<Menu> queryByUserIdAndPermissionAndParentId(Menu par);

}
