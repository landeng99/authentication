package com.xiangrui.lmp.business.admin.menu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.menu.mapper.MenuMapper;
import com.xiangrui.lmp.business.admin.menu.service.MenuService;
import com.xiangrui.lmp.business.admin.menu.vo.Menu;

@Service(value = "menuService")
public class MenuServiceImpl implements MenuService
{

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> queryAll()
    {
        return menuMapper.queryAll();
    }

    @SystemServiceLog(description = "新增用户菜单信息")
    @Override
    public void insert(Menu menu)
    {
        menuMapper.insert(menu);
    }

    @SystemServiceLog(description = "修改用户菜单信息")
    @Override
    public void update(Menu menu)
    {
        menuMapper.update(menu);

    }

    @Override
    public Menu queryById(int id)
    {
        return menuMapper.queryById(id);
    }

    @SystemServiceLog(description = "删除用户菜单信息")
    @Override
    public void delete(int id)
    {
        menuMapper.delete(id);

    }

    @Override
    public List<Menu> queryChildrenByParentId(String parent_id)
    {
        return menuMapper.queryChildrenByParentId(parent_id);
    }

    @Override
    public List<Menu> queryMenu()
    {
        return menuMapper.queryMenu();
    }

    @Override
    public List<Menu> queryChildren(String parent_id)
    {
        return menuMapper.queryChildren(parent_id);
    }

    @Override
    public List<Menu> queryByUserIdAndPermission(Menu menu)
    {
        return menuMapper.queryByUserIdAndPermission(menu);
    }

    @Override
    public List<Menu> queryByUserIdAndPermissionByUserId(Menu menu)
    {
        return menuMapper.queryByUserIdAndPermissionByUserId(menu);
    }

    @Override
    public List<Menu> queryByUserIdAndPermissionAndParentId(Menu par)
    {
        return menuMapper.queryByUserIdAndPermissionAndParentId(par);
    }

}
