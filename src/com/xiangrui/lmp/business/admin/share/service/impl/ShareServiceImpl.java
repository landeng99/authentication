package com.xiangrui.lmp.business.admin.share.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.share.mapper.ShareMapper;
import com.xiangrui.lmp.business.admin.share.service.ShareService;
import com.xiangrui.lmp.business.admin.share.vo.FrontUserShare;
import com.xiangrui.lmp.business.admin.share.vo.PackageShare;
import com.xiangrui.lmp.business.admin.share.vo.Share;
import com.xiangrui.lmp.business.admin.share.vo.UserCoupon;

/**
 * 晒单
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午5:16:33
 * </p>
 */
@Service("shareService")
public class ShareServiceImpl implements ShareService
{

    /**
     * 晒单
     */
    @Autowired
    private ShareMapper shareMapper;

    /**
     * 查询所有晒单信息
     */
    @Override
    public List<Share> queryAll(Map<String, Object> params)
    {
        return shareMapper.queryAll(params);
    }

    /**
     * 查询单个晒单信息
     */
    @Override
    public Share queryAllId(int id)
    {
        return shareMapper.queryAllId(id);
    }

    /**
     * 查询晒单的包裹信息 id 包裹id claim.packageId
     */
    @Override
    public PackageShare queryPackageShare(int id)
    {
        return shareMapper.queryPackageShare(id);
    }

    /**
     * 查询用户的晒单,用户id
     */
    @Override
    public FrontUserShare queryFrontUserShare(int id)
    {
        return shareMapper.queryFrontUserShare(id);
    }

    /**
     * 新增
     */
    @SystemServiceLog(description = "新增晒单")
    @Override
    public void addShare(Share claim)
    {
        shareMapper.addShare(claim);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改晒单")
    @Override
    public void updateShare(Share claim)
    {
        shareMapper.updateShare(claim);
    }

    /**
     * 删除
     */
    @SystemServiceLog(description = "删除晒单")
    @Override
    public void deleteShare(int id)
    {
        shareMapper.deleteShare(id);
    }

    /**
     * 新增用户优惠券
     */
    @SystemServiceLog(description = "新增用户优惠券")
    @Override
    public void addUserCoupon(UserCoupon couponUsed)
    {
        shareMapper.addUserCoupon(couponUsed);
    }

}
