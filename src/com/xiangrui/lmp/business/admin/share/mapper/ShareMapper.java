package com.xiangrui.lmp.business.admin.share.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.share.vo.FrontUserShare;
import com.xiangrui.lmp.business.admin.share.vo.PackageShare;
import com.xiangrui.lmp.business.admin.share.vo.Share;
import com.xiangrui.lmp.business.admin.share.vo.UserCoupon;

/**
 * 晒单
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午5:15:42
 * </p>
 */
public interface ShareMapper
{

    /**
     * 查询所有晒单
     * 
     * @return
     */
    List<Share> queryAll(Map<String, Object> params);

    /**
     * 获取某个晒单信息
     * 
     * @param id
     * @return
     */
    Share queryAllId(int id);

    /**
     * 获取某个晒单信息的包裹信息
     * 
     * @param id
     *            包裹id claim.packageId
     * @return
     */
    PackageShare queryPackageShare(int id);

    /**
     * 获取某个晒单信息的包裹信息中链接的用户
     * 
     * @param id
     *            用户id
     * @return
     */
    FrontUserShare queryFrontUserShare(int id);

    /**
     * 添加
     * 
     * @param claim
     */
    void addShare(Share claim);

    /**
     * 插入对应的用户领取的优惠券
     * 
     * @param couponUsed
     */
    void addUserCoupon(UserCoupon userCoupon);

    /**
     * 修改
     * 
     * @param claim
     */
    void updateShare(Share claim);

    /**
     * 删除
     * 
     * @param id
     */
    void deleteShare(int id);

}
