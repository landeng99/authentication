package com.xiangrui.lmp.business.admin.share.vo;

import org.springframework.beans.factory.annotation.Autowired;

import com.xiangrui.lmp.business.base.BaseShare;

/**
 * 晒单实体
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午5:23:50
 *         </p>
 */
public class Share extends BaseShare
{

    private static final long serialVersionUID = 1L;

    /**
     * 优惠券id
     */
    private int userCouponId;

    /**
     * 优惠券数目 终止位置
     */
    private int couponNumStrat;

    /**
     * 优惠券数目 起始位置
     */
    private int couponNumEnd;

    /**
     * 晒单开始时间
     */
    private String shareTimeStrat;

    /**
     * 晒单结束时间
     */
    private String shareTimeEnd;

    /**
     * 晒单用户名
     */
    private String user_name;
    /**
     * 晒单帐号
     */
    private String account;
    /**
     * 包裹单号
     */
    private String logistics_code;

    /**
     * 取消赠送
     */
    private boolean canCancelAward;
    
    
    public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}

	public String getLogistics_code()
	{
		return logistics_code;
	}

	public void setLogistics_code(String logistics_code)
	{
		this.logistics_code = logistics_code;
	}

	public boolean isCanCancelAward()
	{
		return canCancelAward;
	}

	public void setCanCancelAward(boolean canCancelAward)
	{
		this.canCancelAward = canCancelAward;
	}

	public int getUserCouponId()
    {
        return userCouponId;
    }

    public void setUserCouponId(int userCouponId)
    {
        this.userCouponId = userCouponId;
    }

    public int getCouponNumStrat()
    {
        return couponNumStrat;
    }

    public void setCouponNumStrat(int couponNumStrat)
    {
        this.couponNumStrat = couponNumStrat;
    }

    public int getCouponNumEnd()
    {
        return couponNumEnd;
    }

    public void setCouponNumEnd(int couponNumEnd)
    {
        this.couponNumEnd = couponNumEnd;
    }

    public String getShareTimeStrat()
    {
        return shareTimeStrat;
    }

    public void setShareTimeStrat(String shareTimeStrat)
    {
        this.shareTimeStrat = shareTimeStrat;
    }

    public String getShareTimeEnd()
    {
        return shareTimeEnd;
    }

    public void setShareTimeEnd(String shareTimeEnd)
    {
        this.shareTimeEnd = shareTimeEnd;
    }

}
