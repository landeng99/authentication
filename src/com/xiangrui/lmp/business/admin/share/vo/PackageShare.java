package com.xiangrui.lmp.business.admin.share.vo;

import com.xiangrui.lmp.business.base.BasePackage;

/**
 * 晒单专用包裹
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午5:23:35
 * </p>
 */
public class PackageShare extends BasePackage
{

    private static final long serialVersionUID = 1L;

}
