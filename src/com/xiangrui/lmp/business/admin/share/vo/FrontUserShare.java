package com.xiangrui.lmp.business.admin.share.vo;

import com.xiangrui.lmp.business.admin.claim.vo.FrontUserClaim;

/**
 * 晒单专用用户
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午5:23:14
 * </p>
 */
public class FrontUserShare extends FrontUserClaim
{

    private static final long serialVersionUID = 1L;

}
