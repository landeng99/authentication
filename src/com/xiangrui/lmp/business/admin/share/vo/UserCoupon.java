package com.xiangrui.lmp.business.admin.share.vo;

import com.xiangrui.lmp.business.base.BaseUserCoupon;

/**
 * 晒单连接的优惠券
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午5:24:02
 * </p>
 */
public class UserCoupon extends BaseUserCoupon
{

    private static final long serialVersionUID = 1L;

}
