package com.xiangrui.lmp.business.admin.share.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.Coupon;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;
import com.xiangrui.lmp.business.admin.share.service.ShareService;
import com.xiangrui.lmp.business.admin.share.vo.Share;
import com.xiangrui.lmp.business.base.BaseShare;
import com.xiangrui.lmp.business.homepage.service.PkgImgService;
import com.xiangrui.lmp.business.homepage.vo.PackageImg;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 晒单
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午5:02:42
 *         </p>
 */
@Controller
@RequestMapping("/admin/share")
public class ShareController
{

	/**
	 * 晒单
	 */
	@Autowired
	private ShareService shareService;

	/**
	 * 优惠券
	 */
	@Autowired
	private CouponService couponService;
	/**
	 * 晒单图片
	 */
	@Autowired
	private PkgImgService frontPkgImgService;

	/**
	 * 首次查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryAllOne")
	public String indexFink(HttpServletRequest request)
	{
		return queryAll(request, null);
	}

	/**
	 * 新的分页查询所有索赔
	 * 
	 * @param request
	 * @param claim
	 * @return
	 */
	@RequestMapping("/queryAll")
	public String claimPage(HttpServletRequest request, Share share)
	{
		return queryAll(request, share);
	}

	/**
	 * 新的分页查询所有索赔
	 * 
	 * @param request
	 * @param claim
	 * @return
	 */
	@RequestMapping("/queryAllnew")
	public String queryAll(HttpServletRequest request, Share share)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);

		// 首次查询 当没有有request范围内的参数aAll的设置时,不是首次查询
		if (null != share)
		{
			params.put("USERNAME", share.getUser_name());
			params.put("LOGISTICS_CODE", share.getLogistics_code());
			params.put("STATUS", share.getStatus());
			params.put("COUPONNUMSTRAT", share.getCouponNumStrat());
			params.put("COUPONNUMEND", share.getCouponNumEnd());
			params.put("SHARETIMESTRAT", share.getShareTimeStrat());
			params.put("SHARETIMEEND", share.getShareTimeEnd());
			params.put("APPROVALSTATUS", share.getApproval_status());
		}

		List<Share> list = shareService.queryAll(params);
		if (list != null && list.size() > 0)
		{
			for (Share tshare : list)
			{
				boolean canCancelAward = false;
				UserCoupon userCoupon = couponService.queryUserCouponBycouponId(tshare.getAward_coupon_id());
				// 只有已赠送了优惠券且该优惠券未使用过的才可以取消赠送，点击‘取消赠送
				if (userCoupon != null && userCoupon.getHaveUsedCount() == 0)
				{
					canCancelAward = true;
				}
				tshare.setCanCancelAward(canCancelAward);
			}

		}
		request.setAttribute("pageView", pageView);
		request.setAttribute("shareLists", list);
		return "back/shareList";
	}

	/**
	 * 进入修改
	 * 
	 * @param request
	 * @param share
	 * @return
	 */
	@RequestMapping("toUpdateShare")
	public String toUpdateShare(HttpServletRequest request, Share share)
	{
		share = this.shareService.queryAllId(share.getShare_id());
		UserCoupon userCoupon = new UserCoupon();
		List<PackageImg> pkgImgList = new ArrayList<PackageImg>();
		if (share != null)
		{
			userCoupon = couponService.queryUserCouponBycouponId(share.getAward_coupon_id());
			PackageImg packageImg = new PackageImg();
			packageImg.setImg_type(PackageImg.IMG_SHARE_TYPE);
			packageImg.setPackage_id(share.getPackage_id());
			// 查询晒单图片
			pkgImgList = frontPkgImgService.queryPkgImg(packageImg);
		}
		request.setAttribute("sharePojo", share);
		request.setAttribute("userCoupon", userCoupon);
		request.setAttribute("pkgImgList", pkgImgList);
		return "back/updateShare";
	}

	/**
	 * 进入修改
	 * 
	 * @param request
	 * @param share
	 * @return
	 */
	@RequestMapping("toUpdateShareOne")
	public String toUpdateShareOne(HttpServletRequest request, Share share)
	{
		share = this.shareService.queryAllId(share.getShare_id());
		UserCoupon userCoupon = new UserCoupon();
		List<PackageImg> pkgImgList = new ArrayList<PackageImg>();
		if (share != null)
		{
			userCoupon = couponService.queryUserCouponBycouponId(share.getAward_coupon_id());
			PackageImg packageImg = new PackageImg();
			packageImg.setImg_type(PackageImg.IMG_SHARE_TYPE);
			packageImg.setPackage_id(share.getPackage_id());
			// 查询晒单图片
			pkgImgList = frontPkgImgService.queryPkgImg(packageImg);
		}
		request.setAttribute("sharePojo", share);
		request.setAttribute("userCoupon", userCoupon);
		request.setAttribute("pkgImgList", pkgImgList);

		// 查询所有优惠券
		List<Coupon> list = this.couponService.queryAllType();
		request.setAttribute("couponLists", list);
		return "back/updateShare_approval";
	}

	/**
	 * 进行修改
	 * 
	 * @param share
	 * @return
	 */
	@RequestMapping("updateShare")
	public String updateShare(HttpServletRequest request, Share share, Integer awardCouponId, Integer awardCount)
	{
		int approvalStatus = share.getApproval_status();
		String reason=StringUtils.trimToNull(share.getReason());
		if(reason!=null)
		{
			try
			{
				reason=new String(reason.getBytes("iso-8859-1"), "utf-8");
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
		}
		share = this.shareService.queryAllId(share.getShare_id());
		// 1：未审核，2：审核通过，3：审核拒绝'
		if (BaseShare.APPROVE_THROUGH == approvalStatus)
		{
			// 状态已赠送
			share.setStatus(BaseShare.SHARE_STATUS_GIVE);

			// 赠送数量
			Coupon coupon = this.couponService.queryAllId(awardCouponId);
			coupon.setQuantity(awardCount);
			int userCouponId = couponService.pushUserCoupon(share.getUser_id(), coupon);
			share.setAward_coupon_id(userCouponId);
			share.setCoupon_num(awardCount);
		}
		share.setReason(reason);
		share.setApproval_status(approvalStatus);
		this.shareService.updateShare(share);
		return queryAll(request, null);
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param share
	 * @return
	 */
	@RequestMapping("deleteShare")
	public String deleteShare(Share share)
	{
		this.shareService.deleteShare(share.getShare_id());
		return "back/shareList";
	}

	/**
	 * 取消赠送
	 * 
	 * @param share
	 * @return
	 */
	@RequestMapping("cancelAward")
	public String cancelAward(Share share)
	{
		couponService.deleteUserCoupon(share.getAward_coupon_id());
		share = this.shareService.queryAllId(share.getShare_id());
		share.setAward_coupon_id(-1);
		this.shareService.updateShare(share);
		return "back/shareList";
	}

	/**
	 * 检查是否可以赠送优惠券
	 */
	@RequestMapping("/checkCanAwardCoupon")
	@ResponseBody
	public Map<String, String> checkCanAwardCoupon(HttpServletRequest req, Integer coupon_id, int awardCount)
	{
		Map<String, String> result = new HashMap<String, String>();
		String canAward = "N";
		Coupon coupon = this.couponService.queryAllId(coupon_id);
		if (coupon != null)
		{
			if (coupon.getStatus() != 2)
			{
				if (coupon.getTotal_count() != -1)
				{
					if (coupon.getLeft_count() >= awardCount)
					{
						canAward = "Y";
					}
				}
				else
				{
					canAward = "Y";
				}
			}
		}
		result.put("canAward", canAward);
		return result;
	}
}
