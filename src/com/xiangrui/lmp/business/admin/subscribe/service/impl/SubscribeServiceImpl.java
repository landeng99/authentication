package com.xiangrui.lmp.business.admin.subscribe.service.impl;



import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.subscribe.mapper.SubscribeMapper;
import com.xiangrui.lmp.business.admin.subscribe.service.SubscribeService;
import com.xiangrui.lmp.business.admin.subscribe.vo.Subscribe;

@Service("subscribeService")
public class SubscribeServiceImpl implements SubscribeService
{
    @Autowired
    SubscribeMapper subscribeMapper;
    

    @Override
    public int querySubscribeCnt(String number)
    {
        
        return 0;
    }
    
    @Override
    public Subscribe querySubscribeByParam(Map<String, Object> params)
    {
        return subscribeMapper.querySubscribeByParam(params);
    }
    
    @Override
    public Subscribe querySubscribe(String number)
    {
      
        return subscribeMapper.querySubscribe(number);
    }
    @Override
    public Subscribe querySubscribeByLogisticsCode(String logistics_code)
    {
     
        return subscribeMapper.querySubscribeByLogisticsCode(logistics_code);
    }
}
