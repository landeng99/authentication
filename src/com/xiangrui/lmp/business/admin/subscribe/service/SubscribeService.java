package com.xiangrui.lmp.business.admin.subscribe.service;

import java.util.Map;

import com.xiangrui.lmp.business.admin.subscribe.vo.Subscribe;

public interface SubscribeService
{
    int  querySubscribeCnt(String number);
    
    Subscribe querySubscribe(String number);
    
    Subscribe querySubscribeByParam(Map<String,Object> params);
    
    Subscribe querySubscribeByLogisticsCode(String logistics_code);
}
