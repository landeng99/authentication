package com.xiangrui.lmp.business.admin.subscribe.mapper;

import java.util.Map;

import com.xiangrui.lmp.business.admin.subscribe.vo.Subscribe;

public interface SubscribeMapper
{
    
    Subscribe querySubscribeByParam(Map<String,Object>params);
    
    Subscribe querySubscribe(String number);
    
    Subscribe querySubscribeByLogisticsCode(String logisticsCode);
}
