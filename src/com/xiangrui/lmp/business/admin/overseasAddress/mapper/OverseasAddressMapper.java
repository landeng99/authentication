package com.xiangrui.lmp.business.admin.overseasAddress.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

public interface OverseasAddressMapper
{
	/**
	 * 通过地址ID查询地址
	 * 
	 * @param osaddr_id
	 * @return OverseasAddress
	 */
	OverseasAddress queryOverseasAddressById(int osaddr_id);

	/**
	 * 通过用户ID来查询该用户所拥有的权限仓库
	 * 
	 * @param user_id
	 * @return
	 */
	List<OverseasAddress> queryOverseasAddressByUserId(int user_id);

	/**
	 * 所有国家
	 * 
	 * @param params
	 * @return
	 */
	List<String> selectCountryName();

	/**
	 * 分页查询地址
	 * 
	 * @param params
	 * @return
	 */
	List<OverseasAddress> queryAllOverseasAddress(Map<String, Object> params);

	/**
	 * 添加地址
	 * 
	 * @param overseasAddress
	 * @return
	 */
	int insertOverseasAddress(OverseasAddress overseasAddress);

	/**
	 * 更新地址
	 * 
	 * @param overseasAddress
	 * @return
	 */
	void updateOverseasAddress(OverseasAddress overseasAddress);

	/**
	 * 逻辑删除
	 * 
	 * @param overseasAddress
	 * @return
	 */
	void updateIsdeleted(OverseasAddress overseasAddress);

	/**
	 * 写入海外地址只对客户可见的关联
	 * @param params
	 */
	void insertAuthOverseasAddress(Map<String, Object> params);
	
	/**
	 * 删除
	 * @param osaddr_id
	 */
	void deleteAuthOverseasAddressByOsaddrId(Integer osaddr_id);
	/**
	 * 找出授权的用户
	 * @param osaddr_id
	 * @return
	 */
	List<FrontUser> queryFrontUserByAuthOsaddrId(Integer osaddr_id);
}
