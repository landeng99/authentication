package com.xiangrui.lmp.business.admin.overseasAddress.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.overseasAddress.controller.overseasAddressController;
import com.xiangrui.lmp.business.admin.overseasAddress.mapper.OverseasAddressMapper;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

@Service(value = "overseasAddressService")
public class OverseasAddressServiceImpl implements OverseasAddressService
{
	@Autowired
	private OverseasAddressMapper userAddressMapper;

	/**
	 * 通过地址ID查询地址
	 * 
	 * @param osaddr_id
	 * @return OverseasAddress
	 */
	@Override
	public OverseasAddress queryOverseasAddressById(int osaddr_id)
	{
		return userAddressMapper.queryOverseasAddressById(osaddr_id);
	}

	/**
	 * 所有国家
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<String> selectCountryName()
	{
		return userAddressMapper.selectCountryName();
	}

	/**
	 * 分页查询地址
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<OverseasAddress> queryAllOverseasAddress(Map<String, Object> params)
	{
		return userAddressMapper.queryAllOverseasAddress(params);
	}

	/**
	 * 添加地址
	 * 
	 * @param overseasAddress
	 * @return
	 */
	@SystemServiceLog(description = "新增地址信息")
	@Override
	public int insertOverseasAddress(OverseasAddress overseasAddress)
	{
		return userAddressMapper.insertOverseasAddress(overseasAddress);
	}

	/**
	 * 更新地址
	 * 
	 * @param overseasAddress
	 * @return
	 */
	@SystemServiceLog(description = "修改地址信息")
	@Override
	public void updateOverseasAddress(OverseasAddress overseasAddress)
	{
		userAddressMapper.updateOverseasAddress(overseasAddress);
	}

	/**
	 * 逻辑删除
	 * 
	 * @param overseasAddress
	 * @return
	 */
	@SystemServiceLog(description = "地址信息删除")
	@Override
	public void updateIsdeleted(OverseasAddress overseasAddress)
	{
		userAddressMapper.updateIsdeleted(overseasAddress);
	}

	/**
	 * 通过用户ID来查询该用户所拥有的权限仓库
	 * 
	 * @param user_id
	 * @return
	 */
	@Override
	public List<OverseasAddress> queryOverseasAddressByUserId(int user_id)
	{
		return userAddressMapper.queryOverseasAddressByUserId(user_id);
	}

	/**
	 * 写入海外地址只对客户可见的关联
	 * 
	 * @param params
	 */
	public void insertAuthOverseasAddress(Map<String, Object> params)
	{
		userAddressMapper.insertAuthOverseasAddress(params);
	}

	/**
	 * 删除
	 * 
	 * @param osaddr_id
	 */
	public void deleteAuthOverseasAddressByOsaddrId(Integer osaddr_id)
	{
		userAddressMapper.deleteAuthOverseasAddressByOsaddrId(osaddr_id);
	}

	public List<FrontUser> queryFrontUserByAuthOsaddrId(Integer osaddr_id)
	{
		return userAddressMapper.queryFrontUserByAuthOsaddrId(osaddr_id);
	}
}
