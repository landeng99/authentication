package com.xiangrui.lmp.business.admin.overseasAddress.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.PackageImg;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/overSea")
public class overseasAddressController
{

	@Autowired
	private OverseasAddressService overseasAddressService;
	@Autowired
	private FrontUserMapper frontUserMapper;

	/**
	 * 逻辑删除0：未删除
	 */
	private static final int ISDELETED_NO = 0;

	/**
	 * 逻辑删除1：删除
	 */
	private static final int ISDELETED_YES = 1;

	/**
	 * 海外地址列表初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/overseasAddressInit")
	public String overseasAddressInit(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);
		params.put("isdeleted", ISDELETED_NO);
		// 国家下拉框
		List<String> countryList = overseasAddressService.selectCountryName();

		// 海外地址查询
		List<OverseasAddress> overseasAddressList = overseasAddressService.queryAllOverseasAddress(params);

		request.setAttribute("overseasAddressList", overseasAddressList);
		request.setAttribute("countryList", countryList);
		request.setAttribute("pageView", pageView);

		return "back/overseasAddressList";
	}

	/**
	 * 海外地址列表查询
	 * 
	 * @param request
	 * @param is_tax_free
	 * @return
	 */
	@RequestMapping("/overseasAddressSerach")
	public String overseasAddressSerach(HttpServletRequest request, String is_tax_free, String country)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);
		params.put("is_tax_free", is_tax_free);
		params.put("country", country);
		params.put("isdeleted", ISDELETED_NO);
		// 国家下拉框
		List<String> countryList = overseasAddressService.selectCountryName();

		// 海外地址查询
		List<OverseasAddress> overseasAddressList = overseasAddressService.queryAllOverseasAddress(params);

		request.setAttribute("overseasAddressList", overseasAddressList);
		request.setAttribute("countryList", countryList);
		request.setAttribute("pageView", pageView);

		return "back/overseasAddressList";
	}

	/**
	 * 海外地址列添加初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/addOverseasAddress")
	public String addOverseasAddress(HttpServletRequest request)
	{

		return "back/addOverseasAddress";
	}

	/**
	 * 海外地址列添加
	 * 
	 * @param request
	 * @param overseasAddress
	 * @return
	 */
	@RequestMapping("/insertOverseasAddress")
	@ResponseBody
	public String insertOverseasAddress(HttpServletRequest request, OverseasAddress overseasAddress, String authAccount)
	{
		String need_auth_flag = "Y";
		authAccount = StringUtils.trimToNull(authAccount);
		if (authAccount == null)
		{
			need_auth_flag = "N";
		}
		overseasAddress.setNeed_auth_flag(need_auth_flag);
		overseasAddressService.insertOverseasAddress(overseasAddress);
		if (authAccount != null)
		{
			String[] authAccountArray = authAccount.split(",");
			if (authAccountArray != null && authAccountArray.length > 0)
			{
				for (int i = 0; i < authAccountArray.length; i++)
				{
					Map<String, Object> fparams = new HashMap<String, Object>();
					fparams.put("account", authAccountArray[i]);
					List<FrontUser> frontUserList = frontUserMapper.queryFrontUserByAccount(fparams);
					if (frontUserList != null && frontUserList.size() > 0)
					{
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("osaddr_id", overseasAddress.getId());
						params.put("user_id", frontUserList.get(0).getUser_id());
						overseasAddressService.insertAuthOverseasAddress(params);
					}
				}
			}
		}
		return null;
	}

	/**
	 * 海外地址详情
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/overseasAddressDetail")
	public String overseasAddressDetail(HttpServletRequest request, String id)
	{
		// 海外地址查询
		OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(Integer.parseInt(id));
		String authAccount = "";
		List<FrontUser> frontUserList = overseasAddressService.queryFrontUserByAuthOsaddrId(overseasAddress.getId());
		if (frontUserList != null && frontUserList.size() > 0)
		{
			for (FrontUser frontUser : frontUserList)
			{
				authAccount += frontUser.getAccount() + ",";
			}
		}
		request.setAttribute("authAccount", authAccount);
		request.setAttribute("overseasAddress", overseasAddress);

		return "back/overseasAddressDetail";
	}

	/**
	 * 海外地址详情
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/overseasAddressEdit")
	public String overseasAddressEdit(HttpServletRequest request, String id)
	{
		// 分页查询

		// 海外地址查询
		OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(Integer.parseInt(id));

		String authAccount = "";
		List<FrontUser> frontUserList = overseasAddressService.queryFrontUserByAuthOsaddrId(overseasAddress.getId());
		if (frontUserList != null && frontUserList.size() > 0)
		{
			for (FrontUser frontUser : frontUserList)
			{
				authAccount += frontUser.getAccount() + ",";
			}
		}
		request.setAttribute("authAccount", authAccount);
		request.setAttribute("overseasAddress", overseasAddress);

		return "back/updateOverseasAddress";
	}

	/**
	 * 海外地址列更新
	 * 
	 * @param request
	 * @param overseasAddress
	 * 
	 * @return
	 */
	@RequestMapping("/updateOverseasAddress")
	@ResponseBody
	public String updateOverseasAddress(HttpServletRequest request, OverseasAddress overseasAddress, String authAccount)
	{
		String need_auth_flag = "Y";
		authAccount = StringUtils.trimToNull(authAccount);
		if (authAccount == null)
		{
			need_auth_flag = "N";
		}
		overseasAddress.setNeed_auth_flag(need_auth_flag);
		overseasAddressService.updateOverseasAddress(overseasAddress);
		overseasAddressService.deleteAuthOverseasAddressByOsaddrId(overseasAddress.getId());
		if (authAccount != null)
		{
			String[] authAccountArray = authAccount.split(",");
			if (authAccountArray != null && authAccountArray.length > 0)
			{
				for (int i = 0; i < authAccountArray.length; i++)
				{
					Map<String, Object> fparams = new HashMap<String, Object>();
					fparams.put("account", authAccountArray[i]);
					List<FrontUser> frontUserList = frontUserMapper.queryFrontUserByAccount(fparams);
					if (frontUserList != null && frontUserList.size() > 0)
					{
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("osaddr_id", overseasAddress.getId());
						params.put("user_id", frontUserList.get(0).getUser_id());
						overseasAddressService.insertAuthOverseasAddress(params);
					}
				}
			}
		}
		return null;
	}

	/**
	 * 逻辑删除
	 * 
	 * @param request
	 * @param overseasAddress
	 * 
	 * @return
	 */
	@RequestMapping("/deleteOverseasAddress")
	@ResponseBody
	public String deleteOverseasAddress(HttpServletRequest request, OverseasAddress overseasAddress)
	{
		overseasAddress.setIsdeleted(ISDELETED_YES);
		overseasAddressService.updateIsdeleted(overseasAddress);
		return null;
	}

	/**
	 * 
	 * 上传国旗
	 */
	@RequestMapping(value = "/uploadFlag", method = RequestMethod.POST)
	public void uploadFlag(HttpServletRequest request, HttpServletResponse response, OverseasAddress overseasAddress) throws IOException
	{

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

		// 获取前台传值
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();

		String configPath = File.separator + "upload" + File.separator + "icon" + File.separator;
		// 图片存储路径
		String dataPath = "/upload/icon/";
		String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource";

		ctxPath += configPath;
		// 创建文件夹
		File file = new File(ctxPath);
		if (!file.exists())
		{
			file.mkdirs();
		}

		String fileName = null;

		// 返回前台的图片地址
		String rtnPath = null;

		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet())
		{
			// 上传文件名
			System.out.println("key: " + entity.getKey());
			MultipartFile mf = entity.getValue();
			fileName = mf.getOriginalFilename();
			// 图片格式
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

			// 图片名称
			String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;

			File uploadFile = new File(ctxPath + newFileName);
			System.out.println(newFileName);

			rtnPath = dataPath + newFileName;
			overseasAddress.setFlag_path(rtnPath);
			try
			{
				FileCopyUtils.copy(mf.getBytes(), uploadFile);
			} catch (IOException e)
			{
				rtnPath = "上传失败";
				e.printStackTrace();
			}
			response.setHeader("Content-type", "text/html;charset=UTF-8");

			// 这句话的意思，是告诉servlet用UTF-8转码，而不是用默认的ISO8859
			response.setCharacterEncoding("UTF-8");
			response.getWriter().append(rtnPath);

		}

	}
}
