package com.xiangrui.lmp.business.admin.export.vo;

import com.xiangrui.lmp.business.base.BaseExport;

public class PkgExport extends BaseExport
{
    
    
    private String ems_code;
    
    /**
     * 分运单号
     */
    private String original_num;
    
    
    /**
     * 物流单号
     */
    private String logistics_code;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 详细地址
     */
    private String street;

    /**
     * 邮编
     */
    private String postal_code;

    /**
     * 收件人
     */
    private String receiver;

    /**
     * 收件人手机号码
     */
    private String receiver_mobile;

    /**
     * 身份证号
     */
    private String idcard;

    /**
     * 发送人
     */
    private String user_name;

    /**
     * 重量
     */
    private float actual_weight;

    /**
     * 发送人手机号码
     */
    private String send_mobile;
    
    
    private String send_address;

    /**
     * 物品名称
     */
    private String goods_names;

    /**
     * 数量
     */
    private String quantity;

    /**
     * 单位
     */
    private String unit;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 单价
     */
    private float price;

    /**
     * 规格
     */
    private String spec;
    
    /**
     * 商品详情
     */
    private String goods_detail;
    
    /**
     * 地区字符串
     */
    private String region;
    
    //保费
    private float keep_price;
    
    //业务
    private String business_name;
    
    //渠道
    private String express_package;
    
    //申报类别
    private String name_specs;
    /**
     * 报关号码
     */
    private String declaration_code;
    
    public String getDeclaration_code()
	{
		return declaration_code;
	}

	public void setDeclaration_code(String declaration_code)
	{
		this.declaration_code = declaration_code;
	}

	public float getKeep_price()
	{
		return keep_price;
	}

	public void setKeep_price(float keep_price)
	{
		this.keep_price = keep_price;
	}

	public String getBusiness_name()
	{
		return business_name;
	}

	public void setBusiness_name(String business_name)
	{
		this.business_name = business_name;
	}

	public String getEms_code()
    {
        return ems_code;
    }

    public void setEms_code(String ems_code)
    {
        this.ems_code = ems_code;
    }

    public String getOriginal_num()
    {
        return original_num;
    }

    public void setOriginal_num(String original_num)
    {
        this.original_num = original_num;
    }

    
    public String getExpress_package()
	{
		return express_package;
	}

	public void setExpress_package(String string)
	{
		this.express_package = string;
	}

	public String getLogistics_code()
    {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getPostal_code()
    {
        return postal_code;
    }

    public void setPostal_code(String postal_code)
    {
        this.postal_code = postal_code;
    }

    public String getReceiver()
    {
        return receiver;
    }

    public void setReceiver(String receiver)
    {
        this.receiver = receiver;
    }

    public String getReceiver_mobile()
    {
        return receiver_mobile;
    }

    public void setReceiver_mobile(String receiver_mobile)
    {
        this.receiver_mobile = receiver_mobile;
    }

    public String getIdcard()
    {
        return idcard;
    }

    public void setIdcard(String idcard)
    {
        this.idcard = idcard;
    }

    public String getUser_name()
    {
        return user_name;
    }

    public void setUser_name(String user_name)
    {
        this.user_name = user_name;
    }

 

    public float getActual_weight()
    {
        return actual_weight;
    }

    public void setActual_weight(float actual_weight)
    {
        this.actual_weight = actual_weight;
    }

    public String getSend_mobile()
    {
        return send_mobile;
    }

    public void setSend_mobile(String send_mobile)
    {
        this.send_mobile = send_mobile;
    }

    public String getGoods_names()
    {
        return goods_names;
    }

    public void setGoods_names(String goods_names)
    {
        this.goods_names = goods_names;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public String getSpec()
    {
        return spec;
    }

    public void setSpec(String spec)
    {
        this.spec = spec;
    }

    public String getGoods_detail()
    {
        return goods_detail;
    }

    public void setGoods_detail(String goods_detail)
    {
        this.goods_detail = goods_detail;
    }

    public String getSend_address()
    {
        return send_address;
    }

    public void setSend_address(String send_address)
    {
        this.send_address = send_address;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

    
	public String getName_specs()
	{
		return name_specs;
	}

	public void setName_specs(String name_specs)
	{
		this.name_specs = name_specs;
	}

	@Override
	public String toString()
	{
		return "PkgExport [ems_code=" + ems_code + ", original_num="
				+ original_num + ", logistics_code=" + logistics_code
				+ ", province=" + province + ", city=" + city + ", street="
				+ street + ", postal_code=" + postal_code + ", receiver="
				+ receiver + ", receiver_mobile=" + receiver_mobile
				+ ", idcard=" + idcard + ", user_name=" + user_name
				+ ", actual_weight=" + actual_weight + ", send_mobile="
				+ send_mobile + ", send_address=" + send_address
				+ ", goods_names=" + goods_names + ", quantity=" + quantity
				+ ", unit=" + unit + ", brand=" + brand + ", price=" + price
				+ ", spec=" + spec + ", goods_detail=" + goods_detail
				+ ", region=" + region + ", keep_price=" + keep_price + ", name_specs=" + name_specs
				+ ", business_name=" + business_name + ", express_package="
				+ express_package + ", declaration_code=" + declaration_code
				+ "]";
	}

    
    
}
