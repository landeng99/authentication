package com.xiangrui.lmp.business.admin.export.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.export.vo.PkgExport;

public interface ExportMapper
{

    /**
     * 根据提单导出包裹
     * @return
     */
    List<PkgExport>pkgExportByIdPick(int pick_id);
    
    /**
     * 根据托盘导出包裹
     * @return
     */
    List<PkgExport>pkgExportByIdPallet(int pallet_id);
}
