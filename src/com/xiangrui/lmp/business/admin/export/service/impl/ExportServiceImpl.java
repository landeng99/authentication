package com.xiangrui.lmp.business.admin.export.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.export.mapper.ExportMapper;
import com.xiangrui.lmp.business.admin.export.service.ExportService;
import com.xiangrui.lmp.business.admin.export.vo.PkgExport;
import com.xiangrui.lmp.util.JSONUtil;

@Service("exportService")
public class ExportServiceImpl implements ExportService
{
    private static final Integer PROVINCE_LEVEL=1;
    
    private static final Integer CITY_LEVEL=2;
    
    private static final Integer AREA_LEVEL=3;
 
    
    @Autowired
    private ExportMapper exportMapper;
    
    @Override
    public List<PkgExport> pkgExportByPallet(int pallet_id)
    {
        return exportMapper.pkgExportByIdPallet(pallet_id);
    }

    @Override
    public List<PkgExport> pkgExportByPick(int pick_id)
    {
        List<PkgExport> list=exportMapper.pkgExportByIdPick(pick_id);
        for(PkgExport export:list){
            String region=export.getRegion();
            //导出Excel渠道,此处是新增字段
            String express_package = export.getExpress_package();
            switch (express_package)
			{
				case "1" :
					export.setExpress_package("A");
					break;
				case "2" :
					export.setExpress_package("B");
					break;
				case "3" :
					export.setExpress_package("C");
					break;
				case "4" :
					export.setExpress_package("D");
					break;
				case "5" :
					export.setExpress_package("E");
					break;
				default :
					export.setExpress_package("F");
					break;
			}
           // export.get
            List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

            //字符转为对象
            try
            { 
                items = JSONUtil.jsonToBean(region, items.getClass());
                
                for(Map<String,Object> item:items){
                    
                    
                    Object o=item.get("level");
                    
                    Integer level=0;
                    if(Integer.class.isAssignableFrom(o.getClass())){
                        level=(Integer)o;
                    }else if(String.class.isAssignableFrom(o.getClass())){
                        level=Integer.parseInt((String)o);
                    }
                   
                    if(PROVINCE_LEVEL.equals(level)){
                        export.setProvince((String)item.get("name"));
                    }
                    if(CITY_LEVEL.equals(level)){
                        export.setCity((String)item.get("name"));
                    }
                    //如果是县级别则拼接到详情地址
                    if(AREA_LEVEL.equals(level)){
                        String street=export.getStreet();
                        street=(String)item.get("name")+street;
                        export.setStreet(street);
                    }
                    
                    //export.setOriginal_num(export.getLogistics_code());
              /*     if(export.getEms_code()!=null&&!"".equals(export.getEms_code())){
                       export.setOriginal_num(export.getEms_code());
                   }else{
                      
                   }*/
                }
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        return list;
    }
}
