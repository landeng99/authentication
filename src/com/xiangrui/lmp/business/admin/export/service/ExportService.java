package com.xiangrui.lmp.business.admin.export.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.export.vo.PkgExport;

public interface ExportService
{

    List<PkgExport> pkgExportByPick(int pick_id);
    
    List<PkgExport> pkgExportByPallet(int pallet_id);
}
