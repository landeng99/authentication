package com.xiangrui.lmp.business.admin.warningEmailLog.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.warningEmailLog.vo.WarningEmailLog;

public interface WarningEmailLogMapper
{
	List<WarningEmailLog> queryWarningEmailLog(Map<String, Object> paremt);

	WarningEmailLog queryWarningEmailLogBySeqId(int seq_id);

	void updateWarningEmailLogResentCountBySeqId(Map<String, Object> paremt);

	void insertWarningEmailLog(WarningEmailLog warningEmailLog);
}
