package com.xiangrui.lmp.business.admin.warningEmailLog.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.warningEmailLog.mapper.WarningEmailLogMapper;
import com.xiangrui.lmp.business.admin.warningEmailLog.service.WarningEmailLogService;
import com.xiangrui.lmp.business.admin.warningEmailLog.vo.WarningEmailLog;

@Service("warningEmailLogService")
public class WarningEmailLogImpl implements WarningEmailLogService
{
	@Autowired
	WarningEmailLogMapper warningEmailLogMapper;

	public List<WarningEmailLog> queryWarningEmailLog(Map<String, Object> paremt)
	{
		return warningEmailLogMapper.queryWarningEmailLog(paremt);
	}

	public WarningEmailLog queryWarningEmailLogBySeqId(int seq_id)
	{
		return warningEmailLogMapper.queryWarningEmailLogBySeqId(seq_id);
	}

	public void updateWarningEmailLogResentCountBySeqId(Map<String, Object> paremt)
	{
		warningEmailLogMapper.updateWarningEmailLogResentCountBySeqId(paremt);
	}

	public void insertWarningEmailLog(WarningEmailLog warningEmailLog)
	{
		warningEmailLogMapper.insertWarningEmailLog(warningEmailLog);
	}
}
