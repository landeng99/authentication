package com.xiangrui.lmp.business.admin.warningEmailLog.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.message.service.MessageSendService;
import com.xiangrui.lmp.business.admin.warningEmailLog.service.WarningEmailLogService;
import com.xiangrui.lmp.business.admin.warningEmailLog.vo.WarningEmailLog;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/warningEmailLog")
public class WarningEmailLogController
{
	@Autowired
	private WarningEmailLogService warningEmailLogService;
	@Autowired
	private MessageSendService messageSendService;
	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

	/**
	 * 邮件记录列表初始化
	 * 
	 * @return
	 */
	@RequestMapping("/queryAllOne")
	public String queryAll(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<WarningEmailLog> warningEmailLogList = warningEmailLogService.queryWarningEmailLog(params);
		
		request.setAttribute("warningEmailLogList", warningEmailLogList);

		return "back/warningEmailLogList";
	}
	
	/**
	 * 邮件记录列表搜索
	 * 
	 * @return
	 */
	@RequestMapping("/search")
	public String search(HttpServletRequest request,String fromDate, String toDate, String receriver_email,String receriver_account,String receriver_user_name,String status)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);

		}
		else
		{
			pageView = new PageView(1);

			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			params.put("receriver_email", receriver_email);
			params.put("receriver_account", receriver_account);
			params.put("receriver_user_name", receriver_user_name);
			params.put("status", status);
		}
		request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<WarningEmailLog> warningEmailLogList = warningEmailLogService.queryWarningEmailLog(params);
		
		request.setAttribute("warningEmailLogList", warningEmailLogList);
		request.setAttribute("params", params);
		return "back/warningEmailLogList";
	}
	
	/**
	 * 重发邮件
	 * @param request
	 * @param seq_id
	 * @return
	 */
	@RequestMapping("/reSendEmail")
	@ResponseBody
	public Map<String, Object> reSendEmail(HttpServletRequest request, int seq_id)
	{
		String flag="N";
		WarningEmailLog warningEmailLog=warningEmailLogService.queryWarningEmailLogBySeqId(seq_id);
		if(messageSendService.resendPayWarnEmail(warningEmailLog))
		{
			flag="Y";
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("flag",flag);
		return result;
	}
}
