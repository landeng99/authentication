package com.xiangrui.lmp.business.admin.quotationManage.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

/**
 * 报价单管理
 * <p>
 * 
 * @author <b>宋长旺</b>
 *         </p>
 *         <p>
 *         2016-10-8 下午2:32:45
 *         </p>
 */
public interface QuotationManageMapper {

	/**
	 * 查询所有虚拟账号
	 * 
	 * @param params
	 * @return
	 */
	List<QuotationManage> queryAll(Map<String, Object> params);

	/**
	 * 通过id 查找
	 * 
	 * @param log_id
	 * @return
	 */
	QuotationManage selectQuotationManageByLogId(int log_id);

	/**
	 * 通过logIDList来查找
	 * 
	 * @param logIdList
	 * @return
	 */
	List<QuotationManage> selectQuotationManageByLogIdList(List<String> logIdList);

	/**
	 * 通过id查询
	 * 
	 * @param order_id
	 * @return
	 */
	QuotationManage selectQuotationManageById(String id);

	/**
	 * 更新状态
	 * 
	 * @param QuotationManage
	 */
	void updateStatus(QuotationManage QuotationManage);

	/**
	 * 查询单个个用户的虚拟账号的记录,带对象的各种条件 其实仅仅是一个用户id,开始时间,结束时间
	 * 
	 * @param params
	 * @return
	 */
	List<QuotationManage> queryQuotationManageForAccountBalance(Map<String, Object> params);

	/**
	 * 通过公司单号查询
	 * 
	 * @param logistics_code
	 * @return
	 */
	List<QuotationManage> selectQuotationManageByLogistics_code(QuotationManage QuotationManage);

	void add(QuotationManage qm);

	void update(QuotationManage qm);

	void delete(int quoto_id);

	void delete(QuotationManage qm);

	String checkaddUser(String quota_name);

	List<String> checkUser(Map<String, Object> params);

	void insertAuthQuota(Map<String, Object> params);

	List<FrontUser> queryFrontUserByQuotaId(int quota_id);

	void deleteAuthAccountByQuotaId(int quota_id);

	List<FrontUser> queryFrontUserByAuthQuotaId(int quota_id);

	List<QuotationManage> queryAllUpdate(Map<String, Object> params1);

	void updateAuthQuota(Map<String, Object> params);

	List<QuotationManage> search(Map<String, Object> params);

	List<QuotationManage> getQuotaByOriginum(String originalNum);

	List<QuotationManage> getQuotaByOriginumOrLogistics(Map<String, Object> params);

	List<QuotationManage> getQuotaByPkgInfo(Map<String, Object> map);

	List<QuotationManage> getQuotaByDefault();

	List<QuotationManage> getQuotaByPkgInfo2(Map<String, Object> map);

}
