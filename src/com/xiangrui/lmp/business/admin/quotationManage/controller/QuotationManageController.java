package com.xiangrui.lmp.business.admin.quotationManage.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.quotationManage.service.QuotationManageService;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.init.SystemParamUtil;
import com.xiangrui.lmp.util.BreadcrumbUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 报价单管理
 */
@Controller
@RequestMapping("/admin/quotationManage")
public class QuotationManageController {

	@Autowired
	private QuotationManageService quotationManageService;

	@Autowired
	private FrontUserMapper frontUserMapper;

	/**
	 * 海外仓库
	 */
	@Autowired
	private OverseasAddressService overseasAddressService;
	/**
	 * 海外仓库
	 */
	@Autowired
	private PackageService packageService;

	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

	/**
	 * 获取报价单列表
	 * 
	 * @param request
	 * @param quotationManage
	 * @return
	 */
	@RequestMapping("queryAllOne")
	public String queryAllOne(HttpServletRequest request) {
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		Map<String, Object> params = new HashMap<String, Object>();
		if (user != null) {
			params.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}

		params.put("pageView", pageView);

		List<QuotationManage> quotationManageList = quotationManageService.queryAll(params);
		request.setAttribute("pageView", pageView);
		request.setAttribute("quotationManageList", quotationManageList);
		return "back/quotationManageList";
	}

	/**
	 * 查询报价单列表
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("search")
	public String search(HttpServletRequest request, String osaddr_id, String isHaveTax, String fromDate, String toDate,
			String account, Integer real_name, String express_package, String quota_name, String account_type,
			String user_type) {
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			pageView = new PageView(1);
			params.put("osaddr_id", osaddr_id);
			params.put("isHaveTax", isHaveTax);
			params.put("fromDate", fromDate);
			params.put("toDate", toDate);
			params.put("account", account);
			params.put("real_name", real_name);
			params.put("express_package", express_package);
			params.put("quota_name", quota_name);
			params.put("account_type", account_type);
			params.put("user_type", user_type);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}
		params.put("pageView", pageView);

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			params.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}
		// Map<String, Object> params2 = new HashMap<String, Object>();
		// params2.put("account", account);
		// List<FrontUser> frontUsers =
		// frontUserMapper.queryFrontUserByAccount(params2);
		// if (frontUsers!=null&&frontUsers.size()>0) {
		// FrontUser frontUser = frontUsers.get(0);
		// if (frontUser!=null) {
		// List<FrontUser> fUsers =
		// frontUserMapper.queryAuthQuota(frontUser.getUser_id());
		// if (fUsers!=null&&fUsers.size()==0) {
		//
		// }
		// }
		// }
		List<QuotationManage> quotationManageList = quotationManageService.search(params);
		request.setAttribute("pageView", pageView);
		// 页面显示用
		request.setAttribute("params", params);
		request.setAttribute("quotationManageList", quotationManageList);
		return "back/quotationManageList";
	}

	/**
	 * 新增时校验报价单名称是否存在
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping("/checkQM")
	@ResponseBody
	public Map<String, Object> checkQM(QuotationManage qm) {
		String uname = quotationManageService.checkaddUser(qm.getQuota_name());
		Map<String, Object> map = new HashMap<String, Object>();
		if (null == uname || "".equals(uname)) {
			map.put("result", true);
		} else {
			map.put("result", false);
		}
		return map;
	}

	/**
	 * 修改时验证报价单名称是否已经存在
	 * 
	 * @return
	 */
	@RequestMapping("/checkUpdateQM")
	@ResponseBody
	public Map<String, Object> checkUpdateQM(QuotationManage qm) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("quotaId", qm.getQuota_id());
		List<String> unamelist = quotationManageService.checkUser(params);
		Map<String, Object> map = new HashMap<String, Object>();

		String username = qm.getQuota_name();
		if (unamelist.contains(username)) {
			map.put("result", false);
		} else {
			map.put("result", true);
		}

		return map;
	}

	/**
	 * 进入新增页面
	 * 
	 * @param request
	 * @param coupon
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/toAdd")
	public String initAdd(HttpServletRequest req, HttpServletResponse resp) {
		Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = (Map<Integer, List<RoleOverseasAddress>>) SystemParamUtil
				.getAuthority("userOverseasAddressMap");
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("back_user");
		// 获取用户仓库权限
		List<RoleOverseasAddress> userOverseasAddressList = userOverseasAddressMap.get(user.getUser_id());
		req.setAttribute("overseasAddressList", userOverseasAddressList);

		BreadcrumbUtil.push(req);

		return "back/quotationManageAdd";
	}

	/**
	 * 首先判断，报价单里面是否已经同时添加了：同一仓库、同一用户类型、同一渠道、同一报价单类型，如果存在，则不让添加
	 * 
	 * @param req
	 * @param qm
	 * @return
	 */
//	@RequestMapping("/checkIsHave")
//	@ResponseBody
//	public Map<String, Object> checkIsHave(HttpServletRequest req, QuotationManage qm) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		Integer osaddr_id = Integer.valueOf(req.getParameter("osaddr_id"));
//		Integer user_type = Integer.valueOf(req.getParameter("user_type"));
//		Integer express_package = Integer.valueOf(req.getParameter("express_package"));
//		Integer account_type = Integer.valueOf(req.getParameter("account_type"));
//		Map<String, Object> params1 = new HashMap<String, Object>();
//		List<QuotationManage> quotationManageList = quotationManageService.queryAll(params1);
//		if (quotationManageList != null && quotationManageList.size() > 0) {
//			for (QuotationManage quotationManage : quotationManageList) {
//				if (quotationManage.getOsaddr_id() == osaddr_id && quotationManage.getUser_type() == user_type
//						&& quotationManage.getExpress_package() == express_package
//						&& quotationManage.getAccount_type() == account_type) {
//					map.put("result", true);
//					break;
//				} else {
//					map.put("result", false);
//				}
//			}
//		} else {
//			map.put("result", false);
//		}
//		return map;
//	}

	/**
	 * 新建报价单
	 * 
	 * @param req
	 * @param resp
	 * @param qm
	 * @param authAccount
	 * @return
	 */
	@RequestMapping("/add")
	public String add(HttpServletRequest req, HttpServletResponse resp, QuotationManage qm, String authAccount) {
		try {
			User user = (User) req.getSession().getAttribute("back_user");
			if (user != null && qm != null) {
				qm.setOpr_time(new Timestamp(System.currentTimeMillis()));
				quotationManageService.add(qm);
				List<OverseasAddress> overseasAddressList = overseasAddressService
						.queryOverseasAddressByUserId(user.getUser_id());
				req.setAttribute("overseasAddressList", overseasAddressList);
			}

			// 这个是新建的特殊报价单
			if (authAccount != null && !authAccount.equals("")) {
				String[] authAccountArray = authAccount.split(",");
				if (authAccountArray != null && authAccountArray.length > 0) {
					for (int i = 0; i < authAccountArray.length; i++) {
						Map<String, Object> fparams = new HashMap<String, Object>();
						fparams.put("account", authAccountArray[i]);
						List<FrontUser> frontUserList = frontUserMapper.queryFrontUserByAccount(fparams);
						if (frontUserList != null && frontUserList.size() > 0) {

							fparams.clear();
							fparams.put("user_id", frontUserList.get(0).getUser_id());
							fparams.put("osaddr_id", qm.getOsaddr_id());
							fparams.put("express_package", qm.getExpress_package());
							fparams.put("account_type", qm.getAccount_type());
							frontUserMapper.updateAuthQuotaByUserId2(fparams);//修改操作时，如果有特殊报价单，将默认报价单隐藏
							
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("create_time", new Date());
							params.put("quota_id", qm.getQuota_id());
							params.put("user_id", frontUserList.get(0).getUser_id());
							params.put("user_type", qm.getUser_type());
							params.put("osaddr_id", qm.getOsaddr_id());
							params.put("express_package", qm.getExpress_package());
							params.put("account_type", qm.getAccount_type());
							params.put("is_able", 2);// 默认报价单是否可用：2不可用（有特殊报价单时）
							quotationManageService.insertAuthQuota(params);
						}
					}
				}
			} else {
				// 新建的是默认报价单
				Map<String, Object> fparams = new HashMap<String, Object>();
				fparams.put("user_type", qm.getUser_type());
				List<FrontUser> frontUserList = frontUserMapper.queryFrontUserByUserType(fparams);
				if (frontUserList != null && frontUserList.size() > 0) {
					for (FrontUser frontUser : frontUserList) {
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("create_time", new Date());
						params.put("quota_id", qm.getQuota_id());
						params.put("user_id", frontUser.getUser_id());
						params.put("user_type", qm.getUser_type());
						params.put("osaddr_id", qm.getOsaddr_id());
						params.put("express_package", qm.getExpress_package());
						params.put("account_type", qm.getAccount_type());
						params.put("is_able", 1);// 默认报价单是否可用：1可用（无特殊报价单时）
						quotationManageService.insertAuthQuota(params);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "back/quotationManageList";
	}

	@RequestMapping("/checkUpdateIsHave")
	@ResponseBody
	public Map<String, Object> checUpdatekIsHave(HttpServletRequest req, QuotationManage qm) {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer quota_id = Integer.valueOf(req.getParameter("quota_id"));
		Integer osaddr_id = Integer.valueOf(req.getParameter("osaddr_id"));
		Integer user_type = Integer.valueOf(req.getParameter("user_type"));
		Integer express_package = Integer.valueOf(req.getParameter("express_package"));
		Integer account_type = Integer.valueOf(req.getParameter("account_type"));
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("quota_id", quota_id);
		params.put("osaddr_id", osaddr_id);
		params.put("user_type", user_type);
		params.put("express_package", express_package);
		params.put("account_type", account_type);
		List<QuotationManage> quotationManageList = quotationManageService.queryAllUpdate(params);
		if (quotationManageList != null && quotationManageList.size() > 0) {
			for (QuotationManage quotationManage : quotationManageList) {
				if (quotationManage.getOsaddr_id() == osaddr_id && quotationManage.getUser_type() == user_type
						&& quotationManage.getExpress_package() == express_package
						&& quotationManage.getAccount_type() == account_type) {
					map.put("result", true);
					break;
				} else {
					map.put("result", false);
				}
			}
		} else {
			map.put("result", false);
		}
		return map;
	}

	/**
	 * 进入编辑页面
	 * 
	 * @param request
	 * @param coupon
	 * @return
	 */
	@RequestMapping("/toUpdate")
	public String toUpdate(HttpServletRequest request, QuotationManage qm) {
		qm = quotationManageService.selectQuotationManageByLogId(qm.getQuota_id());
		request.setAttribute("qm", qm);
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}
		String authAccount = "";
		List<FrontUser> frontUserList = quotationManageService.queryFrontUserByQuotaId(qm.getQuota_id());
		if (frontUserList != null && frontUserList.size() > 0) {
			for (FrontUser frontUser : frontUserList) {
				authAccount += frontUser.getAccount() + ",";
			}
		}
		request.setAttribute("authAccount", authAccount);

		return "back/quotationManageUpdate";
	}

	/**
	 * 修改报价单
	 * 
	 * @param request
	 * @param qm 待修改的报价单
	 * @param authAccount 报价单适合的特殊用户
	 * @return
	 */
	@RequestMapping("/update")
	public String update(HttpServletRequest request, QuotationManage qm, String authAccount) {
		quotationManageService.update(qm);
		if (authAccount != null && !"".equals(authAccount)) {// 修改的是特殊报价单
			// 查询所有取消掉的适合该报价单的同行用户
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("quota_id", qm.getQuota_id());
			map.put("osaddr_id", qm.getOsaddr_id());
			map.put("express_package", qm.getExpress_package());
			String[] authAccountArray = authAccount.split(",");
			if (authAccountArray != null && authAccountArray.length > 0) {
				String user_id = "";
				for (int i = 0; i < authAccountArray.length; i++) {
					Map<String, Object> fparams = new HashMap<String, Object>();
					fparams.put("account", authAccountArray[i]);
					List<FrontUser> frontUserList = frontUserMapper.queryFrontUserByAccount(fparams);
					if (frontUserList != null && frontUserList.size() > 0) {
						user_id += frontUserList.get(0).getUser_id();
						user_id += ",";
					}
				}
				user_id = user_id.substring(0,user_id.length()-1); 
				map.put("user_id", user_id);
				List<FrontUser> users = frontUserMapper.selectAuthAccountByInfo(map);
				if (users != null && users.size() > 0) {
					for (FrontUser frontUser : users) {
						map.clear();
						map.put("user_id", frontUser.getUser_id());
						map.put("osaddr_id", qm.getOsaddr_id());
						map.put("express_package", qm.getExpress_package());
						frontUserMapper.updateAuthQuotaByUserId1(map);//后台做修改操作时，如果没有特殊报价单，则将默认报价单显示
					}
				}

				quotationManageService.deleteAuthAccountByQuotaId(qm.getQuota_id());

				for (int i = 0; i < authAccountArray.length; i++) {
					Map<String, Object> fparams = new HashMap<String, Object>();
					fparams.put("account", authAccountArray[i]);
					List<FrontUser> frontUserList = frontUserMapper.queryFrontUserByAccount(fparams);
					if (frontUserList != null && frontUserList.size() > 0) {

						fparams.clear();
						fparams.put("user_id", frontUserList.get(0).getUser_id());
						fparams.put("osaddr_id", qm.getOsaddr_id());
						fparams.put("express_package", qm.getExpress_package());
						fparams.put("account_type", qm.getAccount_type());
						frontUserMapper.updateAuthQuotaByUserId2(fparams);//修改操作时，如果有特殊报价单，将默认报价单隐藏

						// 对于新适合的同行用户，数据库添加对应用户的关联表记录

						Map<String, Object> params = new HashMap<String, Object>();
						params.put("create_time", new Date());
						params.put("quota_id", qm.getQuota_id());
						params.put("user_id", frontUserList.get(0).getUser_id());
						params.put("user_type", qm.getUser_type());
						params.put("osaddr_id", qm.getOsaddr_id());
						params.put("express_package", qm.getExpress_package());
						params.put("account_type", qm.getAccount_type());
						quotationManageService.insertAuthQuota(params);
					}
				}
			}
		}
		return queryAllOne(request);
	}

	/**
	 * 删除特殊报价单
	 * 
	 * @param coupon
	 * @return
	 */
	@RequestMapping("/deleteqm")
	public String deleteqm(HttpServletRequest request, QuotationManage qm) {
		qm = quotationManageService.selectQuotationManageByLogId(qm.getQuota_id());
		this.quotationManageService.delete(qm);

		// 查询出-适合该报价单的所有用户
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("quota_id", qm.getQuota_id());
		map.put("osaddr_id", qm.getOsaddr_id());
		map.put("express_package", qm.getExpress_package());
		List<FrontUser> users = frontUserMapper.selectAuthAccountByDelete(map);
		if (users != null && users.size() > 0) {
			for (FrontUser frontUser : users) {
				map.clear();
				map.put("user_id", frontUser.getUser_id());
				map.put("osaddr_id", qm.getOsaddr_id());
				map.put("express_package", qm.getExpress_package());
				frontUserMapper.updateAuthQuotaByUserId1(map);// 后台做修改操作时，如果没有特殊报价单，则将默认报价单显示
			}
		}
		
		quotationManageService.deleteAuthAccountByQuotaId(qm.getQuota_id());

		return queryAllOne(request);
	}

	/**
	 * 查看详情
	 * 
	 * @param coupon
	 * @return
	 */
	@RequestMapping("/view")
	public String view(HttpServletRequest request, QuotationManage qm) {
		qm = quotationManageService.selectQuotationManageByLogId(qm.getQuota_id());
		request.setAttribute("qm", qm);
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null) {
			List<OverseasAddress> overseasAddressList = overseasAddressService
					.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}

		String authAccount = "";
		List<FrontUser> frontUserList = quotationManageService.queryFrontUserByAuthQuotaId(qm.getQuota_id());
		if (frontUserList != null && frontUserList.size() > 0) {
			for (FrontUser frontUser : frontUserList) {
				authAccount += frontUser.getAccount() + ",";
			}
		}
		request.setAttribute("authAccount", authAccount);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		return "back/quotationManageView";
	}

}
