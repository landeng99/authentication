package com.xiangrui.lmp.business.admin.quotationManage.vo;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;

/**
 * 报价单表
 * <p>
 * 
 * @author <b>宋长旺</b> 2016-8-30 下午5:37:51
 */
public class QuotationManage {
	/**
	 * 主键ID
	 */
	private int quota_id;

	/**
	 * 创建时间
	 */
	private Timestamp opr_time;

	/**
	 * 是否删除：0未删除；1删除
	 */
	private boolean isDelete;

	/**
	 * 报价单名称
	 */
	private String quota_name;

	/**
	 * 海外仓库id
	 */
	private int osaddr_id;

	/**
	 * 海外仓库名
	 */
	private String warehouse;

	/**
	 * 渠道：0默认；1A渠道；2B渠道；3C渠道；4D渠道；5E渠道
	 */
	private int express_package;

	/**
	 * 报价单类型：0默认；1特殊
	 */
	private int account_type;

	/**
	 * 用户类型，1：普通用户，2：同行用户
	 */
	private int user_type;

	/**
	 * 属性（是否含税）：0不含税；1含税
	 */
	private int isHaveTax;

	/**
	 * 账户注册时的姓名（用户名）
	 */
	private String real_name;

	/**
	 * 用户账号
	 */
	private String account;

	/**
	 * 时间格式化
	 */
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * 用户ID
	 */
	private int user_id;

	/**
	 * 备注-描述信息
	 */
	private String description;

	/**
	 * 计量单位：克，千克，磅
	 */
	private String unit;

	/**
	 * 首重
	 */
	private BigDecimal first_weight;

	/**
	 * 首重单价（json串）
	 */
	private String first_weight_price;

	/**
	 * 进位
	 */
	private BigDecimal carry;

	// /**
	// * 单价
	// */
	// private BigDecimal price;

	/**
	 * 是否去除尾数设置：0没有勾选，不设置；1勾选了，设置
	 */
	private boolean isRemoveTail;

	/**
	 * 续重单价（json串）
	 */
	private String go_weight_price;

	/**
	 * 会员等级id
	 */
	private int rate_id;

	/**
	 * 增值服务id
	 */
	private int attach_id;

	@Override
	public String toString() {
		return "id：" + quota_id + "-报价单名称:" + quota_name + "-报价单类型:" + account_type;
	}

	public int getQuota_id() {
		return quota_id;
	}

	public void setQuota_id(int quota_id) {
		this.quota_id = quota_id;
	}

	public Timestamp getOpr_time() {
		return opr_time;
	}

	public void setOpr_time(Timestamp opr_time) {
		this.opr_time = opr_time;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getQuota_name() {
		return quota_name;
	}

	public void setQuota_name(String quota_name) {
		this.quota_name = quota_name;
	}

	public int getOsaddr_id() {
		return osaddr_id;
	}

	public void setOsaddr_id(int osaddr_id) {
		this.osaddr_id = osaddr_id;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public int getExpress_package() {
		return express_package;
	}

	public void setExpress_package(int express_package) {
		this.express_package = express_package;
	}

	public int getAccount_type() {
		return account_type;
	}

	public void setAccount_type(int account_type) {
		this.account_type = account_type;
	}

	public int getUser_type() {
		return user_type;
	}

	public void setUser_type(int user_type) {
		this.user_type = user_type;
	}

	public int getIsHaveTax() {
		return isHaveTax;
	}

	public void setIsHaveTax(int isHaveTax) {
		this.isHaveTax = isHaveTax;
	}

	public String getReal_name() {
		return real_name;
	}

	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BigDecimal getFirst_weight() {
		return first_weight;
	}

	public void setFirst_weight(BigDecimal first_weight) {
		this.first_weight = first_weight;
	}

	public BigDecimal getCarry() {
		return carry;
	}

	public void setCarry(BigDecimal carry) {
		this.carry = carry;
	}

	// public BigDecimal getPrice() {
	// return price;
	// }
	//
	// public void setPrice(BigDecimal price) {
	// this.price = price;
	// }

	public boolean isRemoveTail() {
		return isRemoveTail;
	}

	public void setRemoveTail(boolean isRemoveTail) {
		this.isRemoveTail = isRemoveTail;
	}

	public String getFirst_weight_price() {
		return first_weight_price;
	}

	public void setFirst_weight_price(String first_weight_price) {
		this.first_weight_price = first_weight_price;
	}

	public String getGo_weight_price() {
		return go_weight_price;
	}

	public void setGo_weight_price(String go_weight_price) {
		this.go_weight_price = go_weight_price;
	}

	public int getRate_id() {
		return rate_id;
	}

	public void setRate_id(int rate_id) {
		this.rate_id = rate_id;
	}

	public int getAttach_id() {
		return attach_id;
	}

	public void setAttach_id(int attach_id) {
		this.attach_id = attach_id;
	}

}
