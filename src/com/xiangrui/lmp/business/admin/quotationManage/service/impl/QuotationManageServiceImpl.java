package com.xiangrui.lmp.business.admin.quotationManage.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.quotationManage.mapper.QuotationManageMapper;
import com.xiangrui.lmp.business.admin.quotationManage.service.QuotationManageService;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

/**
 * 虚拟金额
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:33:25
 *         </p>
 */
@Service("QuotationManageService")
public class QuotationManageServiceImpl implements QuotationManageService {

	/**
	 * 虚拟金额
	 */
	@Autowired
	private QuotationManageMapper QuotationManageMapper;

	/**
	 * 查询所有
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<QuotationManage> queryAll(Map<String, Object> params) {
		return QuotationManageMapper.queryAll(params);
	}

	/**
	 * 通过ID查找
	 * 
	 * @param log_id
	 * @return
	 */
	@Override
	public QuotationManage selectQuotationManageByLogId(int log_id) {
		return QuotationManageMapper.selectQuotationManageByLogId(log_id);
	}

	/**
	 * 通过logIDList来查找
	 * 
	 * @param logIdList
	 * @return
	 */
	public List<QuotationManage> selectQuotationManageByLogIdList(List<String> logIdList) {
		return QuotationManageMapper.selectQuotationManageByLogIdList(logIdList);
	}

	/**
	 * 通过订单号查找
	 * 
	 * @param order_id
	 * @return
	 */
	@Override
	public QuotationManage selectQuotationManageById(String id) {

		return QuotationManageMapper.selectQuotationManageById(id);
	}

	/**
	 * 查询单个个用户的虚拟账号的记录,带对象的各种条件 其实仅仅是一个用户id,开始时间,结束时间
	 *
	 * @param params
	 * @return
	 */
	@Override
	public List<QuotationManage> queryQuotationManageForAccountBalance(Map<String, Object> params) {

		return QuotationManageMapper.queryQuotationManageForAccountBalance(params);
	}

	/**
	 * 通过公司单号查询
	 * 
	 * @param logistics_code
	 * @return
	 */
	public QuotationManage selectQuotationManageByLogistics_code(String logistics_code) {
		QuotationManage reQuotationManage = null;
		QuotationManage queryQuotationManage = new QuotationManage();
		// queryQuotationManage.setLogistics_code(logistics_code);
		List<QuotationManage> QuotationManageList = QuotationManageMapper
				.selectQuotationManageByLogistics_code(queryQuotationManage);
		if (QuotationManageList != null && QuotationManageList.size() > 0) {
			reQuotationManage = QuotationManageList.get(0);
		}
		return reQuotationManage;
	}

	@Override
	public void add(QuotationManage qm) {
		QuotationManageMapper.add(qm);
	}

	@Override
	public void update(QuotationManage qm) {
		QuotationManageMapper.update(qm);

	}

	@Override
	public void delete(QuotationManage qm) {
		QuotationManageMapper.delete(qm);

	}

	@Override
	public String checkaddUser(String quota_name) {
		return QuotationManageMapper.checkaddUser(quota_name);
	}

	@Override
	public List<String> checkUser(Map<String, Object> params) {
		return QuotationManageMapper.checkUser(params);
	}

	@Override
	public void insertAuthQuota(Map<String, Object> params) {
		QuotationManageMapper.insertAuthQuota(params);

	}

	@Override
	public List<FrontUser> queryFrontUserByQuotaId(int quota_id) {
		return QuotationManageMapper.queryFrontUserByQuotaId(quota_id);
	}

	@Override
	public void deleteAuthAccountByQuotaId(int quota_id) {
		QuotationManageMapper.deleteAuthAccountByQuotaId(quota_id);
	}

	@Override
	public List<FrontUser> queryFrontUserByAuthQuotaId(int quota_id) {
		return QuotationManageMapper.queryFrontUserByAuthQuotaId(quota_id);
	}

	@Override
	public List<QuotationManage> queryAllUpdate(Map<String, Object> params1) {
		return QuotationManageMapper.queryAllUpdate(params1);
	}

	@Override
	public void updateAuthQuota(Map<String, Object> params) {
		QuotationManageMapper.updateAuthQuota(params);

	}

	/**
	 * 查询搜索
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<QuotationManage> search(Map<String, Object> params) {
		return QuotationManageMapper.search(params);
	}

	/**
	 * 根据公司运单号或关联单号查询报价单
	 */
	@Override
	public List<QuotationManage> getQuotaByOriginumOrLogistics(Map<String, Object> params) {
		return QuotationManageMapper.getQuotaByOriginumOrLogistics(params);
	}

	@Override
	public QuotationManage getQuotaByOriginum(String originalNum) {
		QuotationManage reQuotationManage = null;
		List<QuotationManage> QuotationManageList = QuotationManageMapper.getQuotaByOriginum(originalNum);
		if (QuotationManageList != null && QuotationManageList.size() > 0) {
			reQuotationManage = QuotationManageList.get(0);
		}
		return reQuotationManage;
	}

	@Override
	public List<QuotationManage> getQuotaByDefault() {
		return QuotationManageMapper.getQuotaByDefault();
	}

	@Override
	public QuotationManage getQuotaByPkgInfo(int osaddr_id, int express_package, int user_type, int user_id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("osaddr_id", osaddr_id);
		map.put("express_package", express_package);
		map.put("user_type", user_type);
		map.put("user_id", user_id);
		List<QuotationManage> QuotationManageList = QuotationManageMapper.getQuotaByPkgInfo(map);
		QuotationManage reQuotationManage = null;
		if (QuotationManageList != null && QuotationManageList.size() > 0) {
			reQuotationManage = QuotationManageList.get(0);
		}
		return reQuotationManage;
	}
	@Override
	public QuotationManage getQuotaByPkgInfo2(int osaddr_id, int express_package, int user_type, int user_id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("osaddr_id", osaddr_id);
		map.put("express_package", express_package);
		map.put("user_type", user_type);
		map.put("user_id", user_id);
		List<QuotationManage> QuotationManageList = QuotationManageMapper.getQuotaByPkgInfo2(map);
		QuotationManage reQuotationManage = null;
		if (QuotationManageList != null && QuotationManageList.size() > 0) {
			reQuotationManage = QuotationManageList.get(0);
		}
		return reQuotationManage;
	}

}
