package com.xiangrui.lmp.business.admin.quotationManage.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

/**
 * 虚拟账号操作接口
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:37:10
 *         </p>
 */
public interface QuotationManageService {

	/**
	 * 查询所有用户虚拟找好信息
	 * 
	 * @param params
	 * @return
	 */
	List<QuotationManage> queryAll(Map<String, Object> params);

	/**
	 * 通过ID查找
	 * 
	 * @param log_id
	 */
	QuotationManage selectQuotationManageByLogId(int log_id);

	/**
	 * 通过logIDList来查找
	 * 
	 * @param logIdList
	 * @return
	 */
	List<QuotationManage> selectQuotationManageByLogIdList(List<String> logIdList);

	/**
	 * 通过id查询
	 * 
	 * @param order_id
	 * @return
	 */
	QuotationManage selectQuotationManageById(String id);

	/**
	 * 查询单个个用户的虚拟账号的记录,带对象的各种条件 其实仅仅是一个用户id,开始时间,结束时间
	 *
	 * @param params
	 * @return
	 */
	List<QuotationManage> queryQuotationManageForAccountBalance(Map<String, Object> params);

	/**
	 * 通过公司单号查询
	 * 
	 * @param logistics_code
	 * @return
	 */
	QuotationManage selectQuotationManageByLogistics_code(String logistics_code);

	void add(QuotationManage qm);

	void update(QuotationManage qm);

	void delete(QuotationManage qm);

	String checkaddUser(String quota_name);

	List<String> checkUser(Map<String, Object> params);

	void insertAuthQuota(Map<String, Object> params);

	List<FrontUser> queryFrontUserByQuotaId(int quota_id);

	void deleteAuthAccountByQuotaId(int quota_id);

	List<FrontUser> queryFrontUserByAuthQuotaId(int quota_id);

	List<QuotationManage> queryAllUpdate(Map<String, Object> params1);

	void updateAuthQuota(Map<String, Object> params);

	List<QuotationManage> search(Map<String, Object> params);

	QuotationManage getQuotaByOriginum(String originalNum);

	List<QuotationManage> getQuotaByOriginumOrLogistics(Map<String, Object> params);

	List<QuotationManage> getQuotaByDefault();

	QuotationManage getQuotaByPkgInfo(int osaddr_id, int express_package, int user_type, int user_id);

	QuotationManage getQuotaByPkgInfo2(int osaddr_id, int express_package, int user_type, int user_id);


}
