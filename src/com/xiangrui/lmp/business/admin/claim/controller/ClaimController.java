package com.xiangrui.lmp.business.admin.claim.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.accountlog.service.AccountLogService;
import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;
import com.xiangrui.lmp.business.admin.claim.service.ClaimService;
import com.xiangrui.lmp.business.admin.claim.vo.Claim;
import com.xiangrui.lmp.business.admin.claim.vo.PackageClaim;
import com.xiangrui.lmp.business.admin.pkg.service.PackageImgService;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgGoodsService;
import com.xiangrui.lmp.business.admin.pkg.vo.PackageImg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods;
import com.xiangrui.lmp.business.base.BaseClaim;
import com.xiangrui.lmp.util.PageView;

/**
 * 索赔界面
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:44:50
 *         </p>
 */
@Controller
@RequestMapping("/admin/claim")
public class ClaimController
{
    /**
     * 索赔操作
     */
    @Autowired
    private ClaimService claimService;

    /**
     * 匹配索赔的包裹中给物品
     */
    @Autowired
    private PkgGoodsService pkgGoodsService;

    /**
     * 图片处理
     */
    @Autowired
    private PackageImgService packageImgService;

    /**
     * 虚拟账号操作
     */
    @Autowired
    private AccountLogService accountLogService;

    /**
     * 首次查询
     * 
     * @param request
     * @return
     */
    @RequestMapping("/queryAllOne")
    public String indexFink(HttpServletRequest request)
    {
        return queryAll(request, null);
    }

    /**
     * 新的分页查询所有索赔
     * 
     * @param request
     * @param claim
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request, Claim claim)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);

        // 首次查询 当没有有request范围内的参数aAll的设置时,不是首次查询
        if (null != claim)
        {
            params.put("STATUS", claim.getStatus());
            params.put("USERNAME", claim.getUserName());
            params.put("LOGISTICS_CODE", claim.getLogistics_code());
            params.put("AMOUNTSTART", claim.getAmountStart());
            params.put("AMOUNTEND", claim.getAmountEnd());
            params.put("APPLYTIMESTART", claim.getApplyTimeStart());
            params.put("APPLYTIMEEND", claim.getApplyTimeEnd());
        }

        List<Claim> list = claimService.queryAll(params);
        request.setAttribute("pageView", pageView);
        request.setAttribute("claimLists", list);
        return "back/claimList";
    }

    /**
     * 进入查看详情
     * 
     * @return
     */
    @RequestMapping("/toFinkClaim")
    public String toFinkClaim(HttpServletRequest request, Claim claim)
    {

        // claimId匹配索赔信息
        claim = this.claimService.queryAllId(claim.getClaimId());

        // claim.packageId匹配包裹信息
        PackageClaim packageClaim = this.claimService.queryPackageClaim(claim
                .getPackageId());

        // 商品
        List<PkgGoods> pkgGoodsList = pkgGoodsService
                .selectPkgGoodsByPackageId(claim.getPackageId());

        // 匹配包裹图片信息
        // TODO 原来的方法一个包裹id可能对应多条记录
        PackageImg packageImg = new PackageImg();
        packageImg.setPackage_id(packageClaim.getPackage_id());

        // TODO 设置图片类型
        packageImg.setImg_type(BaseClaim.CLAIM_IMG_TYPE);
        packageImg = packageImgService.queryPackageImg(packageImg);

        request.setAttribute("claimPojo", claim);
        request.setAttribute("retypay_status", packageClaim.getRetpay_status());
        request.setAttribute("pkgGoodsList", pkgGoodsList);
        if (null == packageImg || null == packageImg.getImg_path())
            request.setAttribute("img_path", "无图片");
        else
            request.setAttribute("img_path", packageImg.getImg_path());

        request.setAttribute("CIF_TRUE", "gind");
        return "back/claimInfo";
    }

    /**
     * 审核等各种操作,进入详细页面
     * 
     * @return
     */
    @RequestMapping("/toFinkClaimOne")
    public String toFinkClaimOne(HttpServletRequest request, Claim claim)
    {

        // claimId匹配索赔信息
        claim = this.claimService.queryAllId(claim.getClaimId());

        // claim.packageId匹配包裹信息
        PackageClaim packageClaim = this.claimService.queryPackageClaim(claim
                .getPackageId());

        // 商品
        List<PkgGoods> pkgGoodsList = pkgGoodsService
                .selectPkgGoodsByPackageId(claim.getPackageId());

        // 匹配包裹图片信息
        // TODO 原来的方法一个包裹id可能对应多条记录
        PackageImg packageImg = new PackageImg();
        packageImg.setPackage_id(packageClaim.getPackage_id());

        // TODO 设置图片类型
        packageImg.setImg_type(BaseClaim.CLAIM_IMG_TYPE);
        packageImg = packageImgService.queryPackageImg(packageImg);

        request.setAttribute("claimPojo", claim);
        request.setAttribute("retypay_status", packageClaim.getRetpay_status());
        request.setAttribute("pkgGoodsList", pkgGoodsList);
        if (null == packageImg || null == packageImg.getImg_path())
            request.setAttribute("img_path", "无图片");
        else
            request.setAttribute("img_path", packageImg.getImg_path());

        request.setAttribute("CIF_TRUE", "bgin");
        return "back/claimInfo";
    }

    /**
     * 索赔 拒绝索赔
     * 
     * @param claim
     * @return
     */
    @RequestMapping("/updateClaimRefusing")
    public String updateClaimRefusing(HttpServletRequest request, Claim claim)
    {

        // 匹配数据库中的数据 审核等操作仅仅是状态变化
        claim = this.claimService.queryAllId(claim.getClaimId());

        // 拒绝索赔
        claim.setStatus(BaseClaim.CLAIM_REFUSING);
        this.claimService.updateClaim(claim);
        return indexFink(request);
    }

    /**
     * 索赔通过 同意索赔
     * 
     * @param claim
     * @return
     */
    @RequestMapping("/updateClaimThrough")
    public String updateClaimThrough(HttpServletRequest request, Claim claim)
    {// 1=3
     // 匹配数据库中的数据 审核等操作仅仅是状态变化
        claim = this.claimService.queryAllId(claim.getClaimId());
        // 审核通过
        claim.setStatus(BaseClaim.CLAIM_THROUGH);// 申请通过
        this.claimService.updateClaim(claim);
        return indexFink(request);
    }

    /**
     * 索赔退款中 财务审查
     * 
     * @param claim
     * @return
     */
    @RequestMapping("/updateClaimProcessing")
    public String updateClaimsProcessing(HttpServletRequest request, Claim claim)
    {// 3=4
     // 匹配数据库中的数据 审核等操作仅仅是状态变化
        claim = this.claimService.queryAllId(claim.getClaimId());
        // 审核退款中
        claim.setStatus(BaseClaim.CLAIM_PROCESSING);
        this.claimService.updateClaim(claim);
        return indexFink(request);
    }

    /**
     * 索赔完成付款操作 财务付款
     * 
     * @param claim
     * @return
     */
    @RequestMapping("/updateClaimComplete")
    public String updateClaimComplete(HttpServletRequest request, Claim claim)
    {
        //TODO  0725  黄业恒修改中  AccountLog bean 变更
      //TODO 0725  黄业恒修改中  AccountLog bean 变更
        //TODO 0725  黄业恒修改中  AccountLog bean 变更
        //TODO 0725  黄业恒修改中  AccountLog bean 变更
        //TODO 0725  黄业恒修改中  AccountLog bean 变更
            // 4=5
     // 匹配数据库中的数据 审核等操作仅仅是状态变化
//        claim = this.claimService.queryAllId(claim.getClaimId());
//        // 索赔通过的金额处理
//        AccountLog accountLog = new AccountLog();
//        // 索赔的金额按对应比例换算
//    //    accountLog.setAmount(claim.getAmount());
//        accountLog.setDescription(claim.getReason());
//        // TODO 索赔的单号生成方式?
//        accountLog.setOrderId("SP" + claim.getPackageId());
//        accountLog.setAccountType(BaseClaim.ACCOUNTLOG_CLAIM);
//        accountLog.setUserId(claim.getUserId());
//
//        Date date = new Date();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        accountLog.setOprTime(df.format(date));
//        // 索赔的金额记录信息
//        this.accountLogService.addAccountLog(accountLog);
//        // 审核通过
//        claim.setStatus(BaseClaim.CLAIM_COMPLETE);// 申请通过
//        this.claimService.updateClaim(claim);
        return indexFink(request);
    }

    /**
     * 删除索赔
     * 
     * @param claim
     * @return
     */
    @RequestMapping("/deleteClaim")
    public String deleteClaim(Claim claim)
    {
        this.claimService.deleteClaim(claim.getClaimId());
        // 应该存在给用户信息反馈
        return "back/claimList";
    }
}
