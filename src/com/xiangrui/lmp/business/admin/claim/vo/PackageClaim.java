package com.xiangrui.lmp.business.admin.claim.vo;

import org.springframework.beans.factory.annotation.Autowired;

import com.xiangrui.lmp.business.base.BasePackage;

/**
 * 提现关联的包裹信息实体
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:04:16
 * </p>
 */
public class PackageClaim extends BasePackage
{

    private static final long serialVersionUID = 1L;

    /**
     * 关联的用户对象
     */
    @Autowired
    private FrontUserClaim frontUserClaim;

    public FrontUserClaim getFrontUserClaim()
    {
        if (null == frontUserClaim)
            frontUserClaim = new FrontUserClaim();
        return frontUserClaim;
    }

    public void setFrontUserClaim(FrontUserClaim frontUserClaim)
    {
        this.frontUserClaim = frontUserClaim;
    }

    public PackageClaim()
    {
        super();
    }

    /**
     * 有参数的 包裹对象 的构造方法
     * 
     * @param package_id
     *            包裹id
     * @param original_num
     *            包裹编号
     * @param user_id
     *            用户id
     * @param frontUserClaim
     *            用户对象
     */
    public PackageClaim(int package_id, String original_num, int user_id,
            FrontUserClaim frontUserClaim)
    {
        super(package_id, original_num, user_id);
        this.frontUserClaim = frontUserClaim;
    }

}
