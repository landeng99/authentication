package com.xiangrui.lmp.business.admin.claim.vo;

import java.io.Serializable;

/**
 * 提现关联的用户信息实体
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:01:51
 * </p>
 */
public class FrontUserClaim implements Serializable
{

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private int user_id;

    /**
     * 用户密码
     */
    private String user_pwd;

    /**
     * 用户姓名
     */
    private String user_name;

    /**
	 * 
	 */
    private String mobile;

    /**
     * 用户邮箱
     */
    private String email;

    /**
	 * 
	 */
    private String sms_code;

    /**
	 * 
	 */
    private String sms_deadline;

    /**
	 * 
	 */
    private double balance;

    /**
	 * 
	 */
    private double able_balance;

    /**
	 * 
	 */
    private double frozen_balance;

    /**
	 * 
	 */
    private int integral;

    /**
	 * 
	 */
    private int member_rate;

    /**
	 * 
	 */
    private String register_time;

    /**
	 * 
	 */
    private int user_type;

    /**
	 * 
	 */
    private String first_name;

    /**
	 * 
	 */
    private String last_name;

    /**
	 * 
	 */
    private int status;

    /**
     * 用户账号
     */
    private String account;

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public String getUser_pwd()
    {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd)
    {
        this.user_pwd = user_pwd;
    }

    public String getUser_name()
    {
        return user_name;
    }

    public void setUser_name(String user_name)
    {
        this.user_name = user_name;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getSms_code()
    {
        return sms_code;
    }

    public void setSms_code(String sms_code)
    {
        this.sms_code = sms_code;
    }

    public String getSms_deadline()
    {
        return sms_deadline;
    }

    public void setSms_deadline(String sms_deadline)
    {
        this.sms_deadline = sms_deadline;
    }

    public double getBalance()
    {
        return balance;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }

    public double getAble_balance()
    {
        return able_balance;
    }

    public void setAble_balance(double able_balance)
    {
        this.able_balance = able_balance;
    }

    public double getFrozen_balance()
    {
        return frozen_balance;
    }

    public void setFrozen_balance(double frozen_balance)
    {
        this.frozen_balance = frozen_balance;
    }

    public int getIntegral()
    {
        return integral;
    }

    public void setIntegral(int integral)
    {
        this.integral = integral;
    }

    public int getMember_rate()
    {
        return member_rate;
    }

    public void setMember_rate(int member_rate)
    {
        this.member_rate = member_rate;
    }

    public String getRegister_time()
    {
        return register_time;
    }

    public void setRegister_time(String register_time)
    {
        this.register_time = register_time;
    }

    public int getUser_type()
    {
        return user_type;
    }

    public void setUser_type(int user_type)
    {
        this.user_type = user_type;
    }

    public String getFirst_name()
    {
        return first_name;
    }

    public void setFirst_name(String first_name)
    {
        this.first_name = first_name;
    }

    public String getLast_name()
    {
        return last_name;
    }

    public void setLast_name(String last_name)
    {
        this.last_name = last_name;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getAccount()
    {
        return account;
    }

    public void setAccount(String account)
    {
        this.account = account;
    }

}
