package com.xiangrui.lmp.business.admin.claim.vo;

/*import java.util.Calendar;
 import java.util.Date;
 import java.util.Timer;
 import java.util.TimerTask;*/

import org.springframework.beans.factory.annotation.Autowired;

import com.xiangrui.lmp.business.base.BaseClaim;

/**
 * 索赔实体
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:52:54
 *         </p>
 */
public class Claim extends BaseClaim
{

    private static final long serialVersionUID = 1L;

    /**
     * 查询用的金额参数最小值
     */
    private double amountStart;

    /**
     * 查询用的金额参数的最大值
     */
    private double amountEnd;

    /**
     * 查询用的开始时间
     */
    private String applyTimeStart;

    /**
     * 查询用的结束时间
     */
    private String applyTimeEnd;

    /**
     * 提现对应的包裹信息对象
     */
    @Autowired
    private PackageClaim packageClaim;

    public PackageClaim getPackageClaim()
    {
        if (null == packageClaim)
            packageClaim = new PackageClaim();
        return packageClaim;
    }

    public void setPackageClaim(PackageClaim packageClaim)
    {
        this.packageClaim = packageClaim;
    }

    /**
     * 读取的是 用户对象的名称, <b>userName</b> = packageClaim.frontUserClaim.user_name
     * 
     * @return
     */
    public String getUserName()
    {
        return this.getPackageClaim().getFrontUserClaim().getUser_name();
    }

    /**
     * 设置的是 用户对象的名称 <b>userName</b> = packageClaim.frontUserClaim.user_name
     * 
     * @param userName
     */
    public void setUserName(String userName)
    {
        this.getPackageClaim().getFrontUserClaim().setUser_name(userName);
    }

    /**
     * 获取的是包裹的 用户id <b>userId</b> = packageClaim.user_id
     * 
     * @return
     */
    public int getUserId()
    {
        return this.getPackageClaim().getUser_id();
    }

    /**
     * 设置的是包裹的用户id <b>userId</b> = packageClaim.user_id
     * 
     * @param userId
     */
    public void setUserId(int userId)
    {
        this.getPackageClaim().setUser_id(userId);
    }

    /**
     * 获取的是包裹的编号 <b>logistics_code</b> = packageClaim.logistics_code
     * 
     * @return
     */
    public String getLogistics_code()
    {
        return this.getPackageClaim().getLogistics_code();
    }

    /**
     * 设置的是包裹的编号 <b>logistics_code</b> = packageClaim.logistics_code
     * 
     * @param logistics_code
     */
    public void setLogistics_code(String logistics_code)
    {
        this.getPackageClaim().setLogistics_code(logistics_code);
    }

    public double getAmountStart()
    {
        return amountStart;
    }

    public void setAmountStart(double amountStart)
    {
        this.amountStart = amountStart;
    }

    public double getAmountEnd()
    {
        return amountEnd;
    }

    public void setAmountEnd(double amountEnd)
    {
        this.amountEnd = amountEnd;
    }

    public String getApplyTimeStart()
    {
        return applyTimeStart;
    }

    public void setApplyTimeStart(String applyTimeStart)
    {
        this.applyTimeStart = applyTimeStart;
    }

    public String getApplyTimeEnd()
    {
        return applyTimeEnd;
    }

    public void setApplyTimeEnd(String applyTimeEnd)
    {
        this.applyTimeEnd = applyTimeEnd;
    }

}