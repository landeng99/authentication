package com.xiangrui.lmp.business.admin.claim.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.claim.mapper.ClaimMapper;
import com.xiangrui.lmp.business.admin.claim.service.ClaimService;
import com.xiangrui.lmp.business.admin.claim.vo.Claim;
import com.xiangrui.lmp.business.admin.claim.vo.FrontUserClaim;
import com.xiangrui.lmp.business.admin.claim.vo.PackageClaim;

/**
 * 索赔操作
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午2:49:42
 * </p>
 */
@Service("claimService")
public class ClaimServiceImpl implements ClaimService
{

    /**
     * 索赔操作
     */
    @Autowired
    private ClaimMapper claimMapper;

    /**
     * 查询所有索赔,带条件的
     */
    @Override
    public List<Claim> queryAll(Map<String, Object> params)
    {
        return claimMapper.queryAll(params);
    }

    /**
     * 添加
     */
    @SystemServiceLog(description = "添加理赔信息")
    @Override
    public void addClaim(Claim claim)
    {
        claimMapper.addClaim(claim);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改理赔")
    @Override
    public void updateClaim(Claim claim)
    {
        claimMapper.updateClaim(claim);
    }

    /**
     * 删除
     */
    @SystemServiceLog(description = "删除理赔")
    @Override
    public void deleteClaim(int id)
    {
        claimMapper.deleteClaim(id);
    }

    /**
     * 统计索赔数据条数
     */
    @Override
    public int queryCount()
    {
        return claimMapper.queryCount();
    }

    /**
     * 获取某个索赔信息
     */
    @Override
    public Claim queryAllId(int id)
    {
        return claimMapper.queryAllId(id);
    }

    /**
     * 获取某个包裹信息,包裹id
     */
    @Override
    public PackageClaim queryPackageClaim(int id)
    {
        return claimMapper.queryPackageClaim(id);
    }

    /**
     * 获取某个索赔信息的包裹信息中链接的用户信息 ,包裹id
     */
    @Override
    public FrontUserClaim queryFrontUserClaim(int id)
    {
        return claimMapper.queryFrontUserClaim(id);
    }

}
