package com.xiangrui.lmp.business.admin.claim.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.claim.vo.Claim;
import com.xiangrui.lmp.business.admin.claim.vo.FrontUserClaim;
import com.xiangrui.lmp.business.admin.claim.vo.PackageClaim;

/**
 * 索赔操作
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午2:49:21
 * </p>
 */
public interface ClaimService
{

    /**
     * 查询所有索赔
     * 
     * @return
     */
    List<Claim> queryAll(Map<String, Object> params);

    /**
     * 获取某个索赔信息
     * 
     * @param id
     * @return
     */
    Claim queryAllId(int id);

    /**
     * 获取某个索赔信息的包裹信息
     * 
     * @param id
     *            包裹id claim.packageId
     * @return
     */
    PackageClaim queryPackageClaim(int id);

    /**
     * 获取某个索赔信息的包裹信息中链接的用户
     * 
     * @param id
     *            包裹id
     * @return
     */
    FrontUserClaim queryFrontUserClaim(int id);

    /**
     * 添加
     * 
     * @param claim
     */
    void addClaim(Claim claim);

    /**
     * 修改
     * 
     * @param claim
     */
    void updateClaim(Claim claim);

    /**
     * 删除
     * 
     * @param id
     */
    void deleteClaim(int id);

    /**
     * 查询总记录数
     * 
     * @return
     */
    int queryCount();

}
