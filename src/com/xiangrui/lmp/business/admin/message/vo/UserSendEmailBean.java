package com.xiangrui.lmp.business.admin.message.vo;

public class UserSendEmailBean
{
	private float transportCost;
	private int userId;
	private int packageCount;

	public float getTransportCost()
	{
		return transportCost;
	}

	public void setTransportCost(float transportCost)
	{
		this.transportCost = transportCost;
	}

	public int getUserId()
	{
		return userId;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	public int getPackageCount()
	{
		return packageCount;
	}

	public void setPackageCount(int packageCount)
	{
		this.packageCount = packageCount;
	}

}
