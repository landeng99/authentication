package com.xiangrui.lmp.business.admin.message.service.impl;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * mail授权类
 * @author liuhao
 *
 */
public class MailAuthenticator extends Authenticator
{
    private String strUser;//用名
    private String strPwd; //密码
  
    public MailAuthenticator(String user, String password) 
    {  
        this.strUser = user;  
        this.strPwd = password;  
    }  
  
    protected PasswordAuthentication getPasswordAuthentication() 
    {  
        return new PasswordAuthentication(strUser, strPwd);  
    }  

}
