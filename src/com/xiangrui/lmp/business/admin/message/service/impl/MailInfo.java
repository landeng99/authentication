/**
 * 
 */
package com.xiangrui.lmp.business.admin.message.service.impl;

/**
 * 邮件实体bin
 * @author liuhao
 *
 */
public class MailInfo
{
	private String host;// 邮件服务器域名或IP  
    private String from;// 发件人  
    private String cc;// 抄送人  
    private String username;// 发件人用户名  
    private String password;// 发件人密码  
    private boolean encrypt = true;
    /**
     * 注册
     */
    public static final int REGISTRATION = 0;
    /**
     * 找回
     */
    public static final int RETRIEVE_PWD = 1;
    /**
     * 解绑
     */
    public static final int UNBIND_MAIL = 2;
    /**
     * 绑定
     */
    public static final int BIND_MAIL = 3;

    /**
     * 注册标题模板
     */
    private String registrationTitleTemplate;
    /**
     * 找回密码标题模板
     */
    private String retrievePWDTitleTemplate;
    /**
     * 解绑邮箱邮件标题模板
     */
    private String unbindTitleTemplate;
    /**
     * 绑定邮箱邮件标题模板
     */
    private String bindTitleTemplate;
    /**
     * 注册模板
     */
    private String registrationTemplate;
    /**
     * 找回密码模板
     */
    private String retrievePWDTemplate;
    /**
     * 解绑邮箱邮件模板
     */
    private String unbindTemplate;
    /**
     * 绑定邮箱邮件模板
     */
    private String bindTemplate;

	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isEncrypt() {
		return encrypt;
	}
	public void setEncrypt(boolean encrypt) {
		this.encrypt = encrypt;
	}
	
    public String getRegistrationTemplate() {
		return registrationTemplate;
	}
	public void setRegistrationTemplate(String registrationTemplate) {
		this.registrationTemplate = registrationTemplate;
	}
	public String getRetrievePWDTemplate() {
		return retrievePWDTemplate;
	}
	public void setRetrievePWDTemplate(String retrievePWDTemplate) {
		this.retrievePWDTemplate = retrievePWDTemplate;
	}
	public String getUnbindTemplate() {
		return unbindTemplate;
	}
	public void setUnbindTemplate(String unbindTemplate) {
		this.unbindTemplate = unbindTemplate;
	}
	public String getBindTemplate() {
		return bindTemplate;
	}
	public void setBindTemplate(String bindTemplate) {
		this.bindTemplate = bindTemplate;
	}
	
	public String getRegistrationTitleTemplate() {
		return registrationTitleTemplate;
	}
	public void setRegistrationTitleTemplate(String registrationTitleTemplate) {
		this.registrationTitleTemplate = registrationTitleTemplate;
	}
	public String getRetrievePWDTitleTemplate() {
		return retrievePWDTitleTemplate;
	}
	public void setRetrievePWDTitleTemplate(String retrievePWDTitleTemplate) {
		this.retrievePWDTitleTemplate = retrievePWDTitleTemplate;
	}
	public String getUnbindTitleTemplate() {
		return unbindTitleTemplate;
	}
	public void setUnbindTitleTemplate(String unbindTitleTemplate) {
		this.unbindTitleTemplate = unbindTitleTemplate;
	}
	public String getBindTitleTemplate() {
		return bindTitleTemplate;
	}
	public void setBindTitleTemplate(String bindTitleTemplate) {
		this.bindTitleTemplate = bindTitleTemplate;
	}
	@Override
    public String toString() 
    {    
    	StringBuilder sb = new StringBuilder();
    	sb.append("host=");
    	sb.append(host);
    	sb.append("from=");
    	sb.append(from);
    	sb.append("cc=");
    	sb.append(cc);
    	sb.append("username=");
    	sb.append(username);
    	sb.append("password=");
    	sb.append(password);
    	return sb.toString();
    }
}
