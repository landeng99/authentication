package com.xiangrui.lmp.business.admin.message.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.message.service.MessageSendService;
import com.xiangrui.lmp.business.admin.sysSetting.service.SysSettingService;
import com.xiangrui.lmp.business.admin.warningEmailLog.mapper.WarningEmailLogMapper;
import com.xiangrui.lmp.business.admin.warningEmailLog.vo.WarningEmailLog;
import com.xiangrui.lmp.business.admin.warningEmailTemplate.service.WarningEmailTemplateService;
import com.xiangrui.lmp.business.admin.warningEmailTemplate.vo.WarningEmailTemplate;
import com.xiangrui.lmp.business.admin.warningSmsLog.mapper.WarningSmsLogMapper;
import com.xiangrui.lmp.business.admin.warningSmsLog.vo.WarningSmsLog;
import com.xiangrui.lmp.business.admin.warningSmsTemplate.service.WarningSmsTemplateService;
import com.xiangrui.lmp.business.admin.warningSmsTemplate.vo.WarningSmsTemplate;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.util.HttpRequest;
import com.xiangrui.lmp.util.sendSMS;

@Service("messageSendService")
public class MessageSendServiceImpl implements MessageSendService
{
	@Autowired
	private WarningEmailLogMapper warningEmailLogMapper;

	@Autowired
	private WarningSmsLogMapper warningSmsLogMapper;

	@Autowired
	private WarningEmailTemplateService warningEmailTemplateService;

	@Autowired
	private WarningSmsTemplateService warningSmsTemplateService;

	@Autowired
	private SysSettingService sysSettingService;

	/**
	 * 发送邮件
	 * 
	 * @param mailInfo
	 * @param to
	 * @param title
	 * @param content
	 * @throws MailException
	 */
	public static void sendMail(MailInfo mailInfo, String toEmail, String title, String content) throws MailException
	{
		Properties props = System.getProperties();
		props.put("mail.smtp.host", mailInfo.getHost());// 设置邮件服务器的域名或IP
		props.put("mail.smtp.auth", "true");// 授权邮件,mail.smtp.auth必须设置为true

		String password = mailInfo.getPassword();// 密码

		// 传入发件人的用户名和密码,构造MyAuthenticator对象
		MailAuthenticator myauth = new MailAuthenticator(mailInfo.getUsername(), password);

		// 传入props、myauth对象,构造邮件授权的session对象
		Session session = Session.getDefaultInstance(props, myauth);

		// 将Session对象作为MimeMessage构造方法的参数传入构造message对象
		MimeMessage message = new MimeMessage(session);
		try
		{
			message.setFrom(new InternetAddress(mailInfo.getFrom()));// 发件人
			if (toEmail != null && !"".equals(toEmail))
			{
				String to[] = toEmail.split(",");
				for (int i = 0; i < to.length; i++)
				{
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));// 收件人
				}
			}
			// 对多个抄送人的情况进行处理,每个抄送人之间用逗号隔开的
			if (mailInfo.getCc() != null && !"".equals(mailInfo.getCc()))
			{
				String cc[] = mailInfo.getCc().split(",");
				for (int j = 0; j < cc.length; j++)
				{
					message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[j]));// 抄送
				}
			}
			message.setSubject(title);// 主题
			// message.setText(content);// 内容
			message.setContent(content, "text/html;charset=gb2312");
			Transport.send(message);// 调用发送邮件的方法
		} catch (AddressException e)
		{
			throw new MailException("发送邮件错误:发送邮件失败  AddressException");
		} catch (MessagingException e)
		{
			e.printStackTrace();
			throw new MailException("发送邮件错误:发送邮件失败  MessagingException");
		}
	}

	public void sendPayWarnEmail(String receriver_user_name, String receriver_email, String receriver_account, int pacakageCount, String needPayMenoy)
	{
		if (sysSettingService.isAllownWarningEmail())
		{
			WarningEmailTemplate warningEmailTemplate = warningEmailTemplateService.queryEnableWarningEmailTemplate();
			if (warningEmailTemplate != null)
			{
				String sender_email = SYSConstant.EMAIL_ACCOUNT;
				int status = WarningEmailLog.SEND_FAILURE;
				MailInfo mailInfo = new MailInfo();
				mailInfo.setHost(SYSConstant.EMAIL_HOST);
				mailInfo.setFrom(SYSConstant.EMAIL_SENDER);
				mailInfo.setPassword(SYSConstant.EMAIL_PASSWORD);
				mailInfo.setUsername(sender_email);
				//receriver_email="467275639@qq.com";
				String toEmail = receriver_email;
				String title = warningEmailTemplate.getTemp_title();
				String content = warningEmailTemplate.getTemp_conent();
				content = StringUtils.replace(content, "[receriver_user_name]", receriver_user_name);
				content = StringUtils.replace(content, "[pacakage_count]", "" + pacakageCount);
				content = StringUtils.replace(content, "[need_pay_menoy]", needPayMenoy);
				try
				{
					sendMail(mailInfo, toEmail, title, content);
					status = WarningEmailLog.SEND_SUCCESS;
				} catch (MailException e)
				{
					e.printStackTrace();
				}
				WarningEmailLog warningEmailLog = new WarningEmailLog();
				warningEmailLog.setReceriver_user_name(receriver_user_name);
				warningEmailLog.setReceriver_email(receriver_email);
				warningEmailLog.setReceriver_account(receriver_account);
				warningEmailLog.setSender_email(sender_email);
				warningEmailLog.setSend_time(new Date());
				warningEmailLog.setTitle(title);
				warningEmailLog.setContent(content);
				warningEmailLog.setStatus(status);
				warningEmailLogMapper.insertWarningEmailLog(warningEmailLog);
			}
		}
	}

	public boolean resendPayWarnEmail(WarningEmailLog warningEmailLog)
	{
		boolean resendSuccess = false;
		if (sysSettingService.isAllownWarningEmail())
		{
			if (warningEmailLog != null)
			{
				MailInfo mailInfo = new MailInfo();
				String sender_email = SYSConstant.EMAIL_ACCOUNT;
				mailInfo.setHost(SYSConstant.EMAIL_HOST);
				mailInfo.setFrom(SYSConstant.EMAIL_SENDER);
				mailInfo.setPassword(SYSConstant.EMAIL_PASSWORD);
				mailInfo.setUsername(sender_email);
				String toEmail = warningEmailLog.getReceriver_email();
				//toEmail="467275639@qq.com";
				try
				{
					sendMail(mailInfo, toEmail, warningEmailLog.getTitle(), warningEmailLog.getContent());
					resendSuccess = true;
				} catch (MailException e)
				{
					e.printStackTrace();
				}
				Map<String, Object> paremt = new HashMap<String, Object>();
				paremt.put("seq_id", warningEmailLog.getSeq_id());
				paremt.put("resent_count", (warningEmailLog.getResent_count() + 1));
				paremt.put("last_resent_time", new Date());
				paremt.put("status", resendSuccess ? 0 : 1);
				warningEmailLogMapper.updateWarningEmailLogResentCountBySeqId(paremt);
			}
		}
		return resendSuccess;
	}

	public String sendIdCardImportWarnSMS(String receiverMoblie, String logisticsCodeListStr)
	{
		String result = null;
		int status = WarningSmsLog.SEND_FAILURE;
		if (sysSettingService.isAllowWarningSms())
		{
			WarningSmsTemplate warningSmsTemplate = warningSmsTemplateService.queryEnableWarningSmsTemplate();
			if (warningSmsTemplate != null)
			{
				String str = warningSmsTemplate.getTemp_conent();
				str = StringUtils.replace(str, "[logisticsCodeList]", logisticsCodeListStr);
				if (sysSettingService.isInSMSAllowTimeRange())
				{
					Map<String, String> params = new HashMap<String, String>();
					params.put("UserID", SYSConstant.SMS_USERID);
					params.put("Account", SYSConstant.SMS_ACCOUNT);
					params.put("Password", SYSConstant.SMS_PASSWORD);
					//receiverMoblie = "13728696234";
					params.put("Phones", receiverMoblie);
					String content = "";
					try
					{
						content = URLEncoder.encode(str, "utf-8");
					} catch (UnsupportedEncodingException e1)
					{
						e1.printStackTrace();
					}

					params.put("Content", content);
					params.put("SendTime", "");
					params.put("SendType", "1");
					params.put("PostFixNumber", "1");

					try
					{
						result = HttpRequest.postData(SYSConstant.SMS_URL, params, "UTF-8");
						if (result.contains("Sucess"))
						{
							status = WarningSmsLog.SEND_SUCCESS;
						}
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					status = WarningSmsLog.NEED_WAITTING_SEND;
				}
				WarningSmsLog warningSmsLog = new WarningSmsLog();
				warningSmsLog.setReceriver_moblie(receiverMoblie);
				warningSmsLog.setContent(str);
				warningSmsLog.setSend_time(new Date());
				warningSmsLog.setStatus(status);
				warningSmsLogMapper.insertWarningSmsLog(warningSmsLog);
			}
		}
		return result;
	}

	public boolean resendWarnSMS(WarningSmsLog warningSmsLog, boolean isOnTask)
	{
		boolean resendSuccess = false;
		if (warningSmsLog != null)
		{
			if (sysSettingService.isAllowWarningSms() && sysSettingService.isInSMSAllowTimeRange())
			{
				String content = "";
				try
				{
					content = URLEncoder.encode(warningSmsLog.getContent(), "utf-8");
				} catch (UnsupportedEncodingException e1)
				{
					e1.printStackTrace();
				}

				try
				{
					resendSuccess = sendSMS.send(warningSmsLog.getReceriver_moblie(), content);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
				if (isOnTask)
				{
					Map<String, Object> paremt = new HashMap<String, Object>();
					paremt.put("seq_id", warningSmsLog.getSeq_id());
					paremt.put("status", resendSuccess ? 0 : 1);
					warningSmsLogMapper.updateWarningSmsLogSendTimeBySeqId(paremt);
				}
				else
				{
					Map<String, Object> paremt = new HashMap<String, Object>();
					paremt.put("seq_id", warningSmsLog.getSeq_id());
					paremt.put("resent_count", (warningSmsLog.getResent_count() + 1));
					paremt.put("last_resent_time", new Date());
					paremt.put("status", resendSuccess ? 0 : 1);
					warningSmsLogMapper.updateWarningSmsLogResentCountBySeqId(paremt);
				}
			}
		}
		return resendSuccess;
	}
	
	public void sendTaxWarnEmail(String receriver_user_name, String receriver_email, String receriver_account, String emailContext)
	{
		String sender_email = SYSConstant.EMAIL_ACCOUNT;
		int status = WarningEmailLog.SEND_FAILURE;
		MailInfo mailInfo = new MailInfo();
		mailInfo.setHost(SYSConstant.EMAIL_HOST);
		mailInfo.setFrom(SYSConstant.EMAIL_SENDER);
		mailInfo.setPassword(SYSConstant.EMAIL_PASSWORD);
		mailInfo.setUsername(sender_email);
		// receriver_email="467275639@qq.com";
		String toEmail = receriver_email;
		String title = "跨境物流客户需支付关税通知";
		String content = emailContext;
		try
		{
			sendMail(mailInfo, toEmail, title, content);
			status = WarningEmailLog.SEND_SUCCESS;
		} catch (MailException e)
		{
			e.printStackTrace();
		}
		WarningEmailLog warningEmailLog = new WarningEmailLog();
		warningEmailLog.setReceriver_user_name(receriver_user_name);
		warningEmailLog.setReceriver_email(receriver_email);
		warningEmailLog.setReceriver_account(receriver_account);
		warningEmailLog.setSender_email(sender_email);
		warningEmailLog.setSend_time(new Date());
		warningEmailLog.setTitle(title);
		warningEmailLog.setContent(content);
		warningEmailLog.setStatus(status);
		warningEmailLogMapper.insertWarningEmailLog(warningEmailLog);
	}
}
