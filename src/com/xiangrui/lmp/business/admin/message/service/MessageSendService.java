package com.xiangrui.lmp.business.admin.message.service;

import com.xiangrui.lmp.business.admin.warningEmailLog.vo.WarningEmailLog;
import com.xiangrui.lmp.business.admin.warningSmsLog.vo.WarningSmsLog;

public interface MessageSendService
{
	void sendPayWarnEmail(String receriver_user_name, String receriver_email, String receriver_account, int pacakageCount, String needPayMenoy);
	String sendIdCardImportWarnSMS(String receiverMobile,String logisticsCodeListStr);
	boolean resendPayWarnEmail(WarningEmailLog warningEmailLog);
	boolean resendWarnSMS(WarningSmsLog warningSmsLog, boolean isOnTask);
	void sendTaxWarnEmail(String receriver_user_name, String receriver_email, String receriver_account,String emailContext);
}
