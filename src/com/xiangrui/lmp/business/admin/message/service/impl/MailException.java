package com.xiangrui.lmp.business.admin.message.service.impl;


public class MailException extends Exception
{
	private static final long serialVersionUID = -7665352250908695434L;
	private String info;
	/**
	 * 重新定义构造函数
	 */
	public MailException(String mess)
	{
		super(mess);
		this.info = mess;
	}
	public MailException(String mess,Throwable e)
	{
		super(mess,e);
		this.info = mess;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
}
