package com.xiangrui.lmp.business.admin.consultInfo.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.consultInfo.mapper.ConsultInfoMapper;
import com.xiangrui.lmp.business.admin.consultInfo.service.ConsultInfoService;
import com.xiangrui.lmp.business.admin.consultInfo.vo.ConsultInfo;

@Service("consultInfoService")
public class ConsultInfoServiceImpl implements ConsultInfoService
{
	@Autowired
	private ConsultInfoMapper consultInfoMapper;

	@Override
	public List<ConsultInfo> queryAll(Map<String, Object> params)
	{
		return consultInfoMapper.queryAll(params);
	}

	@Override
	public ConsultInfo queryById(int seq_id)
	{
		return consultInfoMapper.queryById(seq_id);
	}

	@Override
	public List<ConsultInfo> queryAllAvaliable()
	{
		return consultInfoMapper.queryAllAvaliable();
	}

	@Override
	public List<ConsultInfo> queryAllAvaliable4()
	{
		return consultInfoMapper.queryAllAvaliable4();
	}

	@Override
	public void updateConsultInfo(ConsultInfo consultInfo)
	{
		consultInfoMapper.updateConsultInfo(consultInfo);
	}

	@Override
	public int addConsultInfo(ConsultInfo consultInfo)
	{
		return consultInfoMapper.addConsultInfo(consultInfo);
	}

	@Override
	public void updateConsultInfoStatus(ConsultInfo consultInfo)
	{
		consultInfoMapper.updateConsultInfoStatus(consultInfo);
	}

	@Override
	public void deleteConsultInfo(int seq_id)
	{
		consultInfoMapper.deleteConsultInfo(seq_id);
	}

}
