package com.xiangrui.lmp.business.admin.consultInfo.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.consultInfo.vo.ConsultInfo;
public interface ConsultInfoMapper
{
    List<ConsultInfo> queryAll(Map<String, Object> params);

    ConsultInfo queryById(int seq_id);
    
    List<ConsultInfo> queryAllAvaliable();
    
    List<ConsultInfo> queryAllAvaliable4();

    void updateConsultInfo(ConsultInfo consultInfo);

    int addConsultInfo(ConsultInfo consultInfo);
    
    void updateConsultInfoStatus(ConsultInfo consultInfo);
    
    void deleteConsultInfo(int seq_id);
}
