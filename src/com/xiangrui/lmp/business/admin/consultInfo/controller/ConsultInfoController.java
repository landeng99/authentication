package com.xiangrui.lmp.business.admin.consultInfo.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.banner.service.BannerService;
import com.xiangrui.lmp.business.admin.banner.vo.Banner;
import com.xiangrui.lmp.business.admin.consultInfo.service.ConsultInfoService;
import com.xiangrui.lmp.business.admin.consultInfo.vo.ConsultInfo;
import com.xiangrui.lmp.business.base.BaseBanner;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.constant.WebConstants;
import com.xiangrui.lmp.init.AppServerUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/consult")
public class ConsultInfoController
{
	/**
	 * 图片后缀
	 */
	private static final String[] IMG_SUFFIX_ARY = new String[] { ".bmp", ".jpg", ".jpeg", ".png", ".gif", ".pcx", ".tiff", ".tga", ".exif", ".fpx", ".svg", ".psd", ".cdr", ".pcd", ".dxf", ".ufo", ".eps", ".hdri", ".ai", ".raw" };

	/**
	 * 上传图片在服务器存放路径
	 */
	private static final String UPLOAD_IMG_PATH = AppServerUtil.getWebRoot() + WebConstants.UPLOAD_PATH_IMAGES;

	/**
	 * 操作轮播图
	 */
	@Autowired
	private ConsultInfoService consultInfoService;

	@RequestMapping("/queryAll")
	public String queryAll(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);
		request.setAttribute("pageView", pageView);
		List<ConsultInfo> consultInfoLists = consultInfoService.queryAll(params);

		request.setAttribute("consultInfoLists", consultInfoLists);
		return "back/consultInit";
	}

	/**
	 * 启用或者禁用
	 * 
	 * @return
	 */
	@RequestMapping("/updateConsult")
	public String updateConsult(HttpServletRequest request, ConsultInfo consultInfo)
	{
		// 匹配数据库
		consultInfo = consultInfoService.queryById(consultInfo.getSeq_id());

		// 更新状态
		if (consultInfo.getStatus() == BaseBanner.BANNER_STATUS_DISABLE)
		{
			consultInfo.setStatus(BaseBanner.BANNER_STATUS_ENABLE);
		}
		else if (consultInfo.getStatus() == BaseBanner.BANNER_STATUS_ENABLE)
		{
			consultInfo.setStatus(BaseBanner.BANNER_STATUS_DISABLE);
		}

		// 更新数据库
		consultInfoService.updateConsultInfoStatus(consultInfo);

		ServletContext application = request.getSession().getServletContext();
		application.setAttribute("consultInfoLists", null);
		return queryAll(request);
	}

	@RequestMapping("/deleteConsultInfo")
	public String deleteConsultInfo(HttpServletRequest request, ConsultInfo consultInfo)
	{
		// 匹配数据库
		consultInfo = consultInfoService.queryById(consultInfo.getSeq_id());

		// 删除图片数据库信息
		consultInfoService.deleteConsultInfo(consultInfo.getSeq_id());
		ServletContext application = request.getSession().getServletContext();
		application.setAttribute("consultInfoLists", null);
		return queryAll(request);
	}

	@RequestMapping("/toConsultInfo")
	public String toConsultInfo()
	{
		return "back/consultUpload";
	}


	/**
	 * 上传添加
	 * 
	 * @param request
	 * @param banner
	 * @return
	 */
	@RequestMapping("/addConsultInfo")
	public String addConsultInfo(HttpServletRequest request, ConsultInfo consultInfo)
	{
		// 乱码处理
		String tConsultTitle = "";
		String tConsultDetail = "";
//		try
//		{
//			tConsultTitle = new String(consultInfo.getConsult_title().getBytes("ISO-8859-1"), "UTF-8");
//			tConsultDetail = new String(consultInfo.getConsult_detail().getBytes("ISO-8859-1"), "UTF-8");
//		} catch (UnsupportedEncodingException e)
//		{
//			e.printStackTrace();
//		}
//		consultInfo.setConsult_detail(tConsultDetail);
//		consultInfo.setConsult_title(tConsultTitle);

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		
		String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "/resource" + File.separator + "upload" + File.separator + "consult" + File.separator;
		File file = new File(ctxPath);

		if (!file.exists())
		{
			file.mkdirs();
		}
		// 上传文件名
		String fileName = null;
		// 返回前台图片路径
		String img_path = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet())
		{
			MultipartFile mf = entity.getValue();
			fileName = mf.getOriginalFilename();
			String newFilename = "";
			img_path = "/" + "upload" + "/" + "consult" + "/";
			try
			{
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				String newName = format.format(new Date());
				newName += RandomStringUtils.randomAlphanumeric(5);
				String subfix = StringUtils.substringAfterLast(fileName, ".");
				newName += "." + subfix;
				
				newFilename = newName;
				// 将文件输出
				String path = ctxPath + newFilename;
				File uploadFile = new File(path);
				FileCopyUtils.copy(mf.getBytes(), uploadFile);

				// 数据库存储路径
				img_path = img_path + newFilename;
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		consultInfo.setDisplay_image(img_path);
		
		consultInfoService.addConsultInfo(consultInfo);
		ServletContext application = request.getSession().getServletContext();
		application.setAttribute("consultInfoLists", null);
		return queryAll(request);
	}
}
