package com.xiangrui.lmp.business.admin.article.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.article.vo.ArticleClassify;

public interface ArticleClassifyMapper
{

    /**
     * 添加文章分类
     * 
     * @param articleClassify
     */
    void addArticleClassify(ArticleClassify articleClassify);

    /**
     * 分页查询所有文章分类
     * 
     * @param params
     * @return
     */
    public List<ArticleClassify> queryAll(Map<String, Object> params);

    /**
     * 编辑文章分类
     * 
     * @param articleClassify
     * @return
     */
    public void updateArticleClassify(ArticleClassify articleClassify);

    /**
     * 查询文章分类
     * 
     * @param id
     * @return
     */
    public ArticleClassify queryArticleClassify(int id);

    /**
     * 删除文章分类
     * 
     * @param id
     * @return
     */
    public void deleArticleClassify(int id);

    /**
     * 查询所有一级文章分类
     * @return
     */
    List<ArticleClassify> queryArticleClassifyLevel();
    
    /**
     * 查询所有二级文章分类
     * @return
     */
    List<ArticleClassify> queryArticleClassifyLevelNoParent();
}
