package com.xiangrui.lmp.business.admin.article.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.article.vo.Art;
import com.xiangrui.lmp.business.admin.article.vo.Article;

public interface ArticleMapper {

	/**
	 * 添加文章
	 * @param article
	 */
	void addArticle(Article article);
	
	/**
	 * 分页查询所有文章
	 * @param params
	 * @return
	 */
	public List<Article> queryAll(Map<String,Object> params);
	
	/**
	 * 编辑文章
	 * @param article
	 * @return
	 */
	public void updateArticle(Article art);
	
	/**
	 * 查询文章
	 * @param id
	 * @return
	 */
	public Article queryArticle(int id);
	
	/**
	 * 查询文章以及分类名称
	 * @param id
	 * @return
	 */
	public Art queryArt(int id);
	
	/**
	 * 删除文章
	 * @param id
	 * @return
	 */
	public void deleArticle(int id);
}
