package com.xiangrui.lmp.business.admin.article.controller;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.article.service.ArticleClassifyService;
import com.xiangrui.lmp.business.admin.article.service.ArticleService;
import com.xiangrui.lmp.business.admin.article.vo.Art;
import com.xiangrui.lmp.business.admin.article.vo.Article;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.PageView;

@Controller
@RequestMapping("/admin/article")
public class ArticleController
{

    @Autowired
    private ArticleService articleService;
    @Autowired
    private ArticleClassifyService articleClassifyService;

    /**
     * 进入添加文章页面
     * 
     * @return
     */
    @RequestMapping("/add")
    public String loginAddArticle(HttpServletRequest re)
    {
        re.setAttribute("id", 0);
        re.setAttribute("content", "");
        re.setAttribute("title", "");

        User backUser = (User) re.getSession().getAttribute("back_user");
        re.setAttribute("author", backUser.getUsername());

        re.setAttribute("classify_id", 0);
        re.setAttribute("artClaList",
                articleClassifyService.queryArticleClassifyLevelNoParent());
        return "back/updateArticle";
    }

    /**
     * 根据ID查询文章信息
     * 
     * @return
     */
    @RequestMapping("/update")
    public String loginUpdateArticle(HttpServletRequest re, int id)
    {
        // Article article=articleService.queryArticle(id);
        Art art = articleService.queryArt(id);
        re.setAttribute("id", art.getId());
        re.setAttribute("content", art.getContent());
        re.setAttribute("title", art.getTitle());
        re.setAttribute("author", art.getAuthor());
        re.setAttribute("classify_id", art.getClassify_id());

        re.setAttribute("artClaList",
                articleClassifyService.queryArticleClassifyLevelNoParent());
        return "back/updateArticle";
    }

    /**
     * 查询所有文章信息
     * 
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAllArticle(HttpServletRequest request)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");

        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);

        List<Article> list = articleService.queryAll(params);
        request.setAttribute("pageView", pageView);
        request.setAttribute("articleLists", list);
        return "back/articleList";
    }

    /**
     * 修改文章信息
     * 
     * @return
     */
    @RequestMapping("/updataArticle")
    public String updataArticle(HttpServletRequest request, Article art)
    {
        if (art.getId() > 0)
        {
            articleService.updateArticle(art);
        } else
        {
            Date date = new Date();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String create_time = df.format(date);
            art.setCreate_time(create_time);
            articleService.addArticle(art);
        }

        // 前台文章application变化更新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("artClassList", null);
        application.setAttribute("artClassListParent", null);

        application.setAttribute("artList", articleService.queryAll(null));

        return queryAllArticle(request);
    }

    /**
     * 删除文章
     * 
     * @return
     */
    @RequestMapping("/deleArticle")
    public String deleArticle(HttpServletRequest request, int id)
    {
        articleService.deleArticle(id);

        // 前台文章application变化更新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("artClassList", null);
        application.setAttribute("artClassListParent", null);

        application.setAttribute("artList", articleService.queryAll(null));

        return queryAllArticle(request);
    }
}
