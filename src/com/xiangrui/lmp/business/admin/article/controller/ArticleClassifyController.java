package com.xiangrui.lmp.business.admin.article.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.article.service.ArticleClassifyService;
import com.xiangrui.lmp.business.admin.article.service.ArticleService;
import com.xiangrui.lmp.business.admin.article.vo.ArticleClassify;
import com.xiangrui.lmp.util.PageView;

@Controller
@RequestMapping("/admin/articleClassify")
public class ArticleClassifyController
{

    @Autowired
    private ArticleService articleService;
    @Autowired
    private ArticleClassifyService articleClassifyService;

    /**
     * 进入添加文章分类
     * 
     * @return
     */
    @RequestMapping("/add")
    public String loginAddArticleCategory(ArticleClassify articleClassify,
            HttpServletRequest request)
    {
        if (articleClassify.getId() == 0)
        {
            request.setAttribute("pojo", new ArticleClassify());
            request.setAttribute("list",
                    articleClassifyService.queryArticleClassifyLevel());
        } else
        {
            articleClassify = articleClassifyService
                    .queryArticleClassify(articleClassify.getId());
            request.setAttribute("pojo", articleClassify);
            if (articleClassify.getParent_id() == 0)
            {
                request.setAttribute("list", new ArrayList<ArticleClassify>());
            } else
            {
                request.setAttribute("list",
                        articleClassifyService.queryArticleClassifyLevel());
            }
        }

        // 前台文章application变化更新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("artClassList", null);
        application.setAttribute("artClassListParent", null);

        application.setAttribute("artList", null);
        return "back/addHelpClassify";
    }

    /**
     * 添加文章分类
     * 
     * @return
     */
    @RequestMapping("/addArticleClassify")
    public String AddArticleCategory(ArticleClassify articleClassify,
            HttpServletRequest request)
    {
        if (articleClassify.getId() == 0)
        {
            articleClassifyService.addArticleClassify(articleClassify);
        } else
        {
            articleClassifyService.updateArticleClassify(articleClassify);
        }

        // 前台文章application变化更新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("artClassList", null);
        application.setAttribute("artClassListParent", null);

        application.setAttribute("artList", null);
        return queryAllArticleClassify(request);
    }

    /**
     * 查询所有文章分类信息
     * 
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAllArticleClassify(HttpServletRequest request)
    {
 
        Map<String, Object> params = new HashMap<String, Object>();
        List<ArticleClassify> list = articleClassifyService.queryAll(params);
        Collections.sort(list);
        
        Map<Integer, List<ArticleClassify>> map = new HashMap<Integer, List<ArticleClassify>>();
        for (ArticleClassify articleClassify : list)
        {
            
            int parent = articleClassify.getParent_id();
            if(articleClassify.getParent_id() == 0){
                List<ArticleClassify> listParent = new ArrayList<ArticleClassify>();
                listParent.add(articleClassify);
                map.put(articleClassify.getId(), listParent);
            }
            if(map.containsKey(parent)){
                map.get(parent).add(articleClassify);
            }
        }
        
        List<ArticleClassify> listParent = new ArrayList<ArticleClassify>();
        for (Integer key : map.keySet())
        {
            for (ArticleClassify articleClassify : map.get(key))
            {
                listParent.add(articleClassify);
            }
        }
        
        request.setAttribute("articleClassifyLists", listParent);
        return "back/articleClassifyList";
    }

    /**
     * 删除文章分类
     * @return
     */
    @RequestMapping("/deleArticleClassify")
    public String deleArticleClassify(HttpServletRequest request, int id)
    {
        articleClassifyService.deleArticleClassify(id);

        // 前台文章application变化更新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("artClassList", null);
        application.setAttribute("artClassListParent", null);

        application.setAttribute("artList", null);
        return queryAllArticleClassify(request);
    }
}
