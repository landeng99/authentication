package com.xiangrui.lmp.business.admin.article.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.article.vo.ArticleClassify;

public interface ArticleClassifyService
{

    /**
     * 添加文章分类
     * 
     * @param articleClassify
     */
    public void addArticleClassify(ArticleClassify articleClassify);

    /**
     * 查询所有文章分类
     * 
     * @param params
     * @return
     */
    public List<ArticleClassify> queryAll(Map<String, Object> params);

    /**
     * 编辑文章分类
     * 
     * @param articleClassify
     */
    public void updateArticleClassify(ArticleClassify articleClassify);

    /**
     * 查询文章分类
     * 
     * @param id
     */
    public ArticleClassify queryArticleClassify(int id);

    /**
     * 删除文章分类
     * 
     * @param id
     */
    public void deleArticleClassify(int id);

    /**
     * 查询所有一级文章分类
     * 
     * @return
     */
    public List<ArticleClassify> queryArticleClassifyLevel();
    
    /**
     * 查询所有二级文章分类
     * @return
     */
    List<ArticleClassify> queryArticleClassifyLevelNoParent();
}
