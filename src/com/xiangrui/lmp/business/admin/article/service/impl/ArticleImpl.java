package com.xiangrui.lmp.business.admin.article.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.article.mapper.ArticleMapper;
import com.xiangrui.lmp.business.admin.article.service.ArticleService;
import com.xiangrui.lmp.business.admin.article.vo.Art;
import com.xiangrui.lmp.business.admin.article.vo.Article;

@Service(value="articleService")
public class ArticleImpl implements ArticleService{

	@Autowired
	private ArticleMapper articleMapper;
	
	@Override
	public void addArticle(Article article) {

		articleMapper.addArticle(article);
	}

	@Override
	public List<Article> queryAll(Map<String, Object> params) {

		return articleMapper.queryAll(params);
	}

	@Override
	public void updateArticle(Article art) {
		
		articleMapper.updateArticle(art);
	}

	@Override
	public Article queryArticle(int id) {
		
		return articleMapper.queryArticle(id);
	}

	@Override
	public void deleArticle(int id) {
		
		articleMapper.deleArticle(id);
	}

	@Override
	public Art queryArt(int id) {
		
		return articleMapper.queryArt(id);
	}

}
