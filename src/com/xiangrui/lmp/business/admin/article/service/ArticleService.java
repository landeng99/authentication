package com.xiangrui.lmp.business.admin.article.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.article.vo.Art;
import com.xiangrui.lmp.business.admin.article.vo.Article;


public interface ArticleService {

	/**
	 * 添加文章
	 * @param article
	 */
	public void addArticle(Article article);
	
	/**
	 * 查询所有文章
	 * @param params 
	 * @return
	 */
	public List<Article> queryAll(Map<String,Object> params);
	
	/**
	 * 编辑文章
	 * @param article
	 */
	public void updateArticle(Article art);
	
	/**
	 * 查询文章
	 * @param id
	 */
	public Article queryArticle(int id);
	
	
	/**
	 * 查询文章以及文章名称
	 * @param id
	 */
	public Art queryArt(int id);
	/**
	 * 删除文章
	 * @param id
	 */
	public void deleArticle(int id);
}
