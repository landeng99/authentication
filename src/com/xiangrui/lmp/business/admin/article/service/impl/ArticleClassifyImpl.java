package com.xiangrui.lmp.business.admin.article.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.article.mapper.ArticleClassifyMapper;
import com.xiangrui.lmp.business.admin.article.service.ArticleClassifyService;
import com.xiangrui.lmp.business.admin.article.vo.ArticleClassify;

@Service(value = "articleClassifyService")
public class ArticleClassifyImpl implements ArticleClassifyService
{

    @Autowired
    private ArticleClassifyMapper articleClassifyMapper;

    @SystemServiceLog(description = "添加文章分类")
    @Override
    public void addArticleClassify(ArticleClassify articleClassify)
    {

        articleClassifyMapper.addArticleClassify(articleClassify);
    }

    @Override
    public List<ArticleClassify> queryAll(Map<String, Object> params)
    {

        return (List<ArticleClassify>) this.articleClassifyMapper
                .queryAll(params);
    }

    @SystemServiceLog(description = "修改文章分类")
    @Override
    public void updateArticleClassify(ArticleClassify articleClassify)
    {

        articleClassifyMapper.updateArticleClassify(articleClassify);
    }

    @Override
    public ArticleClassify queryArticleClassify(int id)
    {

        return articleClassifyMapper.queryArticleClassify(id);
    }

    @SystemServiceLog(description = "删除文章分类")
    @Override
    public void deleArticleClassify(int id)
    {

        articleClassifyMapper.deleArticleClassify(id);
    }

    @Override
    public List<ArticleClassify> queryArticleClassifyLevel()
    {
        return articleClassifyMapper.queryArticleClassifyLevel();
    }

    @Override
    public List<ArticleClassify> queryArticleClassifyLevelNoParent()
    {
        return articleClassifyMapper.queryArticleClassifyLevelNoParent();
    }

}
