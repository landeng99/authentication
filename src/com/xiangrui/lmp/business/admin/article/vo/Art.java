package com.xiangrui.lmp.business.admin.article.vo;

public class Art extends Article{

	/**
     * 文章文类名称
     */
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
