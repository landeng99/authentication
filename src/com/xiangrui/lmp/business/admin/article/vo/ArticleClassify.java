package com.xiangrui.lmp.business.admin.article.vo;

import com.xiangrui.lmp.business.base.BaseArticleClassify;

public class ArticleClassify extends BaseArticleClassify
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ArticleClassify(int id, String name, int parent_id)
    {
        super(id, name, parent_id);
    }

    public ArticleClassify(String name, int parent_id)
    {
        super(name, parent_id);
    }

    public ArticleClassify()
    {
        super();
    }

}
