package com.xiangrui.lmp.business.admin.article.vo;

import com.xiangrui.lmp.business.base.BaseArticle;

/**
 * 文章实际实体
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-17 下午2:27:45</p>
 */
public class Article extends BaseArticle{
    
    private static final long serialVersionUID = 1L;
    
    /**
     * 文章类别
     */
    private ArticleClassify articleClassify;
    
    public ArticleClassify getArticleClassify()
    {
        return articleClassify;
    }

    public void setArticleClassify(ArticleClassify articleClassify)
    {
        this.articleClassify = articleClassify;
    }

    public Article(String content, String title, String author, int classify_id) {
		super(content, title, author, classify_id);
	}

	public Article(String content, String title, String author,
	        String create_time, int classify_id) {
		super(content, title, author, create_time, classify_id);
	}

	public Article() {
		super();
	}

}
