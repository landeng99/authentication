package com.xiangrui.lmp.business.admin.goodsbrand.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.goodsbrand.service.GoodsBrandService;
import com.xiangrui.lmp.business.admin.goodsbrand.vo.GoodsBrand;
import com.xiangrui.lmp.util.PageView;

/**
 * 品牌分类
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-25 下午7:55:50
 * </p>
 */
@Controller
@RequestMapping("/admin/goodsBrand")
public class GoodsBrandController
{

    /**
     * 品牌分裂
     */
    @Autowired
    private GoodsBrandService goodsBrandService;

    /**
     * 首次进入
     * 
     * @return
     */
    @RequestMapping("queryAllOne")
    public String queryAllOne(HttpServletRequest request)
    {
        return queryAll(request, null);
    }

    /**
     * 查询所有品牌信息
     * 
     * @return
     */
    @RequestMapping("queryAll")
    public String queryAll(HttpServletRequest request, GoodsBrand goodsBrand)
    {
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);

        List<GoodsBrand> list = goodsBrandService.queryAll(params);
        request.setAttribute("pageView", pageView);
        request.setAttribute("goodsBrandLists", list);
        return "back/goodsBrandList";
    }

    /**
     * 进入详细列表
     * 
     * @return
     */
    @RequestMapping("toUpdateGoodsBrand")
    public String toUpdateGoodsBrand(HttpServletRequest request,
            GoodsBrand goodsBrand)
    {
        goodsBrand = this.goodsBrandService.queryAllId(goodsBrand
                .getGoodsTypeId());
        request.setAttribute("goodsBrandPojo", goodsBrand);
        return "back/goodsBrandInfo";
    }

    /**
     * 进入修改
     * 
     * @return
     */
    @RequestMapping("updateGoodsBrand")
    public String updateGoodsBrand(HttpServletRequest request,
            GoodsBrand goodsBrand)
    {
        goodsBrand = this.goodsBrandService.queryAllId(goodsBrand
                .getGoodsTypeId());
        request.setAttribute("goodsBrandPojo", goodsBrand);
        return "back/updateGoodsBrand";
    }

    /**
     * 进入增加
     * 
     * @return
     */
    @RequestMapping("addGoodsBrand")
    public String addGoodsBrand(HttpServletRequest request)
    {
        request.setAttribute("goodsBrandPojo", new GoodsBrand());
        return "back/updateGoodsBrand";
    }

    /**
     * 进行增加或者修改
     * 
     * @return
     */
    @RequestMapping("saveOrUpdateGoodsBrand")
    public String saveOrUpdateGoodsBrand(HttpServletRequest request,
            GoodsBrand goodsBrand)
    {
        // 是否存在id
        if (goodsBrand.getGoodsTypeId() == 0)
        {
            this.goodsBrandService.addGoodsBrand(goodsBrand);
        } else
        {
            this.goodsBrandService.updateGoodsBrand(goodsBrand);
        }
        return queryAll(request, null);
    }

    /**
     * 删除
     * 
     * @return
     */
    @RequestMapping("deleteGoodsBrand")
    public String deleteGoodsBrand(GoodsBrand goodsBrand)
    {
        this.goodsBrandService.delGoodsBrand(goodsBrand.getGoodsTypeId());
        return "back/goodsBrandList";
    }

}
