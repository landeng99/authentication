package com.xiangrui.lmp.business.admin.goodsbrand.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.goodsbrand.mapper.GoodsBrandMapper;
import com.xiangrui.lmp.business.admin.goodsbrand.service.GoodsBrandService;
import com.xiangrui.lmp.business.admin.goodsbrand.vo.GoodsBrand;

/**
 * 品牌分类
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:25:23
 * </p>
 */
@Service("goodsBrandService")
public class GoodsBrandServiceImpl implements GoodsBrandService
{

    /**
     * 品牌分类
     */
    @Autowired
    private GoodsBrandMapper goodsBrandMapper;

    /**
     * 查询所有,带条件,其实在品牌分类中可以不带条件,因为没有分页
     */
    @Override
    public List<GoodsBrand> queryAll(Map<String, Object> parmse)
    {
        return goodsBrandMapper.queryAll(parmse);
    }

    /**
     * 查询具体品牌信息,品牌id
     */
    @Override
    public GoodsBrand queryAllId(int id)
    {
        return goodsBrandMapper.queryAllId(id);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改商品品牌")
    @Override
    public void updateGoodsBrand(GoodsBrand goodsBrand)
    {
        goodsBrandMapper.updateGoodsBrand(goodsBrand);
    }

    /**
     * 删除
     */
    @SystemServiceLog(description = "删除商品品牌")
    @Override
    public void delGoodsBrand(int id)
    {
        goodsBrandMapper.delGoodsBrand(id);
    }

    /**
     * 新增
     */
    @SystemServiceLog(description = "新增商品品牌")
    @Override
    public void addGoodsBrand(GoodsBrand goodsBrand)
    {
        goodsBrandMapper.addGoodsBrand(goodsBrand);
    }
}
