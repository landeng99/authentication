package com.xiangrui.lmp.business.admin.goodsbrand.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.goodsbrand.vo.GoodsBrand;

/**
 * 品牌分类
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:24:57
 * </p>
 */
public interface GoodsBrandService
{

    /**
     * 查询所有品牌类型
     * 
     * @param parmse
     * @return
     */
    List<GoodsBrand> queryAll(Map<String, Object> parmse);

    /**
     * 查询单个品牌
     * 
     * @param id
     * @return
     */
    GoodsBrand queryAllId(int id);

    /**
     * 修改品牌
     * 
     * @param goodsBrand
     */
    void updateGoodsBrand(GoodsBrand goodsBrand);

    /**
     * 删除品牌
     * 
     * @param id
     */
    void delGoodsBrand(int id);

    /**
     * 增加品牌
     * 
     * @param goodsBrand
     */
    void addGoodsBrand(GoodsBrand goodsBrand);

}
