package com.xiangrui.lmp.business.admin.goodsbrand.vo;

import com.xiangrui.lmp.business.base.BaseGoodsBrand;

/**
 * 品牌分类
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:27:30
 * </p>
 */
public class GoodsBrand extends BaseGoodsBrand
{

    private static final long serialVersionUID = 1L;

}
