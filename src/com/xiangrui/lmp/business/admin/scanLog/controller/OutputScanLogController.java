package com.xiangrui.lmp.business.admin.scanLog.controller;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.scanLog.service.OutputScanLogService;
import com.xiangrui.lmp.business.admin.scanLog.vo.InputScanLog;
import com.xiangrui.lmp.business.admin.scanLog.vo.OutputScanLog;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/outputScanLog")
public class OutputScanLogController
{
	@Autowired
	private OutputScanLogService outputScanLogService;

	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

	/**
	 * 详细日志记录列表搜索
	 * 
	 * @return
	 */
	@RequestMapping("/search")
	public String search(HttpServletRequest request, String scan_date, String pkg_no,String pick_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);

		}
		else
		{
			pageView = new PageView(1);

			params.put("scan_date", scan_date);
			params.put("pkg_no", pkg_no);
			params.put("pick_id", pick_id);
		}
		params.put("pageView", pageView);
		request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		request.setAttribute("pageView", pageView);
		List<OutputScanLog> outputScanLogList = outputScanLogService.queryOutputScanLog(params);

		request.setAttribute("outputScanLogList", outputScanLogList);
		request.setAttribute("params", params);
		return "back/outputScanLogList";
	}
	
	@RequestMapping("/queryAllScanLog")
	@ResponseBody
	public Map<String, Object> queryAllScanLog(HttpServletRequest request,String pick_id)
	{
		Map<String, Object> rtnMap = new HashMap<String, Object>();

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pick_id", pick_id);
		List<OutputScanLog> outputScanLogList = outputScanLogService.queryOutputScanLog(params);
		rtnMap.put("outputScanLogList", outputScanLogList);
		return rtnMap;
	}
	
	@RequestMapping("/showAllScanLog")
	public String showAllScanLog(HttpServletRequest request,String pick_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pick_id", pick_id);
		List<OutputScanLog> outputScanLogList = outputScanLogService.queryOutputScanLog(params);
		request.setAttribute("outputScanLogList", outputScanLogList);
		return "back/showAllScanLog";
	}
	
	@RequestMapping("/query5ScanLog")
	@ResponseBody
	public Map<String, Object> query5ScanLog(HttpServletRequest request,String pick_id)
	{
		Map<String, Object> rtnMap = new HashMap<String, Object>();

		Map<String, Object> params = new HashMap<String, Object>();
		PageView pageView = new PageView(5,1);
		params.put("pageView", pageView);
		params.put("pick_id", pick_id);
		List<OutputScanLog> outputScanLogList = outputScanLogService.queryOutputScanLog(params);
		rtnMap.put("outputScanLogList", outputScanLogList);
		return rtnMap;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/exportOutputScanLog")
	public void exportOutputScanLog(HttpServletRequest request, HttpServletResponse response,String pick_id)
	{
		try
		{
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "出库扫描日志";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");
			Map<String, Object> params = new HashMap<String, Object>();
//			params.put("scan_date", scan_date);
			params.put("pick_id", pick_id);
			List<OutputScanLog> outputScanLogList = outputScanLogService.queryOutputScanLog(params);
			@SuppressWarnings("resource")
			XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
			// 新建sheet
			XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
			// 第一列固定
			xssfSheet.createFreezePane(0, 1, 0, 1);

			// 颜色蓝色
			XSSFColor redColor = new XSSFColor(Color.GREEN);
			// 白色
			XSSFColor whiteColor = new XSSFColor(Color.WHITE);

			// 样式白色居中
			XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
			style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			style1.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			style1.setFillForegroundColor(whiteColor);
			style1.setWrapText(true);
			// 样式蓝色居中
			XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
			style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			style2.setFillForegroundColor(redColor);
			style2.setWrapText(true);

			// 列宽
			xssfSheet.setColumnWidth(0, 6000);
			xssfSheet.setColumnWidth(1, 6000);
			xssfSheet.setColumnWidth(2, 3000);
			xssfSheet.setColumnWidth(3, 7000);
			xssfSheet.setColumnWidth(4, 4000);
			xssfSheet.setColumnWidth(5, 3000);

			// 表头列
			XSSFRow firstXSSFRow = xssfSheet.createRow(0);

			List<String> columnsList = new ArrayList<String>();
			columnsList.add("扫描时间");
			columnsList.add("单号");
			columnsList.add("包裹状态");
			columnsList.add("扫描添加托盘失败原因");
			columnsList.add("扫描人员");
			columnsList.add("扫描次数");
			for (int i = 0; i < columnsList.size(); i++)
			{
				XSSFCell cell = firstXSSFRow.createCell(i);
				cell.setCellType(XSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(columnsList.get(i));
				cell.setCellStyle(style1);
			}

			OutputScanLog outputScanLog = null;
			int rowCount = 1;
			if (outputScanLogList != null && outputScanLogList.size() > 0)
			{
				// 时间格式
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				for (int j = 0; j < outputScanLogList.size(); j++)
				{
					outputScanLog = outputScanLogList.get(j);
					XSSFRow row = xssfSheet.createRow(rowCount++);

					// 扫描时间
					XSSFCell cell0 = row.createCell(0);
					cell0.setCellValue(simpleDateFormat.format(outputScanLog.getScan_date()));
					cell0.setCellStyle(outputScanLog.getAdd_pallet_success() == 1 ? style2 : style1);

					// 单号
					XSSFCell cell1 = row.createCell(1);
					cell1.setCellValue(StringUtils.trimToEmpty(outputScanLog.getPkg_no()));
					cell1.setCellStyle(outputScanLog.getAdd_pallet_success() == 1 ? style2 : style1);

					// 包裹状态
					XSSFCell cell2 = row.createCell(2);
					cell2.setCellValue(StringUtils.trimToEmpty(outputScanLog.getPkg_status()));
					cell2.setCellStyle(outputScanLog.getAdd_pallet_success() == 1 ? style2 : style1);

					// 扫描添加托盘失败原因
					XSSFCell cell3 = row.createCell(3);
					cell3.setCellValue(StringUtils.trimToEmpty(outputScanLog.getAdd_fail_reson()));
					cell3.setCellStyle(outputScanLog.getAdd_pallet_success() == 1 ? style2 : style1);
					
					// 扫描人员
					XSSFCell cell4 = row.createCell(4);
					cell4.setCellValue(StringUtils.trimToEmpty(outputScanLog.getScan_user_name()));
					cell4.setCellStyle(outputScanLog.getAdd_pallet_success() == 1 ? style2 : style1);

					// 扫描次数
					XSSFCell cell5 = row.createCell(5);
					cell5.setCellValue(outputScanLog.getScan_count());
					cell5.setCellStyle(outputScanLog.getAdd_pallet_success() == 1 ? style2 : style1);
				}
			}
			OutputStream oStream = response.getOutputStream();
			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
		} catch (IOException e)
		{
			e.printStackTrace();

		}
	}
}
