package com.xiangrui.lmp.business.admin.scanLog.vo;

import java.io.Serializable;

public class InputScanLogDateCountBean implements Serializable
{
	private static final long serialVersionUID = -3676057106450977259L;
	private String days;
	private int day_count;

	public String getDays()
	{
		return days;
	}

	public void setDays(String days)
	{
		this.days = days;
	}

	public int getDay_count()
	{
		return day_count;
	}

	public void setDay_count(int day_count)
	{
		this.day_count = day_count;
	}

}
