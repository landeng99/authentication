package com.xiangrui.lmp.business.admin.scanLog.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.scanLog.vo.InputScanLog;
import com.xiangrui.lmp.business.admin.scanLog.vo.InputScanLogDateCountBean;

public interface InputScanLogMapper
{
	List<InputScanLog> queryInputScanLog(Map<String, Object> paremt);

	int countCurrentDateInputScanLogByPacakgeId(Map<String, Object> paremt);

	int countCurrentDateInputScanLogByPkgNo(Map<String, Object> paremt);
	
	void insertInputScanLog(InputScanLog inputScanLog);

	List<InputScanLogDateCountBean> queryDateCount(Map<String, Object> paremt);
}
