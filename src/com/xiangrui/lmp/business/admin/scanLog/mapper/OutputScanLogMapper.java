package com.xiangrui.lmp.business.admin.scanLog.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.scanLog.vo.OutputScanLog;

public interface OutputScanLogMapper
{
	List<OutputScanLog> queryOutputScanLog(Map<String, Object> paremt);

	int countCurrentDateOutputScanLogByPacakgeId(Map<String, Object> paremt);
	
	int countCurrentDateOutputScanLogByPkgNo(Map<String, Object> paremt);

	void insertOutputScanLog(OutputScanLog outputScanLog);
}
