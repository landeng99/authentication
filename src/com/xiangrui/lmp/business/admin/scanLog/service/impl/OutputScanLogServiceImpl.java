package com.xiangrui.lmp.business.admin.scanLog.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.scanLog.mapper.OutputScanLogMapper;
import com.xiangrui.lmp.business.admin.scanLog.service.OutputScanLogService;
import com.xiangrui.lmp.business.admin.scanLog.vo.OutputScanLog;
import com.xiangrui.lmp.util.DateUtil;

@Service("outputScanLogService")
public class OutputScanLogServiceImpl implements OutputScanLogService
{
	@Autowired
	private OutputScanLogMapper outputScanLogMapper;

	@Override
	public List<OutputScanLog> queryOutputScanLog(Map<String, Object> paremt)
	{
		return outputScanLogMapper.queryOutputScanLog(paremt);
	}

	@Override
	public int countCurrentDateOutputScanLogByPacakgeId(int pkg_id,String pick_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pick_id", pick_id);
		params.put("pkg_id", pkg_id);
		return outputScanLogMapper.countCurrentDateOutputScanLogByPacakgeId(params);
	}
	
	@Override
	public int countCurrentDateOutputScanLogByPkgNo(String pkg_no,String pick_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pick_id", pick_id);
		params.put("pkg_no", pkg_no);
		return outputScanLogMapper.countCurrentDateOutputScanLogByPkgNo(params);
	}

	@Override
	public void insertOutputScanLog(OutputScanLog outputScanLog)
	{
		outputScanLogMapper.insertOutputScanLog(outputScanLog);

	}

}
