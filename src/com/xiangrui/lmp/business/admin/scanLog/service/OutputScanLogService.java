package com.xiangrui.lmp.business.admin.scanLog.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.scanLog.vo.OutputScanLog;

public interface OutputScanLogService
{
	List<OutputScanLog> queryOutputScanLog(Map<String, Object> paremt);

	int countCurrentDateOutputScanLogByPacakgeId(int pkg_id,String pick_id);
	int countCurrentDateOutputScanLogByPkgNo(String pkg_no,String pick_id);
	void insertOutputScanLog(OutputScanLog outputScanLog);
}
