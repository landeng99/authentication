package com.xiangrui.lmp.business.admin.scanLog.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.scanLog.vo.InputScanLog;
import com.xiangrui.lmp.business.admin.scanLog.vo.InputScanLogDateCountBean;

public interface InputScanLogService
{
	List<InputScanLog> queryInputScanLog(Map<String, Object> paremt);

	int countCurrentDateInputScanLogByPacakgeId(int pkg_id);
	
	int countCurrentDateInputScanLogByPkgNo(String pkg_no);

	void insertInputScanLog(InputScanLog inputScanLog);

	List<InputScanLogDateCountBean> queryDateCount(Map<String, Object> paremt);
}
