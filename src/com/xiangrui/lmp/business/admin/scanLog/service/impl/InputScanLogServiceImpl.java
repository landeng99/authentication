package com.xiangrui.lmp.business.admin.scanLog.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.scanLog.mapper.InputScanLogMapper;
import com.xiangrui.lmp.business.admin.scanLog.service.InputScanLogService;
import com.xiangrui.lmp.business.admin.scanLog.vo.InputScanLog;
import com.xiangrui.lmp.business.admin.scanLog.vo.InputScanLogDateCountBean;
import com.xiangrui.lmp.util.DateUtil;
@Service("inputScanLogService")
public class InputScanLogServiceImpl implements InputScanLogService
{
    @Autowired
    private InputScanLogMapper inputScanLogMapper;
	@Override
	public List<InputScanLog> queryInputScanLog(Map<String, Object> paremt)
	{
		return inputScanLogMapper.queryInputScanLog(paremt);
	}

	@Override
	public int countCurrentDateInputScanLogByPacakgeId(int pkg_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("scan_date", DateUtil.getCurrentDate());
		params.put("pkg_id", pkg_id);
		return inputScanLogMapper.countCurrentDateInputScanLogByPacakgeId(params);
	}

	public int countCurrentDateInputScanLogByPkgNo(String pkg_no)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("scan_date", DateUtil.getCurrentDate());
		params.put("pkg_no", pkg_no);
		return inputScanLogMapper.countCurrentDateInputScanLogByPkgNo(params);
	}
	@Override
	public void insertInputScanLog(InputScanLog inputScanLog)
	{
		inputScanLogMapper.insertInputScanLog(inputScanLog);
	}

	@Override
	public List<InputScanLogDateCountBean> queryDateCount(Map<String, Object> paremt)
	{
		return inputScanLogMapper.queryDateCount(paremt);
	}

}
