package com.xiangrui.lmp.business.admin.frontuser.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.frontuser.mapper.ReceiveAddressMapper;
import com.xiangrui.lmp.business.admin.frontuser.service.ReceiveAddressService;
import com.xiangrui.lmp.business.admin.frontuser.vo.FrontUserAddress;
import com.xiangrui.lmp.business.admin.frontuser.vo.ReceiveAddress;

@Service(value = "receiveAddressService")
public class ReceiveAddressServiceImpl implements ReceiveAddressService
{

    @Autowired
    private ReceiveAddressMapper receiveAddressMapper;

    /**
     * 通过地址ID查询地址
     * 
     * @param address_id
     * @return ReceiveAddress
     */
    @Override
    public ReceiveAddress queryAddressById(int address_id)
    {
        return receiveAddressMapper.queryAddressById(address_id);
    }

    /**
     * 更新身份证审核状态址
     * 
     * @param receiveAddress
     * @return
     */
    @Override
    public void updateIdcardStatus(ReceiveAddress receiveAddress)
    {

        receiveAddressMapper.updateIdcardStatus(receiveAddress);
    }

    /**
     * 分页查询用户地址
     * 
     * @param params
     * @return
     */
    @Override
    public List<FrontUserAddress> queryReceiveAddress(Map<String, Object> params)
    {

        return receiveAddressMapper.queryReceiveAddress(params);
    }

}
