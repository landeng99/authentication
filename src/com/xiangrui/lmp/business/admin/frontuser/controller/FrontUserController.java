package com.xiangrui.lmp.business.admin.frontuser.controller;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.frontuser.service.ReceiveAddressService;
import com.xiangrui.lmp.business.admin.frontuser.vo.FrontUserAddress;
import com.xiangrui.lmp.business.admin.frontuser.vo.ReceiveAddress;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.quotationManage.service.QuotationManageService;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/frontUser")
public class FrontUserController {
	@Autowired
	private OverseasAddressService overseasAddressService;
	@Autowired
	private QuotationManageService quotationManageService;
	@Autowired
	private FrontUserService frontUserService;

	@Autowired
	private ReceiveAddressService receiveAddressService;

	@Autowired
	private PackageService packageService;
	/**
	 * 优惠券处理服务
	 */
	@Autowired
	private CouponService couponService;
	
	/**
	 * 用户类型2：同行用户
	 */
	private static final int USER_TYPE_PARTNER = 2;

	/**
	 * 会员1：不存在
	 */
	private static final String FRONT_USER_NOTEXIST = "1";
	/**
	 * 会员0：存在
	 */
	private static final String FRONT_USER_EXIST = "0";

	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

	/**
	 * 会员审核列表初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/frontUserAddressInit")
	public String frontUserVerifyInit(HttpServletRequest request) {
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);

		// 会员查询
		List<FrontUserAddress> frontUserAddressList = receiveAddressService.queryReceiveAddress(params);

		for (FrontUserAddress frontUserAddress : frontUserAddressList) {
			// 解析省市县
			String address = addressInfo(frontUserAddress.getRegion());
			frontUserAddress.setStreet(address + frontUserAddress.getStreet());

		}
		request.setAttribute("frontUserAddressList", frontUserAddressList);
		request.setAttribute("pageView", pageView);

		return "back/frontUserAddressList";
	}

	/**
	 * 会员审核查询
	 * 
	 * @param request
	 * @param user_type
	 * @param idcard_status
	 * @param account
	 * @param fromDate
	 * @param toDate
	 * @param user_name
	 * @param mobile
	 * @param email
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/frontUserAddressSearch")
	public String frontUserVerifySearch(HttpServletRequest request, String user_type, String idcard_status,
			String account, String fromDate, String toDate, String user_name, String mobile, String email,
			String idcard) {
		// 分页查询idcard
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			pageView = new PageView(1);

			// 用户类型
			try {
				user_type = new String(user_type.getBytes("ISO-8859-1"), "UTF-8");
				params.put("user_type", user_type);

				// 身份状态
				idcard_status = new String(idcard_status.getBytes("ISO-8859-1"), "UTF-8");
				params.put("idcard_status", idcard_status);

				// 用户账号
				account = new String(account.getBytes("ISO-8859-1"), "UTF-8");
				params.put("account", account);

				// 注册时间范围
				params.put("fromDate", fromDate);
				params.put("toDate", toDate);
				// 用户姓名

				user_name = new String(user_name.getBytes("ISO-8859-1"), "UTF-8");
				params.put("user_name", user_name);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			// 手机号码
			params.put("mobile", mobile);
			// 用户邮箱
			params.put("email", email);

			// 身份证号
			params.put("idcard", idcard);

			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}

		params.put("pageView", pageView);

		// 会员查询
		List<FrontUserAddress> frontUserAddressList = receiveAddressService.queryReceiveAddress(params);

		for (FrontUserAddress frontUserAddress : frontUserAddressList) {
			// 解析省市县
			String address = addressInfo(frontUserAddress.getRegion());
			frontUserAddress.setStreet(address + frontUserAddress.getStreet());

		}
		request.setAttribute("frontUserAddressList", frontUserAddressList);
		request.setAttribute("pageView", pageView);

		// 查询条件保持
		request.setAttribute("params", params);

		return "back/frontUserAddressList";
	}

	/**
	 * 会员详情初始化
	 * 
	 * @param request
	 * @param user_id
	 * @param address_id
	 * @return
	 */
	@RequestMapping("/frontUserAddressDetail")
	public String frontUserAddressDetail(HttpServletRequest request, String user_id, String address_id) {
		// 直客账号信息
		FrontUser frontUser = frontUserService.queryFrontUserById(Integer.parseInt(user_id));

		// 身份证信息取得
		ReceiveAddress receiveAddress = receiveAddressService.queryAddressById(Integer.parseInt(address_id));
		// 解析省市县
		String address = addressInfo(receiveAddress.getRegion());

		request.setAttribute("frontUser", frontUser);
		request.setAttribute("receiveAddress", receiveAddress);
		request.setAttribute("address", address + receiveAddress.getStreet());

		return "back/frontUserAddressDetail";
	}

	/**
	 * 会员审核初始化
	 * 
	 * @param request
	 * @param user_id
	 * @param address_id
	 * @return
	 */
	@RequestMapping("/frontUserAddressUpdate")
	public String frontUserAddressUpdate(HttpServletRequest request, String user_id, String address_id) {
		// 直客账号信息
		FrontUser frontUser = frontUserService.queryFrontUserById(Integer.parseInt(user_id));
		request.setAttribute("frontUser", frontUser);

		// 身份证信息取得
		ReceiveAddress receiveAddress = receiveAddressService.queryAddressById(Integer.parseInt(address_id));
		// 解析省市县
		String address = addressInfo(receiveAddress.getRegion());

		request.setAttribute("frontUser", frontUser);
		request.setAttribute("receiveAddress", receiveAddress);
		request.setAttribute("address", address + receiveAddress.getStreet());

		return "back/updateFrontUserAddress";
	}

	/**
	 * 会员审核结果保存到数据库
	 * 
	 * @param request
	 * @param receiveAddress
	 * @return
	 */
	@RequestMapping("/frontUserVerifyUpdate")
	@ResponseBody
	public String frontUserVerifyUpdate(HttpServletRequest request, ReceiveAddress receiveAddress) {

		receiveAddressService.updateIdcardStatus(receiveAddress);

		return null;
	}

	/**
	 * 会员信息列表初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/frontUserInit")
	public String frontUserInit(HttpServletRequest request) {
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);

		// 会员查询
		List<FrontUser> frontUserList = frontUserService.queryFrontUser(params);

		request.setAttribute("frontUserList", frontUserList);
		request.setAttribute("pageView", pageView);

		return "back/frontUserList";
	}

	/**
	 * 会员信息列表查询
	 * 
	 * @param request
	 * @param user_type
	 * @param status
	 * @param account
	 * @param fromDate
	 * @param toDate
	 * @param user_name
	 * @param mobile
	 * @param email
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/frontUserSearch")
	public String frontUserSearch(HttpServletRequest request, String user_type, String status, String account,
			String fromDate, String toDate, String user_name, String mobile, String email, String last_name,
			String business_name) {
		// 分页查询
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else {
			pageView = new PageView(1);

			// 用户类型
			params.put("user_type", user_type);
			// 账号状态
			params.put("status", status);
			// 用户账号
			params.put("account", account);
			// 注册时间范围
			params.put("fromDate", fromDate);
			params.put("toDate", toDate);
			// 用户姓名
			params.put("user_name", user_name);
			// 手机号码
			params.put("mobile", mobile);
			// 用户邮箱
			params.put("email", email);
			// 用户邮箱
			params.put("last_name", last_name.trim());

			// 业务名
			params.put("business_name", business_name);

			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}
		params.put("pageView", pageView);
		// 会员查询
		List<FrontUser> frontUserList = frontUserService.queryFrontUser(params);

		request.setAttribute("frontUserList", frontUserList);
		request.setAttribute("pageView", pageView);

		// 查询条件保持
		request.setAttribute("params", params);

		return "back/frontUserList";
	}

	/**
	 * 同行会员添加初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/frontUserAdd")
	public String frontUserAdd(HttpServletRequest request) {

		return "back/addPartnerFrontUser";
	}

	/**
	 * 同行会员账号重复检查
	 * 
	 * @param request
	 * @param account
	 * @return
	 */
	@RequestMapping("/checkPartnerAccount")
	@ResponseBody
	public String checkPartnerAccount(HttpServletRequest request, String account) {
		String checkResult = FRONT_USER_EXIST;

		Map<String, Object> params = new HashMap<String, Object>();
		// 用户账号
		params.put("account", account);

		List<FrontUser> frontUserList = frontUserService.queryFrontUser(params);

		if (frontUserList == null || frontUserList.size() == 0) {
			checkResult = FRONT_USER_NOTEXIST;
		}

		return checkResult;
	}

	/**
	 * 同行会员名称重复检查
	 * 
	 * @param request
	 * @param account
	 * @return
	 */
	@RequestMapping("/checkPartnerUserName")
	@ResponseBody
	public String checkPartnerUserName(HttpServletRequest request, String user_name) {
		String checkResult = FRONT_USER_EXIST;

		Map<String, Object> params = new HashMap<String, Object>();
		// 用户名称
		params.put("user_name", user_name);

		List<FrontUser> frontUserList = frontUserService.queryFrontUser(params);

		if (frontUserList == null || frontUserList.size() == 0) {
			checkResult = FRONT_USER_NOTEXIST;
		}

		return checkResult;
	}

	/**
	 * 手机号称重复检查
	 * 
	 * @param request
	 * @param mobile
	 * @return
	 */
	@RequestMapping("/checkPartnerMobile")
	@ResponseBody
	public String checkPartnerMobile(HttpServletRequest request, String mobile) {
		String checkResult = FRONT_USER_EXIST;

		Map<String, Object> params = new HashMap<String, Object>();
		// 手机号
		params.put("mobile", mobile);

		List<FrontUser> frontUserList = frontUserService.queryFrontUser(params);

		if (frontUserList == null || frontUserList.size() == 0) {
			checkResult = FRONT_USER_NOTEXIST;
		}

		return checkResult;
	}

	/**
	 * 同行会员保存到数据库
	 * @param request
	 * @param frontUser
	 * @return
	 */
	@RequestMapping("/partnerFrontUserInsert")
	@ResponseBody
	public Map<String, Object> partnerFrontUserInsert(HttpServletRequest request, FrontUser frontUser) {
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		try {
			frontUser.setRegister_time(new Timestamp(System.currentTimeMillis()));
			// 账户和邮箱一致
			frontUser.setEmail(frontUser.getAccount());
			frontUser.setUser_type(USER_TYPE_PARTNER);
			frontUser.setIntegral(0);
			frontUserService.insertFrontUser(frontUser);

			// 送注册优惠券
			frontUser = frontUserService.queryFrontUserByName(frontUser.getUser_name());
			if (frontUser != null) {
				couponService.pushRegistCoupon(frontUser.getUser_id());
			}

			//二，PC端-同行客户，用户注册成功后，需要在报价单关联表里面，添加该用户适用的系统已经存在的所有报价单。
			// 获取海外仓库地址id
			Map<String, Object> params = new HashMap<String, Object>();
			List<OverseasAddress> overseasAddressList = overseasAddressService.queryAllOverseasAddress(params);
			for (OverseasAddress overseasAddress : overseasAddressList) {
				params.clear();
				params.put("user_id", frontUser.getUser_id());
				params.put("user_type", USER_TYPE_PARTNER);
				params.put("osaddr_id", overseasAddress.getId());
				params.put("account_type", 0);
				params.put("is_able", 1);// 默认报价单是否可用：1可用

				for (int i = 0; i < 6; i++) {
					params.put("express_package", i);
					QuotationManage qm = quotationManageService.getQuotaByPkgInfo2(overseasAddress.getId(), i,
							USER_TYPE_PARTNER, frontUser.getUser_id());
					if (qm != null) {
						params.put("quota_id", qm.getQuota_id());
						params.put("create_time", new Date());
						quotationManageService.insertAuthQuota(params);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			rtnMap.put("result", false);
			return rtnMap;

		}
		rtnMap.put("result", true);
		return rtnMap;
	}

	/**
	 * 会员详情初始化
	 * 
	 * @param request
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/frontUser")
	public String frontUser(HttpServletRequest request, String user_id) {
		// 账号信息
		FrontUser frontUser = frontUserService.queryFrontUserById(Integer.parseInt(user_id));

		// 包裹信息
		List<Pkg> pkgList = packageService.queryPackageAddressByUserId(Integer.parseInt(user_id));

		request.setAttribute("frontUser", frontUser);
		request.setAttribute("pkgList", pkgList);

		return "back/frontUserDetail";
	}

	/**
	 * 会员审核初始化
	 * 
	 * @param request
	 * @param user_id
	 * @return
	 */
	@RequestMapping("/frontUserUpdate")
	public String frontUserUpdate(HttpServletRequest request, String user_id) {
		// 账号信息
		FrontUser frontUser = frontUserService.queryFrontUserById(Integer.parseInt(user_id));

		// 包裹信息
		List<Pkg> pkgList = packageService.queryPackageAddressByUserId(Integer.parseInt(user_id));

		request.setAttribute("frontUser", frontUser);
		request.setAttribute("pkgList", pkgList);

		return "back/updateFrontUser";
	}

	/**
	 * 会员禁用或解禁
	 * 
	 * @param request
	 * @param frontUser
	 * @return
	 */
	@RequestMapping("/frontUserStatus")
	@ResponseBody
	public String frontUserStatus(HttpServletRequest request, FrontUser frontUser) {
		frontUserService.updatefrontUserStatus(frontUser);

		return null;
	}

	/**
	 * 会员冻结或解冻
	 * 
	 * @param request
	 * @param frontUser
	 * @return
	 */
	@RequestMapping("/frontUserIsFreeze")
	@ResponseBody
	public String frontUserIsFreeze(HttpServletRequest request, FrontUser frontUser) {
		frontUserService.updatefrontUserIsFreeze(frontUser);

		return null;
	}

	/**
	 * 会员逻辑删除
	 * 
	 * @param request
	 * @param user_id
	 * @param status
	 * @return
	 */
	@RequestMapping("/deleteFrontUser")
	public String deleteFrontUser(HttpServletRequest request, String user_id, String status) {
		FrontUser frontUser = new FrontUser();
		// 用户ID
		frontUser.setUser_id(Integer.parseInt(user_id));
		// 用户状态
		frontUser.setStatus(Integer.parseInt(status));
		// 逻辑删除
		frontUserService.updatefrontUserStatus(frontUser);

		return "back/frontUserDetail";
	}

	/**
	 * 解析省市县
	 * 
	 * @param request
	 * @param jsonString
	 * @return
	 */
	private String addressInfo(String jsonString) {
		if (StringUtil.isEmpty(jsonString)) {
			return "";
		}
		List<String> aList = new ArrayList<String>();
		try {
			aList = JSONUtil.readValueFromJson(jsonString, "name");
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		StringBuffer address = new StringBuffer("");
		for (String city : aList) {
			address.append(city);
		}

		return address.toString().replaceAll("\"", "");
	}

	/**
	 * 根据Account来查看FrontUser
	 */
	@RequestMapping("/queryFrontUserLikeByAccount")
	@ResponseBody
	public Map<String, Object> queryFrontUserLikeByAccount(HttpServletRequest req, String account) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("account", account);
		List<FrontUser> frontUserList = frontUserService.queryFrontUserLikeByAccount(params);
		result.put("frontUserList", frontUserList);
		return result;
	}

	/**
	 * 根据用户类型来查看FrontUser
	 */
	@RequestMapping("/queryFrontUserLikeByAccountType")
	@ResponseBody
	public Map<String, Object> queryFrontUserLikeByAccountType(HttpServletRequest req, String account,
			String user_type) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("account", account);
		params.put("user_type", user_type);
		List<FrontUser> frontUserList = frontUserService.queryFrontUserLikeByAccountType(params);
		result.put("frontUserList", frontUserList);
		return result;
	}

	/**
	 * 修改用户的业务名和快件渠道
	 * 
	 * @param request
	 * @param frontUser
	 * @return
	 */
	@RequestMapping("/updateFrontUserBussinessNameAndSelectExpressChannel")
	@ResponseBody
	public String updateFrontUserBussinessNameAndSelectExpressChannel(HttpServletRequest request, FrontUser frontUser) {
		frontUserService.updateFrontUserBussinessNameAndSelectExpressChannel(frontUser);
		return null;
	}
	
	@RequestMapping("/getFrontUserByAccountAndMobile")
    @ResponseBody
    public List<FrontUser> getFrontUserByAccountAndMobile(HttpServletRequest request, String account, String mobile) {
	    return frontUserService.queryFrontUserByAccountAndMobie(account,mobile);
    }
	
	/**
	 * 检查用户账号与手机号是否存在
	 * @param request
	 * @param login_account
	 * @param phone_num
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	@RequestMapping("/checkAccountAndPhone")
    @ResponseBody
    public Map<String, Object> checkAccountAndPhone(HttpServletRequest request, String login_account, String phone_num) {
	    Map<String, Object> rtnMap = new HashMap<String, Object>();
	    List<FrontUser> fList = frontUserService.queryFrontUserByAccountAndMobie(login_account, phone_num);
        if( null==fList || fList.size()==0 ){
            rtnMap.put("result", false);
        }
        else{
            rtnMap.put("result", true);
            FrontUser fu = fList.get(0);	// 前面接口会取过一次保证一定有值
            if( StringUtils.isEmpty( fu.getUser_name() ) )
            	fu.setUser_name(fu.getAccount());
            rtnMap.put("frontUser", fu);
        }
            
        return rtnMap;
	}
}
