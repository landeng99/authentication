package com.xiangrui.lmp.business.admin.frontuser.vo;

import com.xiangrui.lmp.business.base.BaseFrontUser;

public class FrontUserAddress extends BaseFrontUser
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 地址id
     */
    private int address_id;

    /**
     * 城市
     */
    private String region;

    /**
     * 地址
     */
    private String street;

    /**
     * 身份证审核状态
     */
    private int idcard_status;

    /**
     * 身份证号
     */
    private String idcard;
    
    public String getIdcard()
    {
        return idcard;
    }

    public void setIdcard(String idcard)
    {
        this.idcard = idcard;
    }

    public int getAddress_id()
    {
        return address_id;
    }

    public void setAddress_id(int address_id)
    {
        this.address_id = address_id;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public int getIdcard_status()
    {
        return idcard_status;
    }

    public void setIdcard_status(int idcard_status)
    {
        this.idcard_status = idcard_status;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }
}
