package com.xiangrui.lmp.business.admin.frontuser.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.frontuser.vo.FrontUserAddress;
import com.xiangrui.lmp.business.admin.frontuser.vo.ReceiveAddress;

public interface ReceiveAddressMapper
{

    /**
     * 通过地址ID查询地址
     * 
     * @param address_id
     * @return
     */
    ReceiveAddress queryAddressById(int address_id);

    /**
     * 更新身份证审核状态
     * 
     * @param receiveAddress
     * @return
     */
    void updateIdcardStatus(ReceiveAddress receiveAddress);

    /**
     * 分页查询用户地址
     * 
     * @param params
     * @return
     */
    List<FrontUserAddress> queryReceiveAddress(Map<String, Object> params);

}
