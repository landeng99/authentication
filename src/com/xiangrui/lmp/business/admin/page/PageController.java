package com.xiangrui.lmp.business.admin.page;

import java.util.Map;
import java.util.Stack;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xiangrui.lmp.business.admin.breadcrumb.vo.Breadcrumb;

@Controller
@RequestMapping("/admin/page")
public class PageController
{

    @RequestMapping("/go")
    public ModelAndView go(HttpServletRequest req)
    {
        Stack<Breadcrumb> breadcrumbStack = (Stack<Breadcrumb>) req
                .getSession().getAttribute("breadcrumbStack");

        Breadcrumb breadcrumb = null;
        String pageIndex=req.getParameter("pageIndex");
        breadcrumb = breadcrumbStack.peek();

        ModelMap mlMap = new ModelMap();
        
        Map<String, Object> params = breadcrumb.getParams();
        req.getParameter("pageIndex");
        if (params != null)
        {
            for (Entry<String, Object> entry : params.entrySet())
            {
                mlMap.put(entry.getKey(), entry.getValue());
            }
        }
        //分页页码,如果参数中有分页页码，则此时会覆盖掉上层的分页页码
        mlMap.put("pageIndex", pageIndex);
        return new ModelAndView("redirect:" + breadcrumb.getUrl(), mlMap);
    }
 
}
