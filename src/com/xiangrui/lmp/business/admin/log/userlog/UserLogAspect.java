package com.xiangrui.lmp.business.admin.log.userlog;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.admin.userlog.service.UserLogService;
import com.xiangrui.lmp.business.admin.userlog.vo.UserLog;
import com.xiangrui.lmp.util.ParseAnnotationSystemLog;
import com.xiangrui.lmp.util.UserLogAspectUtil;
import com.xiangrui.lmp.util.content.SysContent;

/**
 * 日志处理.跟踪
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-23 上午10:53:36
 * </p>
 */
@Component
@Aspect
public class UserLogAspect
{

    /**
     * 日志保存service
     */
    @Autowired
    private UserLogService logService;

    /**
     * 添加业务逻辑方法切入点 add开头的
     */
    @Pointcut("execution(* com.xiangrui.lmp.business.admin.*.service.impl.*.add*(..))")
    public void addServiceCall()
    {
    }

    /**
     * 添加业务逻辑方法切入点 save开头的
     */
    @Pointcut("execution(* com.xiangrui.lmp.business.admin.*.service.impl.*.save*(..))")
    public void saveServiceCall()
    {
    }

    /**
     * 添加切入点 insert开头的
     */
    @Pointcut("execution(* com.xiangrui.lmp.business.admin.*.service.impl.*.insert*(..))")
    public void insertServiceCall()
    {
    }

    /**
     * 修改业务逻辑方法切入点
     */
    @Pointcut("execution(* com.xiangrui.lmp.business.admin.*.service.impl.*.upd*(..))")
    public void updateServiceCall()
    {
    }

    /**
     * 删除业务逻辑方法切入点
     */
    @Pointcut("execution(* com.xiangrui.lmp.business.admin.*.service.impl.*.del*(..))")
    public void deleteServiceCall()
    {
    }

    /**
     * 日记记录删除
     * 
     * @param joinPoint
     * @param rtv
     * @throws Throwable
     */
    @AfterReturning(value = "deleteServiceCall()", argNames = "rtv", returning = "rtv")
    public void deleteServiceCallCalls(JoinPoint joinPoint, Object rtv)
            throws Throwable
    {
        updateServiceCallCalls(joinPoint, rtv);
    }

    /**
     * 日记记录添加
     * 
     * @param joinPoint
     * @param rtv
     * @throws Throwable
     */
    @AfterReturning(value = "addServiceCall()", argNames = "rtv", returning = "rtv")
    public void addServiceCallCalls(JoinPoint joinPoint, Object rtv)
            throws Throwable
    {
        updateServiceCallCalls(joinPoint, rtv);
    }

    /**
     * 日记记录新增
     * 
     * @param joinPoint
     * @param rtv
     * @throws Throwable
     */
    @AfterReturning(value = "insertServiceCall()", argNames = "rtv", returning = "rtv")
    public void insertServiceCallCalls(JoinPoint joinPoint, Object rtv)
            throws Throwable
    {
        updateServiceCallCalls(joinPoint, rtv);
    }

    /**
     * 日记记录保存
     * 
     * @param joinPoint
     * @param rtv
     * @throws Throwable
     */
    @AfterReturning(value = "saveServiceCall()", argNames = "rtv", returning = "rtv")
    public void saveServiceCallCalls(JoinPoint joinPoint, Object rtv)
            throws Throwable
    {
        updateServiceCallCalls(joinPoint, rtv);
    }

    /**
     * 日记记录修改
     * 
     * @param joinPoint
     * @param rtv
     * @throws Throwable
     */
    @AfterReturning(value = "updateServiceCall()", argNames = "rtv", returning = "rtv")
    public void updateServiceCallCalls(JoinPoint joinPoint, Object rtv)
            throws Throwable
    {
        // SysContent.getRequest()获取登录管理员id
        User back_user = (User) SysContent.getRequest().getSession()
                .getAttribute("back_user");
        
        // 没有用户
        if (null == back_user)
        {
            return;
        }
        int userId = back_user.getUser_id();
       
        // 判断参数
        if (joinPoint.getArgs() == null)
        {
            return;
        }
        
        // 创建日志对象
        UserLog log = new UserLog();
        
        // 获取操作内容
        // 操作对象 的 连接点对象 获取其的方法集合
        Method[] methods = joinPoint.getTarget().getClass().getMethods();
        
        // 操作对象的 方法
        String methodName = joinPoint.getSignature().getName();
        Method method = null;
        for (Method md : methods)
        {
            if (md.getName() == methodName)
            {
                method = md;
                break;
            }
        }
        
        // 获取日志介绍内容,读取的注释文字
        String contentDesc = ParseAnnotationSystemLog
                .parseMethodSystemServiceLogInDescription(method);
        
        // 注释必须要有内容
        if (null != contentDesc && !"".equals(contentDesc))
        {

            log.setContent_desc(contentDesc);
            
            // 获取ip地址
            String ipAddress=UserLogAspectUtil.optionIpValues();
            if(ipAddress!=null&&ipAddress.contains(","))
            {
            	ipAddress=StringUtils.substringAfterLast(ipAddress, ",");
            }
            log.setIp_values(ipAddress);
            
            // 获取日期
            log.setCreate_time(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    .format(new Date()));
            log.setUser_id(userId);
            
            // 添加日志
            logService.log(log);
        }
    }

}
