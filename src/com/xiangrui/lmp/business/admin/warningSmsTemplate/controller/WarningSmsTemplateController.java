package com.xiangrui.lmp.business.admin.warningSmsTemplate.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.sysSetting.service.SysSettingService;
import com.xiangrui.lmp.business.admin.sysSetting.vo.SysSetting;
import com.xiangrui.lmp.business.admin.warningEmailTemplate.vo.WarningEmailTemplate;
import com.xiangrui.lmp.business.admin.warningSmsTemplate.service.WarningSmsTemplateService;
import com.xiangrui.lmp.business.admin.warningSmsTemplate.vo.WarningSmsTemplate;
import com.xiangrui.lmp.util.PageView;

@Controller
@RequestMapping("/admin/warningSmsTemplate")
public class WarningSmsTemplateController
{
	@Autowired
	private WarningSmsTemplateService warningSmsTemplateService;
	@Autowired
	private SysSettingService sysSettingService;

	/**
	 * 短信模板列表初始化
	 * 
	 * @return
	 */
	@RequestMapping("/queryAllOne")
	public String queryAll(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);

		List<WarningSmsTemplate> warningSmsTemplateList = warningSmsTemplateService.queryWarningSmsTemplate(params);
		request.setAttribute("pageView", pageView);
		request.setAttribute("warningSmsTemplateList", warningSmsTemplateList);
		SysSetting smsSysSetting = sysSettingService.querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_SMS);
		if (smsSysSetting != null)
		{
			request.setAttribute("enableSms", smsSysSetting.getSys_setting_value());
		}
		SysSetting smsTimeRangeSysSetting = sysSettingService.querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_SMS_TIME_RANGE);
		if (smsTimeRangeSysSetting != null)
		{
			request.setAttribute("smsTimeRange", smsTimeRangeSysSetting.getSys_setting_value());
		}
		return "back/warningSmsTemplateList";
	}

	/**
	 * 编辑短信模板初始化
	 * 
	 * @return
	 */
	@RequestMapping("/editInit")
	public String editInit(HttpServletRequest request, int seq_id)
	{
		WarningSmsTemplate warningSmsTemplate = warningSmsTemplateService.queryWarningSmsTemplateBySeqId(seq_id);
		if (warningSmsTemplate != null)
		{
			request.setAttribute("warningSmsTemplate", warningSmsTemplate);
		}
		request.setAttribute("seq_id", seq_id);
		return "back/warningSmsTemplateEdit";
	}

	/**
	 * 保存新建或编辑短信模板
	 * 
	 * @return
	 */
	@RequestMapping("/saveOrupdate")
	public String saveOrupdate(HttpServletRequest request, WarningSmsTemplate warningSmsTemplate)
	{
		if (warningSmsTemplate.getSeq_id() == -1)
		{
			warningSmsTemplate.setCreate_time(new Date());
			warningSmsTemplateService.insertWarningSmsTemplate(warningSmsTemplate);
		}
		else
		{
			warningSmsTemplateService.updateWarningSmsTemplate(warningSmsTemplate);
		}
		return "back/warningSmsTemplateAdd_update_success";
	}

	/**
	 * 查看短信模板
	 * 
	 * @return
	 */
	@RequestMapping("/show")
	public String show(HttpServletRequest request, int seq_id)
	{
		WarningSmsTemplate warningSmsTemplate = warningSmsTemplateService.queryWarningSmsTemplateBySeqId(seq_id);
		if (warningSmsTemplate != null)
		{
			request.setAttribute("warningSmsTemplate", warningSmsTemplate);
		}
		return "back/warningSmsTemplateShow";
	}

	/**
	 * 是否允许发送短信设置
	 * 
	 * @param request
	 * @param enableSms
	 * @return
	 */
	@RequestMapping("/enableSmsHandle")
	@ResponseBody
	public Map<String, Object> enableSmsHandle(HttpServletRequest request, String enableSms)
	{
		SysSetting smsSysSetting = sysSettingService.querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_SMS);
		if (smsSysSetting != null)
		{
			smsSysSetting.setSys_setting_value(enableSms);
			sysSettingService.updateSysSetting(smsSysSetting);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("flag", "yes");
		return result;
	}

	/**
	 * 是否允许发送短信设置
	 * 
	 * @param request
	 * @param enableSms
	 * @return
	 */
	@RequestMapping("/setSmsTimeRange")
	@ResponseBody
	public Map<String, Object> setSmsTimeRange(HttpServletRequest request, String smsTimeRange)
	{
		SysSetting smsSysSetting = sysSettingService.querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_SMS_TIME_RANGE);
		if (smsSysSetting != null)
		{
			smsSysSetting.setSys_setting_value(smsTimeRange);
			sysSettingService.updateSysSetting(smsSysSetting);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("flag", "yes");
		return result;
	}

	/**
	 * 删除模板
	 * 
	 * @param request
	 * @param seq_id
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest request, int seq_id)
	{
		warningSmsTemplateService.deleteWarningSmsTemplate(seq_id);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("flag", "yes");
		return result;
	}
}
