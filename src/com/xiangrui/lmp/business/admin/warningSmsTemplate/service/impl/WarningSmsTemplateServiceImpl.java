package com.xiangrui.lmp.business.admin.warningSmsTemplate.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.warningEmailTemplate.vo.WarningEmailTemplate;
import com.xiangrui.lmp.business.admin.warningSmsTemplate.mapper.WarningSmsTemplateMapper;
import com.xiangrui.lmp.business.admin.warningSmsTemplate.service.WarningSmsTemplateService;
import com.xiangrui.lmp.business.admin.warningSmsTemplate.vo.WarningSmsTemplate;

@Service("warningSmsTemplateService")
public class WarningSmsTemplateServiceImpl implements WarningSmsTemplateService
{
	@Autowired
	WarningSmsTemplateMapper warningSmsTemplateMapper;

	@Override
	public List<WarningSmsTemplate> queryWarningSmsTemplate(Map<String, Object> params)
	{
		return warningSmsTemplateMapper.queryWarningSmsTemplate(params);
	}

	@Override
	public WarningSmsTemplate queryWarningSmsTemplateBySeqId(int seq_id)
	{
		return warningSmsTemplateMapper.queryWarningSmsTemplateBySeqId(seq_id);
	}

	@Override
	public void updateWarningSmsTemplate(WarningSmsTemplate warningSmsTemplate)
	{
		warningSmsTemplateMapper.updateWarningSmsTemplate(warningSmsTemplate);

	}

	@Override
	public void insertWarningSmsTemplate(WarningSmsTemplate warningSmsTemplate)
	{
		warningSmsTemplateMapper.insertWarningSmsTemplate(warningSmsTemplate);
	}

	@Override
	public WarningSmsTemplate queryEnableWarningSmsTemplate()
	{
		return warningSmsTemplateMapper.queryEnableWarningSmsTemplate();
	}

	@Override
	public void deleteWarningSmsTemplate(int seq_id)
	{
		warningSmsTemplateMapper.deleteWarningSmsTemplate(seq_id);
	}

}
