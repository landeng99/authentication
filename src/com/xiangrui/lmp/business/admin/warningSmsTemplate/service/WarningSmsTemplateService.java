package com.xiangrui.lmp.business.admin.warningSmsTemplate.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.warningEmailTemplate.vo.WarningEmailTemplate;
import com.xiangrui.lmp.business.admin.warningSmsTemplate.vo.WarningSmsTemplate;

public interface WarningSmsTemplateService
{
	List<WarningSmsTemplate> queryWarningSmsTemplate(Map<String, Object> params);

	WarningSmsTemplate queryWarningSmsTemplateBySeqId(int seq_id);

	void updateWarningSmsTemplate(WarningSmsTemplate warningSmsTemplate);

	void insertWarningSmsTemplate(WarningSmsTemplate warningSmsTemplate);
	
	WarningSmsTemplate queryEnableWarningSmsTemplate();
	
	void deleteWarningSmsTemplate(int seq_id);
}
