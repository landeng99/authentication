package com.xiangrui.lmp.business.admin.warningSmsTemplate.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.warningSmsTemplate.vo.WarningSmsTemplate;

public interface WarningSmsTemplateMapper
{
	List<WarningSmsTemplate> queryWarningSmsTemplate(Map<String, Object> params);

	WarningSmsTemplate queryWarningSmsTemplateBySeqId(int seq_id);

	void updateWarningSmsTemplate(WarningSmsTemplate warningSmsTemplate);

	void insertWarningSmsTemplate(WarningSmsTemplate warningSmsTemplate);
	
	WarningSmsTemplate queryEnableWarningSmsTemplate();
	
	void deleteWarningSmsTemplate(int seq_id);
}
