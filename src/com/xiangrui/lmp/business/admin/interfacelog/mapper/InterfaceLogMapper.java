package com.xiangrui.lmp.business.admin.interfacelog.mapper;

import com.xiangrui.lmp.business.admin.interfacelog.vo.InterfaceLog;

public interface InterfaceLogMapper {
	
	int insertInterfaceLog(InterfaceLog interfaceLog);

}
