package com.xiangrui.lmp.business.admin.interfacelog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.interfacelog.mapper.InterfaceLogMapper;
import com.xiangrui.lmp.business.admin.interfacelog.service.InterfaceLogService;
import com.xiangrui.lmp.business.admin.interfacelog.vo.InterfaceLog;


@Service("interfaceLogService")
public class InterfaceLogServicelmpl implements InterfaceLogService {

	@Autowired
	private InterfaceLogMapper interfaceLogMapper;
	
	@Override
	public int createInterfaceLog(InterfaceLog interfaceLog) {
		 
		return interfaceLogMapper.insertInterfaceLog(interfaceLog);
	}

}
