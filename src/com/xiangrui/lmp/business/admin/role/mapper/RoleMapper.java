package com.xiangrui.lmp.business.admin.role.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.role.vo.Role;

public interface RoleMapper {
	/**
	 * 分页查询所有角色
	 * @return
	 */
	List<Role> queryAll(Map<String,Object>params);
	/**
	 * 修改角色
	 * @param role
	 */
	void update(Role role);
	/**
	 * 删除角色
	 * @param id
	 */
	void delete(int id);
	/**
	 * 新增角色
	 * @param role
	 */
	int insert(Role role);
	/**
	 * 查询总记录数
	 * @param role
	 * @return
	 */
	int queryCount();
	/**
	 * 查询所有角色
	 * @return
	 */
	List<Role> queryRole();
	/**
	 * 添加时检测角色是否已存在
	 * @param rolename
	 * @return
	 */
	String checkRole(String rolename);
	/**
	 * 修改时检测角色是否已存在
	 * @param params
	 * @return
	 */
	List<String> checkUpdateRole(Map<String,Object> params);
	/**
	 * 获取所有的不可见角色信息
	 * @return
	 */
    List<Role> queryAllInVisible();
}
