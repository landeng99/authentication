package com.xiangrui.lmp.business.admin.role.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.role.vo.RoleMenu;

public interface RoleMenuMapper
{
    /**
     * 查询所有
     * 
     * @return
     */
    List<RoleMenu> queryAll();

    /**
     * 修改
     * 
     * @param rolemenu
     */
    void update(RoleMenu rolemenu);

    /**
     * 删除
     * 
     * @param id
     */
    void deleteByRoleId(int roleid);

    /**
     * 新增
     * 
     * @param rolemenu
     */
    void insert(RoleMenu rolemenu);

    /**
     * 根据roleid查询menuid
     * 
     * @return
     */
    List<RoleMenu> queryMenuByRoleId(int id);

    List<RoleMenu> queryRoleByMenuId(int id);

    RoleMenu queryRoleMenu(RoleMenu rm);

    void addRolemenuInList(List<RoleMenu> par);
}
