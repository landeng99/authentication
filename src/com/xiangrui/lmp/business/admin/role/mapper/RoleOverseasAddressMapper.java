package com.xiangrui.lmp.business.admin.role.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;

public interface RoleOverseasAddressMapper
{
    /**
     * 
     * 角色海外仓库信息
     * 
     * @param roleOverseasAddress
     * @return
     */
    List<RoleOverseasAddress> queryRoleOverseasAddressByRoleId(
            RoleOverseasAddress roleOverseasAddress);

    /**
     * 创建仓库权限
     * @param roleOverseasAddress
     */
    void insertRoleOverseasAddress(RoleOverseasAddress roleOverseasAddress);
    
    /**删除仓库权限
     * @param roleOverseasAddress
     */
    void delRoleOverseasAddress(RoleOverseasAddress roleOverseasAddress);
}
