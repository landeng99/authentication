package com.xiangrui.lmp.business.admin.role.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.role.vo.UserRole;

public interface UserRoleMapper {
	/**
	 * 查询用户角色
	 * @return
	 */
	List<UserRole> queryAll();
	/**
	 * 添加数据
	 * @param userRole
	 * @return
	 */
	int insert(UserRole userRole);
	/**
	 * 根据userid查询用户角色
	 * @param user_id
	 * @return
	 */
	List<UserRole> queryByUserId(int user_id);
	/**
	 * 根据用户id删除用户角色 
	 * @param user_id
	 * @return
	 */
	int deleteByUserId(int user_id);
	/**
	 * 根据角色id删除用户角色
	 * @param role_id
	 * @return
	 */
	int deleteByRoleId(int role_id);
    /**
     * 根据userid和roleid查询角色信息 单条角色
     * @param userRole
     *   必须包含user_id 和 role_id俩个信息
     * @return
     */
	List<UserRole> queryByUserIdAndRoleId(UserRole userRole);
}
