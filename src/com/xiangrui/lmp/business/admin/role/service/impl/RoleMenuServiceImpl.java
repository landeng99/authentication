package com.xiangrui.lmp.business.admin.role.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.role.mapper.RoleMenuMapper;
import com.xiangrui.lmp.business.admin.role.service.RoleMenuService;
import com.xiangrui.lmp.business.admin.role.vo.RoleMenu;

@Service("roleMenuService")
public class RoleMenuServiceImpl implements RoleMenuService
{

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @SystemServiceLog(description = "新增前台用户菜单")
    @Override
    public void insert(RoleMenu rolemenu)
    {
        roleMenuMapper.insert(rolemenu);

    }

    @SystemServiceLog(description = "修改前台用户信息")
    @Override
    public void update(RoleMenu rolemenu)
    {
        roleMenuMapper.update(rolemenu);

    }

    @Override
    public List<RoleMenu> queryMenuByRoleId(int id)
    {
        return roleMenuMapper.queryMenuByRoleId(id);
    }

    @SystemServiceLog(description = "删除前台用户信息")
    @Override
    public void deleteByRoleId(int roleid)
    {
        roleMenuMapper.deleteByRoleId(roleid);
    }

    @Override
    public List<RoleMenu> queryRoleByMenuId(int id)
    {
        return roleMenuMapper.queryRoleByMenuId(id);
    }

    @Override
    public RoleMenu queryRoleMenu(RoleMenu rm)
    {
        return roleMenuMapper.queryRoleMenu(rm);
    }

    @SystemServiceLog(description = "新增前台用户菜单")
    @Override
    public void addRolemenuInList(List<RoleMenu> par)
    {
        roleMenuMapper.addRolemenuInList(par);
    }
    
    @Override
    public List<RoleMenu> queryAllRoleMenu()
    {
         
        return roleMenuMapper.queryAll();
    }
    

}
