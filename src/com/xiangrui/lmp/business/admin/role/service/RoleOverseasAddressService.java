package com.xiangrui.lmp.business.admin.role.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;

public interface RoleOverseasAddressService
{
    /**
     * 
     * 角色海外仓库信息
     * @param roleOverseasAddress
     * @return
     */
    List<RoleOverseasAddress> queryRoleOverseasAddressByRoleId(RoleOverseasAddress roleOverseasAddress);
    
    /**
     * 创建角色仓库权限
     * @param roleOverseasAddressList
     */
    void insertRoleOverseasAddress(List<RoleOverseasAddress> roleOverseasAddressList,int roleId);
 
}
