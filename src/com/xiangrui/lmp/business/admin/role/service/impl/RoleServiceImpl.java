package com.xiangrui.lmp.business.admin.role.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.businessModel.mapper.RoleBusinessNameMapper;
import com.xiangrui.lmp.business.admin.role.mapper.RoleMapper;
import com.xiangrui.lmp.business.admin.role.mapper.RoleMenuMapper;
import com.xiangrui.lmp.business.admin.role.mapper.RoleOverseasAddressMapper;
import com.xiangrui.lmp.business.admin.role.mapper.UserRoleMapper;
import com.xiangrui.lmp.business.admin.role.service.RoleService;
import com.xiangrui.lmp.business.admin.role.vo.Role;
import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;

@Service(value = "roleService")
public class RoleServiceImpl implements RoleService
{

    @Autowired
    private RoleMapper roleMapper;
    
    @Autowired
    private RoleMenuMapper roleMenuMapper;
    
    @Autowired
    private UserRoleMapper userRoleMapper;
    
    
    @Autowired
    private RoleOverseasAddressMapper roleOverseasAddressMapper;
    
    @Autowired
    private RoleBusinessNameMapper roleBusinessNameMapper;

    @Override
    public List<Role> queryAll(Map<String, Object> params)
    {

        return roleMapper.queryAll(params);
    }

    @SystemServiceLog(description = "修改角色信息")
    @Override
    public void update(Role role)
    {
        roleMapper.update(role);

    }

    @SystemServiceLog(description = "新增角色信息")
    @Override
    public int insert(Role role)
    {
        return roleMapper.insert(role);

    }

    @SystemServiceLog(description = "删除角色信息")
    @Override
    public void delete(int id)
    {
        //删除用户角色
        userRoleMapper.deleteByRoleId(id);
        
        //删除角色菜单
        roleMenuMapper.deleteByRoleId(id);
        
        RoleOverseasAddress roleOverseasAddress =new RoleOverseasAddress();
        
        roleOverseasAddress.setRole_id(id);
        
        //删除角色仓库权限
        roleOverseasAddressMapper.delRoleOverseasAddress(roleOverseasAddress);
        
        //删除角色
        roleMapper.delete(id);
        
        //删除业务授权
        roleBusinessNameMapper.deleteRoleBusinessNameByRoleId(id);
    }

    @Override
    public int queryCount()
    {

        return roleMapper.queryCount();
    }

    @Override
    public List<Role> queryRole()
    {
        return roleMapper.queryRole();
    }

    @Override
    public String checkRole(String rolename)
    {
        return roleMapper.checkRole(rolename);
    }

    @Override
    public List<String> checkUpdateRole(Map<String, Object> params)
    {
        return roleMapper.checkUpdateRole(params);
    }

    @Override
    public List<Role> queryAllInVisible()
    {
        return roleMapper.queryAllInVisible();
    }

}
