package com.xiangrui.lmp.business.admin.role.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.role.vo.RoleMenu;

public interface RoleMenuService
{
    /**
     * 添加
     * 
     * @param rolemenu
     */
    void insert(RoleMenu rolemenu);

    /**
     * 修改
     * 
     * @param rolemenu
     */
    void update(RoleMenu rolemenu);

    /**
     * 根据roleid查询menuid
     * 
     * @return
     */
    List<RoleMenu> queryMenuByRoleId(int id);

    /**
     * 根据menuid查询roleid
     * 
     * @param id
     */
    List<RoleMenu> queryRoleByMenuId(int id);

    /**
     * 删除,更具roleid删除
     * 
     * @param id
     */
    void deleteByRoleId(int roleid);

    /**
     * 验证是否存在
     * 
     * @param rm
     * @return
     */
    RoleMenu queryRoleMenu(RoleMenu rm);

    void addRolemenuInList(List<RoleMenu> par);
    
    /**
     * 查询所有的角色菜单
     * @return
     */
    List<RoleMenu> queryAllRoleMenu();
}
