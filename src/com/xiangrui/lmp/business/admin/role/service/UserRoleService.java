package com.xiangrui.lmp.business.admin.role.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.role.vo.UserRole;

public interface UserRoleService {
	/**
	 * 添加
	 * @param userRole
	 */
	void insert(UserRole userRole);
	/**
	 *查询用户角色 
	 * @return
	 */
	List<UserRole> queryAll();
	/**
	 * 根据用户id查询用户角色
	 * @param user_id
	 * @return
	 */
	List<UserRole> queryByUserId(int user_id);
	/**
	 * 根据userid删除用户角色
	 * @param user_id
	 * @return
	 */
	int deleteByUserId(int user_id);
	/**
	 * 根据roleid删除用户角色
	 * @param role_id
	 * @return
	 */
	int deleteByRoleId(int role_id);
	/**
	 * 根据userid和roleid查询角色信息 单条角色
	 * @param user_id
	 * @param role_id
	 * @return
	 */
	UserRole queryByUserIdAndRoleId(int user_id, int role_id);
}
