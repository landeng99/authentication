package com.xiangrui.lmp.business.admin.role.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.role.mapper.RoleOverseasAddressMapper;
import com.xiangrui.lmp.business.admin.role.service.RoleOverseasAddressService;
import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;

@Service("roleOverseasAddressService")
public class RoleOverseasAddressServiceImpl implements RoleOverseasAddressService
{
    
    @Autowired
    private RoleOverseasAddressMapper roleOverseasAddressMapper;

    @Override
    public List<RoleOverseasAddress> queryRoleOverseasAddressByRoleId(
            RoleOverseasAddress roleOverseasAddress)
    {
        
        return roleOverseasAddressMapper.queryRoleOverseasAddressByRoleId(roleOverseasAddress);
    }
    @Override
    public void insertRoleOverseasAddress(
            List<RoleOverseasAddress> roleOverseasAddressList,int roleId)
    {
        
        RoleOverseasAddress roleOverseasAddress=new RoleOverseasAddress();
        roleOverseasAddress.setRole_id(roleId);
        roleOverseasAddressMapper.delRoleOverseasAddress(roleOverseasAddress);
        
        for(RoleOverseasAddress data:roleOverseasAddressList){
            roleOverseasAddressMapper.insertRoleOverseasAddress(data);
        }
    }
}
