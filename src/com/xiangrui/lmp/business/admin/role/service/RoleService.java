package com.xiangrui.lmp.business.admin.role.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.role.vo.Role;

public interface RoleService {
	/**
	 * 更新
	 * @param role
	 * @return
	 */
	void update(Role role);
	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
	int insert(Role role);
	/**
	 * 删除 包括其中关联的菜单中间表信息,用户表中间信息的清空
	 * @param id
	 * @return
	 */
	void delete(int id);
	/**
	 * 分页查询所有
	 * @return
	 */
	List<Role> queryAll(Map<String,Object> params);
	/**
	 * 查询总记录数
	 * @param role
	 * @return
	 */
	int queryCount();
	
	/**
	 * 查询所有角色
	 * @return
	 */
	List<Role> queryRole();
	/**
	 * 检测角色是否已存在
	 * @param rolename
	 * @return
	 */
	String checkRole(String rolename);
	/**
	 * 修改时检测角色是否已存在
	 * @param params
	 * @return
	 */
	List<String> checkUpdateRole(Map<String,Object> params);
	/**
	 * 获取所有不可见的角色信息
	 * @return
	 */
    List<Role> queryAllInVisible();
}
