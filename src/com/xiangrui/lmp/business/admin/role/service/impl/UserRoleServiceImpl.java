package com.xiangrui.lmp.business.admin.role.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.role.mapper.UserRoleMapper;
import com.xiangrui.lmp.business.admin.role.service.UserRoleService;
import com.xiangrui.lmp.business.admin.role.vo.UserRole;

@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService
{

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public void insert(UserRole userRole)
    {
        userRoleMapper.insert(userRole);
    }

    @Override
    public List<UserRole> queryAll()
    {

        return userRoleMapper.queryAll();
    }

    @Override
    public List<UserRole> queryByUserId(int user_id)
    {

        return userRoleMapper.queryByUserId(user_id);
    }

    @SystemServiceLog(description = "删除用户")
    @Override
    public int deleteByUserId(int user_id)
    {
        return userRoleMapper.deleteByUserId(user_id);
    }

    @SystemServiceLog(description = "删除用户")
    @Override
    public int deleteByRoleId(int role_id)
    {
        return userRoleMapper.deleteByRoleId(role_id);
    }

    @Override
    public UserRole queryByUserIdAndRoleId(int user_id, int role_id)
    {
        UserRole userRole = new UserRole(user_id, role_id);
        List<UserRole> list = userRoleMapper.queryByUserIdAndRoleId(userRole);
        if(null != list && list.size()>0){
            return list.get(0);
        }else{
            return null;
        }
    }

}
