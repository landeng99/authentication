package com.xiangrui.lmp.business.admin.role.vo;

public class UserRole {
	int user_id;	//用户id
	int role_id;	//角色id
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public UserRole(int user_id, int role_id) {
		super();
		this.user_id = user_id;
		this.role_id = role_id;
	}
	public UserRole() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserRole(int role_id) {
		super();
		this.role_id = role_id;
	}
	
}
