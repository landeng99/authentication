package com.xiangrui.lmp.business.admin.role.vo;

import com.xiangrui.lmp.business.admin.menu.vo.Menu;

public class RoleMenuJspOder
{

    private Menu menu;
    private String level;
    private String orderby;

    public Menu getMenu()
    {
        return menu;
    }

    public void setMenu(Menu menu)
    {
        this.menu = menu;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }

    public String getOrderby()
    {
        return orderby;
    }

    public void setOrderby(String orderby)
    {
        this.orderby = orderby;
    }

    public RoleMenuJspOder()
    {

    }

    public RoleMenuJspOder(Menu menu, String level)
    {
        this.menu = menu;
        this.level = level;
    }

    public RoleMenuJspOder(Menu menu, String level, String orderby)
    {
        this.menu = menu;
        this.level = level;
        this.orderby = orderby;
    }

    public RoleMenuJspOder(Menu menu, String level, boolean isNull)
    {
        this.menu = menu;
        this.level = level;
        if (isNull)
            this.orderby = level + "_" + menu.getId();
    }
}
