package com.xiangrui.lmp.business.admin.role.vo;

import java.util.List;

import com.xiangrui.lmp.business.admin.menu.vo.Menu;

public class Role {
	private int id;			//角色id
	private String rolename;//角色名称
	private String desc;	//角色描述
	private int visible;
	
	/**
	 * 可见
	 */
	public static final int IS_VISIBLE_YES = 0;
	/**
	 * 不可见
	 */
	public static final int IS_VISIBLE_NO = 1;
	private List<Menu> menus;
	
	public int getVisible()
    {
        return visible;
    }
    public void setVisible(int visible)
    {
        this.visible = visible;
    }
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRolename() {
		return rolename;
	}
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
	public List<Menu> getMenus() {
		return menus;
	}
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
	public Role(int id, String rolename, String desc) {
		super();
		this.id = id;
		this.rolename = rolename;
		this.desc = desc;
	}
	public Role(String rolename, String desc) {
		super();
		this.rolename = rolename;
		this.desc = desc;
	}
	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
