package com.xiangrui.lmp.business.admin.role.vo;

public class RoleMenu {
	int id;		//唯一标识
	int role_id;//角色id
	int menu_Id;//菜单id
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public int getMenu_Id() {
		return menu_Id;
	}
	public void setMenu_Id(int menu_Id) {
		this.menu_Id = menu_Id;
	}
	public RoleMenu(int id, int role_id, int menu_Id) {
		super();
		this.id = id;
		this.role_id = role_id;
		this.menu_Id = menu_Id;
	}
	public RoleMenu(int role_id, int menu_Id) {
		super();
		this.role_id = role_id;
		this.menu_Id = menu_Id;
	}
	public RoleMenu() {
		super();
		// TODO Auto-generated constructor stub
	}
		
}
