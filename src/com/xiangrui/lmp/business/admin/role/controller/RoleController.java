package com.xiangrui.lmp.business.admin.role.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.annotation.SystemControllerLog;
import com.xiangrui.lmp.business.admin.businessModel.service.RoleBusinessNameService;
import com.xiangrui.lmp.business.admin.businessModel.vo.RoleBusinessName;
import com.xiangrui.lmp.business.admin.menu.service.MenuService;
import com.xiangrui.lmp.business.admin.menu.vo.Menu;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.role.service.RoleMenuService;
import com.xiangrui.lmp.business.admin.role.service.RoleOverseasAddressService;
import com.xiangrui.lmp.business.admin.role.service.RoleService;
import com.xiangrui.lmp.business.admin.role.service.UserRoleService;
import com.xiangrui.lmp.business.admin.role.vo.Role;
import com.xiangrui.lmp.business.admin.role.vo.RoleMenu;
import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;
import com.xiangrui.lmp.business.admin.role.vo.UserRole;
import com.xiangrui.lmp.init.SystemParamUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/role")
public class RoleController
{
	@Autowired
	private RoleService roleService;
	@Autowired
	private RoleMenuService roleMenuService;
	@Autowired
	private MenuService menuService;
	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleOverseasAddressService roleOverseasAddressService;

	@Autowired
	private OverseasAddressService overseasAddressService;

	@Autowired
	private RoleBusinessNameService roleBusinessNameService;

	private static final String IS_DELETE_YES = "1";
	private static final String IS_DELETE_NO = "2";

	/**
	 * 进入添加页面
	 * 
	 * @return
	 */
	@RequestMapping("/toAddRole")
	public String toAddRole(HttpServletRequest request)
	{

		StringBuffer sb = new StringBuffer("[");

		// 查询所有父级菜单
		List<Menu> listParent = menuService.queryChildrenByParentId("0");
		for (Menu menu : listParent)
		{
			sb.append(initzNodess(menu, false, false, false));

			// 查询所有子级菜单
			List<Menu> listMenu = menuService.queryChildrenByParentId(menu.getId() + "");
			for (Menu menu2 : listMenu)
			{
				sb.append(initzNodess(menu2, false, false, false));

				// 查询所有子级菜单中的增删改等各种操作
				List<Menu> listLeval = menuService.queryChildrenByParentId(menu2.getId() + "");
				for (Menu menu3 : listLeval)
				{
					sb.append(initzNodess(menu3, false, false, false));
				}
			}
		}

		sb.append("]");

		request.setAttribute("zNodess", sb.toString());

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("isdeleted", OverseasAddress.ISDELETE_UNDELETED);

		List<OverseasAddress> overseasAddressList = overseasAddressService.queryAllOverseasAddress(params);

		request.setAttribute("overseasAddressList", overseasAddressList);

		// getAllRoleBusinessWithCheckByRoleId
		List<RoleBusinessName> businessNameList = roleBusinessNameService.queryAllBusinessName();

		request.setAttribute("businessNameList", businessNameList);

		return "back/addRole";
	}

	/**
	 * 添加角色信息
	 * 
	 * @param role
	 * @param menusStr
	 * @return
	 */
	@RequestMapping("/insert")
	@SystemControllerLog(description = "新增角色")
	public String addRole(Role role, String menusStr, String roleWarehouseStr, String roleBusinessStr, HttpServletRequest req)
	{
		String roname = roleService.checkRole(role.getRolename());
		if (roname == null || "".equals(roname))
		{
			this.roleService.insert(role);
			String[] menus = menusStr.split(",");

			List<RoleMenu> roleMenus = new ArrayList<RoleMenu>();
			for (String menu : menus)
			{
				RoleMenu roleMenu = null;
				int menuId = Integer.valueOf(menu);
				roleMenu = new RoleMenu();
				roleMenu.setRole_id(role.getId());
				roleMenu.setMenu_Id(menuId);
				roleMenus.add(roleMenu);
			}
			this.roleMenuService.addRolemenuInList(roleMenus);

			String[] overseasAddressIds = roleWarehouseStr.split(",");

			List<RoleOverseasAddress> roleOverseasAddressList = new ArrayList<RoleOverseasAddress>();
			if (overseasAddressIds.length > 0)
			{
				for (String str : overseasAddressIds)
				{
					RoleOverseasAddress roleOverseasAddress = new RoleOverseasAddress();
					roleOverseasAddress.setRole_id(role.getId());
					roleOverseasAddress.setId(Integer.parseInt(str));
					roleOverseasAddressList.add(roleOverseasAddress);
				}
				roleOverseasAddressService.insertRoleOverseasAddress(roleOverseasAddressList, role.getId());
			}
			roleBusinessStr = StringUtils.trimToNull(roleBusinessStr);
			if (roleBusinessStr != null)
			{
				String[] businessNames = roleBusinessStr.split(",");

				if (businessNames.length > 0)
				{
					for (String str : businessNames)
					{
						RoleBusinessName roleBusinessName = new RoleBusinessName();
						roleBusinessName.setRole_id(role.getId());
						roleBusinessName.setBusiness_name(str);
						roleBusinessNameService.insertRoleBusinessName(roleBusinessName);
					}
				}
			}
			// 刷新用户权限信息
			SystemParamUtil.getInstance().loadUserMenuMap();
		}
		else
		{
			req.setAttribute("fail", "添加失败！角色名已存在！");
		}
		return "back/roleList";
	}

	/**
	 * 查询角色列表
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryAll")
	public String getRoleAll(HttpServletRequest request)
	{

		String pageIndex = request.getParameter("pageIndex");

		PageView pageView = null;

		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);
		List<Role> list = roleService.queryAll(params);
		request.setAttribute("pageView", pageView);
		request.setAttribute("roleLists", list);
		return "back/roleList";
	}

	/**
	 * 进入角色详情页面
	 * 
	 * @param request
	 * @param role
	 * @return
	 */
	@RequestMapping("/rolePage")
	public String rolePage(HttpServletRequest request, Role role)
	{
		Map<String, Object> map = editRoleMenu(request, role, true);
		request.setAttribute("rolemenulist", map.get("rolemenulist"));
		request.setAttribute("id", map.get("id"));
		request.setAttribute("rolename", map.get("rolename"));
		request.setAttribute("desc", map.get("desc"));
		request.setAttribute("zNodess", map.get("zNodess"));

		RoleOverseasAddress roleOverseasAddress = new RoleOverseasAddress();

		roleOverseasAddress.setRole_id(role.getId());

		// 海外地址查询
		List<RoleOverseasAddress> roleOverseasAddressList = roleOverseasAddressService.queryRoleOverseasAddressByRoleId(roleOverseasAddress);

		request.setAttribute("roleOverseasAddressList", roleOverseasAddressList);
		// 业务授权
		List<RoleBusinessName> businessNameList = getAllRoleBusinessWithCheckByRoleId(role.getId());
		request.setAttribute("businessNameList", businessNameList);
		return "back/rolePage";
	}

	/**
	 * 进入修改页面
	 * 
	 * @param role
	 * @return
	 */
	@RequestMapping("/updateRole")
	public String updateRole(HttpServletRequest request, Role role)
	{
		Map<String, Object> map = editRoleMenu(request, role, false);
		request.setAttribute("rolemenulist", map.get("rolemenulist"));
		request.setAttribute("id", map.get("id"));
		request.setAttribute("rolename", map.get("rolename"));
		request.setAttribute("desc", map.get("desc"));
		request.setAttribute("zNodess", map.get("zNodess"));

		RoleOverseasAddress roleOverseasAddress = new RoleOverseasAddress();

		roleOverseasAddress.setRole_id(role.getId());
		// 海外地址查询
		List<RoleOverseasAddress> roleOverseasAddressList = roleOverseasAddressService.queryRoleOverseasAddressByRoleId(roleOverseasAddress);

		request.setAttribute("roleOverseasAddressList", roleOverseasAddressList);

		// 业务授权
		List<RoleBusinessName> businessNameList = getAllRoleBusinessWithCheckByRoleId(role.getId());
		request.setAttribute("businessNameList", businessNameList);
		return "back/editRole";
	}

	@RequestMapping("/queryRole")
	@ResponseBody
	public List<RoleMenu> queryRole(HttpServletRequest request, Role role)
	{
		List<RoleMenu> roleMenuList = roleMenuService.queryMenuByRoleId(role.getId());
		return roleMenuList;
	}

	/**
	 * 编辑角色和权限
	 * 
	 * @param role
	 * @param menusStr
	 * @return
	 */
	@RequestMapping("/update")
	public String update(Role role, String menusStr, String roleWarehouseStr, String roleBusinessStr)
	{
		this.roleService.update(role);
		List<RoleMenu> roleMenuList = roleMenuService.queryMenuByRoleId(role.getId());

		List<String> overseasAddressIds = StringUtil.toStringList(roleWarehouseStr, ",");// roleWarehouseStr.split(",");

		List<RoleOverseasAddress> roleOverseasAddressList = new ArrayList<RoleOverseasAddress>();
		if (overseasAddressIds.size() > 0)
		{
			for (String str : overseasAddressIds)
			{
				RoleOverseasAddress roleOverseasAddress = new RoleOverseasAddress();
				roleOverseasAddress.setRole_id(role.getId());
				roleOverseasAddress.setId(Integer.parseInt(str));
				roleOverseasAddressList.add(roleOverseasAddress);
			}

		}
		roleOverseasAddressService.insertRoleOverseasAddress(roleOverseasAddressList, role.getId());

		// 业务授权
		roleBusinessNameService.deleteRoleBusinessNameByRoleId(role.getId());
		roleBusinessStr = StringUtils.trimToNull(roleBusinessStr);
		if (roleBusinessStr != null)
		{
			String[] businessNames = roleBusinessStr.split(",");

			if (businessNames.length > 0)
			{
				for (String str : businessNames)
				{
					RoleBusinessName roleBusinessName = new RoleBusinessName();
					roleBusinessName.setRole_id(role.getId());
					roleBusinessName.setBusiness_name(str);
					roleBusinessNameService.insertRoleBusinessName(roleBusinessName);
				}
			}
		}

		if (roleMenuList.size() > 0)
		{
			// 添加前删除权限
			this.roleMenuService.deleteByRoleId(role.getId());
		}
		List<RoleMenu> roleMenus = new ArrayList<RoleMenu>();
		String[] menus = menusStr.split(",");
		RoleMenu roleMenu = null;

		for (String menu : menus)
		{
			int menuId = Integer.valueOf(menu);
			roleMenu = new RoleMenu();
			roleMenu.setRole_id(role.getId());
			roleMenu.setMenu_Id(menuId);
			roleMenus.add(roleMenu);
		}
		this.roleMenuService.addRolemenuInList(roleMenus);

		// 刷新用户权限信息
		SystemParamUtil.getInstance().loadUserMenuMap();

		return "back/roleList";
	}

	/**
	 * 删除角色
	 * 
	 * @return
	 */
	@RequestMapping("/deleteRole")
	@ResponseBody
	public String delRole(int id, int user_id)
	{
		UserRole userRole = this.userRoleService.queryByUserIdAndRoleId(user_id, id);
		if (null == userRole)
		{
			this.roleService.delete(id);

			// 刷新用户权限信息
			SystemParamUtil.getInstance().loadUserMenuMap();

			return IS_DELETE_YES;
		}
		else
		{
			return IS_DELETE_NO;
		}

	}

	/**
	 * 检测添加角色时是否重复
	 * 
	 * @param role
	 * @return
	 */
	@RequestMapping("/checkRole")
	@ResponseBody
	public Map<String, Object> checkRole(Role role)
	{
		String roname = roleService.checkRole(role.getRolename());
		Map<String, Object> map = new HashMap<String, Object>();
		if (null == roname || "".equals(roname))
		{
			map.put("result", true);
		}
		else
		{
			map.put("result", false);
		}
		return map;
	}

	/**
	 * 验证修改角色名
	 * 
	 * @return
	 */
	@RequestMapping("/checkUpdateRole")
	@ResponseBody
	public Map<String, Object> checkUpdateRole(Role role)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", role.getId());
		List<String> ronamelist = roleService.checkUpdateRole(params);
		Map<String, Object> map = new HashMap<String, Object>();

		String rolename = role.getRolename();
		if (ronamelist.contains(rolename))
		{
			map.put("result", false);
		}
		else
		{
			map.put("result", true);
		}

		return map;
	}

	/**
	 * 添加znodess的一条数据
	 * 
	 * @param menu
	 *            数据对象
	 * @param isChecked
	 *            是否选中
	 * @param isOpent
	 *            是否可展开
	 * @param isChkDisabled
	 *            是否可编辑
	 * @return
	 */
	private StringBuffer initzNodess(Menu menu, boolean isChecked, boolean isOpent, boolean isChkDisabled)
	{
		StringBuffer sb = new StringBuffer("");
		sb.append("{ id:");
		sb.append(menu.getId());
		sb.append(", pId:");
		sb.append(menu.getParent_id());
		sb.append(", name:\"");
		sb.append(menu.getName());
		sb.append("\"");
		if (isChecked)
		{
			sb.append(", checked:true");
		}
		if (isChkDisabled)
		{
			sb.append(", chkDisabled:true");
		}
		if (isOpent)
		{
			sb.append(", open:true");
		}
		sb.append("},");
		return sb;
	}

	/**
	 * 仅仅对查看详情和修改有作用
	 * 
	 * @param request
	 * @param role
	 * @return
	 */
	private Map<String, Object> editRoleMenu(HttpServletRequest request, Role role, boolean isChkDisabled)
	{
		StringBuffer sb = new StringBuffer("[");

		// 查询所有父级菜单
		List<Menu> listParent = menuService.queryChildrenByParentId("0");

		// 循环通用验证对象
		RoleMenu rm = new RoleMenu();
		rm.setRole_id(role.getId());
		RoleMenu temp = new RoleMenu();
		for (Menu menu : listParent)
		{
			rm.setMenu_Id(menu.getId());
			temp = roleMenuService.queryRoleMenu(rm);

			// 添加父级
			if (null != temp && temp.getMenu_Id() == rm.getMenu_Id())
			{
				sb.append(initzNodess(menu, true, false, isChkDisabled));
			}
			else
			{
				sb.append(initzNodess(menu, false, false, isChkDisabled));
			}

			// 查询所有子级菜单
			List<Menu> listMenu = menuService.queryChildrenByParentId(menu.getId() + "");
			for (Menu menu2 : listMenu)
			{
				rm.setMenu_Id(menu2.getId());
				temp = roleMenuService.queryRoleMenu(rm);

				// 添加子级
				if (null != temp && temp.getMenu_Id() == rm.getMenu_Id())
				{
					sb.append(initzNodess(menu2, true, false, isChkDisabled));
				}
				else
				{
					sb.append(initzNodess(menu2, false, false, isChkDisabled));
				}

				// 查询所有子级菜单中的增删改等各种操作
				List<Menu> listLeval = menuService.queryChildrenByParentId(menu2.getId() + "");
				for (Menu menu3 : listLeval)
				{

					// 添加具体操作
					rm.setMenu_Id(menu3.getId());
					temp = roleMenuService.queryRoleMenu(rm);

					// 添加子级
					if (null != temp && temp.getMenu_Id() == rm.getMenu_Id())
					{
						sb.append(initzNodess(menu3, true, false, isChkDisabled));
					}
					else
					{
						sb.append(initzNodess(menu3, false, false, isChkDisabled));
					}
				}
			}
		}

		Map<String, Object> map = new HashMap<String, Object>();

		List<RoleMenu> roleMenuList = roleMenuService.queryMenuByRoleId(role.getId());
		map.put("rolemenulist", roleMenuList);

		String rolename = role.getRolename();
		String desc = role.getDesc();
		try
		{
			rolename = java.net.URLDecoder.decode(rolename, "UTF-8");
			desc = java.net.URLDecoder.decode(desc, "UTF-8");
		} catch (UnsupportedEncodingException e)
		{

			e.printStackTrace();
		}
		map.put("id", role.getId());
		map.put("rolename", rolename);
		map.put("desc", desc);

		sb.append("]");

		map.put("zNodess", sb.toString());

		return map;
	}

	private List<RoleBusinessName> getAllRoleBusinessWithCheckByRoleId(Integer role_id)
	{
		List<RoleBusinessName> businessNameList = roleBusinessNameService.queryAllBusinessName();
		List<RoleBusinessName> existBusinessNameList = roleBusinessNameService.queryRoleBusinessNameByRoleId(role_id);
		if (businessNameList != null && businessNameList.size() > 0)
		{
			for (RoleBusinessName roleBusinessName : businessNameList)
			{
				if (isExistRoleBusiessName(existBusinessNameList, roleBusinessName.getBusiness_name()))
				{
					roleBusinessName.setRole_id(role_id);
				}
			}
		}
		return businessNameList;
	}

	private boolean isExistRoleBusiessName(List<RoleBusinessName> existBusinessNameList, String bussinessName)
	{
		boolean isExist = false;
		if (existBusinessNameList != null && existBusinessNameList.size() > 0)
		{
			for (RoleBusinessName roleBusinessName : existBusinessNameList)
			{
				if (bussinessName.equals(roleBusinessName.getBusiness_name()))
				{
					isExist = true;
				}
			}
		}
		return isExist;
	}
}
