package com.xiangrui.lmp.business.admin.user.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.role.service.RoleService;
import com.xiangrui.lmp.business.admin.role.service.UserRoleService;
import com.xiangrui.lmp.business.admin.user.service.UserService;

@Controller
@RequestMapping("/admin/manager")
public class AdminController
{
    Logger logger = Logger.getLogger(AdminController.class);

    /*
     * @Autowired private SqlSession sqlSession;
     */

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRoleService userRoleService;

    @RequestMapping("/index")
    public String index()
    {
        return "back/index";
    }

    @RequestMapping("/wellcome")
    public String wellcome()
    {

        return "back/wellcome";
    }

}
