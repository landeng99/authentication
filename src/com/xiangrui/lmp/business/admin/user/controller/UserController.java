package com.xiangrui.lmp.business.admin.user.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.role.service.RoleService;
import com.xiangrui.lmp.business.admin.role.service.UserRoleService;
import com.xiangrui.lmp.business.admin.role.vo.Role;
import com.xiangrui.lmp.business.admin.role.vo.UserRole;
import com.xiangrui.lmp.business.admin.user.service.UserService;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.init.SystemParamUtil;
import com.xiangrui.lmp.util.PageView;

@Controller
@RequestMapping("/admin/user")
public class UserController
{
    Logger logger = Logger.getLogger(UserController.class);

    /*
     * @Autowired private SqlSession sqlSession;
     */

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRoleService userRoleService;

    @RequestMapping("/list")
    public void userList1()
    {

        System.out.println("fffffffffff");
    }

    /*
     * @RequestMapping("/Judge")
     * 
     * @ResponseBody public User userJudge(String username) { User u =
     * sqlSession.selectOne("com.res.mapper.judgeUser",username); return u; }
     */
    /**
     * 用户登录
     * 
     * @param user
     * @param req
     * @param resp
     * @return
     * @throws IOException
     */
    @RequestMapping("/login")
    public String login(User user, HttpServletRequest req,
            HttpServletResponse resp) throws IOException
    {
        User u = userService.userLogin(user);

        // 查到用户，执行登录成功的操作
        if (u != null)
        {
            req.getSession().setAttribute("back_user", u);
            return "redirect:/admin/index";
        } else
        {
            req.setAttribute("fail", "登录失败！用户名或密码错误！");
            return "back/login";
        }
    }

    /**
     * 展示登录人员用户信息
     * 
     * @return
     */
    @RequestMapping("/querylogin")
    public String queryLogin(User user, HttpServletRequest req)
    {
        HttpSession session = req.getSession();
        User us = (User) session.getAttribute("back_user");
        user.setUsername(us.getUsername());
        user.setPassword(us.getPassword());
        return "back/userInfo";

    }

    /**
     * 查询所有用户信息
     * 
     * @param request
     * @return
     */
    @RequestMapping("/queryAll")
    public String getUserAll(HttpServletRequest request)
    {
        // 分页查询

        PageView pageView = null;
        // pagination.setTargetUrl("user/queryAll");
        String pageIndex = request.getParameter("pageIndex");

        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);

        List<User> list = userService.queryAll(params);
        request.setAttribute("pageView", pageView);
        request.setAttribute("userLists", list);
        return "back/userList";
    }

    /**
     * 进入添加页面
     * 
     * @return
     */
    @RequestMapping("/toAddUser")
    public String toAddUser(HttpServletRequest request)
    {
        List<Role> list = roleService.queryRole();
        request.setAttribute("roleLists", list);
        return "back/addUser";
    }

    /**
     * 添加用户
     * 
     * @param user
     * @return
     */
    @RequestMapping("/addUser")
    public String addUser(User user, String roleStr, HttpServletRequest req)
    {
        String sqlusername = userService.checkaddUser(user.getUsername());
        if (sqlusername == null || "".equals(sqlusername))
        {
            this.userService.addUser(user);

            List<Role> roleDefaults = this.roleService.queryAllInVisible();
            for (Role roleObj : roleDefaults)
            {
                if (roleObj.getVisible() == Role.IS_VISIBLE_NO)
                {
                    int roleId = roleObj.getId();
                    UserRole userRole = new UserRole();
                    userRole.setUser_id(user.getUser_id());
                    userRole.setRole_id(roleId);
                    this.userRoleService.insert(userRole);
                }
            }
            String[] roles = roleStr.split(",");
            UserRole userRole = null;
            for (String role : roles)
            {
                int roleId = Integer.valueOf(role);
                userRole = new UserRole();
                userRole.setUser_id(user.getUser_id());
                userRole.setRole_id(roleId);
                this.userRoleService.insert(userRole);
            }
            //刷新用户权限信息
            SystemParamUtil.getInstance().loadUserMenuMap();
        } else
        {
            req.setAttribute("fail", "添加失败！用户名已存在！");
        }
        return "back/userList";

    }

    /**
     * 新增时校验用户是否存在
     * 
     * @param user
     * @return
     */
    @RequestMapping("/checkUser")
    @ResponseBody
    public Map<String, Object> checkUser(User user)
    {
        String uname = userService.checkaddUser(user.getUsername());
        Map<String, Object> map = new HashMap<String, Object>();
        if (null == uname || "".equals(uname))
        {
            map.put("result", true);
        } else
        {
            map.put("result", false);
        }
        return map;

    }

    /**
     * 删除用户
     * 
     * @param user
     * @return
     */
    @RequestMapping("/delUser")
    public String delUser(User user)
    {
        userRoleService.deleteByUserId(user.getUser_id());
        this.userService.delUser(user.getUser_id());
        //刷新用户权限信息
        SystemParamUtil.getInstance().loadUserMenuMap();
        
        return "back/userList";
    }

    /**
     * 进入密码修改页面
     * 
     * @param user
     * @return
     */
    @RequestMapping("/updatePwd")
    public String editUser(User user)
    {
        return "back/updatePwd";
    }

    /**
     * 修改用户密码
     * 
     * @param user
     * @return
     */
    @RequestMapping("/modifyPwd")
    @ResponseBody
    public String modifyPwd(User user, String oldPassword,
            HttpServletRequest req, HttpServletResponse resp)
            throws IOException
    {
        HttpSession session = req.getSession();
        User us = (User) session.getAttribute("back_user");

        // this.userService.editUser(user);
        // User u= new User();
        if (oldPassword.equals(us.getPassword()))
        {
            user.setUser_id(us.getUser_id());
            userService.modifyPwd(user);
        }
        return "back/userList";
    }

    @RequestMapping("/userPageIndex")
    public String userPageIndex(HttpServletRequest request)
    {
        User user = (User) request.getSession().getAttribute("back_user");
        return userPage(request, user);
    }

    /**
     * 进入用户详情页面
     * 
     * @param request
     * @param user
     * @return
     */
    @RequestMapping("/userPage")
    public String userPage(HttpServletRequest request, User user)
    {
        // 查询所有角色
        List<Role> rolelist = roleService.queryRole();
        request.setAttribute("roleLists", rolelist);
        // 根据id查询该用户拥有的角色
        List<UserRole> list = userRoleService.queryByUserId(user.getUser_id());
        request.setAttribute("userRoleList", list);

        String username = user.getUsername();
        request.setAttribute("user_id", user.getUser_id());
        request.setAttribute("username", username);
        return "back/userRole";
    }

    @RequestMapping("/queryUserRole")
    @ResponseBody
    public List<UserRole> queryUserRole(HttpServletRequest request, User user)
    {
        List<UserRole> userRoleList = userRoleService.queryByUserId(user
                .getUser_id());
        return userRoleList;
    }

    /**
     * 进入修改用户页面
     * 
     * @param request
     * @param user
     * @return
     */
    @RequestMapping("/updateUser")
    public String initUpdateUser(HttpServletRequest request, User user)
    {
        // 查询所有角色
        List<Role> rolelist = roleService.queryRole();
        request.setAttribute("roleLists", rolelist);
        // 根据id查询该用户拥有的角色
        List<UserRole> list = userRoleService.queryByUserId(user.getUser_id());
        request.setAttribute("userRoleList", list);

        String username = user.getUsername();
        String password = user.getPassword();
        request.setAttribute("user_id", user.getUser_id());
        request.setAttribute("username", username);
        request.setAttribute("password", password);
        return "back/updateUser";
    }

    /**
     * 修改用户
     * 
     * @param user
     * @param roleStr
     * @param req
     * @return
     */
    @RequestMapping("/updateUserInfo")
    @ResponseBody
    public String updateUser(User user, String roleStr, HttpServletRequest req)
    {

        this.userService.updateUser(user);
        List<UserRole> list = userRoleService.queryByUserId(user.getUser_id());
        if (list.size() > 0)
        {
            this.userRoleService.deleteByUserId(user.getUser_id());
        }

        String[] roles = roleStr.split(",");
        UserRole userRole = null;
        for (String role : roles)
        {
            int roleId = Integer.valueOf(role);
            userRole = new UserRole();
            userRole.setUser_id(user.getUser_id());
            userRole.setRole_id(roleId);
            this.userRoleService.insert(userRole);
        }
        
        //刷新用户权限信息
        SystemParamUtil.getInstance().loadUserMenuMap();
        
        return "back/userList";
    }

    /**
     * 验证修改用户
     * 
     * @return
     */
    @RequestMapping("/checkUpdateUser")
    @ResponseBody
    public Map<String, Object> checkUpdateUser(User user)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userId", user.getUser_id());
        List<String> unamelist = userService.checkUser(params);
        Map<String, Object> map = new HashMap<String, Object>();

        String username = user.getUsername();
        if (unamelist.contains(username))
        {
            map.put("result", false);
        } else
        {
            map.put("result", true);
        }

        return map;
    }

    /**
     * 校验登录用户修改自身密码时的原密码
     * 
     * @param user
     * @return
     */
    @RequestMapping("/checkPwd")
    @ResponseBody
    public Map<String, Object> checkPwd(User user, HttpServletRequest req)
            throws IOException
    {
        HttpSession session = req.getSession();
        User us = (User) session.getAttribute("back_user");
        Map<String, Object> map = new HashMap<String, Object>();
        if (user.getPassword().equals(us.getPassword()))
        {
            map.put("result", true);
        } else
        {
            map.put("result", false);
        }
        return map;
    }
}
