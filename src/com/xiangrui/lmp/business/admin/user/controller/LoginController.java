package com.xiangrui.lmp.business.admin.user.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.user.service.UserService;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.init.SessionListener;
import com.xiangrui.lmp.servlet.AuthImg;
import com.xiangrui.lmp.util.ServletContainer;

@Controller
@RequestMapping("/admin")
public class LoginController
{
    Logger logger = Logger.getLogger(LoginController.class);

    /*
     * @Autowired private SqlSession sqlSession;
     */

    @Autowired
    private UserService userService;

    /**
     * 用户登录
     * 
     * @param user
     * @param req
     * @param resp
     * @return
     * @throws IOException
     */
    @RequestMapping("/login")
    public String login(User user, HttpServletRequest req,
            HttpServletResponse resp, String txtCode) throws IOException
    {

        logger.info("登录开始.......");
        if (user != null && txtCode != null) {
			System.out.println("后台登录，页面传到后台的信息:userName-->" + user.getUsername() + "===验证码是：-->" + txtCode);
		}

        User isUser = (User) req.getSession().getAttribute("back_user");
        if (null != isUser)
        {
        	System.out.println("isUser的值是----》"+toString());
            return "redirect:/admin/manager/index";
        }
        // 用户登录信息校验失败，则返回重新登录
        if (!validateUser(user, req))
        {
            return "back/login";
        }

        // 用户登录信息获取
        User u = userService.userLogin(user);

        // 查到用户，执行登录成功的操作
        if (u != null)
        {
        	System.out.println("用户登录信息获取user的值是----》"+toString());
            HttpSession newSession = req.getSession();
            ServletContext applicate = req.getSession().getServletContext();
            newSession.setAttribute("back_user", u);

            Subject subject = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(
                    u.getUsername(), u.getPassword());
            Session session = subject.getSession();

            session.setAttribute(SYSConstant.SESSION_USER, u);
            ServletContainer.setCurBackUser(u);
            token.setRememberMe(true);
            subject.login(token);
            System.out.println("token的信息是---》"+token.isRememberMe()+"=="+token.getUsername());

            // 判断该用户名的用户是否登录过,又可以使上次登录的用户掉线
            SessionListener.isAlreadyEnter(newSession, u.getUsername());

            // 删除本user_id关联的session,挤下上次登录
            // onlineUserListRemove(u.getUser_id(), req);
            // 记录本次登录
            // onlineUserListAdd(u.getUser_id(), req);

            // if (null != req.getSession().getAttribute("back_user"))
            // {
            return "redirect:/admin/manager/index";
            // }
            // {
            // onlineUserListDel(u.getUser_id(), applicate);
            // req.setAttribute("fail", "你上次没有安全退出后台！请重新登录！");
            // return "back/login";
            // }

        } else
        {
            req.setAttribute("fail", "登录失败！用户名或密码错误！");
            return "back/login";
        }
    }

    /**
     * 保存
     * 
     * @param user_id
     * @param req
     */
    private void onlineUserListAdd(int user_id, HttpServletRequest req)
    {
        ServletContext application = req.getSession().getServletContext();
        // 把用户名放入在线列表
        @SuppressWarnings("unchecked")
        Map<Integer, HttpSession> sessionMap = (Map<Integer, HttpSession>) application
                .getAttribute("sessionMap");
        // 第一次使用前，需要初始化
        if (sessionMap == null)
        {
            sessionMap = new HashMap<Integer, HttpSession>();
            application.setAttribute("sessionMap", sessionMap);
        }
        // 用户id管理登录使用的session
        sessionMap.put(user_id, req.getSession());
    }

    /**
     * 移除
     * 
     * @param user_id
     * @param req
     */
    private void onlineUserListRemove(int user_id, HttpServletRequest req)
    {
        ServletContext application = req.getSession().getServletContext();
        @SuppressWarnings("unchecked")
        Map<Integer, HttpSession> sessionMap = (Map<Integer, HttpSession>) application
                .getAttribute("sessionMap");
        if (null != sessionMap && sessionMap.containsKey(user_id))
        {
            HttpSession otherSession = (HttpSession) sessionMap.get(user_id);
            // 这一行其实没必要,因为操作完删除后,马上操作添加,map集合自动替换芯的session
            sessionMap.remove(user_id);
            otherSession.invalidate();
        }
    }

    /**
     * 删除
     * 
     * @param user_id
     * @param req
     */
    private void onlineUserListDel(int user_id, ServletContext application)
    {
        @SuppressWarnings("unchecked")
        Map<Integer, HttpSession> sessionMap = (Map<Integer, HttpSession>) application
                .getAttribute("sessionMap");
        if (null != sessionMap && sessionMap.containsKey(user_id))
        {
            sessionMap.remove(user_id);
        }
    }

    @RequestMapping("logoutCfLogin")
    @ResponseBody
    public String logoutCfLogin(HttpServletRequest req)
    {
        User user = (User) req.getSession().getAttribute("back_user");

        if (user == null)
        {

        }

        String userName = user.getUsername();

        Subject subject = SecurityUtils.getSubject();
        subject.logout();

        User user1 = (User) req.getSession().getAttribute("back_user");

        ServletContext application = req.getSession().getServletContext();
        Map<String, String> map = (Map<String, String>) application
                .getAttribute("BACK_USER_LOGIN_APPLICATION");

        String sessionId = req.getSession().getId();
        for (String key : map.keySet())
        {
            if (userName.equals(key) || sessionId.equals(map.get(key)))
            {
                map.remove(key);
                return "0";
            }
        }
        return "1";
    }

    /**
     * 
     * @param req
     * @param userName
     * @return ture 重复,false 无重复
     */
    private boolean isCfLogin(HttpServletRequest req, String userName)
    {
        ServletContext application = req.getSession().getServletContext();
        Map<String, String> map = (Map<String, String>) application
                .getAttribute("BACK_USER_LOGIN_APPLICATION");

        String sessionId = req.getSession().getId();
        if (null == map || map.size() == 0)
        {
            map = new HashMap<String, String>();
            map.put(userName, sessionId);
            application.setAttribute("BACK_USER_LOGIN_APPLICATION", map);
            return false;
        }
        for (String key : map.keySet())
        {
            if (userName.equals(key) || sessionId.equals(map.get(key)))
            {
                return true;
            }
        }
        return false;
    }

    @RequestMapping("/denied")
    public String denied(HttpServletRequest req, HttpServletResponse resp)
    {
        return "/back/denied";
    }

    /**
     * 
     * 校验登录信息
     * 
     * @param user
     * @return
     */
    private boolean validateUser(User user, HttpServletRequest req)
    {
        if (null == user)
        {
            return false;
        }
        // 如果用户名为空则跳转
        if (null == user.getUsername() || "" == user.getUsername())
        {

            return false;
        }
        if (null == user.getPassword() || "" == user.getPassword())
        {

            return false;
        }

        String imgVCValues = (String) req.getSession().getAttribute(
                "AUTHIMG_CODE");
        String txtCode = req.getParameter("txtCode");

//        if (null != imgVCValues && !imgVCValues.equals(txtCode))
        if(!AuthImg.checkAuthingCode(imgVCValues, txtCode))
        {
            req.setAttribute("txtCodeErr", "验证码错误");
            return false;
        }

        return true;

    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest req, HttpServletResponse resp)
    {
        // int user_id = ((User) req.getSession().getAttribute("back_user"))
        // .getUser_id();

        // ServletContext application = req.getSession().getServletContext();

        Subject subject = SecurityUtils.getSubject();
        Session session=subject.getSession();
 
         subject.logout();
        // onlineUserListDel(user_id, application);
        return "back/login";
    }

    /**
     * 展示登录人员用户信息
     * 
     * @return
     */
    @RequestMapping("/querylogin")
    public String queryLogin(User user, HttpServletRequest req)
    {
        HttpSession session = req.getSession();
        User us = (User) session.getAttribute("loginUser");
        user.setUsername(us.getUsername());
        user.setPassword(us.getPassword());
        return "back/userInfo";
    }
}
