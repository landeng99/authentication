package com.xiangrui.lmp.business.admin.user.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;
import com.xiangrui.lmp.business.admin.user.vo.User;

public interface UserMapper {

	/**
	 * 登录
	 * @param user
	 * @return
	 */
	User userLogin(User user);
    /**
     * 编辑用户
     * @param user
     */
    void updateUser(User user);
	/**
	 * 添加用户
	 * @param user
	 */
	void addUser(User user);
	/**
	 * 删除用户
	 * @param user_id
	 */
	void delUser(int user_id);
	/**
	 * 分页查询所有用户
	 * @param params
	 * @return
	 */
	public List<User> queryAll(Map<String, Object> params);
	public User getUserById(int user_id);
	
	/**修改密码
	 * @param user
	 * @return 
	 */
	void modifyPwd(User user);
	/**
	 * 修改时验证用户是否存在
	 * @param username
	 */
	List<String> checkUser(Map<String,Object> params);
	/**
	 * 查询总记录数
	 * @return
	 */
	int queryCount();
	/**
	 * 添加时验证用户是否存在
	 * @param username
	 * @return
	 */
	String checkaddUser(String username);
	
	/**
     * 通过用户名查询用户
     * @param username
     * @return
     */
	User getUserByUserName(String username);
	
}
