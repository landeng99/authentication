package com.xiangrui.lmp.business.admin.user.service;


import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.user.vo.User;


public interface UserService {
	
	/**
	 * 用户登录
	 * @param user
	 * @return
	 */
	User userLogin(User user);
	
	/**
	 * 编辑用户
	 * @param user
	 */
	public void updateUser(User user);
	
	/**
	 * 添加用户
	 * @param user
	 */
	public void addUser(User user);
	
	/**
	 * 删除
	 * @param user_id
	 */
	public void delUser(int user_id);
	
	public User getUserById(int user_id);
	
	/**
	 * 查询所有用户
	 * @param params 
	 * @return
	 */
	public List<User> queryAll(Map<String, Object> params);
	
	/**修改密码
	 * @param password
	 * @param newPwd
	 * @param surePwd
	 * @param types
	 * @return
	 */
	void modifyPwd(User user);
	/**
	 * 验证用户是否存在
	 * @param username
	 * @return 
	 */
	List<String> checkUser(Map<String,Object> params);
	/**
	 * 查询总记录数
	 * @return
	 */
	int queryCount();
	/**
	 * 添加时验证用户是否存在
	 * @param username
	 * @return
	 */
	String checkaddUser(String username);
	
	   /**
     * 通过用户名查询用户
     * @param username
     * @return
     */
     User getUserByUserName(String username);
}
