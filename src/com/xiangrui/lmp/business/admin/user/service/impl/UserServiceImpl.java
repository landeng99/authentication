package com.xiangrui.lmp.business.admin.user.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.user.mapper.UserMapper;
import com.xiangrui.lmp.business.admin.user.service.UserService;
import com.xiangrui.lmp.business.admin.user.vo.User;

@Service(value="userService")
public class UserServiceImpl implements UserService {
	
	@Autowired  
	private UserMapper userMapper;
	
	@Override
	public User userLogin(User user) {
		return userMapper.userLogin(user);
	}

	@SystemServiceLog(description="修改后台管理员信息")
	@Override
	public void updateUser(User user) {
		userMapper.updateUser(user);
	}

	@SystemServiceLog(description="新增后台管理员")
	@Override
	public void addUser(User user) {
		userMapper.addUser(user);
	}


	@SystemServiceLog(description="删除后台管理员")
	@Override
	public void delUser(int user_id) {
		userMapper.delUser(user_id);
	}

	@Override
	public User getUserById(int user_id) {		
		return (User)this.userMapper.getUserById(user_id);
	}

	@Override
	public List<User> queryAll(Map<String, Object> params) {		
		return (List<User>)this.userMapper.queryAll(params); 
	}

	@Override
	public void modifyPwd(User user) {		
		userMapper.modifyPwd(user);
	}

	@Override
	public int queryCount() {
		return userMapper.queryCount();
	}

	@Override
	public List<String> checkUser(Map<String,Object> params) {
		return userMapper.checkUser(params);
	}

	@Override
	public String checkaddUser(String username) {
		// TODO Auto-generated method stub
		return userMapper.checkaddUser(username);
	}

    /**
     * 通过用户名查询用户
     * @param username
     * @return
     */
	@Override
    public User getUserByUserName(String username){	    
	    return userMapper.getUserByUserName(username);
    };
}
