package com.xiangrui.lmp.business.admin.memberrate.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;
import com.xiangrui.lmp.business.admin.memberrate.mapper.MemberRateMapper;
import com.xiangrui.lmp.business.admin.memberrate.service.MemberRateService;

@Service("memberRateService")
public class MemberRateServiceImpl implements MemberRateService
{

    @Autowired
    private MemberRateMapper memberRateMapper;

    /**
     * 所有会员等级
     * 
     * @param
     * @return List
     */

    /**
     * 分页查询所有等级信息
     * 
     * @param params
     * @return List
     */
    @Override
    public List<MemberRate> queryAllMemberRate(Map<String, Object> params)
    {

        return memberRateMapper.queryAllMemberRate(params);
    }

    /**
     * 通过id查找等级信息
     * 
     * @param rate_id
     * @return MemberRate
     */
    @Override
    public MemberRate queryMemberRateByRateId(int rate_id)
    {

        return memberRateMapper.queryMemberRateByRateId(rate_id);
    }

    /**
     * 通过等级名称查找等级信息
     * 
     * @param params
     * @return MemberRate
     */
    @Override
    public MemberRate queryMemberRateByRateName(Map<String, Object> params)
    {

        return memberRateMapper.queryMemberRateByRateName(params);
    }

    /**
     * 更新会员等级信息
     * 
     * @param memberRate
     * @return
     */
    @Override
    public void updateMemberRate(MemberRate memberRate)
    {

        memberRateMapper.updateMemberRate(memberRate);
    }

    /**
     * 添加会员等级信息
     * 
     * @param list
     * @return
     */
    @Override
    public void insertMemberRate(MemberRate memberRate)
    {

        memberRateMapper.insertMemberRate(memberRate);
    }

    @Override
    public MemberRate queryMemberRateByIntegral(Map<String, Object>params)
    {
        return memberRateMapper.queryMemberRateByIntegral(params);
    }
    

    /**
     * 逻辑删除
     * @param memberRate
     * @return
     */
    @Override
    public void updateMemberRateIsdeleted(MemberRate memberRate){

        memberRateMapper.updateMemberRateIsdeleted(memberRate);
    }

    @Override
    public MemberRate queryMemberRateByIntrgralMin(Map<String, Object>params)
    {
        return memberRateMapper.queryMemberRateByIntrgralMin(params);
    }
}
