package com.xiangrui.lmp.business.admin.memberrate.contorller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;
import com.xiangrui.lmp.business.admin.memberrate.service.MemberRateService;
import com.xiangrui.lmp.business.base.BaseMemberRate;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/memberRate")
public class MemberRateController
{
    /**
     * 检索条件 记录
     */
    private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

    @Autowired
    private MemberRateService memberRateService;

    /**
     * 会员等级列表
     * 
     * @return
     */
    @RequestMapping("/memberRateList")
    public String memberRateList(HttpServletRequest request)
    {

        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);
        request.setAttribute("pageView", pageView);
        params.put("isdeleted", BaseMemberRate.ISDELETED_NO);

        List<MemberRate> memberRateList = memberRateService
                .queryAllMemberRate(params);

        request.setAttribute("memberRateList", memberRateList);

        return "back/memberRateList";
    }

    /**
     * 会员等级查询
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping("/search")
    public String search(HttpServletRequest request, String rate_type)
    {

        // 分页查询
        PageView pageView = null;

        Map<String, Object> params = new HashMap<String, Object>();

        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
            params = (Map<String, Object>) request.getSession().getAttribute(
                    SESSION_KEY_SEARCH_CONDITION);
        } else
        {
            pageView = new PageView(1);

            params.put("rate_type", rate_type);
            params.put("isdeleted", BaseMemberRate.ISDELETED_NO);
            request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION,
                    params);
        }
        params.put("pageView", pageView);

        List<MemberRate> memberRateList = memberRateService
                .queryAllMemberRate(params);

        request.setAttribute("pageView", pageView);
        request.setAttribute("memberRateList", memberRateList);

        // 查询条件保持
        request.setAttribute("params", params);

        return "back/memberRateList";
    }

    /**
     * 会员等级信息修改
     * 
     * @param request
     * @param rate_id
     * @return
     */
    @RequestMapping("/modify")
    public String modify(HttpServletRequest request, String rate_id)
    {

        MemberRate memberRate = memberRateService
                .queryMemberRateByRateId(Integer.parseInt(rate_id));

        request.setAttribute("memberRate", memberRate);

        ServletContext applicationg = request.getSession().getServletContext();
        applicationg.setAttribute("TariffHomePageList", null);
        // 前台重量查询价格的会员有变化,需要清空会员信息
        applicationg.setAttribute("MemberRateIdHomePageList", null);
        return "back/updateMemberRate";
    }

    /**
     * 会员等级信息修改保存
     * 
     * @param request
     * @param memberRate
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public String update(HttpServletRequest request, MemberRate memberRate)
    {
//        // 更新后的会员积分 != 0
//        if (memberRate.getIntegral_min() != 0)
//        {
//            // 提取会员的名称,避免匹配数据是遗落
//            String rate_name = memberRate.getRate_name();
//
//            // 更新前的会员数据
//            memberRate = memberRateService.queryMemberRateByRateId(memberRate
//                    .getRate_id());
//            // 更新前的会员积分为0 是基础会员,不能够删除
//            if (memberRate.getIntegral_min() == 0)
//            {
//                return "2";
//            }
//
//            // 将可能有修改的会员名称重新补到带修改的会员对象中
//            memberRate.setRate_name(rate_name);
//        }
        memberRateService.updateMemberRate(memberRate);

        // 注销前台资费出的信息,前台刷新可以重新查询
        ServletContext applicationg = request.getSession().getServletContext();
        applicationg.setAttribute("TariffHomePageList", null);
        // 前台重量查询价格的会员有变化,需要清空会员信息
        applicationg.setAttribute("MemberRateIdHomePageList", null);
        return "1";
    }

    /**
     * 删除
     * 
     * @param request
     * @param memberRate
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public String delete(HttpServletRequest request, MemberRate memberRate)
    {
        memberRate = memberRateService.queryMemberRateByRateId(memberRate
                .getRate_id());
        // 会员积分为0 是基础会员,不能够删除
        if (memberRate.getIntegral_min() == 0)
        {
            return "2";
        }

        memberRate.setIsdeleted(BaseMemberRate.ISDELETED_YES);
        memberRateService.updateMemberRateIsdeleted(memberRate);

        // 注销前台资费出的信息,前台刷新可以重新查询
        ServletContext applicationg = request.getSession().getServletContext();
        applicationg.setAttribute("TariffHomePageList", null);
        // 前台重量查询价格的会员有变化,需要清空会员信息
        applicationg.setAttribute("MemberRateIdHomePageList", null);
        return "1";
    }

    /**
     * 会员等级名称重复检查
     * 
     * @param request
     * @param rate_name
     * @return
     */
    @RequestMapping("/checkRateName")
    @ResponseBody
    public String checkRateName(HttpServletRequest request, String rate_name,
            String rate_type)
    {

        String checkResult = BaseMemberRate.RATE_NAME_NOTEXIST;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rate_type", rate_type);
        params.put("rate_name", rate_name);

        MemberRate memberRate = memberRateService
                .queryMemberRateByRateName(params);
        // 会员等级名称存在
        if (null != memberRate)
        {

            checkResult = BaseMemberRate.RATE_NAME_EXIST;
        }

        return checkResult;
    }

    @RequestMapping("/checkRateIntrgralMin")
    @ResponseBody
    public String checkRateIntrgralMin(HttpServletRequest request,
            String integral_min, String rate_type)
    {
        String checkResult = BaseMemberRate.INTRGRAL_MIN_NOTEXIST;
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rate_type", rate_type);
        params.put("integral_min", integral_min);

        MemberRate memberRates = memberRateService
                .queryMemberRateByIntrgralMin(params);
        if (null != memberRates)
        {

                checkResult = BaseMemberRate.INTRGRAL_MIN_EXIST;
        }

        return checkResult;
    }

    /**
     * 会员等级信息添加
     * 
     * @param request
     * @return
     */
    @RequestMapping("/add")
    public String add(HttpServletRequest request)
    {

        return "back/addMemberRate";
    }

    /**
     * 会员等级信息添加保存
     * 
     * @param request
     * @param memberRate
     * @return
     */
    @RequestMapping("/insert")
    @ResponseBody
    public String insert(HttpServletRequest request, MemberRate memberRate)
    {

        String result1 = checkRateName(null, memberRate.getRate_name(),
                String.valueOf(memberRate.getRate_type()));
        
        String result2 = checkRateIntrgralMin(null, String.valueOf(memberRate.getIntegral_min()),
                String.valueOf(memberRate.getRate_type()));
        
        if(BaseMemberRate.RATE_NAME_EXIST.equals(result1)){
            
           return "1"; 
        }
        
        if(BaseMemberRate.INTRGRAL_MIN_EXIST.equals(result2)){
            
            return "2";
        }

        memberRateService.insertMemberRate(memberRate);

        // 注销前台资费出的信息,前台刷新可以重新查询
        ServletContext applicationg = request.getSession().getServletContext();
        applicationg.setAttribute("TariffHomePageList", null);
        // 前台重量查询价格的会员有变化,需要清空会员信息
        applicationg.setAttribute("MemberRateIdHomePageList", null);
        return "0";
    }

}
