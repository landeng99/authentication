package com.xiangrui.lmp.business.admin.memberrate.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;

public interface MemberRateMapper
{

    /**
     * 分页查询所有等级信息
     * 
     * @param params
     * @return List
     */
    List<MemberRate> queryAllMemberRate(Map<String, Object> params);

    /**
     * 通过id查找等级信息
     * 
     * @param rate_id
     * @return MemberRate
     */
    MemberRate queryMemberRateByRateId(int rate_id);

    /**
     * 通过等级名称查找等级信息
     * 
     * @param params
     * @return MemberRate
     */
    MemberRate queryMemberRateByRateName(Map<String, Object> params);

    /**
     * 更新会员等级信息
     * 
     * @param memberRate
     * @return
     */
    void updateMemberRate(MemberRate memberRate);

    /**
     * 添加会员等级信息
     * 
     * @param list
     * @return
     */
    void insertMemberRate(MemberRate memberRate);

    /**
     * 根据积分获取某个等级的会员
     * @param params
     * @return
     */
    MemberRate queryMemberRateByIntegral(Map<String, Object>params);

    /**
     * 逻辑删除
     * @param memberRate
     * @return
     */
    void updateMemberRateIsdeleted(MemberRate memberRate);

    MemberRate queryMemberRateByIntrgralMin(Map<String, Object>params);
}
