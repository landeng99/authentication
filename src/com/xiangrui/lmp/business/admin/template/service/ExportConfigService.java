package com.xiangrui.lmp.business.admin.template.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.template.vo.ExportConfig;

public interface ExportConfigService
{

    /**
     * 添加模板
     * 
     * @param exportConfig
     * @return
     */
    void insertExportConfig(ExportConfig exportConfig);

    /**
     * 模板名称查询模板
     * 
     * @param template_name
     * @return
     */
    List<ExportConfig> selectExportConfigByName(String template_name);

    /**
     * ID名称查询模板
     * 
     * @param export_id
     * @return
     */
    ExportConfig selectExportConfigByExportId(int export_id);

    /**
     * 所有模板名称
     * 
     * @return
     */
    List<String> selectTemplateName();

    /**
     * 分页查询模板列表
     * 
     * @param params
     * @return
     */
    List<ExportConfig> queryAllExportConfig(Map<String, Object> params);

    /**
     * 更新模板
     * 
     * @param exportConfig
     * @return
     */
    void updateExportConfig(ExportConfig exportConfig);
    
    /**
     * 删除模板
     * 
     * @param export_id
     * @return
     */
    void deleteExportConfig(int export_id);
}
