package com.xiangrui.lmp.business.admin.template.service;

import java.util.List;

import com.xiangrui.lmp.business.admin.template.vo.ExcelColumn;

public interface ExcelColumnService
{

    /**
     * excel列名
     * 
     * @param type
     * @return
     */
    List<ExcelColumn> selectExcelColumnByType(int type);
}
