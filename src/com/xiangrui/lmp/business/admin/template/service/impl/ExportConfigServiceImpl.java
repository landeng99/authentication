package com.xiangrui.lmp.business.admin.template.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.template.mapper.ExportConfigMapper;
import com.xiangrui.lmp.business.admin.template.service.ExportConfigService;
import com.xiangrui.lmp.business.admin.template.vo.ExportConfig;

@Service(value = "exportConfigService")
public class ExportConfigServiceImpl implements ExportConfigService
{

    @Autowired
    private ExportConfigMapper exportConfigMapper;

    /**
     * 添加模板
     * 
     * @param exportConfig
     * @return
     */
    @Override
    public void insertExportConfig(ExportConfig exportConfig)
    {

        exportConfigMapper.insertExportConfig(exportConfig);

    }

    /**
     * 模板名称查询模板
     * 
     * @param template_name
     * @return
     */
    @Override
    public List<ExportConfig> selectExportConfigByName(String template_name)
    {

        return exportConfigMapper.selectExportConfigByName(template_name);
    }

    /**
     * ID名称查询模板
     * 
     * @param export_id
     * @return
     */
    @Override
    public ExportConfig selectExportConfigByExportId(int export_id)
    {
        return exportConfigMapper.selectExportConfigByExportId(export_id);
    }

    /**
     * 所有模板名称
     * 
     * 
     * @return
     */
    @Override
    public List<String> selectTemplateName()
    {
        return exportConfigMapper.selectTemplateName();
    }

    /**
     * 分页查询模板列表
     * 
     * @param params
     * @return
     */
    @Override
    public List<ExportConfig> queryAllExportConfig(Map<String, Object> params)
    {
        return exportConfigMapper.queryAllExportConfig(params);
    }

    /**
     * 更新模板
     * 
     * @param exportConfig
     * @return
     */
    @Override
    public void updateExportConfig(ExportConfig exportConfig)
    {
        exportConfigMapper.updateExportConfig(exportConfig);
    }
    
    /**
     * 删除模板
     * 
     * @param export_id
     * @return
     */
    @Override
    public void deleteExportConfig(int export_id){
        
        exportConfigMapper.deleteExportConfig(export_id);
    }
}
