package com.xiangrui.lmp.business.admin.template.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.template.mapper.ExcelColumnMapper;
import com.xiangrui.lmp.business.admin.template.service.ExcelColumnService;
import com.xiangrui.lmp.business.admin.template.vo.ExcelColumn;

@Service(value = "excelColumnService")
public class ExcelColumnServiceImpl implements ExcelColumnService
{

    @Autowired
    private ExcelColumnMapper excelColumnMapper;

    /**
     * excel列名
     * 
     * @param type
     * @return
     */
    @Override
    public List<ExcelColumn> selectExcelColumnByType(int type)
    {

        return excelColumnMapper.selectExcelColumnByType(type);
    }

}
