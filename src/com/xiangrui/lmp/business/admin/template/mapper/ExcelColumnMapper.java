package com.xiangrui.lmp.business.admin.template.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.template.vo.ExcelColumn;

public interface ExcelColumnMapper
{

    /**
     * excel列名
     * 
     * @param type
     * @return
     */
    List<ExcelColumn> selectExcelColumnByType(int type);

}
