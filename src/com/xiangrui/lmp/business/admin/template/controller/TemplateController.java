package com.xiangrui.lmp.business.admin.template.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.template.service.ExcelColumnService;
import com.xiangrui.lmp.business.admin.template.service.ExportConfigService;
import com.xiangrui.lmp.business.admin.template.vo.ColKeyAndValueModel;
import com.xiangrui.lmp.business.admin.template.vo.ExcelColumn;
import com.xiangrui.lmp.business.admin.template.vo.ExportConfig;
import com.xiangrui.lmp.util.JacksonHelper;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/template")
public class TemplateController
{

    @Autowired
    private ExcelColumnService excelColumnService;

    @Autowired
    private ExportConfigService exportConfigService;

    /**
     * 模板名称1：不存在
     */
    private static final String TEMPLATE_NAME_NOTEXIST = "1";

    /**
     * 模板名称0：存在
     */
    private static final String TEMPLATE_NAME_EXIST = "0";

    /**
     * excel列名类型1：导出模板
     */
    private static final int EXCELCOLUMN_TYPE = 1;

    /**
     * 模板列表初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/templateInit")
    public String templateList(HttpServletRequest request)
    {

        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);
        request.setAttribute("pageView", pageView);
        // 模版列表
        List<ExportConfig> exportConfigList = exportConfigService
                .queryAllExportConfig(params);
        // 模版名称下拉框
        List<String> templateNameList = exportConfigService
                .selectTemplateName();

        request.setAttribute("exportConfigList", exportConfigList);
        request.setAttribute("templateNameList", templateNameList);

        return "back/templateList";
    }

    /**
     * 模板列表查询
     * 
     * @param request
     * @param template_name
     * @return
     */
    @RequestMapping("/templateSearch")
    public String templateSearch(HttpServletRequest request,
            String template_name)
    {

        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);
        params.put("template_name", template_name);

        // 模版列表
        List<ExportConfig> exportConfigList = exportConfigService
                .queryAllExportConfig(params);
        // 模版名称下拉框
        List<String> templateNameList = exportConfigService
                .selectTemplateName();

        request.setAttribute("exportConfigList", exportConfigList);
        request.setAttribute("templateNameList", templateNameList);
        request.setAttribute("pageView", pageView);

        return "back/templateList";
    }

    /**
     * 修改模板初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/modifyTemplate")
    public String modifyTemplate(HttpServletRequest request, String export_id)
    {
        try
        {

            List<ExcelColumn> excelColumnList = excelColumnService
                    .selectExcelColumnByType(1);

            ExportConfig exportConfig = exportConfigService
                    .selectExportConfigByExportId(Integer.parseInt(export_id));
            String items = exportConfig.getItems();

            List<ColKeyAndValueModel> templateColumnList = JacksonHelper.fromJSONList(items,
                    ColKeyAndValueModel.class);
            
            for (ColKeyAndValueModel colKeyAndValueModel:templateColumnList)
            {
            // 从原始的列名中找出已经使用的列名
            for (int i = 0; i < excelColumnList.size(); i++)
            {
                String colKeyString = excelColumnList.get(i).getCol_key();
                
                // 去掉已经使用的列名
                if (colKeyString.equals(colKeyAndValueModel.getCol_key()))
                {
                    excelColumnList.remove(i);
                }
            }
            }
            request.setAttribute("excelColumnList", excelColumnList);
            request.setAttribute("templateColumnList", templateColumnList);
            request.setAttribute("exportConfig", exportConfig);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return "back/updateTemplate";
    }

    /**
     * 模板保存
     * 
     * @param request
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public String update(HttpServletRequest request, ExportConfig exportConfig)
    {

        exportConfigService.updateExportConfig(exportConfig);

        return null;
    }

    /**
     * 添加模板初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/addTemplate")
    public String addTemplate(HttpServletRequest request)
    {
        try
        {

            List<ExcelColumn> excelColumnList = excelColumnService
                    .selectExcelColumnByType(EXCELCOLUMN_TYPE);

            request.setAttribute("excelColumnList", excelColumnList);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return "back/addTemplate";
    }

    /**
     * 复位
     * 
     * @param request
     * @return
     */
    @RequestMapping("/refresh")
    @ResponseBody
    public List<ExcelColumn> refresh(HttpServletRequest request)
    {

        List<ExcelColumn> excelColumnList = excelColumnService
                .selectExcelColumnByType(1);

        return excelColumnList;
    }

    /**
     * 名称重复检查
     * 
     * @param request
     * @return
     */
    @RequestMapping("/checkTemplateName")
    @ResponseBody
    public String checkTemplateName(HttpServletRequest request,
            String template_name)
    {
        String checkResult = TEMPLATE_NAME_EXIST;

        List<ExportConfig> excelColumnList = exportConfigService
                .selectExportConfigByName(template_name);
        // 模版名称不存在
        if (excelColumnList == null || excelColumnList.size() == 0)
        {
            checkResult = TEMPLATE_NAME_NOTEXIST;
        }

        return checkResult;
    }

    /**
     * 模板保存
     * 
     * @param request
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public String save(HttpServletRequest request, ExportConfig exportConfig)
    {

        exportConfigService.insertExportConfig(exportConfig);
        
        return null;
    }
    
    /**
     * 模板保存
     * 
     * @param request
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public String delete(HttpServletRequest request, String export_id )
    {

        exportConfigService.deleteExportConfig(Integer.parseInt(export_id));

        return null;
    }
}
