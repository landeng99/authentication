package com.xiangrui.lmp.business.admin.breadcrumb.vo;

import java.io.Serializable;
import java.util.Map;

public class Breadcrumb implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3489066506865810254L;
	private String url;
	private Map<String, Object> params;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
}
