package com.xiangrui.lmp.business.admin.breadcrumb;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.breadcrumb.vo.Breadcrumb;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgAttachServiceService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;

@Controller
@RequestMapping("/admin/breadcrumb")
public class BreadcrumbController
{
	@Autowired
	private PkgAttachServiceService pkgAttachServiceService;
	@Autowired
	private PackageService packageService;

	@RequestMapping("/back")
	public ModelAndView back(HttpServletRequest req)
	{
		Stack<Breadcrumb> breadcrumbStack = (Stack<Breadcrumb>) req.getSession().getAttribute("breadcrumbStack");

		Breadcrumb breadcrumb = null;
		Breadcrumb currentBreadcrumb = null;

		// 当前访问层
		currentBreadcrumb = breadcrumbStack.peek();
		// 出栈
		breadcrumbStack.pop();

		// 获取上一层的访问
		breadcrumb = breadcrumbStack.peek();

		ModelMap mlMap = new ModelMap();
		Map<String, Object> params = breadcrumb.getParams();
		if (params != null)
		{
			for (Entry<String, Object> entry : params.entrySet())
			{
				mlMap.put(entry.getKey(), entry.getValue());
			}
		}
		String logistics_code = "";
		String package_id = "";
		Map<String, Object> currentparams = currentBreadcrumb.getParams();
		if (currentparams != null)
		{
			for (Entry<String, Object> entry : currentparams.entrySet())
			{
				if (entry.getKey().equals("logistics_code"))
				{
					logistics_code = StringUtils.trimToEmpty((String) entry.getValue());
				}
				else if (entry.getKey().equals("package_id"))
				{
					package_id = StringUtils.trimToEmpty((String) entry.getValue());
				}
			}
			if (!package_id.equals(""))
			{
				int intPackageId = Integer.parseInt(package_id);
				List<PkgAttachService> pkgAttachServiceList = pkgAttachServiceService.queryPkgAttachServiceByPackage(intPackageId);
				// 分箱服务不需要跳到磅重页面
				List<PkgAttachService> splitPkgAttachServiceList = pkgAttachServiceService.queryAllPkgAttachServiceByPackageGroup(intPackageId, AttachService.SPLIT_PKG_ID);
				if (splitPkgAttachServiceList != null && splitPkgAttachServiceList.size() > 0)
				{
					boolean isFinish = true;
					for (PkgAttachService pkgAttachService : splitPkgAttachServiceList)
					{
						if (pkgAttachService.getStatus() != 2)
						{
							isFinish = false;
						}
					}
					if (isFinish)
					{
						logistics_code = "";
					}
				}
				// 合箱服务需要跳到合箱后包裹的磅重页面
				PkgAttachService megerPkgAttachService = pkgAttachServiceService.queryAllPkgAttachServiceById(intPackageId, AttachService.MERGE_PKG_ID);
				if (megerPkgAttachService != null && megerPkgAttachService.getStatus() == 2)
				{
					Pkg pkg = packageService.queryPackageById(intPackageId);
					logistics_code = pkg.getLogistics_code();
				}
			}
		}
		return new ModelAndView("redirect:" + breadcrumb.getUrl() + "?query_logistics_code=" + logistics_code, mlMap);
	}

	private boolean isAllFinish(List<PkgAttachService> pkgAttachServiceList)
	{
		boolean isAllFinish = false;
		if (pkgAttachServiceList != null && pkgAttachServiceList.size() > 0)
		{
			isAllFinish = true;
			for (PkgAttachService pkgAttachService : pkgAttachServiceList)
			{
				if (pkgAttachService.getStatus() != 2)
				{
					isAllFinish = false;
				}
			}
		}
		return isAllFinish;
	}

	@RequestMapping("/go")
	public ModelAndView go(HttpServletRequest req)
	{

		Stack<Breadcrumb> breadcrumbStack = (Stack<Breadcrumb>) req.getSession().getAttribute("breadcrumbStack");

		Breadcrumb breadcrumb = null;

		breadcrumb = breadcrumbStack.peek();

		ModelMap mlMap = new ModelMap();

		Map<String, Object> params = breadcrumb.getParams();
		if (params != null)
		{
			for (Entry<String, Object> entry : params.entrySet())
			{
				mlMap.put(entry.getKey(), entry.getValue());
			}
		}
		return new ModelAndView("redirect:" + breadcrumb.getUrl(), mlMap);
	}

}
