package com.xiangrui.lmp.business.admin.security;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import com.xiangrui.lmp.business.admin.menu.service.MenuService;
import com.xiangrui.lmp.business.admin.menu.vo.Menu;
import com.xiangrui.lmp.business.admin.user.service.UserService;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.init.SystemParamUtil;
import com.xiangrui.lmp.util.content.SysContent;

/**
 * @author admin 2015-3-6
 */
public class ShiroRealm extends AuthorizingRealm
{

    private UserService userService;

    /*
     * 登录信息和用户验证信息验证(non-Javadoc)
     * 
     * @see
     * org.apache.shiro.realm.AuthenticatingRealm#doGetAuthenticationInfo(org
     * .apache.shiro.authc.AuthenticationToken)
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken token) throws AuthenticationException
    {

        String username = (String) token.getPrincipal(); // 得到用户名
        String password = new String((char[]) token.getCredentials()); // 得到密码

        if (null != username && null != password)
            
        {
              
            return new SimpleAuthenticationInfo(username, password, getName());
        } else
        {
            throw new AuthorizationException();
        }

    }

    /**
     * 菜单
     */
    @Autowired
    private MenuService menuService;
    /*
     * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用,负责在应用程序中决定用户的访问控制的方法(non-Javadoc)
     * 查询数据库获取用户权限， 并
     * 
     * @see
     * org.apache.shiro.realm.AuthorizingRealm#doGetAuthorizationInfo(org.apache
     * .shiro.subject.PrincipalCollection)
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection pc)
    {
        HttpServletRequest req = SysContent.getRequest();
        String path = req.getServletPath();

        // 获取路径,截取jsp文件名称
        path = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        Set<String> permissions = new HashSet<String>();

        Subject currentUser = SecurityUtils.getSubject();
        Session session = currentUser.getSession();
        User user = (User) session.getAttribute(SYSConstant.SESSION_USER);
        Map<Integer,List<Menu>> optionsMap=(Map<Integer,List<Menu>>) SystemParamUtil.getAuthority("optionsMap");
        Menu menu = new Menu();
        List<Menu> menuList = null;
		if (user!=null) {
			menu.setId(user.getUser_id());
			menuList = optionsMap.get(user.getUser_id());
		}
        if (menuList!=null&&menuList.size()>0) {
			for (Menu m : menuList) {
				String per = m.getPermission();
				if (null != per && !"".equals(per)) {
					if (per.indexOf(";") > -1) {
						String[] pers = per.split(";");
						for (String string : pers) {
							if (string.equals(path)) {
								permissions.add(m.getLink());
							}
						}
					} else {
						if (per.equals(path)) {
							permissions.add(m.getLink());
						}
					}
				}
			} 
		}
		/*  permissions.add("index:edit");*/
        info.addStringPermissions(permissions);
        return info;
    }

}
