package com.xiangrui.lmp.business.admin.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * API跨域访问拦截器
 * @author Will
 *
 */
public class AccessControlHandlerInterceptor extends HandlerInterceptorAdapter
{
    private static String ADMIN_INDEX_URL = "/api/";

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception
    {
        System.out.println("是否API接口 ="+request.getServletPath());
        // 是否API接口
        if (request.getServletPath().contains(ADMIN_INDEX_URL))
        {
        	System.out.println("是否API接口 ---------");
        	response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
            response.setHeader("Access-Control-Max-Age", "3600");
            response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
        }

        return true;
    }
}
