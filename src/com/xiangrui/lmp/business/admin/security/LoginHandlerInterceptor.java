package com.xiangrui.lmp.business.admin.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.menu.vo.Menu;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.init.SystemParamUtil;

/**
 * 
 * 类名称：LoginHandlerInterceptor.java 类描述：
 * 
 * @author FH 作者单位： 联系方式： 创建时间：2015年1月1日
 * @version 1.6
 */
public class LoginHandlerInterceptor extends HandlerInterceptorAdapter
{

    private static String ADMIN_INDEX_URL = "/admin/manager/index";
    
    /**
     * 前台包裹
     */
    @Autowired
    private PkgService frontPkgService;
    
    /**
     * 优惠券处理服务
     */
    @Autowired
    private CouponService couponService;

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception
    {

        // 是否是后台主页
        if (ADMIN_INDEX_URL.equals(request.getServletPath()))
        {
            // 获取登录用户
            Subject currentUser = SecurityUtils.getSubject();
            Session session = currentUser.getSession();
            User user = (User) session.getAttribute(SYSConstant.SESSION_USER);

            if (user != null)
            {
                // 第一级菜单
                List<Menu> listParent = new ArrayList<Menu>();
                Map<Integer, List<Menu>> userMenuMap = (Map<Integer, List<Menu>>) SystemParamUtil
                        .getAuthority("userMenuMap");
                listParent = userMenuMap.get(user.getUser_id());
                request.setAttribute("listParent", listParent);
            }
        }
        else{
            HttpSession session = request.getSession();
            if (null != session)
            {
                FrontUser frontUser = (FrontUser) session
                        .getAttribute("frontUser");
                System.out.println(frontUser);
                if (null != frontUser)
                {
                    // 包裹总数
                    Integer pkgCount = frontPkgService.queryCountByUserId(frontUser.getUser_id());
                    request.setAttribute("pkgCount", pkgCount);
                    // 可用优惠券数
                    Integer countAvailable = couponService.sumAvaliableQuantityByUserId(frontUser.getUser_id());
                    request.setAttribute("countAvailable", countAvailable);
                }
            }
        }

        return true;
    }

}
