package com.xiangrui.lmp.business.admin.security;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.xiangrui.lmp.business.admin.menu.vo.Menu;
import com.xiangrui.lmp.business.admin.role.vo.Role;

public class FilterChainFactoryBean implements FactoryBean<Map<String, String>> {

	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * 权限map
	 */
	@Override
	public Map<String, String> getObject() throws Exception {
		Map<String, String> map = new LinkedHashMap<String, String>();

		map.put("/admin/login", "anon");
		map.put("/admin/wellcome", "anon");
		map.put("/admin/logout", "anon");
		map.put("/callback/receive", "anon");
		map.put("/admin/denied", "anon");
		map.put("/back/js/**", "anon");
		map.put("/back/images/**", "anon");
		map.put("/back/css/**", "anon");
		map.put("/js/**", "anon");
		map.put("/authImg", "anon");
		map.put("/resource/**", "anon");
		map.put("/**", "authc");
		return map;
	}

	@Override
	public Class<?> getObjectType() {
		return Map.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
