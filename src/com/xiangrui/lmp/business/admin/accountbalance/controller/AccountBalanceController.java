package com.xiangrui.lmp.business.admin.accountbalance.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.accountbalance.service.AccountBalanceService;
import com.xiangrui.lmp.business.admin.accountbalance.vo.AccountBalance;
import com.xiangrui.lmp.business.admin.accountlog.service.AccountLogService;
import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.base.BaseAccountBalance;
import com.xiangrui.lmp.util.PageView;

/**
 * 金额统计
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午12:23:07
 *         </p>
 */
@Controller
@RequestMapping("/admin/accountBalance")
public class AccountBalanceController
{
    /**
     * 金额统计service
     */
    @Autowired
    private AccountBalanceService accountBalanceService;
    /**
     * 金额消费service
     */
    @Autowired
    private AccountLogService accountLogService;

    /**
     * 首次查询,没有总计数据
     * 
     * @param request
     * @return
     */
    @RequestMapping("/queryAllOne")
    public String indexFink(HttpServletRequest request)
    {
        return queryAll(request, null);
    }

    /**
     * 首次查询,仅有总计数据
     * 
     * @param request
     * @return
     */
    @RequestMapping("/queryAllCountOne")
    public String indexFinkCount(HttpServletRequest request)
    {
        return queryAllCount(request, null);
    }

    /**
     * 查询账号记录,带条件,没有总计数据
     * 
     * @param request
     * @param accountLog
     * @return
     */
    @RequestMapping("queryAll")
    private String queryAll(HttpServletRequest request,
            AccountBalance accountBalance)
    {
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        List<AccountBalance> list = queryPageList(pageView, accountBalance,
                BaseAccountBalance.IS_USER_ID_PARAMS_TRUE);
        request.setAttribute("pageView", pageView);
        request.setAttribute("accountBalanceLists", list);
        return "back/accountBalanceList";
    }
    
    /**
     * 现金充值
     * @param request
     * @return
     */
    @RequestMapping("/cashRecharge")
    public String cashRecharge(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("back_user");
        request.setAttribute("curUser", user.getUsername());
        return "back/cashRecharge";
    }

    /**
     * 查询账号记录,带条件,仅有总计数据
     * 
     * @param request
     * @param accountBalance
     * @return
     */
    @RequestMapping("queryAllCount")
    private String queryAllCount(HttpServletRequest request,
            AccountBalance accountBalance)
    {
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        List<AccountBalance> list = queryPageList(pageView, accountBalance,
                BaseAccountBalance.IS_USER_ID_PARAMS_FALSE);
        request.setAttribute("pageView", pageView);
        request.setAttribute("accountBalanceLists", list);
        return "back/accountBalanceCountList";
    }

    /**
     * 获取查询所需要的对账记录集合
     * 
     * @param pageView
     *            分页对象
     * @param accountBalance
     *            查询条件对象
     * @param type
     *            AccountBalance的IS_USER_ID三个值的值
     * @return
     */
    private List<AccountBalance> queryPageList(PageView pageView,
            AccountBalance accountBalance, int type)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);
        params.put("IS_USER_ID", type);
        // 当参数不是空的情况,视为条件查询
        if (null != accountBalance)
        {
            try
            {
                params.put("USER_NAME", new String(accountBalance
                        .getUser_name().getBytes("ISO-8859-1"), "UTF-8"));
            } catch (UnsupportedEncodingException e)
            {
                e.printStackTrace();
            }
            params.put("START_TIME", accountBalance.getStart_time());
            params.put("END_TIME", accountBalance.getEnd_time());
        }

        List<AccountBalance> list = accountBalanceService.queryAll(params);
        return list;
    }

    /**
     * 进入详细信息
     * 
     * @return
     */
    @RequestMapping("toFinkAccountBalance")
    public String toFinkAccountLog(HttpServletRequest request,
            AccountBalance accountBalance)
    {
        // 是否有id
        if (accountBalance.getBalance_id() > 0)
        {

            // 验证数据
            accountBalance = accountBalanceService.queryAllId(accountBalance
                    .getBalance_id());
            request.setAttribute("accountBalancePojo", accountBalance);
            request.setAttribute("accountLogLists", null);
            request.setAttribute("accountBalanceLists", null);

            // 有用户id是查看的当前某个用户的记录
            if (accountBalance.getUser_id() > 0)
            {

                Map<String, Object> params = new HashMap<String, Object>();
                params.put("user_id", accountBalance.getBalance_id());
                params.put("start_time", accountBalance.getStart_time());
                params.put("end_time", accountBalance.getEnd_time());

                List<AccountLog> accountLogLists = this.accountLogService
                        .queryAccountLogForAccountBalance(params);
                request.setAttribute("accountLogLists", accountLogLists);
            } else
            {

                // 没有id是查询的某月的所有记录
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("START_TIME", accountBalance.getStart_time());
                params.put("END_TIME", accountBalance.getEnd_time());
                // 用户id大于0的条件,表示所有的记录,不包括总计数据
                params.put("IS_USER_ID",
                        BaseAccountBalance.IS_USER_ID_PARAMS_TRUE);

                List<AccountBalance> accountBalanceLists = accountBalanceService
                        .queryAll(params);

                request.setAttribute("accountBalanceLists", accountBalanceLists);
            }
            return "back/accountBalanceInfo";
        } else
        {
            return indexFink(request);
        }
    }
}
