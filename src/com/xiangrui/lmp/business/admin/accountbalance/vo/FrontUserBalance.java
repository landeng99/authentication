package com.xiangrui.lmp.business.admin.accountbalance.vo;

import com.xiangrui.lmp.business.base.BaseFrontUser;

/**
 * 金额统计专用前台用户
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午2:20:00
 * </p>
 */
public class FrontUserBalance extends BaseFrontUser
{

    private static final long serialVersionUID = 1L;

}
