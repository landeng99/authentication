package com.xiangrui.lmp.business.admin.accountbalance.vo;

import com.xiangrui.lmp.business.base.BaseAccountBalance;

/**
 * 金额统计实体
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午2:14:38
 * </p>
 */
public class AccountBalance extends BaseAccountBalance
{

    private static final long serialVersionUID = 1L;

    /**
     * 账户资金异常
     */
    public boolean isBalanceException()
    {
        if (Math.abs(super.getDifference()) > 0.00001)
        {
            return true;
        }
        return false;
    }

    /**
     * 获取开始时间 年月日那段
     * 
     * @return
     */
    public String getTimeStart()
    {
        return this.getStart_time().substring(0, 11);
    }

    /**
     * 获取结束时间 年月日那段
     * 
     * @return
     */
    public String getTimeEnd()
    {
        return this.getEnd_time().substring(0, 11);
    }

    /**
     * 统计的人员信息
     */
    private FrontUserBalance frontUserBalance;

    public FrontUserBalance getFrontUserBalance()
    {
        return frontUserBalance;
    }

    public void setFrontUserBalance(FrontUserBalance frontUserBalance)
    {
        this.frontUserBalance = frontUserBalance;
    }

    /**
     * 统计的人员名称,实际读取的frontUserBalance.user_name
     * 
     * @return
     */
    public String getUser_name()
    {
        if (null == this.getFrontUserBalance())
            this.setFrontUserBalance(new FrontUserBalance());
        return this.getFrontUserBalance().getUser_name();
    }

    /**
     * 统计的人员名称,实际赋值给frontUserBalance.user_name
     * 
     * @param user_name
     */
    public void setUser_name(String user_name)
    {
        if (null == this.getFrontUserBalance())
            this.setFrontUserBalance(new FrontUserBalance());
        this.getFrontUserBalance().setUser_name(user_name);
    }

    public AccountBalance()
    {
        super();
    }

    /**
     * 构造方法
     * 
     * @param balance_id
     *            id
     * @param user_id
     *            用户id
     * @param last_balance
     *            上期结余
     * @param curr_balance
     *            本期结余
     * @param start_time
     *            开始时间
     * @param total_expend
     *            消费金额
     * @param total_recharge
     *            充值金额
     * @param total_withdraw
     *            索赔金额
     * @param total_claim_cost
     *            提现金额
     * @param end_time
     *            结束时间
     * @param difference
     *            差额
     */
    public AccountBalance(int balance_id, int user_id, double last_balance,
            double curr_balance, String start_time, double total_expend,
            double total_recharge, double total_withdraw,
            double total_claim_cost, String end_time, double difference)
    {
        super(balance_id, user_id, last_balance, curr_balance, start_time,
                total_expend, total_recharge, total_withdraw, total_claim_cost,
                end_time, difference);
    }

    private int is_user_id;
    
    public void setIs_user_id(int is_user_id){
        this.is_user_id = is_user_id;
    }
    
    public int getIs_user_id()
    {
        return is_user_id;
    }

}
