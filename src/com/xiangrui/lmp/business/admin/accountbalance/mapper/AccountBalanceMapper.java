package com.xiangrui.lmp.business.admin.accountbalance.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.accountbalance.vo.AccountBalance;

/**
 * 金额统计
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午12:25:58
 * </p>
 */
public interface AccountBalanceMapper
{

    /**
     * 查询所有
     * 
     * @param params
     * @return
     */
    List<AccountBalance> queryAll(Map<String, Object> params);

    /**
     * 根据对账信息查一条记录
     * 
     * @param balance_id
     * @return
     */
    AccountBalance queryAllId(int balance_id);

    /**
     * 记录信息的存储过程
     */
    void proAcountBalancePen();

    /**
     * 查询某一个月的 金额使用情况
     * 
     * @param params
     *            仅仅是 START_TIME和END_TIME
     *            <p>
     *            START_TIME = '2015-05-01'
     *            </p>
     *            <p>
     *            END_TIME = '2015-06-01'
     *            </p>
     * @return
     */
    AccountBalance queryAllTotal(Map<String, Object> params);

    /**
     * 查询某一天的 上期余额,本期余额,差额
     * 
     * @param params
     *            仅仅是 START_TIME
     *            <p>
     *            START_TIME = '2015-05-01'
     *            </p>
     * @return
     */
    AccountBalance queryAllBalance(Map<String, Object> params);

    /**
     * 新增金额统计数据
     * 
     * @param params
     */
    void add(AccountBalance params);
    
    /**
     * 新增金额统计数据(跳过日志监控)
     * 
     * @param params
     */
    void logAccountBalanceNew(AccountBalance params);
    void logAccountBalanceOld(AccountBalance params);
}
