package com.xiangrui.lmp.business.admin.accountbalance.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.accountbalance.mapper.AccountBalanceMapper;
import com.xiangrui.lmp.business.admin.accountbalance.service.AccountBalanceService;
import com.xiangrui.lmp.business.admin.accountbalance.vo.AccountBalance;

/**
 * 金额统计
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午1:59:34
 * </p>
 */
@Service("accountBalanceService")
public class AccountBalanceServerImpl implements AccountBalanceService
{

    /**
     * 金额统计
     */
    @Autowired
    private AccountBalanceMapper accountBalanceMapper;

    /**
     * 查询所有金额统计数据,可携带带条件的
     */
    @Override
    public List<AccountBalance> queryAll(Map<String, Object> params)
    {
        return accountBalanceMapper.queryAll(params);
    }

    /**
     * 按照金额统计id查询某个金额统计记录
     */
    @Override
    public AccountBalance queryAllId(int balance_id)
    {
        return accountBalanceMapper.queryAllId(balance_id);
    }

    /**
     * 金额统计计算,使用的是存储过程
     */
    @Override
    public void proAcountBalancePen()
    {
        accountBalanceMapper.proAcountBalancePen();
    }

    /**
     * 查询某一个月的 金额使用情况
     * 
     * @param params
     *            仅仅是 START_TIME和END_TIME
     *            <p>
     *            START_TIME = '2015-05-01'
     *            </p>
     *            <p>
     *            END_TIME = '2015-06-01'
     *            </p>
     * @return
     */
    @Override
    public AccountBalance queryAllTotal(Map<String, Object> params)
    {
        return accountBalanceMapper.queryAllTotal(params);
    }

    /**
     * 查询某一天的 上期余额,本期余额,差额
     * 
     * @param params
     *            仅仅是 START_TIME
     *            <p>
     *            START_TIME = '2015-05-01'
     *            </p>
     * @return
     */

    @Override
    public AccountBalance queryAllBalance(Map<String, Object> params)
    {
        return accountBalanceMapper.queryAllBalance(params);
    }

    /**
     * 添加一条金额统计
     */
    @Override
    public void add(AccountBalance params)
    {
        accountBalanceMapper.add(params);
    }

    @Override
    public void logAccountBalance(AccountBalance balanceTotal)
    {
        if(balanceTotal.getBalance_id() == 0){
            accountBalanceMapper.logAccountBalanceNew(balanceTotal);
        }else{
            accountBalanceMapper.logAccountBalanceOld(balanceTotal);
        }
    }
}
