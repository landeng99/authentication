package com.xiangrui.lmp.business.admin.tax.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.tax.service.TaxPkgGoodsService;
import com.xiangrui.lmp.business.admin.tax.util.ExcelValidateUtils;
import com.xiangrui.lmp.business.admin.tax.vo.Goods;
import com.xiangrui.lmp.business.admin.tax.vo.GoodsList;
import com.xiangrui.lmp.business.admin.tax.vo.TaxPkgGoods;
import com.xiangrui.lmp.business.admin.tax.vo.TaxPkgPackage;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.init.AppServerUtil;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.newExcel.ExcelReadUtil;
import com.xiangrui.lmp.util.newExcel.SheetConfig;

/**
 * 关税管理
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午5:32:23
 *         </p>
 */
@Controller
@RequestMapping("/admin/taxPkgGoods")
public class TaxPkgGoodsController
{

    Logger logger = Logger.getLogger(TaxPkgGoodsController.class);

    private static final String RELATIVELY_PATH = "resource" + File.separator
            + "upload" + File.separator;
    /**
     * 上传图片在服务器存放路径
     */
    private static final String UPLOAD_IMG_PATH = AppServerUtil.getWebRoot()
            + RELATIVELY_PATH;

    /**
     * Excel列番号初始化 ：一个不存在的列番号
     */
    private static final int COL_NUMBER_INIT = 9999999;

    /**
     * Excel导入的表名默认值
     */
    private static final String SHEET_NAME = "Sheet1";

    private static final String LOGISTICS_CODE = "公司运单号";

//    private static final String TAX_NUMBER = "税单单号";

//    private static final String TAXRATEPERCENTAGE = "税率";

//    private static final String TAX_PAYMENT_TOTAL = "完税总额";

    private static final String CUSTOMS_COST = "税款金额";

    private static final String GOODS_NAME = "内件名称";

//    private static final String GOODS_TAX_NUMBER = "申报数量";

//    private static final String UNIT = "单位";

    private static final String TAX_STATUS = "审核状态";

    /**
     * 1单号为空
     */
    private static final int ERROR_CODE_NULL = 1;

    /**
     * 2税单状态为空
     */
    private static final int ERROR_STATUS_NULL = 2;

    /**
     * 3单号重复
     */
    private static final int ERROR_CODE_DUPLICATE = 3;

    /**
     * 4，单号不存
     */
    private static final int ERROR_CODE_NOTFOUND = 4;

    /**
     * 0;{ "1", "正常数据" }<br>
     * 1;{ "2", "税单号已在系统中的包裹里使用" }<br>
     * 2;{ "3", "税单号出现在多个包裹中" }<br>
     * 3;{ "4", "在包裹中出现的多个税单号" }<br>
     * 4;{ "5", "包裹的运单号在系统中不存在" }<br>
     * 5;{ "6", "包裹的审核状态存在重复" }<br>
     * 6;{ "7", "包裹的商品信息不存在" }<br>
     * 7;{ "8", "包裹的商品信息不完整" }<br>
     * 8;{ "9", "包裹的商品信息不完善" }<br>
     */
    private static final String[][] ARY_TYPE_MASSAGE = { { "1", "正常数据" },
            { "2", "税单号已在系统中的包裹里使用" }, { "3", "税单号出现在多个包裹中" },
            { "4", "在包裹中出现的多个税单号" }, { "5", "包裹的运单号在系统中不存在" },
            { "6", "包裹的审核状态存在重复" }, { "7", "包裹的商品信息不存在" }, { "8", "包裹的内件不全" },
            { "9", "包裹的内件不全" } };
    /**
     * 关税操作
     */
    @Autowired
    private TaxPkgGoodsService taxPkgGoodsService;

    @Autowired
    private PackageService packageService;

    /**
     * 首次查询
     * 
     * @param request
     * @return
     */
    @RequestMapping("/queryAllOne")
    public String queryAllOne(HttpServletRequest request)
    {
        request.getSession().removeAttribute("objectParams");
        return queryAll(request, null, 1, true);
    }

    /**
     * 正常查询
     * 
     * @param request
     * @param taxPkgGoods
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request, TaxPkgGoods taxPkgGoods,
            int taxPage, boolean isTaxPage)
    {
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        boolean isPageIndex = !"".equals(pageIndex) && pageIndex != null;
        if (isPageIndex)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            if (isTaxPage)
            {
                pageView = new PageView(taxPage);
            } else
            {
                pageView = new PageView(1);
            }
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);

        // 首次查询 当没有有request范围内的参数aAll的设置时,不是首次查询
        String logistics_code = request.getParameter("logistics_code");
        if(!StringUtils.isEmpty(logistics_code)){
        	params.put("LOGISTICS_CODE", logistics_code);
        	taxPkgGoods = new TaxPkgGoods();
        	taxPkgGoods.setLogistics_code(logistics_code);
        	request.getSession().setAttribute("objectParams", taxPkgGoods);
        }
        if (null != taxPkgGoods)
        {	
            params.put("LOGISTICS_CODE", taxPkgGoods.getLogistics_code());
            params.put("STATUS", taxPkgGoods.getStatus());
            params.put("PAY_STATUS", taxPkgGoods.getPay_status());
            params.put("pay_status_custom", taxPkgGoods.getPay_status_custom());
            params.put("TIME_START", taxPkgGoods.getTime_start());
            params.put("TIME_END", taxPkgGoods.getTime_end());
            request.getSession().setAttribute("objectParams", taxPkgGoods);
        }

        List<TaxPkgGoods> list = taxPkgGoodsService.queryAll(params);
        request.setAttribute("pageView", pageView);
        request.setAttribute("taxPage", pageView.getPageNow());
        request.setAttribute("taxPkgGoodsLists", list);
        return "back/taxPkgGoodsList";
    }

    /**
     * 进入修改页面 通过包裹编号查询包裹下的所有单号
     * 
     * @param request
     * @param flink
     * @return
     */
    @RequestMapping("/toUpdateTaxPkgGoods")
    public String toUpdateTaxPkgGoods(HttpServletRequest request, String goods_id,String packageId,
            int taxPage)
    {
    	if(!StringUtils.isEmpty(goods_id)&&!StringUtils.isEmpty(packageId)){
    		request.setAttribute("taxPage", taxPage);
            // 匹配数据库
    		List<TaxPkgPackage> taxPkgPackages = new ArrayList<>();
    		TaxPkgPackage pkPackage = new TaxPkgPackage();
    		pkPackage.setLogistics_code(goods_id);
    		taxPkgPackages.add(pkPackage);
    		List<TaxPkgGoods> goodsType = this.taxPkgGoodsService.finkTaxPkgInGoodsNameToList(taxPkgPackages);
            request.setAttribute("taxPkgGoodsPojo", goodsType);
            request.setAttribute("packageCode", goods_id);
            request.setAttribute("packageId", packageId);
    	}
        return "back/updateTaxPkgGoods";
    }

    /**
     * 修改税率(基本上只是改动税率getTax_rate,变动附带的税价taxes_price)
     * 
     * @param request
     * @param goodsType
     * @return
     */
    @RequestMapping("/updateTaxPkgGoodsSubmit")
    public String updateTaxPkgGoods(HttpServletRequest request,
            TaxPkgGoods goodsType, int taxPage, boolean isTaxPage)
    {
        try
        {
            float taxRate = goodsType.getTax_rate();
            String unit = new String(
                    goodsType.getUnit().getBytes("ISO-8859-1"), "UTF-8");

            goodsType = taxPkgGoodsService.queryAllId(goodsType.getGoods_id());

            // 单位保存
            goodsType.setUnit(unit);

            // 计算税率变法 税率比例
            double shuilvbi = 0;
            if (taxRate != 0)
            {
                shuilvbi = NumberUtils.divide(taxRate, goodsType.getTax_rate(),
                        4);
            }

            // 变化前的完税总额
            double changeBeforeMoney = goodsType.getTax_payment_total();

            // 变化后的完税总额
            double changeAfterMoney = 0;
            if (taxRate != 0)
            {
                changeAfterMoney = NumberUtils.multiply(changeBeforeMoney,
                        shuilvbi, 4);
            }

            goodsType.setTax_payment_total(Float.parseFloat(changeAfterMoney
                    + ""));

            // 包裹对象的id 和税款总额(穿个税率变化值)
            TaxPkgPackage taxPkgPackage = new TaxPkgPackage();
            taxPkgPackage.setPackage_id(goodsType.getPackage_id());

            double customsCostDiff = NumberUtils.subtract(changeBeforeMoney,
                    changeAfterMoney);
            // 完税总额的差价
            taxPkgPackage.setCustoms_cost(Float.parseFloat(NumberUtils
                    .subtract(taxPkgPackage.getCustoms_cost(), customsCostDiff)
                    + ""));

            goodsType.setTax_rate(taxRate);
            // 修改税率和税款总额
            taxPkgGoodsService.updateTaxPkgGoods(goodsType);

            // 修改税款总额
            taxPkgGoodsService.updateTaxPkgPackage(taxPkgPackage);

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        TaxPkgGoods taxPkgGoods = (TaxPkgGoods) request.getSession()
                .getAttribute("objectParams");

        return queryAll(request, taxPkgGoods, taxPage, isTaxPage);

    }

    /**
     * 进入添加税票页面
     * 
     * @param request
     * @param flink
     * @return
     */
    @RequestMapping("/toAddTaxPkgGoods")
    public String toAddTaxPkgGoods(HttpServletRequest request)
    {
        request.setAttribute("EXCEL_TABLE", null);
        return "back/addTaxPkgGoods";
    }

    /**
     * hsjingbest1@163.com nitamaqusi 导入清关税单，同时更新包裹的状态为清关中
     * 
     * @param request
     * @return
     */
    @RequestMapping("/excel")
    @ResponseBody
	public String excel(HttpServletRequest request)
	{
		// Map<String,Object> reMap = new HashMap<String, Object>();

		StringBuffer logMassage = new StringBuffer();

		MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
		MultipartFile excelFile = req.getFile("excel");

		// 文件名
		String fileName = excelFile.getOriginalFilename();

		// 后缀名
		String exName = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());

		Map<String, String> colMap = new HashMap<String, String>();
		//公司单号
		colMap.put("logistics_code", LOGISTICS_CODE);
		//税单单号
		//colMap.put("tax_number", TAX_NUMBER);
		//税率
		//colMap.put("taxRatePercentage", TAXRATEPERCENTAGE);
		//完税总额
		//colMap.put("tax_payment_total", TAX_PAYMENT_TOTAL);
		//税款金额
		colMap.put("customs_cost", CUSTOMS_COST);
		//内件名称
		//colMap.put("goods_name", GOODS_NAME);
		//申报数量
		//colMap.put("goods_tax_number", GOODS_TAX_NUMBER);
		//单位
		//colMap.put("unit", UNIT);
		
		try
		{

			Workbook workbook = null;

			if ("xls".equals(exName))
			{
				logger.info("Excel版本 version = 2003");
				workbook = new HSSFWorkbook(excelFile.getInputStream());
			} else
			{
				workbook = new XSSFWorkbook(excelFile.getInputStream());
			}

			SheetConfig sheetConfig = new SheetConfig();

			// 读取的起始row
			int startRow = 0;

			// 读取的sheet索引
			int sheetIndex = 0;
			sheetConfig.setSheetIndex(sheetIndex);
			sheetConfig.setStartRow(startRow);

			List<Map<String, Object>> list = ExcelReadUtil.parseExcel(workbook,
					colMap, sheetConfig);

			// 税单集合 物品存放的list集合
			List<TaxPkgGoods> taxPkgGoodsLists = new ArrayList<TaxPkgGoods>();
			// 税单结合 包裹存放的list集合
			//List<TaxPkgPackage> taxPkgPackageLists = new ArrayList<TaxPkgPackage>();

			Map<String, TaxPkgPackage> taxPkgPackageMap = new HashMap<String, TaxPkgPackage>();

			for (Map<String, Object> map : list)
			{

				String logistics_code = String.valueOf(map.get("logistics_code")).trim();

//				String tax_number = String.valueOf(map.get("tax_number"))
//						.trim();
//				if (null == tax_number || "".equals(tax_number))
//				{
//					continue;
//				}

				TaxPkgGoods taxPkgGoods = new TaxPkgGoods();
//				taxPkgGoods.setTax_number(tax_number);

				taxPkgGoods.setLogistics_code(logistics_code);

//				taxPkgGoods.setTaxRatePercentage(
//						String.valueOf(map.get("taxRatePercentage")).trim());
//				taxPkgGoods.setGoods_name(
//						String.valueOf(map.get("goods_name")).trim());
//				taxPkgGoods.setTax_payment_total(Float.parseFloat(
//						String.valueOf(map.get("tax_payment_total"))));

				taxPkgGoods.setCustoms_cost(Float.parseFloat(String.valueOf(map.get("customs_cost"))));

//				String goods_tax_number = String
//						.valueOf(map.get("goods_tax_number")).trim();
//				taxPkgGoods.setGoods_tax_number(
//						Float.parseFloat(goods_tax_number));
//
//				String unit = String.valueOf(map.get("unit")).trim();
//
//				taxPkgGoods.setUnit(unit);

				taxPkgGoodsLists.add(taxPkgGoods);

				if (taxPkgPackageMap.containsKey(logistics_code))
				{
					TaxPkgPackage taxPkgPackage = taxPkgPackageMap
							.get(logistics_code);
					float customsCost = taxPkgPackage.getCustoms_cost();
					customsCost = (float) NumberUtils.add(customsCost,
							taxPkgGoods.getCustoms_cost());
					taxPkgPackage.setCustoms_cost(customsCost);

				} else
				{
					TaxPkgPackage taxPkgPackage = new TaxPkgPackage();
					float customsCost = taxPkgGoods.getCustoms_cost();
					taxPkgPackage.setCustoms_cost(customsCost);

					// 包裹号
					taxPkgPackage.setOriginal_num(logistics_code);

					// 税单号
					//taxPkgPackage.setTax_number(taxPkgGoods.getTax_number());

					taxPkgPackageMap.put(logistics_code, taxPkgPackage);
				}
				
				
				// 更新物品税单信息
				this.taxPkgGoodsService.updateAllTaxPkgGoodsList(taxPkgGoodsLists);

				// 将更新包裹的金额编号 全部更新，及包裹状态为待清关
				User user = (User) request.getSession().getAttribute("back_user");
				this.taxPkgGoodsService.updateAllTaxPkgPackageTaxCostList(taxPkgGoodsLists, user);
				
			}
			// 导入正常
			logMassage.append(ARY_TYPE_MASSAGE[0][0]);

			// 新建包裹保存list,更新金额变化(仅仅查出来)
			List<TaxPkgPackage> taxPkgPackages = new ArrayList<TaxPkgPackage>();

			for (Entry<String, TaxPkgPackage> entry : taxPkgPackageMap
					.entrySet())
			{
				TaxPkgPackage taxPkgPackage = entry.getValue();

				
//				 * // 更新包裹为待清关 taxPkgPackage
//				 * .setStatus(TaxPkgPackage.LOGISTICS_CUSTOMS_WAITING);
				 

				taxPkgPackage
						.setLogistics_code(taxPkgPackage.getOriginal_num());

				// 更新前找到金额变化
				taxPkgPackages.add(taxPkgPackage);

			}
			// 发邮件，推送微信信息
			this.taxPkgGoodsService.sendEmailAndPushWeiXunMessage(taxPkgPackages);
			// 验证税单和包裹的同一性
			// 匹配excel数据,一个包裹号中出现多个税单号
//			List<String> listRepeatGood = repeatTaxNumberGood(taxPkgGoodsLists);
//			if (listRepeatGood.size() > 0)
//			{
//				// 导入数据本身存在错误
//				logMassage.append(ARY_TYPE_MASSAGE[3][0]);
//				// reMap.put("type", ARY_TYPE_MASSAGE[3][0]);
//
//				String path = getPathErrorExcel(exName, excelFile, startRow,
//						TAX_NUMBER, listRepeatGood, ARY_TYPE_MASSAGE[3][1]);
//
//				logMassage.append(path);
//				// reMap.put("path", path);
//				// reMap.put("massage", ARY_TYPE_MASSAGE[3][1]);
//			} else
//			{
//
//				// 匹配excel数据,一个税单号出现在多个包裹中
//				List<String> listRepeat = repeatTaxNumberPkg(taxPkgPackages);
//				if (listRepeat.size() > 0)
//				{
//					// 导入数据本身存在错误
//					logMassage.append(ARY_TYPE_MASSAGE[2][0]);
//					// reMap.put("type", ARY_TYPE_MASSAGE[2][0]);
//
//					String path = getPathErrorExcel(exName, excelFile, startRow,
//							TAX_NUMBER, listRepeat, ARY_TYPE_MASSAGE[2][1]);
//
//					logMassage.append(path);
//					// reMap.put("path", path);;
//					// reMap.put("massage", ARY_TYPE_MASSAGE[2][1]);
//				} else
//				{
					// 匹配数据
//					List<TaxPkgGoods> tpGoodObjList = this.taxPkgGoodsService
//							.finkTaxPkgInCodeToList(taxPkgPackages);
//
//					List<String> listrCode = new ArrayList<String>();
//					// 遍历excel中的包裹
//					for (TaxPkgPackage taxPkgPackage : taxPkgPackages)
//					{
//						String excelCode = taxPkgPackage.getLogistics_code();
//						listrCode.add(excelCode);
//					}
//					for (TaxPkgGoods taxPkgGoods : tpGoodObjList)
//					{
//						// 包裹编号
//						String code = taxPkgGoods.getLogistics_code();
//						listrCode.remove(code);
//					}
//
//					// 导入的包裹单号和税单号组成的包裹在数据库中不存在冲突数据
//					if (listrCode.size() > 0)
//					{
//						// 导入数据与数据数据冲突
//						logMassage.append(ARY_TYPE_MASSAGE[4][0]);
//						// reMap.put("type", ARY_TYPE_MASSAGE[4][0]);
//
//						String path = getPathErrorExcel(exName, excelFile,
//								startRow, LOGISTICS_CODE, listrCode,
//								ARY_TYPE_MASSAGE[4][1]);
//
//						logMassage.append(path);
//						// reMap.put("path", path);
//						// reMap.put("massage", ARY_TYPE_MASSAGE[4][1]);
//					} else
//					{
//
////						// 匹配数据库数据
//						List<TaxPkgGoods> tppObjList = this.taxPkgGoodsService
//								.finkTaxPkgInCodeAndNumberToList(
//										taxPkgPackages);
//						// 导入的包裹单号和税单号组成的包裹在数据库中不存在冲突数据
//						if (null == tppObjList || tppObjList.size() <= 0)
//						{
//
//							// 匹配数据库数据
//							List<TaxPkgGoods> tppGoodsNameObjList = this.taxPkgGoodsService
//									.finkTaxPkgInGoodsNameToList(
//											taxPkgPackages);

//							// 存储excel中出现在sql中找不到的数据
//							List<String> listrGoodsNameExcel = new ArrayList<String>();
//							// 存储sql中出现没显示贼excel中的数据
//							List<String> listrGoodsNameSql = new ArrayList<String>();
//							// 遍历excel中的包裹,临时标记excel中的所有数据
//							for (TaxPkgGoods taxPkgGoods : taxPkgGoodsLists)
//							{
//								// 商品名
//								StringBuffer excelGoodsName = new StringBuffer(
//										taxPkgGoods.getLogistics_code());
//								// 商品名无法确定一条数据,所有补充运单号计算
//								excelGoodsName.append("_");
//								excelGoodsName
//										.append(taxPkgGoods.getGoods_name());
//								// 保存(运单号_商品名)的格式
//								listrGoodsNameExcel
//										.add(excelGoodsName.toString());
//							}
//							// 遍历sql
//							for (TaxPkgGoods taxPkgGoods : tppGoodsNameObjList)
//							{
//								StringBuffer goodsName = new StringBuffer(
//										taxPkgGoods.getLogistics_code());
//								goodsName.append("_");
//								goodsName.append(taxPkgGoods.getGoods_name());
//								// excel保存集合中存在
//								if (listrGoodsNameExcel
//										.contains(goodsName.toString()))
//								{
//									// 在excel的错误标记中移除本条信息
//									listrGoodsNameExcel
//											.remove(goodsName.toString());
//								} else
//								{
//									// 在sql的错误标识中添加本条信息
//									listrGoodsNameSql.add(goodsName.toString());
//								}
//							}
//
//							// excel中存在错误标记 || sql中存在错误标记
//							if (listrGoodsNameExcel.size() > 0
//									|| listrGoodsNameSql.size() > 0)
//							{
//								// 包裹的商品信息不完善
//								logMassage.append(ARY_TYPE_MASSAGE[8][0]);
//
//								String path = getPathErrorExcel(exName,
//										excelFile, startRow,
//										listrGoodsNameExcel, listrGoodsNameSql);
//
//								logMassage.append(path);
//						} else
//							{
//								// 更新物品税单信息
//								this.taxPkgGoodsService.updateAllTaxPkgGoodsList(taxPkgGoodsLists);
//
//								// 将更新包裹的金额编号 全部更新，及包裹状态为待清关
//								User user = (User) request.getSession().getAttribute("back_user");
//								this.taxPkgGoodsService.updateAllTaxPkgPackageTaxCostList(taxPkgGoods, user);
//
//								// 导入正常
//								logMas	sage.append(ARY_TYPE_MASSAGE[0][0]);

								// 发邮件，推送微信信息
								//this.taxPkgGoodsService.sendEmailAndPushWeiXunMessage(taxPkgPackages);
//							}
//						} else
//						{
//							// 导入数据与数据数据冲突
//							logMassage.append(ARY_TYPE_MASSAGE[1][0]);
//
//							// 数据
//							List<String> listr = new ArrayList<String>();
//							for (TaxPkgGoods tppObj : tppObjList)
//							{
//								String taxNumber = tppObj.getTax_number();
//								listr.add(taxNumber);
//							}
//							String path = getPathErrorExcel(exName, excelFile,
//									startRow, TAX_NUMBER, listr,
//									ARY_TYPE_MASSAGE[1][1]);
//
//							logMassage.append(path);
//						}
//					}
//				}
//			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return logMassage.toString();
	}

    /**
     * 导入关税审核状态
     * 
     * @param request
     * @return
     * @throws Exception 
     */
    @RequestMapping("/excelone")
    @ResponseBody
    public String excelone1(HttpServletRequest request,
            HttpServletResponse respone) throws Exception
    {

        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;

        MultipartFile excelFile = req.getFile("excelone");

        // 文件名
        String fileName = excelFile.getOriginalFilename();

        // 后缀名
        String fileSuffix = fileName.substring(fileName.lastIndexOf(".") + 1,
                fileName.length());

        Map<String, String> colMap = new HashMap<String, String>();

        colMap.put("logistics_code", LOGISTICS_CODE);
        colMap.put("tax_status", TAX_STATUS);
        //返回结果
        Map<String,Object> result =new HashMap<String,Object>();
        try
        {

            Workbook workbook = null;

            if ("xls".equals(fileSuffix))
            {
                logger.info("Excel版本 version = 2003");
                workbook = new HSSFWorkbook(excelFile.getInputStream());
            } else
            {
                workbook = new XSSFWorkbook(excelFile.getInputStream());
            }

            // 读取的起始row
            int startRow = 0;

            // 解析之后的数据
            List<Map<String, Object>> list = ExcelReadUtil.parseExcel(workbook,
                    SHEET_NAME, colMap);

            // 将解析之后的数据封装到包裹对象中
            List<TaxPkgPackage> taxPkgPackageList = new ArrayList<TaxPkgPackage>();
            
            result = validateData(list, fileSuffix, workbook,
                    startRow);
            String errorFilePath=(String)result.get("path");
            
             
            if (errorFilePath != null)
            {
                result.put("result", false);
                String resultString= JSONTool.getJSONString(result);
                return resultString;
            }

            for (Map<String, Object> map : list)
            {

                String code =  getStringVal(map.get("logistics_code"));
                
                String tax_status = getStringVal(map.get("tax_status"));

                TaxPkgPackage taxPkgPackage = new TaxPkgPackage();

                taxPkgPackage.setLogistics_code(code);
                taxPkgPackage.setTax_status(tax_status);
                taxPkgPackageList.add(taxPkgPackage);
            }
             taxPkgGoodsService.updateAllTaxPkgPackageTaxStatusList(taxPkgPackageList);
       
             result.put("result", true);
             String resultString= JSONTool.getJSONString(result);
             return resultString;
        } catch (IOException e)
        {
            result.put("result", false);
            result .put("errorType", "system  io error ...");
            String resultString= JSONTool.getJSONString(result);
          
            logger.error(e.toString(),e);
            return resultString;
        } catch (Exception e)
        {
            result.put("result", false);
            result .put("errorType", "system exception error....");
            String resultString= JSONTool.getJSONString(result);
            logger.error(e.toString(),e);
            return resultString;
        }

       
    }

    /**
     * 数据校验
     * 
     * @param list
     * @param exName
     * @param workbook
     * @param startRow
     * @return
     * @throws IOException
     */
    private Map<String,Object> validateData(List<Map<String, Object>> list,
            String fileSuffix, Workbook workbook, int startRow)
            throws IOException
    {

        String path = null;
        Map<String,Object>result=new HashMap<String,Object>();

        // 单号为空校验
        List<String> emtryCodeList = validateEmtryCode(list);

        if (!emtryCodeList.isEmpty())
        {

            path = getPathErrorExcel(fileSuffix, workbook, startRow,
                    LOGISTICS_CODE, emtryCodeList, ERROR_CODE_NULL);
            result.put("path", path);
            result.put("errorType", ERROR_CODE_NULL);
            return result;
        }

        // 状态为空校验
        List<String> emtryStatusList = validateStatusEmtry(list);
        if (!emtryStatusList.isEmpty())
        {

            path = getPathErrorExcel(fileSuffix, workbook, startRow,
                    TAX_STATUS, emtryStatusList, ERROR_STATUS_NULL);
            result.put("path", path);
            result.put("errorType", ERROR_STATUS_NULL);
            return result;
        }

        // 单号重复校验
        List<String> duplicateList = validateDuplicateCode(list);
        if (!duplicateList.isEmpty())
        {

            path = getPathErrorExcel(fileSuffix, workbook, startRow,
                    LOGISTICS_CODE, duplicateList, ERROR_CODE_DUPLICATE);
            result.put("path", path);
            result.put("errorType", ERROR_CODE_DUPLICATE);
            return result;
        }

        // 单号不存校验
        List<String> notFoundList = validateNotFoundCode(list);
        if (!notFoundList.isEmpty())
        {

            path = getPathErrorExcel(fileSuffix, workbook, startRow,
                    LOGISTICS_CODE, notFoundList, ERROR_CODE_NOTFOUND);
            result.put("path", path);
            result.put("errorType", ERROR_CODE_NOTFOUND);
            return result;
        }

        return result;
    }

    /**
     * 单号为空校验
     * 
     * @param list
     * @return
     */
    private List<String> validateEmtryCode(List<Map<String, Object>> list)
    {

        // 单号为空集合：key使用空字符串代替，value，错误信息
        List<String> emtryCodeList = new ArrayList<String>();

        for (Map<String, Object> map : list)
        {

            String code = getStringVal(map.get("logistics_code"));
            if ("".equals(code))
            {
                emtryCodeList.add(code);
                return emtryCodeList;
            }
        }
        return emtryCodeList;

    }

    /**
     * 单号不存在校验
     * 
     * @param list
     * @return
     */
    private List<String> validateNotFoundCode(List<Map<String, Object>> list)
    {
        
        // 将解析之后的数据封装到包裹对象中
        List<TaxPkgPackage> taxPkgPackageList = new ArrayList<TaxPkgPackage>();
        List<String> excelContainList=new ArrayList<String>();
        for (Map<String, Object> map : list)
        {
            String logistics_code = getStringVal(map.get("logistics_code"));
            
            TaxPkgPackage taxPkg=new TaxPkgPackage();
            taxPkg.setLogistics_code(logistics_code);
            excelContainList.add(logistics_code);
            taxPkgPackageList.add(taxPkg);
        }
        
        // 通过Excel中的运单号数据库中查询到的数据
        List<TaxPkgGoods> tpGoodObjList = this.taxPkgGoodsService
                .finkTaxPkgInCodeToList(taxPkgPackageList);
        for(TaxPkgGoods data: tpGoodObjList){
            
            String logistics_code= data.getLogistics_code();
            //如果存则除掉
            if(excelContainList.contains(logistics_code)){
                excelContainList.remove(logistics_code);
            }
        }
        return excelContainList;
    }

    /**
     * 税单状态为空校验
     * 
     * @param list
     * @return
     */
    private List<String> validateStatusEmtry(List<Map<String, Object>> list)
    {

        List<String> emtryStatusList = new ArrayList<String>();

        for (Map<String, Object> map : list)
        {

            String tax_status = getStringVal(map.get("tax_status"));
            if ("".equals(tax_status))
            {
                emtryStatusList.add(tax_status);
                return emtryStatusList;
            }
        }
        return emtryStatusList;

    }

    /**
     * 校验单号是否存重复的情况
     * 
     * @return
     */
    private List<String> validateDuplicateCode(List<Map<String, Object>> list)
    {

        // 重复单号信息key:单号，value:错误描述信息
        List<String> duplicateList = new ArrayList<String>();
        List<String> codeList = new ArrayList<String>();
        for (Map<String, Object> map : list)
        {

            String code = String
                    .valueOf(getStringVal(map.get("logistics_code")));

            if (!"".equals(code) && codeList.contains(code))
            {
                duplicateList.add(code);
            } else
            {
                codeList.add(code);
            }
        }
        return duplicateList;
    }

    /**
     * 获取字符串类型的数据
     * 
     * @param valueObj
     * @return
     */
    private String getStringVal(Object valueObj)
    {

        String strValue = "";
        if (null == valueObj)
        {
            strValue = "";
        } else
        {
            if (Long.class.isAssignableFrom(valueObj.getClass()))
            {
                // 转换成整型
                DecimalFormat df = new DecimalFormat("#");
                strValue = df.format(valueObj);
            }
            if (Double.class.isAssignableFrom(valueObj.getClass()))
            {
                // 转换成整型
                DecimalFormat df = new DecimalFormat("#");
                strValue = df.format(valueObj);
            } else
            {
                strValue = String.valueOf(valueObj).trim();
            }
        }
        return strValue;
    }

    /**
     * 删除税票页面
     * 
     * @param request
     * @param flink
     * @return
     */
    @RequestMapping("/deleteTaxPkgGoods")
    public String deleteTaxPkgGoods(int goods_id)
    {
        TaxPkgGoods goodsType = new TaxPkgGoods();
        goodsType.setGoods_id(goods_id);
        taxPkgGoodsService.updateTaxPkgGoods(goodsType);
        return "back/taxPkgGoodsList";
    }

    /**
     * 
     * @param exName
     *            文件后缀
     * @param excelFile
     *            上传文件对象
     * @param startRow
     *            excel的表头位置
     * @param colName
     *            读取的列名/表头出的名称
     * @param cellStyle
     *            修改背景颜色的样式
     * @param listStr
     *            处理依据数据
     * @param massage
     *            处理信息原因
     * @return
     * @throws IOException
     */
    private String getPathErrorExcel(String exName, MultipartFile excelFile,
            int startRow, String colName, List<String> listStr, String massage)
            throws IOException
    {
        Workbook errorWorkbook = null;
        String error = "";
        if ("xls".equals(exName))
        {
            errorWorkbook = new HSSFWorkbook(excelFile.getInputStream());
            error = "errorTaxNumber.xls";
        } else
        {
            errorWorkbook = new XSSFWorkbook(excelFile.getInputStream());
            error = "errorTaxNumber.xlsx";
        }

        // 不存在公司运单号单元格格式
        CellStyle cellStyle = errorWorkbook.createCellStyle();
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        // 税单号已经在其他包裹里面使用过了
        // 错误订单号标识
        ceateDownExcel(errorWorkbook, startRow, colName, cellStyle, listStr,
                massage);

        File file = new File(UPLOAD_IMG_PATH + error);
        if (file.exists())
        {
            file.delete();
        }
        FileOutputStream fs = new FileOutputStream(file);
        errorWorkbook.write(fs);
        fs.close();

        return "/upload/" + error;
    }

    /**
     * 
     * @param fileSuffix
     *            文件后缀
     * @param wookbook
     *            上传文件对象
     * @param startRow
     *            excel的表头位置
     * @param colName
     *            读取的列名/表头出的名称
     * @param cellStyle
     *            修改背景颜色的样式
     * @param listStr
     *            处理依据数据
     * @param massage
     *            处理信息原因
     * @param errorType
     *            ：1单号为空，2状态为空，3单号重复，4，单号不存 错误类型
     * @return
     * @throws IOException
     */
    private String getPathErrorExcel(String fileSuffix, Workbook wookbook,
            int startRow, String colName, List<String> listStr, int errorType)
            throws IOException
    {
        Workbook errorWorkbook = wookbook;
        String error = "";
        if ("xls".equals(fileSuffix))
        {
            error = "errorTaxNumber.xls";
        } else
        {

            error = "errorTaxNumber.xlsx";
        }

        // 不存在公司运单号单元格格式
        CellStyle cellStyle = errorWorkbook.createCellStyle();
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());

        if (errorType == ERROR_CODE_DUPLICATE)
        {
            ExcelValidateUtils.configDuplicateErrorFile(errorWorkbook,
                    startRow, colName, cellStyle, listStr);
        }
        else if (errorType == ERROR_CODE_NOTFOUND)
        {
            ExcelValidateUtils.configNotExistErrorFile(errorWorkbook, startRow,
                    colName, cellStyle, listStr);
        }
        else if (errorType == ERROR_CODE_NULL)
        {
            ExcelValidateUtils.configNullCellErrorFile(errorWorkbook, startRow,
                    colName, cellStyle, listStr);
        }

        else  if (errorType == ERROR_STATUS_NULL)
        {
            ExcelValidateUtils.configNullCellErrorFile(errorWorkbook, startRow,
                    colName, cellStyle, listStr);
        }

        File file = new File(UPLOAD_IMG_PATH + error);

        if (file.exists())
        {
            file.delete();
        }

        FileOutputStream fs = new FileOutputStream(file);
        errorWorkbook.write(fs);
        fs.close();

        return "/upload/" + error;
    }

    /**
     * 定死做运单号中商品的验证excel错误 <br>
     * 因为是定死做的,没有传列名索引,提示信息描述
     * 
     * @param exName
     *            excel名称
     * @param excelFile
     *            文件
     * @param startRow
     *            起始行
     * @param listExcel
     *            excel中的错误
     * @param listSql
     *            sql中的错误
     * @return
     * @throws IOException
     */
    private String getPathErrorExcel(String exName, MultipartFile excelFile,
            int startRow, List<String> listExcel, List<String> listSql)
            throws IOException
    {
        Workbook errorWorkbook = null;
        String error = "";
        if ("xls".equals(exName))
        {
            errorWorkbook = new HSSFWorkbook(excelFile.getInputStream());
            error = "errorTaxNumber.xls";
        } else
        {
            errorWorkbook = new XSSFWorkbook(excelFile.getInputStream());
            error = "errorTaxNumber.xlsx";
        }

        // 不存在公司运单号单元格格式
        CellStyle cellStyle = errorWorkbook.createCellStyle();
        cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cellStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        // 税单号已经在其他包裹里面使用过了
        // 错误订单号标识
        ceateDownExcel(errorWorkbook, startRow, cellStyle, listExcel, listSql);

        File file = new File(UPLOAD_IMG_PATH + error);
        if (file.exists())
        {
            file.delete();
        }
        FileOutputStream fs = new FileOutputStream(file);
        errorWorkbook.write(fs);
        fs.close();

        return "/upload/" + error;
    }

    /**
     * 
     * @param workBook
     *            文件
     * @param startRow
     *            表头位置
     * @param colName
     *            处理列的名称
     * @param cellStyle
     *            处理样式
     * @param list
     *            标记依据数据
     * @param massage
     *            错误信息描述
     */
    private Workbook ceateDownExcel(Workbook workBook, int startRow,
            String colName, CellStyle cellStyle, List<String> list,
            String massage)
    {
        int colNumber = COL_NUMBER_INIT;
        // 取得订单sheet
        Sheet orderSheet = workBook.getSheet(SHEET_NAME);
        // 总行数
        int rows = orderSheet.getLastRowNum();
        // 表头
        Row firstHssfRow = orderSheet.getRow(startRow);
        // 总列数
        int cols = firstHssfRow.getPhysicalNumberOfCells();
        for (int n = 0; n < cols; n++)
        {
            String colNameString = firstHssfRow.getCell(n).getStringCellValue();
            // 公司运单号
            if (colName.equals(colNameString))
            {
                colNumber = n;
            }
        }

        for (int i = 1; i <= rows; i++)
        {
            Row hssfRow = orderSheet.getRow(i);
            // 空行检验
            if (hssfRow == null || ExcelReadUtil.isBlankRow(hssfRow))
            {
                continue;
            }
            Cell cell = orderSheet.getRow(i).getCell(colNumber);
            Object cellVal = ExcelReadUtil.getValue(cell);
            String strCelVal = getStringVal(cellVal);
            if (list.contains(strCelVal))
            {
                // Cell newCell = orderSheet.getRow(i).getCell(colNumber);
                cell.setCellValue(strCelVal + "(" + massage + ")");
                cell.setCellStyle(cellStyle);

            }
        }
        return workBook;
    }

    /**
     * 定死做法
     * 
     * @param workBook
     * @param startRow
     *            表头位置
     * @param cellStyle
     *            标记用样式
     * @param listExcel
     *            excel中的商品(运单号_商品名)
     * @param listSql
     *            数据库中的商品(运单号_商品名)
     * @return
     */
    private Workbook ceateDownExcel(Workbook workBook, int startRow,
            CellStyle cellStyle, List<String> listExcel, List<String> listSql)
    {
        // 取得sheet
        Sheet orderSheet = workBook.getSheet(SHEET_NAME);
        // 总行数
        int rows = orderSheet.getLastRowNum();
        // 表头
        Row firstHssfRow = orderSheet.getRow(startRow);
        // 总列数
        int cols = firstHssfRow.getPhysicalNumberOfCells();

        // 商品名称的列位置
        int colNameNumber = COL_NUMBER_INIT;
        // 运单号的列位置
        int colMingNumber = COL_NUMBER_INIT;
        for (int n = 0; n < cols; n++)
        {
            String colNameString = firstHssfRow.getCell(n).getStringCellValue();
            if (LOGISTICS_CODE.equals(colNameString))
            {
                colMingNumber = n;
            }
            if (GOODS_NAME.equals(colNameString))
            {
                colNameNumber = n;
            }
        }

        for (int i = 1; i <= rows; i++)
        {
            Row hssfRow = orderSheet.getRow(i);
            // 空行检验
            if (hssfRow == null || ExcelReadUtil.isBlankRow(hssfRow))
            {
                continue;
            }

            // excel中的商品名
            String cellNameVal = orderSheet.getRow(i).getCell(colNameNumber)
                    .getStringCellValue();
            // excel中的运单号
            String cellMingVal = orderSheet.getRow(i).getCell(colMingNumber)
                    .getStringCellValue();
            // 编辑listExcel中保存的(运单号_商品名)格式
            String cellVal = cellMingVal + "_" + cellNameVal;
            // 匹配是否为错误位置
            if (listExcel.contains(cellVal))
            {
                Cell cell = orderSheet.getRow(i).getCell(colNameNumber);
                // 在商品名上额外补充错误信息
                cell.setCellValue(cellNameVal + "(" + ARY_TYPE_MASSAGE[6][1]
                        + ")");
                cell.setCellStyle(cellStyle);
            }
        }
        // sql中存在数据
        int iRow = 2;
        if (listSql.size() > 0)
        {
            // 空一行显示
            // 描述sql数据
            Row row = orderSheet
                    .createRow((short) (orderSheet.getLastRowNum() + iRow));
            Cell cell = row.createCell(colNameNumber);
            cell.setCellValue(ARY_TYPE_MASSAGE[7][1]);
            // 重定义样式颜色
            CellStyle cellStyleNew = workBook.createCellStyle();
            cellStyleNew.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cellStyleNew
                    .setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            cell.setCellStyle(cellStyleNew);
            // 取消空行操作
            iRow = 1;
        }
        CellStyle cellStyleOld = workBook.createCellStyle();
        cellStyleOld.setFillPattern(CellStyle.SOLID_FOREGROUND);
        cellStyleOld.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        for (String str : listSql)
        {
            // 商品名称
            String cellNameVal = str.split("_")[1];
            // 运单号
            String cellMingVal = str.split("_")[0];

            Row row = orderSheet
                    .createRow((short) (orderSheet.getLastRowNum() + iRow));
            // 商品名称
            Cell cellName = row.createCell(colNameNumber);
            cellName.setCellValue(cellNameVal);
            cellName.setCellStyle(cellStyleOld);

            // 运单号
            Cell cellMing = row.createCell(colMingNumber);
            cellMing.setCellValue(cellMingVal);
            cellMing.setCellStyle(cellStyleOld);
        }
        return workBook;
    }

    /**
     * 利用list集合可重复,map的key不可重复特性,判断list集合中的tax_number,税单号是否有重复
     * 
     * @param taxPkgGoodsLists
     * @return
     */
    private List<String> repeatTaxNumberGood(List<TaxPkgGoods> taxPkgGoodsLists)
    {
        List<String> list = new ArrayList<String>();

        Map<String, String> map = new HashMap<String, String>();
        for (TaxPkgGoods goods : taxPkgGoodsLists)
        {
            // 商品中的包裹号
            String keyCode = goods.getLogistics_code();
            // 商品中的税单号
            String valNumber = goods.getTax_number();

            // map保存中的税单号,默认税单号和商品中的税单号相同
            String mapNumber = valNumber;
            // 包裹号作为key中map中是否存在数据
            if (map.containsKey(keyCode))
            {
                // 正式读取真实的map中税单号
                mapNumber = map.get(keyCode);
            }

            // 商品中的税单号 与 map中的税单号 不相等
            if (!valNumber.equals(mapNumber))
            {
                // 将同一包裹中出现不同税单号的税单保存起来,过滤重复值的保存
                if (!list.contains(valNumber))
                {
                    list.add(valNumber);
                }
                if (!list.contains(mapNumber))
                {
                    list.add(mapNumber);
                }
            }

            // 更新包裹税单
            map.put(keyCode, valNumber);
        }

        return list;
    }

    /**
     * 利用list集合可重复,map的key不可重复特性,判断list集合中的tax_number,税单号是否有重复
     * 
     * @param taxPkgPackages
     * @return
     */
    private List<String> repeatTaxNumberPkg(List<TaxPkgPackage> taxPkgPackages)
    {
        List<String> list = new ArrayList<String>();

        Map<String, String> map = new HashMap<String, String>();
        int i = 0;
        for (TaxPkgPackage taxPkgPackage : taxPkgPackages)
        {
            map.put(taxPkgPackage.getTax_number(), "");
            i++;
            if (i != map.size())
            {
                list.add(taxPkgPackage.getTax_number());
                i = map.size();
            }
        }
        return list;
    }
    @RequestMapping(value="/updatePkGoodsByList", method=RequestMethod.POST)
    public String updatePkGoodsByList(GoodsList goodsList){
    	//更新单品的税款总额
    	List<Goods> goods = goodsList.getGoods();
    	float cost =0;
    	for(Goods good:goods){
    		taxPkgGoodsService.updateTaxPkgGoodsTaxPaymentTotal(good);
    		cost+=good.getTaxPaymentTotal();
    	}
    	//更新package的金额
    	TaxPkgPackage pkgPack = new TaxPkgPackage();
    	pkgPack.setPackage_id(goodsList.getPackageId());
    	pkgPack.setCustomsCost(cost);
    	taxPkgGoodsService.updateTaxPkgPackage(pkgPack);
    	return "redirect:/admin/taxPkgGoods/queryAllOne?logistics_code="+goodsList.getLogistics_code();
    }
    /**
	 * 已入库包裹导出
	 * 
	 * @param request
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@RequestMapping("/exportPackage")
	public void exportPackage(HttpServletRequest request, HttpServletResponse response) {
		try {
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "关税导出";
			String logistics_code = request.getParameter("logistics_code");
			String status = request.getParameter("status");
			String pay_status = request.getParameter("pay_status");
			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition","attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");
			Map<String, Object> params = new HashMap<String, Object>();
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null) {
				params.put("user_id", user.getUser_id());
			}
			if(!StringUtils.isEmpty(pay_status)){
				params.put("PAY_STATUS", pay_status);
			}
			if(!StringUtils.isEmpty(logistics_code)){
				params.put("LOGISTICS_CODE", logistics_code);
			}
			if(!StringUtils.isEmpty(status)){
				params.put("STATUS", status);
			}
			List<TaxPkgGoods> list = taxPkgGoodsService.queryNotExportPackageAll(params);
			//List<PkgOperate> pkgOperateList = packageService.queryHaveInputedPackage(params);
			XSSFWorkbook xssfWorkbook = taxPkgGoodsService.creatTaxPkgGoodsExcel(fileName, list);
			OutputStream oStream = response.getOutputStream();
			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
		} catch (IOException e) {
			e.printStackTrace();

		}
	}
}
