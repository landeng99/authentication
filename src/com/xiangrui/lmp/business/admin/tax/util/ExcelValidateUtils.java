package com.xiangrui.lmp.business.admin.tax.util;

import java.text.DecimalFormat;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.xiangrui.lmp.util.newExcel.ExcelReadUtil;

public class ExcelValidateUtils
{

    private final static int COL_NUMBER_INIT=9999999;
    
    /**
     * Excel导入的表名默认值
     */
    private static final String SHEET_NAME = "Sheet1";
    
    /**
     * 信息为空
     */
    public synchronized static  void configNullCellErrorFile(Workbook workbook,
            int startRow, String colName,CellStyle cellStyle, List<String> list ){
        
        int colNumber = COL_NUMBER_INIT;
        
        // 取得订单sheet
        Sheet orderSheet = workbook.getSheet(SHEET_NAME);
        // 总行数
        int rows = orderSheet.getLastRowNum();
        // 表头
        Row firstHssfRow = orderSheet.getRow(startRow);
        // 总列数
        int cols = firstHssfRow.getPhysicalNumberOfCells();
        for (int n = 0; n < cols; n++)
        {
            String colNameString = firstHssfRow.getCell(n).getStringCellValue();
            // 公司运单号
            if (colName.equals(colNameString))
            {
                colNumber = n;
            }
        }

        for (int i = 1; i <= rows; i++)
        {
            Row hssfRow = orderSheet.getRow(i);
            // 空行检验
            if (hssfRow == null || ExcelReadUtil.isBlankRow(hssfRow))
            {
                continue;
            }
            Cell cell = orderSheet.getRow(i).getCell(colNumber);
            Object cellVal = ExcelReadUtil.getValue(cell);
            String strCelVal = getStringVal(cellVal);
            if (list.contains(strCelVal))
            {
                // Cell newCell = orderSheet.getRow(i).getCell(colNumber);
                cell.setCellValue(strCelVal + "(不能为空)");
                cell.setCellStyle(cellStyle);

            }
        }
    }

    /**
     * 数据在数据库中不存在错误
     */
    public synchronized static  void configNotExistErrorFile(Workbook workbook,
            int startRow, String colName,CellStyle cellStyle, List<String> list ){
        
        int colNumber = COL_NUMBER_INIT;
        
        // 取得订单sheet
        Sheet orderSheet = workbook.getSheet(SHEET_NAME);
        // 总行数
        int rows = orderSheet.getLastRowNum();
        // 表头
        Row firstHssfRow = orderSheet.getRow(startRow);
        // 总列数
        int cols = firstHssfRow.getPhysicalNumberOfCells();
        for (int n = 0; n < cols; n++)
        {
            String colNameString = firstHssfRow.getCell(n).getStringCellValue();
            // 公司运单号
            if (colName.equals(colNameString))
            {
                colNumber = n;
            }
        }

        for (int i = 1; i <= rows; i++)
        {
            Row hssfRow = orderSheet.getRow(i);
            // 空行检验
            if (hssfRow == null || ExcelReadUtil.isBlankRow(hssfRow))
            {
                continue;
            }
            Cell cell = orderSheet.getRow(i).getCell(colNumber);
            Object cellVal = ExcelReadUtil.getValue(cell);
            String strCelVal = getStringVal(cellVal);
            if (list.contains(strCelVal))
            {
                // Cell newCell = orderSheet.getRow(i).getCell(colNumber);
                cell.setCellValue(strCelVal + "(单号不存在)");
                cell.setCellStyle(cellStyle);
            }
        }
    }
    
    /**
     * 数据存在重复错误
     */
    public synchronized static  void configDuplicateErrorFile(Workbook workbook,
            int startRow, String colName,CellStyle cellStyle, List<String> list ){
        
        int colNumber = COL_NUMBER_INIT;
        
        // 取得订单sheet
        Sheet orderSheet = workbook.getSheet(SHEET_NAME);
        // 总行数
        int rows = orderSheet.getLastRowNum();
        // 表头
        Row firstHssfRow = orderSheet.getRow(startRow);
        // 总列数
        int cols = firstHssfRow.getPhysicalNumberOfCells();
        for (int n = 0; n < cols; n++)
        {
            String colNameString = firstHssfRow.getCell(n).getStringCellValue();
            // 公司运单号
            if (colName.equals(colNameString))
            {
                colNumber = n;
            }
        }

        for (int i = 1; i <= rows; i++)
        {
            Row hssfRow = orderSheet.getRow(i);
            // 空行检验
            if (hssfRow == null || ExcelReadUtil.isBlankRow(hssfRow))
            {
                continue;
            }
            Cell cell = orderSheet.getRow(i).getCell(colNumber);
            Object cellVal = ExcelReadUtil.getValue(cell);
            String strCelVal = getStringVal(cellVal);
            if (list.contains(strCelVal))
            {
                // Cell newCell = orderSheet.getRow(i).getCell(colNumber);
                cell.setCellValue(strCelVal + "(重复)");
                cell.setCellStyle(cellStyle);
            }
        }
    }  
    
    
    /**
     * 获取字符串类型的数据
     * 
     * @param valueObj
     * @return
     */
    private static String getStringVal(Object valueObj)
    {

        String strValue = "";
        if (null == valueObj)
        {
            strValue = "";
        } else
        {
            if (Long.class.isAssignableFrom(valueObj.getClass()))
            {
                // 转换成整型
                DecimalFormat df = new DecimalFormat("#");
                strValue = df.format(valueObj);
            }
            if (Double.class.isAssignableFrom(valueObj.getClass()))
            {
                // 转换成整型
                DecimalFormat df = new DecimalFormat("#");
                strValue = df.format(valueObj);
            } else
            {
                strValue = String.valueOf(valueObj).trim();
            }
        }
        return strValue;
    }
}
