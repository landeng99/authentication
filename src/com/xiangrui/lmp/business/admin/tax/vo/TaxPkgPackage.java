package com.xiangrui.lmp.business.admin.tax.vo;

import com.xiangrui.lmp.business.base.BasePackage;

/**
 * 关税关联的包裹
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午8:18:14
 * </p>
 */
public class TaxPkgPackage extends BasePackage
{

    private static final long serialVersionUID = 1L;

    /**
     * 完税金额
     */
    public float customsCost;

    public float getCustomsCost()
    {
        return this.getCustoms_cost();
    }

    public void setCustomsCost(float customsCost)
    {
        this.setCustoms_cost(customsCost);
    }

}
