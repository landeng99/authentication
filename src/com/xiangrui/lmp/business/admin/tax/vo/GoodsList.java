/**
 * 
 */
package com.xiangrui.lmp.business.admin.tax.vo;

import java.util.List;

/**
 * @author microzhu
 *
 */
public class GoodsList {
	private List<Goods> goods;
	private String logistics_code;
	private Integer packageId;
	public List<Goods> getGoods() {
		return goods;
	}

	public void setGoods(List<Goods> goods) {
		this.goods = goods;
	}

	public String getLogistics_code() {
		return logistics_code;
	}

	public void setLogistics_code(String logistics_code) {
		this.logistics_code = logistics_code;
	}

	public Integer getPackageId() {
		return packageId;
	}

	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}

	
	
}
