package com.xiangrui.lmp.business.admin.tax.vo;

import com.xiangrui.lmp.business.base.BasePkgGoods;
import com.xiangrui.lmp.util.NumberUtils;

/**
 * 税率等数据实体
 * 
 * @author Administrator
 * 
 */
public class TaxPkgGoods extends BasePkgGoods
{

    private static final long serialVersionUID = 1L;

    /**
     * 关税关联的包裹
     */
    private TaxPkgPackage taxPkgPackage;

    /**
     * 管理的提单号
     */
    private String pick_code;    
    private String logistics_code;
    
    private String time_start;
    
    private String time_end;
    private String status;
    private String pay_status;
    private Double taxTotal;
    private String pay_status_freight;
    private String pay_status_custom;
    
    public Double getTaxTotal() {
		return taxTotal;
	}

	public void setTaxTotal(Double taxTotal) {
		this.taxTotal = taxTotal;
	}

	public String getPick_code()
    {
        return pick_code;
    }

    public void setPick_code(String pick_code)
    {
        this.pick_code = pick_code;
    }


    
    
    public String getTime_start()
    {
        return time_start;
    }

    public void setTime_start(String time_start)
    {
        this.time_start = time_start;
    }

    public String getTime_end()
    {
        return time_end;
    }

    public void setTime_end(String time_end)
    {
        this.time_end = time_end;
    }
    
    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPay_status() {
		return pay_status;
	}

	public void setPay_status(String pay_status) {
		this.pay_status = pay_status;
	}

	public String getPay_status_freight() {
		return pay_status_freight;
	}

	public void setPay_status_freight(String pay_status_freight) {
		this.pay_status_freight = pay_status_freight;
	}

	public String getPay_status_custom() {
		return pay_status_custom;
	}

	public void setPay_status_custom(String pay_status_custom) {
		this.pay_status_custom = pay_status_custom;
	}

	/**
     * 查询的最小
     *//*
    private float customsCostStart;

    *//**
     * 查询最大金额
     *//*
    private float customsCostEnd;
    private float taxPaymentTotalStart;
    private float taxPaymentTotalEnd;
    private float taxRateStart;
    private float taxRateEnd;*/
    


    public TaxPkgPackage getTaxPkgPackage()
    {
        return taxPkgPackage;
    }

    public void setTaxPkgPackage(TaxPkgPackage taxPkgPackage)
    {
        this.taxPkgPackage = taxPkgPackage;
    }

    /**
     * 关税状态 <b>tax_status</b> = taxPkgPackage.tax_status
     * 
     * @return
     */
    public String getTax_status()
    {
        if (null == this.getTaxPkgPackage())
            this.setTaxPkgPackage(new TaxPkgPackage());
        return this.getTaxPkgPackage().getTax_status();
    }

    /**
     * 关税状态 <b>tax_status</b> = taxPkgPackage.tax_status
     * 
     * @param tax_status
     */
    public void setTax_status(String tax_status)
    {
        if (null == this.getTaxPkgPackage())
            this.setTaxPkgPackage(new TaxPkgPackage());
        this.getTaxPkgPackage().setTax_status(tax_status);
    }

    /**
     * 关税编号 <b>tax_number</b> = taxPkgPackage.tax_status
     * 
     * @return
     */
    public String getTax_number()
    {
        if (null == this.getTaxPkgPackage())
            this.setTaxPkgPackage(new TaxPkgPackage());
        return this.getTaxPkgPackage().getTax_number();
    }

    /**
     * 关税编号 <b>tax_number</b> = taxPkgPackage.tax_number
     * 
     * @param tax_number
     */
    public void setTax_number(String tax_number)
    {
        if (null == this.getTaxPkgPackage())
            this.setTaxPkgPackage(new TaxPkgPackage());
        this.getTaxPkgPackage().setTax_number(tax_number);
    }

    /**
     * 关税单上的包裹物流编号 <b>logistics_code</b> = taxPkgPackage.logistics_code
     * 
     * @return
     */
    public String getLogistics_code()
    {
        
        if (null == this.getTaxPkgPackage())
            this.setTaxPkgPackage(new TaxPkgPackage());
        return logistics_code;
        
        
    }

    /**
     * 关税单上的包裹物流编号 <b>logistics_code</b> = taxPkgPackage.logistics_code
     * 
     * @param logistics_code
     */
    public void setLogistics_code(String logistics_code)
    {
        
        this.logistics_code=logistics_code;
        if (null == this.getTaxPkgPackage()){
            this.setTaxPkgPackage(new TaxPkgPackage());
            this.getTaxPkgPackage().setLogistics_code(logistics_code);
        }
    }
    

    /**
     * 关税率转换100%格式方法
     * 
     * @return
     */
    public String getTaxRatePercentage()
    {
        return (this.getTax_rate() * 100) + "%";
    }

    /**
     * 关税转换100%格式方法,由100%变成0.00的字符串
     * 
     * @param taxRatePercentage
     */
    public void setTaxRatePercentage(String taxRatePercentage)
    {
        try
        {
            taxRatePercentage = taxRatePercentage.substring(0,
                    taxRatePercentage.lastIndexOf("%"));
            this.setTax_rate(Float.parseFloat(NumberUtils.divide(
                    Double.parseDouble(taxRatePercentage), 100)
                    + ""));
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
