/**
 * 
 */
package com.xiangrui.lmp.business.admin.tax.vo;

import java.io.Serializable;

/**
 * @author microzhu
 *
 */
public class Goods implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5662052258018331275L;
	private String goodsId;//商品id
	private Double taxPaymentTotal;//税费
	public String getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	public Double getTaxPaymentTotal() {
		return taxPaymentTotal;
	}
	public void setTaxPaymentTotal(Double taxPaymentTotal) {
		this.taxPaymentTotal = taxPaymentTotal;
	}
	@Override
	public String toString() {
		return "Goods [goods_id=" + goodsId + ", taxPaymentTotal=" + taxPaymentTotal + "]";
	}
	
}
