package com.xiangrui.lmp.business.admin.tax.service.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.message.service.MessageSendService;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.tax.mapper.TaxPkgGoodsMapper;
import com.xiangrui.lmp.business.admin.tax.service.TaxPkgGoodsService;
import com.xiangrui.lmp.business.admin.tax.vo.Goods;
import com.xiangrui.lmp.business.admin.tax.vo.TaxPkgGoods;
import com.xiangrui.lmp.business.admin.tax.vo.TaxPkgPackage;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 关税
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午6:32:23
 *         </p>
 */
@Service("taxPkgGoodsService")
public class TaxPkgGoodsServiceImpl implements TaxPkgGoodsService
{
	 Logger logger = Logger.getLogger(TaxPkgGoodsServiceImpl.class);
    /**
     * 关税
     */
    @Autowired
    private TaxPkgGoodsMapper taxPkgGoodsMapper;
    
	@Autowired
	private PackageService packageService;
	@Autowired
	private FrontUserService frontUserService;
	
	@Autowired
	private MessageSendService messageSendService;
	

    /**
     * 查询所有
     */
    @Override
    public List<TaxPkgGoods> queryAll(Map<String, Object> params)
    {
        return taxPkgGoodsMapper.queryAll(params);
    }

    /**
     * 查询某个
     */
    @Override
    public TaxPkgGoods queryAllId(int goodsTypeId)
    {
        return taxPkgGoodsMapper.queryAllId(goodsTypeId);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改关税清单中的税率")
    @Override
    public void updateTaxPkgGoods(TaxPkgGoods goodsType)
    {
        taxPkgGoodsMapper.updateTaxPkgGoods(goodsType);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改 关税关联的包裹信息")
    @Override
    public void updateAllTaxPkgGoods(TaxPkgGoods goodsType)
    {
        taxPkgGoodsMapper.updateAllTaxPkgGoods(goodsType);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改 关税关联的包裹信息")
    @Override
    public void updateTaxPkgPackage(TaxPkgPackage goodsType)
    {
        taxPkgGoodsMapper.updateTaxPkgPackage(goodsType);
    }

    /**
     * 修改
     */
    @Override
    public TaxPkgGoods finkCustomsCostTotalInPackageOriginalNum(String str)
    {
        return taxPkgGoodsMapper.finkCustomsCostTotalInPackageOriginalNum(str);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改关税清单 连接的包裹信息包裹信息就 税单号添加,完税总额更新")
    @Override
    public void updateAllTaxPkgPackageTaxCost(TaxPkgPackage goodsType)
    {
        taxPkgGoodsMapper.updateAllTaxPkgPackageTaxCost(goodsType);
    }

    /**
     * 修改
     */
    @SystemServiceLog(description = "修改关税清单 税单审核状态更")
    @Override
    public void updateAllTaxPkgPackageTaxStatus(TaxPkgPackage goodsType)
    {
        taxPkgGoodsMapper.updateAllTaxPkgPackageTaxStatus(goodsType);
    }

    /**
     * 修改大量数据
     */
    @SystemServiceLog(description = "修改关税清单 连接的包裹信息包裹信息就 税单号添加,完税总额更新")
    @Override
    public void updateAllTaxPkgPackageTaxCostList(List<TaxPkgGoods> taxPkgGoodsLists,
            User user)
    {
        
    	for(TaxPkgGoods taxPkgGoods:taxPkgGoodsLists){
    	String logistics_code = taxPkgGoods.getLogistics_code();
            TaxPkgPackage taxPkgPackage =taxPkgGoodsMapper.queryTaxPkgPackageTaxCost(logistics_code);
            
            
            //导入关税不等于当前关税则标记为关税待支付
            //if(taxPkgPackage.getCustoms_cost()!=taxPkgGoods.getCustoms_cost()){
            	
                //关税待支付状态
            	taxPkgPackage.setPay_status(TaxPkgPackage.PAYMENT_CUSTOM_UNPAID);
            	
            	// 如果支付过了就不再变成未支付状态
            	if( taxPkgPackage.getPay_status_custom()!=TaxPkgPackage.PAYMENT_CUSTOM_PAID )
            	    taxPkgPackage.setPay_status_custom(TaxPkgPackage.PAYMENT_CUSTOM_UNPAID);
            	
            	// 如果此次没有初始化则初始化为未支付
            	if( 0==taxPkgPackage.getPay_status_freight() )
            	    taxPkgPackage.setPay_status_freight(TaxPkgPackage.PAYMENT_FREIGHT_UNPAID);
            	taxPkgPackage.setCustoms_cost(taxPkgGoods.getCustoms_cost());
                taxPkgGoodsMapper.updateAllTaxPkgPackageTaxCost(taxPkgPackage);
            //}
        
           // .updateAllTaxPkgPackageTaxCostList(taxPkgPackages);
        }
    
    }

    /**
     * 修改大量数据
     */
    @SystemServiceLog(description = "修改关税清单 税单审核状态更")
    @Override
    public void updateAllTaxPkgGoodsList(List<TaxPkgGoods> taxPkgGoodsLists)
    {
        for(TaxPkgGoods taxPkgGoods:taxPkgGoodsLists){
        	
            //当前已有的物品关税
           // TaxPkgGoods currTaxPkgGoods=  taxPkgGoodsMapper.queryTaxPkgGoods(taxPkgGoods);
            
            //当前待支付关税
            //float currCustoms_cost= currTaxPkgGoods.getCustoms_cost();

            //已支付关税
//            float currPaid= currTaxPkgGoods.getPaid();
//            float currTotalCustoms_cost=(float)NumberUtils.add(currCustoms_cost, currPaid);

            //差额=导入的关税+当前已有关税
//           float difference=(float) NumberUtils.subtract(taxPkgGoods.getCustoms_cost(), currTotalCustoms_cost);

           //新增待当前要支付的关税
//           float customs_cost=(float)NumberUtils.add(currCustoms_cost,difference);
        	float customs_cost = taxPkgGoods.getCustoms_cost();
        	String logistics_code = taxPkgGoods.getLogistics_code();
           taxPkgGoods.setCustoms_cost(customs_cost);
           taxPkgGoods.setLogistics_code(logistics_code);

           //更新包裹关税信息
           taxPkgGoodsMapper.updateAllTaxPkgGoods(taxPkgGoods);
        }
        
    
    }

    /**
     * 修改大量数据
     */
    @SystemServiceLog(description = "修改关税清单 税单审核状态更")
    @Override
    public void updateAllTaxPkgPackageTaxStatusList(
            List<TaxPkgPackage> taxPkgPackages)
    {
        taxPkgGoodsMapper.updateAllTaxPkgPackageTaxStatusList(taxPkgPackages);
    }

    @Override
    public List<TaxPkgGoods> finkTaxPkgInCodeAndNumberToList(
            List<TaxPkgPackage> taxPkgPackages)
    {
        return taxPkgGoodsMapper
                .finkTaxPkgInCodeAndNumberToList(taxPkgPackages);
    }

    @Override
    public List<TaxPkgGoods> finkTaxPkgInCodeToList(
            List<TaxPkgPackage> taxPkgPackages)
    {
        return taxPkgGoodsMapper.finkTaxPkgInCodeToList(taxPkgPackages);
    }

    @Override
    public List<TaxPkgGoods> finkTaxPkgInGoodsNameToList(
            List<TaxPkgPackage> taxPkgPackages)
    {
        return taxPkgGoodsMapper.finkTaxPkgInGoodsNameToList(taxPkgPackages);
    }
    /**
     * 发送邮件和推送微信信息
     * @param taxPkgPackages
     */
    public void sendEmailAndPushWeiXunMessage(List<TaxPkgPackage> taxPkgPackages)
    {
    	 Map<Integer, List<TaxPkgPackage>> taxPkgPackageMap = new HashMap<Integer, List<TaxPkgPackage>>();
    	 if(taxPkgPackages!=null&&taxPkgPackages.size()>0)
    	 {
    		 for(TaxPkgPackage taxPkgPackage:taxPkgPackages)
    		 {
    			 String logistics_code=taxPkgPackage.getLogistics_code();
    			 List<Pkg> pkgList=packageService.queryPackageByLogistics_code(logistics_code);
    			 if(pkgList!=null)
    			 {
    				 Pkg pkg=pkgList.get(0);
    				 int userId=pkg.getUser_id();
    				 if(taxPkgPackageMap.containsKey(userId))
    				 {
    					 List<TaxPkgPackage> taxPkgList=taxPkgPackageMap.get(userId);
    					 taxPkgList.add(taxPkgPackage);
    					 taxPkgPackageMap.put(userId, taxPkgList);
    				 }else
    				 {
    					 List<TaxPkgPackage> taxPkgList = new ArrayList<TaxPkgPackage>();
    					 taxPkgList.add(taxPkgPackage);
    					 taxPkgPackageMap.put(userId, taxPkgList);
    				 }
    			 }
    		 }
    		

    		 Iterator<Integer> iterator=taxPkgPackageMap.keySet().iterator();
    		 while(iterator.hasNext())
    		 {
    			 
        		 int pacakageCount=0;
        		 float totalTaxMenoy=0;
        		 String weixunBody="";
        		 String emailBody="";
    			 int userId = iterator.next();
    			 List<TaxPkgPackage> selfTxaPkgList =taxPkgPackageMap.get(userId);
    			 if(selfTxaPkgList!=null&&selfTxaPkgList.size()>0)
    			 {
    				 FrontUser frontUser = frontUserService.queryFrontUserById(userId);
    				 for(TaxPkgPackage tempTaxPkgPackage: selfTxaPkgList)
    				 {
    					 pacakageCount++;
    					 float customs_cost=tempTaxPkgPackage.getCustoms_cost();
    					 totalTaxMenoy = (float) NumberUtils.add(totalTaxMenoy,customs_cost);
    					 String logistics_code=tempTaxPkgPackage.getLogistics_code();
    					 weixunBody+=logistics_code+",";
    					 emailBody+="<tr><td>"+logistics_code+"</td><td>"+NumberUtils.scaleMoneyData(customs_cost)+"</td></tr>";
    				 }
    				 weixunBody=StringUtils.substringBeforeLast(weixunBody, ",");
    				 
    				 String totalTax=NumberUtils.scaleMoneyData(totalTaxMenoy);
    				 String content=getTaxEmailTemple();
					content = StringUtils.replace(content, "[receriver_user_name]", frontUser.getUser_name());
					content = StringUtils.replace(content, "[pacakage_count]", "" + pacakageCount);
					content = StringUtils.replace(content, "[need_pay_menoy]", totalTax);
					content = StringUtils.replace(content, "[email_body_detail]", emailBody);
					
					
					String sendEmail=frontUser.getEmail();
//					logger.info("--------------------------sendEmail is:"+sendEmail);
//					sendEmail="467275639@qq.com";
					messageSendService.sendTaxWarnEmail(frontUser.getUser_name(), sendEmail, frontUser.getAccount(), content);
    					
    				String weixunMsg="亲爱的会员："+frontUser.getLast_name()+" 您好！\n";
    				weixunMsg+="您有"+pacakageCount+"个包裹产生税金："+weixunBody+"\n";
    				weixunMsg+="共需支付关税："+totalTax+"元\n";
    				weixunMsg+="为了不影响包裹的正常清关派送，请您登陆跨境物流进行付款，谢谢！";
    				PackageLogUtil.pushWeiXunTaxMessage(frontUser.getOpen_id(), weixunMsg, userId);
    			 }
    		 }
    		 
    	 }
    }
    
    private static String getTaxEmailTemple()
    {
    	String temple="<!DOCTYPE html>";
    	temple+="<html>";

    	temple+="<head>";
    	temple+="<meta charset=\"utf-8\">";
    	temple+="<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">";
    	temple+="<title>跨境物流客户需支付关税通知</title>";
    	temple+="<meta name=\"description\" content=\"客户需支付关税通知\">";
    	temple+="</head>";

    	temple+="<body>";
    	temple+="<table align=\"center\" border=\"0\" cellspacing=\"0\" style=\"width:700px;\" width=\"700\">";
    	temple+="<tbody>";
    	temple+="<tr>";
    	temple+="<td>";
    	temple+="<div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;\">";
    	temple+="<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"39\" style=\"font:12px Tahoma, Arial, 宋体;\" width=\"700\">";
    	temple+="<tbody>";
    			temple+="<tr>";
    					temple+="<td width=\"210\">";
    							temple+="<a href=\"http://www.su8exp.com/\" target=\"_blank\">";
    								temple+="<img border=\"0\" height=\"39\" src=\"http://www.kjwl.com/resource/img/logo.jpg\" width=\"110\" alt=\"跨境物流\">";
    									temple+="</a>";
    											temple+="</td>";
    													temple+="<td align=\"right\" style=\"padding-bottom:10px;\" valign=\"bottom\" width=\"490\">";
    													temple+="<a href=\"http://www.kjwl.com/login\" style=\"color:#07f;text-decoration:none;font-size:12px;\" target=\"_blank\">登录</a> |";
    														temple+="<a href=\"http://www.kjwl.com/help\" style=\"color:#07f;text-decoration:none;padding-right:5px;font-size:12px;\" target=\"_blank\">帮助中心</a>";
    															temple+="</td>";
    																	temple+="</tr>";
    																			temple+="</tbody>";
    																					temple+="</table>";
    																							temple+="</div>";
    																									temple+="<div style=\"width: 680px; padding: 0pt 10px; margin: 0pt auto;\">";
    													temple+="<div style=\"line-height: 1.5; font-size: 14px; margin-bottom: 25px; color: rgb(77, 77, 77);\">";
    													temple+="<strong style=\"display: block; margin-bottom: 15px;\">";
    													temple+="亲爱的会员： [receriver_user_name] 您好！";
    															temple+="</strong>";
    																	temple+="<p>您有<span style=\"color:#f60;font-weight: bold;\">[pacakage_count]</span>个包裹产生税金：<br/>";
    													temple+="<table cellspacing=\"2\" cellpadding=\"5\">";
    															temple+="<tr style=\"background-color:#9FF;\">";
    													temple+="<td>公司单号</td>";
    															temple+="<td>应付税金</td>";
    																	temple+="</tr>";
    																	temple+="[email_body_detail]";
							temple+="</table>";
									temple+="<br/>";
											temple+="<table style=\"background:#f5f5f5;\">";
							temple+="<tbody>";
									temple+="<tr>";
											temple+="<td>共需付关税： </td>";
													temple+="<td><span style=\"color:#f60;\">[need_pay_menoy]</span> 元 </td>";
							temple+="</tr>";
									temple+="</tbody>";
											temple+="</table>";
													temple+="<br/>为了不影响包裹的正常清关派送，请您登陆跨境物流进行付款，谢谢！</p>";
															temple+="<p></p>";
																	temple+="<p>";
																			temple+="</p>";
                            
																					temple+="<p></p>";
																							temple+="</div>";
                        
																									temple+="</div>";
					
																											temple+="<div style=\"width:700px;margin:0 auto;\">";
							temple+="<div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#999;margin-bottom:20px;line-height:1.3em;font-size:12px;\">";
							temple+="<p style=\"margin-bottom:15px;\">此为系统邮件，请勿回复";
							temple+="</p>";
									temple+="<p>如有任何疑问，请登录跨境物流（<a href=\"http://www.su8exp.com\" target=\"_blank\" style=\"color:#666;text-decoration:none;\" target=\"_blank\">http://www.su8exp.com</a>）联系客服。";
										temple+="<br> Copyright kjwl.com Inc. <span style=\"border-bottom:1px dashed #ccc;z-index:1\" t=\"7\" onclick=\"return false;\" data=\"2004-2016\">2016-2017</span> All Right Reserved</p>";
										temple+="</div>";
												temple+="</div>";
														temple+="</td>";
																temple+="</tr>";
																		temple+="</tbody>";
																				temple+="</table>";
																						temple+="</body>";

																								temple+="</html>";
	  return temple;
    }

	@Override
	public void updateTaxPkgGoodsTaxPaymentTotal(Goods good) {
		// TODO Auto-generated method stub
		Map<String,Object> params = new HashMap<>();
		params.put("goodsId", good.getGoodsId());
		params.put("taxPament", good.getTaxPaymentTotal());
		taxPkgGoodsMapper.updateTaxPkgGoodsTaxPaymentTotal(params);
	}

	@Override
	public XSSFWorkbook creatTaxPkgGoodsExcel(String fileName, List<TaxPkgGoods> list) {
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		// 新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
		// 第一列固定
		xssfSheet.createFreezePane(0, 1, 0, 1);

		// 颜色蓝色
		XSSFColor blueColor = new XSSFColor(new Color(185, 211, 238));
		// 白色
		XSSFColor whiteColor = new XSSFColor(Color.WHITE);

		// 样式白色居中
		XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
		style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setFillForegroundColor(whiteColor);
		style1.setWrapText(true);

		// 样式蓝色居中
		XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style2.setFillForegroundColor(blueColor);
		style2.setWrapText(true);

		// 列宽
		xssfSheet.setColumnWidth(0, 3500);
		xssfSheet.setColumnWidth(1, 4000);
		xssfSheet.setColumnWidth(2, 1800);
		xssfSheet.setColumnWidth(3, 3000);
		xssfSheet.setColumnWidth(4, 5000);
		xssfSheet.setColumnWidth(5, 1800);
		xssfSheet.setColumnWidth(6, 1800);
		xssfSheet.setColumnWidth(7, 10000);
		xssfSheet.setColumnWidth(8, 5000);
		// 表头列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		List<String> columnsList = new ArrayList<String>();
		columnsList.add("公司单号");
		columnsList.add("关联单号");
		columnsList.add("收件人");
		columnsList.add("收件电话");
		columnsList.add("身份证");
		columnsList.add("省份");
		columnsList.add("城市");
		columnsList.add("详细地址");
		columnsList.add("商品名称");
		columnsList.add("规格");
		columnsList.add("数量");
		columnsList.add("品牌");
		columnsList.add("价格");
		columnsList.add("渠道");
		for (int i = 0; i < columnsList.size(); i++)
		{
			XSSFCell cell = firstXSSFRow.createCell(i);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(columnsList.get(i));
			cell.setCellStyle(style1);
		}
		int rowCount = 1;
		for(TaxPkgGoods pkGoods : list){
			//获取单个包裹的信息
			Pkg pkg = packageService.queryPackageById(pkGoods.getPackage_id());
			Map<String, Object> params = new HashMap<>();
			params.put("code", pkGoods.getLogistics_code());
			List<Map<String,Object>> listExcels = taxPkgGoodsMapper.queryExportExcelInfoByPacCode(params);
			if(!CollectionUtils.isEmpty(listExcels)){
				for(Map<String,Object> listExcel : listExcels){
					if(!CollectionUtils.isEmpty(listExcel)){
						XSSFRow row = xssfSheet.createRow(rowCount++);
						XSSFCell cell0 = row.createCell(0);
						cell0.setCellValue(pkg.getLogistics_code());
						XSSFCell cell1 = row.createCell(1);
						cell1.setCellValue(pkg.getOriginal_num());
						XSSFCell cell2 = row.createCell(2);
						cell2.setCellValue(String.valueOf(listExcel.get("user")));
						XSSFCell cell3 = row.createCell(3);
						cell3.setCellValue(String.valueOf(listExcel.get("mobile")));
						XSSFCell cell4 = row.createCell(4);
						cell4.setCellValue(String.valueOf(listExcel.get("idcard")));
						List<String> listAddr = addressInfo(String.valueOf(listExcel.get("region")));
						if(!CollectionUtils.isEmpty(listAddr)){
							if(listAddr.size()>=2){
								XSSFCell cell5 = row.createCell(5);
								cell5.setCellValue(listAddr.get(0));
								XSSFCell cell6 = row.createCell(6);
								cell6.setCellValue(listAddr.get(1));
							}else{
								XSSFCell cell5 = row.createCell(5);
								cell5.setCellValue(listAddr.get(0));
								XSSFCell cell6 = row.createCell(6);
								cell6.setCellValue("null");
							}
							
						}else{
							XSSFCell cell5 = row.createCell(5);
							cell5.setCellValue("null");
							XSSFCell cell6 = row.createCell(6);
							cell6.setCellValue("null");
						}
						
						XSSFCell cell7 = row.createCell(7);
						cell7.setCellValue(String.valueOf(listExcel.get("street")));
						XSSFCell cell8 = row.createCell(8);
						cell8.setCellValue(String.valueOf(listExcel.get("goodName")));
						XSSFCell cell9 = row.createCell(9);
						cell9.setCellValue(String.valueOf(listExcel.get("spec")));
						XSSFCell cell10 = row.createCell(10);
						cell10.setCellValue(String.valueOf(listExcel.get("quantity")));
						XSSFCell cell11 = row.createCell(11);
						cell11.setCellValue(String.valueOf(listExcel.get("brand")));
						XSSFCell cell12 = row.createCell(12);
						cell12.setCellValue(String.valueOf(listExcel.get("price")));
						XSSFCell cell13 = row.createCell(13);
						cell13.setCellValue(getExpressPackage(pkg.getExpress_package()));
					}
					
				}
			}
			
			
		}
		return xssfWorkbook;
	}
    private String getExpressPackage(int code){
    	String ret = "其它";
    	switch (code){
    		case 0:
	    		ret="默认";
	    		break;
    		case 1:
    			ret="A渠道";
    			break;
    		case 2:
    			ret="B渠道";
    			break;	
    		case 3:
    			ret="C渠道";
    			break;	
    		case 4:
    			ret="D渠道";
    			break;		
    		case 5:
    			ret="E渠道";
    			break;	
    	}
    	
    	return ret;
    	/**
    	<c:if test="${pkgDetail.express_package==0}">默认</c:if>
        <c:if test="${pkgDetail.express_package==1}">A渠道</c:if>
        <c:if test="${pkgDetail.express_package==2}">B渠道</c:if>
        <c:if test="${pkgDetail.express_package==3}">C渠道</c:if>
        <c:if test="${pkgDetail.express_package==4}">D渠道</c:if>
        <c:if test="${pkgDetail.express_package==5}">E渠道</c:if>
        **/
    }
    private List<String> addressInfo(String jsonString) {
		if (StringUtil.isEmpty(jsonString)) {
			return null;
		}
		List<String> aList = new ArrayList<String>();
		try {
			aList = JSONUtil.readValueFromJson(jsonString, "name");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		StringBuffer address = new StringBuffer("");
		for (String city : aList) {
			address.append(city);
		}

		return aList;
	}

	@Override
	public List<TaxPkgGoods> queryNotExportPackageAll(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return taxPkgGoodsMapper.queryNotExportPackageAll(params);
	}
    
}
