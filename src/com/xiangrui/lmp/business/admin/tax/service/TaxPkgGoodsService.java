package com.xiangrui.lmp.business.admin.tax.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.xiangrui.lmp.business.admin.tax.vo.Goods;
import com.xiangrui.lmp.business.admin.tax.vo.TaxPkgGoods;
import com.xiangrui.lmp.business.admin.tax.vo.TaxPkgPackage;
import com.xiangrui.lmp.business.admin.user.vo.User;

/**
 * 关税
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午6:31:56
 *         </p>
 */
public interface TaxPkgGoodsService
{

    /**
     * 查询所有商品的税率
     * 
     * @return
     */
    List<TaxPkgGoods> queryAll(Map<String, Object> params);

    /**
     * 根据id查询单个商品信息(税率)
     * 
     * @param goodsTypeId
     * @return
     */
    TaxPkgGoods queryAllId(int goodsTypeId);

    /**
     * 修改 a) 修改：修改关税清单中的税率。 b) 删除：删除关税清单记录。
     * 
     * @param flink
     */
    void updateTaxPkgGoods(TaxPkgGoods goodsType);

    /**
     * 修改 按照集合的方式,修改大量数据
     * 
     * @param flink
     */
    void updateAllTaxPkgGoods(TaxPkgGoods goodsType);

    /**
     * 修改 按照集合的方式,修改大量数据 修改关税清单 连接的包裹信息 包裹信息就 税单号添加,完税总额更新
     * 
     * @param flink
     */
    void updateAllTaxPkgPackageTaxCost(TaxPkgPackage goodsType);

    /**
     * 修改 按照集合的方式,修改大量数据 修改关税清单 连接的包裹信息 包裹信息就税单审核状态更新
     * 
     * @param flink
     */
    void updateAllTaxPkgPackageTaxStatus(TaxPkgPackage goodsType);

    /**
     * 查找某个包裹的物品 税款金额总数
     * 
     * @param str
     * @return 实际返回的customs_cost
     */
    TaxPkgGoods finkCustomsCostTotalInPackageOriginalNum(String str);

    /**
     * 修改 关税关联的包裹信息
     * 
     * @param flink
     */
    void updateTaxPkgPackage(TaxPkgPackage goodsType);

    /**
     * 添加大量
     * 
     * @param taxPkgGoodsLists
     * @param user
     *            包裹日志操作人
     */
    void updateAllTaxPkgPackageTaxCostList(List<TaxPkgGoods> taxPkgGoodsLists,
            User user);

    /**
     * 添加大量 更新物品税单信息,
     * 
     * @param taxPkgPackages
     */
    void updateAllTaxPkgGoodsList(List<TaxPkgGoods> taxPkgGoodsLists);
    /**
     * 添加大量
     * 
     * @param taxPkgPackages
     */
    void updateAllTaxPkgPackageTaxStatusList(List<TaxPkgPackage> taxPkgPackages);

    /**
     * 查询包裹数据<br>
     * 查询的方式是: 包裹税单号不能为空的基础条件下:当 运单号相同则税单号不相同.反之,税单号相同则运单号不相同.
     * 
     * @param taxPkgPackages
     *            批量包裹数据,用户批量匹配
     * @return
     */
    List<TaxPkgGoods> finkTaxPkgInCodeAndNumberToList(
            List<TaxPkgPackage> taxPkgPackages);

    /**
     * 查询包裹数据<br>
     * 查询的方式是: 一包裹编号为条件 in('','','')方式
     * 
     * @param taxPkgPackages
     *            批量包裹数据,用户批量匹配
     * @return 返回结果为包裹商品信息,实际上是包裹数据
     */
    List<TaxPkgGoods> finkTaxPkgInCodeToList(List<TaxPkgPackage> taxPkgPackages);

    /**
     * 查询包裹商品数据<br>
     * 查询的方式是: 一包裹编号为条件 in('','','')方式
     * 
     * @param taxPkgPackages
     *            批量包裹数据,用户批量匹配
     * @return 返回数据是包裹商品数据,不仅是包裹数据
     */
    List<TaxPkgGoods> finkTaxPkgInGoodsNameToList(
            List<TaxPkgPackage> taxPkgPackages);
    
    /**
     * 发送邮件和推送微信信息
     * @param taxPkgPackages
     */
    void sendEmailAndPushWeiXunMessage(List<TaxPkgPackage> taxPkgPackages);

	void updateTaxPkgGoodsTaxPaymentTotal(Goods good);

	XSSFWorkbook creatTaxPkgGoodsExcel(String fileName, List<TaxPkgGoods> list);

	List<TaxPkgGoods> queryNotExportPackageAll(Map<String, Object> params);
    
}
