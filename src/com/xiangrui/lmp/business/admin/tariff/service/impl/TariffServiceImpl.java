package com.xiangrui.lmp.business.admin.tariff.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.tariff.mapper.TariffMapper;
import com.xiangrui.lmp.business.admin.tariff.service.TariffService;
import com.xiangrui.lmp.business.admin.tariff.vo.Tariff;

@Service(value = "tariffService")
public class TariffServiceImpl implements TariffService
{

    @Autowired
    private TariffMapper tariffMapper;

    /**
     * 分页查询所有资费
     * 
     * @param params
     * @return
     */
    @Override
    public List<Tariff> queryAllTariff(Map<String, Object> params)
    {
        return tariffMapper.queryAllTariff(params);
    }

    /**
     * 查询资费
     * 
     * @param tariff_id
     * @return
     */
    public Tariff queryTariffById(int tariff_id)
    {
        return tariffMapper.queryTariffById(tariff_id);
    }

    /**
     * 更新资费信息
     * 
     * @param tariff
     * @return
     */
    @SystemServiceLog(description = "修改资费")
    @Override
    public void updateTariff(Tariff tariff)
    {
        tariffMapper.updateTariff(tariff);

    }

    /**
     * 添加资费信息
     * 
     * @param tariff
     * @return
     */
    @SystemServiceLog(description = "新增资费")
    @Override
    public void insertTariff(Tariff tariff)
    {

        tariffMapper.insertTariff(tariff);
    }

    /**
     * 逻辑删除资费信息
     * 
     * @param tariff
     * @return
     */
    @SystemServiceLog(description = "更新资费删除数据")
    @Override
    public void updateTariffIsdeleted(Tariff tariff)
    {
        tariffMapper.updateTariffIsdeleted(tariff);
    }

    /**
     * 资费类型
     * 
     * @return
     */
    @Override
    public List<String> selectTariffType()
    {

        return tariffMapper.selectTariffType();
    }

    /**
     * 资费名称
     * 
     * @return
     */
    @Override
    public List<String> selectTariffName()
    {
        return tariffMapper.selectTariffName();
    }

    /**
     * 用户等级关联查询资费
     * 
     * @param tariff_type
     * @return
     */
    @Override
    public Tariff selectTariffByTariffType(int tariff_type)
    {
        return tariffMapper.selectTariffByTariffType(tariff_type);
    }

    /**
     * 名称重复检查
     * 
     * @param tariff_name
     * @return
     */
    @Override
    public List<Tariff> checkTariffName(String tariff_name)
    {
        return tariffMapper.checkTariffName(tariff_name);
    }
}
