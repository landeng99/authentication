package com.xiangrui.lmp.business.admin.tariff.controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.tariff.service.TariffService;
import com.xiangrui.lmp.business.admin.tariff.vo.Tariff;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/tariff")
public class TariffController
{

    @Autowired
    private TariffService tariffService;

    /**
     * 逻辑删除0：未删除
     */
    private static final int ISDELETED_NO = 0;

    /**
     * 逻辑删除1：删除
     */
    private static final int ISDELETED_YES = 1;

    /**
     * 口岸名称1：不存在
     */
    private static final String TARIFF_NAME_NOTEXIST = "1";
    /**
     * 口岸名称0：存在
     */
    private static final String TARIFF_NAME_EXIST = "0";

    /**
     * 资费查询初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/tariffInit")
    public String tariffInit(HttpServletRequest request)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);
        // 逻辑删除0：未删除
        params.put("isdeleted", ISDELETED_NO);

        // 资费列表
        List<Tariff> tariffList = tariffService.queryAllTariff(params);

        // 资费名称下拉列表
        List<String> tariffNameList = tariffService.selectTariffName();
        request.setAttribute("tariffList", tariffList);
        request.setAttribute("tariffNameList", tariffNameList);
        request.setAttribute("pageView", pageView);

        return "back/tariffList";
    }

    /**
     * 资费查询
     * 
     * @param request
     * @param tariff_name
     * @param tariff_type
     * @param percentMin
     * @param percentMax
     * @return
     */
    @RequestMapping("/tariffSearch")
    public String tariffSearch(HttpServletRequest request, String tariff_name,
            String tariff_type, String percentMin, String percentMax)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);

        params.put("tariff_name", tariff_name);

        params.put("tariff_type", tariff_type);

        params.put("percentMin", percentMin);

        params.put("percentMax", percentMax);

        // 逻辑删除0：未删除
        params.put("isdeleted", ISDELETED_NO);
        // 资费列表
        List<Tariff> tariffList = tariffService.queryAllTariff(params);
        // 资费名称下拉列表
        List<String> tariffNameList = tariffService.selectTariffName();
        request.setAttribute("tariffList", tariffList);
        request.setAttribute("tariffNameList", tariffNameList);
        request.setAttribute("pageView", pageView);

        return "back/tariffList";
    }

    /**
     * 资费添加初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/addTariff")
    public String addTariff(HttpServletRequest request)
    {

        return "back/addTariff";
    }

    /**
     * 资费名称重复检查
     * 
     * @param request
     * @param tariff_name
     * @return
     */
    @RequestMapping("/checkTariffName")
    @ResponseBody
    public String checkTariffName(HttpServletRequest request, String tariff_name)
    {

        String checkResult = TARIFF_NAME_EXIST;

        List<Tariff> TariffList = tariffService.checkTariffName(tariff_name);
        // 资费名称不存在
        if (TariffList == null || TariffList.size() == 0)
        {
            checkResult = TARIFF_NAME_NOTEXIST;
        }

        return checkResult;
    }

    /**
     * 保存到数据库
     * 
     * @param request
     * @param tariff
     * 
     * @return
     */
    @RequestMapping("/insertTariff")
    @ResponseBody
    public String insertTariff(HttpServletRequest request, Tariff tariff)
    {

        // 更新时间
        Timestamp updateTime = new Timestamp(System.currentTimeMillis());
        tariff.setUpdate_time(updateTime);

        tariffService.insertTariff(tariff);

        return null;
    }

    /**
     * 资费信息修改初始化
     * 
     * @param request
     * @param tariff_id
     * @return
     */
    @RequestMapping("/modifyTariff")
    public String modifyTariff(HttpServletRequest request, String tariff_id)
    {

        // 资费列表
        Tariff tariff = tariffService.queryTariffById(Integer
                .parseInt(tariff_id));
        request.setAttribute("tariff", tariff);

        return "back/updateTariff";
    }

    /**
     * 更新资费信息
     * 
     * @param request
     * @param tariff
     * 
     * @return
     */
    @RequestMapping("/updateTariff")
    @ResponseBody
    public String updateTariff(HttpServletRequest request, Tariff tariff)
    {

        // 更新时间
        Timestamp updateTime = new Timestamp(System.currentTimeMillis());
        tariff.setUpdate_time(updateTime);

        tariffService.updateTariff(tariff);

        return null;
    }

    /**
     * 资费信息逻辑删除
     * 
     * @param request
     * @param tariff_id
     * @return
     */
    @RequestMapping("/deleteTariff")
    @ResponseBody
    public String deleteTariff(HttpServletRequest request, Tariff tariff)
    {
        tariff.setIsdeleted(ISDELETED_YES);
        // 逻辑删除资费信息
        tariffService.updateTariffIsdeleted(tariff);

        return null;
    }

}
