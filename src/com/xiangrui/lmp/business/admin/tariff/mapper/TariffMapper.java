package com.xiangrui.lmp.business.admin.tariff.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.tariff.vo.Tariff;

public interface TariffMapper {
    
    /**
     * 分页查询所有资费
     * 
     * @param params
     * @return
     */
    List<Tariff> queryAllTariff(Map<String, Object> params);
    
    /**
     * 查询资费
     * 
     * @param tariff_id
     * @return
     */
    Tariff queryTariffById(int tariff_id);
    
    /**
     * 更新资费信息
     * 
     * @param tariff
     * @return
     */
    void updateTariff(Tariff tariff);
    
    /**
     * 添加资费信息
     * 
     * @param tariff
     * @return
     */
    void insertTariff(Tariff tariff);
    
    /**
     * 逻辑删除资费信息
     * 
     * @param tariff
     * @return
     */
    void updateTariffIsdeleted(Tariff tariff);
    
    /**
     * 资费类型
     * 
     * @return
     */
    List<String> selectTariffType();
    
    /**
     * 资费名称
     * 
     * @return
     */
    List<String> selectTariffName();
    
    /**
     * 用户等级关联查询资费
     * 
     * @param tariff_type
     * @return
     */
    Tariff selectTariffByTariffType(int tariff_type);
    
    /**
     * 名称重复检查
     * 
     * @param tariff_name
     * @return
     */
    List<Tariff> checkTariffName(String tariff_name);
    
}
