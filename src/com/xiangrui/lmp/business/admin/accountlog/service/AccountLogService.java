package com.xiangrui.lmp.business.admin.accountlog.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;

/**
 * 虚拟账号操作接口
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:37:10
 *         </p>
 */
public interface AccountLogService
{

    /**
     * 查询所有用户虚拟找好信息
     * 
     * @param params
     * @return
     */
    List<AccountLog> queryAll(Map<String, Object> params);

    /**
     * 通过ID查找
     * 
     * @param log_id
     */
    AccountLog selectAccountLogByLogId(int log_id);
    
    /**
     * 查询没有 LogisticsCode 的日志(与仓库无关的)
     * @return
     */
    List<AccountLog> selectByLogisticsCodeNull();
    
    /**
     * 通过logIDList来查找
     * @param logIdList
     * @return
     */
    List<AccountLog> selectAccountLogByLogIdList(List<String> logIdList);

    /**
     * 通过订单号查询
     * 
     * @param order_id
     * @return
     */
    AccountLog selectAccountLogByOrderId(String order_id);

     /**
     * 查询单个个用户的虚拟账号的记录,带对象的各种条件 其实仅仅是一个用户id,开始时间,结束时间
     *
     * @param params
     * @return
     */
     List<AccountLog> queryAccountLogForAccountBalance(Map<String, Object> params);
    
     /**
      * 通过公司单号查询
      * 
      * @param logistics_code
      * @return
      */
     AccountLog selectAccountLogByLogistics_code(String logistics_code);

}
