package com.xiangrui.lmp.business.admin.accountlog.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.accountlog.mapper.AccountLogMapper;
import com.xiangrui.lmp.business.admin.accountlog.service.AccountLogService;
import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;

/**
 * 虚拟金额
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:33:25
 *         </p>
 */
@Service("accountLogService")
public class AccountLogServiceImpl implements AccountLogService
{

    /**
     * 虚拟金额
     */
    @Autowired
    private AccountLogMapper accountLogMapper;

    /**
     * 查询所有
     * 
     * @param params
     * @return
     */
    @Override
    public List<AccountLog> queryAll(Map<String, Object> params)
    {
        return accountLogMapper.queryAll(params);
    }

    /**
     * 通过ID查找
     * 
     * @param log_id
     * @return
     */
    @Override
    public AccountLog selectAccountLogByLogId(int log_id)
    {
        return accountLogMapper.selectAccountLogByLogId(log_id);
    }
    
    @Override
    public List<AccountLog> selectByLogisticsCodeNull(){
    	return accountLogMapper.selectByLogisticsCodeNull();
    }

    /**
     * 通过logIDList来查找
     * @param logIdList
     * @return
     */
    public List<AccountLog> selectAccountLogByLogIdList(List<String> logIdList)
    {
    	return accountLogMapper.selectAccountLogByLogIdList(logIdList);
    }
    /**
     * 通过订单号查找
     * 
     * @param order_id
     * @return
     */
    @Override
    public AccountLog selectAccountLogByOrderId(String order_id)
    {

        return accountLogMapper.selectAccountLogByOrderId(order_id);
    }

    /**
     * 查询单个个用户的虚拟账号的记录,带对象的各种条件 其实仅仅是一个用户id,开始时间,结束时间
     *
     * @param params
     * @return
     */
    @Override
    public List<AccountLog> queryAccountLogForAccountBalance(
            Map<String, Object> params)
    {

        return accountLogMapper.queryAccountLogForAccountBalance(params);
    }
    /**
     * 通过公司单号查询
     * 
     * @param logistics_code
     * @return
     */
   public AccountLog selectAccountLogByLogistics_code(String logistics_code)
   {
	   	AccountLog reAccountLog=null;
	   	AccountLog queryAccountLog=new AccountLog();
	   	queryAccountLog.setLogistics_code(logistics_code);
	   	List<AccountLog> accountLogList=accountLogMapper.selectAccountLogByLogistics_code(queryAccountLog);
	   	if(accountLogList!=null&&accountLogList.size()>0)
	   	{
	   		reAccountLog=accountLogList.get(0);
	   	}
	   	return reAccountLog;
   }
}
