package com.xiangrui.lmp.business.admin.accountlog.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;

/**
 * 虚拟金额
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:32:45
 *         </p>
 */
public interface AccountLogMapper
{

    /**
     * 查询所有虚拟账号
     * 
     * @param params
     * @return
     */
    List<AccountLog> queryAll(Map<String, Object> params);

    /**
     * 通过id 查找
     * 
     * @param log_id
     * @return
     */
    AccountLog selectAccountLogByLogId(int log_id);
    
    /**
     * 查询没有 LogisticsCode 的日志(与仓库无关的)
     * @return
     */
    List<AccountLog> selectByLogisticsCodeNull();

    /**
     * 通过logIDList来查找
     * @param logIdList
     * @return
     */
    List<AccountLog> selectAccountLogByLogIdList(List<String> logIdList);
    /**
     * 通过订单号查询
     * 
     * @param order_id
     * @return
     */
    AccountLog selectAccountLogByOrderId(String order_id);

    /**
     * 更新状态
     * 
     * @param accountLog
     */
    void updateStatus(AccountLog accountLog);

    /**
     * 查询单个个用户的虚拟账号的记录,带对象的各种条件 其实仅仅是一个用户id,开始时间,结束时间
     * 
     * @param params
     * @return
     */
    List<AccountLog> queryAccountLogForAccountBalance(Map<String, Object> params);
    /**
     * 通过公司单号查询
     * 
     * @param logistics_code
     * @return
     */
    List<AccountLog> selectAccountLogByLogistics_code(AccountLog accountLog);
}
