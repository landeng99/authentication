package com.xiangrui.lmp.business.admin.accountlog.vo;

import com.xiangrui.lmp.business.base.BaseFrontUser;

/**
 * 虚拟账号专用的前台用户信息
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午2:41:10
 * </p>
 */
public class FrontUserAccount extends BaseFrontUser
{

    private static final long serialVersionUID = 1L;

}
