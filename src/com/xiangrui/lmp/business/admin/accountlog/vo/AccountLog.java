package com.xiangrui.lmp.business.admin.accountlog.vo;

import com.xiangrui.lmp.business.base.BaseAccountLog;

/**
 * 虚拟账号表
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:37:51
 *         </p>
 */
public class AccountLog extends BaseAccountLog
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 账户注册时的姓名，
     */
    private String real_name;

    /**
     * 账户
     */
    private String account;

    public String getReal_name()
    {
        return real_name;
    }

    public void setReal_name(String real_name)
    {
        this.real_name = real_name;
    }

    public String getAccount()
    {
        return account;
    }

    public void setAccount(String account)
    {
        this.account = account;
    }
}
