package com.xiangrui.lmp.business.admin.accountlog.controller;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.accountlog.service.AccountLogService;
import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;
import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.cost.service.MemberBillService;
import com.xiangrui.lmp.business.admin.cost.vo.MemberBillModel;
import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.CouponUsed;
import com.xiangrui.lmp.business.admin.freightcost.service.FreightCostService;
import com.xiangrui.lmp.business.admin.freightcost.vo.FreightCost;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.ServletContainer;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 消费记录
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午2:20:52
 *         </p>
 */
@Controller
@RequestMapping("/admin/accountLog")
public class AccountLogController
{

	/**
	 * 消费记录
	 */
	@Autowired
	private AccountLogService accountLogService;

	/**
	 * 申请人的信息管理
	 */
	@Autowired
	private FrontUserService frontUserService;

	/**
	 * 包裹信息管理
	 */
	@Autowired
	private PackageService packageService;
	/**
	 * 增值服务管理
	 */
	@Autowired
	private AttachServiceService attachServiceService;
	/**
	 * 同行帐单详情管理
	 */
	@Autowired
	private MemberBillService memberBillService;
	@Autowired
	private FreightCostService freightCostService;
	
	/**
	 * 优惠券处理服务
	 */
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private OverseasAddressService overseasAddressService;
	
	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";
	/**
	 * 时间格式化
	 */
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * 查询账号初始化
	 * 
	 * @param request
	 * @param accountLog
	 * @return
	 */
	@RequestMapping("queryAllOne")
	public String queryAll(HttpServletRequest request)
	{
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);

//		List<AccountLog> accountLogList = accountLogService.queryAll(params);
		List<AccountLog> accountLogList = this.queryAllByBackUserId(params);
		request.setAttribute("pageView", pageView);
		request.setAttribute("accountLogList", accountLogList);
		return "back/accountLogList";
	}

	/**
	 * 查询
	 * 
	 * @param request
	 * @param user_name
	 * @param account
	 * @param status
	 * @param account_type
	 * @param timeStart
	 * @param timeEnd
	 * @param order_id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("search")
	public String search(HttpServletRequest request, String user_name, String account, Integer status, Integer account_type, String timeStart, String timeEnd, String order_id, String logistics_code, Integer user_type,String rebatesme_type)
	{
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		}
		else
		{
			pageView = new PageView(1);

			params.put("user_name", user_name);
			params.put("account", account);
			params.put("status", status);
			params.put("account_type", account_type);
			params.put("timeStart", timeStart);
			params.put("timeEnd", timeEnd);
			params.put("order_id", order_id);
			params.put("logistics_code", logistics_code);
			params.put("user_type", user_type);
			params.put("rebatesme_type", rebatesme_type);
			
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}

		params.put("pageView", pageView);

//		List<AccountLog> accountLogList = accountLogService.queryAll(params);
		List<AccountLog> accountLogList = this.queryAllByBackUserId(params);
		request.setAttribute("pageView", pageView);
		// 页面显示用
		request.setAttribute("params", params);
		request.setAttribute("accountLogList", accountLogList);
		return "back/accountLogList";
	}
	
	private List<AccountLog> queryAllByBackUserId(Map<String, Object> params){
		User user = ServletContainer.getCurBackUser();
		return this.queryAllByBackUserIdEx(params, user.getUser_id());
	}
	
	/**
	 * 增加当前后台账号关联角色来过滤
	 * @param params
	 * @return
	 */
	private List<AccountLog> queryAllByBackUserIdEx(Map<String, Object> params, int backUserId){
		List<AccountLog> accountLogList = accountLogService.queryAll(params);
		if( backUserId<=0 || null==accountLogList || accountLogList.size()==0 )
			return accountLogList;
		
		List<String> codeListLog = new LinkedList<String>(); 
		for( AccountLog log: accountLogList ){
			for( String tmp: log.getLogistisCodeList() )
				codeListLog.add(tmp);
		}
		List<Pkg> pkgList = packageService.queryPackageByLogistics_codeList(codeListLog);
		
		// 重新找一个 user 与 logList 的交集 codeList
		List<String> codeListAddr = new LinkedList<String>();
		List<OverseasAddress> addrList = overseasAddressService.queryOverseasAddressByUserId(backUserId);
		for( Pkg pkg : pkgList ){
			for( OverseasAddress addr: addrList ){
				if( pkg.getOsaddr_id()==addr.getId() ){
					codeListAddr.add(pkg.getLogistics_code());
					break;
				}
			}
		}
		
		List<AccountLog> resultList = new LinkedList<AccountLog>();
		for( AccountLog log: accountLogList ){
			for( String lCode: codeListAddr ){
				if( log.getLogistisCodeList().contains(lCode) ){
					resultList.add(log);
//					codeListAddr.remove(lCode);
//					break; 支付运费与关税很可能有两条记录，所以会有两个一样的单号，不能  break
				}
			}
		}
		
		// 最后再拼上与仓库无关记录
		accountLogList = accountLogService.selectByLogisticsCodeNull();
		resultList.addAll(accountLogList);
		
		return resultList;
	}

	/**
	 * 详情初始化
	 * 
	 * @param request
	 * @param log_id
	 * @return
	 */
	@RequestMapping("/detail")
	public String detail(HttpServletRequest request, Integer log_id)
	{
		AccountLog accountLog = accountLogService.selectAccountLogByLogId(log_id);

		FrontUser frontUser = frontUserService.queryFrontUserById(accountLog.getUser_id());
		// 通过公司单号查询出对应的包裹
		List<Pkg> packageList = null;
		if (accountLog.getLogistics_code() != null)
		{
			String logisticsStr = accountLog.getLogistics_code();
			String[] logisticsArray = logisticsStr.split("<br>");
			List<String> logistics_codeList = new ArrayList<String>();
			if (logisticsArray != null && logisticsArray.length > 0)
			{
				for (String tempLogisticsCode : logisticsArray)
				{
					logistics_codeList.add(tempLogisticsCode);
				}
				float actualPay=NumberUtils.scaleMoneyDataFloat(accountLog.getAmount());
				if(logisticsArray.length==1)
				{
					CouponUsed couponUsed=couponService.queryCouponUsedByOrder_code(logisticsArray[0]);
					request.setAttribute("couponUsed", couponUsed);
					
					if(couponUsed!=null)
					{
						actualPay=NumberUtils.scaleMoneyDataFloat(accountLog.getAmount()+(float)couponUsed.getDenomination());
					}
				}
				request.setAttribute("actualPay", actualPay);
			}
			packageList = packageService.queryPackageByLogistics_codeList(logistics_codeList);

		}
		frontUser.setAble_balance(NumberUtils.scaleMoneyDataFloat(frontUser.getAble_balance()));
		frontUser.setBalance(NumberUtils.scaleMoneyDataFloat(frontUser.getBalance()));
		frontUser.setFrozen_balance(NumberUtils.scaleMoneyDataFloat(frontUser.getFrozen_balance()));
		accountLog.setAmount(NumberUtils.scaleMoneyDataFloat(accountLog.getAmount()));
		accountLog.setCustoms_cost(NumberUtils.scaleMoneyDataFloat(accountLog.getCustoms_cost()));
		if (packageList != null && packageList.size() > 0)
		{
			for (Pkg pkg : packageList)
			{
				pkg.setPrice(NumberUtils.scaleMoneyDataFloat(pkg.getPrice()));
				pkg.setFreight(NumberUtils.scaleMoneyDataFloat(pkg.getFreight()));
				pkg.setTotalServicePrice(NumberUtils.scaleMoneyDataFloat(pkg.getTotalServicePrice()));
				pkg.setTransport_cost(NumberUtils.scaleMoneyDataFloat(pkg.getTransport_cost()));
			}
		}
		request.setAttribute("packageList", packageList);
		request.setAttribute("accountLog", accountLog);
		request.setAttribute("frontUser", frontUser);

		return "back/accountLogInfo";
	}

	/*    *//**
	 * 导出消费记录
	 * 
	 * @param request
	 * @param response
	 * @param logIds
	 */
	/*
	 * 
	 * @RequestMapping("/exportList") public void exportList(HttpServletRequest
	 * request, HttpServletResponse response, String logIds) { try { //
	 * 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开 response.reset(); //
	 * 中文名称 String fileName = "消费记录";
	 * 
	 * response.setContentType("multipart/form-data"); //
	 * 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
	 * response.setHeader("Content-Disposition", "attachment;filename=" + new
	 * String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
	 * request.setCharacterEncoding("UTF-8");
	 * 
	 * List<AccountLog> accountLogList = accountLogService
	 * .selectAccountLogByLogIdList(StringUtil.toStringList( logIds, ","));
	 * 
	 * XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
	 * 
	 * // 新建sheet XSSFSheet xssfSheet = xssfWorkbook.createSheet("消费记录"); //
	 * 第一列固定 xssfSheet.createFreezePane(0, 1, 0, 1);
	 * 
	 * // 颜色黄色 XSSFColor yellowColor = new XSSFColor(Color.YELLOW);
	 * 
	 * // 样式黄色居中 XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
	 * style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
	 * style2.setFillForegroundColor(yellowColor); style2.setWrapText(true);
	 * 
	 * // 列宽 xssfSheet.setColumnWidth(0, 1500); xssfSheet.setColumnWidth(1,
	 * 2000); xssfSheet.setColumnWidth(2, 5000); xssfSheet.setColumnWidth(3,
	 * 4000); xssfSheet.setColumnWidth(4, 4000); xssfSheet.setColumnWidth(5,
	 * 4000); xssfSheet.setColumnWidth(6, 4000); xssfSheet.setColumnWidth(7,
	 * 5000); xssfSheet.setColumnWidth(8, 4000); xssfSheet.setColumnWidth(9,
	 * 6000); xssfSheet.setColumnWidth(10, 5000); xssfSheet.setColumnWidth(11,
	 * 4000); xssfSheet.setColumnWidth(12, 5000); xssfSheet.setColumnWidth(13,
	 * 5000); xssfSheet.setColumnWidth(14, 5000); xssfSheet.setColumnWidth(15,
	 * 4000); xssfSheet.setColumnWidth(16, 4000);
	 * 
	 * // 增值服务 List<String> serviceNameList = attachServiceService
	 * .queryAttachServiceName(); int serviceCount=0; if(serviceNameList!=null)
	 * { serviceCount=serviceNameList.size(); }
	 * xssfSheet.setColumnWidth(17+serviceCount, 5000);
	 * xssfSheet.setColumnWidth(18+serviceCount, 6000);
	 * xssfSheet.setColumnWidth(19+serviceCount, 8000);
	 * xssfSheet.setColumnWidth(20+serviceCount, 5000);
	 * xssfSheet.setColumnWidth(21+serviceCount, 4000);
	 * xssfSheet.setColumnWidth(22+serviceCount, 4000);
	 * xssfSheet.setColumnWidth(23+serviceCount, 1000);
	 * xssfSheet.setColumnWidth(24+serviceCount, 1500);
	 * xssfSheet.setColumnWidth(25+serviceCount, 5000);
	 * xssfSheet.setColumnWidth(26+serviceCount, 5000);
	 * xssfSheet.setColumnWidth(27+serviceCount, 5000);
	 * 
	 * // 表头列 XSSFRow firstXSSFRow = xssfSheet.createRow(0);
	 * 
	 * List<String> columnsList = new ArrayList<String>();
	 * columnsList.add("业务"); columnsList.add("客户姓名"); columnsList.add("客户账号");
	 * columnsList.add("客户手机号"); columnsList.add("账户余额");
	 * columnsList.add("可用余额"); columnsList.add("冻结余额");
	 * columnsList.add("公司运单号"); columnsList.add("所属仓库"); columnsList.add("品名");
	 * columnsList.add("品牌"); columnsList.add("申报类别");
	 * columnsList.add("申报总价值(RMB)"); columnsList.add("重量(实际/磅)");
	 * columnsList.add("重量(收费/磅)"); columnsList.add("单价(RMB)");
	 * columnsList.add("运费(RMB)");
	 * 
	 * // 增值服务 columnsList.addAll(serviceNameList);
	 * 
	 * columnsList.add("合计(RMB)"); columnsList.add("支付宝号");
	 * columnsList.add("订单号"); columnsList.add("支付宝流水号");
	 * columnsList.add("银行流水号"); columnsList.add("金额"); columnsList.add("类型");
	 * columnsList.add("状态"); columnsList.add("交易时间"); columnsList.add("描述");
	 * columnsList.add("包裹创建日期");
	 * 
	 * for (int i = 0; i < columnsList.size(); i++) { XSSFCell cell =
	 * firstXSSFRow.createCell(i); cell.setCellType(XSSFCell.CELL_TYPE_STRING);
	 * cell.setCellValue(columnsList.get(i)); cell.setCellStyle(style2); }
	 * 
	 * for (int j = 0; j < accountLogList.size(); j++) { XSSFRow row =
	 * xssfSheet.createRow(j + 1); FrontUser frontUser =
	 * frontUserService.queryFrontUserById(accountLogList.get(j).getUser_id());
	 * 
	 * //包裹详情 Pkg pkgDetail =null; //同行帐单信息 MemberBillModel
	 * memberBillModel=null; if (accountLogList.get(j).getLogistics_code() !=
	 * null) { // 通过公司单号查询出对应的包裹 List<Pkg> packageList =
	 * packageService.queryPackageByLogistics_code
	 * (accountLogList.get(j).getLogistics_code());
	 * if(packageList!=null&&packageList.size()>0) { // 包裹 pkgDetail =
	 * packageService.queryPackageById(packageList.get(0).getPackage_id());
	 * List<MemberBillModel> billList =
	 * memberBillService.queryFrontUserBillByuserIds
	 * (StringUtil.toStringList(""+accountLogList.get(j).getUser_id(), ","));
	 * if(billList!=null&&billList.size()>0) { memberBillModel=billList.get(0);
	 * } } }
	 * 
	 * 
	 * // 业务 XSSFCell cell0 = row.createCell(0);
	 * cell0.setCellValue(frontUser.getBusiness_name());
	 * 
	 * // 客户姓名 XSSFCell cell1 = row.createCell(1);
	 * cell1.setCellValue(frontUser.getUser_name());
	 * 
	 * // 客户账号 XSSFCell cell2 = row.createCell(2);
	 * cell2.setCellValue(frontUser.getAccount());
	 * 
	 * // 客户手机号 XSSFCell cell3 = row.createCell(3);
	 * cell3.setCellValue(""+frontUser.getMobile());
	 * 
	 * // 账户余额 XSSFCell cell4 = row.createCell(4);
	 * cell4.setCellValue(""+frontUser.getBalance());
	 * 
	 * // 可用余额 XSSFCell cell5 = row.createCell(5);
	 * cell5.setCellValue(""+frontUser.getAble_balance());
	 * 
	 * // 冻结余额 XSSFCell cell6 = row.createCell(6);
	 * cell6.setCellValue(""+frontUser.getFrozen_balance());
	 * 
	 * // 公司运单号 XSSFCell cell7 = row.createCell(7);
	 * cell7.setCellValue(accountLogList.get(j).getLogistics_code());
	 * 
	 * // 所属仓库 XSSFCell cell8 = row.createCell(8);
	 * cell8.setCellValue(pkgDetail!=null?pkgDetail.getWarehouse():"");
	 * 
	 * //品名 XSSFCell cell9 = row.createCell(9);
	 * cell9.setCellValue(memberBillModel
	 * !=null?memberBillModel.getGoods_name():"");
	 * 
	 * //品牌 XSSFCell cell10 = row.createCell(10);
	 * cell10.setCellValue(memberBillModel!=null?memberBillModel.getBrand():"");
	 * 
	 * //申报类别 XSSFCell cell11 = row.createCell(11);
	 * cell11.setCellValue(memberBillModel
	 * !=null?memberBillModel.getGoods_type():"");
	 * 
	 * //申报总价值(RMB) XSSFCell cell12 = row.createCell(12);
	 * cell12.setCellValue(pkgDetail!=null?""+pkgDetail.getTotal_worth():"");
	 * 
	 * //重量(实际/磅) XSSFCell cell13 = row.createCell(13);
	 * cell13.setCellValue(pkgDetail!=null?""+pkgDetail.getActual_weight():"");
	 * 
	 * //重量(收费/磅) XSSFCell cell14 = row.createCell(14);
	 * cell14.setCellValue(pkgDetail!=null?""+pkgDetail.getWeight():"");
	 * 
	 * //单价(RMB) XSSFCell cell15 = row.createCell(15);
	 * cell15.setCellValue(pkgDetail!=null?""+pkgDetail.getPrice():"");
	 * 
	 * //运费(RMB) XSSFCell cell16 = row.createCell(16);
	 * cell16.setCellValue(pkgDetail!=null?""+pkgDetail.getFreight():"");
	 * 
	 * // 增值服务 for (int k = 0; k < serviceNameList.size(); k++) {
	 * 
	 * XSSFCell attachServiceCell = row.createCell(17 + k); if (memberBillModel
	 * != null) { Map<String, Float> map =
	 * memberBillModel.getAttach_service_map(); if (map != null &&
	 * !map.isEmpty()) { Float price = map.get(serviceNameList.get(k)); if
	 * (price != null && price.compareTo(0.000001f) > 0) {
	 * attachServiceCell.setCellValue(Double.parseDouble(price.toString())); } }
	 * }else { attachServiceCell.setCellValue(""); } } // 合计(RMB) XSSFCell
	 * cell17 = row.createCell(17+serviceCount);
	 * cell17.setCellValue(memberBillModel
	 * !=null?""+memberBillModel.getTransport_cost():"");
	 * 
	 * //支付宝号 XSSFCell cell18 = row.createCell(18+serviceCount);
	 * cell18.setCellValue(accountLogList.get(j).getAlipay_no());
	 * 
	 * //订单号 XSSFCell cell19 = row.createCell(19+serviceCount);
	 * cell19.setCellValue(accountLogList.get(j).getOrder_id());
	 * 
	 * //支付宝流水号 XSSFCell cell20 = row.createCell(20+serviceCount);
	 * cell20.setCellValue(accountLogList.get(j).getTrade_no());
	 * 
	 * //银行流水号 XSSFCell cell21 = row.createCell(21+serviceCount);
	 * cell21.setCellValue(accountLogList.get(j).getBank_seq_no());
	 * 
	 * //金额 XSSFCell cell22 = row.createCell(22+serviceCount);
	 * cell22.setCellValue(accountLogList.get(j).getAccount());
	 * 
	 * //类型 XSSFCell cell23 = row.createCell(23+serviceCount); int
	 * type=accountLogList.get(j).getAccount_type(); String accountType=""; if
	 * (type == 1) { accountType = "充值"; } else if (type == 2) { accountType =
	 * "消费"; } else if (type == 3) { accountType = "提现"; } else if (type == 4) {
	 * accountType = "索赔"; } cell23.setCellValue(accountType);
	 * 
	 * //状态 XSSFCell cell24 = row.createCell(24+serviceCount); int
	 * status=accountLogList.get(j).getStatus(); String accountStatus=""; if
	 * (status == 1) { accountStatus = "申请中"; } else if (status == 2) {
	 * accountStatus = "成功"; } else if (status == 3) { accountStatus = "失败"; }
	 * else if (status == 4) { accountStatus = "取消"; }
	 * cell24.setCellValue(accountStatus);
	 * 
	 * //交易时间 XSSFCell cell25 = row.createCell(25+serviceCount);
	 * cell25.setCellValue(accountLogList.get(j).getOpr_time_string());
	 * 
	 * //描述 XSSFCell cell26 = row.createCell(26+serviceCount);
	 * cell26.setCellValue(accountLogList.get(j).getDescription());
	 * 
	 * // 包裹创建日期 XSSFCell cell27 = row.createCell(27+serviceCount); String
	 * packageCreateDateString=""; if(pkgDetail!=null) {
	 * packageCreateDateString=
	 * simpleDateFormat.format(pkgDetail.getCreateTime()); }
	 * cell27.setCellValue(packageCreateDateString);
	 * 
	 * } OutputStream oStream = response.getOutputStream();
	 * 
	 * xssfWorkbook.write(oStream); oStream.flush(); oStream.close(); } catch
	 * (IOException e) { e.printStackTrace();
	 * 
	 * } }
	 */
	/**
	 * 导出消费记录
	 * 
	 * @param request
	 * @param response
	 * @param logIds
	 */

	@RequestMapping("/exportList")
	public void exportList(HttpServletRequest request, HttpServletResponse response, String user_name, String account, Integer status, Integer account_type, String timeStart, String timeEnd, String order_id, String logistics_code, Integer user_type,String rebatesme_type)
	{
		try
		{
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "消费记录";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");

			user_name=StringUtils.trimToNull(user_name);
			if(user_name!=null)
			{
				try
				{
					user_name=new String(user_name.getBytes("iso-8859-1"), "utf-8");
				} catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}
			}
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("user_name", user_name);
			params.put("account", account);
			params.put("status", status);
			params.put("account_type", account_type);
			params.put("timeStart", timeStart);
			params.put("timeEnd", timeEnd);
			params.put("order_id", order_id);
			params.put("logistics_code", logistics_code);
			params.put("user_type", user_type);
			params.put("rebatesme_type", rebatesme_type);
//			List<AccountLog> accountLogList = accountLogService.queryAll(params);
			List<AccountLog> accountLogList = this.queryAllByBackUserId(params);

			XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

			// 新建sheet
			XSSFSheet xssfSheet = xssfWorkbook.createSheet("消费记录");
			// 第一列固定
			xssfSheet.createFreezePane(0, 1, 0, 1);

			// 颜色黄色
			XSSFColor yellowColor = new XSSFColor(Color.YELLOW);

			// 样式黄色居中
			XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
			style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			style2.setFillForegroundColor(yellowColor);
			style2.setWrapText(true);
			int colCount = 0;
			// 列宽
			xssfSheet.setColumnWidth(colCount++, 1500);
			xssfSheet.setColumnWidth(colCount++, 2000);
			xssfSheet.setColumnWidth(colCount++, 5000);
			xssfSheet.setColumnWidth(colCount++, 4000);
			xssfSheet.setColumnWidth(colCount++, 4000);
			xssfSheet.setColumnWidth(colCount++, 4000);
			xssfSheet.setColumnWidth(colCount++, 4000);
			xssfSheet.setColumnWidth(colCount++, 5000);
			xssfSheet.setColumnWidth(colCount++, 5000);
			xssfSheet.setColumnWidth(colCount++, 4000);
			xssfSheet.setColumnWidth(colCount++, 6000);
			xssfSheet.setColumnWidth(colCount++, 5000);
			xssfSheet.setColumnWidth(colCount++, 4000);
			xssfSheet.setColumnWidth(colCount++, 5000);
			xssfSheet.setColumnWidth(colCount++, 5000);
			xssfSheet.setColumnWidth(colCount++, 5000);
			xssfSheet.setColumnWidth(colCount++, 4000);
			xssfSheet.setColumnWidth(colCount++, 4000);

			// 增值服务
			List<String> serviceNameList = attachServiceService.queryAttachServiceName();
			int serviceCount = 0;
			if (serviceNameList != null)
			{
				serviceCount = serviceNameList.size();
			}
			xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 6000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 8000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 4000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 4000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 1000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 1500);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
			colCount++;
			xssfSheet.setColumnWidth(colCount + serviceCount, 5000);

			// 表头列
			XSSFRow firstXSSFRow = xssfSheet.createRow(0);

			List<String> columnsList = new ArrayList<String>();
			columnsList.add("业务");
			columnsList.add("客户姓名");
			columnsList.add("客户账号");
			columnsList.add("客户手机号");
			columnsList.add("账户余额");
			columnsList.add("可用余额");
			columnsList.add("冻结余额");
			columnsList.add("公司运单号");
			columnsList.add("关联单号");
			columnsList.add("所属仓库");
			columnsList.add("品名");
			columnsList.add("品牌");
			columnsList.add("申报类别");
			columnsList.add("申报总价值(RMB)");
			columnsList.add("重量(实际/磅)");
			columnsList.add("重量(收费/磅)");
			columnsList.add("单价(RMB)");
			columnsList.add("运费(RMB)");

			// 增值服务
			columnsList.addAll(serviceNameList);

			columnsList.add("合计(RMB)");
			columnsList.add("税金");
			columnsList.add("支付宝号");
			columnsList.add("订单号");
			columnsList.add("支付宝流水号");
			columnsList.add("银行流水号");
			columnsList.add("金额");
			columnsList.add("类型");
			columnsList.add("状态");
			columnsList.add("交易时间");
			columnsList.add("描述");
			columnsList.add("包裹创建日期");
			columnsList.add("优惠券名称");
			columnsList.add("优惠券面值");
			columnsList.add("实际支付金额");
			columnsList.add("返利流水号");
			columnsList.add("（现金支付）收银员");

			for (int i = 0; i < columnsList.size(); i++)
			{
				XSSFCell cell = firstXSSFRow.createCell(i);
				cell.setCellType(XSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(columnsList.get(i));
				cell.setCellStyle(style2);
			}

			int rowCount = 1;
			for (int j = 0; j < accountLogList.size(); j++)
			{
				XSSFRow row = null;
				FrontUser frontUser = frontUserService.queryFrontUserById(accountLogList.get(j).getUser_id());
				float price = 0;
				if (frontUser.getUser_type() == 1)
				{
					// 资费费率
					Map<String, Object> parmas = new HashMap<String, Object>();
					parmas.put("integral_min", frontUser.getIntegral());
					parmas.put("rate_type", frontUser.getUser_type());

					FreightCost freightCost = freightCostService.queryFreightCostByIntegralMin(parmas);
					if (freightCost != null)
					{
						price = freightCost.getUnit_price();
					}
				}

				// 包裹详情
				Pkg pkgDetail = null;
				List<Pkg> packageList = null;
				// 同行帐单信息
				MemberBillModel memberBillModel = null;
				boolean needToAddRow = false;
				if (accountLogList.get(j).getLogistics_code() != null)
				{
					// 通过公司单号查询出对应的包裹
					String logisticsStr = accountLogList.get(j).getLogistics_code();
					String[] logisticsArray = logisticsStr.split("<br>");
					List<String> logistics_codeList = new ArrayList<String>();
					if (logisticsArray != null && logisticsArray.length > 0)
					{
						for (String tempLogisticsCode : logisticsArray)
						{
							logistics_codeList.add(tempLogisticsCode);
						}
					}
					packageList = packageService.queryPackageByLogistics_codeList(logistics_codeList);

					if (packageList != null)
					{
						// 包裹
						if (packageList.size() == 1)
						{
							pkgDetail = packageList.get(0);
							memberBillModel = memberBillService.queryMemberBillModelByPackageId(pkgDetail.getPackage_id());
						}
						else if (packageList.size() > 1)
						{
							needToAddRow = true;
						}
					}
				}

				if (needToAddRow)
				{
					int colStart = 20;
					int packageListSize = packageList.size();
					/*
					 * 设定合并单元格区域范围 firstRow 0-based lastRow 0-based firstCol
					 * 0-based lastCol 0-based
					 */
					CellRangeAddress cra19 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					++colStart;
					// 在sheet里增加合并单元格
					xssfSheet.addMergedRegion(cra19);
					CellRangeAddress cra20 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					xssfSheet.addMergedRegion(cra20);
					++colStart;
					CellRangeAddress cra21 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					xssfSheet.addMergedRegion(cra21);
					++colStart;
					CellRangeAddress cra22 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					xssfSheet.addMergedRegion(cra22);
					++colStart;
					CellRangeAddress cra23 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					xssfSheet.addMergedRegion(cra23);
					++colStart;
					CellRangeAddress cra24 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					xssfSheet.addMergedRegion(cra24);
					++colStart;
					CellRangeAddress cra25 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					xssfSheet.addMergedRegion(cra25);
					++colStart;
					CellRangeAddress cra26 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					xssfSheet.addMergedRegion(cra26);
					++colStart;
					CellRangeAddress cra27 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, colStart + serviceCount, colStart + serviceCount);
					xssfSheet.addMergedRegion(cra27);
					++colStart;

					for (int p = 0; p < packageListSize; p++)
					{
						row = xssfSheet.createRow(rowCount++);
						pkgDetail = packageList.get(p);
						if (frontUser.getUser_type() == 1)
						{
							if (pkgDetail != null&&pkgDetail.getPrice()==0)
							{
								pkgDetail.setPrice(price);
							}
						}
						memberBillModel = memberBillService.queryMemberBillModelByPackageId(pkgDetail.getPackage_id());
						if (p == 0)
						{
							addRow(row, frontUser, accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList, serviceCount, true, true);
						}
						else
						{
							addRow(row, frontUser, accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList, serviceCount, false, true);
						}
					}
				}
				else
				{
					if (frontUser.getUser_type() == 1)
					{
						if (pkgDetail != null&&pkgDetail.getPrice()==0)
						{
							pkgDetail.setPrice(price);
						}
					}
					row = xssfSheet.createRow(rowCount++);
					if(accountLogList.get(j)!=null&&memberBillModel!=null&&pkgDetail!=null)
					addRow(row, frontUser, accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList, serviceCount, true, true);
				}
			}
			OutputStream oStream = response.getOutputStream();

			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
			xssfWorkbook.close();
		} catch (IOException e)
		{
			e.printStackTrace();

		}
	}

	private void addRow(XSSFRow row, FrontUser frontUser, AccountLog accountLog, MemberBillModel memberBillModel, Pkg pkgDetail, List<String> serviceNameList, int serviceCount, boolean needAddAccountLogCell, boolean showBusinessName)
	{
		int colCount = 0;
		if (showBusinessName)
		{
			// 业务
			XSSFCell cell0 = row.createCell(colCount++);
			cell0.setCellValue(frontUser.getBusiness_name());
		}

		// 客户姓名
		XSSFCell cell1 = row.createCell(colCount++);
		cell1.setCellValue(frontUser.getUser_name());

		// 客户账号
		XSSFCell cell2 = row.createCell(colCount++);
		cell2.setCellValue(frontUser.getAccount());

		// 客户手机号
		XSSFCell cell3 = row.createCell(colCount++);
		cell3.setCellValue(StringUtils.trimToEmpty(frontUser.getMobile()));

		// 账户余额
		XSSFCell cell4 = row.createCell(colCount++);
		cell4.setCellValue(NumberUtils.scaleMoneyData(frontUser.getBalance()));

		// 可用余额
		XSSFCell cell5 = row.createCell(colCount++);
		cell5.setCellValue(NumberUtils.scaleMoneyData(frontUser.getAble_balance()));

		// 冻结余额
		XSSFCell cell6 = row.createCell(colCount++);
		cell6.setCellValue(NumberUtils.scaleMoneyData(frontUser.getFrozen_balance()));

		// 公司运单号
		XSSFCell cell7 = row.createCell(colCount++);
		cell7.setCellValue(pkgDetail != null ? pkgDetail.getLogistics_code() : "");

		// 关联单号
		XSSFCell cell8 = row.createCell(colCount++);
		cell8.setCellValue(pkgDetail != null ? pkgDetail.getOriginal_num() : "");

		// 所属仓库
		XSSFCell cell8_1 = row.createCell(colCount++);
		cell8_1.setCellValue(memberBillModel != null ? memberBillModel.getWarehouse() : "");

		// 品名
		XSSFCell cell9 = row.createCell(colCount++);
		cell9.setCellValue(memberBillModel != null ? memberBillModel.getGoods_name() : "");

		// 品牌
		XSSFCell cell10 = row.createCell(colCount++);
		cell10.setCellValue(memberBillModel != null ? memberBillModel.getBrand() : "");

		// 申报类别
		XSSFCell cell11 = row.createCell(colCount++);
		cell11.setCellValue(memberBillModel != null ? memberBillModel.getGoods_type() : "");

		// 申报总价值(RMB)
		XSSFCell cell12 = row.createCell(colCount++);
		cell12.setCellValue(memberBillModel != null ? NumberUtils.scaleMoneyData(memberBillModel.getTotal_worth()) : "");

		// 重量(实际/磅)
		XSSFCell cell13 = row.createCell(colCount++);
		cell13.setCellValue(pkgDetail != null ? "" + pkgDetail.getActual_weight() : "");

		// 重量(收费/磅)
		XSSFCell cell14 = row.createCell(colCount++);
		cell14.setCellValue(pkgDetail != null ? "" + pkgDetail.getWeight() : "");

		// 单价(RMB)
		XSSFCell cell15 = row.createCell(colCount++);
		cell15.setCellValue(pkgDetail != null ? NumberUtils.scaleMoneyData(pkgDetail.getPrice()) : "");

		// 运费(RMB)
		XSSFCell cell16 = row.createCell(colCount++);
		cell16.setCellValue(pkgDetail != null ? NumberUtils.scaleMoneyData(pkgDetail.getFreight()) : "");

		// 增值服务
		for (int k = 0; k < serviceNameList.size(); k++)
		{

			XSSFCell attachServiceCell = row.createCell(colCount + k);
			if (memberBillModel != null)
			{
				Map<String, Float> map = memberBillModel.getAttach_service_map();
				if (map != null && !map.isEmpty())
				{
					Float price = map.get(serviceNameList.get(k));
					if (price != null && price.compareTo(0.000001f) > 0)
					{
						attachServiceCell.setCellValue(Double.parseDouble(price.toString()));
					}
				}
			}
			else
			{
				attachServiceCell.setCellValue("");
			}
		}

		// 合计(RMB)
		XSSFCell cell17 = row.createCell(colCount + serviceCount);
		colCount++;
		cell17.setCellValue(memberBillModel != null ? NumberUtils.scaleMoneyData(memberBillModel.getTransport_cost()) : "");
		
		// 税金(RMB)
		XSSFCell cell17_ = row.createCell(colCount + serviceCount);
		colCount++;
		cell17_.setCellValue(NumberUtils.scaleMoneyData(memberBillModel.getCustoms_cost()));
		
		if (needAddAccountLogCell)
		{
			// 支付宝号
			XSSFCell cell18 = row.createCell(colCount + serviceCount);
			cell18.setCellValue(accountLog.getAlipay_no());
			colCount++;

			// 订单号
			XSSFCell cell19 = row.createCell(colCount + serviceCount);
			cell19.setCellValue(accountLog.getOrder_id());
			colCount++;

			// 支付宝流水号
			XSSFCell cell20 = row.createCell(colCount + serviceCount);
			cell20.setCellValue(accountLog.getTrade_no());
			colCount++;

			// 银行流水号
			XSSFCell cell21 = row.createCell(colCount + serviceCount);
			cell21.setCellValue(accountLog.getBank_seq_no());
			colCount++;

			CouponUsed couponUsed=couponService.queryCouponUsedByOrder_code(pkgDetail != null ? pkgDetail.getLogistics_code() : "");
			float actualPay=NumberUtils.scaleMoneyDataFloat(accountLog.getAmount());
			if(couponUsed!=null)
			{
				actualPay=NumberUtils.scaleMoneyDataFloat(actualPay+(float)couponUsed.getDenomination());
			}
			
			// 金额
			XSSFCell cell22 = row.createCell(colCount + serviceCount);
			cell22.setCellValue(NumberUtils.scaleMoneyData(actualPay));
			colCount++;

			// 类型
			XSSFCell cell23 = row.createCell(colCount + serviceCount);
			int type = accountLog.getAccount_type();
			String accountType = "";
			if (type == 1)
			{
				accountType = "充值";
			}
			else if (type == 2)
			{
				accountType = "消费";
			}
			else if (type == 3)
			{
				accountType = "提现";
			}
			else if (type == 4)
			{
				accountType = "索赔";
			}
			cell23.setCellValue(accountType);
			colCount++;

			// 状态
			XSSFCell cell24 = row.createCell(colCount + serviceCount);
			int tempStatus = accountLog.getStatus();
			String accountStatus = "";
			if (tempStatus == 1)
			{
				accountStatus = "申请中";
			}
			else if (tempStatus == 2)
			{
				accountStatus = "成功";
			}
			else if (tempStatus == 3)
			{
				accountStatus = "失败";
			}
			else if (tempStatus == 4)
			{
				accountStatus = "取消";
			}
			cell24.setCellValue(accountStatus);
			colCount++;

			// 交易时间
			XSSFCell cell25 = row.createCell(colCount + serviceCount);
			cell25.setCellValue(accountLog.getOpr_time_string());
			colCount++;

			// 描述
			XSSFCell cell26 = row.createCell(colCount + serviceCount);
			cell26.setCellValue(accountLog.getDescription());
			colCount++;
		}
		// 包裹创建日期
		XSSFCell cell27 = row.createCell(colCount + serviceCount);
		String packageCreateDateString = "";
		if (pkgDetail != null)
		{
			packageCreateDateString = simpleDateFormat.format(pkgDetail.getCreateTime());
		}
		cell27.setCellValue(packageCreateDateString);
		colCount++;
		
		CouponUsed couponUsed=couponService.queryCouponUsedByOrder_code(pkgDetail != null ? pkgDetail.getLogistics_code() : "");
		float actualPay=NumberUtils.scaleMoneyDataFloat(accountLog.getAmount());
//		if(couponUsed!=null)
//		{
//			actualPay=NumberUtils.scaleMoneyDataFloat(accountLog.getAmount()-(float)couponUsed.getDenomination());
//		}
		
		// 优惠券名称
		XSSFCell cell28 = row.createCell(colCount + serviceCount);
		cell28.setCellValue(couponUsed!=null?couponUsed.getCoupon_name():"");
		colCount++;
		
		// 优惠券面值
		XSSFCell cell29 = row.createCell(colCount + serviceCount);
		cell29.setCellValue(couponUsed!=null?""+couponUsed.getDenomination():"");
		colCount++;
		
		// 实际支付金额
		XSSFCell cell30 = row.createCell(colCount + serviceCount);
		cell30.setCellValue(NumberUtils.scaleMoneyData(actualPay));
		colCount++;
		
		// 返利流水号
		XSSFCell cell31 = row.createCell(colCount + serviceCount);
		cell31.setCellValue(accountLog.getRebatesms_serial_no());
		colCount++;
		
		// （现金支付）收银员
		XSSFCell cell32 = row.createCell(colCount + serviceCount);
		cell32.setCellValue(accountLog.getBackUserName());
		colCount++;
	}

	/*	*//**
	 * 导出消费记录
	 * 
	 * @param request
	 * @param response
	 * @param logIds
	 */
	/*
	 * 
	 * @RequestMapping("/frontEndexportList") public void
	 * frontEndexportList(HttpServletRequest request, HttpServletResponse
	 * response, String timeStart, String timeEnd) { try { //
	 * 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开 response.reset(); //
	 * 中文名称 String fileName = "消费记录";
	 * 
	 * response.setContentType("multipart/form-data"); //
	 * 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
	 * response.setHeader("Content-Disposition", "attachment;filename=" + new
	 * String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
	 * request.setCharacterEncoding("UTF-8");
	 * 
	 * HttpSession session = request.getSession(); FrontUser tfrontUser =
	 * (FrontUser) session.getAttribute("frontUser");
	 * 
	 * Map<String, Object> params = new HashMap<String, Object>(); if
	 * (tfrontUser != null) {
	 * 
	 * params.put("user_name", tfrontUser.getUser_name());
	 * params.put("timeStart", timeStart); params.put("timeEnd", timeEnd);
	 * List<AccountLog> accountLogList = accountLogService.queryAll(params);
	 * 
	 * XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
	 * 
	 * // 新建sheet XSSFSheet xssfSheet = xssfWorkbook.createSheet("消费记录"); //
	 * 第一列固定 xssfSheet.createFreezePane(0, 1, 0, 1);
	 * 
	 * // 颜色黄色 XSSFColor yellowColor = new XSSFColor(Color.YELLOW);
	 * 
	 * // 样式黄色居中 XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
	 * style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
	 * style2.setFillForegroundColor(yellowColor); style2.setWrapText(true); int
	 * colCount = 0; // 列宽 xssfSheet.setColumnWidth(colCount++, 2000);
	 * xssfSheet.setColumnWidth(colCount++, 5000);
	 * xssfSheet.setColumnWidth(colCount++, 4000);
	 * xssfSheet.setColumnWidth(colCount++, 4000);
	 * xssfSheet.setColumnWidth(colCount++, 4000);
	 * xssfSheet.setColumnWidth(colCount++, 4000);
	 * xssfSheet.setColumnWidth(colCount++, 5000);
	 * xssfSheet.setColumnWidth(colCount++, 5000);
	 * xssfSheet.setColumnWidth(colCount++, 4000);
	 * xssfSheet.setColumnWidth(colCount++, 6000);
	 * xssfSheet.setColumnWidth(colCount++, 5000);
	 * xssfSheet.setColumnWidth(colCount++, 4000);
	 * xssfSheet.setColumnWidth(colCount++, 5000);
	 * xssfSheet.setColumnWidth(colCount++, 5000);
	 * xssfSheet.setColumnWidth(colCount++, 5000);
	 * xssfSheet.setColumnWidth(colCount++, 4000);
	 * xssfSheet.setColumnWidth(colCount++, 4000);
	 * 
	 * // 增值服务 List<String> serviceNameList =
	 * attachServiceService.queryAttachServiceName(); int serviceCount = 0; if
	 * (serviceNameList != null) { serviceCount = serviceNameList.size(); }
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 5000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 6000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 8000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 5000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 4000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 4000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 1000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 1500); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 5000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 5000); colCount++;
	 * xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
	 * 
	 * // 表头列 XSSFRow firstXSSFRow = xssfSheet.createRow(0);
	 * 
	 * List<String> columnsList = new ArrayList<String>();
	 * columnsList.add("客户姓名"); columnsList.add("客户账号");
	 * columnsList.add("客户手机号"); columnsList.add("账户余额");
	 * columnsList.add("可用余额"); columnsList.add("冻结余额");
	 * columnsList.add("公司运单号"); columnsList.add("关联单号");
	 * columnsList.add("所属仓库"); columnsList.add("品名"); columnsList.add("品牌");
	 * columnsList.add("申报类别"); columnsList.add("申报总价值(RMB)");
	 * columnsList.add("重量(实际/磅)"); columnsList.add("重量(收费/磅)");
	 * columnsList.add("单价(RMB)"); columnsList.add("运费(RMB)");
	 * 
	 * // 增值服务 columnsList.addAll(serviceNameList);
	 * 
	 * columnsList.add("合计(RMB)"); columnsList.add("支付宝号");
	 * columnsList.add("订单号"); columnsList.add("支付宝流水号");
	 * columnsList.add("银行流水号"); columnsList.add("金额"); columnsList.add("类型");
	 * columnsList.add("状态"); columnsList.add("交易时间"); columnsList.add("描述");
	 * columnsList.add("包裹创建日期");
	 * 
	 * for (int i = 0; i < columnsList.size(); i++) { XSSFCell cell =
	 * firstXSSFRow.createCell(i); cell.setCellType(XSSFCell.CELL_TYPE_STRING);
	 * cell.setCellValue(columnsList.get(i)); cell.setCellStyle(style2); }
	 * 
	 * int rowCount = 1; for (int j = 0; j < accountLogList.size(); j++) {
	 * XSSFRow row = null; FrontUser frontUser =
	 * frontUserService.queryFrontUserById(accountLogList.get(j).getUser_id());
	 * 
	 * // 包裹详情 Pkg pkgDetail = null; List<Pkg> packageList = null; // 同行帐单信息
	 * MemberBillModel memberBillModel = null; boolean needToAddRow = false; if
	 * (accountLogList.get(j).getLogistics_code() != null) { // 通过公司单号查询出对应的包裹
	 * String logisticsStr = accountLogList.get(j).getLogistics_code(); String[]
	 * logisticsArray = logisticsStr.split("<br>"); List<String>
	 * logistics_codeList = new ArrayList<String>(); if (logisticsArray != null
	 * && logisticsArray.length > 0) { for (String tempLogisticsCode :
	 * logisticsArray) { logistics_codeList.add(tempLogisticsCode); } }
	 * packageList =
	 * packageService.queryPackageByLogistics_codeList(logistics_codeList);
	 * 
	 * if (packageList != null) { // 包裹 if (packageList.size() == 1) { pkgDetail
	 * = packageList.get(0); memberBillModel =
	 * memberBillService.queryMemberBillModelByPackageId
	 * (pkgDetail.getPackage_id()); } else if (packageList.size() > 1) {
	 * needToAddRow = true; } } }
	 * 
	 * if (needToAddRow) { int packageListSize = packageList.size();
	 * 
	 * 设定合并单元格区域范围 firstRow 0-based lastRow 0-based firstCol 0-based lastCol
	 * 0-based
	 * 
	 * CellRangeAddress cra18 = new CellRangeAddress(rowCount, rowCount +
	 * packageListSize - 1, 18 + serviceCount, 18 + serviceCount);
	 * xssfSheet.addMergedRegion(cra18);
	 * 
	 * CellRangeAddress cra19 = new CellRangeAddress(rowCount, rowCount +
	 * packageListSize - 1, 19 + serviceCount, 19 + serviceCount);
	 * 
	 * // 在sheet里增加合并单元格 xssfSheet.addMergedRegion(cra19); CellRangeAddress
	 * cra20 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 20
	 * + serviceCount, 20 + serviceCount); xssfSheet.addMergedRegion(cra20);
	 * CellRangeAddress cra21 = new CellRangeAddress(rowCount, rowCount +
	 * packageListSize - 1, 21 + serviceCount, 21 + serviceCount);
	 * xssfSheet.addMergedRegion(cra21); CellRangeAddress cra22 = new
	 * CellRangeAddress(rowCount, rowCount + packageListSize - 1, 22 +
	 * serviceCount, 22 + serviceCount); xssfSheet.addMergedRegion(cra22);
	 * CellRangeAddress cra23 = new CellRangeAddress(rowCount, rowCount +
	 * packageListSize - 1, 23 + serviceCount, 23 + serviceCount);
	 * xssfSheet.addMergedRegion(cra23); CellRangeAddress cra24 = new
	 * CellRangeAddress(rowCount, rowCount + packageListSize - 1, 24 +
	 * serviceCount, 24 + serviceCount); xssfSheet.addMergedRegion(cra24);
	 * CellRangeAddress cra25 = new CellRangeAddress(rowCount, rowCount +
	 * packageListSize - 1, 25 + serviceCount, 25 + serviceCount);
	 * xssfSheet.addMergedRegion(cra25); CellRangeAddress cra26 = new
	 * CellRangeAddress(rowCount, rowCount + packageListSize - 1, 26 +
	 * serviceCount, 26 + serviceCount); xssfSheet.addMergedRegion(cra26);
	 * 
	 * 
	 * for (int p = 0; p < packageListSize; p++) { row =
	 * xssfSheet.createRow(rowCount++); pkgDetail = packageList.get(p);
	 * memberBillModel =
	 * memberBillService.queryMemberBillModelByPackageId(pkgDetail
	 * .getPackage_id()); if (p == 0) { addRow(row, frontUser,
	 * accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList,
	 * serviceCount, true, false); } else { addRow(row, frontUser,
	 * accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList,
	 * serviceCount, false, false); } } } else { row =
	 * xssfSheet.createRow(rowCount++); addRow(row, frontUser,
	 * accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList,
	 * serviceCount, true, false); } } OutputStream oStream =
	 * response.getOutputStream();
	 * 
	 * xssfWorkbook.write(oStream); oStream.flush(); oStream.close(); } } catch
	 * (IOException e) { e.printStackTrace();
	 * 
	 * } }
	 */
}
