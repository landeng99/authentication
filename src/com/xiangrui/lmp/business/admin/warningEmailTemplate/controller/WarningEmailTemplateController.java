package com.xiangrui.lmp.business.admin.warningEmailTemplate.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.sysSetting.service.SysSettingService;
import com.xiangrui.lmp.business.admin.sysSetting.vo.SysSetting;
import com.xiangrui.lmp.business.admin.warningEmailTemplate.service.WarningEmailTemplateService;
import com.xiangrui.lmp.business.admin.warningEmailTemplate.vo.WarningEmailTemplate;
import com.xiangrui.lmp.util.PageView;

@Controller
@RequestMapping("/admin/warningEmailTemplate")
public class WarningEmailTemplateController
{
	@Autowired
	private WarningEmailTemplateService warningEmailTemplateService;
	@Autowired
	private SysSettingService sysSettingService;

	/**
	 * 邮件模板列表初始化
	 * 
	 * @return
	 */
	@RequestMapping("/queryAllOne")
	public String queryAll(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);

		List<WarningEmailTemplate> warningEmailTemplateList = warningEmailTemplateService.queryWarningEmailTemplate(params);
		request.setAttribute("pageView", pageView);
		request.setAttribute("warningEmailTemplateList", warningEmailTemplateList);
		SysSetting emailSysSetting = sysSettingService.querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_EMAIL);
		if (emailSysSetting != null)
		{
			request.setAttribute("enableEmail", emailSysSetting.getSys_setting_value());
		}
		SysSetting eamilTimeRangeSysSetting = sysSettingService.querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_EMAIL_TIME_RANGE);
		if (eamilTimeRangeSysSetting != null)
		{
			request.setAttribute("emailTimeRange", eamilTimeRangeSysSetting.getSys_setting_value());
		}
		return "back/warningEmailTemplateList";
	}

	/**
	 * 编辑邮件模板初始化
	 * 
	 * @return
	 */
	@RequestMapping("/editInit")
	public String editInit(HttpServletRequest request, int seq_id)
	{
		WarningEmailTemplate warningEmailTemplate = warningEmailTemplateService.queryWarningEmailTemplateBySeqId(seq_id);
		if (warningEmailTemplate != null)
		{
			request.setAttribute("warningEmailTemplate", warningEmailTemplate);
		}
		request.setAttribute("seq_id", seq_id);
		return "back/warningEmailTemplateEdit";
	}

	/**
	 * 保存新建或编辑邮件模板
	 * 
	 * @return
	 */
	@RequestMapping("/saveOrupdate")
	public String saveOrupdate(HttpServletRequest request, WarningEmailTemplate warningEmailTemplate)
	{
		if (warningEmailTemplate.getSeq_id() == -1)
		{
			warningEmailTemplate.setCreate_time(new Date());
			warningEmailTemplateService.insertWarningEmailTemplate(warningEmailTemplate);
		}
		else
		{
			warningEmailTemplateService.updateWarningEmailTemplate(warningEmailTemplate);
		}
		return "back/warningEmailTemplateAdd_update_success";
	}

	/**
	 * 查看邮件模板
	 * 
	 * @return
	 */
	@RequestMapping("/show")
	public String show(HttpServletRequest request, int seq_id)
	{
		WarningEmailTemplate warningEmailTemplate = warningEmailTemplateService.queryWarningEmailTemplateBySeqId(seq_id);
		if (warningEmailTemplate != null)
		{
			request.setAttribute("warningEmailTemplate", warningEmailTemplate);
		}
		return "back/warningEmailTemplateShow";
	}

	/**
	 * 是否允许发送邮件设置
	 * 
	 * @param request
	 * @param enableEmail
	 * @return
	 */
	@RequestMapping("/enableEmailHandle")
	@ResponseBody
	public Map<String, Object> enableEmailHandle(HttpServletRequest request, String enableEmail)
	{
		SysSetting emailSysSetting = sysSettingService.querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_EMAIL);
		if (emailSysSetting != null)
		{
			emailSysSetting.setSys_setting_value(enableEmail);
			sysSettingService.updateSysSetting(emailSysSetting);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("flag", "yes");
		return result;
	}

	/**
	 * 删除模板
	 * @param request
	 * @param seq_id
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest request, int seq_id)
	{
		warningEmailTemplateService.deleteWarningEmailTemplate(seq_id);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("flag", "yes");
		return result;
	}
	
	/**
	 * 是否允许发送邮件时间段设置
	 * 
	 * @param request
	 * @param enableSms
	 * @return
	 */
	@RequestMapping("/setEmailTimeRange")
	@ResponseBody
	public Map<String, Object> setEmailTimeRange(HttpServletRequest request, String emailTimeRange)
	{
		SysSetting emailSysSetting = sysSettingService.querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_EMAIL_TIME_RANGE);
		if (emailSysSetting != null)
		{
			emailSysSetting.setSys_setting_value(emailTimeRange);
			sysSettingService.updateSysSetting(emailSysSetting);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("flag", "yes");
		return result;
	}
}
