package com.xiangrui.lmp.business.admin.warningEmailTemplate.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.warningEmailTemplate.mapper.WarningEmailTemplateMapper;
import com.xiangrui.lmp.business.admin.warningEmailTemplate.service.WarningEmailTemplateService;
import com.xiangrui.lmp.business.admin.warningEmailTemplate.vo.WarningEmailTemplate;

@Service("warningEmailTemplateService")
public class WarningEmailTemplateServiceImpl implements WarningEmailTemplateService
{
	@Autowired
	WarningEmailTemplateMapper warningEmailTemplateMapper;

	@Override
	public List<WarningEmailTemplate> queryWarningEmailTemplate(Map<String, Object> paremt)
	{
		return warningEmailTemplateMapper.queryWarningEmailTemplate(paremt);
	}

	@Override
	public WarningEmailTemplate queryWarningEmailTemplateBySeqId(int seq_id)
	{
		return warningEmailTemplateMapper.queryWarningEmailTemplateBySeqId(seq_id);
	}

	@Override
	public void updateWarningEmailTemplate(WarningEmailTemplate warningEmailTemplate)
	{
		warningEmailTemplateMapper.updateWarningEmailTemplate(warningEmailTemplate);
	}

	@Override
	public void insertWarningEmailTemplate(WarningEmailTemplate warningEmailTemplate)
	{
		warningEmailTemplateMapper.insertWarningEmailTemplate(warningEmailTemplate);

	}

	public WarningEmailTemplate queryEnableWarningEmailTemplate()
	{
		return warningEmailTemplateMapper.queryEnableWarningEmailTemplate();
	}

	public void deleteWarningEmailTemplate(int seq_id)
	{
		warningEmailTemplateMapper.deleteWarningEmailTemplate(seq_id);
	}
}
