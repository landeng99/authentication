package com.xiangrui.lmp.business.admin.warningEmailTemplate.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.warningEmailTemplate.vo.WarningEmailTemplate;

public interface WarningEmailTemplateMapper
{
	List<WarningEmailTemplate> queryWarningEmailTemplate(Map<String, Object> paremt);

	WarningEmailTemplate queryWarningEmailTemplateBySeqId(int seq_id);

	void updateWarningEmailTemplate(WarningEmailTemplate warningEmailTemplate);

	void insertWarningEmailTemplate(WarningEmailTemplate warningEmailTemplate);
	
	WarningEmailTemplate queryEnableWarningEmailTemplate();
	
	void deleteWarningEmailTemplate(int seq_id);
}
