package com.xiangrui.lmp.business.admin.backup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 系统备份
 * 
 * @author Administrator
 * 
 */
@Controller
@RequestMapping("/admin/backup")
public class BackUpController
{
    
    private Logger logger =Logger.getLogger(BackUpController.class);

    @RequestMapping("/exec")
    public String exec()
    {
        ProcessBuilder pb = new ProcessBuilder();
        try
        {
            // 调用 调用命令文件执行mysqldump备份数据库
            String command = "d:\\test.bat";
            // Process p = Runtime.getRuntime().exec(command);
            Process p = pb.command(command).start();
           
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    p.getInputStream(), "GBK"));// 注意中文编码问题
            
            String line;
            while ((line = br.readLine()) != null)
            {
                logger.debug("StartedLog==>" + line);
            }
            br.close();
        } catch (IOException e)
        {
            logger.error(e.toString());
            e.printStackTrace();
        }
        return "";
    }
 
}
