package com.xiangrui.lmp.business.admin.sysSetting.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.sysSetting.mapper.SysSettingMapper;
import com.xiangrui.lmp.business.admin.sysSetting.service.SysSettingService;
import com.xiangrui.lmp.business.admin.sysSetting.vo.SysSetting;

@Service("sysSettingService")
public class SysSettingServiceImpl implements SysSettingService
{
	static final Logger logger = Logger.getLogger(SysSettingServiceImpl.class);
	
	@Autowired
	SysSettingMapper sysSettingMapper;

	@Override
	public SysSetting querySystemSettingBySettingKey(String setting_key)
	{
		return sysSettingMapper.querySystemSettingBySettingKey(setting_key);
	}

	@Override
	public int updateSysSetting(SysSetting sysSetting)
	{
		return sysSettingMapper.updateSysSetting(sysSetting);
	}

	@Override
	public void insertSysSetting(SysSetting sysSetting)
	{
		sysSettingMapper.insertSysSetting(sysSetting);

	}

	public boolean isAllownWarningEmail()
	{
		boolean isAllow = false;
		SysSetting warningEmailSetting = querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_EMAIL);
		if (warningEmailSetting != null && "Y".equalsIgnoreCase(warningEmailSetting.getSys_setting_value()))
		{
			isAllow = true;
		}
		return isAllow;
	}

	public boolean isAllowWarningSms()
	{
		boolean isAllow = false;
		SysSetting warningSmsSetting = querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_SMS);
		if (warningSmsSetting != null && "Y".equalsIgnoreCase(warningSmsSetting.getSys_setting_value()))
		{
			isAllow = true;
		}
		return isAllow;
	}

	public boolean isInSMSAllowTimeRange()
	{
		boolean isInRange = false;
		SysSetting warningSmsTimeRangeSetting = querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_SMS_TIME_RANGE);
		if (warningSmsTimeRangeSetting != null)
		{
			String timeRangeStr = StringUtils.trimToNull(warningSmsTimeRangeSetting.getSys_setting_value());
			if (timeRangeStr != null && timeRangeStr.contains("~"))
			{
				String[] array = timeRangeStr.split("~");
				SimpleDateFormat format = new SimpleDateFormat("HH:mm");
				try
				{
					Date fromTime = format.parse(array[0]);
					Date toTime = format.parse(array[1]);
					Calendar now = Calendar.getInstance();
					Calendar tempFromCal = Calendar.getInstance();
					tempFromCal.setTime(fromTime);
					Calendar fromCal = Calendar.getInstance();
					fromCal.set(Calendar.HOUR_OF_DAY, tempFromCal.get(Calendar.HOUR_OF_DAY));
					fromCal.set(Calendar.MINUTE, tempFromCal.get(Calendar.MINUTE));
					Calendar tempToCal = Calendar.getInstance();
					tempToCal.setTime(toTime);
					Calendar toCal = Calendar.getInstance();
					toCal.set(Calendar.HOUR_OF_DAY, tempToCal.get(Calendar.HOUR_OF_DAY));
					toCal.set(Calendar.MINUTE, tempToCal.get(Calendar.MINUTE));
					if (now.after(fromCal) && now.before(toCal))
					{
						isInRange = true;
					}
				} catch (ParseException e)
				{
					e.printStackTrace();
				}
			}
		}
		return isInRange;
	}

	public String getUSDtoRMBrate()
	{
		String rate = null;
		SysSetting warningSmsSetting = querySystemSettingBySettingKey(SysSetting.USD_CHANGE_TO_RMB_RATE);
		if (warningSmsSetting != null)
		{
			rate = warningSmsSetting.getSys_setting_value();
		}
		return rate;
	}
	
	public boolean isInEmailAllowTimeRange()
	{
		boolean isInRange = false;
		SysSetting warningEmailTimeRangeSetting = querySystemSettingBySettingKey(SysSetting.ENABLE_WARNING_EMAIL_TIME_RANGE);
		if (warningEmailTimeRangeSetting != null)
		{
			String timeRangeStr = StringUtils.trimToNull(warningEmailTimeRangeSetting.getSys_setting_value());
			if (timeRangeStr != null && timeRangeStr.contains("~"))
			{
				String[] array = timeRangeStr.split("~");
				SimpleDateFormat format = new SimpleDateFormat("HH:mm");
				try
				{
					Date fromTime = format.parse(array[0]);
					Date toTime = format.parse(array[1]);
					Calendar now = Calendar.getInstance();
					Calendar tempFromCal = Calendar.getInstance();
					tempFromCal.setTime(fromTime);
					Calendar fromCal = Calendar.getInstance();
					fromCal.set(Calendar.HOUR_OF_DAY, tempFromCal.get(Calendar.HOUR_OF_DAY));
					fromCal.set(Calendar.MINUTE, tempFromCal.get(Calendar.MINUTE));
					Calendar tempToCal = Calendar.getInstance();
					tempToCal.setTime(toTime);
					Calendar toCal = Calendar.getInstance();
					toCal.set(Calendar.HOUR_OF_DAY, tempToCal.get(Calendar.HOUR_OF_DAY));
					toCal.set(Calendar.MINUTE, tempToCal.get(Calendar.MINUTE));
					if (now.after(fromCal) && now.before(toCal))
					{
						isInRange = true;
					}
				} catch (ParseException e)
				{
					e.printStackTrace();
				}
			}
		}
		return isInRange;
	}

	@Override
	public float yuan2Dollar(float yuan) {
		BigDecimal amountBigDecimal = new BigDecimal(yuan).setScale(2, BigDecimal.ROUND_HALF_UP);
		// 人民币转换美元
		String rate = this.getUSDtoRMBrate();
		if (rate != null)
		{
			try{
			amountBigDecimal = amountBigDecimal.divide(
					new BigDecimal(rate),BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP);
			}catch(Exception e){
				logger.error("yuan2Dollar err, yuan: " + yuan + ", " + e.toString());
				throw new RuntimeException(e);
			}
		}
		
		return amountBigDecimal.floatValue();
	}

	@Override
	public float dollar2Yuan(float dollar) {
		BigDecimal amountBigDecimal = new BigDecimal(dollar).setScale(2, BigDecimal.ROUND_HALF_UP);
		// 美元转换成人民币
		String rate = this.getUSDtoRMBrate();
		if (rate != null)
		{
			amountBigDecimal = amountBigDecimal.multiply(new BigDecimal(rate)).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		
		return amountBigDecimal.floatValue();
	}
}
