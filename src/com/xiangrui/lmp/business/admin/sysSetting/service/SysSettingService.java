package com.xiangrui.lmp.business.admin.sysSetting.service;

import com.xiangrui.lmp.business.admin.sysSetting.vo.SysSetting;

public interface SysSettingService {
	SysSetting querySystemSettingBySettingKey(String setting_key);
	int updateSysSetting(SysSetting sysSetting);
	void insertSysSetting(SysSetting sysSetting);
	boolean isAllownWarningEmail();
	boolean isAllowWarningSms();
	boolean isInSMSAllowTimeRange();
	String getUSDtoRMBrate();
	boolean isInEmailAllowTimeRange();
	float yuan2Dollar(float yuan);	// 人民币转美元
	float dollar2Yuan(float yuan);	// 美元转人民币
}
