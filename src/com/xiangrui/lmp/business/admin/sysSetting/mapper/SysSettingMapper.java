package com.xiangrui.lmp.business.admin.sysSetting.mapper;

import com.xiangrui.lmp.business.admin.sysSetting.vo.SysSetting;

public interface SysSettingMapper
{
	SysSetting querySystemSettingBySettingKey(String setting_key);
	int updateSysSetting(SysSetting sysSetting);
	void insertSysSetting(SysSetting sysSetting);
}
