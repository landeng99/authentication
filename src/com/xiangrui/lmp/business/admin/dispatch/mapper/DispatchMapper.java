package com.xiangrui.lmp.business.admin.dispatch.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.dispatch.vo.Dispatch;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;

public interface DispatchMapper
{

	/**
	 * 新增口岸列表
	 * @param dispatch
	 * @return
	 */
	void addSeaportList(Dispatch dispatch);
	
	
	/**
	 * 校验数据库口岸列表
	 * @param name
	 * @return
	 */
	String validateName(String name);

	
	/**
	 * 初始化查询列表
	 * @param params
	 * @return
	 */
	List<Dispatch> queryAll(Map<String, Object> params);

	
	/**
	 * 根据ID删除列表
	 * @param params
	 */
	void deleteById(Map<String, Object> params);

	
	/**
	 * 根据条件检索
	 * @param params
	 * @return
	 */
	List<Dispatch> query(Map<String, Object> params);


	List<Dispatch> queryById(int id);


	/**
	 * 根据公司单号查询包裹信息
	 * @param logisticsCode
	 * @return
	 */
	List<PkgOperate> queryByLogisticsCode(Map<String, Object> params);

	/**
	 * 根据口岸列表ID更新包裹Slid、Seaport_id
	 * @param params
	 */
	void updateById(Map<String, Object> params);


	/**
	 * 更新Slid、SeaportId关联口岸、口岸列表
	 * @param params
	 */
	void updateSlidAndSeaportId(Map<String, Object> params);

	/**
	 * 待入库包裹信息
	 * @param params
	 * @return
	 */
	List<PkgOperate> queryBeInStatus(Map<String, Object> params);

	/**
	 * 已入库状态包裹
	 * @param params
	 * @return
	 */
	List<PkgOperate> queryAlreadyInStatus(Map<String, Object> params);

	/**
	 * 待发货状态包裹
	 * @param params
	 * @return
	 */
	List<PkgOperate> queryToBeStatus(Map<String, Object> params);


	/**
	 * 根据条件查询口岸列表
	 * @param param
	 * @return
	 */
	Dispatch queryDispatchByCondition(Map<String, Object> param);


	/**
	 * 根据条件查询包裹
	 * @param param
	 * @return
	 */
	List<PkgOperate> queryPkgOperateByCondition(Map<String, Object> param);

	/**
	 * 根据条件删除列表
	 * @param param
	 */
	void ByCondition(Map<String, Object> param);

	/**
	 * 根据Slid查询公司单号
	 * @param params
	 * @return
	 */
	List<PkgOperate> queryByCondition(Map<String, Object> params);


	/**
	 * 根据Slid查询口岸列表的包裹数
	 * @param param
	 * @return
	 */
	int queryCountByCondition(Map<String, Object> param);

}
