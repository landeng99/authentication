package com.xiangrui.lmp.business.admin.dispatch.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.dispatch.service.DispatchService;
import com.xiangrui.lmp.business.admin.dispatch.vo.Dispatch;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.newExcel.ExcelReadUtil;
import com.xiangrui.lmp.util.newExcel.SheetConfig;


@Controller
@RequestMapping("/admin/dispatch")
public class DispatchController
{
	
	@Autowired
	private SeaportService seaportService; 
	
	@Autowired
	private DispatchService dispatchService;
	
	//检索条件
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";
	
	
	/**
	 * 初始化列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/init")
	public String init(HttpServletRequest request, HttpServletResponse response)
	{
		//分页
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtils.isNotEmpty(pageIndex))
		{
			int pageCount = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCount, 20, 0);
			
		} else
		{
			pageView = new PageView(1, 20, 0);
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);
		
		//初始化口岸
		List<Seaport> seaportList = this.seaportService.queryAll();
		
		//初始化列表
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			List<Dispatch> list = this.dispatchService.queryAll(params);
			request.setAttribute("list", list);
		}
		request.setAttribute("seaportList", seaportList);
		request.setAttribute("pageView", pageView);
		request.setAttribute("params", params);
		return "back/dispatch";
	}
	
	/**
	 * 口岸列表详情
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/dispatchDetail")
	public String dispatchDetail(HttpServletRequest request, HttpServletResponse response)
	{
		//分页
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtils.isNotEmpty(pageIndex))
		{
			int pageCount = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCount, 100, 0);
			
		} else
		{
			pageView = new PageView(1, 100, 0);
		}
		
		//获取和转换页面参数
		String parameter = request.getParameter("id");
		int id = Integer.parseInt(parameter);
		int slid = id;
		String paramet = request.getParameter("sid");
		int sid = Integer.parseInt(paramet);
		
		//封装参数
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", id);
		param.put("slid", slid);
		param.put("pageView", pageView);
		
		//通过参数查询口岸列表
		Dispatch dispatch = this.dispatchService.queryDispatchByCondition(param);
		//通过参数查询包裹详情
		List<PkgOperate> list = this.dispatchService.queryPkgOperateByCondition(param);
		
		request.setAttribute("dispatch", dispatch);
		request.setAttribute("list", list);
		request.setAttribute("id", id);
		request.setAttribute("sid", sid);
		request.setAttribute("pageView", pageView);
		request.setAttribute("param", param);
		return "back/dispatchDetail";
	}
	
	/**
	 * 新增口岸列表
	 * @param request
	 * @param response
	 * @param dispatch
	 * @return
	 */
	@RequestMapping("/appendList")
	public String appendList(HttpServletRequest request, HttpServletResponse response, Dispatch dispatch)
	{
		//口岸
		int sid = Integer.parseInt(request.getParameter("sid"));
		//口岸列表名字
		String name = request.getParameter("name");
		//口岸描述
		String description = request.getParameter("description");
		//校验数据库口岸列表
		String validateName = this.dispatchService.validateName(name);
		if ((StringUtils.isEmpty(validateName)))
		{
			//新增口岸列表
			if ((StringUtils.isNotEmpty(name)) && (StringUtils.isNotEmpty(description)))
			{
				//口岸列表
				dispatch.setName(name);
				//口岸
				dispatch.setSid(sid);
				//备注
				dispatch.setDescription(description);
				//创建时间
				dispatch.setCreateTime(new Timestamp(System.currentTimeMillis()));
				//删除状态
				dispatch.setIsDelete(0);
				this.dispatchService.addSeaportList(dispatch);
			}
		}
		return "back/dispatch";	
	}
	
	/**
	 * 删除列表数据
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteLine")
	public String deleteLine(HttpServletRequest request, HttpServletResponse response)
	{
		//获取并封装参数
		String parameter = request.getParameter("id");
		Integer id = Integer.valueOf(parameter);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		
		//更新包裹
		this.dispatchService.updateById(params);
		//删除列表
		this.dispatchService.deleteById(params);
		return "back/dispatch";
	}
	
	/**
	 * 单号查询
	 * @param request
	 * @param response
	 * @param logistics_code
	 * @param original_num
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/search")
	public String search(HttpServletRequest request, HttpServletResponse response, String logistics_code, String original_num)
	{
		//分页
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		Map<String, Object> params = new HashMap<String, Object>();
		if (StringUtils.isNotEmpty(pageIndex))
		{
			int pageCount = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCount,10,0);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		} else
		{
			pageView = new PageView(1,10,0);
			//公司单号
			params.put("logistics_code", logistics_code);
			//关联单号
			params.put("original_num", original_num);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}
		
		//根据条件查询
		params.put("pageView", pageView);
		List<Dispatch> list = this.dispatchService.query(params);
		request.setAttribute("list", list);
		request.setAttribute("pageView", pageView);
		request.setAttribute("params", params);
		return "back/dispatch";
	}
	
	
	/**
	 * 根据状态导出Excel
	 * @param request
	 * @param response
	 */
	@RequestMapping("/exportPackage")
	@ResponseBody
	public void exportPackage(HttpServletRequest request, HttpServletResponse response)
	{
		
		try
		{
			//获取并转换传递的状态值
			String[] parameterValues = request.getParameterValues("status[]"); 
			if((parameterValues != null) && (parameterValues.length > 0))
			{
				String status = new String();
				for (int i = 0; i < parameterValues.length; i++)
				{
					status = parameterValues[i];
				}
				
				//配置Excel文件格式
				String fileName = "";
				response.reset();
				response.setContentType("multipart/form-data");
				request.setCharacterEncoding("UTF-8");
				
				//封装参数
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("status", status);
				List<PkgOperate> packageList = new ArrayList<>();
				
				//查询选中条件的包裹
				switch (status) 
				{
					case "0":
						fileName = "待入库包裹";
						packageList = this.dispatchService.queryBeInStatus(params);
						break;
					case "1":
						fileName = "已入库包裹";
						packageList = this.dispatchService.queryAlreadyInStatus(params);
						break;
					default:
						fileName = "待发货包裹";
						packageList = this.dispatchService.queryToBeStatus(params);
						break;
				}
				response.setHeader("Content-Disposition","attachment;filename = " + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
				
				//根据状态导出Excel文件
				XSSFWorkbook workbook = this.dispatchService.exportPackageByStatus(fileName, packageList);
				OutputStream stream = response.getOutputStream();
				workbook.write(stream);
				stream.flush();
				stream.close();
			}
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Excel导出列表包裹
	 * @param request
	 * @param response
	 */
	@RequestMapping("/exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response) 
	{
		try
		{
			//获取参数ID和Slid
			String parameter = request.getParameter("id");
			int id = Integer.parseInt(parameter);
			int slid = id;
			 
			//封装参数
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("id", id);
			param.put("slid", slid);
			
			//根据Slid查询口岸列表的包裹数
			int count = this.dispatchService.queryCountByCondition(param);
			if (count != 0)
			{
				//根据条件查询口岸列表对应包裹
				List<PkgOperate> list = this.dispatchService.queryPkgOperateByCondition(param);
				
				//设置Excel文件格式
				String fileName = "导出口岸列表包裹";
				response.reset();
				response.setContentType("multipart/form-data");
				request.setCharacterEncoding("UTF-8");
				response.setHeader("Content-Disposition", "attachment;filename = " + new String(fileName.getBytes("UTF-8"), "ISO-8859-1") + ".xlsx");
				
				//导出Excel文件
				XSSFWorkbook xssfWorkbook = this.dispatchService.exportExcel(fileName, list);
				OutputStream stream = response.getOutputStream();
				xssfWorkbook.write(stream);
				stream.flush();
				stream.close();
			}
		}  
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据公司单号导入Excel
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/importPackage")
	@ResponseBody
	public Map<String, Object> importPackage(HttpServletRequest request, HttpServletResponse response)
	{
		//获取ID和Seaport_id参数
		String string = request.getParameter("id");
		int slid = Integer.parseInt(string);
		String stri = request.getParameter("sid");
		int seaport_id = Integer.parseInt(stri);
		
		//获取导入的Excel文件
		MultipartHttpServletRequest re = (MultipartHttpServletRequest) request;
		//文件
		MultipartFile excelFile = re.getFile("importFile");
		//文件名
		String fileName = excelFile.getOriginalFilename();
		//文件后缀
		String suffix = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
		
		//Excel文件导入
		Map<String , Object> result = new HashMap<>();
		String flag = "F";
		String message = "导入失败!";
		try
		{
			Workbook workbook = null;
			if (suffix.equals("xls"))
			{
				workbook = new HSSFWorkbook(excelFile.getInputStream());
			}else
			{
				workbook = new XSSFWorkbook(excelFile.getInputStream());
			}
			//解析Excel文件
			Map<String, String> col = new HashMap<String, String>();
			col.put("logistics_code", "公司单号");
			List<Map<String,Object>> excel = ExcelReadUtil.parseExcel(workbook, col, new SheetConfig());
			if (excel != null && excel.size() > 0)
			{
				String logistics_code = "";
				for (Map<String, Object> map : excel)
				{
					//公司单号
					logistics_code += map.get("logistics_code");
					logistics_code += ",";
				}
				
				//封装参数
				logistics_code = logistics_code.substring(0, logistics_code.length()-1);
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("logistics_code", logistics_code);
				params.put("seaport_id", seaport_id);
				params.put("slid", slid);
				
				//根据Slid查询公司单号
				List<PkgOperate> flagList = this.dispatchService.queryByCondition(params);
				
				if ((flagList == null) || (flagList.size() == 0)) {
					
					//根据公司单号查询包裹
					List<PkgOperate> list = this.dispatchService.queryByLogisticsCode(params);
					//更新Slid、SeaportId关联口岸、口岸列表
					this.dispatchService.updateSlidAndSeaportId(params);
					flag="S";
					message="提示!导入成功...";
					request.setAttribute("list", list);
				} 
				//导出重复单号
				else if((flagList != null) || (flagList.size() != 0))
				{
					//配置Excel文件格式
			        fileName = "已经存在其它口岸列表重复单号";
					response.reset();
					response.setContentType("multipart/form-data");
					request.setCharacterEncoding("UTF-8");
					response.setHeader("Content-Disposition","attachment;filename = " + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
					
					//Excel导出公司单号
					flag="F";
					message="导入失败,上传报关号码在系统中已经存在，请重新确认后再上传";
					XSSFWorkbook errorFile = this.dispatchService.exportError(fileName, flagList);
					OutputStream stream = response.getOutputStream();
					errorFile.write(stream);
					stream.flush();
					stream.close();
				}
			} 
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		result.put("flag", flag);
		result.put("message", message);
		return result;
	}	
	
	/**
	 * 根据条件删除列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/delPackage")
	public String delPackage(HttpServletRequest request, HttpServletResponse response)
	{
		//获取参数
		String packageId = request.getParameter("package_id");
		int package_id = Integer.parseInt(packageId);
		
		Map<String, Object> param = new HashMap<>();
		param.put("package_id", package_id);
		
		//根据参数删除列表
		this.dispatchService.delByCondition(param);
		return "back/dispatchDetail";
	}
	
}





