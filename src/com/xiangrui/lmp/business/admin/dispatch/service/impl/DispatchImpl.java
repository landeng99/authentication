package com.xiangrui.lmp.business.admin.dispatch.service.impl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.dispatch.mapper.DispatchMapper;
import com.xiangrui.lmp.business.admin.dispatch.service.DispatchService;
import com.xiangrui.lmp.business.admin.dispatch.vo.Dispatch;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.NumberUtils;
@Service
public class DispatchImpl implements DispatchService
{
	@Autowired
	private DispatchMapper dispatchMapper;
	
	
	/**
	 * 新增口岸列表
	 */
	@Override
	public void addSeaportList(Dispatch dispatch)
	{
		this.dispatchMapper.addSeaportList(dispatch);
	}

	/**
	 * 校验数据库口岸列表
	 */
	@Override
	public String validateName(String name)
	{
		return this.dispatchMapper.validateName(name);
	}

	/**
	 * 初始化列表查询
	 */
	@Override
	public List<Dispatch> queryAll(Map<String, Object> params)
	{
		return this.dispatchMapper.queryAll(params);
	}

	/**
	 * 根据ID删除列表
	 */
	@Override
	public void deleteById(Map<String, Object> params)
	{
		//删除口岸列表
		this.dispatchMapper.deleteById(params);
	}
	
	/**
	 * 根据条件查询
	 */
	@Override
	public List<Dispatch> query(Map<String, Object> params)
	{
		return this.dispatchMapper.query(params);
	}

	@Override
	public List<Dispatch> queryById(int id)
	{
		
		return this.dispatchMapper.queryById(id);
	}

	
	/**
	 * 根据状态导出Excel
	 * @author Windows10
	 */
	@Override
	public XSSFWorkbook exportPackageByStatus(String fileName, List<PkgOperate> packageList) 
	{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		//新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
		//固定第一列
		xssfSheet.createFreezePane(0, 1, 0, 1);

		//蓝颜色
		XSSFColor blueColor = new XSSFColor(new Color(185, 211, 238));
		//白颜色
		XSSFColor whiteColor = new XSSFColor(Color.WHITE);

		//白色居中样式
		XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
		style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setFillForegroundColor(whiteColor);
		style1.setWrapText(true);

		//蓝色居中样式
		XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style2.setFillForegroundColor(blueColor);
		style2.setWrapText(true);

		//设置列宽
		xssfSheet.setColumnWidth(0, 4000);
		xssfSheet.setColumnWidth(1, 6000);
		xssfSheet.setColumnWidth(2, 10000);
		xssfSheet.setColumnWidth(3, 3000);
		xssfSheet.setColumnWidth(4, 3000);
		xssfSheet.setColumnWidth(5, 3000);
		xssfSheet.setColumnWidth(6, 3000);
		xssfSheet.setColumnWidth(7, 5000);
		xssfSheet.setColumnWidth(8, 3000);
		xssfSheet.setColumnWidth(9, 2000);
		xssfSheet.setColumnWidth(10, 3000);
		xssfSheet.setColumnWidth(11, 3000);

		//标题列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		List<String> columnsList = new ArrayList<String>();
		columnsList.add("公司单号");
		columnsList.add("关联单号");
		columnsList.add("商品明细");
		columnsList.add("实际重量");
		columnsList.add("入库日期");
		columnsList.add("收件人");
		columnsList.add("LastName");
		columnsList.add("身份证");
		columnsList.add("身份证图片");
		columnsList.add("渠道");
		columnsList.add("包裹状态");
		columnsList.add("海外仓库");

		for (int i = 0; i < columnsList.size(); i++)
		{
			XSSFCell cell = firstXSSFRow.createCell(i);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(columnsList.get(i));
			cell.setCellStyle(style1);
		}

		PkgOperate pkgOperate = null;
		int rowCount = 1;
		for (int k = 0; k < packageList.size(); k++)
		{
			pkgOperate = packageList.get(k);
			XSSFRow row = xssfSheet.createRow(rowCount++);
			//公司单号
			XSSFCell cell0 = row.createCell(0);
			cell0.setCellValue(StringUtils.trimToEmpty(pkgOperate.getLogistics_code()));
			cell0.setCellStyle(k % 2 == 0 ? style2 : style1);

			//关联单号
			XSSFCell cell1 = row.createCell(1);
			cell1.setCellValue(StringUtils.trimToEmpty(pkgOperate.getOriginal_num()));
			cell1.setCellStyle(k % 2 == 0 ? style2 : style1);
			
			//商品明细
			XSSFCell cell2 = row.createCell(2);
			cell2.setCellValue(StringUtils.trimToEmpty(pkgOperate.getGoods_name()));
			cell2.setCellStyle(k % 2 == 0 ? style2 : style1);

			//实际重量
			XSSFCell cell3 = row.createCell(3);
			cell3.setCellValue(NumberUtils.scaleMoneyData3(pkgOperate.getActual_weight()));
			cell3.setCellStyle(k % 2 == 0 ? style2 : style1);
						
			//入库日期
			XSSFCell cell4 = row.createCell(4);
			cell4.setCellValue(DateUtil.getDateFormatSting(pkgOperate.getInput_time(), "yyyy-MM-dd"));
			cell4.setCellStyle(k % 2 == 0 ? style2 : style1);

			//收件人
			XSSFCell cell5 = row.createCell(5);
			cell5.setCellValue(StringUtils.trimToEmpty(pkgOperate.getReceiver()));
			cell5.setCellStyle(k % 2 == 0 ? style2 : style1);
						
			//LastName
			XSSFCell cell6 = row.createCell(6);
			cell6.setCellValue(StringUtils.trimToEmpty(pkgOperate.getLast_name()));
			cell6.setCellStyle(k % 2 == 0 ? style2 : style1);

			//身份证
			XSSFCell cell7 = row.createCell(7);
			cell7.setCellValue(StringUtils.trimToEmpty(pkgOperate.getIdcard()));
			cell7.setCellStyle(k % 2 == 0 ? style2 : style1);
			
			//身份证图片
			XSSFCell cell8 = row.createCell(8);
			cell8.setCellValue(StringUtils.trimToEmpty(pkgOperate.getIdcardImg()).equals("") ? "无" : "");
			cell8.setCellStyle(k % 2 == 0 ? style2 : style1);

			
			//渠道
			XSSFCell cell9 = row.createCell(9);
			//cell9.setCellValue(StringUtils.trimToEmpty(String.valueOf(pkgOperate.getExpress_package())));
			switch (pkgOperate.getExpress_package()) {
			case 1:
				cell9.setCellValue("A");
				break;
			case 2:
				cell9.setCellValue("B");
				break;	
			case 3:
				cell9.setCellValue("C");
				break;
			case 4:
				cell9.setCellValue("D");
				break;
			case 5:
				cell9.setCellValue("E");
				break;
			default:
				cell9.setCellValue("默认");
				break;
			}
			cell9.setCellStyle(k % 2 == 0 ? style2 : style1);

			//包裹状态
			XSSFCell cell10 = row.createCell(10);
			switch (pkgOperate.getStatus()) {
			case 0:
				cell10.setCellValue("待入库");
				break;
			case 1:
				cell10.setCellValue("已入库");
				break;	
			case 2:
				cell10.setCellValue("待发货");
				break;
			}
			cell10.setCellStyle(k % 2 == 0 ? style2 : style1);
			
			//海外仓库
			XSSFCell cell11 = row.createCell(11);
			switch (pkgOperate.getOsaddr_id())
			{
				case 17 :
					cell11.setCellValue("纽约仓库");
					break;
				case 27 :
					cell11.setCellValue("香港仓库");
					break;
				case 29 :
					cell11.setCellValue("加拿大仓库");
					break;
				case 31 :
					cell11.setCellValue("美国田那西仓库");
					break;
				default :
					cell11.setCellValue("韩国首尔仓库");
					break;
			}
			cell11.setCellStyle(k % 2 == 0 ? style2 : style1);
		}
		return xssfWorkbook;
	}

	/**
	 * 根据公司单号查询包裹信息
	 * @author Windows10
	 */
	@Override
	public List<PkgOperate> queryByLogisticsCode(Map<String, Object> params) {
		return this.dispatchMapper.queryByLogisticsCode(params);
	}
	

	/**
	 * 根据口岸列表ID更新包裹Slid、Seaport_id
	 */
	@Override
	public void updateById(Map<String, Object> params) {
		this.dispatchMapper.updateById(params);
	}

	/**
	 * 更新Slid、SeaportId关联口岸、口岸列表
	 */
	@Override
	public void updateSlidAndSeaportId(Map<String, Object> params) {
		this.dispatchMapper.updateSlidAndSeaportId(params);
	}

	/**
	 * 待入库状态包裹
	 * @param params
	 * @return
	 */
	@Override
	public List<PkgOperate> queryBeInStatus(Map<String, Object> params) {
		return this.dispatchMapper.queryBeInStatus(params);
	}
	
	/**
	 * 已入库状态包裹
	 * @param params
	 * @return
	 */
	@Override
	public List<PkgOperate> queryAlreadyInStatus(Map<String, Object> params) {
		return this.dispatchMapper.queryAlreadyInStatus(params);
	}

	
	/**
	 * 待发货状态包裹
	 * @param params
	 * @return
	 */
	@Override
	public List<PkgOperate> queryToBeStatus(Map<String, Object> params) {
		return this.dispatchMapper.queryToBeStatus(params);
	}


	/**
	 * 根据条件查询口岸列表
	 */
	@Override
	public Dispatch queryDispatchByCondition(Map<String, Object> param) {
		Dispatch dispatch = this.dispatchMapper.queryDispatchByCondition(param);
		return dispatch;
	}

	
	/**
	 * 根据条件查询包裹
	 */
	@Override
	public List<PkgOperate> queryPkgOperateByCondition(Map<String, Object> param) {
		return this.dispatchMapper.queryPkgOperateByCondition(param);
	}

	
	/**
	 * 根据条件删除列表
	 */
	@Override
	public void delByCondition(Map<String, Object> param) {
		this.dispatchMapper.ByCondition(param);
	}

	/**
	 * Excel导出公司单号
	 */
	@Override
	public XSSFWorkbook exportError(String fileName, List<PkgOperate> flagList)
	{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		//新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
		//固定第一列
		xssfSheet.createFreezePane(0, 1, 0, 1);

		//蓝颜色
		XSSFColor blueColor = new XSSFColor(new Color(185, 211, 238));
		//白颜色
		XSSFColor whiteColor = new XSSFColor(Color.WHITE);

		//白色居中样式
		XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
		style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setFillForegroundColor(whiteColor);
		style1.setWrapText(true);

		//蓝色居中样式
		XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style2.setFillForegroundColor(blueColor);
		style2.setWrapText(true);

		//设置列宽
		xssfSheet.setColumnWidth(0, 4000);

		//标题列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		List<String> columnsList = new ArrayList<String>();
		columnsList.add("已存在的公司单号");

		for (int i = 0; i < columnsList.size(); i++)
		{
			XSSFCell cell = firstXSSFRow.createCell(i);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(columnsList.get(i));
			cell.setCellStyle(style1);
		}

		PkgOperate pkgOperate = null;
		int rowCount = 1;
		for (int k = 0; k < flagList.size(); k++)
		{
			pkgOperate = flagList.get(k);
			XSSFRow row = xssfSheet.createRow(rowCount++);
			
			//公司单号
			XSSFCell cell0 = row.createCell(0);
			cell0.setCellValue(StringUtils.trimToEmpty(pkgOperate.getLogistics_code()));
			cell0.setCellStyle(k % 2 == 0 ? style2 : style1);

		}
		return xssfWorkbook;
		
	}

	
	/**
	 * 根据Slid查询公司单号
	 */
	@Override
	public List<PkgOperate> queryByCondition(Map<String, Object> params) {
		return this.dispatchMapper.queryByCondition(params);
	}

	@Override
	public XSSFWorkbook exportExcel(String fileName, List<PkgOperate> list) 
	{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		//新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
		//固定第一列
		xssfSheet.createFreezePane(0, 1, 0, 1);

		//蓝颜色
		XSSFColor blueColor = new XSSFColor(new Color(185, 211, 238));
		//白颜色
		XSSFColor whiteColor = new XSSFColor(Color.WHITE);

		//白色居中样式
		XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
		style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setFillForegroundColor(whiteColor);
		style1.setWrapText(true);

		//蓝色居中样式
		XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style2.setFillForegroundColor(blueColor);
		style2.setWrapText(true);

		//设置列宽
		xssfSheet.setColumnWidth(0, 4000);
		xssfSheet.setColumnWidth(1, 6000);
		xssfSheet.setColumnWidth(2, 10000);
		xssfSheet.setColumnWidth(3, 3000);
		xssfSheet.setColumnWidth(4, 3000);
		xssfSheet.setColumnWidth(5, 3000);
		xssfSheet.setColumnWidth(6, 3000);
		xssfSheet.setColumnWidth(7, 5000);
		xssfSheet.setColumnWidth(8, 6000);
		xssfSheet.setColumnWidth(9, 2000);
		xssfSheet.setColumnWidth(10, 3000);

		//标题列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		List<String> columnsList = new ArrayList<String>();
		columnsList.add("公司单号");
		columnsList.add("关联单号");
		columnsList.add("商品明细");
		columnsList.add("实际重量");
		columnsList.add("入库日期");
		columnsList.add("收件人");
		columnsList.add("LastName");
		columnsList.add("身份证");
		columnsList.add("身份证图片");
		columnsList.add("渠道");
		columnsList.add("包裹状态");

		for (int i = 0; i < columnsList.size(); i++)
		{
			XSSFCell cell = firstXSSFRow.createCell(i);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(columnsList.get(i));
			cell.setCellStyle(style1);
		}

		PkgOperate pkgOperate = null;
		int rowCount = 1;
		for (int k = 0; k < list.size(); k++)
		{
			pkgOperate = list.get(k);
			XSSFRow row = xssfSheet.createRow(rowCount++);
			//公司单号
			XSSFCell cell0 = row.createCell(0);
			cell0.setCellValue(StringUtils.trimToEmpty(pkgOperate.getLogistics_code()));
			cell0.setCellStyle(k % 2 == 0 ? style2 : style1);

			//关联单号
			XSSFCell cell1 = row.createCell(1);
			cell1.setCellValue(StringUtils.trimToEmpty(pkgOperate.getOriginal_num()));
			cell1.setCellStyle(k % 2 == 0 ? style2 : style1);
			
			//商品明细
			XSSFCell cell2 = row.createCell(2);
			cell2.setCellValue(StringUtils.trimToEmpty(pkgOperate.getGoods_name()));
			cell2.setCellStyle(k % 2 == 0 ? style2 : style1);

			//实际重量
			XSSFCell cell3 = row.createCell(3);
			cell3.setCellValue(NumberUtils.scaleMoneyData3(pkgOperate.getActual_weight()));
			cell3.setCellStyle(k % 2 == 0 ? style2 : style1);
						
			//入库日期
			XSSFCell cell4 = row.createCell(4);
			cell4.setCellValue(DateUtil.getDateFormatSting(pkgOperate.getInput_time(), "yyyy-MM-dd"));
			cell4.setCellStyle(k % 2 == 0 ? style2 : style1);

			//收件人
			XSSFCell cell5 = row.createCell(5);
			cell5.setCellValue(StringUtils.trimToEmpty(pkgOperate.getReceiver()));
			cell5.setCellStyle(k % 2 == 0 ? style2 : style1);
						
			//LastName
			XSSFCell cell6 = row.createCell(6);
			cell6.setCellValue(StringUtils.trimToEmpty(pkgOperate.getLast_name()));
			cell6.setCellStyle(k % 2 == 0 ? style2 : style1);

			//身份证
			XSSFCell cell7 = row.createCell(7);
			cell7.setCellValue(StringUtils.trimToEmpty(pkgOperate.getIdcard()));
			cell7.setCellStyle(k % 2 == 0 ? style2 : style1);
			
			//身份证图片
			XSSFCell cell8 = row.createCell(8);
			cell8.setCellValue(StringUtils.trimToEmpty(pkgOperate.getIdcardImg()).equals("") ? "无" : "");
			cell8.setCellStyle(k % 2 == 0 ? style2 : style1);

			
			//渠道
			XSSFCell cell9 = row.createCell(9);
			//cell9.setCellValue(StringUtils.trimToEmpty(String.valueOf(pkgOperate.getExpress_package())));
			switch (pkgOperate.getExpress_package()) {
			case 1:
				cell9.setCellValue("A");
				break;
			case 2:
				cell9.setCellValue("B");
				break;	
			case 3:
				cell9.setCellValue("C");
				break;
			case 4:
				cell9.setCellValue("D");
				break;
			case 5:
				cell9.setCellValue("E");
				break;
			default:
				cell9.setCellValue("默认");
				break;
			}
			cell9.setCellStyle(k % 2 == 0 ? style2 : style1);
			
			//包裹状态
			XSSFCell cell10 = row.createCell(10);
			switch (pkgOperate.getStatus()) {
			case -1:
				cell10.setCellValue("异常");
				break;
			case 0:
				cell10.setCellValue("待入库");
				break;
			case 1:
				cell10.setCellValue("已入库");
				break;	
			case 2:
				cell10.setCellValue("待发货");
				break;
			case 3:
				cell10.setCellValue("已出库");
				break;
			case 4:
				cell10.setCellValue("空运中");
				break;
			case 5:
				cell10.setCellValue("待清关");
				break;
			case 7:
				cell10.setCellValue("已清关派件中");
				break;
			case 9:
				cell10.setCellValue("已收货");
				break;
			case 20:
				cell10.setCellValue("废弃");
				break;
			case 21:
				cell10.setCellValue("退货");
				break;
			}
			//cell10.setCellValue(StringUtils.trimToEmpty(String.valueOf(pkgOperate.getStatus())));
			cell10.setCellStyle(k % 2 == 0 ? style2 : style1);
		}
		return xssfWorkbook;
	}

	/**
	 * 根据Slid查询口岸列表的包裹数
	 */
	@Override
	public int queryCountByCondition(Map<String, Object> param) {
		return this.dispatchMapper.queryCountByCondition(param);
	}

}
