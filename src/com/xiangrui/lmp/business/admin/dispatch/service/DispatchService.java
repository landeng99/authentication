package com.xiangrui.lmp.business.admin.dispatch.service;


import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.xiangrui.lmp.business.admin.dispatch.vo.Dispatch;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgOperate;
public interface DispatchService
{

	void addSeaportList(Dispatch seaportList);


	String validateName(String name);


	List<Dispatch> queryAll(Map<String, Object> params);


	List<Dispatch> query(Map<String, Object> params);


	List<Dispatch> queryById(int id);


	XSSFWorkbook exportPackageByStatus(String fileName, List<PkgOperate> packageList);


	List<PkgOperate> queryByLogisticsCode(Map<String, Object> params);


	void updateSlidAndSeaportId(Map<String, Object> params);


	List<PkgOperate> queryBeInStatus(Map<String, Object> params);


	List<PkgOperate> queryAlreadyInStatus(Map<String, Object> params);


	List<PkgOperate> queryToBeStatus(Map<String, Object> params);


	Dispatch queryDispatchByCondition(Map<String, Object> param);


	List<PkgOperate> queryPkgOperateByCondition(Map<String, Object> param);


	void delByCondition(Map<String, Object> param);

	
	void updateById(Map<String, Object> params);
	

	void deleteById(Map<String, Object> params);
	

	XSSFWorkbook exportError(String errorName, List<PkgOperate> flagList);
	

	List<PkgOperate> queryByCondition(Map<String, Object> params);


	XSSFWorkbook exportExcel(String fileName, List<PkgOperate> list);


	int queryCountByCondition(Map<String, Object> param);

}
		