package com.xiangrui.lmp.business.admin.pkgcategory.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.pkgcategory.vo.PkgCategory;

public interface PkgCategoryMapper {

	/**
	 * 
	 * 查询包裹大类
	 * @param pkgCategory
	 * @return
	 */
	List<PkgCategory>queryPkgCategory(PkgCategory pkgCategory);
	
	/**
	 * 插入包裹大类
	 * @param pkgCategory
	 * @return
	 */
	int insertPkgCategory(PkgCategory pkgCategory);
	
	/**更新包裹大类信息
	 * @param pkgCategory
	 * @return
	 */
	int updatePkgCategory(PkgCategory pkgCategory);

    void updatePkgCategoryList(List<PkgCategory> arrayList);

    List<PkgCategory> queryPkgCateGoryLikeIdFramework(String id_framework);
	
	
	
}
