package com.xiangrui.lmp.business.admin.pkgcategory.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.pkgcategory.mapper.PkgCategoryMapper;
import com.xiangrui.lmp.business.admin.pkgcategory.service.PkgCategoryService;
import com.xiangrui.lmp.business.admin.pkgcategory.vo.PkgCategory;


@Service("pkgCategoryService")
public class PkgCategoryServiceImpl implements PkgCategoryService {
	
	
	@Autowired
	PkgCategoryMapper pkgCategoryMapper;
	
	@SystemServiceLog(description="新增包裹大类信息")
	@Override
	public int insertPkgCategory(PkgCategory pkgCategory) {
		return pkgCategoryMapper.insertPkgCategory(pkgCategory);
	}
	
	@Override
	public List<PkgCategory> queryPkgCategory(PkgCategory pkgCategory) {		 
		return pkgCategoryMapper.queryPkgCategory(pkgCategory);
	}
	
	@SystemServiceLog(description="修改包裹大类信息")
	@Override
	public int updatePkgCategory(PkgCategory pkgCategory) {		 
		return pkgCategoryMapper.updatePkgCategory(pkgCategory);
	}

	@SystemServiceLog(description="修改包裹大类大量信息")
    @Override
    public void updatePkgCategoryList(List<PkgCategory> arrayList)
    {
        pkgCategoryMapper.updatePkgCategoryList(arrayList);
    }

    @Override
    public List<PkgCategory> queryPkgCateGoryLikeIdFramework(String id_framework)
    {
        id_framework += "%";
        return pkgCategoryMapper.queryPkgCateGoryLikeIdFramework(id_framework);
    }
 
}
