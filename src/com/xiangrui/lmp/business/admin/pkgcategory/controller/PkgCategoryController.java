package com.xiangrui.lmp.business.admin.pkgcategory.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.pkgcategory.service.PkgCategoryService;
import com.xiangrui.lmp.business.admin.pkgcategory.vo.PkgCategory;

@Controller
@RequestMapping("/admin/pkgcategory")
public class PkgCategoryController
{

    /**
     * 包裹分类业务层操作
     */
    @Autowired
    PkgCategoryService pkgCategoryService;

    /**
     * 获取分类列表
     * 
     * @param req
     * @param resp
     * @return
     */
    @RequestMapping("/pkgcategorylist")
    public String list(HttpServletRequest req, HttpServletResponse resp)
    {

        PkgCategory params = new PkgCategory();

        List<PkgCategory> pkgCategoryList = pkgCategoryService
                .queryPkgCategory(params);

        req.setAttribute("pkgCategoryList", pkgCategoryList);

        return "/back/pkgcategorylist";
    }

    @RequestMapping("/initadd")
    public String initAdd(HttpServletRequest req, HttpServletResponse resp)
    {

        PkgCategory params = new PkgCategory();

        List<PkgCategory> pkgCategoryList = pkgCategoryService
                .queryPkgCategory(params);

        req.setAttribute("pkgCategoryList", pkgCategoryList);

        return "/back/pkgcategoryadd";
    }

    /**
     * 增加分类
     * 
     * @param req
     * @param resp
     * @param pkgCategory
     * @param id_framework
     * @return
     */
    @RequestMapping("/add")
    public String add(HttpServletRequest req, HttpServletResponse resp,
            PkgCategory pkgCategory, String id_framework)
    {
        int parentId = pkgCategory.getParent_id();

        String idFrameWork = id_framework + "_" + parentId;
        pkgCategory.setId_framework(idFrameWork);

        pkgCategoryService.insertPkgCategory(pkgCategory);
        PkgCategory params = new PkgCategory();

        List<PkgCategory> pkgCategoryList = pkgCategoryService
                .queryPkgCategory(params);

        req.setAttribute("pkgCategoryList", pkgCategoryList);
        return "/back/pkgcategorylist";
    }

    /**
     * 初始编辑页面
     * 
     * @param req
     * @param resp
     * @param pkgCategory
     * @return
     */
    @RequestMapping("initedit")
    public String initEdit(HttpServletRequest req, HttpServletResponse resp,
            PkgCategory pkgCategory)
    {

        List<PkgCategory> pkgcategorys = pkgCategoryService
                .queryPkgCategory(pkgCategory);
        PkgCategory pkgcate = pkgcategorys.get(0);
        req.setAttribute("pkgcate", pkgcate);
        PkgCategory params = new PkgCategory();

        List<PkgCategory> pkgCategoryList = pkgCategoryService
                .queryPkgCategory(params);

        req.setAttribute("pkgCategoryList", pkgCategoryList);

        return "/back/pkgcategoryedit";
    }

    /**
     * 编辑操作
     * 
     * @param req
     * @param resp
     * @param pkgCategory
     *            更新的分类
     * @param pframework
     *            分类对象的排序编码
     * @return
     */
    @RequestMapping("edit")
    public String edit(HttpServletRequest req, HttpServletResponse resp,
            PkgCategory pkgCategory, String pframework)
    {
        int parentId = pkgCategory.getParent_id();
        // 组建新的排序编码
        StringBuffer id_framework = new StringBuffer("");
        // 父类id不为0时,编码有俩部分组成,为0是,编码仅仅是0
        if (0 != parentId)
        {
            id_framework.append(pkgCategory.getId_framework());
            id_framework.append("_");
        }
        id_framework.append(parentId);
        pkgCategory.setId_framework(id_framework.toString());

        pkgCategoryService.updatePkgCategory(pkgCategory);

        String pframeworkTemp = "_" + pkgCategory.getId();
        // 通过原编码获取他的子类
        List<PkgCategory> list = pkgCategoryService
                .queryPkgCateGoryLikeIdFramework(pframework + pframeworkTemp);
        for (PkgCategory pkgCate : list)
        {
            // 更新子类的编码
            String idFram = pkgCate.getId_framework();
            // 新排序id_framework代替pframework
            String idWork = idFram.replace(pframework, id_framework);
            pkgCate.setId_framework(idWork);
        }
        pkgCategoryService.updatePkgCategoryList(list);
        PkgCategory params = new PkgCategory();

        List<PkgCategory> pkgCategoryList = pkgCategoryService
                .queryPkgCategory(params);

        req.setAttribute("pkgCategoryList", pkgCategoryList);

        return "/back/pkgcategorylist";
    }

}
