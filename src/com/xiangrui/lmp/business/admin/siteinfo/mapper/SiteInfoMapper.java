package com.xiangrui.lmp.business.admin.siteinfo.mapper;

import com.xiangrui.lmp.business.admin.siteinfo.vo.SiteInfo;

/**
 * 网站信息
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-8 下午6:34:11
 * </p>
 */
public interface SiteInfoMapper
{

    /**
     * 查询
     * 
     * @return
     */
    SiteInfo queryAll();

    /**
     * 更新
     * 
     * @param siteInfo
     */
    void updateSiteInfo(SiteInfo siteInfo);

    /**
     * 首次增加
     * 
     * @param siteInfo
     */
    void addSiteInfo(SiteInfo siteInfo);
}
