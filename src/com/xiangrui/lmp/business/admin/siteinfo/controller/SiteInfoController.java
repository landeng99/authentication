package com.xiangrui.lmp.business.admin.siteinfo.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.siteinfo.service.SiteInfoSerice;
import com.xiangrui.lmp.business.admin.siteinfo.vo.SiteInfo;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.constant.WebConstants;
import com.xiangrui.lmp.init.AppServerUtil;

/**
 * 网站信息
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-8 下午6:34:00
 *         </p>
 */
@Controller
@RequestMapping("/admin/siteInfo")
public class SiteInfoController
{
    /**
     * 图片后缀集合 [.jpg],[.png]
     */
    private static final String[] IMG_SUFFIX_ARY = new String[] { ".jpg",
            ".png", ".gif", ".bmp", ".jpeg", ".pcx", ".tiff", ".tga", ".exif",
            ".fpx", ".svg", ".psd", ".cdr", ".pcd", ".dxf", ".ufo", ".eps",
            ".hdri", ".ai", ".raw" };

    /**
     * 上传图片在服务器存放路径,物理路径
     */
    private static final String UPLOAD_IMG_PATH = AppServerUtil.getWebRoot()
            + WebConstants.UPLOAD_PATH_IMAGES;

    /**
     * 网站信息
     */
    @Autowired
    private SiteInfoSerice siteInfoSerice;


    /**
     * 网站信息
     * 
     * @param request
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request)
    {
        SiteInfo siteInfo = siteInfoSerice.queryAll();
        request.setAttribute("siteInfoPojo", siteInfo);

        return "back/webinfomanagement";
    }

    /**
     * 上传logo图片
     * 
     * @param request
     * @return
     */
    @RequestMapping("uploadSiteInfoLogo")
    @ResponseBody
    public String uploadSiteInfoLogo(HttpServletRequest request, String imgText)
    {

        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        MultipartFile imgFile1 = req.getFile("logoPic");

        // 获取文件后缀 并转化成小写
        String imgSuffix = imgFile1.getOriginalFilename()
                .substring(imgFile1.getOriginalFilename().lastIndexOf("."))
                .toLowerCase();

        // **************后台验证是否为图片格式************
        // imgSuffix是否是图片格式
        boolean isImgSuffix = false;
        for (String suffix : IMG_SUFFIX_ARY)
        {
            if (suffix.equals(imgSuffix))
            {
                isImgSuffix = true;
                break;
            }
        }
        // 不是图片格式,结束上传操作
        if (isImgSuffix == false)
        {
            return "";
        }
        // **************在前台已经js验证处理了************

        // 获取随机名称,补全图片格式
        String fileName = String.valueOf(System.currentTimeMillis())
                + imgSuffix;

        try
        {
            String flysString = getUploadImgPath(UPLOAD_IMG_PATH + fileName);

            // 上传保存到服务器物理路径去
            FileUtils.copyInputStreamToFile(imgFile1.getInputStream(),
                    new File(flysString));

            // 如果存在上次上传图片,并且没有保存到数据库中,清除上次上传没有保存数据库的图片文件信息
            if (null != imgText && !"".equals(imgText))
            {
                flysString = getUploadImgPath(imgText.replace(
                        SYSConstant.UPLOAD_IMG_PATH_DB, UPLOAD_IMG_PATH));
                File fileTemp = new File(flysString);
                if (fileTemp.exists())
                {
                    fileTemp.delete();
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // 返回到前台页面显示
        return getUploadImgPath(SYSConstant.UPLOAD_IMG_PATH_DB + fileName);
    }

    /**
     * 更新网站信息
     * 
     * @param request
     * @param siteInfo
     * @return
     */
    @RequestMapping("/updateSiteInfo")
    public String updateSiteInfo(HttpServletRequest request, SiteInfo siteInfo)
    {
        // 乱码处理
        String title = "";
        String logo = "";
        String copyright = "";
        try
        {
            title = new String(siteInfo.getTitle().getBytes("ISO-8859-1"),
                    "UTF-8");
            logo = new String(siteInfo.getLogo().getBytes("ISO-8859-1"),
                    "UTF-8");
            copyright = new String(siteInfo.getCopyright().getBytes(
                    "ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        siteInfo.setTitle(title);
        siteInfo.setLogo(logo);
        siteInfo.setCopyright(copyright);

        // 查询数据库信息
        SiteInfo siteInfodb = siteInfoSerice.queryAll();

        // 如果数据没有数据
        if (null == siteInfodb)
        {
            // 新增
            siteInfoSerice.addSiteInfo(siteInfo);
            request.setAttribute("siteInfoPojo", siteInfo);
            return "back/webinfomanagement";
        }

        // 是否要进行更新
        boolean isUpdate = false;

        // 如果 版权信息不是null和"" 并且与数据库不相同
        if (null != siteInfo.getCopyright()
                && !"".equals(siteInfo.getCopyright())
                && !siteInfo.getCopyright().equals(siteInfodb.getCopyright()))
        {

            // 更新最新版权
            siteInfodb.setCopyright(siteInfo.getCopyright());

            // 需要更新
            isUpdate = true;
        }

        // 原存储logo图片位置
        String logoOld = "";
        // logo信息
        if (null != siteInfo.getLogo() && !"".equals(siteInfo.getLogo())
                && !siteInfo.getLogo().equals(siteInfodb.getLogo()))
        {
            logoOld = null == siteInfodb.getLogo() ? "" : siteInfodb.getLogo();
            siteInfodb.setLogo(siteInfo.getLogo());
            isUpdate = true;
        }

        // title信息
        if (null != siteInfo.getCopyright() && !"".equals(siteInfo.getTitle())
                && !siteInfo.getTitle().equals(siteInfodb.getTitle()))
        {
            siteInfodb.setTitle(siteInfo.getTitle());
            isUpdate = true;
        }

        // 是否需要数据库操作
        if (isUpdate)
        {
            // 跟数据
            siteInfoSerice.updateSiteInfo(siteInfodb);

            // 删除原位置的图片
            if (!"".equals(logoOld))
            {
                /*
                 * // 获取原图片位置的物理位置 // 上传图片在服务器存放路径+文件名称 // 文件名称是,数据库数据去掉数据库开头位置
                 * logoOld = UPLOAD_IMG_PATH +
                 * logoOld.substring(UPLOAD_IMG_PATH_DB.length());
                 */
                // 删除文件
                new File(getUploadImgPath(logoOld.replace(
                        SYSConstant.UPLOAD_IMG_PATH_DB, UPLOAD_IMG_PATH)))
                        .delete();
            }
        }
        request.setAttribute("siteInfoPojo", siteInfodb);

        // 前台application的数据更新
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("siteInfo", null);
        return "back/webinfomanagement";
    }

    private String getUploadImgPath(String uploadImgPath)
    {
        String separator = File.separator;
        return uploadImgPath.replace("\\", separator);
    }
}
