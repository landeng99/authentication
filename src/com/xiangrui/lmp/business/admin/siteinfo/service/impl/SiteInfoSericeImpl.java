package com.xiangrui.lmp.business.admin.siteinfo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.siteinfo.mapper.SiteInfoMapper;
import com.xiangrui.lmp.business.admin.siteinfo.service.SiteInfoSerice;
import com.xiangrui.lmp.business.admin.siteinfo.vo.SiteInfo;

/**
 * 网站信息
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-8 下午6:34:18
 * </p>
 */
@Service("siteInfoSerice")
public class SiteInfoSericeImpl implements SiteInfoSerice
{

    @Autowired
    private SiteInfoMapper siteInfoMapper;

    @Override
    public SiteInfo queryAll()
    {
        return siteInfoMapper.queryAll();
    }

    @SystemServiceLog(description = "跟新网站基础信息")
    @Override
    public void updateSiteInfo(SiteInfo siteInfo)
    {
        siteInfoMapper.updateSiteInfo(siteInfo);
    }

    @SystemServiceLog(description = "首次添加网站信息")
    @Override
    public void addSiteInfo(SiteInfo siteInfo)
    {
        siteInfoMapper.addSiteInfo(siteInfo);
    }
}
