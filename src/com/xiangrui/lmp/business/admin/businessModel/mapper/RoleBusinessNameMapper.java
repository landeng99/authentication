package com.xiangrui.lmp.business.admin.businessModel.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.businessModel.vo.RoleBusinessName;

public interface RoleBusinessNameMapper
{
	List<RoleBusinessName> queryAllBusinessName();

	List<RoleBusinessName> queryRoleBusinessNameByRoleId(Integer role_id);

	void insertRoleBusinessName(RoleBusinessName roleBusinessName);

	void deleteRoleBusinessNameByRoleId(Integer role_id);
	
	List<RoleBusinessName> queryRoleBusinessNameByBackUserId(Integer user_id);
}
