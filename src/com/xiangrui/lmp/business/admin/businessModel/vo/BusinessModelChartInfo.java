package com.xiangrui.lmp.business.admin.businessModel.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class BusinessModelChartInfo implements Serializable
{
	private static final long serialVersionUID = -1L;
	private String months;
	private float actual_weight_count;
	private String actual_weight_count_str;

	public String getActual_weight_count_str()
	{
		BigDecimal bigDecimal = new BigDecimal(actual_weight_count);
		bigDecimal=bigDecimal.divide(new BigDecimal(1),2,BigDecimal.ROUND_HALF_UP);
		//bigDecimal.setScale(2);
		return bigDecimal.toPlainString();
	}

	public void setTotal_actual_weight_count_str(String actual_weight_count_str)
	{
		this.actual_weight_count_str = actual_weight_count_str;
	}
	public String getMonths()
	{
		return months;
	}
	public void setMonths(String months)
	{
		this.months = months;
	}
	public float getActual_weight_count()
	{
		return actual_weight_count;
	}
	public void setActual_weight_count(float actual_weight_count)
	{
		this.actual_weight_count = actual_weight_count;
	}
	
}
