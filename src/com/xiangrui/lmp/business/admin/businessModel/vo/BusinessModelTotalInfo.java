package com.xiangrui.lmp.business.admin.businessModel.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class BusinessModelTotalInfo implements Serializable
{
	private static final long serialVersionUID = 3566565718897208068L;
	private int total_client;
	private int total_package_count;
	private float total_actual_weight_count;
	private String total_actual_weight_count_str;

	public String getTotal_actual_weight_count_str()
	{
		BigDecimal bigDecimal = new BigDecimal(total_actual_weight_count);
		bigDecimal=bigDecimal.divide(new BigDecimal(1),2,BigDecimal.ROUND_HALF_UP);
		//bigDecimal.setScale(2);
		return bigDecimal.toPlainString();
	}

	public void setTotal_actual_weight_count_str(String total_actual_weight_count_str)
	{
		this.total_actual_weight_count_str = total_actual_weight_count_str;
	}

	public int getTotal_client()
	{
		return total_client;
	}

	public void setTotal_client(int total_client)
	{
		this.total_client = total_client;
	}

	public int getTotal_package_count()
	{
		return total_package_count;
	}

	public void setTotal_package_count(int total_package_count)
	{
		this.total_package_count = total_package_count;
	}

	public float getTotal_actual_weight_count()
	{
		return total_actual_weight_count;
	}

	public void setTotal_actual_weight_count(float total_actual_weight_count)
	{
		this.total_actual_weight_count = total_actual_weight_count;
	}

}
