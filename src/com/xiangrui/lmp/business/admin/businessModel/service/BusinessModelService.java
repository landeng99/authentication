package com.xiangrui.lmp.business.admin.businessModel.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessClientInfo;
import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessModelChartInfo;
import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessModelTotalInfo;

public interface BusinessModelService
{
	/**
	 * back_user_id后台用户ID
	 * user_type前台用户类型
	 * account帐户
	 * user_name前台用户名
	 * mobile手机号码
	 * last_name
	 * business_name业务名称
	 * start_time开始时间
	 * end_time结束时间
	 * query_day 查看天数
	 * @param params
	 * @return
	 */
	List<BusinessClientInfo> queryAll(Map<String, Object> params);
	
	BusinessModelTotalInfo queryTotal(Map<String, Object> params);
	
	List<BusinessModelChartInfo> queryCharInfoByFrontUserId(int user_id);
	
	/**
	 * user_id用户ID
	 * query_day 查看天数
	 * @param params
	 * @return
	 */
	BusinessModelTotalInfo queryInputPackageCountByFrontUserIdAndQueryDay(Map<String, Object> params);
	
	/**
	 * back_user_id后台用户ID
	 * business_name业务名称
	 * start_time开始时间
	 * end_time结束时间
	 * @param params
	 * @return
	 */
	List<BusinessModelChartInfo> queryCharInfo(Map<String, Object> params);
	
	/**
	 * 通过用户ID统计用户所有的包裹数
	 * @param user_id
	 * @return
	 */
	BusinessModelTotalInfo queryTotalPackageCountByFrontUserId(int user_id);
}
