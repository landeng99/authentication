package com.xiangrui.lmp.business.admin.businessModel.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.businessModel.mapper.BusinessModelMapper;
import com.xiangrui.lmp.business.admin.businessModel.service.BusinessModelService;
import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessClientInfo;
import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessModelChartInfo;
import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessModelTotalInfo;

@Service("businessModelService")
public class BusinessModelServiceImpl implements BusinessModelService
{

	@Autowired
	private BusinessModelMapper businessModelMapper;

	@Override
	public List<BusinessClientInfo> queryAll(Map<String, Object> params)
	{
		return businessModelMapper.queryAll(params);
	}

	@Override
	public BusinessModelTotalInfo queryTotal(Map<String, Object> params)
	{
		return businessModelMapper.queryTotal(params);
	}

	@Override
	public List<BusinessModelChartInfo> queryCharInfoByFrontUserId(int user_id)
	{
		return businessModelMapper.queryCharInfoByFrontUserId(user_id);
	}

	@Override
	public BusinessModelTotalInfo queryInputPackageCountByFrontUserIdAndQueryDay(Map<String, Object> params)
	{
		return businessModelMapper.queryInputPackageCountByFrontUserIdAndQueryDay(params);
	}

	@Override
	public List<BusinessModelChartInfo> queryCharInfo(Map<String, Object> params)
	{
		return businessModelMapper.queryCharInfo(params);
	}

	public 	/**
	 * 通过用户ID统计用户所有的包裹数
	 * @param user_id
	 * @return
	 */
	BusinessModelTotalInfo queryTotalPackageCountByFrontUserId(int user_id)
	{
		return businessModelMapper.queryTotalPackageCountByFrontUserId(user_id);
	}
}
