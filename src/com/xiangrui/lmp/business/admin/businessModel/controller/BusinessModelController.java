package com.xiangrui.lmp.business.admin.businessModel.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSON;
import com.xiangrui.lmp.business.admin.businessModel.service.BusinessModelService;
import com.xiangrui.lmp.business.admin.businessModel.service.RoleBusinessNameService;
import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessClientInfo;
import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessModelChartInfo;
import com.xiangrui.lmp.business.admin.businessModel.vo.BusinessModelTotalInfo;
import com.xiangrui.lmp.business.admin.businessModel.vo.RoleBusinessName;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.util.DateFormatUtils;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/businessModel")
public class BusinessModelController
{
	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";
	@Autowired
	private BusinessModelService businessModelService;
	/**
	 * 前台用户
	 */
	@Autowired
	private FrontUserService memberService;
	// 业务员授权相关
	@Autowired
	private RoleBusinessNameService roleBusinessNameService;

	@RequestMapping("/queryAll")
	public String queryAll(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent, 10, 0);
		}
		else
		{
			pageView = new PageView(1, 10, 0);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			params.put("back_user_id", user.getUser_id());
			List<RoleBusinessName> businessNameList = roleBusinessNameService.queryRoleBusinessNameByBackUserId(user.getUser_id());
			request.setAttribute("businessNameList", businessNameList);
		}
//		BusinessModelTotalInfo businessModelTotalInfo = businessModelService.queryTotal(params);
		params.put("pageView", pageView);
		List<BusinessClientInfo> businessModelList = businessModelService.queryAll(params);
		
		request.setAttribute("pageView", pageView);
		request.setAttribute("params", params);
		request.setAttribute("businessModelList", businessModelList);
//		request.setAttribute("businessModelTotalInfo", businessModelTotalInfo);
		return "back/businessModelList";
	}

	@RequestMapping("/search")
	public String search(HttpServletRequest request, String user_type, String account, String user_name, String mobile, String last_name, String business_name, String start_time, String end_time, String query_day)
	{
		// 分页查询
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent, 15, 0);
			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		}
		else
		{
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null)
			{
				params.put("back_user_id", user.getUser_id());
				List<RoleBusinessName> businessNameList = roleBusinessNameService.queryRoleBusinessNameByBackUserId(user.getUser_id());
				request.setAttribute("businessNameList", businessNameList);
			}
			pageView = new PageView(1, 15, 0);
			// 前台用户类型
			params.put("user_type", user_type);
			// 帐户
			params.put("account", account);
			// 前台用户名
			params.put("user_name", user_name);
			// 手机号码
			params.put("mobile", mobile);
			// last_name
			params.put("last_name", last_name);
			// 业务名称
			params.put("business_name", business_name);
			// 开始时间
			params.put("start_time", start_time);
			// 结束时间
			params.put("end_time", end_time);
			// 查看天数
			params.put("query_day", query_day);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);

		}
//		BusinessModelTotalInfo businessModelTotalInfo = businessModelService.queryTotal(params);
		params.put("pageView", pageView);
		List<BusinessClientInfo> businessModelList = businessModelService.queryAll(params);
		request.setAttribute("params", params);
		request.setAttribute("pageView", pageView);
		request.setAttribute("businessModelList", businessModelList);
//		request.setAttribute("businessModelTotalInfo", businessModelTotalInfo);
		return "back/businessModelList";
	}

	@RequestMapping("/personDetailCount")
	public String personDetailCount(HttpServletRequest request, String user_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", user_id);
		List<BusinessModelChartInfo> businessModelChartList = businessModelService.queryCharInfoByFrontUserId(Integer.parseInt(user_id));
		// 近一周入库包裹数
		params.put("query_day", "7");
		BusinessModelTotalInfo businessModel7TotalInfo = businessModelService.queryInputPackageCountByFrontUserIdAndQueryDay(params);
		request.setAttribute("businessModel7TotalInfo", businessModel7TotalInfo);
		// 近半个月入库包裹数
		params.put("query_day", "15");
		BusinessModelTotalInfo businessModel15TotalInfo = businessModelService.queryInputPackageCountByFrontUserIdAndQueryDay(params);
		request.setAttribute("businessModel15TotalInfo", businessModel15TotalInfo);
		// 近1个月入库包裹数
		params.put("query_day", "30");
		BusinessModelTotalInfo businessModel30TotalInfo = businessModelService.queryInputPackageCountByFrontUserIdAndQueryDay(params);
		request.setAttribute("businessModel30TotalInfo", businessModel30TotalInfo);
		// 近3个月入库包裹数
		params.put("query_day", "90");
		BusinessModelTotalInfo businessModel90TotalInfo = businessModelService.queryInputPackageCountByFrontUserIdAndQueryDay(params);
		request.setAttribute("businessModel90TotalInfo", businessModel90TotalInfo);
		// 近6个月入库包裹数
		params.put("query_day", "180");
		BusinessModelTotalInfo businessModel180TotalInfo = businessModelService.queryInputPackageCountByFrontUserIdAndQueryDay(params);
		request.setAttribute("businessModel180TotalInfo", businessModel180TotalInfo);
		// 总共包裹数
		BusinessModelTotalInfo businessModelTotalInfo = businessModelService.queryTotalPackageCountByFrontUserId(Integer.parseInt(user_id));
		request.setAttribute("businessModelTotalInfo", businessModelTotalInfo);
		request.setAttribute("params", params);
		String currentYear = DateFormatUtils.getCurrentYearString();
		List<String> currentYearLabelList = createLabelListByYearString(currentYear);
		List<String> currentYearDataList = createDataListByChartList(businessModelChartList, currentYearLabelList);
		String currentYearLabel = JSON.toJSONString(removeYear(currentYearLabelList));
		String currentYearData = JSON.toJSONString(currentYearDataList);
		request.setAttribute("currentYearLabel", currentYearLabel);
		request.setAttribute("currentYearData", currentYearData);
		FrontUser frontUser = memberService.queryFrontUserByUserId(Integer.parseInt(user_id));
		request.setAttribute("frontUser", frontUser);
		request.setAttribute("currentYear", currentYear);
		return "back/businessModelPersonCount";
	}

	@RequestMapping("/totalCount")
	public String totalCount(HttpServletRequest request, String business_name)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		// 业务名称
		if (StringUtils.trimToNull(business_name) != null)
		{
			try
			{
				business_name = new String(business_name.getBytes("iso8859-1"), "UTF-8");
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
			params.put("business_name", business_name);
		}
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			params.put("back_user_id", user.getUser_id());
		}
		// 统计信息
		BusinessModelTotalInfo businessModelTotalInfo = businessModelService.queryTotal(params);
		request.setAttribute("businessModelTotalInfo", businessModelTotalInfo);
		request.setAttribute("params", params);

		// 未走货用户查询
		// 半个月未走货用户
		params.put("query_day", "15");
		List<BusinessClientInfo> businessClientInfo15List = businessModelService.queryAll(params);
		request.setAttribute("businessClientInfo15String", createClientStringByBusinessClientInfoList(businessClientInfo15List));
		// 1个月未走货用户
		params.put("query_day", "30");
		List<BusinessClientInfo> businessClientInfo30List = businessModelService.queryAll(params);
		request.setAttribute("businessClientInfo30String", createClientStringByBusinessClientInfoList(businessClientInfo30List));
		// 3个月未走货用户
		params.put("query_day", "90");
		List<BusinessClientInfo> businessClientInfo90List = businessModelService.queryAll(params);
		request.setAttribute("businessClientInfo90String", createClientStringByBusinessClientInfoList(businessClientInfo90List));
		// 6个月未走货用户
		params.put("query_day", "180");
		List<BusinessClientInfo> businessClientInfo180List = businessModelService.queryAll(params);
		request.setAttribute("businessClientInfo180String", createClientStringByBusinessClientInfoList(businessClientInfo180List));

		String currentYear = DateFormatUtils.getCurrentYearString();
		String nextYear = DateFormatUtils.getNextYearString();
		String previousYear = DateFormatUtils.getPreviousYearString();
		// 当前年图表信息
		// 开始时间yyyy-MM-dd HH:mm:ss
		params.put("start_time", currentYear + "-01-01 00:00:00");
		// 结束时间
		params.put("end_time", nextYear + "-01-01 00:00:00");
		List<BusinessModelChartInfo> currentYearBusinessModelChartList = businessModelService.queryCharInfo(params);
		List<String> currentYearLabelList = createLabelListByYearString(currentYear);
		List<String> currentYearDataList = createDataListByChartList(currentYearBusinessModelChartList, currentYearLabelList);
		String currentYearLabel = JSON.toJSONString(removeYear(currentYearLabelList));
		String currentYearData = JSON.toJSONString(currentYearDataList);
		request.setAttribute("currentYearLabel", currentYearLabel);
		request.setAttribute("currentYearData", currentYearData);

		// 上一年图表信息
		// 开始时间yyyy-MM-dd HH:mm:ss
		params.put("start_time", previousYear + "-01-01 00:00:00");
		// 结束时间
		params.put("end_time", currentYear + "-01-01 00:00:00");
		List<BusinessModelChartInfo> previousYearBusinessModelChartList = businessModelService.queryCharInfo(params);
		List<String> previousYearLabelList = createLabelListByYearString(previousYear);
		List<String> previousYearDataList = createDataListByChartList(previousYearBusinessModelChartList, previousYearLabelList);
		String previousYearLabel = JSON.toJSONString(removeYear(previousYearLabelList));
		String previousYearData = JSON.toJSONString(previousYearDataList);
		request.setAttribute("previousYearLabel", previousYearLabel);
		request.setAttribute("previousYearData", previousYearData);
		request.setAttribute("currentYear", currentYear);
		request.setAttribute("previousYear", previousYear);
		return "back/businessModelTotalCount";
	}

	private static List<String> createLabelListByYearString(String yearStr)
	{
		List<String> lableList = new ArrayList<String>();
		lableList.add(yearStr + "-01");
		lableList.add(yearStr + "-02");
		lableList.add(yearStr + "-03");
		lableList.add(yearStr + "-04");
		lableList.add(yearStr + "-05");
		lableList.add(yearStr + "-06");
		lableList.add(yearStr + "-07");
		lableList.add(yearStr + "-08");
		lableList.add(yearStr + "-09");
		lableList.add(yearStr + "-10");
		lableList.add(yearStr + "-11");
		lableList.add(yearStr + "-12");
		return lableList;
	}

	private static List<String> createDataListByChartList(List<BusinessModelChartInfo> businessModelChartList, List<String> lableList)
	{
		List<String> dataList = new ArrayList<String>();
		Map<String, String> existChartInfoMap = new HashMap<String, String>();
		if (businessModelChartList != null && businessModelChartList.size() > 0)
		{
			for (BusinessModelChartInfo businessModelChartInfo : businessModelChartList)
			{
				existChartInfoMap.put(businessModelChartInfo.getMonths(), businessModelChartInfo.getActual_weight_count_str());
			}
		}
		if (lableList != null && lableList.size() > 0)
		{
			for (String label : lableList)
			{
				if (existChartInfoMap.containsKey(label))
				{
					dataList.add(existChartInfoMap.get(label));
				}
				else
				{
					dataList.add("0");
				}
			}
		}
		return dataList;
	}

	public static String createClientStringByBusinessClientInfoList(List<BusinessClientInfo> businessClientInfoList)
	{
		String clientString = "";
		if (businessClientInfoList != null && businessClientInfoList.size() > 0)
		{
			for (BusinessClientInfo businessClientInfo : businessClientInfoList)
			{
				clientString += businessClientInfo.getAccount() + ",    ";
			}
		}
		return clientString;
	}

	private static List<String> removeYear(List<String> lableList)
	{
		List<String> realList = new ArrayList<String>();
		if (lableList != null && lableList.size() > 0)
		{
			for (String label : lableList)
			{
				realList.add(StringUtils.substringAfterLast(label, "-"));
			}
		}
		return realList;
	}
}
