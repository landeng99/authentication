package com.xiangrui.lmp.business.admin.frontuserwithlog.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.frontuserwithlog.mapper.FrontUserWithLogMapper;
import com.xiangrui.lmp.business.admin.frontuserwithlog.service.FrontUserWithLogService;
import com.xiangrui.lmp.business.admin.frontuserwithlog.vo.FrontUserWithLog;

/**
 * 索赔用的到用户信息
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:20:20
 * </p>
 */
@Service("frontUserWithLogService")
public class FrontUserWithLogServiceImpl implements FrontUserWithLogService
{

    /**
     * 索赔用到的用户信息
     */
    @Autowired
    private FrontUserWithLogMapper frontUserWithLogMapper;

    /**
     * 根据用户id查询用户信息
     */
    @Override
    public FrontUserWithLog getFrontUserWithLogById(int user_id)
    {
        return frontUserWithLogMapper.getFrontUserWithLogById(user_id);
    }

}
