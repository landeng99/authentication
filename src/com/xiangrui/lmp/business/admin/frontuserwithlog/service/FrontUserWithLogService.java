package com.xiangrui.lmp.business.admin.frontuserwithlog.service;

import com.xiangrui.lmp.business.admin.frontuserwithlog.vo.FrontUserWithLog;

/**
 * 索赔用到的用户信息
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:20:02
 * </p>
 */
public interface FrontUserWithLogService
{

    /**
     * 根据用户id查询用户信息
     * 
     * @param user_id
     * @return
     */
    FrontUserWithLog getFrontUserWithLogById(int user_id);
}
