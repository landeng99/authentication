package com.xiangrui.lmp.business.admin.frontuserwithlog.vo;

import com.xiangrui.lmp.business.base.BaseFrontUser;

/**
 * 索赔用的用户信息
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 下午3:21:27
 * </p>
 */
public class FrontUserWithLog extends BaseFrontUser
{

    private static final long serialVersionUID = 1L;

    /**
     * 状态
     */
    private int status;

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

}
