package com.xiangrui.lmp.business.admin.attachService.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.attachService.mapper.AttachServiceMapper;
import com.xiangrui.lmp.business.admin.attachService.mapper.MemberAttachMapper;
import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.attachService.vo.MemberAttach;
import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;
import com.xiangrui.lmp.business.admin.memberrate.mapper.MemberRateMapper;
import com.xiangrui.lmp.business.admin.store.mapper.FrontUserMapper;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;

@Service(value = "attachServiceService")
public class AttachServiceServiceImpl implements AttachServiceService
{

    /**
     * 用户信息
     */
    @Autowired
    private FrontUserMapper frontUserMapper;

    /**
     * 关于会员信息
     */
    @Autowired
    private MemberRateMapper memberRateMapper;

    /**
     * 增值服务
     */
    @Autowired
    private AttachServiceMapper attachServiceMapper;

    /**
     * 增值与会员关联的价格
     */
    @Autowired
    private MemberAttachMapper memberAttachMapper;

    /**
     * 分页查询所有增值服务
     * 
     * @param params
     * @return
     */
    @Override
    public List<AttachService> queryAllAttachService(Map<String, Object> params)
    {

        return attachServiceMapper.queryAllAttachService(params);
    }

    /**
     * 查询增值服务
     * 
     * @param attach_id
     * @return
     */
    @Override
    public AttachService queryAttachServiceById(int attach_id)
    {

        return attachServiceMapper.queryAttachServiceById(attach_id);
    }

    /**
     * 更新增值服务信息
     * 
     * @param attachService
     * @return
     */
    @SystemServiceLog(description = "更新增值服务信息")
    @Override
    public void updateAttachService(AttachService attachService)
    {

        attachServiceMapper.updateAttachService(attachService);
    }

    /**
     * 添加增值服务
     * 
     * @param attachService
     * @return
     */
    @SystemServiceLog(description = "添加增值服务")
    @Override
    public void insertAttachService(AttachService attachService)
    {

        attachServiceMapper.insertAttachService(attachService);
    }

    /**
     * 逻辑删除增值服务
     * 
     * @param attachService
     * @return
     */
    @SystemServiceLog(description = "删除增值服务")
    @Override
    public void updateAttachServiceIsdeleted(AttachService attachService)
    {

        attachServiceMapper.updateAttachServiceIsdeleted(attachService);
    }

    /**
     * 服务名称
     * 
     * @return
     */
    @Override
    public List<String> queryAttachServiceName()
    {

        return attachServiceMapper.queryAttachServiceName();
    }

    /**
     * 服务名称重复检查
     * 
     * @param service_name
     * @return
     */
    @Override
    public List<AttachService> checkAttachService(String service_name)
    {
        return attachServiceMapper.checkAttachService(service_name);
    }

    @SystemServiceLog(description = "修改增值服务及其各会员价格")
    @Override
    public void updateAttachService(AttachService attachService,
            List<MemberAttach> list)
    {
        this.attachServiceMapper.updateAttachService(attachService);
        for (MemberAttach memberAttach : list)
        {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("RATE_ID", memberAttach.getRate_id());
            map.put("ATTACH_ID", memberAttach.getAttach_id());
            List<MemberAttach> attachs = this.memberAttachMapper
                    .queryMemberAttach(map);
            if (null == attachs || attachs.size() < 1)
            {
                this.memberAttachMapper.addMemberAttach(memberAttach);
            } else
            {
                this.memberAttachMapper.updateMemberAttach(memberAttach);
            }
        }
    }

    @SystemServiceLog(description = "新增增值服务及其各会员价格")
    @Override
    public void insertAttachService(AttachService attachService,
            List<MemberAttach> list)
    {
        this.attachServiceMapper.insertAttachService(attachService);
        for (MemberAttach memberAttach : list)
        {
            memberAttach.setAttach_id(attachService.getAttach_id());
            this.memberAttachMapper.addMemberAttach(memberAttach);
        }
    }

    @Override
    public List<AttachService> selectAttachServiceByPackageId(int package_id)
    {
        return this.attachServiceMapper
                .selectAttachServiceByPackageId(package_id);
    }
    
    @Override
    public List<AttachService> selectAttachServiceByOnlyPackageId(int package_id)
    {
    	return this.attachServiceMapper
    			.selectAttachServiceByOnlyPackageId(package_id);
    }

    @Override
    public List<AttachService> selectAttachService(int user_id)
    {
        // user_id获取用户得到其的积分
        FrontUser frontUser = this.frontUserMapper.queryFrontUserById(user_id);
        int integral = frontUser.getIntegral();
        int rate_id = 0;
        
        Map<String, Object> params =new HashMap<String, Object>();
        params.put("integral", integral);
        params.put("rate_type", frontUser.getUser_type());
       
        MemberRate memberRate = this.memberRateMapper
                .queryMemberRateByIntegral(params);
        if (memberRate!=null)
        {
            // integral获取会员得到其的id
   
            if (null != memberRate)
            {
               
            }
            rate_id = memberRate.getRate_id();
        }
        // 组合会员id得到这个会员或者用户的一系列增值服务及其价格信息
        return this.attachServiceMapper.selectAttachService(rate_id);
    }

}
