package com.xiangrui.lmp.business.admin.attachService.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.attachService.vo.MemberAttach;

public interface AttachServiceService
{
    /**
     * 分页查询所有增值服务
     * 
     * @param params
     * @return
     */
    List<AttachService> queryAllAttachService(Map<String, Object> params);

    /**
     * 查询增值服务
     * 
     * @param attach_id
     * @return
     */
    AttachService queryAttachServiceById(int attach_id);

    /**
     * 更新增值服务信息
     * 
     * @param attachService
     * @return
     */
    void updateAttachService(AttachService attachService);

    /**
     * 添加增值服务
     * 
     * @param attachService
     * @return
     */
    void insertAttachService(AttachService attachService);

    /**
     * 逻辑删除增值服务
     * 
     * @param attachService
     * @return
     */
    void updateAttachServiceIsdeleted(AttachService attachService);

    /**
     * 服务名称
     * 
     * @return
     */
    List<String> queryAttachServiceName();

    /**
     * 服务名称重复检查
     * 
     * @param service_name
     * @return
     */
    List<AttachService> checkAttachService(String service_name);

    void updateAttachService(AttachService attachService,
            List<MemberAttach> list);

    void insertAttachService(AttachService attachService,
            List<MemberAttach> list);
    
    /**
     * 连接查询的增值服务信息,即某个包裹的所有增值服务,当本服务的价格(serlver_price)为0时,本服务没开启
     * @param package_id
     * @return
     */
    List<AttachService> selectAttachServiceByPackageId(int package_id);
    
    /**
     * 连接查询的增值服务信息,即某个用户的的所有增值服务,serlet_price为会员价格
     * @param user_id
     * @return
     */
    List<AttachService> selectAttachService(int user_id);

	List<AttachService> selectAttachServiceByOnlyPackageId(int package_id);
}
