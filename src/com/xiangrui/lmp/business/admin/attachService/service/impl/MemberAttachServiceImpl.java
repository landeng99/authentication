package com.xiangrui.lmp.business.admin.attachService.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.attachService.mapper.MemberAttachMapper;
import com.xiangrui.lmp.business.admin.attachService.mapper.MemberRateAttachMapper;
import com.xiangrui.lmp.business.admin.attachService.service.MemberAttachService;
import com.xiangrui.lmp.business.admin.attachService.vo.MemberAttach;
import com.xiangrui.lmp.business.admin.attachService.vo.MemberRateAttach;

/**
 * 增值与会员关联的价格
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-19 上午11:34:57</p>
 */
@Service("memberAttachService")
public class MemberAttachServiceImpl implements MemberAttachService
{

    @Autowired
    private MemberAttachMapper mapper;
    
    @Autowired
    private MemberRateAttachMapper rateMapper;
    
    @SystemServiceLog(description="新增增值服务关联会员的价格信息")
    @Override
    public void saveMemberAttach(MemberAttach memberAttach)
    {
        this.mapper.addMemberAttach(memberAttach);
    }

    @SystemServiceLog(description="删除某个增值服务的所有价格信息")
    @Override
    public void deleteByAttachId(int attachId)
    {
        MemberAttach memberAttach = new MemberAttach();
        memberAttach.setAttach_id(attachId);
        this.mapper.deleteMemberAttach(memberAttach);
    }

    @SystemServiceLog(description="删除某个会员的所有价格信息")
    @Override
    public void deleteByRateId(int rateId)
    {
        MemberAttach memberAttach = new MemberAttach();
        memberAttach.setRate_id(rateId);
        this.mapper.deleteMemberAttach(memberAttach);
    }

    @SystemServiceLog(description="删除某个价格信息")
    @Override
    public void deleteById(int attachId, int rateId)
    {
        MemberAttach memberAttach = new MemberAttach();
        memberAttach.setAttach_id(attachId);
        memberAttach.setRate_id(rateId);
        this.mapper.deleteMemberAttach(memberAttach);
    }

    @SystemServiceLog(description="修改某个增值服务的所有价格信息")
    @Override
    public void updateMemberAttachByAttachId(float price, int attachId)
    {
        MemberAttach memberAttach = new MemberAttach();
        memberAttach.setAttach_id(attachId);
        memberAttach.setService_price(price);
        this.mapper.updateMemberAttach(memberAttach);
    }

    @SystemServiceLog(description="修改某个增值服务的所有价格信息")
    @Override
    public void updateMemberAttachByRateId(float price, int rateId)
    {
        MemberAttach memberAttach = new MemberAttach();
        memberAttach.setService_price(price);
        memberAttach.setRate_id(rateId);
        this.mapper.updateMemberAttach(memberAttach);
    }

    @SystemServiceLog(description="修改某个增值服务的所有价格信息")
    @Override
    public void updateMemberAttach(float price, int attachId, int rateId)
    {
        MemberAttach memberAttach = new MemberAttach();
        memberAttach.setAttach_id(attachId);
        memberAttach.setRate_id(rateId);
        memberAttach.setService_price(price);
        this.mapper.updateMemberAttach(memberAttach);
    }

    @Override
    public List<MemberAttach> queryRate(Map<String, Object> map)
    {
        return this.mapper.queryRateAll(map);
    }

    @Override
    public List<MemberAttach> queryAttach(Map<String, Object> map)
    {
        return this.mapper.queryAttachAll(map);
    }

    @Override
    public List<MemberAttach> queryAll(Map<String, Object> map)
    {
        return this.mapper.queryAll(map);
    }

    @Override
    public List<MemberAttach> queryMemberAttach(Map<String, Object> map)
    {
        return this.mapper.queryMemberAttach(map);
    }

    @Override
    public List<MemberAttach> queryByRateId(int rateId)
    {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("RATE_ID", rateId);
        return this.mapper.queryRateAll(map);
    }

    @Override
    public List<MemberAttach> queryByAttachId(int attachId)
    {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ATTACH_ID", attachId);
        return this.mapper.queryAttachAll(map);
    }

    @Override
    public MemberAttach queryById(int attachId, int rateId)
    {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ATTACH_ID", attachId);
        map.put("RATE_ID", rateId);
        List<MemberAttach> lists = this.mapper.queryAll(map);
        if(null != lists && lists.size()>0){
            return lists.get(0);
        }
        return null;
    }

    @Override
    public MemberAttach queryAllById(int attachId, int rateId)
    {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ATTACH_ID", attachId);
        map.put("RATE_ID", rateId);
        List<MemberAttach> lists = this.mapper.queryMemberAttach(map);
        if(null != lists && lists.size()>0){
            return lists.get(0);
        }
        return null;
    }

    @Override
    public List<MemberRateAttach> findRate()
    {
        return this.rateMapper.findRate();
    }

    @SystemServiceLog(description="更新增值服务于会员关联的价格数据")
    @Override
    public void updateMemberAttach(List<MemberAttach> list)
    {
        for (MemberAttach memberAttach : list)
        {   
            this.mapper.updateMemberAttach(memberAttach);
        }
    }

    @SystemServiceLog(description="更新某会员的增值服务各价格数据")
    @Override
    public void updateMemberAttachByRateId(List<MemberAttach> list)
    {
        for (MemberAttach memberAttach : list)
        {   
            MemberAttach mem = new MemberAttach();
            mem.setService_price(memberAttach.getService_price());
            mem.setRate_id(memberAttach.getRate_id());
            this.mapper.updateMemberAttach(mem);
        }
    }
    
}
