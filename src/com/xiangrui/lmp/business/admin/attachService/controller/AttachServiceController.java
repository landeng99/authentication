package com.xiangrui.lmp.business.admin.attachService.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.attachService.service.MemberAttachService;
import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.attachService.vo.MemberAttach;
import com.xiangrui.lmp.business.admin.attachService.vo.MemberRateAttach;
import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;
import com.xiangrui.lmp.business.admin.memberrate.service.MemberRateService;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/attachService")
public class AttachServiceController
{
    @Autowired
    private AttachServiceService attachServiceService;

    @Autowired
    private MemberAttachService memberAttachService;

    @Autowired
    private MemberRateService memberRateService;

    /**
     * 增值服务名称名称1：不存在
     */
    private static final String SERVICE_NAME_NOTEXIST = "1";
    /**
     * 增值服务名称名称0：存在
     */
    private static final String SERVICE_NAME_EXIST = "0";

    /**
     * 增值服务初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/attachServiceInit")
    public String attachServiceInit(HttpServletRequest request)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);

        return attachServiceSearch(request, params);
    }



    /**
     * 增值服务查询
     * 
     * @param request
     * @param service_name
     * @param priceMin
     * @param priceMax
     * 
     * @return
     */
    @RequestMapping("/attachServiceSearch")
    public String attachServiceSearch(HttpServletRequest request,
            String service_name)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);

        params.put("service_name", service_name);


        return attachServiceSearch(request, params);
    }

    /**
     * 添加增值服务初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/addAttachService")
    public String addAttachService(HttpServletRequest request)
    {
        List<MemberRateAttach> memberRates = memberAttachService.findRate();
        request.setAttribute("memberRates", memberRates);
        return "back/addAttachService";
    }

    /**
     * 增值服务名称重复检查
     * 
     * @param request
     * @param scode
     * @return
     */
    @RequestMapping("/checkServiceName")
    @ResponseBody
    public String checkServiceName(HttpServletRequest request,
            String attach_id, String service_name)
    {
        String checkResult = SERVICE_NAME_EXIST;

        List<AttachService> seaportList = attachServiceService
                .checkAttachService(service_name);

        if (seaportList == null || seaportList.size() == 0)
        {
            checkResult = SERVICE_NAME_NOTEXIST;
        }

        return checkResult;
    }

    /**
     * 增值服务保存到数据库
     * 
     * @param request
     * @param attachService
     * 
     * 
     * @return
     */
    @RequestMapping("/insertAttachService")
    @ResponseBody
    public String insertAttachService(HttpServletRequest request,
            AttachService attachService, String resultPrice, String resultRateId)
    {
        List<MemberAttach> list = new ArrayList<MemberAttach>();
        String[] prices = resultPrice.split(",");
        String[] rateIds = resultRateId.split(",");
        for (int i = 0; i < prices.length; i++)
        {
            MemberAttach memberAttach = new MemberAttach();
            int rate_id = Integer.parseInt(rateIds[i]);
            memberAttach.setRate_id(rate_id);
            float service_price = Float.parseFloat(prices[i]);
            memberAttach.setService_price(service_price);
            memberAttach.setAttach_id(attachService.getAttach_id());
            list.add(memberAttach);
        }
        attachServiceService.insertAttachService(attachService, list);

        ServletContext applicationg = request.getSession().getServletContext();
        applicationg.setAttribute("TariffHomePageList", null);
        //前台重量查询价格的会员有变化,需要清空会员信息
        applicationg.setAttribute("MemberRateIdHomePageList", null);
        return null;
    }

    /**
     * 增值服务修改初始化
     * 
     * @param request
     * @param attach_id
     * @return
     */
    @RequestMapping("/modifyAttachService")
    public String modifyAttachService(HttpServletRequest request,
            String attach_id)
    {
        // 增值服务信息
        AttachService attachService = attachServiceService
                .queryAttachServiceById(Integer.parseInt(attach_id));
        request.setAttribute("attachService", attachService);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ATTACH_ID", attach_id);

        List<MemberAttach> memberAttachs = this.memberAttachService
                .queryRate(map);
        request.setAttribute("memberAttachs", memberAttachs);

        List<MemberRateAttach> memberRates = memberAttachService.findRate();
        request.setAttribute("memberRates", memberRates);

        return "back/updateAttachService";
    }

    /**
     * 更新增值服务信息
     * 
     * @param request
     * @param attachService
     * 
     * @return
     */
    @RequestMapping("/updateAttachService")
    @ResponseBody
    public Map<String,Object> updateAttachService(HttpServletRequest request,
            AttachService attachService)
    {
        List<MemberAttach> list = new ArrayList<MemberAttach>();
        
        String ratePriceList=  request.getParameter("ratePriceList");
        @SuppressWarnings("unchecked")
        Map<String,String>  map= JSONTool.getMapFromJson(ratePriceList);
 
        for (Entry<String,String> entry:map.entrySet())
        {
            MemberAttach memberAttach = new MemberAttach();
            int rate_id = Integer.parseInt(entry.getKey());
            memberAttach.setRate_id(rate_id);
            float service_price = Float.parseFloat(entry.getValue());
            memberAttach.setService_price(service_price);
            memberAttach.setAttach_id(attachService.getAttach_id());
            list.add(memberAttach);
        }
        attachServiceService.updateAttachService(attachService, list);

        // 注销前台资费出的信息,前台刷新可以重新查询
        ServletContext applicationg = request.getSession().getServletContext();
        applicationg.setAttribute("TariffHomePageList", null);
        //前台重量查询价格的会员有变化,需要清空会员信息
        applicationg.setAttribute("MemberRateIdHomePageList", null);
        Map<String,Object> resultMap=new HashMap<String,Object>();
        resultMap.put("result", true);
        return resultMap;
    }

    /**
     * 增值服务信息删除
     * 
     * @param request
     * @param attachService
     * @return
     */
    @RequestMapping("/deleteAttachService")
    @ResponseBody
    public String deleteAttachService(HttpServletRequest request,
            AttachService attachService)
    {
        attachService.setIsdeleted(BaseAttachService.ISDELETED_YES);
        // 删除资费信息
        attachServiceService.updateAttachServiceIsdeleted(attachService);

        // 注销前台资费处的信息,前台刷新可以重新查询
        ServletContext applicationg = request.getSession().getServletContext();
        applicationg.setAttribute("TariffHomePageList", null);
        //前台重量查询价格的会员有变化,需要清空会员信息
        applicationg.setAttribute("MemberRateIdHomePageList", null);
        return null;
    }
    
    /**
     * 跳转增值服务首页的数据查询即其处理
     * @param request
     * @param map 查询用到的参数,至少有个pageView
     * @return 固定为  back/attachServiceList
     */
    public String attachServiceSearch(HttpServletRequest request,
            Map<String, Object> map)
    {
        Map<String, Object> params = new HashMap<String, Object>();

        // 逻辑删除0：未删除
        params.put("isdeleted", BaseAttachService.ISDELETED_NO);
        params.put("ISDELETED", BaseAttachService.ISDELETED_NO);

        // 获取所有的会员信息
        List<MemberRate> memberRates = this.memberRateService
                .queryAllMemberRate(params);
        // 获取所有的价格信息
        List<MemberAttach> attachs = this.memberAttachService.queryAll(params);
        // 增值服务名称
        List<String> attachServiceName = attachServiceService
                .queryAttachServiceName();

        params.put("pageView", map.get("pageView"));
        params.put("service_name", map.get("service_name"));
        /*params.put("priceMin", map.get("priceMin"));
        params.put("priceMax", map.get("priceMax"));*/

        // 增值服务列表
        List<AttachService> attachServiceList = attachServiceService
                .queryAllAttachService(params);

        // 页面显示用的纯价格数据
        List<Float> servicePriceList = new ArrayList<Float>();
        // 遍历所有增值服务
        for (AttachService asObj : attachServiceList)
        {
            int asId = asObj.getAttach_id();
            // 遍历所有会员
            for (MemberRate mrObj : memberRates)
            {
                int mrId = mrObj.getRate_id();

                // 找到价格 retFloat[0]是在attachs中找到价格的那条记录位置的下标
                float[] retFloat = getServicePrice(attachs, mrId, asId);
                // 在attachs中找到价格
                if (retFloat[0] != -1)
                {
                    // 获取下标,删除attachs中的这条数据
                    // getServicePrice中遍历了attachs这个数据,找到一条,删除一条,在逻辑上似乎提供了些效率
                    int i = (int) retFloat[0];
                    attachs.remove(attachs.get(i));
                }
                // 读取价格
                servicePriceList.add(retFloat[1]);
            }
        }
        // 所有价格信息,全部用于数据的主体内容显示
        request.setAttribute("servicePriceList", servicePriceList);
        // 会员信息的数据,目的是在jsp中显示是,一行数据从servicePriceList中读取几个数据,相当于表格信息的列数
        request.setAttribute("lengSize", memberRates.size());

        // 所有增值服务名称,用于查询条件中
        request.setAttribute("attachServiceName", attachServiceName);
        // 所有增值服务信息,主要用于数据的纵向显示,他能确定行数=attachServiceList.size()
        request.setAttribute("attachServiceList", attachServiceList);
        // 所有会员信息,全部用于数据的横向表头显示,他能确定列数=memberRates.size()+3
        request.setAttribute("memberRates", memberRates);
        request.setAttribute("pageView", map.get("pageView"));

        return "back/attachServiceList";
    }
    
    /**
     * 找到价格的那条记录位置的下标,找到数据的价格具体值
     * 
     * @param list
     *            所有价格
     * @param mrId
     *            会员id
     * @param asId
     *            服务id
     * @return 下标为-1时,价格默认为0,是没有在list中找到对应信息
     */
    public float[] getServicePrice(List<MemberAttach> list, int mrId, int asId)
    {
        int i = 0;
        for (MemberAttach memberAttach : list)
        {
            //
            boolean isMr = mrId == memberAttach.getMemberRateAttach()
                    .getRate_id();
            //
            boolean isAs = asId == memberAttach.getAttachService()
                    .getAttach_id();

            if (isMr && isAs)
            {
                float[] retFloat = new float[2];
                retFloat[0] = i;
                retFloat[1] = memberAttach.getService_price();
                return retFloat;
            }
            i++;
        }
        float[] retFloat = new float[2];
        retFloat[0] = -1;
        retFloat[1] = 0;
        return retFloat;
    }
}
