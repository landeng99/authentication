package com.xiangrui.lmp.business.admin.attachService.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.attachService.vo.MemberAttach;

/**
 * 增值与会员关联的价格
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-19 上午11:35:47</p>
 */
public interface MemberAttachMapper
{

    /**
     * 关联会员
     * @param map RATE_ID 会员id,ATTACH_ID 增值id,RATE_NAME 会员名称
     * @return 
     */
    List<MemberAttach> queryRateAll(Map<String, Object> map);
    /**
     * 关联增值
     * @param map RATE_ID 会员id,ATTACH_ID 增值id,
     *  SERVICE_NAME 增值服务名,ISDELETED 增值服务删除状态
     * @return
     */
    List<MemberAttach> queryAttachAll(Map<String, Object> map);
    /**
     * 关联会员和增值
     * @param map RATE_ID 会员id,ATTACH_ID 增值id,RATE_NAME 会员名称,
     *  SERVICE_NAME 增值服务名,ISDELETED 增值服务删除状态
     * @return
     */
    List<MemberAttach> queryAll(Map<String, Object> map);
    /**
     * 仅仅价格表 RATE_ID 会员id,ATTACH_ID 增值id
     * @param map
     * @return
     */
    List<MemberAttach> queryMemberAttach(Map<String, Object> map);
    /**
     * 新增价格数据
     * @param memberAttach
     */
    void addMemberAttach(MemberAttach memberAttach);
    /**
     * 修改价格 rate_id=0则修改等于attach_id的数据,反之,一样.如果俩个值都为0,修改所有
     * @param memberAttach rate_id,attach_id
     */
    void updateMemberAttach(MemberAttach memberAttach);
    /**
     * 删除价格 rate_id=0则删除等于attach_id的数据,反之,一样.如果俩个值都为0,删除所有
     * @param memberAttach rate_id,attach_id
     */
    void deleteMemberAttach(MemberAttach memberAttach);
}
