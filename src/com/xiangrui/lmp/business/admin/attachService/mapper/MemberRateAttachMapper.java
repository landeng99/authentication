package com.xiangrui.lmp.business.admin.attachService.mapper;

import java.util.List;

import com.xiangrui.lmp.business.admin.attachService.vo.MemberRateAttach;

public interface MemberRateAttachMapper
{

    /**
     * 
     * @return
     */
    List<MemberRateAttach> findRate();

}
