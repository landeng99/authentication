package com.xiangrui.lmp.business.admin.attachService.vo;

import com.xiangrui.lmp.business.base.BaseMemberAttach;

/**
 * 增值服务关联会员的价格关系表
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-19 上午11:24:56
 * </p>
 */
public class MemberAttach extends BaseMemberAttach
{

    private static final long serialVersionUID = 1L;

    /**
     * 增值信息
     */
    private AttachService attachService;
    /**
     * 会员信息
     */
    private MemberRateAttach memberRateAttach;
    public AttachService getAttachService()
    {
        return attachService;
    }
    public void setAttachService(AttachService attachService)
    {
        this.attachService = attachService;
    }
    public MemberRateAttach getMemberRateAttach()
    {
        return memberRateAttach;
    }
    public void setMemberRateAttach(MemberRateAttach memberRateAttach)
    {
        this.memberRateAttach = memberRateAttach;
    }
    
    
}
