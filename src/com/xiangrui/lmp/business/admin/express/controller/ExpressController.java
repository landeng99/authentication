package com.xiangrui.lmp.business.admin.express.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.kuaidi100.service.ExpressInfoService;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.JacksonHelper;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.kuaidiUtil.PostToKuaidi100;
import com.xiangrui.lmp.util.kuaidiUtil.pojo.ResultItem;
import com.xiangrui.lmp.util.kuaidiUtil.pojo.TaskResponse;

@Controller
@RequestMapping("/admin/express")
public class ExpressController
{

    /**
     * 快递单号1：不存在
     */
    private static final String EMS_CODE_NOTEXIST = "1";

    /**
     * 快递单号0：存在
     */
    private static final String EMS_CODE_EXIST = "0";

    /**
     * 快递公司
     */
    // TODO 当前只有EMS吗
    private static final String COMPANY = "ems";

    /**
     * 重复订阅代号
     */
    private static final String REPEAT_SUBSCRIPTION = "501";

    /**
     * 检索条件 记录
     */
    private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

    @Autowired
    private ExpressInfoService expressInfoService;

    @Autowired
    private PackageService packageService;

    /**
     * 快递信息列表初始化
     * 
     * @param request
     * @return
     */
    @RequestMapping("/expressInit")
    public String expressInit(HttpServletRequest request)
    {
        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("pageView", pageView);
        // 快递列表
        List<ExpressInfo> expressInfoList = expressInfoService
                .queryAllExpressInfo(params);

        request.setAttribute("expressInfoList", expressInfoList);
        request.setAttribute("pageView", pageView);

        return "back/expressList";
    }

    /**
     * 快递信息列表查询
     * 
     * @param request
     * @param com
     * @param nu
     * @return
     */
    @SuppressWarnings("unchecked")
    @RequestMapping("/searchExpress")
    public String searchExpress(HttpServletRequest request,
            String subscribe_status, String logistics_code, String nu,
            String monitoring_status)
    {
        // 分页查询
        PageView pageView = null;

        Map<String, Object> params = new HashMap<String, Object>();

        String pageIndex = request.getParameter("pageIndex");
        if (StringUtil.isNotEmpty(pageIndex))
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);

            params = (Map<String, Object>) request.getSession().getAttribute(
                    SESSION_KEY_SEARCH_CONDITION);

        } else
        {
            pageView = new PageView(1);

            // 订阅状态
            params.put("subscribe_status", subscribe_status);
            // 物流跟踪号
            params.put("logistics_code", logistics_code);
            // 快递单号
            params.put("nu", nu);
            // 监控状态
            params.put("monitoring_status", monitoring_status);

            request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION,
                    params);

        }

        params.put("pageView", pageView);
        // 快递列表
        List<ExpressInfo> expressInfoList = expressInfoService
                .queryAllExpressInfo(params);

        request.setAttribute("expressInfoList", expressInfoList);
        request.setAttribute("pageView", pageView);

        // 查询条件保持
        request.setAttribute("params", params);

        return "back/expressList";
    }

    /**
     * 快递信息详情
     * 
     * @param request
     * @param express_info_id
     * @return
     */
    @RequestMapping("/expressDetail")
    public String expressDetail(HttpServletRequest request,
            String express_info_id)
    {
        // 快递列表
        ExpressInfo expressInfo = expressInfoService
                .queryExpressInfoById(Integer.parseInt(express_info_id));

        String dataString = expressInfo.getData();

        if (StringUtil.isNotEmpty(dataString))
        {
            // 时间地点跟踪进度
            List<ResultItem> resultList = JacksonHelper.fromJSONList(
                    dataString, ResultItem.class);
            request.setAttribute("resultList", resultList);
            // 跟踪数据存在
            request.setAttribute("exist", 1);
        }
        request.setAttribute("expressInfo", expressInfo);

        return "back/expressDetail";
    }

    /**
     * 重新订阅
     * 
     * @param request
     * @param Pkg
     * @return
     */
    @RequestMapping("/restart")
    @ResponseBody
    public Map<String, Object> restart(HttpServletRequest request,
            String express_info_id)
    {

        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            // 快递列表
            ExpressInfo expressInfo = expressInfoService
                    .queryExpressInfoById(Integer.parseInt(express_info_id));

            Pkg pkg = packageService.queryPackageByEmsCode(expressInfo.getNu());

            TaskResponse resp = PostToKuaidi100.sendJson(COMPANY,
                    expressInfo.getNu(), "", city(pkg.getRegion()));

            if (resp.getResult()
                    || REPEAT_SUBSCRIPTION.equals(resp.getReturnCode()))
            {

                expressInfo
                        .setSubscribe_status(ExpressInfo.SUBSCRIBE_STATUS_SUCCESS);
                expressInfo.setReceive_time(new Timestamp(System
                        .currentTimeMillis()));

                expressInfoService.updatesubscribeStatus(expressInfo);
            } else
            {
                map.put("result", false);
                map.put("message", "订阅失败！");
                return map;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            map.put("result", false);
            map.put("message", "地址信息有误！");
            return map;
        }
        map.put("result", true);
        map.put("message", "订阅成功！");

        return map;
    }

    /**
     * 修改订单号
     * 
     * @param request
     * @param Pkg
     * @return
     */
    @RequestMapping("/modify")
    public String modify(HttpServletRequest request, String express_info_id)
    {
        // 快递列表
        ExpressInfo expressInfo = expressInfoService
                .queryExpressInfoById(Integer.parseInt(express_info_id));

        request.setAttribute("expressInfo", expressInfo);
        return "back/expressModify";
    }

    /**
     * 订单号重复检查
     * 
     * @param request
     * @param nu
     * @return
     */
    @RequestMapping("/checkEmsCode")
    @ResponseBody
    public String checkEmsCode(HttpServletRequest request, String newEmsCode)
    {

        String checkResult = EMS_CODE_EXIST;

        Pkg pkg = packageService.queryPackageByEmsCode(newEmsCode);
        // 订单号不存在
        if (pkg == null)
        {
            checkResult = EMS_CODE_NOTEXIST;
        }

        return checkResult;
    }

    /**
     * 更新快递单号
     * 
     * @param request
     * @param oldEmsCode
     * @param newEmsCode
     * @return
     */
    @RequestMapping("/updateEmsCode")
    @ResponseBody
    public String updateEmsCode(HttpServletRequest request,String logistics_code, String newEmsCode, String express_info_id,String com)
    {
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("logistics_code", logistics_code);
        params.put("new_ems_code", newEmsCode);
        params.put("express_info_id", express_info_id);
        params.put("com", com);
        
        
        
        Pkg pkg = packageService.queryPackageByEmsCode(newEmsCode);
        // 订单号不存在
        if (pkg == null)
        {
            
            // 更新快递单号(包裹表，快递信息表)
            User user = (User) request.getSession().getAttribute("back_user");
            packageService.updateEmsCode(params, user);
            return null;
        }

        return "1";
    }

    /**
     * 解析省市
     * 
     * @param request
     * @param jsonString
     * @return
     */
    private static String city(String jsonString)
    {
        if (StringUtil.isEmpty(jsonString))
        {
            return "";
        }

        List<String> aList = new ArrayList<String>();
        try
        {
            aList = JSONUtil.readValueFromJson(jsonString, "name");
        } catch (Exception e)
        {
            e.printStackTrace();

            return "";
        }
        StringBuffer address = new StringBuffer("");

        int index = 0;

        for (String city : aList)
        {
            // 只要省和市
            if (index > 1)
            {
                break;
            }
            address.append(city);
            index++;
        }

        return address.toString().replaceAll("\"", "");
    }

}
