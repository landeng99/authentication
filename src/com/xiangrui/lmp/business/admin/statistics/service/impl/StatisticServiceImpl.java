package com.xiangrui.lmp.business.admin.statistics.service.impl;

import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.ALL_PKG;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.BUSINESS_PKG;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.GENERAL_PKG;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.JOIN;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.MEMBER;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.PKG_DELIVER;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.SHARE;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.THISMONTH;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.THISWEEK;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.THISYEAR;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.TIMEPATTERN;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.TODAY;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.YESTERDAY;
import static java.util.Calendar.DATE;
import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.statistics.mapper.StatisticMapper;
import com.xiangrui.lmp.business.admin.statistics.service.StatisticService;
import com.xiangrui.lmp.business.admin.statistics.vo.DeliverStatistic;
import com.xiangrui.lmp.business.admin.statistics.vo.GeneralPkgStatistic;
import com.xiangrui.lmp.business.admin.statistics.vo.ShareStatistic;
import com.xiangrui.lmp.business.admin.statistics.vo.Statistic;
import com.xiangrui.lmp.business.admin.statistics.vo.Statistic.StatisticModel;
import com.xiangrui.lmp.business.admin.statistics.vo.Statistic.TimeRange;
import com.xiangrui.lmp.business.admin.statistics.vo.UserStatistic;
import com.xiangrui.lmp.util.DateFormatUtils;

@Service("statisticService")
public class StatisticServiceImpl implements StatisticService
{

    @Autowired
    private StatisticMapper statisticMapper;

    /**
     * 根据传入参数统计信息
     * 
     * @param type
     * @param statisticModel
     * @param calendar
     * @param restrictions
     * @return
     */
    public List<Statistic> statisticByModel(int type,
            StatisticModel statisticModel, Integer year, Integer month,
            Integer day, Map<String, Object> restrictions)
    {

        Calendar calendar;
        if (month == null)
        {
            month = 0;
        } else
        {
            month = month - 1;
        }
        if (day == null)
        {
            day = 1;
        }
        if (year == null)
        {
            calendar = new GregorianCalendar();
        } else
        {
            calendar = new GregorianCalendar(year, month, day);
        }

        return statisticByModel(type, statisticModel, calendar, restrictions);

    }

    /**
     * 根据传入参数统计信息
     * 
     * @param type
     * @param statisticModel
     * @param calendar
     * @param restrictions
     * @return
     */
    private List<Statistic> statisticByModel(int type,
            StatisticModel statisticModel, Calendar calendar,
            Map<String, Object> restrictions)
    {
        switch (statisticModel)
        {
        case day:
        {
            return statisticByDay(type, calendar, restrictions);
        }
        case week:
        {
            return statisticByWeek(type, calendar, restrictions);
        }
        case month:
        {
            return statisticByMonth(type, calendar, restrictions);
        }
        case year:
        {
            return statisticByYear(type, calendar, restrictions);
        }
        }
        return new ArrayList<Statistic>();
    }

    /**
     * 按日统计
     * 
     * @param type
     *            统计类型:会员注册1、普通用户包裹统计2、同行包裹统计3、 包裹状态统计4、未妥投统计5、晒单统计6
     * @param calendar
     *            统计时间
     * @param restrictions
     *            参数
     * @return
     */
    private List<Statistic> statisticByDay(int type, Calendar calendar,
            Map<String, Object> restrictions)
    {

        calendar = clearTime(calendar);
        List<Statistic> list = new ArrayList<Statistic>();
        Date begin, end;
        Calendar clone = (Calendar) calendar.clone();

        // 计算时间段内总数
        Statistic statisticTotal = statistic(type, getTimeRange(TODAY, clone),
                restrictions);
        for (int i = 0; i < 24; i++)
        {
            calendar.set(HOUR_OF_DAY, i);
            begin = calendar.getTime();
            calendar.set(HOUR_OF_DAY, i + 1);
            end = calendar.getTime();

            // 计算时间间隔内的总数
            Statistic statistic = statistic(type,
                    TimeRange.getInstance(begin, end), restrictions);
            statistic.setDescription(format(i));
            statistic.setTotal(statisticTotal.getCount());
            list.add(statistic);
        }

        return list;
    }

    /**
     * 按周统计
     * 
     * @param type
     *            统计类型:会员注册1、普通用户包裹统计2、同行包裹统计3、 包裹状态统计4、未妥投统计5、晒单统计6
     * @param calendar
     *            统计时间
     * @param restrictions
     *            参数
     * @return
     */
    private List<Statistic> statisticByWeek(int type, Calendar calendar,
            Map<String, Object> restrictions)
    {
        calendar = clearTime(calendar);
        List<Statistic> list = new ArrayList<Statistic>();
        Date begin, end;
        Calendar clone = (Calendar) calendar.clone();

        // 计算时间段内总数
        Statistic statisticTotal = statistic(type,
                getTimeRange(THISWEEK, clone), restrictions);
        for (int i = 1; i <= 7; i++)
        {
            calendar.set(DAY_OF_WEEK, i);
            begin = calendar.getTime();
            if (i == 7)
            {
                calendar.add(DAY_OF_WEEK, 1);
            } else
            {
                calendar.set(DAY_OF_WEEK, i + 1);
            }

            end = calendar.getTime();
            // 计算时间间隔内的总数
            Statistic statistic = statistic(type,
                    TimeRange.getInstance(begin, end), restrictions);
            statistic.setDescription("星期" + formatWeek(i-1));
            statistic.setTotal(statisticTotal.getCount());
            list.add(statistic);
        }
        return list;

    }

    /**
     * 按月统计
     * 
     * @param type
     *            统计类型:会员注册1、普通用户包裹统计2、同行包裹统计3、 包裹状态统计4、未妥投统计5、晒单统计6
     * @param calendar
     *            统计时间
     * @param restrictions
     *            参数
     * @return
     */
    private List<Statistic> statisticByMonth(int type, Calendar calendar,
            Map<String, Object> restrictions)
    {

        List<Statistic> list = new ArrayList<Statistic>();
        int year = getYear(calendar);
        int month = getMonth(calendar);
        int day = 1, days;
        Date begin, end;
        calendar = new GregorianCalendar(year, month, day);

        // 计算时间段内总数
        Statistic statisticTotal = statistic(type,
                getTimeRange(THISMONTH, (Calendar) calendar.clone()),
                restrictions);
        Calendar clone = (Calendar) calendar.clone();
        clone.set(MONTH, month + 1);
        end = clone.getTime();
        clone.add(DATE, -1);
        days = getDay(clone);

        // 统计当月每天
        for (int i = 1; i <= days; i++)
        {
            calendar.set(DATE, i);
            begin = calendar.getTime();
            calendar.set(DATE, i + 1);
            end = calendar.getTime();

            // 计算时间间隔内的总数
            Statistic statistic = statistic(type,
                    TimeRange.getInstance(begin, end), restrictions);
            statistic.setDescription(String.valueOf(i) + "日");
            statistic.setTotal(statisticTotal.getCount());
            list.add(statistic);
        }
        return list;
    }

    /**
     * 按年统计
     * 
     * @param type
     *            统计类型:会员注册1、普通用户包裹统计2、同行包裹统计3、 包裹状态统计4、未妥投统计5、晒单统计6
     * @param calendar
     *            统计时间
     * @param restrictions
     *            参数
     * @return
     */
    private List<Statistic> statisticByYear(int type, Calendar calendar,
            Map<String, Object> restrictions)
    {
        List<Statistic> list = new ArrayList<Statistic>();
        int year = getYear(calendar);
        int day = 1, month = 0;
        Date begin, end;
        calendar = new GregorianCalendar(year, month, day);
        Calendar clone = (Calendar) calendar.clone();
        Statistic statisticTotal = statistic(type,
                getTimeRange(THISYEAR, clone), restrictions);
        // 统计当年每月
        for (int i = 0; i < 12; i++)
        {
            calendar.set(MONTH, i);
            begin = calendar.getTime();
            calendar.set(MONTH, i + 1);
            end = calendar.getTime();

            // 计算时间间隔内的总数
            Statistic statistic = statistic(type,
                    TimeRange.getInstance(begin, end), restrictions);
            statistic.setDescription(String.valueOf(i + 1) + "月");
            statistic.setTotal(statisticTotal.getCount());
            list.add(statistic);
        }
        return list;
    }

    /**
     * 根据各个类型统计信息
     * 
     * @param type
     * @param timeRange
     * @param restrictions
     * @return
     */
    private Statistic statistic(int type, TimeRange timeRange,
            Map<String, Object> restrictions)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("begin", timeRange.getBegin());
        params.put("end", timeRange.getEnd());

        switch (type)
        {

        // 会员注册统计
        case MEMBER:
        {
            long count = statisticMapper.memberStatistic(params);
            UserStatistic statistic = new UserStatistic();
            statistic.setCount(count);
            return statistic;
        }

        // 普通用户包裹统计
        case GENERAL_PKG:
        {
            long count = statisticMapper.generalPkgStatistic(params);
            GeneralPkgStatistic statistic = new GeneralPkgStatistic();
            statistic.setCount(count);
            return statistic;
        }

        // 同行包裹统计
        case BUSINESS_PKG:
        {
            long count = statisticMapper.businessPkgStatistic(params);
            GeneralPkgStatistic statistic = new GeneralPkgStatistic();
            statistic.setCount(count);
            return statistic;
        }

        // 所有包裹
        case ALL_PKG:
        {
            long count = statisticMapper.allPkgStatistic(params);
            GeneralPkgStatistic statistic = new GeneralPkgStatistic();
            statistic.setCount(count);
            return statistic;
        }

        // 包裹状态统计
        case PKG_DELIVER:
        {
            String state=(String)restrictions.get("state");
            params.put("state", state);
            long count = statisticMapper.deliverStatistic(params);
            DeliverStatistic statistic = new DeliverStatistic();
            statistic.setCount(count);
            return statistic;
        }

        // 晒单统计
        case SHARE:
        {
            long count = statisticMapper.shareStatistic(params);
            ShareStatistic statistic = new ShareStatistic();
            statistic.setCount(count);
            return statistic;
        }
        }
        return null;
    }

    /**
     * 重置时间
     * 
     * @param calendar
     * @return
     */
    private Calendar clearTime(Calendar calendar)
    {
        return new GregorianCalendar(getYear(calendar), getMonth(calendar),
                getDay(calendar));
    }

    /**
     * 格式化时间yyyy-mm-dd hh:mm:ss
     * 
     * @param time
     * @return
     */
    private String format(int time)
    {
        Calendar calendar = clearTime(new GregorianCalendar());
        calendar.set(HOUR_OF_DAY, time);
        String begin, end;
        begin = DateFormatUtils.format(calendar.getTime(), TIMEPATTERN);
        calendar.add(HOUR_OF_DAY, 1);
        end = DateFormatUtils.format(calendar.getTime(), TIMEPATTERN);
        return begin + JOIN + end;
    }

    private String formatWeek(int week)
    {
        String[] weeks = { "一", "二", "三", "四", "五", "六", "日" };

        return weeks[week];

    }

    /**
     * 获取年
     * 
     * @param calendar
     * @return
     */
    private int getYear(Calendar calendar)
    {
        return calendar.get(YEAR);
    }

    /**
     * 获取月
     * 
     * @param calendar
     * @return
     */
    private int getMonth(Calendar calendar)
    {
        return calendar.get(MONTH);
    }

    /**
     * 获取日
     * 
     * @param calendar
     * @return
     */
    private int getDay(Calendar calendar)
    {
        return calendar.get(DATE);
    }

    /**
     * 获取今日、昨日、本周、本月时间范围
     * 
     * @param type
     * @param calendar
     * @return
     */
    private TimeRange getTimeRange(int type, Calendar calendar)
    {
        calendar = clearTime(calendar);
        Date begin, end;
        switch (type)
        {
        case TODAY:
        {
            begin = calendar.getTime();
            calendar.add(DATE, 1);
            end = calendar.getTime();
            return TimeRange.getInstance(begin, end);
        }
        case YESTERDAY:
        {
            calendar.add(DATE, -1);
            begin = calendar.getTime();
            calendar.add(DATE, 1);
            end = calendar.getTime();
            return TimeRange.getInstance(begin, end);
        }
        case THISWEEK:
        {
            flush(calendar);
            calendar.set(DAY_OF_WEEK, 1);
            begin = calendar.getTime();
            calendar.add(DAY_OF_WEEK, 7);
            end = calendar.getTime();
            return TimeRange.getInstance(begin, end);
        }
        case THISMONTH:
        {
            int month = calendar.get(MONTH);
            calendar.set(DATE, 1);
            begin = calendar.getTime();
            calendar.set(MONTH, month + 1);
            end = calendar.getTime();
            return TimeRange.getInstance(begin, end);
        }
        case THISYEAR:
        {
            int year = calendar.get(YEAR);
            calendar.set(MONTH, 0);
            calendar.set(DATE, 1);
            begin = calendar.getTime();
            calendar.set(YEAR, year + 1);
            end = calendar.getTime();
            return TimeRange.getInstance(begin, end);
        }
        }
        return null;
    }

    private void flush(Calendar calendar)
    {
        calendar.getTime();
    }

}
