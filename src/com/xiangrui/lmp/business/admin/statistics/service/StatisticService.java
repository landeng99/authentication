package com.xiangrui.lmp.business.admin.statistics.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.statistics.vo.Statistic;
import com.xiangrui.lmp.business.admin.statistics.vo.Statistic.StatisticModel;

public interface StatisticService {
	 List<Statistic> statisticByModel(int type,
			StatisticModel statisticModel, Integer year, Integer month,
			Integer day, Map<String, Object> restrictions);
	
	
}
