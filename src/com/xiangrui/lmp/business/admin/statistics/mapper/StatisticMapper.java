package com.xiangrui.lmp.business.admin.statistics.mapper;

import java.util.Map;

public interface StatisticMapper
{

    /**
     * 总用户数
     * 
     * @param timeRange
     * @return
     */
    long memberStatistic(Map<String, Object> timeRange);

    /**
     * 直客包总裹数
     * 
     * @param timeRange
     * @return
     */
    long generalPkgStatistic(Map<String, Object> timeRange);

    /**
     * 同行包裹总数
     * 
     * @param timeRange
     * @return
     */
    long businessPkgStatistic(Map<String, Object> timeRange);
    
    
    /**所有包裹统计
     * @param timeRange
     * @return
     */
    long allPkgStatistic(Map<String, Object> timeRange);
    

    /**
     * 晒单总数
     * 
     * @param timeRange
     * @return
     */
    long shareStatistic(Map<String, Object> timeRange);
    
    
    /**
     * 投递统计
     * @param timeRange
     * @return
     */
    long deliverStatistic(Map<String, Object> timeRange);

}
