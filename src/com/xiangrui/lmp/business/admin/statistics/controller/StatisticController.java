package com.xiangrui.lmp.business.admin.statistics.controller;

import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.GENERAL_PKG;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.BUSINESS_PKG;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.ALL_PKG;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.MEMBER;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.SHARE;
import static com.xiangrui.lmp.business.admin.statistics.vo.Statistic.PKG_DELIVER;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.statistics.service.StatisticService;
import com.xiangrui.lmp.business.admin.statistics.vo.Statistic;
import com.xiangrui.lmp.business.admin.statistics.vo.Statistic.StatisticModel;

@Controller
@RequestMapping("/admin/statistic")
public class StatisticController
{

    /**
     * 普通用户
     */
    private static final int USER_TYPE_GENERAL = 1;

    /**
     * 同行快递用户
     */
    private static final int USER_TYPE_BUSINESS = 2;

    /**
     * 
     */
    @Autowired
    private StatisticService statisticService;

    /**
     * 用户统计
     * 
     * @param req
     * @param queryModel
     *            查询模型
     * @param year
     *            年
     * @param month
     *            月
     * @param day
     *            日
     * @return
     */
    @RequestMapping("/userlist")
    public String userStatistic(HttpServletRequest req, String queryModel,
            Integer year, Integer month, Integer day)
    {

        // 获取时间模型
        StatisticModel statisticModel = getStatisticModel(queryModel);

        List<Statistic> list = statisticService.statisticByModel(MEMBER,
                statisticModel, year, month, day, null);

        req.setAttribute("statisticList", list);
        req.setAttribute("statisticModel", statisticModel.name());
        req.setAttribute("year", year);
        req.setAttribute("month", month);
        req.setAttribute("day", day);

        return "/back/user_statistic";
    }

    /**
     * 晒单统计
     * 
     * @param req
     * @param queryModel
     *            查询模型
     * @param year
     *            年
     * @param month
     *            月
     * @param day
     *            日
     * @return
     */
    @RequestMapping("/sharelist")
    public String shareStatistic(HttpServletRequest req, String queryModel,
            Integer year, Integer month, Integer day)
    {

        // 获取时间模型
        StatisticModel statisticModel = getStatisticModel(queryModel);

        List<Statistic> list = statisticService.statisticByModel(SHARE,
                statisticModel, year, month, day, null);

        req.setAttribute("statisticList", list);
        req.setAttribute("statisticModel", statisticModel.name());
        req.setAttribute("year", year);
        req.setAttribute("month", month);
        req.setAttribute("day", day);

        return "/back/share_statistic";
    }

    /**
     * 包裹统计
     * 
     * @param req
     * @param queryModel
     *            查询模型
     * @param year
     *            年
     * @param month
     *            月
     * @param day
     *            日
     * @return
     */
    @RequestMapping("/pkglist")
    public String pkgStatistic(HttpServletRequest req, String queryModel,
            Integer year, Integer month, Integer day)
    {

        // 获取时间模型
        StatisticModel statisticModel = getStatisticModel(queryModel);

        // 用户类型
        String userType = req.getParameter("userType");

        // 查询类型
        int type = ALL_PKG;

        // 当用户类型不为空，则统计不同类型的用户
        if (null != userType && !"".equals(userType))
        {
            // 根据包裹查询类型2：表示普通用户包裹统计，3快递物流包裹统计
            if (String.valueOf(USER_TYPE_GENERAL).equals(userType))
            {
                type = GENERAL_PKG;
            } else if (String.valueOf(USER_TYPE_BUSINESS).equals(userType))
            {
                type = BUSINESS_PKG;
            }

        }

        // 查询包裹
        List<Statistic> list = statisticService.statisticByModel(type,
                statisticModel, year, month, day, null);

        req.setAttribute("statisticList", list);
        req.setAttribute("statisticModel", statisticModel.name());
        req.setAttribute("year", year);
        req.setAttribute("month", month);
        req.setAttribute("day", day);
        req.setAttribute("userType", userType);

        return "/back/pkg_statistic";
    }

    /**
     * 投递统计
     * 
     * @param req
     * @param queryModel
     *            查询模型
     * @param year
     *            年
     * @param month
     *            月
     * @param day
     *            日
     * @return
     */
    @RequestMapping("/deliverlist")
    public String deliverStatistic(HttpServletRequest req, String queryModel,
            Integer year, Integer month, Integer day)
    {

        // 获取时间模型
        StatisticModel statisticModel = getStatisticModel(queryModel);

        // 根据投递状态查询
        Map<String, Object> restrictions = new HashMap<String, Object>();
        String state = req.getParameter("state");
        restrictions.put("state", state);

        // 查询包裹
        List<Statistic> list = statisticService.statisticByModel(PKG_DELIVER,
                statisticModel, year, month, day, restrictions);

        req.setAttribute("statisticList", list);
        req.setAttribute("statisticModel", statisticModel.name());
        req.setAttribute("year", year);
        req.setAttribute("month", month);
        req.setAttribute("day", day);
        req.setAttribute("state", state);

        return "/back/deliver_statistic";
    }

    /**
     * 包裹查询模型
     * 
     * @param queryModel
     * @return
     */
    private StatisticModel getStatisticModel(String queryModel)
    {

        if (!StringUtils.isBlank(queryModel))
        {
            return StatisticModel.valueOf(queryModel);
        }
        return StatisticModel.year;
    }
}
