package com.xiangrui.lmp.business.admin.statistics.vo;

public class UserStatistic extends Statistic{
	
	
	/**
	 * 活跃用户数
	 */
	private long activeUserCount;

	public long getActiveUserCount() {
		return activeUserCount;
	}

	public void setActiveUserCount(long activeUserCount) {
		this.activeUserCount = activeUserCount;
	}
	
}
