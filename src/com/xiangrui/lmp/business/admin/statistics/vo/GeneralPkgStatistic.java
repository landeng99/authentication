package com.xiangrui.lmp.business.admin.statistics.vo;

public class GeneralPkgStatistic extends Statistic
{

    /**
     * 包裹总数
     */
    private int pkgTotal;

    /**
     * 金额
     */
    private int costTotal;

    public int getPkgTotal()
    {
        return pkgTotal;
    }

    public void setPkgTotal(int pkgTotal)
    {
        this.pkgTotal = pkgTotal;
    }

    public int getCostTotal()
    {
        return costTotal;
    }

    public void setCostTotal(int costTotal)
    {
        this.costTotal = costTotal;
    }

}
