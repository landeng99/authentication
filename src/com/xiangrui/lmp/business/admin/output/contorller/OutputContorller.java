package com.xiangrui.lmp.business.admin.output.contorller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.output.service.OutputService;
import com.xiangrui.lmp.business.admin.output.vo.Output;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pickorder.service.PickOrderService;
import com.xiangrui.lmp.business.admin.pickorder.vo.PickOrder;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.util.BreadcrumbUtil;
import com.xiangrui.lmp.util.PageView;

/**
 * 出库管理
 * 
 * @author Will
 *
 */
@Controller
@RequestMapping("/admin/output")
public class OutputContorller
{

	@Autowired
	private OutputService outputService;
	@Autowired
	private OverseasAddressService overseasAddressService;
	@Autowired
	private SeaportService seaportService;
	@Autowired
	private PickOrderService pickOrderService;

	/**
	 * 分页查询所有出库
	 * 
	 * @return
	 */
	@RequestMapping("/queryAll")
	public String outputPage(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (!"".equals(pageIndex) && pageIndex != null)
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);

		List<Output> list = outputService.queryAll(params);
		if (list != null && list.size() > 0)
		{
			for (Output outpput : list)
			{
				String seaportDisplay = "";
				String seaportIdListStr = StringUtils.trimToNull(outpput.getSeaport_id_list());
				if (seaportIdListStr != null)
				{
					String[] array = seaportIdListStr.split(",");
					if (array != null && array.length > 0)
					{
						for (String seaportId : array)
						{
							Seaport seaport = seaportService.querySeaportBySid(Integer.parseInt(seaportId));
							if (seaport != null)
							{
								seaportDisplay += seaport.getSname() + "/";
							}
						}
					}
				}
				if (seaportDisplay.contains("/"))
				{
					seaportDisplay = StringUtils.substringBeforeLast(seaportDisplay, "/");
				}
				outpput.setSeaportDisplay(seaportDisplay);
			}
		}
		request.setAttribute("pageView", pageView);
		request.setAttribute("outputLists", list);
		return "back/outputList";
	}

	/**
	 * 前往新增出库
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/outputAdd")
	public String outputAdd(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			List<OverseasAddress> overseasAddressList = overseasAddressService.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
		}
		List<Seaport> seaportList = seaportService.queryAll();
		request.setAttribute("output_sn", outputService.createSNCode());
		request.setAttribute("seaportList", seaportList);
		return "back/outputAdd";
	}

	/**
	 * 保存出库
	 * 
	 * @param request
	 * @param output
	 * @return
	 */
	@RequestMapping("/saveOutput")
	public String saveOutput(HttpServletRequest request, Output output)
	{
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			output.setCreate_user_id(user.getUser_id());
		}
		outputService.addOutput(output);
		return outputDetail(request, output.getOutput_sn());
	}

	/**
	 * 前往编辑出库
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/outputDetail")
	public String outputDetail(HttpServletRequest request, String output_sn)
	{
		String output_id = request.getParameter("output_id");
		List<String> existList = new ArrayList<String>();
		List<PickOrder> pickOrderList = new ArrayList<PickOrder>();
		Output output = outputService.queryByOutputSN(output_sn);
		if(output==null)
		{
			output=outputService.queryOutputBySeqId(Integer.parseInt(output_id));
		}
		String pageIndexStr = request.getParameter("pageIndex");
		PageView pageView = null;

		if (null != pageIndexStr && !"".equals(pageIndexStr))
		{
			int pageIndex = Integer.valueOf(pageIndexStr);
			pageView = new PageView(pageIndex);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pageView", pageView);
		if (output != null)
		{
			String seaportDisplay = "";
			String seaportIdListStr = StringUtils.trimToNull(output.getSeaport_id_list());
			if (seaportIdListStr != null)
			{
				String[] array = seaportIdListStr.split(",");
				if (array != null && array.length > 0)
				{
					for (String seaportId : array)
					{
						Seaport seaport = seaportService.querySeaportBySid(Integer.parseInt(seaportId));
						if (seaport != null)
						{
							seaportDisplay += seaport.getSname() + "&nbsp;&nbsp;";
							existList.add(seaportId);
						}
					}
				}
			}
			output.setSeaportDisplay(seaportDisplay);
			params.put("output_id", "" + output.getSeq_id());
			pickOrderList = pickOrderService.queryByMapOutput(params);
		}
		List<Seaport> seaportList = seaportService.queryAll();
		List<Seaport> leavesSaportList = new ArrayList<Seaport>();
		if (seaportList != null && seaportList.size() > 0)
		{
			for (Seaport seaport : seaportList)
			{
				if (!existList.contains("" + seaport.getSid()))
				{
					leavesSaportList.add(seaport);
				}
			}
		}
		seaportList.removeAll(existList);
		request.setAttribute("output", output);
		request.setAttribute("create_time", output.getCreate_time());
		request.setAttribute("seaportDisplay", output.getSeaportDisplay());
		request.setAttribute("seaportList", leavesSaportList);
		request.setAttribute("pickOrderList", pickOrderList);
		request.setAttribute("pageView", pageView);
		BreadcrumbUtil.push(request);
		return "back/outputDetail";
	}

	/**
	 * 添加口岸
	 * 
	 * @param request
	 * @param output
	 * @return
	 */
	@RequestMapping("/addSeaport")
	public String addSeaport(HttpServletRequest request, Integer seq_id, String seaportListStr)
	{
		Output output = outputService.queryOutputBySeqId(seq_id);

		if (output != null)
		{
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null)
			{

				String existSeaportIdList = output.getSeaport_id_list();

				output.setSeaport_id_list(existSeaportIdList + seaportListStr);
				outputService.updateOutput(output);
				// 添加新的提单和托盘
				String[] array = seaportListStr.split(",");
				if (array != null && array.length > 0)
				{
					for (String seaportId : array)
					{
						Seaport seaport = seaportService.querySeaportBySid(Integer.parseInt(seaportId));
						if (seaport != null)
						{
							outputService.addPickorderAndPalletByOutputIdAndUserId(seq_id, seaport.getSid(), user.getUser_id(), output.getOsaddr_id());
						}
					}
				}
			}
		}
		return outputDetail(request, output.getOutput_sn());
	}

	/**
	 * 设置自动打印
	 * 
	 * @param request
	 * @param output
	 * @return
	 */
	@RequestMapping("/setAutoPrint")
	public String setAutoPrint(HttpServletRequest request, Integer seqId, Integer autoPrint)
	{
		String message = "操作成功！";
		Output output = outputService.queryOutputBySeqId(seqId);
		if (output != null)
		{
			output.setAuto_print(autoPrint);
			outputService.updateOutput(output);
		}
		return message;
	}

	@RequestMapping("/deleteOutput")
	@ResponseBody
	public Map<String, String> deleteOutput(HttpServletRequest request, Integer id)
	{
		Map<String, String> result = new HashMap<String, String>();
		String flag = "F";// 默认删除操作失败
		String message = "删除操作失败！";
		if (outputService.countPickorderByOutputId(id) == 0)
		{
			outputService.deleteOutput(id);
			flag = "S";//
			message = "删除操作成功！";
		}
		result.put("flag", flag);
		result.put("message", message);
		return result;
	}
	
	@RequestMapping("/queryPickOrderByOutputId")
	@ResponseBody
	public Map<String, Object> queryPickOrderByOutputId(HttpServletRequest request, Integer output_id)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("output_id", output_id);
		List<PickOrder> pickOrderList = pickOrderService.queryByMapOutput(params);
		result.put("pickOrderList", pickOrderList);
		return result;
	}
}
