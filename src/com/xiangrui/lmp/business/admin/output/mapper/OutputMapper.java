package com.xiangrui.lmp.business.admin.output.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.output.vo.Output;

public interface OutputMapper
{

	/**
	 * 添加出库信息
	 * 
	 * @param output
	 */
	int addOutput(Output output);

	/**
	 * 修改出库信息
	 * 
	 * @param Notice
	 */
	void updateOutput(Output output);

	/**
	 * 删除出库信息
	 * 
	 * @param id
	 */
	void deleteOutput(int seq_id);

	Output queryOutputBySeqId(int seq_id);
	/**
	 * 分页查询列表
	 * @param params
	 * @return
	 */
	List<Output> queryAll(Map<String, Object> params);
	
	/**
	 * 查询当天新建出库记录数，协助生成出库编号
	 * @return
	 */
	int countTodayOutputRecord();
	/**
	 * 通过出库ID来统计提单数
	 * @param output_id
	 * @return
	 */
	int countPickorderByOutputId(int output_id);
	
	Output queryByOutputSN(String output_sn);
}
