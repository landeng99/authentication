package com.xiangrui.lmp.business.admin.output.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.output.mapper.OutputMapper;
import com.xiangrui.lmp.business.admin.output.service.OutputService;
import com.xiangrui.lmp.business.admin.output.vo.Output;
import com.xiangrui.lmp.business.admin.pallet.service.PalletService;
import com.xiangrui.lmp.business.admin.pallet.vo.Pallet;
import com.xiangrui.lmp.business.admin.pickorder.service.PickOrderService;
import com.xiangrui.lmp.business.admin.pickorder.vo.PickOrder;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.IdentifierCreator;

@Service("outputService")
public class OutputServiceImpl implements OutputService
{
	@Autowired
	private OutputMapper outputMapper;
	@Autowired
	private PickOrderService pickOrderService;

	@Autowired
	private PalletService palletService;
	@Autowired
	private SeaportService seaportService;

	@Override
	public void addOutput(Output output)
	{
		output.setAuto_print(0);
		outputMapper.addOutput(output);
		int seqId =outputMapper.queryByOutputSN(output.getOutput_sn()).getSeq_id();
		// 添加新的提单和托盘
		if (seqId > 0)
		{
			String[] array = output.getSeaport_id_list().split(",");
			if (array != null && array.length > 0)
			{
				for (String seaportId : array)
				{
					Seaport seaport = seaportService.querySeaportBySid(Integer.parseInt(seaportId));
					if (seaport != null)
					{
						addPickorderAndPalletByOutputIdAndUserId(seqId, seaport.getSid(), output.getCreate_user_id(),output.getOsaddr_id());
					}
				}
			}
		}
	}

	@Override
	public void updateOutput(Output output)
	{
		outputMapper.updateOutput(output);
	}

	@Override
	public void deleteOutput(int id)
	{
		outputMapper.deleteOutput(id);
	}

	public Output queryOutputBySeqId(int seq_id)
	{
		return outputMapper.queryOutputBySeqId(seq_id);
	}

	/**
	 * 分页查询列表
	 * 
	 * @param params
	 * @return
	 */
	public List<Output> queryAll(Map<String, Object> params)
	{
		return outputMapper.queryAll(params);
	}

	/**
	 * 通过出库ID来统计提单数
	 * 
	 * @param output_id
	 * @return
	 */
	public int countPickorderByOutputId(int output_id)
	{
		return outputMapper.countPickorderByOutputId(output_id);
	}

	public void addPickorderAndPalletByOutputIdAndUserId(int outputId, int seaportId, int backUserId,int overseas_address_id)
	{
		PickOrder pickOrder = new PickOrder();
		String pick_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PICKORDER);
		Timestamp createTime = new Timestamp(System.currentTimeMillis());
		pickOrder.setPick_code(pick_code);
		pickOrder.setCreate_user(backUserId);
		pickOrder.setCreate_time(createTime);
		pickOrder.setStatus(PickOrder.PICKORDER_SENT_ALREADY);
		pickOrder.setSeaport_id(seaportId);
		pickOrder.setOutput_id(outputId);
		pickOrder.setOverseas_address_id(overseas_address_id);
		pickOrderService.save(pickOrder);
		int pickOrderId =pickOrderService.queryByPickCode(pick_code).getPick_id();
		if (pickOrderId > 0)
		{
			Pallet pallet = new Pallet();
			pallet.setCreate_user(backUserId);
			String pallet_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PALLET);
			pallet.setPallet_code(pallet_code);
			// 出库
			pallet.setStatus(Pallet.PALLET_SENT_ALREADY);
			pallet.setCreate_time(new Timestamp(System.currentTimeMillis()));
			pallet.setSeaport_id(seaportId);
			pallet.setPick_id(pickOrderId);
			pallet.setOutput_id(outputId);
			palletService.insert(pallet);
		}
	}
	
	public String createSNCode()
	{
		int todayCount = outputMapper.countTodayOutputRecord();
		String sn = DateUtil.getStringToday(new Date());
		if (todayCount < 10)
		{
			sn += "0";
		}
		sn += todayCount;
		return sn;
	}
	
	public Output queryByOutputSN(String output_sn)
	{
		return outputMapper.queryByOutputSN(output_sn);
	}
}
