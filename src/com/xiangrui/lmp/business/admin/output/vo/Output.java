package com.xiangrui.lmp.business.admin.output.vo;

import com.xiangrui.lmp.business.base.BaseOutput;

public class Output extends BaseOutput
{

	private static final long serialVersionUID = 1L;

	private String seaportDisplay;
	
	private String warehouse;

	public String getWarehouse()
	{
		return warehouse;
	}

	public void setWarehouse(String warehouse)
	{
		this.warehouse = warehouse;
	}

	public String getSeaportDisplay()
	{
		return seaportDisplay;
	}

	public void setSeaportDisplay(String seaportDisplay)
	{
		this.seaportDisplay = seaportDisplay;
	}
}
