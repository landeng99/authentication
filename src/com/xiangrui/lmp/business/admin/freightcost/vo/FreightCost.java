package com.xiangrui.lmp.business.admin.freightcost.vo;

import com.xiangrui.lmp.business.base.BaseFreightCost;

/**
 * 费用计算
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-13 下午4:09:41
 * </p>
 */
public class FreightCost extends BaseFreightCost
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private MemberRate memberRate;

    public MemberRate getMemberRate()
    {
        return memberRate;
    }

    public void setMemberRate(MemberRate memberRate)
    {
        this.memberRate = memberRate;
    }

}
