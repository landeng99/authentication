package com.xiangrui.lmp.business.admin.freightcost.vo;

import com.xiangrui.lmp.business.base.BaseMemberRate;

/**
 * 会员等级,专用于价格计算
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-16 下午12:10:43</p>
 */
public class MemberRate extends BaseMemberRate
{
    private static final long serialVersionUID = 1L;
}
