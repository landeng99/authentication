package com.xiangrui.lmp.business.admin.freightcost.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.freightcost.vo.FreightCost;

/**
 * 用户等级的价格计算方式
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-16 上午10:22:59
 * </p>
 */
public interface FreightCostMapper
{
    /**
     * 查询所有用户等级和计算价格的方式
     * 
     * @param parmas
     * @return
     */
    List<FreightCost> queryAll(Map<String, Object> parmas);

    /**
     * 分页查询
     * 
     * @param parmas
     * @return List
     */
    List<FreightCost> queryAllFreightCost(Map<String, Object> parmas);

    /**
     * 添加
     * 
     * @param freightCost
     * @return
     */
    void insertFreightCost(FreightCost freightCost);

    /**
     * 更新
     * 
     * @param freightCost
     * @return
     */
    void updateFreightCost(FreightCost freightCost);

    /**
     * 
     * id查询
     * 
     * @param freight_id
     * @return FreightCost
     */
    FreightCost queryFreightCostByFreightId(int freight_id);

    /**
     * 
     * 会员等级
     * 
     * @param rate_id
     * @return FreightCost
     */
    FreightCost queryFreightCostByRateId(int rate_id);

    /**
     * 
     * 用户积分查找运费
     * 
     * @param parmas
     * @return FreightCost
     */
    FreightCost queryFreightCostByIntegralMin(Map<String, Object> parmas);

}
