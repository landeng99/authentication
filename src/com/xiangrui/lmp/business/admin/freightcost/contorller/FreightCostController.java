package com.xiangrui.lmp.business.admin.freightcost.contorller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.freightcost.service.FreightCostService;
import com.xiangrui.lmp.business.admin.freightcost.vo.FreightCost;
import com.xiangrui.lmp.business.base.BaseFreightCost;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/freightCost")
public class FreightCostController {

	@Autowired
	private FreightCostService freightCostService;

	/**
	 * 单位重量费用列表
	 * 
	 * @return
	 */
	@RequestMapping("/freightCostList")
	public String freightCostList(HttpServletRequest request) {
		// 分页查询
		PageView pageView = null;
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex)) {
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		} else {
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("pageView", pageView);

		List<FreightCost> freightCostList = freightCostService.queryAllFreightCost(params);

		request.setAttribute("freightCostList", freightCostList);
		request.setAttribute("pageView", pageView);

		return "back/freightCostList";

	}

	/**
	 * 单位重量费用详情
	 * 
	 * @return
	 */
	@RequestMapping("/detail")
	public String freightCostDetail(HttpServletRequest request, String freight_id, String rate_name,
			String unit_price,String first_weight_price,String go_weight_price) {
		String temp = "";
		try {
			temp = new String(rate_name.getBytes("ISO-8859-1"), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("rate_name", rate_name);
		}
		request.setAttribute("freight_id", freight_id);
		request.setAttribute("rate_name", temp);
		request.setAttribute("unit_price", unit_price);
		request.setAttribute("first_weight_price", first_weight_price);
		request.setAttribute("go_weight_price", go_weight_price);
		return "back/freightCostDetail";
	}

	/**
	 * 单位重量费用编辑
	 * 
	 * @return
	 */
	@RequestMapping("/modify")
	public String freightCostModify(HttpServletRequest request, String freight_id, String rate_name, String unit_price,String first_weight_price,String go_weight_price,
			String rate_id) {
		String temp = "";
		try {
			temp = new String(rate_name.getBytes("ISO-8859-1"), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("rate_name", rate_name);
		}
		request.setAttribute("rate_name", temp);
		request.setAttribute("rate_id", rate_id);
		request.setAttribute("freight_id", freight_id);
		request.setAttribute("unit_price", unit_price);
		request.setAttribute("first_weight_price", first_weight_price);
		request.setAttribute("go_weight_price", go_weight_price);

		// 注销前台资费出的信息,前台刷新可以重新查询
		ServletContext applicationg = request.getSession().getServletContext();
		applicationg.setAttribute("TariffHomePageList", null);
		// 前台重量查询价格的会员有变化,需要清空会员信息
		applicationg.setAttribute("MemberRateIdHomePageList", null);
		return "back/updateFreightCost";
	}

	/**
	 * 修改保存
	 * 
	 * @return
	 */
	@RequestMapping("/update")
	@ResponseBody
	public String update(HttpServletRequest request, int freight_id, String rate_name, String unit_price,String first_weight_price,String go_weight_price,
			String rate_id) {
		// 有号码是修改个,么有是新增
		if (freight_id > 0) {
			FreightCost freightCost = new FreightCost();
			freightCost.setFreight_id(freight_id);
			freightCost = freightCostService.queryFreightCostByFreightId(freight_id);
			freightCost.setUnit_price(Float.parseFloat(unit_price));
			freightCost.setFirst_weight_price(Float.parseFloat(first_weight_price));
			freightCost.setGo_weight_price(Float.parseFloat(go_weight_price));
			freightCostService.updateFreightCost(freightCost);
		} else {
			FreightCost freightCost = new FreightCost();
			// 通过会员名称找到修改会员信息
			// 会员id保存
			freightCost.setRate_id(Integer.parseInt(rate_id));
			freightCost.setUnit_price(Float.parseFloat(unit_price));
			freightCost.setFirst_weight_price(Float.parseFloat(first_weight_price));
			freightCost.setGo_weight_price(Float.parseFloat(go_weight_price));
			// 新增
			freightCostService.insertFreightCost(freightCost);
		}

		// 注销前台资费出的信息,前台刷新可以重新查询
		ServletContext applicationg = request.getSession().getServletContext();
		applicationg.setAttribute("TariffHomePageList", null);
		// 前台重量查询价格的会员有变化,需要清空会员信息
		applicationg.setAttribute("MemberRateIdHomePageList", null);
		return "";
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	@RequestMapping("/add")
	public String add(HttpServletRequest request) {
		return "back/addFreightCost";
	}

	/**
	 * 会员等级重复
	 * 
	 * @return
	 */
	@RequestMapping("/check")
	@ResponseBody
	public String check(HttpServletRequest request, String rate_id) {

		String checkResult = BaseFreightCost.RATE_ID_EXIST;

		FreightCost freightCost = freightCostService.queryFreightCostByRateId(Integer.parseInt(rate_id));
		// 会员等级不存在
		if (freightCost == null) {
			checkResult = BaseFreightCost.RATE_ID_NOTEXIST;
		}

		return checkResult;

	}

	/**
	 * 添加保存
	 * 
	 * @return
	 */
	@RequestMapping("/insert")
	public String insert(HttpServletRequest request, FreightCost freightCost) {

		freightCostService.insertFreightCost(freightCost);

		// 注销前台资费出的信息,前台刷新可以重新查询
		ServletContext applicationg = request.getSession().getServletContext();
		applicationg.setAttribute("TariffHomePageList", null);
		// 前台重量查询价格的会员有变化,需要清空会员信息
		applicationg.setAttribute("MemberRateIdHomePageList", null);

		return "back/addFreightCost";
	}

}
