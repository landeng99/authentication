package com.xiangrui.lmp.business.admin.freightcost.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.freightcost.mapper.FreightCostMapper;
import com.xiangrui.lmp.business.admin.freightcost.service.FreightCostService;
import com.xiangrui.lmp.business.admin.freightcost.vo.FreightCost;

/**
 * 用户计算价格方式
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-16 上午10:24:58
 * </p>
 */
@Service("freightCostService")
public class FreightCostServiceImpl implements FreightCostService
{

    /**
     * 用户计算价格方式
     */
    @Autowired
    private FreightCostMapper freightCostMapper;

    @Override
    public List<FreightCost> queryAll(Map<String, Object> parmas)
    {
        return freightCostMapper.queryAll(parmas);
    }

    /**
     * 分页查询
     * 
     * @param parmas
     * @return List
     */
    @Override
    public List<FreightCost> queryAllFreightCost(Map<String, Object> parmas)
    {
        return freightCostMapper.queryAllFreightCost(parmas);
    }

    /**
     * 添加
     * 
     * @param freightCost
     * @return
     */
    @Override
    public void insertFreightCost(FreightCost freightCost)
    {
        freightCostMapper.insertFreightCost(freightCost);
    }

    /**
     * 更新
     * 
     * @param freightCost
     * @return
     */
    @Override
    public void updateFreightCost(FreightCost freightCost)
    {
        freightCostMapper.updateFreightCost(freightCost);
    }

    /**
     * id查询
     * 
     * @param freight_id
     * @return List
     */
    @Override
    public FreightCost queryFreightCostByFreightId(int freight_id)
    {
        return freightCostMapper.queryFreightCostByFreightId(freight_id);
    }

    /**
     * 
     * 会员等级
     * 
     * @param rate_id
     * @return FreightCost
     */
    @Override
    public FreightCost queryFreightCostByRateId(int rate_id)
    {
        return freightCostMapper.queryFreightCostByRateId(rate_id);
    }

    /**
     * 
     * 用户积分查找运费
     * 
     * @param parmas
     * @return FreightCost
     */
    @Override
    public FreightCost queryFreightCostByIntegralMin(Map<String, Object> parmas)
    {

        return freightCostMapper.queryFreightCostByIntegralMin(parmas);
    }
}
