package com.xiangrui.lmp.business.admin.userlog.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.userlog.vo.UserLog;

/**
 * 后台用户操作日志
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-23 上午10:35:37
 * </p>
 */
public interface UserLogService
{

    /**
     * 查询所有操作记录
     * 
     * @param paremt
     * @return
     */
    List<UserLog> queryAll(Map<String, Object> paremt);

    /**
     * 查询某个操作记录
     * 
     * @param logId
     * @return
     */
    UserLog queryId(int logId);

    /**
     * 修改操作记录
     * 
     * @param log
     */
    void update(UserLog log);

    /**
     * 添加操作记录
     * 
     * @param log
     */
    void add(UserLog log);

    /**
     * 添加日志,不用add等方式,aop跟踪处理变成死循环
     * 
     * @param log
     */
    void log(UserLog log);

    /**
     * 删除
     * 
     * @param log
     */
    void delete(UserLog log);

    /**
     * 清除在endTime时间以前的用户操作日志
     * @param log
     */
    void clear(UserLog log);

}
