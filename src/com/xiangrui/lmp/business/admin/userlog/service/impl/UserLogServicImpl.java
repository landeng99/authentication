package com.xiangrui.lmp.business.admin.userlog.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.userlog.mapper.UserLogMapper;
import com.xiangrui.lmp.business.admin.userlog.service.UserLogService;
import com.xiangrui.lmp.business.admin.userlog.vo.UserLog;

/**
 * 后台用户操作日志
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-23 上午10:40:50
 * </p>
 */
@Service("userLogService")
public class UserLogServicImpl implements UserLogService
{

    /**
     * log日志记录
     */
    Logger logger = Logger.getLogger(UserLogServicImpl.class);

    /**
     * 后台用户操作日志
     */
    @Autowired
    private UserLogMapper maaper;

    /**
     * 查询所有
     */
    @Override
    public List<UserLog> queryAll(Map<String, Object> paremt)
    {
        return maaper.queryAll(paremt);
    }

    /**
     * 查询某个
     */
    @Override
    public UserLog queryId(int logId)
    {
        return maaper.queryId(logId);
    }

    /**
     * 修改日志记录
     */
    @SystemServiceLog(description = "修改日志记录")
    @Override
    public void update(UserLog log)
    {
        maaper.update(log);
    }

    /**
     * 添加日志记录
     */
    @SystemServiceLog(description = "手动添加日志记录")
    @Override
    public void add(UserLog log)
    {
        maaper.add(log);
    }

    /**
     * 删除日志记录
     */
    @SystemServiceLog(description = "删除日志记录")
    @Override
    public void delete(UserLog log)
    {
        maaper.delete(log);
    }

    /**
     * 日志记录方法,本质就是add,只是与add分开,不让aop监控到
     * 
     * @param log
     */
    @Override
    public void log(UserLog log)
    {
        logger.info("UserLogServicImpl log start");
        maaper.log(log);
        logger.info("UserLogServicImpl log end");
    }

    @Override
    public void clear(UserLog log)
    {
        logger.info("UserLogServicImpl log start");
        maaper.clear(log.getCreate_time());
        logger.info("UserLogServicImpl log end");
    }

}
