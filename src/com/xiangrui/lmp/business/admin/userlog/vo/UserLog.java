package com.xiangrui.lmp.business.admin.userlog.vo;

import java.io.Serializable;

/**
 * 后台用户操作日志
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-23 上午10:33:57
 * </p>
 */
public class UserLog implements Serializable
{

    private static final long serialVersionUID = 1L;

    /**
     * ip地址
     */
    private String ip_values;

    /**
     * 创建事假
     */
    private String create_time;

    /**
     * 开始时间,页面传来的条件
     */
    private String start_time;

    /**
     * 结束时间,页面传来的条件
     */
    private String end_time;

    /**
     * 用户id
     */
    private int user_id;

    /**
     * 用户名称
     */
    private String user_name;

    /**
     * 表主键
     */
    private int log_id;

    /**
     * 操作内容
     */
    private String content_desc;

    /**
     * 操作内容的前面20个字符
     * 
     * @return
     */
    public String getContentDesc()
    {
        if (content_desc.length() > 20)
            return content_desc.substring(0, 20) + "...";
        else
            return content_desc;
    }

    public String getStart_time()
    {
        return start_time;
    }

    public void setStart_time(String start_time)
    {
        this.start_time = start_time;
    }

    public String getEnd_time()
    {
        return end_time;
    }

    public void setEnd_time(String end_time)
    {
        this.end_time = end_time;
    }

    public String getUser_name()
    {
        return user_name;
    }

    public void setUser_name(String user_name)
    {
        this.user_name = user_name;
    }

    public String getIp_values()
    {
        return ip_values;
    }

    public void setIp_values(String ip_values)
    {
        this.ip_values = ip_values;
    }

    public String getContent_desc()
    {
        return content_desc;
    }

    public void setContent_desc(String content_desc)
    {
        this.content_desc = content_desc;
    }

    public String getCreate_time()
    {
        return create_time;
    }

    public void setCreate_time(String create_time)
    {
        this.create_time = create_time;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public int getLog_id()
    {
        return log_id;
    }

    public void setLog_id(int log_id)
    {
        this.log_id = log_id;
    }

}
