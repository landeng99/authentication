package com.xiangrui.lmp.business.admin.userlog.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.userlog.service.UserLogService;
import com.xiangrui.lmp.business.admin.userlog.vo.UserLog;
import com.xiangrui.lmp.util.PageView;

/**
 * 后台用户操作日志记录
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-4 下午8:17:20
 *         </p>
 */
@Controller
@RequestMapping("/admin/userLog")
public class UserLogController
{

    /**
     * 后台用户操作日志
     */
    @Autowired
    private UserLogService logService;

    /**
     * 首次查询
     * 
     * @param request
     * @return
     */
    @RequestMapping("queryAllOne")
    public String queryAllOne(HttpServletRequest request)
    {
        return queryAll(request, null);
    }

    /**
     * 主要查询
     * 
     * @param request
     * @param userLog
     * @return
     */
    @RequestMapping("queryAll")
    public String queryAll(HttpServletRequest request, UserLog userLog)
    {
        // 获取分页
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);

        // 是否带参数
        if (null != userLog)
        {
            params.put("END_TIME", userLog.getEnd_time());
            params.put("START_TIME", userLog.getStart_time());
            params.put("USER_NAME", userLog.getUser_name());
        }

        List<UserLog> list = logService.queryAll(params);
        request.setAttribute("pageView", pageView);
        request.setAttribute("userLogLists", list);
        return "back/userLogList";
    }

    @RequestMapping("toFinkUserLog")
    public String toFinkUserLog(HttpServletRequest request, UserLog userLog)
    {
        // 验证数据库信息
        userLog = logService.queryId(userLog.getLog_id());

        // 传递到页面
        request.setAttribute("userLogPojo", userLog);

        // 页面位置
        return "back/userLogInfo";
    }
}
