package com.xiangrui.lmp.business.admin.navigation.vo;

import java.util.List;

import com.xiangrui.lmp.business.base.BaseNavigation;

/**
 * 导航栏
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-9 下午4:37:29
 *         </p>
 */
public class Navigation extends BaseNavigation
{

    private static final long serialVersionUID = 1L;

    /**
     * 提供最新的排序值
     * 
     * @return
     */
    public String getOneId()
    {
        return this.getParent_id() + "_" + this.getNav_id();
    }

    /**
     * 分解 类似 [0_1_2_3],[0_3],[0_48_25_3_15]等类型的方法
     * <p>
     * [0_1_2_3 ==> 1_2]; [0_1_2 ==> 0_1]; [0_3 ==> 0_0]; [0 ==> 0_0]
     * </p>
     * <p>
     * 这个方法的结合 数据库的数据来看<br>
     * 在数据库中,idFramework=0_1_2_3,则有id=3,parent_id=2<br>
     * 通过getOneId(),可以读取到[2_3]<br>
     * 在办法中getTwoId(),可以读到[1_2]<br>
     * 在页面中就可以充分化解了:
     * </p>
     * <p>
     * data-tt-id="${goodsType.oneId}" data-tt-parent-id="${goodsType.twoId}"
     * </p>
     * 
     * @return
     */
    public String getTwoId()
    {
        String[] ary = this.getId_framework().split("_");

        if (ary.length < 2)
            return "0_0";
        else if (ary.length == 2)
            return "0_" + ary[0];
        else
        {
            return ary[ary.length - 3] + "_" + ary[ary.length - 2];
        }
    }

    /**
     * 将查询数据出来的id_framework值,还原成数据库中的值
     * 
     * @return
     */
    public String getNewIdFramework()
    {
        String temp = this.getId_framework();
        temp = temp.substring(0, temp.lastIndexOf("_"));
        return temp;
    }
    
    private List<Navigation> childrenLst;

	public List<Navigation> getChildrenLst() {
		return childrenLst;
	}

	public void setChildrenLst(List<Navigation> childrenLst) {
		this.childrenLst = childrenLst;
	}
}
