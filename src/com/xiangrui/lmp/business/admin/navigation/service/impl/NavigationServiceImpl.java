package com.xiangrui.lmp.business.admin.navigation.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.navigation.mapper.NavigationMapper;
import com.xiangrui.lmp.business.admin.navigation.service.NavigationService;
import com.xiangrui.lmp.business.admin.navigation.vo.Navigation;

/**
 * 导航栏
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-9 下午4:39:56
 *         </p>
 */
@Service("navigationService")
public class NavigationServiceImpl implements NavigationService
{

    /**
     * 导航栏
     */
    @Autowired
    private NavigationMapper navigationMapper;

    @Override
    public List<Navigation> queryAll()
    {
        return navigationMapper.queryAll();
    }

    @Override
    public Navigation queryAllId(int nav_id)
    {
        return navigationMapper.queryAllId(nav_id);
    }

    @SystemServiceLog(description = "新增导航栏")
    @Override
    public void addNavigation(Navigation navigation)
    {
        navigationMapper.addNavigation(navigation);
    }

    @SystemServiceLog(description = "调整导航栏的状态")
    @Override
    public void updateNavigationStatus(Navigation navigation)
    {
        // 完善 id_framework 参数的设置
        navigation.setId_framework(navigation.getId_framework() + "%");
        navigationMapper.updateNavigationStatus(navigation);
    }

    @SystemServiceLog(description = "删除导航栏")
    @Override
    public void deleteNavigation(int nav_id)
    {
        navigationMapper.deleteNavigation(nav_id);
    }

    @SystemServiceLog(description = "删除自身导航栏,并级联删除子导航栏数据")
    @Override
    public void deleteNavigationCascade(Navigation navigation)
    {
        // 完善 id_framework 参数的设置
        navigation.setId_framework(navigation.getId_framework() + "%");
        navigationMapper.deleteNavigationCascade(navigation);
    }

    @Override
    public List<Navigation> queryAllNoParent(Navigation navigation)
    {
        // 完善 id_framework 参数的设置
        navigation.setId_framework(navigation.getId_framework() + "%");
        return navigationMapper.queryAllNoParent(navigation);
    }

    @Override
    public List<Navigation> queryAllParent(Navigation navigation)
    {
        // 完善 id_framework 参数的设置
        navigation.setId_framework(navigation.getId_framework() + "%");
        return navigationMapper.queryAllParent(navigation);
    }

    @Override
    public void updateNavigationList(List<Navigation> navigationSubsetDds)
    {
        navigationMapper.updateNavigationList(navigationSubsetDds);
    }

    @Override
    public List<Navigation> queryAllHomepage(Navigation navigation)
    {
        return navigationMapper.queryAllHomepage(navigation);
    }
}
