package com.xiangrui.lmp.business.admin.navigation.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.navigation.vo.Navigation;

/**
 * 导航栏
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-9 下午4:41:03
 * </p>
 */
public interface NavigationService
{
    /**
     * 查询所有导航栏,带参数
     * 
     * @return
     */
    List<Navigation> queryAll();

    /**
     * 查询所有启用的导航栏
     * 
     * @param navigation
     *            仅仅一个parent_id的参数,为小于等于0是第一级,大于0位子级
     * @return
     */
    List<Navigation> queryAllHomepage(Navigation navigation);

    /**
     * 查询单个导航栏信息
     * 
     * @param nav_id
     * @return
     */
    Navigation queryAllId(int nav_id);

    /**
     * 新增导航栏
     * 
     * @param navigation
     */
    void addNavigation(Navigation navigation);

    /**
     * 修改导航栏的状态变更
     * 
     * @param navigation
     */
    void updateNavigationStatus(Navigation navigation);

    /**
     * 删除导航栏
     * 
     * @param nav_id
     */
    void deleteNavigation(int nav_id);

    /**
     * 删除导航栏,并级联删除子导航栏数据
     * 
     * @param navigation
     *            注意参数 navigation.id_framework是原基础上增加nav_id和%补充而来
     */
    void deleteNavigationCascade(Navigation navigation);

    /**
     * 查询所有非自身和子级别的所有导航,还不包括父
     * 
     * @param navigation
     * @return
     */
    List<Navigation> queryAllNoParent(Navigation navigation);

    /**
     * 查询所有的子级别
     * 
     * @param navigation
     * @return
     */
    List<Navigation> queryAllParent(Navigation navigation);

    /**
     * 修改导航栏
     * 
     * @param navigationSubsetDds
     */
    void updateNavigationList(List<Navigation> navigationSubsetDds);
}
