package com.xiangrui.lmp.business.admin.navigation.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.navigation.service.NavigationService;
import com.xiangrui.lmp.business.admin.navigation.vo.Navigation;
import com.xiangrui.lmp.business.base.BaseNavigation;

/**
 * 导航栏
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-9 下午4:41:27
 *         </p>
 */
@Controller
@RequestMapping("/admin/navigation")
public class NavigationController
{

    /**
     * 导航栏
     */
    @Autowired
    private NavigationService navigationService;

    /**
     * 进入查询
     * 
     * @param request
     * @param navigation
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request)
    {
        List<Navigation> list = navigationService.queryAll();
        request.setAttribute("navigationLists", list);
        return "back/navigationList";
    }

    /**
     * 启用或者禁用
     * 
     * @return
     */
    @RequestMapping("/updateNavigationStatus")
    public String updateNavigationStatus(HttpServletRequest request,
            Navigation navigation)
    {
        // 匹配数据库
        navigation = navigationService.queryAllId(navigation.getNav_id());

        // 如果原数据库是禁用状态 也就是有禁用转变成启用的情况
        if (navigation.getStatus() == BaseNavigation.NAVIGATION_STATUS_DISABLE)
        {
            // 不更新子级别
            navigation.setId_framework("a");
            // 是否存在父级
            if (navigation.getParent_id() > 0)
            {

                // 获取父级
                Navigation navigationParent = navigationService
                        .queryAllId(navigation.getParent_id());

                // 父级状态必须是启用状态,不然会存在父级禁用,子级启用的不合逻辑情况
                if (navigationParent.getStatus() == BaseNavigation.NAVIGATION_STATUS_ENABLE)
                {
                    navigation
                            .setStatus(BaseNavigation.NAVIGATION_STATUS_ENABLE);
                } else
                {
                    // 结束修改操作,返回导航栏主页面,启用状态修改不合逻辑,无需变动数据
                    queryAll(request);
                }
            } else
            {
                // 不存在父级,则为最高级,启用和禁用就不需过多考虑
                navigation.setStatus(BaseNavigation.NAVIGATION_STATUS_ENABLE);
            }
        } else if (navigation.getStatus() == BaseNavigation.NAVIGATION_STATUS_ENABLE)
        {
            // 在启用转换禁用的情况中,理论上是无需考虑父级了,因为子级是启用,父级必定是启用的,不然数据结构有问题
            navigation.setStatus(BaseNavigation.NAVIGATION_STATUS_DISABLE);
        }

        // 更新数据库,级联变动子级的状态
        navigationService.updateNavigationStatus(navigation);

        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("navigationParents", null);
        application.setAttribute("navigationChilds", null);

        return queryAll(request);
    }

    /**
     * 删除,联动删除子导航栏
     * 
     * @param request
     * @param navigation
     * @return
     */
    @RequestMapping("/deleteNavigation")
    public String deleteNavigation(HttpServletRequest request,
            Navigation navigation)
    {
        navigation = navigationService.queryAllId(navigation.getNav_id());

        // 删除本身导航栏和子导航栏
        navigationService.deleteNavigationCascade(navigation);

        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("navigationParents", null);
        application.setAttribute("navigationChilds", null);


        return queryAll(request);
    }

    /**
     * 进入添加页面
     * 
     * @return
     */
    @RequestMapping("/toAddNavigation")
    public String toAddNavigation(HttpServletRequest request)
    {
        request.setAttribute("navigationPojo", new Navigation());
        // 准备 select option 数据
        List<Navigation> list = navigationService.queryAll();
        request.setAttribute("navigationLists", list);
        return "back/addNavigation";
    }

    /**
     * 新增
     * 
     * @param request
     * @param navigation
     * @return
     */
    @RequestMapping("/addNavigation")
    public String addNavigation(HttpServletRequest request,
            Navigation navigation)
    {
        // 查询父级导航栏
        Navigation navigationParent = navigationService.queryAllId(navigation
                .getParent_id());

        // 是否存在父级
        if (null == navigationParent)
        {

            // 最高级的 id_framework
            navigation.setId_framework(BaseNavigation.NAVIGATION_ID_FRAMEWORK);
        } else
        {

            // 插入到表中的自身id_framework = 查询出来的父级的 id_framework
            navigation.setId_framework(navigationParent.getId_framework());
        }

        navigationService.addNavigation(navigation);

        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("navigationParents", null);
        application.setAttribute("navigationChilds", null);

        return queryAll(request);
    }

    /**
     * 进入修改页面
     * 
     * @return
     */
    @RequestMapping("/toUpdateNavigation")
    public String toUpdateNavigation(HttpServletRequest request,
            Navigation navigation)
    {
        // 自身
        navigation = navigationService.queryAllId(navigation.getNav_id());
        request.setAttribute("navigationPojo", navigation);

        // 父级
        Navigation navigationParent = navigationService.queryAllId(navigation
                .getParent_id());
        if (null != navigationParent)
        {
            request.setAttribute("navigationParentPojo", navigationParent);
        } else
        {
            request.setAttribute("navigationParentPojo", new Navigation());
        }

        // 准备 select option 数据 不能包括自身和自身的子级别,还不包括父级
        List<Navigation> list = navigationService.queryAllNoParent(navigation);
        request.setAttribute("navigationLists", list);
        return "back/addNavigation";
    }

    /**
     * 更新导航栏信息
     * 
     * @param request
     * @param navigation
     * @return
     */
    @RequestMapping("/updateNavigation")
    public String updateNavigation(HttpServletRequest request,
            Navigation navigation)
    {
        // 原数据库中的
        Navigation navigationDb = navigationService.queryAllId(navigation
                .getNav_id());

        // 原父级
        Navigation navigationParentOld = new Navigation();
        if (navigationDb.getParent_id() > 0)
        {
            navigationParentOld = navigationService.queryAllId(navigationDb
                    .getParent_id());
        } else
        {
            navigationParentOld
                    .setId_framework(BaseNavigation.NAVIGATION_ID_FRAMEWORK);
        }

        // 新父级
        Navigation navigationParentNew = new Navigation();
        if (navigation.getParent_id() > 0)
        {
            navigationParentNew = navigationService.queryAllId(navigation
                    .getParent_id());
        } else
        {
            navigationParentNew
                    .setId_framework(BaseNavigation.NAVIGATION_ID_FRAMEWORK);
        }

        // 查询所有的子级别
        List<Navigation> navigationSubsets = navigationService
                .queryAllParent(navigationDb);

        // 新旧导航的启用和禁用状态是否变化 有变动为true
        boolean isStatusUpdate = navigationDb.getStatus() != navigation
                .getStatus();
        // 新旧导航的父级是否有变更是否变化 有变动为true
        boolean isParentUpdate = navigationParentNew.getId_framework() != navigationParentOld
                .getId_framework();

        // 准备数据库更新对象
        List<Navigation> navigationDds = new ArrayList<Navigation>();

        // 更新本身id_framework的数据库形式, 目标父级查询出的id_framework值
        navigation.setId_framework(navigationParentNew.getId_framework());
        navigationDds.add(navigation);

        // 遍历更新子级别
        for (Navigation temp : navigationSubsets)
        {
            if (isStatusUpdate)
            {
                // 状态更新
                temp.setStatus(navigation.getStatus());
            }

            if (isParentUpdate)
            {
                // 更新新旧分类的标识 新父级的id_framework替换旧父级的id_framework
                temp.setId_framework(temp.getId_framework().replace(
                        navigationParentOld.getId_framework(),
                        navigationParentNew.getId_framework()));
            }

            // 将查询出的id_framework值转换成数据库的id_framework
            temp.setId_framework(temp.getNewIdFramework());
            navigationDds.add(temp);
        }

        // 没有更新父级id的设置,父级更新另外计算
        navigationService.updateNavigationList(navigationDds);

        return queryAll(request);
    }

    /**
     * 进入查询
     * 
     * @param request
     * @param navigation
     * @return
     */
    @RequestMapping(value="/queryMenu", method = RequestMethod.POST)
    @ResponseBody
    public List<Navigation> queryMenu(HttpServletRequest request)
    {
        List<Navigation> list = navigationService.queryAll();
        List<Navigation> result = new ArrayList<>();
        Map<Integer, Navigation> navMap = new HashMap<>();
        for (Navigation nav : list) {
        	if (nav.getStatus() != 1) {
        		continue;
        	}
			if (navMap.get(nav.getNav_id()) != null) {
				nav.setChildrenLst(navMap.get(nav.getNav_id()).getChildrenLst());
			}
			navMap.put(nav.getNav_id(), nav);
			if (nav.getParent_id() == 0) {
				result.add(nav);
				continue;
			}
			Navigation parentNav = navMap.get(nav.getParent_id());
			if (parentNav == null) {
				parentNav = new Navigation();
			}
			if (parentNav.getChildrenLst() == null) {
				parentNav.setChildrenLst(new ArrayList<Navigation>());
			}
			parentNav.getChildrenLst().add(nav);
		}
        return result;
    }
}
