package com.xiangrui.lmp.business.admin.friendlylink.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.friendlylink.service.FriendlyLinkService;
import com.xiangrui.lmp.business.admin.friendlylink.vo.FriendlyLink;
import com.xiangrui.lmp.pojo.Pagination;
import com.xiangrui.lmp.util.PageView;

@Controller
@RequestMapping("/admin/friendlylink")
public class FriendlyLinkController
{

    @Autowired
    private FriendlyLinkService flinkService;

    /**
     * 启用状态
     */
    private static final int LINK_STATUS_ENABLE = 1;
    /**
     * 禁用状态
     */
    private static final int LINK_STATUS_DISABLE = 2;

    /**
     * 分页查询所有友情链接
     * 
     * @return
     */
    @RequestMapping("/queryAll")
    public String queryAll(HttpServletRequest request)
    {

        // 分页查询
        PageView pageView = null;
        String pageIndex = request.getParameter("pageIndex");
        if (!"".equals(pageIndex) && pageIndex != null)
        {
            int pageCurrent = Integer.parseInt(pageIndex);
            pageView = new PageView(pageCurrent);
        } else
        {
            pageView = new PageView(1);
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageView", pageView);

        List<FriendlyLink> list = flinkService.queryAll(params);

        request.setAttribute("linkLists", list);
        return "back/linkList";
    }

    /**
     * 进入添加页面
     * 
     * @return
     */
    @RequestMapping("/toAddLink")
    public String toAddLink()
    {
        return "back/addFriendlyLink";
    }

    /**
     * 添加
     * 
     * @param request
     * @param link
     * @return
     */
    @RequestMapping("/addLink")
    public String addLink(HttpServletRequest request, FriendlyLink link)
    {
        link.setCreatetime(new SimpleDateFormat("yyyy-MM-dd")
                .format(new Date()));
        flinkService.addLink(link);

        // 更新前台显示连接的application对象的内容
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("friendlyLinkLists", null);

        return queryAll(request);
    }

    /**
     * 进入修噶页面
     * 
     * @return
     */
    @RequestMapping("/toUpdateLink")
    public String toUpdateLink(HttpServletRequest request, FriendlyLink link)
    {
        request.setAttribute("friendlyLinkPojo",
                flinkService.queryAllId(link.getId()));
        return "back/updateLink";
    }

    /**
     * 修改
     * 
     * @param request
     * @param link
     * @return
     */
    @RequestMapping("/updateLink")
    public String updateLink(HttpServletRequest request, FriendlyLink link)
    {
        flinkService.updateLink(link);

        // 更新前台显示连接的application对象的内容
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("friendlyLinkLists", null);
        return queryAll(request);
    }

    /**
     * 删除
     * 
     * @param request
     * @param link
     * @return
     */
    @RequestMapping("/delLink")
    public String delLink(HttpServletRequest request, FriendlyLink link)
    {
        flinkService.deleteLink(link.getId());

        // 更新前台显示连接的application对象的内容
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("friendlyLinkLists", null);
        return queryAll(request);
    }

    /**
     * 启用或者禁用
     * 
     * @param request
     * @param link
     * @return
     */
    @RequestMapping("/udpateStatus")
    public String udpateStatus(HttpServletRequest request, FriendlyLink link)
    {
        link = flinkService.queryAllId(link.getId());

        if (link.getStatus() == LINK_STATUS_ENABLE)
        {
            link.setStatus(LINK_STATUS_DISABLE);
        } else
        {
            link.setStatus(LINK_STATUS_ENABLE);
        }

        flinkService.updateLink(link);

        // 更新前台显示连接的application对象的内容
        ServletContext application = request.getSession().getServletContext();
        application.setAttribute("friendlyLinkLists", null);
        return queryAll(request);
    }
}
