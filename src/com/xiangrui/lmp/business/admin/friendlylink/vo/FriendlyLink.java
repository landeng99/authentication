package com.xiangrui.lmp.business.admin.friendlylink.vo;

import java.util.Date;

public class FriendlyLink {		//友情链接管理
	private int id;		//主键id
	private String name;	//链接名称
	private String link;	//链接地址
	private String opentype;	//链接打开方式
	private int status;		//状态 0：禁用 1：启用
	private String createtime;//添加时间
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreatetime() {
	    
	    if (null != createtime && createtime.lastIndexOf(".0") == 19)
            return createtime.substring(0, 19);
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getOpentype() {
		return opentype;
	}
	public void setOpentype(String opentype) {
		this.opentype = opentype;
	}
	
	public FriendlyLink(int id, String name, String link, String opentype,
			int status, String createtime) {
		super();
		this.id = id;
		this.name = name;
		this.link = link;
		this.opentype = opentype;
		this.status = status;
		this.createtime = createtime;
	}
	
	public FriendlyLink(String name, String link, String opentype, int status,
	        String createtime) {
		super();
		this.name = name;
		this.link = link;
		this.opentype = opentype;
		this.status = status;
		this.createtime = createtime;
	}
	public FriendlyLink() {
		super();
	}
	
}
