package com.xiangrui.lmp.business.admin.friendlylink.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.friendlylink.vo.FriendlyLink;


public interface FriendlyLinkService {
	/**
	 *  查询所有链接
	 * @return
	 */
	List<FriendlyLink> queryAll(Map<String,Object> params);
	/**
     *  查询所有链接,为前台准备,过滤没启用的链接
     * @return
     */
    List<FriendlyLink> queryAllHomepage();
    /**
	 * 添加
	 * @param flink
	 */
	void addLink(FriendlyLink flink);
	/**
	 * 修改
	 * @param flink
	 */
	void updateLink(FriendlyLink flink);
	/**
	 * 删除
	 * @param id
	 */
	void deleteLink(int id);
	/**
	 * 查询总记录数
	 * @param role
	 * @return
	 */
	int queryCount();
	
	/**
	 * 查询某个
	 * @param id
	 * @return
	 */
	FriendlyLink queryAllId(int id);
}
