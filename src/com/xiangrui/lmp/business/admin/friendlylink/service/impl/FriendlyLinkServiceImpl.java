package com.xiangrui.lmp.business.admin.friendlylink.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.friendlylink.mapper.FriendlyLinkMapper;
import com.xiangrui.lmp.business.admin.friendlylink.service.FriendlyLinkService;
import com.xiangrui.lmp.business.admin.friendlylink.vo.FriendlyLink;

@Service("value=friendlyLinkService")
public class FriendlyLinkServiceImpl implements FriendlyLinkService{
	@Autowired 
	private FriendlyLinkMapper flinkMapper;
	
	@Override
	public List<FriendlyLink> queryAll(Map<String,Object> params) {
		return flinkMapper.queryAll(params);
	}

	@SystemServiceLog(description="新增链接信息")
	@Override
	public void addLink(FriendlyLink flink) {
		flinkMapper.addLink(flink);
	}

	@SystemServiceLog(description="修改链接信息")
	@Override
	public void updateLink(FriendlyLink flink) {
		flinkMapper.updateLink(flink);
	}

	@SystemServiceLog(description="删除链接")
	@Override
	public void deleteLink(int id) {
		flinkMapper.deleteLink(id);
	}

	@Override
	public int queryCount() {
		return flinkMapper.queryCount();
	}

    @Override
    public List<FriendlyLink> queryAllHomepage()
    {
        return flinkMapper.queryAllHomepage();
    }

    @Override
    public FriendlyLink queryAllId(int id)
    {
        return flinkMapper.queryAllId(id);
    }

}
