package com.xiangrui.lmp.business.handler;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xiangrui.lmp.business.admin.interfacelog.service.InterfaceLogService;
import com.xiangrui.lmp.business.admin.interfacelog.vo.InterfaceLog;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkglog.service.PkgLogService;
import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;
import com.xiangrui.lmp.business.admin.subscribe.mapper.SubscribeMapper;
import com.xiangrui.lmp.business.admin.subscribe.vo.Subscribe;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.init.SpringContextHolder;
import com.xiangrui.lmp.util.HttpClientTool;
import com.xiangrui.lmp.util.UUIDUtil;

@Component("expressInfoPushHandler")
public class ExpressInfoPushHandler
{
    Logger logger =Logger.getLogger(ExpressInfoPushHandler.class);
    
    @Autowired
    private SubscribeMapper subscribeMapper;
    
    @Autowired
    private PackageService packageService;
    
    @Autowired
    private PkgLogService pkgLogService;
    
    private final static String XML_TOP = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    
    public void execute(Object data)
    {
        
        MessageConfig messageConfig=(MessageConfig)data;
        ExpressInfo expressInfo = messageConfig.getExpressInfo();
        
        String nu=expressInfo.getNu();
        
        Map<String,Object> params=new HashMap<String,Object>();
        
        params.put("number", nu);

        //国外订阅类型
        params.put("subscribe_type", Subscribe.ABROAD_SUB_TYPE);
        
        
        //根据快递100推送的单号查询订阅表是否有国外订阅
        Subscribe  abroadSubscribe=subscribeMapper.querySubscribeByParam(params);
        
        //国外信息订阅,订阅表中有快递100的单号，并且是国外快递的订阅则直接返回，反之则是跨境物流平台快递信息订阅
        if(abroadSubscribe!=null){
            
            String xml=createXml(expressInfo);
            sendInfo(abroadSubscribe.getCallbackurl(),xml);
        }
        //国内订阅
        else{
            
            //根据包裹ems单号查询包裹
            List<Pkg> pkgList= packageService.queryPackageByEms_code(nu);
            if(pkgList.isEmpty())
            {
                return ;
            }
            Pkg pkg=pkgList.get(0);
            
            
            String original_num=pkg.getOriginal_num();
            
            //通过关联单号国内订阅中，查询用户是否已有订阅该物物流信息
            Map<String,Object> home_params=new HashMap<String,Object>();
            home_params.put("number", original_num);
            home_params.put("subscribe_type", Subscribe.HOME_SUB_TYPE);
            
            Subscribe homeSu=  subscribeMapper.querySubscribeByParam(home_params);

            //根据关联单号查询为空，则查询是否属于公司单号订阅类型
            if(homeSu==null){
                homeSu=  subscribeMapper.querySubscribeByLogisticsCode(pkg.getLogistics_code());
            }
            
            //如果订阅不存在则直接返回
            if(homeSu==null){
                return;
            }

            //属于用户的跨境物流物流信息，则推送给用户
            if(homeSu.getSubscribe_type()==Subscribe.HOME_SUB_TYPE){
                String callbackUrl=homeSu.getCallbackurl();
                sendHomeInfo(callbackUrl,expressInfo, pkg);
            }
            
        }
        
    }
    
    
    private void sendHomeInfo(String callbackUrl,ExpressInfo expressInfo, Pkg pkg){
         
         // 读取包裹的操作日志
         List<PkgLog> pkgLogs = this.pkgLogService.queryPkgLogByPackageId(pkg
                .getPackage_id());
         
         Document doc = DocumentHelper.createDocument();
         Element root = doc.addElement("pushRequest");
         createTextE(root, "status", expressInfo.getMonitoring_status());
         createTextE(root, "message", expressInfo.getMonitoring_message());
         createTextE(root, "ischeck", expressInfo.getIscheck());
         createTextE(root,"logistics_code",pkg.getLogistics_code());
         createTextE(root,"state",expressInfo.getState());
         String original_num=pkg.getOriginal_num()==null?"":pkg.getOriginal_num();
         createTextE(root,"number",original_num);
         
         String dataList = expressInfo.getData();

         JSONArray ja = JSONArray.fromObject(dataList);
         Iterator<JSONObject> its = ja.iterator();
         Element dataListE = root.addElement("dataList");

         while (its.hasNext())
         {
             JSONObject jo = its.next();
             String time = jo.getString("time");
             String context = jo.getString("context");
             Element dataE = dataListE.addElement("data");
             createTextE(dataE, "time", time);
             createTextE(dataE, "context", context);
         }
         for(PkgLog pkgLog:pkgLogs){
             Element dataE = dataListE.addElement("data");
             createTextE(dataE, "time", pkgLog.getOperate_time_toStr());
             createTextE(dataE, "context", pkgLog.getDescription());
         }
         StringBuffer buffer = new StringBuffer();
         String xml=buffer.append(doc.asXML()).toString();
         sendInfo(callbackUrl,xml);
    }
    
    
    private void sendInfo(String callbackUrl,String xml){
        
        InterfaceLog log=new InterfaceLog();
        log.setStart_time(new Timestamp(System.currentTimeMillis()));
        log.setInterace_type(InterfaceLog.TYPE_EXPRESSINFO_PUSH);
        log.setLog_id(UUIDUtil.uuid());
        log.setReq_msg(xml);
       
        
        String url=callbackUrl;
        String result="";
        //累加器
        int i=0;
        
        //如果返回异常则循环执行3次，直到发送为止。
        int cnt=3;
        
        while("".equals(result)&&i<cnt){
            result= send(url,xml);
            i++;
        }
        if("".equals(result)){
            log.setStatus(InterfaceLog.STATUS_FAIL);
        }else {
            log.setStatus(InterfaceLog.STATUS_SUCCESS);
        }
        log.setResp_msg(result);
        log.setEnd_time(new Timestamp(System.currentTimeMillis()));
        log(log);
    }
    
    
    /**
     * 记录日志
     * @param log
     */
    private void log(InterfaceLog log){
        InterfaceLogService interfaceLogService = SpringContextHolder
                .getBean("interfaceLogService");
        interfaceLogService.createInterfaceLog(log);
        
    }
    
    private String send(String url,String data){
        String result="";
        try
        {
            result= HttpClientTool.post(url, data, "utf-8");
        } catch (IOException e)
        {
            logger.error(e.toString(),e);
        }
       return  result;
    }
    
    /**
     * 拼接国外快递物流信息
     * @param expressInfo
     * @return
     */
    private String createXml(ExpressInfo expressInfo)
    {

        Document doc = DocumentHelper.createDocument();
        Element root = doc.addElement("pushRequest");
        createTextE(root, "status", expressInfo.getMonitoring_status());
        createTextE(root, "message", expressInfo.getMonitoring_message());
        createTextE(root, "ischeck", expressInfo.getIscheck());
        createTextE(root, "state", expressInfo.getState());
        createTextE(root,"number",expressInfo.getNu());

        String dataList = expressInfo.getData();

        JSONArray ja = JSONArray.fromObject(dataList);
        Iterator<JSONObject> its = ja.iterator();
        Element dataListE = root.addElement("dataList");
        while (its.hasNext())
        {
            JSONObject jo = its.next();
            String time = jo.getString("time");
            String context = jo.getString("context");
            Element dataE = dataListE.addElement("data");
            createTextE(dataE, "time", time);
            createTextE(dataE, "context", context);
        }

        StringBuffer buffer = new StringBuffer();
     //   buffer.append(XML_TOP);
        buffer.append(doc.asXML());
        return buffer.toString();
    }
    
    private Element createTextE(Element e, String name, String value)
    {
        Element element = e.addElement(name);
        String str=(value==null)?"":value;
        element.setText(str);
        return element;
    }

}
