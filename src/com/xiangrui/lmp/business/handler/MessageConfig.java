package com.xiangrui.lmp.business.handler;

import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;

public class MessageConfig
{
    
    private ExpressInfo expressInfo;

    public ExpressInfo getExpressInfo()
    {
        return expressInfo;
    }

    public void setExpressInfo(ExpressInfo expressInfo)
    {
        this.expressInfo = expressInfo;
    }
    
}
