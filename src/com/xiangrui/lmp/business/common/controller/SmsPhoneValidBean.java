package com.xiangrui.lmp.business.common.controller;

import java.util.Date;

import com.xiangrui.lmp.util.DateFormatUtils;
import com.xiangrui.lmp.util.DateUtil;

public class SmsPhoneValidBean
{
	private String phone;
	private Date lastSendTime;
	private int count;

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public Date getLastSendTime()
	{
		return lastSendTime;
	}

	public void setLastSendTime(Date lastSendTime)
	{
		this.lastSendTime = lastSendTime;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public SmsPhoneValidBean(String phone)
	{
		this.phone = phone;
		this.lastSendTime = new Date();
		this.count = 1;
	}

	public boolean canSend()
	{
		boolean canSend = false;
		Date now = new Date();
		String lastSendStr = DateUtil.getStringToday(lastSendTime);
		String nowStr = DateUtil.getStringToday(now);
		if (!nowStr.equals(lastSendStr))
		{
			this.setLastSendTime(now);
			this.count = 1;
			return true;
		}
		//一天只能发五次
		if (count <= 5)
		{
			// 2分钟内不可以重发
			if ((now.getTime() - lastSendTime.getTime()) > 2 * 60 * 1000)
			{
				this.setLastSendTime(now);
				this.count = count + 1;
				canSend = true;
			}
		}
		return canSend;
	}
}
