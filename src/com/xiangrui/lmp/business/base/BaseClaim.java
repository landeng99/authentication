package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 索赔
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午9:57:17
 *         </p>
 */
public abstract class BaseClaim implements Serializable
{
    /**
     * 索赔状态申请
     */
    public static final int CLAIM_APPLICATION = 1;

    /**
     * 索赔状态审核拒绝
     */
    public static final int CLAIM_REFUSING = 2;

    /**
     * 索赔状态审核通过
     */
    public static final int CLAIM_THROUGH = 3;

    /**
     * 索赔状态退款中
     */
    public static final int CLAIM_PROCESSING = 4;

    /**
     * 索赔状态完成
     */
    public static final int CLAIM_COMPLETE = 5;

    /**
     * 虚拟账号金额的索赔
     */
    public static final int ACCOUNTLOG_CLAIM = 4;

    /**
     * 理赔图片 包裹图片中的类型
     */
    public static final int CLAIM_IMG_TYPE = 3;

    private static final long serialVersionUID = 1L;
    /**
     * '用户索赔id，主键， 自增长',
     */
    private int claimId;
    /**
     * '包裹id',
     */
    private int packageId;
    /**
     * '申请原因',
     */
    private String reason;
    /**
     * '申请理赔金额',
     */
    private double amount;
    /**
     * '申请时间',
     */
    private String applyTime;
    /**
     * '处理状态；1：提交申请，2：审批拒绝，3：通过审批，4：退款中，5：完成',
     */
    private int status;

    public int getClaimId()
    {
        return claimId;
    }

    public void setClaimId(int claimId)
    {
        this.claimId = claimId;
    }

    public int getPackageId()
    {
        return packageId;
    }

    public void setPackageId(int packageId)
    {
        this.packageId = packageId;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public String getApplyTime()
    {
        if (null != applyTime && applyTime.lastIndexOf(".0") == 19)
            return applyTime.substring(0, 19);
        return applyTime;
    }

    public void setApplyTime(String applyTime)
    {
        this.applyTime = applyTime;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "BaseClaim [claimId=" + claimId + ", packageId=" + packageId
                + ", reason=" + reason + ", amount=" + amount + ", applyTime="
                + applyTime + ", status=" + status + "]";
    }

}
