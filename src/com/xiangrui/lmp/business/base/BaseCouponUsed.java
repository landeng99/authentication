package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 用户的优惠券信息
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午9:58:31
 *         </p>
 */
public abstract class BaseCouponUsed implements Serializable
{

    private static final long serialVersionUID = 1L;
    /**
	 *  
	 */
    private int coupon_id;
    /**
     * '编号',
     */
    private String coupon_code;
    /**
     * '用户id',
     */
    private int user_id;
    /**
     * '来源',
     */
    private String source;
    /**
     * '面额',
     */
    private double denomination;
    /**
     * '数量',
     */
    private int quantity;
    /**
     * '使用日期',
     */
    private String use_date;
    /**
     * '订单号',
     */
    private String order_code;
    /**
     * 优惠券名称
     */
    private String coupon_name;
    /**
     * 用户获得优惠券ID
     */
    private int real_user_coupon_id;
    
    
    public int getReal_user_coupon_id()
	{
		return real_user_coupon_id;
	}

	public void setReal_user_coupon_id(int real_user_coupon_id)
	{
		this.real_user_coupon_id = real_user_coupon_id;
	}

	public String getCoupon_name()
	{
		return coupon_name;
	}

	public void setCoupon_name(String coupon_name)
	{
		this.coupon_name = coupon_name;
	}

	public int getCoupon_id()
    {
        return coupon_id;
    }

    public void setCoupon_id(int coupon_id)
    {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_code()
    {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code)
    {
        this.coupon_code = coupon_code;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public double getDenomination()
    {
        return denomination;
    }

    public void setDenomination(double denomination)
    {
        this.denomination = denomination;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public String getUse_date()
    {
        if (null != use_date && use_date.lastIndexOf(".0") == 19)
            return use_date.substring(0, 19);
        return use_date;
    }

    public void setUse_date(String use_date)
    {
        this.use_date = use_date;
    }

    public String getOrder_code()
    {
        return order_code;
    }

    public void setOrder_code(String order_code)
    {
        this.order_code = order_code;
    }
}
