package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

public class BaseWithdrawalLog implements Serializable
{
    
    /**
     * 1提交申请
     */
    public static final int APPLY = 1;
    /**
     * 2审核通过
     */
    public static final int SUCCESS = 2;
    
    /**
     * 3审核拒绝
     */
    public static final int FAIL = 2;
    
    /**
     * 4提现取消
     */
    public static final int CANCEL = 4;

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private int log_id;

    /**
     * 用户ID
     */
    private int user_id;

    /**
     * 金额
     */
    private float amount;

    /**
     * 时间
     */
    private Timestamp time;

    /**
     * 交易号
     */
    private String trans_id;

    /**
     * 状态 1：提交申请，2：审核通过，3：审核拒绝
     */
    private int status;

    /**
     * 提现用户名
     */
    private String user_name;

    /**
     * 支付宝账号（银行账号）
     */
    private String alipay_no;

    /**
     * 提现备注
     */
    private String remark;
    
    /**
     * 开户银行名称
     */
    private String bank_name;
    
    /**
     * 审核人员
     */
    private int operator;
    
    /**
     * 审核时间
     */
    private Timestamp operate_time;

    public int getLog_id()
    {
        return log_id;
    }

    public void setLog_id(int log_id)
    {
        this.log_id = log_id;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public float getAmount()
    {
        return amount;
    }

    public void setAmount(float amount)
    {
        this.amount = amount;
    }

    public Timestamp getTime()
    {
        return time;
    }

    public String getTime_toStr()
    {
        if (null != time)
        {
            return time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setTime(Timestamp time)
    {
        this.time = time;
    }

    public String getTrans_id()
    {
        return trans_id;
    }

    public void setTrans_id(String trans_id)
    {
        this.trans_id = trans_id;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getUser_name()
    {
        return user_name;
    }

    public void setUser_name(String user_name)
    {
        this.user_name = user_name;
    }

    public String getAlipay_no()
    {
        return alipay_no;
    }

    public void setAlipay_no(String alipay_no)
    {
        this.alipay_no = alipay_no;
    }

    public String getRemark()
    {
        return remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public String getBank_name()
    {
        return bank_name;
    }

    public void setBank_name(String bank_name)
    {
        this.bank_name = bank_name;
    }

    public int getOperator()
    {
        return operator;
    }

    public void setOperator(int operator)
    {
        this.operator = operator;
    }

    public Timestamp getOperate_time()
    {
        return operate_time;
    }

    public void setOperate_time(Timestamp operate_time)
    {
        this.operate_time = operate_time;
    }


}
