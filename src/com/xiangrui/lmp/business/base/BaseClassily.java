package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 文章类型
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 上午9:57:51
 * </p>
 */
public abstract class BaseClassily implements Serializable
{
    private static final long serialVersionUID = 1L;
    /**
     * '文章分类id',
     */
    private int id;
    /**
     * '文章类型名',
     */
    private String name;
    /**
     * '上级id',
     */
    private int parentId;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getParent_id()
    {
        return parentId;
    }

    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }

}
