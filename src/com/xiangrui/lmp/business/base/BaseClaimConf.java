package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 索赔类型
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 上午10:05:15
 * </p>
 */
public abstract class BaseClaimConf implements Serializable
{
    private static final long serialVersionUID = 1L;
    /**
     * '索赔id，主键，自增长',
     */
    private int claimId;
    /**
     * '索赔类型，如：丢失，破损',
     */
    private int claimType;
    /**
     * '赔偿比例',
     */
    private double payEatio;

    public int getClaimId()
    {
        return claimId;
    }

    public void setClaimId(int claimId)
    {
        this.claimId = claimId;
    }

    public int getClaimType()
    {
        return claimType;
    }

    public void setClaimType(int claimType)
    {
        this.claimType = claimType;
    }

    public double getPayEatio()
    {
        return payEatio;
    }

    public void setPayEatio(double payEatio)
    {
        this.payEatio = payEatio;
    }

}
