package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Will
 *
 */
public abstract class BaseWarningSmsTemplate implements Serializable
{
	/**
	 * 主键ID
	 */
	private int seq_id;
	
	private String temp_name;
	/**
	 * 短信模板内容
	 */
	private String temp_conent;

	/**
	 * 是否启用
	 */
	private String enable;

	/**
	 * 创建时间
	 */
	private Date create_time;
	/**
	 * 备注
	 */
	private String remark;

	public int getSeq_id()
	{
		return seq_id;
	}

	public void setSeq_id(int seq_id)
	{
		this.seq_id = seq_id;
	}

	public String getTemp_conent()
	{
		return temp_conent;
	}

	public void setTemp_conent(String temp_conent)
	{
		this.temp_conent = temp_conent;
	}

	public String getEnable()
	{
		return enable;
	}

	public void setEnable(String enable)
	{
		this.enable = enable;
	}

	public Date getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(Date create_time)
	{
		this.create_time = create_time;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public String getTemp_name()
	{
		return temp_name;
	}

	public void setTemp_name(String temp_name)
	{
		this.temp_name = temp_name;
	}
}
