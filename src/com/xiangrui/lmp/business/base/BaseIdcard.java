package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public class BaseIdcard implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String idcard;	// 身份证号
	private String realname;	// 人名
	private String area;	// 身份证地址
	private String sex;
	private String birthday;

	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}
	public String getIdcard(){
		return this.idcard;
	}
	public void setIdcard(String idcard){
		this.idcard=idcard;
	}
	public String getRealname(){
		return this.realname;
	}
	public void setRealname(String realname){
		this.realname=realname;
	}
	public String getArea(){
		return this.area;
	}
	public void setArea(String area){
		this.area=area;
	}
	public String getSex(){
		return this.sex;
	}
	public void setSex(String sex){
		this.sex=sex;
	}
	public String getBirthday(){
		return this.birthday;
	}
	public void setBirthday(String birthday){
		this.birthday=birthday;
	}
}
