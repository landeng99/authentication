package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public class BaseFreightAccountingDetail implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private int id;

    /**
     * 批次号
     */
    private int batch_id;

    /**
     * 公司运单号
     */
    private String logistics_code;

    /**
     * 单价
     */
    private float price;

    /**
     * 计费重量
     */
    private float weight;
    /**
     * 运费
     */
    private float freight;
    /**
     * 总运费
     */
    private float transport_cost;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getBatch_id()
    {
        return batch_id;
    }

    public void setBatch_id(int batch_id)
    {
        this.batch_id = batch_id;
    }

    public String getLogistics_code()
    {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public float getWeight()
    {
        return weight;
    }

    public void setWeight(float weight)
    {
        this.weight = weight;
    }

    public float getFreight()
    {
        return freight;
    }

    public void setFreight(float freight)
    {
        this.freight = freight;
    }

    public float getTransport_cost()
    {
        return transport_cost;
    }

    public void setTransport_cost(float transport_cost)
    {
        this.transport_cost = transport_cost;
    }

    public static long getSerialversionuid()
    {
        return serialVersionUID;
    }

}
