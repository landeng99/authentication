package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public abstract class BaseExportConfig implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 模板ID
     */
    private int export_id;
    /**
     * 模板名称
     */
    private String template_name;
    /**
     * 模板项目
     */
    private String items;
    /**
     * 描述
     */
    private String description;

    public int getExport_id()
    {
        return export_id;
    }

    public void setExport_id(int export_id)
    {
        this.export_id = export_id;
    }

    public String getTemplate_name()
    {
        return template_name;
    }

    public void setTemplate_name(String template_name)
    {
        this.template_name = template_name;
    }

    public String getItems()
    {
        return items;
    }

    public void setItems(String items)
    {
        this.items = items;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

}
