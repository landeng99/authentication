package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

public class BaseTariff implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 资费Id
     */
    private int tariff_id;

    /**
     * 资费名称
     */
    private String tariff_name;

    /**
     * 资费类型
     */
    private int tariff_type;

    /**
     * 资费税率
     */
    private float percent;

    /**
     * 资费单位
     */
    private String unit;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 更新时间
     */
    private Timestamp update_time;

    /**
     * 逻辑删除状态
     */
    private int isdeleted;

    public int getTariff_id()
    {
        return tariff_id;
    }

    public void setTariff_id(int tariff_id)
    {
        this.tariff_id = tariff_id;
    }

    public String getTariff_name()
    {
        return tariff_name;
    }

    public void setTariff_name(String tariff_name)
    {
        this.tariff_name = tariff_name;
    }

    public int getTariff_type()
    {
        return tariff_type;
    }

    public void setTariff_type(int tariff_type)
    {
        this.tariff_type = tariff_type;
    }

    public float getPercent()
    {
        return percent;
    }

    public void setPercent(float percent)
    {
        this.percent = percent;
    }

    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Timestamp getUpdate_time()
    {
        return update_time;
    }

    public String getUpdate_time_toStr()
    {
        if (null != update_time)
        {
            return update_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setUpdate_time(Timestamp update_time)
    {
        this.update_time = update_time;
    }

    public int getIsdeleted()
    {
        return isdeleted;
    }

    public void setIsdeleted(int isdeleted)
    {
        this.isdeleted = isdeleted;
    }

}
