package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

public class BaseFreightAccounting implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 批次号id
     */

    private int batch_id;

    /**
     * 批次号
     */
    private String batch_code;

    /**
     * 管理用户
     */
    private int user_id;

    /**
     * 创建时间
     */

    private Timestamp createtime;

    public int getBatch_id()
    {
        return batch_id;
    }

    public void setBatch_id(int batch_id)
    {
        this.batch_id = batch_id;
    }

    public String getBatch_code()
    {
        return batch_code;
    }

    public void setBatch_code(String batch_code)
    {
        this.batch_code = batch_code;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public Timestamp getCreatetime()
    {
        return createtime;
    }

    public void setCreatetime(Timestamp createtime)
    {
        this.createtime = createtime;
    }

    public static long getSerialversionuid()
    {
        return serialVersionUID;
    }
}
