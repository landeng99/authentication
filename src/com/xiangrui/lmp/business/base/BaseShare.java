package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 晒单信息
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午10:01:23
 *         </p>
 */
public abstract class BaseShare implements Serializable
{
	/**
	 * 未赠送
	 */
	public static final int SHARE_STATUS_NOGIVE = 1;

	/**
	 * 已赠送
	 */
	public static final int SHARE_STATUS_GIVE = 2;

	/**
	 * 未审核
	 */
	public static final int APPROVE_PROCESSING = 1;

	/**
	 * 审核通过
	 */
	public static final int APPROVE_THROUGH = 2;

	/**
	 * 审核拒绝
	 */
	public static final int APPROVE_REFUSING = 3;

	private static final long serialVersionUID = 1L;
	/**
	 * '分享id',
	 */
	private int share_id;
	/**
	 * '分享用户id',
	 */
	private int user_id;
	/**
	 * '包裹id',
	 */
	private int package_id;
	/**
	 * '分享时间',
	 */
	private String share_time;
	/**
	 * '晒单链接',
	 */
	private String share_link;
	/**
	 * '获得优惠券数量',
	 */
	private int coupon_num;
	/**
	 * '审核状态，0：未审核，1：审核通过，2：审核拒绝',
	 */
	private int approval_status;
	/**
	 * '处理状态，0：未赠送，1：已赠送优惠券',
	 */
	private int status;
	/**
	 * 
	 */
	private String reason;

	/**
	 * 赠送优惠券ID
	 */
	private int award_coupon_id;

	public int getAward_coupon_id()
	{
		return award_coupon_id;
	}

	public void setAward_coupon_id(int award_coupon_id)
	{
		this.award_coupon_id = award_coupon_id;
	}


	public String getShare_time()
	{
		if (null != share_time && share_time.lastIndexOf(".0") == 19)
			return share_time.substring(0, 19);
		return share_time;
	}

	public void setShare_time(String share_time)
	{
		this.share_time = share_time;
	}

	public int getShare_id()
	{
		return share_id;
	}

	public void setShare_id(int share_id)
	{
		this.share_id = share_id;
	}

	public int getUser_id()
	{
		return user_id;
	}

	public void setUser_id(int user_id)
	{
		this.user_id = user_id;
	}

	public int getPackage_id()
	{
		return package_id;
	}

	public void setPackage_id(int package_id)
	{
		this.package_id = package_id;
	}

	public String getShare_link()
	{
		return share_link;
	}

	public void setShare_link(String share_link)
	{
		this.share_link = share_link;
	}

	public int getCoupon_num()
	{
		return coupon_num;
	}

	public void setCoupon_num(int coupon_num)
	{
		this.coupon_num = coupon_num;
	}

	public int getApproval_status()
	{
		return approval_status;
	}

	public void setApproval_status(int approval_status)
	{
		this.approval_status = approval_status;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}
}
