package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 网站信息
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-8 下午5:45:26
 * </p>
 */
public abstract class BaseSiteInfo implements Serializable
{

    private static final long serialVersionUID = 1L;
    /**
     * 信息名称
     */
    private String title;
    /**
     * 网站logo
     */
    private String logo;
    /**
     * 版权说明
     */
    private String copyright;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getLogo()
    {
        return logo;
    }

    public void setLogo(String logo)
    {
        this.logo = logo;
    }

    public String getCopyright()
    {
        return copyright;
    }

    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

}
