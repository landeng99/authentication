package com.xiangrui.lmp.business.base;
import java.util.Date;

public class BaseSubscribe
{
    
    /**
     * 订阅id
     */
    private String subscribe_id;
    
    
    /**
     * 订阅类型：1国外订阅，2国内订阅
     */
    private int subscribe_type;

    /**
     * 用户id
     */
    private int user_id;

    /**
     * 快递公司编码
     */
    private String com;

    /**
     * 物流单号
     */
    private String number;

    /**
     * 订阅时间
     */
    private Date create_time;
    /**
     * 最近更新时间
     */
    private Date last_modify_time;

    /**
     * 订阅状态：
     */
    private int sub_status;

    /**
     * 回调地址
     */
    private String callbackurl;

    /**
     * 用户订阅成功
     */
    public final static int STATUS_USER_SUB_SUCCESS=0;
    
    
    /**
     * 快递100订阅成功
     */
    public final static int STATUS_KUAID100_SUB_SUCCESS=1;
    
    /**
     * 快递100订阅失败
     */
    public final static int STATUS_KUAID100_SUB_ERROR=2;
    
    
    /**
     * 国外订阅
     */
    public final static int ABROAD_SUB_TYPE=1;
    
    /**
     * 国内订阅
     */
    public final static int HOME_SUB_TYPE=2;
    
    
    
    public String getSubscribe_id()
    {
        return subscribe_id;
    }

    public void setSubscribe_id(String subscribe_id)
    {
        this.subscribe_id = subscribe_id;
    }

    public int getSubscribe_type()
    {
        return subscribe_type;
    }

    public void setSubscribe_type(int subscribe_type)
    {
        this.subscribe_type = subscribe_type;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public String getCom()
    {
        return com;
    }

    public void setCom(String com)
    {
        this.com = com;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public Date getCreate_time()
    {
        return create_time;
    }

    public void setCreate_time(Date create_time)
    {
        this.create_time = create_time;
    }

    public Date getLast_modify_time()
    {
        return last_modify_time;
    }

    public void setLast_modify_time(Date last_modify_time)
    {
        this.last_modify_time = last_modify_time;
    }

    public int getSub_status()
    {
        return sub_status;
    }

    public void setSub_status(int sub_status)
    {
        this.sub_status = sub_status;
    }

    public String getCallbackurl()
    {
        return callbackurl;
    }

    public void setCallbackurl(String callbackurl)
    {
        this.callbackurl = callbackurl;
    }

}
