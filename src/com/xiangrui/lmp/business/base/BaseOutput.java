package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Will
 *
 */
public class BaseOutput implements Serializable
{

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private int seq_id;

	/**
	 * 出库编号
	 */
	private String output_sn;

	/**
	 * 海外仓库地址ID
	 */
	private int osaddr_id;

	/**
	 * 已出库口岸ID列表
	 */
	private String seaport_id_list;

	/**
	 * 自动打印面单 0:否 1：打印
	 */
	private int auto_print;

	/**
	 * 创建时间
	 */
	private Date create_time;
	/**
	 * 创建者用户ID
	 */
	private int create_user_id;

	/**
	 * 备注
	 */
	private String remark;

	public int getSeq_id()
	{
		return seq_id;
	}

	public void setSeq_id(int seq_id)
	{
		this.seq_id = seq_id;
	}

	public String getOutput_sn()
	{
		return output_sn;
	}

	public void setOutput_sn(String output_sn)
	{
		this.output_sn = output_sn;
	}

	public int getOsaddr_id()
	{
		return osaddr_id;
	}

	public void setOsaddr_id(int osaddr_id)
	{
		this.osaddr_id = osaddr_id;
	}

	public String getSeaport_id_list()
	{
		return seaport_id_list;
	}

	public void setSeaport_id_list(String seaport_id_list)
	{
		this.seaport_id_list = seaport_id_list;
	}

	public int getAuto_print()
	{
		return auto_print;
	}

	public void setAuto_print(int auto_print)
	{
		this.auto_print = auto_print;
	}

	public Date getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(Date create_time)
	{
		this.create_time = create_time;
	}

	public int getCreate_user_id()
	{
		return create_user_id;
	}

	public void setCreate_user_id(int create_user_id)
	{
		this.create_user_id = create_user_id;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}
}
