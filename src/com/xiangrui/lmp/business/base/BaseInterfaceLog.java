package com.xiangrui.lmp.business.base;

import java.sql.Timestamp;

public class BaseInterfaceLog
{

    /**
     * 接口类型：1短信接口，2快递100订阅接口，3快递100消息同步接口
     * ，4支付宝支付请求接口，5支付宝状态返回接收接口，6银联支付请求接口，7银联支付状态返回接口,8推送快递信息至客户平台接口
     */

    /**
     * 短信接口
     */
    public final static int TYPE_SMS_TYPE = 1;

    /**
     * 快递100订阅
     */
    public final static int TYPE_KUAIDI100_SUBSCRIBE = 2;

    /**
     * 快递100消息同步
     */
    public final static int type_kuaidi100_msgsub = 3;

    /**
     * 支付宝接口支付
     */
    public final static int TYPE_ALIPAYMENT = 4;

    
    /**
     * 5支付宝状态同步返回接收接口
     */
    public static final int INTERFACE_RETURN = 5;
    /**
     * 6支付宝状态异步通知接收接口
     */
    public static final int INTERFACE_NOTIFY = 6;
    /**
     * 8银联同步状态返回接口
     */
    public static final int INTERFACE_RETURN_BANK = 8;
    /**
     * 9银联异步状态返回接口
     */
    public static final int INTERFACE_NOTIFY_BANK = 9;
    
    /**
     * 10推送快递信息至客户平台接口
     */
    public final static int TYPE_EXPRESSINFO_PUSH = 10;
    
    /**
     * 11用户推送消息至跨境物流平台接口
     */
    public final static int CLIENT_MSG_PUSH = 11;
    
    /**
     * 通讯是成功
     */
    public final static int STATUS_SUCCESS = 1;

    /**
     * 通讯失败
     */
    public final static int STATUS_FAIL = -1;

    /**
     * 日志id
     */
    private String log_id;

    /**
     * 请求报文内容
     */
    private String req_msg;

    /**
     * 返回报文
     */
    private String resp_msg;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 开始时间
     */
    private Timestamp start_time;

    /**
     * 结束时间
     */
    private Timestamp end_time;

    /**
     * 接口类型：1短信接口， 2快递100订阅接口， 3快递100消息同步接口 ， 4支付宝支付请求接口， 5支付宝状态返回接收接口，
     * 6银联支付请求接口， 7银联支付状态返回接口
     */
    private int interace_type;

    /**
     * 状态：1，标识成功，失败则标识为-1
     */
    private int status;

    public String getLog_id()
    {
        return log_id;
    }

    public void setLog_id(String log_id)
    {
        this.log_id = log_id;
    }

    public String getReq_msg()
    {
        return req_msg;
    }

    public void setReq_msg(String req_msg)
    {
        this.req_msg = req_msg;
    }

    public String getResp_msg()
    {
        return resp_msg;
    }

    public void setResp_msg(String resp_msg)
    {
        this.resp_msg = resp_msg;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public Timestamp getStart_time()
    {
        return start_time;
    }

    public String getStart_time_toStr()
    {
        if (null != start_time)
        {
            return start_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setStart_time(Timestamp start_time)
    {
        this.start_time = start_time;
    }

    public Timestamp getEnd_time()
    {
        return end_time;
    }

    public String getEnd_time_toStr()
    {
        if (null != end_time)
        {
            return end_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setEnd_time(Timestamp end_time)
    {
        this.end_time = end_time;
    }

    public int getInterace_type()
    {
        return interace_type;
    }

    public void setInterace_type(int interace_type)
    {
        this.interace_type = interace_type;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

}
