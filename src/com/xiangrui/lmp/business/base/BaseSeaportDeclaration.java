package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Will
 *
 */
public class BaseSeaportDeclaration implements Serializable
{

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private int seq;

	/**
	 * 报关号码
	 */
	private String declaration_code;

	/**
	 * 所属口岸ID
	 */
	private int seaport_id;

	/**
	 * 对应包裹ID
	 */
	private int package_id;

	/**
	 * 使用标志 0：未使用 1：已使用
	 */
	private int use_flag;

	/**
	 * 创建时间
	 */
	private Date create_time;
	/**
	 * 创建者用户ID
	 */
	private int create_user_id;

	public int getSeq()
	{
		return seq;
	}

	public void setSeq(int seq)
	{
		this.seq = seq;
	}

	public String getDeclaration_code()
	{
		return declaration_code;
	}

	public void setDeclaration_code(String declaration_code)
	{
		this.declaration_code = declaration_code;
	}

	public int getSeaport_id()
	{
		return seaport_id;
	}

	public void setSeaport_id(int seaport_id)
	{
		this.seaport_id = seaport_id;
	}

	public int getPackage_id()
	{
		return package_id;
	}

	public void setPackage_id(int package_id)
	{
		this.package_id = package_id;
	}

	public int getUse_flag()
	{
		return use_flag;
	}

	public void setUse_flag(int use_flag)
	{
		this.use_flag = use_flag;
	}

	public Date getCreate_time()
	{
		return create_time;
	}

	public void setCreate_time(Date create_time)
	{
		this.create_time = create_time;
	}

	public int getCreate_user_id()
	{
		return create_user_id;
	}

	public void setCreate_user_id(int create_user_id)
	{
		this.create_user_id = create_user_id;
	}

}
