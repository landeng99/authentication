package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 禁运物品
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-11 上午10:59:30</p>
 */
public abstract class BaseProhibition implements Serializable
{

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    private int prohibition_id;
    /**
     * 大类名
     */
    private String class_name;
    /**
     * 子类名集合
     */
    private String child_cla_group;
    /**
     * 描述
     */
    private String description;
    /**
     * 图片路径
     */
    private String imageurl;
    public int getProhibition_id()
    {
        return prohibition_id;
    }
    public void setProhibition_id(int prohibition_id)
    {
        this.prohibition_id = prohibition_id;
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public String getImageurl()
    {
        return imageurl;
    }
    public void setImageurl(String imageurl)
    {
        this.imageurl = imageurl;
    }
    public String getClass_name()
    {
        return class_name;
    }
    public void setClass_name(String class_name)
    {
        this.class_name = class_name;
    }
    public String getChild_cla_group()
    {
        return child_cla_group;
    }
    public void setChild_cla_group(String child_cla_group)
    {
        this.child_cla_group = child_cla_group;
    }
    
}
