package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 海淘咨询信息
 * 
 * @author Will
 *
 */
public abstract class BaseConsultInfo implements Serializable
{
	/**
	 * 启用状态
	 */
	public static final int BANNER_STATUS_ENABLE = 1;
	/**
	 * 禁用状态
	 */
	public static final int BANNER_STATUS_DISABLE = 2;
	private static final long serialVersionUID = 1L;
	/**
	 * 自增长id
	 */
	private int seq_id;
	private String consult_title;
	private String consult_detail;
	private String display_image;
	private String forward_url;
	/**
	 * 状态:1启用，2禁用
	 */
	private int status;

	public int getSeq_id()
	{
		return seq_id;
	}

	public void setSeq_id(int seq_id)
	{
		this.seq_id = seq_id;
	}

	public String getConsult_title()
	{
		return consult_title;
	}

	public void setConsult_title(String consult_title)
	{
		this.consult_title = consult_title;
	}

	public String getConsult_detail()
	{
		return consult_detail;
	}

	public void setConsult_detail(String consult_detail)
	{
		this.consult_detail = consult_detail;
	}

	public String getDisplay_image()
	{
		return display_image;
	}

	public void setDisplay_image(String display_image)
	{
		this.display_image = display_image;
	}

	public String getForward_url()
	{
		return forward_url;
	}

	public void setForward_url(String forward_url)
	{
		this.forward_url = forward_url;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

}
