package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public abstract class BaseFrontUser implements Serializable
{

	/**
	 * 时间格式化
	 */
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	public static final int DEFAULT_INTEGRAL = 0;
	/**
	 * 普通用户类型
	 */
	public static final int USER_TYPE_OWN = 1;

	/**
	 * 同行用户类型
	 */
	public static final int USER_TYPE_PEER = 2;

	/**
	 * 账号已经被删除
	 */
	public static final int ISDELETED_YES = 1;

	/**
	 * 账号没有被删除
	 */
	public static final int ISDELETED_NO = 0;

	/**
	 * 账号状态为正常
	 */
	public static final int FRONT_USER_STATUS_NORMAL = 1;

	/**
	 * 账号状态为禁用
	 */
	public static final int FRONT_USER_STATUS_DISABLE = 2;

	/**
	 * 账号状态为未激活
	 */
	public static final int FRONT_USER_STATUS_NO_ACTIVATION = 0;

	/**
	 * 没有冻结
	 */
	public static final int IS_NOT_FREEZE_FRONT_USER = 0;

	/**
	 * 冻结中
	 */
	public static final int IS_FREEZE_FRONT_USER = 1;

	/**
	 * first_name默认值
	 */
	public static final String FIRST_NAME_INIT = "ellis";

	/**
	 * 是否冻结
	 */
	private int isfreeze;
	/**
	 * 用户id
	 */
	private int user_id;

	/**
	 * 用户账号
	 */
	private String account;

	/**
	 * 密码
	 */
	private String user_pwd;

	/**
	 * 用户姓名
	 */
	private String user_name;

	/**
	 * 手机号码
	 */
	private String mobile;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 短信验证码
	 */
	private String sms_code;

	/**
	 * 验证码过期时间
	 */
	private Timestamp sms_deadline;

	/**
	 * 账户余额
	 */
	private float balance;

	/**
	 * 可用余额
	 */
	private float able_balance;

	/**
	 * 冻结余额
	 */
	private float frozen_balance;

	/**
	 * 用户积分
	 */
	private int integral;

	/**
	 * 用户等级
	 */
	private int member_rate;

	/**
	 * 注册时间
	 */
	private Timestamp register_time;

	/**
	 * 用户类型，1：普通用户，2：同行用户
	 */
	private int user_type;

	/**
	 * first name
	 */
	private String first_name;

	/**
	 * last name
	 */
	private String last_name;

	/**
	 * 账号状态
	 */
	private int status;

	/**
	 * 逻辑删除
	 */
	private int isdeleted;

	/**
     * 
     */
	private String register_time_string;

	/**
	 * 银行卡号
	 */
	private String bank_card;
	
	/**
	 * 支付宝姓名
	 */
	private String alipayName;

	/**
	 * 支付宝号
	 */
	private String pay_treasure;

	/**
	 * 开户人
	 */
	private String account_holder;

	/**
	 * 开户银行
	 */
	private String bank_account;

	/**
	 * 业务名
	 */
	private String business_name;

	/**
	 * 默认支付方式
	 */
	private int default_pay_type;
	/**
	 * 虚拟预付款标志
	 */
	private String virtual_pay_flag;

	/**
	 * 用户微信绑定ID
	 */
	private String open_id;

	/**
	 * 个人头像
	 */
	private String person_icon;
	/**
	 * 快件渠道 0：不选择 1：选择
	 */
	private int select_express_channel;
	/**
	 * 推荐人用户ID
	 */
	private int recommand_user_id;
	/**
	 * 来源
	 */
	private String source;
	/**
	 * 个人推荐二维码URL
	 */
	private String person_qr_code;
	/**
	 * 个人推荐链接
	 */
	private String person_recommand_url;
	/**
	 * 是否已经赠送推荐优惠券：0：未赠送 1：已赠送
	 */
	private int is_sent_recommand_coupon;
	
	//推广链接
	private int pushLinkId;
	@Override
	public String toString() {
		return "id："+user_id+"-用户名："+user_name+"-类型："+user_type;
	}

	public int getIs_sent_recommand_coupon()
	{
		return is_sent_recommand_coupon;
	}

	public void setIs_sent_recommand_coupon(int is_sent_recommand_coupon)
	{
		this.is_sent_recommand_coupon = is_sent_recommand_coupon;
	}

	public int getRecommand_user_id()
	{
		return recommand_user_id;
	}

	public void setRecommand_user_id(int recommand_user_id)
	{
		this.recommand_user_id = recommand_user_id;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public String getPerson_qr_code()
	{
		return person_qr_code;
	}

	public void setPerson_qr_code(String person_qr_code)
	{
		this.person_qr_code = person_qr_code;
	}

	public String getPerson_recommand_url()
	{
		return person_recommand_url;
	}

	public void setPerson_recommand_url(String person_recommand_url)
	{
		this.person_recommand_url = person_recommand_url;
	}

	public int getSelect_express_channel()
	{
		return select_express_channel;
	}

	public void setSelect_express_channel(int select_express_channel)
	{
		this.select_express_channel = select_express_channel;
	}

	public String getPerson_icon()
	{
		return person_icon;
	}

	public void setPerson_icon(String person_icon)
	{
		this.person_icon = person_icon;
	}

	public String getOpen_id()
	{
		return open_id;
	}

	public void setOpen_id(String open_id)
	{
		this.open_id = open_id;
	}

	public String getVirtual_pay_flag()
	{
		return virtual_pay_flag;
	}

	public void setVirtual_pay_flag(String virtual_pay_flag)
	{
		this.virtual_pay_flag = virtual_pay_flag;
	}

	public int getDefault_pay_type()
	{
		return default_pay_type;
	}

	public void setDefault_pay_type(int default_pay_type)
	{
		this.default_pay_type = default_pay_type;
	}

	public String getAccount_holder()
	{
		return account_holder;
	}

	public void setAccount_holder(String account_holder)
	{
		this.account_holder = account_holder;
	}

	public String getBank_account()
	{
		return bank_account;
	}

	public void setBank_account(String bank_account)
	{
		this.bank_account = bank_account;
	}

	public String getBank_card()
	{
		return bank_card;
	}

	public void setBank_card(String bank_card)
	{
		this.bank_card = bank_card;
	}

	public String getAlipayName()
	{
		return alipayName;
	}

	public void setAlipayName(String alipayName)
	{
		this.alipayName = alipayName;
	}

	public void setRegister_time_string(String register_time_string)
	{
		this.register_time_string = register_time_string;
	}

	public String getPay_treasure()
	{
		return pay_treasure;
	}

	public void setPay_treasure(String pay_treasure)
	{
		this.pay_treasure = pay_treasure;
	}

	public int getIsdeleted()
	{
		return isdeleted;
	}

	public void setIsdeleted(int isdeleted)
	{
		this.isdeleted = isdeleted;
	}

	public int getUser_id()
	{
		return user_id;
	}

	public void setUser_id(int user_id)
	{
		this.user_id = user_id;
	}

	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}

	public String getUser_pwd()
	{
		return user_pwd;
	}

	public void setUser_pwd(String user_pwd)
	{
		this.user_pwd = user_pwd;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getSms_code()
	{
		return sms_code;
	}

	public void setSms_code(String sms_code)
	{
		this.sms_code = sms_code;
	}

	public Timestamp getSms_deadline()
	{
		return sms_deadline;
	}

	public String getSms_deadline_toStr()
	{
		if (null != sms_deadline)
		{
			return sms_deadline.toString().trim().replace(".0", "");
		}
		else
		{
			return "";
		}
	}

	public void setSms_deadline(Timestamp sms_deadline)
	{
		this.sms_deadline = sms_deadline;
	}

	public float getBalance()
	{
		return balance;
	}

	public void setBalance(float balance)
	{
		this.balance = balance;
	}

	public float getAble_balance()
	{
		return able_balance;
	}

	public void setAble_balance(float able_balance)
	{
		this.able_balance = able_balance;
	}

	public float getFrozen_balance()
	{
		return frozen_balance;
	}

	public void setFrozen_balance(float frozen_balance)
	{
		this.frozen_balance = frozen_balance;
	}

	public int getIntegral()
	{
		return integral;
	}

	public void setIntegral(int integral)
	{
		this.integral = integral;
	}

	public int getMember_rate()
	{
		return member_rate;
	}

	public void setMember_rate(int member_rate)
	{
		this.member_rate = member_rate;
	}

	public Timestamp getRegister_time()
	{
		return register_time;
	}

	public String getRegister_time_toStr()
	{
		if (null != register_time)
		{
			return register_time.toString().trim().replace(".0", "");
		}
		else
		{
			return "";
		}
	}

	public void setRegister_time(Timestamp register_time)
	{
		this.register_time = register_time;
		this.register_time_string = simpleDateFormat.format(register_time).toString();
	}

	public int getUser_type()
	{
		return user_type;
	}

	public void setUser_type(int user_type)
	{
		this.user_type = user_type;
	}

	public String getFirst_name()
	{
		return first_name;
	}

	public void setFirst_name(String first_name)
	{
		this.first_name = first_name;
	}

	public String getLast_name()
	{
		return last_name;
	}

	public void setLast_name(String last_name)
	{
		this.last_name = last_name;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public int getIsfreeze()
	{
		return isfreeze;
	}

	public void setIsfreeze(int isfreeze)
	{
		this.isfreeze = isfreeze;
	}

	public BaseFrontUser()
	{
		super();
	}

	public BaseFrontUser(int user_id, String user_name)
	{
		super();
		this.user_id = user_id;
		this.user_name = user_name;
	}

	public String getRegister_time_string()
	{
		return register_time_string;
	}

	public String getBusiness_name()
	{
		return business_name;
	}

	public void setBusiness_name(String business_name)
	{
		this.business_name = business_name;
	}

	public int getPushLinkId()
	{
		return pushLinkId;
	}

	public void setPushLinkId(int pushLinkId)
	{
		this.pushLinkId = pushLinkId;
	}
	
}
