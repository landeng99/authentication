package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public class BaseSender implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private int front_user_id;	// 对应前台创建者 id
	private String name;
	private String phone;
	private String address;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getFront_user_id() {
		return front_user_id;
	}
	public void setFront_user_id(int front_user_id) {
		this.front_user_id = front_user_id;
	}
	
	
}
