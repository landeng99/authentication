package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * 口岸列表
 * 
 * @author Windows10
 */
public class BaseDispatch implements Serializable
{

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	// 口岸ID
	private Integer sid;

	// 口岸列表名
	private String name;
	
	// 删除状态
	private Integer isDelete;

	// 创建时间
	private Timestamp createTime;

	// 备注
	private String description;
	
	// 口岸
	private String sname;
	
	//包裹数量
	private int count;
	
	//包裹ID
	private int package_id;
	
	private int seaport_id;
	
	private int slid;
	
	private String logistics_code;

	private String original_num;
	

	public Integer getSid()
	{
		return sid;
	}

	public void setSid(Integer sid)
	{
		this.sid = sid;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	

	public Timestamp getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Timestamp timestamp)
	{
		this.createTime = timestamp;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getIsDelete()
	{
		return isDelete;
	}

	public void setIsDelete(Integer isDelete)
	{
		this.isDelete = isDelete;
	}

	public String getSname()
	{
		return sname;
	}

	public void setSname(String sname)
	{
		this.sname = sname;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public int getPackage_id()
	{
		return package_id;
	}

	public void setPackage_id(int package_id)
	{
		this.package_id = package_id;
	}

	public int getSeaport_id()
	{
		return seaport_id;
	}

	public void setSeaport_id(int seaport_id)
	{
		this.seaport_id = seaport_id;
	}

	public int getSlid()
	{
		return slid;
	}

	public void setSlid(int slid)
	{
		this.slid = slid;
	}

	public String getLogistics_code()
	{
		return logistics_code;
	}

	public void setLogistics_code(String logistics_code)
	{
		this.logistics_code = logistics_code;
	}

	public String getOriginal_num()
	{
		return original_num;
	}

	public void setOriginal_num(String original_num)
	{
		this.original_num = original_num;
	}
	
	@Override
	public String toString()
	{
		return "BaseDispatch [id=" + id + ", sid=" + sid + ", name=" + name
				+ ", isDelete=" + isDelete + ", createTime=" + createTime
				+ ", description=" + description + ", sname=" + sname
				+ ", count=" + count + ", package_id=" + package_id
				+ ", seaport_id=" + seaport_id + ", slid=" + slid + "]";
	}

}
