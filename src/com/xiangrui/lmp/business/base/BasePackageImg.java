package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public abstract class BasePackageImg implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 图片id
     */
    private int img_id;

    /**
     * 退运的包裹id
     */
    private int package_id;

    /**
     * 图片路径
     */
    private String img_path;

    /**
     * 图片类型
     */
    private int img_type;

    /**
     * 购物小票
     */
    public final static int IMG_TICKET_TYPE = 1;

    /**
     * 拍照
     */
    public final static int IMG_RETURN_TYPE = 2;

    /**
     * 索赔
     */
    public final static int IMG_CLAIMS_TYPE = 3;

    /**
     * 拍照
     */
    public final static int IMG_PICTRUE_TYPE = 4;
    
    /**
     * 晒单
     */
    public final static int IMG_SHARE_TYPE = 5;

    public int getImg_id()
    {
        return img_id;
    }

    public void setImg_id(int img_id)
    {
        this.img_id = img_id;
    }

    public int getPackage_id()
    {
        return package_id;
    }

    public void setPackage_id(int package_id)
    {
        this.package_id = package_id;
    }

    public String getImg_path()
    {
        return img_path;
    }

    public void setImg_path(String img_path)
    {
        this.img_path = img_path;
    }

    public int getImg_type()
    {
        return img_type;
    }

    public void setImg_type(int img_type)
    {
        this.img_type = img_type;
    }

    public static long getSerialversionuid()
    {
        return serialVersionUID;
    }

}
