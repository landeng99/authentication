package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

public class ExpressNumLotConnectBean extends BaseBean implements Serializable
{
	private static final long serialVersionUID = -4402118260883718148L;
	private int seq_id;
	private String express_num;
	private String batch_lot;
	private String author;
	private Timestamp create_ime;

	public int getSeq_id()
	{
		return seq_id;
	}

	public void setSeq_id(int seq_id)
	{
		this.seq_id = seq_id;
	}

	public String getExpress_num()
	{
		return express_num;
	}

	public void setExpress_num(String express_num)
	{
		this.express_num = express_num;
	}

	public String getBatch_lot()
	{
		return batch_lot;
	}

	public void setBatch_lot(String batch_lot)
	{
		this.batch_lot = batch_lot;
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor(String author)
	{
		this.author = author;
	}

	public Timestamp getCreate_ime()
	{
		return create_ime;
	}

	public void setCreate_ime(Timestamp create_ime)
	{
		this.create_ime = create_ime;
	}
}
