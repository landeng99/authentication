package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public class BaseSeaport implements Serializable
{

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/**
	 * 口岸id
	 */
	private int sid;

	/**
	 * 口岸名
	 */
	private String sname;

	/**
	 * 口岸编码
	 */
	private String scode;

	/**
	 * 口岸分类
	 */
	private String stype;

	/**
	 * 口岸限制用户信息数
	 */
	private int userinfo_limit;

	/**
	 * 逻辑删除状态
	 */
	private int isdeleted;
	/**
	 * 检查身份证照片
	 */
	private int ischeck_idcard_img;
	
	/**
	 * 快件口岸 0：不是 1：是
	 */
	private int express_seaport;
	
	/**
	 * 出库打印模板路径
	 */
	private String output_print_template;
	
	private int leave_warning_count;
	
	public int getLeave_warning_count()
	{
		return leave_warning_count;
	}

	public void setLeave_warning_count(int leave_warning_count)
	{
		this.leave_warning_count = leave_warning_count;
	}

	public String getOutput_print_template()
	{
		return output_print_template;
	}

	public void setOutput_print_template(String output_print_template)
	{
		this.output_print_template = output_print_template;
	}

	public int getExpress_seaport()
	{
		return express_seaport;
	}

	public void setExpress_seaport(int express_seaport)
	{
		this.express_seaport = express_seaport;
	}

	public int getIscheck_idcard_img()
	{
		return ischeck_idcard_img;
	}

	public void setIscheck_idcard_img(int ischeck_idcard_img)
	{
		this.ischeck_idcard_img = ischeck_idcard_img;
	}

	public int getSid()
	{
		return sid;
	}

	public void setSid(int sid)
	{
		this.sid = sid;
	}

	public String getSname()
	{
		return sname;
	}

	public void setSname(String sname)
	{
		this.sname = sname;
	}

	public String getScode()
	{
		return scode;
	}

	public void setScode(String scode)
	{
		this.scode = scode;
	}

	public String getStype()
	{
		return stype;
	}

	public void setStype(String stype)
	{
		this.stype = stype;
	}

	public int getUserinfo_limit()
	{
		return userinfo_limit;
	}

	public void setUserinfo_limit(int userinfo_limit)
	{
		this.userinfo_limit = userinfo_limit;
	}

	public int getIsdeleted()
	{
		return isdeleted;
	}

	public void setIsdeleted(int isdeleted)
	{
		this.isdeleted = isdeleted;
	}

}
