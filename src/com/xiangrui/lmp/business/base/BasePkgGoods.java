package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 商品关税
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午10:00:05
 *         </p>
 */
public class BasePkgGoods implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     * 物品id
     */
    private int goods_id;

    /**
     * 包裹id
     */
    private int package_id;

    /**
     * 商品名称
     */
    private String goods_name;

    /**
     * 商品申报类别
     */
    private int goods_type_id;

    /**
     * 价格
     */
    private float price;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 关税费用
     */
    private float customs_cost;

    /**
     * 分箱前包裹id
     */
    private int before_pkg_id;

    /**
     * 分箱后包裹id
     */
    private int after_pkg_id;

    /**
     * 物品单位
     */
    private String unit;

    /**
     * 税率
     */
    private float tax_rate;

    /**
     * 税款总额
     */
    private float tax_payment_total;

    /**
     * 已支付的税款总额
     */
    private float paid;

    /**
     * 关税读取数量
     */
    private float goods_tax_number;

    /**
     * 物品规格
     */
    private String spec;

    /**
     * 物品品牌
     */
    private String brand;

    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public float getTax_rate()
    {
        return tax_rate;
    }

    public void setTax_rate(float tax_rate)
    {
        this.tax_rate = tax_rate;
    }

    public float getTax_payment_total()
    {
        return tax_payment_total;
    }

    public void setTax_payment_total(float tax_payment_total)
    {
        this.tax_payment_total = tax_payment_total;
    }

    public float getGoods_tax_number()
    {
        return goods_tax_number;
    }

    public void setGoods_tax_number(float goods_tax_number)
    {
        this.goods_tax_number = goods_tax_number;
    }

    public int getGoods_id()
    {
        return goods_id;
    }

    public void setGoods_id(int goods_id)
    {
        this.goods_id = goods_id;
    }

    public int getPackage_id()
    {
        return package_id;
    }

    public void setPackage_id(int package_id)
    {
        this.package_id = package_id;
    }

    public String getGoods_name()
    {
        return goods_name;
    }

    public void setGoods_name(String goods_name)
    {
        this.goods_name = goods_name;
    }

    public int getGoods_type_id()
    {
        return goods_type_id;
    }

    public void setGoods_type_id(int goods_type_id)
    {
        this.goods_type_id = goods_type_id;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public float getCustoms_cost()
    {
        return customs_cost;
    }

    public void setCustoms_cost(float customs_cost)
    {
        this.customs_cost = customs_cost;
    }

    public int getBefore_pkg_id()
    {
        return before_pkg_id;
    }

    public void setBefore_pkg_id(int before_pkg_id)
    {
        this.before_pkg_id = before_pkg_id;
    }

    public int getAfter_pkg_id()
    {
        return after_pkg_id;
    }

    public void setAfter_pkg_id(int after_pkg_id)
    {
        this.after_pkg_id = after_pkg_id;
    }

    public String getSpec()
    {
        return spec;
    }

    public void setSpec(String spec)
    {
        this.spec = spec;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public float getPaid()
    {
        return paid;
    }

    public void setPaid(float paid)
    {
        this.paid = paid;
    }

}
