package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;

public class BasePackage extends BaseBean implements Serializable
{
	/**
	 * 体积重量常数
	 */
	private static final int BULKFACTOR_CONSTANT = 6000;

	/**
	 * 物流状态：0待入库
	 */
	public static final int LOGISTICS_STORE_WAITING = 0;
	
	/**
     * 物流状态:-2包裹已删除，待处理包裹变成正常时，先删除再增加，被删除的变成 此状态
     */
    public static final int LOGISTICS_DELETED = -2;

	/**
	 * 物流状态:-1包裹异常，待入库包裹 在仓库中查询不到对应的数据
	 */
	public static final int LOGISTICS_UNUSUALLY = -1;

	/**
	 * 物流状态：已入库
	 */
	public static final int LOGISTICS_STORAGED = 1;

	/**
	 * 物流状态：待发货
	 */
	public static final int LOGISTICS_SEND_WAITING = 2;

	/**
	 * 物流状态：已出库
	 */
	public static final int LOGISTICS_SENT_ALREADY = 3;

	/**
	 * 物流状态：已空运
	 */
	public static final int LOGISTICS_AIRLIFT_ALREADY = 4;

	/**
	 * 物流状态：待清关
	 */
	public static final int LOGISTICS_CUSTOMS_WAITING = 5;

	/**
	 * 物流状态：已清关并已派件中
	 */
	public static final int LOGISTICS_CUSTOMS_ALREADY = 7;

	/**
	 * 物流状态：已签收
	 */
	public static final int LOGISTICS_SIGN_IN = 9;

	/**
	 * 物流状态：废弃
	 */
	public static final int LOGISTICS_DISCARD = 20;

	/**
	 * 物流状态：退货
	 */
	public static final int LOGISTICS_RETURN = 21;

	/**
	 * 待支付
	 */
	public static final int PAYMENT_UNPAID = 1;
	public static final int PAYMENT_FREIGHT_UNPAID = 1; // 运费未支付

	/**
	 * 已支付
	 */
	public static final int PAYMENT_PAID = 2;
	public static final int PAYMENT_FREIGHT_PAID = 2; // 运费已支付

	/**
	 * 关税待支付
	 */
	public static final int PAYMENT_CUSTOM_UNPAID = 3;

	/**
	 * 关税已支付
	 */
	public static final int PAYMENT_CUSTOM_PAID = 4;

	/**
	 * 已支付（预扣）
	 */
	public static final int PAYMENT_VIRTUAL_PAID = 5;

	/**
	 * 预扣失败
	 */
	public static final int PAYMENT_VIRTUAL_PAID_FAILURE = 6;

	/**
	 * 已支付（预扣）并入帐，待结算处理
	 */
	public static final int PAYMENT_VIRTUAL_PAID_HANDLE = 7;

	/**
	 * 退货：待支付
	 */
	public static final int RET_PAYMENT_UNPAID = 1;

	/**
	 * 退货：已支付
	 */
	public static final int RET_PAYMENT_PAID = 2;

	/**
	 * 订阅未提交
	 */
	public static final int SUBSCRIBE_UNSUBMIT = 1;

	/**
	 * 订阅状态：已订阅
	 */
	public static final int SUBSCRIBE_SUBMIT = 2;

	/**
	 * 订阅：取消
	 */
	public static final int SUBSCRIBE_CANCEL = 3;

	/**
	 * 支付方式1：快速出库（账户余额自动支付）
	 */
	public static final int PAY_TYPE_BALANCE = 1;

	/**
	 * 支付方式2：网上支付
	 */
	public static final int PAY_TYPE_ONLINE = 2;
	
	/**
	 * 支付方式3：现金支付
	 */
	public static final int PAY_TYPE_CASH = 3;

	/**
	 * 包裹可用
	 */
	public static final int PACKAGE_ENABLE = 1;

	/**
	 * 包裹不可用
	 */
	public static final int PACKAGE_DISABLE = 2;

	/**
	 * 包裹未抵达 仓库
	 */
	public static final int STORE_PACKAGE_NOT_ARRIVED = 0;

	/**
	 * 包裹已抵达仓库
	 */
	public static final int STORE_PACKAGE_ARRIVED = 1;

	/**
	 * 待核算
	 */
	public static final int ACCOUNTING_UNDO = 1;

	/**
	 * 已核算
	 */
	public static final int ACCOUNTING_DONE = 2;

	/**
	 * 包裹未打印
	 */
	public static final int PACKAGE__NO_PRINT = 0;

	/**
	 * 包裹已打印
	 */
	public static final int PACKAGE__YES_PRINT = 1;

	@Override
	public String toString()
	{
		return "BasePackage [package_id=" + package_id + ", user_id=" + user_id + ", logistics_code=" + logistics_code + ", original_num=" + original_num + ", weight=" + weight + ", createTime=" + createTime + ", status=" + status + ", pay_type=" + pay_type + ", transport_cost=" + transport_cost + ", address_id=" + address_id + ", osaddr_id=" + osaddr_id + ", freight=" + freight + ", total_worth=" + total_worth
				+ ", customs_cost=" + customs_cost + ", house_id=" + house_id + ", remark=" + remark + ", pkg_class_id=" + pkg_class_id + ", ems_code=" + ems_code + ", return_code=" + return_code + ", return_cost=" + return_cost + ", pay_status=" + pay_status + ", retpay_status=" + retpay_status + ", tax_number=" + tax_number + ", tax_status=" + tax_status
				+ ", width=" + width + ", height=" + height + ", length=" + length + ", parent_id=" + parent_id + ", redelivery=" + redelivery + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 包裹id
	 */
	private int package_id;

	/**
	 * 税单单号
	 */
	private String tax_number;

	/**
	 * 包裹创建用户id
	 */
	private int user_id;

	/**
	 * 关联单号，在购物网站购买商品后，商家发货后的物流单号
	 */
	private String original_num;

	/**
	 * 转运费用支付方式
	 */
	private int pay_type;

	/**
	 * 转运费用，运费+增值服务
	 */
	private float transport_cost;

	/**
	 * 公司运单号
	 */
	private String logistics_code;

	/**
	 * 包裹状态：-1：异常，0：待入库，1：已入库，2：待发货，3：已出库，4：空运中，5：待清关，6：清关中，7：已清关，8：派件中，9：已收货； 20：废弃，21：退货
	 */
	private int status;

	/**
	 * 收货地址id
	 */
	private int address_id;

	/**
	 * 海外仓库地址
	 */
	private int osaddr_id;
	
	/**
	 * 报价单id
	 */
	private int quota_id;

	/**
	 * 包裹重量
	 */
	private float weight;

	/**
	 * 由包裹重量计算出的运费
	 */
	private float freight;

	/**
	 * 申报总价值
	 */
	private float total_worth;

	/**
	 * 关税费用
	 */
	private float customs_cost;

	/**
	 * 存放仓库id
	 */
	private int house_id;

	/**
	 * 备注
	 */
	private String remark;

	/**
     * 
     */
	private int pkg_class_id;

	/**
	 * 派送单号
	 */
	private String ems_code;
	
	/**
	 * 快递公司编码
	 */
	private String com;
	
	

	public String getCom() {
		return com;
	}

	public void setCom(String com) {
		this.com = com;
	}

	/**
	 * 退货单号
	 */
	private String return_code;

	/**
	 * 退货费用
	 */
	private float return_cost;

	/**
	 * 包裹支付状态：1，未支付，2：已支付；3：关税未支付，4关税已支付
	 */
	private int pay_status;
	
	private int pay_status_custom; // 关税支付状态 0：默认 3：关税未支付，4关税已支付
	
	private int pay_status_freight; // 运费支付状态：0默认 1，未支付，2：已支付

	/**
	 * 退货支付状态
	 */
	private int retpay_status;

	/**
	 * 关税审核状态
	 */
	private String tax_status;

	/**
	 * 父包裹id
	 */
	private String parent_id;

	/**
	 * 重新投递产生的包裹编号
	 */
	private String redelivery;

	/**
	 * 是否可用：1可用，2不可用
	 */
	private int enable;

	/**
	 * 包裹打印：0未打印，1已打印
	 */
	private int pkg_print;

	/**
	 * 创建包裹时间
	 */
	private Timestamp createTime;
	/**
	 * 宽度
	 */
	private float width;
	/**
	 * 高度
	 */
	private float height;
	/**
	 * 长度
	 */
	private float length;

	/**
	 * 到库状态:0,未到库，1已到库
	 */
	private int arrive_status;

	/**
	 * 到库时间
	 */
	private Timestamp arrive_time;

	/**
	 * 实际重量重量
	 */
	private float actual_weight;

	/**
	 * 核算状态
	 */
	private int accounting_status;

	/**
	 * 同行账户 运费单价
	 */
	private float price;
	/**
	 * 物流单号
	 */
	private String express_num;
	/**
	 * 导入批次号
	 */
	private String batch_lot;
	/**
	 * 待合箱处理标志
	 */
	private String waitting_merge_flag;

	/**
	 * 包裹到库显示标志
	 */
	private String arrived_package_display_flag;
	/**
	 * 异常包裹标志
	 */
	private String exception_package_flag;
	/**
	 * 同行导入账单标志
	 */
	private String memberbill_import_flag;
	/**
	 * 超重分箱标志
	 */
	private String over_weight_split_pkg_flag;
	/**
	 * 预付款金额
	 */
	private float virtual_pay;

	/**
	 * 快件包裹 0：不是 1：是（原来的字段意思）
	 * 渠道：渠道：0默认；1A渠道；2B渠道；3C渠道；4D渠道；5E渠道 （现在的字段意思）
	 */
	private int express_package;
	
	/**
	 * 晒单审核状态1：未审核，2：审核通过，3：审核拒绝
	 */
	private int shareOrderApprovalStatus;

	/**
	 * 需要取出包裹标志
	 */
	private String need_check_out_flag;
	
	/**
	 * 包裹所属口岸ID，出库扫描时用到
	 */
	private int seaport_id;
	
	public int getSeaport_id()
	{
		return seaport_id;
	}

	public void setSeaport_id(int seaport_id)
	{
		this.seaport_id = seaport_id;
	}

	public String getNeed_check_out_flag()
	{
		return need_check_out_flag;
	}

	public void setNeed_check_out_flag(String need_check_out_flag)
	{
		this.need_check_out_flag = need_check_out_flag;
	}

	public int getShareOrderApprovalStatus()
	{
		return shareOrderApprovalStatus;
	}

	public void setShareOrderApprovalStatus(int shareOrderApprovalStatus)
	{
		this.shareOrderApprovalStatus = shareOrderApprovalStatus;
	}

	public int getExpress_package()
	{
		return express_package;
	}

	public void setExpress_package(int express_package)
	{
		this.express_package = express_package;
	}

	public float getVirtual_pay()
	{
		return virtual_pay;
	}

	public void setVirtual_pay(float virtual_pay)
	{
		this.virtual_pay = virtual_pay;
	}

	public String getOver_weight_split_pkg_flag()
	{
		return over_weight_split_pkg_flag;
	}

	public void setOver_weight_split_pkg_flag(String over_weight_split_pkg_flag)
	{
		this.over_weight_split_pkg_flag = over_weight_split_pkg_flag;
	}

	public String getMemberbill_import_flag()
	{
		return memberbill_import_flag;
	}

	public void setMemberbill_import_flag(String memberbill_import_flag)
	{
		this.memberbill_import_flag = memberbill_import_flag;
	}

	public String getException_package_flag()
	{
		return exception_package_flag;
	}

	public void setException_package_flag(String exception_package_flag)
	{
		this.exception_package_flag = exception_package_flag;
	}

	public String getArrived_package_display_flag()
	{
		return arrived_package_display_flag;
	}

	public void setArrived_package_display_flag(String arrived_package_display_flag)
	{
		this.arrived_package_display_flag = arrived_package_display_flag;
	}

	public String getWaitting_merge_flag()
	{
		return waitting_merge_flag;
	}

	public void setWaitting_merge_flag(String waitting_merge_flag)
	{
		this.waitting_merge_flag = waitting_merge_flag;
	}

	public String getTax_status()
	{
		return tax_status;
	}

	public void setTax_status(String tax_status)
	{
		this.tax_status = tax_status;
	}

	public int getPackage_id()
	{
		return package_id;
	}

	public void setPackage_id(int package_id)
	{
		this.package_id = package_id;
	}

	public int getUser_id()
	{
		return user_id;
	}

	public void setUser_id(int user_id)
	{
		this.user_id = user_id;
	}

	public String getOriginal_num()
	{
		return original_num;
	}

	public void setOriginal_num(String original_num)
	{
		this.original_num = original_num;
	}

	public int getPay_type()
	{
		return pay_type;
	}

	public void setPay_type(int pay_type)
	{
		this.pay_type = pay_type;
	}

	public float getTransport_cost()
	{
		return transport_cost;
	}

	public void setTransport_cost(float transport_cost)
	{
		this.transport_cost = transport_cost;
	}

	public String getLogistics_code()
	{
		return logistics_code;
	}

	public void setLogistics_code(String logistics_code)
	{
		this.logistics_code = logistics_code;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public int getAddress_id()
	{
		return address_id;
	}

	public void setAddress_id(int address_id)
	{
		this.address_id = address_id;
	}

	public int getOsaddr_id()
	{
		return osaddr_id;
	}

	public void setOsaddr_id(int osaddr_id)
	{
		this.osaddr_id = osaddr_id;
	}

	public float getWeight()
	{
		return weight;
	}

	public void setWeight(float weight)
	{
		this.weight = weight;
	}

	public float getFreight()
	{
		return freight;
	}

	public void setFreight(float freight)
	{
		this.freight = freight;
	}

	public float getTotal_worth()
	{
		return total_worth;
	}

	public void setTotal_worth(float total_worth)
	{
		this.total_worth = total_worth;
	}

	public float getCustoms_cost()
	{
		return customs_cost;
	}

	public void setCustoms_cost(float customs_cost)
	{
		this.customs_cost = customs_cost;
	}

	public int getHouse_id()
	{
		return house_id;
	}

	public float getPrice()
	{
		return price;
	}

	public void setPrice(float price)
	{
		this.price = price;
	}

	public void setHouse_id(int house_id)
	{
		this.house_id = house_id;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public int getPkg_class_id()
	{
		return pkg_class_id;
	}

	public void setPkg_class_id(int pkg_class_id)
	{
		this.pkg_class_id = pkg_class_id;
	}

	public String getEms_code()
	{
		return ems_code;
	}

	public void setEms_code(String ems_code)
	{
		this.ems_code = ems_code;
	}

	public String getReturn_code()
	{
		return return_code;
	}

	public void setReturn_code(String return_code)
	{
		this.return_code = return_code;
	}

	public float getReturn_cost()
	{
		return return_cost;
	}

	public void setReturn_cost(float return_cost)
	{
		this.return_cost = return_cost;
	}

	public int getPay_status()
	{
		return pay_status;
	}

	public void setPay_status(int pay_status)
	{
		this.pay_status = pay_status;
	}

	public int getPay_status_custom() {
		return pay_status_custom;
	}

	public void setPay_status_custom(int pay_status_custom) {
		this.pay_status_custom = pay_status_custom;
	}

	public int getPay_status_freight() {
		return pay_status_freight;
	}

	public void setPay_status_freight(int pay_status_freight) {
		this.pay_status_freight = pay_status_freight;
	}

	public int getRetpay_status()
	{
		return retpay_status;
	}

	public void setRetpay_status(int retpay_status)
	{
		this.retpay_status = retpay_status;
	}

	public float getActual_weight()
	{
		return actual_weight;
	}

	public void setActual_weight(float actual_weight)
	{
		this.actual_weight = actual_weight;
	}

	public BasePackage()
	{
		super();
	}

	public BasePackage(int package_id, String original_num, int user_id)
	{
		super();
		this.package_id = package_id;
		this.original_num = original_num;
		this.user_id = user_id;
	}

	public Timestamp getCreateTime()
	{
		return createTime;
	}

	public String getCreateTime_toStr()
	{
		if (null != createTime)
		{
			return createTime.toString().trim().replace(".0", "");
		}
		else
		{
			return "";
		}
	}

	public void setCreateTime(Timestamp createTime)
	{
		this.createTime = createTime;
	}

	public String getTax_number()
	{
		return tax_number;
	}

	public void setTax_number(String tax_number)
	{
		this.tax_number = tax_number;
	}

	public float getWidth()
	{
		return width;
	}

	public float getHeight()
	{
		return height;
	}

	public float getLength()
	{
		return length;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}

	public void setLength(float length)
	{
		this.length = length;
	}

	public String getParent_id()
	{
		return parent_id;
	}

	public void setParent_id(String parent_id)
	{
		this.parent_id = parent_id;
	}

	public String getRedelivery()
	{
		return redelivery;
	}

	public void setRedelivery(String redelivery)
	{
		this.redelivery = redelivery;
	}

	public int getEnable()
	{
		return enable;
	}

	public void setEnable(int enable)
	{
		this.enable = enable;
	}

	public int getArrive_status()
	{
		return arrive_status;
	}

	public void setArrive_status(int arrive_status)
	{
		this.arrive_status = arrive_status;
	}

	public int getPkg_print()
	{
		return pkg_print;
	}

	public void setPkg_print(int pkg_print)
	{
		this.pkg_print = pkg_print;
	}

	public int getAccounting_status()
	{
		return accounting_status;
	}

	public void setAccounting_status(int accounting_status)
	{
		this.accounting_status = accounting_status;
	}

	public float getBulkfactor()
	{
		float bulkfactor = getLength() * getWidth() * getHeight() / BULKFACTOR_CONSTANT;

		bulkfactor = (float) (Math.round(bulkfactor * 10000)) / 10000;

		return bulkfactor;
	}

	public Timestamp getArrive_time()
	{
		return arrive_time;
	}

	public void setArrive_time(Timestamp arrive_time)
	{
		this.arrive_time = arrive_time;
	}

	public String getExpress_num()
	{
		return express_num;
	}

	public void setExpress_num(String express_num)
	{
		this.express_num = express_num;
	}

	public int getQuota_id() {
		return quota_id;
	}

	public void setQuota_id(int quota_id) {
		this.quota_id = quota_id;
	}

	public String getBatch_lot()
	{
		return batch_lot;
	}

	public void setBatch_lot(String batch_lot)
	{
		this.batch_lot = batch_lot;
	}
	
	/**
	 * 返回此包裹是否已经支付完成了(不通过 pay_status)
	 * @param pkg
	 * @return
	 */
	public static boolean isPaid(BasePackage pkg){
		if( null==pkg )
			return false;
		
		return PAYMENT_CUSTOM_PAID==pkg.getPay_status_custom()
		    && PAYMENT_FREIGHT_PAID==pkg.getPay_status_freight();
	}
	
	/**
	 * 设置包裹状态为已支付(通过设置运费与关税标识)
	 * @param pkg
	 * @return
	 */
	public static BasePackage setPaid(BasePackage pkg){
		if( null==pkg )
			return null;
		
		pkg.setPay_status(Pkg.PAYMENT_PAID);
		pkg.setPay_status_custom(Pkg.PAYMENT_CUSTOM_PAID);
		pkg.setPay_status_freight(Pkg.PAYMENT_FREIGHT_PAID);
		return pkg;
	}
}
