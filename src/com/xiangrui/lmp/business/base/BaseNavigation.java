package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 导航栏
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-9 下午4:32:25
 *         </p>
 */
public abstract class BaseNavigation implements Serializable
{

    /**
     * 启用状态
     */
    public static final int NAVIGATION_STATUS_ENABLE = 1;

    /**
     * 禁用状态
     */
    public static final int NAVIGATION_STATUS_DISABLE = 2;

    /**
     * 第一级导航栏的 id_framework 值
     */
    public static final String NAVIGATION_ID_FRAMEWORK = "0";

    private static final long serialVersionUID = 1L;
    /**
     * 自增id
     */
    private int nav_id;
    /**
     * 状态;1,启用;2,禁用
     */
    private int status;
    /**
     * 父类id
     */
    private int parent_id;
    /**
     * 导航栏链接
     */
    private String url;
    /**
     * 导航栏名称
     */
    private String nav_name;
    /**
     * 排序,分类使用
     */
    private String id_framework;

    public int getNav_id()
    {
        return nav_id;
    }

    public void setNav_id(int nav_id)
    {
        this.nav_id = nav_id;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public int getParent_id()
    {
        return parent_id;
    }

    public void setParent_id(int parent_id)
    {
        this.parent_id = parent_id;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getNav_name()
    {
        return nav_name;
    }

    public void setNav_name(String nav_name)
    {
        this.nav_name = nav_name;
    }

    public String getId_framework()
    {
        return id_framework;
    }

    public void setId_framework(String id_framework)
    {
        this.id_framework = id_framework;
    }

}
