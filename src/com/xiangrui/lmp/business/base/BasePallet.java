package com.xiangrui.lmp.business.base;

import java.sql.Timestamp;

public class BasePallet
{

    /**
     * 托盘状态:已出库
     */
    public static final int PALLET_SENT_ALREADY = 1;
    /**
     * 托盘状态:空运中
     */
    public static final int PALLET_AIRLIFT_ALREADY = 2;

    /**
     * 托盘状态:待清关
     */
    public static final int PALLET_CUSTOMS_ALREADY = 3;

    /**
     * 托盘ID
     */
    private int pallet_id;

    /**
     * 托盘单号
     */
    private String pallet_code;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 创建时间
     */
    private Timestamp create_time;

    /**
     * 创建用户
     */
    private int create_user;

    /**
     * 托盘状态：0未出库，1、已出库，2、空运中，3已清关
     */
    private int status;

    private int pick_id;

    private Timestamp declaration_time;
	/**
	 * 所属出库ID
	 */
	private int output_id;

	public int getOutput_id()
	{
		return output_id;
	}

	public void setOutput_id(int output_id)
	{
		this.output_id = output_id;
	}

    public int getPallet_id()
    {
        return pallet_id;
    }

    public void setPallet_id(int pallet_id)
    {
        this.pallet_id = pallet_id;
    }

    public String getPallet_code()
    {
        return pallet_code;
    }

    public void setPallet_code(String pallet_code)
    {
        this.pallet_code = pallet_code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Timestamp getCreate_time()
    {
        return create_time;
    }

    public String getCreate_time_toStr()
    {
        if (null != create_time)
        {
            return create_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setCreate_time(Timestamp create_time)
    {
        this.create_time = create_time;
    }

    public int getCreate_user()
    {
        return create_user;
    }

    public void setCreate_user(int create_user)
    {
        this.create_user = create_user;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public int getPick_id()
    {
        return pick_id;
    }

    public void setPick_id(int pick_id)
    {
        this.pick_id = pick_id;
    }

    public Timestamp getDeclaration_time()
    {
        return declaration_time;
    }

    public String getDeclaration_time_toStr()
    {
        if (null != declaration_time)
        {
            return declaration_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setDeclaration_time(Timestamp declaration_time)
    {
        this.declaration_time = declaration_time;
    }

}
