package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 包裹记录
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-7-2 下午2:39:38
 *         </p>
 */
public abstract class BasePkgLog implements Serializable
{

    private static final long serialVersionUID = 1L;

    /**
     * ‘仓库id’,
     */
    private int log_id;
    /**
     * ‘包裹id’,
     */
    private int package_id;
    /**
     * ‘更新时间’,
     */
    private Timestamp operate_time;
    /**
     * ‘操作之后的包裹状态’,
     */
    private int operated_status;
    /**
     * ‘操作人’,
     */
    private int operate_user;
    /**
     * ‘描述’,
     */
    private String description;

	public int getLog_id()
    {
        return log_id;
    }

    public void setLog_id(int log_id)
    {
        this.log_id = log_id;
    }

    public int getPackage_id()
    {
        return package_id;
    }

    public void setPackage_id(int package_id)
    {
        this.package_id = package_id;
    }

    public Timestamp getOperate_time()
    {
        return operate_time;
    }

    public String getOperate_time_toStr()
    {
        if (null != operate_time)
        {
            return operate_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setOperate_time(Timestamp operate_time)
    {
        this.operate_time = operate_time;
    }

    public int getOperated_status()
    {
        return operated_status;
    }

    public void setOperated_status(int operated_status)
    {
        this.operated_status = operated_status;
    }

    public int getOperate_user()
    {
        return operate_user;
    }

    public void setOperate_user(int operate_user)
    {
        this.operate_user = operate_user;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

}
