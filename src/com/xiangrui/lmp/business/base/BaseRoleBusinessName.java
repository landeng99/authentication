package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public class BaseRoleBusinessName implements Serializable
{
	private int role_id;
	private String business_name;

	public int getRole_id()
	{
		return role_id;
	}

	public void setRole_id(int role_id)
	{
		this.role_id = role_id;
	}

	public String getBusiness_name()
	{
		return business_name;
	}

	public void setBusiness_name(String business_name)
	{
		this.business_name = business_name;
	}

}
