package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 增值服务关联会员的价格关系表
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-19 上午11:22:42
 * </p>
 */
public abstract class BaseMemberAttach implements Serializable
{

    private static final long serialVersionUID = 1L;

    /**
     * 增值id
     */
    private int attach_id;
    /**
     * 会员id
     */
    private int rate_id;
    /**
     * 价格
     */
    private float service_price;

    public int getAttach_id()
    {
        return attach_id;
    }

    public void setAttach_id(int attach_id)
    {
        this.attach_id = attach_id;
    }

    public int getRate_id()
    {
        return rate_id;
    }

    public void setRate_id(int rate_id)
    {
        this.rate_id = rate_id;
    }

    public float getService_price()
    {
        return service_price;
    }

    public void setService_price(float service_price)
    {
        this.service_price = service_price;
    }

}
