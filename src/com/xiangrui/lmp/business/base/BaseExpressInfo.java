package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

public class BaseExpressInfo implements Serializable
{

    /**
     * 签收状态1：已签收
     */
    public static final String ISCHECK_YES = "1";

    /**
     * 订阅状态0：修改后未订阅
     */
    public static final int SUBSCRIBE_STATUS_INIT = 0;

    /**
     * 订阅状态1：订阅成功
     */
    public static final int SUBSCRIBE_STATUS_SUCCESS = 1;

    /**
     * 订阅状态2：订阅失败
     */
    public static final int SUBSCRIBE_STATUS_FAIL = 2;
    
    
    /**
     * 订阅状态3：导入后 订阅中
     */
    public static final int SUBSCRIBE_STATUS_SUBSCRIBE = 3;

    /**
     * 有效状态：1有效
     */
    public static final int ABLE_YES = 1;

    /**
     * 有效状态：，2无效
     */
    public static final int ABLE_NO = 2;

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private int express_info_id;

    /**
     * 物流跟踪号
     */
    private String logistics_code;

    /**
     * 订阅状态
     */
    private int subscribe_status;

    /**
     * 监控状态:polling:监控中，shutdown:结束，abort:中止，updateall：重新推送。
     * 其中当快递单为已签收时status=shutdown，当message为“3天查询无记录”或“60天无变化时”status= abort
     * ，对于stuatus=abort的状度，需要增加额外的处理逻辑，详见本节最后的说明
     */
    private String monitoring_status;

    /**
     * 包括got、sending、check三个状态，由于意义不大，已弃用，请忽略
     */
    private String billstatus;
    /**
     * 监控状态相关消息，如:3天查询无记录，60天无变化
     */
    private String monitoring_message;

    /**
     * 查询结果消息
     */
    private String message;
    /**
     * 快递单当前签收状态
     */
    private String state;
    /**
     * 通讯状态
     */
    private String communication_status;
    /**
     * 快递单明细状态标记
     */
    private String condition;
    /**
     * 是否签收标记
     */
    private String ischeck;
    /**
     * 快递公司编码
     */
    private String com;

    /**
     * 单号
     */
    private String nu;

    /**
     * 快递详情
     */
    private String data;

    /**
     * 接受到信息的时间
     */
    private Timestamp receive_time;
    
    /**
     * 有效状态：1有效，2无效
     */
    private int able;

    public int getExpress_info_id()
    {
        return express_info_id;
    }

    public void setExpress_info_id(int express_info_id)
    {
        this.express_info_id = express_info_id;
    }

    public String getMonitoring_status()
    {
        return monitoring_status;
    }

    public void setMonitoring_status(String monitoring_status)
    {
        this.monitoring_status = monitoring_status;
    }

    public String getBillstatus()
    {
        return billstatus;
    }

    public void setBillstatus(String billstatus)
    {
        this.billstatus = billstatus;
    }

    public String getMonitoring_message()
    {
        return monitoring_message;
    }

    public void setMonitoring_message(String monitoring_message)
    {
        this.monitoring_message = monitoring_message;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getCommunication_status()
    {
        return communication_status;
    }

    public void setCommunication_status(String communication_status)
    {
        this.communication_status = communication_status;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public String getIscheck()
    {
        return ischeck;
    }

    public void setIscheck(String ischeck)
    {
        this.ischeck = ischeck;
    }

    public String getCom()
    {
        return com;
    }

    public void setCom(String com)
    {
        this.com = com;
    }

    public String getNu()
    {
        return nu;
    }

    public void setNu(String nu)
    {
        this.nu = nu;
    }

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public Timestamp getReceive_time()
    {
        return receive_time;
    }

    public void setReceive_time(Timestamp receive_time)
    {
        this.receive_time = receive_time;
    }

    public String getReceive_time_toStr()
    {
        if (null != receive_time)
        {
            return receive_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public String getLogistics_code()
    {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }

    public int getAble()
    {
        return able;
    }

    public void setAble(int able)
    {
        this.able = able;
    }

    public int getSubscribe_status()
    {
        return subscribe_status;
    }

    public void setSubscribe_status(int subscribe_status)
    {
        this.subscribe_status = subscribe_status;
    }
}
