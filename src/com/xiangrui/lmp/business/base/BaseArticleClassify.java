package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 文章分类
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-17 下午12:35:55
 *         </p>
 */
public abstract class BaseArticleClassify implements Serializable,Comparable<BaseArticleClassify>
{

    private static final long serialVersionUID = 1L;
    /**
     * ID自增
     */
    private int id;
    /**
     * 分类名称
     */
    private String name;
    /**
     * 父id
     */
    private int parent_id;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getParent_id()
    {
        return parent_id;
    }

    public void setParent_id(int parent_id)
    {
        this.parent_id = parent_id;
    }

    public BaseArticleClassify(int id, String name, int parent_id)
    {
        super();
        this.id = id;
        this.name = name;
        this.parent_id = parent_id;
    }

    public BaseArticleClassify(String name, int parent_id)
    {
        super();
        this.name = name;
        this.parent_id = parent_id;
    }

    public BaseArticleClassify()
    {
        super();
    }

    @Override
    public int compareTo(BaseArticleClassify o)
    { 
        int orderInt = 1;
        int parent = o.getParent_id() - this.getParent_id();
        int id = o.getId() - this.getId();
        if(parent >= 0 && id > 0){
            orderInt = -1;
        }
        if(parent == 0 && id == 0){
            orderInt = 0;
        }
        return orderInt;
    }
    
}
