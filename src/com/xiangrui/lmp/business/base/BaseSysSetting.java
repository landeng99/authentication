package com.xiangrui.lmp.business.base;

import java.io.Serializable;
/**
 * 
 * @author Will
 *
 */
public abstract class BaseSysSetting implements Serializable
{
	public static final String ENABLE_WARNING_EMAIL = "ENABLE_WARNING_EMAIL";
	public static final String ENABLE_WARNING_SMS = "ENABLE_WARNING_SMS";
	public static final String ENABLE_WARNING_SMS_TIME_RANGE = "ENABLE_WARNING_SMS_TIME_RANGE";
	public static final String ENABLE_WARNING_EMAIL_TIME_RANGE = "ENABLE_WARNING_EMAIL_TIME_RANGE";
	public static final String USD_CHANGE_TO_RMB_RATE="USD_CHANGE_TO_RMB_RATE";
	/**
	 * 主键ID
	 */
	private int seq_id;
	/**
	 * 设置key
	 */
	private String sys_setting_key;
	/**
	 * 设置value
	 */
	private String sys_setting_value;
	/**
	 * 备注
	 */
	private String remark;

	public int getSeq_id()
	{
		return seq_id;
	}

	public void setSeq_id(int seq_id)
	{
		this.seq_id = seq_id;
	}

	public String getSys_setting_key()
	{
		return sys_setting_key;
	}

	public void setSys_setting_key(String sys_setting_key)
	{
		this.sys_setting_key = sys_setting_key;
	}

	public String getSys_setting_value()
	{
		return sys_setting_value;
	}

	public void setSys_setting_value(String sys_setting_value)
	{
		this.sys_setting_value = sys_setting_value;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

}
