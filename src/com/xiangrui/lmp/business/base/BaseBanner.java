package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 轮播图
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-8 上午10:04:10
 * </p>
 */
public abstract class BaseBanner implements Serializable
{
    /**
     * 启用状态
     */
    public static final int BANNER_STATUS_ENABLE = 1;
    /**
     * 禁用状态
     */
    public static final int BANNER_STATUS_DISABLE = 2;
    /**
     * 临时存储状态,随时准备删除的数据
     */
    public static final int BANNER_STATUS_DELETE = 3;
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 自增长id
     */
    private int banner_id;
    /**
     * 图片地址
     */
    private String imageurl;
    /**
     * 关键字
     */
    private String keyword;
    /**
     * 图片链接
     */
    private String url;
    /**
     * 状态:1启用，2禁用
     */
    private int status;

    public int getBanner_id()
    {
        return banner_id;
    }

    public void setBanner_id(int banner_id)
    {
        this.banner_id = banner_id;
    }

    public String getImageurl()
    {
        return imageurl;
    }

    public void setImageurl(String imageurl)
    {
        this.imageurl = imageurl;
    }

    public String getKeyword()
    {
        return keyword;
    }

    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

}
