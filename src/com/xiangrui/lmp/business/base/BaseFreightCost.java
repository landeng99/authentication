package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 费用计算表
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-13 下午4:07:55
 * </p>
 */
public abstract class BaseFreightCost implements Serializable
{
    /**
     * 会员等级1：不存在
     */
    public static final String RATE_ID_NOTEXIST = "1";
    /**
     * 会员等级0：存在
     */
    public static final String RATE_ID_EXIST = "0";
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * int not null auto_increment comment ‘费用id’,
     */
    private int freight_id;
    /**
     * int comment ‘会员等级id’,
     */
    private int rate_id;
    /**
     * float comment ‘单价，n元/500 bl，n元500磅’,
     */
    private float unit_price;
    /**
     * float comment ‘首重单价，n元/500 bl，n元500磅’,
     */
    private float first_weight_price;
    /**
     * float comment ‘续重单价，n元/500 bl，n元500磅’,
     */
    private float go_weight_price;
    /**
     * float comment ‘合箱费用，n元/票’,
     */
    private float merge_price;
    /**
     * float comment ‘分箱费用，n元/票’,
     */
    private float unpack_price;
    /**
     * float comment ‘清点费用，n元/票’,
     */
    private float check_price;
    /**
     * float comment ‘加固费用，n元/票’,
     */
    private float reinforce_price;
    /**
     * float comment ‘退货费用，n元/票’,
     */
    private float return_price;
    /**
     * float comment ‘取发票费用，￥n元/票’,
     */
    private float invoice_price;
    /**
     * int comment ‘免仓租期’,
     */
    private int rent_days;

    public int getFreight_id()
    {
        return freight_id;
    }

    public void setFreight_id(int freight_id)
    {
        this.freight_id = freight_id;
    }

    public int getRate_id()
    {
        return rate_id;
    }

    public void setRate_id(int rate_id)
    {
        this.rate_id = rate_id;
    }

    public float getUnit_price()
    {
        return unit_price;
    }

    public void setUnit_price(float unit_price)
    {
        this.unit_price = unit_price;
    }

    public float getMerge_price()
    {
        return merge_price;
    }

    public void setMerge_price(float merge_price)
    {
        this.merge_price = merge_price;
    }

    public float getUnpack_price()
    {
        return unpack_price;
    }

    public void setUnpack_price(float unpack_price)
    {
        this.unpack_price = unpack_price;
    }

    public float getCheck_price()
    {
        return check_price;
    }

    public void setCheck_price(float check_price)
    {
        this.check_price = check_price;
    }

    public float getReinforce_price()
    {
        return reinforce_price;
    }

    public void setReinforce_price(float reinforce_price)
    {
        this.reinforce_price = reinforce_price;
    }

    public float getReturn_price()
    {
        return return_price;
    }

    public void setReturn_price(float return_price)
    {
        this.return_price = return_price;
    }

    public float getInvoice_price()
    {
        return invoice_price;
    }

    public void setInvoice_price(float invoice_price)
    {
        this.invoice_price = invoice_price;
    }

    public int getRent_days()
    {
        return rent_days;
    }

    public void setRent_days(int rent_days)
    {
        this.rent_days = rent_days;
    }

	public float getFirst_weight_price() {
		return first_weight_price;
	}

	public void setFirst_weight_price(float first_weight_price) {
		this.first_weight_price = first_weight_price;
	}

	public float getGo_weight_price() {
		return go_weight_price;
	}

	public void setGo_weight_price(float go_weight_price) {
		this.go_weight_price = go_weight_price;
	}
    
    @Override
    public String toString() {
    	return "会员等级:"+rate_id+"-首重单价"+first_weight_price+"-续重单价"+go_weight_price;
    }

}
