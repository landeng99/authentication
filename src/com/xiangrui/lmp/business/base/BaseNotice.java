package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 公告
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 上午9:59:38
 * </p>
 */
public abstract class BaseNotice implements Serializable
{
    private static final long serialVersionUID = 1L;
    /**
     * '主键ID',
     */
    private int noticeId;
    /**
     * '标题',
     */
    private String title;
    /**
     * '公告内容',
     */
    private String context;
    /**
     * '生效时间',
     */
    private String effectTime;
    /**
     * '失效时间',
     */
    private String timeOut;
    /**
     * '状态，0：禁用，1：启用',
     */
    private int status;
    /**
     * '公告创建时间',
     */
    private String create_time;

    private String tt_profile;
    
    public String getTt_profile()
	{
		return tt_profile;
	}

	public void setTt_profile(String tt_profile)
	{
		this.tt_profile = tt_profile;
	}

	public int getNoticeId()
    {
        return noticeId;
    }

    public void setNoticeId(int noticeId)
    {
        this.noticeId = noticeId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getEffectTime()
    {
        if (null != effectTime && effectTime.lastIndexOf(".0") == 19)
            return effectTime.substring(0, 19);
        return effectTime;
    }

    public void setEffectTime(String effectTime)
    {
        this.effectTime = effectTime;
    }

    public String getTimeOut()
    {
        if (null != timeOut && timeOut.lastIndexOf(".0") == 19)
            return timeOut.substring(0, 19);
        return timeOut;
    }

    public void setTimeOut(String timeOut)
    {
        this.timeOut = timeOut;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getCreate_time()
    {
        return create_time;
    }

    public void setCreate_time(String create_time)
    {
        this.create_time = create_time;
    }

}
