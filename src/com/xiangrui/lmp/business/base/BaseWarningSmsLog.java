package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Will
 *
 */
public abstract class BaseWarningSmsLog implements Serializable
{
	/**
	 * 发送成功
	 */
	public final static int SEND_SUCCESS = 0;
	/**
	 * 发送失败
	 */
	public final static int SEND_FAILURE = 1;
	/**
	 * 需要等候发送
	 */
	public final static int NEED_WAITTING_SEND = -1;
	/**
	 * 主键ID
	 */
	private int seq_id;
	/**
	 * 收件人手机号
	 */
	private String receriver_moblie;
	/**
	 * 短信内容
	 */
	private String content;
	/**
	 * 发送时间
	 */
	private Date send_time;
	/**
	 * -1:需要到时段发送 0:发送成功 1:发送失败
	 */
	private int status;
	/**
	 * 重发次数
	 */
	private int resent_count;
	/**
	 * 最后重发时间
	 */
	private Date last_resent_time;

	public int getSeq_id()
	{
		return seq_id;
	}

	public void setSeq_id(int seq_id)
	{
		this.seq_id = seq_id;
	}

	public String getReceriver_moblie()
	{
		return receriver_moblie;
	}

	public void setReceriver_moblie(String receriver_moblie)
	{
		this.receriver_moblie = receriver_moblie;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public Date getSend_time()
	{
		return send_time;
	}

	public void setSend_time(Date send_time)
	{
		this.send_time = send_time;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public int getResent_count()
	{
		return resent_count;
	}

	public void setResent_count(int resent_count)
	{
		this.resent_count = resent_count;
	}

	public Date getLast_resent_time()
	{
		return last_resent_time;
	}

	public void setLast_resent_time(Date last_resent_time)
	{
		this.last_resent_time = last_resent_time;
	}

}
