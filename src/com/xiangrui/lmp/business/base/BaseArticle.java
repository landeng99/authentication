package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 文章
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午9:56:53
 *         </p>
 */
public abstract class BaseArticle implements Serializable
{
    private static final long serialVersionUID = 1L;
    /**
     * '主键ID',
     */
    private int id;
    /**
     * '文章内容',
     */
    private String content;
    /**
     * '标题',
     */
    private String title;
    /**
     * '创建者',
     */
    private String author;
    /**
     * 
     */
    private String create_time;
    /**
     * '文章分类ID',
     */
    private int classify_id;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getCreate_time()
    {
        if (null != create_time && create_time.lastIndexOf(".0") == 19)
            return create_time.substring(0, 19);
        return create_time;
    }

    public void setCreate_time(String create_time)
    {
        this.create_time = create_time;
    }

    public int getClassify_id()
    {
        return classify_id;
    }

    public void setClassify_id(int classify_id)
    {
        this.classify_id = classify_id;
    }

    public BaseArticle(String content, String title, String author,
            int classify_id)
    {
        super();
        this.content = content;
        this.title = title;
        this.author = author;
        this.classify_id = classify_id;
    }

    public BaseArticle(String content, String title, String author,
            String create_time, int classify_id)
    {
        super();
        this.content = content;
        this.title = title;
        this.author = author;
        this.create_time = create_time;
        this.classify_id = classify_id;
    }

    public BaseArticle()
    {
        super();
    }
}
