package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 包裹退货信息
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午10:01:02
 *         </p>
 */
public abstract class BasePkgReturn implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     * 包裹退货申请ID
     */
    private int return_id;

    /**
     * 包裹ID
     */
    private int pkg_id;

    /**
     * 退货理由
     */
    private String reason;

    /**
     * 审批状态
     */
    private int approve_status;

    /**
     * 拒绝理由
     */
    private String approve_reason;

    /**
     * 审批人员
     */
    private int operater;

    /**
     * 申请时间
     */
    private Timestamp create_time;

    /**
     * 审批时间
     */
    private Timestamp approve_time;

    public int getReturn_id()
    {
        return return_id;
    }

    public void setReturn_id(int return_id)
    {
        this.return_id = return_id;
    }

    public int getPkg_id()
    {
        return pkg_id;
    }

    public void setPkg_id(int pkg_id)
    {
        this.pkg_id = pkg_id;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public int getApprove_status()
    {
        return approve_status;
    }

    public void setApprove_status(int approve_status)
    {
        this.approve_status = approve_status;
    }

    public String getApprove_reason()
    {
        return approve_reason;
    }

    public void setApprove_reason(String approve_reason)
    {
        this.approve_reason = approve_reason;
    }

    public int getOperater()
    {
        return operater;
    }

    public void setOperater(int operater)
    {
        this.operater = operater;
    }

    public Timestamp getCreate_time()
    {
        return create_time;
    }

    public String getCreate_time_toStr()
    {
        if (null != create_time)
        {
            return create_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setCreate_time(Timestamp create_time)
    {
        this.create_time = create_time;
    }

    public String getApprove_time_toStr()
    {
        if (null != approve_time)
        {
            return approve_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public Timestamp getApprove_time()
    {
        return approve_time;
    }

    public void setApprove_time(Timestamp approve_time)
    {
        this.approve_time = approve_time;
    }

}
