package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Will
 *
 */
public abstract class BaseOutputScanLog implements Serializable
{
	private static final long serialVersionUID = -2823476043195204198L;
	/**
	 * 主键ID
	 */
	private int seq_id;
	/**
	 * 扫描时间
	 */
	private Date scan_date;
	/**
	 * 包裹ID
	 */
	private int pkg_id;
	/**
	 * 扫描单号
	 */
	private String pkg_no;
	/**
	 * 包裹状态
	 */
	private String pkg_status;
	/**
	 * 是否成功添加进托盘 0-否 1-是
	 */
	private int add_pallet_success;
	/**
	 * 未成功添加进托盘原因
	 */
	private String add_fail_reson;
	/**
	 * 扫描人员
	 */
	private String scan_user_name;
	/**
	 * 扫描次数
	 */
	private int scan_count;
	
	/**
	 * 提单ID
	 */
	private String pick_id;

	public String getPick_id()
	{
		return pick_id;
	}

	public void setPick_id(String pick_id)
	{
		this.pick_id = pick_id;
	}

	public int getSeq_id()
	{
		return seq_id;
	}

	public void setSeq_id(int seq_id)
	{
		this.seq_id = seq_id;
	}

	public Date getScan_date()
	{
		return scan_date;
	}

	public void setScan_date(Date scan_date)
	{
		this.scan_date = scan_date;
	}

	public int getPkg_id()
	{
		return pkg_id;
	}

	public void setPkg_id(int pkg_id)
	{
		this.pkg_id = pkg_id;
	}

	public String getPkg_no()
	{
		return pkg_no;
	}

	public void setPkg_no(String pkg_no)
	{
		this.pkg_no = pkg_no;
	}

	public String getPkg_status()
	{
		return pkg_status;
	}

	public void setPkg_status(String pkg_status)
	{
		this.pkg_status = pkg_status;
	}

	public int getAdd_pallet_success()
	{
		return add_pallet_success;
	}

	public void setAdd_pallet_success(int add_pallet_success)
	{
		this.add_pallet_success = add_pallet_success;
	}

	public String getAdd_fail_reson()
	{
		return add_fail_reson;
	}

	public void setAdd_fail_reson(String add_fail_reson)
	{
		this.add_fail_reson = add_fail_reson;
	}

	public String getScan_user_name()
	{
		return scan_user_name;
	}

	public void setScan_user_name(String scan_user_name)
	{
		this.scan_user_name = scan_user_name;
	}

	public int getScan_count()
	{
		return scan_count;
	}

	public void setScan_count(int scan_count)
	{
		this.scan_count = scan_count;
	}
	public String getScan_dateStr()
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return simpleDateFormat.format(scan_date);
	}
}
