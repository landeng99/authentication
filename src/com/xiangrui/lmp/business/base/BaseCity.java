package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public abstract class BaseCity implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    private int id;

    /**
     * 地区id
     */
    private int city_id;

    /**
     * 父级id
     */
    private int father_id;

    /**
     * 地区名称
     */
    private String city_name;

    /**
     * 邮政编码
     */
    private String postal_code;
    
    
    private String keyword;
    
    
    /**
     * 父子级树形结构
     */
    private String i_framework;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getCity_id()
    {
        return city_id;
    }

    public void setCity_id(int city_id)
    {
        this.city_id = city_id;
    }

    public int getFather_id()
    {
        return father_id;
    }

    public void setFather_id(int father_id)
    {
        this.father_id = father_id;
    }

    public String getCity_name()
    {
        return city_name;
    }

    public void setCity_name(String city_name)
    {
        this.city_name = city_name;
    }

    public String getPostal_code()
    {
        return postal_code;
    }

    public void setPostal_code(String postal_code)
    {
        this.postal_code = postal_code;
    }
    
    public String getKeyword()
    {
        return keyword;
    }

    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getI_framework()
    {
        return i_framework;
    }

    public void setI_framework(String i_framework)
    {
        this.i_framework = i_framework;
    }

}
