package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public class BaseOverseasAddress implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 仓库id
     */
    private int id;

    /**
     * 地址1
     */
    private String address_first;

    /**
     * 地址2
     */
    private String address_second;

    /**
     * 城市
     */
    private String city;

    /**
     * 州
     */
    private String state;

    /**
     * 县/郡
     */
    private String county;

    /**
     * 国家
     */
    private String country;

    /**
     * 邮编
     */
    private String postcode;

    /**
     * 电话
     */
    private String tell;

    /**
     * 免税
     */
    private int is_tax_free;

    /**
     * 仓库
     */
    private String warehouse;
    
    /**
     * 国旗路径
     */
    private String flag_path;
    
    /**
     * 逻辑删除状态
     */
    private int isdeleted;
    
    private String need_auth_flag;
    
    
    /**
     * 未删除
     */
    public static final int ISDELETE_UNDELETED=0;
    
    /**
     * 已删除
     */
    public static final int ISDELETE_DELETED=1;
    
    public String getFlag_path()
    {
        return flag_path;
    }

    public void setFlag_path(String flag_path)
    {
        this.flag_path = flag_path;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getAddress_first()
    {
        return address_first;
    }

    public void setAddress_first(String address_first)
    {
        this.address_first = address_first;
    }

    public String getAddress_second()
    {
        return address_second;
    }

    public void setAddress_second(String address_second)
    {
        this.address_second = address_second;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getCounty()
    {
        return county;
    }

    public void setCounty(String county)
    {
        this.county = county;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    public String getTell()
    {
        return tell;
    }

    public void setTell(String tell)
    {
        this.tell = tell;
    }

    public int getIs_tax_free()
    {
        return is_tax_free;
    }

    public void setIs_tax_free(int is_tax_free)
    {
        this.is_tax_free = is_tax_free;
    }

    public int getIsdeleted()
    {
        return isdeleted;
    }

    public void setIsdeleted(int isdeleted)
    {
        this.isdeleted = isdeleted;
    }

    public String getWarehouse()
    {
        return warehouse;
    }

    public String getWarehouseIndex()
    {
        if(null!=warehouse && warehouse.indexOf("仓库") != -1){
            return warehouse.replace("仓库","");
        }else{
            return warehouse;
        }
    }
    
    public void setWarehouse(String warehouse)
    {
        this.warehouse = warehouse;
    }

	public String getNeed_auth_flag()
	{
		return need_auth_flag;
	}

	public void setNeed_auth_flag(String need_auth_flag)
	{
		this.need_auth_flag = need_auth_flag;
	}

}
