package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 金额统计
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午9:50:29
 *         </p>
 */
public class BaseAccountBalance implements Serializable
{
    /**
     * 查询设置条件,is_user_id的条件设置,map中参数(queryAll方法中),表示所有的记录,不包括总计数据
     */
    public static final int IS_USER_ID_PARAMS_TRUE = 1;
    /**
     *  查询设置条件,is_user_id的条件设置,map中参数(queryAll方法中),仅仅查询总计的数据
     */
    public static final int IS_USER_ID_PARAMS_FALSE = 0;
    /**
     *  查询设置条件,is_user_id的条件设置,map中参数(queryAll方法中),查询所有的,包括总计数据,本条不设置和设置效果一样
     */
    public static final int IS_USER_ID_PARAMS = -1;
    
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private int balance_id;
    /**
     * 用户id
     */
    private int user_id;
    /**
     * 上期余额
     */
    private double last_balance;
    /**
     * 本期余额
     */
    private double curr_balance;
    /**
     * 统计开始时间
     */
    private String start_time;
    /**
     * 消费金额　　
     */
    private double total_expend;
    /**
     * 充值金额
     */
    private double total_recharge;
    /**
     * 索赔金额
     */
    private double total_withdraw;
    /**
     * 提现金额
     */
    private double total_claim_cost;
    /**
     * 统计结束时间
     */
    private String end_time;
    /**
     * 差额
     */
    private double difference;
    
    /**
     * 网上消费
     */
    private double alipay_amount;
    
    /**
     * 账户消费
     */
    private double balance_amount;
    
    /**
     * 账户消费
     */
    private double  balance;

    public double getBalance()
    {
        return balance;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }

    public int getBalance_id()
    {
        return balance_id;
    }

    public void setBalance_id(int balance_id)
    {
        this.balance_id = balance_id;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public double getLast_balance()
    {
        return last_balance;
    }

    public void setLast_balance(double last_balance)
    {
        this.last_balance = last_balance;
    }

    public double getCurr_balance()
    {
        return curr_balance;
    }

    public void setCurr_balance(double curr_balance)
    {
        this.curr_balance = curr_balance;
    }

    public String getStart_time()
    {
        if (null != start_time && start_time.lastIndexOf(".0") == 19)
            return start_time.substring(0, 19);
        return start_time;
    }

    public void setStart_time(String start_time)
    {
        this.start_time = start_time;
    }

    public double getTotal_expend()
    {
        return total_expend;
    }

    public void setTotal_expend(double total_expend)
    {
        this.total_expend = total_expend;
    }

    public double getTotal_recharge()
    {
        return total_recharge;
    }

    public void setTotal_recharge(double total_recharge)
    {
        this.total_recharge = total_recharge;
    }

    public double getTotal_withdraw()
    {
        return total_withdraw;
    }

    public void setTotal_withdraw(double total_withdraw)
    {
        this.total_withdraw = total_withdraw;
    }

    public double getTotal_claim_cost()
    {
        return total_claim_cost;
    }

    public void setTotal_claim_cost(double total_claim_cost)
    {
        this.total_claim_cost = total_claim_cost;
    }

    public String getEnd_time()
    {
        if (null != end_time && end_time.lastIndexOf(".0") == 19)
            return end_time.substring(0, 19);
        return end_time;
    }

    public void setEnd_time(String end_time)
    {
        this.end_time = end_time;
    }

    public double getDifference()
    {
        return difference;
    }

    public void setDifference(double difference)
    {
        this.difference = difference;
    }


    public double getAlipay_amount()
    {
        return alipay_amount;
    }

    public void setAlipay_amount(double alipay_amount)
    {
        this.alipay_amount = alipay_amount;
    }

    public double getBalance_amount()
    {
        return balance_amount;
    }

    public void setBalance_amount(double balance_amount)
    {
        this.balance_amount = balance_amount;
    }

    public BaseAccountBalance()
    {
        super();
    }

    public BaseAccountBalance(int balance_id, int user_id, double last_balance,
            double curr_balance, String start_time, double total_expend,
            double total_recharge, double total_withdraw,
            double total_claim_cost, String end_time, double difference)
    {
        super();
        this.balance_id = balance_id;
        this.user_id = user_id;
        this.last_balance = last_balance;
        this.curr_balance = curr_balance;
        this.start_time = start_time;
        this.total_expend = total_expend;
        this.total_recharge = total_recharge;
        this.total_withdraw = total_withdraw;
        this.total_claim_cost = total_claim_cost;
        this.end_time = end_time;
        this.difference = difference;
    }

}
