package com.xiangrui.lmp.business.base;

import java.math.BigDecimal;

public class BaseIdentifier {

    public static final int IDENTIFIER_ID_PACKAGE = 1;
	/**
	 * 自增长主键
	 */
	private int identifier_id;
	
	/**
	 * 输出前缀
	 */
	private String prefix;
	
	/**
	 *最新的一个序号
	 */
	private BigDecimal last_num;
	
	/**
	 * 描述信息
	 */
	private String description;

	public int getIdentifier_id() {
		return identifier_id;
	}

	public void setIdentifier_id(int identifier_id) {
		this.identifier_id = identifier_id;
	}
 
	
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

 

	public BigDecimal getLast_num() {
		return last_num;
	}

	public void setLast_num(BigDecimal last_num) {
		this.last_num = last_num;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
