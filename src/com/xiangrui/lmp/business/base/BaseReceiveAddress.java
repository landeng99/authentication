package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public class BaseReceiveAddress implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 份证审核状态，0：未审核，
     */
    public static final int IDCARD_APPROVE_UNOT = 0;

    /**
     * 1：审核通过
     */
    public static final int IDCARD_APPROVE_OK = 1;

    /**
     * 2：审核拒绝
     */
    public static final int IDCARD_APPROVE_REFUSE = 2;


    /**
     * 地址id
     */
    private int address_id;

    /**
     * 用户id
     */
    private int user_id;

    /**
     * 地区
     */
    private String region;

    /**
     * 地址字符串
     */
    private String street;

    /**
     * 邮政编码
     */
    private String postal_code;

    /**
     * 收件人
     */
    private String receiver;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 电话号码
     */
    private String tel;

    /**
     * 是否默认地址，0：不是；1：是
     */
    private String isdefault;

    /**
     * 身份证号
     */
    private String idcard;

    /**
     * 身份证正面图
     */
    private String front_img;

    /**
     * 身份证反面图片
     */
    private String back_img;

    /**
     * 身份证审核状态，0：未审核，1：审核通过，2：审核拒绝
     */
    private int idcard_status;

    /**
     * 拒绝理由
     */
    private String refuse_reason;

    public int getAddress_id()
    {
        return address_id;
    }

    public void setAddress_id(int address_id)
    {
        this.address_id = address_id;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public String getStreet()
    {
        return street;
    }

    public void setStreet(String street)
    {
        this.street = street;
    }

    public String getPostal_code()
    {
        return postal_code;
    }

    public void setPostal_code(String postal_code)
    {
        this.postal_code = postal_code;
    }

    public String getReceiver()
    {
        return receiver;
    }

    public void setReceiver(String receiver)
    {
        this.receiver = receiver;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getTel()
    {
        return tel;
    }

    public void setTel(String tel)
    {
        this.tel = tel;
    }

    public String getIsdefault()
    {
        return isdefault;
    }

    public void setIsdefault(String isdefault)
    {
        this.isdefault = isdefault;
    }

    public String getIdcard()
    {
        return idcard;
    }

    public void setIdcard(String idcard)
    {
        this.idcard = idcard;
    }

    public String getFront_img()
    {
        return front_img;
    }

    public void setFront_img(String front_img)
    {
        this.front_img = front_img;
    }

    public String getBack_img()
    {
        return back_img;
    }

    public void setBack_img(String back_img)
    {
        this.back_img = back_img;
    }

    public int getIdcard_status()
    {
        return idcard_status;
    }

    public void setIdcard_status(int idcard_status)
    {
        this.idcard_status = idcard_status;
    }

    public String getRefuse_reason()
    {
        return refuse_reason;
    }

    public void setRefuse_reason(String refuse_reason)
    {
        this.refuse_reason = refuse_reason;
    }

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

}
