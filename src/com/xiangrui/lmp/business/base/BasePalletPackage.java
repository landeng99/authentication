package com.xiangrui.lmp.business.base;

public class BasePalletPackage
{

    /**
     * 托盘ID
     */
    private int pallet_id;

    /**
     * 包裹
     */
    private int package_id;

    public int getPallet_id()
    {
        return pallet_id;
    }

    public void setPallet_id(int pallet_id)
    {
        this.pallet_id = pallet_id;
    }

    public int getPackage_id()
    {
        return package_id;
    }

    public void setPackage_id(int package_id)
    {
        this.package_id = package_id;
    }



}
