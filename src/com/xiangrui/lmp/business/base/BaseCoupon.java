package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 代金券
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 上午9:58:12
 * </p>
 */
public abstract class BaseCoupon implements Serializable
{
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private int couponId;
    /**
     * '编号',
     */
    private String couponCode;
    /**
     * '来源',
     */
    private String resource;
    /**
     * '面额',
     */
    private double denomination;
    /**
     * '用户可以一次性获得的数量',
     */
    private int quantity;
    /**
     * '是否可以叠加使用，0：否，1：是',
     */
    private int isRepeatUse;
    /**
     * '状态，1：未使用，2：已使用',
     */
    private int status;
    /**
     * '生效时间',
     */
    private String effectTime;
    /**
     * '有效期，n天',
     */
    private int validDays;
    /**
     * '统一有效期截止日期',
     */
    private String expirationTime;
    /**
     * 优惠券名称
     */
    private String coupon_name;
    
    /**
     * 总数量 -1表示无限
     */
    private int total_count;
    
    /**
     * 剩余数量 -1表示无限
     */
    private int left_count;
    
    /**
     * 是否可以删除，存在固定的特殊的优惠券不可以删除
     */
    private int can_remove;
    

    public String getCoupon_name()
	{
		return coupon_name;
	}

	public void setCoupon_name(String coupon_name)
	{
		this.coupon_name = coupon_name;
	}

	public int getTotal_count()
	{
		return total_count;
	}

	public void setTotal_count(int total_count)
	{
		this.total_count = total_count;
	}

	public int getLeft_count()
	{
		return left_count;
	}

	public void setLeft_count(int left_count)
	{
		this.left_count = left_count;
	}

	public int getCan_remove()
	{
		return can_remove;
	}

	public void setCan_remove(int can_remove)
	{
		this.can_remove = can_remove;
	}

	public int getCouponId()
    {
        return couponId;
    }

    public void setCouponId(int couponId)
    {
        this.couponId = couponId;
    }

    public String getCouponCode()
    {
        return couponCode;
    }

    public void setCouponCode(String couponCode)
    {
        this.couponCode = couponCode;
    }

    public String getResource()
    {
        return resource;
    }

    public void setResource(String resource)
    {
        this.resource = resource;
    }

    public double getDenomination()
    {
        return denomination;
    }

    public void setDenomination(double denomination)
    {
        this.denomination = denomination;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public int getIsRepeatUse()
    {
        return isRepeatUse;
    }

    public void setIsRepeatUse(int isRepeatUse)
    {
        this.isRepeatUse = isRepeatUse;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getEffectTime()
    {
        if (null != effectTime && effectTime.lastIndexOf(".0") == 19)
            return effectTime.substring(0, 19);
        return effectTime;
    }

    public void setEffectTime(String effectTime)
    {
        this.effectTime = effectTime;
    }

    public int getValidDays()
    {
        return validDays;
    }

    public void setValidDays(int validDays)
    {
        this.validDays = validDays;
    }

    public String getExpirationTime()
    {
        if (null != expirationTime && expirationTime.lastIndexOf(".0") == 19)
            return expirationTime.substring(0, 19);
        return expirationTime;
    }

    public void setExpirationTime(String expirationTime)
    {
        this.expirationTime = expirationTime;
    }

}
