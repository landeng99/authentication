package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Will
 *
 */
public abstract class BaseWarningEmailLog implements Serializable
{
	/**
	 * 发送成功
	 */
	public final static int SEND_SUCCESS = 0;
	/**
	 * 发送失败
	 */
	public final static int SEND_FAILURE = 1;
	/**
	 * 主键ID
	 */
	private int seq_id;
	/**
	 * 接收者名称
	 */
	private String receriver_user_name;
	/**
	 * 接收者帐户
	 */
	private String receriver_account;
	/**
	 * 接收邮箱
	 */
	private String receriver_email;
	/**
	 * 发送者邮箱
	 */
	private String sender_email;
	/**
	 * 邮件主题
	 */
	private String title;
	/**
	 * 邮件内容
	 */
	private String content;
	/**
	 * 发送时间
	 */
	private Date send_time;
	/**
	 * 0:发送成功 1:发送失败
	 */
	private int status;
	/**
	 * 重发次数
	 */
	private int resent_count;
	/**
	 * 最后重发时间
	 */
	private Date last_resent_time;

	public int getSeq_id()
	{
		return seq_id;
	}

	public void setSeq_id(int seq_id)
	{
		this.seq_id = seq_id;
	}

	public String getReceriver_user_name()
	{
		return receriver_user_name;
	}

	public void setReceriver_user_name(String receriver_user_name)
	{
		this.receriver_user_name = receriver_user_name;
	}

	public String getReceriver_account()
	{
		return receriver_account;
	}

	public void setReceriver_account(String receriver_account)
	{
		this.receriver_account = receriver_account;
	}

	public String getReceriver_email()
	{
		return receriver_email;
	}

	public void setReceriver_email(String receriver_email)
	{
		this.receriver_email = receriver_email;
	}

	public String getSender_email()
	{
		return sender_email;
	}

	public void setSender_email(String sender_email)
	{
		this.sender_email = sender_email;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public Date getSend_time()
	{
		return send_time;
	}

	public void setSend_time(Date send_time)
	{
		this.send_time = send_time;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public int getResent_count()
	{
		return resent_count;
	}

	public void setResent_count(int resent_count)
	{
		this.resent_count = resent_count;
	}

	public Date getLast_resent_time()
	{
		return last_resent_time;
	}

	public void setLast_resent_time(Date last_resent_time)
	{
		this.last_resent_time = last_resent_time;
	}

}
