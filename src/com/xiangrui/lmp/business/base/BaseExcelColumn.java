package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public abstract class BaseExcelColumn implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 列名ID
     */
    private int col_id;

    /**
     * 数据库对应字段
     */
    private String col_key;

    /**
     * 列名
     */
    private String col_name;

    /**
     * 分类
     */
    private int type;

    public int getCol_id()
    {
        return col_id;
    }

    public void setCol_id(int col_id)
    {
        this.col_id = col_id;
    }

    public String getCol_key()
    {
        return col_key;
    }

    public void setCol_key(String col_key)
    {
        this.col_key = col_key;
    }

    public String getCol_name()
    {
        return col_name;
    }

    public void setCol_name(String col_name)
    {
        this.col_name = col_name;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

}
