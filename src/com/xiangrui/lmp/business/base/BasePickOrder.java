package com.xiangrui.lmp.business.base;

import java.sql.Timestamp;

public class BasePickOrder {
 

    /**
     * 提单状态:已出库
     */
    public static final int PICKORDER_SENT_ALREADY = 1;
    /**
     * 提单状态:空运中
     */
    public static final int PICKORDER_AIRLIFT_ALREADY = 2;
    
    /**
     * 提单状态:待清关
     */
    public static final int PICKORDER_CUSTOMS_ALREADY = 3;
	
	/**
	 * 提单id
	 */
	private int pick_id;
	
	/**
	 * 提单号
	 */
	private String pick_code;
	
	/**
	 * 航班号
	 */
	private String flight_num;
	
	/**
	 * 口岸ID
	 */
	private int seaport_id;
	
	/**
	 * 描述信息
	 */
	private String description;
	
	/**
	 * 托盘状态
	 */
	private int status;
	
	/**
	 * 创建时间
	 */
	private Timestamp create_time;
	
	/**
	 * 创建人
	 */
	private int create_user;
	
	/**
	 * 发货时间
	 */
	private Timestamp send_time;
	
	/**
	 *报关时间
	 */
	private Timestamp declaration_time;
	
	/**
	 * 仓库id
	 */
	private int overseas_address_id;
	
	/**
	 * 所属出库ID
	 */
	private int output_id;

	public int getOutput_id()
	{
		return output_id;
	}

	public void setOutput_id(int output_id)
	{
		this.output_id = output_id;
	}

	public int getPick_id() {
		return pick_id;
	}

	public void setPick_id(int pick_id) {
		this.pick_id = pick_id;
	}

	public String getPick_code() {
		return pick_code;
	}

	public void setPick_code(String pick_code) {
		this.pick_code = pick_code;
	}

	public String getFlight_num() {
		return flight_num;
	}

	public void setFlight_num(String flight_num) {
		this.flight_num = flight_num;
	}

	public int getSeaport_id() {
		return seaport_id;
	}

	public void setSeaport_id(int seaport_id) {
		this.seaport_id = seaport_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Timestamp getCreate_time() {
		return create_time;
	}

    public String getCreate_time_toStr()
    {
        if (null != create_time)
        {
            return create_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

	public void setCreate_time(Timestamp create_time) {
		this.create_time = create_time;
	}

	public int getCreate_user() {
		return create_user;
	}

	public void setCreate_user(int create_user) {
		this.create_user = create_user;
	}

	public Timestamp getSend_time() {
		return send_time;
	}

    public String getSend_time_toStr(){
        if(null != send_time){
            return send_time.toString().trim().replace(".0", "");
        }else{
            return "";
        }
    }

	public void setSend_time(Timestamp send_time) {
		this.send_time = send_time;
	}

	public Timestamp getDeclaration_time() {
		return declaration_time;
	}

    public String getDeclaration_time_toStr(){
        if(null != declaration_time){
            return declaration_time.toString().trim().replace(".0", "");
        }else{
            return "";
        }
    }

	public void setDeclaration_time(Timestamp declaration_time) {
		this.declaration_time = declaration_time;
	}

    public int getOverseas_address_id()
    {
        return overseas_address_id;
    }

    public void setOverseas_address_id(int overseas_address_id)
    {
        this.overseas_address_id = overseas_address_id;
    }
	
}
