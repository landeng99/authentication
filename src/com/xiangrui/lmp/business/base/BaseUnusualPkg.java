package com.xiangrui.lmp.business.base;

import java.util.Date;

/**
 * 
 * 异常包裹
 * @author Administrator
 *
 */
 
public class BaseUnusualPkg
{
    /**
     * 异常包裹id
     */
    private int unusual_pkg_id;
    
    /**
     * 关联单号
     */
    private String original_num;
    
    /**
     * 实际重量
     */
    private String actual_weight;
    
    /**
     * 异常状态：-1异常状态，1正常状态
     */
    private int unusual_status;
    
    /**
     * 异常原因
     */
    private String reason;
    
    /**
     * 创建时间
     */
    private Date create_time;
    
    /**
     * 创建用户
     */
    private int  create_user;
    
    /**
     * 更新时间
     */
    private Date update_time;
    
    /**
     * 更新人
     */
    private int update_user;
    
    /**
     * 归属人
     */
    private int owner;
    
    /**
     * 海外仓库地址id
     */
    private int osaddr_id;
    
    /**
     * 关联包裹ID
     */
    private int connect_package_id;
    
    /**
     * LASTNAME
     */
    private String last_name;
    
    /**
     * 用户账号
     */
    private String account;
    
    /**
     * 未认领
     */
    public final  static int STATUS_UNUSUAL=-1;
    
    /**
     * 已认领
     */
    public final static int STATUS_NORMAL=1; 
    
    /**
     * 备注
     */
    private String remark;
            
    public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public int getConnect_package_id()
	{
		return connect_package_id;
	}

	public void setConnect_package_id(int connect_package_id)
	{
		this.connect_package_id = connect_package_id;
	}

	public String getLast_name()
	{
		return last_name;
	}

	public void setLast_name(String last_name)
	{
		this.last_name = last_name;
	}

	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}

	public int getUnusual_pkg_id()
    {
        return unusual_pkg_id;
    }

    public void setUnusual_pkg_id(int unusual_pkg_id)
    {
        this.unusual_pkg_id = unusual_pkg_id;
    }

    public String getOriginal_num()
    {
        return original_num;
    }

    public void setOriginal_num(String original_num)
    {
        this.original_num = original_num;
    }

    public String getActual_weight()
    {
        return actual_weight;
    }

    public void setActual_weight(String actual_weight)
    {
        this.actual_weight = actual_weight;
    }

    public int getUnusual_status()
    {
        return unusual_status;
    }

    public void setUnusual_status(int unusual_status)
    {
        this.unusual_status = unusual_status;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public Date getCreate_time()
    {
        return create_time;
    }

    public void setCreate_time(Date create_time)
    {
        this.create_time = create_time;
    }

 

    public int getCreate_user()
    {
        return create_user;
    }

    public void setCreate_user(int create_user)
    {
        this.create_user = create_user;
    }

    public Date getUpdate_time()
    {
        return update_time;
    }

    public void setUpdate_time(Date update_time)
    {
        this.update_time = update_time;
    }

    public int getUpdate_user()
    {
        return update_user;
    }

    public void setUpdate_user(int update_user)
    {
        this.update_user = update_user;
    }

    public int getOwner()
    {
        return owner;
    }

    public void setOwner(int owner)
    {
        this.owner = owner;
    }

    public int getOsaddr_id()
    {
        return osaddr_id;
    }

    public void setOsaddr_id(int osaddr_id)
    {
        this.osaddr_id = osaddr_id;
    }
    
}
