package com.xiangrui.lmp.business.base;

import java.io.Serializable;

import com.xiangrui.lmp.business.admin.goodstype.vo.GoodsType;

/**
 * 商品
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午9:59:25
 *         </p>
 */
public abstract class BaseGoodsType  implements Serializable/*,
        Comparable*/
{
    private static final long serialVersionUID = 1L;
    /**
     *  
     */
    private int goodsTypeId;
    /**
     *  
     */
    private String taxCode;
    /**
     *  
     */
    private String nameSpecs;
    /**
     *  
     */
    private int parentId;
    /**
     *  
     */
    private String unit;
    /**
     *  
     */
    private double taxesPrice;
    /**
     *  
     */
    private double taxRate;
    /**
     * 申报类别排序用的
     */
    private String idFramework;

    public String getIdFramework()
    {
        return idFramework;
    }

    public void setIdFramework(String idFramework)
    {
        this.idFramework = idFramework;
    }

    public int getGoodsTypeId()
    {
        return goodsTypeId;
    }

    public void setGoodsTypeId(int goodsTypeId)
    {
        this.goodsTypeId = goodsTypeId;
    }

    public String getTaxCode()
    {
        return taxCode;
    }

    public void setTaxCode(String taxCode)
    {
        this.taxCode = taxCode;
    }

    public String getNameSpecs()
    {
        return nameSpecs;
    }

    public void setNameSpecs(String nameSpecs)
    {
        this.nameSpecs = nameSpecs;
    }

    public int getParentId()
    {
        return parentId;
    }

    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }

    public String getUnit()
    {
        return unit;
    }

    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public double getTaxesPrice()
    {
        return taxesPrice;
    }

    public void setTaxesPrice(double taxesPrice)
    {
        this.taxesPrice = taxesPrice;
    }

    public double getTaxRate()
    {
        return taxRate;
    }

    public void setTaxRate(double taxRate)
    {
        this.taxRate = taxRate;
    }
/*
    @Override
    public int compareTo(Object obj)
    {
        BaseGoodsType o=(BaseGoodsType)obj;
        String[] oifw = o.getIdFramework().split("_");
        String[] ifw = this.getIdFramework().split("_");
        int size = oifw.length > ifw.length ? ifw.length : oifw.length;
        boolean isEq = false;
        for (int i = 0; i < size; i++)
        {
            int oifwInt = Integer.parseInt(oifw[i]);
            int ifwInt = Integer.parseInt(ifw[i]);
            if (oifwInt > ifwInt)
            {
                return -1;
            }
            isEq = (oifwInt == ifwInt);
        }
        if (isEq)
        {
            if (oifw.length > ifw.length)
            {

                return -1;
            } else if (oifw.length == ifw.length)
            {

                return 0;
            }
        }
        return 1;
    }*/
}
