package com.xiangrui.lmp.business.base;

//[{"id":"440000","name":"广东省","level":"1"},{"id":"440300","name":"深圳市","level":"2"}]
public class Region implements Comparable
{
    private String id;

    private String name;

    private String level;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLevel()
    {
        return level;
    }

    public void setLevel(String level)
    {
        this.level = level;
    }

    @Override
    public int compareTo(Object o)
    {
        Region region = (Region) o;
        int level=Integer.parseInt(region.getLevel());
        int curr_level=Integer.parseInt(this.getLevel());
        return curr_level-level;
    }
}