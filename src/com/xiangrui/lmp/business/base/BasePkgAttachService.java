package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public class BasePkgAttachService implements Serializable
{

    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 包裹ID
     */
    private int package_id;

    /**
     * 增值服务ID
     */
    private int attach_id;

    /**
     * 增值服务描述
     */
    private String description;

    /**
     * 增值服务状态。（1：未完成，2：已完成）
     */
    private int status;

    /**
     * 分箱后 或者合箱前的包裹ID，原始包裹id
     */
    private String package_group;

    

    /**
     * 增值服务价格
     */

    private float service_price;
    
    
    /**
     * 增值服务未完成
     */
    public static final int ATTACH_UNFINISH=1;
    
    /**
     * 增值服务已完成
     */
    public static final int ATTACH_FINISH=2;
    
    public int getPackage_id()
    {
        return package_id;
    }

    public void setPackage_id(int package_id)
    {
        this.package_id = package_id;
    }

    public int getAttach_id()
    {
        return attach_id;
    }

    public void setAttach_id(int attach_id)
    {
        this.attach_id = attach_id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getPackage_group()
    {
        return package_group;
    }

    public void setPackage_group(String package_group)
    {
        this.package_group = package_group;
    }
    public float getService_price()
    {
        return service_price;
    }

    public void setService_price(float service_price)
    {
        this.service_price = service_price;
    }
}
