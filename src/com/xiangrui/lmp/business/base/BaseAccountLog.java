package com.xiangrui.lmp.business.base;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import com.xiangrui.lmp.util.StringUtil;

/**
 * 消费记录
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午9:56:23
 *         </p>
 */
public abstract class BaseAccountLog implements Serializable
{

    /**
     * 交易类型:1充值金额
     */
    public static final int ACCOUNT_TYPE_RECHARGE = 1;
    /**
     * 交易类型:2消费金额
     */
    public static final int ACCOUNT_TYPE_CONSUME = 2;
    /**
     * 交易类型:3提现金额
     */
    public static final int ACCOUNT_TYPE_WITHDRAW = 3;
    /**
     * 交易类型:4索赔
     */
    public static final int ACCOUNT_TYPE_COMPENSATION = 4;

    /**
     * 交易状态:1提交
     */
    public static final int APPLY = 1;
    /**
     * 交易状态:2成功
     */
    public static final int SUCCESS = 2;
    
    /**
     * 交易状态:3失败
     */
    public static final int FAIL = 3;
    
    /**
     * 交易状态:4提现取消
     */
    public static final int CANCEL = 4;

    /**
     * 时间格式化
     */
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private int log_id;
    /**
     * 金额
     */
    private float amount;
    /**
     * 类型：1充值金额、2消费金额、3提现金额、 4理赔金额
     */
    private int account_type;
    /**
     * 时间
     */
    private Timestamp opr_time;
    /**
     * 用户ID
     */
    private int user_id;
    /**
     * 描述信息
     */
    private String description;
    /**
     * 虚拟订单
     */
    private String order_id;
    /**
     * 状态
     */
    private int status;

    /**
     * 公司运单号 list,用<br>分隔
     */
    private String logistics_code;
    
    //支付宝姓名
    private String alipayName;
    
    /**
     * 支付宝号
     */
    private String alipay_no;
    /**
     * 支付宝交易号
     */
    private String trade_no;
    /**
     * 银行流水号
     */
    private String bank_seq_no;
    
    /**
     * 账户支付部分
     */
    private float balance_amount;
    
    /**
     * 网上支付部分
     */
    private float alipay_amount;

    /**
     * json显示时间用
     */
    private String opr_time_string;
    /**
     * 返利流水号
     */
    private String rebatesms_serial_no;
    
    private float customs_cost = 0;	// 税金（关税费用）
    
    private int backUserId = 0;		// （现金支付时）收银员Id
 
    private String backUserName = "";	// （现金支付时）收银员名字

	public int getBackUserId() {
		return backUserId;
	}

	public void setBackUserId(int backUserId) {
		this.backUserId = backUserId;
	}

	public String getBackUserName() {
		return backUserName;
	}

	public void setBackUserName(String backUserName) {
		this.backUserName = backUserName;
	}

	public float getCustoms_cost() {
		return customs_cost;
	}

	public void setCustoms_cost(float customs_cost) {
		this.customs_cost = customs_cost;
	}

	public String getRebatesms_serial_no()
	{
		return rebatesms_serial_no;
	}

	public void setRebatesms_serial_no(String rebatesms_serial_no)
	{
		this.rebatesms_serial_no = rebatesms_serial_no;
	}

	public int getLog_id()
    {
        return log_id;
    }

    public void setLog_id(int log_id)
    {
        this.log_id = log_id;
    }

    public float getAmount()
    {
        return amount;
    }

    public void setAmount(float amount)
    {
        this.amount = amount;
    }

    public int getAccount_type()
    {
        return account_type;
    }

    public void setAccount_type(int account_type)
    {
        this.account_type = account_type;
    }

    public Timestamp getOpr_time()
    {
        return opr_time;
    }

    public String getOpr_time_toStr()
    {
        if (null != opr_time)
        {
            return opr_time.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setOpr_time(Timestamp opr_time)
    {
        this.opr_time = opr_time;

        // json 显示时间用
        this.opr_time_string = simpleDateFormat.format(opr_time).toString();
    }

    public int getUser_id()
    {
        return user_id;
    }

    public void setUser_id(int user_id)
    {
        this.user_id = user_id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getOrder_id()
    {
        return order_id;
    }

    public void setOrder_id(String order_id)
    {
        this.order_id = order_id;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getOpr_time_string()
    {
        return opr_time_string;
    }

    public String getLogistics_code()
    {
        return logistics_code;
    }
    
    public List<String> getLogistisCodeList(){
    	return StringUtil.toStringList(this.getLogistics_code(), "<br>");
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }

    
    public String getAlipayName()
	{
		return alipayName;
	}

	public void setAlipayName(String alipayName)
	{
		this.alipayName = alipayName;
	}

	public String getAlipay_no()
    {
        return alipay_no;
    }

    public void setAlipay_no(String alipay_no)
    {
        this.alipay_no = alipay_no;
    }

    public String getTrade_no()
    {
        return trade_no;
    }

    public void setTrade_no(String trade_no)
    {
        this.trade_no = trade_no;
    }

    public String getBank_seq_no()
    {
        return bank_seq_no;
    }

    public void setBank_seq_no(String bank_seq_no)
    {
        this.bank_seq_no = bank_seq_no;
    }

    public float getBalance_amount()
    {
        return balance_amount;
    }

    public void setBalance_amount(float balance_amount)
    {
        this.balance_amount = balance_amount;
    }

    public float getAlipay_amount()
    {
        return alipay_amount;
    }

    public void setAlipay_amount(float alipay_amount)
    {
        this.alipay_amount = alipay_amount;
    }

}
