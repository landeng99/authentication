package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 用户优惠券记录
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-6 上午10:01:48
 *         </p>
 */
public abstract class BaseUserCoupon implements Serializable
{

	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private int coupon_id;
	/**
	 * '编号',
	 */
	private String coupon_code;
	/**
	 * '用户id',
	 */
	private int user_id;
	/**
	 * '来源',
	 */
	private String source;
	/**
	 * '面额',
	 */
	private double denomination;
	/**
	 * '数量',
	 */
	private int quantity;
	/**
	 * '是否可以叠加使用，0：否，1：是',
	 */
	private int is_repeat_use;
	/**
	 * '有效期',
	 */
	private String valid_date;
	/**
	 * '状态，1：未使用，2：已使用',
	 */
	private int status;
	/**
	 * '使用日期',
	 */
	private String use_date;

	private String coupon_name;
	/**
	 * 来源好友
	 */
	private String source_friend;
	/**
	 * 开放时间
	 */
	private String create_time;
	
	public String getCreate_time()
	{
		if (null != create_time && create_time.lastIndexOf(".0") == 19)
			return create_time.substring(0, 19);
		return create_time;
	}

	public void setCreate_time(String create_time)
	{
		this.create_time = create_time;
	}

	public String getSource_friend()
	{
		return source_friend;
	}

	public void setSource_friend(String source_friend)
	{
		this.source_friend = source_friend;
	}

	public String getCoupon_name()
	{
		return coupon_name;
	}

	public void setCoupon_name(String coupon_name)
	{
		this.coupon_name = coupon_name;
	}

	public int getCoupon_id()
	{
		return coupon_id;
	}

	public void setCoupon_id(int coupon_id)
	{
		this.coupon_id = coupon_id;
	}

	public String getCoupon_code()
	{
		return coupon_code;
	}

	public void setCoupon_code(String coupon_code)
	{
		this.coupon_code = coupon_code;
	}

	public int getUser_id()
	{
		return user_id;
	}

	public void setUser_id(int user_id)
	{
		this.user_id = user_id;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public double getDenomination()
	{
		return denomination;
	}

	public void setDenomination(double denomination)
	{
		this.denomination = denomination;
	}

	public int getQuantity()
	{
		return quantity;
	}

	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}

	public int getIs_repeat_use()
	{
		return is_repeat_use;
	}

	public void setIs_repeat_use(int is_repeat_use)
	{
		this.is_repeat_use = is_repeat_use;
	}

	public String getValid_date()
	{
		return valid_date;
	}

	public String getValid_date_toStr()
	{
		if (null != valid_date)
		{
			return valid_date.toString().trim().replace(".0", "");
		}
		else
		{
			return "";
		}
	}

	public void setValid_date(String valid_date)
	{
		this.valid_date = valid_date;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getUse_date()
	{
		if (null != use_date && use_date.lastIndexOf(".0") == 19)
			return use_date.substring(0, 19);
		return use_date;
	}

	public void setUse_date(String use_date)
	{
		this.use_date = use_date;
	}
}
