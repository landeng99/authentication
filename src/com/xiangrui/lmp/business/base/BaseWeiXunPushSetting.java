package com.xiangrui.lmp.business.base;

import java.io.Serializable;

public abstract class BaseWeiXunPushSetting implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int weixun_push_id;
	private int user_id;
	// open_id
	private String open_id;
	//
	private String last_update_time;
	// 包裹入库消息推送开关: Y -允许; N -不允许
	private String allow_input;
	// 包裹出库消息推送开关: Y -允许; N -不允许
	private String allow_output;
	// 包裹已清关并转送国内派送消息推送开关: Y -允许; N -不允许
	private String allow_delivery;
	// 包裹已签收消息推送开关: Y -允许; N -不允许
	private String allow_sign;

	public int getWeixun_push_id()
	{
		return weixun_push_id;
	}

	public void setWeixun_push_id(int weixun_push_id)
	{
		this.weixun_push_id = weixun_push_id;
	}

	public int getUser_id()
	{
		return user_id;
	}

	public void setUser_id(int user_id)
	{
		this.user_id = user_id;
	}

	public String getOpen_id()
	{
		return open_id;
	}

	public void setOpen_id(String open_id)
	{
		this.open_id = open_id;
	}

	public String getLast_update_time()
	{
		return last_update_time;
	}

	public void setLast_update_time(String last_update_time)
	{
		this.last_update_time = last_update_time;
	}

	public String getAllow_input()
	{
		return allow_input;
	}

	public void setAllow_input(String allow_input)
	{
		this.allow_input = allow_input;
	}

	public String getAllow_output()
	{
		return allow_output;
	}

	public void setAllow_output(String allow_output)
	{
		this.allow_output = allow_output;
	}

	public String getAllow_delivery()
	{
		return allow_delivery;
	}

	public void setAllow_delivery(String allow_delivery)
	{
		this.allow_delivery = allow_delivery;
	}

	public String getAllow_sign()
	{
		return allow_sign;
	}

	public void setAllow_sign(String allow_sign)
	{
		this.allow_sign = allow_sign;
	}

}
