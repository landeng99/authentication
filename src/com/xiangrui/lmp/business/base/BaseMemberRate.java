package com.xiangrui.lmp.business.base;

import java.io.Serializable;

/**
 * 会员信息
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-16 下午12:08:05</p>
 */
public abstract class BaseMemberRate implements Serializable
{

    
    public static final String INTRGRAL_MIN_NOTEXIST = "1";

    public static final String INTRGRAL_MIN_EXIST = "0";

    /**
     * 会员等级名称1：不存在
     */
    public static final String RATE_NAME_NOTEXIST = "1";
    

    /**
     * 会员等级名称0：存在
     */
    public static final String RATE_NAME_EXIST = "0";

    /**
     * 逻辑删除0：未删除
     */
    public static final int ISDELETED_NO = 0;

    /**
     * 逻辑删除1：删除
     */
    public static final int ISDELETED_YES = 1;
    
    /**
     * 同行会员等级
     */
    public static final int RATE_TYPE_PEER_LEVEL = 2;
    
    /**
     * 普通会员等级
     */
    public static final int RATE_TYPE_ORDINARY_LEVEL = 1;

    private static final long serialVersionUID = 1L;
    
    /**
     * 会员等级id
     */
    private int rate_id;
    /**
     * 等级类型
     */
    private int rate_type;
    /**
     * 等级名称
     */
    private String rate_name;
    /**
     * 积分下限
     */
    private int integral_min;
    /**
     * 积分上限
     */
    private int integral_max;
    
    /**
     * 逻辑删除
     */
    private int isdeleted;
    
    public int getRate_id()
    {
        return rate_id;
    }
    public void setRate_id(int rate_id)
    {
        this.rate_id = rate_id;
    }

    public String getRate_name()
    {
        return rate_name;
    }
    public void setRate_name(String rate_name)
    {
        this.rate_name = rate_name;
    }
    public int getIntegral_min()
    {
        return integral_min;
    }
    public void setIntegral_min(int integral_min)
    {
        this.integral_min = integral_min;
    }
    public int getIntegral_max()
    {
        return integral_max;
    }
    public void setIntegral_max(int integral_max)
    {
        this.integral_max = integral_max;
    }
    public int getRate_type()
    {
        return rate_type;
    }
    public void setRate_type(int rate_type)
    {
        this.rate_type = rate_type;
    }
    public int getIsdeleted()
    {
        return isdeleted;
    }
    public void setIsdeleted(int isdeleted)
    {
        this.isdeleted = isdeleted;
    }
    
}
