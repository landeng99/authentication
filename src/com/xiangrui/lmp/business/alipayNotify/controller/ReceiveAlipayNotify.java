package com.xiangrui.lmp.business.alipayNotify.controller;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xiangrui.lmp.business.admin.interfacelog.service.InterfaceLogService;
import com.xiangrui.lmp.business.admin.interfacelog.vo.InterfaceLog;
import com.xiangrui.lmp.business.homepage.controller.PaymentController;
import com.xiangrui.lmp.business.homepage.service.FrontAccountLogService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.init.SpringContextHolder;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.UUIDUtil;
import com.xiangrui.lmp.util.alipay.util.AlipayNotify;

@Controller
@RequestMapping("/receiveAlipayNotify")
public class ReceiveAlipayNotify extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    Logger logger = Logger.getLogger(PaymentController.class);

    @Autowired
    private FrontAccountLogService frontAccountLogService;

    @Autowired
    private PkgService pkgService;

    @Autowired
    private FrontUserService memberService;

    /**
     * 充值
     */
    private static final String RECHARGE = "1";

    /**
     * 网银充值
     */
    private static final String RECHARGE_BANK = "2";
    /**
     * 支付
     */
    private static final String PAYMENT = "3";

    /**
     * 网银支付
     */
    private static final String PAYMENT_BANK = "4";

    /**
     * 5支付宝状态同步返回接收接口
     */
    private static final int INTERFACE_RETURN = 5;
    /**
     * 6支付宝状态异步通知接收接口
     */
    private static final int INTERFACE_NOTIFY = 6;
    /**
     * 8银联同步状态返回接口
     */
    private static final int INTERFACE_RETURN_BANK = 8;
    /**
     * 9银联异步状态返回接口
     */
    private static final int INTERFACE_NOTIFY_BANK = 9;

    /**
     * 支付宝支付结果同步返回
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/pay", method = RequestMethod.GET)
    public String pay(HttpServletRequest request, HttpServletResponse response)
    {

        logger.info("--------------运费支付同步结果返回开始--不做处理--------------");
        // 处理结果
        result(request, response, "支付成功！请返回支付页面确认结果。", INTERFACE_RETURN);

        logger.info("--------------运费支付同步结果返回结束--不做处理--------------");

        return "/homepage/success";
    }

    /**
     * 支付宝充值结果同步返回
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/recharge", method = RequestMethod.GET)
    public String recharge(HttpServletRequest request,
            HttpServletResponse response)
    {

        logger.info("--------------支付宝充值同步结果返回开始--不做处理--------------");
        // 处理结果
        result(request, response, "充值成功！请返回账户记录页面确认结果。", INTERFACE_RETURN);

        logger.info("--------------支付宝充值同步结果返回结束--不做处理--------------");

        return "/homepage/success";
    }

    /**
     * 网银支付结果同步返回
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/payFromBank", method = RequestMethod.GET)
    public String payFromBank(HttpServletRequest request,
            HttpServletResponse response)
    {

        logger.info("--------------运费网银支付同步结果返回开始--不做处理--------------");
        // 处理结果
        result(request, response, "支付成功！请返回支付页面确认结果。", INTERFACE_RETURN_BANK);

        logger.info("--------------运费网银支付同步结果返回结束--不做处理--------------");

        return "/homepage/success";
    }

    /**
     * 网银充值结果同步返回
     * 
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/rechargeFromBank", method = RequestMethod.GET)
    public String rechargeFromBank(HttpServletRequest request,
            HttpServletResponse response)
    {

        logger.info("--------------网银充值同步结果返回开始--不做处理--------------");
        // 处理结果
        result(request, response, "充值成功！请返回账户记录页面确认结果。", INTERFACE_RETURN_BANK);

        logger.info("--------------网银充值同步结果返回结束--不做处理--------------");

        return "/homepage/success";
    }

    /**
     * (支付宝 网银)同步返回结果共通处理（充值 支付）
     * 
     * @param request
     * @param response
     * @param message
     *            消息
     * @param interace_type
     *            接口类型
     */
    public void result(HttpServletRequest request,
            HttpServletResponse response, String message, int interace_type)
    {

        try
        {
            // 获取支付宝GET过来反馈信息 5同步返回
            Map<String, String> params = returnInfo(request, interace_type);

            // 交易状态
            String trade_status = params.get("trade_status");

            // 计算得出通知验证结果
            boolean verify_result = AlipayNotify.verify(params);
            response.setCharacterEncoding("utf-8");

            // 验证成功
            if (verify_result)
            {
                // 交易成功
                if (trade_status.equals("TRADE_FINISHED")
                        || trade_status.equals("TRADE_SUCCESS"))
                {
                    logger.info("交易成功");

                    request.setAttribute("message", message);

                } else
                {
                    request.setAttribute("message", "交易失败!");

                    logger.info("交易失败");
                }

                System.out.println("验证成功");

            } else
            {
                System.out.println("验证失败");
            }

        } catch (Exception e)
        {

            e.printStackTrace();
        }
    }

    /**
     * 支付宝支付结果返回异步处理
     * 
     * @param request
     * @param response
     */
    @RequestMapping(value = "/payNotify", method = RequestMethod.POST)
    public void payNotify(HttpServletRequest request,
            HttpServletResponse response)
    {

        logger.info("--------------运费支付宝支付异步结果返回开始--------------");

        notifyCommon(request, response, PAYMENT, INTERFACE_NOTIFY);

        logger.info("--------------运费支付宝支付异步结果返回结束--------------");

    }

    /**
     * 支付宝充值结果返回异步处理
     * 
     * @param request
     * @param response
     */
    @RequestMapping(value = "/rechargeNotify", method = RequestMethod.POST)
    public void rechargeNotify(HttpServletRequest request,
            HttpServletResponse response)
    {

        logger.info("--------------支付宝充值异步结果返回开始--------------");

        notifyCommon(request, response, RECHARGE, INTERFACE_NOTIFY);

        logger.info("--------------支付宝充值异步结果返回结束--------------");

    }

    /**
     * 网银支付结果返回异步处理
     * 
     * @param request
     * @param response
     */
    @RequestMapping(value = "/payNotifyFromBank", method = RequestMethod.POST)
    public void payNotifyFromBank(HttpServletRequest request,
            HttpServletResponse response)
    {

        logger.info("--------------运费网银支付异步结果返回开始--------------");

        notifyCommon(request, response, PAYMENT_BANK, INTERFACE_NOTIFY_BANK);

        logger.info("--------------运费网银支付异步结果返回结束--------------");

    }

    /**
     * 网银充值结果返回异步处理
     * 
     * @param request
     * @param response
     */
    @RequestMapping(value = "/rechargeNotifyFromBank", method = RequestMethod.POST)
    public void rechargeNotifyFromBank(HttpServletRequest request,
            HttpServletResponse response)
    {

        logger.info("--------------网银充值异步结果返回开始--------------");

        notifyCommon(request, response, RECHARGE_BANK, INTERFACE_NOTIFY_BANK);

        logger.info("--------------网银充值异步结果返回结束--------------");

    }

    /**
     * (支付宝 网银)结果异步通知共通处理(支付 充值)
     * 
     * @param request
     * @param response
     * @param flag
     * 
     * @param interace_type
     *            接口类型
     */

    public void notifyCommon(HttpServletRequest request,
            HttpServletResponse response, String flag, int interace_type)
    {
        try
        {
            // 获取支付宝GET过来反馈信息
            Map<String, String> params = returnInfo(request, interace_type);
            // 订单号
            String out_trade_no = params.get("out_trade_no");
            // 交易状态
            String trade_status = params.get("trade_status");
            // 交易金额
            String total_fee = params.get("total_fee");
            // 支付宝交易号
            String trade_no = params.get("trade_no");
            // 银行流水号
            String bank_seq_no = params.get("bank_seq_no");

            // 支付宝号
            String buyer_email = "";
            // 支付宝 充值 支付 记录买家支付宝号
            if (INTERFACE_NOTIFY == interace_type)
            {
                buyer_email = params.get("buyer_email");
            }
            // 计算得出通知验证结果
            boolean verify_result = AlipayNotify.verify(params);
            response.setCharacterEncoding("utf-8");

            // 记录交易信息
            FrontAccountLog frontAccountLog = new FrontAccountLog();
            // 交易号
            frontAccountLog.setOrder_id(out_trade_no);
            // 时间
            frontAccountLog.setOpr_time(new Timestamp(System
                    .currentTimeMillis()));

            PrintWriter out = response.getWriter();
            // 验证成功
            if (verify_result)
            {
                // 交易成功
                if (trade_status.equals("TRADE_FINISHED")
                        || trade_status.equals("TRADE_SUCCESS"))
                {
                    // 充值
                    if (RECHARGE.equals(flag) || RECHARGE_BANK.equals(flag))
                    {
                        // 交易成功更新信息
                        updateRecharge(out_trade_no, buyer_email, trade_no,
                                bank_seq_no, flag);
                    }
                    // 支付
                    else
                    {
                        // 交易成功更新信息
                        updateAlipay(out_trade_no, total_fee, buyer_email,
                                trade_no, bank_seq_no);
                    }

                    logger.info("交易成功");

                } else
                {
                    frontAccountLog.setStatus(FrontAccountLog.FAIL);
                    logger.info("交易失败");
                }

                System.out.println("验证成功");

                out.println("success");

            } else
            {
                frontAccountLog.setStatus(FrontAccountLog.FAIL);
                out.println("fail");
            }
            // 交易失败
            if (FrontAccountLog.FAIL == frontAccountLog.getStatus())
            {
                frontAccountLogService.updateStatusByOrderId(frontAccountLog);
            }

        } catch (Exception e)
        {

            e.printStackTrace();
        }
    }

    /**
     * 支付交易信息
     * 
     * @param out_trade_no
     *            商户订单号
     * @param total_fee
     *            支付宝支付金额
     * @param alipay_no
     *            支付宝号
     * @param trade_no
     *            支付宝交易号
     * @param bank_seq_no
     *            银行流水号
     * 
     */
    private void updateAlipay(String out_trade_no, String total_fee,
            String alipay_no, String trade_no, String bank_seq_no)
    {
        // 更新数据库用
        Map<String, Object> params = new HashMap<String, Object>();

        // 登陆账户记录
        FrontAccountLog frontAccountLog = frontAccountLogService
                .selectAccountLogByOrderId(out_trade_no);
        if (FrontAccountLog.SUCCESS == frontAccountLog.getStatus())
        {
            // 重复处理
            logger.debug("订单已处理！");
            return;
        }

        // 时间
        frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
        // 交易成功
        frontAccountLog.setStatus(FrontAccountLog.SUCCESS);
        // 支付宝号
        frontAccountLog.setAlipay_no(alipay_no);
        // 支付宝流水号
        frontAccountLog.setTrade_no(trade_no);
        // 银行流水号
        frontAccountLog.setBank_seq_no(bank_seq_no);

        // 更新用户账户信息
        FrontUser frontUser = memberService
                .queryFrontUserByUserId(frontAccountLog.getUser_id());

        if (frontUser.getAble_balance() > 0)
        {

            // 账户余额
            frontUser.setBalance(frontUser.getBalance()
                    - frontUser.getAble_balance());
            frontUser.setAble_balance(0);
        }

        // 更新包裹列表的支付状态
        List<Pkg> pkgList = pkgService.queryPackageBylogisticsCodes(StringUtil
                .toStringList(frontAccountLog.getLogistics_code(), "<br>"));

        for (Pkg pkg : pkgList)
        {
            // 支付状态
			Pkg.setPaid(pkg);
            // 支付方式
            pkg.setPay_type(Pkg.PAY_TYPE_ONLINE);
            // 物流状态:待发货
            pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING);
        }
        params.put("frontUser", frontUser);
        params.put("frontAccountLog", frontAccountLog);
        params.put("pkgList", pkgList);

        memberService.alipay(params);
        logger.debug("运费支付后更新业务表正常");
    }

    /**
     * 充值信息
     * 
     * @param out_trade_no
     *            商户订单号
     * @param alipay_no
     *            支付宝号
     * @param trade_no
     *            银行流水号
     * @param bank_seq_no
     *            支付宝交易号
     * @param flag
     */
    private void updateRecharge(String out_trade_no, String alipay_no,
            String trade_no, String bank_seq_no, String flag)
    {

        // 更新数据库用
        Map<String, Object> params = new HashMap<String, Object>();

        // 登陆账户记录
        FrontAccountLog frontAccountLog = frontAccountLogService
                .selectAccountLogByOrderId(out_trade_no);

        if (FrontAccountLog.SUCCESS == frontAccountLog.getStatus())
        {
            // 重复处理
            logger.debug("订单已处理！");
            return;

        }
        // 时间
        frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
        // 交易成功
        frontAccountLog.setStatus(FrontAccountLog.SUCCESS);
        // 支付宝号
        frontAccountLog.setAlipay_no(alipay_no);
        // 支付宝流水号
        frontAccountLog.setTrade_no(trade_no);
        // 银行流水号
        frontAccountLog.setBank_seq_no(bank_seq_no);

        // 更新用户账户信息
        FrontUser frontUser = memberService
                .queryFrontUserByUserId(frontAccountLog.getUser_id());
        // 账户余额
        frontUser.setBalance(frontUser.getBalance()
                + frontAccountLog.getAmount());
        // 可用余额
        frontUser.setAble_balance(frontUser.getAble_balance()
                + frontAccountLog.getAmount());
        // 首次充值绑定支付宝账号
        if (RECHARGE.equals(flag)
                && StringUtil.isEmpty(frontUser.getPay_treasure()))
        {
            frontUser.setPay_treasure(alipay_no);
        }

        params.put("frontUser", frontUser);
        params.put("frontAccountLog", frontAccountLog);

        memberService.recharge(params);
        logger.debug("充值后更新业务表正常");

    }

    /**
     * 获取支付宝GET过来反馈信息(同步，异步共通)
     * 
     * @param params
     * @param interace_type
     *            接口类型
     * @return
     */
    private Map<String, String> returnInfo(HttpServletRequest request,
            int interace_type)
    {

        // 获取支付宝GET过来反馈信息
        Map<String, String> params = new HashMap<String, String>();
        @SuppressWarnings("rawtypes")
        Map requestParams = request.getParameterMap();
        try
        {
            for (@SuppressWarnings("rawtypes")
            Iterator iter = requestParams.keySet().iterator(); iter.hasNext();)
            {
                String name = (String) iter.next();
                String[] values = (String[]) requestParams.get(name);
                String valueStr = "";
                for (int i = 0; i < values.length; i++)
                {
                    valueStr = (i == values.length - 1) ? valueStr + values[i]
                            : valueStr + values[i] + ",";
                }
                // 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化

                valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");

                params.put(name, valueStr);
                logger.info("name=" + name + "    value=" + valueStr);
            }

            // 商户订单号
            String out_trade_no = new String(request.getParameter(
                    "out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
            // 交易金额
            String total_fee = new String(request.getParameter("total_fee")
                    .getBytes("ISO-8859-1"), "UTF-8");

            // 支付宝交易号
            String trade_no = new String(request.getParameter("trade_no")
                    .getBytes("ISO-8859-1"), "UTF-8");

            String buyer_email = new String(request.getParameter("buyer_email")
                    .getBytes("ISO-8859-1"), "UTF-8");

            // 交易状态
            String trade_status = new String(request.getParameter(
                    "trade_status").getBytes("ISO-8859-1"), "UTF-8");
            logger.info("商户订单号out_trade_no=" + out_trade_no);
            logger.info("支付宝交易号trade_no=" + trade_no);
            logger.info("交易金额total_fee=" + total_fee);
            logger.info("交易状态trade_status=" + trade_status);
            logger.info("买家buyer_email=" + buyer_email);
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            logger.error("接口类型为：" + interace_type + "，请求异常：" + e.getMessage());
        }
        logger.debug("接口类型为：" + interace_type + "，接口正常");
        // 记录日志 ,6异步通知
        recordLog(params, interace_type);
        return params;
    }

    /**
     * 取得日志信息
     * 
     * @param request
     * @param interace_type
     *            接口类型
     */
    private static void recordLog(Map<String, String> params, int interace_type)
    {

        // 日志对象
        InterfaceLog interfaceLog = new InterfaceLog();
        interfaceLog.setLog_id(UUIDUtil.uuid());
        interfaceLog.setStart_time(new Timestamp(System.currentTimeMillis()));
        interfaceLog.setEnd_time(new Timestamp(System.currentTimeMillis()));

        // 将请求参数记录数据库
        String req_msg = getConfigLogMsg(params);
        interfaceLog.setReq_msg(req_msg);

        interfaceLog.setIp(getIp());

        log(interfaceLog, interace_type);

    }

    /**
     * 获取本机ip
     * 
     * @return
     */
    private static String getIp()
    {

        @SuppressWarnings("rawtypes")
        Enumeration allNetInterfaces;
        try
        {
            allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip = null;
            while (allNetInterfaces.hasMoreElements())
            {
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces
                        .nextElement();

                @SuppressWarnings("rawtypes")
                Enumeration addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements())
                {
                    ip = (InetAddress) addresses.nextElement();
                    if (ip != null && ip instanceof Inet4Address)
                    {
                        return ip.getHostAddress();
                    }
                }
            }
            return ip.getHostAddress();

        } catch (SocketException e)
        {

            e.printStackTrace();
        }

        return null;

    }

    /**
     * 将请求参数拼接，为字符串
     * 
     * @param params
     * @return
     */
    private static String getConfigLogMsg(Map<String, String> params)
    {
        StringBuffer buffer = new StringBuffer();
        for (Entry<String, String> entry : params.entrySet())
        {
            buffer.append(entry.getKey() + ":" + entry.getValue() + "\n");
        }
        return buffer.toString();
    }

    /**
     * 日志记录
     * 
     * @param interfaceLog
     *            日志记对象
     * @param interace_type
     *            接口类型
     */
    private static void log(InterfaceLog interfaceLog, int interace_type)
    {
        InterfaceLogService interfaceLogService = SpringContextHolder
                .getBean("interfaceLogService");
        interfaceLog.setInterace_type(interace_type);
        // 记录接口日志
        interfaceLogService.createInterfaceLog(interfaceLog);

    }
}
