package com.xiangrui.lmp.business.homepage.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.FrontUserCoupon;

;

public interface FrontUserCouponService
{

    /**
     * 可用优惠券
     * 
     * @param params
     * @return List
     */
    List<FrontUserCoupon> selectUserCouponByUserId(Map<String, Object> params);

    /**
     * 可用优惠券
     * 
     * @param params
     * @return List
     */
    List<FrontUserCoupon> selectOverdueUserCouponByUserId(
            Map<String, Object> params);

    /**
     * 数量统计 可使用 status == 1 已过期  and status == 2 time 当前时间
     * 
     * @param params
     * @return List
     */
    int sumQuantityByUserId(Map<String, Object> params);
}
