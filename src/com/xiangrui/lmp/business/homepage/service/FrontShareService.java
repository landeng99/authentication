package com.xiangrui.lmp.business.homepage.service;

import com.xiangrui.lmp.business.homepage.vo.FrontShare;

public interface FrontShareService
{

    /**
     * 添加晒单
     * @param 
     */
    void addShare(FrontShare frontShare);
    
    /**
     * 通过package_id查询晒单
     * @param  package_id
     */
    FrontShare queryPkgShare(int package_id);
    
    FrontShare queryShareByUserId(int user_id);
    
    /**
     * 修改晒单
     * @param  frontShare
     */
    void updatePkgShare(FrontShare frontShare);
}
