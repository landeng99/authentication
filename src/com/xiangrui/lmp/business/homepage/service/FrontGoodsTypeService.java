package com.xiangrui.lmp.business.homepage.service;

import java.util.List;

import com.xiangrui.lmp.business.homepage.vo.FrontGoodsType;

public interface FrontGoodsTypeService
{

    /**
     * 查询所有物品分类信息
     * @param parent_id
     * @return list
     */
    List<FrontGoodsType> queryAllGoodsType(int parent_id);
    
    /**
     * 查询所有物品子类信息
     * @return list
     */
    List<FrontGoodsType> queryChildGoodsType();
}
