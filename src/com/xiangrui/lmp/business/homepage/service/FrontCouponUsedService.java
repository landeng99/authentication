package com.xiangrui.lmp.business.homepage.service;

import java.util.List;

import com.xiangrui.lmp.business.homepage.vo.FrontUserCoupon;

;

public interface FrontCouponUsedService
{

    /**
     * 已使用优惠券(为方便页面显示  此处用FrontUserCoupon)
     * 
     * @param user_id
     * @return List
     */
    List<FrontUserCoupon> selectCouponUsedByUserId(int user_id);
    
    /**
     * 用户优惠券数量统计
     * 
     * @param user_id
     * @return int
     */
    int sumQuantityByUserId(int user_id);

}
