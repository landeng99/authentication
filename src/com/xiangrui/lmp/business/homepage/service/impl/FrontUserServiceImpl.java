package com.xiangrui.lmp.business.homepage.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;
import com.xiangrui.lmp.business.admin.sysSetting.mapper.SysSettingMapper;
import com.xiangrui.lmp.business.admin.sysSetting.vo.SysSetting;
import com.xiangrui.lmp.business.homepage.mapper.FfrontUserMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontAccountLogMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgGoodsMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgMapper;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.constant.WebConstants;
import com.xiangrui.lmp.util.CommonUtil;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.StringUtil;

@Service(value = "memberService")
public class FrontUserServiceImpl implements FrontUserService
{
	Logger logger = Logger.getLogger(FrontUserServiceImpl.class);
	@Autowired
	private FfrontUserMapper frontUserMapper;
	@Autowired
	private FrontAccountLogMapper frontAccountLogMapper;
	@Autowired
	private FrontPkgMapper frontPkgMapper;
	@Autowired
	private FrontPkgGoodsMapper frontPkgGoodsMapper;

	@Autowired
	private PkgService pkgService;

	/**
	 * 优惠券
	 */
	@Autowired
	private CouponService couponService;
	
	@Autowired
	SysSettingMapper sysSettingMapper;
	
	private String getUSDtoRMBrate()
	{
		String rate = null;
		SysSetting warningSmsSetting = sysSettingMapper.querySystemSettingBySettingKey(SysSetting.USD_CHANGE_TO_RMB_RATE);
		if (warningSmsSetting != null)
		{
			rate = warningSmsSetting.getSys_setting_value();
		}
		return rate;
	}
	
	private float dollar2Yuan(float dollar) {
		BigDecimal amountBigDecimal = new BigDecimal(dollar).setScale(2, BigDecimal.ROUND_HALF_UP);
		// 美元转换成人民币
		String rate = this.getUSDtoRMBrate();
		if (rate != null)
		{
			amountBigDecimal = amountBigDecimal.multiply(new BigDecimal(rate)).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		
		return amountBigDecimal.floatValue();
	}
	
	/**
	 * 取出用户数据后立即计算人民币
	 * @param fu
	 * @return
	 */
	private FrontUser calcYuan( FrontUser fu ){
		if( null==fu ) return null;
		fu.setAble_balance_yuan(this.dollar2Yuan(fu.getAble_balance()));
		return fu;
	}

	/**
	 * 根据用户邮箱查询用户信息
	 * 
	 * @param account
	 * @return FrontUser
	 */
	@Override
	public FrontUser queryFrontUserByEmail(String email)
	{
		FrontUser fu = frontUserMapper.queryFrontUserByEmail(email);
		return calcYuan(fu);
	}
	/**
	 * 前台用户登录的判断
	 * 
	 * @param frontUser
	 * @return FrontUser
	 */
	@Override
	public FrontUser frontUserLogin(FrontUser frontUser)
	{
		FrontUser fu = frontUserMapper.frontUserLogin(frontUser); 
		return calcYuan(fu);
	}

	/**
	 * 用户注册
	 * 
	 * @param frontUser
	 */
	@Override
	public void addFrontUser(FrontUser frontUser)
	{

		// 是否存在，默认值是存在
		boolean isExist = true;
		while (isExist)
		{

			// 6位随机字符串
			String last_name = StringUtil.radomString();
			Map<String, String> param = new HashMap<String, String>();

			param.put("last_name", last_name);
			int cnt = frontUserMapper.isLastNameExist(param);
			if (cnt == 0)
			{
				isExist = false;
				frontUser.setLast_name(last_name);
			}
			else
			{
				isExist = true;
			}
		}
		frontUser.setFirst_name(FrontUser.FIRST_NAME_INIT);
		frontUserMapper.addFrontUser(frontUser);
	}

	/**
	 * 根据用户ID查询用户信息
	 * 
	 * @param user_id
	 */
	@Override
	public FrontUser queryFrontUserByUserId(int user_id)
	{
		FrontUser fu = frontUserMapper.queryFrontUserByUserId(user_id);
		return calcYuan(fu);
	}

	@Override
	public FrontUser queryFrontUserByMobile(String mobile)
	{
		FrontUser fu = frontUserMapper.queryFrontUserByMobile(mobile);
		return calcYuan(fu);
	}

	/**
	 * 根据用户微信绑定ID查询用户信息
	 * 
	 * @param open_id
	 * @return
	 */
	public FrontUser queryFrontUserByOpenId(String open_id)
	{
		FrontUser fu = frontUserMapper.queryFrontUserByOpenId(open_id);
		return calcYuan(fu);
	}

	@Override
	public FrontUser queryFrontUserByUserName(String mobile)
	{
		FrontUser fu = frontUserMapper.queryFrontUserByUserName(mobile);
		return calcYuan(fu);
	}

	/**
	 * 修改密码
	 * 
	 * @param frontUser
	 */
	@Override
	public int updateUserPwd(FrontUser frontUser)
	{

		return frontUserMapper.updateUserPwd(frontUser);
	}

	/**
	 * 修改手机号
	 * 
	 * @param frontUser
	 */
	@Override
	public int updateMobile(FrontUser frontUser)
	{

		return frontUserMapper.updateMobile(frontUser);
	}

	/**
	 * 修改姓名
	 * 
	 * @param frontUser
	 */
	@Override
	public int updateUserName(FrontUser frontUser)
	{

		return frontUserMapper.updateUserName(frontUser);
	}

	@Override
	public FrontUser queryFrontUserByEmailOrMoible(String email)
	{
		FrontUser fu = null;
		if (email.indexOf("@") >= 0)
		{
			fu = frontUserMapper.queryFrontUserByEmail(email);
		}
		else
		{
			fu = frontUserMapper.queryFrontUserByMobile(email);
		}
		return calcYuan(fu);
	}

	/**
	 * 账户支付运费
	 * 
	 * @param params
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void payment(Map<String, Object> params)
	{

		frontAccountLogMapper.insertFrontAccountLog((FrontAccountLog) params.get("frontAccountLog"));
		@SuppressWarnings("unchecked")
		List<Pkg> pkglist = (List<Pkg>) params.get("pkgList");

		FrontUser frontUser = (FrontUser) params.get("frontUser");
		// 用户积分
		int integral = (int) frontAccountLogMapper.sumAmount(frontUser.getUser_id());

		frontUser.setIntegral(integral);

		frontUserMapper.updateBalanceForpayment(frontUser);

		frontPkgMapper.updatePackageForPaymentFreight(pkglist);
		for (Pkg pkg : pkglist)
		{

			PackageLogUtil.log(pkg, null);
			logger.info("FrontPay(前端支付) --- 用户：" + frontUser.getUser_name() + "      包裹：" + pkg.getLogistics_code() + "     运费：" + pkg.getTransport_cost() + "     预付款运费：" + pkg.getVirtual_pay() + "     账户总额：" + frontUser.getBalance() + "     账户可用余额：" + frontUser.getAble_balance() + "     账户冻结额：" + frontUser.getFrozen_balance());
		}

	}

	/**
	 * 支付宝支付运费
	 * 
	 * @param params
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void alipay(Map<String, Object> params)
	{

		frontAccountLogMapper.updateStatusByOrderId((FrontAccountLog) params.get("frontAccountLog"));

		FrontUser frontUser = (FrontUser) params.get("frontUser");

		// 用户积分
		int integral = (int) frontAccountLogMapper.sumAmount(frontUser.getUser_id());

		frontUser.setIntegral(integral);

		frontUserMapper.updateBalanceForpayment(frontUser);

		@SuppressWarnings("unchecked")
		List<Pkg> pkglist = (List<Pkg>) params.get("pkgList");

		frontPkgMapper.updatePackageForPayment(pkglist);
		for (Pkg pkg : pkglist)
		{

			PackageLogUtil.log(pkg, null);
		}
	}

	/**
	 * 充值
	 * 
	 * @param params
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void recharge(Map<String, Object> params)
	{

		frontUserMapper.updateBalanceForpayment((FrontUser) params.get("frontUser"));
		frontAccountLogMapper.updateStatusByOrderId((FrontAccountLog) params.get("frontAccountLog"));

	}

	@Override
	public int updateBank(FrontUser frontUser)
	{
		// TODO Auto-generated method stub
		return frontUserMapper.updateBank(frontUser);
	}

	@Override
	public int updatePay(FrontUser frontUser)
	{
		// TODO Auto-generated method stub
		return frontUserMapper.updatePay(frontUser);
	}

	/**
	 * 账户支付关税
	 * 
	 * @param params
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void payTax(Map<String, Object> params)
	{

		frontPkgGoodsMapper.updateTax((List<PkgGoods>) params.get("pkgGoodsList"));

		frontAccountLogMapper.insertFrontAccountLog((FrontAccountLog) params.get("frontAccountLog"));

		FrontUser frontUser = (FrontUser) params.get("frontUser");

		// 用户积分
		int integral = (int) frontAccountLogMapper.sumAmount(frontUser.getUser_id());

		frontUser.setIntegral(integral);

		frontUserMapper.updateBalanceForpayment(frontUser);

		List<Pkg> pkglist = (List<Pkg>) params.get("pkgList");

		frontPkgMapper.updatePackageForPaymentCustom(pkglist);

		for (Pkg pkg : pkglist)
		{
			PackageLogUtil.log(pkg, null);
		}

	}

	/**
	 * 网上支付关税
	 * 
	 * @param params
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void payTaxOL(Map<String, Object> params)
	{

		frontPkgGoodsMapper.updateTax((List<PkgGoods>) params.get("pkgGoodsList"));

		frontAccountLogMapper.updateStatusByOrderId((FrontAccountLog) params.get("frontAccountLog"));

		FrontUser frontUser = (FrontUser) params.get("frontUser");

		// 用户积分
		int integral = (int) frontAccountLogMapper.sumAmount(frontUser.getUser_id());

		frontUser.setIntegral(integral);

		frontUserMapper.updateBalanceForpayment(frontUser);

		List<Pkg> pkglist = (List<Pkg>) params.get("pkgList");

		frontPkgMapper.updatePackageForPayment(pkglist);

		for (Pkg pkg : pkglist)
		{
			PackageLogUtil.log(pkg, null);
		}

	}

	/**
	 * 修改默认支付方式
	 * 
	 * @param frontUser
	 */
	public int updateDefaultPayType(FrontUser frontUser)
	{
		return frontUserMapper.updateDefaultPayType(frontUser);
	}

	/**
	 * 修改虚拟预付款方式
	 * 
	 * @param frontUser
	 */
	public int updateVirtualPayFlag(FrontUser frontUser)
	{
		return frontUserMapper.updateVirtualPayFlag(frontUser);
	}

	/**
	 * 绑定微信ID
	 * 
	 * @param frontUser
	 * @return
	 */
	public int updateopen_id(FrontUser frontUser)
	{
		return frontUserMapper.updateopen_id(frontUser);
	}

	/**
	 * 更新个人头像
	 * 
	 * @param frontUser
	 * @return
	 */
	public int updatePersion_icon(FrontUser frontUser)
	{
		return frontUserMapper.updatePersion_icon(frontUser);
	}

	/**
	 * 账户交易信息
	 * 
	 * @param transport_cost
	 *            付款金额
	 * @param user_id
	 *            用户
	 * @param logisticsCodes
	 *            公司运单号
	 */
	public String updatePayment(String transport_cost, int user_id, String logisticsCodes, int coupon_id, String errorCode)
	{
		String payMessage = null;
		// 更新数据库用
		Map<String, Object> params = new HashMap<String, Object>();

		float amount = Float.parseFloat(transport_cost);

		// 更新用户账户信息
		FrontUser frontUser = this.queryFrontUserByUserId(user_id);

		// 登陆账户记录
		FrontAccountLog frontAccountLog = new FrontAccountLog();
		// 必填 商户订单号
		String out_trade_no = CommonUtil.getCurrentDateNum();

		// 公司运单号
		frontAccountLog.setLogistics_code(logisticsCodes.replace(",", "<br>"));

		frontAccountLog.setOrder_id(out_trade_no);
		// 交易金额
		frontAccountLog.setAmount(amount);
		// 时间
		frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
		// 用户
		frontAccountLog.setUser_id(user_id);
		// 交易类型
		frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_CONSUME);
		// 交易成功
		frontAccountLog.setStatus(FrontAccountLog.SUCCESS);
		// 描述
		frontAccountLog.setDescription("运费 账户支付");
		// 网上支付部分
		frontAccountLog.setAlipay_amount(0);
		// 账务余额支付部分
		frontAccountLog.setBalance_amount(amount);

		// 更新包裹列表的支付状态
		List<Pkg> pkgList = pkgService.queryPackageBylogisticsCodes(StringUtil.toStringList(logisticsCodes, ","));

		float ableBalance = frontUser.getAble_balance();
		float frozenBalance = frontUser.getFrozen_balance();
		float total = ableBalance + frozenBalance;

		boolean canCoupanPay = false;
		if (coupon_id > 0)
		{
			if (pkgList.size() > 1)
			{
				return "支付多个包裹不能使用优惠券";
			}
			else
			{
				canCoupanPay = true;
			}
		}
		float actualTransport_cost = 0;
		String couponLogistics_code = null;
		float denomination = 0;
		for (Pkg pkg : pkgList)
		{
//			if (Pkg.PAYMENT_PAID == pkg.getPay_status())
//			if( Pkg.isPaid(pkg) )
		    if (Pkg.PAYMENT_FREIGHT_PAID == pkg.getPay_status_freight())
			{
				return "支付失败(包裹已支付)";
			}
			// 支付状态
			pkg.setPay_status(Pkg.PAYMENT_PAID);
			pkg.setPay_status_freight(Pkg.PAYMENT_FREIGHT_PAID);
			// 支付方式
			pkg.setPay_type(Pkg.PAY_TYPE_ONLINE);
			// 物流状态:待发货
			if( pkg.getPay_status_custom()==Pkg.PAYMENT_CUSTOM_PAID )
			    pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING);
			// 虚拟扣款处理
			float transport_costFloat = pkg.getTransport_cost();
		
			if (canCoupanPay)
			{
				UserCoupon userCoupon = couponService.queryUserCouponBycouponId(coupon_id);
			    denomination = (float) userCoupon.getDenomination();
				if (denomination < pkg.getTransport_cost())
				{
					transport_costFloat = pkg.getTransport_cost() - denomination;
				}
				else
				{
					return "优惠券不可用(面值过大)";
				}
			}
			float virtual_pay = pkg.getVirtual_pay();
			if (virtual_pay > 0.00001f)
			{
				if (total >= transport_costFloat && transport_costFloat > 0.00001f)
				{
					float less = transport_costFloat - virtual_pay;
					ableBalance = ableBalance - less;
					frozenBalance = frozenBalance - virtual_pay;
					if (frozenBalance < 0)
					{
						frozenBalance = 0;
					}
					total = ableBalance + frozenBalance;
					// 标志虚拟扣款已结算处理
					pkg.setVirtual_pay(-1);
				}
				else
				{
					// 余额不足
					payMessage = "账户余额不足，去网上支付！";
					errorCode = "2";
				}
			}
			else
			// 正常支付处理
			{
				if (ableBalance >= transport_costFloat)
				{
					total = total - transport_costFloat;
					ableBalance = ableBalance - transport_costFloat;
				}
				else
				{
					// 余额不足
					payMessage = "账户余额不足，去网上支付！";
					errorCode = "2";
				}
			}
			couponLogistics_code = pkg.getLogistics_code();
			actualTransport_cost += transport_costFloat;
		}
		// 可用余额
		frontUser.setAble_balance(ableBalance);
		// 账户余额
		frontUser.setBalance(total);
		// 冻结金额
		frontUser.setFrozen_balance(frozenBalance);

		params.put("frontUser", frontUser);
		params.put("frontAccountLog", frontAccountLog);
		params.put("pkgList", pkgList);
		actualTransport_cost = NumberUtils.scaleMoneyDataFloat(actualTransport_cost);
		//要计算上优惠券的
		if (( (actualTransport_cost+denomination)- amount) != 0)
		{
			logger.info("actualTransport_cost:" + actualTransport_cost + " total:" + amount);
			payMessage = "支付金额不匹配";
		}
		if (payMessage == null && canCoupanPay)
		{
			payMessage = couponService.useCoupon(frontUser.getUser_id(), coupon_id, couponLogistics_code, 1);
		}
		 if (payMessage == null)
 		{
			this.payment(params);
			
			//通过推广链接或扫描二维码注册并成功支付一个包裹的客户可自动获得‘推荐优惠券’
			int recommandUserId=frontUser.getRecommand_user_id();
			FrontUser recommandFrontUser = this.queryFrontUserByUserId(recommandUserId);
			if(recommandFrontUser!=null&&frontUser.getIs_sent_recommand_coupon()==0)
			{
				couponService.pushRecommandCoupon(recommandUserId, frontUser.getAccount());
				frontUserMapper.updateIs_sent_recommand_coupon(user_id);
			}
		}
		logger.info("errorCode:" + errorCode + " payMessage:" + payMessage);
		return payMessage;
	}

	/**
	 * 生成推荐信息（二维码和推荐链接）
	 * 
	 * @param frontUser
	 * @return
	 */
	public FrontUser createRecommandInfo(FrontUser frontUser, String ctxPath) throws Exception
	{
		String prefix = WebConstants.FRONT_URL_PREFIX;
		String userId = "" + frontUser.getUser_id();
		byte[] trByte = Base64.encodeBase64(userId.getBytes("utf-8"));
		prefix += "/registe?tr=" + new String(trByte);
		// ts
		byte[] urltrByte = Base64.encodeBase64("URL".getBytes("utf-8"));
		String person_recommand_url = prefix + "&ts=" + new String(urltrByte);
		// 二维码内容
		byte[] qrtrByte = Base64.encodeBase64("QR_CODE".getBytes("utf-8"));
		String person_recommand_qr_code = prefix + "&ts=" + new String(qrtrByte);
		int width = 300;
		int height = 300;
		File file = new File(ctxPath);

		if (!file.exists())
		{
			file.mkdirs();
		}
		String fileName = "qrcode" + frontUser.getUser_id() + ".jpg";
		String basePath = File.separator + "resource" + File.separator + "upload" + File.separator + "person_qr_code" + File.separator + fileName;
		FileOutputStream os = new FileOutputStream(new File(ctxPath + fileName));
		// 二维码的图片格式
		String format = "jpg";
		Hashtable hints = new Hashtable();
		// 内容所使用编码
		hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
		BitMatrix bitMatrix = new MultiFormatWriter().encode(person_recommand_qr_code, BarcodeFormat.QR_CODE, width, height, hints);
		// 生成二维码
		MatrixToImageWriter.writeToStream(bitMatrix, format, os);// writeToFile(bitMatrix,
		os.close();
		frontUser.setPerson_recommand_url(person_recommand_url);
		frontUser.setPerson_qr_code(WebConstants.FRONT_URL_PREFIX+basePath);
		frontUserMapper.updatePersonQRCodeAndRecommandURL(frontUser);
		return frontUser;
	}

	/**
	 * 查询个人的邀请用户列表
	 * 
	 * @param user_id
	 * @return
	 */
	public List<FrontUser> queryMyInviteUser(Integer recommand_user_id)
	{
		return frontUserMapper.queryMyInviteUser(recommand_user_id);
	}
	
	/**
	 * 根据用户ID查询推广信息
	 */
	@Override
	public List<FrontUser> queryPushById(int user_id) {
		return frontUserMapper.queryPushById(user_id);
	}
}
