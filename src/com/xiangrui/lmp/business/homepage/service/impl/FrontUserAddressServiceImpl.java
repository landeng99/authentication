package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontUserAddressMapper;
import com.xiangrui.lmp.business.homepage.service.FrontUserAddressService;
import com.xiangrui.lmp.business.homepage.vo.FrontCity;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;

@Service(value = "frontUserAddressService")
public class FrontUserAddressServiceImpl implements FrontUserAddressService
{

    @Autowired
    private FrontUserAddressMapper frontUserAddressMapper;


    @Override
    public List<FrontCity> queryAddressByName(String name)
    {
        // TODO Auto-generated method stub
        return frontUserAddressMapper.queryAddressByName(name);
    }

    @Override
    public List<FrontUserAddress> queryUserAddressByAddressId(int address_id)
    {
        // TODO Auto-generated method stub
        return frontUserAddressMapper.queryUserAddressByAddressId(address_id);
    }
    
    @Override
    public FrontUserAddress queryUseraddress(int address_id)
    {
        // TODO Auto-generated method stub
        return frontUserAddressMapper.queryUseraddress(address_id);
    }

    @Override
    public void updateUserAddressByReceiveAddress(
            FrontUserAddress frontUserAddress)
    {
        // TODO Auto-generated method stub
        frontUserAddressMapper.updateUserAddressByReceiveAddress(frontUserAddress);
    }

    @Override
    public void updateUserAddress(FrontUserAddress frontUserAddress)
    {
        // TODO Auto-generated method stub
        frontUserAddressMapper.updateUserAddress(frontUserAddress);
    }

    @Override
    public int updateUserFrontIdcard(Map<String, Object> params)
    {
        // TODO Auto-generated method stub
        return frontUserAddressMapper.updateUserFrontIdcard(params);
    }

    @Override
    public int updateUserBackIdcard(Map<String, Object> params)
    {
        // TODO Auto-generated method stub
        return frontUserAddressMapper.updateUserBackIdcard(params);
    }

    @Override
    public List<FrontUserAddress> queryUserUnloadIdcard(int user_id)
    {
        // TODO Auto-generated method stub
        return frontUserAddressMapper.queryUserUnloadIdcard(user_id);
    }
    
    @Override
    public List<FrontUserAddress> queryAddressByLogisticsCode(String logistics_code)
    {
        return frontUserAddressMapper.queryAddressByLogisticsCode(logistics_code);
    }
    
    @Override
    public List<FrontUserAddress> queryAddressByMobileAndReceiver(Map<String, Object> params)
    {
    	return frontUserAddressMapper.queryAddressByMobileAndReceiver(params);
    }
    
    /**
     * 更新正面身份证图片和身份证号码
     * @param params
     * @return
     */
    public int updateUserFrontIdcardAndPhoto(Map<String,Object> params)
    {
    	return frontUserAddressMapper.updateUserFrontIdcardAndPhoto(params);
    }
    /**
     * 更新反面身份证图片和身份证号码
     * @param params
     * @return
     */
    public int updateUserBackIdcardAndPhoto(Map<String,Object> params)
    {
    	return frontUserAddressMapper.updateUserBackIdcardAndPhoto(params);
    }
    
    /**
     * 根据receiver和mobile和idcard查询包裹收货地址
     * @param params
     * @return
     */
    public List<FrontUserAddress> queryAddressByMobileAndReceiverAndIdcard(Map<String, Object> params)
    {
    	return frontUserAddressMapper.queryAddressByMobileAndReceiverAndIdcard(params);
    }
}
