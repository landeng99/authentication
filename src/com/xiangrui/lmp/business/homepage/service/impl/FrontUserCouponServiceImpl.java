package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontUserCouponMapper;
import com.xiangrui.lmp.business.homepage.service.FrontUserCouponService;
import com.xiangrui.lmp.business.homepage.vo.FrontUserCoupon;

@Service(value = "frontUserCouponService")
public class FrontUserCouponServiceImpl implements FrontUserCouponService
{

    @Autowired
    private FrontUserCouponMapper frontUserCouponMapper;

    /**
     * 根据用户查询优惠券
     * 
     * @param params
     * @return List
     */
    @Override
    public List<FrontUserCoupon> selectUserCouponByUserId(
            Map<String, Object> params)
    {

        return frontUserCouponMapper.selectUserCouponByUserId(params);
    }

    /**
     * 根据用户查询优惠券
     * 
     * @param params
     * @return List
     */
    @Override
    public List<FrontUserCoupon> selectOverdueUserCouponByUserId(
            Map<String, Object> params)
    {

        return frontUserCouponMapper.selectOverdueUserCouponByUserId(params);
    }

    /**
     * 数量统计 可使用 status == 1 已过期  and status == 2 time 当前时间
     * 
     * @param params
     * @return List
     */
    @Override
    public int sumQuantityByUserId(Map<String, Object> params)
    {

        return frontUserCouponMapper.sumQuantityByUserId(params);
    }
}
