package com.xiangrui.lmp.business.homepage.service.impl;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FfrontUserMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontAccountLogMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontWithdrawalLogMapper;
import com.xiangrui.lmp.business.homepage.service.FrontWithdrawalLogService;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.FrontWithdrawalLog;

@Service(value = "frontWithdrawalLogService")
public class FrontWithdrawalLogServiceImpl implements FrontWithdrawalLogService
{

    @Autowired
    private FrontWithdrawalLogMapper frontWithdrawalLogMapper;
    
    @Autowired
    private FfrontUserMapper ffrontUserMapper;
    
    @Autowired
    private  FrontAccountLogMapper frontAccountLogMapper;

    /**
     * 提现申请 账户提现记录表登录，前台用户更新金额项目,账户记录表登录
     * 
     * @param params
     * @return
     */
    @Override
    public void withdrawal(Map<String, Object> params)
    {
    	//提现申请
        frontWithdrawalLogMapper.insertWithdrawalLog((FrontWithdrawalLog) params.get("frontWithdrawalLog"));

        //更新金额
        ffrontUserMapper.updateBalance((FrontUser) params.get("frontUser"));
        
        //新增消费记录
        frontAccountLogMapper.insertFrontAccountLog((FrontAccountLog) params.get("frontAccountLog"));
    }
    
    /**
     * 提现取消账户提现记录表登录，前台用户更新金额项目,账户记录表登录
     * 
     * @param params
     * @return
     */
    @Override
    public void withdrawalCancel(Map<String, Object> params)
    {

        frontWithdrawalLogMapper
                .updateWithdrawalLogByTransId((FrontWithdrawalLog) params
                        .get("frontWithdrawalLog"));

        ffrontUserMapper.updateBalance((FrontUser) params.get("frontUser"));

        frontAccountLogMapper.updateStatusByLogId((FrontAccountLog) params
                .get("frontAccountLog"));
    }
    
    /**
     * 根据用户ID统计存在申请提现记录
     * @param front_user_id
     * @return
     */
    public int countWithdrawalLogByFrontUserId(int front_user_id)
    {
    	return frontWithdrawalLogMapper.countWithdrawalLogByFrontUserId(front_user_id);
    }
}
