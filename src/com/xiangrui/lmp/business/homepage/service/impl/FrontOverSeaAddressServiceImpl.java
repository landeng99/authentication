package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontOverSeaAddressMapper;
import com.xiangrui.lmp.business.homepage.service.FrontOverSeaAddressService;
import com.xiangrui.lmp.business.homepage.vo.FrontOverSeaAddress;

@Service("frontOverSeaAddressService")
public class FrontOverSeaAddressServiceImpl implements FrontOverSeaAddressService
{

	@Autowired
	private FrontOverSeaAddressMapper frontOverSeaAddressMapper;

	@Override
	public List<FrontOverSeaAddress> queryAll()
	{
		return frontOverSeaAddressMapper.queryAll();
	}

	@Override
	public FrontOverSeaAddress queryAllById(int id)
	{
		// TODO Auto-generated method stub
		return frontOverSeaAddressMapper.queryAllById(id);
	}

	public List<FrontOverSeaAddress> queryAllByUserId(Integer user_id)
	{
		return frontOverSeaAddressMapper.queryAllByUserId(user_id);
	}
}
