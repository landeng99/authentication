package com.xiangrui.lmp.business.homepage.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.xiangrui.lmp.business.homepage.mapper.FrontIdcardMapper;
import com.xiangrui.lmp.business.homepage.service.FrontIdcardService;
import com.xiangrui.lmp.business.homepage.vo.FrontIdcard;
import com.xiangrui.lmp.business.homepage.vo.FrontIdcardResult;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.util.HttpUtils;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.StringUtil;

@Service(value = "frontIdcardService")
public class FrontIdcardServiceImpl implements FrontIdcardService{
	
	private static Logger logger = Logger.getLogger(FrontIdcardServiceImpl.class);
	
	@Autowired
	private FrontIdcardMapper frontIdcardMapper;
	
	/**
	 * 向阿里接口拿数据：
	 * https://market.aliyun.com/products/57000002/cmapi012484.html?spm=5176.730006-cmapi012484.102.10.oO6lMT#sku=yuncode648400000
	 * @param idcard 身份证号
	 * @param realName 身份证上名字
	 * @return
	 */
	private FrontIdcard getIdcardFromAli(String idcard, String realName){
		 String host = "http://aliyunverifyidcard.haoservice.com";
		    String path = "/idcard/VerifyIdcardv2";
		    String method = "GET";
		    Map<String, String> headers = new HashMap<String, String>();
		    //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
		    headers.put("Authorization", "APPCODE " + SYSConstant.IDCARD_APPCODE);
		    Map<String, String> querys = new HashMap<String, String>();
		    querys.put("cardNo", idcard);
		    querys.put("realName", realName);
//		    try {
//				querys.put("realName", URLEncoder.encode(realName,"UTF-8"));
//			} catch (UnsupportedEncodingException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//				logger.error("getIdcardFromAli encode err realName: " + realName);
//				return null;
//			}

		    String body = "";
		    try {
		    	/**
		    	* 重要提示如下:
		    	* HttpUtils请从
		    	* https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
		    	* 下载
		    	*
		    	* 相应的依赖请参照
		    	* https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
		    	*/
		    	HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
		    	logger.info("response: "+response.toString());
		    	//获取response的body
//		    	System.out.println(EntityUtils.toString(response.getEntity()));
		    	body = EntityUtils.toString(response.getEntity());
		    	logger.info("body: "+body);
		    } catch (Exception e) {
		    	e.printStackTrace();
		    	logger.error("getIdcardFromAli doGet err idcard: " + idcard);
				return null;
		    }
		    
		    if( StringUtil.isEmpty(body) )
		    	return null;
		    
		    try {
		        
		    	List<String> re = JSONUtil.readValueFromJson(body,"result");
		    	FrontIdcardResult idResult = JSON.parseObject(re.get(0), FrontIdcardResult.class);
		    	if( null==idResult || !idResult.isIsok() )
		    	    return null;
		    	
		    	return new FrontIdcard(idResult);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("getIdcardFromAli jsonToBean err idcard: " + idcard + " name: "+ realName +" result: " + body+
				    " error: "+e.toString());
				return null;
			}
	}

	@Override
	public FrontIdcard queryByIdcard(String idcard, String realName) {
		List<FrontIdcard> idcList = frontIdcardMapper.queryByIdcard(idcard);
		if( !CollectionUtils.isEmpty(idcList) ){
//		    String realNameUtf8 = "";
//		    try {
//                realNameUtf8 = URLEncoder.encode(realName,"UTF-8");
//            }
//            catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//                logger.error("realName encode utf8 error, realName: "+realName+" error: "+e.toString());
//            }
		    for( FrontIdcard f: idcList ){
		        if( f.getRealname().equals(realName) )
		            return f;
		    }
		}
		
		// 向阿里接口拿
		FrontIdcard frontIdcard = getIdcardFromAli(idcard, realName);
		if( null==frontIdcard )
		    return null;
		
		frontIdcardMapper.insertFrontIdcard(frontIdcard);
		
		return frontIdcard;
	}

	@Override
	public void insertFrontIdcard(FrontIdcard frontIdcard) {
		frontIdcardMapper.insertFrontIdcard(frontIdcard);
	}
}
