package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontCouponUsedMapper;
import com.xiangrui.lmp.business.homepage.service.FrontCouponUsedService;
import com.xiangrui.lmp.business.homepage.vo.FrontUserCoupon;

@Service(value = "frontCouponUsedService")
public class FrontCouponUsedServiceImpl implements FrontCouponUsedService
{

    @Autowired
    private FrontCouponUsedMapper frontCouponUsedMapper;

    /**
     * 已使用优惠券(为方便页面显示 此处用FrontUserCoupon)
     * 
     * @param user_id
     * @return List
     */
    @Override
    public List<FrontUserCoupon> selectCouponUsedByUserId(int user_id)
    {

        return frontCouponUsedMapper.selectCouponUsedByUserId(user_id);
    }

    /**
     * 用户优惠券数量统计
     * 
     * @param user_id
     * @return int
     */
    @Override
    public int sumQuantityByUserId(int user_id)
    {

        return frontCouponUsedMapper.sumQuantityByUserId(user_id);
    }

}
