package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontGoodsTypeMapper;
import com.xiangrui.lmp.business.homepage.service.FrontGoodsTypeService;
import com.xiangrui.lmp.business.homepage.vo.FrontGoodsType;
@Service(value = "frontGoodsTypeService")
public class FrontGoodsTypeServiceImpl implements FrontGoodsTypeService
{

    @Autowired
    private FrontGoodsTypeMapper frontGoodsTypeMapper;
    
    @Override
    public List<FrontGoodsType> queryAllGoodsType(int parent_id)
    {
        // TODO Auto-generated method stub
        return frontGoodsTypeMapper.queryAllGoodsType(parent_id);
    }

    @Override
    public List<FrontGoodsType> queryChildGoodsType()
    {
        // TODO Auto-generated method stub
        return frontGoodsTypeMapper.queryChildGoodsType();
    }

}
