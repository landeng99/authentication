package com.xiangrui.lmp.business.homepage.service.impl;

import java.awt.Color;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.xiangrui.lmp.business.admin.pkg.controller.PackageController;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.UnusualPkgMapper;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
import com.xiangrui.lmp.business.admin.quotationManage.mapper.QuotationManageMapper;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.homepage.controller.HomePkgGoodsController;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgGoodsMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgMapper;
import com.xiangrui.lmp.business.homepage.service.FrontPkgAttachServiceService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontPkgAttachService;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.util.AddressUtil;
import com.xiangrui.lmp.util.DateUtil;
import com.xiangrui.lmp.util.IdentifierCreator;
import com.xiangrui.lmp.util.NumberUtils;

@Service(value = "frontPkgService")
public class PkgServiceImpl implements PkgService
{

	@Autowired
	private QuotationManageMapper quotationManageMapper;
	@Autowired
	private FrontPkgMapper frontPkgMapper;
	@Autowired
	private FrontPkgAttachServiceService frontPkgAttachServiceService;
	@Autowired
	private FrontPkgGoodsMapper frontPkgGoodsMapper;
	@Autowired
	private UnusualPkgMapper unusualPkgMapper;
	@Autowired
	private PkgAttachServiceMapper pkgAttachServiceMapper;
	@Autowired
	private FrontUserService frontUserService;

	@Override
	public List<Pkg> queryPkgByParam(Map<String, Object> params)
	{
		return frontPkgMapper.queryPkgByParam(params);
	}

	@Override
	public List<Pkg> queryPackageByLogisticsCode(String val)
	{
		return frontPkgMapper.queryPackageByLogisticsCode(val);
	}

	@Override
	public Pkg queryPackageByPackageId(int package_id)
	{
		return frontPkgMapper.queryPackageByPackageId(package_id);
	}

	@Override
	public void deletePkgByPackageId(int package_id)
	{
		frontPkgMapper.deletePkgByPackageId(package_id);
	}

	@Override
	public void deletePkgInGoodsByPackageId(int package_id)
	{
		frontPkgMapper.deletePkgInGoodsByPackageId(package_id);
	}

	@Override
	public void deletePkgAttachByPackageId(int package_id)
	{
		frontPkgMapper.deletePkgAttachByPackageId(package_id);
	}

	@Override
	public void deletePkgAndGoodsOrAttachByPackageId(int package_id)
	{

		Pkg pkg = frontPkgMapper.queryPackageByPackageId(package_id);
		// PackageLogUtil.log(pkg, null);
		if (pkg != null)
		{
			frontPkgMapper.deletePkgAttachByPackageId(package_id);
			frontPkgMapper.deletePkgInGoodsByPackageId(package_id);
			frontPkgMapper.deletePkgByPackageId(package_id);
			if (pkg.getOriginal_num()!=null) {
				HomePkgGoodsController.deleteOriginalNum(pkg.getOriginal_num());
				// 通过关联ID删除异常包裹
				List<UnusualPkg> unusualPkgList = unusualPkgMapper.queryUnusualPkgsByoriginalNum(pkg.getOriginal_num());
				if (unusualPkgList != null && unusualPkgList.size() > 0)
				{
					unusualPkgMapper.deleteUnusualPkgByConnectPackageId(package_id);
				}
			}
		}
	}

	public void deletePkgAndGoodsOrAttachByPackageId(int package_id, boolean isUnusualPKGDelete)
	{

		Pkg pkg = frontPkgMapper.queryPackageByPackageId(package_id);
		// PackageLogUtil.log(pkg, null);
		if (pkg != null)
		{
			if (pkg.getOriginal_num()!=null) {
				HomePkgGoodsController.deleteOriginalNum(pkg.getOriginal_num());
			}
			frontPkgMapper.deletePkgAttachByPackageId(package_id);
			frontPkgMapper.deletePkgInGoodsByPackageId(package_id);
			frontPkgMapper.deletePkgByPackageId(package_id);
			if (!isUnusualPKGDelete && pkg.getOriginal_num()!=null)
			{
				// 通过关联ID删除异常包裹
				List<UnusualPkg> unusualPkgList = unusualPkgMapper.queryUnusualPkgsByoriginalNum(pkg.getOriginal_num());
				if (unusualPkgList != null && unusualPkgList.size() > 0)
				{
					unusualPkgMapper.deleteUnusualPkgByConnectPackageId(package_id);
				}
			}
		}
	}

	@Override
	public List<Pkg> queryPackageByUserIdAndStatus(Map<String, Object> params)
	{

		return frontPkgMapper.queryPackageByUserIdAndStatus(params);
	}
	
	@Override
	public List<Pkg> queryPackageByUserIdAndStatusNormal(Map<String, Object> params)
	{
		return frontPkgMapper.queryPackageByUserIdAndStatusNormal(params);
	}	

	@Override
	public List<Pkg> queryPackageByUserIdAndPayStatus(Map<String, Object> params)
	{

		return frontPkgMapper.queryPackageByUserIdAndPayStatus(params);
	}
	@Override
	public List<Pkg> queryPackageByUserIdAndPayStatusCustom(Map<String, Object> params)
	{

		return frontPkgMapper.queryPackageByUserIdAndPayStatusCustom(params);
	}
	@Override
	public List<Pkg> queryPackageByUserIdAndPayStatusFreight(Map<String, Object> params)
	{

		return frontPkgMapper.queryPackageByUserIdAndPayStatusFreight(params);
	}
	@Override
	public List<Pkg> queryPackageByUserIdAndPaid(Map<String,Object> params){
		return frontPkgMapper.queryPackageByUserIdAndPaid(params);
	}
	
	@Override
	public List<Pkg> queryPackageByUserIdAndUnPaid(Map<String,Object> params){
		return frontPkgMapper.queryPackageByUserIdAndUnPaid(params);
	}
	
	@Override
	public void updatePkgForUseraddress(Pkg pkg)
	{
		frontPkgMapper.updatePkgForUseraddress(pkg);
	}

	@Override
	public List<Pkg> queryPackageByUserId(int user_id)
	{
		return frontPkgMapper.queryPackageByUserId(user_id);
	}

	@Override
	public List<Pkg> queryPkgBySearch(Map<String, Object> params)
	{
		return frontPkgMapper.queryPkgBySearch(params);
	}

	@Override
	public Pkg queryPkgBylogistics(Map<String, Object> params)
	{
		return frontPkgMapper.queryPkgBylogistics(params);
	}

	@Override
	public Pkg queryPkgByOriginalNum(Map<String, Object> params)
	{
		return frontPkgMapper.queryPkgByOriginalNum(params);
	}

	@Override
	public Pkg queryPkgByOriginalNumByStatus(Map<String, Object> params)
	{
		return frontPkgMapper.queryPkgByOriginalNumByStatus(params);
	}

	/**
	 * <!--包裹ID（多）查询包裹-->
	 * 
	 * @param list
	 * @return List
	 */
	@Override
	public List<Pkg> queryPackageByPackageIds(List<String> list)
	{
		if (list == null || list.size() == 0)
		{
			return null;
		}
		return frontPkgMapper.queryPackageByPackageIds(list);
	}

	/**
	 * <!--公司单号（多）查询包裹-->
	 * 
	 * @param list
	 * @return List
	 */
	@Override
	public List<Pkg> queryPackageBylogisticsCodes(List<String> list)
	{
		if (list == null || list.size() == 0)
		{
			return null;
		}
		return frontPkgMapper.queryPackageBylogisticsCodes(list);
	}

	@Override
	public List<Pkg> queryPackageByUserIdAndPkgPrint(Map<String, Object> params)
	{
		// TODO Auto-generated method stub
		return frontPkgMapper.queryPackageByUserIdAndPkgPrint(params);
	}

	/**
	 * 根据关联单号查询包裹
	 * 
	 * @param original_num
	 * @return
	 */
	@Override
	public List<Pkg> queryPackageByOriginalNum(String original_num)
	{
		return frontPkgMapper.queryPackageByOriginalNum(original_num);
	}

	/**
	 * 根据user_id和包裹状态、到库状态查询包裹
	 * 
	 * @param user_id
	 *            ,status
	 * @return list
	 */
	public List<Pkg> queryArrvivedPackage(Map<String, Object> params)
	{
		return frontPkgMapper.queryArrvivedPackage(params);
	}

	/**
	 * 删除包裹和包裹相关的所有信息
	 * 
	 * @param package_id
	 */
	public void deleteAllPkgByPackageId(int package_id, boolean isUnusualPKGDelete)
	{
		List<FrontPkgAttachService> frontPkgAttachService = frontPkgAttachServiceService.queryPkgAttachBypkgid(package_id);
		if (null != frontPkgAttachService && !frontPkgAttachService.isEmpty())
		{
			Map<String, Object> params = new HashMap<String, Object>();
			// 存放package_id
			List<String> list = new ArrayList<String>();
			list.add(String.valueOf(package_id));
			for (FrontPkgAttachService frontPkgAttach : frontPkgAttachService)
			{
				// 获取package_group
				String package_group = frontPkgAttach.getPackage_group();
				String[] array = package_group.split(",");
				// 去重,将package_Id放入list中
				for (String package_Id : array)
				{
					if (!list.contains(package_Id))
					{
						list.add(package_Id);
					}
					// 判断是否为合箱增值服务
					if (frontPkgAttach.getAttach_id() != BaseAttachService.MERGE_PKG_ID)
					{
						params.put("package_group", package_Id);
						List<FrontPkgAttachService> frontPkgAttachGroup = frontPkgAttachServiceService.queryPkgAttachByPkgGroup(params);
						if (null != frontPkgAttachGroup && !frontPkgAttachGroup.isEmpty())
						{
							for (FrontPkgAttachService fPkgService : frontPkgAttachGroup)
							{
								if (!list.contains(String.valueOf(fPkgService.getPackage_id())))
								{
									list.add(String.valueOf(fPkgService.getPackage_id()));
								}
							}
						}
					}
				}
			}
			// 遍历list
			for (String pkgid : list)
			{
				// 通过package_id查询增值服务信息
				List<FrontPkgAttachService> pkgAttach = frontPkgAttachServiceService.queryPkgAttachBypkgid(Integer.parseInt(pkgid));
				if (null != pkgAttach && !pkgAttach.isEmpty())
				{
					for (FrontPkgAttachService pkgAttachService : pkgAttach)
					{
						// 增值服务未完成
						// if (pkgAttachService.getStatus() !=
						// FrontPkgAttachService.ATTACH_FINISH)
						// {
						this.deletePkgAndGoodsOrAttachByPackageId(pkgAttachService.getPackage_id(), isUnusualPKGDelete);
						// }
					}
				}
				else
				{
					this.deletePkgAndGoodsOrAttachByPackageId(Integer.parseInt(pkgid), isUnusualPKGDelete);
				}
			}
		}
		else
		{
			this.deletePkgAndGoodsOrAttachByPackageId(package_id, isUnusualPKGDelete);
		}
	}

	/**
	 * 新增包裹
	 * 
	 * @param user_id
	 * @param original_num
	 * @param actual_weight
	 * @param osaddr_id
	 * @return
	 */
	public int addPkg(int user_id, String original_num, float actual_weight, int osaddr_id, String exception_package_flag, int payType)
	{
		Pkg pkg = new Pkg();
		FrontUser frontUser = frontUserService.queryFrontUserById(user_id);
		int userType = frontUser.getUser_type();
		String logistics_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PACKAGE);
		pkg.setLogistics_code(logistics_code);
		pkg.setUser_id(user_id);
		pkg.setOriginal_num(original_num);
		pkg.setActual_weight(actual_weight);
		pkg.setWeight(PackageController.calculateFeeWeith(userType, actual_weight));
		pkg.setOsaddr_id(osaddr_id);
		pkg.setException_package_flag(exception_package_flag);
		pkg.setPay_type(payType);
		pkg.setStatus(Pkg.LOGISTICS_STORE_WAITING);
		pkg.setArrive_status(Pkg.STORE_PACKAGE_ARRIVED);
		pkg.setArrive_time(new Timestamp(System.currentTimeMillis()));
		pkg.setCreateTime(new Timestamp(System.currentTimeMillis()));
		// 001)此处查询报价单，将报价单id做set包裹处理
		QuotationManage qm = null;
		int express_package = 0;
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("osaddr_id", osaddr_id);
		map.put("express_package", express_package);
		map.put("user_type", userType);
		map.put("user_id", user_id);
		List<QuotationManage> QuotationManageList = quotationManageMapper.getQuotaByPkgInfo(map);
		if (QuotationManageList != null && QuotationManageList.size() > 0) {
			qm = QuotationManageList.get(0);
			if (qm != null) {
				pkg.setQuota_id(qm.getQuota_id());
			}
		}
		
		frontPkgGoodsMapper.addPkg(pkg);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("original_num", original_num);
		params.put("user_id", user_id);
		Pkg tempPkg = frontPkgMapper.queryPkgByOriginalNum(params);
		return tempPkg.getPackage_id();
	}

//	private float countFeeWeight(float actual_weight)
//	{
//		float result = 2f;
//		if (actual_weight > 2.1)
//		{
//			int intPart = (int) actual_weight;
//			float baseCount = 0f + intPart;
//			if (actual_weight < (baseCount + 0.1f))
//			{
//				result = baseCount;
//			}
//			else if (actual_weight >= (baseCount + 0.1f) && actual_weight < (baseCount + 0.51f))
//			{
//				result = baseCount + 0.5f;
//			}
//			else if (actual_weight >= (baseCount + 0.51f) && actual_weight < (baseCount + 1.1f))
//			{
//				result = baseCount + 1.0f;
//			}
//		}
//		return result;
//	}

	/**
	 * 根据user_id和包裹状态查询前台待处理包裹（后台异常包裹关联后的显示）
	 * 
	 * @param user_id
	 *            ,status
	 * @return list
	 */
	public List<Pkg> queryNeedToHandlePackage(Map<String, Object> params)
	{
		return frontPkgMapper.queryNeedToHandlePackage(params);
	}
	
	public boolean isNeedToHandlePackage(int status, int enable, int user_id){
	    Map<String, Object> map = new HashMap<>();
	    map.put("status", status);
	    map.put("enable", enable);
	    map.put("user_id", user_id);
	    List<Pkg> list = queryNeedToHandlePackage(map);
	    return !CollectionUtils.isEmpty(list);
	}

	/**
	 * 根据运单号、包裹创建时间范围、打印状态查询包裹
	 * 
	 * @param params
	 * @return
	 */
	public List<Pkg> queryPkgBylogisticsAndPackageCreateTimeRangeAndPrintStatus(Map<String, Object> params)
	{
		return frontPkgMapper.queryPkgBylogisticsAndPackageCreateTimeRangeAndPrintStatus(params);
	}

	/**
	 * 根据用户ID,包裹新建时间段查询导出包裹
	 * 
	 * @param params
	 * @return
	 */
	public List<Pkg> queryFrontEndExportPackage(Map<String, Object> params)
	{
		List<Pkg> reList = new ArrayList<Pkg>();
		List<Pkg> queryList = frontPkgMapper.queryFrontEndExportPackage(params);
		if (queryList != null && queryList.size() > 0)
		{
			String splitPackageGroup = "";
			String splitOriginal_num = "";
			Timestamp splitArrive_time = null;
			int needSetColor = 0;
			int splitPackage_id = -1;
			for (Pkg pkg : queryList)
			{
				if (checkPklExist(reList, pkg))
				{
					String attach_id = StringUtils.trimToNull(pkg.getAttach_id_list());
					if (attach_id != null)
					{
						// 分箱
						if (attach_id.contains("" + BaseAttachService.SPLIT_PKG_ID))
						{
							List<PkgAttachService> pkgAttachServices = pkgAttachServiceMapper.queryPkgAttachServiceForSplitPackageDisplay("" + pkg.getPackage_id());
							if (pkgAttachServices != null && pkgAttachServices.size() > 0)
							{
								PkgAttachService pkgAttachService = pkgAttachServices.get(0);
								String tempSplitPackageGroup = pkgAttachService.getPackage_group();
								if (!tempSplitPackageGroup.equals(splitPackageGroup))
								{
									List<String> splitPackageIdsList = new ArrayList<String>();
									for (PkgAttachService tempPkgAttachService : pkgAttachServices)
									{
										splitPackageIdsList.add("" + tempPkgAttachService.getPackage_id());
									}
									List<String> tempSplitPackageIdList = new ArrayList<String>();
									tempSplitPackageIdList.add(tempSplitPackageGroup);
									// 物流单号
									String splitExpress_num = null;
									List<Pkg> beforeSplitList = frontPkgMapper.queryFrontEndExportPackageByPackageIdList(tempSplitPackageIdList);
									if (beforeSplitList != null && beforeSplitList.size() > 0)
									{
										Pkg beforeSplitPkg = beforeSplitList.get(0);
										splitOriginal_num = beforeSplitPkg.getOriginal_num();
										splitPackage_id = beforeSplitPkg.getPackage_id();
										splitArrive_time = beforeSplitPkg.getArrive_time();
										splitExpress_num = beforeSplitPkg.getExpress_num();
									}
									List<Pkg> splitList = frontPkgMapper.queryFrontEndExportPackageByPackageIdList(splitPackageIdsList);
									if (splitList != null && splitList.size() > 0)
									{
										boolean addSplit = true;
										int ddSetColor = needSetColor++ % 2 == 0 ? 0 : 1;
										for (Pkg temppkg : splitList)
										{
											if (addSplit)
											{
												temppkg.setSplitCount(splitList.size());
												addSplit = false;
											}
											else
											{
												temppkg.setSplitCount(-1);
											}
											temppkg.setOriginal_num(splitOriginal_num);
											temppkg.setDisplayColor(ddSetColor);
											temppkg.setSplitPackage_id(splitPackage_id);
											// temppkgOperate.setPackage_id(splitPackage_id);
											temppkg.setAttach_id(BaseAttachService.SPLIT_PKG_ID);
											temppkg.setArrive_time(splitArrive_time);
											temppkg.setExpress_num(splitExpress_num);
											addPkgOperate(reList, temppkg);
										}
									}
								}
								splitPackageGroup = tempSplitPackageGroup;
							}
						}
						// 合箱
						else if (attach_id.contains("" + BaseAttachService.MERGE_PKG_ID))
						{
							List<PkgAttachService> pkgAttachServices = pkgAttachServiceMapper.queryPkgAttachServiceForMergePackageDisplay("" + pkg.getPackage_id());
							if (pkgAttachServices != null && pkgAttachServices.size() > 0)
							{
								PkgAttachService pkgAttachService = pkgAttachServices.get(0);
								String mergePackageGroup = pkgAttachService.getPackage_group();
								List<String> mergePackageIdsList = new ArrayList<String>();
								if (mergePackageGroup != null)
								{
									String[] mergePackageGroupArray = mergePackageGroup.split(",");
									for (String tempMergePackageGroup : mergePackageGroupArray)
									{
										mergePackageIdsList.add("" + tempMergePackageGroup);
									}
								}
								List<Pkg> mergeList = frontPkgMapper.queryFrontEndExportPackageByPackageIdList(mergePackageIdsList);
								if (mergeList != null && mergeList.size() > 0)
								{
									boolean addMerge = true;
									int ddSetColor = needSetColor++ % 2 == 0 ? 0 : 1;
									for (Pkg temppkg : mergeList)
									{
										if (addMerge)
										{
											temppkg.setMergeCount(mergeList.size());
											addMerge = false;
										}
										else
										{
											temppkg.setMergeCount(-1);
										}
										temppkg.setAfterMergeLogistics_code(pkg.getLogistics_code());
										temppkg.setAttach_service(pkg.getAttach_service());
										temppkg.setEms_code(pkg.getEms_code());
										temppkg.setOperated_status(pkg.getOperated_status());
										temppkg.setOperate_time(pkg.getOperate_time());
										temppkg.setActual_weight(pkg.getActual_weight());
										temppkg.setPay_status(pkg.getPay_status());
										temppkg.setReceiver(pkg.getReceiver());
										temppkg.setMobile(pkg.getMobile());
										temppkg.setStreet(pkg.getStreet());
										temppkg.setRegion(pkg.getRegion());
										temppkg.setDisplayColor(ddSetColor);
										temppkg.setAttach_id(BaseAttachService.MERGE_PKG_ID);
										temppkg.setIdcard(pkg.getIdcard());
										temppkg.setIdcardImg(pkg.getIdcardImg());
										addPkgOperate(reList, temppkg);
									}
								}
							}
						}
						else
						{
							pkg.setDisplayColor(needSetColor++ % 2 == 0 ? 0 : 1);
							addPkgOperate(reList, pkg);
						}
					}
					else
					{
						pkg.setDisplayColor(needSetColor++ % 2 == 0 ? 0 : 1);
						addPkgOperate(reList, pkg);
					}
				}
			}
		}
		return reList;
	}

	/**
	 * 导出前台包裹
	 * 
	 * @param fileName
	 * @param pkgList
	 * @return
	 */
	public XSSFWorkbook frontEndExportPackageHandle(String fileName, List<Pkg> pkgList)
	{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		// 新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(fileName);
		// 第一列固定
		xssfSheet.createFreezePane(0, 1, 0, 1);

		// 颜色蓝色
		XSSFColor yellowColor = new XSSFColor(new Color(185, 211, 238));
		// 白色
		XSSFColor whiteColor = new XSSFColor(Color.WHITE);

		// 样式白色居中
		XSSFCellStyle style1 = xssfWorkbook.createCellStyle();
		style1.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style1.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style1.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style1.setFillForegroundColor(whiteColor);
		style1.setWrapText(true);

		// 样式蓝色居中
		XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
		style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		style2.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style2.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style2.setFillForegroundColor(yellowColor);
		style2.setWrapText(true);

		// 列宽
		xssfSheet.setColumnWidth(0, 4000);
		xssfSheet.setColumnWidth(1, 4000);
		xssfSheet.setColumnWidth(2, 8000);
		xssfSheet.setColumnWidth(3, 4000);
		xssfSheet.setColumnWidth(4, 4000);
		xssfSheet.setColumnWidth(5, 4000);
		xssfSheet.setColumnWidth(6, 4000);
		xssfSheet.setColumnWidth(7, 4000);
		xssfSheet.setColumnWidth(8, 8000);
		xssfSheet.setColumnWidth(9, 4000);
		xssfSheet.setColumnWidth(10, 4000);
		xssfSheet.setColumnWidth(11, 4000);
		xssfSheet.setColumnWidth(12, 4000);
		xssfSheet.setColumnWidth(13, 4000);
		xssfSheet.setColumnWidth(14, 8000);
		xssfSheet.setColumnWidth(15, 8000);
		xssfSheet.setColumnWidth(16, 4000);

		// 表头列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		List<String> columnsList = new ArrayList<String>();
		columnsList.add("包裹创建日期");
		columnsList.add("公司单号");
		columnsList.add("关联单号");
		columnsList.add("物流单号");
		columnsList.add("国内派送单号");
		columnsList.add("所属仓库");
		columnsList.add("当前状态");
		columnsList.add("当前状态时间");
		columnsList.add("内件明细");
		columnsList.add("实际重量");
		columnsList.add("增值服务");
		columnsList.add("支付状态");
		columnsList.add("收件人");
		columnsList.add("收件人电话");
		columnsList.add("收件人地址");
		columnsList.add("身份证");
		columnsList.add("身份证图片");

		for (int i = 0; i < columnsList.size(); i++)
		{
			XSSFCell cell = firstXSSFRow.createCell(i);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(columnsList.get(i));
			cell.setCellStyle(style1);
		}

		Pkg pkg = null;
		int rowCount = 1;
		for (int j = 0; j < pkgList.size(); j++)
		{
			pkg = pkgList.get(j);
			XSSFRow row = xssfSheet.createRow(rowCount++);

			// 包裹创建日期
			XSSFCell cell0 = row.createCell(0);
			cell0.setCellValue(DateUtil.getDateFormatSting(pkg.getCreateTime(), "yyyy/MM/dd"));
			cell0.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
			// 公司单号
			if (pkg.getMergeCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkg.getMergeCount() - 2, 1, 1);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			XSSFCell cell1 = row.createCell(1);
			if (pkg.getMergeCount() != -1)
			{
				cell1.setCellValue(pkg.getMergeCount() == 0 ? StringUtils.trimToEmpty(pkg.getLogistics_code()) : StringUtils.trimToEmpty(pkg.getAfterMergeLogistics_code()));
			}
			cell1.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 关联单号
			if (pkg.getSplitCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkg.getSplitCount() - 2, 2, 2);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}

			XSSFCell cell2 = row.createCell(2);
			if (pkg.getSplitCount() != -1)
			{
				cell2.setCellValue(StringUtils.trimToEmpty(pkg.getOriginal_num()));
			}
			cell2.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
			// 物流单号
			if (pkg.getExpressNumCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkg.getExpressNumCount() - 2, 3, 3);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			XSSFCell cell3 = row.createCell(3);
			String expressNum = StringUtils.trimToEmpty(pkg.getExpress_num());
			cell3.setCellValue(expressNum);
			cell3.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 国内派送单号
			XSSFCell cell4 = row.createCell(4);
			cell4.setCellValue(StringUtils.trimToEmpty(pkg.getEms_code()));
			cell4.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 所属仓库
			XSSFCell cell5 = row.createCell(5);
			cell5.setCellValue(StringUtils.trimToEmpty(pkg.getWarehouse()));
			cell5.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 当前状态
			XSSFCell cell6 = row.createCell(6);
			cell6.setCellValue(getStatusTextByStatus(pkg.getOperated_status()));
			cell6.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 当前状态时间
			XSSFCell cell7 = row.createCell(7);
			cell7.setCellValue(DateUtil.getDateFormatSting(pkg.getOperate_time(), "yyyy/MM/dd HH:mm:ss"));
			cell7.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 内件明细
			XSSFCell cell8 = row.createCell(8);
			cell8.setCellValue(StringUtils.trimToEmpty(pkg.getGoods_name()));
			cell8.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 实际重量
			XSSFCell cell9 = row.createCell(9);
			cell9.setCellValue(NumberUtils.scaleMoneyData3(pkg.getActual_weight()));
			cell9.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);

			// 增值服务
			if (pkg.getSplitCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkg.getSplitCount() - 2, 10, 10);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			else if (pkg.getMergeCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkg.getMergeCount() - 2, 10, 10);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			else if (pkg.getExpressNumCount() > 0)
			{
				CellRangeAddress cra18 = new CellRangeAddress(rowCount - 1, rowCount + pkg.getExpressNumCount() - 2, 10, 10);
				// 在sheet里增加合并单元格
				xssfSheet.addMergedRegion(cra18);
			}
			XSSFCell cell10 = row.createCell(10);
			if (pkg.getAttach_id_list() != null)
			{
				if (pkg.getAttach_id_list().contains("4"))
				{
					if (pkg.getSplitCount() != -1)
					{
						cell10.setCellValue(StringUtils.trimToEmpty(pkg.getAttach_service()));
					}
				}
				else if (pkg.getAttach_id_list().contains("7"))
				{
					if (pkg.getMergeCount() != -1)
					{
						cell10.setCellValue(StringUtils.trimToEmpty(pkg.getAttach_service()));
					}
				}
				else if (pkg.getAttach_id_list().contains("-100"))
				{
					if (pkg.getExpressNumCount() != -1)
					{
						cell10.setCellValue(StringUtils.trimToEmpty(pkg.getAttach_service()));
					}
				}
				else
				{
					cell10.setCellValue(StringUtils.trimToEmpty(pkg.getAttach_service()));
				}
			}
			else
			{
				cell10.setCellValue(StringUtils.trimToEmpty(pkg.getAttach_service()));
			}
			cell10.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
			// 支付状态
			XSSFCell cell11 = row.createCell(11);
			// 1，未支付，2：已支付；3：关税未支付，4关税已支付
			String payText = null;
			if (pkg.getPay_status() == 1)
			{
				payText = "未支付";
			}
			else if (pkg.getPay_status() == 2)
			{
				payText = "已支付";
			}
			else if (pkg.getPay_status() == 3)
			{
				payText = "关税未支付";
			}
			else if (pkg.getPay_status() == 4)
			{
				payText = "关税已支付";
			}
			cell11.setCellValue(payText);
			cell11.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
			// 收件人
			XSSFCell cell12 = row.createCell(12);
			cell12.setCellValue(StringUtils.trimToEmpty(pkg.getReceiver()));
			cell12.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
			// 收件人电话
			XSSFCell cell13 = row.createCell(13);
			cell13.setCellValue(StringUtils.trimToEmpty(pkg.getMobile()));
			cell13.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
			// 收件人地址
			XSSFCell cell14 = row.createCell(14);
			cell14.setCellValue(StringUtils.trimToEmpty(AddressUtil.getAddrssFromJsonStr(pkg.getRegion())+pkg.getStreet()));
			cell14.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
			
			// 身份证
			XSSFCell cell15 = row.createCell(15);
			cell15.setCellValue(StringUtils.trimToEmpty(pkg.getIdcard()));
			cell15.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
			
			// 身份证图片
			XSSFCell cell16 = row.createCell(16);
			cell16.setCellValue(StringUtils.trimToEmpty(pkg.getIdcardImg()).equals("")?"无":"");
			cell16.setCellStyle(pkg.getDisplayColor() % 2 == 0 ? style2 : style1);
		}
		return xssfWorkbook;
	}

	private List<Pkg> addPkgOperate(List<Pkg> pkgList, Pkg pkg)
	{
		if (checkPklExist(pkgList, pkg))
		{
			pkgList.add(pkg);
		}
		return pkgList;
	}

	private boolean checkPklExist(List<Pkg> pkgList, Pkg pkg)
	{
		boolean canAdd = true;
		if (pkgList != null && pkgList.size() > 0)
		{
			for (Pkg tempkg : pkgList)
			{
				if (tempkg.getPackage_id() == pkg.getPackage_id())
				{
					canAdd = false;
					break;
				}
			}
		}
		return canAdd;
	}

	public static String getStatusTextByStatus(int status)
	{
		String text = null;
		switch (status)
		{
			case BasePackage.LOGISTICS_STORE_WAITING:
				text = "待入库";
				break;
			case BasePackage.LOGISTICS_UNUSUALLY:
				text = "包裹异常";
				break;
			case BasePackage.LOGISTICS_STORAGED:
				text = "已入库";
				break;
			case BasePackage.LOGISTICS_SENT_ALREADY:
				text = "已出库";
				break;
			case BasePackage.LOGISTICS_SEND_WAITING:
				text = "待发货";
				break;
			case BasePackage.LOGISTICS_AIRLIFT_ALREADY:
				text = "已空运";
				break;
			case BasePackage.LOGISTICS_CUSTOMS_WAITING:
				text = "待清关";
				break;
			case BasePackage.LOGISTICS_CUSTOMS_ALREADY:
				text = "已清关派件中";
				break;
			case BasePackage.LOGISTICS_SIGN_IN:
				text = "已签收";
				break;
			case BasePackage.LOGISTICS_DISCARD:
				text = "废弃";
				break;
			case BasePackage.LOGISTICS_RETURN:
				text = "退货";
				break;
		}

		return text;
	}
	
    /**
     * 根据单号查询包裹
     * @param pkg
     * @return 
     */ 
    public void updatePkgForOriginalNum(Pkg pkg)
    {
    	frontPkgMapper.updatePkgForOriginalNum(pkg);
    }

    @Override
    public Integer queryCountByUserId(Integer userId) {
        return frontPkgMapper.queryCountByUserId(userId);
    } 
    public  List<Pkg>  queryMypagesByqueryPames(Map<String, Object> paramsCount) {
        return frontPkgMapper.queryMypagesByqueryPames(  paramsCount);
    }
    @Override
    public  Integer queryMypagesCountByqueryPames(Map<String, Object> paramsCount) {
        return frontPkgMapper.queryMypagesCountByqueryPames(  paramsCount);
    }
}
