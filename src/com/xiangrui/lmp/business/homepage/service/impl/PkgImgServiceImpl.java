package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontPkgImgMapper;
import com.xiangrui.lmp.business.homepage.service.PkgImgService;
import com.xiangrui.lmp.business.homepage.vo.PackageImg;


@Service(value = "frontPkgImgService")
public class PkgImgServiceImpl implements PkgImgService
{

    @Autowired
    private FrontPkgImgMapper frontPkgImgMapper;
    
    @Override
    public int insertPkgImg(PackageImg packageImg)
    {
        
        return frontPkgImgMapper.insertPkgImg(packageImg);
        
    }

    @Override
    public List<PackageImg> queryPkgImg(PackageImg packageImg)
    {
      
        return frontPkgImgMapper.queryPkgImg(packageImg);
    }

    @Override
    public void deletePkgImg(int img_id)
    {
        // TODO Auto-generated method stub
        frontPkgImgMapper.deletePkgImg(img_id);
    }

}
