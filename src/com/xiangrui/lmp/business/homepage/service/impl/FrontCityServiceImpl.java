package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontCityMapper;
import com.xiangrui.lmp.business.homepage.service.FrontCityService;
import com.xiangrui.lmp.business.homepage.vo.FrontCity;

@Service(value = "frontCityService")
public class FrontCityServiceImpl implements FrontCityService
{

    @Autowired
    private FrontCityMapper frontCityMapper;
    
    @Override
    public List<FrontCity> queryAllCity()
    {
        // TODO Auto-generated method stub
        return frontCityMapper.queryAllCity();
    }

    @Override
    public List<FrontCity> queryCity(int father_id)
    {
        // TODO Auto-generated method stub
        return frontCityMapper.queryCity(father_id);
    }

}
