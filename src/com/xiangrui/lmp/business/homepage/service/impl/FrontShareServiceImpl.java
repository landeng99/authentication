package com.xiangrui.lmp.business.homepage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontShareMapper;
import com.xiangrui.lmp.business.homepage.service.FrontShareService;
import com.xiangrui.lmp.business.homepage.vo.FrontShare;

@Service(value = "frontShareService")
public class FrontShareServiceImpl implements FrontShareService
{

    @Autowired
    private FrontShareMapper frontShareMapper;

    @Override
    public void addShare(FrontShare frontShare)
    {
        // TODO Auto-generated method stub
        frontShareMapper.addShare(frontShare);
    }

    @Override
    public FrontShare queryPkgShare(int package_id)
    {
        // TODO Auto-generated method stub
        return frontShareMapper.queryPkgShare(package_id);
    }

    @Override
    public void updatePkgShare(FrontShare frontShare)
    {
        // TODO Auto-generated method stub
        frontShareMapper.updatePkgShare(frontShare);
    }

	@Override
	public FrontShare queryShareByUserId(int user_id) {
		return frontShareMapper.queryShareByUserId(user_id);
	}
}
