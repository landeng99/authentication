package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontReceiveAddressMapper;
import com.xiangrui.lmp.business.homepage.service.FrontReceiveAddressService;
import com.xiangrui.lmp.business.homepage.vo.FrontReceiveAddress;

@Service(value = "frontReceiveAddressService")
public class FrontReceiveAddressServiceImpl implements FrontReceiveAddressService
{
	@Autowired
	private FrontReceiveAddressMapper frontReceiveAddressMapper;

	@Override
	public List<FrontReceiveAddress> queryReceiveAddress(Map<String, Object> params)
	{
		// TODO Auto-generated method stub
		return frontReceiveAddressMapper.queryReceiveAddress(params);
	}

	@Override
	public List<FrontReceiveAddress> queryReceiveAddressByUserId(Map<String, Object> params)
	{
		// TODO Auto-generated method stub
		return frontReceiveAddressMapper.queryReceiveAddressByUserId(params);
	}

	@Override
	public FrontReceiveAddress queryReceiveAddressByAddressId(int address_id)
	{
		// TODO Auto-generated method stub
		return frontReceiveAddressMapper.queryReceiveAddressByAddressId(address_id);
	}

	@Override
	public int insertFrontReceiveAddress(FrontReceiveAddress frontReceiveAddress)
	{
		// TODO Auto-generated method stub
		return frontReceiveAddressMapper.insertFrontReceiveAddress(frontReceiveAddress);
	}

	@Override
	public void updateReceiveAddress(FrontReceiveAddress frontReceiveAddress)
	{
		// TODO Auto-generated method stub
		frontReceiveAddressMapper.updateReceiveAddress(frontReceiveAddress);
	}

	@Override
	public void deleteReceiveAddress(int address_id)
	{
		// TODO Auto-generated method stub
		frontReceiveAddressMapper.deleteReceiveAddress(address_id);
	}

	@Override
	public int updateReceiveFrontIdcard(Map<String, Object> params)
	{
		// TODO Auto-generated method stub
		return frontReceiveAddressMapper.updateReceiveFrontIdcard(params);
	}

	@Override
	public int updateReceiveBackIdcard(Map<String, Object> params)
	{
		// TODO Auto-generated method stub
		return frontReceiveAddressMapper.updateReceiveBackIdcard(params);
	}

	@Override
	public List<FrontReceiveAddress> queryReceiveUnloadIdcard(int user_id)
	{
		// TODO Auto-generated method stub
		return frontReceiveAddressMapper.queryReceiveUnloadIdcard(user_id);
	}

	/**
	 * 更新正面身份证图片和身份证号
	 * 
	 * @param params
	 * @return
	 */
	public int updateReceiveFrontIdcardAndPhoto(Map<String, Object> params)
	{
		return frontReceiveAddressMapper.updateReceiveFrontIdcardAndPhoto(params);
	}

	/**
	 * 更新背面身份证图片和身份证号
	 * 
	 * @param params
	 * @return
	 */
	public int updateReceiveBackIdcardAndPhoto(Map<String, Object> params)
	{
		return frontReceiveAddressMapper.updateReceiveBackIdcardAndPhoto(params);
	}

	/**
	 * 根据receiver和mobile查询包裹收货地址
	 * 
	 * @param params
	 * @return
	 */
	public List<FrontReceiveAddress> queryReceiveAddressByMobileAndReceiver(Map<String, Object> params)
	{
		return frontReceiveAddressMapper.queryReceiveAddressByMobileAndReceiver(params);
	}
	
    /**
     * 根据用户ID和receiver和mobile查询用户收货地址
     * @param params
     * @return
     */
    public List<FrontReceiveAddress> queryReceiveAddressByUserIdAndMobileAndReceiver(Map<String, Object> params)
    {
    	return frontReceiveAddressMapper.queryReceiveAddressByUserIdAndMobileAndReceiver(params);
    }

    /**
     * 根据收件人或手机查询用户地址 
     * @param param
     * @return
     */
	@Override
	public List<FrontReceiveAddress> queryAddress(Map<String, Object> param)
	{
		return this.frontReceiveAddressMapper.queryAddress(param);
	}
}
