package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontPkgAttachServiceMapper;
import com.xiangrui.lmp.business.homepage.service.FrontPkgAttachServiceService;
import com.xiangrui.lmp.business.homepage.vo.FrontPkgAttachService;

@Service(value = "frontPkgAttachServiceService")
public class FrontPkgAttachServiceServiceImpl implements
        FrontPkgAttachServiceService
{
    @Autowired
    private FrontPkgAttachServiceMapper frontPkgAttachServiceMapper;

    /**
     * 包裹增值服务费用
     * 
     * @param package_id
     * @return
     */
    @Override
    public List<FrontPkgAttachService> queryPkgAttachServiceForPayment(
            int package_id)
    {
        return frontPkgAttachServiceMapper
                .queryPkgAttachServiceForPayment(package_id);
    }

    @Override
    public List<FrontPkgAttachService> queryPkgAttachBypkgid(int package_id)
    {
        // TODO Auto-generated method stub
        return frontPkgAttachServiceMapper.queryPkgAttachBypkgid(package_id);
    }

    @Override
    public void deletePkgAttachBypkgid(int package_id)
    {
        // TODO Auto-generated method stub
        frontPkgAttachServiceMapper.deletePkgAttachBypkgid(package_id);
    }

    @Override
    public List<FrontPkgAttachService> queryPkgAttachByPkgGroup(
            Map<String, Object> params)
    {
        // TODO Auto-generated method stub
        return frontPkgAttachServiceMapper.queryPkgAttachByPkgGroup(params);
    }
}
