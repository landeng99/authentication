package com.xiangrui.lmp.business.homepage.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.homepage.mapper.FrontAccountLogMapper;
import com.xiangrui.lmp.business.homepage.service.FrontAccountLogService;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;

@Service(value = "frontAccountLog")
public class FrontAccountLogServiceImpl implements FrontAccountLogService
{

    @Autowired
    private FrontAccountLogMapper frontAccountLogMapper;

    /**
     * 
     * 
     * @param params
     * @return list
     */
    @Override
    public List<FrontAccountLog> queryAccountLog(Map<String, Object> params)
    {

        return frontAccountLogMapper.queryAccountLog(params);
    }

    /**
     * 会员三个月前 两年之内数据
     * 
     * @param params
     * @return list
     */
    @Override
    public List<FrontAccountLog> queryAccountLogBefore(
            Map<String, Object> params)
    {

        return frontAccountLogMapper.queryAccountLogBefore(params);
    }

    /**
     * 近一年实际消费额
     * 
     * @param params
     * @return float
     */
    @Override
    public float sumAmountByYear(Map<String, Object> params)
    {

        return frontAccountLogMapper.sumAmountByYear(params);
    }

    /**
     * 新增消费记录
     * 
     * @param frontAccountLog
     * @return
     */
    @Override
    public void insertFrontAccountLog(FrontAccountLog frontAccountLog)
    {

        frontAccountLogMapper.insertFrontAccountLog(frontAccountLog);
    }

    /**
     * 支付宝回传结果更新
     * 
     * @param frontAccountLog
     * @return
     */
    @Override
    public void updateStatusByOrderId(FrontAccountLog frontAccountLog)
    {

        frontAccountLogMapper.updateStatusByOrderId(frontAccountLog);
    }

    /**
     * 订单号查找消费记录
     * 
     * @param order_id
     * @return
     */
    @Override
    public FrontAccountLog selectAccountLogByOrderId(String order_id)
    {

        return frontAccountLogMapper.selectAccountLogByOrderId(order_id);
    }

    /**
     * ID查找消费记录
     * 
     * @param log_id
     * @return
     */
    @Override
    public FrontAccountLog selectAccountLogByLogId(int log_id)
    {

        return frontAccountLogMapper.selectAccountLogByLogId(log_id);
    }

}
