package com.xiangrui.lmp.business.homepage.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.attachService.mapper.AttachServiceMapper;
import com.xiangrui.lmp.business.admin.message.service.MessageSendService;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.UnusualPkgMapper;
import com.xiangrui.lmp.business.admin.pkg.service.PkgAttachServiceService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;
import com.xiangrui.lmp.business.admin.quotationManage.mapper.QuotationManageMapper;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.base.ExpressNumLotConnectBean;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgGoodsMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontReceiveAddressMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontUserAddressMapper;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontCity;
import com.xiangrui.lmp.business.homepage.vo.FrontReceiveAddress;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.init.SystemParamUtil;
import com.xiangrui.lmp.util.IdentifierCreator;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PackageLogUtil;

import net.sf.json.JSONArray;

@Service(value = "frontPkgGoodsService")
public class PkgGoodsServiceImpl implements PkgGoodsService
{
	
	@Autowired
	private QuotationManageMapper quotationManageMapper;

	@Autowired
	private FrontPkgGoodsMapper frontPkgGoodsMapper;
	
	@Autowired
	private FrontPkgMapper frontPkgMapper;

	@Autowired
	private FrontUserAddressMapper frontUserAddressMapper;

	@Autowired
	private FrontReceiveAddressMapper frontReceiveAddressMapper;

	@Autowired
	private PkgAttachServiceMapper pkgAttachServiceMapper;

	@Autowired
	private AttachServiceMapper attachServiceMapper;

	@Autowired
	private UnusualPkgMapper unusualPkgMapper;
	@Autowired
	private PkgAttachServiceService pkgAttachServiceService;
	@Autowired
	private PkgService frontPkgService;
	/**
	 * 前台用户
	 */
	@Autowired
	private FrontUserService ffrontUserService;
	// 发送Email｜SMS服务
	@Autowired
	private MessageSendService messageSendService;

	@Override
	public List<PkgGoods> queryPkgGoodsById(int package_id)
	{
		return frontPkgGoodsMapper.queryPkgGoodsById(package_id);
	}

	@Override
	public void addPkgGoods(List<PkgGoods> pkgGoods)
	{
		frontPkgGoodsMapper.addPkgGoods(pkgGoods);

		/*
		 * // 同步包裹日志记录 pkg.setPackage_id(package_id); //
		 * 前台客户添加的包裹,没有后台人员操作,故user=null PackageLogUtil.log(pkg, null);
		 */
	}

	@Override
	public int addPkg(Pkg pkg)
	{
		int cnt = 0;
		// 当前地址为用户地址id
		int receiveAddr_id = pkg.getAddress_id();
		List<Pkg> existPkgList = frontPkgService.queryPackageByLogisticsCode(pkg.getLogistics_code());
		// 防止重复提交
		if (existPkgList == null || existPkgList.size() == 0)
		{
			// 根据用户地址创建包裹地址
			FrontReceiveAddress receiveAddress = frontReceiveAddressMapper.queryReceiveAddressByAddressId(receiveAddr_id);

			// 身份证不存在，需要用收件人和手机号码去系统查检是否已经填写过身份证号码
			boolean wantToSendSMS = true;
			String existIdcard = null;
			String front_img = null;
			String backImg = null;
			int idcardStatus = 0;
			// if (StringUtils.trimToNull(receiveAddress.getIdcard()) == null)
			if (StringUtils.trimToNull(receiveAddress.getFront_img()) == null || StringUtils.trimToNull(receiveAddress.getBack_img()) == null)
			{

				Map<String, Object> params = new HashMap<String, Object>();
				params.put("mobile", receiveAddress.getMobile());
				params.put("receiver", receiveAddress.getReceiver());
				List<FrontUserAddress> addresslist = frontUserAddressMapper.queryAddressByMobileAndReceiver(params);
				if (addresslist != null && addresslist.size() > 0)
				{
					for (FrontUserAddress frontUserAddress : addresslist)
					{
						if(StringUtils.trimToNull(frontUserAddress.getIdcard())!=null)
						{
							existIdcard = frontUserAddress.getIdcard();
						}
						if (StringUtils.trimToNull(frontUserAddress.getFront_img()) != null && StringUtils.trimToNull(frontUserAddress.getBack_img()) != null)
						{
							wantToSendSMS = false;
							front_img = frontUserAddress.getFront_img();
							backImg = frontUserAddress.getBack_img();
							idcardStatus = frontUserAddress.getIdcard_status();
						}
					}
				}
				if (!wantToSendSMS)
				{
					List<FrontReceiveAddress> receiveAddresslist = frontReceiveAddressMapper.queryReceiveAddressByMobileAndReceiver(params);
					if (receiveAddresslist != null && receiveAddresslist.size() > 0)
					{
						for (FrontReceiveAddress frontReceiveAddress : receiveAddresslist)
						{
							if(StringUtils.trimToNull(frontReceiveAddress.getIdcard())!=null)
							{
								existIdcard = frontReceiveAddress.getIdcard();
							}
							if (StringUtils.trimToNull(frontReceiveAddress.getFront_img()) != null && StringUtils.trimToNull(frontReceiveAddress.getBack_img()) != null)
							{
								wantToSendSMS = false;
								front_img = frontReceiveAddress.getFront_img();
								backImg = frontReceiveAddress.getBack_img();
								idcardStatus = frontReceiveAddress.getIdcard_status();
							}
						}
					}
				}
			}
			else
			{
				wantToSendSMS = false;
				existIdcard = receiveAddress.getIdcard();
				front_img = receiveAddress.getFront_img();
				backImg = receiveAddress.getBack_img();
				idcardStatus = receiveAddress.getIdcard_status();
			}

			FrontUserAddress frontUserAddress = new FrontUserAddress();

			frontUserAddress.setUser_id(receiveAddress.getUser_id());

			frontUserAddress.setRegion(receiveAddress.getRegion());
			frontUserAddress.setStreet(receiveAddress.getStreet());
			frontUserAddress.setReceiver(receiveAddress.getReceiver());
			frontUserAddress.setMobile(receiveAddress.getMobile());
			frontUserAddress.setPostal_code(receiveAddress.getPostal_code());
			frontUserAddress.setTel(receiveAddress.getTel());
			frontUserAddress.setIsdefault(receiveAddress.getIsdefault());
			frontUserAddress.setIdcard(existIdcard);
			frontUserAddress.setFront_img(front_img);
			frontUserAddress.setBack_img(backImg);
			frontUserAddress.setIdcard_status(idcardStatus);
			frontUserAddress.setRefuse_reason(receiveAddress.getRefuse_reason());

			// 新建包裹地址
			frontUserAddressMapper.insertFrontUserAddress(frontUserAddress);

			pkg.setAddress_id(frontUserAddress.getAddress_id());

			Map<String, Object> param = new HashMap<String, Object>();
			param.put("original_num", pkg);
			param.put("status", BasePackage.LOGISTICS_UNUSUALLY);

			UnusualPkg unusualPkg = unusualPkgMapper.queryUnusualPkgByoriginalNum(pkg.getOriginal_num());

			// 异常包裹存在，则需要更新异常包裹即可
			if (null != unusualPkg)
			{
				// 正常状态
				unusualPkg.setUnusual_status(UnusualPkg.STATUS_NORMAL);

				// 归属人
				unusualPkg.setOwner(receiveAddress.getUser_id());
				unusualPkg.setUpdate_time(new Date());
				unusualPkgMapper.updateUnusualPkgStatus(unusualPkg);
				
				pkg.setException_package_flag("N");
				cnt = frontPkgGoodsMapper.updatePkgByOriginalNum(pkg);
				List<Pkg> pList = frontPkgMapper.queryPackageByOriginalNum(pkg.getOriginal_num());
				pkg.setPackage_id(pList.get(0).getPackage_id());
			}
			else
			    cnt = frontPkgGoodsMapper.addPkg(pkg);

			// 前台客户添加的包裹,没有后台人员操作,故user=null
			PackageLogUtil.log(pkg, null);

			// 发送SMS提醒上传身份证
			if (wantToSendSMS)
			{
				String logisticsCodeListStr = pkg.getLogistics_code();
				messageSendService.sendIdCardImportWarnSMS(receiveAddress.getMobile(), logisticsCodeListStr);
			}
		}
		return cnt;
	}

	/**
	 * 批量导入包裹-重点-批量导入Excel表格时调用的方法
	 */
	@Override
	public int addPkgBatch(List<Map<String, Object>> list, FrontUser frontUser,
			int osaddr_id, int express_package) {
		Map<String, String> userIdLogisticsCodeMap = new HashMap<String, String>();
		Map<String, Pkg> pkgMap = parse(list, express_package);
		int cnt = 0;
		List<PkgGoods> goodsAllList = new ArrayList<PkgGoods>();

		List<PkgLog> pkgLogList = new ArrayList<PkgLog>();
		String maxBatchLot = getMaxBatchLotByUserId(frontUser.getUser_id());

		List<ExpressNumLotConnectBean> expressNumLotList = new ArrayList<ExpressNumLotConnectBean>();
		for (Entry<String, Pkg> entry : pkgMap.entrySet()) {
			Pkg pkg = entry.getValue();

			List<Pkg> existPkgList = frontPkgService.queryPackageByLogisticsCode(pkg.getLogistics_code());
			// 防止重复提交
			if (existPkgList == null || existPkgList.size() == 0) {
				// 包裹地址
				FrontUserAddress frontUserAddress = pkg.getFrontUserAddress();

				frontUserAddress.setUser_id(frontUser.getUser_id());

				boolean wantToSendSMS = true;
				String existIdcard = null;
				String front_img = null;
				String backImg = null;
				int idcardStatus = 0;

				Map<String, Object> params = new HashMap<String, Object>();
				params.put("mobile", frontUserAddress.getMobile());
				params.put("receiver", frontUserAddress.getReceiver());
				List<FrontUserAddress> addresslist = frontUserAddressMapper
						.queryAddressByMobileAndReceiver(params);
				if (addresslist != null && addresslist.size() > 0) {
					for (FrontUserAddress tfrontUserAddress : addresslist) {
						if (StringUtils.trimToNull(tfrontUserAddress
								.getIdcard()) != null) {
							existIdcard = tfrontUserAddress.getIdcard();
						}
						if (StringUtils.trimToNull(tfrontUserAddress
								.getFront_img()) != null
								&& StringUtils.trimToNull(tfrontUserAddress
										.getBack_img()) != null) {
							wantToSendSMS = false;
							front_img = tfrontUserAddress.getFront_img();
							backImg = tfrontUserAddress.getBack_img();
							idcardStatus = tfrontUserAddress.getIdcard_status();
						}
					}
				}
				if (!wantToSendSMS) {
					List<FrontReceiveAddress> receiveAddresslist = frontReceiveAddressMapper
							.queryReceiveAddressByMobileAndReceiver(params);
					if (receiveAddresslist != null
							&& receiveAddresslist.size() > 0) {
						for (FrontReceiveAddress frontReceiveAddress : receiveAddresslist) {
							if (StringUtils.trimToNull(frontReceiveAddress
									.getIdcard()) != null) {
								existIdcard = frontReceiveAddress.getIdcard();
							}
							if (StringUtils.trimToNull(frontReceiveAddress
									.getFront_img()) != null
									&& StringUtils
											.trimToNull(frontReceiveAddress
													.getBack_img()) != null) {
								wantToSendSMS = false;
								existIdcard = frontReceiveAddress.getIdcard();
								front_img = frontReceiveAddress.getFront_img();
								backImg = frontReceiveAddress.getBack_img();
								idcardStatus = frontReceiveAddress
										.getIdcard_status();
							}
						}
					}
				}
				// 用户收货地址
				FrontReceiveAddress frontReceiveAddress = pkg
						.getFrontReceiveAddress();
				if (existIdcard != null) {
					frontUserAddress.setIdcard(existIdcard);
					frontReceiveAddress.setIdcard(existIdcard);
				}
				if (front_img != null) {
					frontUserAddress.setFront_img(front_img);
					frontReceiveAddress.setFront_img(front_img);
				}
				if (backImg != null) {
					frontUserAddress.setBack_img(backImg);
					frontReceiveAddress.setBack_img(backImg);
				}
				frontUserAddress.setIdcard_status(idcardStatus);

				// 将地址插入包裹收货地址表
				frontUserAddressMapper.insertFrontUserAddress(frontUserAddress);

				frontReceiveAddress.setUser_id(frontUser.getUser_id());
				if ((StringUtils.trimToNull(frontUserAddress.getFront_img()) == null || StringUtils
						.trimToNull(frontUserAddress.getBack_img()) == null)
						&& (StringUtils.trimToNull(frontReceiveAddress
								.getFront_img()) == null || StringUtils
								.trimToNull(frontReceiveAddress.getBack_img()) == null)) {
					if (userIdLogisticsCodeMap.containsKey(frontReceiveAddress
							.getMobile())) {
						String orgLogistics = userIdLogisticsCodeMap
								.get(frontReceiveAddress.getMobile());
						userIdLogisticsCodeMap.put(
								frontReceiveAddress.getMobile(), orgLogistics
										+ "," + pkg.getLogistics_code());
					} else {
						userIdLogisticsCodeMap.put(
								frontReceiveAddress.getMobile(),
								pkg.getLogistics_code());
					}

				}
				// 将地址插入用户收货地址表
				frontReceiveAddressMapper
						.insertFrontReceiveAddress(frontReceiveAddress);

				FrontUser realTimefrontUser = ffrontUserService
						.queryFrontUserByUserId(frontUserAddress.getUser_id());
				// 使用默认支付方式
				pkg.setPay_type(realTimefrontUser.getDefault_pay_type());
				pkg.setAddress_id(frontUserAddress.getAddress_id());
				pkg.setUser_id(frontUser.getUser_id());
				Timestamp createTime = new Timestamp(System.currentTimeMillis());
				pkg.setCreateTime(createTime);
				pkg.setOsaddr_id(osaddr_id);
				pkg.setStatus(Pkg.LOGISTICS_STORE_WAITING);
				pkg.setBatch_lot(maxBatchLot);
				
				// 004)此处查询报价单，将报价单id做set包裹处理
				QuotationManage qm = null;
				Map<String, Object> map = new HashMap<String,Object>();
				map.put("osaddr_id", osaddr_id);
				map.put("express_package", pkg.getExpress_package());
				map.put("user_type", frontUser.getUser_type());
				map.put("user_id", frontUser.getUser_id());
				List<QuotationManage> QuotationManageList = quotationManageMapper.getQuotaByPkgInfo(map);
				if (QuotationManageList != null && QuotationManageList.size() > 0) {
					qm = QuotationManageList.get(0);
					if (qm != null) {
						pkg.setQuota_id(qm.getQuota_id());
					}
				}
				// 添加包裹
				cnt = frontPkgGoodsMapper.addPkg(pkg);

				List<PkgGoods> pkgGoodsList = pkg.getGoods();

				for (PkgGoods pkgGoods : pkgGoodsList) {
					pkgGoods.setPackage_id(pkg.getPackage_id());
					goodsAllList.add(pkgGoods);
				}

				// 添加保价服务
				if (pkg.getKeep_price() != 0) {
					// 新增保价服务
					PkgAttachService pkgAttachService = new PkgAttachService();
					pkgAttachService.setAttach_id(32);
					pkgAttachService.setPackage_id(pkg.getPackage_id());
					// 设置原始包裹
					pkgAttachService.setPackage_group(String.valueOf(pkg
							.getPackage_id()));
					// 初始状态为未完成
					pkgAttachService.setStatus(PkgAttachService.ATTACH_FINISH);
					pkgAttachService.setService_price(pkg.getKeep_price()
							/ (float) 100.0);
					pkgAttachServiceService
							.addPkgAttachService(pkgAttachService);
				}
				// 生成日志对象
				PkgLog pkgLog = new PkgLog();

				Timestamp operate_time = new Timestamp(
						System.currentTimeMillis());
				String description = PackageLogUtil.getDescription(pkg);
				pkgLog.setPackage_id(pkg.getPackage_id());
				pkgLog.setOperated_status(Pkg.LOGISTICS_STORE_WAITING);
				pkgLog.setOperate_time(operate_time);
				pkgLog.setDescription(description);
				pkgLogList.add(pkgLog);

				String expressNum = StringUtils
						.trimToNull(pkg.getExpress_num());
				if (expressNum != null
						&& checkExpressLotNoExist(expressNumLotList, expressNum)) {
					// 生成物流单号关联
					ExpressNumLotConnectBean expressNumLotConnectBean = new ExpressNumLotConnectBean();
					expressNumLotConnectBean.setExpress_num(expressNum);
					expressNumLotConnectBean.setBatch_lot(maxBatchLot);
					expressNumLotConnectBean.setAuthor(""
							+ frontUser.getUser_id());
					expressNumLotList.add(expressNumLotConnectBean);
				}
			}
		}

		// 批量保存包裹物品
		if (goodsAllList.size() > 0) {
			frontPkgGoodsMapper.addPkgGoods(goodsAllList);

			// 批量保存日志， 前台客户添加的包裹,没有后台人员操作,故user=null
			PackageLogUtil.logBatch(pkgLogList);
		}

		// 批量保存物流单号与批量导入批号的关联
		if (expressNumLotList.size() > 0) {
			List<ExpressNumLotConnectBean> inputExpressNumLotList = new ArrayList<ExpressNumLotConnectBean>();
			for (ExpressNumLotConnectBean expressNumLotConnectBean : expressNumLotList) {
				if (frontPkgGoodsMapper
						.checkBatchLotExpressExist(expressNumLotConnectBean) == 0) {
					inputExpressNumLotList.add(expressNumLotConnectBean);
				}
			}
			if (expressNumLotList.size() > 0) {
				frontPkgGoodsMapper
						.addExpressNumLotConnect(inputExpressNumLotList);
			}
		}
		// 发送SMS提醒上传身份证
		if (userIdLogisticsCodeMap != null) {
			boolean wantSendSMS = true;// todo, need to check system setting
			if (wantSendSMS) {
				Iterator<String> iterator = userIdLogisticsCodeMap.keySet()
						.iterator();
				while (iterator.hasNext()) {
					String key = iterator.next();
					String logisticsCodeListStr = userIdLogisticsCodeMap
							.get(key);
					String[] array = logisticsCodeListStr.split(",");
					int lenght = array.length;
					if (lenght > 10) {
						int round = lenght / 10;
						for (int i = 0; i <= round; i++) {
							int base = i * 10;
							int endBase = (i + 1) * 10;
							String tempLogisticsCodeListSt = "";
							for (int j = base; j < (endBase > lenght ? lenght
									: endBase); j++) {
								tempLogisticsCodeListSt += array[j] + ",";
							}
							tempLogisticsCodeListSt = StringUtils
									.substringBefore(tempLogisticsCodeListSt,
											",");
							messageSendService.sendIdCardImportWarnSMS(key,
									tempLogisticsCodeListSt);
						}
					} else {
						messageSendService.sendIdCardImportWarnSMS(key,
								logisticsCodeListStr);
					}

				}
			}
		}
		return cnt;
	}
	/**
	 * 解析数据，封装成对象集合*****重点
	 * 
	 * @param list
	 * @return
	 */
	private Map<String, Pkg> parse(List<Map<String, Object>> list,
			int express_package) {
		Map<String, Pkg> pkgMap = new HashMap<String, Pkg>();

		for (Map<String, Object> map : list) {

			Object original_numObj = map.get("original_num");

			String original_num = getStringVal(original_numObj);
			original_num = original_num.toUpperCase();

			Object remarkObj = map.get("remark");

			String remark = getStringVal(remarkObj);

			String receiver = StringUtils.trimToNull(getStringVal(map
					.get("receiver")));

			String mobile = StringUtils.trimToNull(getStringVal(map
					.get("mobile")));
			//各个渠道
			String ep = getStringVal(map.get("express_package"));
			String province = getStringVal(map.get("province"));

			String city = getStringVal(map.get("city"));

			String street = getStringVal(map.get("street"));

			String postal_code = getStringVal(map.get("postal_code"));

			String goods_name = getStringVal(map.get("goods_name"));

			String temp = getStringVal(map.get("quantity"));
			float f = 0;
			if (temp != null || !"".equals(temp)) {
				f = Float.valueOf(temp);
			}
			int quantity = (int) f;

			String spec = getStringVal(map.get("spec"));

			String brand = getStringVal(map.get("brand"));

			String idcard = getStringVal(map.get("idcard"));

			String express_num = getStringVal(map.get("express_num"));

			temp = "";

			Object priceObj = map.get("price");
			if (priceObj != null) {
				if (Long.class.isAssignableFrom(priceObj.getClass())) {
					// 转换成整型
					DecimalFormat df = new DecimalFormat("0.00");
					temp = df.format(priceObj);
				}

				if (Double.class.isAssignableFrom(priceObj.getClass())) {
					// 转换成整型
					DecimalFormat df = new DecimalFormat("0.00");
					temp = df.format(priceObj);
				}
				if (String.class.isAssignableFrom(priceObj.getClass())) {
					try {
						Integer.parseInt((String) priceObj);
						temp = (String) priceObj;
					} catch (Exception e) {
						temp = "";
					}
				}
			}

			float price = (temp == null || "".equals(temp)) ? 0 : Float
					.valueOf(temp);

			temp = "";
			Object keepPriceObj = map.get("keep_price");
			if (keepPriceObj != null) {
				if (Long.class.isAssignableFrom(keepPriceObj.getClass())) {
					// 转换成整型
					DecimalFormat df = new DecimalFormat("0.00");
					temp = df.format(keepPriceObj);
				}

				if (Double.class.isAssignableFrom(keepPriceObj.getClass())) {
					// 转换成整型
					DecimalFormat df = new DecimalFormat("0.00");
					temp = df.format(keepPriceObj);
				}
				if (String.class.isAssignableFrom(keepPriceObj.getClass())) {
					try {
						Integer.parseInt((String) keepPriceObj);
						temp = (String) keepPriceObj;
					} catch (Exception e) {
						temp = "";
					}
				}
			}

			float keep_price = (temp == null || "".equals(temp)) ? 0 : Float
					.valueOf(temp);

			PkgGoods pkgGoods = new PkgGoods();

			pkgGoods.setQuantity(quantity);

			pkgGoods.setGoods_name(goods_name);

			pkgGoods.setPrice(price);

			pkgGoods.setSpec(spec);

			pkgGoods.setBrand(brand);

			if (pkgMap.containsKey(original_num)) {
				Pkg pkg = pkgMap.get(original_num);

				List<PkgGoods> pkgGoodsList = pkg.getGoods();

				pkgGoodsList.add(pkgGoods);
				/*
				 * float total_worth = pkg.getTotal_worth(); float goods_price =
				 * pkgGoods.getPrice(); total_worth = (float)
				 * NumberUtils.add(total_worth, goods_price);
				 * pkg.setTotal_worth(total_worth);
				 */
			} else {
				//包裹渠道
				Pkg pkg = new Pkg();

				if (ep!=null && "A".equals(ep.trim())) {
					pkg.setExpress_package(0);
				}else if (ep!=null && "B".equals(ep.trim())) {
					pkg.setExpress_package(2);
				}
				else if (ep!=null && "C".equals(ep.trim())) {
					pkg.setExpress_package(2);
				}else if (ep!=null && "D".equals(ep.trim())) {
					pkg.setExpress_package(4);
				}else if (ep!=null && "E".equals(ep.trim())) {
					pkg.setExpress_package(5);
				}
				else {
					pkg.setExpress_package(0);
				}
				pkg.setKeep_price(keep_price);
				String logistics_code = IdentifierCreator
						.createIdentifier(IdentifierCreator.TYPE_PACKAGE);

				pkg.setLogistics_code(logistics_code);
				pkg.setOriginal_num(original_num);

				pkg.setRemark(remark);
				pkg.setExpress_num(express_num);

				List<PkgGoods> pkgGoodsList = new ArrayList<PkgGoods>();

				pkgGoodsList.add(pkgGoods);

				// 包裹地址
				FrontUserAddress userAddress = new FrontUserAddress();
				userAddress.setMobile(mobile);
				userAddress.setIdcard(idcard);
				userAddress.setPostal_code(postal_code);
				userAddress.setStreet(street);
				userAddress.setReceiver(receiver);

				// 身份证审核状态
				userAddress
						.setIdcard_status(FrontUserAddress.IDCARD_APPROVE_UNOT);
				String region = getRegion(province, city);
				// 用户所在省、市地址json字符串
				userAddress.setRegion(region);

				// 用户收货地址
				FrontReceiveAddress receiveAddress = new FrontReceiveAddress();
				receiveAddress.setMobile(mobile);
				receiveAddress.setIdcard(idcard);
				receiveAddress.setPostal_code(postal_code);
				receiveAddress.setStreet(street);
				receiveAddress.setReceiver(receiver);

				// 身份证审核状态
				receiveAddress
						.setIdcard_status(FrontReceiveAddress.IDCARD_APPROVE_UNOT);
				String recegion = getRegion(province, city);
				// 用户所在省、市地址json字符串
				receiveAddress.setRegion(recegion);

				// 赋值包裹地址地址给包裹
				pkg.setFrontUserAddress(userAddress);

				// 赋值用户收货地址给包裹
				pkg.setFrontReceiveAddress(receiveAddress);

				// 初始化用户包裹物料列表
				pkg.setGoods(pkgGoodsList);

				pkgMap.put(original_num, pkg);
			}
		}

		return pkgMap;
	}
	private boolean isNumber(String value)
	{
		return isInteger(value) || isDouble(value);
	}

	private boolean isInteger(String value)
	{
		try
		{
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException e)
		{
			return false;
		}
	}

	/**
	 * 判断字符串是否是浮点数
	 */
	private boolean isDouble(String value)
	{
		try
		{
			Double.parseDouble(value);
			if (value.contains("."))
				return true;
			return false;
		} catch (NumberFormatException e)
		{
			return false;
		}
	}

	private String getStringVal(Object valueObj)
	{

		String strValue = "";
		if (null == valueObj)
		{
			strValue = "";
		}
		else
		{
			if (Long.class.isAssignableFrom(valueObj.getClass()))
			{
				// 转换成整型
				DecimalFormat df = new DecimalFormat("#");
				strValue = df.format(valueObj);
			}
			if (Double.class.isAssignableFrom(valueObj.getClass()))
			{
				// 转换成整型
				DecimalFormat df = new DecimalFormat("#");
				strValue = df.format(valueObj);
			}
			else
			{
				strValue = String.valueOf(valueObj).trim();
			}
		}
		return strValue;
	}

	/**
	 * 获取地址信息
	 * 
	 * @param provinceName
	 * @param cityName
	 * @return
	 */
	private String getRegion(String provinceName, String cityName)
	{

		// 省级别的父id
		int provinceFatherId = 0;

		Map<String, Object> provinceParam = new HashMap<String, Object>();

		provinceParam.put("name", provinceName);
		provinceParam.put("father_id", provinceFatherId);

		// 获取省份
		List<FrontCity> provinceList = getProvince(provinceFatherId, provinceName);

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (FrontCity province : provinceList)
		{
			if (provinceFatherId == province.getFather_id())
			{
				Map<String, Object> map = new HashMap<String, Object>();

				map.put("id", province.getCity_id());
				map.put("name", province.getCity_name());
				map.put("level", 1);
				list.add(map);

				List<FrontCity> cityList = getCity(province.getCity_id(), cityName);

				if (!cityList.isEmpty())
				{
					for (FrontCity city : cityList)
					{

						if (city.getFather_id() == province.getCity_id())
						{
							Map<String, Object> cityMap = new HashMap<String, Object>();
							cityMap.put("id", city.getCity_id());
							cityMap.put("name", city.getCity_name());
							cityMap.put("level", 2);
							list.add(cityMap);
						}
						else
						{
							Map<String, Object> cityMap = new HashMap<String, Object>();
							cityMap.put("id", city.getCity_id());
							cityMap.put("name", city.getCity_name());
							cityMap.put("level", 3);
							list.add(cityMap);

						}

					}
				}
				break;
			}
		}

		JSONArray jsonArray = JSONArray.fromObject(list);
		String json = jsonArray.toString();
		return json;
	}

	/**
	 * 获取省份
	 * 
	 * @param father_id
	 * @param name
	 * @return
	 */
	private List<FrontCity> getProvince(int father_id, String name)
	{
		List<FrontCity> provinceList = new ArrayList<FrontCity>();
		@SuppressWarnings("unchecked")
		List<FrontCity> frontCityList = (List<FrontCity>) SystemParamUtil.getData("frontCityList");

		for (FrontCity city : frontCityList)
		{
			String keyword = city.getKeyword();
			if (name.contains(keyword) && city.getFather_id() == father_id)
			{
				provinceList.add(city);
				break;
			}
		}
		return provinceList;
	}

	private List<FrontCity> getCity(int father_id, String name)
	{
		List<FrontCity> frontCityList = (List<FrontCity>) SystemParamUtil.getData("frontCityList");

		// 当前省份下所有地区级城市
		List<FrontCity> allCityList = new ArrayList<FrontCity>();

		List<FrontCity> cityList = new ArrayList<FrontCity>();

		String framework = "_" + father_id;
		for (FrontCity city : frontCityList)
		{
			if (city.getI_framework().contains(framework))
			{
				allCityList.add(city);
			}
		}

		for (FrontCity city : allCityList)
		{

			if (name.contains(city.getKeyword()) && city.getFather_id() == father_id)
			{
				cityList.add(city);
				break;
			}
			else if (name.contains(city.getKeyword()) && city.getFather_id() != father_id)
			{
				int cityFather_id = city.getFather_id();
				for (FrontCity cityFather : allCityList)
				{
					if (cityFather.getCity_id() == cityFather_id)
					{
						cityList.add(cityFather);
					}
				}
				cityList.add(city);
			}
		}
		return cityList;
	}

	@Override
	public void delPkgGoods(int package_id)
	{

		frontPkgGoodsMapper.delPkgGoods(package_id);
	}

	@Override
	public Pkg queryPkgByPkgid(int package_id)
	{
		// TODO Auto-generated method stub
		return frontPkgGoodsMapper.queryPkgByPkgid(package_id);
	}

	@Override
	public List<Pkg> isExistPkg(List<String> originalNumList)
	{
		return frontPkgGoodsMapper.isExistPkg(originalNumList);
	}

	/**
	 * 1、将分箱之后的包裹数据保存 2、分箱之后的包裹数据保存，同时生成对应的增值服务信息。
	 * 3、查询旧包裹是否有增值服务记录表是否有分箱、跟合箱中任意一项增值服务，则将包裹对应增值服记录删除。
	 * 4、如果不包含拆、合两项增值服务，则包裹为原始包裹，无需删除，可能该包裹包含有其他项目服务：如清点。
	 * 
	 */

	@Override
	public int insertNewDisassemblePkg(Map<Integer, List<Pkg>> params, float service_price)
	{

		int cnt = 0;
		List<PkgGoods> goodsAllList = new ArrayList<PkgGoods>();

		List<PkgLog> pkgLogList = new ArrayList<PkgLog>();

		for (Entry<Integer, List<Pkg>> entry : params.entrySet())
		{

			int package_id = entry.getKey();

			List<Pkg> newPkgList = entry.getValue();

			// 获取旧包裹数据
			Pkg oldPkg = frontPkgGoodsMapper.queryPkgByPkgid(package_id);

			// 查询旧包裹地址信息
			int address_id = oldPkg.getAddress_id();
			FrontUserAddress frontUserAddress = frontUserAddressMapper.queryUseraddress(address_id);

			PkgAttachService oldPkgAttachServiceParams = new PkgAttachService();
			oldPkgAttachServiceParams.setAttach_id(BaseAttachService.SPLIT_PKG_ID);
			oldPkgAttachServiceParams.setPackage_id(oldPkg.getPackage_id());

			// 查询旧包裹是否已经有分箱服务
			PkgAttachService oldAttachService = pkgAttachServiceMapper.queryAllPkgAttachServiceById(oldPkgAttachServiceParams);

			for (Pkg newPkg : newPkgList)
			{

				// 根据旧包裹地址信息，为当前新包裹创建一条地址信息
				frontUserAddressMapper.insertFrontUserAddress(frontUserAddress);
				newPkg.setAddress_id(frontUserAddress.getAddress_id());

				// 将旧包裹信息映射到新包裹中
				newPkg.setOsaddr_id(oldPkg.getOsaddr_id());
				newPkg.setUser_id(oldPkg.getUser_id());
				newPkg.setQuota_id(oldPkg.getQuota_id());

				// 同时包裹状态设置为待入库
				newPkg.setStatus(Pkg.LOGISTICS_STORE_WAITING);
				FrontUser frontUser = ffrontUserService.queryFrontUserByUserId(frontUserAddress.getUser_id());
				// 使用默认支付方式
				newPkg.setPay_type(frontUser.getDefault_pay_type());
				
				newPkg.setExpress_package(oldPkg.getExpress_package());
				// 保存新包裹
				cnt = frontPkgGoodsMapper.addPkg(newPkg);

				List<PkgGoods> pkgGoodsList = newPkg.getGoods();

				for (PkgGoods pkgGoods : pkgGoodsList)
				{
					pkgGoods.setPackage_id(newPkg.getPackage_id());
					goodsAllList.add(pkgGoods);
				}

				PkgAttachService pkgAttachService = new PkgAttachService();
				pkgAttachService.setDescription("分箱");
				pkgAttachService.setAttach_id(BaseAttachService.SPLIT_PKG_ID);
				pkgAttachService.setPackage_id(newPkg.getPackage_id());
				pkgAttachService.setStatus(PkgAttachService.ATTACH_UNFINISH);
				pkgAttachService.setService_price(service_price);
				if (oldAttachService == null)
				{
					pkgAttachService.setPackage_group(String.valueOf(oldPkg.getPackage_id()));
				}
				else
				{
					pkgAttachService.setPackage_group(oldAttachService.getPackage_group());
				}

				pkgAttachServiceMapper.addPkgAttachService(pkgAttachService);

				// 生成新包裹日志对象
				PkgLog pkgLog = new PkgLog();
				Timestamp operate_time = new Timestamp(System.currentTimeMillis());
				String description = PackageLogUtil.getDescription(newPkg);
				pkgLog.setPackage_id(newPkg.getPackage_id());
				pkgLog.setOperated_status(Pkg.LOGISTICS_STORE_WAITING);
				pkgLog.setOperate_time(operate_time);
				pkgLog.setDescription(description);
				pkgLogList.add(pkgLog);
			}

			// 将旧包裹设置为已不可用
			oldPkg.setEnable(Pkg.PACKAGE_DISABLE);
			frontPkgGoodsMapper.disablePkg(oldPkg);
		}

		// 批量保存包裹物品
		frontPkgGoodsMapper.addPkgGoods(goodsAllList);

		// 批量保存日志， 前台客户添加的包裹,没有后台人员操作,故user=null
		PackageLogUtil.logBatch(pkgLogList);

		return cnt;
	}

	@Override
	public int insertMergePkg(Map<String, Object> params, float service_price,int express_pacakge)
	{
		@SuppressWarnings("unchecked")
		List<Integer> package_ids = (List<Integer>) params.get("package_ids");

		Integer osaddr_id = (Integer) params.get("osaddr_id");

		Pkg oldPkg = null;

		List<PkgGoods> goodsList = new ArrayList<PkgGoods>();

		// 新包裹
		Pkg newPkg = new Pkg();

		newPkg.setStatus(Pkg.LOGISTICS_STORE_WAITING);

		// 生成新的物流单号
		String logistics_code = IdentifierCreator.createIdentifier(IdentifierCreator.TYPE_PACKAGE);
		newPkg.setLogistics_code(logistics_code);
		newPkg.setOsaddr_id(osaddr_id);
		newPkg.setEnable(Pkg.PACKAGE_ENABLE);
		Timestamp time = new Timestamp(System.currentTimeMillis());
		newPkg.setCreateTime(time);
		StringBuffer package_groupBuffer = new StringBuffer();
		String spec = ",";
		// 循环设置待合并的包裹为不可用，同时循环获取待合并的包裹的物品
		for (Integer package_id : package_ids)
		{
			oldPkg = new Pkg();
			oldPkg.setPackage_id(package_id);
			PkgAttachService oldPkgAttachServiceParams = new PkgAttachService();
			oldPkgAttachServiceParams.setAttach_id(BaseAttachService.MERGE_PKG_ID);
			oldPkgAttachServiceParams.setPackage_id(oldPkg.getPackage_id());

			// 查询旧包裹是否已经有分箱服务
			PkgAttachService oldAttachService = pkgAttachServiceMapper.queryAllPkgAttachServiceById(oldPkgAttachServiceParams);

			// 如果旧包裹不含合箱，则包裹原始包裹
			if (oldAttachService == null)
			{
				package_groupBuffer.append(package_id);
			}
			else
			{
				// 非原始包裹，则获取原始包裹组合。
				package_groupBuffer.append(oldAttachService.getPackage_group());
			}

			package_groupBuffer.append(spec);

			// 根据前台package_id查询包裹下的所有商品
			List<PkgGoods> templist = frontPkgGoodsMapper.queryPkgGoodsById(package_id);

			// 旧包裹将设置为不可用状态
			oldPkg.setEnable(Pkg.PACKAGE_DISABLE);

			goodsList.addAll(templist);
			frontPkgGoodsMapper.disablePkg(oldPkg);
		}

		// 将待合并的包裹，放入合并后的包裹中
		newPkg.setGoods(goodsList);

		// 根据用户地址id,获取用户地址
		Integer address_id = (Integer) params.get("address_id");
		FrontReceiveAddress receiveAddress = frontReceiveAddressMapper.queryReceiveAddressByAddressId(address_id);

		FrontUserAddress frontUserAddress = new FrontUserAddress();
		frontUserAddress.setUser_id(receiveAddress.getUser_id());

		frontUserAddress.setRegion(receiveAddress.getRegion());
		frontUserAddress.setStreet(receiveAddress.getStreet());
		frontUserAddress.setReceiver(receiveAddress.getReceiver());
		frontUserAddress.setMobile(receiveAddress.getMobile());
		frontUserAddress.setPostal_code(receiveAddress.getPostal_code());
		frontUserAddress.setTel(receiveAddress.getTel());
		frontUserAddress.setIsdefault(receiveAddress.getIsdefault());
		frontUserAddress.setIdcard(receiveAddress.getIdcard());
		frontUserAddress.setFront_img(receiveAddress.getBack_img());
		frontUserAddress.setBack_img(receiveAddress.getBack_img());
		frontUserAddress.setIdcard_status(receiveAddress.getIdcard_status());
		frontUserAddress.setRefuse_reason(receiveAddress.getRefuse_reason());

		// 设置当前新包裹的包裹地址
		frontUserAddressMapper.insertFrontUserAddress(frontUserAddress);

		// 将新增的包裹地址与当前包裹进行关联
		newPkg.setAddress_id(frontUserAddress.getAddress_id());

		// 将新增的包裹用户与当前包裹进行关联
		newPkg.setUser_id(frontUserAddress.getUser_id());

		FrontUser frontUser = ffrontUserService.queryFrontUserByUserId(frontUserAddress.getUser_id());
		// 使用默认支付方式
		newPkg.setPay_type(frontUser.getDefault_pay_type());
		
		newPkg.setExpress_package(express_pacakge);

		// 005)此处查询报价单，将报价单id做set包裹处理
		QuotationManage qm = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("osaddr_id", osaddr_id);
		map.put("express_package", express_pacakge);
		map.put("user_type", frontUser.getUser_type());
		map.put("user_id", frontUser.getUser_id());
		List<QuotationManage> QuotationManageList = quotationManageMapper.getQuotaByPkgInfo(map);
		if (QuotationManageList != null && QuotationManageList.size() > 0) {
			qm = QuotationManageList.get(0);
			if (qm != null) {
				newPkg.setQuota_id(qm.getQuota_id());
			}
		}

		// 新增包裹
		int cnt = frontPkgGoodsMapper.addPkg(newPkg);

		// 新增包裹物品
		for (PkgGoods pkgGoods : newPkg.getGoods())
		{
			pkgGoods.setPackage_id(newPkg.getPackage_id());
			frontPkgGoodsMapper.insertPkgGoods(pkgGoods);
		}

		PkgAttachService pkgAttachService = new PkgAttachService();
		pkgAttachService.setDescription("合箱");

		pkgAttachService.setAttach_id(BaseAttachService.MERGE_PKG_ID);
		pkgAttachService.setPackage_id(newPkg.getPackage_id());
		pkgAttachService.setStatus(PkgAttachService.ATTACH_UNFINISH);
		pkgAttachService.setService_price(service_price);
		int endIndex = package_groupBuffer.length();
		int startIndex = package_groupBuffer.length() - 1;
		package_groupBuffer = package_groupBuffer.delete(startIndex, endIndex);

		String package_group = package_groupBuffer.toString();

		pkgAttachService.setPackage_group(package_group);

		// 新增包裹增值服务
		pkgAttachServiceMapper.addPkgAttachService(pkgAttachService);

		// 生成新包裹日志对象
		PkgLog pkgLog = new PkgLog();
		String description = PackageLogUtil.getDescription(newPkg);
		pkgLog.setPackage_id(newPkg.getPackage_id());
		pkgLog.setOperated_status(Pkg.LOGISTICS_STORE_WAITING);
		pkgLog.setOperate_time(time);
		pkgLog.setDescription(description);

		// 记录日志
		PackageLogUtil.log(pkgLog);
		return cnt;
	}

	/**
	 * 支付后更新商品的税项目
	 * 
	 * @param pkgGoods
	 * @return
	 */
	@Override
	public int updateTax(List<PkgGoods> pkgGoods)
	{

		return frontPkgGoodsMapper.updateTax(pkgGoods);
	}

	/**
	 * 公司单号（多）查询包裹物品
	 * 
	 * @param list
	 * @return
	 */
	@Override
	public List<PkgGoods> queryGoodsBylogisticsCodes(List<String> list)
	{

		return frontPkgGoodsMapper.queryGoodsBylogisticsCodes(list);
	}

	private String getMaxBatchLotByUserId(int user_id)
	{
		String maxBatchLot = "0001";
		String packageMaxBatchLot = StringUtils.trimToNull(frontPkgGoodsMapper.queryMaxBatchLotByUserId_package(user_id));
		String expressMaxBatchLot = StringUtils.trimToNull(frontPkgGoodsMapper.queryMaxBatchLotByUserId_express_lot(user_id));
		if (packageMaxBatchLot == null && expressMaxBatchLot == null)
		{
			maxBatchLot = "0001";
		}
		else if (packageMaxBatchLot == null && expressMaxBatchLot != null)
		{
			maxBatchLot = expressMaxBatchLot;
		}
		else if (packageMaxBatchLot != null && expressMaxBatchLot == null)
		{
			maxBatchLot = packageMaxBatchLot;
		}
		else if (packageMaxBatchLot != null && expressMaxBatchLot != null)
		{
			if (packageMaxBatchLot.compareTo(expressMaxBatchLot) > 0)
			{
				maxBatchLot = packageMaxBatchLot;
			}
			else
			{
				maxBatchLot = expressMaxBatchLot;
			}
		}
		if (packageMaxBatchLot == null && expressMaxBatchLot == null)
		{
			maxBatchLot = "0001";
		}
		else
		{
			BigDecimal bigDecimal = new BigDecimal(maxBatchLot);
			maxBatchLot = bigDecimal.add(new BigDecimal(1)).toPlainString();
		}
		if (maxBatchLot.length() < 4)
		{
			maxBatchLot = StringUtils.leftPad(maxBatchLot, 4, "0");
		}
		return maxBatchLot;
	}

	private boolean checkExpressLotNoExist(List<ExpressNumLotConnectBean> expressNumLotList, String expressNum)
	{
		boolean notExist = true;
		if (expressNumLotList != null && expressNumLotList.size() > 0)
		{
			for (ExpressNumLotConnectBean expressNumLotConnectBean : expressNumLotList)
			{
				if (expressNumLotConnectBean.getExpress_num().equals(expressNum))
				{
					notExist = false;
				}
			}
		}
		return notExist;
	}

	/**
	 * 1、将分箱之后的包裹数据保存 2、分箱之后的包裹数据保存，同时生成对应的增值服务信息。
	 * 3、查询旧包裹是否有增值服务记录表是否有分箱、跟合箱中任意一项增值服务，则将包裹对应增值服记录删除。
	 * 4、如果不包含拆、合两项增值服务，则包裹为原始包裹，无需删除，可能该包裹包含有其他项目服务：如清点。
	 * 
	 */

	@Override
	public int insertNewDisassemblePkgBack(Map<Integer, List<Pkg>> params, float service_price)
	{

		int cnt = 0;
		List<PkgGoods> goodsAllList = new ArrayList<PkgGoods>();

		List<PkgLog> pkgLogList = new ArrayList<PkgLog>();

		for (Entry<Integer, List<Pkg>> entry : params.entrySet())
		{

			int package_id = entry.getKey();

			List<Pkg> newPkgList = entry.getValue();

			// 获取旧包裹数据
			Pkg oldPkg = frontPkgGoodsMapper.queryPkgByPkgid(package_id);

			// 查询旧包裹地址信息
			int address_id = oldPkg.getAddress_id();
			FrontUserAddress frontUserAddress = frontUserAddressMapper.queryUseraddress(address_id);

			PkgAttachService oldPkgAttachServiceParams = new PkgAttachService();
			oldPkgAttachServiceParams.setAttach_id(BaseAttachService.SPLIT_PKG_ID);
			oldPkgAttachServiceParams.setPackage_id(oldPkg.getPackage_id());

			// 查询旧包裹是否已经有分箱服务
			PkgAttachService oldAttachService = pkgAttachServiceMapper.queryAllPkgAttachServiceById(oldPkgAttachServiceParams);

			List<PkgAttachService> allOldAttachServiceList = pkgAttachServiceMapper.queryPkgAttachServiceByPackage(package_id);
			int i = 1;
			int newPkgSize = newPkgList.size();
			for (Pkg newPkg : newPkgList)
			{

				// 根据旧包裹地址信息，为当前新包裹创建一条地址信息
				frontUserAddressMapper.insertFrontUserAddress(frontUserAddress);
				newPkg.setAddress_id(frontUserAddress.getAddress_id());

				// 将旧包裹信息映射到新包裹中
				newPkg.setOsaddr_id(oldPkg.getOsaddr_id());
				newPkg.setUser_id(oldPkg.getUser_id());
				newPkg.setQuota_id(oldPkg.getQuota_id());

				// 同时包裹状态设置为待入库
				newPkg.setStatus(Pkg.LOGISTICS_STORE_WAITING);
				FrontUser frontUser = ffrontUserService.queryFrontUserByUserId(frontUserAddress.getUser_id());
				// 使用默认支付方式
				newPkg.setPay_type(frontUser.getDefault_pay_type());
				// 超重分箱标志
				newPkg.setOver_weight_split_pkg_flag("Y");
				newPkg.setArrive_status(Pkg.STORE_PACKAGE_ARRIVED);
				// 保存新包裹
				cnt = frontPkgGoodsMapper.addPkg(newPkg);

				List<PkgGoods> pkgGoodsList = newPkg.getGoods();

				for (PkgGoods pkgGoods : pkgGoodsList)
				{
					pkgGoods.setPackage_id(newPkg.getPackage_id());
					goodsAllList.add(pkgGoods);
				}

				PkgAttachService pkgAttachService = new PkgAttachService();
				pkgAttachService.setDescription("分箱(自动)");
				pkgAttachService.setAttach_id(BaseAttachService.SPLIT_PKG_ID);
				pkgAttachService.setPackage_id(newPkg.getPackage_id());
				pkgAttachService.setStatus(PkgAttachService.ATTACH_UNFINISH);
				pkgAttachService.setService_price(service_price);
				if (oldAttachService == null)
				{
					pkgAttachService.setPackage_group(String.valueOf(oldPkg.getPackage_id()));
				}
				else
				{
					pkgAttachService.setPackage_group(oldAttachService.getPackage_group());
				}

				pkgAttachServiceMapper.addPkgAttachService(pkgAttachService);

				// 写入已有增值服务
				if (allOldAttachServiceList != null && allOldAttachServiceList.size() > 0)
				{
					for (PkgAttachService tempPkgAttachService : allOldAttachServiceList)
					{
						// 分箱的前面已经添加过
						if (BaseAttachService.SPLIT_PKG_ID != tempPkgAttachService.getAttach_id())
						{
							float new_service_price = tempPkgAttachService.getService_price();
							int status = PkgAttachService.ATTACH_UNFINISH;
							if (BaseAttachService.KEEP_PRICE_ID == tempPkgAttachService.getAttach_id())
							{
								status = PkgAttachService.ATTACH_FINISH;
								float keepPrice = new_service_price * 100;
								keepPrice = NumberUtils.scaleMoneyDataFloat(keepPrice);
								int keepPriceInt = (int) keepPrice;
								int less = keepPriceInt % newPkgSize;
								int mod = keepPriceInt / newPkgSize;
								if (i < newPkgSize)
								{
									float partTotal = mod;
									new_service_price = partTotal / 100;
								}
								else if (i == newPkgSize)
								{
									float partTotal = mod + less;
									if (keepPriceInt < keepPrice)
									{
										partTotal += (keepPrice - keepPriceInt);
									}
									new_service_price = partTotal / 100;
								}
							}
							PkgAttachService newpkgAttachService = new PkgAttachService();
							newpkgAttachService.setDescription(tempPkgAttachService.getDescription());
							newpkgAttachService.setAttach_id(tempPkgAttachService.getAttach_id());
							newpkgAttachService.setPackage_id(newPkg.getPackage_id());
							newpkgAttachService.setStatus(status);
							newpkgAttachService.setService_price(new_service_price);
							newpkgAttachService.setPackage_group(String.valueOf(newPkg.getPackage_id()));
							pkgAttachServiceMapper.addPkgAttachService(newpkgAttachService);
						}
					}
				}
				// 生成新包裹日志对象
				PkgLog pkgLog = new PkgLog();
				Timestamp operate_time = new Timestamp(System.currentTimeMillis());
				String description = PackageLogUtil.getDescription(newPkg);
				pkgLog.setPackage_id(newPkg.getPackage_id());
				pkgLog.setOperated_status(Pkg.LOGISTICS_STORE_WAITING);
				pkgLog.setOperate_time(operate_time);
				pkgLog.setDescription(description);
				pkgLogList.add(pkgLog);
				i++;
			}

			// 将旧包裹设置为已不可用
			oldPkg.setEnable(Pkg.PACKAGE_DISABLE);
			frontPkgGoodsMapper.disablePkg(oldPkg);
		}

		// 批量保存包裹物品
		frontPkgGoodsMapper.addPkgGoods(goodsAllList);

		// 批量保存日志， 前台客户添加的包裹,没有后台人员操作,故user=null
		PackageLogUtil.logBatch(pkgLogList);

		return cnt;
	}
}
