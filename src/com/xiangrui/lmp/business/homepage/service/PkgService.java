package com.xiangrui.lmp.business.homepage.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.xiangrui.lmp.business.homepage.vo.Pkg;

public interface PkgService {	
	/**
	 * 根据user_id
	 * 查询包裹
	 * @param user_id
	 * @return List
	 */
	List<Pkg> queryPackageByUserId(int user_id);
	
	
	/**
	 * 根据参数查询包裹信息
	 * @return
	 */
	List<Pkg>queryPkgByParam(Map<String,Object> params);

	/**
	 * 根据logistics_code单号查询包裹
	 * @param val
	 * @return
	 */
	List<Pkg> queryPackageByLogisticsCode(String val);
	
    /**
     * 根据关联单号查询包裹
     * @param original_num
     * @return
     */
    List<Pkg> queryPackageByOriginalNum(String original_num);
	/**
     * 根据package_id
     * @param package_id
     * @return List
     */
    Pkg queryPackageByPackageId(int package_id);
    /**
     * 删除包裹
     * @param package_id
     */
    void deletePkgByPackageId(int package_id);
    /**
     * 删除包裹里的所有商品
     * @param package_id
     */
    void deletePkgInGoodsByPackageId(int package_id);
    /**
     * 删除包裹及其商品与增值服务
     * @param package_id
     */
    void deletePkgAndGoodsOrAttachByPackageId(int package_id);
    /**
     * 删除包裹的增值服务
     * @param package_id
     */
    void deletePkgAttachByPackageId(int package_id);
    
    /**
     * 根据user_id和包裹状态查询包裹
     * @param user_id,status
     * @return list
     */    
    List<Pkg> queryPackageByUserIdAndStatus(Map<String,Object> params);
    
    /**
     * 根据user_id和包裹状态查询非异常状态包裹
     * @param params
     * @return
     */
    List<Pkg> queryPackageByUserIdAndStatusNormal(Map<String,Object> params);
    


    /**
     * 根据user_id和包裹支付状态查询包裹
     * @param user_id,pay_status
     * @return list
     */    
    List<Pkg> queryPackageByUserIdAndPayStatus(Map<String,Object> params);
    List<Pkg> queryPackageByUserIdAndPayStatusCustom(Map<String,Object> params); // 只找是否支付关税的
    List<Pkg> queryPackageByUserIdAndPayStatusFreight(Map<String,Object> params); // 只找是否支付运费的
    List<Pkg> queryPackageByUserIdAndPaid(Map<String,Object> params); // 查已关税与运费都已经支付的状态
    List<Pkg> queryPackageByUserIdAndUnPaid(Map<String,Object> params); // 查已关税或运费至少有一个未支付的状态
    
    /**
     * 根据user_id和包裹打印状态查询包裹
     * @param user_id,pkg_print
     * @return list
     */ 
    List<Pkg> queryPackageByUserIdAndPkgPrint(Map<String,Object> params);
    
    /**
     * 包裹收货地址更新
     * @param pkg
     * @return 
     */ 
    void updatePkgForUseraddress(Pkg pkg);
    
    /**
     * 根据运单号，关联单号，收件人，手机号码，商品名称查询包裹
     * @param params
     * @return 
     */
    List<Pkg> queryPkgBySearch(Map<String,Object> params);
    
    /**
     * 根据运单号查询包裹(用户查询)
     * @param params
     * @return 
     */
    Pkg queryPkgBylogistics(Map<String,Object> params);
    
    /**
     * 根据包裹关联单号查询包裹
     * @param params
     * @return 
     */
    Pkg queryPkgByOriginalNum(Map<String,Object> params);
    
    /**
     * 根据包裹关联单号和异常状态查询包裹
     * @param params
     * @return 
     */
    Pkg queryPkgByOriginalNumByStatus(Map<String,Object> params);
    
    /**
     * <!--包裹ID（多）查询包裹-->
     * @param list
     * @return List
     */
    List<Pkg> queryPackageByPackageIds(List<String> list);
    
    /**
     * <!--公司单号（多）查询包裹-->
     * @param list
     * @return List
     */
    List<Pkg> queryPackageBylogisticsCodes(List <String> list);
    /**
     * 根据user_id和包裹状态、到库状态查询包裹
     * @param user_id,status
     * @return list
     */    
    List<Pkg> queryArrvivedPackage(Map<String,Object> params);
    
    /**
     * 根据user_id和包裹状态查询前台待处理包裹（后台异常包裹关联后的显示）
     * @param user_id,status
     * @return list
     */    
    List<Pkg> queryNeedToHandlePackage(Map<String,Object> params);
    
    boolean isNeedToHandlePackage(int status, int enable, int user_id);
    
    /**
     * 删除包裹和包裹相关的所有信息
     * @param package_id
     */
    void deleteAllPkgByPackageId(int package_id,boolean isUnusualPKGDelete);
    
    /**
     * 新增包裹
     * @param user_id
     * @param original_num
     * @param actual_weight
     * @param osaddr_id
     * @param exception_package_flag
     * @return
     */
    int addPkg(int user_id,String original_num,float actual_weight,int osaddr_id,String exception_package_flag,int payType);
    
    /**
     * 根据运单号、包裹创建时间范围、打印状态查询包裹
     * @param params
     * @return 
     */
    List<Pkg> queryPkgBylogisticsAndPackageCreateTimeRangeAndPrintStatus(Map<String,Object> params);
    
    /**
     * 根据用户ID,包裹新建时间段查询导出包裹
     * @param params
     * @return
     */
    List<Pkg> queryFrontEndExportPackage(Map<String,Object> params);
    /**
     * 导出前台包裹
     * @param fileName
     * @param pkgList
     * @return
     */
    XSSFWorkbook frontEndExportPackageHandle(String fileName, List<Pkg> pkgList);
    
    /**
     * 根据单号查询包裹
     * @param pkg
     * @return 
     */ 
    void updatePkgForOriginalNum(Pkg pkg);
    
    Integer queryCountByUserId(Integer userId);
    /**
     * 根据支付状态，打印状态，单号查询包裹
     * @param pkg
     * @return 
     */ 
    List<Pkg>  queryMypagesByqueryPames(Map<String, Object> params) ;
    /**
     * 根据支付状态，打印状态，单号查询包裹总数
     * @param pkg
     * @return 
     */ 
    Integer  queryMypagesCountByqueryPames(Map<String, Object> params);

 }
