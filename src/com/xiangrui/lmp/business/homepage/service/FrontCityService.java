package com.xiangrui.lmp.business.homepage.service;

import java.util.List;

import com.xiangrui.lmp.business.homepage.vo.FrontCity;

public interface FrontCityService
{
    /**
     * 查询所有省，市，区
     * @return list
     */
    List<FrontCity> queryAllCity();
    
    /**
     * 根据father_id查询省，市，区
     * @return list
     */
    List<FrontCity> queryCity(int father_id);
}
