package com.xiangrui.lmp.business.homepage.service;

import java.util.Map;

public interface FrontWithdrawalLogService
{

    /**
     * 提现取消 账户提现记录表登录，前台用户更新金额项目
     * 
     * @param params
     * @return
     */
    void withdrawal(Map<String, Object> params);
    
    /**
     * 提现申请 账户提现记录表登录，前台用户更新金额项目
     * 
     * @param params
     * @return
     */
    void withdrawalCancel(Map<String, Object> params);
    
    /**
     * 根据用户ID统计存在申请提现记录
     * @param front_user_id
     * @return
     */
    int countWithdrawalLogByFrontUserId(int front_user_id);
}
