package com.xiangrui.lmp.business.homepage.service;

import com.xiangrui.lmp.business.homepage.vo.FrontIdcard;

public interface FrontIdcardService {
	FrontIdcard queryByIdcard(String idcard, String realName);
	
	void insertFrontIdcard(FrontIdcard frontIdcard);
}
