package com.xiangrui.lmp.business.homepage.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.Pkg;

public interface FrontUserService
{

    /**
     * 根据用户邮箱查询用户信息
     * 
     * @param account
     * @return FrontUser
     */
    FrontUser queryFrontUserByEmail(String email);

    /**
     * 前台用户登录的判断
     * 
     * @param frontUser
     * @return FrontUser
     */
    FrontUser frontUserLogin(FrontUser frontUser);

    /**
     * 用户注册
     * 
     * @param frontUser
     */
    void addFrontUser(FrontUser frontUser);

    /**
     * 根据用户ID查询用户信息
     * 
     * @param user_id
     */
    FrontUser queryFrontUserByUserId(int user_id);

    /**
     * 根据用户手机查询
     * 
     * @param mobile
     * @return FrontUser
     */
    FrontUser queryFrontUserByMobile(String mobile);
    
    /**
     * 根据用户微信绑定ID查询用户信息
     * @param open_id
     * @return
     */
    FrontUser queryFrontUserByOpenId(String open_id);
    /**
     * 根据用户名查询
     * 
     * @param user_name
     * @return FrontUser
     */
    FrontUser queryFrontUserByUserName(String user_name);

    /**
     * 修改密码
     * 
     * @param frontUser
     */
    int updateUserPwd(FrontUser frontUser);

    /**
     * 修改手机号
     * 
     * @param frontUser
     */
    int updateMobile(FrontUser frontUser);

    /**
     * 修改姓名
     * 
     * @param frontUser
     */
    int updateUserName(FrontUser frontUser);

    FrontUser queryFrontUserByEmailOrMoible(String email);
    
    /**
     * 账户支付运费
     * 
     * @param params
     */
     void payment(Map<String, Object> params);
     
     /**
      * 支付宝支付运费
      * 
      * @param params
      */
      void alipay(Map<String, Object> params);
      
      /**
       * 充值
       * 
       * @param params
       */
       void recharge(Map<String, Object> params);
       
       /**
        * 绑定银行卡号
        * 
        * @param frontUser
        */
       int updateBank(FrontUser frontUser);
       
       /**
        * 绑定支付宝
        * 
        * @param frontUser
        */
       int updatePay(FrontUser frontUser);
       
       /**
        * 账户支付关税
        * 
        * @param params
        */
        void payTax(Map<String, Object> params);
        
        /**
         * 网上支付关税
         * 
         * @param params
         */
         void payTaxOL(Map<String, Object> params);
         /**
          * 修改默认支付方式 
          * 
          * @param frontUser
          */
         int updateDefaultPayType(FrontUser frontUser);
         
         /**
          * 修改虚拟预付款方式
          * 
          * @param frontUser
          */
         int updateVirtualPayFlag(FrontUser frontUser);
         
         /**
          * 绑定微信ID
          * @param frontUser
          * @return
          */
         int updateopen_id(FrontUser frontUser);
         
         /**
          * 更新个人头像
          * @param frontUser
          * @return
          */
         int updatePersion_icon(FrontUser frontUser);
     	/**
     	 * 账户交易信息
     	 * 
     	 * @param transport_cost
     	 *            付款金额
     	 * @param user_id
     	 *            用户
     	 * @param logisticsCodes
     	 *            公司运单号
     	 */
     	String updatePayment(String transport_cost, int user_id, String logisticsCodes,int coupon_id,String errorCode);
     	
     	/**
     	 * 生成推荐信息（二维码和推荐链接）
     	 * @param frontUser
     	 * @return
     	 */
        FrontUser createRecommandInfo(FrontUser frontUser,String ctxPath) throws Exception;
        
        /**
         * 查询个人的邀请用户列表
         * @param user_id
         * @return
         */
        List<FrontUser> queryMyInviteUser(Integer user_id);

        /**
         * 根据用户ID查询推广信息
         * @param user_id
         * @return
         */
		List<FrontUser> queryPushById(int user_id);
}
