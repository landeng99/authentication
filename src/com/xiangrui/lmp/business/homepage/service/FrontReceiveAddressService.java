package com.xiangrui.lmp.business.homepage.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.FrontReceiveAddress;

public interface FrontReceiveAddressService
{

    /**
     * 根据用户ID查询用户收货地址
     * @param user_id
     */
    List<FrontReceiveAddress> queryReceiveAddress(Map<String,Object> params);
       
    /**
     * 根据用户ID查询用户收货地址
     * @param user_id
     */
    List<FrontReceiveAddress> queryReceiveAddressByUserId(Map<String,Object> params);
    
    /**
     * 根据address_id查询用户收货地址
     * @param address_id 新表
     */
    FrontReceiveAddress queryReceiveAddressByAddressId(int address_id);
    
    /**
     * 创建用户地址
     * @param frontReceiveAddress
     * @return
     */
    int insertFrontReceiveAddress(FrontReceiveAddress frontReceiveAddress);
     
   /**
   * 修改用户地址
   * @param frontUserAddress
   * @return
   */
   void updateReceiveAddress(FrontReceiveAddress frontReceiveAddress);
   
   /**
    * 删除用户地址
    * @param address_id
    * @return
    */
   void deleteReceiveAddress(int address_id);
   
   /**
    * 更新正面身份证图片
    * @param params
    * @return
    */
   int updateReceiveFrontIdcard(Map<String,Object> params);
   
   /**
    * 更新背面身份证图片
    * @param params
    * @return
    */
   int updateReceiveBackIdcard(Map<String,Object> params);
   
   /**
    * 查询用户未上传身份证地址(用户收货地址)
    * @param user_id
    * @return
    */
   List<FrontReceiveAddress> queryReceiveUnloadIdcard(int user_id);
   
   /**
    * 更新正面身份证图片和身份证号
    * @param params
    * @return
    */
   int updateReceiveFrontIdcardAndPhoto(Map<String,Object> params);
   
   /**
    * 更新背面身份证图片和身份证号
    * @param params
    * @return
    */
   int updateReceiveBackIdcardAndPhoto(Map<String,Object> params);
   
   /**
    * 根据receiver和mobile查询包裹收货地址
    * @param params
    * @return
    */
   List<FrontReceiveAddress> queryReceiveAddressByMobileAndReceiver(Map<String, Object> params);
   
   /**
    * 根据用户ID和receiver和mobile查询用户收货地址
    * @param params
    * @return
    */
   List<FrontReceiveAddress> queryReceiveAddressByUserIdAndMobileAndReceiver(Map<String, Object> params);
   
   /**
    * 根据收件人或手机查询用户地址 
    * @param param
    * @return
    */
   List<FrontReceiveAddress> queryAddress(Map<String, Object> param);
}
