package com.xiangrui.lmp.business.homepage.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;

public interface PkgGoodsService {
    
    /**
     * 查询同一包裹下的所有商品
     * 
     * @param package_id
     * @return List
     */
    List<PkgGoods> queryPkgGoodsById(int package_id);
    
    /**
	 * 添加商品
	 * @param pkgGoods
	 */
    void addPkgGoods(List<PkgGoods> pkgGoods);
    
    /**
     * 添加包裹
     * @param pkg
     */
    int addPkg(Pkg pkg);
    
    /**
     * 根据package_id查询包裹
     * @param package_id
     */
    Pkg queryPkgByPkgid(int package_id);
    
    /**批量创建包裹
     * @param list
     * @return
     */
    int addPkgBatch(List<Map<String, Object>> list,FrontUser frontUser,int osaddr_id,int express_package);
    
    /**
     * 删除商品
     * @param package_id
     */
    void delPkgGoods(int package_id);
    
    /**
     * 查询关联单号是否已经被使用
     * @param originalNumList
     * @return
     */
    List<Pkg> isExistPkg(List<String> originalNumList);
    
    /**
     * 创建分箱产生的新包裹
     * @param params
     * @return
     */
    int insertNewDisassemblePkg(Map<Integer,List<Pkg>> params,float service_price);
    
    /**
     * 包裹合箱操作
     * @param package_ids
     * @param pkg
     * @return
     */
    int insertMergePkg(Map<String,Object> params,float service_price,int express_pacakge);

    /**
     * 支付后更新商品的税项目
     * 
     * @param pkgGoods
     * @return
     */
    int updateTax(List<PkgGoods> pkgGoods);
    
    /**
     * 公司单号（多）查询包裹物品
     * @param list
     * @return
     */
    List<PkgGoods> queryGoodsBylogisticsCodes(List<String> list);
    
    /**
     * 创建分箱产生的新包裹
     * @param params
     * @return
     */
    int insertNewDisassemblePkgBack(Map<Integer,List<Pkg>> params,float service_price);
}
