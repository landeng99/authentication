package com.xiangrui.lmp.business.homepage.vo;

import com.xiangrui.lmp.business.base.BaseGoodsType;

public class FrontGoodsType extends BaseGoodsType
{


    private static final long serialVersionUID = 1L;
    /**
     * 申报类别排序用的
     */
    private String idFramework;
    
    public String getIdFramework()
    {
        return idFramework;
    }
    public void setIdFramework(String idFramework)
    {
        this.idFramework = idFramework;
    }

    
}
