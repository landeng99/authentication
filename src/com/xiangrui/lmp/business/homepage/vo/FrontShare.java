package com.xiangrui.lmp.business.homepage.vo;

import java.sql.Timestamp;

public class FrontShare
{
	private static final long serialVersionUID = 1L;

	/**
	 * 未赠送
	 */
	public static final int SHARE_STATUS_NOGIVE = 1;

	/**
	 * 已赠送
	 */
	public static final int SHARE_STATUS_GIVE = 2;

	/**
	 * 未审核
	 */
	public static final int APPROVE_PROCESSING = 1;

	/**
	 * 审核通过
	 */
	public static final int APPROVE_THROUGH = 2;

	/**
	 * 审核拒绝
	 */
	public static final int APPROVE_REFUSING = 3;

	/**
	 * '分享id',
	 */
	private int share_id;
	/**
	 * '分享用户id',
	 */
	private int user_id;
	/**
	 * '包裹id',
	 */
	private int package_id;
	/**
	 * '分享时间',
	 */
	private Timestamp share_time;
	/**
	 * '晒单链接',
	 */
	private String share_link;
	/**
	 * '获得优惠券数量',
	 */
	private int coupon_num;
	/**
	 * '审核状态，1：未审核，2：审核通过，3：审核拒绝',
	 */
	private int approval_status;
	/**
	 * '处理状态，1：未赠送，2：已赠送优惠券',
	 */
	private int status;
	/**
     * 
     */
	private String reason;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 内容
	 */
	private String content;

	private int award_coupon_id;

	public int getAward_coupon_id()
	{
		return award_coupon_id;
	}

	public void setAward_coupon_id(int award_coupon_id)
	{
		this.award_coupon_id = award_coupon_id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public int getShare_id()
	{
		return share_id;
	}

	public void setShare_id(int share_id)
	{
		this.share_id = share_id;
	}

	public int getUser_id()
	{
		return user_id;
	}

	public void setUser_id(int user_id)
	{
		this.user_id = user_id;
	}

	public int getPackage_id()
	{
		return package_id;
	}

	public void setPackage_id(int package_id)
	{
		this.package_id = package_id;
	}

	public Timestamp getShare_time()
	{
		return share_time;
	}

	public void setShare_time(Timestamp share_time)
	{
		this.share_time = share_time;
	}

	public String getShare_link()
	{
		return share_link;
	}

	public void setShare_link(String share_link)
	{
		this.share_link = share_link;
	}

	public int getCoupon_num()
	{
		return coupon_num;
	}

	public void setCoupon_num(int coupon_num)
	{
		this.coupon_num = coupon_num;
	}

	public int getApproval_status()
	{
		return approval_status;
	}

	public void setApproval_status(int approval_status)
	{
		this.approval_status = approval_status;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}

}
