package com.xiangrui.lmp.business.homepage.vo;

/**
 * 包裹打印状态
 * @author yeyi771
 */
public enum PrintEnum {
	NOT_PRINT(0,"未打印"),
	PRINTED(1,"已打印");
    
    private int id;
    private String msg;

    private PrintEnum(int id, String msg) {
        this.msg = msg;
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public int getId() {
        return id;
    }
}
