package com.xiangrui.lmp.business.homepage.vo;

import com.xiangrui.lmp.business.base.BasePkgAttachService;

public class FrontPkgAttachService extends BasePkgAttachService
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 物流跟踪号
     */
    private String logistics_code;

    /**
     * 增值服务名称
     */
    private String service_name;

    /**
     * 增值服务价格
     */

    private float service_price;

    public String getLogistics_code()
    {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code)
    {
        this.logistics_code = logistics_code;
    }

    public String getService_name()
    {
        return service_name;
    }

    public void setService_name(String service_name)
    {
        this.service_name = service_name;
    }

    public float getService_price()
    {
        return service_price;
    }

    public void setService_price(float service_price)
    {
        this.service_price = service_price;
    }
}