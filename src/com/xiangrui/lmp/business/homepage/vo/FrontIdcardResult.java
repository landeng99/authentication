package com.xiangrui.lmp.business.homepage.vo;

import java.io.Serializable;

public class FrontIdcardResult implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idcard;	// 身份证号
	private String realname;	// 人名
	private boolean isok;	// 是否匹配成功
	private FrontIdCardInfor IdCardInfor;
	
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public boolean isIsok() {
		return isok;
	}
	public void setIsok(boolean isok) {
		this.isok = isok;
	}
	public FrontIdCardInfor getIdCardInfor() {
		return IdCardInfor;
	}
	public void setIdCardInfor(FrontIdCardInfor idCardInfor) {
		IdCardInfor = idCardInfor;
	}
	
}
