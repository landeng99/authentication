package com.xiangrui.lmp.business.homepage.vo;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

public class PackageImportCacheBean
{
	private Workbook workbook;
	private List<Map<String, Object>> list;
	private FrontUser frontUser;
	private int osaddr_id;
	private String exName;
	private int express_package;

	public int getExpress_package()
	{
		return express_package;
	}

	public void setExpress_package(int express_package)
	{
		this.express_package = express_package;
	}

	public Workbook getWorkbook()
	{
		return workbook;
	}

	public void setWorkbook(Workbook workbook)
	{
		this.workbook = workbook;
	}

	public List<Map<String, Object>> getList()
	{
		return list;
	}

	public void setList(List<Map<String, Object>> list)
	{
		this.list = list;
	}

	public FrontUser getFrontUser()
	{
		return frontUser;
	}

	public void setFrontUser(FrontUser frontUser)
	{
		this.frontUser = frontUser;
	}

	public int getOsaddr_id()
	{
		return osaddr_id;
	}

	public void setOsaddr_id(int osaddr_id)
	{
		this.osaddr_id = osaddr_id;
	}

	public String getExName()
	{
		return exName;
	}

	public void setExName(String exName)
	{
		this.exName = exName;
	}
}
