package com.xiangrui.lmp.business.homepage.vo;

import com.xiangrui.lmp.business.base.BasePkgGoods;

public class PkgGoods  extends BasePkgGoods{
	
	/**
     * 
     */
    private static final long serialVersionUID = 1L;

    public PkgGoods() {
		super();
	}
    /**
     * '物品名称及规格'
     */
    private String name_specs;

    public String getName_specs()
    {
        return name_specs;
    }
    public void setName_specs(String name_specs)
    {
        this.name_specs = name_specs;
    }
    
}
