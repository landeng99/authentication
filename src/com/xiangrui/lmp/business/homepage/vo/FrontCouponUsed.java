package com.xiangrui.lmp.business.homepage.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class FrontCouponUsed implements Serializable
{
    /**
     * 时间格式化
     */
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 优惠券id
     */
    private int coupon_id;

    /**
     * 编号
     */
    private String coupon_code;

    /**
     * 用户id
     */
    private String user_id;

    /**
     * 来源
     */
    private String resource;

    /**
     * 面额
     */
    private float denomination;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 使用日期
     */
    private Timestamp use_date;

    /**
     * 订单号
     */
    private int order_code;

    /**
     * json显示时间用
     */
    private String use_date_string;
    
    private String logistics_code;

    public String getLogistics_code()
	{
		return logistics_code;
	}

	public void setLogistics_code(String logistics_code)
	{
		this.logistics_code = logistics_code;
	}

	public int getCoupon_id()
    {
        return coupon_id;
    }

    public void setCoupon_id(int coupon_id)
    {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_code()
    {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code)
    {
        this.coupon_code = coupon_code;
    }

    public String getUser_id()
    {
        return user_id;
    }

    public void setUser_id(String user_id)
    {
        this.user_id = user_id;
    }

    public String getResource()
    {
        return resource;
    }

    public void setResource(String resource)
    {
        this.resource = resource;
    }

    public float getDenomination()
    {
        return denomination;
    }

    public void setDenomination(float denomination)
    {
        this.denomination = denomination;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public Timestamp getUse_date()
    {
        return use_date;
    }

    public String getUse_date_toStr()
    {
        if (null != use_date)
        {
            return use_date.toString().trim().replace(".0", "");
        } else
        {
            return "";
        }
    }

    public void setUse_date(Timestamp use_date)
    {
        this.use_date = use_date;
        // json 显示时间用
        this.use_date_string = simpleDateFormat.format(use_date).toString();
    }

    public int getOrder_code()
    {
        return order_code;
    }

    public void setOrder_code(int order_code)
    {
        this.order_code = order_code;
    }

    public String getUse_date_string()
    {
        return use_date_string;
    }

}
