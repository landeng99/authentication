package com.xiangrui.lmp.business.homepage.vo;


/**
 * ClassName:AdvertVo. <br/>
 * description [翔锐物流首页轮播广告]
 * Date:     2015年4月23日19:55:06<br/>
 * @author   ltp
 * @version  1.0
 * @since    JDK 1.7
 */
public class AdvertVo {

	/**.
	 * 广告名称
	 */
	private String advertName;
	
	/**.
	 * 广告图片路径
	 */
	private String advertImage;
	
	/**.
	 * 广告图片标题
	 */
	private String advertTitle;
	
	/**.
	 * 广告图片简介
	 */
	private String advertIntroduction;
	
	/**.
	 * 广告链接
	 */
	private String advertLink;

    /**.
     * 链接打开方式
     */
	private String openType;
	
	/**.
	 * 显示顺序
	 */
	private String advertOrder;
	
	public AdvertVo() {
		super();
	}

	public String getAdvertName() {
		return advertName;
	}

	public void setAdvertName(String advertName) {
		this.advertName = advertName;
	}

	public String getAdvertImage() {
		return advertImage;
	}

	public void setAdvertImage(String advertImage) {
		this.advertImage = advertImage;
	}

	public String getAdvertTitle() {
		return advertTitle;
	}

	public void setAdvertTitle(String advertTitle) {
		this.advertTitle = advertTitle;
	}

	public String getAdvertIntroduction() {
		return advertIntroduction;
	}

	public void setAdvertIntroduction(String advertIntroduction) {
		this.advertIntroduction = advertIntroduction;
	}

	public String getAdvertLink() {
		return advertLink;
	}

	public void setAdvertLink(String advertLink) {
		this.advertLink = advertLink;
	}

	public String getOpenType() {
		return openType;
	}

	public void setOpenType(String openType) {
		this.openType = openType;
	}

	public String getAdvertOrder() {
		return advertOrder;
	}

	public void setAdvertOrder(String advertOrder) {
		this.advertOrder = advertOrder;
	}

}
