package com.xiangrui.lmp.business.homepage.vo;

import java.sql.Timestamp;

import com.xiangrui.lmp.business.base.BaseFrontUser;

public class FrontUser extends BaseFrontUser
{

    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 包裹数量统计
     */
    private int package_count;
    //公司单号
    private String logistics_code;
    
    //下单时间
    private Timestamp createTime;
    
    //转运费用
    private Double transport_cost;
    
    //业务姓名
    private String businessName;
    
    /**
	 * 可用余额 人民币
	 */
	private float able_balance_yuan;
	
    public float getAble_balance_yuan() {
		return able_balance_yuan;
	}

	public void setAble_balance_yuan(float able_balance_yuan) {
		this.able_balance_yuan = able_balance_yuan;
	}

	public String getLogistics_code() {
		return logistics_code;
	}

	public void setLogistics_code(String logistics_code) {
		this.logistics_code = logistics_code;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Double getTransport_cost() {
		return transport_cost;
	}

	public void setTransport_cost(Double transport_cost) {
		this.transport_cost = transport_cost;
	}

	public int getPackage_count()
    {
        return package_count;
    }

    public void setPackage_count(int package_count)
    {
        this.package_count = package_count;
    }

	public String getBusinessName()
	{
		return businessName;
	}

	public void setBusinessName(String businessName)
	{
		this.businessName = businessName;
	}
    
}
