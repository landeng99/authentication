package com.xiangrui.lmp.business.homepage.vo;

import java.sql.Timestamp;
import java.util.List;

import com.xiangrui.lmp.business.admin.pkg.vo.SubPackageBean;
import com.xiangrui.lmp.business.base.BasePackage;

public class Pkg extends BasePackage
{

	/**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    /**
	 * 包裹创建用户
	 */
	private String user_name;
	/**
	 * 状态的中文
	 */
	private String status_name;
	/**
	 * 包裹物品
	 */
	private List<PkgGoods> goods;
	/**
	 * 商品数量
	 */
	private int count;
	/**
	 * 增值服务
	 */
	private float attach_service_price;
	/**
	 * 包裹用户地址
	 */
	private FrontUserAddress frontUserAddress;
	/**
	 * 用户收货地址
	 */
	private FrontReceiveAddress frontReceiveAddress;
	/**
	 * 免税
	 */
	private int is_tax_free;
	/**
	 * 增值服务ID(合箱，分箱)
	 */
	private int attach_id;
	/**
	 * 增值服务状态(0:无增值服务1:未完成，2:已完成)
	 */
	private int attach_status;
	/**
	 * 增值服务状态描述(前台)
	 */
	private String service_status;
	/**
	 * 仓库
	 */
	private String warehouse;
	/**
	 * 增值服务
	 */
	private List<FrontPkgAttachService> pkgAttachServiceList;
	/**
	 * 晒单审核状态
	 */
	private int approval_status;

	/**
	 * 待支付关税
	 */
	private float pay_tax;
	/**
	 * 关联包裹（合箱、分箱时会有多个，其它情况是原包裹）
	 */
	private List<SubPackageBean> subPackageList;

	// 保价金额
	private float keep_price;
	// 物流单号
	private String express_num;
	// 当前状态
	private int operated_status;
	// 当前状态时间
	private Timestamp operate_time;
	// 内件明细
	private String goods_name;
	// 增值服务
	private String attach_service;
	// 增值服务
	private String attach_id_list;
	// 收件人
	private String receiver;
	// 收件人电话
	private String mobile;
	// 收件人地址1
	private String region;
	// 收件人地址2
	private String street;
	/**
	 * 分箱数
	 */
	private int splitCount;
	/**
	 * 合箱数
	 */
	private int mergeCount;
	/**
	 * 合箱后的packageId
	 */
	private String afterMergeLogistics_code;

	/**
	 * 显示行color
	 */
	private int displayColor;

	private int expressNumCount;
	/**
	 * 分箱前的package Id
	 */
	private int splitPackage_id;
	/**
	 * 身份证
	 */
	private String idcard;
	/**
	 * 身份证照片
	 */
	private String idcardImg;
	
	/**
     * 状态描述信息
     */
    private String statusDesc;

	/**
	 * 原关联单号
	 */
	private String old_original_num;
	/**
	 * 格式化的到库时间字符串
	 */
	private String arrive_time_formatStr;

	public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getArrive_time_formatStr()
	{
		return arrive_time_formatStr;
	}

	public void setArrive_time_formatStr(String arrive_time_formatStr)
	{
		this.arrive_time_formatStr = arrive_time_formatStr;
	}

	public String getOld_original_num()
	{
		return old_original_num;
	}

	public void setOld_original_num(String old_original_num)
	{
		this.old_original_num = old_original_num;
	}

	public String getIdcard()
	{
		return idcard;
	}

	public void setIdcard(String idcard)
	{
		this.idcard = idcard;
	}

	public String getIdcardImg()
	{
		return idcardImg;
	}

	public void setIdcardImg(String idcardImg)
	{
		this.idcardImg = idcardImg;
	}

	public int getSplitPackage_id()
	{
		return splitPackage_id;
	}

	public void setSplitPackage_id(int splitPackage_id)
	{
		this.splitPackage_id = splitPackage_id;
	}

	public String getAfterMergeLogistics_code()
	{
		return afterMergeLogistics_code;
	}

	public void setAfterMergeLogistics_code(String afterMergeLogistics_code)
	{
		this.afterMergeLogistics_code = afterMergeLogistics_code;
	}

	public int getDisplayColor()
	{
		return displayColor;
	}

	public void setDisplayColor(int displayColor)
	{
		this.displayColor = displayColor;
	}

	public int getExpressNumCount()
	{
		return expressNumCount;
	}

	public void setExpressNumCount(int expressNumCount)
	{
		this.expressNumCount = expressNumCount;
	}

	public int getSplitCount()
	{
		return splitCount;
	}

	public void setSplitCount(int splitCount)
	{
		this.splitCount = splitCount;
	}

	public int getMergeCount()
	{
		return mergeCount;
	}

	public void setMergeCount(int mergeCount)
	{
		this.mergeCount = mergeCount;
	}

	public float getKeep_price()
	{
		return keep_price;
	}

	public void setKeep_price(float keep_price)
	{
		this.keep_price = keep_price;
	}

	public List<SubPackageBean> getSubPackageList()
	{
		return subPackageList;
	}

	public void setSubPackageList(List<SubPackageBean> subPackageList)
	{
		this.subPackageList = subPackageList;
	}

	public int getApproval_status()
	{
		return approval_status;
	}

	public void setApproval_status(int approval_status)
	{
		this.approval_status = approval_status;
	}

	public String getService_status()
	{
		return service_status;
	}

	public void setService_status(String service_status)
	{
		this.service_status = service_status;
	}

	public int getAttach_id()
	{
		return attach_id;
	}

	public void setAttach_id(int attach_id)
	{
		this.attach_id = attach_id;
	}

	public int getAttach_status()
	{
		return attach_status;
	}

	public void setAttach_status(int attach_status)
	{
		this.attach_status = attach_status;
	}

	public String getWarehouse()
	{
		return warehouse;
	}

	public String getWarehouseIndexof()
	{
		if (null != warehouse && warehouse.indexOf("仓库") != -1)
		{
			return warehouse.replace("仓库", "");
		}
		else
		{
			return warehouse;
		}

	}

	public void setWarehouse(String warehouse)
	{
		this.warehouse = warehouse;
	}

	public int getIs_tax_free()
	{
		return is_tax_free;
	}

	public void setIs_tax_free(int is_tax_free)
	{
		this.is_tax_free = is_tax_free;
	}

	public String getUser_name()
	{
		return user_name;
	}

	public void setUser_name(String user_name)
	{
		this.user_name = user_name;
	}

	public String getStatus_name()
	{
		/*
		 * -1：异常，0：待入库，1：已入库，2：待发货， 3：已出库，4：空运中， 5：待清关， 6：清关中，7：已清关，8：派件中，9：已收货；
		 * 20：废弃，21：退货，22：废弃（分箱、合箱之后原包裹被废弃）’
		 */
		switch (this.getStatus())
		{
			case -1:
				status_name = "异常";
				break;
			case 0:
				status_name = "待入库";
				break;
			case 1:
				status_name = "已入库";
				break;
			case 2:
				status_name = "待发货";
				break;
			case 3:
				status_name = "已出库";
				break;
			case 4:
				status_name = "空运中";
				break;
			case 5:
				status_name = "待清关";
				break;
			case 6:
				status_name = "清关中";
				break;
			case 7:
				status_name = "已清关";
				break;
			case 8:
				status_name = "派件中";
				break;
			case 9:
				status_name = "已收货";
				break;
			case 20:
				status_name = "废弃";
				break;
			case 21:
				status_name = "退货";
				break;
			case 22:
				status_name = "废弃（分箱、合箱之后原包裹被废弃）";
				break;
			default:
				status_name = "";
				break;
		}
		return status_name;
	}

	public void setStatus_name(String status_name)
	{
		this.status_name = status_name;
	}

	public List<PkgGoods> getGoods()
	{
		return goods;
	}

	public void setGoods(List<PkgGoods> goods)
	{
		this.goods = goods;
	}

	public FrontUserAddress getFrontUserAddress()
	{
		return frontUserAddress;
	}

	public void setFrontUserAddress(FrontUserAddress frontUserAddress)
	{
		this.frontUserAddress = frontUserAddress;
	}

	public FrontReceiveAddress getFrontReceiveAddress()
	{
		return frontReceiveAddress;
	}

	public void setFrontReceiveAddress(FrontReceiveAddress frontReceiveAddress)
	{
		this.frontReceiveAddress = frontReceiveAddress;
	}

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public float getAttach_service_price()
	{
		return attach_service_price;
	}

	public void setAttach_service_price(float attach_service_price)
	{
		this.attach_service_price = attach_service_price;
	}

	public List<FrontPkgAttachService> getPkgAttachServiceList()
	{
		return pkgAttachServiceList;
	}

	public void setPkgAttachServiceList(List<FrontPkgAttachService> pkgAttachServiceList)
	{
		this.pkgAttachServiceList = pkgAttachServiceList;
	}

	public float getPay_tax()
	{
		return pay_tax;
	}

	public void setPay_tax(float pay_tax)
	{
		this.pay_tax = pay_tax;
	}

	public String getExpress_num()
	{
		return express_num;
	}

	public void setExpress_num(String express_num)
	{
		this.express_num = express_num;
	}

	public int getOperated_status()
	{
		return operated_status;
	}

	public void setOperated_status(int operated_status)
	{
		this.operated_status = operated_status;
	}

	public Timestamp getOperate_time()
	{
		return operate_time;
	}

	public void setOperate_time(Timestamp operate_time)
	{
		this.operate_time = operate_time;
	}

	public String getGoods_name()
	{
		return goods_name;
	}

	public void setGoods_name(String goods_name)
	{
		this.goods_name = goods_name;
	}

	public String getAttach_service()
	{
		return attach_service;
	}

	public void setAttach_service(String attach_service)
	{
		this.attach_service = attach_service;
	}

	public String getAttach_id_list()
	{
		return attach_id_list;
	}

	public void setAttach_id_list(String attach_id_list)
	{
		this.attach_id_list = attach_id_list;
	}

	public String getReceiver()
	{
		return receiver;
	}

	public void setReceiver(String receiver)
	{
		this.receiver = receiver;
	}

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public String getRegion()
	{
		return region;
	}

	public void setRegion(String region)
	{
		this.region = region;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}
	public void setStatus(int status)
    {
        switch (status)
        {
        case LOGISTICS_STORE_WAITING:{
            if( "Y".equals(this.getException_package_flag()) ){
                this.statusDesc = "待处理";break;
            }
            else{
                this.statusDesc = "待入库";break;
            }
        }
            
        case LOGISTICS_UNUSUALLY:
            this.statusDesc = "包裹异常";break;
        case LOGISTICS_STORAGED:
            this.statusDesc = "已入库";break;
        case LOGISTICS_SENT_ALREADY:
            this.statusDesc="已出库";break;    
        case LOGISTICS_SEND_WAITING:
            this.statusDesc = "待发货";break;
        case LOGISTICS_AIRLIFT_ALREADY:
            this.statusDesc = "已空运";break;
        case LOGISTICS_CUSTOMS_WAITING:
            this.statusDesc = "待清关";break;
        case LOGISTICS_CUSTOMS_ALREADY:
            this.statusDesc = "已清关派件中";break;
        case LOGISTICS_SIGN_IN:
            this.statusDesc = "已签收";break;
        case LOGISTICS_DISCARD:
            this.statusDesc = "废弃";break;
        case LOGISTICS_RETURN:
            this.statusDesc = "退货";break;
        case LOGISTICS_DELETED:
            this.statusDesc = "已删除";break;
        }

        super.setStatus(status);
    }
}
