package com.xiangrui.lmp.business.homepage.vo;

import com.xiangrui.lmp.business.base.BaseIdcard;

public class FrontIdcard extends BaseIdcard{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FrontIdcard(){
	    
	}

	public FrontIdcard(FrontIdcardResult ir){
		this.setArea(ir.getIdCardInfor().getArea());
		this.setBirthday(ir.getIdCardInfor().getBirthday());
		this.setIdcard(ir.getIdcard());
		this.setRealname(ir.getRealname());
		this.setSex(ir.getIdCardInfor().getSex());
	}
}
