package com.xiangrui.lmp.business.homepage.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 前台资费管理的数据结构
 * <p>@author <b>hsjing</b></p>
 * <p>2015-7-15 下午3:37:58</p>
 */
public class TariffHomePage
{
    /**
     * 铜 Cu/copper
     */
    public static final String LAVEL_IMG_CU = "copper.jpg";
    /**
     * 银 Ag/silver
     */
    public static final String LAVEL_IMG_AG = "silver.jpg";
    /**
     * 金 Au/gold
     */
    public static final String LAVEL_IMG_AU = "gold.jpg";
    /**
     * 铂(白金)Pt/platinum
     */
    public static final String LAVEL_IMG_PT = "platinum.jpg";
    /**
     * 钻石(碳)C/diamonds
     */
    public static final String LAVEL_IMG_C = "diamonds.jpg";
    public static final String CSS_STD_SE_LIT = "std-se-lit";
    public static final String CSS_STD_SE_LIF = "std-se-lif";
    public static final String CSS_STD_SE_LIFO = "std-se-lifo";
    public static final String CSS_STD_SE_LIFV = "std-se-lifv";
    public static final String CSS_STD_SE_LISX = "std-se-lisx";
    public static final String CSS_STD_SE_LISE = "std-se-lise";
    public static final String CSS_STD_SE_LIEG = "std-se-lieg";
    public static final String CSS_STD_SE_LIT_SPAN = "std-se-lit-span";
    public static final String CSS_STD_SE_LIS = "std-se-lis";
    public static final String CSS_STD_SE_LITX = "std-se-litx";
    
    private String title;
    private List<String> infoList;

    private String titileCss;
    private String infoStratCss;
    private String infoEndCss;
    
    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }
    public List<String> getInfoList()
    {
        if(null == infoList){
            infoList = new ArrayList<String>();
        }
        return infoList;
    }
    public void setInfoList(List<String> infoList)
    {
        this.infoList = infoList;
    }
    public String getTitileCss()
    {
        return titileCss;
    }
    public void setTitileCss(String titileCss)
    {
        this.titileCss = titileCss;
    }
    public String getInfoStratCss()
    {
        return infoStratCss;
    }
    public void setInfoStratCss(String infoStratCss)
    {
        this.infoStratCss = infoStratCss;
    }
    public String getInfoEndCss()
    {
        return infoEndCss;
    }
    public void setInfoEndCss(String infoEndCss)
    {
        this.infoEndCss = infoEndCss;
    }
    
}
