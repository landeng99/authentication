package com.xiangrui.lmp.business.homepage.vo;
/**
 * 快件包裹 0：不是 1：是（原来的字段意思）
 * 渠道：渠道：0默认；1A渠道；2B渠道；3C渠道；4D渠道；5E渠道 （现在的字段意思）
 */
//private int express_package;
public enum ExpressEnum {
	F_(0,"F渠道"),
	A_(1,"A渠道"),
	B_(2,"B渠道");
    
    private int id;
    private String msg;

    private ExpressEnum(int id, String msg) {
        this.msg = msg;
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public int getId() {
        return id;
    }
}
