package com.xiangrui.lmp.business.homepage.vo;

import com.xiangrui.lmp.business.base.BaseUserAddress;

public class FrontUserAddress extends BaseUserAddress
{



    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String addressStr;

    public String getAddressStr()
    {
        return addressStr;
    }

    public void setAddressStr(String addressStr)
    {
        this.addressStr = addressStr;
    }

}
