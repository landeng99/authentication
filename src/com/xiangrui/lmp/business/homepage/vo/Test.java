package com.xiangrui.lmp.business.homepage.vo;

import java.util.List;
import com.xiangrui.lmp.business.homepage.vo.Pkg;

/**
 * ClassName:AdvertVo. <br/>
 * description [翔锐物流首页轮播广告]
 * Date:     2015年4月23日19:55:06<br/>
 * @author   ltp
 * @version  1.0
 * @since    JDK 1.7
 */
public class Test {

	private Pkg pkg;
	private List<PkgGoods> goods;
	public Pkg getPkg() {
		return pkg;
	}
	public void setPkg(Pkg pkg) {
		this.pkg = pkg;
	}
	public List<PkgGoods> getGoods() {
		return goods;
	}
	public void setGoods(List<PkgGoods> goods) {
		this.goods = goods;
	}

}
