package com.xiangrui.lmp.business.homepage.vo;

/**
 * 包裹支付状态  ：1，未支付，2：已支付；3：关税未支付，4关税已支付
 * @author yeyi771
 */
public enum PaidEnum {
	UNPAYID(1,"未支付"),
	PAID(2,"已支付"),
	CUSTOM_UNPAID(3,"关税未支付"),
	CUSTOM_PAID(4,"关税已支付");
    
    private int id;
    private String msg;

    private PaidEnum(int id, String msg) {
        this.msg = msg;
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public int getId() {
        return id;
    }
}
