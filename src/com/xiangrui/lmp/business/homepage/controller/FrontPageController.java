package com.xiangrui.lmp.business.homepage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.admin.article.service.ArticleClassifyService;



@Controller
@RequestMapping(value = "/frontPage")
public class FrontPageController {
	
	@Autowired
	private ArticleClassifyService articleClassifyService;
	
	@RequestMapping(value="/help")
	public String loginHelp(){
		
		return "/homepage/help";
	}
}
