package com.xiangrui.lmp.business.homepage.controller;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.homepage.service.PkgImgService;
import com.xiangrui.lmp.business.homepage.vo.PackageImg;
import com.xiangrui.lmp.init.AppServerUtil;


@Controller
@RequestMapping("/upload")
public class UploadController {
	//Logger logger =Logger.getLogger(UploadController.class);
	
    /**
     * 图片类型 2：购物小票
     */
    private static final int IMG_TYPE_RETURN = 1;
    
	@Autowired
	private PkgImgService frontPkgImgService;

    
	/**
     * 身份证照片剪切
     */
	@RequestMapping("/pic")
	@ResponseBody
	public Map<String,String> uploadPic(String x1,String y1,String x2,String y2,String w,String h,String pic,HttpServletRequest request) throws IOException{
		int x11=Integer.parseInt(x1);
		int y11=Integer.parseInt(y1);
		int w1=Integer.parseInt(w);
		int h1=Integer.parseInt(h);
		//String srcImageFile="E:/插件/jiaoben289/jiaoben289/images/photo.jpg";
		String path = request.getContextPath();
		String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/resource";
		System.out.println(basePath);
		String  stringArr= pic.substring(pic.lastIndexOf(".")+1);
		System.out.println(pic);
		//裁剪图片的地址
		String picPath=basePath+pic;
		BufferedImage image=getBufferedImage(picPath);
		
		FileOutputStream fos=null;
		
		Color color=new Color(242, 8, 8);
		//返回前台的图片地址
        String resourcePath="/upload/idcard/";
		try {
			//BufferedImage bi = ImageIO.read(new File(srcImageFile));
			CropImageFilter cropFilter = new CropImageFilter(x11, y11, w1, h1);
			Image tagImg = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(image.getSource(), cropFilter));
			
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
	          
            //图片名称
            String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "."+stringArr;
            //剪切后的图片名称
			String newName = AppServerUtil.getWebRoot()+"resource"+File.separator+"upload"+File.separator+"idcard"+File.separator+newFileName;
			
			
			System.out.println(newName);
			
			OutputStream outs = new FileOutputStream(newName); 
			
			BufferedImage tag = new BufferedImage(w1, h1,BufferedImage.TYPE_INT_RGB); 
			
			Graphics graphics = tag.createGraphics();
			
			graphics.drawImage(tagImg, 0, 0, w1, h1, null);
			graphics.dispose();  
			ImageIO.write(tag, "JPEG", outs);
			outs.close();
			
			resourcePath=resourcePath+newFileName;
			//图片水印
			BufferedImage image2 = ImageIO.read(new File(newName));
			
			//创建java2D对象
            Graphics2D g2d=image2.createGraphics();
            
            //用源图像填充背景
            g2d.drawImage(image2, 0, 0, image2.getWidth(), image2.getHeight(), null, null);
            g2d.setFont(new Font("Arial", Font.BOLD, 20));
           
            //设置字体颜色
            g2d.setColor(color);
	       
            //输入水印文字及其起始x、y坐标
            g2d.drawString("SOE", w1-40, h1-10); 
            g2d.dispose();
           
            fos=new FileOutputStream(AppServerUtil.getWebRoot()+"resource"+File.separator+"upload"+File.separator+"photoBack4.jpg");
            ImageIO.write(image2, "JPEG", fos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
            if(fos!=null){
                fos.close();
            }
        }
		Map<String,String> result=new HashMap<String,String>();
		result.put("imgpath", resourcePath);
		return result;
	}
	/** 
     *跳转至上传购物小票  
     */ 
	@RequestMapping("/shopTicket")
	public String uploadshopTicket(String package_id,String img_type,PackageImg packageImg,HttpServletRequest req){
		System.out.println(package_id);
		System.out.println(img_type);
		int packageid=Integer.parseInt(package_id);
		int imgtype=Integer.parseInt(img_type);
		packageImg.setPackage_id(packageid);
		packageImg.setImg_type(imgtype);
		List<PackageImg> shopImgList=frontPkgImgService.queryPkgImg(packageImg);
		req.setAttribute("shopImgList", shopImgList);
		req.setAttribute("package_id",packageid);
		req.setAttribute("count",shopImgList.size());
		return "homepage/uploadImgPage";
	}
	/** 
	 * 
     *上传购物小票
     */ 
	@RequestMapping(value="/uploadTicket", method=RequestMethod.POST)
	public void uploadTicket(HttpServletRequest request, HttpServletResponse response,PackageImg packageImg) throws IOException{
      
	    // String responseStr = "";
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        
        // 获取前台传值
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
 
        String configPath = File.separator + "upload" + File.separator;
   
 
        String ctxPath = request.getSession().getServletContext().getRealPath("/")+"resource";
   
        
        //图片存储路径
        ctxPath += configPath;
        //获取包裹ID
        String package_id=request.getParameter("package_id");
       
        int packageid=Integer.parseInt(package_id);
        
        // 创建文件夹
        File file = new File(ctxPath);
        if (!file.exists()) {
            file.mkdirs();
        }
    
        String fileName = null;
      
        //返回前台的图片地址
        String rtnPath = null;
       
        //一个包裹中购物小票数量
        int num=0;
        
        //插入购物小票返回的img_id
        int imgId=0;
        
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            // 上传文件名
            System.out.println("key: " + entity.getKey());
            MultipartFile mf = entity.getValue();
            fileName = mf.getOriginalFilename();
            //图片格式
            String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
          
            //图片名称
            String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
    
            File uploadFile = new File(ctxPath + newFileName);
            System.out.println(newFileName);
            
            rtnPath=configPath+newFileName;
            packageImg.setImg_type(IMG_TYPE_RETURN);
            packageImg.setPackage_id(packageid);
            packageImg.setImg_path(rtnPath);
            //查询购物小票
            List<PackageImg> pkgImgList=frontPkgImgService.queryPkgImg(packageImg);
            num=pkgImgList.size();
            if(num<10){
                //插入购物小票
                frontPkgImgService.insertPkgImg(packageImg);
                imgId=packageImg.getImg_id();
                try {
                    FileCopyUtils.copy(mf.getBytes(), uploadFile);
                } catch (IOException e) {
                    rtnPath = "上传失败";
                    e.printStackTrace();
                }
                String nm=Integer.toString(num);
                String img_id=Integer.toString(imgId);
                response.setHeader("Content-type", "text/html;charset=UTF-8");
              
                // 这句话的意思，是告诉servlet用UTF-8转码，而不是用默认的ISO8859
                response.setCharacterEncoding("UTF-8");
                response.getWriter().append(rtnPath);
                response.getWriter().append("^");
                response.getWriter().append(nm);
                response.getWriter().append("^");
                response.getWriter().append(img_id);
            }else{
                response.setHeader("Content-type", "text/html;charset=UTF-8");
                response.setCharacterEncoding("UTF-8");
            }            
        
        }
        
	}
	/** 
     * 
     *删除购物小票
     */ 
	@RequestMapping("/deleteTicket")
	@ResponseBody
    public Map<String,String> deleteTicket(String img_id,HttpServletRequest req){
	    System.out.println(img_id);
	    frontPkgImgService.deletePkgImg(Integer.parseInt(img_id));
	    
	    Map<String,String> result=new HashMap<String, String>();
	    result.put("img_id", img_id);
	    
        return result;
    }
	
	  /**
     * 在源图片上设置水印文字
     * @param srcImagePath   源图片路径
     * @param alpha  透明度（0<alpha<1）
     * @param font   字体（例如：宋体）
     * @param fontStyle      字体格式(例如：普通样式--Font.PLAIN、粗体--Font.BOLD )
     * @param fontSize   字体大小
     * @param color  字体颜色(例如：黑色--Color.BLACK)
     * @param inputWords     输入显示在图片上的文字
     * @param x      文字显示起始的x坐标
     * @param y      文字显示起始的y坐标
     * @param imageFormat    写入图片格式（png/jpg等）
     * @param toPath 写入图片路径
     * @throws IOException 
     */
    public void alphaWords2Image(String srcImagePath,float alpha,
            String font,int fontStyle,int fontSize,Color color,
            String inputWords,int x,int y,String imageFormat,String toPath) throws IOException{
        FileOutputStream fos=null;
        try {
            BufferedImage image = ImageIO.read(new File(srcImagePath));
            
            //创建java2D对象
            Graphics2D g2d=image.createGraphics();
            
            //用源图像填充背景
            g2d.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null, null);
           
            //设置透明度
            AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            g2d.setComposite(ac);
            
            //设置文字字体名称、样式、大小
            g2d.setFont(new Font(font, fontStyle, fontSize));
            
            //设置字体颜色
            g2d.setColor(color);
            
            //输入水印文字及其起始x、y坐标
            g2d.drawString(inputWords, x, y); 
            g2d.dispose();
            
            fos=new FileOutputStream(toPath);
            ImageIO.write(image, imageFormat, fos);
        } catch (Exception e) {
           e.printStackTrace();
        }finally{
            if(fos!=null){
                fos.close();
            }
        }
    }
	/** 
     *  
     * @param imgUrl 图片地址 
     * @return  
     */  
    public static BufferedImage getBufferedImage(String imgUrl) {  
        URL url = null;  
        InputStream is = null;  
        BufferedImage img = null;  
        try {  
            url = new URL(imgUrl);  
            is = url.openStream();  
            img = ImageIO.read(is);  
        } catch (MalformedURLException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } finally {  
              
            try {  
                is.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
        return img;  
    }
    
    /**
     * 初始化上传身份证页面
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/initUploadIdCard")
    public String initUploadIdCard(HttpServletRequest request, HttpServletResponse response){
        
        System.out.println("init upload ....");
        return "/homepage/upfile";
    }
    @RequestMapping("/uploadIdCard")
    public String uploadIdCard(HttpServletRequest request, HttpServletResponse response){
        
        
        // String responseStr = "";
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        
        // 获取前台传值
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
 
        String configPath = File.separator + "upload" +  File.separator+"idcard"+ File.separator;
   
        //返回前台的图片地址
        String resourcePath="/upload/idcard/";
 
        String ctxPath = request.getSession().getServletContext().getRealPath("/")+"resource";

        //图片存储路径
        ctxPath += configPath;
        
        // 创建文件夹
        File file = new File(ctxPath);
        if (!file.exists()) {
            file.mkdirs();
        }
    
        String fileName = null;
       
  
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            // 上传文件名
            System.out.println("key: " + entity.getKey());
            MultipartFile mf = entity.getValue();
            fileName = mf.getOriginalFilename();
            //图片格式
            String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
            
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
          
            //图片名称
            String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
    
            File uploadFile = new File(ctxPath + newFileName);
            
            resourcePath=resourcePath+newFileName;
            
            try {
                FileCopyUtils.copy(mf.getBytes(), uploadFile);
            } catch (IOException e) {
                resourcePath = "上传失败";
                e.printStackTrace();
            }
        }
        request.setAttribute("path", resourcePath);
        return "/homepage/upfile";
    }
}
