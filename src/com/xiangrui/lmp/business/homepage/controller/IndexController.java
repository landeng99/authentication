package com.xiangrui.lmp.business.homepage.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.xiangrui.lmp.business.admin.article.service.ArticleClassifyService;
import com.xiangrui.lmp.business.admin.article.service.ArticleService;
import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.attachService.service.MemberAttachService;
import com.xiangrui.lmp.business.admin.attachService.vo.AttachService;
import com.xiangrui.lmp.business.admin.attachService.vo.MemberAttach;
import com.xiangrui.lmp.business.admin.banner.service.BannerService;
import com.xiangrui.lmp.business.admin.consultInfo.service.ConsultInfoService;
import com.xiangrui.lmp.business.admin.consultInfo.vo.ConsultInfo;
import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.freightcost.service.FreightCostService;
import com.xiangrui.lmp.business.admin.freightcost.vo.FreightCost;
import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;
import com.xiangrui.lmp.business.admin.friendlylink.service.FriendlyLinkService;
import com.xiangrui.lmp.business.admin.memberrate.service.MemberRateService;
import com.xiangrui.lmp.business.admin.navigation.service.NavigationService;
import com.xiangrui.lmp.business.admin.navigation.vo.Navigation;
import com.xiangrui.lmp.business.admin.notice.service.NoticeService;
import com.xiangrui.lmp.business.admin.notice.vo.Notice;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.PkgAttachServiceService;
import com.xiangrui.lmp.business.admin.pkg.service.UserAddressService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.admin.pkg.vo.SubPackageBean;
import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;
import com.xiangrui.lmp.business.admin.pkglog.service.PkgLogService;
import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;
import com.xiangrui.lmp.business.admin.prohibition.service.ProhibitionService;
import com.xiangrui.lmp.business.admin.quotationManage.service.QuotationManageService;
import com.xiangrui.lmp.business.admin.quotationManage.vo.QuotationManage;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportDeclarationService;
import com.xiangrui.lmp.business.admin.seaport.service.SeaportService;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.business.admin.seaport.vo.SeaportDeclaration;
import com.xiangrui.lmp.business.admin.siteinfo.service.SiteInfoSerice;
import com.xiangrui.lmp.business.admin.siteinfo.vo.SiteInfo;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.base.BaseBanner;
import com.xiangrui.lmp.business.base.BaseFrontUser;
import com.xiangrui.lmp.business.base.BaseMemberRate;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.base.Region;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgMapper;
import com.xiangrui.lmp.business.homepage.service.FrontCityService;
import com.xiangrui.lmp.business.homepage.service.FrontIdcardService;
import com.xiangrui.lmp.business.homepage.service.FrontOverSeaAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontReceiveAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontShareService;
import com.xiangrui.lmp.business.homepage.service.FrontUserAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.ExpressEnum;
import com.xiangrui.lmp.business.homepage.vo.FrontCity;
import com.xiangrui.lmp.business.homepage.vo.FrontIdcard;
import com.xiangrui.lmp.business.homepage.vo.FrontOverSeaAddress;
import com.xiangrui.lmp.business.homepage.vo.FrontReceiveAddress;
import com.xiangrui.lmp.business.homepage.vo.FrontShare;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.business.homepage.vo.TariffHomePage;
import com.xiangrui.lmp.business.kuaidi100.service.ExpressInfoService;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.servlet.AuthImg;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.ValidatorUtil;
import com.xiangrui.lmp.util.bean.PkgLogExpress;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/")
public class IndexController
{
	/**
	 * LOG.
	 */
	private static final Logger logger = Logger.getLogger(HomepageController.class);
	/**
	 * 包裹日志
	 */
	@Autowired
	private PkgLogService pkgLogService;

	/**
	 * 包裹商品
	 */
	@Autowired
	private PkgGoodsService frontPkgGoodsService;

	/**
	 * 前台用户
	 */
	@Autowired
	private FrontUserService ffrontUserService;

	/**
	 * 前台用户
	 */
	@Autowired
	private FrontUserService memberService;
	@Autowired
	private QuotationManageService quotationManageService;

	/**
	 * 前台包裹
	 */
	@Autowired
	private PkgService frontPkgService;

	/**
	 * 底部链接查询
	 */
	@Autowired
	private FriendlyLinkService friendlyLinkService;

	/**
	 * 禁运物品查询
	 */
	@Autowired
	private ProhibitionService prohibitionService;

	/**
	 * 导航栏查询
	 */
	@Autowired
	private NavigationService navigationService;
	/**
	 * 公告信息查询
	 */
	@Autowired
	private NoticeService noticeService;

	/**
	 * 轮播图信息查询
	 */
	@Autowired
	private BannerService bannerService;

	/**
	 * 会员价格信息表
	 */
	@Autowired
	private FreightCostService freightCostService;

	/**
	 * 文章类别
	 */
	@Autowired
	private ArticleClassifyService articleClassifyService;

	/**
	 * 文章
	 */
	@Autowired
	private ArticleService articleService;

	/**
	 * 会员等级
	 */
	@Autowired
	private MemberRateService memberRateService;
	/**
	 * 增值服务
	 */
	@Autowired
	private AttachServiceService attachServiceService;

	/**
	 * 增值服务会员价格
	 */
	@Autowired
	private MemberAttachService memberAttachService;

	/**
	 * 收货地址
	 */
	@Autowired
	private FrontUserAddressService frontUserAddressService;

	/**
	 * 海外仓库
	 */
	@Autowired
	private FrontReceiveAddressService frontReceiveAddressService;

	@Autowired
	private FrontOverSeaAddressService frontOverSeaAddressService;

	/**
	 * 海外地址
	 */
	@Autowired
	private OverseasAddressService overseasAddressService;

	@Autowired
	private FrontCityService frontCityService;

	@Autowired
	private PackageMapper packageMapper;

	@Autowired
	private PkgAttachServiceMapper pkgAttachServiceMapper;

	// dddd
	@Autowired
	private PackageService packageService;

	@Autowired
	private com.xiangrui.lmp.business.admin.store.service.FrontUserService frontUserService;

	@Autowired
	private UserAddressService userAddressService;

	@Autowired
	private FrontPkgMapper frontPkgMapper;

	/**
	 * 优惠券处理服务
	 */
	@Autowired
	private CouponService couponService;
	/**
	 * 晒单服务
	 */
	@Autowired
	private FrontShareService frontShareService;

	@Autowired
	private SeaportService seaportService;

	@Autowired
	private com.xiangrui.lmp.business.admin.pkg.service.PackageService backpackageService;

	@Autowired
	private com.xiangrui.lmp.business.admin.store.service.FrontUserService backfrontUserService;
	@Autowired
	private com.xiangrui.lmp.business.admin.pkg.service.PkgGoodsService backpkgGoodsService;
	@Autowired
	private SeaportDeclarationService seaportDeclarationService;

	@Autowired
	private ConsultInfoService consultInfoService;
	
	@Autowired
	private FrontIdcardService frontIdcardService;
	
	/**
	 * 逻辑删除0：未删除
	 */
	private static final int ISDELETED_NO = 0;

	/**
	 * 包裹状态：可用
	 */
	private static final int PKG_ENABLE = 1;
	/**
	 * 逻辑删除1：删除
	 */
	@SuppressWarnings("unused")
	private static final int ISDELETED_YES = 1;

	/**
	 * 邮箱方式
	 */
	public static final int USER_INFO_TYPE_EMAIL = 1;
	/**
	 * 手机方式
	 */
	public static final int USER_INFO_TYPE_PHONE = 2;
	/**
	 * 账号存在
	 */
	public static final int USER_INFO_ACCOUNT_EXIST = 1;
	/**
	 * 账号不存在
	 */
	public static final int USER_INFO_ACCOUNT_NOEXIST = 0;
	/**
	 * 验证码匹配正确
	 */
	public static final int USER_INFO_SMS_YES = 1;
	/**
	 * 验证码匹配错误
	 */
	public static final int USER_INFO_SMS_NO = 0;
	/**
	 * 增值服务状态(0:无增值服务,1:未完成，2:已完成)
	 */
	private static final int ATTACH_STATUS_NOEXIST = 0;

	/**
	 * 公司运单号的形式
	 */
	private static final String[] ORIGINALNUM_TYPE = { "9577" };

	/**
	 * . 跳转到首页
	 * 
	 * @param modelMap
	 *            ModelMap
	 * @param httpRequest
	 *            HttpServletRequest
	 * @param httpSession
	 *            HttpSession
	 * @return String String
	 */
	@RequestMapping(value = "/")
	public String getHomepagelList(ModelMap modelMap, HttpServletRequest req, HttpSession httpSession)
	{
		ServletContext application = req.getSession().getServletContext();
		// 在application中不存在时
		if (null == application.getAttribute("TariffHomePageList"))
		{
			// 资费管理信息
			List<TariffHomePage> tariffHomePages = new ArrayList<TariffHomePage>();

			// 查询条件
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("isdeleted", BaseMemberRate.ISDELETED_NO);
			params.put("ISDELETED", BaseMemberRate.ISDELETED_NO);
			params.put("rate_type", BaseMemberRate.RATE_TYPE_ORDINARY_LEVEL);
			params.put("order", "DESC");

			// 会员等级处理
			List<MemberRate> memberRateList = memberRateService.queryAllMemberRate(params);

			TariffHomePage tariffMrName = new TariffHomePage();
			tariffMrName.setTitle("       ");
			tariffMrName.setTitileCss(TariffHomePage.CSS_STD_SE_LIT);
			tariffMrName.setInfoStratCss(TariffHomePage.CSS_STD_SE_LITX);
			tariffMrName.setInfoEndCss(TariffHomePage.CSS_STD_SE_LITX);

			TariffHomePage tariffMr = new TariffHomePage();
			tariffMr.setTitle("会员等级");
			tariffMr.setTitileCss(TariffHomePage.CSS_STD_SE_LIT);
			tariffMr.setInfoStratCss(TariffHomePage.CSS_STD_SE_LIF);
			tariffMr.setInfoEndCss(TariffHomePage.CSS_STD_SE_LIS);

			List<Integer> glList = new ArrayList<Integer>();

			int lavle = 1;
			for (MemberRate memberRate : memberRateList)
			{
				if (memberRate.getRate_type() == BaseMemberRate.RATE_TYPE_PEER_LEVEL)
				{
					continue;
				}
				glList.add(memberRate.getRate_id());
				tariffMrName.getInfoList().add(memberRate.getRate_name());
				if (lavle == memberRateList.size())
				{
					tariffMr.getInfoList().add(TariffHomePage.LAVEL_IMG_CU);
				}
				else if (lavle == memberRateList.size() - 1)
				{
					tariffMr.getInfoList().add(TariffHomePage.LAVEL_IMG_AG);
				}
				else if (lavle == memberRateList.size() - 2)
				{
					tariffMr.getInfoList().add(TariffHomePage.LAVEL_IMG_AU);
				}
				else if (lavle == memberRateList.size() - 3)
				{
					tariffMr.getInfoList().add(TariffHomePage.LAVEL_IMG_PT);
				}
				else if (lavle == memberRateList.size() - 4)
				{
					tariffMr.getInfoList().add(TariffHomePage.LAVEL_IMG_C);
				}
				else
				{
					tariffMr.getInfoList().add(TariffHomePage.LAVEL_IMG_C);
				}
				lavle++;
			}
			// 存储会员信息
			tariffHomePages.add(tariffMrName);
			tariffHomePages.add(tariffMr);

			// 标准服务
			List<FreightCost> freightCosts = freightCostService.queryAll(null);
			TariffHomePage tariffFc = new TariffHomePage();
			tariffFc.setTitle("标准服务");
			tariffFc.setTitileCss(TariffHomePage.CSS_STD_SE_LIFV);
			tariffFc.setInfoStratCss(TariffHomePage.CSS_STD_SE_LIFO);
			tariffFc.setInfoEndCss(TariffHomePage.CSS_STD_SE_LIFV);
			for (Integer rate_id : glList)
			{
				boolean isCun = false;
				for (FreightCost freightCost : freightCosts)
				{
					int fcMrId = freightCost.getRate_id();
					if (rate_id == fcMrId)
					{
						tariffFc.getInfoList().add(freightCost.getUnit_price() + "");
						isCun = true;
						freightCosts.remove(freightCost);
						break;
					}
				}
				if (!isCun)
				{
					tariffFc.getInfoList().add("0");
				}
			}

			// 存储标准服务
			tariffHomePages.add(tariffFc);

			// 增值服务处理
			// 增值服务列表
			List<AttachService> attachServiceList = attachServiceService.queryAllAttachService(params);
			// 增值服务关联的价格
			List<MemberAttach> attachs = this.memberAttachService.queryAll(params);

			boolean isCss = true;
			// 遍历增值服务列表
			for (AttachService attachService : attachServiceList)
			{
				String asName = attachService.getService_name();
				TariffHomePage tariffAs = new TariffHomePage();
				tariffAs.setTitle(asName);

				if (isCss)
				{
					tariffAs.setTitileCss(TariffHomePage.CSS_STD_SE_LIT);
					tariffAs.setInfoStratCss(TariffHomePage.CSS_STD_SE_LISX);
					tariffAs.setInfoEndCss(TariffHomePage.CSS_STD_SE_LISE);
					isCss = false;
				}
				else
				{
					tariffAs.setTitileCss(TariffHomePage.CSS_STD_SE_LIT_SPAN);
					tariffAs.setInfoStratCss(TariffHomePage.CSS_STD_SE_LIFO);
					tariffAs.setInfoEndCss(TariffHomePage.CSS_STD_SE_LIEG);
					isCss = true;
				}
				int arrach_id = attachService.getAttach_id();
				for (Integer rate_id : glList)
				{
					boolean isCun = false;
					// 遍历服务价格
					for (MemberAttach memberAttach : attachs)
					{
						int maAId = memberAttach.getAttach_id();
						int maRId = memberAttach.getMemberRateAttach().getRate_id();
						if (arrach_id == maAId && rate_id == maRId)
						{
							tariffAs.getInfoList().add(memberAttach.getService_price() + "");
							attachs.remove(memberAttach);
							isCun = true;
							break;
						}
					}
					if (!isCun)
					{
						tariffAs.getInfoList().add("0");
					}
				}

				// 存储增值服务信息
				tariffHomePages.add(tariffAs);
			}

			// 保存到application
			if (null == application.getAttribute("MemberRateIdHomePageList"))
			{
				application.setAttribute("MemberRateIdHomePageList", glList);
				int lavesize = 100 / lavle;
				if (100 % lavle == 0)
				{
					lavesize--;
				}
				application.setAttribute("lavlesize", lavesize);

			}
			application.setAttribute("TariffHomePageList", tariffHomePages);
		}

		List<ConsultInfo> consultInfoList = consultInfoService.queryAllAvaliable4();
		if (consultInfoList != null && consultInfoList.size() > 0)
		{
			for (ConsultInfo consultInfo : consultInfoList)
			{
				String tile = consultInfo.getConsult_title();
				if (tile.length() > 23)
				{
					tile = tile.substring(0, 23);
					tile += "...";
					consultInfo.setConsult_title(tile);
				}
				
				String detail = consultInfo.getConsult_detail();
				if (detail.length() > 23)
				{
					detail = detail.substring(0, 30);
					detail += "...";
					consultInfo.setConsult_detail(detail);
				}
			}
		}
		req.setAttribute("consultInfoList", consultInfoList);

		// 头部信息
		setHeadRequestAttribute(req, "/homepage/index");

		// 底部链接数据
		setFooterRequestAttribute(req);
		List<Notice> noticeInfoList=noticeService.queryAllHomepage5();
		req.setAttribute("noticeInfoList", noticeInfoList);
		
		return "/homepage/index";
	}

	/**
	 * 跳转首页
	 * 
	 * @param modelMap
	 * @param req
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/index")
	public String getHomepagelListIndex(ModelMap modelMap, HttpServletRequest req, HttpSession httpSession)
	{
		return getHomepagelList(modelMap, req, httpSession);
	}

	/**
	 * 查询重量,或者包裹 ajax
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/queryTypeWeight")
	@ResponseBody
	public Map<String, Object> queryTypeWeight(HttpServletRequest req, String val)
	{
		ServletContext application = req.getSession().getServletContext();

		List<Integer> glList = new ArrayList<Integer>();
		if (null == application.getAttribute("MemberRateIdHomePageList"))
		{
			// 查询条件
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("isdeleted", BaseMemberRate.ISDELETED_NO);
			params.put("rate_type", BaseMemberRate.RATE_TYPE_ORDINARY_LEVEL);

			// 会员等级处理
			List<MemberRate> memberRateList = memberRateService.queryAllMemberRate(params);
			for (MemberRate memberRate : memberRateList)
			{
				glList.add(memberRate.getRate_id());
			}
			application.setAttribute("MemberRateIdHomePageList", glList);
		}
		glList = (List<Integer>) application.getAttribute("MemberRateIdHomePageList");
		Map<String, Object> map = searchWeight(glList, val.trim());
		return map;
	}

	/**
	 * 查询重量,或者包裹 ajax
	 * 
	 * @return
	 */
	@RequestMapping("/queryTypeCode")
	@ResponseBody
	public List<PkgLogExpress> queryTypeCode(String val)
	{
		try
		{
			val = val.trim();
			Pkg pkgObj = new Pkg();
			List<PkgLog> pkgLogs = new ArrayList<PkgLog>();
			// 通过运单号匹配包裹
			List<Pkg> pkgs = new ArrayList<Pkg>();

			
			if (!isOriginalNum(val))
			{
				// 公司运单号
				pkgs = this.frontPkgService.queryPackageByLogisticsCode(val);				
			}
			else
			{
				// 关联单号
				pkgs = this.frontPkgService.queryPackageByOriginalNum(val);
			}

			if (null != pkgs && pkgs.size() > 0)
			{
				pkgObj = pkgs.get(0);
				// 读取包裹的操作日志
				pkgLogs = this.pkgLogService.queryPkgLogByPackageId(pkgObj.getPackage_id());
			}

			List<PkgLogExpress> list = new ArrayList<PkgLogExpress>();

			// 匹配快件发送返回包裹状态信息,查询是id倒序
			List<ExpressInfo> expressInfos = expressInfoService.queryExpressInfoByCode(pkgObj.getLogistics_code(), pkgObj.getEms_code());

			if (null != expressInfos && expressInfos.size() > 0)
			{
				// 仅仅获取最近的一次发送返回信息,集合第一条
				ExpressInfo expressInfo = expressInfos.get(0);

				// 快递100报文数据
				String data = expressInfo.getData();
				if (null != data && !"".equals(data))
				{
					Object[] pkgLogExpresses = JSONTool.getDTOArray(data, PkgLogExpress.class, null);

					int i = 0;
					for (Object pkgLogExpress : pkgLogExpresses)
					{
						PkgLogExpress express = (PkgLogExpress) pkgLogExpress;
						// 签收状态
						express.setStatus(BasePackage.LOGISTICS_SIGN_IN);

						// 主动创建一个包裹已签收的显示数据,仅仅一条
						if (i == 0)
						{
							// 已签收
							if (ExpressInfo.ISCHECK_YES.equals(expressInfo.getIscheck()))
							{
								PkgLogExpress expressSignIn = new PkgLogExpress();
								expressSignIn.setFtime(express.getFtime());
								expressSignIn.setTime(express.getTime());
								expressSignIn.setStatus(BasePackage.LOGISTICS_SIGN_IN);
								expressSignIn.setContext("包裹已签收");
								list.add(expressSignIn);
							}
						}
						i++;
						list.add(express);
					}
				}
			}

			// 是否有快递信息
			boolean isHasExpressInfo = (list.size() > 0);

			// 拼接包裹操作记录的页面显示数据
			for (PkgLog pkgLog : pkgLogs)
			{

				// 判定某个状态是否为废弃状态
				boolean isDiscard = (PackageLogUtil.STATUS_DESC_NO.equals(pkgLog.getDescription()));

				// 废弃则跳出
				if (isDiscard)
				{
					continue;
				}
				// (有快递信息)
				if (isHasExpressInfo)
				{
					// (签收信息不为已废弃&&不为签收状态)
					if (pkgLog.getOperated_status() != BasePackage.LOGISTICS_SIGN_IN)
					{
						String time = pkgLog.getOperate_time_toStr();
						int status = pkgLog.getOperated_status();
						String context = pkgLog.getDescription();
						String ftime = "";
						PkgLogExpress pkgLogExprEntity = new PkgLogExpress(time, status, context, ftime);
						list.add(pkgLogExprEntity);
					}
				}
				else
				{
					String time = pkgLog.getOperate_time_toStr();
					int status = pkgLog.getOperated_status();
					String context = pkgLog.getDescription();
					String ftime = "";
					PkgLogExpress pkgLogExprEntity = new PkgLogExpress(time, status, context, ftime);
					list.add(pkgLogExprEntity);
				}
			}

			return list;
		} catch (Exception e)
		{
			e.printStackTrace();
			List<PkgLogExpress> list = new ArrayList<PkgLogExpress>();
			list.add(new PkgLogExpress("", 0, "数据异常", ""));
			return null;
		}
	}

	/**
	 * 跳转到禁运物品
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/prohibition")
	public String getProhibition(HttpServletRequest httpRequest)
	{
		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/prohibition");
		// 头部登录信息

		// 禁运物品数据
		ServletContext application = httpRequest.getSession().getServletContext();
		if (null == application.getAttribute("prohibitionLists"))
		{
			application.setAttribute("prohibitionLists", prohibitionService.queryAll(null));
		}

		// 底部链接数据
		setFooterRequestAttribute(httpRequest);

		return "/homepage/prohibition";
	}

	/**
	 * . 跳转到公告信息
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/noticeInfo")
	public String getNoticeInfo(HttpServletRequest httpRequest, int noticeId)
	{
		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/notice");

		// 公告信息
		Notice notice = noticeService.queryAllId(noticeId);
		List<Notice> notiList = noticeService.queryAllHomepage6();
		if(notice == null){
			if(notiList != null && notiList.size() > 0){
				notice = notiList.get(0);
			}
		}
		
		// 公告信息
		httpRequest.setAttribute("noticePojo", notice );
		
		//查询公告列表信息
		httpRequest.setAttribute("noticeInfoList", notiList);

		// 底部数据
		setFooterRequestAttribute(httpRequest);

		return "/homepage/notice";
	}

	@Autowired
	private PkgAttachServiceService pkgAttachServiceService;
	
	/**
	 * 个人中心
	 */
	@RequestMapping(value = "/personalCenter")
	public String personalCenter(HttpServletRequest httpRequest, String status, String payStatus, String searchVal, String pkgPrint)
	{
		// 没有登录用户
		if (null == httpRequest.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}

		HttpSession session = httpRequest.getSession();
		int user_id = ((FrontUser) session.getAttribute("frontUser")).getUser_id();
		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);
		
		httpRequest.setAttribute("frontUser", frontUser);
		
		// 我的包裹六种状态
		int[] cAry = this.getTransportSumNew(user_id);
		httpRequest.setAttribute("c1", cAry[0]);
		httpRequest.setAttribute("c2", cAry[1]);
		httpRequest.setAttribute("c3", cAry[2]);
		httpRequest.setAttribute("c4", cAry[3]);
		httpRequest.setAttribute("c5", cAry[4]);
		httpRequest.setAttribute("c6", cAry[5]);
		
		// 待处理订单、待支付运费、待支付关税
		int[] tAry = this.getTransportSumThree(user_id);
		httpRequest.setAttribute("t1", tAry[0]);
		httpRequest.setAttribute("t2", tAry[1]);
		httpRequest.setAttribute("t3", tAry[2]);
		
		// 优惠券数量
		Integer coupon_num = couponService.sumAvaliableQuantityByUserId(frontUser.getUser_id());
		httpRequest.setAttribute("coupon_num", coupon_num);
		
		// 转到哪里这个前端改吧
		return "/homepage/personalCenter";
	}
	
	/**
	 * 取所有渠道
	 * @return
	 */
	@RequestMapping("/getExpressAll")
	@ResponseBody
	public List<Map<String,Object>> getExpressAll(){
		
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		for(ExpressEnum exp:ExpressEnum.values()){
			Map<String,Object> result=new HashMap<String,Object>();
			result.put("id",exp.getId());
			result.put("msg",exp.getMsg());
			list.add(result);
		}
		return list;
	}
	
	private List<FrontReceiveAddress> getAddressByUserId(int userId){
		Map<String, Object> paramsUser = new HashMap<String, Object>();
		paramsUser.put("user_id", userId);
		List<FrontReceiveAddress> userAddressList = frontReceiveAddressService.queryReceiveAddressByUserId(paramsUser);
		List<FrontReceiveAddress> userAddressNewList = new ArrayList<FrontReceiveAddress>();
		for (FrontReceiveAddress addr : userAddressList)
		{
			// 处理地址数据
			String regionStr = addr.getRegion();
			try
			{
				// json数据转换
				@SuppressWarnings("unchecked")
				List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);
				Collections.sort(regionlist);

				StringBuffer buffer = new StringBuffer();

				// 拼接地址信息
				for (Region region : regionlist)
				{
					buffer.append(region.getName());
					buffer.append(",");
				}
				String street = addr.getStreet();
				buffer.append(street);

				addr.setAddressStr(buffer.toString());

				userAddressNewList.add(addr);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return userAddressNewList;
	}
	
	/**
	 * 新建包裹
	 */
	@RequestMapping(value = "/newPackage")
	public String newPackage(HttpServletRequest httpRequest, String flag, String logistics_code, Integer osaddr_id )
	{
		FrontUser fUser = (FrontUser) httpRequest.getSession().getAttribute("frontUser");
		// 没有登录用户
		if (null == fUser)
		{
			return "/homepage/login";
		}
		int userId = fUser.getUser_id();
		
		logger.debug("flag: "+flag);
		httpRequest.setAttribute("newPackage flag", flag);
		
		// 关联单号
		httpRequest.setAttribute("logistics_code", logistics_code);
		
		// 海外仓库地址ID
		httpRequest.setAttribute("osaddr_id", osaddr_id);

		// 收获地址
		httpRequest.setAttribute("addressList", getAddressByUserId(userId));
		
		List<FrontOverSeaAddress> frontOverSeadAddressList = frontOverSeaAddressService.queryAllByUserId(userId);
		// 海外地址
		httpRequest.setAttribute("frontOverSeadAddressList", frontOverSeadAddressList);
		
		// 包裹总数
		httpRequest.setAttribute("pkgCount", frontPkgService.queryCountByUserId(userId));
		
		List<AttachService> attachServiceList = attachServiceService.selectAttachService(userId);
		List<AttachService> list = new ArrayList<AttachService>();
		StringBuffer tool_price = new StringBuffer("");
		for (AttachService attachService : attachServiceList)
		{
			// 过滤掉特殊性质的增值服务数据,比如分箱,
			if (attachService.getAttach_id() != BaseAttachService.MERGE_PKG_ID && attachService.getAttach_id() != BaseAttachService.SPLIT_PKG_ID)
			{
				list.add(attachService);
				tool_price.append(attachService.getService_price() + ",");
			}
		}
		// 增值服务的各价格组成的 X,X,X,X, 格式,放置到jsp中便于页面调用
		httpRequest.setAttribute("TOOL_PRICE", tool_price);
		if (list.size() > 0)
		{
			// 数据库的增值服务信息
			httpRequest.getSession().setAttribute("attachServiceList", list);
		}
		
		return "/homepage/new-package";
	}

	/**
	 * . 跳转到我的包裹——旧接口
	 * 
	 * @param httpRequest
	 *            req
	 * @return String String
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/transport")
	public String transport(HttpServletRequest httpRequest, String status,
	    String payStatus, String searchVal, String pkgPrint)
	   {
		// 没有登录用户
		if (null == httpRequest.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}

		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/transport");
		// 低部信息
		setFooterRequestAttribute(httpRequest);
		HttpSession session = httpRequest.getSession();
		int user_id = ((FrontUser) session.getAttribute("frontUser")).getUser_id();
		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);
		// 数据库的增值服务信息
		List<AttachService> attachServiceList = attachServiceService.selectAttachService(frontUser.getUser_id());
         
		/*增值服务信息**/
		List<AttachService> list = new ArrayList<AttachService>();
		StringBuffer tool_price = new StringBuffer("");
		for (AttachService attachService : attachServiceList)
		{
			// 过滤掉特殊性质的增值服务数据,比如分箱,
			if (attachService.getAttach_id() != BaseAttachService.MERGE_PKG_ID && attachService.getAttach_id() != BaseAttachService.SPLIT_PKG_ID)
			{
				list.add(attachService);
				tool_price.append(attachService.getService_price() + ",");
			}
		}
		// 增值服务的各价格组成的 X,X,X,X, 格式,放置到jsp中便于页面调用
		httpRequest.setAttribute("TOOL_PRICE", tool_price);
		if (list.size() > 0)
		{
			httpRequest.getSession().setAttribute("attachServiceList", list);
		} 
		 /*增值服务信息**/
		
		// 包裹查询
		//打印，未打印
        //支付，未支付
		//运单号，手机号，关联号
		
		String pageIndexStr = httpRequest.getParameter("pageIndex");
		int pageIndex = 1;
		int pagesize = 20;
		PageView pageView = null;
	    int allcount  = 0;
        int pageCount = 0;
         Map<String, Object> paramsCount = new HashMap<String, Object>();
         
        if (null != pageIndexStr && !"".equals(pageIndexStr) && !"undefined".equals(pageIndexStr))
		{
		    pageIndex = Integer.valueOf(pageIndexStr);
			pageView = new PageView(pageIndex, pagesize, 0);
		}
		else
		{
			pageView = new PageView(1, pagesize, 0);
		}
		Map<String, Object> params = new HashMap<String, Object>();

    	int curentStart = (pageIndex -1) * pagesize;
		params.put("start", curentStart);
		params.put("pagesize", pagesize);
		params.put("user_id", frontUser.getUser_id());
		params.put("enable", PKG_ENABLE);
		params.put("payStatus", payStatus); // pay info 
		params.put("pkgPrint", pkgPrint); //  打印信息 
 		 if (null != searchVal && !searchVal.equals(""))
		{
			// 搜索条件转码
			try
			{
				searchVal = URLDecoder.decode(searchVal, "utf-8");
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
			params.put("searchVal", searchVal); //运单号，手机号，关联号
		}
		// 查询包裹信息
		List<Pkg> pkgList = new ArrayList<Pkg>();
		 if (String.valueOf(BasePackage.LOGISTICS_SENT_ALREADY).equals(status))
		 {
			// 3-7状态的包裹：3:已出库,4:空运中,5:待清关,7:已清关并已派件中
			String str = String.valueOf(BasePackage.LOGISTICS_SENT_ALREADY) + "," + String.valueOf(BasePackage.LOGISTICS_AIRLIFT_ALREADY) + "," + String.valueOf(BasePackage.LOGISTICS_CUSTOMS_WAITING) + "," + String.valueOf(BasePackage.LOGISTICS_CUSTOMS_ALREADY);
			params.put("status",  str );

		 }else{
			 params.put("status",  status );
		 }
		 //查询数据
		 pkgList =  frontPkgService.queryMypagesByqueryPames(params);
		 
	    if (null != searchVal && !searchVal.equals(""))
		{ 
	    	// 如果是有条件输入了可能是分相单号，则，查询不到数据局时，再根据分相号去查询数据，只有一条数据
			// 根据原（分箱、合箱前）单号搜索
			if (pkgList == null || pkgList.size() == 0)
			{
				params.put("enable", 2);// 不可用
				pkgList = frontPkgService.queryPkgBySearch(params);
				pkgList = getRealPkgByOriginalPkgList(pkgList);
			}
		}  
		   // 汇总数据
	       allcount = frontPkgService.queryMypagesCountByqueryPames(params);
	       int tmppageCount = allcount/pagesize;
	       pageCount = tmppageCount;
	       if(tmppageCount* pagesize < allcount){
	    	   pageCount = tmppageCount + 1;
	       }
	       pageView.setRowCount(allcount);
		   pageView.setPageCount(pageCount);
		   pageView.setPageNow(pageIndex); 
		// 遍历加载包裹相关信息
		List<Pkg> pkgListNew = new ArrayList<Pkg>();
		for (Pkg pkg : pkgList)
		{
			// 包裹的商品
			List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());

			// 申报总价值
			BigDecimal total_worth = new BigDecimal("0");

			// 待支付关税
			BigDecimal pay_tax = new BigDecimal("0");

			for (PkgGoods pkgGoods : gooldList)
			{

				total_worth = total_worth.add(new BigDecimal(pkgGoods.getPrice()).multiply(new BigDecimal(pkgGoods.getQuantity())));

				pay_tax = pay_tax.add(new BigDecimal(pkgGoods.getCustoms_cost()));
			}

			pkg.setTotal_worth(total_worth.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());

			pkg.setPay_tax(pay_tax.floatValue());

			pkg.setGoods(gooldList);

			String zhuangtai = "";
			// 包裹的增值费
			BigDecimal attach_service_price = new BigDecimal("0");
			// 判断有无增值服务
			boolean flag = false;
			// 判断所有增值服务中是否有完成的状态
			boolean fagb = false;
			// 遍历增值服务
			for (AttachService service : attachServiceList)
			{
				// 是否存在这个增值服务数据
				PkgAttachService attachService = this.pkgAttachServiceService.queryAllPkgAttachServiceById(pkg.getPackage_id(), service.getAttach_id());

				if (null != attachService)
				{
					flag = true;
					// 计算每个包裹的增值服务价格总和
					attach_service_price = attach_service_price.add(new BigDecimal(attachService.getService_price()));
					// 增值服务完成
					if (attachService.getStatus() == PkgAttachService.ATTACH_FINISH && attachService.getAttach_id() != BaseAttachService.KEEP_PRICE_ID)
					{
						fagb = true;
						pkg.setAttach_status(PkgAttachService.ATTACH_FINISH);
					}
					// 产生分箱,合箱增值服务
					if (service.getAttach_id() == BaseAttachService.MERGE_PKG_ID || service.getAttach_id() == BaseAttachService.SPLIT_PKG_ID)
					{
						attachService.setService_name(service.getService_name());
						pkg.setAttach_id(service.getAttach_id());
						if (attachService.getStatus() == 1)
						{
							zhuangtai = "待" + attachService.getService_name();
						}
						else
						{
							zhuangtai = "已" + attachService.getService_name();
						}
					}

				}
				if (!flag)
				{
					pkg.setAttach_status(ATTACH_STATUS_NOEXIST);
				}
				if (!fagb)
				{
					pkg.setAttach_status(PkgAttachService.ATTACH_UNFINISH);
				}

			}

			pkg.setService_status(zhuangtai);
			pkg.setAttach_service_price(attach_service_price.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());

			// 根据Osaddr_id查询海外仓库地址
			FrontOverSeaAddress frontOverSeaAddress = frontOverSeaAddressService.queryAllById(pkg.getOsaddr_id());
			pkg.setIs_tax_free(frontOverSeaAddress.getIs_tax_free());
			pkg.setWarehouse(frontOverSeaAddress.getWarehouse());
			// 根据address_id查询用户收货地址
			FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(pkg.getAddress_id());
			// 不存在收货地址
			if (null == frontUserAddress)
			{
				pkg.setFrontUserAddress(new FrontUserAddress());
			}
			else
			{
				// 处理地址信息
				String regionStr = frontUserAddress.getRegion();
				try
				{
					List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);
					Collections.sort(regionlist);
					StringBuffer buffer = new StringBuffer();

					// 拼接地址信息
					for (Region region : regionlist)
					{
						buffer.append(region.getName());
						buffer.append(",");
					}
					String street = frontUserAddress.getStreet();
					buffer.append(street);
					frontUserAddress.setAddressStr(buffer.toString());
				} catch (Exception e)
				{
					e.printStackTrace();
				}
				// 保存到包裹
				pkg.setFrontUserAddress(frontUserAddress);
			}

			List<SubPackageBean> subPackageList = new ArrayList<SubPackageBean>();
			Map<String, Object> subparams = new HashMap<String, Object>();
			subparams.put("package_id", pkg.getPackage_id());
			String spec = ",";

			// 查询包裹是否有合箱服务
			subparams.put("attach_id", AttachService.MERGE_PKG_ID);
			List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(subparams);

			// 查询包裹是否有分箱服务
			subparams.put("attach_id", AttachService.SPLIT_PKG_ID);
			List<PkgAttachServiceGroup> splitlist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(subparams);

			if (mergelist != null && mergelist.size() > 0)
			{

				for (PkgAttachServiceGroup serviceGroup : mergelist)
				{
					String package_id_group = serviceGroup.getOg_package_id_group();
					String[] package_ids = package_id_group.split(spec);
					for (String id : package_ids)
					{
						com.xiangrui.lmp.business.admin.pkg.vo.Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(id));
						SubPackageBean subPackageBean = new SubPackageBean();
						subPackageBean.setOldOriginal_num(childpkg.getOriginal_num());
						subPackageBean.setArriveStatus(childpkg.getArrive_status());
						subPackageList.add(subPackageBean);
					}
				}
			}
			else if (splitlist != null && splitlist.size() > 0)
			{

				for (PkgAttachServiceGroup serviceGroup : splitlist)
				{
					String package_id_group = serviceGroup.getOg_package_id_group();
					com.xiangrui.lmp.business.admin.pkg.vo.Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(package_id_group));
					SubPackageBean subPackageBean = new SubPackageBean();
					subPackageBean.setOldOriginal_num(childpkg.getOriginal_num());
					subPackageBean.setArriveStatus(childpkg.getArrive_status());
					subPackageList.add(subPackageBean);
				}
			}
			else
			{
				SubPackageBean subPackageBean = new SubPackageBean();
				subPackageBean.setOldOriginal_num(pkg.getOriginal_num());
				subPackageBean.setArriveStatus(pkg.getArrive_status());
				subPackageList.add(subPackageBean);
			}
			// 晒单审核状态
			FrontShare frontShare = frontShareService.queryPkgShare(pkg.getPackage_id());
			int shareOrderApprovalStatus = 0;
			if (null != frontShare)
			{
				shareOrderApprovalStatus = frontShare.getApproval_status();
			}
			pkg.setShareOrderApprovalStatus(shareOrderApprovalStatus);

			// 前台要显示关联单号
			Pkg expressNumPkg = frontPkgMapper.queryExpressPackageByPackageId(pkg.getPackage_id());

			if (expressNumPkg != null && expressNumPkg.getExpress_num() != null)
			{
				String expressNum = expressNumPkg.getExpress_num();
				pkg.setExpress_num(expressNum.replaceAll(",", "<br>"));
			}
			pkg.setSubPackageList(subPackageList);
			// 重新保存到新的包裹集合
			pkgListNew.add(pkg);
		}
		httpRequest.setAttribute("pkgList", pkgListNew);
		httpRequest.setAttribute("pageView", pageView);

		// 获取用户邮寄地址
		Map<String, Object> paramsUser = new HashMap<String, Object>();
		paramsUser.put("user_id", frontUser.getUser_id());
		List<FrontReceiveAddress> userAddressList = frontReceiveAddressService.queryReceiveAddressByUserId(paramsUser);
		List<FrontReceiveAddress> userAddressNewList = new ArrayList<FrontReceiveAddress>();
		for (FrontReceiveAddress addr : userAddressList)
		{
			// 处理地址数据
			String regionStr = addr.getRegion();
			try
			{
				// json数据转换
				List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);

				Collections.sort(regionlist);

				StringBuffer buffer = new StringBuffer();

				// 拼接地址信息
				for (Region region : regionlist)
				{
					buffer.append(region.getName());
					buffer.append(",");
				}
				String street = addr.getStreet();
				buffer.append(street);

				addr.setAddressStr(buffer.toString());

				userAddressNewList.add(addr);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		// 有新的待处理包裹
		Map<String, Object> subparams = new HashMap<String, Object>();
		subparams.put("user_id", frontUser.getUser_id());
		subparams.put("status", 0);
		subparams.put("enable", PKG_ENABLE);
		List<Pkg> needToHandlepkgList = frontPkgService.queryNeedToHandlePackage(subparams);
		int neetToHandleCount = 0;
		if (needToHandlepkgList != null && needToHandlepkgList.size() > 0)
		{
			neetToHandleCount = needToHandlepkgList.size();
			httpRequest.setAttribute("neetToHandleCount", neetToHandleCount);
		}

		int[] cAry = getTransportSum(frontUser.getUser_id());
		httpRequest.setAttribute("c1", cAry[0]);
		httpRequest.setAttribute("c2", cAry[1]);
		httpRequest.setAttribute("c3", cAry[2]);
		httpRequest.setAttribute("c4", cAry[3]);
		httpRequest.setAttribute("c5", cAry[4]);
		httpRequest.setAttribute("c6", cAry[5]);
		httpRequest.setAttribute("frontUser", frontUser);
		if(status == ""){
			status = null;
		}
		httpRequest.setAttribute("status", status);
		httpRequest.setAttribute("payStatus", payStatus);
		httpRequest.setAttribute("pkgPrint", pkgPrint);
		httpRequest.setAttribute("addressList", userAddressNewList);

		List<FrontOverSeaAddress> frontOverSeadAddressList = frontOverSeaAddressService.queryAllByUserId(user_id);

		httpRequest.setAttribute("frontOverSeadAddressList", frontOverSeadAddressList);

		return "/homepage/transport";
	}
	/*@SuppressWarnings("unchecked")
	@RequestMapping(value = "/transport")
	public String transport(HttpServletRequest httpRequest, String status,
	    String payStatus, String searchVal, String pkgPrint)
	{
		// 没有登录用户
		if (null == httpRequest.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}
		
//		String result = checkIdCard(httpRequest,101041);

		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/transport");
		// 低部信息
		setFooterRequestAttribute(httpRequest);

		HttpSession session = httpRequest.getSession();

		int user_id = ((FrontUser) session.getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);
		// 数据库的增值服务信息
		List<AttachService> attachServiceList = attachServiceService.selectAttachService(frontUser.getUser_id());

		List<AttachService> list = new ArrayList<AttachService>();
		StringBuffer tool_price = new StringBuffer("");
		for (AttachService attachService : attachServiceList)
		{
			// 过滤掉特殊性质的增值服务数据,比如分箱,
			if (attachService.getAttach_id() != BaseAttachService.MERGE_PKG_ID && attachService.getAttach_id() != BaseAttachService.SPLIT_PKG_ID)
			{
				list.add(attachService);
				tool_price.append(attachService.getService_price() + ",");
			}
		}
		// 增值服务的各价格组成的 X,X,X,X, 格式,放置到jsp中便于页面调用
		httpRequest.setAttribute("TOOL_PRICE", tool_price);
		if (list.size() > 0)
		{
			httpRequest.getSession().setAttribute("attachServiceList", list);
		}
		
	    // 包裹总数
//        httpRequest.setAttribute("pkgCount", frontPkgService.queryCountByUserId(user_id));
//        // 可用优惠券数
//        httpRequest.setAttribute("countAvailable", couponService.sumAvaliableQuantityByUserId(user_id));

		// 包裹分页
		String pageIndexStr = httpRequest.getParameter("pageIndex");
		int pageIndex = 1;
		int pagesize = 10;
		PageView pageView = null;
	    int allcount  = 0;
        int pageCount = 0;
         Map<String, Object> paramsCount = new HashMap<String, Object>();
         paramsCount.put("user_id", user_id);
         if(status == null){
        	 status = "";
         }
       
        if (null != pageIndexStr && !"".equals(pageIndexStr) && !"undefined".equals(pageIndexStr))
		{
		    pageIndex = Integer.valueOf(pageIndexStr);
			pageView = new PageView(pageIndex, pagesize, 0);
		}
		else
		{
			pageView = new PageView(1, pagesize, 0);
		}
		
		 
		Map<String, Object> params = new HashMap<String, Object>();
		//params.put("pageView", pageView);
		// (#{pageno} -1)*#{pagesize} ,#{pagesize};
		int curentStart = (pageIndex -1) * pagesize;
		params.put("start", curentStart);
		params.put("pagesize", pagesize);
		params.put("user_id", frontUser.getUser_id());

		params.put("enable", PKG_ENABLE);
		// 查询包裹信息
		List<Pkg> pkgList = new ArrayList<Pkg>();

		if (null != payStatus && !payStatus.equals(""))
		{
			params.put("payStatus", payStatus);
			pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);

		}
		else if (null != status && !status.equals("") && !String.valueOf(BasePackage.LOGISTICS_SENT_ALREADY).equals(status))
		{
			// 待处理查询(后台创建的异常包裹，关联至客户)
			if (status.equals("-1"))
			{
				params.put("status", 0);
				pkgList = frontPkgService.queryNeedToHandlePackage(params);
			}
			// 已到库理查询
			else if (status.equals("-2"))
			{
				params.put("status", 0);
				pkgList = frontPkgService.queryArrvivedPackage(params);
			}
			else
			{
				params.put("status", status);
				pkgList = frontPkgService.queryPackageByUserIdAndStatusNormal(params);
			}

		}
		else if (null != pkgPrint && !pkgPrint.equals(""))
		{
			params.put("pkgPrint", pkgPrint);
			pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		}
		else if (String.valueOf(BasePackage.LOGISTICS_SENT_ALREADY).equals(status))
		{
			// 3-7状态的包裹：3:已出库,4:空运中,5:待清关,7:已清关并已派件中
			String str = String.valueOf(BasePackage.LOGISTICS_SENT_ALREADY) + "," + String.valueOf(BasePackage.LOGISTICS_AIRLIFT_ALREADY) + "," + String.valueOf(BasePackage.LOGISTICS_CUSTOMS_WAITING) + "," + String.valueOf(BasePackage.LOGISTICS_CUSTOMS_ALREADY);
			params.put("status", str);
			pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		}
		else if ("10".equals(status))
		{
			pkgList = frontPkgService.queryPkgByParam(params);
		}
		else if (null != searchVal && !searchVal.equals(""))
		{
			// 搜索条件转码
			try
			{
				searchVal = URLDecoder.decode(searchVal, "utf-8");
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
			params.put("searchVal", searchVal);
			params.put("enable", PKG_ENABLE);
			pkgList = frontPkgService.queryPkgBySearch(params);
			// 根据关联单号搜索
			if (pkgList == null || pkgList.size() == 0)
			{
				Map<String, Object> sparams = new HashMap<String, Object>();
				sparams.put("express_num", searchVal);
				sparams.put("user_id", frontUser.getUser_id());
				sparams.put("enable", PKG_ENABLE);
				sparams.put("pageView", pageView);
				pkgList = frontPkgMapper.queryExpressPackageByExpressNum(sparams);
			}
			// 根据原（分箱、合箱前）单号搜索
			if (pkgList == null || pkgList.size() == 0)
			{
				params.put("enable", 2);// 不可用
				pkgList = frontPkgService.queryPkgBySearch(params);
				pkgList = getRealPkgByOriginalPkgList(pkgList);
			}
		}
		else
		{
			// 包裹页面默认显示全部状态的包裹
			// params.put("status", BasePackage.LOGISTICS_STORE_WAITING);
			// pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
			pkgList = frontPkgService.queryPkgByParam(params);
			//取个总数出来
		}
		 if (String.valueOf(BasePackage.LOGISTICS_SENT_ALREADY).equals(status))
		 {
			// 3-7状态的包裹：3:已出库,4:空运中,5:待清关,7:已清关并已派件中
			String str = String.valueOf(BasePackage.LOGISTICS_SENT_ALREADY) + "," + String.valueOf(BasePackage.LOGISTICS_AIRLIFT_ALREADY) + "," + String.valueOf(BasePackage.LOGISTICS_CUSTOMS_WAITING) + "," + String.valueOf(BasePackage.LOGISTICS_CUSTOMS_ALREADY);
	      	paramsCount.put("status",  str );

		 }else{
			   paramsCount.put("status",  status );
		 }
	       allcount = frontPkgService.queryCountByUserIdStatus(paramsCount);
	       int tmppageCount = allcount/pagesize;
	       pageCount = tmppageCount;
	       if(tmppageCount* pagesize < allcount){
	    	   pageCount = tmppageCount + 1;
	       }
	       pageView.setRowCount(allcount);
		   pageView.setPageCount(pageCount);
		   pageView.setPageNow(pageIndex); 
		// 遍历加载包裹相关信息
		List<Pkg> pkgListNew = new ArrayList<Pkg>();
		for (Pkg pkg : pkgList)
		{
			// 包裹的商品
			List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());

			// 申报总价值
			BigDecimal total_worth = new BigDecimal("0");

			// 待支付关税
			BigDecimal pay_tax = new BigDecimal("0");

			for (PkgGoods pkgGoods : gooldList)
			{

				total_worth = total_worth.add(new BigDecimal(pkgGoods.getPrice()).multiply(new BigDecimal(pkgGoods.getQuantity())));

				pay_tax = pay_tax.add(new BigDecimal(pkgGoods.getCustoms_cost()));
			}

			pkg.setTotal_worth(total_worth.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());

			pkg.setPay_tax(pay_tax.floatValue());

			pkg.setGoods(gooldList);

			String zhuangtai = "";
			// 包裹的增值费
			BigDecimal attach_service_price = new BigDecimal("0");
			// 判断有无增值服务
			boolean flag = false;
			// 判断所有增值服务中是否有完成的状态
			boolean fagb = false;
			// 遍历增值服务
			for (AttachService service : attachServiceList)
			{
				// 是否存在这个增值服务数据
				PkgAttachService attachService = this.pkgAttachServiceService.queryAllPkgAttachServiceById(pkg.getPackage_id(), service.getAttach_id());

				if (null != attachService)
				{
					flag = true;
					// 计算每个包裹的增值服务价格总和
					attach_service_price = attach_service_price.add(new BigDecimal(attachService.getService_price()));
					// 增值服务完成
					if (attachService.getStatus() == PkgAttachService.ATTACH_FINISH && attachService.getAttach_id() != BaseAttachService.KEEP_PRICE_ID)
					{
						fagb = true;
						pkg.setAttach_status(PkgAttachService.ATTACH_FINISH);
					}
					// 产生分箱,合箱增值服务
					if (service.getAttach_id() == BaseAttachService.MERGE_PKG_ID || service.getAttach_id() == BaseAttachService.SPLIT_PKG_ID)
					{
						attachService.setService_name(service.getService_name());
						pkg.setAttach_id(service.getAttach_id());
						if (attachService.getStatus() == 1)
						{
							zhuangtai = "待" + attachService.getService_name();
						}
						else
						{
							zhuangtai = "已" + attachService.getService_name();
						}
					}

				}
				if (!flag)
				{
					pkg.setAttach_status(ATTACH_STATUS_NOEXIST);
				}
				if (!fagb)
				{
					pkg.setAttach_status(PkgAttachService.ATTACH_UNFINISH);
				}

			}

			pkg.setService_status(zhuangtai);
			pkg.setAttach_service_price(attach_service_price.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());

			// 根据Osaddr_id查询海外仓库地址
			FrontOverSeaAddress frontOverSeaAddress = frontOverSeaAddressService.queryAllById(pkg.getOsaddr_id());
			pkg.setIs_tax_free(frontOverSeaAddress.getIs_tax_free());
			pkg.setWarehouse(frontOverSeaAddress.getWarehouse());
			// 根据address_id查询用户收货地址
			FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(pkg.getAddress_id());
			// 不存在收货地址
			if (null == frontUserAddress)
			{
				pkg.setFrontUserAddress(new FrontUserAddress());
			}
			else
			{
				// 处理地址信息
				String regionStr = frontUserAddress.getRegion();
				try
				{
					List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);
					Collections.sort(regionlist);
					StringBuffer buffer = new StringBuffer();

					// 拼接地址信息
					for (Region region : regionlist)
					{
						buffer.append(region.getName());
						buffer.append(",");
					}
					String street = frontUserAddress.getStreet();
					buffer.append(street);
					frontUserAddress.setAddressStr(buffer.toString());
				} catch (Exception e)
				{
					e.printStackTrace();
				}
				// 保存到包裹
				pkg.setFrontUserAddress(frontUserAddress);
			}

			List<SubPackageBean> subPackageList = new ArrayList<SubPackageBean>();
			Map<String, Object> subparams = new HashMap<String, Object>();
			subparams.put("package_id", pkg.getPackage_id());
			String spec = ",";

			// 查询包裹是否有合箱服务
			subparams.put("attach_id", AttachService.MERGE_PKG_ID);
			List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(subparams);

			// 查询包裹是否有分箱服务
			subparams.put("attach_id", AttachService.SPLIT_PKG_ID);
			List<PkgAttachServiceGroup> splitlist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(subparams);

			if (mergelist != null && mergelist.size() > 0)
			{

				for (PkgAttachServiceGroup serviceGroup : mergelist)
				{
					String package_id_group = serviceGroup.getOg_package_id_group();
					String[] package_ids = package_id_group.split(spec);
					for (String id : package_ids)
					{
						com.xiangrui.lmp.business.admin.pkg.vo.Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(id));
						SubPackageBean subPackageBean = new SubPackageBean();
						subPackageBean.setOldOriginal_num(childpkg.getOriginal_num());
						subPackageBean.setArriveStatus(childpkg.getArrive_status());
						subPackageList.add(subPackageBean);
					}
				}
			}
			else if (splitlist != null && splitlist.size() > 0)
			{

				for (PkgAttachServiceGroup serviceGroup : splitlist)
				{
					String package_id_group = serviceGroup.getOg_package_id_group();
					com.xiangrui.lmp.business.admin.pkg.vo.Pkg childpkg = packageMapper.queryPackageById(Integer.parseInt(package_id_group));
					SubPackageBean subPackageBean = new SubPackageBean();
					subPackageBean.setOldOriginal_num(childpkg.getOriginal_num());
					subPackageBean.setArriveStatus(childpkg.getArrive_status());
					subPackageList.add(subPackageBean);
				}
			}
			else
			{
				SubPackageBean subPackageBean = new SubPackageBean();
				subPackageBean.setOldOriginal_num(pkg.getOriginal_num());
				subPackageBean.setArriveStatus(pkg.getArrive_status());
				subPackageList.add(subPackageBean);
			}
			// 晒单审核状态
			FrontShare frontShare = frontShareService.queryPkgShare(pkg.getPackage_id());
			int shareOrderApprovalStatus = 0;
			if (null != frontShare)
			{
				shareOrderApprovalStatus = frontShare.getApproval_status();
			}
			pkg.setShareOrderApprovalStatus(shareOrderApprovalStatus);

			// 前台要显示关联单号
			Pkg expressNumPkg = frontPkgMapper.queryExpressPackageByPackageId(pkg.getPackage_id());

			if (expressNumPkg != null && expressNumPkg.getExpress_num() != null)
			{
				String expressNum = expressNumPkg.getExpress_num();
				pkg.setExpress_num(expressNum.replaceAll(",", "<br>"));
			}
			pkg.setSubPackageList(subPackageList);
			// 重新保存到新的包裹集合
			pkgListNew.add(pkg);
		}
		httpRequest.setAttribute("pkgList", pkgListNew);
		httpRequest.setAttribute("pageView", pageView);

		// 获取用户邮寄地址
		Map<String, Object> paramsUser = new HashMap<String, Object>();
		paramsUser.put("user_id", frontUser.getUser_id());
		List<FrontReceiveAddress> userAddressList = frontReceiveAddressService.queryReceiveAddressByUserId(paramsUser);
		List<FrontReceiveAddress> userAddressNewList = new ArrayList<FrontReceiveAddress>();
		for (FrontReceiveAddress addr : userAddressList)
		{
			// 处理地址数据
			String regionStr = addr.getRegion();
			try
			{
				// json数据转换
				List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);

				Collections.sort(regionlist);

				StringBuffer buffer = new StringBuffer();

				// 拼接地址信息
				for (Region region : regionlist)
				{
					buffer.append(region.getName());
					buffer.append(",");
				}
				String street = addr.getStreet();
				buffer.append(street);

				addr.setAddressStr(buffer.toString());

				userAddressNewList.add(addr);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		// 有新的待处理包裹
		Map<String, Object> subparams = new HashMap<String, Object>();
		subparams.put("user_id", frontUser.getUser_id());
		subparams.put("status", 0);
		subparams.put("enable", PKG_ENABLE);
		List<Pkg> needToHandlepkgList = frontPkgService.queryNeedToHandlePackage(subparams);
		int neetToHandleCount = 0;
		if (needToHandlepkgList != null && needToHandlepkgList.size() > 0)
		{
			neetToHandleCount = needToHandlepkgList.size();
			httpRequest.setAttribute("neetToHandleCount", neetToHandleCount);
		}

		int[] cAry = getTransportSum(frontUser.getUser_id());
		httpRequest.setAttribute("c1", cAry[0]);
		httpRequest.setAttribute("c2", cAry[1]);
		httpRequest.setAttribute("c3", cAry[2]);
		httpRequest.setAttribute("c4", cAry[3]);
		httpRequest.setAttribute("c5", cAry[4]);
		httpRequest.setAttribute("c6", cAry[5]);
		httpRequest.setAttribute("frontUser", frontUser);
		httpRequest.setAttribute("status", status);
		httpRequest.setAttribute("payStatus", payStatus);
		httpRequest.setAttribute("pkgPrint", pkgPrint);
		httpRequest.setAttribute("addressList", userAddressNewList);

		List<FrontOverSeaAddress> frontOverSeadAddressList = frontOverSeaAddressService.queryAllByUserId(user_id);

		httpRequest.setAttribute("frontOverSeadAddressList", frontOverSeadAddressList);

		return "/homepage/transport";
	}*/

	/**
	 * 首页渠道修改
	 * 
	 * @param request
	 * @param response
	 * @param package_id
	 * @param express_package
	 * @return
	 */
	@RequestMapping("/editExpress")
	public String editExpress(HttpServletRequest request, HttpServletResponse response, int package_id, int express_package)
	{
		// 封装参数
		Map<String, Object> param = new HashMap<>();
		param.put("package_id", package_id);
		param.put("express_package", express_package);

		// 更新渠道
		this.packageService.updateExpressPackage(param);
		request.setAttribute("express_package", express_package);
		return "/homepage/transport";
	}

	/**
	 * 渠道修改窗口初始化
	 * 
	 * @param request
	 * @param response
	 * @param package_id
	 * @return
	 */
	@RequestMapping(value = "/expressPackage")
	public String expressPackage(HttpServletRequest request, HttpServletResponse response, int package_id)
	{
		request.setAttribute("package_id", package_id);
		return "/homepage/expressPackage";
	}

	/**
	 * 加载包裹页面的公共信息
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/warehouse")
	public String getWarehouse(HttpServletRequest request)
	{
		// 没有登录用户
		if (null == request.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}

		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

		int[] cAry = getTransportSum(frontUser.getUser_id());

		setFooterRequestAttribute(request);
		setHeadRequestAttribute(request, "/homepage/warehouse");

		Map<String, Object> map = new HashMap<String, Object>();
		// 逻辑删除0：未删除
		map.put("isdeleted", ISDELETED_NO);
		// List<OverseasAddress> overseasAddresses =
		// overseasAddressService.queryAllOverseasAddress(map);
		List<FrontOverSeaAddress> overseasAddresses = frontOverSeaAddressService.queryAllByUserId(user_id);
		request.setAttribute("overseasAddresses", overseasAddresses);
		request.setAttribute("c1", cAry[0]);
		request.setAttribute("c2", cAry[1]);
		request.setAttribute("c3", cAry[2]);
		request.setAttribute("c4", cAry[3]);
		request.setAttribute("c5", cAry[4]);
		request.setAttribute("c6", cAry[5]);

		// 有新的待处理包裹
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("enable", PKG_ENABLE);
		params.put("user_id", frontUser.getUser_id());
		params.put("status", 0);
		List<Pkg> needToHandlepkgList = frontPkgService.queryNeedToHandlePackage(params);
		int neetToHandleCount = 0;
		if (needToHandlepkgList != null && needToHandlepkgList.size() > 0)
		{
			neetToHandleCount = needToHandlepkgList.size();
			request.setAttribute("neetToHandleCount", neetToHandleCount);
		}

		return "/homepage/warehouse";
	}

	/**
	 * . 跳转到帮助中心
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/help")
	public String getHelp(HttpServletRequest httpRequest)
	{
		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/help");

		ServletContext application = httpRequest.getSession().getServletContext();
		String item = StringUtils.trimToNull(httpRequest.getParameter("item"));
		if (item != null)
		{
			httpRequest.setAttribute("item", item);
		}
		if (null == application.getAttribute("artClassList"))
		{
			application.setAttribute("artClassList", articleClassifyService.queryArticleClassifyLevelNoParent());
			application.setAttribute("artClassListParent", articleClassifyService.queryArticleClassifyLevel());

			application.setAttribute("artList", articleService.queryAll(null));
			application.setAttribute("prohibitionLists", prohibitionService.queryAll(null));
		}
		// 底部数据
		setFooterRequestAttribute(httpRequest);

		return "/homepage/help";
	}

	/**
	 * . 跳转到登录
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/login")
	public String getLogin(HttpServletRequest httpRequest)
	{

		return "/homepage/login";
	}

	/**
	 * 进入找回密码
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("retrievePassword")
	public String retrievePassword(HttpServletRequest request)
	{
		return "/homepage/retrievePassword";
	}

	/**
	 * 验证账号是否存在
	 * 
	 * @param userInfoAccount
	 * @return 0不存在,1存在
	 */
	@RequestMapping("existUserInfoAccount")
	@ResponseBody
	public int existUserAccount(String userInfoAccount)
	{
		FrontUser f = memberService.queryFrontUserByEmailOrMoible(userInfoAccount);
		if (null == f)
		{
			return USER_INFO_ACCOUNT_NOEXIST;
		}
		return USER_INFO_ACCOUNT_EXIST;
	}

	/*
	 * @RequestMapping("retPassType") public String
	 * retrievePasswordType(HttpServletRequest request){ return
	 * "homepage/login"; }
	 */
	/**
	 * 选择找回方式,进入下一步
	 * 
	 * @param request
	 * @param userInfoAccount
	 * @param userInfoType
	 * @return
	 */
	@RequestMapping("retPassType")
	public String retrievePasswordType(HttpServletRequest request, String userInfoAccount, int userInfoType)
	{
		if (null == userInfoAccount)
		{
			return "homepage/login";
		}
		request.setAttribute("account", userInfoAccount);
		request.setAttribute("type", userInfoType);

		return "/homepage/retPassType";
	}

	/**
	 * 重新选择找回方式,点击上一步
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("retPassword")
	public String retPassword(HttpServletRequest request)
	{
		// 清空验证码信息,防止已在手机端收到短信,换到邮箱出时,客户用手机端的验证信息匹配邮箱的信息
		request.getSession().removeAttribute("smsCode");

		return "/homepage/retrievePassword";
	}

	/**
	 * 验证手机验证码是否匹配
	 * 
	 * @param request
	 * @param smsCode
	 * @return 0不匹配,1匹配
	 */
	@RequestMapping("existSmsCode")
	@ResponseBody
	public int existUserSmsCode(HttpServletRequest request, String smsCode)
	{
		// request.getSession().setAttribute("smsCode","1234");
		String smsCodeSystem = "";
		if (null == request.getSession().getAttribute("smsCode"))
		{
			return USER_INFO_SMS_NO;
		}
		else
		{
			smsCodeSystem = request.getSession().getAttribute("smsCode").toString();
		}

		if (!"".equals(smsCodeSystem) && smsCode.equals(smsCodeSystem))
		{
			return USER_INFO_SMS_YES;
		}
		return USER_INFO_SMS_NO;
	}

	/**
	 * 更新账户密码
	 * 
	 * @param request
	 * @param account
	 *            账号,邮箱或者手机
	 * @param user_pwd
	 *            新密码
	 * @param sms_code
	 *            新验证码
	 * @param type
	 *            1是邮箱,2是手机
	 * @return
	 */
	@RequestMapping("retrieveUser")
	public String retrieveUser(HttpServletRequest request, String account, String user_pwd, String sms_code, int type)
	{
		if (null == account)
		{
			request.getSession().removeAttribute("smsCode");
			request.setAttribute("fail", "修改失败！网络故障！");
			return "/homepage/retrievePassword";
		}
		FrontUser frontUser = memberService.queryFrontUserByEmailOrMoible(account);
		frontUser.setSms_code(sms_code);
		frontUser.setUser_pwd(user_pwd);
		memberService.updateUserPwd(frontUser);
		request.getSession().removeAttribute("smsCode");
		return "homepage/login";
	}

	/**
	 * . 前台登录验证
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/flogin")
	public String flogin(FrontUser frontUser, HttpServletRequest req, HttpServletResponse resp, String txtCode)
	{
		String count = req.getParameter("count");
		int ct;
		if (null != count && !"".equals(count))
		{
			int cts = Integer.parseInt(count);
			ct = cts + 1;
		}
		else
		{
			ct = 1;
		}
		// 用户登录信息校验失败，则返回重新登录
		if (!validateUser(frontUser, req, ct))
		{
			req.setAttribute("count", ct);
			return "homepage/login";
		}
		// 查询用户信息
		FrontUser f = memberService.queryFrontUserByEmailOrMoible(frontUser.getEmail());

		if (f.getStatus() == BaseFrontUser.FRONT_USER_STATUS_DISABLE)
		{
			req.setAttribute("fail", "登录失败！当前用户已被禁用！");
			req.setAttribute("count", ct);
			return "/homepage/login";
		}
		if (f.getIsdeleted() == BaseFrontUser.ISDELETED_YES)
		{
			req.setAttribute("fail", "登录失败！当前用户已被删除！");
			req.setAttribute("count", ct);
			return "/homepage/login";
		}
		if (f.getStatus() == BaseFrontUser.FRONT_USER_STATUS_NO_ACTIVATION)
		{
			req.setAttribute("fail", "登录失败！当前用户未激活！");
			req.setAttribute("count", ct);
			return "/homepage/login";
		}

		HttpSession session = req.getSession();

		session.setAttribute("frontUser", f);

		return "redirect:/personalCenter";
	}

	/**
	 * . 用户退出
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/exit")
	public String exit(HttpServletRequest req)
	{
		HttpSession session = req.getSession();
		session.removeAttribute("frontUser");
		return "redirect:/";
	}

	/**
	 * . 跳转到注册
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/registe")
	public String getRegiste(HttpServletRequest httpRequest)
	{

		String recommand_user_id = StringUtils.trimToNull(httpRequest.getParameter("tr"));
		if (recommand_user_id != null)
		{
			try
			{
				byte[] recommandByte = Base64.decodeBase64(recommand_user_id.getBytes("utf-8"));
				recommand_user_id = new String(recommandByte);
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
		}
		// 推广链接
		String pushLinkId = StringUtils.trimToNull(httpRequest.getParameter("pl"));
		if (pushLinkId != null)
		{
			try
			{
				byte[] pushLinkIdByte = Base64.decodeBase64(pushLinkId.getBytes("utf-8"));
				pushLinkId = new String(pushLinkIdByte);
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
		}
		String source = StringUtils.trimToNull(httpRequest.getParameter("ts"));
		if (source != null)
		{
			try
			{
				byte[] sourceByte = Base64.decodeBase64(source.getBytes("utf-8"));
				source = new String(sourceByte);
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
		}
		httpRequest.setAttribute("source", source);
		httpRequest.setAttribute("pushLinkId", pushLinkId);
		httpRequest.setAttribute("recommand_user_id", recommand_user_id);
		return "/homepage/registe";
	}

	/**
	 * . 用户注册
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/fregiste")
	public String fregiste(FrontUser frontUser, HttpServletRequest req)
	{
		if (!validateRegUser(frontUser, req))
		{
			return "homepage/registe";
		}
		String trecommand_user_id = StringUtils.trimToNull(req.getParameter("trecommand_user_id"));
		if (trecommand_user_id != null)
		{
			frontUser.setRecommand_user_id(Integer.parseInt(trecommand_user_id));
		}

		Timestamp register_time = new Timestamp(System.currentTimeMillis());
		frontUser.setRegister_time(register_time);
		frontUser.setAccount(frontUser.getEmail());
		frontUser.setIntegral(BaseFrontUser.DEFAULT_INTEGRAL);
		frontUser.setUser_type(BaseFrontUser.USER_TYPE_OWN);
		frontUser.setStatus(BaseFrontUser.FRONT_USER_STATUS_NORMAL);
		frontUser.setIsfreeze(BaseFrontUser.IS_NOT_FREEZE_FRONT_USER);
		memberService.addFrontUser(frontUser);
		// 送注册优惠券
		frontUser = memberService.queryFrontUserByEmail(frontUser.getEmail());
		if (frontUser != null)
		{
			couponService.pushRegistCoupon(frontUser.getUser_id());
		}
		return "/homepage/login";
	}

	/**
	 * 前台普通用户注册（普通客户）
	 * 
	 * @param frontUser
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/userRegist")
	@ResponseBody
	// public Map<String, Object> userRegistHandle(FrontUser frontUser,
	// HttpServletRequest req)
	public Map<String, Object> userRegistHandle(HttpServletRequest req)
	{
		logger.info("用户注册处理...");
		Map<String, Object> result = new HashMap<String, Object>();
		boolean isSuccess = false;
		FrontUser frontUser = new FrontUser();
		String email = StringUtils.trimToNull(req.getParameter("email"));
		String user_pwd = StringUtils.trimToNull(req.getParameter("user_pwd"));
		String mobile = StringUtils.trimToNull(req.getParameter("mobile"));
		String sms_code = StringUtils.trimToNull(req.getParameter("sms_code"));
		String source = StringUtils.trimToNull(req.getParameter("source"));
		String pushLinkId = StringUtils.trimToNull(req.getParameter("pushLinkId"));
		frontUser.setEmail(email);
		frontUser.setUser_pwd(user_pwd);
		frontUser.setMobile(mobile);
		frontUser.setSms_code(sms_code);
		frontUser.setSource(source);
		if (pushLinkId != null)
		{
			frontUser.setPushLinkId(Integer.parseInt(pushLinkId));
		}
		if (validateRegUser(frontUser, req))
		{

			String trecommand_user_id = StringUtils.trimToNull(req.getParameter("trecommand_user_id"));
			if (trecommand_user_id != null)
			{
				frontUser.setRecommand_user_id(Integer.parseInt(trecommand_user_id));
			}
			Timestamp register_time = new Timestamp(System.currentTimeMillis());
			frontUser.setPushLinkId(frontUser.getPushLinkId());
			frontUser.setRegister_time(register_time);
			frontUser.setAccount(frontUser.getEmail());
			frontUser.setIntegral(BaseFrontUser.DEFAULT_INTEGRAL);
			frontUser.setUser_type(BaseFrontUser.USER_TYPE_OWN);
			frontUser.setStatus(BaseFrontUser.FRONT_USER_STATUS_NORMAL);
			frontUser.setIsfreeze(BaseFrontUser.IS_NOT_FREEZE_FRONT_USER);
			memberService.addFrontUser(frontUser);

			// 送注册优惠券
			frontUser = memberService.queryFrontUserByEmail(frontUser.getEmail());
			if (frontUser != null)
			{
				couponService.pushRegistCoupon(frontUser.getUser_id());
			}
			isSuccess = true;

			// 一，PC端-普通客户，用户注册成功后，需要在报价单关联表里面，添加该用户适用的系统已经存在的所有报价单。
			// 获取海外仓库地址id
			Map<String, Object> params = new HashMap<String, Object>();
			List<OverseasAddress> overseasAddressList = overseasAddressService.queryAllOverseasAddress(params);
			for (OverseasAddress overseasAddress : overseasAddressList)
			{
				params.clear();
				params.put("user_id", frontUser.getUser_id());
				params.put("user_type", BaseFrontUser.USER_TYPE_OWN);
				params.put("osaddr_id", overseasAddress.getId());
				params.put("account_type", 0);
				params.put("is_able", 1);// 默认报价单是否可用：1可用

				for (int i = 0; i < 6; i++)
				{
					params.put("express_package", i);
					QuotationManage qm = quotationManageService.getQuotaByPkgInfo2(overseasAddress.getId(), i, BaseFrontUser.USER_TYPE_OWN, frontUser.getUser_id());
					if (qm != null)
					{
						params.put("quota_id", qm.getQuota_id());
						params.put("create_time", new Date());
						quotationManageService.insertAuthQuota(params);
					}
				}
			} 
		}else{
			result.put("smsCodeErr", "短信验证码错误！");
 		}
		result.put("isSuccess", isSuccess);
		return result;
	}

	/**
	 * 注册时手机号码唯一的ajax
	 * 
	 * @param mobile
	 *            手机号码
	 * @return 返回 1,0 1是可以使用,0是不可使用
	 */
	@RequestMapping("/fregisteMobile")
	@ResponseBody
	public int getFregisteMobile(String mobile, String txtCode, HttpServletRequest req)
	{
		String imgVCValues = (String) req.getSession().getAttribute("AUTHIMG_CODE");

		// if (null != imgVCValues && !imgVCValues.equals(txtCode))
		if (!AuthImg.checkAuthingCode(imgVCValues, txtCode))
		{
			req.setAttribute("txtCodeErr", "验证码错误");
			
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(mobile);
			if (null == frontUser)
			{
				return 3;
			} 
			return 2;
		}
		else
		{
			FrontUser frontUser = ffrontUserService.queryFrontUserByMobile(mobile);
			if (null == frontUser)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

	/**
	 * 注册时邮箱唯一的ajax
	 * 
	 * @param email
	 *            邮箱
	 * @return 返回 1,0 1是可以使用,0是不可使用
	 */
	@RequestMapping("/fregisteEmail")
	@ResponseBody
	public int getFregisteEmail(String email)
	{
		FrontUser frontUser = ffrontUserService.queryFrontUserByEmail(email);
		if (null == frontUser)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * . 发送验证码
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/sendCode")
	public void sendCode(HttpServletRequest httpRequest, String mobile)
	{

	}

	/**
	 * 
	 * 校验登录信息
	 * 
	 * @param user
	 * @return
	 */
	private boolean validateUser(FrontUser frontUser, HttpServletRequest req, int ct)
	{
		String imgVCValues = (String) req.getSession().getAttribute("AUTHIMG_CODE");
		String txtCode = req.getParameter("txtCode");
		// System.out.println(imgVCValues);
		// System.out.println(txtCode);
		// if (null != imgVCValues && !imgVCValues.equals(txtCode) &&
		// !txtCode.equals("请输入验证码"))
		if ((!AuthImg.checkAuthingCode(imgVCValues, txtCode)) && ct > 4)
		{
			req.setAttribute("CodeErr", "验证码错误");
			return false;
		}
		FrontUser ff = memberService.frontUserLogin(frontUser);
		if (null == ff)
		{
			req.setAttribute("fail", "登录失败！用户名或密码错误！");
			return false;
		}
		return true;

	}

	/**
	 * 
	 * 校验注册信息
	 * 
	 * @param user
	 * @return
	 */
	private boolean validateRegUser(FrontUser frontUser, HttpServletRequest req)
	{
		HttpSession session = req.getSession();
		String smsCode = (String) session.getAttribute("smsCode");
		System.out.println(smsCode);
		if (null != smsCode && !smsCode.equals(frontUser.getSms_code()))
		{
			req.setAttribute("smsCodeErr", "验证码错误！");
			return false;
		}
		FrontUser ff = memberService.queryFrontUserByEmail(frontUser.getEmail());
		if (null != ff)
		{
			req.setAttribute("failReg", "注册失败，该用户已存在！");
			return false;
		}
		return true;
	}

	/**
	 * 主页查询包裹编号,状态轨迹list字符串
	 * 
	 * @param status
	 *            查询包裹的状态值
	 * @return list的格式是 状态名称,真假值(0/1)
	 */
	/**
	 * 主页查询包裹编号转换状态
	 * 
	 * @param val
	 * @return
	 */
	/*
	 * private String getPkgStatus(int val) { switch (val) { case -1: return
	 * "异常";
	 * 
	 * case 0: return "待入库"; case 1: return "已入库";
	 * 
	 * case 2: return "待发货"; case 3: return "已出库";
	 * 
	 * case 4: return "空运中";
	 * 
	 * case 5: return "待清关"; case 6: return "清关中"; case 7: return "已清关";
	 * 
	 * case 8: return "派件中"; case 9: return "已收货";
	 * 
	 * case 20: return "废弃"; case 21: return "退货"; case 22: return
	 * "废弃（分箱、合箱之后原包裹被废弃）"; default: return "错误"; } }
	 *//**
	 * 头部公用信息
	 * 
	 * @param request
	 * @param navClass
	 *            目标界面/跳转界面
	 */
	private void setHeadRequestAttribute(HttpServletRequest request, String navClass)
	{

		// 公告信息
		ServletContext application = request.getSession().getServletContext();

//		if (null == application.getAttribute("noticeLists"))
//		{
//			application.setAttribute("noticeLists", noticeService.queryAllHomepage());
//		}
		
		if (null == request.getAttribute("lastNotice"))
		{
			request.setAttribute("lastNotice", noticeService.queryLastNotice());
		}
		
		if (null == application.getAttribute("navigationParents"))
		{ // 导航信息
			Navigation navigation = new Navigation();
			navigation.setParent_id(0);
			application.setAttribute("navigationParents", navigationService.queryAllHomepage(navigation));
			navigation.setParent_id(1);
			application.setAttribute("navigationChilds", navigationService.queryAllHomepage(navigation));
			application.setAttribute("navClass", navClass);
		}
		if (null == request.getAttribute("bannerLists"))
		{
			request.setAttribute("bannerLists", bannerService.queryALlStatus(BaseBanner.BANNER_STATUS_ENABLE));
		}
	}

	/**
	 * 网站信息
	 */
	@Autowired
	private SiteInfoSerice siteInfoSerice;

	/**
	 * 底部公用信息
	 * 
	 * @param request
	 */
	private void setFooterRequestAttribute(HttpServletRequest request)
	{
		// 底部链接数据
		ServletContext application = request.getSession().getServletContext();
		if (null == application.getAttribute("friendlyLinkLists"))
		{
			application.setAttribute("friendlyLinkLists", friendlyLinkService.queryAllHomepage());
		}
		if (null == application.getAttribute("siteInfo"))
		{
			SiteInfo siteInfo = siteInfoSerice.queryAll();
			application.setAttribute("siteInfo", siteInfo);
		}
	}

	/**
	 * 获取重量查询的会员信息的json段代码
	 * 
	 * @param freightCosts
	 *            会员集合
	 * @param unitVal
	 *            重量/已经转换成为..x*b
	 * @param unitValg
	 *            重量/已经转换成为..x*kg
	 * @return
	 */
	private List<Map<String, Object>> getWeightLevel(List<FreightCost> freightCosts, float unitVal, float unitValg)
	{

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		int i = freightCosts.size();
		for (; i > 0;)
		{
			FreightCost levelVal = freightCosts.get(--i);
			Map<String, Object> map = new HashMap<String, Object>();
			// 会员名称
			String title = levelVal.getMemberRate().getRate_name();
			// 单价
			float priceb = levelVal.getUnit_price() * unitVal;

			float priceg = levelVal.getUnit_price() * unitValg;

			map.put("title", title);
			map.put("priceb", toFloatWei(priceb + "", 2));
			map.put("priceg", toFloatWei(priceg + "", 2));
			list.add(map);
		}
		return list;
	}

	public static String toFloatWei(String num, int wei)
	{
		String temp = num;
		int index = num.indexOf(".");
		int length = num.length();
		if (index == -1)
		{
			temp = num;
		}
		else if (wei <= 0)
		{
			temp = num.substring(0, index);
		}
		else if (length <= index + wei)
		{
			temp = num;
		}
		else if (length > index)
		{
			temp = num.substring(0, index + wei + 1);
		}
		System.out.println("num=" + num + "       temp=" + temp);
		return temp;
	}

	/**
	 * 查询重量的价格计算
	 * 
	 * @param val
	 * @return
	 */
	private Map<String, Object> searchWeight(List<Integer> glList, String val)
	{
		//
		Map<String, Object> result = new HashMap<String, Object>();

		float unitValg = Float.parseFloat(val) * 2.20462262f;
		float unitValb = Float.parseFloat(val);

		List<FreightCost> freightCosts = freightCostService.queryAll(null);
		List<FreightCost> freightCostPages = new ArrayList<FreightCost>();
		for (FreightCost freightCost : freightCosts)
		{
			int rate_id = freightCost.getRate_id();
			if (glList.contains(rate_id))
			{
				freightCostPages.add(freightCost);
			}
		}
		List<Map<String, Object>> list = getWeightLevel(freightCostPages, unitValb, unitValg);
		result.put("list", list);
		return result;

	}

	/**
     * 
     */
	@Autowired
	private ExpressInfoService expressInfoService;

	/**
	 * 查询包裹的数据
	 * 
	 * @param val
	 * @return
	 */
	/*
	 * private Map<String, Object> searchExpress(String val) { Map<String,
	 * Object> result = new HashMap<String, Object>();
	 * 
	 * List<Pkg> pkgs = frontPkgService
	 * .queryPackageByLogisticsCode(val.trim());
	 * 
	 * if (null != pkgs && pkgs.size() > 0) { Pkg pkg = pkgs.get(0);
	 * 
	 * String status_text = getPkgStatus(pkg.getStatus());
	 * result.put("status_text", status_text); List<Map<String, Object>>
	 * levelLists = getPkgLevel(pkg.getStatus()); result.put("list",
	 * levelLists); } else { result.put("list", null); } return result; }
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List getDTOList(String jsonString, Class clazz)
	{
		JSONArray array = null;
		try
		{
			array = JSONArray.fromObject(jsonString);
			List list = new ArrayList();
			for (Iterator iter = array.iterator(); iter.hasNext();)
			{
				JSONObject jsonObject = (JSONObject) iter.next();
				list.add(JSONObject.toBean(jsonObject, clazz));
			}
			return list;

		} catch (Exception e)
		{

			logger.error("json字符串转换为json对象解析错误", e);
			return null;
		}

	}

	/**
	 * 跳转到收货地址
	 * 
	 * @param request
	 */
	@RequestMapping("/destination")
	public String destination(HttpServletRequest request, HttpServletResponse response)
	{
		// 头部信息
		setHeadRequestAttribute(request, "/homepage/destination");
		// 低部信息
		setFooterRequestAttribute(request);

		// 获取省的数据
		List<FrontCity> allCityList = frontCityService.queryCity(0);

		JSONArray provinceArray = new JSONArray();

		JSONArray cityArray = new JSONArray();

		JSONArray areaArray = new JSONArray();

		for (FrontCity province : allCityList)
		{
			// 省
			JSONObject provinceObject = new JSONObject();
			provinceObject.put("city_id", province.getCity_id());
			provinceObject.put("father_id", province.getFather_id());
			provinceObject.put("city_name", province.getCity_name());
			provinceObject.put("postal_code", province.getPostal_code());
			provinceArray.add(provinceObject);
			// 获取市的数据
			List<FrontCity> cityList = frontCityService.queryCity(province.getCity_id());

			for (FrontCity city : cityList)
			{
				// 市
				JSONObject cityObject = new JSONObject();
				cityObject.put("city_id", city.getCity_id());
				cityObject.put("father_id", city.getFather_id());
				cityObject.put("city_name", city.getCity_name());
				cityObject.put("postal_code", city.getPostal_code());
				cityArray.add(cityObject);
				// 获取区的数据
				List<FrontCity> areaList = frontCityService.queryCity(city.getCity_id());

				for (FrontCity area : areaList)
				{
					// 区
					JSONObject areaObject = new JSONObject();
					areaObject.put("city_id", area.getCity_id());
					areaObject.put("father_id", area.getFather_id());
					areaObject.put("city_name", area.getCity_name());
					areaObject.put("postal_code", area.getPostal_code());
					areaArray.add(areaObject);
				}
			}
		}
		String provinceString = provinceArray.toString();
		String cityString = cityArray.toString();
		String areaString = areaArray.toString();
		System.out.println(provinceString);
		request.setAttribute("provinceString", provinceString);
		request.setAttribute("cityString", cityString);
		request.setAttribute("areaString", areaString);
		return "/homepage/destination";
	}

	/**
	 * 根据user_id找到用户各种包裹状态的数据
	 * 
	 * @param user_id
	 * @return int[]数组,总长度为6,6个值,分别是待入库数,已入库数,未支付数,支付数,未打印数，打印数
	 */
	public int[] getTransportSum(int user_id)
	{
		int[] array = new int[] { 0, 0, 0, 0, 0, 0 };
		// 待入库状态
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", user_id);
		params.put("enable", PKG_ENABLE);
		params.put("status", BasePackage.LOGISTICS_STORE_WAITING);
		List<Pkg> pkgList = frontPkgService.queryPackageByUserIdAndStatusNormal(params);
		array[0] = pkgList.size();

		// 已入库状态
		params.put("status", BasePackage.LOGISTICS_STORAGED);
		pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[1] = pkgList.size();

		params.remove("status");
		// 未支付状态
//		params.put("payStatus", BasePackage.PAYMENT_UNPAID);
//		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
//		array[2] = pkgList.size();
		params.put("payStatusFreight", BasePackage.PAYMENT_FREIGHT_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatusFreight(params);
		array[2] = pkgList.size();
		params.put("payStatusCustom", BasePackage.PAYMENT_CUSTOM_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatusCustom(params);
		array[2] += pkgList.size();

		// 已支付状态
//		params.put("payStatus", BasePackage.PAYMENT_PAID);
//		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
//		array[3] = pkgList.size();
		pkgList = frontPkgService.queryPackageByUserIdAndPaid(params);
		array[3] = pkgList.size();

		params.remove("payStatus");
		// 未打印状态
		params.put("pkgPrint", BasePackage.PACKAGE__NO_PRINT);
		pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		array[4] = pkgList.size();

		// 打印状态
		params.put("pkgPrint", BasePackage.PACKAGE__YES_PRINT);
		pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		array[5] = pkgList.size();

		return array;
	}
	
	/**
	 * 根据user_id找到用户各种包裹状态的数据 New
	 * 
	 * @param user_id
	 * @return int[]数组,总长度为6,6个值,分别是待入库数,已入库数,待发货,转运中/已出库,已签收,所有
	 */
	private int[] getTransportSumNew(int user_id){
		int[] array = new int[] { 0, 0, 0, 0, 0, 0 };
		// 待入库状态
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", user_id);
		params.put("enable", PKG_ENABLE);
		params.put("status", BasePackage.LOGISTICS_STORE_WAITING);
		List<Pkg> pkgList = frontPkgService.queryPackageByUserIdAndStatusNormal(params);
		array[0] = pkgList.size();

		// 已入库状态
		params.put("status", BasePackage.LOGISTICS_STORAGED);
		pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[1] = pkgList.size();
		
		// 待发货
		params.put("status", BasePackage.LOGISTICS_SEND_WAITING);
		pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[2] = pkgList.size();
		
		// 转运中/已出库
		params.put("status", BasePackage.LOGISTICS_SENT_ALREADY);
		pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[3] = pkgList.size();
		
		// 已签收
		params.put("status", BasePackage.LOGISTICS_SIGN_IN);
		pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[4] = pkgList.size();
		
		// 所有
		//临时修改 landeng
	    int pageno = 1;
	    int pagesize = 10;
	    
		pkgList = frontPkgService.queryPackageByUserId(user_id);
		array[5] = pkgList.size();
		
		return array;
	}
	
	/**
	 * 个人中心 待处理订单 待支付运费 待支付关税 的包裹数量
	 * @param user_id
	 * @return
	 */
	private int[] getTransportSumThree(int user_id){
		int[] array = new int[] { 0, 0, 0};
		
		// 待处理订单
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", user_id);
		params.put("enable", PKG_ENABLE);
		params.put("status", BasePackage.LOGISTICS_STORE_WAITING);
		List<Pkg> pkgList = frontPkgService.queryNeedToHandlePackage(params);
		array[0] = pkgList.size();
		
		// 待支付运费
		Map<String, Object> paramsA = new HashMap<String, Object>();
        paramsA.put("user_id", user_id);
//        paramsA.put("payStatus", Pkg.PAYMENT_UNPAID);
        paramsA.put("payStatusFreight", Pkg.PAYMENT_FREIGHT_UNPAID);
        paramsA.put("enable", Pkg.PACKAGE_ENABLE);
//		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(paramsA);
        pkgList = frontPkgService.queryPackageByUserIdAndPayStatusFreight(paramsA);
		array[1] = pkgList.size();
		
		// 待支付关税
        Map<String, Object> paramsB = new HashMap<String, Object>();
        paramsB.put("user_id", user_id);
//        paramsB.put("payStatus", Pkg.PAYMENT_CUSTOM_UNPAID);
        paramsB.put("payStatusCustom", Pkg.PAYMENT_CUSTOM_UNPAID);
        paramsB.put("enable", Pkg.PACKAGE_ENABLE);
//		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(paramsB);
        pkgList = frontPkgService.queryPackageByUserIdAndPayStatusCustom(paramsB);
		array[2] = pkgList.size();
		
		return array;
	}

	/**
	 * 判断是关联单号还是公司运单号
	 * 
	 * @param val
	 * @return 公司运单号 false 关联单号 true
	 */
	private boolean isOriginalNum(String val)
	{

		for (String originalNumStart : ORIGINALNUM_TYPE)
		{

			if (val.startsWith(originalNumStart))
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * . 跳转到帮助中心
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/sound_test")
	public String getSoundTest(HttpServletRequest httpRequest)
	{
		// 头部信息
		// setHeadRequestAttribute(httpRequest, "/homepage/help");
		// setFooterRequestAttribute(httpRequest);

		return "/homepage/sound_test";
	}

	/**
	 * 物流追踪
	 * 
	 * @return
	 */
	@RequestMapping("/query_logistics_detail")
	public String queryLogisticsDetail(HttpServletRequest request, HttpServletResponse response, String logistics_code)
	{
		List<PkgLogExpress> list = new ArrayList<PkgLogExpress>();
		try
		{
			logistics_code = logistics_code.trim();
			Pkg pkgObj = new Pkg();
			List<PkgLog> pkgLogs = new ArrayList<PkgLog>();
			// 通过运单号匹配包裹
			List<Pkg> pkgs = new ArrayList<Pkg>();

			// 公司运单号
			pkgs = this.frontPkgService.queryPackageByLogisticsCode(logistics_code);
			if (null != pkgs && pkgs.size() > 0)
			{
				pkgObj = pkgs.get(0);
				// 读取包裹的操作日志
				pkgLogs = this.pkgLogService.queryPkgLogByPackageId(pkgObj.getPackage_id());
			}

			// 匹配快件发送返回包裹状态信息,查询是id倒序
			List<ExpressInfo> expressInfos = expressInfoService.queryExpressInfoByCode(pkgObj.getLogistics_code(), pkgObj.getEms_code());

			if (null != expressInfos && expressInfos.size() > 0)
			{
				// 仅仅获取最近的一次发送返回信息,集合第一条
				ExpressInfo expressInfo = expressInfos.get(0);

				// 快递100报文数据
				String data = expressInfo.getData();
				if (null != data && !"".equals(data))
				{
					Object[] pkgLogExpresses = JSONTool.getDTOArray(data, PkgLogExpress.class, null);

					int i = 0;
					for (Object pkgLogExpress : pkgLogExpresses)
					{
						PkgLogExpress express = (PkgLogExpress) pkgLogExpress;
						// 签收状态
						express.setStatus(BasePackage.LOGISTICS_SIGN_IN);

						// 主动创建一个包裹已签收的显示数据,仅仅一条
						if (i == 0)
						{
							// 已签收
							if (ExpressInfo.ISCHECK_YES.equals(expressInfo.getIscheck()))
							{
								PkgLogExpress expressSignIn = new PkgLogExpress();
								expressSignIn.setFtime(express.getFtime());
								expressSignIn.setTime(express.getTime());
								expressSignIn.setStatus(BasePackage.LOGISTICS_SIGN_IN);
								expressSignIn.setContext("包裹已签收");
								list.add(expressSignIn);
							}
						}
						i++;
						list.add(express);
					}
				}
			}

			// 是否有快递信息
			boolean isHasExpressInfo = (list.size() > 0);

			// 拼接包裹操作记录的页面显示数据
			for (PkgLog pkgLog : pkgLogs)
			{

				// 判定某个状态是否为废弃状态
				boolean isDiscard = (PackageLogUtil.STATUS_DESC_NO.equals(pkgLog.getDescription()));

				// 废弃则跳出
				if (isDiscard)
				{
					continue;
				}
				// (有快递信息)
				if (isHasExpressInfo)
				{
					// (签收信息不为已废弃&&不为签收状态)
					if (pkgLog.getOperated_status() != BasePackage.LOGISTICS_SIGN_IN)
					{
						String time = pkgLog.getOperate_time_toStr();
						int status = pkgLog.getOperated_status();
						String context = pkgLog.getDescription();
						String ftime = "";
						PkgLogExpress pkgLogExprEntity = new PkgLogExpress(time, status, context, ftime);
						list.add(pkgLogExprEntity);
					}
				}
				else
				{
					String time = pkgLog.getOperate_time_toStr();
					int status = pkgLog.getOperated_status();
					String context = pkgLog.getDescription();
					String ftime = "";
					PkgLogExpress pkgLogExprEntity = new PkgLogExpress(time, status, context, ftime);
					list.add(pkgLogExprEntity);
				}
			}

		} catch (Exception e)
		{
		}
		request.setAttribute("list", list);
		request.setAttribute("logistics_code", logistics_code);
		return "/homepage/query_logistics_detail_show";
	}

	/**
	 * . 身份证上传中转
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/id")
	public String idCardUploadPre(HttpServletRequest httpRequest)
	{
		return "/homepage/idCardUpload";
	}

	/**
	 * 移动版身份证上传中转
	 * 
	 * @param httpRequest
	 *            HttpServletRequest
	 * @return String String
	 */
	@RequestMapping(value = "/mid")
	public String idCardMoblieUploadPre(HttpServletRequest httpRequest)
	{
		return "/homepage/idCardMobileUpload";
	}

	@RequestMapping("/uploadIdcardImage")
	public void uploadIdcardImage(HttpServletRequest request, HttpServletResponse resp) throws IOException
	{
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();

		String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource" + File.separator + "upload" + File.separator + "idcard" + File.separator;
		File file = new File(ctxPath);

		if (!file.exists())
		{
			file.mkdirs();
		}
		// 上传文件名
		String fileName = null;
		// 返回前台图片路径
		String img_path = null;
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet())
		{
			MultipartFile mf = entity.getValue();
			fileName = mf.getOriginalFilename();
			String newFilename = "";
			img_path = "/" + "upload" + "/" + "idcard" + "/";
			try
			{
				newFilename = getIdcardNewName(fileName);
				// 将文件输出
				String path = ctxPath + newFilename;
				File uploadFile = new File(path);
				FileCopyUtils.copy(mf.getBytes(), uploadFile);

				// 数据库存储路径
				img_path = img_path + newFilename;
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		resp.setHeader("Content-type", "text/html;charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().append(img_path);
	}

	@RequestMapping("/uploadIdcardImageNew")
	@ResponseBody
	public String uploadIdcardImageNew(HttpServletRequest request, HttpServletResponse resp) 
			throws IOException {
		String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource" + File.separator + "upload" + File.separator + "idcard" + File.separator;
		File file = new File(ctxPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		// 上传文件
		String fileStr = request.getParameter("fileStr");
		// 上传文件名
		String fileName = request.getParameter("fileName");
		// 返回前台图片路径
		String img_path = null;
		String newFilename = "";
		img_path = "/" + "upload" + "/" + "idcard" + "/";
		try {
			newFilename = getIdcardNewName(fileName);
			// 将文件输出
			String path = ctxPath + newFilename;
			File uploadFile = new File(path);
			byte[] fileByte = Base64.decodeBase64(fileStr);
			FileCopyUtils.copy(fileByte, uploadFile);

			// 数据库存储路径
			img_path = img_path + newFilename;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img_path;
	}

	private static String getIdcardNewName(String oldName)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String newName = format.format(new Date());
		newName += RandomStringUtils.randomAlphanumeric(5);
		String subfix = StringUtils.substringAfterLast(oldName, ".");
		newName += "." + subfix;
		return newName;
	}

	/**
	 * 包裹列表 打印面单
	 * 
	 * @param request
	 * @param package_id
	 * @return String
	 */
	@RequestMapping("/backprint")
	public String backprint(HttpServletRequest request, String package_id)
	{

		// 包裹信息
		com.xiangrui.lmp.business.admin.pkg.vo.Pkg pkgDetail = packageService.queryPackageById(Integer.parseInt(package_id));

		com.xiangrui.lmp.business.admin.store.vo.FrontUser frontUser = frontUserService.queryFrontUserById(pkgDetail.getUser_id());

		String address = "";
		// 包裹地址
		UserAddress userAddress = userAddressService.queryAddressById(pkgDetail.getAddress_id());
		if (userAddress != null)
		{
			// 地址拼接
			address = addressInfo(userAddress.getRegion()) + userAddress.getStreet();
		}

		String osaddr = "";

		// 海外仓库地址
		OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
		if (overseasAddress != null)
		{
			// 海外地址拼接
			osaddr = joinOverseasAddress(overseasAddress);
			request.setAttribute("country", overseasAddress.getCountry());
		}

		// 面单信息
		request.setAttribute("frontUser", frontUser);
		request.setAttribute("osaddr", osaddr);
		request.setAttribute("pkgDetail", pkgDetail);
		request.setAttribute("userAddress", userAddress);
		request.setAttribute("address", address);

		return "back/printExpress_non_review";
	}

	/**
	 * 海外地址拼接
	 * 
	 * @param osa
	 * @return String
	 */
	private String joinOverseasAddress(OverseasAddress osa)
	{
		String osaddr = osa.getAddress_first() + osa.getCity() + osa.getState() + osa.getCounty() + osa.getCountry();
		return osaddr;
	};

	/**
	 * 解析省市县
	 * 
	 * @param request
	 * @param jsonString
	 * @return
	 */
	private String addressInfo(String jsonString)
	{
		if (StringUtil.isEmpty(jsonString))
		{
			return "";
		}
		List<String> aList = new ArrayList<String>();
		try
		{
			aList = JSONUtil.readValueFromJson(jsonString, "name");
		} catch (Exception e)
		{
			e.printStackTrace();
			return "";
		}
		StringBuffer address = new StringBuffer("");
		for (String city : aList)
		{
			address.append(city);
		}

		return address.toString().replaceAll("\"", "");
	}

	private List<Pkg> getRealPkgByOriginalPkgList(List<Pkg> pkgList)
	{
		List<Pkg> realpkgList = new ArrayList<Pkg>();
		if (pkgList != null && pkgList.size() > 0)
		{
			for (Pkg pkg : pkgList)
			{
				Map<String, Object> subparams = new HashMap<String, Object>();
				subparams.put("package_id", pkg.getPackage_id());
				String spec = ",";

				// 查询包裹是否有合箱服务
				subparams.put("attach_id", AttachService.MERGE_PKG_ID);
				List<PkgAttachServiceGroup> mergelist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(subparams);

				// 查询包裹是否有分箱服务
				subparams.put("attach_id", AttachService.SPLIT_PKG_ID);
				List<PkgAttachServiceGroup> splitlist = pkgAttachServiceMapper.queryPkgAttachServiceGroup(subparams);

				if (mergelist != null && mergelist.size() > 0)
				{

					for (PkgAttachServiceGroup serviceGroup : mergelist)
					{
						String package_id_group = serviceGroup.getTgt_package_id_group();
						Pkg childpkg = frontPkgService.queryPackageByPackageId(Integer.parseInt(package_id_group));
						realpkgList.add(childpkg);
					}
				}
				else if (splitlist != null && splitlist.size() > 0)
				{

					for (PkgAttachServiceGroup serviceGroup : splitlist)
					{
						String package_id_group = serviceGroup.getTgt_package_id_group();
						String[] package_ids = package_id_group.split(spec);
						for (String id : package_ids)
						{
							Pkg childpkg = frontPkgService.queryPackageByPackageId(Integer.parseInt(id));
							realpkgList.add(childpkg);
						}
					}
				}
			}
		}
		return realpkgList;
	}

	@RequestMapping("/printOutput")
	public String printOutput(HttpServletRequest request, int package_id)
	{
		String seaport_idstr = StringUtils.trimToNull(request.getParameter("seaport_id"));
		String template_path = null;
		if (seaport_idstr != null)
		{
			Seaport seaport = seaportService.querySeaportBySid(Integer.parseInt(seaport_idstr));
			template_path = seaport.getOutput_print_template();
			if (template_path != null)
			{

				template_path = template_path.replaceAll("-", "/");
				// 包裹
				com.xiangrui.lmp.business.admin.pkg.vo.Pkg pkgDetail = backpackageService.queryPackageById(package_id);

				// 客户信息
				com.xiangrui.lmp.business.admin.store.vo.FrontUser frontUser = backfrontUserService.queryFrontUserById(pkgDetail.getUser_id());

				String address = "";
				// 包裹地址
				UserAddress userAddress = userAddressService.queryAddressById(pkgDetail.getAddress_id());
				if (userAddress != null)
				{
					// 地址拼接
					address = addressInfo(userAddress.getRegion()) + userAddress.getStreet();
				}

				String osaddr = "";

				// 海外仓库地址
				OverseasAddress overseasAddress = overseasAddressService.queryOverseasAddressById(pkgDetail.getOsaddr_id());
				if (overseasAddress != null)
				{
					// 海外地址拼接
					osaddr = joinOverseasAddress(overseasAddress);
					request.setAttribute("country", overseasAddress.getCountry());
				}
				// 商品
				List<com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods> pkgGoodsList = backpkgGoodsService.selectPkgGoodsByPackageId(package_id);

				// 计算总价值
				BigDecimal total_worth = new BigDecimal("0");
				for (com.xiangrui.lmp.business.admin.pkg.vo.PkgGoods pkgGoods : pkgGoodsList)
				{
					total_worth = total_worth.add(new BigDecimal(pkgGoods.getPrice()).multiply(new BigDecimal(pkgGoods.getQuantity())));
				}

				SeaportDeclaration seaportDeclaration = seaportDeclarationService.queryDeclarationBySeaportIdAndPackageId(seaport.getSid(), pkgDetail.getPackage_id());
				String declaration_code = "";
				if (seaportDeclaration != null)
				{
					declaration_code = seaportDeclaration.getDeclaration_code();
				}
				// 页面显示信息
				request.setAttribute("pkgDetail", pkgDetail);
				request.setAttribute("frontUser", frontUser);
				request.setAttribute("userAddress", userAddress);
				request.setAttribute("address", address);
				request.setAttribute("osaddr", osaddr);
				request.setAttribute("pkgGoodsList", pkgGoodsList);
				request.setAttribute("total_worth", total_worth);
				request.setAttribute("express_logo", "/theme/images/demo_express_logo.png");// 快递logo
				request.setAttribute("declaration_code", declaration_code);// 报关单号
				if( -1!=address.indexOf("北京") ){
                    request.setAttribute("channel_code", "");// 渠道号
                    request.setAttribute("channel_num", 1);// 渠道数字
                }
                else if( -1!=address.indexOf("河北") //河北省、宁夏回族自治区、青海省、天津市、西藏自治区、新疆维吾尔自治区、甘肃省
                    || -1!=address.indexOf("宁夏")
                    || -1!=address.indexOf("青海")
                    || -1!=address.indexOf("天津")
                    || -1!=address.indexOf("西藏")
                    || -1!=address.indexOf("新疆")
                    || -1!=address.indexOf("甘肃") ){
                    request.setAttribute("channel_code", "B");// 渠道号    
                    request.setAttribute("channel_num", 2);// 渠道数字
                }
                else {
                    request.setAttribute("channel_code", "J");// 渠道号
                    request.setAttribute("channel_num", 3);// 渠道数字
                }
			}
			else
			{
				template_path = "back/error_output_print";
			}
		}
		return template_path;
	}

	@RequestMapping(value = "/free")
	public String getFres(HttpServletRequest httpRequest)
	{
		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/free");
		return "/homepage/free";
	}
	
	
	@RequestMapping(value = "/consultShow")
	public String getConsultShow(HttpServletRequest req, HttpSession httpSession)
	{
		String consultId=StringUtils.trimToNull(req.getParameter("consult_id"));
		 List<ConsultInfo> list=consultInfoService.queryAllAvaliable();
		 ConsultInfo consultInfo=null;
		if (consultId != null)
		{
			consultInfo = consultInfoService.queryById(Integer.parseInt(consultId));
		}
		int preId = -1;
		int netId = -1;
		if (list != null && list.size() > 0)
		{
			int size = list.size();
			if (consultInfo == null)
			{
				consultInfo=list.get(0);
				if (size > 1)
				{
					netId=list.get(1).getSeq_id();
				}
			}else
			{
				
				for(int i=0;i<size;i++)
				{
					if(list.get(i).getSeq_id()==consultInfo.getSeq_id())
					{
						if(i==0)
						{
							if(size>1)
							{
								netId=list.get(1).getSeq_id();
							}
						}else
						{
							preId = list.get(i-1).getSeq_id();
							if((i+1)<size)
							{
								netId=list.get(i+1).getSeq_id();
							}
						}
					}
				}
			}
		}
		req.setAttribute("preId", preId);
		req.setAttribute("netId", netId);
		req.setAttribute("consultInfo", consultInfo);
		return "/homepage/consultShow";
	}
	
	/**
	 * 公告新闻列表
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "/noticeInfoList")
	public String noticeInfoList(HttpServletRequest httpRequest)
	{
		
		String noticeId = httpRequest.getParameter("noticeId");
		httpRequest.setAttribute("noticeId", noticeId);
		
		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/noticeInfoList");

		// 公告信息
		httpRequest.setAttribute("noticeInfoList", noticeService.queryAllHomepage6());

		// 底部数据
		setFooterRequestAttribute(httpRequest);

		return "/homepage/noticeInfoList";
	}
	
	/**
	 * 首页公告新闻列表
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "/noticeInfoListIndex")
	@ResponseBody
	public List<Notice> noticeInfoListIndex(HttpServletRequest httpRequest) {
		// 公告信息
		List<Notice> result = noticeService.queryAllHomepage6();
		return result;
	}
	
	@RequestMapping(value = "/fax_query")
	public String faxQuery(HttpServletRequest httpRequest)
	{
		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/fax_query");
		return "/homepage/fax_query";
	}
	
	@RequestMapping(value = "/package_require")
	public String packageRequire(HttpServletRequest httpRequest)
	{
		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/package_require");
		return "/homepage/package_require";
	}

	/**
	 * 验证该地址人名与身份证名是否对应
	 * 暂时不管数据库中的身份证审核状态，只用阿里接口判断
	 * @param httpRequest
	 * @param addressId
	 * @return
	 */
	@RequestMapping(value = "/checkIdCard")
	@ResponseBody
    public String checkIdCard(HttpServletRequest httpRequest, Integer addressId)
    {
	    FrontReceiveAddress address = frontReceiveAddressService.queryReceiveAddressByAddressId(addressId);
	    if( null==address )
	        return "false";
	    
	    if( !ValidatorUtil.isIdCardNo(address.getIdcard()) )
	        return "false";
	    
	    FrontIdcard fIdcard = frontIdcardService.queryByIdcard(address.getIdcard(),address.getReceiver());
	    if( null==fIdcard ) return "false";
	    
	    return "true";
    }
	
	/**
	 * 获取首页左侧菜单及其相应的内容
	 * @param httpRequest
	 * @return
	 */
	@RequestMapping(value = "/queryLeftIndexPage")
	public String queryLeftIndexPage(HttpServletRequest httpRequest){
		// 头部信息
		setHeadRequestAttribute(httpRequest, "/homepage/leftIndexPage");
		
		ServletContext application = httpRequest.getSession().getServletContext();
		if (null == application.getAttribute("artClassList")){
			application.setAttribute("artClassList", articleClassifyService.queryArticleClassifyLevelNoParent());
			application.setAttribute("artClassListParent", articleClassifyService.queryArticleClassifyLevel());
		}
		if(null == application.getAttribute("artList")){
			application.setAttribute("artList", articleService.queryAll(null));
		}
		
		if(null == application.getAttribute("prohibitionLists")){
			application.setAttribute("prohibitionLists", prohibitionService.queryAll(null));
		}
		
		return "/homepage/leftIndexPage";
	}
}
