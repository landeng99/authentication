package com.xiangrui.lmp.business.homepage.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.xiangrui.lmp.business.base.BaseController;
@Controller
@RequestMapping("/testPage")
public class TestController extends BaseController
{

    /**
     * 前台批量导入包裹数据--点击保存触发的接口
     * 
     * @param req
     * @param resp
     * @return
     */
    @RequestMapping(value = "/leadPkg")
    public String getLogin(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/leadingPkg";
    }
    @RequestMapping(value = "/index")
    public String index(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/index";
    }
    @RequestMapping(value = "/newPpackage")
    public String newPpackage(HttpServletRequest httpRequest)
    {

        return "/homepage/new-package";
    }
    @RequestMapping(value = "/myPackage")
    public String myPackage(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/mypackage";
    }
    @RequestMapping(value = "/overeaWare")
    public String overeaWare(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/overeaWare";
    }
    @RequestMapping(value = "/address")
    public String address(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/address";
    }
    @RequestMapping(value = "/record")
    public String record(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/record";
    }
    @RequestMapping(value = "/account")
    public String account(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/account";
    }
    @RequestMapping(value = "/coupon")
    public String coupon(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/coupon";
    }
    @RequestMapping(value = "/recommand")
    public String recommand(HttpServletRequest httpRequest)
    {

        return "/homepage/frt/recommand";
    }
    @RequestMapping(value = "/importpkg")
    public String importpkg(HttpServletRequest httpRequest)
    {

        return "/homepage/import_pkg";
    }
}
