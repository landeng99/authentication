package com.xiangrui.lmp.business.homepage.controller;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.CouponUsed;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.homepage.service.FrontCouponUsedService;
import com.xiangrui.lmp.business.homepage.service.FrontUserCouponService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.FrontUserCoupon;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping(value = "/account")
public class AccountController
{

	/**
	 * 手机号1：不存在
	 */
	private static final String MOBILE_NOTEXIST = "1";

	/**
	 * 手机号0：存在
	 */
	private static final String MOBILE_EXIST = "0";

	/**
	 * 优惠券1：可使用
	 */
	private static final int STATUS_AVAILABLE = 1;

	/**
	 * 优惠券1：已过期
	 */
	private static final int STATUS_OVERDUE = 2;

	/**
	 * 包裹状态：可用
	 */
	private static final int PKG_ENABLE = 1;

	@Autowired
	private FrontUserCouponService frontUserCouponService;

	@Autowired
	private FrontCouponUsedService frontCouponUsedService;

	@Autowired
	private FrontUserService frontUserService;

	@Autowired
	private PkgService frontPkgService;
	/**
	 * 优惠券处理服务
	 */
	@Autowired
	private CouponService couponService;

	/**
	 * 我的账户
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/accountInit")
	public String accountInit(HttpServletRequest request)
	{
		// 没有登录用户
		if (null == request.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}

		try
		{
			int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();
			
			// 包裹总数
//			request.setAttribute("pkgCount", frontPkgService.queryCountByUserId(user_id));
//	        // 可用优惠券数
//			request.setAttribute("countAvailable", couponService.sumAvaliableQuantityByUserId(user_id));

			FrontUser frontUser = frontUserService.queryFrontUserByUserId(user_id);
			// 根据user_id找到用户各种包裹状态的数据
			int[] cAry = getTransportSum(frontUser.getUser_id());
			request.setAttribute("c1", cAry[0]);
			request.setAttribute("c2", cAry[1]);
			request.setAttribute("c3", cAry[2]);
			request.setAttribute("c4", cAry[3]);
			request.setAttribute("c5", cAry[4]);
			request.setAttribute("c6", cAry[5]);
			request.setAttribute("frontUser", frontUser);
			// 有新的待处理包裹
			Map<String, Object> tparams = new HashMap<String, Object>();
			tparams.put("user_id", frontUser.getUser_id());
			tparams.put("status", 0);
			tparams.put("enable", PKG_ENABLE);
			List<Pkg> needToHandlepkgList = frontPkgService.queryNeedToHandlePackage(tparams);
			int neetToHandleCount = 0;
			if (needToHandlepkgList != null && needToHandlepkgList.size() > 0)
			{
				neetToHandleCount = needToHandlepkgList.size();
				request.setAttribute("neetToHandleCount", neetToHandleCount);
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return "/homepage/account";
	}

	/**
	 * 我的账户
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/couponInit")
	public String couponInit(HttpServletRequest request)
	{

		try
		{
			// 没有登录用户
			if (null == request.getSession().getAttribute("frontUser"))
			{
				return "/homepage/login";
			}
			int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

			FrontUser frontUser = frontUserService.queryFrontUserByUserId(user_id);

			Map<String, Object> params = new HashMap<String, Object>();

			params.put("user_id", user_id);

/*			// 所有优惠券统计
			int countAll = frontUserCouponService.sumQuantityByUserId(params) + frontCouponUsedService.sumQuantityByUserId(user_id);

			params.put("status", STATUS_AVAILABLE);
			params.put("time", new Timestamp(System.currentTimeMillis()));
			// 可用优惠券统计
			int countAvailable = frontUserCouponService.sumQuantityByUserId(params);
			// 优惠券 可用
			List<FrontUserCoupon> frontUserCouponList = frontUserCouponService.selectUserCouponByUserId(params);*/
			
			List<UserCoupon> avliableCouponList=couponService.queryAvliableCouponByUserId(params);
			List<CouponUsed> usedCouponList=couponService.queryUsedCouponByUserId(params);
			List<UserCoupon> expiredCouponList=couponService.queryExpiredCouponByUserId(params);

			int countAvailable=couponService.sumAvaliableQuantityByUserId(user_id);
			int countAll=couponService.sumQuantityByUserId(user_id);
			
			// 根据user_id找到用户各种包裹状态的数据
			int[] cAry = getTransportSum(frontUser.getUser_id());
			request.setAttribute("c1", cAry[0]);
			request.setAttribute("c2", cAry[1]);
			request.setAttribute("c3", cAry[2]);
			request.setAttribute("c4", cAry[3]);
			request.setAttribute("c5", cAry[4]);
			request.setAttribute("c6", cAry[5]);
			request.setAttribute("frontUser", frontUser);
			request.setAttribute("countAvailable", countAvailable);
			request.setAttribute("countAll", countAll);
			request.setAttribute("avliableCouponList", avliableCouponList);
			request.setAttribute("usedCouponList", usedCouponList);
			request.setAttribute("expiredCouponList", expiredCouponList);
			// 有新的待处理包裹
			Map<String, Object> tparams = new HashMap<String, Object>();
			tparams.put("user_id", frontUser.getUser_id());
			tparams.put("status", 0);
			tparams.put("enable", PKG_ENABLE);
			List<Pkg> needToHandlepkgList = frontPkgService.queryNeedToHandlePackage(tparams);
			int neetToHandleCount = 0;
			if (needToHandlepkgList != null && needToHandlepkgList.size() > 0)
			{
				neetToHandleCount = needToHandlepkgList.size();
				request.setAttribute("neetToHandleCount", neetToHandleCount);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return "/homepage/coupon";
	}

	/**
	 * 优惠券
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/coupon")
	@ResponseBody
	public List<FrontUserCoupon> coupon(HttpServletRequest request, String id)
	{
		FrontUser frontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("user_id", frontUser.getUser_id());
		params.put("time", new Timestamp(System.currentTimeMillis()));
		List<FrontUserCoupon> frontUserCouponList = new ArrayList<FrontUserCoupon>();
		// 优惠券 可用优惠券
		if ("one".equals(id))
		{
			params.put("status", STATUS_AVAILABLE);
			frontUserCouponList = frontUserCouponService.selectUserCouponByUserId(params);
			// 已使用优惠券
		}
		else if ("two".equals(id))
		{
			frontUserCouponList = frontCouponUsedService.selectCouponUsedByUserId(frontUser.getUser_id());

			// 已过期优惠券
		}
		else
		{

			params.put("status", STATUS_OVERDUE);
			frontUserCouponList = frontUserCouponService.selectUserCouponByUserId(params);
		}

		return frontUserCouponList;
	}
/*	*//**
	 * 可使用优惠券
	 * @param httpRequest
	 *            request
	 * @return String
	 *//*
	@RequestMapping(value = "/avliableCoupon")
	@ResponseBody
	public List<UserCoupon> avliableCoupon(HttpServletRequest request, String id)
	{
		FrontUser frontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", frontUser.getUser_id());
		List<UserCoupon> avliableCoupon=couponService.queryAvliableCouponByUserId(params);
		return avliableCoupon;
	}
	
	*//**
	 * 已使用优惠券
	 * @param httpRequest
	 *            request
	 * @return String
	 *//*
	@RequestMapping(value = "/usedCoupon")
	@ResponseBody
	public List<FrontCouponUsed> usedCoupon(HttpServletRequest request, String id)
	{
		FrontUser frontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", frontUser.getUser_id());
		List<FrontCouponUsed> usedCouponList=couponService.queryUsedCouponByUserId(params);
		return usedCouponList;
	}
	*//**
	 * 已过期优惠券
	 * @param httpRequest
	 *            request
	 * @return String
	 *//*
	@RequestMapping(value = "/expiredCoupon")
	@ResponseBody
	public List<UserCoupon> expiredCoupon(HttpServletRequest request, String id)
	{
		FrontUser frontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", frontUser.getUser_id());
		List<UserCoupon> expiredCouponList=couponService.queryExpiredCouponByUserId(params);
		return expiredCouponList;
	}*/
	/**
	 * 修改密码
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/editPasswordInit")
	public String editPasswordInit(HttpServletRequest request)
	{

		return "/homepage/editPassword";
	}

	/**
	 * 保存密码
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/savePassword")
	@ResponseBody
	public Map<String, Object> savePassword(HttpServletRequest request, String old_pwd, String user_pwd)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		FrontUser loginFrontUser = (FrontUser) request.getSession().getAttribute("frontUser");
		loginFrontUser.setUser_pwd(old_pwd);
		// 旧密码验证
		FrontUser frontUser = frontUserService.frontUserLogin(loginFrontUser);

		if (frontUser == null || StringUtil.isEmpty(frontUser.getEmail()))
		{
			result.put("result", false);
			result.put("message", "旧密码输入有误！");
			return result;
		}
		loginFrontUser.setUser_pwd(user_pwd);
		int count = frontUserService.updateUserPwd(loginFrontUser);

		if (count > 0)
		{
			result.put("result", true);
			result.put("message", "修改成功！");
		}
		else
		{
			result.put("result", false);
			result.put("message", "保存失败！");
		}

		return result;
	}

	/**
	 * 修改姓名
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/editUserNameInit")
	public String editUserNameInit(HttpServletRequest request)
	{

		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(user_id);

		request.setAttribute("old_user_name", frontUser.getUser_name());

		return "/homepage/editUserName";
	}

	/**
	 * 修改手机号重复
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/checkUserName")
	@ResponseBody
	public String checkUserName(HttpServletRequest request, String user_name)
	{
		String checkResult = MOBILE_EXIST;

		FrontUser frontUser = frontUserService.queryFrontUserByUserName(user_name);

		if (frontUser == null)
		{

			checkResult = MOBILE_NOTEXIST;
		}

		return checkResult;
	}

	/**
	 * 保存姓名
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/saveUserName")
	@ResponseBody
	public Map<String, Object> saveUserName(HttpServletRequest request, String user_name)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		FrontUser loginFrontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		loginFrontUser.setUser_name(user_name);

		int count = frontUserService.updateUserName(loginFrontUser);

		if (count > 0)
		{
			result.put("result", true);
			result.put("message", "修改成功！");
		}
		else
		{
			result.put("result", false);
			result.put("message", "保存失败！");
		}

		return result;
	}

	/**
	 * 修改手机号
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/editMobileInit")
	public String editMobileInit(HttpServletRequest request)
	{
		// 没有登录用户
		if (null == request.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(user_id);

		request.setAttribute("old_mobile", frontUser.getMobile());

		return "/homepage/editMobile";
	}

	/**
	 * 修改手机号重复
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/checkMobile")
	@ResponseBody
	public String checkMobile(HttpServletRequest request, String mobile)
	{
		String checkResult = MOBILE_EXIST;

		FrontUser frontUser = frontUserService.queryFrontUserByMobile(mobile);

		if (frontUser == null)
		{

			checkResult = MOBILE_NOTEXIST;
		}

		return checkResult;
	}

	/**
	 * 保存手机号
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/saveMobile")
	@ResponseBody
	public Map<String, Object> saveMobile(HttpServletRequest request, String mobile, String code)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		FrontUser loginFrontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		// 手机验证码
		String smsCode = (String) request.getSession().getAttribute("smsCode");

		if (StringUtil.isEmpty(smsCode) || !smsCode.endsWith(code))
		{

			result.put("result", false);
			result.put("message", "验证码错误！");
			return result;
		}

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(loginFrontUser.getUser_id());
		frontUser.setMobile(mobile);

		int count = frontUserService.updateMobile(frontUser);

		if (count > 0)
		{
			result.put("result", true);
			result.put("message", "修改成功！");
		}
		else
		{
			result.put("result", false);
			result.put("message", "保存失败！");
		}

		return result;
	}

	/**
	 * 根据user_id找到用户各种包裹状态的数据
	 * 
	 * @param user_id
	 * @return int[]数组,总长度为6,6个值,分别是待入库数,已入库数,未支付数,支付数,未打印数，打印数
	 */
	public int[] getTransportSum(int user_id)
	{
		int[] array = new int[] { 0, 0, 0, 0, 0, 0 };
		// 待入库状态
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", user_id);
		params.put("enable", PKG_ENABLE);
		params.put("status", BasePackage.LOGISTICS_STORE_WAITING);
		List<Pkg> pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[0] = pkgList.size();

		// 已入库状态
		params.put("status", BasePackage.LOGISTICS_STORAGED);
		pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[1] = pkgList.size();

		params.remove("status");
		// 未支付状态
//		params.put("payStatus", BasePackage.PAYMENT_UNPAID);
//		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
		params.put("payStatusFreight", BasePackage.PAYMENT_FREIGHT_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatusFreight(params);
		array[2] = pkgList.size();
		params.put("payStatusCustom", BasePackage.PAYMENT_CUSTOM_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatusCustom(params);
		array[2] += pkgList.size();

		// 已支付状态
//		params.put("payStatus", BasePackage.PAYMENT_PAID);
//		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
//		array[3] = pkgList.size();
		pkgList = frontPkgService.queryPackageByUserIdAndPaid(params);
		array[3] = pkgList.size();

		params.remove("payStatus");
		// 未打印状态
		params.put("pkgPrint", BasePackage.PACKAGE__NO_PRINT);
		pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		array[4] = pkgList.size();

		// 打印状态
		params.put("pkgPrint", BasePackage.PACKAGE__YES_PRINT);
		pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		array[5] = pkgList.size();

		return array;
	}

	/**
	 * 跳转绑定银行卡
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/bankBindInit")
	public String bankBindInit(HttpServletRequest request)
	{
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(user_id);

		request.setAttribute("old_mobile", frontUser.getMobile());

		return "/homepage/bankBind";
	}

	/**
	 * 绑定银行卡
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/bankBind")
	@ResponseBody
	public Map<String, Object> bankBind(HttpServletRequest request, String bank_card, String code, String account_holder, String bank_account)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		FrontUser loginFrontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		// 手机验证码
		String smsCode = (String) request.getSession().getAttribute("smsCode");

		if (StringUtil.isEmpty(smsCode) || !smsCode.endsWith(code))
		{

			result.put("result", false);
			result.put("message", "验证码错误！");
			return result;
		}

		loginFrontUser.setBank_card(bank_card);
		loginFrontUser.setAccount_holder(account_holder);
		loginFrontUser.setBank_account(bank_account);
		// 绑定银行卡
		int count = frontUserService.updateBank(loginFrontUser);

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(loginFrontUser.getUser_id());

		if (count > 0)
		{
			result.put("result", true);
			result.put("message", "绑定成功！");
			result.put("bank_card", frontUser.getBank_card());
			result.put("account_holder", frontUser.getAccount_holder());
			result.put("bank_account", frontUser.getBank_account());
		}
		else
		{
			result.put("result", false);
			result.put("message", "绑定失败！");
		}

		return result;
	}

	/**
	 * 跳转绑定支付宝
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/payBindInit")
	public String payBindInit(HttpServletRequest request)
	{
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(user_id);

		request.setAttribute("old_mobile", frontUser.getMobile());

		return "/homepage/payBind";
	}

	
	/**
	 * 绑定支付宝
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/payBind")
	@ResponseBody
	public Map<String, Object> payBind(HttpServletRequest request, String pay_treasure, String code, String alipayName)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		FrontUser loginFrontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		// 手机验证码
		String smsCode = (String) request.getSession().getAttribute("smsCode");

		if (StringUtil.isEmpty(smsCode) || !smsCode.endsWith(code))
		{

			result.put("result", false);
			result.put("message", "验证码错误！");
			return result;
		}

		loginFrontUser.setPay_treasure(pay_treasure);
		loginFrontUser.setAlipayName(alipayName);
		// 绑定支付宝
		int count = frontUserService.updatePay(loginFrontUser);

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(loginFrontUser.getUser_id());

		if (count > 0)
		{
			result.put("result", true);
			result.put("message", "绑定成功！");
			result.put("alipayName", frontUser.getAlipayName());
			result.put("pay_treasure", frontUser.getPay_treasure());
		}
		else
		{
			result.put("result", false);
			result.put("message", "绑定失败！");
		}

		return result;
	}

	/**
	 * 保存手机号
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/modifyDefaultPayType")
	@ResponseBody
	public Map<String, Object> modifyDefaultPayType(HttpServletRequest request, int defaultPayType)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		FrontUser loginFrontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(loginFrontUser.getUser_id());
		frontUser.setDefault_pay_type(defaultPayType);

		int count = frontUserService.updateDefaultPayType(frontUser);

		if (count > 0)
		{
			result.put("result", true);
			result.put("message", "修改成功！");
		}
		else
		{
			result.put("result", false);
			result.put("message", "保存失败！");
		}

		return result;
	}

	/**
	 * 设置虚拟预付款
	 * @param request
	 * @param defaultPayType
	 * @return
	 */
	@RequestMapping(value = "/modifyVirtualPayFlag")
	@ResponseBody
	public Map<String, Object> modifyVirtualPayFlag(HttpServletRequest request, String virtual_pay_flag)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		FrontUser loginFrontUser = (FrontUser) request.getSession().getAttribute("frontUser");

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(loginFrontUser.getUser_id());
		frontUser.setVirtual_pay_flag(virtual_pay_flag);

		int count = frontUserService.updateVirtualPayFlag(frontUser);
		if("Y".equals(virtual_pay_flag))
		{
			frontUser.setDefault_pay_type(1);//设置为快速支付
			frontUserService.updateDefaultPayType(frontUser);
		}

		if (count > 0)
		{
			result.put("result", true);
			result.put("message", "修改成功！");
		}
		else
		{
			result.put("result", false);
			result.put("message", "保存失败！");
		}

		return result;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/recommand")
	public String recommand(HttpServletRequest request)
	{
		// 没有登录用户
		if (null == request.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}
		try
		{
			int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();
			FrontUser frontUser = frontUserService.queryFrontUserByUserId(user_id);

			if(frontUser!=null)
			{
				String person_qr_code=StringUtils.trimToNull(frontUser.getPerson_qr_code());
				if(person_qr_code==null)
				{
					String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource" + File.separator + "upload" + File.separator + "person_qr_code" + File.separator;
					// 生成推荐信息（二维码和推荐链接）
					frontUser=frontUserService.createRecommandInfo(frontUser,ctxPath);
				}
				//个人的邀请用户列表
				List<FrontUser> myInviteUserList=frontUserService.queryMyInviteUser(frontUser.getUser_id());
				request.setAttribute("myInviteUserList", myInviteUserList);
				//获得的推荐优惠券
				List<UserCoupon> recommandCouponList=couponService.queryUserRecommandUserCoupon(frontUser.getUser_id());
				request.setAttribute("recommandCouponList", recommandCouponList);
				//推广详情
				List<FrontUser> pushDetailList = this.frontUserService.queryPushById(frontUser.getUser_id());
				request.setAttribute("pushDetailList", pushDetailList);
			}
			// 根据user_id找到用户各种包裹状态的数据
			int[] cAry = getTransportSum(frontUser.getUser_id());
			request.setAttribute("c1", cAry[0]);
			request.setAttribute("c2", cAry[1]);
			request.setAttribute("c3", cAry[2]);
			request.setAttribute("c4", cAry[3]);
			request.setAttribute("c5", cAry[4]);
			request.setAttribute("c6", cAry[5]);
			request.setAttribute("frontUser", frontUser);
			// 有新的待处理包裹
			Map<String, Object> tparams = new HashMap<String, Object>();
			tparams.put("user_id", frontUser.getUser_id());
			tparams.put("status", 0);
			tparams.put("enable", PKG_ENABLE);
			List<Pkg> needToHandlepkgList = frontPkgService.queryNeedToHandlePackage(tparams);
			int neetToHandleCount = 0;
			if (needToHandlepkgList != null && needToHandlepkgList.size() > 0)
			{
				neetToHandleCount = needToHandlepkgList.size();
				request.setAttribute("neetToHandleCount", neetToHandleCount);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return "/homepage/recommand";
	}
	/**
	 * 打开新的支付窗口
	 * 
	 * @param httpRequest
	 *            request
	 * @return String
	 */
	@RequestMapping(value = "/torecharge")
	public String torecharge(HttpServletRequest request)
	{
		// 没有登录用户
		if (null == request.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = frontUserService.queryFrontUserByUserId(user_id);
 
		request.setAttribute("old_mobile", frontUser.getMobile());
		request.setAttribute("frontUser", frontUser);
		return "/homepage/recharge_new";
	}
}
