package com.xiangrui.lmp.business.homepage.controller;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.accountlog.service.AccountLogService;
import com.xiangrui.lmp.business.admin.accountlog.vo.AccountLog;
import com.xiangrui.lmp.business.admin.attachService.service.AttachServiceService;
import com.xiangrui.lmp.business.admin.cost.service.MemberBillService;
import com.xiangrui.lmp.business.admin.cost.vo.MemberBillModel;
import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.CouponUsed;
import com.xiangrui.lmp.business.admin.sysSetting.service.SysSettingService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.homepage.service.FrontAccountLogService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.FrontWithdrawalLogService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.FrontWithdrawalLog;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.util.CommonUtil;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.alipay.controller.AlipayController;

@Controller
@RequestMapping(value = "/member")
public class MemberController
{

    /**
     * 包裹状态：可用
     */
    private static final int PKG_ENABLE = 1;

    @Autowired
    private FrontUserService memberService;

    @Autowired
    private FrontAccountLogService frontAccountLogService;

    @Autowired
    private FrontWithdrawalLogService frontWithdrawalLogService;

    @Autowired
    private PkgService frontPkgService;
	/**
	 * 优惠券处理服务
	 */
	@Autowired
	private CouponService couponService;

	/**
	 * 时间格式化
	 */
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * 消费记录
	 */
	@Autowired
	private AccountLogService accountLogService;
	/**
	 * 增值服务管理
	 */
	@Autowired
	private AttachServiceService attachServiceService;
	/**
	 * 同行帐单详情管理
	 */
	@Autowired
	private MemberBillService memberBillService;
	/**
	 * 申请人的信息管理
	 */
	@Autowired
	private com.xiangrui.lmp.business.admin.store.service.FrontUserService frontUserService;

	/**
	 * 包裹信息管理
	 */
	@Autowired
	private com.xiangrui.lmp.business.admin.pkg.service.PackageService packageService;
	
	@Autowired
	private SysSettingService sysSettingService;
    /**
     * 用户账户状态
     * 
     * @param req
     */
    @RequestMapping(value = "/frontUserStatus")
    @ResponseBody
    public Map<String, Object> checkStatus(HttpServletRequest req)
    {

        Map<String, Object> rtnMap = new HashMap<String, Object>();
        int user_id = ((FrontUser) req.getSession().getAttribute("frontUser"))
                .getUser_id();

        FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

        if (FrontUser.FRONT_USER_STATUS_NO_ACTIVATION == frontUser.getStatus())
        {

            rtnMap.put("message", "该账户未激活！");
            rtnMap.put("result", "0");
            return rtnMap;

        } else if (FrontUser.FRONT_USER_STATUS_DISABLE == frontUser.getStatus())
        {

            rtnMap.put("message", "该账户已被禁用！");
            rtnMap.put("result", "2");
            return rtnMap;
        }

        rtnMap.put("result", "1");
        return rtnMap;
    }

    /**
     * 跳转到我的消费记录
     * 
     * @param httpRequest
     *            req
     * @return String
     */
    @RequestMapping(value = "/record")
    public String record(HttpServletRequest req,String flag)
    {
        // 没有登录用户
        if (null == req.getSession().getAttribute("frontUser"))
        {
            return "/homepage/login";
        }

        int user_id = ((FrontUser) req.getSession().getAttribute("frontUser"))
                .getUser_id();

        FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

        // 时间格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        // 当前时间
        Calendar time = Calendar.getInstance();
        //  计算时间
        time.add(Calendar.YEAR, -2);

        Map<String, Object> paramRecharge = new HashMap<String, Object>();
        // 会员ID
        paramRecharge.put("user_id", frontUser.getUser_id());
        // 消费类型
        paramRecharge
                .put("account_type", FrontAccountLog.ACCOUNT_TYPE_RECHARGE);

        // 充值完成
        paramRecharge.put("status", FrontAccountLog.SUCCESS);

        paramRecharge.put("opr_time", simpleDateFormat.format(time.getTime()));
        // 充值
        List<FrontAccountLog> frontAccountLogListRecharge = frontAccountLogService
                .queryAccountLog(paramRecharge);

        Map<String, Object> paramConsume = new HashMap<String, Object>();
        // 会员ID
        paramConsume.put("user_id", frontUser.getUser_id());
        // 消费类型
        paramConsume.put("account_type", FrontAccountLog.ACCOUNT_TYPE_CONSUME);
        // 完成消费
        paramConsume.put("status", FrontAccountLog.SUCCESS);

        paramConsume.put("opr_time", simpleDateFormat.format(time.getTime()));
        // 消费
        List<FrontAccountLog> frontAccountLogListConsume = frontAccountLogService
                .queryAccountLog(paramConsume);
        
        if(frontAccountLogListConsume!=null&&frontAccountLogListConsume.size()>0)
        {
        	for(FrontAccountLog frontAccountLog:frontAccountLogListConsume)
        	{
				CouponUsed couponUsed=couponService.queryCouponUsedByOrder_code(frontAccountLog.getLogistics_code());
				float actualPay=NumberUtils.scaleMoneyDataFloat(frontAccountLog.getAmount());
				if(couponUsed!=null)
				{
					actualPay=NumberUtils.scaleMoneyDataFloat(frontAccountLog.getAmount()-(float)couponUsed.getDenomination());
				}
				frontAccountLog.setAmount(actualPay);
        	}
        }

        Map<String, Object> paramWithdraw = new HashMap<String, Object>();
        // 会员ID
        paramWithdraw.put("user_id", frontUser.getUser_id());
        // 消费类型
        paramWithdraw
                .put("account_type", FrontAccountLog.ACCOUNT_TYPE_WITHDRAW);
        // 完成提现 完成未完成

        paramWithdraw.put("opr_time", simpleDateFormat.format(time.getTime()));
        // 提现
        List<FrontAccountLog> frontAccountLogListWithdraw = frontAccountLogService
                .queryAccountLog(paramWithdraw);

        Map<String, Object> paramCompensation = new HashMap<String, Object>();
        // 会员ID
        paramCompensation.put("user_id", frontUser.getUser_id());
        // 消费类型
        paramCompensation.put("account_type",
                FrontAccountLog.ACCOUNT_TYPE_COMPENSATION);

        paramCompensation.put("opr_time",
                simpleDateFormat.format(time.getTime()));
        // 索赔
        List<FrontAccountLog> frontAccountLogListCompensation = frontAccountLogService
                .queryAccountLog(paramCompensation);

        Map<String, Object> param4Amount = new HashMap<String, Object>();
        // 当前时间
        Calendar timeB = Calendar.getInstance();
        // 计算时间
        timeB.add(Calendar.YEAR, -1);

        // 消费类型
        param4Amount.put("account_type", FrontAccountLog.ACCOUNT_TYPE_CONSUME);
        // 会员ID
        param4Amount.put("user_id", frontUser.getUser_id());
        // 完成消费
        param4Amount.put("status", FrontAccountLog.SUCCESS);
        // 时间
        param4Amount.put("opr_time", timeB.get(Calendar.YEAR));

        // 近一年实际消费额
        float sumAmount = frontAccountLogService.sumAmountByYear(param4Amount);

        frontUser = memberService
                .queryFrontUserByUserId(frontUser.getUser_id());
        // 根据user_id找到用户各种包裹状态的数据
        int[] cAry = getTransportSum(frontUser.getUser_id());
        req.setAttribute("frontAccountLogListRecharge",
                frontAccountLogListRecharge);
        req.setAttribute("frontAccountLogListConsume",
                frontAccountLogListConsume);
        req.setAttribute("frontAccountLogListWithdraw",
                frontAccountLogListWithdraw);
        req.setAttribute("frontAccountLogListCompensation",
                frontAccountLogListCompensation);
        req.setAttribute("sumAmount", sumAmount);
        req.setAttribute("frontUser", frontUser);
        req.setAttribute("c1", cAry[0]);
        req.setAttribute("c2", cAry[1]);
        req.setAttribute("c3", cAry[2]);
        req.setAttribute("c4", cAry[3]);
        req.setAttribute("c5", cAry[4]);
        req.setAttribute("c6", cAry[5]);
        req.setAttribute("flag", flag);
		//有新的待处理包裹
		Map<String, Object> tparams = new HashMap<String, Object>();
		tparams.put("user_id", frontUser.getUser_id());
		tparams.put("status", 0);
		tparams.put("enable", PKG_ENABLE);
		List<Pkg> needToHandlepkgList = frontPkgService.queryNeedToHandlePackage(tparams);
		int neetToHandleCount=0;
		if(needToHandlepkgList!=null&&needToHandlepkgList.size()>0)
		{
			neetToHandleCount=needToHandlepkgList.size();
			req.setAttribute("neetToHandleCount", neetToHandleCount);
		}
        return "/homepage/record";

    }

    /**
     * 消费明细检索
     * 
     * @param httpRequest
     *            req
     * @param id
     * @return List
     */
    @RequestMapping(value = "/allConsumption")
    @ResponseBody
    public List<FrontAccountLog> allConsumption(HttpServletRequest req,
            String id)
    {

        int user_id = ((FrontUser) req.getSession().getAttribute("frontUser"))
                .getUser_id();
        // 时间格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        // 当前
        Calendar rightNow = Calendar.getInstance();
        // 计算三个月前的日期
        rightNow.add(Calendar.MONTH, -3);

        Map<String, Object> params = new HashMap<String, Object>();
        // 会员ID
        params.put("user_id", user_id);
        // 近三个月消费明细
        List<FrontAccountLog> frontAccountLogList = new ArrayList<FrontAccountLog>();
        // 近三个月数据
        if ("one".equals(id))
        {
            // 时间带 近三个月
            // params.put("opr_time",
            // simpleDateFormat.format(rightNow.getTime()));
            // frontAccountLogList = frontAccountLogService
            // .queryAccountLogNearly(params);
            // 三个月前数据
        } else
        {
            // 当前时间
            Calendar calendar = Calendar.getInstance();
            // 两年之内 TODO 怎么理解这个词
            calendar.add(Calendar.YEAR, -1);

            params.put("fromTime", calendar.get(Calendar.YEAR));
            params.put("toTime", simpleDateFormat.format(rightNow.getTime()));
            frontAccountLogList = frontAccountLogService
                    .queryAccountLogBefore(params);
        }
        
//        if(frontAccountLogList!=null&&frontAccountLogList.size()>0)
//        {
//        	for(FrontAccountLog frontAccountLog:frontAccountLogList)
//        	{
//				CouponUsed couponUsed=couponService.queryCouponUsedByOrder_code(frontAccountLog.getLogistics_code());
//				float actualPay=NumberUtils.scaleMoneyDataFloat(frontAccountLog.getAmount());
//				if(couponUsed!=null)
//				{
//					actualPay=NumberUtils.scaleMoneyDataFloat(frontAccountLog.getAmount()-(float)couponUsed.getDenomination());
//				}
//				frontAccountLog.setAmount(actualPay);
//        	}
//        }

        return frontAccountLogList;
    }

    /**
     * 充值页面初始化
     * 
     * @param httpRequest
     *            req
     * @return String
     */
    @RequestMapping(value = "/rechargeInit")
    public String rechargeInit(HttpServletRequest request)
    {
        // 没有登录用户
        if (null == request.getSession().getAttribute("frontUser"))
        {
            return "/homepage/login";
        }
        
        int user_id = ((FrontUser) request.getSession().getAttribute(
                "frontUser")).getUser_id();

        FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

        request.setAttribute("frontUser", frontUser);

        return "/homepage/recharge";

    }

    /**
     * 充值
     * 
     * @param request
     * @return String
     */
    @RequestMapping(value = "/recharge")
    public String recharge(HttpServletRequest request,
            HttpServletResponse response, String rechargeAmount, String bankCode)
    {
        // 没有登录用户
        if (null == request.getSession().getAttribute("frontUser"))
        {
            return "/homepage/login";
        }
        
        int user_id = ((FrontUser) request.getSession().getAttribute(
                "frontUser")).getUser_id();

        // 必填 商户订单号
        String out_trade_no = CommonUtil.getCurrentDateNum();

        insertFrontAccountLog(out_trade_no, rechargeAmount, user_id, "从"
                + CommonUtil.getBankName(bankCode) + "充值");

        if ("ALIPAY".equals(bankCode))
        {
            AlipayController.alipay(request, response, out_trade_no,
                    rechargeAmount, out_trade_no, "", "",
                    SYSConstant.NOTIFY_URL_RECHARGE,
                    SYSConstant.RETURN_URL_RECHARGE);
        } else
        {
            AlipayController.bank(request, response, out_trade_no,
                    rechargeAmount, out_trade_no, "", bankCode, "",
                    SYSConstant.NOTIFY_URL_RECHARGE_BANK,
                    SYSConstant.RETURN_URL_RECHARGE_BANK);
        }

        return null;

    }

    /**
     * 提现页面初始化
     * 
     * @param httpRequest
     *            req
     * @return String
     */
    @RequestMapping(value = "/withdrawalInit")
    public String withdrawalInit(HttpServletRequest request)
    {
        // 没有登录用户
        if (null == request.getSession().getAttribute("frontUser"))
        {
            return "/homepage/login";
        }
        
        int user_id = ((FrontUser) request.getSession().getAttribute(
                "frontUser")).getUser_id();

        FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

        request.setAttribute("frontUser", frontUser);

        return "/homepage/withdrawal";

    }

    /**
     * 提现申请
     * 
     * @param request
     * 
     * @return String
     */
    @RequestMapping(value = "/withdrawal")
    @ResponseBody
	public Map<String, Object> withdrawal(HttpServletRequest request,
			FrontWithdrawalLog frontWithdrawalLog, String flag, String alipayName)
	{
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

		if ("1".equals(flag))
		{
			if (StringUtil.isEmpty(frontUser.getPay_treasure()))
			{
				rtnMap.put("result", false);
				rtnMap.put("message", "支付宝账号未绑定！");

				return rtnMap;

			}
			frontWithdrawalLog.setBank_name("支付宝");
			frontWithdrawalLog.setUser_name(alipayName);
			// 支付宝账号
			frontWithdrawalLog.setAlipay_no(frontUser.getPay_treasure());
		} else
		{
			if (StringUtil.isEmpty(frontUser.getBank_card()))
			{
				rtnMap.put("result", false);
				rtnMap.put("message", "银行账号未绑定！");

				return rtnMap;

			}
			frontWithdrawalLog.setBank_name(frontUser.getBank_account());
			frontWithdrawalLog.setUser_name(frontUser.getAccount_holder());
			frontWithdrawalLog.setAlipay_no(frontUser.getBank_card());
		}
		// 交易号
		String out_trade_no = CommonUtil.getCurrentDateNum();

		frontWithdrawalLog.setUser_id(frontUser.getUser_id());
		// 提交申请
		frontWithdrawalLog.setStatus(FrontWithdrawalLog.APPLY);

		// 交易号
		frontWithdrawalLog.setTrans_id(out_trade_no);
		frontWithdrawalLog.setTime(new Timestamp(System.currentTimeMillis()));

		// 提现金额 js不能比较 两个数的大小
		float amount = frontWithdrawalLog.getAmount();
		// 可用余额
		frontUser.setAble_balance(frontUser.getAble_balance() - amount);
		// 冻结金额
		frontUser.setFrozen_balance(frontUser.getFrozen_balance() + amount);
		
		//提现申请前更新alipayName
		Map<String, Object> param = new HashMap<>();
		param.put("user_id", user_id);
		param.put("alipayName", alipayName);
		frontUserService.updateAlipayName(param);

		// 账户记录
		FrontAccountLog frontAccountLog = new FrontAccountLog();
		// 交易号
		frontAccountLog.setOrder_id(out_trade_no);
		// 交易金额
		frontAccountLog.setAmount(amount);
		// 时间
		frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
		// 用户
		frontAccountLog.setUser_id(user_id);
		// 交易类型
		frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_WITHDRAW);
		// 交易状态
		frontAccountLog.setStatus(FrontAccountLog.APPLY);
		//支付宝姓名
		frontAccountLog.setAlipayName(alipayName);
		//支付宝账号
		frontAccountLog.setAlipay_no(frontUser.getPay_treasure());
		frontAccountLog.setDescription("提现到：" + frontWithdrawalLog.getBank_name());

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("frontWithdrawalLog", frontWithdrawalLog);
		params.put("frontUser", frontUser);
		params.put("frontAccountLog", frontAccountLog);

		// 账户提现记录表登录，前台用户更新金额项目，账户记录表登录
		frontWithdrawalLogService.withdrawal(params);

		rtnMap.put("result", true);
		rtnMap.put("message", "申请成功！");
		return rtnMap;
	}

    /**
     * 提现取消
     * 
     * @param request
     * 
     * @return String
     */
    @RequestMapping(value = "/withdrawalCancel")
    @ResponseBody
    public Map<String, Object> withdrawalCancel(HttpServletRequest request, Integer log_id)
    {

        int user_id = ((FrontUser) request.getSession().getAttribute(
                "frontUser")).getUser_id();

        FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

        FrontAccountLog frontAccountLog = frontAccountLogService
                .selectAccountLogByLogId(log_id);

        frontAccountLog.setStatus(FrontAccountLog.CANCEL);

        // 取消金额
        float amount = frontAccountLog.getAmount();

        // 用户账户金额变更
        frontUser.setAble_balance(frontUser.getAble_balance() + amount);
        frontUser.setFrozen_balance(frontUser.getFrozen_balance() - amount);

        FrontWithdrawalLog frontWithdrawalLog = new FrontWithdrawalLog();

        frontWithdrawalLog.setTrans_id(frontAccountLog.getOrder_id());
        // 提现取消
        frontWithdrawalLog.setStatus(FrontWithdrawalLog.CANCEL);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("frontWithdrawalLog", frontWithdrawalLog);
        params.put("frontUser", frontUser);
        params.put("frontAccountLog", frontAccountLog);

        // 账户提现记录表登录，前台用户更新金额项目，账户记录表登录
        frontWithdrawalLogService.withdrawalCancel(params);
        
        Map<String, Object> rtnMap =new HashMap<String, Object>();
        rtnMap.put("message", "取消成功！");
        rtnMap.put("result", true);

        return rtnMap;

    }

    /**
     * 记录交易信息
     * 
     * @param out_trade_no
     *            商户订单号
     * @param total_fee
     *            充值金额
     * @param user_id
     *            用户
     */
    private void insertFrontAccountLog(String out_trade_no,
            String rechargeAmount, int user_id, String description)
    {
        //
        FrontAccountLog frontAccountLog = new FrontAccountLog();
        // 交易号
        frontAccountLog.setOrder_id(out_trade_no);
        // 涉及单号
        frontAccountLog.setLogistics_code("");
        
    	// 充值肯定都是人民币,要先转成美元
    	float dollar = sysSettingService.yuan2Dollar(Float.parseFloat(rechargeAmount));
        // 交易金额
        frontAccountLog.setAmount(dollar);
        // 时间
        frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
        // 用户
        frontAccountLog.setUser_id(user_id);
        // 交易类型
        frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_RECHARGE);
        // 交易状态
        frontAccountLog.setStatus(FrontAccountLog.APPLY);
        //
        frontAccountLog.setDescription(description);

        frontAccountLogService.insertFrontAccountLog(frontAccountLog);
    }

    /**
     * 根据user_id找到用户各种包裹状态的数据
     * 
     * @param user_id
     * @return int[]数组,总长度为6,6个值,分别是待入库数,已入库数,未支付数,支付数,未打印数，打印数
     */
    public int[] getTransportSum(int user_id)
    {
        int[] array = new int[] { 0, 0, 0, 0, 0, 0 };
        // 待入库状态
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user_id", user_id);
        params.put("enable", PKG_ENABLE);
        params.put("status", BasePackage.LOGISTICS_STORE_WAITING);
        List<Pkg> pkgList = frontPkgService
                .queryPackageByUserIdAndStatus(params);
        array[0] = pkgList.size();

        // 已入库状态
        params.put("status", BasePackage.LOGISTICS_STORAGED);
        pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
        array[1] = pkgList.size();

        params.remove("status");
        // 未支付状态
//        params.put("payStatus", BasePackage.PAYMENT_UNPAID);
//        pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
//        array[2] = pkgList.size();
		params.put("payStatusFreight", BasePackage.PAYMENT_FREIGHT_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatusFreight(params);
		array[2] = pkgList.size();
		params.put("payStatusCustom", BasePackage.PAYMENT_CUSTOM_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatusCustom(params);
		array[2] += pkgList.size();

        // 已支付状态
//        params.put("payStatus", BasePackage.PAYMENT_PAID);
//        pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
//        array[3] = pkgList.size();
        pkgList = frontPkgService.queryPackageByUserIdAndPaid(params);
		array[3] = pkgList.size();
        
        params.remove("payStatus");
        //未打印状态
        params.put("pkgPrint", BasePackage.PACKAGE__NO_PRINT);
        pkgList =frontPkgService.queryPackageByUserIdAndPkgPrint(params);
        array[4] = pkgList.size();
        
        //打印状态
        params.put("pkgPrint", BasePackage.PACKAGE__YES_PRINT);
        pkgList =frontPkgService.queryPackageByUserIdAndPkgPrint(params);
        array[5] = pkgList.size();
        
        return array;
    }

    @RequestMapping("/validateLogin")
    @ResponseBody
    public Map<String,Object> validateLogin(HttpServletRequest request,HttpServletResponse response){
        Map<String,Object> map=new HashMap<String,Object>();    
        HttpSession session = request.getSession();
        FrontUser f=(FrontUser)session.getAttribute("frontUser");
        String str="no login";
        if(f==null){
            map.put("isLogin", false);
        }else{
        	str="login";
            map.put("isLogin", true);
        }
        System.out.println("登录验证xxxxxx "+str);
        return map;
    }
    
	private void addRow(XSSFRow row, com.xiangrui.lmp.business.admin.store.vo.FrontUser frontUser, AccountLog accountLog, MemberBillModel memberBillModel, com.xiangrui.lmp.business.admin.pkg.vo.Pkg pkgDetail, List<String> serviceNameList, int serviceCount, boolean needAddAccountLogCell, boolean showBusinessName)
	{
		int colCount = 0;
		if (showBusinessName)
		{
			// 业务
			XSSFCell cell0 = row.createCell(colCount++);
			cell0.setCellValue(frontUser.getBusiness_name());
		}

		// 客户姓名
		XSSFCell cell1 = row.createCell(colCount++);
		cell1.setCellValue(frontUser.getUser_name());

		// 客户账号
		XSSFCell cell2 = row.createCell(colCount++);
		cell2.setCellValue(frontUser.getAccount());

		// 客户手机号
		XSSFCell cell3 = row.createCell(colCount++);
		cell3.setCellValue(StringUtils.trimToEmpty(frontUser.getMobile()));

		// 账户余额
		XSSFCell cell4 = row.createCell(colCount++);
		cell4.setCellValue(NumberUtils.scaleMoneyData(frontUser.getBalance()));

		// 可用余额
		XSSFCell cell5 = row.createCell(colCount++);
		cell5.setCellValue(NumberUtils.scaleMoneyData(frontUser.getAble_balance()));

		// 冻结余额
		XSSFCell cell6 = row.createCell(colCount++);
		cell6.setCellValue(NumberUtils.scaleMoneyData(frontUser.getFrozen_balance()));

		// 公司运单号
		XSSFCell cell7 = row.createCell(colCount++);
		cell7.setCellValue(pkgDetail != null ? pkgDetail.getLogistics_code() : "");

		// 关联单号
		XSSFCell cell8 = row.createCell(colCount++);
		cell8.setCellValue(pkgDetail != null ? pkgDetail.getOriginal_num() : "");

		// 所属仓库
		XSSFCell cell8_1 = row.createCell(colCount++);
		cell8_1.setCellValue(memberBillModel != null ? memberBillModel.getWarehouse() : "");

		// 品名
		XSSFCell cell9 = row.createCell(colCount++);
		cell9.setCellValue(memberBillModel != null ? memberBillModel.getGoods_name() : "");

		// 品牌
		XSSFCell cell10 = row.createCell(colCount++);
		cell10.setCellValue(memberBillModel != null ? memberBillModel.getBrand() : "");

		// 申报类别
		XSSFCell cell11 = row.createCell(colCount++);
		cell11.setCellValue(memberBillModel != null ? memberBillModel.getGoods_type() : "");

		// 申报总价值(RMB)
		XSSFCell cell12 = row.createCell(colCount++);
		cell12.setCellValue(memberBillModel != null ? NumberUtils.scaleMoneyData(memberBillModel.getTotal_worth()) : "");

		// 重量(实际/磅)
		XSSFCell cell13 = row.createCell(colCount++);
		cell13.setCellValue(pkgDetail != null ? "" + pkgDetail.getActual_weight() : "");

		// 重量(收费/磅)
		XSSFCell cell14 = row.createCell(colCount++);
		cell14.setCellValue(pkgDetail != null ? "" + pkgDetail.getWeight() : "");

		// 单价(RMB)
		XSSFCell cell15 = row.createCell(colCount++);
		cell15.setCellValue(pkgDetail != null ? NumberUtils.scaleMoneyData(pkgDetail.getPrice()) : "");

		// 运费(RMB)
		XSSFCell cell16 = row.createCell(colCount++);
		cell16.setCellValue(pkgDetail != null ? NumberUtils.scaleMoneyData(pkgDetail.getFreight()) : "");

		// 增值服务
		for (int k = 0; k < serviceNameList.size(); k++)
		{

			XSSFCell attachServiceCell = row.createCell(colCount + k);
			if (memberBillModel != null)
			{
				Map<String, Float> map = memberBillModel.getAttach_service_map();
				if (map != null && !map.isEmpty())
				{
					Float price = map.get(serviceNameList.get(k));
					if (price != null && price.compareTo(0.000001f) > 0)
					{
						attachServiceCell.setCellValue(Double.parseDouble(price.toString()));
					}
				}
			}
			else
			{
				attachServiceCell.setCellValue("");
			}
		}

		// 合计(RMB)
		XSSFCell cell17 = row.createCell(colCount + serviceCount);
		colCount++;
		int createTimeColCount=colCount;
		cell17.setCellValue(memberBillModel != null ? NumberUtils.scaleMoneyData(memberBillModel.getTransport_cost()) : "");
		if (needAddAccountLogCell)
		{
			// 支付宝号
			XSSFCell cell18 = row.createCell(colCount + serviceCount);
			cell18.setCellValue(accountLog.getAlipay_no());
			colCount++;

			// 订单号
			XSSFCell cell19 = row.createCell(colCount + serviceCount);
			cell19.setCellValue(accountLog.getOrder_id());
			colCount++;

			// 支付宝流水号
			XSSFCell cell20 = row.createCell(colCount + serviceCount);
			cell20.setCellValue(accountLog.getTrade_no());
			colCount++;
			
			// 银行流水号
			XSSFCell cell21 = row.createCell(colCount + serviceCount);
			cell21.setCellValue(accountLog.getBank_seq_no());
			colCount++;

			CouponUsed couponUsed=couponService.queryCouponUsedByOrder_code(pkgDetail != null ? pkgDetail.getLogistics_code() : "");
			float actualPay=NumberUtils.scaleMoneyDataFloat(accountLog.getAmount());
			if(couponUsed!=null)
			{
				actualPay=NumberUtils.scaleMoneyDataFloat(actualPay+(float)couponUsed.getDenomination());
			}
			
			// 金额
			XSSFCell cell22 = row.createCell(colCount + serviceCount);
			cell22.setCellValue(NumberUtils.scaleMoneyData(actualPay));
			colCount++;

			// 类型
			XSSFCell cell23 = row.createCell(colCount + serviceCount);
			int type = accountLog.getAccount_type();
			String accountType = "";
			if (type == 1)
			{
				accountType = "充值";
			}
			else if (type == 2)
			{
				accountType = "消费";
			}
			else if (type == 3)
			{
				accountType = "提现";
			}
			else if (type == 4)
			{
				accountType = "索赔";
			}
			cell23.setCellValue(accountType);
			colCount++;

			// 状态
			XSSFCell cell24 = row.createCell(colCount + serviceCount);
			int tempStatus = accountLog.getStatus();
			String accountStatus = "";
			if (tempStatus == 1)
			{
				accountStatus = "申请中";
			}
			else if (tempStatus == 2)
			{
				accountStatus = "成功";
			}
			else if (tempStatus == 3)
			{
				accountStatus = "失败";
			}
			else if (tempStatus == 4)
			{
				accountStatus = "取消";
			}
			cell24.setCellValue(accountStatus);
			colCount++;

			// 交易时间
			XSSFCell cell25 = row.createCell(colCount + serviceCount);
			cell25.setCellValue(accountLog.getOpr_time_string());
			colCount++;

			// 描述
			XSSFCell cell26 = row.createCell(colCount + serviceCount);
			cell26.setCellValue(accountLog.getDescription());
			colCount++;
		}
		// 包裹创建日期
		XSSFCell cell27 = row.createCell(createTimeColCount + serviceCount+9);
		String packageCreateDateString = "";
		if (pkgDetail != null)
		{
			packageCreateDateString = simpleDateFormat.format(pkgDetail.getCreateTime());
		}
		cell27.setCellValue(packageCreateDateString);
		colCount++;
		
		
		CouponUsed couponUsed=couponService.queryCouponUsedByOrder_code(pkgDetail != null ? pkgDetail.getLogistics_code() : "");
		float actualPay=NumberUtils.scaleMoneyDataFloat(accountLog.getAmount());
//		if(couponUsed!=null)
//		{
//			actualPay=NumberUtils.scaleMoneyDataFloat(accountLog.getAmount()-(float)couponUsed.getDenomination());
//		}
		
		// 优惠券名称
		XSSFCell cell28 = row.createCell(colCount + serviceCount);
		cell28.setCellValue(couponUsed!=null?couponUsed.getCoupon_name():"");
		colCount++;
		
		// 优惠券面值
		XSSFCell cell29 = row.createCell(colCount + serviceCount);
		cell29.setCellValue(couponUsed!=null?""+couponUsed.getDenomination():"");
		colCount++;
		
		// 实际支付金额
		XSSFCell cell30 = row.createCell(colCount + serviceCount);
		cell30.setCellValue(NumberUtils.scaleMoneyData(actualPay));
		colCount++;
		
	}

	/**
	 * 导出消费记录
	 * 
	 * @param request
	 * @param response
	 * @param logIds
	 */

	@RequestMapping("/frontEndexportList")
	public void frontEndexportList(HttpServletRequest request, HttpServletResponse response, String timeStart, String timeEnd)
	{
		try
		{
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "消费记录";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");

			HttpSession session = request.getSession();
			FrontUser tfrontUser = (FrontUser) session.getAttribute("frontUser");

			Map<String, Object> params = new HashMap<String, Object>();
			if (tfrontUser != null)
			{ 
				String user_name = tfrontUser.getUser_name();
				String useraccount =  tfrontUser.getAccount();
				//params.put("user_name",user_name );
				params.put("account",useraccount );
				params.put("timeStart", timeStart);
				params.put("timeEnd", timeEnd);
				List<AccountLog> accountLogList = accountLogService.queryAll(params);

				XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

				// 新建sheet
				XSSFSheet xssfSheet = xssfWorkbook.createSheet("消费记录");
				// 第一列固定
				xssfSheet.createFreezePane(0, 1, 0, 1);

				// 颜色黄色
				XSSFColor yellowColor = new XSSFColor(Color.YELLOW);

				// 样式黄色居中
				XSSFCellStyle style2 = xssfWorkbook.createCellStyle();
				style2.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
				style2.setFillForegroundColor(yellowColor);
				style2.setWrapText(true);
				int colCount = 0;
				// 列宽
				xssfSheet.setColumnWidth(colCount++, 2000);
				xssfSheet.setColumnWidth(colCount++, 5000);
				xssfSheet.setColumnWidth(colCount++, 4000);
				xssfSheet.setColumnWidth(colCount++, 4000);
				xssfSheet.setColumnWidth(colCount++, 4000);
				xssfSheet.setColumnWidth(colCount++, 4000);
				xssfSheet.setColumnWidth(colCount++, 5000);
				xssfSheet.setColumnWidth(colCount++, 5000);
				xssfSheet.setColumnWidth(colCount++, 4000);
				xssfSheet.setColumnWidth(colCount++, 6000);
				xssfSheet.setColumnWidth(colCount++, 5000);
				xssfSheet.setColumnWidth(colCount++, 4000);
				xssfSheet.setColumnWidth(colCount++, 5000);
				xssfSheet.setColumnWidth(colCount++, 5000);
				xssfSheet.setColumnWidth(colCount++, 5000);
				xssfSheet.setColumnWidth(colCount++, 4000);
				xssfSheet.setColumnWidth(colCount++, 4000);

				// 增值服务
				List<String> serviceNameList = attachServiceService.queryAttachServiceName();
				int serviceCount = 0;
				if (serviceNameList != null)
				{
					serviceCount = serviceNameList.size();
				}
				xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 6000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 8000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 4000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 4000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 1000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 1500);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 5000);
				colCount++;
				xssfSheet.setColumnWidth(colCount + serviceCount, 5000);

				// 表头列
				XSSFRow firstXSSFRow = xssfSheet.createRow(0);

				List<String> columnsList = new ArrayList<String>();
				columnsList.add("客户姓名");
				columnsList.add("客户账号");
				columnsList.add("客户手机号");
				columnsList.add("账户余额");
				columnsList.add("可用余额");
				columnsList.add("冻结余额");
				columnsList.add("公司单号");
				columnsList.add("关联单号");
				columnsList.add("所属仓库");
				columnsList.add("品名");
				columnsList.add("品牌");
				columnsList.add("申报类别");
				columnsList.add("申报总价值(RMB)");
				columnsList.add("重量(实际/磅)");
				columnsList.add("重量(收费/磅)");
				columnsList.add("单价(RMB)");
				columnsList.add("运费(RMB)");

				// 增值服务
				columnsList.addAll(serviceNameList);

				columnsList.add("合计(RMB)");
				columnsList.add("支付宝号");
				columnsList.add("订单号");
				columnsList.add("支付宝流水号");
				columnsList.add("银行流水号");
				columnsList.add("金额");
				columnsList.add("类型");
				columnsList.add("状态");
				columnsList.add("交易时间");
				columnsList.add("描述");
				columnsList.add("包裹创建日期");
				columnsList.add("优惠券名称");
				columnsList.add("优惠券面值");
				columnsList.add("实际支付金额");

				for (int i = 0; i < columnsList.size(); i++)
				{
					XSSFCell cell = firstXSSFRow.createCell(i);
					cell.setCellType(XSSFCell.CELL_TYPE_STRING);
					cell.setCellValue(columnsList.get(i));
					cell.setCellStyle(style2);
				}

				int rowCount = 1;
				for (int j = 0; j < accountLogList.size(); j++)
				{
					XSSFRow row = null;
					com.xiangrui.lmp.business.admin.store.vo.FrontUser frontUser = frontUserService.queryFrontUserById(accountLogList.get(j).getUser_id());

					// 包裹详情
					com.xiangrui.lmp.business.admin.pkg.vo.Pkg pkgDetail = null;
					List<com.xiangrui.lmp.business.admin.pkg.vo.Pkg> packageList = null;
					// 同行帐单信息
					MemberBillModel memberBillModel = null;
					boolean needToAddRow = false;
					if (accountLogList.get(j).getLogistics_code() != null)
					{
						// 通过公司单号查询出对应的包裹
						String logisticsStr = accountLogList.get(j).getLogistics_code();
						String[] logisticsArray = logisticsStr.split("<br>");
						List<String> logistics_codeList = new ArrayList<String>();
						if (logisticsArray != null && logisticsArray.length > 0)
						{
							for (String tempLogisticsCode : logisticsArray)
							{
								logistics_codeList.add(tempLogisticsCode);
							}
						}
						packageList = packageService.queryPackageByLogistics_codeList(logistics_codeList);

						if (packageList != null)
						{
							// 包裹
							if (packageList.size() == 1)
							{
								pkgDetail = packageList.get(0);
								memberBillModel = memberBillService.queryMemberBillModelByPackageId(pkgDetail.getPackage_id());
							}
							else if (packageList.size() > 1)
							{
								needToAddRow = true;
							}
						}
					}

					if (needToAddRow)
					{
						int packageListSize = packageList.size();
						/*
						 * 设定合并单元格区域范围 firstRow 0-based lastRow 0-based firstCol
						 * 0-based lastCol 0-based
						 */
						CellRangeAddress cra18 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 18 + serviceCount, 18 + serviceCount);
						xssfSheet.addMergedRegion(cra18);
						
						CellRangeAddress cra19 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 19 + serviceCount, 19 + serviceCount);

						// 在sheet里增加合并单元格
						xssfSheet.addMergedRegion(cra19);
						CellRangeAddress cra20 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 20 + serviceCount, 20 + serviceCount);
						xssfSheet.addMergedRegion(cra20);
						CellRangeAddress cra21 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 21 + serviceCount, 21 + serviceCount);
						xssfSheet.addMergedRegion(cra21);
						CellRangeAddress cra22 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 22 + serviceCount, 22 + serviceCount);
						xssfSheet.addMergedRegion(cra22);
						CellRangeAddress cra23 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 23 + serviceCount, 23 + serviceCount);
						xssfSheet.addMergedRegion(cra23);
						CellRangeAddress cra24 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 24 + serviceCount, 24 + serviceCount);
						xssfSheet.addMergedRegion(cra24);
						CellRangeAddress cra25 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 25 + serviceCount, 25 + serviceCount);
						xssfSheet.addMergedRegion(cra25);
						CellRangeAddress cra26 = new CellRangeAddress(rowCount, rowCount + packageListSize - 1, 26 + serviceCount, 26 + serviceCount);
						xssfSheet.addMergedRegion(cra26);


						for (int p = 0; p < packageListSize; p++)
						{
							row = xssfSheet.createRow(rowCount++);
							pkgDetail = packageList.get(p);
							memberBillModel = memberBillService.queryMemberBillModelByPackageId(pkgDetail.getPackage_id());
							if (p == 0)
							{
								addRow(row, frontUser, accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList, serviceCount, true, false);
							}
							else
							{
								addRow(row, frontUser, accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList, serviceCount, false, false);
							}
						}
					}
					else
					{
						row = xssfSheet.createRow(rowCount++);
						addRow(row, frontUser, accountLogList.get(j), memberBillModel, pkgDetail, serviceNameList, serviceCount, true, false);
					}
				}
				OutputStream oStream = response.getOutputStream();

				xssfWorkbook.write(oStream);
				oStream.flush();
				oStream.close();
			}
		} catch (IOException e)
		{
			e.printStackTrace();

		}
	}
	
	/**
	 * 异步查询获取消费记录信息
	 * @param req
	 * @return
	 */
    @RequestMapping(value = "/getRecordInfoList2")
    public String getRecordInfoList2(HttpServletRequest req){

    	List<FrontAccountLog> recordInfoList = new ArrayList<FrontAccountLog>();
    	 //没有登录用户返回空的数据信息
        if (null == req.getSession().getAttribute("frontUser")){
        	return "/homepage/login";
        }

        //查询用户信息
        int user_id = ((FrontUser) req.getSession().getAttribute("frontUser")).getUser_id();
        FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

        // 时间格式
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // 当前时间
        Calendar time = Calendar.getInstance();
        //  计算时间
        time.add(Calendar.YEAR, -2);
        
        /**
         * 交易类型:1充值金额,交易类型:2消费金额,交易类型:3提现金额 , 交易类型:4索赔
         */
        int accountType = 0;
        String stringAccountType = req.getParameter("accountType");
        if(stringAccountType != null && !"".equals(stringAccountType)){
        	accountType = Integer.valueOf(stringAccountType);	 //参数中获取数据的类型
        }
        int pageNo = 1;
        String pageParam = req.getParameter("pageNo");
        if(pageParam != null && !"".equals(pageParam)){
        	pageNo = Integer.valueOf(pageParam); //参数中获取数据的类型
        }

        

        Map<String, Object> paramRecharge = new HashMap<String, Object>();
        // 会员ID
        paramRecharge.put("user_id", frontUser.getUser_id());
        // 消费类型
        if(accountType != 0){
            paramRecharge.put("account_type", accountType);
        }
        // 充值完成       // 充值
        paramRecharge.put("status", FrontAccountLog.SUCCESS);
        paramRecharge.put("opr_time", simpleDateFormat.format(time.getTime()));
        PageView pageView = new PageView(pageNo);
        pageView.setPageSize(20);
        paramRecharge.put("pageView", pageView);
        recordInfoList = frontAccountLogService.queryAccountLog(paramRecharge);
        req.setAttribute("recordInfoList",recordInfoList);
        req.setAttribute("pageView", pageView);
        req.setAttribute("accountType", accountType);

        return "/homepage/recordList";
    }
}
