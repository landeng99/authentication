package com.xiangrui.lmp.business.homepage.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/template")
public class TemplateUploadController
{

    @RequestMapping("/upload")
    public void TemplateUpload(HttpServletResponse response,
            HttpServletRequest request)
    {

        String saveFile = "12.xls";
        String filePath = "E:\\翔锐物流文档\\翔锐物流任务计划.xls";
        downloadAttachment(response, request, filePath, saveFile);
        System.out.println("Uload Success");
        // return "admin/success";
    }

    public static void downloadAttachment(HttpServletResponse response,
            HttpServletRequest request, String filePath, String saveFileName)
    {
        response.reset();
        // response.setContentType("application/octet-stream;charset=utf-8");
        response.setContentType("application/x-msdownload");
        // response.setContentType("application/x-download");
        ServletOutputStream outp = null;
        FileInputStream in = null;
        try
        {

            response.addHeader("Content-Disposition", "attachment;filename=\""
                    + encodeFilename(request, saveFileName) + "\"");// 名称两边的双引号不能省略
                                                                    // 兼容火狐
                                                                    // 文件名中的空格
            outp = response.getOutputStream();
            in = new FileInputStream(filePath);
            byte[] b = new byte[1024];
            int i = 0;
             while ((i = in.read(b)) > 0)
            {
                 outp.write(b, 0, i);
            }
            outp.flush();
            outp.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            if (in != null)
            {
                try
                {
                    in.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                in = null;
            }
            if (outp != null)
            {
                try
                {
                    outp.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                outp = null;
            }
        }
    }

    private static String encodeFilename(HttpServletRequest request,
            String fileName) throws UnsupportedEncodingException
    {
        String agent = request.getHeader("USER-AGENT");
        try
        {
            // IE
            if (null != agent && -1 != agent.indexOf("MSIE"))
            {
                fileName = URLEncoder.encode(fileName, "UTF-8");
                // Firefox
            } else if (null != agent && -1 != agent.indexOf("Mozilla"))
            {
                fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
            }
        } catch (UnsupportedEncodingException e)
        {
            try
            {
                fileName = new String(fileName.getBytes("UTF-8"), "iso-8859-1");
            } catch (UnsupportedEncodingException e1)
            {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
        return fileName;
    }

    // private static String getContentType(String fileName) {
    // String ext = FileUtils.getExtension(fileName).toLowerCase();
    // if (ext.equals(".zip")) {
    // return "application/zip";
    // } else if (ext.equals(".xls") || ext.equals(".xlsx")) {
    // return "application/x-excel";
    // } else if (ext.equals(".doc") || ext.equals(".docx")) {
    // return "application/msword";
    // } else if (ext.equals(".pdf")) {
    // return "application/pdf";
    // } else if (ext.equals(".jpg") || ext.equals(".jpeg")) {
    // return "image/jpeg";
    // } else if (ext.equals(".gif")) {
    // return "image/gif";
    // } else if (ext.equals(".png")) {
    // return "image/png";
    // }else if(ext.equals(".bmp")){
    // return "image/bmp";
    // }
    // return "application/force-download";
    // }

    // public void down(File f, String imgUrl) {
    // byte[] buffer = new byte[8 * 1024];
    // URL u;
    // URLConnection connection = null;
    // try {
    // u = new URL(imgUrl);
    // connection = u.openConnection();
    // } catch (Exception e) {
    // System.out.println("ERR:" + imgUrl);
    // return;
    // }
    // connection.setReadTimeout(100000);
    // InputStream is = null;
    // FileOutputStream fos = null;
    // try {
    // f.createNewFile();
    // is = connection.getInputStream();
    // fos = new FileOutputStream(f);
    // int len = 0;
    // while ((len = is.read(buffer)) != -1) {
    // fos.write(buffer, 0, len);
    // }
    //
    // } catch (Exception e) {
    // f.delete();
    // } finally {
    // if (fos != null) {
    // try {
    // fos.close();
    // } catch (IOException e) {
    // }
    // }
    // if (is != null) {
    // try {
    // is.close();
    // } catch (IOException e) {
    // }
    // }
    // }
    // buffer = null;
    // // System.gc();
    // }

}
