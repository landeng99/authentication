package com.xiangrui.lmp.business.homepage.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.coupon.service.CouponService;
import com.xiangrui.lmp.business.admin.coupon.vo.UserCoupon;
import com.xiangrui.lmp.business.admin.sysSetting.service.SysSettingService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.homepage.service.FrontAccountLogService;
import com.xiangrui.lmp.business.homepage.service.FrontPkgAttachServiceService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.business.homepage.vo.FrontPkgAttachService;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.util.CommonUtil;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.alipay.controller.AlipayController;

/**
 * 支付
 * 
 * @author
 * @serial 2015年7月7日
 */
@Controller
@RequestMapping(value = "/payment")
public class PaymentController
{

	Logger logger = Logger.getLogger(PaymentController.class);

	@Autowired
	private FrontAccountLogService frontAccountLogService;

	@Autowired
	private PkgService pkgService;

	@Autowired
	private FrontPkgAttachServiceService frontPkgAttachServiceService;

	@Autowired
	private FrontUserService memberService;

	@Autowired
	private PkgService frontPkgService;

	@Autowired
	private PkgGoodsService pkgGoodsService;

	/**
	 * 优惠券处理服务
	 */
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private SysSettingService sysSettingService;

	/**
	 * 包裹状态：可用
	 */
	private static final int PKG_ENABLE = 1;

	/**
	 * 区分：1.运费 2.关税
	 */
	private static final String SUBJECT_FREIGHT = "1";

	/**
	 * 区分：1.运费 2.关税
	 */
	private static final String SUBJECT_TAX = "2";

	/**
	 * 转运费用初始化清单
	 * 
	 * @param request
	 * @param response
	 * @param package_id
	 * @return
	 */
	//payselect
	@RequestMapping("/paymentList")
	public String paymentList(HttpServletRequest request, HttpServletResponse response, String logisticsCodes)
	{
		try
		{
			// 没有登录用户
			if (null == request.getSession().getAttribute("frontUser"))
			{
				return "/homepage/login";
			}
			String logisticsCodesResponse = "";
			HttpSession session = request.getSession();
			int user_id = ((FrontUser) session.getAttribute("frontUser")).getUser_id();

			FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);
            // //-------------运费logisticsCodes    运单号列表
			List<Pkg> pkgList = pkgService.queryPackageBylogisticsCodes(StringUtil.toStringList(logisticsCodes, ","));
          
			// 账单总费用
			BigDecimal total = new BigDecimal("0"); 
			if(pkgList != null){
				for (Pkg pkg : pkgList)
				{ 
					//pay_status_freight == 未支付的
					if(Pkg.PAYMENT_FREIGHT_UNPAID==pkg.getPay_status_freight())
					{
						List<FrontPkgAttachService> pkgAttachServiceList = frontPkgAttachServiceService.queryPkgAttachServiceForPayment(pkg.getPackage_id());
						total = total.add(new BigDecimal(pkg.getTransport_cost()));
						pkg.setPkgAttachServiceList(pkgAttachServiceList);
						if(logisticsCodesResponse !=""){
							logisticsCodesResponse +=",";
						}
						logisticsCodesResponse+=pkg.getLogistics_code();
					} 
				}
			}
		    //----------------运费 end
			/*优惠券*/
			List<UserCoupon> avliableCouponList = new ArrayList<UserCoupon>();
			logger.info("frontUser.getAble_balance()="+frontUser.getAble_balance());
		 
			avliableCouponList = couponService.queryAvliableCouponByUserIdAndNeedPay(user_id, total.floatValue());

			if(pkgList!=null&&pkgList.size()>1)
			{
				avliableCouponList=new ArrayList<UserCoupon>();
			} 
			
			// -----------税总费用-- start 			//taxtlogisticsCodes 关税 列表
			BigDecimal totalTax = new BigDecimal("0"); 
            String taxtlogisticsCodes = request.getParameter("taxtlogisticsCodes"); 
        	String taxtlogisticsCodesResponse = "";
			List<Pkg> pkgListTax = pkgService.queryPackageBylogisticsCodes(StringUtil.toStringList(taxtlogisticsCodes, ","));
		    if(pkgListTax != null){
		    	for (Pkg pkg : pkgListTax)
				{  
		    		//未支付的
		    		if(Pkg.PAYMENT_CUSTOM_UNPAID == pkg.getPay_status_custom()){
						BigDecimal customs_cost = new BigDecimal("0");
						List<PkgGoods> pkgGoodsList = pkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());

						for (PkgGoods pkgGoods : pkgGoodsList)
						{
							customs_cost = customs_cost.add(new BigDecimal(pkgGoods.getCustoms_cost()));
						} 
						pkg.setGoods(pkgGoodsList); 
						totalTax = totalTax.add(new BigDecimal(pkg.getCustoms_cost()));
						
						if(taxtlogisticsCodesResponse !=""){
							taxtlogisticsCodesResponse +=",";
						}
						taxtlogisticsCodesResponse+=pkg.getLogistics_code();
						
		    		} 
	 			} 
		    } 
 			// --------------税总费用 
 			BigDecimal totalAll = null;
 			totalAll = total.add(totalTax); 
			if (totalAll.compareTo(new BigDecimal(frontUser.getAble_balance())) > 0)
			{
				request.setAttribute("message", "账户可用余额不足！");
			} 
			
			int[] cAry = getTransportSum(frontUser.getUser_id());
			request.setAttribute("frontUser", frontUser);
			request.setAttribute("pkgList", pkgList);
			request.setAttribute("logisticsCodes", logisticsCodesResponse);
			request.setAttribute("taxtlogisticsCodes", taxtlogisticsCodesResponse);  
			//将列表设置为列表
			List<String> logisticsCodesList = new ArrayList<String>();
			List<String> taxtlogisticsCodesList = new ArrayList<String>();
			if(logisticsCodesResponse !=null && logisticsCodesResponse != ""){
 				String[] logisticsArray = logisticsCodesResponse.split(",");
 				if (logisticsArray != null && logisticsArray.length > 0)
				{
					for (String tempLogisticsCode : logisticsArray)
					{
						logisticsCodesList.add(tempLogisticsCode);
					}
				}
			}
			if(taxtlogisticsCodesResponse !=null && taxtlogisticsCodesResponse != ""){
 				String[] logisticsArray = taxtlogisticsCodesResponse.split(",");
 				if (logisticsArray != null && logisticsArray.length > 0)
				{
					for (String tempLogisticsCode : logisticsArray)
					{
						taxtlogisticsCodesList.add(tempLogisticsCode);
					}
				}
			}
			request.setAttribute("logisticsCodesList", logisticsCodesList);
			request.setAttribute("taxtlogisticsCodesList", taxtlogisticsCodesList);
			
			
			request.setAttribute("total", total);
			request.setAttribute("totalTax", totalTax);
			request.setAttribute("alltotal", totalAll);
			request.setAttribute("totalYuan", sysSettingService.dollar2Yuan(total.floatValue()));
			request.setAttribute("c1", cAry[0]);
			request.setAttribute("c2", cAry[1]);
			request.setAttribute("c3", cAry[2]);
			request.setAttribute("c4", cAry[3]);
			request.setAttribute("c5", cAry[4]);
			request.setAttribute("c6", cAry[5]);
			request.setAttribute("avliableCouponList", avliableCouponList);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return "/homepage/paymentList";

	}

	/**
	 * 支付
	 * 
	 * @param request
	 * @param response
	 * @param transport_cost
	 * @return
	 */

	@RequestMapping("/payment")
	@ResponseBody
	public Map<String, Object> payment(HttpServletRequest request, HttpServletResponse response, String logisticsCodes, String total)
	{
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();
		int coupon_id = -1;
		String coupon_idStr = request.getParameter("coupon_id");
		if (coupon_idStr != null)
		{
			coupon_id = Integer.parseInt(coupon_idStr);
		}
		try
		{ 
			// 支付金额
			if(total == null || total == ""){
				total = "0";
				rtnMap.put("message", "支付总金额为0！");
				rtnMap.put("result", "6");
				return rtnMap;
			}
			BigDecimal cost = new BigDecimal(total);
			//税金的订单号。 
            String taxtlogisticsCodes = request.getParameter("taxtlogisticsCodes");  
			// 用户最新信息
			FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

			if (FrontUser.FRONT_USER_STATUS_NO_ACTIVATION == frontUser.getStatus())
			{ 
				rtnMap.put("message", "该账户未激活！");
				rtnMap.put("result", "4");
				return rtnMap;

			}
			else if (FrontUser.FRONT_USER_STATUS_DISABLE == frontUser.getStatus())
			{

				rtnMap.put("message", "该账户已被禁用！");
				rtnMap.put("result", "5");
				return rtnMap;
			} 
			if (cost.compareTo(new BigDecimal("0.0001")) < 0)
			{
				rtnMap.put("message", "没有要支付的项目！");
				rtnMap.put("result", "6");
				return rtnMap;
			}
			if (cost.compareTo(new BigDecimal(frontUser.getAble_balance()).add(new BigDecimal("0.0001"))) > 0)
			{
				// 支付宝支付
				rtnMap.put("message", "账户余额不足，去网上支付！");
				rtnMap.put("result", "2");
				double alowpay =   cost.subtract( new BigDecimal(frontUser.getAble_balance())).doubleValue();  
				
				 DecimalFormat df = new DecimalFormat("#.00");  
		         String realowpay = df.format(alowpay);  
				 rtnMap.put("alowpay", realowpay);
				//计算还差多少钱
				
				return rtnMap;
			} 
			/*
			 * if (cost.compareTo(new
			 * BigDecimal(frontUser.getAble_balance()).add(new
			 * BigDecimal("0.0001"))) > 0) { // 支付宝支付 rtnMap.put("message",
			 * "账户余额不足，去网上支付！"); rtnMap.put("result", "2"); return rtnMap; }
			 * 
			 * // 更新用户余额，包裹支付状态，账户记录 boolean payStatus = updatePayment(total,
			 * frontUser.getUser_id(), logisticsCodes); if (!payStatus) {
			 * rtnMap.put("message", "支付失败！"); rtnMap.put("result", "3");
			 * 
			 * return rtnMap; }
			 */

			// 更新用户余额，包裹支付状态，账户记录
			//运费专用
			String errorCode = "7";
		    String sportotal = request.getParameter("sportotal");    
			BigDecimal costsportotal = new BigDecimal(sportotal);
			String payStatus = "";
			if(costsportotal.compareTo(new BigDecimal("0.0001"))>0){
			   payStatus = memberService.updatePayment(sportotal, frontUser.getUser_id(), logisticsCodes, coupon_id, errorCode);
				if (payStatus != null)
				{
					if ("账户余额不足，去网上支付！".equals(payStatus))
					{
						errorCode = "2";
					}
					rtnMap.put("message", payStatus);
					rtnMap.put("result", errorCode);
 					return rtnMap;
				}
			}
			//处理税金  
			// 更新用户余额，包裹支付状态，账户记录
		    String totaltax = request.getParameter("totaltax");    
			BigDecimal costtotaltax = new BigDecimal(totaltax);
			if (costtotaltax.compareTo(new BigDecimal(frontUser.getAble_balance()).add(new BigDecimal("0.0001"))) > 0)
			{
				// 支付宝支付 --税金支付，余额不足时，不会吐扣减，直接提示失败
				rtnMap.put("message", "账户余额不足，去网上支付！");
				rtnMap.put("result", "2");
				rtnMap.put("alowpay", totaltax);//仍需支付
				return rtnMap;
			}
			
 			if(costtotaltax.compareTo(new BigDecimal("0.0001"))>0 ){
 				boolean bpayStatus = updatePayTax(totaltax, frontUser.getUser_id(), taxtlogisticsCodes);
 				if (!bpayStatus)
 				{
 					rtnMap.put("message", "支付失败！");
 					rtnMap.put("result", "3");
 					return rtnMap;
 				} 
			} 
		} catch (Exception e)
		{ 
			logger.error(e.getMessage());
			e.printStackTrace();
			rtnMap.put("message", "支付异常！"+e.getMessage());
			rtnMap.put("result", "4");

			return rtnMap;
		}
		rtnMap.put("message", "支付成功！");
		rtnMap.put("result", "1");
		return rtnMap;

	}

	/**
	 * 余额不足支付宝支付
	 * 
	 * @param request
	 * @param response
	 * @param logisticsCodes
	 * @param cost
	 *            支付费用
	 * @param flag
	 *            1运费2关税
	 * @return
	 */
	@RequestMapping("/selectPayType")
	public String selectPayType(HttpServletRequest request, HttpServletResponse response, String logisticsCodes, String total, String flag)
	{
		String coupon_idStr = request.getParameter("coupon_id");
		int coupon_id = -1;
		if (coupon_idStr != null)
		{
			coupon_id = Integer.parseInt(coupon_idStr);
		}
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();
		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

		BigDecimal totalBigDecimal = new BigDecimal(total);
		if (coupon_id != -1)
		{
			UserCoupon userCoupon = couponService.queryUserCouponBycouponId(coupon_id);
			if (userCoupon != null)
			{
				totalBigDecimal = totalBigDecimal.subtract(new BigDecimal(userCoupon.getDenomination()));
			}
			request.setAttribute("userCoupon", userCoupon);
		}
		float actualPay = NumberUtils.scaleMoneyDataFloat(totalBigDecimal.floatValue());
		BigDecimal total_fee = totalBigDecimal.subtract(new BigDecimal(frontUser.getAble_balance()));

		request.setAttribute("total", total);
		request.setAttribute("flag", flag);
		request.setAttribute("logisticsCodes", logisticsCodes);
		request.setAttribute("frontUser", frontUser);
		request.setAttribute("total_fee", total_fee);
		request.setAttribute("total_yuan", sysSettingService.dollar2Yuan(total_fee.floatValue()));
		request.setAttribute("actualPay", actualPay);

		return "/homepage/selectPayType";

	}
	/***
	 *全新的支付界面 
	 * @param request
	 * @param response
	 * @param logisticsCodes
	 * @param total
	 * @param flag
	 * @return
	 */
	@RequestMapping("/selectPayType_new")
	public String selectPayType_new(HttpServletRequest request, HttpServletResponse response, String logisticsCodes, String total, String flag)
	{
	 
  		BigDecimal totalBigDecimal = new BigDecimal(total);

		request.setAttribute("total", total); 
 		request.setAttribute("total_yuan", sysSettingService.dollar2Yuan(Float.parseFloat(total)));
 
		return "/homepage/selectPayType_new";

	}
	/**
	 * 网上支付
	 * 
	 * @param request
	 * @param response
	 * @param logisticsCodes
	 * @param transport_cost
	 * @param bankCode
	 * @param flag
	 *            1运费 2 关税
	 * @return
	 */
	@RequestMapping(value = "/paymentOnline")
	public String Online(HttpServletRequest request, HttpServletResponse response, String logisticsCodes, String total, String bankCode, String flag)
	{
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		// 必填 商户订单号
		String out_trade_no = CommonUtil.getCurrentDateNum();

		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

		BigDecimal able_balance = new BigDecimal(frontUser.getAble_balance());

		float yuan = sysSettingService.dollar2Yuan(Float.parseFloat(total));
		BigDecimal total_fee = new BigDecimal(yuan);

		// 备注
		String bankName = CommonUtil.getBankName(bankCode);
		String description = "运费　";

		if (SUBJECT_TAX.equals(flag))
		{
			description = "关税　";
		}

		if (able_balance.compareTo(new BigDecimal("0")) > 0)
		{
			total_fee = total_fee.subtract(new BigDecimal(frontUser.getAble_balance()));

			description = description + "其中账户支付：￥" + able_balance.setScale(2, BigDecimal.ROUND_HALF_UP).toString() + "　　" + bankName + "：￥" + total_fee.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		}
		else
		{
			description = description + bankName;
		}

		insertFrontAccountLog(out_trade_no, total, user_id, logisticsCodes, description, able_balance.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), total_fee.setScale(2, BigDecimal.ROUND_HALF_UP).toString());

		if (SUBJECT_FREIGHT.equals(flag))
		{

			if ("ALIPAY".equals(bankCode))
			{
				AlipayController.alipay(request, response, out_trade_no, total_fee.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), out_trade_no, "", "", SYSConstant.NOTIFY_URL_PAY, SYSConstant.RETURN_URL_PAY);
			}
			else
			{
				AlipayController.bank(request, response, out_trade_no, total_fee.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), out_trade_no, "", bankCode, "", SYSConstant.NOTIFY_URL_PAY_BANK, SYSConstant.RETURN_URL_PAY_BANK);
			}
		}
		else
		{

			if ("ALIPAY".equals(bankCode))
			{
				AlipayController.alipay(request, response, out_trade_no, total_fee.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), out_trade_no, "", "", SYSConstant.NOTIFY_URL_PAY_TAX, SYSConstant.RETURN_URL_PAY_TAX);
			}
			else
			{
				AlipayController.bank(request, response, out_trade_no, total_fee.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), out_trade_no, "", bankCode, "", SYSConstant.NOTIFY_URL_PAY_BANK_TAX, SYSConstant.RETURN_URL_PAY_BANK_TAX);

			}
		}

/*		// 处理使用优惠券
		String coupon_idStr = request.getParameter("coupon_id");
		int coupon_id = -1;
		if (coupon_idStr != null)
		{
			coupon_id = Integer.parseInt(coupon_idStr);
		}
		if (coupon_id != -1)
		{
			UserCoupon userCoupon = couponService.queryUserCouponBycouponId(coupon_id);
			if (userCoupon != null)
			{
				couponService.useCoupon(user_id, coupon_id, logisticsCodes, 1);
			}
		}*/
		//20160911 这里不需要处理使用优惠券

		return null;

	}

	/*	*//**
	 * 账户交易信息
	 * 
	 * @param transport_cost
	 *            付款金额
	 * @param user_id
	 *            用户
	 * @param logisticsCodes
	 *            公司运单号
	 */
	/*
	 * private boolean updatePayment(String transport_cost, int user_id, String
	 * logisticsCodes) { boolean isSucess = true; // 更新数据库用 Map<String, Object>
	 * params = new HashMap<String, Object>();
	 * 
	 * float amount = Float.parseFloat(transport_cost);
	 * 
	 * // 更新用户账户信息 FrontUser frontUser =
	 * memberService.queryFrontUserByUserId(user_id);
	 * 
	 * // 登陆账户记录 FrontAccountLog frontAccountLog = new FrontAccountLog(); // 必填
	 * 商户订单号 String out_trade_no = CommonUtil.getCurrentDateNum();
	 * 
	 * // 公司运单号 frontAccountLog.setLogistics_code(logisticsCodes.replace(",",
	 * "<br>"));
	 * 
	 * frontAccountLog.setOrder_id(out_trade_no); // 交易金额
	 * frontAccountLog.setAmount(amount); // 时间 frontAccountLog.setOpr_time(new
	 * Timestamp(System.currentTimeMillis())); // 用户
	 * frontAccountLog.setUser_id(user_id); // 交易类型
	 * frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_CONSUME); //
	 * 交易成功 frontAccountLog.setStatus(FrontAccountLog.SUCCESS); // 描述
	 * frontAccountLog.setDescription("运费 账户支付"); // 网上支付部分
	 * frontAccountLog.setAlipay_amount(0); // 账务余额支付部分
	 * frontAccountLog.setBalance_amount(amount);
	 * 
	 * // 更新包裹列表的支付状态 List<Pkg> pkgList =
	 * pkgService.queryPackageBylogisticsCodes
	 * (StringUtil.toStringList(logisticsCodes, ","));
	 * 
	 * float ableBalance = frontUser.getAble_balance(); float frozenBalance =
	 * frontUser.getFrozen_balance(); float total = ableBalance + frozenBalance;
	 * 
	 * for (Pkg pkg : pkgList) { if (Pkg.PAYMENT_PAID == pkg.getPay_status()) {
	 * return false; } // 支付状态 pkg.setPay_status(Pkg.PAYMENT_PAID); // 支付方式
	 * pkg.setPay_type(Pkg.PAY_TYPE_ONLINE); // 物流状态:待发货
	 * pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING); // 虚拟扣款处理 float
	 * transport_costFloat = pkg.getTransport_cost(); float virtual_pay =
	 * pkg.getVirtual_pay(); if (virtual_pay > 0.00001f) { if (total >=
	 * transport_costFloat && transport_costFloat > 0.00001f) { float less =
	 * transport_costFloat - virtual_pay; ableBalance = ableBalance - less;
	 * frozenBalance = frozenBalance - virtual_pay; if (frozenBalance < 0) {
	 * frozenBalance = 0; } total = ableBalance + frozenBalance; //标志虚拟扣款已结算处理
	 * pkg.setVirtual_pay(-1); } else { // 余额不足 isSucess = false; } } else //
	 * 正常支付处理 { if (ableBalance >= transport_costFloat) { total = total -
	 * transport_costFloat; ableBalance = ableBalance - transport_costFloat; }
	 * else { // 余额不足 isSucess = false; } }
	 * 
	 * } // 可用余额 frontUser.setAble_balance(ableBalance); // 账户余额
	 * frontUser.setBalance(total); // 冻结金额
	 * frontUser.setFrozen_balance(frozenBalance);
	 * 
	 * params.put("frontUser", frontUser); params.put("frontAccountLog",
	 * frontAccountLog); params.put("pkgList", pkgList);
	 * 
	 * if (isSucess) { memberService.payment(params); } return isSucess; }
	 */

	/**
	 * 网上支付信息
	 * 
	 * @param out_trade_no
	 *            商户订单号
	 * @param total_fee
	 *            付款金额
	 * @param user_id
	 *            用户
	 * @param logisticsCodes
	 *            公司运单号
	 * @param description
	 *            备注
	 * @param balance_amount
	 *            账务余额支付部分
	 * 
	 * @param alipay_amount
	 *            网上支付部分
	 */
	private void insertFrontAccountLog(String out_trade_no, String total_fee, int user_id, String logisticsCodes, String description, String balance_amount, String alipay_amount)
	{
		//
		FrontAccountLog frontAccountLog = new FrontAccountLog();
		// 交易号
		frontAccountLog.setOrder_id(out_trade_no);

		// 公司运单号
		frontAccountLog.setLogistics_code(logisticsCodes.replace(",", "<br>"));
		// 交易金额
		frontAccountLog.setAmount(Float.parseFloat(total_fee));
		// 时间
		frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
		// 用户
		frontAccountLog.setUser_id(user_id);
		// 交易类型
		frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_CONSUME);
		// 交易状态
		frontAccountLog.setStatus(FrontAccountLog.APPLY);
		// 备注
		frontAccountLog.setDescription(description);
		// 网上支付部分
		frontAccountLog.setAlipay_amount(Float.parseFloat(alipay_amount));
		// 账务余额支付部分
		frontAccountLog.setBalance_amount(Float.parseFloat(balance_amount));

		frontAccountLogService.insertFrontAccountLog(frontAccountLog);
	}

	/**
	 * 根据user_id找到用户各种包裹状态的数据
	 * 
	 * @param user_id
	 * @return int[]数组,总长度为6,6个值,分别是待入库数,已入库数,未支付数,支付数,未打印数，打印数
	 */
	public int[] getTransportSum(int user_id)
	{
		int[] array = new int[] { 0, 0, 0, 0, 0, 0 };
		// 待入库状态
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", user_id);
		params.put("enable", PKG_ENABLE);
		params.put("status", BasePackage.LOGISTICS_STORE_WAITING);
		List<Pkg> pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[0] = pkgList.size();

		// 已入库状态
		params.put("status", BasePackage.LOGISTICS_STORAGED);
		pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[1] = pkgList.size();

		params.remove("status");
		// 未支付状态
//		params.put("payStatus", BasePackage.PAYMENT_UNPAID);
//		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
//		array[2] = pkgList.size();
		params.put("payStatusFreight", BasePackage.PAYMENT_FREIGHT_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatusFreight(params);
		array[2] = pkgList.size();
		params.put("payStatusCustom", BasePackage.PAYMENT_CUSTOM_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatusCustom(params);
		array[2] += pkgList.size();

		// 已支付状态
//		params.put("payStatus", BasePackage.PAYMENT_PAID);
//		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
//		array[3] = pkgList.size();
		pkgList = frontPkgService.queryPackageByUserIdAndPaid(params);
		array[3] = pkgList.size();

		params.remove("payStatus");
		// 未打印状态
		params.put("pkgPrint", BasePackage.PACKAGE__NO_PRINT);
		pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		array[4] = pkgList.size();

		// 打印状态
		params.put("pkgPrint", BasePackage.PACKAGE__YES_PRINT);
		pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		array[5] = pkgList.size();

		return array;
	}

	// 运费关税支付分割================运费关税支付分割================运费关税支付分割===============运费关税支付分割

	/**
	 * 关税
	 * 
	 * @param request
	 * @param response
	 * @param package_id
	 * @return
	 */
	@RequestMapping("/taxList")
	public String taxList(HttpServletRequest request, HttpServletResponse response, String logisticsCodes)
	{
		try
		{
			// 没有登录用户
			if (null == request.getSession().getAttribute("frontUser"))
			{
				return "/homepage/login";
			}

			HttpSession session = request.getSession();
			int user_id = ((FrontUser) session.getAttribute("frontUser")).getUser_id();

			FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

			// 用户最新信息
			frontUser = memberService.queryFrontUserByUserId(frontUser.getUser_id());
			List<Pkg> pkgList = pkgService.queryPackageBylogisticsCodes(StringUtil.toStringList(logisticsCodes, ","));
			// 账单总费用
			BigDecimal total = new BigDecimal("0");
			for (Pkg pkg : pkgList)
			{

				BigDecimal customs_cost = new BigDecimal("0");
				List<PkgGoods> pkgGoodsList = pkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());

				for (PkgGoods pkgGoods : pkgGoodsList)
				{
					customs_cost = customs_cost.add(new BigDecimal(pkgGoods.getCustoms_cost()));
				}

				pkg.setGoods(pkgGoodsList);
				/*关税价格修改，此处也修改*/
//				total = total.add(customs_cost);
				total = total.add(new BigDecimal(pkg.getCustoms_cost()));
				pkg.setCustoms_cost(customs_cost.floatValue());
			}
			if (total.compareTo(new BigDecimal(frontUser.getAble_balance())) > 0)
			{
				request.setAttribute("message", "账户可用余额不足！");
			}

			int[] cAry = getTransportSum(frontUser.getUser_id());
			request.setAttribute("frontUser", frontUser);
			request.setAttribute("pkgList", pkgList);
			request.setAttribute("logisticsCodes", logisticsCodes);
			request.setAttribute("total", total);
			request.setAttribute("c1", cAry[0]);
			request.setAttribute("c2", cAry[1]);
			request.setAttribute("c3", cAry[2]);
			request.setAttribute("c4", cAry[3]);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return "/homepage/taxList";

	}

	/**
	 * 支付
	 * 
	 * @param request
	 * @param response
	 * @param transport_cost
	 * @return
	 */

	@RequestMapping("/payTax")
	@ResponseBody
	public Map<String, Object> payTax(HttpServletRequest request, HttpServletResponse response, String logisticsCodes, String total)
	{
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();
		try
		{

			// 支付金额
			BigDecimal cost = new BigDecimal(total);

			// 用户最新信息
			FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

			if (FrontUser.FRONT_USER_STATUS_NO_ACTIVATION == frontUser.getStatus())
			{

				rtnMap.put("message", "该账户未激活！");
				rtnMap.put("result", "4");
				return rtnMap;

			}
			else if (FrontUser.FRONT_USER_STATUS_DISABLE == frontUser.getStatus())
			{

				rtnMap.put("message", "该账户已被禁用！");
				rtnMap.put("result", "5");
				return rtnMap;
			}

			if (cost.compareTo(new BigDecimal("0.0001")) < 0)
			{

				rtnMap.put("message", "没有要支付的项目！");
				rtnMap.put("result", "6");
				return rtnMap;
			}

			if (cost.compareTo(new BigDecimal(frontUser.getAble_balance()).add(new BigDecimal("0.0001"))) > 0)
			{
				// 支付宝支付
				rtnMap.put("message", "账户余额不足，去网上支付！");
				rtnMap.put("result", "2");
				return rtnMap;
			}

			// 更新用户余额，包裹支付状态，账户记录
			boolean payStatus = updatePayTax(total, frontUser.getUser_id(), logisticsCodes);

			if (!payStatus)
			{
				rtnMap.put("message", "支付失败！");
				rtnMap.put("result", "3");

				return rtnMap;
			}

		} catch (Exception e)
		{
			e.printStackTrace();
			rtnMap.put("message", "支付失败！");
			rtnMap.put("result", "3");

			return rtnMap;
		}
		rtnMap.put("message", "支付成功！");
		rtnMap.put("result", "1");
		return rtnMap;

	}

	/**
	 * 账户支付关税信息
	 * 
	 * @param customs_cost
	 *            付款金额
	 * @param user_id
	 *            用户
	 * @param logisticsCodes
	 *            公司运单号
	 * @throws Exception
	 */
	private boolean updatePayTax(String customs_cost, int user_id, String logisticsCodes)
	{
		// 更新数据库用
		Map<String, Object> params = new HashMap<String, Object>();

		float amount = Float.parseFloat(customs_cost);

		// 更新用户账户信息
		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

		// 可用余额
		frontUser.setAble_balance(frontUser.getAble_balance() - amount);
		// 账户余额
		frontUser.setBalance(frontUser.getBalance() - amount);

		// 登陆账户记录
		FrontAccountLog frontAccountLog = new FrontAccountLog();
		// 必填 商户订单号
		String out_trade_no = CommonUtil.getCurrentDateNum();

		// 公司运单号
		frontAccountLog.setLogistics_code(logisticsCodes.replace(",", "<br>"));

		frontAccountLog.setOrder_id(out_trade_no);
		// 交易金额
		frontAccountLog.setAmount(amount);
		// 时间
		frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
		// 用户
		frontAccountLog.setUser_id(user_id);
		// 交易类型
		frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_CONSUME);
		// 交易成功
		frontAccountLog.setStatus(FrontAccountLog.SUCCESS);
		// 描述
		frontAccountLog.setDescription("关税 账户支付");
		frontAccountLog.setCustoms_cost(amount);

		List<String> list = StringUtil.toStringList(logisticsCodes, ",");

		List<PkgGoods> pkgGoodsList = pkgGoodsService.queryGoodsBylogisticsCodes(list);

		for (PkgGoods pkgGoods : pkgGoodsList)
		{
			pkgGoods.setPaid(pkgGoods.getPaid() + pkgGoods.getCustoms_cost());
			pkgGoods.setCustoms_cost(0);
		}

		// 更新包裹列表的支付状态
		List<Pkg> pkgList = pkgService.queryPackageBylogisticsCodes(StringUtil.toStringList(logisticsCodes, ","));

		for (Pkg pkg : pkgList)
		{
//			if (Pkg.PAYMENT_PAID == pkg.getPay_status())
//			if( Pkg.isPaid(pkg) )
		    if (Pkg.PAYMENT_CUSTOM_PAID == pkg.getPay_status_custom())
			{
		        logger.info("已支付过关税，不用再支付了");
				return true;
			}
			// 支付状态
			pkg.setPay_status(Pkg.PAYMENT_CUSTOM_PAID);
			pkg.setPay_status_custom(Pkg.PAYMENT_CUSTOM_PAID);
			// 支付方式
			pkg.setPay_type(Pkg.PAY_TYPE_ONLINE);
			
			// 物流状态:待发货
			if( Pkg.PAYMENT_FREIGHT_PAID==pkg.getPay_status_freight() )
			    pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING);
		}
		params.put("frontUser", frontUser);
		params.put("frontAccountLog", frontAccountLog);
		params.put("pkgGoodsList", pkgGoodsList);
		params.put("pkgList", pkgList);

		memberService.payTax(params);
		return true;
	}

	/**
	 * 批量支付
	 * 
	 * @param request
	 * @param response
	 * @param package_id
	 * @return
	 */
	@RequestMapping("/payManyInit")
	public String payManyInit(HttpServletRequest request, HttpServletResponse response, String package_ids)
	{
		// 没有登录用户
		if (null == request.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}

		HttpSession session = request.getSession();
		int user_id = ((FrontUser) session.getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

		// 用户最新信息
		frontUser = memberService.queryFrontUserByUserId(frontUser.getUser_id());

		// 运费待支付
		Map<String, Object> paramsA = new HashMap<String, Object>();
		paramsA.put("user_id", user_id);
//		paramsA.put("payStatus", Pkg.PAYMENT_UNPAID);
		paramsA.put("payStatusFreight", Pkg.PAYMENT_FREIGHT_UNPAID);
		paramsA.put("enable", Pkg.PACKAGE_ENABLE);
//		List<Pkg> pkgListA = pkgService.queryPackageByUserIdAndPayStatus(paramsA);
		List<Pkg> pkgListA = pkgService.queryPackageByUserIdAndPayStatusFreight(paramsA);
		
		List<Pkg> payTransportCostList = new ArrayList<Pkg>();
		// 总转运费用
		BigDecimal transportCostTotal = new BigDecimal("0");

		for (Pkg pkg : pkgListA)
		{
			// 运费为0
			if (pkg.getTransport_cost() < 0.00001f)
			{
				continue;
			}
			// 包裹内件
			List<PkgGoods> pkgGoodsList = pkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());
			pkg.setGoods(pkgGoodsList);

			// 增值服务价格
			BigDecimal attach_service_price = new BigDecimal("0");

			List<FrontPkgAttachService> pkgAttachServiceList = frontPkgAttachServiceService.queryPkgAttachServiceForPayment(pkg.getPackage_id());

			for (FrontPkgAttachService frontPkgAttachService : pkgAttachServiceList)
			{
				attach_service_price = attach_service_price.add(new BigDecimal(frontPkgAttachService.getService_price()));
			}
			// 增值服务价格
			pkg.setAttach_service_price(attach_service_price.floatValue());

			transportCostTotal = transportCostTotal.add(new BigDecimal(pkg.getTransport_cost()));

			payTransportCostList.add(pkg);
		}

		// 关税待支付
		Map<String, Object> paramsC = new HashMap<String, Object>();
		paramsC.put("user_id", user_id);
//		paramsC.put("payStatus", Pkg.PAYMENT_CUSTOM_UNPAID);
		paramsC.put("payStatusCustom", Pkg.PAYMENT_CUSTOM_UNPAID);
		paramsC.put("enable", Pkg.PACKAGE_ENABLE);
//		List<Pkg> payListB = pkgService.queryPackageByUserIdAndPayStatus(paramsC);
		List<Pkg> payListB = pkgService.queryPackageByUserIdAndPayStatusCustom(paramsC);

		List<Pkg> payTaxtList = new ArrayList<Pkg>();
		// 总关税
		BigDecimal payTaxtTotal = new BigDecimal("0");

		for (Pkg pkg : payListB)
		{
			BigDecimal customs_cost = new BigDecimal(pkg.getCustoms_cost());
			List<PkgGoods> pkgGoodsList = pkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());

//			for (PkgGoods pkgGoods : pkgGoodsList) 关税是从最后一个物品里面找出来的？不是吧
//			{
//				customs_cost = new BigDecimal(pkgGoods.getCustoms_cost());
//			}
			// 关税为0
			if (customs_cost.compareTo(new BigDecimal("0.00001")) < 0)
			{
				continue;
			}

			pkg.setGoods(pkgGoodsList);

			pkg.setCustoms_cost(customs_cost.floatValue());

			payTaxtTotal = payTaxtTotal.add(customs_cost);

			payTaxtList.add(pkg);
		}

		request.setAttribute("payTransportCostList", payTransportCostList);
		request.setAttribute("payTransportCostListcount", payTransportCostList.size());
		request.setAttribute("payTaxtList", payTaxtList);
		request.setAttribute("transportCostTotal", transportCostTotal);//运费总计
		request.setAttribute("payTaxtListcount", payTaxtList.size());//税金 的包裹数

		request.setAttribute("payTaxtTotal", payTaxtTotal); // 税金总计

		return "/homepage/selectPayList_new";

	}

	/**
	 * 支付确认
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/payConfirm")
	public String payConfirm(HttpServletRequest request, HttpServletResponse response)
	{
		String coupon_id=request.getParameter("coupon_id");
		String couponLogistics_code=request.getParameter("couponLogistics_code");
		request.setAttribute("coupon_id", coupon_id);
		request.setAttribute("couponLogistics_code", couponLogistics_code);
		return "/homepage/payConfirm";

	}

	@RequestMapping("/selectCouponCountLeftPay")
	@ResponseBody
	public Map<String, Object> selectCouponCountLeftPay(HttpServletRequest request, HttpServletResponse response, String total)
	{
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		int coupon_id = -1;
		String coupon_idStr = request.getParameter("coupon_id");
		if (coupon_idStr != null)
		{
			coupon_id = Integer.parseInt(coupon_idStr);
		}
		// 支付金额
		BigDecimal cost = new BigDecimal(total);
		UserCoupon userCoupon = couponService.queryUserCouponBycouponId(coupon_id);
		if (userCoupon != null)
		{
			BigDecimal denomination = new BigDecimal(userCoupon.getDenomination());
			cost = cost.subtract(denomination);
		}
		rtnMap.put("leftPay", "￥" + NumberUtils.scaleMoneyData(cost.floatValue()));
		rtnMap.put("actualPay", NumberUtils.scaleMoneyData(cost.floatValue()));
		return rtnMap;

	}
	
	@RequestMapping("/payConfirmCouponUsed")
	@ResponseBody
	public Map<String, Object> payConfirmCouponUsed(HttpServletRequest request, HttpServletResponse response)
	{
		Map<String, Object> rtnMap = new HashMap<String, Object>();
		int coupon_id = -1;
		String coupon_idStr = StringUtils.trim(request.getParameter("coupon_id"));
		String couponLogistics_code = request.getParameter("couponLogistics_code");
		if (coupon_idStr != null)
		{
			int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("user_id", user_id);
			params.put("logistics_code", couponLogistics_code);
			//如有必须时，等支付宝那边反馈修改pay_status之后才记录使用优惠券
			//Pkg pkg = frontPkgService.queryPkgBylogistics(params);
//			if (pkg != null && pkg.getPay_status() == 2)
//			{
				coupon_id = Integer.parseInt(coupon_idStr);
				couponService.useCoupon(user_id, coupon_id, couponLogistics_code, 1);
//			}
		}
		return rtnMap;

	}
}
