package com.xiangrui.lmp.business.homepage.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.business.homepage.service.FrontCouponUsedService;
import com.xiangrui.lmp.business.homepage.service.FrontGoodsTypeService;
import com.xiangrui.lmp.business.homepage.vo.FrontGoodsType;


@Controller
@RequestMapping("/catalog")
public class CatalogController {

    @Autowired
    private FrontGoodsTypeService frontGoodsTypeService;
    
	@RequestMapping("/add")
	public String catalogAdd(String ID,Model model,HttpServletRequest request){
		model.addAttribute("ID", ID);
		
		//查询父类
		List<FrontGoodsType> parentGoodsTypeList=frontGoodsTypeService.queryAllGoodsType(0);
		System.out.println(parentGoodsTypeList);
		//查询子类
		List<FrontGoodsType> childGoodsTypeList=frontGoodsTypeService.queryChildGoodsType();

		request.setAttribute("parentTypeList", parentGoodsTypeList);
		request.setAttribute("childTypeList", childGoodsTypeList);
		return "homepage/Catalog";
	}
}
