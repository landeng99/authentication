package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;

import com.xiangrui.lmp.business.homepage.vo.FrontSender;

public interface FrontSenderMapper {
	List<FrontSender> queryByFrontUserId(String frontUserId);
	
	void insertSender(FrontSender frontSender);
}
