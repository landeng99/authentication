package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;

public interface FrontAccountLogMapper
{
    /**
     * 
     * 
     * @param params
     * @return list
     */
    List<FrontAccountLog> queryAccountLog(Map<String, Object> params);

    /**
     * 会员三个月前 两年之内数据
     * 
     * @param params
     * @return list
     */
    List<FrontAccountLog> queryAccountLogBefore(Map<String, Object> params);

    /**
     * 近一年实际消费额
     * 
     * @param params
     * @return float
     */
    float sumAmountByYear(Map<String, Object> params);

    /**
     * 消费总额
     * @param user_id
     * @return float
     */
    float sumAmount(int user_id);

    /**
     * 新增消费记录
     * 
     * @param frontAccountLog
     * @return
     */
    void insertFrontAccountLog(FrontAccountLog frontAccountLog);

    /**
     * 支付宝回传结果更新
     * 
     * @param frontAccountLog
     * @return
     */
    void updateStatusByOrderId(FrontAccountLog frontAccountLog);

    /**
     * 订单号查找消费记录
     * 
     * @param order_id
     * @return
     */
    FrontAccountLog selectAccountLogByOrderId(String order_id);

    /**
     * ID查找消费记录
     * 
     * @param log_id
     * @return
     */
    FrontAccountLog selectAccountLogByLogId(int log_id);

    /**
     * 更新状态
     * 
     * @param frontAccountLog
     * @return
     */
    void updateStatusByLogId(FrontAccountLog frontAccountLog);
}
