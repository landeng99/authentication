package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;

import com.xiangrui.lmp.business.homepage.vo.FrontOverSeaAddress;

public interface FrontOverSeaAddressMapper
{
    /**
     * 根据逻辑删除状态查询海外仓库地址
     * @param 
     * @return 
     */
    List<FrontOverSeaAddress> queryAll();
    
    /**
     * 根据ID查询海外仓库地址
     * @param id
     * @return 
     */
    FrontOverSeaAddress queryAllById(int id);
    
    
    List<FrontOverSeaAddress> queryAllByUserId(Integer user_id);
}
