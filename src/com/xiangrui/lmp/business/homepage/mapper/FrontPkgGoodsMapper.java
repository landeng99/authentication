package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;

import com.xiangrui.lmp.business.base.ExpressNumLotConnectBean;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;


public interface FrontPkgGoodsMapper {
	/**
	 * 查询同一包裹下的所有商品
	 * @param package_id
	 * @return List
	 */
    List<PkgGoods> queryPkgGoodsById(int package_id);

    /**
	 * 添加商品
	 * @param pkgGoods
	 */
    void addPkgGoods(List<PkgGoods> pkgGoods);
    
    /**
     * 添加物流单号批量导入关系的关联
     * @param expressNumLotList
     */
    void addExpressNumLotConnect(List<ExpressNumLotConnectBean> expressNumLotList);
    
    /**
     * 保存包裹物品
     * @param pkgGoods
     */
    void insertPkgGoods(PkgGoods pkgGoods);
    
    /**
     * 添加包裹
     * @param pkg
     */
    int addPkg(Pkg pkg);
    
    int updatePkgByOriginalNum(Pkg pkg);
    
    /**
     * 根据package_id查询包裹
     * @param package_id
     */
    Pkg queryPkgByPkgid(int package_id);
    /**
     * 删除商品
     * @param package_id
     */
    void delPkgGoods(int package_id);
    
    /**
     * 查询关联单号是否已经存在
     * @param originalNumList
     * @return
     */
    List<Pkg> isExistPkg(List<String> originalNumList);
    
    /**
     * 使包裹不可用
     * @param pkg
     * @return
     */
    int disablePkg(Pkg pkg);
    
    /**
     * 支付后更新商品的税项目
     * 
     * @param pkgGoods
     * @return
     */
    int updateTax(List<PkgGoods> pkgGoods);
    
    /**
     * <!--公司单号（多）查询包裹物品-->
     * @param list
     * @return
     */
    List<PkgGoods> queryGoodsBylogisticsCodes(List<String> list);
    
    /**
     * 通过用户ID来查询该用户批量包裹导入的最大批次号
     * @param user_id
     * @return
     */
    String queryMaxBatchLotByUserId_package(int user_id);
    
    String queryMaxBatchLotByUserId_express_lot(int user_id);
    
    int checkBatchLotExpressExist(ExpressNumLotConnectBean expressNumLotConnect);
    
	/**
	 * 查询商品详情
	 * @param good_id
	 * @return List
	 */
    PkgGoods queryPkgGoodsByGoodId(int good_id);
}
