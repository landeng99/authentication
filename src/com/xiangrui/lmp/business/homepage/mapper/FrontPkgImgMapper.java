package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;

import com.xiangrui.lmp.business.homepage.vo.PackageImg;
public interface FrontPkgImgMapper
{

    /**
     * 图片插入
     * 
     * @return
     */
    int insertPkgImg(PackageImg packageImg);
    
    /**
     * 查询图片
     * @return list
     */
    List<PackageImg> queryPkgImg(PackageImg packageImg);
    
    /**
     * 删除图片
     */
    void deletePkgImg(int img_id);
}
