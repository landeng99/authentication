package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.FrontCity;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;

public interface FrontUserAddressMapper
{

    /**
     * 根据address_id查询收货地址
     * @param address_id
     */
    List<FrontUserAddress> queryUserAddressByAddressId(int address_id);
    
    /**
     * 根据address_id查询收货地址
     * @param address_id
     */
    FrontUserAddress queryUseraddress(int address_id);
    

    /**
     * 根据地址名称查询
     * @param name
     * @return
     */
    List<FrontCity> queryAddressByName(String name);
    
    
    List<FrontCity>  queryAddressByMapParam(Map<String,Object> param);
    

    
    
    /**根据用户ID查询没有身份证照片的收货地址
     * @param user_id
     * @return
     */
    List<FrontUserAddress> queryUserUnloadIdcard(int user_id);
    
    
    /**
     * 更新正面身份证图片
     * @param params
     * @return
     */
    int updateUserFrontIdcard(Map<String,Object> params);
    
    /**
     * 更新背面身份证图片
     * @param params
     * @return
     */
    int updateUserBackIdcard(Map<String,Object> params);
    
    /**
     * 将新表的数据更新到旧表中
     * @param frontUserAddress
     * @return
     */
    void updateUserAddressByReceiveAddress(FrontUserAddress frontUserAddress);
    
    /**
     * 创建包裹地址
     * @param frontUserAddress
     * @return
     */
    int insertFrontUserAddress(FrontUserAddress frontUserAddress);
    
    /**
     * 根据address_id修改包裹收货地址
     * @param frontUserAddress
     * @return
     */
    void updateUserAddress(FrontUserAddress frontUserAddress);
    
    /**
     * 根据logistics_code查询包裹收货地址
     * @param logistics_code
     * @return
     */
    List<FrontUserAddress> queryAddressByLogisticsCode(String logistics_code);
    
    /**
     * 根据receiver和mobile查询包裹收货地址
     * @param params
     * @return
     */
    List<FrontUserAddress> queryAddressByMobileAndReceiver(Map<String, Object> params);
    
    /**
     * 更新正面身份证图片和身份证号码
     * @param params
     * @return
     */
    int updateUserFrontIdcardAndPhoto(Map<String,Object> params);
    
    /**
     * 更新反面身份证图片和身份证号码
     * @param params
     * @return
     */
    int updateUserBackIdcardAndPhoto(Map<String,Object> params);
    
    /**
     * 根据receiver和mobile和idcard查询包裹收货地址
     * @param params
     * @return
     */
    List<FrontUserAddress> queryAddressByMobileAndReceiverAndIdcard(Map<String, Object> params);
}
