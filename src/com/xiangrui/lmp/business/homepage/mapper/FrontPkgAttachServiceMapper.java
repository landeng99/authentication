package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.FrontPkgAttachService;

public interface FrontPkgAttachServiceMapper
{

    /**
     * 包裹增值服务费用
     * 
     * @param package_id
     * @return
     */
    List<FrontPkgAttachService> queryPkgAttachServiceForPayment(
            int package_id);
    
    /**
     * 根据pkgid查询该包裹的增值服务
     * @param package_id
     * @return
     */
    List<FrontPkgAttachService> queryPkgAttachBypkgid(int package_id);
    
    /**
     * 根据pkgid删除该包裹的增值服务
     * @param package_id
     * @return
     */
    void deletePkgAttachBypkgid(int package_id);
    
    /**
     * 根据package_group查询该包裹的增值服务
     * @param package_group
     * @return
     */
    List<FrontPkgAttachService> queryPkgAttachByPkgGroup(Map<String,Object> params);
}
