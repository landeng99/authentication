package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.FrontUser;

public interface FfrontUserMapper
{

    /**
     * 根据用户邮箱查询用户信息
     * 
     * @param email
     * @return FrontUser
     */
    FrontUser queryFrontUserByEmail(String email);

    /**
     * 前台用户登录的判断
     * 
     * @param frontUser
     * @return FrontUser
     */
    FrontUser frontUserLogin(FrontUser frontUser);

    /**
     * 用户注册
     * 
     * @param frontUser
     */
    void addFrontUser(FrontUser frontUser);

    /**
     * 检查用户lastname是否被使用
     * 
     * @param params
     * @return
     */
    int isLastNameExist(Map<String, String> params);

    /**
     * 更新金额项目
     * 
     * @param frontUser
     */
    void updateBalance(FrontUser frontUser);
    
    /**
     * 根据用户ID查询用户信息
     * 
     * @param user_id
     */
    FrontUser queryFrontUserByUserId(int user_id);

    /**
     * 根据用户手机查询用户信息
     * 
     * @param email
     * @return FrontUser
     */
    FrontUser queryFrontUserByMobile(String mobile);
    
    /**
     * 根据用户微信绑定ID查询用户信息
     * @param open_id
     * @return
     */
    FrontUser queryFrontUserByOpenId(String open_id);
    
    /**
     * 根据用户名查询
     * 
     * @param user_name
     * @return FrontUser
     */
    FrontUser queryFrontUserByUserName(String user_name);

    /**
     * 修改密码
     * 
     * @param frontUser
     */
    int updateUserPwd(FrontUser frontUser);

    /**
     * 修改手机号
     * 
     * @param frontUser
     */
    int updateMobile(FrontUser frontUser);

    /**
     * 修改姓名
     * 
     * @param frontUser
     */
    int updateUserName(FrontUser frontUser);
    
    /**
     * 账户支付
     * 
     * @param frontUser
     */
    int updateBalanceForpayment(FrontUser frontUser);
    
    /**
     * 绑定银行卡号
     * 
     * @param frontUser
     */
    int updateBank(FrontUser frontUser);
    
    /**
     * 绑定支付宝
     * 
     * @param frontUser
     */
    int updatePay(FrontUser frontUser);
    /**
     * 修改默认支付方式 
     * 
     * @param frontUser
     */
    int updateDefaultPayType(FrontUser frontUser);
    
    /**
     * 修改虚拟预付款方式
     * 
     * @param frontUser
     */
    int updateVirtualPayFlag(FrontUser frontUser);
    /**
     * 绑定微信ID
     * @param frontUser
     * @return
     */
    int updateopen_id(FrontUser frontUser);
    
    /**
     * 更新个人头像
     * @param frontUser
     * @return
     */
    int updatePersion_icon(FrontUser frontUser);
    
    /**
     * 更新个人推荐二维码URL和推荐链接
     * @param frontUser
     * @return
     */
    int updatePersonQRCodeAndRecommandURL(FrontUser frontUser);
    
    
    /**
     * 更新已经赠送推荐优惠券标志
     * @param frontUser
     * @return
     */
    int updateIs_sent_recommand_coupon(Integer user_id);
    
    /**
     * 查询个人的邀请用户列表
     * @param user_id
     * @return
     */
    List<FrontUser> queryMyInviteUser(Integer recommand_user_id);

	FrontUser queryFrontUserById(int user_id);

	/**
	 * 根据用户ID查询推广信息
	 * @param user_id
	 * @return
	 */
	List<FrontUser> queryPushById(int user_id);
}
