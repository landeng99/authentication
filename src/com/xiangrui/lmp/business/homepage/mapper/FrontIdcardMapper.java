package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;

import com.xiangrui.lmp.business.homepage.vo.FrontIdcard;

public interface FrontIdcardMapper {
	List<FrontIdcard> queryByIdcard(String idcard);
	
	void insertFrontIdcard(FrontIdcard frontIdcard);
}
