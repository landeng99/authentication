package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;

import com.xiangrui.lmp.business.homepage.vo.FrontCity;

public interface FrontCityMapper
{
    /**
     * 查询所有省，市，区
     * @return list
     */
    List<FrontCity> queryAllCity();
    
    /**
     * 根据father_id查询省，市，区
     * @return list
     */
    List<FrontCity> queryCity(int father_id);
}
