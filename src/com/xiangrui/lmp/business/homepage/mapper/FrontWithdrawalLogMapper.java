package com.xiangrui.lmp.business.homepage.mapper;

import com.xiangrui.lmp.business.homepage.vo.FrontWithdrawalLog;

public interface FrontWithdrawalLogMapper
{

    /**
     * 提现申请
     * 
     * @param frontWithdrawalLog
     * @return
     */
    void insertWithdrawalLog(FrontWithdrawalLog frontWithdrawalLog);
    
    /**
     * 提现取消
     * 
     * @param frontWithdrawalLog
     * @return
     */
    void updateWithdrawalLogByTransId(FrontWithdrawalLog frontWithdrawalLog);
    
    /**
     * 根据用户ID统计存在申请提现记录
     * @param front_user_id
     * @return
     */
    int countWithdrawalLogByFrontUserId(int front_user_id);
}
