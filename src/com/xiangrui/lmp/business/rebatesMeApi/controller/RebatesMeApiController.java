package com.xiangrui.lmp.business.rebatesMeApi.controller;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.api.controller.PackageApiController;
import com.xiangrui.lmp.business.rebatesMeApi.bean.RejectCode;
import com.xiangrui.lmp.business.rebatesMeApi.service.RebatesMeApiRecordService;
import com.xiangrui.lmp.business.rebatesMeApi.vo.HandleResultBean;
import com.xiangrui.lmp.business.rebatesMeApi.vo.RebatesMeApiRecord;
import com.xiangrui.lmp.constant.WebConstants;

/**
 * RebatesMe快速反利合作调用接口
 * 
 * @author Will
 *
 */
@Controller
@RequestMapping(value = "/lmpRMapi")
public class RebatesMeApiController
{
	private static final Logger logger = Logger.getLogger(PackageApiController.class);
	@Autowired
	private RebatesMeApiRecordService rebatesMeApiRecordService;
	public static List<String> FUNCTION_LIST;
	static
	{
		FUNCTION_LIST = new ArrayList<String>();
		FUNCTION_LIST.add("query");
		FUNCTION_LIST.add("recharge");
		FUNCTION_LIST.add("recharge-trust");
	}

	/**
	 * 快速反利合作调用接口
	 * 
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/do", method = RequestMethod.GET)
	public void handle(HttpServletRequest request, HttpServletResponse response)
	{
		try
		{
			response.setContentType("application/xml");
			request.setCharacterEncoding("UTF-8");
			String function = StringUtils.trimToNull(request.getParameter("function"));
			int error_code = RejectCode.QueryRejectCode.OTHER_ERROR;
			String error_desc = "";
			Document doc = DocumentHelper.createDocument();
			Element root = doc.addElement("result");
			HandleResultBean handleResultBean = null;
			RebatesMeApiRecord rebatesMeApiRecord = new RebatesMeApiRecord();
			rebatesMeApiRecord.setR_function(function);
			logger.info("快速反利合作调用接口");
			if (function == null || !FUNCTION_LIST.contains(function))
			{
				error_desc = "function填写不正确，必须是query,recharge,recharge-trust其中之一";
			}
			else if ("query".equals(function))
			{
				boolean canHandle = true;
				String member_email = StringUtils.trimToNull(request.getParameter("member_email"));
				String serial_no = StringUtils.trimToNull(request.getParameter("serial_no"));
				String shipping_no = StringUtils.trimToNull(request.getParameter("shipping_no"));
				String check_code = StringUtils.trimToNull(request.getParameter("check_code"));
				if (member_email == null)
				{
					error_desc += "member_email必须填入	";
					canHandle = false;
				}
				if (serial_no == null)
				{
					error_desc += "serial_no必须填入	";
					canHandle = false;
				}
				if (shipping_no == null)
				{
					error_desc += "shipping_no必须填入	";
					canHandle = false;
				}
				if (check_code == null)
				{
					error_desc += "check_code必须填入	";
					canHandle = false;
				}
				if (!check_code_validate(check_code, function + WebConstants.REBATES_ME_SITE_CODE + member_email + serial_no + shipping_no))
				{
					error_desc += "md5校验不通过	";
					canHandle = false;
				}
				if (!rebatesMeApiRecordService.isSerialNoUnqiueRecharge(serial_no))
				{
					error_desc += "申请流水号重复	";
					canHandle = false;
				}
				if (canHandle)
				{

					handleResultBean = rebatesMeApiRecordService.query(member_email, serial_no, shipping_no);
				}
				rebatesMeApiRecord.setMember_email(member_email);
				rebatesMeApiRecord.setSerial_no(serial_no);
				rebatesMeApiRecord.setShipping_no(shipping_no);
				rebatesMeApiRecord.setCheck_code(check_code);
			}
			else if ("recharge".equals(function))// 快速返利
			{
				boolean canHandle = true;
				String member_email = StringUtils.trimToNull(request.getParameter("member_email"));
				String serial_no = StringUtils.trimToNull(request.getParameter("serial_no"));
				String shipping_no = StringUtils.trimToNull(request.getParameter("shipping_no"));
				String amount = StringUtils.trimToNull(request.getParameter("amount"));
				String currency = StringUtils.trimToNull(request.getParameter("currency"));
				String check_code = StringUtils.trimToNull(request.getParameter("check_code"));
				String validateCurrency=currency;
				if (member_email == null)
				{
					error_desc += "member_email必须填入	";
					canHandle = false;
				}
				if (serial_no == null)
				{
					error_desc += "serial_no必须填入	";
					canHandle = false;
				}
				if (shipping_no == null)
				{
					error_desc += "shipping_no必须填入	";
					canHandle = false;
				}
				if (amount == null)
				{
					error_desc += "amount必须填入	";
					canHandle = false;
				}
				if (check_code == null)
				{
					error_desc += "check_code必须填入	";
					canHandle = false;
				}
				if (currency == null)
				{
					currency = "USD";
					validateCurrency="";
				}
				if (!check_code_validate(check_code, function + WebConstants.REBATES_ME_SITE_CODE + member_email + serial_no + shipping_no + amount + validateCurrency))
				{
					error_desc += "md5校验不通过	";
					canHandle = false;
				}
				if (!rebatesMeApiRecordService.isSerialNoUnqiueRecharge(serial_no))
				{
					error_code = RejectCode.RechargeRejectCode.SERIAL_NO_HAVE_CHARGE;
					error_desc += "该流水号已充值	";
					canHandle = false;
				}
				if (canHandle)
				{
					handleResultBean = rebatesMeApiRecordService.recharge(member_email, serial_no, shipping_no, amount, currency);
				}
				rebatesMeApiRecord.setMember_email(member_email);
				rebatesMeApiRecord.setSerial_no(serial_no);
				rebatesMeApiRecord.setShipping_no(shipping_no);
				rebatesMeApiRecord.setCheck_code(check_code);
				rebatesMeApiRecord.setAmount(amount);
				rebatesMeApiRecord.setCurrency(currency);
			}
			else if ("recharge-trust".equals(function))// 快速充值
			{
				boolean canHandle = true;
				String member_email = StringUtils.trimToNull(request.getParameter("member_email"));
				String serial_no = StringUtils.trimToNull(request.getParameter("serial_no"));
				//String shipping_no = StringUtils.trimToNull(request.getParameter("shipping_no"));
				String amount = StringUtils.trimToNull(request.getParameter("amount"));
				String currency = StringUtils.trimToNull(request.getParameter("currency"));
				String check_code = StringUtils.trimToNull(request.getParameter("check_code"));
				String validateCurrency=currency;
				if (member_email == null)
				{
					error_desc += "member_email必须填入	";
					canHandle = false;
				}
				if (serial_no == null)
				{
					error_desc += "serial_no必须填入	";
					canHandle = false;
				}
				if (amount == null)
				{
					error_desc += "amount必须填入	";
					canHandle = false;
				}
				if (check_code == null)
				{
					error_desc += "check_code必须填入	";
					canHandle = false;
				}
				if (currency == null)
				{
					currency = "USD";
					validateCurrency="";
				}
				if (!check_code_validate(check_code, function + WebConstants.REBATES_ME_SITE_CODE + member_email + serial_no + amount + validateCurrency))
				{
					error_desc += "md5校验不通过	";
					canHandle = false;
				}
				if (!rebatesMeApiRecordService.isSerialNoUnqiueRecharge(serial_no))
				{
					error_code = RejectCode.RechargeRejectCode.SERIAL_NO_HAVE_CHARGE;
					error_desc += "该流水号已充值	";
					canHandle = false;
				}
				if (canHandle)
				{
					handleResultBean = rebatesMeApiRecordService.recharge(member_email, serial_no, null, amount, currency);
				}
				rebatesMeApiRecord.setMember_email(member_email);
				rebatesMeApiRecord.setSerial_no(serial_no);
				//rebatesMeApiRecord.setShipping_no(shipping_no);
				rebatesMeApiRecord.setCheck_code(check_code);
				rebatesMeApiRecord.setAmount(amount);
				rebatesMeApiRecord.setCurrency(currency);
			}
			if (handleResultBean != null)
			{
				error_code = handleResultBean.getError_code();
				error_desc = handleResultBean.getError_desc();
			}
			createTextE(root, "error_code", "" + error_code);
			createTextE(root, "error_desc", error_desc);
			String respondStr = doc.asXML();
			OutputStream oStream = response.getOutputStream();
			oStream.write(respondStr.getBytes());
			oStream.flush();
			oStream.close();
			rebatesMeApiRecord.setRespond_error_code("" + error_code);
			rebatesMeApiRecord.setRespond_str(respondStr);
			rebatesMeApiRecordService.recordRebatesMeApiRequest(rebatesMeApiRecord);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * 创建xml节点
	 * 
	 * @param e
	 * @param name
	 * @param value
	 * @return
	 */
	private static Element createTextE(Element e, String name, String value)
	{
		Element element = e.addElement(name);
		String str = (value == null) ? "" : value;
		element.setText(str);
		return element;
	}

	private static boolean check_code_validate(String input_check_code, String paramStr)
	{
		boolean isValid = false;
		logger.info("input_check_code=" + input_check_code);
		if (input_check_code != null && paramStr != null)
		{
			paramStr += WebConstants.REBATES_ME_SITE_KEY;
			logger.info("paramStr=" + paramStr);
			String check_code = DigestUtils.md5Hex(paramStr);
			logger.info("check_code=" + check_code);
			if (input_check_code.equals(check_code))
			{
				isValid = true;
			}
		}
		return isValid;
	}
}
