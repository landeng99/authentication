package com.xiangrui.lmp.business.rebatesMeApi.controller;

import org.apache.commons.codec.digest.DigestUtils;

import com.xiangrui.lmp.constant.WebConstants;


public class CheckCodeTool
{

	public static void main(String[] args)
	{
		String function_query = "query";
		String function_recharge = "recharge";
		String member_email = "18634953095@139.com";
		String serial_no = "serialNo2";
		String shipping_no = "9405509699938058804687";
		String amount = "20.256";
		String currency = "RMB";
		String toQueryCheckCode=function_query+WebConstants.REBATES_ME_SITE_CODE+member_email+serial_no+shipping_no+WebConstants.REBATES_ME_SITE_KEY;
		String query_check_code = DigestUtils.md5Hex(toQueryCheckCode);
		System.out.println("toQueryCheckCode="+toQueryCheckCode);
		System.out.println("query_check_code="+query_check_code);
		String toRechargeCheckCode=function_recharge+WebConstants.REBATES_ME_SITE_CODE+member_email+serial_no+amount+currency+WebConstants.REBATES_ME_SITE_KEY;
		String recharge_check_code = DigestUtils.md5Hex(toRechargeCheckCode);
		System.out.println("toRechargeCheckCode="+toRechargeCheckCode);
		System.out.println("recharge_check_code="+recharge_check_code);
		
		String toRechargeCheckCode1=function_recharge+WebConstants.REBATES_ME_SITE_CODE+member_email+serial_no+shipping_no+amount+currency+WebConstants.REBATES_ME_SITE_KEY;
		String recharge_check_code1 = DigestUtils.md5Hex(toRechargeCheckCode1);
		System.out.println("toRechargeCheckCode1="+toRechargeCheckCode1);
		System.out.println("recharge_check_code1="+recharge_check_code1);
	}

}
