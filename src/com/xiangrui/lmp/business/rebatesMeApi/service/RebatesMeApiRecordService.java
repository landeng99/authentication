package com.xiangrui.lmp.business.rebatesMeApi.service;

import com.xiangrui.lmp.business.rebatesMeApi.vo.HandleResultBean;
import com.xiangrui.lmp.business.rebatesMeApi.vo.RebatesMeApiRecord;

public interface RebatesMeApiRecordService
{
	/**
	 * 运单查询
	 * @param member_email
	 * @param serial_no
	 * @param shipping_no
	 * @return
	 */
	public HandleResultBean query(String member_email,String serial_no,String shipping_no);
	
	/**
	 * 充值
	 * @param member_email
	 * @param serial_no
	 * @param shipping_no
	 * @param amount
	 * @param currency
	 * @return
	 */
	public HandleResultBean recharge(String member_email,String serial_no,String shipping_no,String amount,String currency);
	
	/**
	 * 记录rebatesMe请求和回复内容
	 * @param rebatesMeApiRecord
	 */
	public void recordRebatesMeApiRequest(RebatesMeApiRecord rebatesMeApiRecord);
	
	/**
	 * 检查申请流水号是否唯一
	 * @param serial_no
	 * @return
	 */
	public boolean isSerialNoUnqiue(String serial_no);
	
	/**
	 * query请求时检查申请流水号是否唯一
	 * 
	 * @param serial_no
	 * @return
	 */
	public boolean isSerialNoUnqiueQuery(String serial_no);
	/**
	 * 检查serial_no唯一性（只有充值成功后的序列号才不可以给其它用）
	 * 
	 * @param serial_no
	 * @return
	 */
	public boolean isSerialNoUnqiueRecharge(String serial_no);
}
