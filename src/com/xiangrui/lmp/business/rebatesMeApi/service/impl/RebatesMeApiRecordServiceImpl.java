package com.xiangrui.lmp.business.rebatesMeApi.service.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
import com.xiangrui.lmp.business.admin.store.vo.StorePackage;
import com.xiangrui.lmp.business.admin.sysSetting.service.SysSettingService;
import com.xiangrui.lmp.business.homepage.mapper.FfrontUserMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontAccountLogMapper;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.rebatesMeApi.bean.RejectCode;
import com.xiangrui.lmp.business.rebatesMeApi.mapper.RebatesMeApiRecordMapper;
import com.xiangrui.lmp.business.rebatesMeApi.service.RebatesMeApiRecordService;
import com.xiangrui.lmp.business.rebatesMeApi.vo.HandleResultBean;
import com.xiangrui.lmp.business.rebatesMeApi.vo.RebatesMeApiRecord;
import com.xiangrui.lmp.util.CommonUtil;

@Service(value = "rebatesMeApiRecordService")
public class RebatesMeApiRecordServiceImpl implements RebatesMeApiRecordService
{
	private static final Logger logger = Logger.getLogger(RebatesMeApiRecordServiceImpl.class);
	@Autowired
	private RebatesMeApiRecordMapper rebatesMeApiRecordMapper;
	@Autowired
	private SysSettingService sysSettingService;
	@Autowired
	private FrontAccountLogMapper frontAccountLogMapper;
	@Autowired
	private FfrontUserMapper frontUserMapper;

	@Override
	public HandleResultBean query(String member_email, String serial_no, String shipping_no)
	{
		int error_code = RejectCode.QueryRejectCode.ORDER_HAVE_INPUT;
		String error_desc = "";
		FrontUser frontUser = frontUserMapper.queryFrontUserByEmail(member_email);
		if (frontUser != null)
		{
			List<StorePackage> packageList = rebatesMeApiRecordMapper.queryPkgByOriginal_num(shipping_no);
			List<UnusualPkg> unusualPackageList = rebatesMeApiRecordMapper.queryUnusualPkgByoriginalNum(shipping_no);
			if (packageList == null || packageList.size() == 0)
			{
				int length = shipping_no.length();
				String tempShipping_no = shipping_no;
				if (length >= 12)
				{
					tempShipping_no = StringUtils.substring(shipping_no, length - 12, length);
				}
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("original_num", tempShipping_no);
				packageList = rebatesMeApiRecordMapper.queryPkgLikeByOriginal_num(params);
			}
			if (unusualPackageList == null || unusualPackageList.size() == 0)
			{
				int length = shipping_no.length();
				String tempShipping_no = shipping_no;
				if (length >= 12)
				{
					tempShipping_no = StringUtils.substring(shipping_no, length - 12, length);
				}
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("original_num", tempShipping_no);
				unusualPackageList = rebatesMeApiRecordMapper.queryUnusualPkgLikeByoriginalNum(params);
			}
			if (packageList != null && packageList.size() > 0)
			{
				boolean noError = true;
				StorePackage pkg = packageList.get(0);
				if (pkg.getStatus() >= 3 && pkg.getStatus() != 20 && pkg.getStatus() != 21)
				{
					// //运单号已从仓库发出，error_desc 同时返回发出时间
					error_code = RejectCode.QueryRejectCode.ORDER_HAVE_OUTPUT;
					error_desc = pkg.getOutputTime();
					noError = false;
				}

				if (!member_email.equals(pkg.getAccount()))
				{
					error_code = RejectCode.QueryRejectCode.ORDER_FOUND_BUT_OTHER_USER;
					error_desc = "运单号已找到，但是其他用户";
					noError = false;
				}
				if (pkg.getEnable() == 2)
				{
					error_code = RejectCode.QueryRejectCode.OTHER_ERROR;
					error_desc = "运单已废弃";
					noError = false;
				}
				// 包裹状态，-1：异常，0：待入库，1：已入库，2：待发货，3：已出库，4：空运中，5：待清关，6：清关中，7已清关并已派件中，9：已收货；
				// 20：废弃，21：退货
				if (pkg.getStatus() == 0)
				{
					logger.error("运单号未入库状态为" + pkg.getStatus());
					error_code = RejectCode.QueryRejectCode.ORDER_NOT_FOUND_OR_INPUT;
					error_desc = "运单号未找到、或预报未入库";
					noError = false;
				}
				else if (pkg.getStatus() == -1)
				{
					error_code = RejectCode.QueryRejectCode.OTHER_ERROR;
					error_desc = "运单异常状态";
					noError = false;
				}
				else if (pkg.getStatus() == 20)
				{
					error_code = RejectCode.QueryRejectCode.OTHER_ERROR;
					error_desc = "运单已废弃";
					noError = false;
				}
				else if (pkg.getStatus() == 21)
				{
					error_code = RejectCode.QueryRejectCode.OTHER_ERROR;
					error_desc = "运单已退货";
					noError = false;
				}

				if (noError && pkg.getStatus() == 1)
				{
					error_code = RejectCode.QueryRejectCode.ORDER_HAVE_INPUT;
					error_desc = "运单已到库";
				}
			}
			else
			{
				if (unusualPackageList != null && unusualPackageList.size() > 0)
				{
					error_code = RejectCode.QueryRejectCode.ORDER_FOUND_BUT_NO_USER;
					error_desc = "运单号已找到，但是无主货";
				}
				else
				{
					logger.error("运单号未找到");
					error_code = RejectCode.QueryRejectCode.ORDER_NOT_FOUND_OR_INPUT;
					error_desc = "运单号未找到、或预报未入库";
				}
			}
		}
		else
		{
			error_code = RejectCode.QueryRejectCode.EMAIL_NOT_FOUND;
			error_desc = "Email 未找到";
		}
		HandleResultBean handleResultBean = new HandleResultBean();
		handleResultBean.setError_code(error_code);
		handleResultBean.setError_desc(error_desc);
		return handleResultBean;
	}

	@Override
	public HandleResultBean recharge(String member_email, String serial_no, String shipping_no, String amount, String currency)
	{
		int error_code = RejectCode.RechargeRejectCode.CHARGE_SUCCESS;
		String error_desc = "";
		FrontUser frontUser = frontUserMapper.queryFrontUserByEmail(member_email);
		if (frontUser != null)
		{
			boolean canCharge = true;
			// 登陆账户记录
			FrontAccountLog frontAccountLog = new FrontAccountLog();
			// 必填 商户订单号
			String out_trade_no = CommonUtil.getCurrentDateNum();
			// 快速返利
			if (shipping_no != null)
			{
				canCharge = false;
				List<StorePackage> packageList = rebatesMeApiRecordMapper.queryPkgByOriginal_num(shipping_no);
				List<UnusualPkg> unusualPackageList = rebatesMeApiRecordMapper.queryUnusualPkgByoriginalNum(shipping_no);
				if (packageList == null || packageList.size() == 0)
				{
					int length = shipping_no.length();
					String tempShipping_no = shipping_no;
					if (length >= 12)
					{
						tempShipping_no = StringUtils.substring(shipping_no, length - 12, length);
					}
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("original_num", tempShipping_no);
					packageList = rebatesMeApiRecordMapper.queryPkgLikeByOriginal_num(params);
				}
				if (unusualPackageList == null || unusualPackageList.size() == 0)
				{
					int length = shipping_no.length();
					String tempShipping_no = shipping_no;
					if (length >= 12)
					{
						tempShipping_no = StringUtils.substring(shipping_no, length - 12, length);
					}
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("original_num", tempShipping_no);
					unusualPackageList = rebatesMeApiRecordMapper.queryUnusualPkgLikeByoriginalNum(params);
				}
				if (packageList != null && packageList.size() > 0)
				{
					boolean noError = true;
					StorePackage pkg = packageList.get(0);
					if (pkg.getStatus() >= 3 && pkg.getStatus() != 20 && pkg.getStatus() != 21)
					{
						// //运单号已从仓库发出，error_desc 同时返回发出时间
						error_code = RejectCode.RechargeRejectCode.OTHER_ERROR;
						error_desc = "运单号已从仓库发出	"+pkg.getOutputTime();
						noError = false;
					}

					if (!member_email.equals(pkg.getAccount()))
					{
						error_code = RejectCode.RechargeRejectCode.ORDER_FOUND_BUT_OTHER_USER;
						error_desc = "运单号已找到，但是其他用户";
						noError = false;
					}
					if (pkg.getEnable() == 2)
					{
						error_code = RejectCode.RechargeRejectCode.OTHER_ERROR;
						error_desc = "运单已废弃";
						noError = false;
					}
					// 包裹状态，-1：异常，0：待入库，1：已入库，2：待发货，3：已出库，4：空运中，5：待清关，6：清关中，7已清关并已派件中，9：已收货；
					// 20：废弃，21：退货
					if (pkg.getStatus() == 0)
					{
						logger.error("运单号未入库状态为" + pkg.getStatus());
						error_code = RejectCode.RechargeRejectCode.ORDER_NOT_FOUND_OR_INPUT;
						error_desc = "运单号未找到、或预报未入库";
						noError = false;
					}
					else if (pkg.getStatus() == -1)
					{
						error_code = RejectCode.RechargeRejectCode.OTHER_ERROR;
						error_desc = "运单异常状态";
						noError = false;
					}
					else if (pkg.getStatus() == 20)
					{
						error_code = RejectCode.RechargeRejectCode.OTHER_ERROR;
						error_desc = "运单已废弃";
						noError = false;
					}
					else if (pkg.getStatus() == 21)
					{
						error_code = RejectCode.RechargeRejectCode.OTHER_ERROR;
						error_desc = "运单已退货";
						noError = false;
					}

					if (noError && (pkg.getStatus() == 1||pkg.getStatus()==2))
					{
						canCharge = true;
						// 公司运单号
						frontAccountLog.setLogistics_code(pkg.getLogistics_code());
						// 描述
						frontAccountLog.setDescription("RebatesMe 快速返利");
					}
				}
				else
				{
					if (unusualPackageList != null && unusualPackageList.size() > 0)
					{
						error_code = RejectCode.RechargeRejectCode.ORDER_FOUND_BUT_NO_USER;
						error_desc = "运单号已找到，但是无主货";
					}
					else
					{
						logger.error("运单号未找到");
						error_code = RejectCode.RechargeRejectCode.ORDER_NOT_FOUND_OR_INPUT;
						error_desc = "运单号未找到、或预报未入库";
					}
				}
			}
			// 快速充值
			else
			{
				// 描述
				frontAccountLog.setDescription("RebatesMe 快速充值");
			}
			if (canCharge)
			{
				BigDecimal amountBigDecimal = new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
				// 美元转换成人民币
				if ("USD".equals(currency))
				{
					String rate = sysSettingService.getUSDtoRMBrate();
					if (rate != null)
					{
						amountBigDecimal = amountBigDecimal.multiply(new BigDecimal(rate)).setScale(2, BigDecimal.ROUND_HALF_UP);
					}
				}
				frontAccountLog.setOrder_id(out_trade_no);
				// 交易金额
				frontAccountLog.setAmount(amountBigDecimal.floatValue());
				// 时间
				frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
				// 用户
				frontAccountLog.setUser_id(frontUser.getUser_id());
				// 交易类型
				frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_RECHARGE);
				// 交易成功
				frontAccountLog.setStatus(FrontAccountLog.SUCCESS);

				// 网上支付部分
				frontAccountLog.setAlipay_amount(0);
				// 账务余额支付部分
				frontAccountLog.setBalance_amount(amountBigDecimal.floatValue());
				frontAccountLog.setRebatesms_serial_no(serial_no);
				frontAccountLogMapper.insertFrontAccountLog(frontAccountLog);
				// 可用余额
				frontUser.setAble_balance(frontUser.getAble_balance() + amountBigDecimal.floatValue());
				// 账户余额
				frontUser.setBalance(frontUser.getBalance() + amountBigDecimal.floatValue());
				frontUserMapper.updateBalanceForpayment(frontUser);
				// success
				logger.info(frontAccountLog.getDescription() + " 成功");
				error_code = RejectCode.RechargeRejectCode.CHARGE_SUCCESS;
				error_desc = "充值成功";
			}
		}
		else
		{
			error_code = RejectCode.RechargeRejectCode.EMAIL_NOT_FOUND;
			error_desc = "Email 未找到";
		}
		HandleResultBean handleResultBean = new HandleResultBean();
		handleResultBean.setError_code(error_code);
		handleResultBean.setError_desc(error_desc);
		return handleResultBean;
	}

	/**
	 * 记录rebatesMe请求和回复内容
	 * 
	 * @param rebatesMeApiRecord
	 */
	public void recordRebatesMeApiRequest(RebatesMeApiRecord rebatesMeApiRecord)
	{
		rebatesMeApiRecordMapper.insertRebatesMeApiRecord(rebatesMeApiRecord);
	}

	/**
	 * 检查申请流水号是否唯一
	 * 
	 * @param serial_no
	 * @return
	 */
	public boolean isSerialNoUnqiue(String serial_no)
	{
		boolean isUnqiue = false;
		if (rebatesMeApiRecordMapper.countRebatesMeApiRecordBySerial_no(serial_no) == 0)
		{
			isUnqiue = true;
		}
		return isUnqiue;
	}
	
	/**
	 * query请求时检查申请流水号是否唯一
	 * 
	 * @param serial_no
	 * @return
	 */
	public boolean isSerialNoUnqiueQuery(String serial_no)
	{
		boolean isUnqiue = false;
		if (rebatesMeApiRecordMapper.countRebatesMeApiRecordBySerial_noForQuery(serial_no) == 0)
		{
			isUnqiue = true;
		}
		return isUnqiue;
	}
	
	/**
	 * 检查serial_no唯一性（只有充值成功后的序列号才不可以给其它用）
	 * 
	 * @param serial_no
	 * @return
	 */
	public boolean isSerialNoUnqiueRecharge(String serial_no)
	{
		boolean isUnqiue = false;
		if (rebatesMeApiRecordMapper.countRebatesMeApiRecordBySerial_noForRecharge(serial_no) == 0)
		{
			isUnqiue = true;
		}
		return isUnqiue;
	}
}
