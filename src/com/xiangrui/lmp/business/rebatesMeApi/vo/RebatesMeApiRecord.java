package com.xiangrui.lmp.business.rebatesMeApi.vo;

import java.io.Serializable;

public class RebatesMeApiRecord implements Serializable
{
	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/**
	 * 记录ID
	 */
	private int record_id;
	/**
	 * 请求类型，目前有query和recharge
	 */
	private String r_function;

	/**
	 * 物流商用户Email
	 */
	private String member_email;
	/**
	 * 申请流水号，唯一
	 */
	private String serial_no;
	/**
	 * 运单号码
	 */
	private String shipping_no;
	/**
	 * 充值金额（美元）
	 */
	private String amount;
	/**
	 * 充值金额单位
	 */
	private String currency;
	/**
	 * 校验码
	 */
	private String check_code;
	/**
	 * 返回代码
	 */
	private String respond_error_code;
	/**
	 * 请求回复内容
	 */
	private String respond_str;
	/**
	 * 记录时间
	 */
	private String record_time;

	public int getRecord_id()
	{
		return record_id;
	}

	public void setRecord_id(int record_id)
	{
		this.record_id = record_id;
	}

	public String getR_function()
	{
		return r_function;
	}

	public void setR_function(String r_function)
	{
		this.r_function = r_function;
	}

	public String getMember_email()
	{
		return member_email;
	}

	public void setMember_email(String member_email)
	{
		this.member_email = member_email;
	}

	public String getSerial_no()
	{
		return serial_no;
	}

	public void setSerial_no(String serial_no)
	{
		this.serial_no = serial_no;
	}

	public String getShipping_no()
	{
		return shipping_no;
	}

	public void setShipping_no(String shipping_no)
	{
		this.shipping_no = shipping_no;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(String currency)
	{
		this.currency = currency;
	}

	public String getCheck_code()
	{
		return check_code;
	}

	public void setCheck_code(String check_code)
	{
		this.check_code = check_code;
	}

	public String getRespond_error_code()
	{
		return respond_error_code;
	}

	public void setRespond_error_code(String respond_error_code)
	{
		this.respond_error_code = respond_error_code;
	}

	public String getRespond_str()
	{
		return respond_str;
	}

	public void setRespond_str(String respond_str)
	{
		this.respond_str = respond_str;
	}

	public String getRecord_time()
	{
		if (null != record_time && record_time.lastIndexOf(".0") == 19)
		{
			return record_time.substring(0, 19);
		}
		return record_time;
	}

	public void setRecord_time(String record_time)
	{
		this.record_time = record_time;
	}

}
