package com.xiangrui.lmp.business.rebatesMeApi.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
import com.xiangrui.lmp.business.admin.store.vo.StorePackage;
import com.xiangrui.lmp.business.rebatesMeApi.vo.RebatesMeApiRecord;

public interface RebatesMeApiRecordMapper
{
	/**
	 * 通过serial_no检查其唯一性
	 * @param serial_no
	 * @return
	 */
	public int countRebatesMeApiRecordBySerial_no(String serial_no);
	
	/**
	 * 记录rebatesMe请求和回复内容
	 * @param rebatesMeApiRecord
	 */
	public void insertRebatesMeApiRecord(RebatesMeApiRecord rebatesMeApiRecord);
	/**
	 * 按关联单号精确查询包裹 
	 * @param original_num
	 * @return
	 */
	public List<StorePackage> queryPkgByOriginal_num(String original_num);
	/**
	 * 按关联单号模糊查询包裹
	 * @param original_num
	 * @return
	 */
	public List<StorePackage> queryPkgLikeByOriginal_num(Map<String, Object> params);
	
	/**
	 * 按关联单号精确查询异常包裹
	 * @param original_num
	 * @return
	 */
	public List<UnusualPkg> queryUnusualPkgByoriginalNum(String original_num);
	/**
	 * 按关联单号模糊查询异常包裹
	 * @param original_num
	 * @return
	 */
	public List<UnusualPkg> queryUnusualPkgLikeByoriginalNum(Map<String, Object> params);
	
	/**
	 * 通过query中serial_no检查其唯一性
	 * @param serial_no
	 * @return
	 */
	public int countRebatesMeApiRecordBySerial_noForQuery(String serial_no);
	
	/**
	 * 检查serial_no唯一性（只有充值成功后的序列号才不可以给其它用）
	 * @param serial_no
	 * @return
	 */
	public int countRebatesMeApiRecordBySerial_noForRecharge(String serial_no);
}
