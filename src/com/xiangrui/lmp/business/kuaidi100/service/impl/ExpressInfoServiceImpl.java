package com.xiangrui.lmp.business.kuaidi100.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.kuaidi100.mapper.ExpressInfoMapper;
import com.xiangrui.lmp.business.kuaidi100.service.ExpressInfoService;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.util.PackageLogUtil;

@Service(value = "expressInfoService")
public class ExpressInfoServiceImpl implements ExpressInfoService
{

    @Autowired
    private ExpressInfoMapper expressInfoMapper;

    @Autowired
    private PackageMapper packageMapper;

    /**
     * 
     * 插入快递信息
     * 
     * @param expressInfoList
     * @return
     */
    @Override
    public void batchInsertExpressInfo(List<ExpressInfo> expressInfoList)
    {
        expressInfoMapper.batchInsertExpressInfo(expressInfoList);
    }

    /**
     * 
     * 导入后订阅
     * 
     * @param expressInfoList
     * @return
     */
    @Override
    public void batchUpdatesubscribeStatus(List<ExpressInfo> expressInfoList)
    {

        expressInfoMapper.batchUpdatesubscribeStatus(expressInfoList);
    }

    /**
     * 
     * 更新快递信息(签收时 更新包裹状态未已签收)
     * 
     * @param expressInfo
     * @return
     */
    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void batchUpdateExpressInfo(List<ExpressInfo> expressInfoList)
    {
        expressInfoMapper.batchUpdateExpressInfo(expressInfoList);
        
        for(ExpressInfo expressInfo:expressInfoList){

        if (ExpressInfo.ISCHECK_YES.equals(expressInfo.getIscheck()))
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("ems_code", expressInfo.getNu());
            params.put("status", Pkg.LOGISTICS_SIGN_IN);
            packageMapper.updateStatusByEmsCode(params);

            List<Pkg> pkgs = packageMapper.queryPackageByEms_code(expressInfo
                    .getNu());
            // 通过ems_code找到的包裹,记录日志清空logistics_code的数据
            for (Pkg pkg : pkgs)
            {
                pkg.setLogistics_code(null);
                PackageLogUtil.log(pkg, null);
            }
        }
        }
    }

    /**
     * 
     * 快递单号查询快递信息
     * 
     * @param expressInfo
     * @return
     */
    @Override
    public ExpressInfo queryExpressInfoByNu(String nu)
    {
        return expressInfoMapper.queryExpressInfoByNu(nu);
    }

    /**
     * 
     * 快递ID查询快递信息
     * 
     * @param express_info_id
     * @return
     */
    @Override
    public ExpressInfo queryExpressInfoById(int express_info_id)
    {
        return expressInfoMapper.queryExpressInfoById(express_info_id);
    }

    /**
     * 
     * 查询快递信息
     * 
     * @param params
     * @return
     */
    @Override
    public List<ExpressInfo> queryAllExpressInfo(Map<String, Object> params)
    {
        return expressInfoMapper.queryAllExpressInfo(params);
    }

    @Override
    public List<ExpressInfo> queryExpressInfoByLogistics_code(
            String logistics_code)
    {
        return expressInfoMapper
                .queryExpressInfoByLogistics_code(logistics_code);
    }

    /**
     * 重新订阅
     * 
     * @param expressInfo
     * @return
     */
    @Override
    public void updatesubscribeStatus(ExpressInfo expressInfo)
    {

        expressInfoMapper.updatesubscribeStatus(expressInfo);
    }

    @Override
    public List<ExpressInfo> queryExpressInfoByCode(String logistics_code,
            String ems_code)
    {
        ExpressInfo expressInfo = new ExpressInfo();
        expressInfo.setLogistics_code(logistics_code);
        expressInfo.setNu(ems_code);
        return expressInfoMapper.queryExpressInfoByCode(expressInfo);
    }

    /**
     * 更新快递信息(签收时 更新包裹状态未已签收)
     * @param expressInfo
     * @return
     */
    @Override
    public int updateExpressInfo(ExpressInfo expressInfo)
    {
        
        if (ExpressInfo.ISCHECK_YES.equals(expressInfo.getIscheck()))
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("ems_code", expressInfo.getNu());
            params.put("status", Pkg.LOGISTICS_SIGN_IN);
            packageMapper.updateStatusByEmsCode(params);

            List<Pkg> pkgs = packageMapper.queryPackageByEms_code(expressInfo
                    .getNu());
            // 通过ems_code找到的包裹,记录日志清空logistics_code的数据
            for (Pkg pkg : pkgs)
            {
                pkg.setLogistics_code(null);
                PackageLogUtil.log(pkg, null);
            }
        }
        
      return  expressInfoMapper.updateExpressInfo(expressInfo);
        
    }
}
