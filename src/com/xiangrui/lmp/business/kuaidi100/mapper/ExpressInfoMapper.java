package com.xiangrui.lmp.business.kuaidi100.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;

public interface ExpressInfoMapper
{
    /**
     * 
     * 插入快递信息
     * 
     * @param expressInfoList
     * @return
     */
    void batchInsertExpressInfo(List<ExpressInfo> expressInfoList);
    
    /**
     * 
     * 导入后订阅
     * 
     * @param expressInfoList
     * @return
     */
    void batchUpdatesubscribeStatus(List<ExpressInfo> expressInfoList);

    /**
     * 
     * 更新快递信息
     * 
     * @param expressInfo
     * @return
     */
    void batchUpdateExpressInfo(List<ExpressInfo> expressInfoList);

    /**
     * 
     * 更新快递信息
     * 
     * @param expressInfo
     * @return
     */
    int updateExpressInfo(ExpressInfo expressInfo);

    /**
     * 
     * 快递单号查询快递信息
     * 
     * @param expressInfo
     * @return
     */
    ExpressInfo queryExpressInfoByNu(String nu);

    /**
     * 
     * 快递ID查询快递信息
     * 
     * @param express_info_id
     * @return
     */
    ExpressInfo queryExpressInfoById(int express_info_id);

    /**
     * 
     * 查询快递信息
     * 
     * @param params
     * @return
     */
    List<ExpressInfo> queryAllExpressInfo(Map<String, Object> params);

    /**
     * 更新快递单号
     * 
     * @param expressInfo
     * @return
     */
    void updateEmsCode(ExpressInfo expressInfo);
    
    /**
     * 
     * @param logistics_code
     * @return
     */
    List<ExpressInfo> queryExpressInfoByLogistics_code(String logistics_code);

    /**
     * 重新订阅
     * 
     * @param expressInfo
     * @return
     */
    void updatesubscribeStatus(ExpressInfo expressInfo);

    /**
     * 
     * @param expressInfo
     * @return
     */
    List<ExpressInfo> queryExpressInfoByCode(ExpressInfo expressInfo);
    
    /**
     * 导入后已存在订阅信息的更新成无效
     * 
     * @param logistics_codes
     * @return
     */
    void batchUpdateAble(List<String> logistics_codes);
}
