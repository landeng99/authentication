package com.xiangrui.lmp.business.kuaidi100.controller;

import java.sql.Timestamp;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.admin.interfacelog.service.InterfaceLogService;
import com.xiangrui.lmp.business.admin.interfacelog.vo.InterfaceLog;
import com.xiangrui.lmp.business.handler.ExpressInfoPushHandler;
import com.xiangrui.lmp.business.handler.MessageConfig;
import com.xiangrui.lmp.business.kuaidi100.service.ExpressInfoService;
import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;
import com.xiangrui.lmp.init.SpringContextHolder;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.UUIDUtil;
import com.xiangrui.lmp.util.kuaidiUtil.PostToKuaidi100;

/**
 * 快递信息更新线程
 * 
 * @author hyh
 * 
 */
public class UpdateExpressThread implements Runnable
{
    private static Logger logger = Logger.getLogger(PostToKuaidi100.class);

    /**
     * 监控状态:中止
     */
    private static final String MONITORING_STATUS_ABORT = "abort";

    /**
     * 快递信息
     */
    private Timestamp receiveTime;

    private String param;

    public UpdateExpressThread(String param, Timestamp receiveTime)
    {
        this.param = param;
        this.receiveTime = receiveTime;
    }

    @Override
    public void run()
    {
        InterfaceLogService interfaceLogService = SpringContextHolder
                .getBean("interfaceLogService");
        // 日志对象
        InterfaceLog interfaceLog = new InterfaceLog();
        interfaceLog.setLog_id(UUIDUtil.uuid());
        interfaceLog.setStart_time(receiveTime);
        interfaceLog.setEnd_time(receiveTime);
        interfaceLog.setReq_msg(param);
        interfaceLog.setInterace_type(InterfaceLog.type_kuaidi100_msgsub);
        
        // 记录接口日志
        interfaceLogService.createInterfaceLog(interfaceLog);

        ExpressInfo expressInfo = new ExpressInfo();
        JSONObject jSONObject = JSONObject.fromObject(param);
        // 监控状态
        expressInfo.setMonitoring_status(jSONObject.getString("status"));
        // 包括got、sending、check三个状态
        expressInfo.setBillstatus(jSONObject.getString("billstatus"));
        // 监控状态相关消息
        expressInfo.setMonitoring_message(jSONObject.getString("message"));
        // 最新查询结果
        JSONObject result = jSONObject.getJSONObject("lastResult");
        // 查询消息体
        expressInfo.setMessage(result.getString("message"));
        // 快递单当前签收状态
        expressInfo.setState(result.getString("state"));
        // 通讯状态
        expressInfo.setCommunication_status(result.getString("status"));
        // 快递单明细状态标记
        expressInfo.setCondition(result.getString("condition"));
        // 是否签收标记
        expressInfo.setIscheck(result.getString("ischeck"));
        // 快递公司编码
        expressInfo.setCom(result.getString("com"));
        // 单号
        expressInfo.setNu(result.getString("nu"));
        // 详情
        expressInfo.setData(result.getString("data"));
        // 接受到以上信息的时间
        expressInfo.setReceive_time(this.receiveTime);

        // status= abort而且message中包含“3天”关键字的快递单处理
        String message = jSONObject.getString("message");
        if (MONITORING_STATUS_ABORT.equals(jSONObject.getString("status"))
                || (StringUtil.isNotEmpty(message) && message.contains("3天")))
        {

            expressInfo.setSubscribe_status(ExpressInfo.SUBSCRIBE_STATUS_FAIL);

        } else
        {
            expressInfo
                    .setSubscribe_status(ExpressInfo.SUBSCRIBE_STATUS_SUCCESS);
        }

        // 获取数据处理service
        ExpressInfoService expressInfoService = SpringContextHolder
                .getBean("expressInfoService");
        
        ExpressInfoPushHandler handler= SpringContextHolder
                .getBean("expressInfoPushHandler");
        
        MessageConfig messageConfig=new MessageConfig();
        
        messageConfig.setExpressInfo(expressInfo);
        try{

            //推送快递信息给客户平台
            handler.execute(messageConfig);   
        }catch(Exception e){
            logger.error("快递100推送客户平台异常："+e.toString());
        }
        
        int count = expressInfoService.updateExpressInfo(expressInfo);

        if (count > 0)
        {
            logger.debug("单号:" + expressInfo.getNu() + "  更新成功");
        } else
        {
            logger.debug("单号:" + expressInfo.getNu() + "  无记录");
        }

    }
}
