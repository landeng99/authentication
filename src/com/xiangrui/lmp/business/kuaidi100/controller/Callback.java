package com.xiangrui.lmp.business.kuaidi100.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.util.JacksonHelper;
import com.xiangrui.lmp.util.kuaidiUtil.pojo.NoticeResponse;

@Controller
@RequestMapping("/callback")
public class Callback extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    /**
     * 处理快递100 推送信息 线程池
     */
    private static ExecutorService threadPool = Executors.newCachedThreadPool();

    Logger logger = Logger.getLogger(Callback.class);

    @RequestMapping("/receive")
    public String receive(HttpServletRequest request,
            HttpServletResponse response) throws IOException
    {
        logger.info("----------------快递信息接收开始--------------------");
        NoticeResponse resp = new NoticeResponse();
        resp.setResult(false);
        resp.setReturnCode("500");
        resp.setMessage("保存失败");
        try
        {
            // 当前时间
            Timestamp receiveTime = new Timestamp(System.currentTimeMillis());
            String param = request.getParameter("param");

            // 更新快递信息线程
            threadPool.execute(new UpdateExpressThread(param, receiveTime));

            resp.setResult(true);
            resp.setReturnCode("200");
            // 这里必须返回，否则认为失败，过30分钟又会重复推送。
            response.getWriter().print(JacksonHelper.toJSON(resp));
            logger.info("快递信息接收正常");

        } catch (Exception e)
        {
            e.printStackTrace();
            logger.error("快递信息接收异常" + e.getMessage());
            resp.setMessage("保存失败" + e.getMessage());
            // 保存失败，服务端等30分钟会重复推送。
            response.getWriter().print(JacksonHelper.toJSON(resp));
        }
        logger.info("----------------快递信息接收结束--------------------");
        return null;
    }

}
