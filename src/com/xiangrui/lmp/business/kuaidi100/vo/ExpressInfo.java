package com.xiangrui.lmp.business.kuaidi100.vo;

import com.xiangrui.lmp.business.base.BaseExpressInfo;

public class ExpressInfo extends BaseExpressInfo
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 目的城市
     */
    private String region;

    public String getRegion()
    {
        return region;
    }

    public void setRegion(String region)
    {
        this.region = region;
    }

}
