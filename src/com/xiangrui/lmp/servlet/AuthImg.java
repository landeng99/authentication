package com.xiangrui.lmp.servlet;

import java.awt.Color;
import java.io.IOException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.xiangrui.lmp.util.StringUtil;

import cn.dsna.util.images.ValidateCode;

/**
 * 创建验证码
 * @author Administrator
 *
 */
public class AuthImg extends HttpServlet
{
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

//    /**
//     * 创建验证码
//     */
//    public void service(HttpServletRequest req, HttpServletResponse resp)
//            throws ServletException, IOException
//    {
//        //图片的：长*高
//        int width = 60;
//        int height = 20;
//        
//       //图片对象
//        BufferedImage image = new BufferedImage(width, height,
//                BufferedImage.TYPE_INT_RGB);
//        Graphics g = image.getGraphics();
//        Random random = new Random();
//        g.setColor(getRandColor(200, 250));
//        g.fillRect(0, 0, width, height);
//        g.setColor(getRandColor(160, 200));
//        for (int i = 0; i < 150; i++)
//        {
//            int x = random.nextInt(width);
//            int y = random.nextInt(height);
//            int x1 = random.nextInt(12);
//            int y1 = random.nextInt(12);
//            g.drawLine(x, y, x + x1, y + y1);
//        }
//        String sRand = "";
//        for (int i = 0; i < 4; i++)
//        {
//            String rand = String.valueOf(random.nextInt(10));
//            sRand += rand;
//            
//            //设置字体
//            Font fnt = new Font("ITALIC", Font.BOLD, 12);
//            g.setFont(fnt);
//            g.setColor(new Color(20 + random.nextInt(110), 20 + random
//                    .nextInt(110), 20 + random.nextInt(110)));
//            g.drawString(rand, 13 * i + 6, 16);
//        }
//
//        
//        //将随机字符串放入会话
//        req.getSession().setAttribute("AUTHIMG_CODE", sRand);
//        
//        resp.setHeader("Pragma", "no-cache");
//        resp.setHeader("Cache-control", "no-cache");
//        resp.setDateHeader("Expires", 0);
//        resp.setContentType("image/jpeg");
//        
//        //输出图片
//        ServletOutputStream sos = resp.getOutputStream(); 
//        JPEGImageEncoder encoder = new JPEGImageEncoderImpl(sos);
//        ImageIO.write(image, "jpeg", sos);
//        sos.close();
//
//    }
    /**
     * 创建验证码
     */
    public void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        //图片的：长*高
        int width = 60;
        int height = 20;
		ValidateCode imgVCode = new ValidateCode(width, height, 4, 30);
        
        //将随机字符串放入会话
        req.getSession().setAttribute("AUTHIMG_CODE", imgVCode.getCode());
        
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Cache-control", "no-cache");
        resp.setDateHeader("Expires", 0);
        resp.setContentType("image/jpeg");
        
        //输出图片
        imgVCode.write(resp.getOutputStream());

    }
    private Color getRandColor(int fc, int bc)
    {
        Random random = new Random();
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }
    
    public static boolean checkAuthingCode(String imgVCValues,String txtCode)
    {
    	boolean isSuccess=false;
    	imgVCValues=StringUtils.trimToNull(imgVCValues);
    	txtCode=StringUtils.trimToNull(txtCode);
        if (null != imgVCValues && txtCode!=null)
        {
        	imgVCValues=imgVCValues.toUpperCase();
        	txtCode=txtCode.toUpperCase();
        	if(imgVCValues.equals(txtCode))
        	{
        		isSuccess=true;
        	}
        }
        return isSuccess;
    }
}
