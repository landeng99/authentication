package com.xiangrui.lmp.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.simple.JSONObject;

public class ArticleFileUploadServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
    private static final int ERROR_CODE = 1;
    private static final int SUCCESS_CODE = 0;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
		//文件保存目录路径
		String savePath = request.getServletContext().getRealPath("/") + "/resource/upload/";
		
		//文件保存目录URL
		String saveUrl  = request.getContextPath() + "/resource/upload/";

		//定义允许上传的文件扩展名
		HashMap<String, String> extMap = new HashMap<String, String>();
		extMap.put("image", "gif,jpg,jpeg,png,bmp");
		extMap.put("flash", "swf,flv");
		extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
		extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");

		//最大文件大小
		long maxSize = 100000000;
        
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter print = response.getWriter();
		if ( !ServletFileUpload.isMultipartContent(request)){
			getError(ERROR_CODE,"请选择文件。",print);
			return;
		}
		
		//检查目录
		File uploadDir = new File(savePath);
		if(!uploadDir.isDirectory()){
			uploadDir.mkdirs();
		}
		//检查目录写权限
		if(!uploadDir.canWrite()){
			getError(ERROR_CODE,"上传目录没有写权限。",print);
			return;
		}

		String dirName = request.getParameter("dir");
		if (null == dirName) {
		    dirName = "image";
		}
					
		if(!extMap.containsKey(dirName)){
			getError(ERROR_CODE,"目录名不正确。",print);
		    return;
		}
					
		//创建文件夹
		savePath += dirName + "/";
		saveUrl += dirName + "/";
		File saveDirFile = new File(savePath);
		if (!saveDirFile.exists()) {
		    saveDirFile.mkdirs();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String ymd = sdf.format(new Date());
		savePath += ymd + "/";
		saveUrl += ymd + "/";
		File dirFile = new File(savePath);
		if (!dirFile.exists()) {
		    dirFile.mkdirs();
		}

		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setHeaderEncoding("UTF-8");
		List items = null;
		try {
		    items = upload.parseRequest(request);
		} catch (FileUploadException e) {
		     e.printStackTrace();
		}
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			FileItem item = (FileItem) itr.next();
			String fileName = item.getName();
			long fileSize = item.getSize();
			if (!item.isFormField()) {
			    //检查文件大小
				if(item.getSize() > maxSize){
					getError(ERROR_CODE,"上传文件大小超过限制。",print);
					return;
				}
				//检查扩展名
				String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
				if(!Arrays.asList(extMap.get(dirName).split(",")).contains(fileExt)){
					getError(ERROR_CODE,"上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。",print);
				    return;
			    }
	
				SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
				String newFileName = df.format(new Date()) + "_" + new Random().nextInt(1000) + "." + fileExt;
				try{
				    File uploadedFile = new File(savePath, newFileName);
					item.write(uploadedFile);
				}catch(Exception e){
					getError(ERROR_CODE,"上传文件失败。",print);
					return;
				}
		
				JSONObject obj = new JSONObject();
				obj.put("error", SUCCESS_CODE);
				obj.put("url", saveUrl + newFileName);
				print.write(obj.toJSONString());
				print.flush();
				print.close();
				return;
			}
	   }
	}
	
	 /**
	  * 返回码
	  * @param message
	  * @return
	  */
	 private void getError(int errorNumber,String message,PrintWriter print) {
	    JSONObject obj = new JSONObject();
		obj.put("error", errorNumber);
		obj.put("message", message);
		print.write(obj.toJSONString());
		print.flush();
		print.close();
	}

	
	
}
