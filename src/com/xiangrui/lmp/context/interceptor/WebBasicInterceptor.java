package com.xiangrui.lmp.context.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.xiangrui.lmp.business.homepage.vo.FrontUser;

/**
 * @spring.bean id="webBasicInterceptor"
 * 
 * @author ltp
 * @version 1.0
 */
public class WebBasicInterceptor extends HandlerInterceptorAdapter
{

    private List<String> noFilterUrls;

    public List<String> getNoFilterUrls()
    {
        return noFilterUrls;
    }

    public void setNoFilterUrls(List<String> noFilterUrls)
    {
        this.noFilterUrls = noFilterUrls;
    }

    public WebBasicInterceptor()
    {
    }

    /**
     * LOG.
     */
    private static final Logger logger = Logger
            .getLogger(WebBasicInterceptor.class);

    /**
     * 可以进行编码、安全控制等处理.
     * 
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @param handler
     *            Object
     * @return boolean
     * @throws Exception
     *             if has Exception
     */
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception
    {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String contextPath = request.getContextPath();
        String uri = request.getRequestURI();
       
        if (uri.indexOf(contextPath) != -1)
        {   
            String path= uri.substring(contextPath.length());
            
            // 如果要访问的资源是不需要验证的
            if (noFilterUrls.contains(path))
            {
                return true;
            }

            HttpSession session = request.getSession();
            if (null != session)
            {
                FrontUser frontUser = (FrontUser) session
                        .getAttribute("frontUser");
                System.out.println(frontUser);
                if (null == frontUser)
                {
                    request.getRequestDispatcher("/homepage/login").forward(
                            request, response);
                    return false;
                }
            }

        }

        return super.preHandle(request, response, handler);
    }

    /**
     * 验证session是否在线.
     * 
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @return boolean
     */
    public boolean validateSession(HttpServletRequest request,
            HttpServletResponse response)
    {
        String loginInfo = (String) request.getSession().getAttribute(
                "LOGIN_INFO");
        if (null != loginInfo && !"".equals(loginInfo))
        {
            return true;
        } else
        {
            String headerStr = request.getHeader("x-requested-with");
            if (null != headerStr
                    && "XMLHttpRequest".equalsIgnoreCase(headerStr))
            {
                response.setHeader("sessionstatus", "timeOut");
                return false;
            }
        }
        return true;
    }

    // 在业务处理器处理请求执行完成后,生成视图之前执行的动作
    @Override
    public void postHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception
    {
    }

    /**
     * 可以根据ex是否为null判断是否发生了异常，进行日志记录.
     * 
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    public void afterCompletion(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception ex)
            throws Exception
    {
    }

}
