package com.xiangrui.lmp.init;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.xiangrui.lmp.business.admin.menu.service.MenuService;
import com.xiangrui.lmp.business.admin.menu.vo.Menu;
import com.xiangrui.lmp.business.admin.role.service.RoleMenuService;
import com.xiangrui.lmp.business.admin.role.service.RoleOverseasAddressService;
import com.xiangrui.lmp.business.admin.role.service.UserRoleService;
import com.xiangrui.lmp.business.admin.role.vo.RoleMenu;
import com.xiangrui.lmp.business.admin.role.vo.RoleOverseasAddress;
import com.xiangrui.lmp.business.admin.role.vo.UserRole;
import com.xiangrui.lmp.business.admin.user.service.UserService;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.homepage.service.FrontCityService;
import com.xiangrui.lmp.business.homepage.vo.FrontCity;

public class SystemParamUtil
{

    private static Map<String, Object> dataMap = new HashMap<String, Object>();

    /**
     * 权限集合
     */
    private static Map<String, Object> authorityMap = new HashMap<String, Object>();

    public void load()
    {
        loadCity();
        loadUserMenuMap();
    }
    
    private SystemParamUtil()
    {
    }

    private static SystemParamUtil instance;

    public synchronized static SystemParamUtil getInstance()
    {
        if (instance == null)
        {
            instance = new SystemParamUtil();
        }
        return instance;
    }

    public static Object getData(String key)
    {
        return dataMap.get(key);
    }

    public static Object getAuthority(String key)
    {
        return authorityMap.get(key);
    }

    public synchronized void loadCity()
    {
        // 城市地址信息
        FrontCityService frontCityService = (FrontCityService) SpringContextHolder
                .getBean("frontCityService");
        List<FrontCity> list = frontCityService.queryAllCity();

        int father_id = 0;
        String i_framework = "0";
        configIframework(list, father_id, i_framework);
        dataMap.put("frontCityList", list);

    }

    /**
     * 配置用户菜单权限
     * 
     * @param menuList
     * @param userRoles
     * @param roleMenus
     * @param user_id
     * @return
     */
    private synchronized List<Menu> configUserMenus(List<UserRole> userRoles,
            List<RoleMenu> roleMenus, int user_id)
    {

        List<UserRole> currUserRoles = new ArrayList<UserRole>();

        // 当前用户角色
        for (UserRole userRole : userRoles)
        {
            if (userRole.getUser_id() == user_id)
            {
                currUserRoles.add(userRole);
            }
        }

        Set<Integer> menuIds = new HashSet<Integer>();

        for (UserRole userRole : currUserRoles)
        {
            int roleId = userRole.getRole_id();
            for (RoleMenu roleMenu : roleMenus)
            {

                // 菜单属于角色，且菜单并未被被放入集合
                if (roleId == roleMenu.getRole_id())
                {
                    menuIds.add(roleMenu.getMenu_Id());
                }
            }
        }
        List<Menu> listParent = new ArrayList<Menu>();
        Map<String, List<Menu>> childMenuMap = new HashMap<String, List<Menu>>();

        MenuService menuService = (MenuService) SpringContextHolder
                .getBean("menuService");

        List<Menu> menuList = menuService.queryAll();

        for (Integer menu_id : menuIds)
        {
            for (Menu menu : menuList)
            {
                // 找到关联中的菜单对象
                if (menu_id == menu.getId())
                {

                    // 父级菜单
                    if ("0".equals(menu.getParent_id()))
                    {
                        listParent.add(menu);
                    } else
                    {
                        // 在非父级菜单中,找到有链接的,为第二级菜单
                        // 没有链接的,而且不是父级菜单,那些为具体权限的体现
                        // 第二级菜单的link格式为 "/"+功能名+"/"+页面 组合成为首次查询的 链接名
                        if (null != menu.getLink()
                                && !"".equals(menu.getLink())
                                && menu.getLink().indexOf("/") != -1)
                        {

                            String key = menu.getParent_id();
                            if (childMenuMap.containsKey(key))
                            {
                                List<Menu> childList = childMenuMap.get(key);
                                childList.add(menu);
                            } else
                            {
                                List<Menu> childList = new ArrayList<Menu>();
                                childList.add(menu);
                                childMenuMap.put(key, childList);
                            }
                        }
                    }
                }
            }
        }
        Collections.sort(listParent);
        // 获取父级菜单的子级菜单
        for (Menu parentMenu : listParent)
        {
            String key = String.valueOf(parentMenu.getId());
            List<Menu> childList = childMenuMap.get(key);
            Collections.sort(childList);
            parentMenu.setMenus(childList);
        }
        return listParent;
    }

    private synchronized List<RoleOverseasAddress> getUserOverseasAddressList(
            List<UserRole> userRoles, int user_id)
    {

        RoleOverseasAddressService roleOverseasAddressService = (RoleOverseasAddressService) SpringContextHolder
                .getBean("roleOverseasAddressService");

        Map<Integer, RoleOverseasAddress> overseasAddressMap = new HashMap<Integer, RoleOverseasAddress>();

        for (UserRole userRole : userRoles)
        {
            if (userRole.getUser_id() != user_id)
            {
                continue;
            }
            RoleOverseasAddress roleOverseasAddress = new RoleOverseasAddress();
            roleOverseasAddress.setRole_id(userRole.getRole_id());
            List<RoleOverseasAddress> dataList = roleOverseasAddressService
                    .queryRoleOverseasAddressByRoleId(roleOverseasAddress);

            for (RoleOverseasAddress data : dataList)
            {

                if (!overseasAddressMap.containsKey(data.getId())
                        && data.getRole_id() == userRole.getRole_id())
                {
                    overseasAddressMap.put(data.getId(), data);
                }
            }
        }
        Collection<RoleOverseasAddress> coll = overseasAddressMap.values();
        RoleOverseasAddress[] roleOverseasAddressArray = new RoleOverseasAddress[coll
                .size()];
        coll.toArray(roleOverseasAddressArray);

        List<RoleOverseasAddress> roleOverseasAddressList = Arrays
                .asList(roleOverseasAddressArray);
        return roleOverseasAddressList;
    }

    /**
     * 加载用户菜单及权限
     */
    public synchronized void loadUserMenuMap()
    {
        UserService userService = (UserService) SpringContextHolder
                .getBean("userService");

        UserRoleService userRoleService = (UserRoleService) SpringContextHolder
                .getBean("userRoleService");

        RoleMenuService roleMenuService = (RoleMenuService) SpringContextHolder
                .getBean("roleMenuService");

        // 获取所有的用户角色
        List<UserRole> userRoles = null;
        userRoles = userRoleService.queryAll();

        // 获取角色菜单
        List<RoleMenu> roleMenus = null;
        roleMenus = roleMenuService.queryAllRoleMenu();

        Map<String, Object> params = new HashMap<String, Object>();

        // 获取所有的用户
        List<User> userList = userService.queryAll(params);

        Map<Integer, List<Menu>> userMenuMap = new HashMap<Integer, List<Menu>>();

        Map<Integer, List<RoleOverseasAddress>> userOverseasAddressMap = new HashMap<Integer, List<RoleOverseasAddress>>();

        // 拼装菜单权限及海外仓库权限
        for (User user : userList)
        {
            int user_id = user.getUser_id();
            List<Menu> userMenuList = configUserMenus(userRoles, roleMenus,
                    user_id);

            // 菜单权限
            userMenuMap.put(user.getUser_id(), userMenuList);

            // 海外仓库权限
            List<RoleOverseasAddress> roleOverseasAddressList = getUserOverseasAddressList(
                    userRoles, user_id);

            userOverseasAddressMap.put(user.getUser_id(),
                    roleOverseasAddressList);
        }

        // 拼装用户按钮操作权限
        Map<Integer, List<Menu>> optionsMap = new HashMap<Integer, List<Menu>>();

        for (User user : userList)
        {
            int user_id = user.getUser_id();
            // 获取用户菜单
            List<Menu> userMenuList = userMenuMap.get(user.getUser_id());

            List<Menu> options = new ArrayList<Menu>();

            for (Menu userMenu : userMenuList)
            {
                // 获取该菜单的按钮权限
                options.addAll(getOptions(userRoles, userMenu.getMenus(),
                        roleMenus, user_id));
            }
            optionsMap.put(user_id, options);
        }

        authorityMap.clear();

        // 操作权限
        authorityMap.put("optionsMap", optionsMap);

        // 菜单权限
        authorityMap.put("userMenuMap", userMenuMap);

        // 仓库权限
        authorityMap.put("userOverseasAddressMap", userOverseasAddressMap);
    }

    /**
     * 页面操作权限
     * 
     * @param menuList
     * @param childList
     * @return
     */
    private synchronized List<Menu> getOptions(List<UserRole> userRoles,
            List<Menu> userMenuList, List<RoleMenu> roleMenus, int user_id)
    {
        Set<Integer> set = new HashSet<Integer>();

        for (UserRole userRole : userRoles)
        {
            if (userRole.getUser_id() == user_id)
            {
                for (RoleMenu roleMenu : roleMenus)
                {

                    if (userRole.getRole_id() == roleMenu.getRole_id())
                    {
                        set.add(roleMenu.getMenu_Id());
                    }
                }
            }
        }

        List<Menu> list = new ArrayList<Menu>();

        MenuService menuService = (MenuService) SpringContextHolder
                .getBean("menuService");

        List<Menu> menuList = menuService.queryAll();

        for (Menu menu : menuList)
        {
            for (Menu userMenu : userMenuList)
            {
                // 菜单操作权限
                if (null != menu.getParent_id()
                        && set.contains(menu.getId())
                        && Integer.parseInt(menu.getParent_id()) == userMenu
                                .getId())
                {
                    list.add(menu);
                }
            }
        }
        return list;
    }

    private synchronized static void configIframework(List<FrontCity> list,
            int father_id, String i_framework)
    {
        for (FrontCity city : list)
        {
            if (city.getFather_id() == father_id)
            {
                city.setI_framework(i_framework);
                String childi_framework = city.getI_framework() + "_"
                        + city.getCity_id();
                configIframework(list, city.getCity_id(), childi_framework);
            }
        }
    }
}
