package com.xiangrui.lmp.init;

import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import sun.misc.BASE64Encoder;

public class Encrypt
{
    private Key key;
    private byte[] byteMi = null;
    private byte[] byteMing = null;
    private String strMi = "";
    private String strM = "";

    
    private static Encrypt instance=null;
    
    public static Encrypt getInstance(){
        
        if(instance==null){
            instance=new Encrypt();
            
        }
         return instance;
    }

 
    
    public void setKey(String strKey)
    {
        try
        {
            KeyGenerator _generator = KeyGenerator.getInstance("DES");
            _generator.init(new SecureRandom(strKey.getBytes()));
            key = _generator.generateKey();
            _generator = null;
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    // 加密String明文输入,String密文输出
    public   void  setEncString(String strMing)
    {
        BASE64Encoder base64en = new BASE64Encoder();
        try
        {
            this.byteMing = strMing.getBytes("UTF8");
            this.byteMi = this.getEncCode(this.byteMing);
            this.strMi = base64en.encode(this.byteMi);
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {

            this.byteMing = null;
            this.byteMi = null;
        }
    }

    // 加密以byte[]明文输入,byte[]密文输出
    private byte[] getEncCode(byte[] byteS)
    {
        byte[] byteFina = null;
        Cipher cipher;
        try
        {
            cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byteFina = cipher.doFinal(byteS);
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            cipher = null;
        }

        return byteFina;
    }

    // 解密以byte[]密文输入,以byte[]明文输出
    private byte[] getDesCode(byte[] byteD)
    {
        Cipher cipher;
        byte[] byteFina = null;
        try
        {
            cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byteFina = cipher.doFinal(byteD);
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            cipher = null;
        }
        return byteFina;
    }

    // 返回加密后的密文strMi
    public String getStrMi()
    {
        return strMi;
    }

    // 返回解密后的明文
    public String getStrM()
    {
        return strM;
    }
    
    public static void main(String[] args)
    {
        
        Encrypt enc=  getInstance();
        
        //明文加密：
        String key = "06"; //初始化密钥，该秘钥硬编码，用户看不到该秘钥信息。
        enc.setKey(key);    //调用set函数设置密钥。
        enc.setEncString("1111111");//将要加密的明文传送给Encrypt.java进行加密计算。
        String Mi=enc.getStrMi();  //调用get函数获取加密后密文。
        System.out.println("xxxenc:"+Mi);
        //将密码、及报文通过加密算法生密文，再获取md5字符串，然后系统拿到报文，对内容进行加密， 并做md5完成加密
    }
}
