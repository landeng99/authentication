package com.xiangrui.lmp.task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.admin.userlog.service.UserLogService;
import com.xiangrui.lmp.business.admin.userlog.vo.UserLog;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.init.SpringContextHolder;

/**
 * 用户操作日志的定时删除任务
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-6-11 下午5:00:38
 *         </p>
 */
public class UserLogDeleteTask extends TimerTask
{
    /**
     * log4c日志
     */
    Logger logger = Logger.getLogger(UserLogDeleteTask.class);

    @Override
    public void run()
    {
        Date date = new Date();

        logger.info("开始本次用户操作日志的删除,开始时间为:"
                + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));

        // 获取数据处理service
        UserLogService service = SpringContextHolder.getBean("userLogService");

        // 获取配置的天数
        //int dayNum = 0 - 30;
        int dayNum = 0 - SYSConstant.USER_LOG_DEL_DAYS;

        // 配置转换参数
        Calendar startDT = Calendar.getInstance();
        startDT.setTime(date);
        startDT.add(Calendar.DAY_OF_MONTH, dayNum);
        String create_time = new SimpleDateFormat("yyyy-MM-dd").format(startDT
                .getTime());

        // 开始清理
        UserLog log = new UserLog();
        log.setCreate_time(create_time);
        service.clear(log);

        logger.info("结束本次用户操作日志的删除,结束时间为:"
                + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        .format(new Date()));
    }
}
