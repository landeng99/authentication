package com.xiangrui.lmp.task;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.store.service.FrontUserService;
import com.xiangrui.lmp.business.admin.store.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.service.FrontWithdrawalLogService;
import com.xiangrui.lmp.business.homepage.vo.FrontAccountLog;
import com.xiangrui.lmp.init.SpringContextHolder;
import com.xiangrui.lmp.util.CommonUtil;

/**
 * 快速支付定时器
 * <p>
 * 
 * @author <b>hyh</b>
 *         </p>
 *         <p>
 *         2015-8-4 上午11:35:13
 *         </p>
 */
public class FastPaymentTask extends TimerTask
{
	/**
	 * 总运费
	 */
	private static final String FLAG_TRANSPORT_COST = "1";

	/**
	 * 关税
	 */
	//private static final String FLAG_CUSTOMS_COST = "2";

	Logger logger = Logger.getLogger(FastPaymentTask.class);

	@Override
	public void run()
	{
		logger.info("---------------快速支付开始----------------");
		try
		{

			// 获取数据处理service
			PackageService packageService = SpringContextHolder.getBean("packageService");

			// 获取数据处理service
			FrontUserService frontUserService = SpringContextHolder.getBean("frontUserService");

			// 获取数据处理service
			//PkgGoodsService pkgGoodsService = SpringContextHolder.getBean("frontPkgGoodsService");

			Map<String, Object> params = new HashMap<String, Object>();
//			params.put("pay_status", Pkg.PAYMENT_UNPAID);
			params.put("pay_status_freight", Pkg.PAYMENT_FREIGHT_UNPAID);
			params.put("pay_type", Pkg.PAY_TYPE_BALANCE);
			params.put("virtual_pay_flag", -1);

			List<Pkg> pkgListTransportCost = packageService.selectPackageForFastPayment(params);

			for (Pkg pkg : pkgListTransportCost)
			{

				FrontUser frontUser = frontUserService.queryFrontUserById(pkg.getUser_id());

				if (pkg.getTransport_cost() > 0.00001f && frontUser.getAble_balance() > pkg.getTransport_cost())
				{
					// 账户余额
					frontUser.setAble_balance(frontUser.getAble_balance() - pkg.getTransport_cost());
					frontUser.setBalance(frontUser.getBalance() - pkg.getTransport_cost());

					FrontAccountLog frontAccountLog = getFrontAccountLog(pkg.getLogistics_code(), pkg.getTransport_cost(), frontUser.getUser_id(), FLAG_TRANSPORT_COST);

					// 包裹支付状态 已支付
					pkg.setPay_status(Pkg.PAYMENT_PAID);
//					Pkg.setPaid(pkg);
					pkg.setPay_status_freight(Pkg.PAYMENT_FREIGHT_PAID);
					// 物流状态:待发货
					if( Pkg.PAYMENT_CUSTOM_PAID==pkg.getPay_status_custom() )
					    pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING);
					packageService.taskFastPayment(pkg, frontUser, frontAccountLog, null);
					logger.info("用户：" + frontUser.getUser_name() + "      包裹：" + pkg.getLogistics_code() + "     运费：" + pkg.getTransport_cost());
				}
			}

//			Map<String, Object> paramsA = new HashMap<String, Object>();
//			paramsA.put("pay_status", Pkg.PAYMENT_CUSTOM_UNPAID);
//			paramsA.put("pay_type", Pkg.PAY_TYPE_BALANCE);

			//List<Pkg> pkgListCustomsCost = packageService.selectPackageForFastPayment(paramsA);

//			for (Pkg pkg : pkgListCustomsCost)
//			{
//
//				FrontUser frontUser = frontUserService.queryFrontUserById(pkg.getUser_id());
//
//				List<PkgGoods> pkgGoodsList = pkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());
//
//				float customs_cost = 0l;
//
//				for (PkgGoods pkgGoods : pkgGoodsList)
//				{
//					customs_cost += pkgGoods.getCustoms_cost();
//					pkgGoods.setPaid(pkgGoods.getPaid() + pkgGoods.getCustoms_cost());
//					pkgGoods.setCustoms_cost(0);
//				}
//
//				if (customs_cost > 0.00001f && frontUser.getAble_balance() > customs_cost)
//				{
//					// 账户余额
//					frontUser.setAble_balance(frontUser.getAble_balance() - customs_cost);
//					frontUser.setBalance(frontUser.getBalance() - customs_cost);
//
//					// 登陆账户记录
//					FrontAccountLog frontAccountLog = getFrontAccountLog(pkg.getLogistics_code(), customs_cost, frontUser.getUser_id(), FLAG_CUSTOMS_COST);
//
//					// 包裹支付状态 已支付
//					pkg.setPay_status(Pkg.PAYMENT_CUSTOM_PAID);
//
//					packageService.taskFastPayment(pkg, frontUser, frontAccountLog, pkgGoodsList);
//					logger.info("用户：" + frontUser.getUser_name() + "      包裹：" + pkg.getLogistics_code() + "     关税：" + customs_cost);
//				}
//			}

			// 虚拟预付款结算处理
			Map<String, Object> paramsB = new HashMap<String, Object>();
//			paramsB.put("pay_status", Pkg.PAYMENT_UNPAID);
			paramsB.put("pay_status_freight", Pkg.PAYMENT_FREIGHT_UNPAID);
			paramsB.put("pay_type", Pkg.PAY_TYPE_BALANCE);
			paramsB.put("virtual_pay_flag", 1);

			List<Pkg> pkgListVirtualPay = packageService.selectPackageForFastPayment(paramsB);

			for (Pkg pkg : pkgListVirtualPay)
			{

				FrontUser frontUser = frontUserService.queryFrontUserById(pkg.getUser_id());
				float ableBalance = frontUser.getAble_balance();
				float frozenBalance = frontUser.getFrozen_balance();
				float total = ableBalance + frozenBalance;
				float transport_cost = pkg.getTransport_cost();
				float virtual_pay = pkg.getVirtual_pay();

				if (virtual_pay > 0.00001f && total > transport_cost&&transport_cost>0.00001f)
				{
					float less = transport_cost - virtual_pay;
					ableBalance = ableBalance - less;
					frozenBalance = frozenBalance - virtual_pay;
					if (frozenBalance < 0)
					{
						frozenBalance = 0;
					}
					// 账户余额
					frontUser.setAble_balance(ableBalance);
					frontUser.setBalance(ableBalance + frozenBalance);
					frontUser.setFrozen_balance(frozenBalance);

					FrontAccountLog frontAccountLog = getFrontAccountLog(pkg.getLogistics_code(), pkg.getTransport_cost(), frontUser.getUser_id(), FLAG_TRANSPORT_COST);
					logger.info("VirtualPay --- 用户：" + frontUser.getUser_name() + "      包裹：" + pkg.getLogistics_code() + "     运费：" + pkg.getTransport_cost()+ "     预付款运费：" + pkg.getVirtual_pay()+ "     账户总额：" + frontUser.getBalance()+ "     账户可用余额：" + frontUser.getAble_balance()+ "     账户冻结额：" + frontUser.getFrozen_balance());
					// 包裹支付状态 已支付
//					pkg.setPay_status(Pkg.PAYMENT_PAID);
					Pkg.setPaid(pkg);
					pkg.setVirtual_pay(-1);
					// 物流状态:待发货
					pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING);
					packageService.taskFastPayment(pkg, frontUser, frontAccountLog, null);
					logger.info("用户：" + frontUser.getUser_name() + "      包裹：" + pkg.getLogistics_code() + "     运费：" + pkg.getTransport_cost());
				}
//				else
//				{
//					pkg.setPay_status(Pkg.PAYMENT_VIRTUAL_PAID_FAILURE);
//					packageService.update(pkg, null);
//				}

			}
			
			
			
			//冻结金额解冻任务（当用户存在冻结金额，但是并没有预捐款包裹需要处理，同时也没有提现申请时需要解冻金额）
			FrontWithdrawalLogService frontWithdrawalLogService = SpringContextHolder.getBean("frontWithdrawalLogService");
			List<FrontUser> frozenBalanceFrontUserList=frontUserService.queryFrozenBalanceFrontUser();
			if(frozenBalanceFrontUserList!=null&&frozenBalanceFrontUserList.size()>0)
			{
				for(FrontUser frongUser:frozenBalanceFrontUserList)
				{
					int withdrawalLogCount=frontWithdrawalLogService.countWithdrawalLogByFrontUserId(frongUser.getUser_id());
					int needVirtualPayPackageCount=packageService.countNeedVirtualPayPackageByFrontUserId(frongUser.getUser_id());
					if(withdrawalLogCount==0&&needVirtualPayPackageCount==0)
					{
						logger.info("unfrozen --- 解冻用户：" + frongUser.getUser_name() + "      冻结金额：" + frongUser.getFrozen_balance());
						frongUser.setAble_balance(frongUser.getAble_balance()+frongUser.getFrozen_balance());
						frongUser.setFrozen_balance(0);
						try{
							packageService.updateBalance(frongUser);
						}catch (Exception dd)
						{
							logger.info("正常解冻处理");
						}
						
					}
				}
			}

		} catch (Exception e)
		{
			logger.info("更新信息发生异常");
			e.printStackTrace();
		}
		logger.info("---------------快速支付结束----------------");
	}

	/**
	 * 账户记录
	 * 
	 * @param logistics_code
	 *            公司运单号
	 * @param amount
	 *            消费金额
	 * @param user_id
	 *            前台用户
	 * @param flag
	 *            1 总运费 2 关税
	 * @return
	 */
	private FrontAccountLog getFrontAccountLog(String logistics_code, float amount, int user_id, String flag)
	{

		// 登陆账户记录
		FrontAccountLog frontAccountLog = new FrontAccountLog();

		// 公司运单号
		frontAccountLog.setLogistics_code(logistics_code);
		// 虚拟订单
		String order_id = CommonUtil.getCurrentDateNum();
		frontAccountLog.setOrder_id(order_id);
		// 交易金额
		frontAccountLog.setAmount(amount);
		// 时间
		frontAccountLog.setOpr_time(new Timestamp(System.currentTimeMillis()));
		// 用户
		frontAccountLog.setUser_id(user_id);
		// 交易类型
		frontAccountLog.setAccount_type(FrontAccountLog.ACCOUNT_TYPE_CONSUME);
		// 交易成功
		frontAccountLog.setStatus(FrontAccountLog.SUCCESS);

		frontAccountLog.setBalance_amount(amount);
		frontAccountLog.setAlipay_amount(0);
		// 描述
		if (FLAG_TRANSPORT_COST.equals(flag))
		{
			frontAccountLog.setDescription("运费 快速支付(自动)");
		}
		else
		{
			frontAccountLog.setDescription("关税 快速支付(自动)");
		}

		return frontAccountLog;
	}

}
