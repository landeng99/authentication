package com.xiangrui.lmp.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.admin.message.service.MessageSendService;
import com.xiangrui.lmp.business.admin.warningSmsLog.service.WarningSmsLogService;
import com.xiangrui.lmp.business.admin.warningSmsLog.vo.WarningSmsLog;
import com.xiangrui.lmp.init.SpringContextHolder;

public class SmsAutoResendTask extends TimerTask
{
	/**
	 * log4c日志
	 */
	Logger logger = Logger.getLogger(SmsAutoResendTask.class);

	@Override
	public void run()
	{
		Date date = new Date();

		logger.info("开始本次短信自动发送任务,开始时间为:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));

		// 获取数据处理service
		WarningSmsLogService warningSmsLogService = SpringContextHolder.getBean("warningSmsLogService");
		Map<String, Object> paremt = new HashMap<String, Object>();
		paremt.put("status", -1);
		List<WarningSmsLog> needResendList = warningSmsLogService.queryWarningSmsLog(paremt);
		if (needResendList != null && needResendList.size() > 0)
		{
			MessageSendService messageSendService = SpringContextHolder.getBean("messageSendService");
			for (WarningSmsLog warningSmsLog : needResendList)
			{
				messageSendService.resendWarnSMS(warningSmsLog, true);
			}
		}
		logger.info("结束本次短信自动发送任务,结束时间为:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}

}
