package com.xiangrui.lmp.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.admin.accountbalance.service.AccountBalanceService;
import com.xiangrui.lmp.business.admin.accountbalance.vo.AccountBalance;
import com.xiangrui.lmp.business.base.BaseAccountBalance;
import com.xiangrui.lmp.init.SpringContextHolder;
import com.xiangrui.lmp.util.AccountBalanceTimeUtil;

/**
 * 金额记录定时器
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-4 上午11:35:13
 * </p>
 */
public class BalanceLogTask extends TimerTask
{

    Logger logger = Logger.getLogger(BalanceLogTask.class);
    /**
     * 运行次数
     */
    private long runIndex = 0;
    
    /**
     * 第几次才开始运行开始
     */
    private static final int INIT_RUN_COUNT = 0;
    
    /**
     * 按月统计,起始时间为1号
     */
    private static final int DATE_NUM = 2;
    
    @Override
    public void run()
    {
        logger.info("当前金额统计计算开始");
        logger.info("这是第"
                + (++runIndex)
                + "次运行;当前时间为:"
                + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        .format(new Date()));
        try
        {
            // 第一次不更新,是因为发布程序当天是没有数据的
            // 第二次不更新是因为对于当前来说,前台是查不到数据的,正式统计,应该是程序运行一天了的
            if (runIndex > INIT_RUN_COUNT)
            {
                // 获取数据处理service
                AccountBalanceService service = SpringContextHolder
                        .getBean("accountBalanceService");
                
                logger.info("开始昨天的记录统计");
                
                service.proAcountBalancePen();
               
                logger.info("结束昨天的记录统计");

                // 统计月数据
                Date date = new Date();
                SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
                int dayNum = Integer.parseInt(dayFormat.format(date));
                
                // 当日期出现了,某月1日时
                if (dayNum == DATE_NUM)
                {
                    SimpleDateFormat ymdFormat = new SimpleDateFormat(
                            "yyyy-MM-dd");

                    Map<String, Object> params = new HashMap<String, Object>();

                    // 获取昨天,即月底,结束时间
                    String par_end_time = ymdFormat
                            .format(AccountBalanceTimeUtil.addDay(date, -DATE_NUM));
                    params.put("END_TIME", par_end_time);
                    
                    Map<String, Object> paramBalas = new HashMap<String, Object>();
                    paramBalas.put("START_TIME", par_end_time);
                    paramBalas.put("IS_USER_ID", BaseAccountBalance.IS_USER_ID_PARAMS_FALSE);
                    List<AccountBalance> bala = service.queryAll(paramBalas);
                    int accountBalanceId = 0;
                    if(bala.size()>0){
                        accountBalanceId = bala.get(0).getBalance_id();
                    }
                    // 获取上月最后一天的所有用户的记录,并统计上月的余额,上上月的总余额,上月的差额
                    AccountBalance balanceSum = service.queryAllBalance(params);
                    if(null == balanceSum){
                        balanceSum = new AccountBalance();
                    }
                    
                    // 获取上月月初,开始时间
                    String par_start_time = ymdFormat
                            .format(AccountBalanceTimeUtil.addMonth(date, -1));
                    params.put("START_TIME", par_start_time);
                    
                    // 获取上月所有用户的各种金额操作,统计出总充值,总消费,总提现,总退费
                    AccountBalance balanceLog = service.queryAllTotal(params);

                    // 合并统计到对象.用户id设置为0来标记为所有用户的记录
                    AccountBalance balanceTotal = new AccountBalance(accountBalanceId, 0,
                            balanceLog.getLast_balance(),
                            balanceLog.getCurr_balance(),
                            (String) params.get("START_TIME"),
                            balanceSum.getTotal_expend(),
                            balanceSum.getTotal_recharge(),
                            balanceSum.getTotal_withdraw(),
                            balanceSum.getTotal_claim_cost(),
                            (String) params.get("END_TIME"),
                            balanceLog.getDifference());
                    // 增加上月记录
                    logger.info("开始上个月的总数据记录统计");
                    
                    service.logAccountBalance(balanceTotal);
                    
                    logger.info("结束上个月的总数据记录统计");
                }
            } else
            {
                logger.info("统计任务尚未正式计算");
            }
        } catch (Exception e)
        {
            logger.info("解析信息发生异常");
            e.printStackTrace();
        }
        logger.info("当前金额统计计算结束");
    }
}
