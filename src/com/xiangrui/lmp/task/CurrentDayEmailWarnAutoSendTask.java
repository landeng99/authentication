package com.xiangrui.lmp.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.admin.message.service.MessageSendService;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.sysSetting.service.SysSettingService;
import com.xiangrui.lmp.init.SpringContextHolder;
import com.xiangrui.lmp.util.NumberUtils;

public class CurrentDayEmailWarnAutoSendTask extends TimerTask
{
	/**
	 * log4c日志
	 */
	Logger logger = Logger.getLogger(CurrentDayEmailWarnAutoSendTask.class);

	@Override
	public void run()
	{
		Date date = new Date();

		logger.info("开始本次邮件提醒付款自动发送任务,开始时间为:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));

		SysSettingService sysSettingService = SpringContextHolder.getBean("sysSettingService");

		if (sysSettingService.isInEmailAllowTimeRange())
		{
			// 获取数据处理service
			PackageService packageService = SpringContextHolder.getBean("packageService");
			Map<String, Object> paremt = new HashMap<String, Object>();
			paremt.put("status", -1);
			List<Pkg> needWarnRecordList = packageService.queryCurrentDayEmailWarnToPayRecord();
			if (needWarnRecordList != null && needWarnRecordList.size() > 0)
			{
				MessageSendService messageSendService = SpringContextHolder.getBean("messageSendService");
				for (Pkg pkg : needWarnRecordList)
				{
					String needPayMenoy = NumberUtils.scaleMoneyData(pkg.getNeed_pay_money());
					String receriver_email=pkg.getEmail();
					messageSendService.sendPayWarnEmail(pkg.getUser_name(), receriver_email, pkg.getAccount(), pkg.getPackage_count(), needPayMenoy);
				}
			}
		}
		else
		{
			logger.info("当前时间不在自动邮件设置范围内，此自动任务不执行。。。");
		}

		logger.info("结束本次邮件提醒付款自动发送任务,结束时间为:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
	}

}
