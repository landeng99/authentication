package com.xiangrui.lmp.util;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.constant.SYSConstant;

/**
 * 可以为每个 servlet 存储额外数据的容器
 * @author yeyi771
 */
public class ServletContainer {
	public final static String AJAX_HEADER = "x-requested-with";
	public final static String XMLHTTPREQUEST = "XMLHttpRequest";  
	
	public static boolean isAjaxRequest() {
		return isAjaxRequest(getHttpServletRequest());
	}
	
	public static boolean isAjaxRequest(HttpServletRequest request) {
		String requestType = request.getHeader(AJAX_HEADER);
		return XMLHTTPREQUEST.equals(requestType);
	}

	public static HttpSession getHttpSession() {
		return getHttpServletRequest().getSession();
	}
	
	public static HttpServletRequest getHttpServletRequest() {
		return ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
	}
	
	public static String[] getParameterValues(String parameterName) {
		return getHttpServletRequest().getParameterValues(parameterName);
	}
	
	public static String getParameterValue(String parameterName) {
		return getHttpServletRequest().getParameter(parameterName);
	}
	
	public static Object getAttribute(String attributeName) {
		return getHttpServletRequest().getAttribute(attributeName);
	}
	
	public static void setAttribute(String key, Object obj){
		getHttpServletRequest().setAttribute(key, obj);
	}
	
	public static Object getObjectInSession(String key) {
		return getHttpSession().getAttribute(key);
	}
	
	public static void setObjectInSession(String key, Object obj) {
		getHttpSession().setAttribute(key, obj);
	}
	
	public static void jsonWrite(Object model, HttpServletResponse response) {
		MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
		MediaType contentType = MediaType.APPLICATION_JSON;
		response.setCharacterEncoding("UTF-8");
		HttpOutputMessage outputMessage = new ServletServerHttpResponse(response);
		try {
			converter.write(model, contentType, outputMessage);
		} catch (HttpMessageNotWritableException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
//	验证码判断
//	public static boolean validateCode(HttpServletRequest request) {
//		String validateCode = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
//		String userValidateCode = request.getParameter("validateCode");
//		if(validateCode != null && userValidateCode != null && validateCode.equalsIgnoreCase(userValidateCode)){
//			return true;
//		}else if("pass".equals(userValidateCode)){
//			return true;
//		}else{
//			return false;
//		}
//	}

    /**
     * 获得当前请求的传参
     * 
     * @param request 当前请求实例
     * @return 传参
     */
    public static String getQueryString(HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        StringBuffer buffer = new StringBuffer();
        for (String key : params.keySet()) {
            String[] values = params.get(key);
            int i = 0;
            int countValues = values.length;
            for (; i < countValues; i++) {
                buffer.append(key);
                buffer.append("=");
                buffer.append(values[i]==null || values[i].trim() == ""?"":values[i]);
                buffer.append("&");
            }
        }
        String queryString = buffer.toString();
        if (queryString.equals("")) {
            return queryString;
        } 
        else {
            return queryString.substring(0, queryString.length() - 1);
        }
    }
    
    public static User getCurBackUser(){
    	return (User) getObjectInSession(SYSConstant.SESSION_USER);
    }
    
    public static void setCurBackUser(User u){
    	setObjectInSession(SYSConstant.SESSION_USER,u);
    }
}
