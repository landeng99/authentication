package com.xiangrui.lmp.util.bean;

/**
 * 运单查询的的bean对象,配合jsontool使用的
 * <p>@author <b>hsjing</b></p>
 * <p>2015-7-10 上午10:54:04</p>
 */
public class PkgLogExpress
{
    private String time;
    private int status;
    private String context;
    private String ftime;
	public String getTime()
    {
        return time;
    }
    public void setTime(String time)
    {
        this.time = time;
    }
    public int getStatus()
    {
        return status;
    }
    public void setStatus(int status)
    {
        this.status = status;
    }
    public String getContext()
    {
        return context;
    }
    public void setContext(String context)
    {
        this.context = context;
    }
    public String getFtime()
    {
        return ftime;
    }
    public void setFtime(String ftime)
    {
        this.ftime = ftime;
    }
    @Override
    public String toString()
    {
        return "PkgLogExpress [time=" + time + ", status=" + status
                + ", context=" + context + ", ftime=" + ftime + "]";
    }
    public PkgLogExpress(String time, int status, String context, String ftime)
    {
        super();
        this.time = time;
        this.status = status;
        this.context = context;
        this.ftime = ftime;
    }
	public PkgLogExpress()
	{
		super();
	}
   
}
