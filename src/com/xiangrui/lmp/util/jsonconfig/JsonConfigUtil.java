package com.xiangrui.lmp.util.jsonconfig;
 
import net.sf.json.JsonConfig;

public class JsonConfigUtil
{
    
    public static JsonConfig getJsonConfig(AbstractJsonValueProcessor processor){
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(java.util.Date.class,processor);
        return jsonConfig;
    }
}
