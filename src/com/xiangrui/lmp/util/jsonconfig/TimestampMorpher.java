package com.xiangrui.lmp.util.jsonconfig;

import net.sf.ezmorph.object.AbstractObjectMorpher;
import java.sql.Timestamp;  
import java.text.ParseException;  
import java.text.SimpleDateFormat;  
import net.sf.ezmorph.MorphException;  
public class TimestampMorpher extends AbstractObjectMorpher
{

    /*** 日期字符串格式*/  
    private String[] formats;  
    public TimestampMorpher(String[] formats) {  
        this.formats = formats;  
    }  
    public Object morph(Object value) {  
    if( value == null){  
        return null;  
    }  
    if( Timestamp.class.isAssignableFrom(value.getClass()) ){  
        return (Timestamp) value;  
    }  
    if(supportLong(value.getClass())){
        Timestamp timestamp=  new Timestamp((Long)value);
        return timestamp;
    }
    if( !supports( value.getClass()) ){  
        throw new MorphException( value.getClass() + " 是不支持的类型");  
    }
    String strValue=(String) value;  
    SimpleDateFormat dateParser=null;
    for( int i = 0; i < formats.length ; i++ ){  
        if( null == dateParser ){  
            dateParser=new SimpleDateFormat(formats[i]);  
        }else{  
            dateParser.applyPattern(formats[i]);  
        }  
        try{  
            
            return new Timestamp( dateParser.parse( strValue.toLowerCase()).getTime() );}  
        catch (ParseException e) {  
            //e.printStackTrace();  
        }  
    }  
    return null;  
    }  
    @Override  
    public Class morphsTo() {  
        return Timestamp.class;  
    }  
    public boolean supports( Class clazz ){  
        return String.class.isAssignableFrom( clazz );  
    }  
    
    public boolean supportLong(Class clazz ){
        return Long.class.isAssignableFrom( clazz );  
    }
    
}
