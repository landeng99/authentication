package com.xiangrui.lmp.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.admin.interfacelog.service.InterfaceLogService;
import com.xiangrui.lmp.business.admin.interfacelog.vo.InterfaceLog;
import com.xiangrui.lmp.init.SpringContextHolder;

public class HttpRequest {

	private static Logger logger = Logger.getLogger(HttpRequest.class);
	/**
	 * 接口类型：1短信接口， 2快递100订阅接口， 3快递100消息同步接口 ， 4支付宝支付请求接口， 5支付宝状态同步返回接收接口，6支付宝状态异步通知接收接口，
	 * 7银联支付请求接口， 8银联同步状态返回接口 9银联异步状态返回接口
	 */

	/**
	 * 短信接口
	 */
	public static int SMS_SEND_INTERFACE = 1;

	/**
	 * 快递100订阅接口
	 */
	public static int KUAIDI100_SUB_INTERFACE = 2;

	/**
	 * 支付接口
	 */
	public static int ALIPAY_INTERFACE = 4;

	/**
	 * 银联支付请求接口
	 */
	public static int alipay_INTERFACE = 6;

	public static String addUrl(String head, String tail) {
		if (head.endsWith("/")) {
			if (tail.startsWith("/")) {
				return head.substring(0, head.length() - 1) + tail;
			} else {
				return head + tail;
			}
		} else {
			if (tail.startsWith("/")) {
				return head + tail;
			} else {
				return head + "/" + tail;
			}
		}
	}

	public synchronized static String postData(String url,
			Map<String, String> params, String codePage) throws Exception {

		final HttpClient httpClient = new HttpClient();
		httpClient.getHttpConnectionManager().getParams()
				.setConnectionTimeout(10 * 1000);
		httpClient.getHttpConnectionManager().getParams()
				.setSoTimeout(10 * 1000);

		final PostMethod method = new PostMethod(url);
		if (params != null) {
			method.getParams().setParameter(
					HttpMethodParams.HTTP_CONTENT_CHARSET, codePage);
			method.setRequestBody(assembleRequestParams(params));
		}
		String result = "";
		try {
			httpClient.executeMethod(method);
			result = new String(method.getResponseBody(), codePage);
		} catch (final Exception e) {
			throw e;
		} finally {
			method.releaseConnection();
		}
		return result;
	}

	public synchronized static String postData(String url, String codePage)
			throws Exception {
		final HttpClient httpClient = new HttpClient();
		httpClient.getHttpConnectionManager().getParams()
				.setConnectionTimeout(10 * 1000);
		httpClient.getHttpConnectionManager().getParams()
				.setSoTimeout(10 * 1000);

		final GetMethod method = new GetMethod(url);
		String result = "";
		try {
			httpClient.executeMethod(method);
			result = new String(method.getResponseBody(), codePage);
		} catch (final Exception e) {
			throw e;
		} finally {
			method.releaseConnection();
		}
		return result;
	}

	/**
	 * post方式提交 请求
	 * 
	 * @param url
	 *            请求地址
	 * @param params
	 *            参数
	 * @param codePage
	 *            编码，如UTF-8
	 * @param interfaceType
	 *            接口类型
	 * @return
	 */
	public synchronized static String postData(String url,
			Map<String, String> params, String codePage, int interfaceType) {

		// 日志对象
		InterfaceLog interfaceLog = new InterfaceLog();
		interfaceLog.setLog_id(UUIDUtil.uuid());
		interfaceLog.setStart_time(new Timestamp(System.currentTimeMillis()));

		// 将请求参数记录数据库
		String req_msg = getConfigLogMsg(params);
		interfaceLog.setReq_msg(req_msg);

		interfaceLog.setInterace_type(interfaceType);
		interfaceLog.setIp(getIp());

		String ret = null;
		String resp_msg = null;

		try {

			// 请求数据
			ret = postData(url, params, codePage);
			
			resp_msg = ret;

		} catch (Exception e) {

			// 将异常记录数据库
			resp_msg = e.getMessage();
			logger.error("接口类型为：" + interfaceType + "，请求异常：" + resp_msg);
		}

		interfaceLog.setEnd_time(new Timestamp(System.currentTimeMillis()));

		interfaceLog.setResp_msg(ret);

		// 记录日志
		log(interfaceLog);
		return ret;
	}

	/**
	 * 将请求参数拼接，为字符串
	 * 
	 * @param params
	 * @return
	 */

	private static String getConfigLogMsg(Map<String, String> params) {
		StringBuffer buffer = new StringBuffer();
		for (Entry<String, String> entry : params.entrySet()) {
			buffer.append(entry.getKey() + ":" + entry.getValue() + "\n");
		}
		return buffer.toString();
	}

	/**
	 * 组装http请求参数
	 * 
	 * @param params
	 * @param menthod
	 * @return
	 */
	private synchronized static NameValuePair[] assembleRequestParams(
			Map<String, String> data) {
		final List<NameValuePair> nameValueList = new ArrayList<NameValuePair>();

		Iterator<Map.Entry<String, String>> it = data.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> entry = (Map.Entry<String, String>) it
					.next();
			nameValueList.add(new NameValuePair((String) entry.getKey(),
					(String) entry.getValue()));
		}

		return nameValueList.toArray(new NameValuePair[nameValueList.size()]);
	}

	/**
	 * 获取本机ip
	 * 
	 * @return
	 */
	private static String getIp() {

		Enumeration allNetInterfaces;
		try {
			allNetInterfaces = NetworkInterface.getNetworkInterfaces();
			InetAddress ip = null;
			while (allNetInterfaces.hasMoreElements()) {
				NetworkInterface netInterface = (NetworkInterface) allNetInterfaces
						.nextElement();

				Enumeration addresses = netInterface.getInetAddresses();
				while (addresses.hasMoreElements()) {
					ip = (InetAddress) addresses.nextElement();
					if (ip != null && ip instanceof Inet4Address) {
						return ip.getHostAddress();
					}
				}
			}
			return ip.getHostAddress();

		} catch (SocketException e) {

			e.printStackTrace();
		}

		return null;

	}

	/**
	 * 日志记录
	 * 
	 * @param interfaceLog日志记对象
	 */
	private static void log(InterfaceLog interfaceLog) {

		InterfaceLogService interfaceLogService = SpringContextHolder
				.getBean("interfaceLogService");

		// 记录接口日志
		interfaceLogService.createInterfaceLog(interfaceLog);

	}

}
