package com.xiangrui.lmp.util;

import java.io.Serializable;

public class TokenConstants implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1099936257059468252L;

	/**
	 * token 名称
	 */
	public static String DEFAULT_TOKEN_NAME = "TK";
	
	/**
	 * token 值
	 */
	public static String TOKEN_VALUE ;
	
	/**
	 * token 失效返回请求地址
	 */
	public static String DEFAULT_TOKEN_RETURN_PAGE = "userPage/linkFailure.htm" ;
	
}