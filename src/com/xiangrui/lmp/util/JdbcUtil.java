package com.xiangrui.lmp.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * jdbc工具类,作废
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 上午9:35:47
 * </p>
 */
public class JdbcUtil
{
    /**
     * 默认的连接驱动
     */
    private static final String POOLED_URL = "jdbc:mysql://172.16.0.90:3306/lmp?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true";
    /**
     * 默认的连接数据库登录名
     */
    private static final String POOLED_USER = "root";
    /**
     * 默认的的连接数据库密码
     */
    private static final String POOLED_PASSWORD = "root";
    /**
     * 默认的的连接数据地址
     */
    private static final String POOLED_DRIVER_MANAGER = "com.mysql.jdbc.Driver";

    /**
     * 获取数据库连接
     * 
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Connection getConnection() throws InstantiationException,
            IllegalAccessException, ClassNotFoundException, SQLException
    {
        Connection connection = null;
        Class.forName(POOLED_DRIVER_MANAGER).newInstance();
        // 获取数据库连接
        connection = DriverManager.getConnection(POOLED_URL, POOLED_USER,
                POOLED_PASSWORD);
        System.out.println("成功创建connection");
        return connection;
    }

    /**
     * 获取数据库连接
     * 
     * @param driverManager
     *            连接驱动
     * @param url
     *            连接数据地址
     * @param user
     *            登录数据库名
     * @param password
     *            登录数据库密码
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Connection getConnection(String driverManager, String url,
            String user, String password) throws InstantiationException,
            IllegalAccessException, ClassNotFoundException, SQLException
    {
        Connection connection = null;
        if (POOLED_DRIVER_MANAGER.equals(driverManager))
            Class.forName(POOLED_DRIVER_MANAGER).newInstance();
        else
            Class.forName(driverManager).newInstance();
        // 获取数据库连接
        connection = DriverManager.getConnection(url, user, password);
        System.out.println("成功创建connection");
        return connection;
    }

    /**
     * 查询
     * 
     * @param con
     * @param sql
     *            完整的查询语句
     * @return 返回所有
     * @throws SQLException
     */
    public static ResultSet executeQuery(Connection con, String sql)
            throws SQLException
    {
        Statement st = con.createStatement();
        System.out.println("成功创建statement");
        System.out.println("成功创建resultSet");
        System.out.println("开始数据库对[" + sql + "]处理");
        ResultSet rs = st.executeQuery(sql);
        System.out.println("结束数据库对[" + sql + "]处理");
        return rs;
    }

    /**
     * 增加或者修改
     * 
     * @param con
     * @param sql
     *            完整的sql语句
     * @return 返回影响行数
     * @throws SQLException
     */
    public static int executeUpdate(Connection con, String sql)
            throws SQLException
    {
        Statement st = con.createStatement();
        System.out.println("成功创建statement");
        System.out.println("成功创建statement");
        System.out.println("开始数据库对[" + sql + "]处理");
        int row = st.executeUpdate(sql);
        System.out.println("结束数据库对[" + sql + "]处理");
        System.out.println("更新数据库影响行数 " + row);
        return row;
    }

    /**
     * 关闭连接
     * 
     * @param con
     * @param st
     * @param rs
     * @throws SQLException
     */
    public static void close(Connection con, Statement st, ResultSet rs)
            throws SQLException
    {
        if (rs != null)
        {
            rs.close();
            rs = null;
            System.out.println("成功关闭resultSet");
        }
        if (st != null)
        {
            st.close();
            st = null;
            System.out.println("成功关闭statement");
        }
        if (con != null)
        {
            con.close();
            con = null;
            System.out.println("成功关闭connection");
        }
    }
}
