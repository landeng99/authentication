package com.xiangrui.lmp.util.alipay.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiangrui.lmp.util.alipay.util.AlipayNotify;



/**.
 * 接收支付宝退款发来的异步通知
 * @author luyabin
 * @serial 2014 11 3
 */
@Controller
@RequestMapping(value = "/alipayRefundNotify")
@SuppressWarnings("all")
public class AlipayRefundNotifyUrl {
    
    
    /**.
     * 
     */
	//@Autowired
	//private OrderService orderService;
	/**.
	 * 
	 */
	@Autowired
	private AlipayLog alipayLog;
    
    /**.
     * 接收支付宝处理结果
     * @param request HttpServletRequest
     * @throws IOException
     * @throws IOException 
     */
   @RequestMapping(value = "/notify")
   public void alipayNotfy (HttpServletRequest request, HttpServletResponse response)
           throws IOException {

      //获取支付宝POST过来反馈信息
       Map<String,String> params = new HashMap<String,String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        
        //批次号   
        String batchNo = new String(request.getParameter("batch_no").getBytes("ISO-8859-1"),"UTF-8");
        //批量退款数据中转账成功的笔数
        String successNum = new String(request.getParameter("success_num").getBytes("ISO-8859-1"),"UTF-8");
        //批量退款数据中的详细信息
        String resultDetails = new String(request.getParameter("result_details").getBytes("ISO-8859-1"),"UTF-8");
        
	    //String batchNo = "20141106C201411051750529856";
	    //String resultDetails = "2014110656942483^0.01^success$jax_chuanhang@alipay.com^2088101003147483^0.01^SUCCESS";
        //退单编号
        String outTradeNo = batchNo.substring(8);
        System.out.println("tuikuan_dingdanbiaohao:"+outTradeNo);
        
        String[] resultFirst=resultDetails.split("\\$");
        String[] resultEnd = resultFirst[0].split("\\^");
        //支付宝交易号
        System.out.println("tuikuan_zhifubaojiaoyihao:"+resultEnd[0]);
        //处理状态
        System.out.println("tuikuan_chulizhuangt:"+resultEnd[2]);
        
        PrintWriter out =  response.getWriter();    
        if(AlipayNotify.verify(params)){//验证成功
          //写入修改成功日志
        	System.out.println("退款验证成功");
        	if("success".equals(resultEnd[2]) || "SUCCESS".equals(resultEnd[2])){
        		try {
                    //操作数据库，修改订单交易状态              	
                	//OrderVo oVo = orderService.findOrderByOcode(outTradeNo);              	
                	//orderService.updateOrderReturns(oVo.getoId());
                	//插入日志                    	
                	//alipayLog.alipayLog("修改发货状态：退货中 -> 已退货", oVo);                   
                } catch (Exception e) {
                    e.printStackTrace();
                }
        	}else{
        		//OrderVo oVo = orderService.findOrderByOcode(outTradeNo);
        		//插入日志
        		//alipayLog.alipayLog("退款notify通知,支付宝处理退款失败(处理状态不是success)，退单发货状态未修改", oVo);    
        	}                          
            out.println("success"); //请不要修改或删除
        }else{//验证失败
            out.println("fail");
        }
   }
}
