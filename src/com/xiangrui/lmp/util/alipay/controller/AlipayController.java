package com.xiangrui.lmp.util.alipay.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.Timestamp;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

import com.xiangrui.lmp.business.admin.interfacelog.service.InterfaceLogService;
import com.xiangrui.lmp.business.admin.interfacelog.vo.InterfaceLog;
import com.xiangrui.lmp.init.SpringContextHolder;
import com.xiangrui.lmp.util.UUIDUtil;
import com.xiangrui.lmp.util.alipay.util.AlipayConfig;

/**
 * . 支付宝网银接口提交类
 * 
 * @author 
 * @serial 2015年7月7日
 */

public class AlipayController
{
    private static Logger logger = Logger.getLogger(AlipayController.class);
    
    /**
     * 网银接口
     * 
     * @param request
     * @param response
     * @param out_trade_no
     *            商户订单号 (必填)
     * @param total_fee
     *            付款金额 (必填)
     *            @param subject 订单名称
     * @param body
     *            订单描述(中文出错)
     *    @param        defaultbank 银行简码请参考接口技术文档(必填)
     * @param show_url
     *            商品展示地址
     *  @param notify_url 服务器异步通知页面路径
     *  @param return_url 页面跳转同步通知页面路径
     */
    public static void bank(HttpServletRequest request,
            HttpServletResponse response, String out_trade_no,
            String total_fee,String subject, String body,String defaultbank, String show_url,
            String notify_url,String return_url)
    {

        // 必填，不能修改 支付类型
        String payment_type = "1";

        System.out.println("notify_url=" + notify_url);
        System.out.println("return_url=" + return_url);

        // 默认支付方式
        String paymethod = "bankPay";
        // 必填

        // 防钓鱼时间戳,使用请调用类文件submit中的query_timestamp函数
        String anti_phishing_key = "";

        try
        {
            anti_phishing_key = AlipaySubmit.query_timestamp();
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        // 把请求参数打包成数组
        Map<String, String> sParaTemp = new HashMap<String, String>();
        sParaTemp.put("service", "create_direct_pay_by_user");
        sParaTemp.put("partner", AlipayConfig.partner);
        sParaTemp.put("seller_email", AlipayConfig.seller_email);
        sParaTemp.put("input_charset", AlipayConfig.input_charset);
        sParaTemp.put("payment_type", payment_type);
        sParaTemp.put("notify_url", notify_url);
        sParaTemp.put("return_url", return_url);
        sParaTemp.put("out_trade_no", out_trade_no);
        sParaTemp.put("subject", subject);
        sParaTemp.put("total_fee", total_fee);
        sParaTemp.put("body", body);
        sParaTemp.put("paymethod", paymethod);
        sParaTemp.put("defaultbank", defaultbank);
        sParaTemp.put("show_url", show_url);
        sParaTemp.put("anti_phishing_key", anti_phishing_key);
        sParaTemp.put("exter_invoke_ip", getIp());

        // 建立请求
        String sHtmlText = AlipaySubmit.buildRequest(sParaTemp, "get", "确认");

        // 日志对象
        InterfaceLog interfaceLog = new InterfaceLog();
        interfaceLog.setLog_id(UUIDUtil.uuid());
        interfaceLog.setStart_time(new Timestamp(System.currentTimeMillis()));

        // 将请求参数记录数据库
        String req_msg = getConfigLogMsg(sParaTemp);
        interfaceLog.setReq_msg(req_msg);
        // 7网银请求接口
        interfaceLog.setInterace_type(7);

        interfaceLog.setIp(getIp());

        try
        {
            response.setContentType("text/html; charset=utf-8");
            PrintWriter out = response.getWriter();
            out.print(sHtmlText);
            out.flush();

        } catch (IOException e)
        {
            e.printStackTrace();
            logger.error("接口类型为：" + 7 + "，请求异常：" + e.getMessage());
        }
        interfaceLog.setEnd_time(new Timestamp(System.currentTimeMillis()));
        // 记录日志
        log(interfaceLog);

    }

    /**
     * 支付宝请求
     * 
     * @param request
     * @param response
     * @param out_trade_no
     *            商户订单号 (必填)
     * @param total_fee
     *            付款金额 (必填)
     * @param subject 订单名称
     * @param body
     *            订单描述(中文出错)
     * @param show_url
     *            商品展示地址
     *  @param notify_url 服务器异步通知页面路径
     *  @param return_url 页面跳转同步通知页面路径
     */
    public static void alipay(HttpServletRequest request,
            HttpServletResponse response, String out_trade_no,
            String total_fee,String subject, String body, String show_url,
            String notify_url,String return_url)
    {

        // 必填，不能修改 支付类型
        String payment_type = "1";

        System.out.println("notify_url=" + notify_url);
        System.out.println("return_url=" + return_url);

        // 防钓鱼时间戳,使用请调用类文件submit中的query_timestamp函数
        String anti_phishing_key = "";

        try
        {
            anti_phishing_key = AlipaySubmit.query_timestamp();
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (DocumentException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        // 把请求参数打包成数组
        Map<String, String> sParaTemp = new HashMap<String, String>();
        sParaTemp.put("service", "create_direct_pay_by_user");
        sParaTemp.put("partner", AlipayConfig.partner);
        sParaTemp.put("seller_email", AlipayConfig.seller_email);
        sParaTemp.put("input_charset", AlipayConfig.input_charset);
        sParaTemp.put("payment_type", payment_type);
        sParaTemp.put("notify_url", notify_url);
        sParaTemp.put("return_url", return_url);
        sParaTemp.put("out_trade_no", out_trade_no);
        sParaTemp.put("subject", subject);
        sParaTemp.put("total_fee", total_fee);
        sParaTemp.put("body", body);
        sParaTemp.put("show_url", show_url);
        sParaTemp.put("anti_phishing_key", anti_phishing_key);
        sParaTemp.put("exter_invoke_ip", getIp());

        // 建立请求
        String sHtmlText = AlipaySubmit.buildRequest(sParaTemp, "get", "确认");

        // 日志对象
        InterfaceLog interfaceLog = new InterfaceLog();
        interfaceLog.setLog_id(UUIDUtil.uuid());
        interfaceLog.setStart_time(new Timestamp(System.currentTimeMillis()));

        // 将请求参数记录数据库
        String req_msg = getConfigLogMsg(sParaTemp);
        interfaceLog.setReq_msg(req_msg);
        // 4支付宝支付请求接口
        interfaceLog.setInterace_type(4);

        interfaceLog.setIp(getIp());

        try
        {
            response.setContentType("text/html; charset=utf-8");
            PrintWriter out = response.getWriter();
            out.print(sHtmlText);
            out.flush();

        } catch (IOException e)
        {
            e.printStackTrace();
            logger.error("接口类型为：" + 4 + "，请求异常：" + e.getMessage());
        }
        interfaceLog.setEnd_time(new Timestamp(System.currentTimeMillis()));
        // 记录日志
        log(interfaceLog);

    }
    
    
    /**
     * 将请求参数拼接，为字符串
     * 
     * @param params
     * @return
     */

    private static String getConfigLogMsg(Map<String, String> params) {
        StringBuffer buffer = new StringBuffer();
        for (Entry<String, String> entry : params.entrySet()) {
            buffer.append(entry.getKey() + ":" + entry.getValue() + "\n");
        }
        return buffer.toString();
    }
    
    /**
     * 获取本机ip
     * 
     * @return
     */
    private static String getIp() {

        @SuppressWarnings("rawtypes")
        Enumeration allNetInterfaces;
        try {
            allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip = null;
            while (allNetInterfaces.hasMoreElements()) {
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces
                        .nextElement();

                @SuppressWarnings("rawtypes")
                Enumeration addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    ip = (InetAddress) addresses.nextElement();
                    if (ip != null && ip instanceof Inet4Address) {
                        return ip.getHostAddress();
                    }
                }
            }
            return ip.getHostAddress();

        } catch (SocketException e) {

            e.printStackTrace();
        }

        return null;

    }
    
    /**
     * 日志记录
     * 
     * @param interfaceLog日志记对象
     */
    private static void log(InterfaceLog interfaceLog) {

        InterfaceLogService interfaceLogService = SpringContextHolder
                .getBean("interfaceLogService");

        // 记录接口日志
        interfaceLogService.createInterfaceLog(interfaceLog);

    }
}
