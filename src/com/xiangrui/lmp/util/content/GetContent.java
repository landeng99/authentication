package com.xiangrui.lmp.util.content;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 仅仅保存下 request和response对象 filter过滤器
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 下午6:29:17
 * </p>
 */
public class GetContent implements Filter
{

    @Override
    public void destroy()
    {

    }

    /**
     * 过滤一下
     */
    @Override
    public void doFilter(ServletRequest arg0, ServletResponse arg1,
            FilterChain arg2) throws IOException, ServletException
    {
        // 设置request
        SysContent.setRequest((HttpServletRequest) arg0);

        // 设置response
        SysContent.setResponse((HttpServletResponse) arg1);

        // 过滤结束,正常跳转
        arg2.doFilter(arg0, arg1);
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException
    {

    }

}
