package com.xiangrui.lmp.util.content;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 保存request和response的位置
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 下午6:28:42
 * </p>
 */
public class SysContent
{

    /**
     * 
     */
    private static ThreadLocal<HttpServletRequest> requestLocal = new ThreadLocal<HttpServletRequest>();

    /**
	 * 
	 */
    private static ThreadLocal<HttpServletResponse> responseLocal = new ThreadLocal<HttpServletResponse>();

    /**
     * 获取request
     * 
     * @return
     */
    public static HttpServletRequest getRequest()
    {
        return (HttpServletRequest) requestLocal.get();
    }

    /**
     * 设置request
     * 
     * @param request
     */
    public static void setRequest(HttpServletRequest request)
    {
        requestLocal.set(request);
    }

    /**
     * 获取response
     * 
     * @return
     */
    public static HttpServletResponse getResponse()
    {
        return (HttpServletResponse) responseLocal.get();
    }

    /**
     * 设置response
     * 
     * @param response
     */
    public static void setResponse(HttpServletResponse response)
    {
        responseLocal.set(response);
    }

    /**
     * 获取session request.getSession()
     * 
     * @return
     */
    public static HttpSession getSession()
    {
        return (HttpSession) ((HttpServletRequest) requestLocal.get())
                .getSession();
    }
}
