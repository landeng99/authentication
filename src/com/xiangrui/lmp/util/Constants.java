package com.xiangrui.lmp.util;

public class Constants { 
    public static class ExpLog { 
        public static final String Exp_Log_CreateInSys = "包裹信息已录入速8系统";
        public static final String Exp_Log_CreateInDATA = "已入库";
        public static final String Exp_Log_SplitWaitSend = "包裹分拣完成等待出库";
        public static final String Exp_Log_PrintOut = "包裹已扫描出库";
        public static final String Exp_Log_SentoChina = "航空公司已揽件正飞往中国";
        public static final String Exp_Log_loadChina = "包裹已到达中国目的口岸等待清关放行";


    }

    public static class ChgFlag {
        // C：新增
        public static final String C = "C";

        // R：不变
        public static final String R = "R";

        // U：修改
        public static final String U = "U";

        // D：删除
        public static final String D = "D";
    }
     
}
