package com.xiangrui.lmp.util;

import java.math.BigDecimal;

public class IntegerUtil
{
	public static boolean isIntegerEqual(Integer a, Integer b)
	{
		boolean isEqual = false;
		if (a != null && b != null)
		{
			BigDecimal aBig = new BigDecimal(a);
			BigDecimal bBig = new BigDecimal(b);
			if (aBig.compareTo(bBig) == 0)
			{
				isEqual = true;
			}
		}
		else if (a == null && b == null)
		{
			isEqual = true;
		}
		return isEqual;
	}
}
