package com.xiangrui.lmp.util;

import java.io.Serializable;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * cookie 工具类
 * 
 * @author yunfei_li@qq.com
 * @version 1.0
 */

public class CookieUtil implements Serializable {

	/** 描述 */

	private static final long serialVersionUID = -3319135021443539594L;

	/**
	 * 
	 * 获取cookie
	 * @param request		request
	 * @param cookieName	cookie 名称
	 * @return				""
	 */
	public static String getCookieValue(HttpServletRequest request, String cookieName) {
		Cookie cookies[] = request.getCookies();
		if (cookies == null || cookieName == null || cookieName.length() == 0) {
			return null;
		}
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if (cookieName.equals(cookie.getName())) {
				return cookie.getValue();
			}
		}
		return "";
	}

	/**
	 * 
	 * 获取cookie
	 * @param request		request
	 * @param cookieName	cookie 名称
	 * @return				""
	 */
	public static Cookie getCookie(HttpServletRequest request, String cookieName) {
		Cookie cookies[] = request.getCookies();
		if (cookies == null || cookieName == null || cookieName.length() == 0) {
			return null;
		}
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if (cookieName.equals(cookie.getName())) {
				return cookie;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * 删除cookie
	 * @param request	请求
	 * @param response	响应
	 * @param cookie	cookie
	 */
	public static void deleteCookie(HttpServletRequest request,
			HttpServletResponse response, Cookie cookie) {
		if (cookie != null) {
			cookie.setPath(getPath(request));
			cookie.setValue("");
			cookie.setMaxAge(0);
			response.addCookie(cookie);
		}
	}

	/**
	 * 
	 * 设置 cookie
	 * @param request	请求
	 * @param response	响应
	 * @param name		存放cookie名称
	 * @param value		存放cookie value
	 */
	public static void setCookie(HttpServletRequest request,
			HttpServletResponse response, String name, String value) {
		setCookie(request, response, name, value, 0x278d00);
	}

	/**
	 * 
	 * 设置 cookie
	 * @param request	请求
	 * @param response	响应
	 * @param name		存放cookie名称
	 * @param value		存放cookie value
	 * @param maxAge	cookie 有效时间
	 */
	public static void setCookie(HttpServletRequest request,
			HttpServletResponse response, String name, String value, int maxAge) {
		Cookie cookie = new Cookie(name, value == null ? "" : value);
		cookie.setMaxAge(maxAge);
		cookie.setPath(getPath(request));
		response.addCookie(cookie);
	}

	/**
	 * 
	 * 获取路径
	 * @param request	请求
	 * @return			路径
	 */
	private static String getPath(HttpServletRequest request) {
		String path = request.getContextPath();
		return (path == null || path.length() == 0) ? "/" : path;
	}

}
