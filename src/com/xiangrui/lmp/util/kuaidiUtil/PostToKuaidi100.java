package com.xiangrui.lmp.util.kuaidiUtil;

import java.sql.Timestamp;
import java.util.HashMap;
import org.apache.log4j.Logger;

import com.xiangrui.lmp.business.base.BaseInterfaceLog;
import com.xiangrui.lmp.constant.SYSConstant;
import com.xiangrui.lmp.util.HttpRequest;
import com.xiangrui.lmp.util.JacksonHelper;
import com.xiangrui.lmp.util.StringUtil;
import com.xiangrui.lmp.util.kuaidiUtil.pojo.TaskRequest;
import com.xiangrui.lmp.util.kuaidiUtil.pojo.TaskResponse;

/**
 * 
 * 向快递100 发送订阅请求
 * 
 * @author hyh
 * @version 1.0
 * @since JDK 1.7
 */
public class PostToKuaidi100
{

    private static Logger logger = Logger.getLogger(PostToKuaidi100.class);

    /**
     * 向快递100 发送订阅请求
     * 
     * @param com
     *            快递公司
     * @param nu
     *            快递单号
     * @param from
     *            寄件地址（非必须）
     * @param to
     *            收件地址
     * 
     * @return
     */
    public static synchronized TaskResponse sendJson(String com, String nu, String from,
            String to)
    {
        logger.info("----------------发送快递单号开始--------------------");

        TaskRequest taskRequest = new TaskRequest();
        // 快递公司
        taskRequest.setCompany(com);
        // 快递单号
        taskRequest.setNumber(nu);
        // 寄件地址 这个参数不提供也可以
        taskRequest.setFrom(from);
        // 收件地址
        taskRequest.setTo(to);
        // 回传URL
        taskRequest.getParameters()
                .put("callbackurl", SYSConstant.CALLBACK_URL);
        // 授权码
        taskRequest.setKey(SYSConstant.KUAIDI100_KEY);

        HashMap<String, String> p = new HashMap<String, String>();
        String sendString = JacksonHelper.toJSON(taskRequest);
        p.put("schema", "json");
        p.put("param", sendString);

        logger.info("请求时间=" + new Timestamp(System.currentTimeMillis()));
        logger.info("快递单号=" + taskRequest.getNumber());
        logger.info("收货地址=" + taskRequest.getTo());
        logger.info("请求内容=" + sendString);

        TaskResponse resp = new TaskResponse();

        if (SYSConstant.KUAIDI100_OPEN)
        {
            try
            {
                String ret = HttpRequest.postData(SYSConstant.POST_URL, p,
                        "UTF-8", BaseInterfaceLog.TYPE_KUAIDI100_SUBSCRIBE);
                
               if(StringUtil.isEmpty(ret)) {
                   resp.setResult(false);
                   logger.info("订阅信息发生异常");
                   return resp;  
               }
                resp = JacksonHelper.fromJSON(ret, TaskResponse.class);

                if (resp.getResult())
                {
                    logger.info("订阅成功");
                    System.out.println("returnCode=" + resp.getReturnCode());
                    System.out.println("message=" + resp.getMessage());

                } else
                {
                    logger.info("订阅失败");
                    logger.info("错误代号=" + resp.getReturnCode());
                    logger.info("错误信息=" + resp.getMessage());
                    return resp;
                }
            } catch (Exception e)
            {
                e.printStackTrace();
                resp.setResult(false);
                logger.info("订阅信息发生异常");
                logger.error(e.getMessage());
                return resp;
            }
        } else
        {
            resp.setResult(false);
            resp.setMessage("接口未开启");
            logger.debug("快递100接口未开启!");
            return resp;
        }
        resp.setResult(true);
        resp.setMessage("订阅成功");
        logger.info("----------------发送快递单号结束--------------------");

        return resp;
    }

}
