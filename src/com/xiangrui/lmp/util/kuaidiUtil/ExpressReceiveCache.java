package com.xiangrui.lmp.util.kuaidiUtil;

import java.util.ArrayList;
import java.util.List;

import com.xiangrui.lmp.business.kuaidi100.vo.ExpressInfo;

/**
 * 快递接受缓存
 * <p>
 * 
 * @author <b>hyh</b>
 *         </p>
 *         <p>
 *         2015-8-4
 *         </p>
 */
public class ExpressReceiveCache
{

    private static ExpressReceiveCache singleton = null;

    private static List<ExpressInfo> cacheList = new ArrayList<ExpressInfo>();

    private ExpressReceiveCache()
    {
    }

    public static synchronized ExpressReceiveCache getInstance()
    {
        if (singleton == null)
        {
            singleton = new ExpressReceiveCache();
        }
        return singleton;
    }

    /**
     * 添加缓存数据
     * 
     * @param expressInfo
     */
    public void addExpressInfo(ExpressInfo expressInfo)
    {

        cacheList.add(expressInfo);
    }

    /**
     * 取得缓存数据
     * 
     * @return
     */
    public List<ExpressInfo> getExpressInfo()
    {

        return cacheList;
    }

    /**
     * 缓存数据大小
     * 
     * @return
     */
    public int getCacheSize()
    {

        return cacheList.size();
    }

    /**
     * 清空缓存
     */
    public void clear()
    {

        cacheList.clear();
    }

}
