package com.xiangrui.lmp.util.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.xiangrui.lmp.util.StringUtil;

public class POIExcelTools implements Serializable {

	/**  描述  */
	
	private static final long serialVersionUID = 6984602558613638957L;

	/** 总行数 */
	private int totalRows = 0;

	/** 总列数 */
	private int totalCells = 0;

	/** 错误信息 */
	private String errorInfo;

	/**
	 * 
	 * 构造函数
	 */
	public POIExcelTools() {

	}

	/**
	 * 验证文件格式 描述
	 * 
	 * @param fileName
	 *            文件名
	 * @return str 为空 文件正常，否则返回错误信息
	 */
	public boolean validateExcel(String fileName) {
		/** 检查文件名是否为空或者是否是Excel格式的文件 */
		boolean flag = true;
		if (fileName == null
				|| !(isExcelThree(fileName) || isExcelSeven(fileName))) {
			errorInfo = "文件名不是excel格式";
			flag = false;
		}
		/** 检查文件是否存在 */
		File file = new File(fileName);
		if (file == null || !file.exists()) {
			errorInfo = "文件不存在";
			flag = false;
		}
		return flag;
	}

	/**
	 * 
	 * @描述：根据文件名读取excel文件
	 * @参数：@return List
	 */
	public Map<String, List<List<String>>> read(String filePath, Integer readRow) {
		Map<String, List<List<String>>> sheetMap = new TreeMap<String, List<List<String>>>();
		InputStream is = null;
		try {
			if (!validateExcel(filePath)) {
				System.out.println(errorInfo);
				return null;
			}
			boolean isExcel2003 = true;
			if (isExcelSeven(filePath)) {
				isExcel2003 = false;
			}
			File file = new File(filePath);
			is = new FileInputStream(file);
			sheetMap = read(is, isExcel2003, readRow);
			is.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					is = null;
					e.printStackTrace();
				}
			}
		}
		return sheetMap;
	}

	/**
	 * 
	 * @描述：根据文件名读取excel文件
	 * @参数：@return List
	 */
	public Map<String, List<List<String>>> read(File file, Integer readRow) {
		Map<String, List<List<String>>> sheetMap = new TreeMap<String, List<List<String>>>();
		InputStream is = null;
		try {
			String fileName = file.getName();
			if (!validateExcel(fileName)) {
				System.out.println(errorInfo);
				return null;
			}
			boolean isExcel2003 = true;
			if (isExcelSeven(fileName)) {
				isExcel2003 = false;
			}
			is = new FileInputStream(file);
			sheetMap = read(is, isExcel2003, readRow);
			is.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					is = null;
					e.printStackTrace();
				}
			}
		}
		return sheetMap;
	}

	/**
	 * 
	 * @描述：根据流读取Excel文件
	 * @参数：@return List
	 */
	public Map<String, List<List<String>>> read(InputStream inputStream,
			boolean isExcel2003, Integer readRow) {
		Map<String, List<List<String>>> sheetMap = null;
		try {
			Workbook wb = WorkbookFactory.create(inputStream);
			sheetMap = read(wb, readRow);
		} catch (InvalidFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sheetMap;
	}

	/**
	 * @描述：读取数据
	 */
	private Map<String, List<List<String>>> read(Workbook wb, Integer readRow) {
		Map<String, List<List<String>>> sheetMap = new TreeMap<String, List<List<String>>>();
		int sheetNum = wb.getNumberOfSheets();
		for (int i = 0; i < sheetNum; i++) {
			Sheet sheet = wb.getSheetAt(i);
			this.totalRows = sheet.getPhysicalNumberOfRows();
			if (this.totalRows >= 1 && sheet.getRow(0) != null) {
				this.totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
			}
			List<List<String>> dataLst = new LinkedList<List<String>>();
			for (int r = readRow; r < this.totalRows; r++) {
				Row row = sheet.getRow(r);
				if (null == row || isBlankRow(row)) {
					continue;
				}
				List<String> rowLst = new ArrayList<String>();
				for (short c = 0; c < this.getTotalCells(); c++) {
					rowLst.add(getCellStringValue(row.getCell(c)));
				}
				dataLst.add(rowLst);
			}
			sheetMap.put(wb.getSheetName(i), dataLst);
		}
		return sheetMap;
	}

	/**
	 * 
	 * Description: 获取指定列的值
	 * 
	 * @param cell
	 *            单元格
	 * @return String
	 */
	private static String getCellStringValue(Cell cell) {
		String cellValue = "";
		if (null == cell) {
			return "";
		}
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC: // 数值型 0
			if (DateUtil.isCellDateFormatted(cell)) {// 如果是日期
				cellValue = new DataFormatter().formatRawCellContents(
						cell.getNumericCellValue(), 0, "yyyy-mm-dd");// 格式化日期
			} else {
				cell.setCellType(Cell.CELL_TYPE_STRING);
				String temp = cell.getStringCellValue();
				if(!StringUtil.isEmpty(temp)) {
					if(temp.indexOf(".") >- 1) {
						String suffix = temp.substring(temp.lastIndexOf(".") + 1, temp.length());
						if(suffix.length() > 2) {
							DecimalFormat nf = new DecimalFormat("0.00");
							cellValue = nf.format(Double.parseDouble(cell.getStringCellValue()));
						} else {
							cellValue = cell.getStringCellValue();
						}
					}  else {
						cellValue = cell.getStringCellValue();
					}
				} else {
					cellValue = cell.getStringCellValue();
				}
			}
			break;
		case Cell.CELL_TYPE_STRING:// 字符串型 1
			cellValue = cell.getStringCellValue().trim();
			if ("".equals(cellValue) || cellValue.length() <= 0)
				cellValue = "";
			break;
		case Cell.CELL_TYPE_FORMULA: // 公式型 2
			cell.setCellType(Cell.CELL_TYPE_NUMERIC);
			cellValue = String.valueOf(cell.getNumericCellValue());
			break;
		case Cell.CELL_TYPE_BLANK: // 空值 3
			cellValue = "";
			break;
		case Cell.CELL_TYPE_BOOLEAN: // 布尔型 4
			cellValue = String.valueOf(cell.getBooleanCellValue());
			break;
		case Cell.CELL_TYPE_ERROR: // 错误 5
			break;
		default:
			break;
		}
		return cellValue;
	}

	/**
	 * 检查当前行是否是空白行
	 * @param row		当前行
	 * @return			true/false
	 */
	public static boolean isBlankRow(Row row) {
		if (row == null)
			return true;
		boolean result = true;
		for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
			Cell cell = row.getCell(i, Row.RETURN_BLANK_AS_NULL);
			String value = "";
			if (cell != null) {
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_NUMERIC:// 数值型 0
					if (DateUtil.isCellDateFormatted(cell)) {// 如果是日期
						value = new DataFormatter().formatRawCellContents(
								cell.getNumericCellValue(), 0, "yyyy-mm-dd");// 格式化日期
					} else {
						cell.setCellType(Cell.CELL_TYPE_STRING);
						String temp = cell.getStringCellValue();
						if(temp.indexOf(".") >- 1) {
							value = String.valueOf(new Double(temp)).trim();
						} else {
							value = temp.trim();
						}
					}
					break;
				case Cell.CELL_TYPE_STRING:// 字符串型 1
					value = cell.getStringCellValue();
					break;
				case Cell.CELL_TYPE_FORMULA:// 公式型 2
					value = String.valueOf(cell.getCellFormula());
					break;
				case Cell.CELL_TYPE_BLANK:// 空值 3
					value = "";
					break;
				case Cell.CELL_TYPE_BOOLEAN:// 布尔型 4
					value = String.valueOf(cell.getBooleanCellValue());
					break;
				case Cell.CELL_TYPE_ERROR :// 错误 5
					break;
				default:
					break;
				}
				if (!value.trim().equals("")) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * 返回 totalRows 的值
	 * 
	 * @return totalRows
	 */

	public int getTotalRows() {
		return totalRows;
	}

	/**
	 * 设置 totalRows 的值
	 * 
	 * @param totalRows
	 *            totalRows
	 */
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}

	/**
	 * 返回 totalCells 的值
	 * 
	 * @return totalCells
	 */

	public int getTotalCells() {
		return totalCells;
	}

	/**
	 * 设置 totalCells 的值
	 * 
	 * @param totalCells
	 *            totalCells
	 */
	public void setTotalCells(int totalCells) {
		this.totalCells = totalCells;
	}

	/**
	 * 返回 errorInfo 的值
	 * 
	 * @return errorInfo
	 */

	public String getErrorInfo() {
		return errorInfo;
	}

	/**
	 * 设置 errorInfo 的值
	 * 
	 * @param errorInfo
	 *            errorInfo
	 */
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public static boolean isExcelThree(String fileName) {
		return fileName.matches("^.+\\.(?i)(xls)$");
	}

	/**
	 * @描述：是否是2007的excel
	 * @参数：@return boolean
	 */

	public static boolean isExcelSeven(String fileName) {
		return fileName.matches("^.+\\.(?i)(xlsx)$");
	}
	
	/**
	 * 
	 * @描述：main测试方法
	 * @参数：@param args args
	 * @参数：@throws Exception Exception
	 * @返回值：void
	 */
	public static void main(String[] args) throws Exception {
//		POIExcelTools poi = new POIExcelTools();
		// List<List<String>> list = poi.read("d:/aaa.xls");
//		String filePath = "E:\\svn\\项目管理\\品牌排行\\需求文档\\概要需求\\雨打导入模版\\456.xls";
////		File file = new File(filePath);
//		// poi.read(filePath, 1);
//		long a = System.currentTimeMillis();
//		Map<String, List<List<String>>> map = new POIExcelTools().read(
//				filePath, 1);
//		
//		System.out.println("\r<br> 解析文件时间 : " + (System.currentTimeMillis()-a) / 1000f + " 秒");
//		
//		Iterator ite = map.keySet().iterator();
//		System.out.println(map.size());
//		while (ite.hasNext()) {
//			Object key = ite.next();
//			String sheetName = key.toString();
//			System.out.println("sheet 名称>" + sheetName);
//			List<List<String>> dataLst = map.get(key);
//			if (dataLst != null) {
//				for (int i = 0, ilen = dataLst.size(); i < ilen; i++) {
//					System.out.println("第" + (i + 1) + "行");
//					List<String> cellList = dataLst.get(i);
//					for (int j = 0, jlen = cellList.size(); j < jlen; j++) {
//						System.out.print("    第" + (j + 1) + "列值：");
//						System.out.println(cellList.get(j));
//					}
//				}
//			}
//		}
		
		System.out.println("----start----");
		int value = -1;
		assert 0 < value : "value="+value;
		System.out.println("---- end ----");
		
	}
	
}
