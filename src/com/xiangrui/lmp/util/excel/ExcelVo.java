package com.xiangrui.lmp.util.excel;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author 每个ExcelVo为一个excel对象
 *
 */
public class ExcelVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7657932155182968542L;
	private List<String> head;
	private Map<Integer,Map<Integer,String>> con;//Map<行号，Map<列号，表格内容>>
	private String msg;
	private boolean res;
	private List<ExcelVo> sheets;  //每一个为sheet对象
	private String name;
	private Integer num;
	
	private String ebId;
	private String ciType;
	public List<String> getHead() {
		return head;
	}
	public void setHead(List<String> head) {
		this.head = head;
	}
	public Map<Integer, Map<Integer, String>> getCon() {
		return con;
	}
	public void setCon(Map<Integer, Map<Integer, String>> con) {
		this.con = con;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public boolean isRes() {
		return res;
	}
	public void setRes(boolean res) {
		this.res = res;
	}
	public String getEbId() {
		return ebId;
	}
	public void setEbId(String ebId) {
		this.ebId = ebId;
	}
	public String getCiType() {
		return ciType;
	}
	public void setCiType(String ciType) {
		this.ciType = ciType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public List<ExcelVo> getSheets() {
		return sheets;
	}
	public void setSheets(List<ExcelVo> sheets) {
		this.sheets = sheets;
	}
		
}
