package com.xiangrui.lmp.util.excel;

import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExportExcel {

	/**
	 * 
	 * @param sheetName
	 * @param title
	 * @param key
	 * @param rowsList
	 */
	public static void exportExcel(HttpServletResponse response, String name,
			String[] title, String[] key, List<Map<String, Object>> rowsList) {

		response.setContentType("application/ms-excel");
		String filedisplay = name + ".xls";
		try {
			filedisplay = URLEncoder.encode(filedisplay, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		response.addHeader("Content-Disposition", "attachment;filename="
				+ filedisplay);

		// 第一步，创建一个webbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();

		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = wb.createSheet(name);

		// 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
		HSSFRow row = sheet.createRow((int) 0);

		// 第四步，创建单元格，并设置值表头 设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		HSSFCell cell = null;
		for (int i = 0; i < title.length; i++) {
			cell = row.createCell((short) i);
			cell.setCellValue(new HSSFRichTextString(title[i]));
			cell.setCellStyle(style);
		}
		// 第五步，写入实体数据 实际应用中这些数据从数据库得到
		Map<String, Object> map = new HashMap<String, Object>();
		Object obj = new Object();
		for (int i = 0; i < rowsList.size(); i++) {

			map = rowsList.get(i);

			row = sheet.createRow((int) i + 1);

			for (int k = 0; k < key.length; k++) {

				obj = map.get(key[k]);
				if (null == obj) {
					obj = "";
				}
				row.createCell((short) k).setCellValue(obj.toString());

			}

		}
		// 第六步，将文件存到指定位置
		try {
			OutputStream out = response.getOutputStream();
			wb.write(out);
			out.flush();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
