package com.xiangrui.lmp.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.xiangrui.lmp.annotation.SystemServiceLog;

/**
 * 注释解析
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-26 上午10:52:08
 * </p>
 */
public class ParseAnnotationSystemLog
{
    /**
     * 后去某个方法上的 SystemServiceLog 注释的 description 值
     * 
     * @param method
     *            方法对象
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     */
    public static String parseMethodSystemServiceLogInDescription(Method method)
            throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException, SecurityException,
            NoSuchMethodException, InstantiationException
    {

        SystemServiceLog say = method.getAnnotation(SystemServiceLog.class);
        if (say != null)
        {
            return say.description();
        }
        return "";
    }
}
