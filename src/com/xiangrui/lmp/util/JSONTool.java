package com.xiangrui.lmp.util;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsDateJsonValueProcessor;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.log4j.Logger;

import com.xiangrui.lmp.exception.BaseException;



@SuppressWarnings("unchecked")
public class JSONTool {
	private static Logger logger = Logger.getLogger(JSONTool.class);

	public static <T> T getDTO(String jsonString, Class<T> clazz) {
		JSONObject jsonObject = null;
		try {
			jsonObject = JSONObject.fromObject(jsonString);
		} catch (Exception e) {
			logger.error("json??????json??????", e);
			e.printStackTrace();

		}
		return (T)JSONObject.toBean(jsonObject, clazz);
	}

	public static List getDTArray(String jsonString, Class clazz) {

		return JSONArray.fromObject(jsonString);
	}

	/**
	 * ???JSON ???????????java?????beansList?????????? {"id" : idValue, "name" :
	 * nameValue, "aBean" : {"aBeanId" : aBeanIdValue, ...}, beansList:[{}, {},
	 * ...]}
	 * 
	 * @param jsonString
	 * @param clazz
	 * @param map
	 *            ??????? (key : ?????, value : ??????class) eg: ("beansList" :
	 *            Bean.class)
	 * @return
	 */
	public static Object getDTO(String jsonString, Class clazz, Map map) {

		JSONObject jsonObject = null;

		try {

			jsonObject = JSONObject.fromObject(jsonString);

		} catch (Exception e) {

			logger.error("json??????json??????", e);

			e.printStackTrace();
		}
		return JSONObject.toBean(jsonObject, clazz, map);
	}

	public static void main(String[] args) {
	/*	String jsonString = "[{name:'fff',password:'gggg'},{name:'fff',password:'gggg'}]";
		List<Bean> beans = (List<Bean>) getDTOList(jsonString, Bean.class);
		System.out.println(beans.get(0).getName());*/
		String jsonString="[\"112\",\"1112\"]";
		List<String> list = getDTOStringList(jsonString);
		System.out.println(list.get(0));
	}

	/**
	 * ???JSON??????java???????? [{"id" : idValue, "name" : nameValue}, {"id" :
	 * idValue, "name" : nameValue}, ...]
	 * 
	 * @param object
	 * @param clazz
	 * @return
	 */
	public static Object[] getDTOArray(String jsonString, Class clazz) {
		JSONArray array = null;
		try {

			array = JSONArray.fromObject(jsonString);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("pase error jsonString", e);
		}

		Object[] obj = new Object[array.size()];
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			obj[i] = JSONObject.toBean(jsonObject, clazz);
		}
		return obj;
	}

	/**
	 * ???JSON??????java???????? [{"id" : idValue, "name" : nameValue}, {"id" :
	 * idValue, "name" : nameValue}, ...]
	 * 
	 * @param object
	 * @param clazz
	 * @param map
	 * @return
	 */
	public static Object[] getDTOArray(String jsonString, Class clazz, Map map) {

		JSONArray array = JSONArray.fromObject(jsonString);
		Object[] obj = new Object[array.size()];
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			obj[i] = JSONObject.toBean(jsonObject, clazz, map);
		}
		return obj;
	}

	/**
	 * ???JSON??????java????
	 * 
	 * @param object
	 * @param clazz
	 * @return
	 */
	public static List getDTOList(String jsonString, Class clazz) {
		JSONArray array = null;
		try {

			array = JSONArray.fromObject(jsonString);
			List list = new ArrayList();
			for (Iterator iter = array.iterator(); iter.hasNext();) {
				JSONObject jsonObject = (JSONObject) iter.next();
				list.add(JSONObject.toBean(jsonObject, clazz));
			}
			return list;

		} catch (Exception e) {

			logger.error(jsonString+"json??????json??????", e);
			return new ArrayList();
		}

	}
	public static List<String> getDTOStringList(String jsonString) {
		JSONArray array = null;
		try {

			array = JSONArray.fromObject(jsonString);
			List<String> list = new ArrayList<String>();
			for (Iterator iter = array.iterator(); iter.hasNext();) {
				String jsonObject = (String) iter.next();
				list.add(jsonObject);
			}
			return list;

		} catch (Exception e) {

			logger.error(jsonString+"json??????json??????", e);
			return new ArrayList();
		}

	}
	/**
	 * ???JSON??????java?????????????????
	 * 
	 * @param object
	 * @param clazz
	 * @param map
	 *            ???????
	 * @return
	 */
	public static List getDTOList(String jsonString, Class clazz, Map map) {

		JSONArray array = JSONArray.fromObject(jsonString);
		List list = new ArrayList();
		for (Iterator iter = array.iterator(); iter.hasNext();) {
			JSONObject jsonObject = (JSONObject) iter.next();
			list.add(JSONObject.toBean(jsonObject, clazz, map));
		}
		return list;
	}

	/**
	 * ?json HASH????????map??map?????? ???{"id" : "johncon", "name" : "??"}
	 * ??commons
	 * -collections???????org.apache.commons.collections.map.MultiKeyMap
	 * 
	 * @param object
	 * @return
	 */
	public static Map getMapFromJson(String jsonString) {

		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		Map map = new HashMap();
		for (Iterator iter = jsonObject.keys(); iter.hasNext();) {
			String key = (String) iter.next();
			map.put(key, jsonObject.get(key));
		}
		return map;
	}

	/**
	 * ?json???????java?? json???["123", "456"]
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Object[] getObjectArrayFromJson(String jsonString) {
		JSONArray jsonArray = JSONArray.fromObject(jsonString);
		return jsonArray.toArray();
	}

	/**
	 * ????????json??? DTO?????{"id" : idValue, "name" : nameValue, ...}
	 * ???????[{}, {}, {}, ...] map?????{key1 : {"id" : idValue, "name" :
	 * nameValue, ...}, key2 : {}, ...}
	 * 
	 * @param object
	 * @return
	 */
	public static String getJSONString(Object object) throws Exception {
		String jsonString = null;
		// ??????
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(java.util.Date.class,
				new JsDateJsonValueProcessor());

		if (object != null) {
			if (object instanceof Collection || object instanceof Object[]) {
				jsonString = JSONArray.fromObject(object, jsonConfig)
						.toString();
			} else {
				jsonString = JSONObject.fromObject(object, jsonConfig)
						.toString();
			}
		}
		return jsonString == null ? "{}" : jsonString;
	}

	/**
	 * ????
	 * 
	 * @param excludes
	 *            ?????????
	 * @param datePattern
	 *            ??????
	 * @return
	 */
	public static JsonConfig configJson(String[] excludes, String datePattern) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(excludes);
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		jsonConfig.registerJsonValueProcessor(Date.class,
				new DateJsonValueProcessor(datePattern));

		return jsonConfig;
	}

	public static synchronized JSONObject addJSON(JSONObject source,
			JSONObject target) {

		Iterator its = source.keys();
		Object key;
		while (its.hasNext()) {
			key = its.next();
			Object value = source.get(key);
			target.put(key, value);
		}

		return target;
	}
	
	public static JSONObject merge(JSONObject source,JSONObject element){
		Iterator<String> its=element.keys();
		
		while(its.hasNext()){
			String key=its.next();
			source.element(key, element.get(key));
		}
		
		return source;
	}
	public static JSONObject merge(Object source,Object element){
		JSONObject jo=JSONObject.fromObject(source);
		JSONObject e=JSONObject.fromObject(element);
		Iterator<String> its=e.keys();
		while(its.hasNext()){
			String key=its.next();
			jo.element(key, e.get(key));
		}
		return jo;
	}
	
	
}
