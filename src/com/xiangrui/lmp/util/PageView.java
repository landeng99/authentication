package com.xiangrui.lmp.util;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("rawtypes")
/**
 * //??????
 * @param <T>
 */
public class PageView implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2154296153061551350L;

	/**
	 * ????
	 */
	
	private List records;
	
	/**
	 * ????????
	 * ??????
	 * startindex?????
	 * endindex??????
	 * ?????????
	 */
	private PageIndex pageindex;
	
	/**
	 * ???
	 * ?????????
	 * 
	 */
	private long pageCount;


	/**
	 * ????????
	 */
	private int pageSize = 30;


	/**
	 *?? ??? ????
	 *?????????
	 */
	private int pageNow = 1;

	/**
	 * ????
	 */
	private long rowCount;
	
	/**
	 * ????????
	 */
	private int startPage;

	/**
	 * ????5???
	 */
	private int pagecode = 5;
	public PageView() {
	}

	/**
	 * ?????????????????
	 * @return
	 */
	public int getFirstResult(){
		return (this.pageNow-1)* this.pageSize;
	}
	
	public int getPagecode() {
		return pagecode;
	}

	public void setPagecode(int pagecode) {
		this.pagecode = pagecode;
	}
	
	/**
	 * ??????????????
	 * ????????????
	 * @param pageSize????????
	 * @param pageNow????
	 */
	public PageView(int pageSize, int pageNow){
		this.pageSize = pageSize;
		this.pageNow = pageNow;
	}
	/**
	 * ??????????????
	 * ???
	 * @param pageNow????
	 */
	public PageView(int pageNow){
		this.pageNow = pageNow;
		startPage = (this.pageNow - 1) * this.pageSize;
	}
	
	public PageView(int pageNow,int pageSize,int i){
		this.pageNow = pageNow;
		this.pageSize = pageSize;
		startPage = (this.pageNow - 1) * this.pageSize;
	}
	public PageView(int pageNow,int pageSize,int allcount,int currentpage){
		this.pageNow = pageNow;//???
		this.pageSize = pageSize;//??????
		this.setRowCount(allcount);//???
	    long tmppageCount = allcount/pageSize;
	    this.pageCount = tmppageCount;//???
	    if(tmppageCount* pageSize < allcount){
	    	   this.pageCount = tmppageCount + 1;
	    }
	}
	/**
	 * ??????
	 * ???????????????PageView??
	 * @param rowCount ????
	 * @param records ????
	 */

	public void setQueryResult(long rowCount, List records){
		setRowCount(rowCount);
		setRecords(records);
	}

	public void setRowCount(long rowCount) {
		this.rowCount = rowCount;
		setPageCount(this.rowCount % this.pageSize == 0?
					this.rowCount/this.pageSize :
					this.rowCount/this.pageSize+1
		);
	}
	
	public List getRecords() {
		return records;
	}

	public void setRecords(List records) {
		this.records = records;
	}


	public PageIndex getPageindex() {
		return pageindex;
	}

	public void setPageindex(PageIndex pageindex) {
		this.pageindex = pageindex;
	}


	/**
	 * WebTool?????????
	 * @author Administrator
	 *  
	 *?pagecode??????????????????
	 *  pageNow ????
	 *?pageCount ???
	 *
	 *  ??????????????PageIndex
	 *  
	 *  ???????????????????????????????
	 *  ????
	 */
	public void setPageCount(long pageCount) {
		this.pageCount = pageCount;
		this.pageindex = WebTool.getPageIndex(pagecode, pageNow, pageCount);
	}

	public int getPageNow() {
		return pageNow;
	}

	public void setPageNow(int pageNow) {
		this.pageNow = pageNow;
	}

	public long getPageCount() {
		return pageCount;
	}
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public long getRowCount() {
		return rowCount;
	}

	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}


}
