package com.xiangrui.lmp.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.xiangrui.lmp.business.admin.identifier.service.IdentifierService;
import com.xiangrui.lmp.init.SpringContextHolder;

/**
 * 
 * 编号生成工具类
 * 
 * @author chinasoft
 *
 */
public class IdentifierCreator
{

	/**
	 * 包裹类型
	 */
	public final static int TYPE_PACKAGE = 1;

	/**
	 * 提单编号类型
	 */
	public final static int TYPE_PICKORDER = 2;

	/**
	 * 托盘类型
	 */
	public final static int TYPE_PALLET = 3;

	/**
	 * 同行账户导入批次
	 */
	public final static int TYPE_PARTNER = 4;

	private static List<String> TOTAL_LOGISTICS_CODE_LIST = new ArrayList<String>();

	public static void addLogistics_code_list(String logistics_code_list)
	{
		ReadWriteLock lock = new ReentrantReadWriteLock();
		lock.writeLock().lock();
		if (!checkLogistics_code_listExist(logistics_code_list))
		{
			TOTAL_LOGISTICS_CODE_LIST.add(logistics_code_list);
		}
		lock.writeLock().unlock();
	}

	public static boolean checkLogistics_code_listExist(String logistics_code_list)
	{
		boolean isExist = false;
		ReadWriteLock lock = new ReentrantReadWriteLock();
		lock.readLock().lock();
		if (TOTAL_LOGISTICS_CODE_LIST.contains(logistics_code_list))
		{
			isExist = true;
		}
		lock.readLock().unlock();
		return isExist;
	}

	public static String createIdentifier(int type)
	{
		String code = null;
		ReadWriteLock lock = new ReentrantReadWriteLock();
		lock.writeLock().lock();
		IdentifierService identifierService = SpringContextHolder.getBean("identifierService");

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("identifier_id", type);

		code = identifierService.getCode(params);
		lock.writeLock().unlock();

		return code;
	}

}
