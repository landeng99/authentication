package com.xiangrui.lmp.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpClientTool
{
    
    /**
     * 
     * post请求
     * @param url
     * @param data
     * @param charset
     * @return
     * @throws IOException
     */
    public static String post(String url,String data, String charset) throws IOException{

        HttpPost httppost = new HttpPost(url);
        
        StringEntity stEntity;
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try
        {
            httppost.setHeader("Content-Type","text/html;charset=UTF-8");
            stEntity = new StringEntity(data,charset);
            httppost.setEntity(stEntity);
           
            CloseableHttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            
            if (entity != null)
            {
                String resultString="";
                resultString=EntityUtils.toString(entity,charset);
                System.out.println("--------------------------------------");
                System.out.println("Response content: " +resultString);
                System.out.println("--------------------------------------");
                return  resultString;  
            }
        } catch (UnsupportedEncodingException e)
        {
          throw e;
        } catch (ClientProtocolException e)
        {
           throw e;
        } catch (IOException e)
        {
           throw e;
        }finally{
            httpclient.close();
        }
        return null;
    }
    
    public synchronized static String postData(String url,
            Map<String, String> params, String codePage) throws Exception {

        final HttpClient httpClient = new HttpClient();
        httpClient.getHttpConnectionManager().getParams()
                .setConnectionTimeout(10 * 1000);
        httpClient.getHttpConnectionManager().getParams()
                .setSoTimeout(10 * 1000);

        final PostMethod method = new PostMethod(url);
        if (params != null) {
            method.getParams().setParameter(
                    HttpMethodParams.HTTP_CONTENT_CHARSET, codePage);
            method.setRequestBody(assembleRequestParams(params));
        }
        String result = "";
        try {
            httpClient.executeMethod(method);
            result = new String(method.getResponseBody(), codePage);
        } catch (final Exception e) {
            throw e;
        } finally {
            method.releaseConnection();
        }
        return result;
    }
    
    /**
     * 组装http请求参数
     * 
     * @param params
     * @param menthod
     * @return
     */
    private synchronized static NameValuePair[] assembleRequestParams(
            Map<String, String> data) {
        final List<NameValuePair> nameValueList = new ArrayList<NameValuePair>();

        Iterator<Map.Entry<String, String>> it = data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it
                    .next();
            nameValueList.add(new NameValuePair((String) entry.getKey(),
                    (String) entry.getValue()));
        }

        return nameValueList.toArray(new NameValuePair[nameValueList.size()]);
    }
}
