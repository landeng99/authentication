package com.xiangrui.lmp.util.newExcel;

import java.util.List;

/**
 * 
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 上午9:43:12
 * </p>
 */
public class MapTableSingle
{
    /**
     * 表名
     */
    String name;
    /**
     * 列名集合
     */
    List<String> colTableNames;
    /**
     * 列数
     */
    int colNum;
    /**
     * 总行数
     */
    int rowCount;
    /**
     * 数据行数
     */
    int rowMum;
    /**
     * 表头位置
     */
    int rowIndex;
    /**
     * 表头名集合
     */
    List<String> colNames;
    /**
     * 所有数据集合
     */
    List<List<String>> table;

    /**
     * 获取表名
     * 
     * @return
     */
    public String getName()
    {
        return name;
    }

    /**
     * 设置表名
     * 
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * 获取列名集合
     * 
     * @return
     */
    public List<String> getColTableNames()
    {
        return colTableNames;
    }

    /**
     * 设置列名集合
     * 
     * @param colTableNames
     */
    public void setColTableNames(List<String> colTableNames)
    {
        this.colTableNames = colTableNames;
    }

    /**
     * 获取数据列数
     * 
     * @return
     */
    public int getColNum()
    {
        return colNum;
    }

    /**
     * 设置数据列数
     * 
     * @param colNum
     */
    public void setColNum(int colNum)
    {
        this.colNum = colNum;
    }

    /**
     * 获取总行数
     * 
     * @return
     */
    public int getRowCount()
    {
        return rowCount;
    }

    /**
     * 设置总行数
     * 
     * @param rowCount
     */
    public void setRowCount(int rowCount)
    {
        this.rowCount = rowCount;
    }

    /**
     * 获取数据行数
     * 
     * @return
     */
    public int getRowMum()
    {
        return rowMum;
    }

    /**
     * 设置数据行数
     * 
     * @param rowMum
     */
    public void setRowMum(int rowMum)
    {
        this.rowMum = rowMum;
    }

    /**
     * 获取表头位置
     * 
     * @return
     */
    public int getRowIndex()
    {
        return rowIndex;
    }

    /**
     * 设置表头位置
     * 
     * @param rowIndex
     */
    public void setRowIndex(int rowIndex)
    {
        this.rowIndex = rowIndex;
    }

    /**
     * 获取表头名集合
     * 
     * @return
     */
    public List<String> getColNames()
    {
        return colNames;
    }

    /**
     * 设置表头名集合
     * 
     * @param colNames
     */
    public void setColNames(List<String> colNames)
    {
        this.colNames = colNames;
    }

    /**
     * 获取所有数据集合
     * 
     * @return
     */
    public List<List<String>> getTable()
    {
        return table;
    }

    /**
     * 设置所有数据集合
     * 
     * @param table
     */
    public void setTable(List<List<String>> table)
    {
        this.table = table;
    }

    /**
     *  table </br>
     *      c:forEach items="${mapTableSingle}"
     *          var="lists" begin="${mapTableSingle.rowIndex}"</br> 
     *   tr </br>
     *      c:forEach items="${lists}"
     *          var="list" </br>
     *    td ${list} td </br>
     *      c:forEach</br> 
     *   tr </br>
     *      c:forEach</br> 
     *   table</br>
     * @return
     */
    public String getPrintTable()
    {
        StringBuffer sb = new StringBuffer("<table>");
        for (List<String> list : table)
        {
            sb.append("<tr>");
            for (String string : list)
            {
                sb.append("<td>" + string + "</td>");
            }
            sb.append("</tr>");
        }
        sb.append("<table>");
        return sb.toString();
    }
    
    /**
     * 当返回为-1是,不存在
     * @param colName 表头名称
     * @return
     */
    public int getExcelColNameIndex(String colName){
        int colNum = 0;
        for (String colNameTemp : this.getColNames())
        {
            if(colName.equals(colNameTemp)){
                return colNum;
            }
            colNum++;
        }
        return -1;
    }
    
}
