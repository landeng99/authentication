package com.xiangrui.lmp.util.newExcel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 生成Excel
 * 
 * @author
 */
public class ExcelWriteUtil
{

	/**
	 * xlsx类型的文件
	 */
	private static final String XSS_TYPE = "xlsx";

	/**
	 * xls类型的文件
	 */
	private static final String HSS_TYPE = "xls";

	/**
	 * JavaBean 写成Excel2007 javabean的属性 ：1.字符型 2.串整形 3.浮点型
	 * 
	 * @param sheetName
	 *            sheet名
	 * @param colNameMap
	 *            LinkedHashMap(排列顺序) javabean的属性(不区分大小写)和Excel列名关联
	 *            key：javabean的属性，value：Excel列名
	 * @param javaBeanList
	 * @return
	 * @throws Exception
	 */
	public static <T> XSSFWorkbook beanToExcel2007(String sheetName, Map<String, String> colNameMap, List<T> javaBeanList) throws Exception
	{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook();

		// 新建sheet
		XSSFSheet xssfSheet = xssfWorkbook.createSheet(sheetName);

		// 表头列
		XSSFRow firstXSSFRow = xssfSheet.createRow(0);

		// bean的所有方法
		Method[] methods = javaBeanList.get(0).getClass().getMethods();

		// get方法和列番号
		Map<String, String> colNumberAndMethodName = new LinkedHashMap<String, String>();
		// 计数器
		int index = 0;
		for (Map.Entry<String, String> entry : colNameMap.entrySet())
		{
			// 设置表头
			XSSFCell xssfCell = firstXSSFRow.createCell(index);
			xssfCell.setCellValue(new XSSFRichTextString(entry.getValue()));

			// get方法和列番号配对
			for (Method method : methods)
			{
				String methodName = method.getName();

				// get方法
				if (methodName.startsWith("get"))
				{
					String key = methodName.substring(3);
					// get方法和列番号配对 不区分大小写
					if (key.equalsIgnoreCase(entry.getKey()))
					{
						// key： 方法名 value： 列番号
						colNumberAndMethodName.put(methodName, String.valueOf(index));
						break;
					}
				}
			}
			index++;
		}

		for (int i = 0; i < javaBeanList.size(); i++)
		{
			XSSFRow xssfRow = xssfSheet.createRow(i + 1);
			for (Map.Entry<String, String> entry : colNumberAndMethodName.entrySet())
			{

				XSSFCell xssfCell = xssfRow.createCell(Integer.parseInt(entry.getValue()));

				Method method = javaBeanList.get(i).getClass().getMethod(entry.getKey());

				// 浮点型
				if (method.invoke(javaBeanList.get(i)) instanceof Float)
				{
					xssfCell.setCellValue((String) method.invoke(javaBeanList.get(i)));
					// 整形
				}
				else if (method.invoke(javaBeanList.get(i)) instanceof Integer)
				{
					xssfCell.setCellValue((Integer) method.invoke(javaBeanList.get(i)));
					// 字符型
				}
				else
				{
					xssfCell.setCellValue((String) method.invoke(javaBeanList.get(i)));
				}
			}
		}

		return xssfWorkbook;
	}

	/**
	 * JavaBean 写成Excel2003 javabean的属性 ：1.字符型 2.串整形 3.浮点型
	 * 
	 * @param sheetName
	 *            sheet名
	 * @param colNameMap
	 *            LinkedHashMap(排列顺序) javabean的属性(不区分大小写)和Excel列名关联
	 *            key：javabean的属性，value：Excel列名
	 * @param javaBeanList
	 * @return
	 * @throws Exception
	 */
	public static <T> HSSFWorkbook beanToExcel2003(String sheetName, Map<String, String> colNameMap, List<T> javaBeanList) throws Exception
	{
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();

		// 新建sheet
		HSSFSheet hssfSheet = hssfWorkbook.createSheet(sheetName);

		// 表头列
		HSSFRow firstHSSFRow = hssfSheet.createRow(0);

		// bean的所有方法
		Method[] methods = javaBeanList.get(0).getClass().getMethods();

		// get方法和列番号
		Map<String, String> colNumberAndMethodName = new LinkedHashMap<String, String>();

		// 计数器
		int index = 0;
		for (Map.Entry<String, String> entry : colNameMap.entrySet())
		{
			// 设置表头
			HSSFCell hssfCell = firstHSSFRow.createCell(index);
			hssfCell.setCellValue(new HSSFRichTextString(entry.getValue()));

			// get方法和列番号配对
			for (Method method : methods)
			{
				String methodName = method.getName();

				// get方法
				if (methodName.startsWith("get"))
				{
					String key = methodName.substring(3);

					// get方法和列番号配对 不区分大小写
					if (key.equalsIgnoreCase(entry.getKey()))
					{
						// key： 方法名 value： 列番号
						colNumberAndMethodName.put(methodName, String.valueOf(index));
						break;
					}
				}
			}
			index++;
		}

		for (int i = 0; i < javaBeanList.size(); i++)
		{
			HSSFRow hssfRow = hssfSheet.createRow(i + 1);
			for (Map.Entry<String, String> entry : colNumberAndMethodName.entrySet())
			{

				HSSFCell hssfCell = hssfRow.createCell(Integer.parseInt(entry.getValue()));

				Method method = javaBeanList.get(i).getClass().getMethod(entry.getKey());
				// 浮点型
				if (method.invoke(javaBeanList.get(i)) instanceof Float)
				{
					hssfCell.setCellValue((String) method.invoke(javaBeanList.get(i)));
					// 整形
				}
				else if (method.invoke(javaBeanList.get(i)) instanceof Integer)
				{
					hssfCell.setCellValue((Integer) method.invoke(javaBeanList.get(i)));
					// 字符型
				}
				else
				{
					hssfCell.setCellValue((String) method.invoke(javaBeanList.get(i)));
				}
			}
		}

		return hssfWorkbook;
	}

	/**
	 * excel文件输出
	 * 
	 * @param fileType
	 * @param sheetName
	 * @param colNameMap
	 * @param javaBeanList
	 * @return
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public static Workbook createExcel(String fileType, String sheetName, Map<String, String> colNameMap, List beanList) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
	{
		Workbook workbook = null;

		// 根据文件类型选择创建格式excel
		if (HSS_TYPE.equals(fileType))
		{
			workbook = new HSSFWorkbook();
		}
		if (XSS_TYPE.equals(fileType))
		{
			workbook = new XSSFWorkbook();
		}

		workbook = makeWorkbook(sheetName, workbook, colNameMap, beanList);
		// 配置excel
		return workbook;
	}

	/**
	 * 配置excel
	 * 
	 * @param sheetName
	 * @param wookbook
	 * @param colNameMap
	 * @param javaBeanList
	 * @return
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private static Workbook makeWorkbook(String sheetName, Workbook wookbook, Map<String, String> colNameMap, List beanList) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
	{
		// 新建sheet
		Sheet sheet = wookbook.createSheet(sheetName);

		// 获取表头
		Row header = sheet.createRow(0);

		// 设置表头
		Map<String, String> colNumberMap = null;

		colNumberMap = configHeader(header, colNameMap, beanList);
		if (!beanList.isEmpty())
		{
			// 添加数据
			addData(sheet, beanList, colNumberMap);
		}

		return wookbook;
	}

	/**
	 * 表头配置
	 * 
	 * @param header
	 * @param colNameMap
	 * @param methods
	 * @return
	 */
	private static Map<String, String> configHeader(Row header, Map<String, String> colNameMap, List beanList)
	{
		Map<String, String> colNumberAndMethodName = new LinkedHashMap<String, String>();

		// 计数器
		int index = 0;
		for (Map.Entry<String, String> entry : colNameMap.entrySet())
		{
			// 设置表头
			Cell cell = header.createCell(index);

			RichTextString rts = null;

			// 判断表头类型
			if (header instanceof HSSFRow)
			{
				rts = new HSSFRichTextString(entry.getValue());
			}
			else
			{
				rts = new XSSFRichTextString(entry.getValue());
			}

			cell.setCellValue(rts);

			Method[] methods = new Method[0];
			if (!beanList.isEmpty())
			{
				methods = beanList.get(0).getClass().getMethods();
			}

			// get方法和列番号配对
			for (Method method : methods)
			{
				String methodName = method.getName();

				// get方法
				if (methodName.startsWith("get"))
				{
					String key = methodName.substring(3);

					// get方法和列番号配对 不区分大小写
					if (key.equalsIgnoreCase(entry.getKey()))
					{
						// key： 方法名 value： 列番号
						colNumberAndMethodName.put(methodName, String.valueOf(index));
						break;
					}
				}
			}
			index++;
		}
		return colNumberAndMethodName;
	}

	private static Map<String, String> configHeader(Row header, Map<String, String> colNameMap, Set<String> keys)
	{

		Map<String, String> colNumberAndKey = new LinkedHashMap<String, String>();
		int index = 0;
		for (Map.Entry<String, String> entry : colNameMap.entrySet())
		{
			// 设置表头
			Cell cell = header.createCell(index);

			RichTextString rts = null;

			// 判断表头类型
			if (header instanceof HSSFRow)
			{
				rts = new HSSFRichTextString(entry.getValue());
			}
			else
			{
				rts = new XSSFRichTextString(entry.getValue());
			}
			cell.setCellValue(rts);

			// get方法和列番号配对
			for (String key : keys)
			{
				// get方法和列番号配对 不区分大小写
				if (key.equalsIgnoreCase(key))
				{
					// key： 方法名 value： 列番号
					colNumberAndKey.put(key, String.valueOf(index));
					break;
				}

			}
			index++;
		}
		return colNumberAndKey;
	}

	/**
	 * 增加数据
	 * 
	 * @param <T>
	 * 
	 * @param sheet
	 * @param javaBeanList
	 * @param colNumberAndMethodName
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private static void addData(Sheet sheet, List javaBeanList, Map<String, String> colNumberAndMethodName) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{

		for (int i = 0; i < javaBeanList.size(); i++)
		{
			Row row = sheet.createRow(i + 1);
			for (Map.Entry<String, String> entry : colNumberAndMethodName.entrySet())
			{

				Cell cell = row.createCell(Integer.parseInt(entry.getValue()));

				Object bean = javaBeanList.get(i);

				Method method = bean.getClass().getMethod(entry.getKey());

				// 对象值
				Object value = method.invoke(bean);
				if (value == null)
				{
					value = "";
				}
				if (entry.getKey().equals("keep_price"))
				{
					if (value != null)
					{
						String valueStr = String.valueOf(value);
						float keepPrice = Float.parseFloat(valueStr);
						if (keepPrice != 0)
						{
							value = "" + (keepPrice * 100);
						}else
						{
							value = "";
						}
					}
				}

				cell.setCellValue(String.valueOf(value));

				/*
				 * // 浮点型 if (value instanceof Float) {
				 * 
				 * cell.setCellValue(String.valueOf(value)); // 整形 } else if
				 * (value instanceof Integer) {
				 * cell.setCellValue((Integer)value);
				 * 
				 * // 字符型 } else { cell.setCellValue((String)value); }
				 */
			}
		}

	}

}
