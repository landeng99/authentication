package com.xiangrui.lmp.util;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class TokenHandler {

	private static Logger logger = Logger.getLogger(TokenHandler.class);

	static Map<String, String> springmvc_token = new HashMap<String, String>();

	// 生成一个唯一值的token
	@SuppressWarnings("unchecked")
	public synchronized static String generateGUID(HttpSession session) {
		String token = "";
		try {
			Object obj = session.getServletContext().getAttribute(TokenConstants.DEFAULT_TOKEN_NAME);
			if (obj != null)
				springmvc_token = (Map<String, String>) session.getServletContext()
						.getAttribute(TokenConstants.DEFAULT_TOKEN_NAME);
			token = new BigInteger(165, new Random()).toString(36)
					.toUpperCase();
			springmvc_token.put(
					TokenConstants.DEFAULT_TOKEN_NAME + "." + token, token);
			session.getServletContext().setAttribute(TokenConstants.DEFAULT_TOKEN_NAME, springmvc_token);
			TokenConstants.TOKEN_VALUE = token;

		} catch (IllegalStateException e) {
			logger.error("generateGUID() mothod find bug,by token session...");
		}
		return token;
	}

	// 验证表单token值和session中的token值是否一致
	@SuppressWarnings("unchecked")
	public static boolean validToken(HttpServletRequest request) {
		String inputToken = getInputToken(request);

		if (inputToken == null) {
			logger.warn("token is not valid!inputToken is NULL");
			return false;
		}

		HttpSession session = request.getSession();
		Map<String, String> tokenMap = (Map<String, String>) session.getServletContext()
				.getAttribute(TokenConstants.DEFAULT_TOKEN_NAME);
		if (tokenMap == null || tokenMap.size() < 1) {
			logger.warn("token is not valid!sessionToken is NULL");
			return false;
		}
		String sessionToken = tokenMap.get(TokenConstants.DEFAULT_TOKEN_NAME
				+ "." + inputToken);
		if (!inputToken.equals(sessionToken)) {
			logger.warn("token is not valid!inputToken='" + inputToken
					+ "',sessionToken = '" + sessionToken + "'");
			return false;
		}
		tokenMap.remove(TokenConstants.DEFAULT_TOKEN_NAME + "." + inputToken);
		session.getServletContext().setAttribute(TokenConstants.DEFAULT_TOKEN_NAME, tokenMap);

		return true;
	}

	// 获取表单中token值
	public static String getInputToken(HttpServletRequest request) {
		Map params = request.getParameterMap();

		if (!params.containsKey(TokenConstants.DEFAULT_TOKEN_NAME)) {
			logger.warn("Could not find token name in params.");
			return null;
		}

		String[] tokens = (String[]) (String[]) params
				.get(TokenConstants.DEFAULT_TOKEN_NAME);

		if ((tokens == null) || (tokens.length < 1)) {
			logger.warn("Got a null or empty token name.");
			return null;
		}

		return tokens[0];
	}

}