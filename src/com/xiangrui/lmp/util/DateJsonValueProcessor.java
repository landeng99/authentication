package com.xiangrui.lmp.util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.log4j.Logger;

public class DateJsonValueProcessor implements JsonValueProcessor {  
	private static Logger logger = Logger.getLogger(DateJsonValueProcessor.class);
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";  
    private DateFormat dateFormat;  
  
    /** 
     * 构造方法. 
     * 
     * @param datePattern 日期格式 
     */  
    public DateJsonValueProcessor(String datePattern) {  
        try {  
            dateFormat = new SimpleDateFormat(datePattern);  
        } catch (Exception ex) {  
            dateFormat = new SimpleDateFormat(DEFAULT_DATE_PATTERN);  
        }  
    }  
  
    public Object processArrayValue(Object value, JsonConfig jsonConfig) {  
        return process(value);  
    }  
  
    public Object processObjectValue(String key, Object value,  
        JsonConfig jsonConfig) {  
        return process(value);  
    }  
  
    private Object process(Object value) {  
    	try{
    		if(value==null){
    			return null;
    		}else{
    			Object str= dateFormat.format((Date) value);
                return str;
    		}
    		
    	}catch(Exception ex)
    	{
    		logger.error("时间转化错误"+value,ex);
    		return null;
    	}
    	  
    }  
   
    /**
     * demo
     * @param args
     */
    public static void main(String[] args) {
    	
    	//excludes 不转换的属性数组 
    	
    	String[] excludes = new String[1];
		excludes[0] = "333";
    	JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(excludes);
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		jsonConfig.registerJsonValueProcessor(Date.class,
				new DateJsonValueProcessor(DEFAULT_DATE_PATTERN));
    	Object bean=null;
		JSONObject jsonObject = JSONObject.fromObject(bean, jsonConfig);
	}
 
}  