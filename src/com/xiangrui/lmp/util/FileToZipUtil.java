package com.xiangrui.lmp.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;

public class FileToZipUtil
{

    private static final Logger logger = Logger.getLogger(FileToZipUtil.class);

    public synchronized static boolean fileToZip(String sourceFilePath,
            ZipOutputStream zos)
    {
        boolean flag = false;

        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try
        {
            File file = new File(sourceFilePath);

            if (!file.exists())
            {
                return false;
            }

            byte[] bufs = new byte[1024 * 10];

            // 创建ZIP实体，并添加进压缩包
            ZipEntry zipEntry = new ZipEntry(file.getName());
             
            zos.putNextEntry(zipEntry);

            // 读取待压缩的文件并写进压缩包里
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis, 1024 * 10);

            int read = 0;
            while ((read = bis.read(bufs, 0, 1024 * 10)) != -1)
            {
                zos.write(bufs, 0, read);
            }
            zos.flush();
            
            if (fis != null)
            {
                fis.close();
            }

            if (bis != null)
            {
                bis.close();
            }

            flag = true;

        } catch (FileNotFoundException e)
        {
            logger.error("文件找不到", e);
            return false;

        } catch (IOException e)
        {
            logger.error("输出异常", e);
            return false;

        }
        return flag;
    }

    public synchronized static ZipOutputStream createZip(String zipFilePath)
    {
        ZipOutputStream zos = null;
        File zipFile = new File(zipFilePath);
        if (zipFile.exists())
        {
            zipFile.delete();
        }

        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(new BufferedOutputStream(fos));
        } catch (FileNotFoundException e)
        {

            e.printStackTrace();
        }
        return zos;

    }

}
