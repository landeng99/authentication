/** 
 * @projectName lmp. 
 * @fileName StringUtil.java 
 * @packageName com.xiangrui.lmp.util 
 * @Date:2015年4月23日11:02:59
 * @Copyright (c) 2014, http://www.xabuild.com All Rights Reserved. 
 */
package com.xiangrui.lmp.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

/**
 * 
 * class Name [StringUtil].
 * description [String工具类] 
 * @author ltp
 * @version 1.0
 * @since JDK 1.7
 */
public class StringUtil implements Serializable {

    public StringUtil() {
        super();
    }

    /**
     * 序列化id.
     */
    private static final long serialVersionUID = -4070301361637214448L;

    /**
     * 日期格式字符串.
     */
    public static final String DEFAULT_TIEMSTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss";
    
    /**
     * 大于号.
     */
    public static final int HIGHEST_SPECIAL = '>';
    
    /**
     * 20.
     */
    public static final int LENGHT_STRING = 20;
    
    /**
     * 长度增加值.
     */
    public static final int LENGHT_ADD = 5;
    
    /**
     * 逗号.
     */
    private static final String COMMA_SYMBOL = ",";
    
    /**
     * 
     */
    public static char[][] specialCharactersRepresentation = new char[HIGHEST_SPECIAL + 1][];
    
    static {
        specialCharactersRepresentation['&'] = "&amp;".toCharArray();
        specialCharactersRepresentation['<'] = "&lt;".toCharArray();
        specialCharactersRepresentation['>'] = "&gt;".toCharArray();
        specialCharactersRepresentation['"'] = "&#034;".toCharArray();
        specialCharactersRepresentation['\''] = "&#039;".toCharArray();
    }
    
    /**
     * <p>Checks if a String is whitespace, empty ("") or null.</p>
     *
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank("")        = true
     * StringUtils.isBlank(" ")       = true
     * StringUtils.isBlank("bob")     = false
     * StringUtils.isBlank("  bob  ") = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @return <code>true</code> if the String is null, empty or whitespace
     * @since 2.0
     */
    public static boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((Character.isWhitespace(str.charAt(i)) == false)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 
     * isNotEmpty:(判断不为空).
     * @author Administrator 
     * @param str 字符串
     * @return 不为空-true; 空-false
     */
    public static boolean isNotEmpty(String str) {
        if (str == null || str.trim().length() == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 
     * isEmpty:(判断为空).
     * @author Administrator 
     * @param str 字符串
     * @return 空-true; 不为空-false
     */
    public static boolean isEmpty(String str) {
        if (str == null || str.trim().length() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
	 * 字符串 长度验证 描述
	 * 
	 * @param str
	 *            传入字符串
	 * @param length
	 *            验证的长度
	 * @return str 为空 验证通过 反之返回提示信息
	 */
	public static String verifyStrLength(String str, Integer length) {
		String _str = "";
		if (!isEmpty(str)) {// 字符串不为空
			if (str.length() > length) {// 字符串 超长
				_str = "超长";
			} else {// 合法字符串
				_str = "";
			}
		} else {// 字符串为空
			_str = "为空";
		}
		return _str;
	}
    
    /**
     * 
     * toArray:(将字符串转换为字符数组).
     * @author Administrator 
     * @param value 待转字符串
     * @param delimeters 分隔符
     * @return 字符数组
     */
    public static String[] toArray(String value, String delimeters) {
        return (String[]) toList(value, delimeters).toArray();
    }

    /**
     * 
     * toList:(将字符串转换为字符列表).
     * @author Administrator 
     * @param value 待转字符串
     * @param delimeters 分隔符
     * @return 字符列表
     */
    public static List<Object> toList(String value, String delimeters) {
        List<Object> list = new ArrayList<Object>();
        if (isEmpty(value)) {
            return list;
        }
        StringTokenizer st = new StringTokenizer(value, delimeters, false);
        while (st.hasMoreTokens()) {
            list.add(st.nextToken());
        }
        return list;
    }
    
    /**
     * 
     * toList:(将字符串转换为字符列表).
     * @author Administrator 
     * @param value 待转字符串
     * @param delimeters 分隔符
     * @return 字符列
     */
    public static List<String> toStringList(String value, String delimeters) {
        List<String> list = new ArrayList<String>();
        if (isEmpty(value)) {
            return list;
        }
        StringTokenizer st = new StringTokenizer(value, delimeters, false);
        while (st.hasMoreTokens()) {
            list.add(st.nextToken());
        }
        return list;
    }

    /**
     * 
     * toListByStr:(将字符串通过分隔符转换为字符串列表).
     * @author Administrator 
     * @param value 待转字符串
     * @param separator 分隔符
     * @return 字符串列表
     */
    public static List<Object> toListByStr(String value, String separator) {
        List<Object> list = new ArrayList<Object>();
        String token;
        int index0 = 0;
        int index1 = value.indexOf(separator, 0);
        if (index1 == -1) {
            list.add(value);
        } else {
            while (true) {
                token = value.substring(index0, index1);
                if (StringUtil.isNotEmpty(token)) {
                    list.add(token);
                }
                index0 = index1 + separator.length();
                index1 = value.indexOf(separator, index0);
                if (index1 == -1) {
                    token = value.substring(index0);
                    if (StringUtil.isNotEmpty(token)) {
                        list.add(token);
                    }
                    break;
                }
            }
        }
        return list;
    }

    /**
     * 
     * convertToQuestionMark:(将列表转换为问号字符串).
     * @author Administrator 
     * @param s 待转列表
     * @return 问号字符串
     */
    @SuppressWarnings("rawtypes")
    public static String convertToQuestionMark(List s) {
        StringBuffer buf = new StringBuffer();
        for (int index = 0; index < s.size(); index++) {
            buf.append("?,");
        }
        int end = buf.lastIndexOf(COMMA_SYMBOL);
        return buf.substring(0, end);
    }

    /**
     * 
     * convertToQuestionMark:(将对象数组转换为问号字符串).
     * @author Administrator 
     * @param s 待转对象列表
     * @return 问号字符串
     */
    public static String convertToQuestionMark(Object[] s) {
        StringBuffer buf = new StringBuffer();
        for (int index = 0; index < s.length; index++) {
            buf.append("?,");
        }
        int end = buf.lastIndexOf(COMMA_SYMBOL);
        return buf.substring(0, end);
    }

    /**
     * 
     * convertToStr:(将字符串数组转换为字符串).
     * @author Administrator 
     * @param s 待转字符串数组
     * @return 转换后的字符串
     */
    public static String convertToStr(String[] s) {
        StringBuffer buf = new StringBuffer();
        for (int index = 0; index < s.length; index++) {
            buf.append("'").append(s[index]).append("'").append(COMMA_SYMBOL);
        }
        int end = buf.lastIndexOf(COMMA_SYMBOL);
        return buf.substring(0, end);
    }

    /**
     * 
     * convertToStr:(将Long型数组转换为字符串).
     * @author Administrator 
     * @param s 待转数组
     * @return 转换后字符串
     */
    public static String convertToStr(Long[] s) {
        StringBuffer buf = new StringBuffer();
        for (int index = 0; index < s.length; index++) {
            buf.append(s[index]).append(COMMA_SYMBOL);
        }
        int end = buf.lastIndexOf(COMMA_SYMBOL);
        return buf.substring(0, end);
    }

    /**
     * 
     * exist:(判断字符串数组中是否包含某个字符串).
     * @author Administrator 
     * @param arr 字符串数组
     * @param item 目标字符串
     * @return 包含-true;不包含-false
     */
    public static boolean exist(String[] arr, String item) {
        if (arr != null) {
            for (int index = 0; index < arr.length; index++) {
                if (arr[index].equals(item)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 
     * escapeXml:(将字符串中需要转移的字符替换为转移字符).
     * @author Administrator 
     * @param buffer 待转字符串
     * @return 转移后的字符串
     */
    public static String escapeXml(String buffer) {
        int start = 0;
        int length = buffer.length();
        char[] arrayBuffer = buffer.toCharArray();
        StringBuffer escapedBuffer = null;

        for (int i = 0; i < length; i++) {
            char c = arrayBuffer[i];
            if (c <= HIGHEST_SPECIAL) {
                char[] escaped = specialCharactersRepresentation[c];
                if (escaped != null) {
                    // create StringBuffer to hold escaped xml string
                    if (start == 0) {
                        escapedBuffer = new StringBuffer(length + LENGHT_ADD);
                    }
                    // add unescaped portion
                    if (start < i) {
                        escapedBuffer.append(arrayBuffer, start, i - start);
                    }
                    start = i + 1;
                    // add escaped xml
                    escapedBuffer.append(escaped);
                }
            }
        }
        // no xml escaping was necessary
        if (start == 0) {
            return buffer;
        }
        // add rest of unescaped portion
        if (start < length) {
            escapedBuffer.append(arrayBuffer, start, length - start);
        }
        return escapedBuffer.toString();
    }

    /**
     * 
     * trimTo20Char:(字符串截取长度20).
     * @author Administrator 
     * @param str 待截取的字符串
     * @return 截取后的字符串
     */
    public static String trimTo20Char(String str) {
        if (str.length() > LENGHT_STRING) {
            return str.substring(0, LENGHT_STRING) + "...";
        } else {
            return str;
        }
    }

    /**
     * 
     * escapeVBS:(转移字符).
     * @author Administrator 
     * @param str 待转字符串
     * @return 转义后的字符串
     */
    public static String escapeVBS(String str) {
        if (str == null)
            return "";

        StringBuffer sb = new StringBuffer();
        char ch;
        for (int i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (ch == '\"') {
                sb.append("\"&chr(34)&\"");
            } else {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    /**
     * 
     * replaceMatchAsSQL:(替换匹配符).
     * @author Administrator 
     * @param str 待转字符串
     * @return 替换后的字符串
     */
    public static String replaceMatchAsSQL(String str) {
        if (isEmpty(str)) {
            return null;
        }
        boolean isOnlyWildcard = true;
        char[] chs = str.toCharArray();
        for (char ch : chs) {
            if (ch != '?' && ch != '*') {
                isOnlyWildcard = false;
                break;
            }
        }
        if (isOnlyWildcard) {
            return null;
        }
        return escapeSQL(str).replaceAll("\\*", "%").replaceAll("\\?", "_");
    }

    /**
     * 
     * escapeSQL:(转义字符).
     * @author Administrator 
     * @param str 待转字符串
     * @return 转义后的字符串
     */
    public static String escapeSQL(String str) {
        boolean isWildcard = false;
        char[] chs = str.toCharArray();
        for (char ch : chs) {
            if (ch == '%' || ch == '_') {
                isWildcard = true;
                break;
            }
        }
        if (!isWildcard) {
            return str;
        }
        return str.replaceAll("%", "/%").replaceAll("_", "/_");
    }

    /**
     * 
     * getParameter:(获取参数).
     * @author Administrator 
     * @param parameterSuffix 后缀
     * @param parameterName 参数名称
     * @return 后缀存在-参数名_后缀；后缀不存在-空字符串
     */
    public static String getParameter(Long parameterSuffix, String parameterName) {
        if (parameterSuffix == null || isEmpty(parameterName)) {
            return "";
        }
        return parameterName + "_" + String.valueOf(parameterSuffix);
    }

    /**
     * 
     * decodeURI:(解码RUI).
     * @author Administrator 
     * @param uri 统一资源标识符
     * @return 解码后的URI
     */
    public static String decodeURI(String uri) {
        try {
            return java.net.URLDecoder.decode(uri, "UTF-8");
        } 
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 
     * isLong:(是否是Long型).
     * @author Administrator 
     * @param value 待判断字符串
     * @return 是-ture;否-false
     */
    public static boolean isLong(String value) {
        if (StringUtil.isEmpty(value)) {
            return false;
        }
        if (value.matches("[1-9]{1}[0-9]*")) {            
            return true;
        }
        return false;
    }
    
    /**
     * 
     * exchangeExp:(交换表达式).
     * @author Administrator 
     * @param newexp 新表达式
     * @param oldexp 旧表达式
     * @param content 内容
     * @return 交换后的表达式
     */
    public static String exchangeExp(String newexp, String oldexp, String content) {
        if (StringUtil.isEmpty(content)) {
            return content;
        }
        if (newexp.equals(oldexp)) {
            return content;
        }
        String result = content;
        int o1 = result.indexOf(oldexp);
        int index = 0;
        while (-1 != o1) {
            result = exchangeExp1(newexp, oldexp, result, index);
            index = o1 + newexp.length();
            o1 = result.indexOf(oldexp, index);
        }
        return result;
    }
    
    /**
     * 
     * exchangeExp1:(交换表达式).
     * @author Administrator 
     * @param newexp 新表达式
     * @param oldexp 旧表达式
     * @param content 待处理字符串
     * @param index 索引
     * @return 交换后的字符串
     */
    private static String exchangeExp1(String newexp, String oldexp, String content, int index) {
        int o1 = content.indexOf(oldexp, index);
        if (-1 == o1) {
            return content;
        }
        StringBuffer sb = new StringBuffer();
        sb.append(content.substring(0, o1));
        sb.append(newexp);
        sb.append(content.substring(o1 + oldexp.length(), content.length()));
        return sb.toString();
    }

    /**
     * 
     * empty:(判断对象是否为空).
     * @author Administrator 
     * @param obj 待判断对象
     * @return 为空返回true,否则返回false
     */
    public static boolean isEmpty(Object obj) {
        
        boolean flag = false;
        if (obj == null) {
            flag = true;
        } 
        else if (obj instanceof String && ("".equals(obj))) {
            flag = true;
        } 
        else if (obj instanceof Number && 0 == ((Number) obj).doubleValue()) {
            flag = true;
        } 
        else if (obj instanceof Boolean && !((Boolean) obj)) {
            flag = true;
        } 
        else if (obj instanceof Collection && ((Collection) obj).isEmpty()) {
            flag = true;
        } 
        else if (obj instanceof Map && ((Map) obj).isEmpty()) {
            flag = true;
        } 
        else if (obj instanceof Object[] && ((Object[]) obj).length == 0) {
            flag = true;
        }
        return flag;
    }
    
    /**
     * 
     * exceptSplit:(去掉字符串最后的逗号).
     * @author Administrator 
     * @param str 待处理的字符串
     * @return 处理后的字符串
     */
    public static StringBuilder exceptSplit(StringBuilder str) {
        Integer len = str.length();
        if (len > 0) {
            Integer _str = (str.lastIndexOf(COMMA_SYMBOL) + 1);
            if (len.equals(_str)) {
                str.deleteCharAt(len - 1);
            }
        }
        return str;
    }
     
    public static String radomString(){
         char[] digits = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
            'Z' };
         int number=digits.length;
        
        
         StringBuffer buffer=new StringBuffer();
         for(int i=0;i<6;i++){
             int index=new Random().nextInt(26);
             System.out.println(index);
             buffer.append(digits[index]);       
         }
         //String str=
         return buffer.toString();
    }
    
    public static String getFromResourceBundle(ResourceBundle config,String key)
    {
    	String value=null;
    	if(config.containsKey(key))
    	{
    		value=config.getString(key);
    	}
    	return value;
    }
    public static void main(String[] args)
    {
        System.out.println(radomString());
    }
    
}
