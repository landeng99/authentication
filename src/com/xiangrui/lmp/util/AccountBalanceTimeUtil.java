package com.xiangrui.lmp.util;

import java.util.Date;
import java.util.Calendar;

/**
 * 金额统计工具类
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-6-3 下午9:03:17
 * </p>
 */
public class AccountBalanceTimeUtil
{

    /**
     * 时间改变天数
     * 
     * @param date
     *            改变时间的基础
     * @param num
     *            改变的时间间隔,以天为单位,正为增,负为减
     * @return
     */
    public static Date addDay(Date date, int num)
    {
        Calendar startDT = Calendar.getInstance();
        startDT.setTime(date);
        startDT.add(Calendar.DAY_OF_MONTH, num);
        return startDT.getTime();
    }

    /**
     * 时间改变天数
     * 
     * @param date
     *            改变时间的基础
     * @param num
     *            改变的时间间隔,以月为单位,正为增,负为减
     * @return
     */
    public static Date addMonth(Date date, int num)
    {
        Calendar startDT = Calendar.getInstance();
        startDT.setTime(date);
        startDT.add(Calendar.MONTH, num);
        return startDT.getTime();
    }
}
