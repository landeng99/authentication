package com.xiangrui.lmp.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFormatUtils extends org.apache.commons.lang.time.DateFormatUtils
{
	private DateFormatUtils()
	{
	}

	public static String formatDate(Date date)
	{
		return DateFormat.getDateInstance().format(date);
	}

	/**
	 * 日期加1天
	 * 
	 * @param dateString
	 * @return String
	 */
	public static String addOneDay(String dateString)
	{

		if (dateString == null || dateString == "")
		{
			return "";
		}
		// 时间格式
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date date = null;
		try
		{
			date = simpleDateFormat.parse(dateString);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		Calendar calendar = Calendar.getInstance();
		// 设置时间
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		return simpleDateFormat.format(calendar.getTime());
	}

	/**
	 * 获取当年的字符串
	 * 
	 * @return
	 */
	public static String getCurrentYearString()
	{

		// 时间格式
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date();
		return simpleDateFormat.format(date);
	}

	/**
	 * 获取下一年的字符串
	 * 
	 * @return
	 */
	public static String getNextYearString()
	{

		// 时间格式
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		// 设置时间
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, 1);
		return simpleDateFormat.format(calendar.getTime());
	}
	
	/**
	 * 获取上一年的字符串
	 * 
	 * @return
	 */
	public static String getPreviousYearString()
	{

		// 时间格式
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		// 设置时间
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, -1);
		return simpleDateFormat.format(calendar.getTime());
	}
	
	/**
	 * 获取下n日的时间字符串
	 * 
	 * @return
	 */
	public static String getNextDayString(int n)
	{

		// 时间格式
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		// 设置时间
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, n);
		return simpleDateFormat.format(calendar.getTime());
	}
}