/** 
 * @projectName lmp. 
 * @fileName IpUtil.java 
 * @packageName com.xiangrui.lmp.util 
 * @company kingleadsw.
 * @create 2014年8月6日下午3:30:34 
 */
package com.xiangrui.lmp.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 * class Name [IpUtil]. 
 * description [IP工具类]
 * @author ltp
 * @version 1.0
 * @since JDK 1.7
 */
public class IpUtil {

    public IpUtil() {
        super();
    }
    
    public static long ipToLong(String ipAddress) {
		   
        long result = 0;  
        try{
        	 String[] ipAddressInArray = ipAddress.split("\\.");  
             
             for (int i = 0; i <= 3; i++) {  
                 long ip = Long.parseLong(ipAddressInArray[3-i]);  
                 result |= ip << ((3-i) * 8);  
        
             }  
        } catch (Exception e) {
        	
        } finally {
        	
        }
   
        return result;  
    }  
   
    public static String longToIp(long i) {  
    	return (i & 0xFF) +   
	            "." + ((i >> 8) & 0xFF) +   
	            "." + ((i >> 16) & 0xFF) +   
	            "." + ((i >> 24) & 0xFF);
   
    } 
    
	public static void main(String[] args) {
		String ip="172.16.0.77";
		Long ipL = 1291849900L;
		System.out.println(ipToLong(ip));
		System.out.println(longToIp(ipL));
	}

    /**
     * 
     * getIpAddr:(获取登录用户的IP地址).
     * @author Administrator 
     * @param request 请求
     * @return ip地址
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if ("0:0:0:0:0:0:0:1".equals(ip)) {
            ip = "本地";
        }
        if (ip.split(",").length > 1) {
            ip = ip.split(",")[0];
        }
        return ip;
    }

    /**
     * 
     * getIpInfo:(通过IP获取地址).
     * @author Administrator 
     * @param ip 地址
     * @return ip地址
     */
    public static String getIpInfo(String ip) {
        String info = "";
        try {
            URL url = new URL("" + ip);
            HttpURLConnection htpcon = (HttpURLConnection) url.openConnection();
            htpcon.setRequestMethod("GET");
            htpcon.setDoOutput(true);
            htpcon.setDoInput(true);
            htpcon.setUseCaches(false);

            InputStream in = htpcon.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(in));
            StringBuffer temp = new StringBuffer();
            String line = bufferedReader.readLine();
            while (line != null) {
                temp.append(line).append("\r\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            JSONObject obj = (JSONObject) JSON.parse(temp.toString());
            if (obj.getIntValue("code") == 0) {
                JSONObject data = obj.getJSONObject("data");
                info += data.getString("country") + " ";
                info += data.getString("region") + " ";
                info += data.getString("city") + " ";
                info += data.getString("isp");
            }
        } 
        catch (MalformedURLException e) {
            e.printStackTrace();
        } 
        catch (ProtocolException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
        return info;
    }

}
