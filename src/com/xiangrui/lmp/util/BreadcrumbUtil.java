package com.xiangrui.lmp.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.xiangrui.lmp.business.admin.breadcrumb.vo.Breadcrumb;

public class BreadcrumbUtil
{

    /**
     * 面包切
     * 
     * @param request
     */
    @SuppressWarnings("unchecked")
	public synchronized static void push(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        String path = request.getServletPath();

        Stack<Breadcrumb> breadcrumbStack = (Stack<Breadcrumb>) session
                .getAttribute("breadcrumbStack");
        if (breadcrumbStack == null)
        {
            breadcrumbStack = new Stack<Breadcrumb>();
            session.setAttribute("breadcrumbStack", breadcrumbStack);
        }

        if (!breadcrumbStack.isEmpty())
        {

            // 连续访问同一地址则更换参数
            Breadcrumb currBreadcrumb = breadcrumbStack.peek();
            if (currBreadcrumb.getUrl().equals(path))
            {
                Map<String, Object> params = parseRequest(request);

                // 更新当前请求
                currBreadcrumb.setParams(params);
                return;
            }
        }

        // 记录当前路径
        Breadcrumb item = new Breadcrumb();
        item.setUrl(path);
        Map<String, Object> params = parseRequest(request);
        item.setParams(params);
        breadcrumbStack.push(item);
    }

    /**
     * 解析请求参数
     * 
     * @param request
     * @return
     */
    private static Map<String, Object> parseRequest(HttpServletRequest request)
    {

        Map properties = request.getParameterMap();

        Iterator entries = properties.entrySet().iterator();

        Map<String, Object> params = new HashMap<String, Object>();
        String name = "";
        String value = "";
        Map.Entry entry;
        while (entries.hasNext())
        {
            entry = (Map.Entry) entries.next();
            name = (String) entry.getKey();
            Object valueObj = entry.getValue();
            if (null == valueObj)
            {
                value = "";
            } else if (valueObj instanceof String[])
            {
                String[] values = (String[]) valueObj;
                for (int i = 0; i < values.length; i++)
                {
                    value = values[i] + ",";
                }
                value = value.substring(0, value.length() - 1);
            } else
            {
                value = valueObj.toString();
            }
            params.put(name, value);
        }

        return params;
    }

}
