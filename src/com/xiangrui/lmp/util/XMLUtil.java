package com.xiangrui.lmp.util;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;


/**
 * 
 * @author DWH
 * @version 1.0
 */

public class XMLUtil {
	
	/**
	 * 
	 * 描述 生成XML文件
	 * @param list
	 * @param xmlPath
	 * @return
	 */
	public static Document generateDocument(Object obj, String xmlPath) {
		Document doc = DocumentHelper.createDocument();
		// 解析类
		Element root = doc.addElement(obj.getClass().getSimpleName());
		Field[] fields = obj.getClass().getDeclaredFields();
		for (Field field : fields) {
			try {
				if (null != field.get(obj)) {
					Class<?> fieldClass = field.getType();
					if (ifNotBaseType(fieldClass)) {

					} else {
						Element element = root.addElement(field.getName()
								.toString());
						element.addText(field.get(obj).toString());
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		// 写文件
		writeDocument(doc, xmlPath);
		return doc;
	}
	
	/**
	 * 
	 * 描述 生成XML文件
	 * @param list
	 * @param rootNodeName
	 * @param xmlPath
	 * @return
	 */
	public static Document generateDocument(List<Object> list,
			String rootNodeName, String xmlPath) {
		Document doc = DocumentHelper.createDocument();
		// 解析List
		// 写文件
		writeDocument(doc, xmlPath);
		return doc;
	}
	
	/**
	 * 
	 * 描述 写XML文件
	 * @param doc
	 * @param xmlPath
	 */
	public static void writeDocument(Document doc, String xmlPath) {
		try {
			OutputFormat xmlFormat = new OutputFormat();
			xmlFormat.setEncoding("UTF-8");
			XMLWriter xmlWriter = new XMLWriter(new FileWriter(xmlPath),
					xmlFormat);
			xmlWriter.write(doc);
			xmlWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 描述 判断是否为基本类型
	 * @param clazz
	 * @return
	 */
	public static boolean ifNotBaseType(Class<?> clazz) {
		if (!(clazz == Byte.class || clazz == Character.class
				|| clazz == Boolean.class || clazz == Integer.class
				|| clazz == Long.class || clazz == Float.class
				|| clazz == Double.class || clazz == String.class || clazz == Date.class))
			return true;
		return false;
	}
}
