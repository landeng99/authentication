package com.xiangrui.lmp.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;


public class CommonUtil {
	private static Logger logger=Logger.getLogger(CommonUtil.class);
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("http_client_ip");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		// 如果是多级代理，那么取第一个ip为客户ip
		if (ip != null && ip.indexOf(",") != -1) {
			ip = ip.substring(ip.lastIndexOf(",") + 1, ip.length()).trim();
		}
		return ip;
	}

	/**
	 * 获取随机数据
	 * 
	 * @return
	 */
	public static synchronized String radomCode() {

		int account = (int) ((Math.random() * 9 + 1) * 100000);
		return String.valueOf(account);
	}

	public static String sendGet(String url, Map<String, String> params) {
		// 请求参数
		StringBuffer bf = new StringBuffer("");
		for (Entry<String, String> entry : params.entrySet()) {
			String name = entry.getKey();
			String value = entry.getValue();
			bf.append(name);
			bf.append("=");
			bf.append(value);
			bf.append("&");
		}
		bf.deleteCharAt(bf.length() - 1);

		StringBuffer result = new StringBuffer("");
		BufferedReader in = null;
		try {
			String urlNameString = url + "?" + bf.toString();
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 建立实际的连接
			connection.connect();

			// 获取所有响应头字段
			/*
			 * Map<String, List<String>> map = connection.getHeaderFields();
			 * 
			 * // 遍历所有的响应头字段 for (String key : map.keySet()) {
			 * System.out.println(key + "--->" + map.get(key)); }
			 */
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String line;

			while ((line = in.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
			logger.error("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
		//记录日志
	//	log(null);
		return result.toString();
	}

	/**
	 * 发送post 的http请求
	 * 
	 * @param url
	 * @param params
	 * @return
	 */
	public static synchronized String sendPost(String url,
			Map<String, String> params) {

		// 请求参数
		StringBuffer bf = new StringBuffer("?");
		for (Entry<String, String> entry : params.entrySet()) {
			String name = entry.getKey();
			String value = entry.getValue();
			bf.append(name);
			bf.append("=");
			bf.append(value);
			bf.append("&");
		}
		bf.deleteCharAt(bf.length() - 1);
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent",
					"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());

			// 发送请求参数
			out.print(bf.toString());
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	// 5160709715699
	public static String getCurrentDateNum() {
		Date dttime = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String currentTime = sdf.format(dttime);
		String str = currentTime + getRandomInteger();
		return str;
	}

	/**
	 * 随机产生一个随机数据
	 * 
	 * @return
	 */
	public static String getRandomInteger() {
		int account = (int) ((Math.random() * 6 + 1) * 100000);
		return String.valueOf(account);
	}

	public static String str(String money) {

		double ble = Double.parseDouble(money);
		NumberFormat formatter = NumberFormat.getNumberInstance();
		// 保留12位小数
		formatter.setMinimumIntegerDigits(12);
		formatter.setGroupingUsed(false);
		String mat = formatter.format(ble * 100);
		return mat;
	}
	
    /**
     * 银行名称取得
     * 
     * @param bankCode
     *            银行简码
     * @return 银行名称
     */
    public static String getBankName(String bankCode)
    {

        String nameString = "";

        // 招商银行
        if ("CMB-DEBIT".equals(bankCode))
        {
            nameString = "招商银行";
        }

        // 中国建设银行
        else if ("CCB-DEBIT".equals(bankCode))
        {
            nameString = "中国建设银行";
        }
        // 中国工商银行
        else if ("ICBC-DEBIT".equals(bankCode))
        {
            nameString = "中国工商银行";
        }

        // 中国农业银行
        else if ("ABC".equals(bankCode))
        {
            nameString = "中国农业银行";
        }

        // 交通银行
        else if ("COMM-DEBIT".equals(bankCode))
        {
            nameString = "交通银行";
        }
        // 广发银行
        else if ("GDB-DEBIT".equals(bankCode))
        {
            nameString = "广发银行";
        }
        // 中国银行
        else if ("BOC-DEBIT".equals(bankCode))
        {
            nameString = "中国银行";
        }
        // 中国光大银行
        else if ("CEB-DEBIT".equals(bankCode))
        {
            nameString = "中国光大银行";
        }
        // 上海浦东发展银行
        else if ("SPDB-DEBIT".equals(bankCode))
        {
            nameString = "上海浦东发展银行";
        }
        // 中国邮政储蓄银行
        else if ("PSBC-DEBIT".equals(bankCode))
        {
            nameString = "中国邮政储蓄银行";
        }

        // 北京银行
        else if ("BJBANK".equals(bankCode))
        {
            nameString = "北京银行";
        }
        // 上海农商银行
        else if ("SHRCB".equals(bankCode))
        {
            nameString = "上海农商银行";
        }
        // 温州银行
        else if ("WZCBB2C-DEBIT".equals(bankCode))
        {
            nameString = "温州银行";
        }
        // 交通银行
        else if ("COMM".equals(bankCode))
        {
            nameString = "交通银行";
        }
        // 中国民生银行
        else if ("CMBC".equals(bankCode))
        {
            nameString = "中国民生银行";
        }
        // 北京农村商业银行
        else if ("BJRCB".equals(bankCode))
        {
            nameString = "北京农村商业银行";
        }
        // 平安银行
        else if ("SPA-DEBIT".equals(bankCode))
        {
            nameString = "平安银行";
        }
        // 中信银行
        else if ("CITIC-DEBIT".equals(bankCode))
        {
            nameString = "中信银行";
        }
        if ("ALIPAY".equals(bankCode))
        {
            nameString = "支付宝";
        }

        return nameString;
    }

}
