/** 
 * Project Name:lmp
 * File Name:SortList.java 
 * Package Name:com.xiangrui.lmp.util 
 * Date:2015年4月23日11:02:59
 * Copyright (c) 2014, http://www.xabuild.com All Rights Reserved. 
 * 
 */
package com.xiangrui.lmp.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * ClassName: SortList <br/>
 * Function: list排序. <br/>
 * date: 2015年4月23日11:03:13 <br/>
 * 
 * @author ltp
 * @version 1.0
 * @since JDK 1.7
 * @see
 * 
 */
public class SortList<E> {

    /**
     * 定义日志.
     **/
    protected Logger logger = Logger.getLogger(SortList.class);
    
    /**
     * 
     * Sort:(对list排序). 
     * 
     * @author 朱凯 
     * @param list 待排序的list
     * @param method 通过那个字段排序，此方法为排序字段的getter方法名
     * @param sort 正序（asc或其他字符均可）/倒序（desc）
     * 
     * 使用举例：
     *  调用排序通用类  
     *  SortList<UserInfo> sortList = new SortList<UserInfo>();  
     *  按userId排序  
     *  sortList.Sort(list, "getUserId", "desc");
     */
    public void Sort(List<E> list, final String method, final String sort) {
        Collections.sort(list, new Comparator<Object>() {
            public int compare(Object a, Object b) {
                int ret = 0;
                try {
                    // 根据对象查找方法对象
                    Method m1 = ((E) a).getClass().getMethod(method, null);
                    
                    // 根据对象查找方法对象
                    Method m2 = ((E) b).getClass().getMethod(method, null);
                    
                    if (sort != null && "desc".equals(sort))
                        // 倒序
                        ret = m2.invoke(((E) b), null).toString()
                                .compareTo(m1.invoke(((E) a), null).toString());
                    else {
                        // 正序
                        ret = m1.invoke(((E) a), null).toString()
                                .compareTo(m2.invoke(((E) b), null).toString());
                    }
                } catch (NoSuchMethodException ne) {
                    logger.error(ne);
                } catch (IllegalAccessException ie) {
                    logger.error(ie);
                } catch (InvocationTargetException it) {
                    logger.error(it);
                }
                return ret;
            }
        });
    }
    
}
