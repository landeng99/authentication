package com.xiangrui.lmp.util;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;




/**
 * 
 * 
 * @author zhangliang
 * @version 1.0
 */

public class JSONUtil {
	private static ObjectMapper mapper;

	public static synchronized ObjectMapper getMapperInstance(boolean createNew) {
		if (createNew) {
			return new ObjectMapper();
		} else if (mapper == null) {
			mapper = new ObjectMapper();
		}
		return mapper;
	}

	/**
	 * 
	 * 描述　普通的javaBean转化为json
	 * 
	 * @param obj  javaBean
	 * @return 转化为json
	 * @throws IOException
	 */
	public static String beanToJson(Object obj) throws IOException {
		// 这里异常都未进行处理，而且流的关闭也不规范。开发中请勿这样写，如果发生异常流关闭不了
		ObjectMapper mapper = getMapperInstance(false);
		StringWriter writer = new StringWriter();
		JsonGenerator gen = new JsonFactory().createJsonGenerator(writer);
		mapper.writeValue(gen, obj);
		gen.close();
		String json = writer.toString();
		writer.close();
		return json;
	}

	/**
	 * 
	 * 描述 json转化为javaBean
	 * 
	 * @param json
	 * @param cls
	 * @return 
	 * @return
	 * @throws Exception
	 */
	public static <T> T jsonToBean(String json, Class<T> cls) throws Exception {
	     mapper = getMapperInstance(false);
		T vo = mapper.readValue(json, cls);
		return vo;
	}

	/**
	 * 从json中读取tagPath处的值 tagPath用 :分隔
	 * 
	 * @param json
	 * @param tagPath
	 * @return
	 * @throws Exception
	 */
	public static List<String> readValueFromJson(String json, String tagPath)
			throws Exception {
		// 返回值
		List<String> value = new ArrayList<String>();
		if (JSONUtil.isEmpty(json) || (JSONUtil.isEmpty(tagPath))) {
			return value;
		}
		ObjectMapper mapper = getMapperInstance(false);
		String[] path = tagPath.split(":");
		JsonNode node = mapper.readTree(json);
		getJsonValue(node, path, value, 1);
		return value;
	}

	public static void getJsonValue(JsonNode node, String[] path,
			List<String> values, int nextIndex) {
		if (JSONUtil.isEmpty(node)) {
			return;
		}
		// 是路径的最后就直接取值
		if (nextIndex == path.length) {
			if (node.isArray()) {
				for (int i = 0; i < node.size(); i++) {
					JsonNode child = node.get(i).get(path[nextIndex - 1]);
					if (JSONUtil.isEmpty(child)) {
						continue;
					}
					values.add(child.toString());
				}
			} else {
				JsonNode child = node.get(path[nextIndex - 1]);
				if (!JSONUtil.isEmpty(child)) {
					values.add(child.toString());
				}
			}
			return;
		}
		// 判断是Node下是集合还是一个节点
		node = node.get(path[nextIndex - 1]);
		if (node.isArray()) {
			for (int i = 0; i < node.size(); i++) {
				getJsonValue(node.get(i), path, values, nextIndex + 1);
			}
		} else {
			getJsonValue(node, path, values, nextIndex + 1);
		}
	}

	/**
	 * 使用find的方法从实体中取出所有匹配的值
	 * 
	 * @param vo
	 * @param path
	 * @return
	 */
	public static List<String> getValueByFind(JsonNode node, String path)
			throws Exception {
		List<String> values = new ArrayList<String>();
		/*
		 * values = node.findValuesAsText(path); 这里提供两种方法 一种是只填写path
		 * 它会返回List<String>，另外一种就是下面用的 它会直接把找到的value填到你传入的集合中
		 */
		node.findValuesAsText(path, values);
		System.out.println(Arrays.toString(values.toArray()));
		return values;
	}
	/** 
	 * 查找当前Node中第一个匹配的值 
	 *  
	 * @param node 
	 * @param path 
	 * @return 
	 * @throws Exception 
	 */  
	public static int getFirstValueByFind(JsonNode node, String path) throws Exception {  
	    /* 
	     * 注意这点不能使用getTextValue()方法，因为找到的值为Int类型的所以使用getTextValue是查不到值的。 
	     * 不过如果想返回String字符串可以使用asText()方法。这里使用asInt是为了看到其实JackSon是可以直接返回相应类型的值的。 
	     */  
	    int value = node.findValue(path).asInt();  
	    System.out.println(value);  
	    return value;  
	}  
	/**
	 * 判断对象是否为空
	 * 
	 * @param obj
	 * @return
	 */
	public static boolean isEmpty(Object obj) {
		boolean result = true;
		if (obj == null) {
			return true;
		}
		if (obj instanceof String) {
			result = (obj.toString().trim().length() == 0)
					|| obj.toString().trim().equals("null");
		} else if (obj instanceof Collection) {
			result = ((Collection) obj).size() == 0;
		} else {
			result = ((obj == null) || (obj.toString().trim().length() < 1)) ? true
					: false;
		}
		return result;
	}

}

