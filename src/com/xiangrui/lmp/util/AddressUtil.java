package com.xiangrui.lmp.util;

import java.util.List;

import com.alibaba.fastjson.JSON;

public class AddressUtil
{
	public static String getAddrssFromJsonStr(String jsonStr)
	{
		String address = "";
		try
		{
			List<AddressInfoBean> list = JSON.parseArray(jsonStr, AddressInfoBean.class);
			if (list != null && list.size() > 0)
			{
				for (AddressInfoBean addressInfoBean : list)
				{
					address += addressInfoBean.getName();
				}
			}
		} catch (Exception e)
		{
		}
		return address;
	}
}
