package com.xiangrui.lmp.util;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.google.gson.Gson;
import com.xiangrui.lmp.business.admin.interfacelog.service.InterfaceLogService;
import com.xiangrui.lmp.business.admin.interfacelog.vo.InterfaceLog;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkglog.service.PkgLogService;
import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;
import com.xiangrui.lmp.business.admin.subscribe.service.SubscribeService;
import com.xiangrui.lmp.business.admin.subscribe.vo.Subscribe;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.admin.weixunPush.service.WeiXunPushService;
import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushLog;
import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushRequestBean;
import com.xiangrui.lmp.business.admin.weixunPush.vo.WeiXunPushSetting;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.constant.WebConstants;
import com.xiangrui.lmp.init.SpringContextHolder;

/**
 * 包裹日志手动录取的工具类
 * <p>
 * 
 * @author <b>hsjing</b>
 *         </p>
 *         <p>
 *         2015-7-2 下午4:27:31
 *         </p>
 */
public class PackageLogUtil
{

	private static final Logger logger = Logger.getLogger(PackageLogUtil.class);
	/**
	 * 不存在日志记录里面的状态,使用的默认描述信息
	 */
	public static final String STATUS_DESC_NO = "不存在这个类型的包裹了,废弃";

	/**
	 * 保存包裹日志记录
	 * 
	 * @param basePackage
	 *            更新或创建后的包裹对象,<br>
	 *            主要使用其中的<strong>package_id,status以及logistics_code和ems_code</
	 *            strong>中的一项
	 * @param user_name
	 *            包裹的操作人名称
	 */
	public static void logUserName(BasePackage basePackage, String user_name)
	{
		// 包裹日志操作对象
		PkgLogService pkgLogService = SpringContextHolder.getBean("pkgLogService");
		// 包裹数据验证
		PackageService packageService = SpringContextHolder.getBean("packageService");

		int package_id = basePackage.getPackage_id();
		// 匹配到数据库的包裹数据
		Pkg pkg = packageService.queryPackageById(package_id);
		if (null != pkg)
		{
			PkgLog pkgLog = new PkgLog();
			pkgLog.setPackage_id(package_id);
			pkgLog.setOperated_status(pkg.getStatus());
			// 不存在操作人
			pkgLog.setOperate_user(0);
			// 自动拼写描述信息
			String description = getDescription(pkg);
			pkgLog.setDescription(description);

			pkgLogService.pkgLogCreate(pkgLog);
		}
		sendExpressInfo(pkg);
	}

	public static void logBatch(List<PkgLog> pkgLogList)
	{
		// 包裹日志操作对象
		PkgLogService pkgLogService = SpringContextHolder.getBean("pkgLogService");
		pkgLogService.pkgLogBatchCreate(pkgLogList);

		for (PkgLog pkgLog : pkgLogList)
		{
			PackageService packageService = SpringContextHolder.getBean("packageService");
			Pkg pkg = packageService.queryPackageById(pkgLog.getPackage_id());
			sendExpressInfo(pkg);
			pushWeiXunMessage(pkg);
		}
	}

	public static void log(PkgLog pkgLog)
	{
		// 包裹日志操作对象
		PkgLogService pkgLogService = SpringContextHolder.getBean("pkgLogService");
		pkgLogService.pkgLogCreate(pkgLog);

		PackageService packageService = SpringContextHolder.getBean("packageService");

		Pkg pkg = packageService.queryPackageById(pkgLog.getPackage_id());

		sendExpressInfo(pkg);

	}

	/**
	 * 包裹日记记录
	 * 
	 * @param basePackage
	 *            更新或创建后的包裹对象,<br>
	 *            主要使用其中的<strong>package_id,status以及logistics_code和ems_code</
	 *            strong>中的一项
	 * @param user_name
	 *            包裹的操作人对象,前台操作或者不存在user==null
	 */
	public static void log(BasePackage basePackage, User user)
	{
		if( null==user )
			log(basePackage, 0);
		else
			log(basePackage, user.getUser_id());
	}
	public static void log(BasePackage basePackage, int userId){
		// 包裹日志操作对象
		PkgLogService pkgLogService = SpringContextHolder.getBean("pkgLogService");

		// 包裹数据验证
		PackageService packageService = SpringContextHolder.getBean("packageService");

		int package_id = basePackage.getPackage_id();
		// 匹配到数据库的包裹数据
		Pkg pkg = packageService.queryPackageById(package_id);
		if (null != pkg)
		{
			// 包裹变更后状态
			int status = basePackage.getStatus();

			PkgLog pkgLog = new PkgLog();
			pkgLog.setPackage_id(package_id);
			pkgLog.setOperated_status(status);

			if (userId>0)
			{
				// 用户不为空,读取用户id
				pkgLog.setOperate_user(userId);
			}
			else
			{
				// 用户为空,默认用户id为0
				pkgLog.setOperate_user(0);
			}
			// 自动拼写描述信息
			pkg.setStatus(status);
			String description = getDescription(pkg);

			// 查询包裹状态的记录
			PkgLog pkgLogExist = pkgLogService.queryPkgLogByPackageIdAndStatus(package_id, status);

			// 如果对应状态已经存在则更新包裹状态的时间
			if (null != pkgLogExist)
			{
				pkgLogExist.setDescription(description);
				pkgLogService.pkgLogUpdate(pkgLogExist);
			}
			else
			{
				pkgLog.setDescription(description);
				pkgLogService.pkgLogCreate(pkgLog);
			}
		}

		sendExpressInfo(pkg);
		pushWeiXunMessage(pkg);
	}

	/**
	 * 包裹日记记录
	 * 
	 * @param basePackage
	 *            更新或创建后的包裹对象,<br>
	 *            主要使用其中的<strong>package_id,status以及logistics_code和ems_code</
	 *            strong>中的一项
	 * @param user_name
	 *            包裹的操作人对象,前台操作或者不存在user==null
	 * @param description
	 *            记录描述
	 */
	public static void log(BasePackage basePackage, User user, String description)
	{
		// 包裹日志操作对象
		PkgLogService pkgLogService = SpringContextHolder.getBean("pkgLogService");
		// 包裹数据验证
		PackageService packageService = SpringContextHolder.getBean("packageService");

		int package_id = basePackage.getPackage_id();
		// 匹配到数据库的包裹数据
		Pkg pkg = packageService.queryPackageById(package_id);
		if (null != pkg)
		{
			PkgLog pkgLog = new PkgLog();

			pkgLog.setPackage_id(package_id);
			pkgLog.setOperated_status(pkg.getStatus());

			if (null != user)
			{
				pkgLog.setOperate_user(user.getUser_id());
			}
			else
			{
				pkgLog.setOperate_user(0);
			}
			// 描述信息手动拼写
			pkgLog.setDescription(description);
			pkgLogService.pkgLogCreate(pkgLog);
		}

		sendExpressInfo(pkg);
		pushWeiXunMessage(pkg);
	}

	public static String getDescription(BasePackage pkg)
	{
		//快递公司编码转换
		Map<String, Object> param = new HashMap<>();
		param.put("ems", "EMS");
		param.put("zhongtong", "中通速递");
		param.put("shentong", "申通快递");
		param.put("shunfeng", "顺丰速运");
		param.put("huitongkuaidi", "百世快递");
		param.put("debangwuliu", "德邦物流");
		param.put("yunda", "韵达快运");
		param.put("yuantong", "圆通速递");
		param.put("zhaijisong", "宅急送");
		param.put("youjikuaidi", "优寄快递");
		param.put("zhongyouwuliu", "中邮物流");
		param.put("tiantian", "天天快递");
		param.put("yuntongkuaidi", "运通快递");
		param.put("yousuwuliu", "优速物流");
		param.put("jialidatong", "嘉里大通");
		param.put("quanyikuaidi", "全一快递");
		param.put("rufengda", "如风达快递");
			
		StringBuffer sb = new StringBuffer();
		
		switch (pkg.getStatus())
		{
			case BasePackage.LOGISTICS_UNUSUALLY:
				sb.append("出现异常");
				break;
			case BasePackage.LOGISTICS_STORE_WAITING:
				sb.append(Constants.ExpLog.Exp_Log_CreateInSys);
				break;
			case BasePackage.LOGISTICS_STORAGED:
				Pkg packageObj = (Pkg) pkg;
				String warhouse = packageObj.getWarehouse();
				if (warhouse.indexOf("分拣中心") == -1)
				{
					warhouse += "分拣中心";
				}
				//【XXX分拣中心】已入库
				sb.append("【"+warhouse+"】"+Constants.ExpLog.Exp_Log_CreateInDATA);
				break;
			case BasePackage.LOGISTICS_SEND_WAITING:
				 packageObj = (Pkg) pkg;
				 warhouse = packageObj.getWarehouse();
				if (warhouse.indexOf("分拣中心") == -1)
				{
					warhouse += "分拣中心";
				}
				sb.append("【"+warhouse+"】"+Constants.ExpLog.Exp_Log_SplitWaitSend);
				//sb.append("等待发货");
				break;
			case BasePackage.LOGISTICS_SENT_ALREADY:
				 packageObj = (Pkg) pkg;
				 warhouse = packageObj.getWarehouse();
				if (warhouse.indexOf("分拣中心") == -1)
				{
					warhouse += "分拣中心";
				}
				sb.append("【"+warhouse+"】"+Constants.ExpLog.Exp_Log_PrintOut);
				//sb.append("已出库");
				break;
			case BasePackage.LOGISTICS_AIRLIFT_ALREADY:
				sb.append( Constants.ExpLog.Exp_Log_SentoChina);
				//sb.append("空运中");
				break;
			case BasePackage.LOGISTICS_CUSTOMS_WAITING:
				sb.append( Constants.ExpLog.Exp_Log_loadChina); 
				//sb.append("已落地，等待清关");
				break;
			/*
			 * case BasePackage.LOGISTICS_CUSTOMS_UNDERWAY: sb.append("正在清关中");
			 * break;
			 */
			case BasePackage.LOGISTICS_CUSTOMS_ALREADY:
				sb.append("包裹");
				//sb.append("已清关转国内EMS派送  派送单号：" + pkg.getEms_code());
				sb.append("已清关转国内" + param.get(pkg.getCom())  + "派送," + "派送单号：" + pkg.getEms_code());
				break;
			case BasePackage.LOGISTICS_SIGN_IN:
				sb.append("包裹");
				sb.append("已被签收");
				break;
			case BasePackage.LOGISTICS_DISCARD:
				sb.append("包裹");
				sb.append("已经废弃");
				break;
			case BasePackage.LOGISTICS_RETURN:
				sb.append("包裹");
				sb.append("正在退货中，详情请联系客服");
				break;
			default:
				sb = new StringBuffer(STATUS_DESC_NO);
				break;
		}
		return sb.toString();

	}

	private static void sendExpressInfo(Pkg pkg)
	{

		// 如果状态为已签收则不返回用户，该状态已在快递100信息中已经包含
		if (Pkg.LOGISTICS_SIGN_IN == pkg.getStatus())
		{
			return;
		}

		// 包裹废弃不推送用户
		if (Pkg.LOGISTICS_DISCARD == pkg.getStatus())
		{
			return;
		}

		String original_num = pkg.getOriginal_num();

		SubscribeService subscribeService = SpringContextHolder.getBean("subscribeService");

		// 通过关联单号国内订阅中，查询用户是否已有订阅该物物流信息
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("number", original_num);
		params.put("subscribe_type", Subscribe.HOME_SUB_TYPE);

		Subscribe homeSu = subscribeService.querySubscribeByParam(params);

		// 如果关联单号无法查询则通过公司运单号查询是否是属于公司运单号订阅形式
		if (homeSu == null)
		{
			homeSu = subscribeService.querySubscribeByLogisticsCode(pkg.getLogistics_code());
		}

		// 如果订阅不存在则直接返回
		if (homeSu == null)
		{
			return;
		}
		// 属于用户的跨境物流物流信息，则推送给用户
		if (homeSu.getSubscribe_type() == Subscribe.HOME_SUB_TYPE)
		{
			String callbackUrl = homeSu.getCallbackurl();
			sendHomeInfo(callbackUrl, pkg);
		}
	}

	/**
	 * 推送平台物流信息
	 * 
	 * @param callbackUrl
	 * @param pkg
	 */
	private static void sendHomeInfo(String callbackUrl, Pkg pkg)
	{

		PkgLogService pkgLogService = SpringContextHolder.getBean("pkgLogService");

		// 读取包裹的操作日志
		List<PkgLog> pkgLogs = pkgLogService.queryPkgLogByPackageId(pkg.getPackage_id());

		Document doc = DocumentHelper.createDocument();
		Element root = doc.addElement("pushRequest");
		createTextE(root, "status", "transport");
		createTextE(root, "message", "跨境物流平台转运中...");

		// 平台接收，默认状态为0
		createTextE(root, "ischeck", "0");

		// 增加平台接收，签收状态10
		createTextE(root, "state", "10");
		createTextE(root, "logistics_code", pkg.getLogistics_code());
		String original_num = pkg.getOriginal_num() == null ? "" : pkg.getOriginal_num();
		createTextE(root, "number", original_num);

		Element dataListE = root.addElement("dataList");

		// 将平台物流状态拼接成报文
		for (PkgLog pkgLog : pkgLogs)
		{
			Element dataE = dataListE.addElement("data");
			createTextE(dataE, "time", pkgLog.getOperate_time_toStr());
			createTextE(dataE, "context", pkgLog.getDescription());
		}
		StringBuffer buffer = new StringBuffer();
		String xml = buffer.append(doc.asXML()).toString();
		sendInfo(callbackUrl, xml);
	}

	/**
	 * 推送报文并记录接口日志
	 * 
	 * @param callbackUrl
	 * @param xml
	 */
	private static void sendInfo(String callbackUrl, String xml)
	{

		InterfaceLog log = new InterfaceLog();
		log.setStart_time(new Timestamp(System.currentTimeMillis()));
		log.setInterace_type(InterfaceLog.TYPE_EXPRESSINFO_PUSH);
		log.setLog_id(UUIDUtil.uuid());
		log.setReq_msg(xml);

		String url = callbackUrl;
		String result = "";
		// 累加器
		int i = 0;

		// 如果返回异常则循环执行3次，直到发送为止。
		int cnt = 3;

		while ("".equals(result) && i < cnt)
		{
			result = send(url, xml);
			i++;
		}
		if ("".equals(result))
		{
			log.setStatus(InterfaceLog.STATUS_FAIL);
		}
		else
		{
			log.setStatus(InterfaceLog.STATUS_SUCCESS);
		}
		log.setResp_msg(result);
		log.setEnd_time(new Timestamp(System.currentTimeMillis()));
		log(log);
	}

	/**
	 * 创建xml节点
	 * 
	 * @param e
	 * @param name
	 * @param value
	 * @return
	 */
	private static Element createTextE(Element e, String name, String value)
	{
		Element element = e.addElement(name);
		String str = (value == null) ? "" : value;
		element.setText(str);
		return element;
	}

	/**
	 * 记录日志
	 * 
	 * @param log
	 */
	private static void log(InterfaceLog log)
	{
		InterfaceLogService interfaceLogService = SpringContextHolder.getBean("interfaceLogService");
		interfaceLogService.createInterfaceLog(log);

	}

	private static String send(String url, String data)
	{
		String result = "";
		try
		{
			result = HttpClientTool.post(url, data, "utf-8");
		} catch (IOException e)
		{
			logger.error(e.toString(), e);
		}
		return result;
	}

	/**
	 * 
	 * @param pkg
	 */
	private static void pushWeiXunMessage(Pkg pkg)
	{
		WeiXunPushService weiXunPushService = SpringContextHolder.getBean("weiXunPushService");
		FrontUserService frontUserService = SpringContextHolder.getBean("memberService");
		if (pkg != null)
		{
			FrontUser frontUser = frontUserService.queryFrontUserByUserId(pkg.getUser_id());
			if (frontUser != null)
			{
				WeiXunPushSetting weiXunPushSetting = weiXunPushService.queryWeiXunPushSettingByOpenId(frontUser.getOpen_id());
				if (weiXunPushSetting != null)
				{
					if (weiXunPushSetting.checkCanPushWeiXunMessage(pkg.getStatus()))
					{
						WeiXunPushRequestBean weiXunPushRequestBean = new WeiXunPushRequestBean();
						weiXunPushRequestBean.setOpenid(frontUser.getOpen_id());
						String msg = getDescription(pkg);
						msg = msg.replace("包裹", "包裹【" + pkg.getLogistics_code() + "】");
						weiXunPushRequestBean.setMsg(msg);
						Gson gson = new Gson();
						String jsonRequestMsg = gson.toJson(weiXunPushRequestBean);

						WeiXunPushLog weiXunPushLog = new WeiXunPushLog();
						String url = WebConstants.WEIXIN_PUSH_REQUEST_URL;
						String result = "";
						weiXunPushLog.setStart_time(new Timestamp(System.currentTimeMillis()));
						// 累加器
						int i = 0;

						// 如果返回异常则循环执行3次，直到发送为止。
						int cnt = 1;

						while ("".equals(result) && i < cnt)
						{
							result = send(url, jsonRequestMsg);
							i++;
						}
						if ("".equals(result))
						{
							weiXunPushLog.setStatus(InterfaceLog.STATUS_FAIL);
						}
						else
						{
							weiXunPushLog.setStatus(InterfaceLog.STATUS_SUCCESS);
						}
						weiXunPushLog.setEnd_time(new Timestamp(System.currentTimeMillis()));
						weiXunPushLog.setOpen_id(frontUser.getOpen_id());
						weiXunPushLog.setPackage_id(pkg.getPackage_id());
						weiXunPushLog.setUser_id(frontUser.getUser_id());
						weiXunPushLog.setReq_msg(jsonRequestMsg);
						weiXunPushLog.setResp_msg(result);
						weiXunPushService.saveWeiXunPushLog(weiXunPushLog);
					}
				}
			}
		}
	}

	/**
	 * 关税微信推送信息
	 * @param openId
	 * @param weixunContext
	 * @param userId
	 */
	public static void pushWeiXunTaxMessage(String openId, String weixunContext, int userId)
	{
		WeiXunPushService weiXunPushService = SpringContextHolder.getBean("weiXunPushService");
		WeiXunPushRequestBean weiXunPushRequestBean = new WeiXunPushRequestBean();
		weiXunPushRequestBean.setOpenid(openId);
		weiXunPushRequestBean.setMsg(weixunContext);
		Gson gson = new Gson();
		String jsonRequestMsg = gson.toJson(weiXunPushRequestBean);

		WeiXunPushLog weiXunPushLog = new WeiXunPushLog();
		String url = WebConstants.WEIXIN_PUSH_REQUEST_URL;
		String result = "";
		weiXunPushLog.setStart_time(new Timestamp(System.currentTimeMillis()));
		// 累加器
		int i = 0;

		// 如果返回异常则循环执行3次，直到发送为止。
		int cnt = 1;

		while ("".equals(result) && i < cnt)
		{
			result = send(url, jsonRequestMsg);
			i++;
		}
		if ("".equals(result))
		{
			weiXunPushLog.setStatus(InterfaceLog.STATUS_FAIL);
		}
		else
		{
			weiXunPushLog.setStatus(InterfaceLog.STATUS_SUCCESS);
		}
		weiXunPushLog.setEnd_time(new Timestamp(System.currentTimeMillis()));
		weiXunPushLog.setOpen_id(openId);
		weiXunPushLog.setPackage_id(0);
		weiXunPushLog.setUser_id(userId);
		weiXunPushLog.setReq_msg(jsonRequestMsg);
		weiXunPushLog.setResp_msg(result);
		weiXunPushService.saveWeiXunPushLog(weiXunPushLog);
	}
	
	public static String getDescription(int pkgStatus)
	{
		StringBuffer sb = new StringBuffer();
		switch (pkgStatus)
		{
			case BasePackage.LOGISTICS_UNUSUALLY:
				sb.append("出现异常");
				break;
			case BasePackage.LOGISTICS_STORE_WAITING:
				sb.append("待入库");
				break;
			case BasePackage.LOGISTICS_STORAGED:
				sb.append("已入库");
				break;
			case BasePackage.LOGISTICS_SEND_WAITING:
				sb.append("待发货");
				break;
			case BasePackage.LOGISTICS_SENT_ALREADY:
				sb.append("已出库");
				break;
			case BasePackage.LOGISTICS_AIRLIFT_ALREADY:
				sb.append("空运中");
				break;
			case BasePackage.LOGISTICS_CUSTOMS_WAITING:
				sb.append("待清关");
				break;
			case BasePackage.LOGISTICS_CUSTOMS_ALREADY:
				sb.append("已清关并已派件中");
				break;
			case BasePackage.LOGISTICS_SIGN_IN:
				sb.append("已签收");
				break;
			case BasePackage.LOGISTICS_DISCARD:
				sb.append("废弃");
				break;
			case BasePackage.LOGISTICS_RETURN:
				sb.append("退货");
				break;
		}
		return sb.toString();
	}
}
