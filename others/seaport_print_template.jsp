<%@ page language="java"  pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="${resource_path}/js/jquery-1.9.0.js"></script>
<script src="${resource_path}/js/jquery.PrintArea.js"></script>
<%-- <script src="${resource_path}/js/jquery-1.7.2.min.js"></script> --%>
<%-- <script src="${resource_path}/js/jquery.jqprint.js"></script> --%>
<script src="${resource_path}/js/jquery-barcode.js"></script>
<link rel="stylesheet" href="${resource_path}/css/print.css" media="print" />
<body>
<style>
	body{
		font-size: 12px;
	}

 	.my_show{border: 1px solid #000;height:800px;}
 	.my_show .top{margin-top: 10px;}
	.my_show .logo_title{
		float:left;margin-left:60px;margin-top:0px;
		
	}
	 .logo_title h1{
		margin:auto;
		margin:0px;
		font-size:60px;	
		font-weight:bold;
	}
	
	
	.my_show .top .part_2{
			border-top: 1px solid #000;
			height: 80px;
	}

.my_show .top .part_3{
		border-top:1pt solid #000;
		height: 100px;
	}
 .my_show  .middle{
	    border-top:1px dashed #000;
	}
.my_show  .middle  .part_1{
	height: 200px;
}
.my_show  .middle .part_2{
	border-top:1pt solid #000;
	height: 25px;
	line-height: 25px;
}
.my_show  .middle .part_3{
	border-top:1px solid #000;
	height: 100px;
}
.my_show .buttom{
	border-top:1px dashed #000;
}
.my_show .buttom .part_1{
	margin-top: 10px;
}
.td_style1 {
	BORDER-RIGHT: 1px ; 
	BORDER-LEFT:  1px ; 
	BORDER-TOP: 1px solid ;
	BORDER-BOTTOM: 1px solid ;
}

.td_style2 {
	BORDER-TOP: 1px dashed;
	BORDER-BOTTOM: 1px solid;
	height: 25px;margin-top:7px;
}

.td_style3 {
	BORDER-BOTTOM: 1px solid;
}
 	.title_black {
		font-size:12pt;
		color: #000000;
		font-style: 宋体;
		font-weight:bold;
		float:left;
		margin-left:10px;
	}
.text_left{
	float:left;margin-left:20px;width:150px;font-size:13px;
}
.text_addr{width: 300px;}
#128Code1{float:left;margin-left:20px;margin-top:20px;}

#128Code2{
	float:left;margin-left:20px;margin-top:14px;
}
.qr_code{
	float: left;
	margin-left: 40px;
}
.code2{float: left}
.addr_context{height: 30px;	clear: both;margin-top: 10px;}
</style>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:100px;
    }
    .div-front{
        float:left;
        width:400px;
    }
    .div-front-List{
        /*float:left;*/
    }
    
     .th-title{
        height:30px;
        vertical-align:middle;
    }
    
        .td-List-left{
        height:30px;
        vertical-align:middle;
        text-align:left;
        padding-left:5px;
    }
        .td-List-right{
        height:30px;
        vertical-align:middle;
        text-align:right;
        padding-right:5px;
        
    }
</style>

    <div class="admin">
        <div >
            <div style="float:left;">
              <input type="button" id="print"  value="打印"/>
              <input type="hidden" id="code"  value="${pkgDetail.logistics_code}"/>
            </div>
            <br/>
                  <div style="height:850px;margin-top:10px ">	
                        <div class ="my_show" style="width:558px;">
                            <div class="top">
                            	<div class="part_1">
                                     <div class="logo_title" style="">
                                   	  <H1><strong>速&nbsp翼</strong></H1>
                                     </div>
                                     <div id="128Code1"></div> 
                            	</div>
                            	<div class="part_2">
                            		 <div style="height: 30px;">
                                        <div class="title_black" ><span>寄件人(from)：</span></div>
                                        <div class="text_left" >
                                          <span>${frontUser.user_name}</span>
                                        </div>
                                       <div class="title_black" style="float:left;"><span>电话/Tel：</span></div>
                                       <div  style="float:left;margin-left:5px;">
                                           <Span>${frontUser.mobile}</Span>
                                        </div>
                                      </div>
                                     <div class="addr_context">
                                       <div class="title_black" style="float:left;margin-left:10px"><span >地址/Address：</span></div>
                                       <div class="text_addr"  style="float:left;margin-left:20px;width:300px;">
                                         <Span>${osaddr}</Span>
                                       </div>
                                     </div>
                            	</div>
                            	<div class="part_3">
                            		 <div style="height: 25px;margin-top:7px;">
                                       <div class="title_black" style="float:left;margin-left:10px">
                                       <span class ="title_black">收件人/To：</span></div>
                                        <div class="text_left" >
                                          <Span>${userAddress.receiver}</Span>
                                        </div>
                                        <div class="title_black" style="float:left;">
                                        <span class ="title_black">电话/Tel：</span></div>
                                        <div style="float:left;margin-left:5px;">
                                           <Span>${userAddress.mobile}</Span>
                                        </div>
                                     </div>
                                     <div class="addr_context">
                                        <div class="title_addr" style="float:left;margin-left:10px">
                                        <span class ="title_black">地址/Address：</span></div>
                                        <div class="text_addr" style="float:left;margin-left:20px;">
                                           <Span>${address}</Span>
                                        </div>
                                     </div>
									 <div style="height: 25px;margin-top:7px;">
                                       <div class="title_black" style="float:left;margin-left:10px">
                                       <span class ="title_black">大客户代码：</span></div>
                                        <div class="text_left" >
                                          <Span></Span>
                                        </div>
                                        <div class="title_black" style="float:left;">
                                        <span class ="title_black">邮编/Post Code：</span></div>
                                        <div style="float:left;margin-left:5px;">
                                           <Span>${userAddress.mobile}</Span>
                                        </div>
                                     </div>
                            	</div>
                             </div>
                             <div class="middle">
                             	<div class="part_1">
                                 <div style="height: 20px;margin-top:7px;">
                                       <div  style="float:left;margin-left:10px"><span >实际重量：</span></div>
                                       <div style="float:left;width:20px;">
                                          <Span>${pkgDetail.actual_weight}</Span>
                                       </div>
                                       <div style="float:left;margin-left:10px;">
                                         <Span>磅</Span>
                                       </div>
                                        <div style="float:left;margin-left:30px;">
                                         <Span>收费重量：</Span>
                                       </div>
                                       <div style="float:left;width:20px;">
                                          <Span>${pkgDetail.weight}</Span>
                                       </div>
                                        <div style="float:left;margin-left:10px;">
                                         <Span>磅</Span>
                                       </div>
                                        <div style="float:left;margin-left:30px;">
                                         <Span>体积：</Span>
                                       </div>
                                       <div >
                                          <Span>长:${pkgDetail.length}CM&nbsp;&nbsp;宽：${pkgDetail.width}CM&nbsp;&nbsp;高：${pkgDetail.height}CM</Span>
                                       </div>                                    
                                     </div>
<!--                                      <div style="height: 20px;"> -->
<!--                                         <div style="float:left;margin-left:10px;font-weight:bold;font-size:16px;">关联单号：</div> -->
<%--                                         <div style="float:left;margin-left:10px;font-size:22px;font-weight:bold">${pkgDetail.original_num}</div> --%>
<!--                                         <div style="float:left;margin-left:10px;font-weight:bold;font-size:16px;">报关单号：</div> -->
<%--                                         <div style="float:left;margin-left:10px;font-size:22px;font-weight:bold">${declaration_code}</div> --%>
<!--                                      </div> -->
                 <div>
                      <div class="div-front-List"><span class="span-title">内件:</span></div>
                      <div>
                      <span>
                           <table border="1" cellpadding="0" cellspacing="0">
                             <tr>
                               <th width ="300" class="th-title">商品名称</th>
                               <th width ="100" class="th-title">品牌</th>
                               <th width ="100" class="th-title">商品申报类别</th>
                               <th width ="100" class="th-title">单价</th>
                               <th width ="50" class="th-title">数量</th>
                               <th width ="100" class="th-title">规格</th>
                               <th width ="100" class="th-title">关税费用</th>
                             </tr>
                             <c:forEach var="good"  items="${pkgGoodsList}">
                                <tr>
                                  <td width ="300" class="td-List-left">${good.goods_name}</td>
                                  <td width ="100" class="td-List-left">${good.brand}</td>
                                  <td width ="100" class="td-List-left">${good.name_specs}</td>
                                  <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.price}" type="currency" pattern="￥#0.00#"/></td>
                                  <td width ="50" class="td-List-right">${good.quantity}</td>
                                  <td width ="100" class="td-List-left">${good.spec}</td>
                                  <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="￥#0.00#"/></td>
                                </tr>
                             </c:forEach>
                           </table>
                      </span>
                    </div>
                 </div>									                   
                                     
                             	</div>
							<div class="part_2">
                                       <div style="float:left;margin-left:10px;width:200px;">备注：</div>
                                       <div style="float:left;margin-left:20px;">
                                         <span></span>
                                       </div>
                                   
                             </div>
							 <div class="part_2">
                                       <div style="float:left;margin-left:10px;width:200px;">申报货值/Value：</div>
                                       <div style="float:left;margin-left:20px;">
                                         <span>原产地：${country}</span>
                                       </div>
                                   
                             </div>
                             <div class="part_2">
                                       <div style="float:left;margin-left:10px;width:200px;">收件人签名：</div>
                                       <div style="float:left;margin-left:20px;">
                                         <span>签收时间： &nbsp&nbsp&nbsp&nbsp年&nbsp&nbsp&nbsp&nbsp月 &nbsp&nbsp&nbsp日 &nbsp&nbsp&nbsp时</span>
                                       </div>
                                   
                             </div>
                             <div class="part_3">
                             	    <div style="height:25px;margin-top:7px;">
                                       <div style="float:left;margin-left:10px"><span >寄件人(from)：</span></div>
                                       <div class="text_left">
                                          <span>${frontUser.user_name}</span>
                                       </div>
                                       <div style="float:left;"><span>电话/Tel：</span></div>
                                       <div style="float:left;margin-left:5px;">
                                         <Span>${frontUser.mobile}</Span>
                                       </div>
                                     </div>
                                     <div style="height: 50px;">
                                        <div class="title_addr" style="float:left;margin-left:10px"><span >地址/Address：</span></div>
                                        <div class="text_addr" style="float:left;margin-left:20px;">
                                          <Span>${osaddr}</Span>
                                        </div>
                                     </div>
                             </div>
                             </div>
                             
                             <div class="buttom">
                             	
                             	<div class="part_1">
                                   <div class="button_addr" style="height:25px;">
                                        <div style="float:left;margin-left:10px"><span class ="title_black">收件人/To：</span></div>
                                         <div class="text_left">
                                            <span>${userAddress.receiver}</span>
                                         </div>
                                         <div style="float:left;"><span class ="title_black">电话/Tel：</span></div>
                                         <div  style="float:left;margin-left:5px;">
                                            <Span>${userAddress.mobile}</Span>
                                        </div>
                                     </div>
                                     <div class="addr_context">
                                       <div class="title_addr" style="float:left;margin-left:10px"><span class ="title_black">地址/Address：</span></div>
                                       <div class="text_addr" style="float:left;margin-left:20px;">
                                         <Span>${address}</Span>
                                       </div>
                                     </div>
                                     <div class="button_addr" style="height:25px;">
                                        <div style="float:left;margin-left:10px"><span class ="title_black">渠道编码：</span></div>
                                         <div class="text_left">
                                            <span>${userAddress.receiver}</span>
                                         </div>
                                         <div style="float:left;"><span class ="title_black">关联单号：</span></div>
                                         <div  style="float:left;margin-left:5px;">
                                            <Span>${userAddress.mobile}</Span>
                                        </div>
                                     </div>
                                     <div class="button_addr" style="height:25px;">
                                        <div style="float:left;margin-left:10px"><span class ="title_black">进口口岸：</span></div>
                                         <div class="text_left">
                                            <span>${userAddress.receiver}</span>
                                         </div>
                                         <div style="float:left;"><span class ="title_black">客服电话：</span></div>
                                         <div  style="float:left;margin-left:5px;">
                                            <Span>${userAddress.mobile}</Span>
                                        </div>
                                     </div>
                                     <div class="print_footer" style="height:100px;clear: both;">
                                          <div class="code2">
	                                          <div id="128Code2"></div>
	                                          <div style="float:left;margin-left:120px;margin-top:10px;clear: both;"><span>原寄地：${country}</span></div>
                                          </div>
                                          <div class="qr_code" >
                                          	<img src="${resource_path}/img/site.png" />
                                          	<div class="site">www.91soe.com</div>
                                          </div>
                                          <div>
                                            <img src="${resource_path}/download/express_logo/demo_express_logo.png" />
                                          </div>                                          
                                     </div>
                             	</div>
                             </div>
                        </div>
                     </div>
                     
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function() {
        var code =$('#code').val();
        $('#128Code1').barcode(code, "code128",{barWidth:2,fontSize:20});
        $('#128Code2').barcode(code, "code128",{barWidth:2,fontSize:20});
         $('#print').click(function() {
     /*        $('.my_show').jqprint({
            	debug:true,
            	importCSS:true,
            }); */
            $('.my_show').printArea();
        }); 
    });
</script>
</body>
</html>