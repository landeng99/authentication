SELECT a.*,
 b.user_name AS real_name,
 b.account
FROM t_account_log a
LEFT JOIN t_front_user b ON a.user_id = b.user_id
WHERE a.logistics_code = ''
ORDER BY a.opr_time DESC