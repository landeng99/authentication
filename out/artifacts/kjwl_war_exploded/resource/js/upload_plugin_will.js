function onUpload(imagePath,setValueElement,showImageElement)
{
		$('#uploadImage_div').empty();
		$('#uploadImage_div').append("<div id=\"uploadImage_juf\">点击选择图片</div> ");
		var uploadObj = $("#uploadImage_juf").uploadFile({
											url : '${mainServer}/homepkg/importIdcard',
											allowedTypes:"png,gif,jpg,jpeg",
											multiple : false,
											autoSubmit : true,
											fileName : "myfile",
											formData : {
												"basePath" : imagePath
											},
											maxFileSize : 1024 * 1024*2,
											maxFileCount : 1,
											showStatusAfterSuccess : true,
											dragDropStr : "<span><b>也可将图片拖到此处</b></span>",
											abortStr : "中止",
											cancelStr : "取消",
											doneStr : "确定",
											multiDragErrorStr : "不支持多张图片拖动上传.",
											extErrorStr : "不允许上传. 只允许后缀为: ",
											sizeErrorStr : "超过最大允许上传大小:",
											uploadErrorStr : "不允许上传!",
											onSuccess:function(files,data,xhr)
											{
												if(data.uploadStatus=='S')
												{
													if(setValueElement)
													{
														$("#"+setValueElement).val(data.url);
													}
													if(showImageElement)
													{
														$("#"+showImageElement).attr("src",data.url);
													}
													$('#uploadImage_div').parent().find(".panel-tool-close").click();
												}else
												{
													$("#eventsmessage").html($("#eventsmessage").html()+"<br/>Error for: "+JSON.stringify(files));
												}
											},
											onError: function(files,status,errMsg)
											{
												$("#eventsmessage").html($("#eventsmessage").html()+"<br/>Error for: "+JSON.stringify(files));
											}
							});
		
	$('#uploadImage_div').dialog({
    	title: '上传图片',
    	top:100,
    	width: 550,
    	height: 300,
    	closed: false,
    	cache: false,
		modal: true
	});
	$('#uploadImage_div').css("display","block");
	$('#uploadImage_div').dialog('refresh');
}