﻿$(function () {
	
	
});

/**
 * 编辑地址
 */
$('.edit').click(function(){
	if(!validateLogin()){
		return ;
	}
	//展示弹框
	addAndQueryAddrChange("01");
	
	var $ol=$(this).parent().parent();
	var inputs=$ol.find('input');
	$("#addAddress").html("修改收货地址");
	$.each(inputs,function(i,input){
		var name=input.name;
		var val=input.value;
		 
		if('front_img'==name){
    		var newSrc=mainServer+"/resource"+val;
    		
    		if(val!=null && val!=""){
    			var drDestroy = $('#frontFace').dropify();
    			drDestroy = drDestroy.data('dropify');
    			drDestroy.settings.defaultFile = newSrc;
    			drDestroy.destroy(); 
    			drDestroy.init(); 
    			$('#front_img').val(val);
    		} else {
    			var drDestroy = $('#frontFace').dropify();
    			drDestroy = drDestroy.data('dropify');
    			drDestroy.settings.defaultFile = "";
    			drDestroy.destroy(); 
    			drDestroy.init(); 
    		}		    		
		}
		if('back_img'==name){
    		var newSrc=mainServer+"/resource"+val;
    		if(val!=null && val!=""){
    			var drDestroy = $('#backFace').dropify();
    			drDestroy = drDestroy.data('dropify');
    			drDestroy.settings.defaultFile = newSrc;
    			drDestroy.destroy(); 
    			drDestroy.init(); 
    			$("#back_img").val(val);
    		}else {
    			var drDestroy = $('#backFace').dropify();
    			drDestroy = drDestroy.data('dropify');
    			drDestroy.settings.defaultFile = "";
    			drDestroy.destroy(); 
    			drDestroy.init(); 
    		}
    		
		}
		if('receiver'==name){
			$('#TrueName').val(val);
		}
		if('street'==name){
			$('#Address').val(val);
		}
		if('postal_code'==name){
			$('#PostCode').val(val);
		}
		if('mobile'==name){
			$('#Mobile').val(val);
		}
		if('idcard'==name){
			$('#Identity').val(val);
		}
		if('address_id'==name){
			$('#address_id').val(val);
		}
		if('idcard_status'==name){
			if(val=="1"){
				$('.ke-inline-block-front').css("display","none");
				$('.ke-inline-block-back').css("display","none");
			}else{	    				 
				$('.ke-inline-block-front').show();
				$('.ke-inline-block-back').show();
			}
		}
		if('region'==name){
			var array=$.parseJSON(val);	    			
			//初始化地区的默认值
			configRegion(array);
		}
		if('isdefault'==name){
			$('#IsDefault').val(val);
			if(val == '1'){
				$('#IsDefault').prop("checked",true);
			} else {
				$('#IsDefault').prop("checked",false);
			}
		}
		
	});
	scrollTo(0,0);
});

/**
 * 新增或查询地址切换方法
 * @param addFlag
 */
function addAndQueryAddrChange(addFlag){
	if(addFlag == "01"){
		cancel();
		$("#addressListDiv").attr("class","hidden");
		$("#addAddressDiv").attr("class","");
	} else {
		$("#addressListDiv").attr("class","");
		$("#addAddressDiv").attr("class","hidden");
	}
}

/**
 * 监听保存表单的字段
 */
function bindClickSaveFrom(_this, hasError, text){
	var fromvalue = $(_this).val();
	if(fromvalue == ""){
		$(_this).parent().parent().addClass("has-error");
		$(_this).next("span").removeClass("hidden");
	} else {
		$(_this).parent().parent().removeClass("has-error");
		$(_this).next("span").addClass("hidden");
	}
}

/**
 * 显示错误的提示并且聚集到对应的位置信息
 * @param _this
 * @param hasError
 * @param text
 */
function showErrorInfo(_this, hasError, text){
	if("01" == hasError ){
		$(_this).parent().parent().addClass("has-error");
		$(_this).next("span").removeClass("hidden");
		var html = '<i class="glyphicon glyphicon-info-sign pr5"></i>' + text;
		$(_this).next("span").empty();
		$(_this).next("span").html(html);
		$(_this).focus();
	} else {
		$(_this).parent().parent().removeClass("has-error");
		$(_this).next("span").addClass("hidden");
	}
}
/**
 * 检验form表单的元素是否已经正确填写
 */
function checkFormValue(){
	
	//检验省份为必填项
	var modifyProvince = $('#modifyProvince');
	if(modifyProvince.val() == 0 || modifyProvince.val() == null){
		showErrorInfo(modifyProvince, "01", "请填写省份");
		return;
	} else{
		showErrorInfo(modifyProvince, "", "");
	}
	
	//请选择地市
	var modifyCity = $('#modifyCity');
	if(modifyCity.val() == 0 || modifyProvince.val() == null){
		showErrorInfo(modifyCity, "01", "请选择城市");
		return;
	}else{
		showErrorInfo(modifyCity, "", "");
	}
	
	//请选择区县
	var modifyQzone=$('#modifyQzone');
	if(modifyQzone.val() == 0 || modifyProvince.val() == null){
		showErrorInfo(modifyQzone, "01", "请选择区县");
		return;
	}else{
		showErrorInfo(modifyQzone, "", "");
	}
	
	//校验邮编
	var postCodeVal=$("#PostCode");
	if(postCodeVal.val() == null || postCodeVal.val() == ""){
		showErrorInfo(postCodeVal, "", "");
	} else {
		var reg=/^\d+$/;
		if(reg.test(postCodeVal.val())==false){
			showErrorInfo(postCodeVal, "01", "邮政编码只能为数字");
			return;
		}
		showErrorInfo(postCodeVal, "", "");
	}
	
	//校验地址
	var textVal=$("#Address");
	if(textVal.val() == 0 || textVal.val() == null){
		showErrorInfo(textVal, "01", "请填写街道地址。不需要重复填写省市区，必须大于5个字符，小于120个字符。");
		return;
	}else{
		var textLength = textVal.val().length;//获取街道地址长度
		if(textLength<=5 || textLength >= 120){
			showErrorInfo(textVal, "01", "街道地址地址长度必须大于5个字符。");
			return;
		}
		showErrorInfo(textVal, "", "");
	}
	
	//校验收件人姓名
	var trueNameVal = $("#TrueName");
	if(trueNameVal.val() == 0 || trueNameVal.val() == null){
		showErrorInfo(trueNameVal, "01", "请填写收货人姓名，长度不超过25个字。");
		return;
	}else{
		var textLength = trueNameVal.val().length;//获取街道地址长度
		if(textLength >= 25){
			showErrorInfo(trueNameVal, "01", "收货人姓名长度不能超过25个字。");
			return;
		}
		showErrorInfo(trueNameVal, "", "");
	}
	
	//校验手机号码或者电话号码
	var mobileVal = $("#Mobile").val().trim();
	if(mobileVal != null && mobileVal != "" && !mobileVal.match(/^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/)){
		showErrorInfo($("#Mobile"), "01", "手机号码格式不正确。");
		return;
	} else {
		showErrorInfo($("#Mobile"), "", "");
	}
	
	//获取电话号码
	var Code = $('#Code').val();
	var Tel = $('#Tel').val();
	var Extension = $('#Extension').val();
	var tel = Code+Tel+Extension;
	if(mobileVal == "" && (Code =="" || Tel =="") ){
		showErrorInfo($("#Mobile"), "01", "电话、手机号码必填一项。");
		return;
	}else {
		showErrorInfo($("#Mobile"), "", "");
	}
	
	//校验通过提交此方法
	save();
	
}

/**
 * 新增或者编辑保存地址方法
 */
function save(){
	if(!validateLogin()){
		return ;
	}
	var modifyProvince=$('#modifyProvince').val();
	var modifyProvinceText=$("#modifyProvince").find("option:selected").text();
	
	var modifyCity=$('#modifyCity').val();
	var modifyCityText=$("#modifyCity").find("option:selected").text();
	
	var modifyQzone=$('#modifyQzone').val();
	var modifyQzoneText=$("#modifyQzone").find("option:selected").text();
	
	//拼接省，市，区								
	var data=[];
	if(modifyCity == 0){
		data=[{"id":modifyProvince,"name":modifyProvinceText,"level":"1"}];
	}else if(modifyCity != 0 && modifyQzone == 0){
		data=[{"id":modifyProvince,"name":modifyProvinceText,"level":"1"},{"id":modifyCity,"name":modifyCityText,"level":"2"}];
	}else{
		data=[{"id":modifyProvince,"name":modifyProvinceText,"level":"1"},{"id":modifyCity,"name":modifyCityText,"level":"2"},{"id":modifyQzone,"name":modifyQzoneText,"level":"3"}];
	}
	var jsonStr =JSON.stringify(data);
	//获取用户的身份
	var userType=$("#userType").val();
	//获取电话号码
	var Code=$('#Code').val();
	var Tel=$('#Tel').val();
	var Extension=$('#Extension').val();
	var tel=Code+Tel+Extension;
	
								
	$('#region').val(jsonStr);
	$('#tel').val(tel);
		
	$('#destinationFrom').submit();
}

/**
 * 取消新增或者编辑地址信息
 */
function cancel(){
	$("#modifyProvince").val(0);
	$("#modifyCity").val(0);
	$("#modifyQzone").val(0);
	$("#PostCode").val("");
	$("#Address").val("");
	$("#TrueName").val("");
	$("#Mobile").val("");
	$("#Code").val("");
	$("#Tel").val("");
	$("#Extension").val("");
	$("#Identity").val("");
	$("#IsDefault").attr("checked",false);
	$("#front_img").val(domUrl+"/img/Iden.jpg");
	$("#back_img").val(domUrl+"/img/IdenEx.jpg");
}

/**
 * 重写确认框 fun:函数对象 params:参数列表， 可以是数组, content 确认框展示的内容
 */
function confirm(fun, params, content) {
    if ($("#myConfirm").length > 0) {
        $("#myConfirm").remove();
    } 
    var html = "<div class='modal fade' id='myConfirm' >"
            + "<div class='modal-backdrop in' style='opacity:0; '></div>"
            + "<div class='modal-dialog' style='z-index:2901; margin-top:60px; width:400px; padding:0;border-radius:8px;'>"
            + "<div class='modal-content'>"
            + "<div class='modal-header'  style='font-size:16px; '>"
            + "确认框<button type='button' class='close' data-dismiss='modal'>"
            + "<span style='font-size:20px;  ' class='glyphicon glyphicon-remove'></span><tton></div>"
            + "<div class='modal-body text-center' id='myConfirmContent' style='font-size:18px; '>"
            + content
            + "</div>"
            + "<div class='modal-footer ' style=''>"
            + "<button class='btn btn-primary ' id='confirmOk' >确定<tton>"
            + "<button class='btn btn-default ' data-dismiss='modal'>取消<tton>"
            + "</div>" + "</div></div></div>";
    $("body").append(html);

    $("#myConfirm").modal("show");

    $("#confirmOk").on("click", function() {
        $("#myConfirm").modal("hide");
        fun(params); // 执行函数
    });
}
    
/**
 * 地址删除弹框前校验是否符合删除逻辑
 * @param addRessId
 */
function deleteAddress(addRessId){
	if(!validateLogin()){
		return;
	}
	confirm(deleteAddresstt, addRessId, "确定删除该地址？");
}

/**
 * 实现地址的删除
 * @param addRessId
 */
function deleteAddresstt(addRessId){
	var url = mainServer + "/useraddr/deleteAddress";
	$.ajax({
	url:url,
	data:{addRessId:addRessId},
	success:function(data){
		 window.location.href = mainServer + "/useraddr/destination";
		}
	});
}

/**
 * 地址列表查询按钮（原有逻辑）
 */
function query_button_click(){
	var q_userName=$("#q_userName").val();
	var q_moblieNum=$("#q_moblieNum").val();
	window.location.href= mainServer + "/useraddr/destination?q_moblieNum="+q_moblieNum+"&q_userName="+encodeURI(encodeURI(q_userName));
}