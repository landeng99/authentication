﻿var inviteCode, inviteCodeIndex = 0, inviteCodeResult = false, email, emailIndex = 0, checkEmailResult = false, emailPass, emailPassIndex = 0, emailAgainPass, emailAgainPassIndex = 0, emailValidCode, emailValidCodeIndex = 0, emailValidCodeResult = false, mobile, mobileIndex = 0, checkMobileResult = false, mobilePass, mobilePassIndex = 0, mobileAgainPass, mobileAgainPassIndex = 0, mobileValidCode, mobileValidCodeIndex = 0, mobileValidResult = false, submitClick, AgreementIndex = 0;
$(function () {
    inviteCode = $("#inviteCode");
    email = $("#email");
    emailPass = $("#emailPass");
    emailAgainPass = $("#emailAgainPass");
    emailValidCode = $("#emailValidCode");
    mobile = $("#mobile");
    mobilePass = $("#mobilePass");
    mobileAgainPass = $("#mobileAgainPass");
    mobileValidCode = $("#mobileValidCode");
    inviteCodeByMobile = $("#inviteCodeByMobile");

    inviteCode.on("focus", function () {
        CheckInviteCode($(this), true);
    }).blur(function () { CheckInviteCode($(this), false, true); });

    email.on("focus", function () {
        CheckEmail($(this), true);
    }).blur(function () { CheckEmail($(this), false, true); });

    emailPass.on("focus", function () {
        CheckPass($(this), true);
    }).blur(function () { CheckPass($(this), false, true); });

    emailAgainPass.on("focus", function () {
        CheckReapetPass($(this), emailPass, true);
    }).blur(function () { CheckReapetPass($(this), emailPass, false, true); });

    emailValidCode.on("focus", function () {
        CheckEmailValidate($(this), true);
    }).blur(function () { CheckEmailValidate($(this), false, true); });

    $("#showValid").click(function () {
        $.BDEX.ChangeValidCode($(this), "RegisterEmail");
        emailValidCode.val("");
    });

    document.body.onclick = clickEvent;  //文档其他地方的点击事件
    //----------------------执行邮箱注册---------------
    $("#btnSubmit").click(function () {
        $("#regStyle").val("1");
        submitClick = true;
        if (!checkEmailResult) {
            CheckEmail(email, false, true);
        } else if (checkEmailResult && CheckPass(emailPass, false, true) && CheckReapetPass(emailAgainPass, emailPass, false, true)) {
            if (valid == 2) {
                if (!inviteCodeResult) {
                    CheckInviteCode(inviteCode, false, true);
                    return false;
                }
            } else if (valid == 1) {
                if (!emailValidCodeResult) {
                    CheckEmailValidate(emailValidCode, false, true);
                    return false;
                }
            }
            EmailRegister();
            //            if (!inviteCodeResult) {
            //                CheckInviteCode(inviteCode, false, true);
            //            } else {
            //                
            //            }
        }
        return false;
    });
    //-----------------------执行手机注册-----------------
    $("#btnRegisterMobile").click(function () {
        $("#regStyle").val("2");
        submitClick = true;
        if (!inviteCodeResult) {
            CheckInviteCode(inviteCodeByMobile, true);
        } else if (!checkMobileResult) {
            CheckMobile(mobile, true);
        } else if (!mobileValidResult) {
            CheckMobileValid(mobileValidCode, true);
        } else if (inviteCodeResult && checkMobileResult && mobileValidResult && CheckPass(mobilePass) && CheckReapetPass(mobileAgainPass, mobilePass)) {
            MobileRegister();
        }
        return false;
    });
    //    //----------------------------发送手机校验码---------------------
    $("#sendMobileValid").click(function () {
        if (!checkMobileResult) {
            CheckMobile(mobile, true);
        } else {
            SendMobileValidCode();
        }

    });

    $("#Agreement").click(function () {
        if (!this.checked) {
            AgreementIndex = layer.tips('请勾选用户协议', this, {
                guide: 3,
                style: ['background-color:#c00; color:#fff;top:-10px;', '#c00']
            });
        } else {
            $.BDEX.Close(AgreementIndex);
        }
    });

    document.onkeydown = function (event) {
        e = event ? event : (window.event ? window.event : null);
        if (e.keyCode == 13 && $("#regStyle").val() == "1") {
            $("#btnSubmit").click();
        }
    }

    email.focus();
});
//=======================检测邀请码检测=================
function CheckInviteCode(obj, isFocus, isSon) {
    CheckReapetPass(emailAgainPass, emailPass, false,true);
    var inviteCodeVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(inviteCodeVal)) {
        if (isSon && !isFocus) {
            obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        } else if (isFocus) {
            obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
        }
        inviteCodeResult = false;
        if (emailPassIndex > 0 && submitClick) {
            return false;
        } else {
            if (isSon && !isFocus) {
                obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
            } else if (isFocus) {
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
            }
            if (emailIndex == 0 && emailPassIndex == 0 && emailAgainPassIndex == 0) {
                inviteCodeIndex = $.BDEX.Tips('请填写邀请码信息', obj[0], '100');
            }
        }
        return false;
    }
    var param = {
        AssemblyName: "Beyond.MemberCore.Business.dll",
        ClassName: "Beyond.MemberCore.Business.Mem_InviteCodeBLL",
        MethodName: "ValidInviteCode",
        ParamModelName: "System.String",
        onRequest: function (parms) {
            return inviteCodeVal;
        },
        onComplete: function (result) {
            if (result != null && result.BoolOut) {
                inviteCodeResult = true;
                $.BDEX.Close(inviteCodeIndex);
                inviteCodeIndex = 0;
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
            } else {
                //obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
                if (isSon && !isFocus) {
                    obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
                } else if (isFocus) {
                    obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
                }
                inviteCodeResult = false;
                if (emailIndex == 0 && emailPassIndex == 0 && emailAgainPassIndex == 0) {
                    if (isFocus) {
                        inviteCodeIndex = $.BDEX.Tips('请填写邀请码信息', obj[0], '100');
                    } else {
                        inviteCodeIndex = $.BDEX.Tips(result.ErrMsg, obj[0], '100');
                    }
                }
            }
        }
    };
    $.ajaxRequest(param);
};
//=======================检测邮箱=====================
function CheckEmail(obj, isFocus,isSon) {
    var emailVal = obj.val().trim();
    if (isFocus) {
        if (emailVal == "e-mail地址") {
            obj.val("");
            emailVal = "";
        }
    } else {
        if (emailVal == "") {
            obj.val("e-mail地址");
        }
    }
    if ($.BDEX.CheckNullOrEmpty(emailVal)) {
        emailIndex = $.BDEX.Tips('请填写真实有效的邮箱地址', obj[0], '100');
        checkEmailResult = false;
        if (isSon && !isFocus) {
            obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        } else if (isFocus) {
            obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
        }
        return false;
    }
    if (!RegRule.Email.test(emailVal)) {
        if (isSon && !isFocus) {
            emailIndex = $.BDEX.Tips('邮箱格式不正确', obj[0], '100');
            obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        } else if (isFocus) {
            emailIndex = $.BDEX.Tips('请填写真实有效的邮箱地址', obj[0], '100');
            obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
        }
        checkEmailResult = false;
        return false;
    } else {
    var param = {
        AssemblyName: "Beyond.MemberCore.Business.dll",
        ClassName: "Beyond.MemberCore.Business.Mem_UserBLL",
        MethodName: "IsRepeatEmail",
        ParamModelName: "System.String",
        onRequest: function (parms) {
            return emailVal;
        },
        onComplete: function (result) {
            if (result != null && result.BoolOut) {
                checkEmailResult = true;
                $.BDEX.Close(emailIndex);
                emailIndex = 0;
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
            } else {
                if (isFocus || isSon) {
                    emailIndex = $.BDEX.Tips("该邮箱已注册，请更换注册邮箱或立即登录", obj[0], '100');
                }
                obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
                checkEmailResult = false;
            }
        }
    };
        $.ajaxRequest(param);
    }
};
//========================检测邮箱密码=================
function CheckPass(obj,isFocus, isSon) {
    if (!checkEmailResult) {
        CheckEmail(email, false,true);
    }
    var passVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(passVal)) {
        if (isSon && !isFocus) {
            obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        } else if (isFocus) {
            obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
        }
        if (emailIndex > 0 && submitClick) {
            return false;
        } else {
            if (emailIndex == 0) {
                emailPassIndex = $.BDEX.Tips('密码由6-16个字符组成，不能单独使用英文字母、数字或符号', obj[0], '110');
            }
        }

        return false;
    }
    if (passVal.length < 6) {
        obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        if (emailIndex > 0 && submitClick) {
            return false;
        } else {
            if (isSon && !isFocus) {
                obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
            } else if (isFocus) {
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
            }
            if (emailIndex == 0) {
                emailPassIndex = $.BDEX.Tips('密码长度不能少于6位', obj[0], '110');
            }
        }
        return false;
    }
    if (passVal == "123456") {
        obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        if (emailIndex > 0 && submitClick) {
            return false;
        } else {
            if (isSon && !isFocus) {
                obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
            } else if (isFocus) {
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
            }
            if (emailIndex == 0) {
                emailPassIndex = $.BDEX.Tips('密码设置的过于简单', obj[0], '110');
            }
        }
        return false;
    }
    if (!RegRule.Pass.test(passVal)) {
        obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        if (emailIndex > 0 && submitClick) {
            return false;
        } else {
            if (isSon && !isFocus) {
                obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
            } else if (isFocus) {
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
            }
            if (emailIndex == 0) {
                emailPassIndex = $.BDEX.Tips('密码由6-16个字符组成，不能单独使用英文字母、数字或符号', obj[0], '110');
            }
        }
        return false;
    }
    $.BDEX.Close(emailPassIndex);
    emailPassIndex = 0;
    obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
    return true;
};
//=======================检测重复密码================
function CheckReapetPass(obj, toObj, isFocus,isSon) {
    CheckPass(emailPass,false,true);
    var repeatPassVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(repeatPassVal)) {
        if (isSon && !isFocus) {
            obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        } else if (isFocus) {
            obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
        }
        if (emailPassIndex > 0 && submitClick) {
            return false;
        } else {
            if (emailIndex == 0 && emailPassIndex == 0) {
                emailAgainPassIndex = $.BDEX.Tips('再次输入登录密码', obj[0], '110');
            }
        }
        return false;
    }
    if (!RegRule.Pass.test(repeatPassVal)) {
        obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        if (emailPassIndex > 0 && submitClick) {
            return false;
        } else {
            if (isSon && !isFocus) {
                obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
            } else if (isFocus) {
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
            }
            if (emailIndex == 0 && emailPassIndex == 0) {
                if (isFocus) {
                    emailAgainPassIndex = $.BDEX.Tips('再次输入登录密码', obj[0], '110');
                } else {
                    emailAgainPassIndex = $.BDEX.Tips('重复密码不正确', obj[0], '110');
                }
            }
        }
        return false;
    }
    if (toObj.val().trim() != repeatPassVal) {
        obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        if (emailPassIndex > 0 && submitClick) {
            return false;
        } else {
            if (isSon && !isFocus) {
                obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
            } else if (isFocus) {
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
            }
            if (emailIndex == 0 && emailPassIndex == 0) {
                emailAgainPassIndex = $.BDEX.Tips('密码不一致', obj[0], '110');
            }
        }
        return false;
    }
    $.BDEX.Close(emailAgainPassIndex);
    emailAgainPassIndex = 0;
    obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
    return true;
};
//======================检测邮箱验证码===============
//检测邮箱验证码
function CheckEmailValidate(obj) {
    var emailValidVal = obj.val().trim();
    if ($.BDEX.CheckNullOrEmpty(emailValidVal)) {
        emailValidCodeResult = false;
        if (emailAgainPassIndex > 0 && submitClick) {
            return false;
        }
        emailValidCodeIndex = $.BDEX.Tips('请输入右边图片字符，不区分大小写', obj[0], '110');
        return false;
    }
    if (emailValidVal.length != 4) {
        emailValidCodeResult = false;
        if (emailAgainPassIndex > 0 && submitClick) {
            return false;
        }
        obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
        emailValidCodeIndex = $.BDEX.Tips('请正确填写验证码', obj[0], '110');
        return false;
    }

    $.ajax({
        type: "GET",
        url: "/Common/CheckValidCode.ashx?code=" + emailValidVal + "&c=RegisterEmail",
        async: false,
        success: function (data) {
            if (data == 1) {
                $.BDEX.Close(emailValidCodeIndex);
                emailAgainPassIndex = 0;
                emailValidCodeResult = true;
                obj.removeClass("input_Light_red").removeClass("input_all_red").addClass("input_Light_blue");
                return true;
            }
            else {
                emailValidCodeResult = false;
                if (emailAgainPassIndex > 0 && submitClick) {
                    return false;
                }
                obj.removeClass("input_Light_blue").addClass("input_Light_red").addClass("input_all_red");
                emailValidCodeIndex = $.BDEX.Tips('请正确填写验证码', obj[0], '110');
                return false;
            }
        }
    });
};
//------------------------------执行邮箱注册操作-------------------------
function EmailRegister() {
    //-----------------数据遮罩层，表示数据正在加载中----------------------
    $.BDEX.Wait();
    function sparam(param) {
        param.Email = email.val().trim();
        param.Password = emailPass.val().trim();
        param.AgainPassword = emailAgainPass.val().trim();
        if (valid == 2) {
            param.InviteCode = inviteCode.val().trim();
        } else if (valid == 1) {
            param.ValidCode = emailValidCode.val().trim();
        }
        param.RegDataType = 1;
        param.RegisterMark = mark;
        return param;
    };
    var param = {
        AssemblyName: "Beyond.MemberCore.Business.dll",
        ClassName: "Beyond.MemberCore.Business.Mem_UserBLL",
        MethodName: "NewRegister",
        ParamModelName: " Beyond.MemberCore.Param.RegParam",
        ParamAssemblyName: "Beyond.MemberCore.Entity.dll",
        onRequest: sparam,
        onComplete: function (result) {
            $.BDEX.Close();
            if (result != null && result.BoolOut) {
                asyncConfig = false;
                //CreateCoupon(result.ObjectOut.UserID);
                $("form").submit();
            } else {
                $.BDEX.Success(result.ErrMsg);
            }
        }
    };
    $.ajaxRequest(param);
};

function QQLoginUrl() {  //腾讯QQ
    location.href = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=101065418&redirect_uri=http://passport.birdex.cn/App/QQReturn.htm&scope=get_user_info";
}

//注册赠送优惠券
function CreateCoupon(userid) {
    function sparam(param) {
        return userid;
    };
    var param = {
        AssemblyName: "Beyond.Birdex.Business.dll",
        ClassName: "Beyond.Birdex.Business.Market.Mar_CouponBLL",
        MethodName: "CreateByRegisterGet",
        ParamModelName: "System.Int32",
        ParamAssemblyName: "Beyond.WebFrame.Param.dll",
        onRequest: sparam,
        onComplete: function (result) {
        }
    };
    $.ajaxRequest(param);
}