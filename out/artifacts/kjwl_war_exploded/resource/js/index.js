﻿$(function () {
    $("#head").css("border-bottom", "none");
    $("#tab_conbox").show();
    $("li.tab1").show();
    $("#tabs>li").click(function () {
        var className = $(this).find("i").attr("class");
        var index = $(this).attr("index");
        $("li.tab_con").hide();
        $("li.tab" + index).show();
        $("#tabs>li").find("i").removeClass();
        $("#tabs>li").find("i").each(function () {
            $(this).addClass($(this).attr("hasClass"));
        });

        if (className.indexOf("hover") == -1) {
            $(this).find("i").removeClass().addClass(className + "_hover");
        }

        $("#tabs>li").find("a").removeClass().addClass("tab_a");
        $(this).find("a").removeClass().addClass("tab_a_hover");
    });

    $("#checkNO").blur(function () {
        var content = $(this).val().trim();
        if (content == "") {
            $(this).val("你可以输入1Z1234567890");
        }
    }).focus(function () {
        var content = $(this).val().trim();
        if (content == "你可以输入1Z1234567890") {
            $(this).val("");
        }
    })
    .keyup(function () {
        var content = $(this).val().trim();
        if (content.length == 0) {
            $(".Outcome").hide();
        }
        //        if (content.length > 0 && !$(".Outcome").is(":visible")) {
        //            $(".Outcome").show();
        //        }
        //        $("#noData").hide();
        //        $("#loading").show();
        //        $("#result").hide();
        //        $("#bagNO").html(content);
        //        $("#bagStatus").html("...");

        //GetResultByBagNO();
    })
    ;

    $("#goFind").click(function () {
        var content = $("#checkNO").val().trim();
        if (content == "你可以输入1Z1234567890") {
            content = "";
        }

        if (content.length == 0) {
            $("#checkNO").focus();
            $(".Outcome").hide();
        }

        if (content.length > 0 && !$(".Outcome").is(":visible")) {
            $(".Outcome").show();
        }
        //$("#bagNO").html(content);
        $("#bagStatus").html("...");
        GetResultByBagNO();
        return false;
    });

    document.onkeydown = function (event) {
        e = event ? event : (window.event ? window.event : null);
        if (e.keyCode == 13) {
            $("#goFind").click();
        }
    }
});
//预报状态
var EnumForecastStatus = { "-15": "待认领信息", "-10": "预报已取消", "-5": "无效预报", "0": "等待入库", "5": "包裹揽收", "10": "包裹已入库", "15": "完结" };
//转运状态
var EnumTransportOderFlag = { "-5": "订单已取消", "0": "等待合箱", "5": "等待支付运费", "10": "等待出库", "15": "包裹出库中", "20": "包裹已出库", "25": "包裹空运中", "27": "待清关", "30": "包裹清关中", "35": "包裹已清关", "40": "完结" };
//===========================获取对应包裹号的状态数据=========================
function GetResultByBagNO() {
    $(".step").removeClass("green").removeClass("blue");
    function onCompleteSuccess(result) {
        if (result != null) {
            var status = result.StatusFlag;
            var type = result.OrderType;
            $("#bagNO").html(result.OrderCode);
            if (type == 0) { //预报单
                for (var item in EnumForecastStatus) {
                    if (item == status) {
                        $("#bagStatus").html(EnumForecastStatus[item]);
                    }
                }
                $(".step0").addClass("green");
                $(".step").each(function () {
                    if ($(this).hasClass("step1")) {
                        $(this).addClass((status != 10 ? " blue" : " green"));
                        if ($(this).hasClass("green")) {
                            $(this).find("span.wz").html("已入库");
                        }

                        if (status == 5) {
                            $(this).find("span.wz").html("入库中");
                        }
                    }
                });
            } else {
                for (var item in EnumTransportOderFlag) {
                    if (item == status) {
                        $("#bagStatus").html(EnumTransportOderFlag[item]);
                    }
                }
                $(".step0").addClass("green");
                $(".step").each(function () {
                    if ($(this).hasClass("step1") && status >= 0) {
                        $(this).addClass(" green");
                        if ($(this).hasClass("green")) {
                            $(this).find("span.wz").html("已入库");
                        }
                    }
                    if ($(this).hasClass("step2") && status >= 5) {
                        $(this).addClass((status == 5 ? "" : (status == 10 || status == 15 ? " blue" : " green")));
                        if ($(this).hasClass("green")) {
                            $(this).find("span.wz").html("已出库");
                        } else {
                            $(this).find("span.wz").html("等待出库");
                        }
                        if (status == 15) {
                            $(this).find("span.wz").html("出库中");
                        }
                    }
                    if ($(this).hasClass("step3") && status >= 25) {
                        $(this).addClass("green");
                        if ($(this).hasClass("green")) {
                            $(this).find("span.wz").html("已空运");
                        } else {
                            $(this).find("span.wz").html("待空运");
                        }
                    }

                    if ($(this).hasClass("step4") && (status == 27 || status == 30 || status == 35 || status >= 40)) {
                        if (status == 27) {
                            $(this).addClass("blue");
                            $(this).find("span.wz").html("待清关");
                        } else if (status == 30) {
                            $(this).addClass("blue");
                            $(this).find("span.wz").html("清关中");
                        } else if (status == 35 || status == 40) {
                            $(this).addClass("green");
                            $(this).find("span.wz").html("已清关");
                        }
                        else if (status > 40) {
                            $(this).addClass("green");
                            $(this).find("span.wz").html("已清关");
                        }
                    }

                    if ($(this).attr("class") == "Next06" && (status >= 40 || status == 35)) {
                        if (status == 35) {
                            $(this).attr("class", "Next06 blue");
                            $(this).find("[class='wz']").html("待收货");
                        } else if (status == 40) {
                            $(this).attr("class", "Next06 green");
                            $(this).find("[class='wz']").html("已收货");
                        }
                        else if (status > 40) {
                            $(this).attr("class", "Next06 green");
                            $(this).find("[class='wz']").html("已收货");
                        }
                    }

                    if ($(this).hasClass("step5") && (status >= 40 || status == 35)) {
                        if (status == 35) {
                            $(this).addClass("blue");
                            $(this).find("span.wz").html("待收货");
                        } else if (status == 40) {
                            $(this).addClass("green");
                            $(this).find("span.wz").html("已收货");
                        }
                        else if (status > 40) {
                            $(this).addClass("green");
                            $(this).find("span.wz").html("已收货");
                        }
                    }
                    if ($(this).hasClass("step6") && status >= 40) {
                        if (status >= 40) {
                            $(this).addClass("green");
                        }
                    }
                });
            }
            $("#noData").hide()
            $("#loading").hide();
            $("#result").show();
        } else {
            $("#noData").show()
            $("#loading").hide();
            $("#result").hide();
        }
    };
    function sparam(param) {
        return $("#checkNO").val().trim();
    };
    //初始化数据
    var param = {
        AssemblyName: "Beyond.Birdex.Business.dll",
        ClassName: "Beyond.Birdex.Business.Transport.Tst_TransportTempBLL",
        MethodName: "GetTempByCode",
        ParamModelName: "System.String",
        onComplete: onCompleteSuccess,
        onRequest: sparam
    }
    $.ajaxRequest(param);
};