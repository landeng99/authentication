     /**
      * 重写确认框 fun:函数对象 params:参数列表， 可以是数组, content 确认框展示的内容
      */
function msginfo(content) {
         if ($("#myConfirm").length > 0) {
             $("#myConfirm").remove();
         } 
         var html = "<div class='modal fade' id='myConfirm' >"
                 + "<div class='modal-backdrop in' style='opacity:0; '></div>"
                 + "<div class='modal-dialog' style='z-index:2901; margin-top:60px; width:400px; padding:0;border-radius:8px;'>"
                 + "<div class='modal-content'>"
                 + "<div class='modal-header'  style='font-size:16px; '>"
                 + "友情提示：<button type='button' class='close' data-dismiss='modal'>"
                 + "<span style='font-size:20px;  ' class='glyphicon glyphicon-remove'></span><tton></div>"
                 + "<div class='modal-body text-center' id='myConfirmContent' style='font-size:18px; '>"
                 + content
                 + "</div>"
                 + "<div class='modal-footer ' style=''>"
                 + "<button class='btn btn-primary ' id='confirmOk' >确定<tton>"
                  + "</div>" + "</div></div></div>";
         $("body").append(html);

         $("#myConfirm").modal("show");

         $("#confirmOk").on("click", function() {
             $("#myConfirm").modal("hide");
          });
     }
     // use :msginfo( "确定删除该地址？");
     /**
      * 重写确认框 fun:函数对象 params:参数列表， 可以是数组, content 确认框展示的内容
      */
     function confirm(fun, params, content) {
         if ($("#myConfirm").length > 0) {
             $("#myConfirm").remove();
         } 
         var html = "<div class='modal fade' id='myConfirm' >"
                 + "<div class='modal-backdrop in' style='opacity:0; '></div>"
                 + "<div class='modal-dialog' style='z-index:2901; margin-top:60px; width:400px; padding:0;border-radius:8px;'>"
                 + "<div class='modal-content'>"
                 + "<div class='modal-header'  style='font-size:16px; '>"
                 + "友情提示：<button type='button' class='close' data-dismiss='modal'>"
                 + "<span style='font-size:20px;  ' class='glyphicon glyphicon-remove'></span><tton></div>"
                 + "<div class='modal-body text-center' id='myConfirmContent' style='font-size:18px; '>"
                 + content
                 + "</div>"
                 + "<div class='modal-footer ' style=''>"
                 + "<button class='btn btn-primary ' id='confirmOk' >确定<tton>"
                 + "<button class='btn btn-default ' data-dismiss='modal'>取消<tton>"
                 + "</div>" + "</div></div></div>";
         $("body").append(html);

         $("#myConfirm").modal("show");

         $("#confirmOk").on("click", function() {
             $("#myConfirm").modal("hide");
              fun(params); // 执行函数
         });
     }
     // use :confirm(deleteAddresstt, addRessId, "确定删除该地址？");