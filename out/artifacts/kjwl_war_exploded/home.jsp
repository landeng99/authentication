<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<% 
	String hostName = "http://"+request.getServerName();
	if(hostName.startsWith("http://hth.tv")) {
	    String queryString = (request.getQueryString() == null ? "" : "?"+request.getQueryString());
	    response.setStatus(301);
	    String requestUrl = request.getRequestURL().toString();
	    requestUrl = requestUrl.replace("http://hth.tv", "http://www.hth.tv");
	    response.setHeader( "Location", requestUrl + queryString);
	    response.setHeader( "Connection", "close" );
	}
	request.getRequestDispatcher("/homepage/index").include(request, response);
%>

