<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0021)http://www.birdex.cn/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<!-- 
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/sylogin.css" rel="stylesheet" type="text/css"/>
 -->
<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
<script type="text/javascript" src="${resource_path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
</head>
<body>
<jsp:include page="topIndexPage.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/login.css"></link>
<%--
<form method="post" action="${mainServer}/fregiste" id="regform">
<div id="wrap" style="height: 660px;width:100%;">
<div id="subwrap" style="width:100%;">
    <div class="forgotpassword" style="padding-top: 0px; padding-bottom: 0px; height: 660px;" id="wrapModal">
         <div class="modal-dialog" style="margin-left:35%;margin-top:10px;">
              <div class="bt2"><span style="font-size:20px;">注册用户</span><span style="margin-left:5px;color:#B2A9A9;">RegisterUserinfo</span></div>  
          
              <dl class="Box02">
                  <dt><span>电子邮件地址： </dt>
                  <dd><input name="email" id="email" type="text" class="input_Light_red input_all_red"  style="width:280px;height:40px;line-height:40px"  value="请输入电子邮箱地址" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="emailblur()">
                  	<br /><span id="emailNone"></span>
                  </dd>
                  <dt><span>登录密码：</span></dt>
                  <dd><input name="user_pwd" id="user_pwd" type="password" class="input_Light_blue" value="" style="width:280px;height:40px;line-height:40px"   onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="user_pwdblur()">
                  	<br /><span id="user_pwdNone"></span>
                  </dd>
                  <dt><span>手机号码：请选择国别以免收不到短信验证码！</span></dt>
                  <dd><select id="mobile_prefix"><option value="+86">中国</option><option value="+1">美国</option></select><input name="mobile" id="mobile" type="text" class="input_Light_blue" style="width:280px;height:40px;line-height:40px" value="请输入手机号码" onkeyup="value=value.replace(/[^\d]/g,'') " onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="mobileblur()"/>
                  	<br /><span id="mobileNone"></span>
                  </dd>
                  
                  <dt><span>验证码：</span></dt>
                  <dd>
                  	<div style="height: 50px;">
                    <input id="txtCode" type="text"  style="width:160px;height:30px;line-height:30px" maxlength="4" name="txtCode" value="请输入右边图形验证码" onFocus="if (this.value==this.defaultValue) this.value='';" onblur="LostFocus_Code();"/>
                    <img id="imgVC" src="${mainServer}/authImg" alt="看不清？点击刷新。" width="80" height="30" hspace="2" onclick="refresh()"
                        style="cursor: pointer;" class="verification-code" />
                    </div>                      
					<input name="sms_code" id="sms_code" type="text" class="input_Light_blue" style="width:160px;height:40px;line-height:40px" value="" onblur="sms_codeblur()"/>
					<input type="button" value="免费获取验证码" style="width:100px;height:40px;" id="freeCode" name="freeCode"/>
					<br /><span id="sms_codeNone"></span>
                    <label id="errCode" class="txtErr" style="color:#F50D0D;">
                    ${smsCodeErr}
                    </label>
				  </dd>
                  
                  <dt><span>点击注册，代表我已阅读并接受<a href="${mainServer}/help" class="blue">《用户服务协议》</a></span></dt>
              </dl>
			  <div style="margin:0 auto;width:90%;margin-top:10px;text-align:center;font-size:16px;color:#F50D0D;">
				   ${failReg}
		      </div>
		      <input type="hidden" id="trecommand_user_id" name="trecommand_user_id" value="${recommand_user_id}"/>
		      <input type="hidden" id="source" name="source" value="${source}"/>
		      <input type="hidden" id="pushLinkId" value="${pushLinkId}"/>
              <div class="Bottm2">
                   <input id="btnreg" type="button" value="立即注册" class="button_bule button pull-left" style="width:110px;margin-left:10px;">
				   <input type="button" id="btnReset" value="重置" class="button_bule button pull-left" style="width:110px;margin-left:40px;">
              </div>
         </div>
    </div>
</div>
</div>
</form>
 --%>
 <div class="wrap bgImg">
        <div class="panel ">
            <h1 class="tit">用户注册</h1>
            <div id="div_err" class="alert alert-danger hidden" role="alert">${failReg}</div>
            <div id="div_success" class="alert alert-success hidden" role="alert"></div>
            <form class="floating-labels ">
                <div class="form-group mb40">
                    <input class="form-control" name="email" id="email" type="email" placeholder="输入电子邮件" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="input1">电子邮箱</label>
                    <span class="help-block hidden"><small></small></span>
                </div>
                <div class="form-group mb40">
                    <input class="form-control" name="user_pwd" id="user_pwd" type="password" placeholder="最少8位密码" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="input1">密码</label> 
                    <span class="help-block hidden"><small></small></span>
                </div>
                <div class="form-group form-group-btn mb40">
                    <input class="form-control" name="mobile" id="mobile" type="telephone"  placeholder="输入手机号码" />
                    <span class="input-group-btn J-select-country"><!--加 selected -->
                        <button id="mobile_prefix" class="btn btn-default btn-outline bootstrap-touchspin-up" type="button" data-value="+86" data-text="中国">
                        	中国<div class="caret"></div>
                        </button>
                        <ul class="select-country J-select-country-ul"> <!-- 选择内容 -->
                            <li data-value="+86" data-text="中国" class="selected"><a ><i class="glyphicon glyphicon-ok text-success"></i>中国</a></li>
                            <li data-value="+1" data-text="美国"><a><i class="glyphicon glyphicon-ok text-success"></i>美国</a></li>
                        </ul>
                    </span>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="input1">手机号码</label>
                    <span class="help-block hidden"><small></small></span>
                </div>

                <div class="form-group form-group-btn mb40">
                    <input class="form-control" id="txtCode" name="txtCode" type="text" placeholder="输入右边图形验证码">
                    <span class="input-group-btn">
                        <img id="imgVC" src="${mainServer}/authImg" alt="看不清？点击刷新。" hspace="2" onclick="refresh()" class="ver-code" />
                    </span>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="">验证码</label>
                    <span class="help-block hidden"><small></small></span>
                </div>
                 <div class="form-group form-group-btn mb40">
                    <input class="form-control" name="sms_code" id="sms_code" type="text" placeholder="输入短信验证码" />
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-outline bootstrap-touchspin-up" type="button" id="freeCode" name="freeCode">获取验证码</button>
                    </span>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="">短信验证码</label>
                    <span class="help-block hidden"><small></small></span>
                </div>
                <div class="agreement">
                    <p><span class="p5">点击注册，代表我已阅读并接受</span><a href="${mainServer}/help?item=86" class="blue">《用户服务协议》</a></p>
                </div>
			    <input type="hidden" id="trecommand_user_id" name="trecommand_user_id" value="${recommand_user_id}"/>
			    <input type="hidden" id="source" name="source" value="${source}"/>
			    <input type="hidden" id="pushLinkId" value="${pushLinkId}"/>
            </form>
            <div class="text-center mb20"><button id="btnreg" class="btn-primary btn-input btn">注册</button></div>           
        </div>
    </div>
<script>
var second=120;
var successSecond = 5;
var successTimer = function(){
	successSecond--;
	if (successSecond > 0) {
		$('#div_success').text("注册成功！"+successSecond+"秒后跳转登录页面");
	} else {
		var url = "${mainServer}/homepage/login";
		window.location.href = url;
	}
}
function resetClick(opType) {
	if (opType == 'sms') {
		$("#freeCode").off('click');
		$("#freeCode").on('click', function(){
			$("#freeCode").off('click');
			emailblur($('#email'), opType);
		});
	} else if (opType == 'reg') {
		$("#btnreg").off('click');
		$("#btnreg").on('click', function(){
			emailblur($('#email'), opType);
		});
	}
}
$(function () {
	$("#freeCode").on('click', function(){
		$("#freeCode").off('click');
		emailblur($('#email'), 'sms');
	});
	$("#btnreg").on('click', function(){
		$("#btnreg").off('click');
		emailblur($('#email'), 'reg');
	});
	$('form.floating-labels').on('blur', 'input.form-control', function(){
		var val=$(this).val().trim();
		$(this).val(val);
		var opType = 'blur';
		switch ($(this).prop('id')) {
		case 'email':
			emailblur(this, opType);
			break;
		case 'user_pwd':
			user_pwdblur(this, opType);
			break;
		case 'sms_code':
			sms_codeblur(this, opType);
			break;
		case 'mobile':
			mobileblur(this, opType);
			break;
		case 'txtCode':
			LostFocus_Code(this, opType);
			break;
		default:
			break;
		}
	});
	$(".J-select-country").click(function() {
        $(this).toggleClass("selected");
    });
    $(".J-select-country-ul li").click(function() {
        $(this).addClass("selected");
        $(this).siblings().removeClass("selected");
        var value = $(this).data('value');
        var text = $(this).data('text');
        $('#mobile_prefix').data('value', value);
        $('#mobile_prefix').data('text', text);
        $('#mobile_prefix').html(text+'<div class="caret"></div>');
    });
});

function inputErr(obj){
	$(obj).parent().removeClass('has-success');
	$(obj).parent().addClass('has-error');
	$(obj).siblings('span.help-block').removeClass('hidden');
	$(obj).siblings('span.glyphicon-remove').removeClass('hidden');
	$(obj).siblings('span.glyphicon-ok').addClass('hidden');
}

function inputOk(obj){
	$(obj).parent().removeClass('has-error');
	$(obj).siblings('span.help-block').addClass('hidden');
	$(obj).siblings('span.glyphicon-remove').addClass('hidden');
	$(obj).siblings('span.glyphicon-ok').removeClass('hidden');
	$(obj).parent().addClass('has-success');
}
//邮箱
function emailblur(obj, opType){
	var emailVal = $(obj).val();
	if (emailVal == "请输入电子邮箱地址" || emailVal == "") {
		$(obj).siblings('span.help-block').html('<small>邮箱不能为空!</small>');
		inputErr(obj);
		resetClick(opType);
		return false; 
	}
	if (!emailVal.match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/)) {
		$(obj).siblings('span.help-block').html('<small>邮箱格式不正确!</small>');
		inputErr(obj);
		resetClick(opType);
		return false; 
	}	
	$.ajax({
		url:'${mainServer}/fregisteEmail',
		type:'post',
		data:{email:emailVal},
		success:function(data){	
			isemail = data;
			if(isemail == 0){
				$(obj).siblings('span.help-block').html('<small>邮箱已经注册！</small>');
				inputErr(obj);
				resetClick(opType);
			}else{
				$(obj).siblings('span.help-block').html('<small>邮箱可以使用！</small>');
				inputOk(obj);
				if (opType != 'blur') {
					user_pwdblur($('#user_pwd'), opType);
				}
			}
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			resetClick(opType);
		}
	});
}
//密码
function user_pwdblur(obj, opType){
	var pwd=$(obj).val();
	if(pwd== ""){
		$(obj).siblings('span.help-block').html('<small>密码不能为空!</small>');
		inputErr(obj);
		resetClick(opType);
		return false; 
	}
	var bValidate = RegExp(/^((?=.*\d)(?=.*\D)|(?=.*[a-zA-Z])(?=.*[^a-zA-Z]))^.{8,16}$/).test(pwd);
	if(!bValidate){
		$(obj).siblings('span.help-block').html('<small>长度8~16位，数字、字母、符号至少包含两种!</small>');
		inputErr(obj);
		resetClick(opType);
		return false;
	}
	inputOk(obj);
	if (opType != 'blur') {
		mobileblur($('#mobile'), opType);
	}
}
//手机号码
function mobileblur(obj, opType){
	var phone=$(obj).val();
	if (phone == "") {		 
		$(obj).siblings('span.help-block').html('<small>手机号码不能为空！</small>');
		inputErr(obj);
		resetClick(opType);
		return false; 
	}
	var txtCode = $("#txtCode");
	if(!LostFocus_Code(txtCode)) {
		$(obj).parent().removeClass('has-error');
		$(obj).siblings('span.help-block').addClass('hidden');
		$(obj).siblings('span.glyphicon-remove').addClass('hidden');
		resetClick(opType);
		return false; 
	}
	$.ajax({
		url:'${mainServer}/fregisteMobile',
		type:'post',
		data:{mobile:phone,txtCode:$(txtCode).val()},
		success:function(data){
 			switch (data) {
			case 0:
				$(obj).siblings('span.help-block').html('<small>手机号码已注册！</small>');
				inputErr($("#mobile"));
	 			resetClick(opType);
				break;
			case 1:
				inputOk($("#mobile"));
				if (opType == 'sms') {
					sendCode();
				} else if (opType == 'reg') {
					sms_codeblur($('#sms_code'), opType);
				}
				break;
			case 2:
				$(txtCode).siblings('span.help-block').html('<small>图形验证码错误!</small>');
				inputErr(txtCode);
	 			resetClick(opType);
				break;
			case 3:
 				$(obj).siblings('span.help-block').html('<small>手机号码可以使用！</small>');
				$(txtCode).siblings('span.help-block').html('<small>图形验证码错误!</small>');
				inputErr(txtCode);
	 			resetClick(opType);
				break;
			default:
	 			resetClick(opType);
				break;
			}
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			resetClick(opType);
		}
	});
}
//图形验证码
function LostFocus_Code(obj) {
    if ($(obj).val() == "") {
		$("#txtCode").siblings('span.help-block').html('<small>图形验证码不能为空!</small>');
		inputErr($("#txtCode"));
		return false;
    } else {
    	inputOk($("#txtCode"));
    	return true;
    }
}
//手机验证码
function sms_codeblur(obj, opType){
	if ($(obj).val() == "") { 
		$(obj).siblings('span.help-block').html('<small>验证码不能为空！</small>');
		inputErr(obj);
		resetClick(opType);
		return false; 
	}
	inputOk(obj);
	if (opType == 'reg') {
		Registe();
	}
}
// 获取验证码
function sendCode(){
	$("#email").attr('readonly','readonly');
	$("#user_pwd").attr('readonly','readonly');
	$("#mobile").attr('readonly','readonly');
	$("#freeCode").html('120 获取中......');
	var mobile = $('#mobile').val();
	var code = $("#txtCode").val();
	var mobilePrefix=$("#mobile_prefix").data('value');
	setTimeout(timer, 1000);
    $.ajax({
		url:'${mainServer}/sms/sendRegistCode',
		type:'post',
		data:{mobile:mobile,txtCode:code,mobile_prefix:mobilePrefix},
		success:function(data){			
		}		
	});
}
//计时器
var timer=function(){
	second--;
	if (second > 0) {
		$("#freeCode").html(second+" 获取中......");
		setTimeout(timer, 1000);
	} else {
		$("#freeCode").off('click');
		$("#freeCode").on('click', function(){
			$("#freeCode").off('click');
			opType = 'sms';
			emailblur($('#email'), 'sms');
		});
		$("#freeCode").html('获取验证码');
		second=120;
	}
}
//注册
function Registe(){
 	var email = $("#email").val();
	var user_pwd = $("#user_pwd").val();
	var mobile = $("#mobile").val();
	var sms_code = $("#sms_code").val();
	var trecommand_user_id = $("#trecommand_user_id").val();
	var source = $("#source").val();
	var pushLinkId = $("#pushLinkId").val();
	$.ajax({
		url:'${mainServer}/userRegist',
		type:'post',
		data:{
			"email":email,
			"user_pwd":user_pwd,
			"mobile":mobile,
			"sms_code":sms_code,
			"trecommand_user_id":trecommand_user_id,
			"source":source,
			"pushLinkId":pushLinkId
		},
		success:function(data){	
			if(data.isSuccess){
				$('#div_success').text("注册成功！"+successSecond+"秒后跳转登录页面");
				$('#div_err').addClass('hidden');
				$('#div_success').removeClass('hidden');
				setInterval(successTimer, 1000);
			}else{ 
				$('#div_err').text("注册失败！原因："+data.smsCodeErr);
				$('#div_err').removeClass('hidden');
				//将验证码设置为空
				$("#sms_code").val("");
				gototop();
				$("#btnreg").off('click');
				$("#btnreg").on('click', function(){
					$("#btnreg").off('click');
					emailblur($('#email'), 'reg');
				});
			}
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			$("#btnreg").off('click');
			$("#btnreg").on('click', function(){
				$("#btnreg").off('click');
				emailblur($('#email'), 'reg');
			});
		}
	}); 
}
function gototop(){
	{$('html,body').animate({scrollTop: '0px'}, 800);} 
}
function refresh(){
	document.getElementById("imgVC").src="${mainServer}/authImg?change="+(new Date()).getTime();
}
</script>
 <jsp:include page="footer-1.jsp"></jsp:include>
</body>
</html>