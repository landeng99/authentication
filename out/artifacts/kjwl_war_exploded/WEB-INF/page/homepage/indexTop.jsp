<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- 头部 -->
<header class="row header">
	<div class="logo col-sm-2">
		<img src="${resource_path}/images/logo-t.png" alt="" class="logo-img">
	</div>
	<div class="col-sm-8">
		<ul class="menu" id="topMenu"></ul>
	</div>
	<div class="r-head col-sm-2">
	<c:if test="${null == frontUser || 'id：0-用户名：null-类型：0' == frontUser}"> 
		<a href="${mainServer}/login" class="item"><i class="iconfont icon-login"></i>登录</a>
		<a href="${mainServer}/registe" class="item"><i class="iconfont icon-zhuce"></i>注册</a>
	</c:if>
	<c:if test="${null != frontUser && 'id：0-用户名：null-类型：0' != frontUser}">
		<a href="${mainServer}/exit" class="item"><i class="iconfont icon-tuichu"></i>退出</a>
		<a href="${mainServer}/transport" class="item"><i class="iconfont icon-login"></i>我的账户</a>
	</c:if>
	</div>
</header>

<script type="text/javascript">
$(function(){    
    $('#topMenu').on('mouseover', 'li[name=firstMenu]', function(){
    	var children = $(this).data('children');
    	if (children) {
    		$(this).addClass('active');
    	}
    });
    
    $('#topMenu').on('mouseout', 'li[name=firstMenu]', function(){
    	var children = $(this).data('children');
    	if (children) {
    		$(this).removeClass('active');
    	}
    });
	queryMenu();
});

/**
 * 设置活动菜单
 */
function setSelectedLi(){
	var curPath = window.document.location.href;       //获取访问当前页的目录，如：  http://localhost:8080/test/index.jsp 
	var pathName = window.document.location.pathname;  //获取主机地址之后的目录，如： test/index.jsp 
    var projectName = pathName.substring(0,pathName.substr(1).indexOf('/')+1);   //获取带"/"的项目名，如：/test 
    var posProjectName = projectName.indexOf(projectName)+1;
    var pathUrl = pathName.substring(projectName.length,pathName.length); //index.jsp
    var isIndexFlag = true;
    if(pathUrl != null && pathUrl.length > 1){//因首页只有"/"
    	isIndexFlag = false;
    }
    
    //菜单遍历
	$("#topMenu").children("li").each(function(i){
		var menu = $(this).find("a").first().attr("href");
		if(i == 0){ //首页
			if(isIndexFlag){
				$(this).attr("class","selected");
			} else {
				$(this).attr("class","");
			}
		} else { //非首页
			if(menu != "" && curPath.indexOf(menu) > -1){
				$(this).attr("class","selected");
			} else {
				$(this).attr("class","");
			}
		}
	});
}

function queryMenu() {
	$.ajax({
		type: "post",
		dataType: "json",
		url: '${mainServer}/admin/navigation/queryMenu',
		data: {},
		success: function (data) {
			$("#topMenu").empty();
			$.each(data, function(i, nav){
				var li = $('<li name="firstMenu"></li>');
				var div = $('<div class="m-item"></div>');
				if (nav.childrenLst != null && nav.childrenLst.length > 0) {
					$(li).data('children', true);
					$(div).append('<a href="'+(nav.url==''?'#':nav.url)+'">'+nav.nav_name+'<i class="caret"></i></a>');
					var ul = $('<ul class="m-sc-item"></ul>');
					$.each(nav.childrenLst, function(i, cnav) {
						$(ul).append('<li ><a href="'+(cnav.url==''?'#':cnav.url)+'">'+cnav.nav_name+'</a></li>');
					});
					$(div).append(ul);
				} else {
					$(li).data('children', false);
					$(div).append('<a href="'+nav.url+'">'+nav.nav_name+'</a>');
				}
				$(li).append(div);
				$("#topMenu").append(li);
			});
			setSelectedLi();
		}
	});
}
</script>
