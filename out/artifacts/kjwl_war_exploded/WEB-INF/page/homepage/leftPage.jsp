<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >

<div class="p-sidebar" id="leftMenu">
    <ul>
        <li><a href="${mainServer}/personalCenter"><i class="iconfont icon-shouye"></i>个人中心首页</a> </li>
        <li><a href="${mainServer}/transport"><i class="iconfont icon-baoguo"></i>我的包裹<span class="label label-warning">${pkgCount}</span></a> </li>
        <li><a href="${mainServer}/warehouse"><i class="iconfont icon-xt5"></i>海外仓库地址</a> </li>
        <li><a href="${mainServer}/useraddr/destination"><i class="iconfont icon-dingwei"></i>收货地址</a> </li>
        <li><a href="${mainServer}/member/record"><i class="iconfont icon-zijin"></i>资金记录</a> </li>
        <li><a href="${mainServer}/account/accountInit"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
        <li><a href="${mainServer}/account/couponInit"><i class="iconfont icon-youhuiquan"></i>优惠券<span class="label label-warning">${countAvailable}</span></a> </li>
    </ul>
</div>
<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.min.js"></script>

<script type="text/javascript">
$(function(){
	setSelectedLi();
});

/**
 * 设置活动菜单
 */
function setSelectedLi(){
	var curPath = window.document.location.href;       //获取访问当前页的目录，如：  http://localhost:8080/test/index.jsp 
	$("#leftMenu ul li").each(function(){
		var menu = $(this).find("a").attr("href");
		if(curPath.indexOf(menu) > -1){
			$(this).attr("class","selected");
		} else {
			$(this).attr("class","");
		}
	});
}
</script>