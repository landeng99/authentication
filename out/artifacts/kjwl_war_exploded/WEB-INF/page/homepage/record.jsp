<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>速8快递资金记录</title>
    
    <script src="${backJsFile}/My97DatePicker/WdatePicker.js"></script>
    
    <!-- 新增的css -->
	<jsp:include page="../common/commonCss.jsp"></jsp:include>
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css"> <!-- 前店公共框架样式 头部 -->
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/dropify/dist/css/dropify.min.css"> <!-- 图片上传控件样式 --> 
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css"> 
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/p-capital.css">
    
	<script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script>
</head>
<body>
	<!-- 头部页面 -->
	<jsp:include page="topPage.jsp"></jsp:include>
	
	<!-- banner -->
    <div class="min-banner">
        <div class="bgImg"></div>
        <div class="logo"></div>
    </div>
    
    <div class="wrap bg-gray">
        <div class="wrap-box pb20 pt20 clearfix">
            <!-- 内容 -->
            <div class="p-content">
                <div class="p20">
                    <h5 class="tit p10" style="margin-bottom: 10px;">账户余额</h5>
                    <div class="p-capital-money container-fluid">
                        <div class="col-sm-9">
                            <dl class="p-capital-money-dl">
                                <dt><span><fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#,###.##"/></span></dt>
                                <dd >冻结余额：
	                                <span class="text-muted">
		                                <fmt:formatNumber value="${frontUser.frozen_balance}" type="currency" pattern="$#,###.##"/>
	                                </span>
                                </dd>
                                <dd>可用余额：
                                	<span class="text-info">
                                		<span><fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#,###.##"/></span>
                                	</span>
                                </dd>
                            </dl>
                        </div>
                        <!-- <div class="col-sm-3 text-right pt15">
                            <a class="btn btn-primary btn-radius pr20 pl20" id="charge">充值</a>
                            <a class="btn btn-default btn-radius ">提现</a>
                        </div> -->
                    </div>
                </div>
                <div class="bg-gray p10"></div>
                <div class="pt20 pl20 pr20">
                    <h5 class="tit"><span class="mr10">资金记录</span>
                        <div class="alert alert-warning text-overflow" style="display: inline-block;width:auto; padding: 5px 10px;margin-bottom: 0;">
                             <i class="glyphicon glyphicon-info-sign pr5"></i>
                             	交易记录不包括使用优惠券金额
                         </div>
                    </h5>
                 </div>
                <div class="tab-box">
                   <!-- tab -->
                   <ul class="nav nav-tabs " role="tablist" id="recordTab">
                       <li class="active">
                            <a href="#all" data-toggle="tab" role="tab" aria-expanded="true" accountType="0" >  
                               <span>全部记录</span>
                           </a>
                       </li>
                       <li class="">
                           <a href="#rechargeRecord" data-toggle="tab" role="tab" aria-expanded="false" accountType="1" >
                               <span>充值记录</span>
                           </a>
                       </li>
                       <li class="">
                           <a href="#consumeRecord" data-toggle="tab" role="tab" aria-expanded="false" accountType="2" >
                               <span>消费记录</span>
                           </a>
                       </li>
                       <li class="">
                           <a href="#cashRecord" data-toggle="tab" role="tab" aria-expanded="false" accountType="3" >
                               <span>提现记录</span>
                           </a>
                       </li>
                       <li class="">
                           <a href="#payforRecord" data-toggle="tab" role="tab" aria-expanded="false" accountType="4" >
                               <span>索赔记录</span>
                           </a>
                       </li>
                   </ul>
                   <!-- tab end-->

                   <!-- tab内容 -->
                   <div class="listbox">
                      <div class="m20 row">
                        <div class="col-sm-7">
                          <form class="form-inline">
                          <div class="form-group " style="margin-bottom: 0;"> 
                              <label  class="pr5 control-label text-right">时间:</label>
                              <input type="text" class="form-control" id="fromDate" name="fromDate" 
                              	onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
								value="${params.fromDate}"
								onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
								onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
                              <span class="text-muted">-</span>
                              <input type="text" class="form-control" id="toDate" name="toDate"
                              	onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                               	value="${params.toDate}"
								onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
								onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
                          </div> 
                          </form>
                        </div>
                        <div class="col-sm-5 text-right">
                          <span class="pr10 f12">近一年实际消费金额<strong class="text-danger p5 "><span class="orange size16" id="TotalSpendi"><fmt:formatNumber value="${sumAmount}" type="currency" pattern="$#,###.##"/></span></strong></span>
                          <a href="javascript:exportPkgConfirm()" class="btn btn-default btn-radius btn-sm pl20 pr20">导出记录</a>
                        </div>
                     </div>
                   </div>
                   
                   
                   <div class="tab-content ">
                     <div role="tabpanel" class="tab-pane  active" id="all">
                     </div>
                     
                     <!-- 消费记录 -->
                     <div role="tabpanel" class="tab-pane" id="consumeRecord">
                          
                     </div>
                     
                     <!-- 充值记录 -->
                     <div role="tabpanel" class="tab-pane" id="rechargeRecord">
                          
                     </div>
                     
                      <!-- 提现记录 -->
                     <div role="tabpanel" class="tab-pane" id="cashRecord">
                          
                     </div>
                     
                     <!-- 索赔记录 -->
                     <div role="tabpanel" class="tab-pane" id="payforRecord">
                          
                     </div>
                     
                   </div>
               </div>
               
            </div>
            <!-- 左侧页面 -->
			<jsp:include page="leftPage.jsp"></jsp:include>
        </div>
    </div>
    
    <!-- 底部页面 -->
	<jsp:include page="bottomPage.jsp"></jsp:include>


    <script type="text/javascript" src="${resource_path}/font/iconfont.js"></script>
	<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="${resource_path}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<script src="${resource_path}/js/layer.min.js"></script>
    

 <script type="text/javascript">

    $(function () {
        //  最近三月，三月以前
        $(".recordliXXXXX").click(function () {
            
            $("ul.recordul>li").removeClass("current");
            $(this).addClass("current");
            var url = "${mainServer}/member/allConsumption";
           $.ajax({
                url:url,
                type:'post',
                async:false,
                dataType:'json',
                data:{"id":(this).id,
                    "status":status},
                success:function(data){
                   $("#recordlist>li").not(":first").remove();
                   $.each(data, function(i, ele) {

                   var li = "<li style='background-color:White;'><ol>"
                          + "<li style='text-align:center;width:150px;padding-left:0;'>" + ele.opr_time_string + "</li>"
                          + "<li style='text-align:center;width:150px;padding-left:0;'>" + ele.logistics_code + "</li>"
                          + "<li style='width:110px;padding-left:0;'><div style='float:right;margin-right:10px;'>$" + fmoney(ele.amount,2) + "</div></li>"
                          + "<li style='width:407px;padding-left:0;'><div style='float:left;margin-left:10px;'>" + ele.description + "</div></li>"
                          + "</ol></li>";
                          
                   $("#recordlist").append(li);
                   });
                }
            });
  
        });
        // 提现取消刷新页面
        $(document).ready(function(){

              if($("#flag").val() ==3){
              $("#three").click(); 
              }

            }); 
        //  充值消费提现索赔
        $(".recordli").click(function () {
            
            $("ul.recordul>li").removeClass("current");
            $(this).addClass("current");
            var clickId =(this).id;
            
            if(clickId =="one"){
                    $("#recordOne").show();
                    $("#recordTwo").hide();
                    $("#recordThree").hide();
                    $("#recordFour").hide();
                
            }else if(clickId =="two"){
                   $("#recordOne").hide();
                   $("#recordTwo").show();
                   $("#recordThree").hide();
                   $("#recordFour").hide();
                
            }else if(clickId =="three"){
                   $("#recordOne").hide();
                   $("#recordTwo").hide();
                   $("#recordThree").show();
                   $("#recordFour").hide();
                
            }else if(clickId =="four"){
                   $("#recordOne").hide();
                   $("#recordTwo").hide();
                   $("#recordThree").hide();
                   $("#recordFour").show();
                
            }
   
        });

        //格式化金额
    function fmoney(s, n) {
            n = n > 0 && n <= 20 ? n : 2;
            s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
            var l = s.split(".")[0].split("").reverse();
            var r = s.split(".")[1];
            var t = "";
            for ( var i = 0; i < l.length; i++) {
                t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
            }

            var str = t.split("").reverse().join("") + "." + r;

            return  str;
        }
    });
</script>
<script type="text/javascript" src="${resource_path}/js/ZeroClipboard.js"></script>
<script type="text/javascript">
$(function(){
	 $(document).ready(function(){
		 bindRecordTab(); 
		 ajaxRecord(1);
	 });
    $("#charge").click(function () {
        var url = "${mainServer}/member/frontUserStatus";
        $.ajax({
            url:url,
            type:'post',
            async:false,
            dataType:'json',
            success:function(data){
                // 账户正常
                if(data.result =="1"){
                	 window.location.href="${mainServer}/account/torecharge"; 
                }
                else{
                   alert(data.message);
                }
            }
        });
    });
});

//取消提现
function cancel(log_id){
     var url = "${mainServer}/member/withdrawalCancel";
     $.ajax({
          url:url,
          type:'post',
          async:false,
          dataType:'json',
          data:{"log_id":log_id},
          success:function(data){
              alert(data.message);
              if(data.result){
                 window.location.href="${mainServer}/member/record?flag="+3; 
              }

          }
      });
}



var exportPackageWindow;
function showExportPackageWindow() {
	showIndex = 0;
	var off = 0;
	off = ($(window).height() - 200) / 2;
	exportPackageWindow = $.layer({
		bgcolor: '#fff',
		type: 1,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [0, 0.3, '#666', true],          
		area: ['450px', '150px'],
		offset: [off, ''],
		shadeClose:true,
		title: false,
		zIndex:10002,
		page: { dom: '#exportPkgWinId' },
		closeBtn: [0, false],//显示关闭按钮
		close: function (index) {
			layer.close(index);
			$('#exportPkgWinId').hide();
			//ActivityID = 0;
		}

	});
}
$('#closeExportPkgWinId').on('click', function(){
	layer.close(exportPackageWindow);
});
function exportPkgConfirm(){
	var timeStart=$("#fromDate").val();
	var timeEnd=$("#toDate").val();
	window.location.href="${mainServer}/member/frontEndexportList?timeStart="+timeStart+"&timeEnd="+timeEnd;
		layer.close(exportPackageWindow);
}

var accountType = "0";
var showId = "#all";
function bindRecordTab(){
	$("#recordTab").find("li").each(function(){
		$(this).on("click",function(){
			showId = $(this).find("a").attr("href");
			accountType = $(this).find("a").attr("accountType");
			ajaxRecord(1);
		});
	});
}

function ajaxRecord(pageNo){
	var url = "${mainServer}/member/getRecordInfoList2";
	var param = {
			accountType : accountType,
			pageNo : pageNo
	};
    $.ajax({
         url:url,
         type:'post',
         async:true,
         dataType:'html',
         data:param,
         success:function(data){
        	 $(showId).empty();
        	 $(showId).html(data);
         }
     });
}

</script>
</body>
</html>