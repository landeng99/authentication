<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<script type="text/javascript"
	src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<link rel="stylesheet" type="text/css" href="${resource_path}/font/iconfont.css"/> <!-- 字体图标 -->
<link rel="stylesheet" type="text/css" href="${resource_path}/css/bootstrap.min.css"> <!--物流信息展现 -->
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/p-myPackage.css"> <!--物流信息展现 -->

<style type="text/css">
p {
	color: #666666;
	font-family: "微软雅黑";
	font-size: 12px;
	font-weight: normal;
	list-style-type: none;
	margin-bottom: 0;
	margin-left: 0;
	margin-right: 0;
	margin-top: 0;
	padding-bottom: 0;
	padding-left: 0;
	padding-right: 0;
	padding-top: 0;
}

.mg18 {
	margin-bottom: 18px;
	margin-left: 0;
	margin-right: 0;
	margin-top: 18px;
}

.hr {
	background-color: #EBEBEB;
	height: 1px;
	overflow-x: hidden;
	overflow-y: hidden;
	width: 100%;
}

.List06 {
	overflow-x: hidden;
	overflow-y: hidden;
	width: 100%;
}

.List06 li {
	height: 20px;
	list-style-type: none
}

.List06 li span.pull-left {
	overflow-x: hidden;
	overflow-y: hidden;
	text-align: right;
	width: 63px;
}

.List06 li input {
	margin-right: 4px;
}

.List06 li .text08 {
	-moz-border-bottom-colors: none;
	-moz-border-left-colors: none;
	-moz-border-right-colors: none;
	-moz-border-top-colors: none;
	background-attachment: scroll;
	background-clip: border-box;
	background-color: rgba(0, 0, 0, 0);
	background-image: url("${resource_path}/img/input24.png");
	background-origin: padding-box;
	background-position: 0 0;
	background-repeat: no-repeat;
	background-size: auto auto;
	border-bottom-color: -moz-use-text-color;
	border-bottom-style: none;
	border-bottom-width: medium;
	border-image-outset: 0 0 0 0;
	border-image-repeat: stretch stretch;
	border-image-slice: 100% 100% 100% 100%;
	border-image-source: none;
	border-image-width: 1 1 1 1;
	border-left-color-ltr-source: physical;
	border-left-color-rtl-source: physical;
	border-left-color-value: -moz-use-text-color;
	border-left-style-ltr-source: physical;
	border-left-style-rtl-source: physical;
	border-left-style-value: none;
	border-left-width-ltr-source: physical;
	border-left-width-rtl-source: physical;
	border-left-width-value: medium;
	border-right-color-ltr-source: physical;
	border-right-color-rtl-source: physical;
	border-right-color-value: -moz-use-text-color;
	border-right-style-ltr-source: physical;
	border-right-style-rtl-source: physical;
	border-right-style-value: none;
	border-right-width-ltr-source: physical;
	border-right-width-rtl-source: physical;
	border-right-width-value: medium;
	border-top-color: -moz-use-text-color;
	border-top-style: none;
	border-top-width: medium;
	height: 32px;
	line-height: 32px;
	overflow-x: hidden;
	overflow-y: hidden;
	width: 178px;
}

.PushButton {
	overflow-x: hidden;
	overflow-y: hidden;
	width: 250px;
}

.PushButton a {
	float: right;
	font-size: 16px;
	line-height: 32px;
	margin-left: 16px;
}

.PushButton a.button {
	-moz-text-blink: none;
	-moz-text-decoration-color: -moz-use-text-color;
	-moz-text-decoration-line: none;
	-moz-text-decoration-style: solid;
	background-attachment: scroll;
	background-clip: border-box;
	background-color: rgba(0, 0, 0, 0);
	background-image: url("${resource_path}/img/button2.png");
	background-origin: padding-box;
	background-position: 0 0;
	background-repeat: no-repeat;
	background-size: auto auto;
	color: #FFFFFF;
	height: 32px;
	overflow-x: hidden;
	overflow-y: hidden;
	text-align: center;
	width: 72px;
}

.span4 .label {
	width: 70px;
	float: left;
}
</style>
<body  > <!-- 新的物流  showWuliu-->
   <h5 class="package-title">运单号码： ${logistics_code}</h5>
         <div class="package-status order-detail " style="background:#fff;">
            <div class="status-box">
                <ul class="status-list">
                <c:forEach var="item" items="${list}" varStatus="status"> 
                <script>
              	var expressTimeStr1 = "${item.time}"; 
            	var datetime = expressTimeStr1.substr(0,10);
                var timetime = expressTimeStr1.substr(11,8);
                var content = "${item.context}"; 
                var statuss ="${status.index}";
                if( parseInt(statuss) == 0 ){
                    $(".status-list").append("<li class='latest'><i class='iconfont icon-right'></i><span class='date'>"+datetime+"</span><span class='time'>"+timetime+"</span><span class='text'>"+content+"</span></li>");

                }else{
                    $(".status-list").append("<li><i class='iconfont icon-xiangxia'></i><span class='date'>"+datetime+"</span><span class='time'>"+timetime+"</span><span class='text'>"+content+"</span></li>");

               }
                 </script>  
			 </c:forEach>
                </ul>
            </div>
        </div> 
         <!-- 新的物流 -->
  	   <%-- <div style="margin-left: 5px;" class="right">
		<div>
			<div style="display: block;" id="withdrawal">
				<ul class="List06">
					<li>
						<p>
							<span style="margin-top: 5px;">运单号：</span> <span>&nbsp;${logistics_code}&nbsp;&nbsp;</span>
						</p>
					</li>
				</ul>
				<ul class="PROcess">
					<c:forEach var="item" items="${list}" varStatus="status">
					<li 					<c:choose>
					<c:when test="${status.index==0 }">style="line-height: 2;color:#ff0000;font-weight: 600;font-size: small;"</c:when>
					<c:otherwise>style="line-height: 2;font-size: small;"</c:otherwise>
					</c:choose>>
						<span class="wz2">${item.time} : </span>
						<span class="express_text">${item.context}</span>
					</li>
					</c:forEach>
				</ul>
				<div class="PushButton">
					<a id="no" class="button" style="cursor: pointer;">返回</a> 
				</div>
			</div>
		</div>
	</div>    --%>

	<script type="text/javascript">

		//获取窗口索引
		var index = parent.layer.getFrameIndex(window.name);
		$("#no").click(function() {
			// 关闭窗口
				parent.layer.close(index);
		});

	</script>
</body>
</html>