<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../common/commonCss.jsp"></jsp:include> <!-- 前店公共框架样式 头部 -->
  <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css"> 
  <script src="${resource_path}/js/page.js"></script>
  
<form id="gopage">
 
 <div class="pagebox clearfix gray ">
                            <div class="pageInfo">
                                <span class="p5">总条数：${pageView.rowCount}条</span>|
                                <span class="p5">每页：${pageView.pageSize}条</span>|
                                <span class="p5">总页数：${pageView.pageCount}页</span>
                            </div>
                           <nav aria-label="Page navigation" class="pageNav">
                            <a href="javascript:pageNow(1)" class="page-position" style="top:10px">首页</a>
                            <ul class="pagination">
                               <li >
                                 <a href="javascript:goPrent();"  aria-label="Previous"> 
                                   <span aria-hidden="true" class="glyphicon glyphicon-chevron-left" style="padding:3px 0px;"></span>
                                 </a>
                               </li>
			                     <c:forEach begin="${pageView.pageindex.startindex}" end="${pageView.pageindex.endindex}" var="key">
									<c:if test="${pageView.pageNow==key}">
										 <li class="active"><a href="javascript:;" onclick="pageNow(${key})">${key}</a></li>
									</c:if>
									<c:if test="${pageView.pageNow!=key}">
										 <li><a href="javascript:;" onclick="pageNow(${key})">${key}</a></li>
									</c:if>
								</c:forEach>&nbsp; 
                               <li>
                                 <a  href="javascript:goAfter();"   aria-label="Next">
                                   <span aria-hidden="true" class="glyphicon glyphicon-chevron-right" style="padding:3px 0px;"></span>
                                 </a>
                               </li>
                            </ul>
                             <a href="javascript:pageNow(${pageView.pageCount});" class="page-position" style="top:10px" >尾页</a>
                            
                           </nav>
                           <div class="pageTz">
                               <span class="p5">到第</span><input type="text" class="form-control form-control-inline" name="pagenoto" id="pagenoto"><span class="p5">页</span>
                               <a   class="btn btn-primary"   href="javascript:gotopage()">跳转</a>
                           </div>
                       </div> 
    <input type="hidden"  name="allpageno" id="allpageno" type="text"  value="${pageView.pageCount}" >
    <input type="hidden"  name="nowpage" id="nowpage" type="text"   value="${pageView.pageNow}"  >

   </form> 