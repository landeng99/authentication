<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">
.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 270px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button2.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 72px;
}
</style>
<body>
<div style="background-image:none;border-right:none;width:350px;padding-top:5px;"/>
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
        <div>
            <div class="hr mg18"></div>
            <div style="display: block;" id="withdrawal">
                  <div class ="div-front">
					<input type="hidden" id="package_id" value="${package_id}"/>
					<label for="Express" style="float: left;margin-top: 55px;margin-left: 80px;font-size: 15px;">渠道:</label> 
					<select id="Express" class="input blue" name="Express" style="float: right; margin-right: 100px; margin-top: 50px; height: 33px; width: 100px;">
						<option value="0">F渠道</option>
						<!-- <option value="1">A渠道</option> -->
						<option value="2">B渠道</option>
						<!-- <option value="3">C渠道</option> -->
						<!-- <option value="4">奶粉专线</option> -->
						<!-- <option value="5">E渠道</option> -->
					</select> 
				  </div>
                  <div class="PushButton">
                       <a id="no"  class="button" style="cursor:pointer;margin-top: 60px;margin-left: 50px;float: left;">取消</a>
                       <a id="yes" class="button" style="cursor:pointer;margin-top: 60px;float: right;margin-left: 70px;">确定</a>
                  </div>
           </div>
      </div>
   </div>
<script type="text/javascript">
//获取窗口索引
var index = parent.layer.getFrameIndex(window.name);
$("#no").click(function () {
    // 关闭窗口
    parent.layer.close(index);
}); 

$("#yes").click(function () {
    var package_id = $("#package_id").val();
	var express_package = $("#Express").val();
		var url = "${mainServer}/editExpress";
		$.ajax({
			url:url,
			data:{
				"package_id":package_id,
				"express_package":express_package,
			},
			type:'post',
			datatype:'json',
			async:false,
			success:function(data){
				alert("提示！渠道修改成功！");
				 //window.parent.location.href="${mainServer}/transport";
				window.parent.location.href=window.parent.location.href;
			}
		});
}); 
</script>
</body>
</html>