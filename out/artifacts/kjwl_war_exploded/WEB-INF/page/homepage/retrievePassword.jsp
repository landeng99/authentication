<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0021)http://www.birdex.cn/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<!-- 
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/sylogin.css" rel="stylesheet" type="text/css"/>
 -->
<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
<script type="text/javascript" src="${resource_path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
</head>
<body onkeydown="doKeyDown()">
<jsp:include page="topIndexPage.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/login.css"></link>
<%-- 
<form method="get" action="${mainServer}/retPassType" id="loginform">
<div id="wrap" style="height:660px;width:100%;">
<div id="subwrap">
    <div class="forgotpassword" style="padding-top: 0px; padding-bottom: 0px; height:660px;" id="wrapModal">
         <div class="modal-dialog" style="margin-left:35%;margin-top:90px;background-color: #ECFBFB;">
              <div class="bt2"><span style="font-size:20px;">找回密码</span><span style="margin-left:5px;color:#B2A9A9;">RetrievePassword</span></div>  
              <dl class="Box02">
                  <dt><span>找回方式：</span></dt>
                  <dd> 
                  <select id="userInfoType" name="userInfoType" 
                  class="input_Light_red input_all_red"
                  style="width:287px;height:40px;" >
                  	<!-- <option value="1">邮箱方式</option> -->
                  	<option value="2" selected="selected">手机方式</option>
                  </select>
                  </dd>
                  
                  <dt><span>手机号码：</span></dt>
                  <dd>
                  <input name="userInfoAccount" type="text" id="userInfoAccount" 
                  class="input_Light_blue" value="请输入手机号码" 
                  style="width:280px;height:40px;" 
                  onFocus="focusUserInfoAccount()"  onblur="blurUserInfoAccount()"/>
                  </dd>
              </dl>
			  <div id="fail" style="margin:0 auto;width:90%;margin-top:10px;text-align:center;font-size:16px;color:#F50D0D;">
				    ${fail}
		      </div>
              <div class="Bottm2">
                   <input  type="button" id="btnlogin" value="下一步" class="button_bule button pull-left" style="width:110px;margin-left:10px;"/>
				   <input  type="button" id="btnReset" value="取消" class="button_bule button pull-left" style="width:110px;margin-left:40px;"/>
              </div>
         </div>
    </div>
</div>
</div>
</form>
 --%>
 <div class="wrap bgImg">
        <div class="panel ">
            <h1 class="tit">找回密码</h1>
            <div id="div_err" class="alert alert-danger hidden" role="alert"></div>
            <form class="floating-labels" method="post" action="${mainServer}/retPassType" id="loginform">
                <div class="form-group form-group-btn mb40">
                    <input class="form-control" name="userInfoAccount" id="userInfoAccount" type="telephone"  placeholder="输入手机号码" />
                    <input type="hidden" id="userInfoType" name="userInfoType" />
                    <span class="input-group-btn J-select-country"><!--加 selected -->
                        <button id="userInfoTypeBtn" class="btn btn-default btn-outline bootstrap-touchspin-up" type="button" data-value="2" data-text="手机号">
                        	手机号<div class="caret"></div>
                        </button>
                        <ul class="select-country J-select-country-ul"> <!-- 选择内容 -->
                            <li data-value="2" data-text="手机号" class="selected"><a ><i class="glyphicon glyphicon-ok text-success"></i>手机号</a></li>
<!--                             <li data-value="1" data-text="邮箱"><a><i class="glyphicon glyphicon-ok text-success"></i>邮箱</a></li> -->
                        </ul>
                    </span>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="input1">手机号码</label>
                    <span class="help-block hidden"><small></small></span>
                </div>
            </form>
            <div class="text-center mb20"><button id="btnlogin" class="btn-primary btn-input btn">下一步</button></div>
        </div>
    </div>
<script>
function doKeyDown(event) {
	var e=event || window.event || arguments.callee.caller.arguments[0];
	if (e.keyCode == 13) {
	   nextstep();
	}
}

$(function () {
	$("#btnlogin").on('click', function(){
		nextstep();
	});
	$('#userInfoAccount').on('blur', function(){
		var val=$(this).val().trim();
		$(this).val(val);
		isValid();
	});
	$(".J-select-country").click(function() {
        $(this).toggleClass("selected");
    });
    $(".J-select-country-ul li").click(function() {
        $(this).addClass("selected");
        $(this).siblings().removeClass("selected");
        var value = $(this).data('value');
        var text = $(this).data('text');
        $('#mobile_prefix').data('value', value);
        $('#mobile_prefix').data('text', text);
        $('#mobile_prefix').html(text+'<div class="caret"></div>');
    });
});

function inputErr(obj){
	$(obj).parent().removeClass('has-success');
	$(obj).parent().addClass('has-error');
	$(obj).siblings('span.help-block').removeClass('hidden');
	$(obj).siblings('span.glyphicon-remove').removeClass('hidden');
	$(obj).siblings('span.glyphicon-ok').addClass('hidden');
}

function inputOk(obj){
	$(obj).parent().removeClass('has-error');
	$(obj).siblings('span.help-block').addClass('hidden');
	$(obj).siblings('span.glyphicon-remove').addClass('hidden');
	$(obj).siblings('span.glyphicon-ok').removeClass('hidden');
	$(obj).parent().addClass('has-success');
}

function isValid(){
	var obj = $("#userInfoAccount");
	var varType = $("#userInfoTypeBtn").data('value');
	var massInfo = "请输入你的手机号码";
	var massTestInfo = "你的手机格式不正确,请正确输入";
	var varTest = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}$/;
	if(varType == 1){
		massInfo = "请输入你的邮箱账号";
		massTestInfo = "你的邮箱格式不正确,请正确输入";
		varTest = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	}
	
	var varAccount = $(obj).val();
	if (varAccount == "") {
		$(obj).siblings('span.help-block').html('<small>'+massInfo+'</small>');
		inputErr(obj);
		return false;
	}
	/*
	if (!(varAccount.match(varTest) || varTest.test(varAccount))) {
		$(obj).siblings('span.help-block').html('<small>'+massTestInfo+'</small>');
		inputErr(obj);
		return false;
	}
	*/
	inputOk(obj);
	return true;
}

function nextstep(){
	if(isValid()){
		var varType = $("#userInfoTypeBtn").data('value');
		var varAccount = $("#userInfoAccount").val();
		var url = "${mainServer}/existUserInfoAccount";
		$.ajax({
			url:url,
			data:{userInfoAccount:varAccount},
			type:'post',
			success:function(data){
				if(data == 1){
					$("#div_err").addClass('hidden');
					$("#userInfoType").val(varType);
					$('#loginform').submit();
				}else{
					if(varType == 1){
						$("#div_err").html("邮箱账号不存在");
					}else{
						$("#div_err").html("手机账号不存在");
					}
					$("#div_err").removeClass('hidden');
				}
			}
		}); 
	}
}
function Reset(){
	history.go(-1);
}
</script>
<jsp:include page="footer-1.jsp"></jsp:include>
</body>
</html>