<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
</head>
<style>
 body {
 	margin: 0px;
 }
 
 .info{text-align: center;margin-top:200px;margin-bottom:20px; font-weight:bold;font-family:"黑体";}
 .info p{font-size:15px;}
 .error_info{height:30px;margin:20px auto;width: 336px;}
 .error_info span{float: left}
 .error_info .block{background:red;width:15px;margin-left: 5px;margin-right: 5px;}
 .error_info ul{line-height: 14px;}
  .error_info ul li{margin-top:10px; }
</style>
<body>
	 <div class="info">
	 	
	 </div>
	 <div style="text-align: center;">
		 <a class="Block input35 size13 white" href="${resource_path}${errorFilePath}">单击这里</a>
		 <a class="Block input35 size13 white"  href="javascript:void(0)" onClick="back()">返回</a>
	 </div>
      <div style="height:100px;width: 100%">

	 	<div class="error_info">
	 					<strong>导入文件错误：</strong>
	 					<ul >
	 						<li>1、详情请查看标记位置以及原因;</li>
	 						<li><span>2、用</span>
                       		<span class="block">&nbsp;</span>
                       		<span>标记文件出错位置,括号里的是出错原因。</span></li>
	 					</ul>
				    
				        
			
         </div>
	 </div>
	 <script type="text/javascript">
	    function back(){
	    	window.history.go(-1);
	    }
	 </script>
</body>
</html>