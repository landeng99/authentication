<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">
p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}


.mg18 {
    margin-bottom: 18px;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
}
.hr {
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.List06 {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.List06 li {
    height: 52px;
    line-height: 32px;
    list-style-type:none
}
.List06 li span.pull-left {
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: right;
    width: 63px;
}
.List06 li input {
    margin-right: 4px;
}
.List06 li .text08 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 178px;
}

.List06 li .text09 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 78px;
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 270px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button2.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 72px;
}

.span4 .label{
     width:100px;
     float: left;
}

</style>
<body>
<div style="background-image:none;border-right:none;width:350px;padding-top:5px;"/>
<input type="hidden" id="checkMobileResult" value="1">
<input type="hidden" id="mobile" value="${old_mobile}">
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
        <div>
            <div class="hr mg18"></div>
            <div style="display: block;" id="withdrawal">
                <ul class="List06">
                	  <li class="span4">
                         <p>
                            <span class="label">原绑定银行卡号：</span>
                            <span>${bank_card}</span>
                            <input type="hidden" value="${bank_card}"/>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label">新绑定银行卡号：</span>
                            <input type="text" class="text08" id="bankBind" name="bank_card" style="padding-left: 5px;" onblur="checkBank()">
                            <span style="color:Red;">*</span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label">开户人：</span>
                            <input type="text" class="text08" id="accountHolder" name="account_holder" style="padding-left: 5px;"onblur="checkHolder()">
                            <span style="color:Red;">*</span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label">开户银行：</span>
                            <select id="bankAccount" name="bankAccount" class="text08" style="padding-left: 5px;">
                              <option value="中国工商银行">中国工商银行</option>
                              <option value="中国建设银行">中国建设银行</option>
                              <option value="中国农业银行">中国农业银行</option>
                              <option value="中国银行">中国银行</option>
                              <option value="交通银行">交通银行</option>
                              <option value="招商银行">招商银行</option>
                              <option value="中国邮政储蓄">中国邮政储蓄</option>
                              <option value="中国光大银行">中国光大银行</option>
                              <option value="广发银行">广发银行</option>
                              <option value="上海浦东发展银行">上海浦东发展银行</option>
                              <option value="北京银行">北京银行</option>
                              <option value="中国民生银行">中国民生银行</option>
                              <option value="平安银行">平安银行</option>
                              <option value="中信银行">中信银行</option>
                            </select>
                            <span style="color:Red;">*</span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label">验证码：</span>
                            <input type="text" class="text09" id="code" name="code" style="padding-left: 5px;">
                            <span style="color:Red;">*</span>
                            <input type="button" value="获取验证码" style="width:100px;height:30px;" id="getCode" name="getCode"/>
                         </p>
                      </li>
                  </ul>
                  <div class="PushButton">
                       <a id="no"  class="button" style="cursor:pointer;">取消</a>
                       <a id="yes" class="button" style="cursor:pointer;">确定</a>
                  </div>
           </div>
      </div>
   </div>

    <script type="text/javascript">
    
    function checkBank(){
    	var reg=/^\d+$/;
		var bank_card=$("#bankBind").val();
		
		if(bank_card==null || bank_card.trim()==""){
            alert("银行卡号不能为空！");
            return;
        }
		
		if(reg.test(bank_card)==false){
			alert("银行卡号只能为数字");
			$('#bankBind').focus();
			return;
		}        
    }
    
    function checkHolder(){
    	var accountHolder=$("#accountHolder").val();
    	if(accountHolder==null || accountHolder.trim()==""){
            alert("开户人姓名不能为空");
            //$('#accountHolder').focus();
            return;
        }   	
    }
    
    function checkBankAccount(){
    	var bankAccount=$("#bankAccount").val();
    	if(bankAccount==null || bankAccount.trim()==""){
            alert("开户银行不能为空");
            return;
        } 
    }
    
    var second=120;
    var recover=function (){
        $("#getCode").removeAttr('disabled');
        $("#getCode").val('免费获取验证码');
        clearInterval(sh);
        clearInterval(timer_sh);
        //120秒之后重新设置为120
        second=120;
    };
    
  	//计时器开始
    var timer=function(){
        second=second-1;
        $("#getCode").val(second+" 获取中......");
    }
    var sh=null;
    var timer_sh=null;
    $("#getCode").click(function () {
    	var bank_card =$('#bankBind').val();
        var accountHolder=$("#accountHolder").val();
        var bankAccount=$("#bankAccount").val();
        var mobile=$('#mobile').val();
        if(bank_card==null || bank_card.trim()==""){
            alert("银行卡号不能为空！");
            $('#bankBind').focus();
            return;
        }
        
        if(accountHolder==null || accountHolder.trim()==""){
            alert("开户人姓名不能为空");
            return;
        }
    	
    	if(bankAccount==null || bankAccount.trim()==""){
            alert("开户银行不能为空");
            return;
        } 
             
        $(this).attr('disabled','disabled');
        $(this).val('120 获取中......');
        sh=setInterval(recover,120000);
        timer_sh=setInterval(timer,1000);

        $.ajax({
            url:'${mainServer}/sms/sendRegistCode',
            type:'post',
            data:{"mobile":mobile,"uncheckImgVC":"Y"},
            success:function(data){ 
            }
        });
    })

    //获取窗口索引
    var index = parent.layer.getFrameIndex(window.name);
    $("#no").click(function () {
        // 关闭窗口
        parent.layer.close(index);
    }); 

      $("#yes").click(function () {
    	  
    	  var reg=/^\d+$/;
    	  var accountHolder=$("#accountHolder").val();
          var bankAccount=$("#bankAccount").val();
          var bank_card =$('#bankBind').val();
          var code =$('#code').val();

          if(bank_card==null || bank_card.trim()==""){
              alert("银行卡号不能为空！");
              $('#bankBind').focus();
              return;
          }
         
  		  if(reg.test(bank_card)==false){
  			alert("银行卡号只能为数字!");
  			return;
  		  }
  		  
  		 if(accountHolder==null || accountHolder.trim()==""){
             alert("开户人姓名不能为空");
             return;
         }
     	
     	if(bankAccount==null || bankAccount.trim()==""){
             alert("开户银行不能为空");
             return;
         } 
     	
          if(code==null || code.trim()==""){
              alert("验证码不能为空！");
              $('#code').focus();
              return;
          }
          

          
          var url="${mainServer}/account/bankBind";
          $.ajax({
              url:url,
              data:{"bank_card":bank_card,"code":code,"bank_account":bankAccount,"account_holder":accountHolder},
              type:'post',
              async:false,
              dataType:'json',
              success:function(data){
                  alert(data.message);                  
                  if(data.result){
                	  var bank_card=data['bank_card'];
                	  //alert(newMobile);
                	  //将新的手机号码传给父页面
                	  //showNewMobile(newMobile);
                      // 标识点击了保存 给父窗口传值标识修改成功
                      parent.$('#returnCode').val(bank_card);
                      // 关闭窗口
                      parent.layer.close(index);
                  }
              }
          });
      }); 
</script>
</body>
</html>