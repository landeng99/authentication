<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="overflow: hidden;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
    <script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
	<script type="text/javascript" src="${resource_path}/js/jquery.nicescroll.min.js" charset="utf-8"></script>    
    <script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>      
<script type="text/javascript">
    $(function () {
        $("#one1").addClass("current");
        var defaultVal = $("#one1").attr("categoryid");
        $("#fistChar_" + defaultVal).show();
        $("#fistChar_" + defaultVal + " span.all").addClass("button-primary")
        $("span.pagebtn").click(function () {
            $("span.pagebtn").removeClass("button-primary");
            var index = $(this).attr("val");
            var start = (index - 1) * 5 + 1;
            var end = index * 5;
            $("a[name='ctipHeard']").hide().removeClass("current");
            for (var a = start; a <= end; a++) {
                $("a#one" + a).show();
            }
            $(this).addClass("button-primary");
            var categoryid = 0;
            $("a#one" + start).addClass("current");
            categoryid = $("a#one" + start).attr("categoryid");
            setTab('one', (index - 1) * 5 + 1, categoryid);
        });

        $("span.button-circle").click(function () {
            var pid = $(this).attr("parentid");
            var key = $(this).html();
            if (key == "All") {
                $(".second_" + pid + " a").show();
            } else {
                $(".second_" + pid + " a").hide();
                $(".second_" + pid + " a." + key).show();
            }
            $("span.button-circle").removeClass("button-primary");
            $(this).addClass("button-primary");
        });

        $("span.direction").mouseover(function () {
            $(this).css("color", "#0098db");
        }).mouseout(function () {
            $(this).css("color", "#666666");
        });

        $("a.smallCategory").mouseover(function () {
            $(this).css("color", "#0098db");
        }).mouseout(function () {
            $(this).css("color", "#666666");
        });
    });
    function leftright(num) {
        var first = "";
        var last = "";
        $("a[name='ctipHeard']").each(function () {
            //            if ($(this).attr("style") != "display: none;" && first == "") {
            //                first = $(this).attr("ID").replace("one", "");
            //            }
            //            if ($(this).attr("style") != "display: none;") {
            //                last = $(this).attr("ID").replace("one", "");
            //            }
            if ($(this).is(":visible") && first == "") {
                first = $(this).attr("ID").replace("one", "");
            }
            if ($(this).is(":visible")) {
                last = $(this).attr("ID").replace("one", "");
            }
        });
        $("span.pagebtn").removeClass("button-primary");
        var nowPage = 0;
        if (num == -1) {
            //==================上一页=============
            nowPage = (first - 1) / 5;
            if (nowPage == 0) {
                nowPage = 1;
            }
        } else {
            nowPage = ((last) / 5) + 1;
            if (nowPage == 0) {
                nowPage = 1;
            }
            //if (nowPage >= parseInt('4')) {
            //    nowPage = parseInt('4');
            //}
        }

        $("span.pagebtn").each(function () {
            if ($(this).attr("val") == nowPage) {
                $(this).addClass("button-primary");
                $(this).click();
            }
        });
        //不能再向左了
        if (first == 1 && num == -1){
			return;
		}
        //不能再向右了
        if (last == $("a[name='ctipHeard']").length && num == 1){
			return;
		}
            
    }

    function setTab(name, cursel,catagoryID) {
         
        var n = $("a[name='ctipHeard']").length;
        for (i = 1; i <= n; i++) {
            var menu = $("#"+name + i);
            var con = $("#con_" + name + "_" + i);
            //menu.className = i == cursel ? "hover" : "";
			if(i == cursel){
				menu.attr("class","hover");
			}else{
				menu.attr("class","");
			}
			if(i == cursel){
				con.css("display","block");
			}else{
				con.css("display","none");
			}
            //con.style.display = i == cursel ? "block" : "none";
        }
        $("a.firstCategory").removeClass("current");
        $("#one" + cursel).addClass("current");
        $(".second_" + catagoryID + " a").show();
        $("span.button-circle").removeClass("button-primary");
        $("#fistChar_" + catagoryID).show();
        $("#fistChar_" + catagoryID + " span.all").addClass("button-primary");
    }

    function choose(id, name, taxs, remarkmsg, tip, check, service, producttip) {
		var ID=${ID};
        window.parent.SetCatalog(id, name, taxs, getUrlParam("ID"), remarkmsg, tip, check, service, producttip);
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }

    function Close() {
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }

    function getUrlParam(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象

        var r = window.location.search.substr(1).match(reg);  //匹配目标参数

        if (r != null) return unescape(r[2]); 
		return null; //返回参数值

    } 

</script> 

<style type="text/css">
*{margin:0; padding:0;}
ul,li{ list-style:none;}
.main{ width:1400px; border:1px solid #ccc; float:left;font-size:12px;}
.main ul{ float:left; height:28px; line-height:28px; border-bottom:1px solid #ccc;width:1400px;}
.main ul li{ float:left; width:80px; text-align:center; border-right:1px solid #ccc; }
.main ul li.hover{ color:red; font-weight:900;}
.mainbox{float:left;width:400px;}
.subbox{ padding:0 12px 12px 12px;}
.subbox a{    border-left: 1px solid #CCCCCC;
    float: left;
    height: 20px;
    line-height: 16px;
    margin: 6px 0;
    padding: 0 8px;}
</style>

 <style>
.ui-autocomplete-category {
font-weight: bold;
padding: .2em .4em;
margin: .8em 0 .2em;
line-height: 1.5;
}
.ui-helper-hidden-accessible
{
    display:none;
    }
.ac_results
{
border: 1px solid #8F8FBD;
    background: none repeat scroll 0 0 #FFFFFF;
    overflow: hidden; 
    position: absolute;
    text-align: left;
    top: 84px;
    width: 262px; 
    z-index: 9999;
    padding:10px;
}
.ac_results li
{
 line-height:22px;
}

 .ac_over
 {
    background: none repeat scroll 0 0 #cbe6f7;
    display: inline-block;
    text-decoration:none;
    width:100%;
    cursor:pointer;
 }
#selectItemCount {
	overflow: auto;
	height: 200px;
}
.text{display:block;float:left;}
.triangle-address {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #CDCDCD;
    border-radius: 5px;
    color: #333333;
    padding:5px;
    position: relative;
	box-shadow:-2px 0px 8px #e6e6e6,0px -2px 8px #e6e6e6,0px 2px 8px #e6e6e6,2px 0px 8px #e6e6e6;
}

.triangle-address:before {
    border-color: #CDCDCD rgba(0, 0, 0, 0);
    border-style: solid;
    border-width: 9px 9px 0;
    bottom: -9px;
    content: "";
    display: block;
    left: 90px;
    position: absolute;
    width: 0;
}
.triangle-address:after {
    border-color: #FFFFFF rgba(0, 0, 0, 0);
    border-style: solid;
    border-width: 8px 8px 0;
    bottom: -7px;
    content: "";
    display: block;
    left: 91px;
    position: absolute;
    width: 0;
}

</style>

</head>

<body>    
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount1.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/button.css">
<div class="selectItemhidden radius3" id="selectItem" style="display:block;border:0;"> 
          <div style="display:inline;width:850px;float:left;"> 

     <div id="Search" style="margin-left:180px;height: 58px;width: 100%;">          
	  <div class="selectItemright" id="selectItemClose" style="margin-right:180px;display:inline;width:30px;float:right;" onclick="Close();"><img src="${resource_path}/img/icon66.png" alt="soe"></div>
     </div>
     </div>
	 <div class="selectItemtit" id="selectItemAd" style="display:inline;width:850px;float:left;"> 
        <div style="width:720px;height:40px;line-height:40px;float:left;"> 
        <div style="display:inline;width:35px;float:left;"><span onclick="leftright(-1);" style="padding:0 0 0 5px;margin:0;width:30px;display:block;cursor:pointer;" class="direction">&lt;&lt;</span></div>
        <div style="width:650px;height:40px;line-height:40px;float:left;">
			<c:forEach var="parentType"  items="${parentTypeList}" varStatus="nav">
				 <a id="one${nav.count}" href="#" categoryid="${parentType.goodsTypeId}" class="" onmouseover="setTab('one',${nav.count},${parentType.goodsTypeId})" name="ctipHeard"  <c:if test="${nav.count>5}">style="display: none;"</c:if>>
					<div style="height: 40px; line-height: 40px; margin: 0px auto; width: 105px;" class="f_div">
					<span class="text">${parentType.nameSpecs}</span>
					</div>
				</a>
			</c:forEach>    
		</div>
        <div style="display:inline;width:35px;float:right;"><span onclick="leftright(1);" style="padding: 0px; margin: 0px; width: 30px; display: block; cursor: pointer; color: rgb(102, 102, 102);" class="direction">&gt;&gt;</span></div>
        </div>
        <div style="width:130px;height:40px;line-height:40px;float:left;text-align:right;display:none;">
            <span class="button  pagebtn button-primary" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="1">1</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="2">2</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="3">3</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="4">4</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="5">5</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="6">6</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="7">7</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="8">8</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="9">9</span>
			<span class="button pagebtn" style="float:left;margin-left:5px;margin-top: 10px;cursor:pointer;" val="10">10</span>
        </div>
	 </div> 
      <div class="selectItemcont" id="selectItemCount" style="overflow: hidden; outline: none;" tabindex="5000"> 
      <c:forEach var="parent"  items="${parentTypeList}" varStatus="fds">
		<div class="subbox second_${parent.goodsTypeId}" <c:if test="${fds.count==1}">style="display: block;"</c:if> <c:if test="${fds.count!=1}">style="display: none;"</c:if>id="con_one_${fds.count}">
			<c:forEach var="child"  items="${childTypeList}">
				<c:if test="${child.parentId==parent.goodsTypeId}">
				<a class="smallCategory" href="javascript:choose(${child.goodsTypeId},'${child.nameSpecs}',0.10,'','','0','0','请备注品牌与规格')">${child.nameSpecs}&nbsp;</a>
				</c:if>
			</c:forEach> 
		</div>	
	  </c:forEach> 
        
	 </div> 
</div>
<script type="text/javascript">
    $(function () {
        $("html").niceScroll({ styler: "fb", cursorcolor: "#000", cursoropacitymin: 1, cursoropacitymax: 1, autohidemode: false });
        $("#selectItemCount").niceScroll({
            autohidemode: false,
            cursorwidth: '6px'
        });

        $("div.f_div").each(function () {
            if ($(this).find("span.tip").length > 0) {
                $(this).css("width", $(this).find("span.text").html().length * 15 + 20);
            } else {
                $(this).css("width", $(this).find("span.text").html().length * 15);
            }
        });
    });
    function pageX(elem) {
        return elem.offsetParent ? (elem.offsetLeft + pageX(elem.offsetParent)) : elem.offsetLeft;
    }

    function pageY(elem) {
        return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
    }
</script>
</body></html>