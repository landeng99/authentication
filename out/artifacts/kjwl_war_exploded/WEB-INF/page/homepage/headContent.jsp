<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div class="middle-head">
	<input type="hidden" id="returnCode" />
	<div class="middle-head-fi">
		<ul><li  class="pull-left">${frontUser.user_name}</li><li style="margin-left:10px;" class="pull-left"><a href="${mainServer}/account/accountInit" title="账户设置"><img src="${resource_path}/img/is1.jpg" alt="账户设置"/></a></li><li class="pull-right blue"><c:if test="${frontUser.user_type==1}"><!-- 散客 --></c:if><c:if test="${frontUser.user_type==2}">同行</c:if></li></ul>
		<ul class="clear"><li  class="pull-left">${frontUser.email}</li><li  class="pull-right email-status"><a href="#" class="blue"><c:if test="${frontUser.status==0}">未激活</c:if><c:if test="${frontUser.status==1}">正常</c:if><c:if test="${frontUser.status==2}">禁用</c:if></a></li></ul>
		<ul class="clear"><li  class="pull-left" id="mobile">${frontUser.mobile}</li><li  class="pull-right"><a href="javascript:;" class="blue" id="editMobileHead">修改</a></li></ul>
		<ul class="clear"><li  class="pull-left orange size16"><fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#,###.##"/></li><li  class="pull-right"><a href="javascript:;" class="blue" id="charge">充值</a></li></ul>
	</div>
	<div  class="middle-head-se">
		<div>
						<ul>
							<li class="middle-head-se-lifi"><a
								href="javascript:showAllTransport()"
								<c:if test="${status==null}">class="blue"</c:if>>全部包裹</a></li>
							<li class="middle-head-se-lise"><a
								href="javascript:showTransportStatus(2)"
								<c:if test="${status==2}">class="blue"</c:if>>待发货</a></li>
							<li class="middle-head-se-lith"><a
								href="javascript:showTransportStatus(3)"
								<c:if test="${status==3}">class="blue"</c:if>>转运中</a></li>
							<li class="middle-head-se-litcl"><a
								href="javascript:showTransportStatus(-1)"
								<c:if test="${status==-1}">class="blue"</c:if>>待处理</a><c:if test="${neetToHandleCount>0}"><span style="font-size: smaller;color: greenyellow;line-height: 0;vertical-align: top;">new</span></c:if></li>
							<li class="middle-head-se-lidk"><a
								href="javascript:showTransportStatus(-2)"
								<c:if test="${status==-2}">class="blue"</c:if>>已到库</a></li>
							<li class="middle-head-se-lifo"><a
								href="javascript:showTransportStatus(9)"
								<c:if test="${status==9}">class="blue"</c:if>>已签收</a></li>

						</ul>
		</div>
		<div class="border-bott"></div>
		<div class="middle-head-se-fi">
			<ul class="middle-head-se-fi-ul">
				<li class="middle-head-se-fi-ul-lifi"><a href="javascript:showTransportStatus(0)" <c:if test="${status==0}">class="blue"</c:if>>待入库<span>${c1}</span></a></li>
				<li class="middle-head-se-fi-ul-lise"><a href="javascript:showTransportStatus(1)" <c:if test="${status==1}">class="blue"</c:if>>已入库<span>${c2}</span></a></li>
				<li class="middle-head-se-fi-ul-lith"><a href="javascript:showTransportPayStatus(1)" <c:if test="${payStatus==1}">class="blue"</c:if>>未支付<span>${c3}</span></a></li>
				<li class="middle-head-se-fi-ul-lifo"><a href="javascript:showTransportPayStatus(2)" <c:if test="${payStatus==2}">class="blue"</c:if>>已支付<span>${c4}</span></a></li>
				<c:if test="${frontUser.user_type==2}">
				<li class="middle-head-se-fi-ul-lith"><a href="javascript:showTransportPkgPrint(0)" <c:if test="${pkgPrint==0}">class="blue"</c:if>>未打印<span>${c5}</span></a></li>
				<li class="middle-head-se-fi-ul-lifo"><a href="javascript:showTransportPkgPrint(1)" <c:if test="${pkgPrint==1}">class="blue"</c:if>>已打印<span>${c6}</span></a></li>
				</c:if>	
			</ul>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$("#editMobileHead").click(function () {
        var height = ($(window).height() - 320) / 2;
        $.layer({
            title :'修改手机号',
            type: 2,
            fix: true,
            shade: [0.5, '#ccc', true],
            border: [0, 0.3, '#666', true],
            offset: [height + 'px', ''],
            area: ['400px', '330px',],
            iframe: { src: '${mainServer}/account/editMobileInit' },
            end : function(index) {
                // 点击保存的时候 刷新手机号码
                if ($('#returnCode').val() != "") {
                    $("#mobile").html($('#returnCode').val());
                }
            }
        });
    });
});
</script>