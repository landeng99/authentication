<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0021)http://www.birdex.cn/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<%--
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syproblem.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syembgo.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="${resource_path}/css/uploadify.css"/>
 --%>
<link href="${resource_path}/img/favicon.ico" media="screen" rel="shortcut icon" type="image/x-icon" />

<script src="${backJsFile}/json2.js"></script>
<script type="text/javascript">
	var mainServer = '${mainServer}';
	var backServer = '${backServer}';
	var jsFileServer = '${backJsFile}';
	var cssFileServer = '${backCssFile}';
	var imgFileServer = '${backImgFile}';
	var uploadPath = '${uploadPath}';
	
	function goPAGE() {
	if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
		window.location.href="/mid";
}

}
	goPAGE();

</script>
<%-- <script type="text/javascript" src="${resource_path}/js/layer.min.js"></script> --%>
</head>
<body>

<jsp:include page="topIndexPage.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/dropify/dist/css/dropify.min.css"></link>
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/id-upload.css"></link>
<%--
<div>
	<div class="embargo">
		<div class="page-heading">
            <p class="page-heading-pf">
                身份信息上传</p>
            <p class="page-heading-ps">
                1.证件照只供海关清关使用，请上传真实有效的证件图片
2.所上传身份证人名要与收件人名一致<br />
3.请将身份证正面与反面单独分开上传
4.若自己添加水印，请勿遮挡身份证中信息以及头像<br />
5.请保持四角齐全，尽量清晰，不要有反光，缺损
6.图片格式为jpg或jpeg或png，大小建议控制在600KB以内</p>
        </div>
		 <ul class="list-pic-left" style="display:block;">
         <div style="float:left; margin-left: 250px;">
		 <form action="${mainServer}/homepkg/idcardUploadHandle" id="id_card_form" method="post" style >
			<table style="border-spacing:15px;">
			<tr>
            （运单号与手机号选填一项即可，若填写运单号，系统会自动匹配相应的手机号）<br />
			<td style="font-size:16px; ">运单号（选填一项）：</td>
			<td><input type="text" style="height:28px; border:solid 1px #ccc; border-radius:2px; padding-left:5px;" id="logistics_code" name="logistics_code" size="30" onblur="logistics_code_keyUp();"/></td>
			</tr>
            
			<tr>
			<td style="font-size:16px; ">手机号（选填一项）：</td>
			<td><input type="text"  style="height:28px; border:solid 1px #ccc; border-radius:2px; padding-left:5px;" id="mobile" name="mobile" size="30"/></td>
			</tr>
            
			<tr>
			<td style="font-size:16px;">姓名（必填）：</td>
			<td><input type="text" style="height:28px; border:solid 1px #ccc; border-radius:2px; padding-left:5px;" id="real_name" name="real_name" size="30"/></td>
			</tr>
            
			<tr>
			<td style="font-size:16px;">身份证号（必填）：</td>
			<td><input type="text" style="height:28px; border:solid 1px #ccc; border-radius:2px; padding-left:5px;" id="id_card_num" name="id_card_num" size="30"/></td>
			</tr>
            
			<tr>
			<td>
			<input type="hidden" id="id_card_face" name="id_card_face" value=""/>
			<img id="id_card_df_face" src="${resource_path}/img/id_card_df_face.png" onclick="onFaceUploadClick();" width="106" height="106" alt="身份证正面"/></td>
			<td>
			<input type="hidden" id="id_card_back" name="id_card_back" value=""/>
			<img id="id_card_df_back" src="${resource_path}/img/id_card_df_face.png" onclick="onBackUploadClick();" width="106" height="106" alt="身份证反面"/></td>
			</tr>
            
			<tr>
			<td style="padding-left:10%">正面</td>
			<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;反面</td>
			</tr>
            
			<tr>
			<td><input type="button" name="upload" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;上传&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" size="20" onclick="submitUpload();"/></td>
			<td><input type="button" name="reset" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;重置&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" onclick="resetHandle();"/></td>
			</tr>
            
			</table>
			  </form>
         </div>

			
        </ul>
      
	</div>
</div>
<br />
<br />
<br />

			<div class="leightbox1 modal-dialog" id="uploadImage_div_face_dialog"
				style="background-image: url('');">
				<a href="javascript:;"
					class="lbAction modal-dialog-title-close close" rel="deactivate"
					style="background: transparent url(${resource_path}/img/close-x.png) no-repeat scroll;background-position: 25% 25%;"
					id="uploadImage_div_face_dialog_close"></a>
				<div class="Whole" style="width: 100%;">
						<div id="uploadImage_div_face" style="display:none"></div>
				</div>
				<ul style="width: 300px;">
				<li>1.证件照只供海关清关使用，请上传真实有效的证件图片</li>
				<li>2.所上传身份证人名要与收件人名一致</li>
				<li>3.请将身份证正面与反面单独分开上传</li>
				<li>4.若自己添加水印，请勿遮挡身份证中信息以及头像</li>
				<li>5.请保持四角齐全，尽量清晰，不要有反光，缺损</li>
				<li>6.图片格式为jpg或jpeg或png，大小建议控制在600KB以内</li>
				</ul>
			</div>


			<div class="leightbox1 modal-dialog" id="uploadImage_div_back_dialog"
				style="background-image: url('');">
				<a href="javascript:;"
					class="lbAction modal-dialog-title-close close" rel="deactivate"
					style="background: transparent url(${resource_path}/img/close-x.png) no-repeat scroll;background-position: 25% 25%;"
					id="uploadImage_div_back_dialog_close"></a>
				<div class="Whole" style="width: 100%;">
						<div id="uploadImage_div_back" style="display:none"></div>
				</div>
				<ul style="width: 300px;">
				<li>1.证件照只供海关清关使用，请上传真实有效的证件图片</li>
				<li>2.所上传身份证人名要与收件人名一致</li>
				<li>3.请将身份证正面与反面单独分开上传</li>
				<li>4.若自己添加水印，请勿遮挡身份证中信息以及头像</li>
				<li>5.请保持四角齐全，尽量清晰，不要有反光，缺损</li>
				<li>6.图片格式为jpg或jpeg或png，大小建议控制在600KB以内</li>
				</ul>
			</div>
			 --%>

    <div class="min-banner">
        <div class="bgImg"></div>
        <div class="logo"></div>
    </div>
    
    <div class="wrap-box container-fluid">
        <div class="idbox row">
            <div class="col-sm-5 ">
                <h4 class="tit">身份信息填写</h4>
                <form class="form-horizontal" action="${mainServer}/homepkg/idcardUploadHandle" id="id_card_form" method="post">
                    <div class="form-group clearfix mb-lg">
                        <div class="col-sm-6">
                            <label  class="control-label">运单号</label>
                            <input type="text" class="form-control " id="logistics_code" name="logistics_code" placeholder="输入运单号" />
                        	<span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请输入正确的运单号。</span>
                        </div>
                        <div class="col-sm-6">
                            <label  class="control-label">手机号</label>
                            <input type="text" class="form-control " id="mobile" name="mobile" placeholder="输入手机号" />
                        	<span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请输入手机号。</span>
                        </div>
                    </div>
                    <div class="form-group col-sm-12 mb-lg">
                        <label  class="control-label"><span class="text-danger">*</span>姓名</label>
                        <input type="text" class="form-control " id="real_name" name="real_name" placeholder="输入姓名" />
                        <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请输入姓名。</span>
                    </div>
                    <div class="form-group col-sm-12 mb-lg">
                        <label  class="control-label"><span class="text-danger">*</span>身份证号</label>
                        <input type="text" class="form-control " id="id_card_num" name="id_card_num" placeholder="输入身份证号" />
                        <span class="help-block hidden"><i class="glyphicon glyphicon-info-sign pr5"></i>请输入身份证号。</span>
                    </div>
                </form>
            </div>
            <div class="col-sm-6 col-sm-offset-1">
                <h4 class="tit">身份证照片上传</h4>
                <form class="form-horizontal">
                    <div class="form-group clearfix ">
                        <label for="" class=" control-label col-sm-12"><span class="text-danger">*</span>证件示例：</label>
                        <div class="col-sm-6">
                          
                            <p><img src="${resource_path}/images/id-z.jpg"></p>
                        </div>
                        <div class="col-sm-6">
                           
                             <p><img src="${resource_path}/images/id-f.jpg"></p>
                        </div>
                    </div>
                    <div class="form-group">
                       <label for="" class=" control-label col-sm-12"><span class="text-danger">*</span>证件照片：</label>
                      
                       <div class="col-sm-6">
                         <input id="id_card_df_face" class="dropify" type="file" data-max-file-size="1M" />
                         <span class="help-block "><i class="glyphicon glyphicon-info-sign pr5"></i>身份证正面照</span>
                       </div>
                       <div class="col-sm-6 ">
                          <input id="id_card_df_back" class="dropify" type="file" data-max-file-size="1M" />
                          <span class="help-block "><i class="glyphicon glyphicon-info-sign pr5"></i>身份证反面照</span>
                       </div>
					   <input type="hidden" id="id_card_face" name="id_card_face" value=""/>
					   <input type="hidden" id="id_card_back" name="id_card_back" value=""/>
                    </div>
                </form>
            </div>
        </div>
        <div class="text-center idbtn">
            <a href="#" class="btn btn-primary btn-p-radius" onclick="submitUpload();">提交</a>
        </div>
    </div>

    <!-- 身份证上传失败 -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document" style="width: 400px;">
        <div class="modal-content">
          
          <div class="modal-body  ">
            <p class="m20 text-center text-danger" style="font-size: 20px;" id="err_p"><i class="glyphicon glyphicon-remove pr10"></i>身份证上传失败</p>
          </div>
         <div class="modal-footer">
             <div class="text-center">
             <button type="button" class="btn btn-default btn-sm btn-radius" data-dismiss="modal">确定</button>
             </div>
         </div>
        </div>
      </div>
    </div>
<jsp:include page="footer-1.jsp"></jsp:include>
<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="${resource_path}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<!-- 图片上传 -->
<script type="text/javascript" src="${resource_path}/new_css/dropify/dist/js/dropify.js"></script>
<%-- <script type="text/javascript" src="${resource_path}/new_css/dropify/dist/js/dropify.min.js"></script> --%>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<script type="text/javascript" src="${resource_path}/js/uploadify/swfobject.js"></script>
<script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    // Translated
    $('.dropify').dropify({
        messages: {
            default: '点击或拖拽文件到这里上传',
            replace: '点击或拖拽文件到这里来替换文件',
            remove:  '移除文件',
            error:   '对不起，你上传的文件太大了'
        }
    });

    // Used events
    var drEventFace = $('#id_card_df_face').dropify();
    drEventFace.on('dropify.errors', function(event, element){
    	alert('程序出错');
    });
    drEventFace.on('dropify.fileReady', function(event, previewable, src, file){
    	var fileStr = src.substr(src.lastIndexOf(',')+1);
    	$.ajax({
    		url:'${mainServer}/uploadIdcardImageNew',
    		type:'post',
    		data:{fileStr:fileStr,fileName:file.name},
    		success:function(data){
    			$('#id_card_face').val(data);
    		}
    	});
    });
    var drEventBack = $('#id_card_df_back').dropify();
    drEventBack.on('dropify.errors', function(event, element){
    	alert('程序出错');
    });
    drEventBack.on('dropify.fileReady', function(event, previewable, src, file){
    	var fileStr = src.substr(src.lastIndexOf(',')+1);
    	$.ajax({
    		url:'${mainServer}/uploadIdcardImageNew',
    		type:'post',
    		data:{fileStr:fileStr,fileName:file.name},
    		success:function(data){
    			$('#id_card_back').val(data);
    		}
    	});
    });
    
    $('form.form-horizontal').on('blur', 'input.form-control', function(){
		var val=$(this).val().trim();
		$(this).val(val);
		switch ($(this).prop('id')) {
		case 'real_name':
			if(val==""){
				inputErr(this);
				return;
			}
			inputOk(this);
			break;
		case 'id_card_num':
			if(val==""){
		    	$(this).siblings('span.help-block').html('<small>请输入身份证号。</small>');
				inputErr(this);
				return;
			}
			var regIdCard=/^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
		    if(!(regIdCard.test(val))){
		    	$(this).siblings('span.help-block').html('<small>身份证号码有误，请重填！</small>');
		    	inputErr(this);
		    	return;
		    }
			inputOk(this);
			break;
		case 'mobile':
			if(val==""){
		    	$(this).siblings('span.help-block').html('<small>请输入手机号。</small>');
				inputErr(this);
				return;
			}
			if(!(/^1[3|4|5|7|8]\d{9}$/.test(val))){
		    	$(this).siblings('span.help-block').html('<small>手机号码有误，请重填！</small>');
		    	inputErr(this);
		    	return;
		    }
			inputOk(this);
			break;
		case 'logistics_code':
			logistics_code_keyUp(this);
			break;
		default:
			break;
		}
	});
});

function inputErr(obj){
	$(obj).parent().removeClass('has-success');
	$(obj).parent().addClass('has-error');
	$(obj).siblings('span.help-block').removeClass('hidden');
}

function inputOk(obj){
	$(obj).parent().removeClass('has-error');
	$(obj).siblings('span.help-block').addClass('hidden');
	$(obj).parent().addClass('has-success');
}

function submitUpload()
{
	var faceImgPath=$("#id_card_face").val();
	var backImgPath=$("#id_card_back").val();
	var mobile=$("#mobile").val();
	var real_name=$("#real_name").val();
	var id_card_num=$("#id_card_num").val();

 	if(mobile==null||mobile=='') {
 		$("#mobile").blur();
		return false;
	}
    if(!(/^1[3|4|5|7|8]\d{9}$/.test(mobile))){
 		$("#mobile").blur();
        return false; 
    }
	if(real_name==null||real_name=='') {
		$("#real_name").blur();
		return false;
	}
	if(id_card_num==null||id_card_num=='') {
		$("#id_card_num").blur();
		return false;
	}
	var regIdCard=/^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
    if(!(regIdCard.test(id_card_num))){
		$("#id_card_num").blur();
        return false; 
    }
	if(faceImgPath==null||faceImgPath==''||backImgPath==null||backImgPath=='') {
		$('#err_p').html('<i class="glyphicon glyphicon-remove pr10"></i>请上传身份证照片（正反面都需要上传）');
		$('#myModal2').modal('show');
		return false;
	}
	var canSubmit=false;
	$.ajax({
		url:'${mainServer}/homepkg/validByMobileReceiver',
		type:'post',
		data:{i_mobile:mobile,i_real_name:real_name},
		dataType:"json",
		async:false,
		success:function(data){
			if("S" == data.result){
				canSubmit=true;
			} else {
				canSubmit=false;
				$('#err_p').html('<i class="glyphicon glyphicon-remove pr10"></i>输入信息有误，手机号码、姓名不匹配，请重新核对。');
				$('#myModal2').modal('show');
			}
		}			
	});
	if(canSubmit) {
		$("#id_card_form").submit();
	}
}

function resetHandle()
{
	$("#logistics_code").val('');
	$("#mobile").val('');
	$("#real_name").val('');
	$("#id_card_num").val('');
	$("#id_card_face").val('');
	$("#id_card_back").val('');
	$("#id_card_df_face").attr("src","${resource_path}/img/id_card_df_face.png");
	$("#id_card_df_back").attr("src","${resource_path}/img/id_card_df_back.png");
}

function logistics_code_keyUp(obj) {
	var v_logistics_code=$(obj).val();
	if(v_logistics_code==null||v_logistics_code==''){
		inputOk(obj);
		return false;
	}
	$.ajax({
		url:'${mainServer}/homepkg/queryMobileNumByLogisticsCode',
		type:'post',
		data:{logistics_code:v_logistics_code},
		dataType:"json",
		async:false,
		success:function(data){
			if("S" == data.result){
				$("#mobile").val(data.mobile);
				$("#mobile").blur();
				inputOk(obj);
			} else {
				inputErr(obj);
			}
		}			
	});
}
</script>
</body>
</html>