<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<head>

<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<!-- 
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syproblem.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syembgo.css" rel="stylesheet" type="text/css"/>
 -->
<link href="${resource_path}/img/favicon.ico" media="screen" rel="shortcut icon" type="image/x-icon" />
<script type="text/javascript">
	var mainServer = '${mainServer}';
	var backServer = '${backServer}';
	var jsFileServer = '${backJsFile}';
	var cssFileServer = '${backCssFile}';
	var imgFileServer = '${backImgFile}';
	var uploadPath = '${uploadPath}';
</script>
</head>
<body>

<jsp:include page="topIndexPage.jsp"></jsp:include>
    <div class="min-banner">
        <div class="bgImg"></div>
        <div class="logo"></div>
    </div>
	<div class="admin">
		<div style="height:500px;margin-top:200px;">
			<div style="color:#F00;font-size:35px;text-align: center;">身份证上传成功！<img alt="身份证上传成功！" src="${backImgFile}/ok.jpg"></div>
			<div class="text-center " style="margin-top: 200px;">
			    <a href="${mainServer}" class="btn btn-primary" data-toggle="modal">返回首页</a>
			    <a href="${mainServer}/id" class="btn btn-primary" data-toggle="modal">继续上传身份证</a>
			</div>
		</div>
	</div>
</body>
</html>