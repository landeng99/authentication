<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>速8快递我的包裹</title>
   
    
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
	<script src="${resource_path}/js/layer.min.js"></script>
	<script src="${resource_path}/js/common.js"></script>
	<script src="${backJsFile}/My97DatePicker/WdatePicker.js"></script>
	<script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
	<link rel="stylesheet" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" href="${resource_path}/css/common.css">
	<style type="text/css">
	.opbtn{
	height: 24px;
    border: 1px solid #116ea7;
    background: transparent;
    color: #106da6;
    margin-right: 3px;
	}
	</style>
	<script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script>
<script type="text/javascript">
	function checkBoxClick(checkBoxElem,v_package_id,package_status)
	{
		var handle_flag=false;
		var needEdit=false;
		if(package_status==0)
		{
			$.ajax({
				url:'${mainServer}/homepkg/packageMergeSplitCheck',
				type:'post',
				data:{packageIds:v_package_id,checkException:'N'},
				dataType:"json",
				async:false,
				success:function(data){
					if("S" == data.result){
						handle_flag=true;
					}
					if("Y" ==data.needEdit)
					{
						needEdit=true;
					}
				}			
			});
		}
		var v_waitting_merge_flag="";
		if(handle_flag)
		{
			if(/checked/ig.test(checkBoxElem.className))
			{
				checkBoxElem.className=checkBoxElem.className.replace('checked','');
				v_waitting_merge_flag="N";
			}else
			{
				checkBoxElem.className=(checkBoxElem.className+' checked');
				v_waitting_merge_flag="Y";
			}
		}else
		{
			if(needEdit)
			{
				alert("包裹信息不完整，无法进行分箱合箱");
			}else
			{
				alert("您所选择包裹已进行分箱｜合箱服务，不能再选择待合箱服务!");
			}
		}
		$("#waitting_merge_flag_div").html("<input type=\"hidden\" id=\"waitting_merge_flag"+v_package_id+"\" value=\""+v_waitting_merge_flag+"\"/>");
	}
</script>
</head>
<body class="bg-f1">
<jsp:include page="header-3.jsp"></jsp:include>
<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="${mainServer}/personalCenter"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li class="active"><a href="${mainServer}/transport"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="${mainServer}/warehouse"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="${mainServer}/useraddr/destination"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="${mainServer}/member/record"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="${mainServer}/account/accountInit"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="${mainServer}/account/couponInit"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
         </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
		<div id="waitting_merge_flag_div"></div>
        <div class="column mt25 p20">
            <div class="btn-list">
                <p class="upload-ID floatR">
                    <button class="btn btn-orange" onClick="uploadId()"><i class="icon iconfont icon-nanrenzhuanhuan01"></i>上传身份证</button>
                </p>
                <p class="btn-merge">
                    <button class="btn btn-primary" onClick="newtransp(1)"><i class="icon iconfont icon-zhaohuo2press"></i>新增包裹</button>
                    <button class="btn btn-black" id="importPkg">导入包裹</button>
                    <button class="btn btn-black" onClick="showExportPackageWindow()">导出包裹</button>
                </p>
            </div>
			<div class="leightbox2 modal-dialog" id="exportPkgWinId" style="background-image: url('');width:550px;">
				<a href="javascript:;"
					class="lbAction modal-dialog-title-close close" rel="deactivate"
					style="background: transparent url(${resource_path}/img/close-x.png) no-repeat scroll;background-position: 25% 25%;"
					id="closeExportPkgWinId"></a>
				<div class="Whole" style="width: 100%;">
					<h1 class="blue">
						<span style="float: left;">请导出时间段（不选默认导出全部）：</span>
					</h1>
					<div class="Search1">
						从<input type="text" class="input" id="fromDate" name="fromDate"
							style="width: 200px"
							onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							value="${params.fromDate}"
							onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
							onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
							&nbsp;&nbsp;到 <input type="text" class="input" id="toDate"
							name="toDate" style="width: 200px"
							onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
							value="${params.toDate}"
							onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
							onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
					</div>
					<div class="PushButton"
						style="width: 120px; padding-top: 10px; padding-left: 250px;">
						<a class="Block input14 white" href="javascript:exportPkgConfirm()">导出包裹</a>
					</div>
				</div>
			</div>
			<form id="updatePkg" name="updatePkg">								
            <div class="package-tab">
                <ul class="nav nav-tabs">
                    <li class="nvopt <c:if test="${status==null}">active</c:if>"><a href="javascript:showAllTransport()">全部包裹</a></li>
                    <li class="nvopt <c:if test="${status==0}">active</c:if>"><a href="javascript:showTransportStatus(0)" >待入库</a></li>
                    <li class="nvopt <c:if test="${status==1}">active</c:if>"><a href="javascript:showTransportStatus(1)" >已入库</a></li>
                    <li class="nvopt <c:if test="${status==2}">active</c:if>"><a href="javascript:showTransportStatus(2)" >待发货</a></li>
                    <li class="nvopt <c:if test="${status==-1}">active</c:if>"><a href="javascript:showTransportStatus(-1)">待处理</a></li>
                    <li class="nvopt <c:if test="${status==3}">active</c:if>"><a href="javascript:showTransportStatus(3)">转运中</a></li>
                    <li class="nvopt <c:if test="${status==9}">active</c:if>"><a href="javascript:showTransportStatus(9)">已签收</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab01">
                        <div class="package-search pt30 pb20">
                            <p class="package-search-detail floatR">
                                <input type="text" placeholder="请输入运单号/关联号/收件人/手机号" id="SearchKey">
                                <a style="width: 64px;height: 30px;" class="btn btn-black" onClick="ShowData();"><i class="icon iconfont icon-sousuo-liebiao"></i>搜索</a>
                            </p>
                            <p class="package-search-select">
                                <label>筛选：</label>
                                <select onChange="showTransportPkgPrint()" id="prntst">
                                    <option value="-1">请选择打印状态</option>
									<option value="0" <c:if test="${pkgPrint==0}">selected</c:if>>未打印</option>
									<option value="1" <c:if test="${pkgPrint==1}">selected</c:if>>已打印</option>
                                </select>
                                <select onChange="showTransportPayStatus()" id="payst">
                                    <option value="-1">请选择支付状态</option>
									<option value="1" <c:if test="${payStatus==1}">selected</c:if>>未支付</option>
									<option value="2" <c:if test="${payStatus==2}">selected</c:if>>已支付</option>
                                </select>
                            </p>
                        </div>
						<input type="hidden" value="${frontUser.user_type}" id="userType" name="userType" />
						<input type="hidden" id="statusId" value="${status}" /> 
						<input type="hidden" id="payStatusId" value="${payStatus}" />
                        <div class="package-table">
                            <table class="table" cellspacing="0" cellpadding="0">
                                <thead>
                                <tr>
                                    <th>商品/运单信息</th>
                                    <th colspan="3">费用详细</th>
                                    <th width="118">金额</th>
                                    <th width="100">操作</th>
                                </tr>
                                </thead>
                                <tbody  class="merge-btn">
                                <tr>
                                    <td colspan="7">
                                        <div class="merge-btn-list">
                                            <p class="merge-btn-right floatR">
                                                <!-- 展开/收起状态-->
                                                <a href="#" class="show-btn contr"><i class="glyphicon glyphicon-menu-down" temp="true"></i>展开/收起</a>
                                                <a href="#" class="slide-btn"><i class="glyphicon glyphicon-menu-left"></i></a>
                                                <a href="#" class="slide-btn"><i class="glyphicon glyphicon-menu-right"></i></a>
                                            </p>
                                            <label><input type="checkbox" class="" name="checkALL" id="checkALL2" onClick="checkALLItem(2);">全选</label>
                                            <span>已选择包裹：<span class="col_red" id="orderCount2">0</span>件 </span>
                                            <a style="border: 1px solid #99bfd6;color: #446984;" class="btn btn-default" href="javascript:deleMany()">批量删除</a>
                                            <a style="border: 1px solid #99bfd6;color: #446984;" class="btn btn-default" href="javascript:printMany()">批量打印</a>
                                            <a style="border: 1px solid #99bfd6;color: #446984;" class="btn btn-default" href="javascript:payMany()">运费/关税支付</a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
								<c:forEach var="pkg" items="${pkgList}">
                                <!--展开/隐藏列表开始 循环内容-->
                                <tbody class="table-merge <c:if test="${pkg.status==0 && pkg.arrive_status==1 && pkg.exception_package_flag=='Y'}">merge-red</c:if>">
                                <tr class="title-merge">
                                    <td colspan="7">
                                        <div class="title-merge-detail">
                                            <p class="title-merge-right floatR">
                                                <a class="opbtn" style="font-size: 14px;" onClick="showPrint(${pkg.package_id})">打印</a>
                                                <c:if test="${pkg.status ==0 && (pkg.attach_status ==0 || pkg.attach_status ==1)}">
                                                <a class="opbtn disassemble" style="font-size: 14px;" relateid="${pkg.package_id}">分箱</a>
                                                </c:if>
                                                <c:if test="${pkg.status==0 && (pkg.attach_status ==0 || pkg.attach_status ==1)}">
                                                <a class="opbtn" style="font-size: 14px;" onClick="Modify(${pkg.package_id})" id="edit_${pkg.package_id}">编辑</a>
                                                </c:if>
                                                <c:if test="${pkg.status==0 &&(pkg.attach_status ==0 || pkg.attach_status ==1)&&pkg.arrive_status==0}">
                                                <a class="opbtn"style="font-size: 14px;" onClick="delPkg(${pkg.package_id})">删除</a>
                                                </c:if>
                                                <c:if test="${pkg.status==0 && pkg.arrive_status==1 && pkg.exception_package_flag=='Y'}">
                                                <a class="opbtn" style="font-size: 14px;" onClick="newtransp2(2,${pkg.logistics_code},${pkg.osaddr_id})">下单</a>
                                                </c:if>
                                                <a href="javascript:telesp(${pkg.package_id})" class="open"><i class="glyphicon glyphicon-menu-down"></i></a>
                                            </p>
											<input type="checkbox" id="item${pkg.package_id}" name="chkOrder" express_package = "${pkg.express_package}" attach_id="${pkg.attach_id}" attach_status="${pkg.attach_status}" arrive_status="${pkg.arrive_status}" pkg_status="${pkg.status}" onClick="checkItem();" value="${pkg.package_id}" />
                                            <span>运单号：<label class="wbill">${pkg.logistics_code}</label></span>
                                            <span class="pr20">关联单号：<label class="rela" id="label_d${pkg.package_id}">${pkg.original_num}</label><input type="hidden" id="old_originalNum${pkg.package_id}" value="${pkg.original_num}" /></span>
                                            <span class="pr20">状态：<b>${pkg.statusDesc}</b></span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                                <!--展开内容请用块display: table-row-group;   display: none; position: relative; 开始-->
                                <tbody class="merge-detail"  style="display: none; position: relative;" id="glt${pkg.package_id}">
                                <tr>
                                    <td rowspan="3" class="border-left-no">
                                        <ul id="ul${pkg.package_id}">
											<li class="fist">
												<ol>
													<li class="addgods">内件名</li>
													<li class="addgods">规格</li>
													<li class="addgods">物品类别</li>
													<li class="addgods">申报价值($)</li>
													<li class="addgods">品牌</li>
													<li class="addgods">数量</li>
												</ol>
											</li>
											<c:forEach var="pg" items="${pkg.goods}" varStatus="ld">
											<li class="show" id="item${pg.goods_id}" name="item${pkg.package_id}" type="new" dtl="${pg.goods_id}">
												<ol>
													<li class="addgods">
														<p name="name${pkg.package_id}" id="name${pg.goods_id}">${pg.goods_name}</p>
														<input type="hidden" name="pg_goods_name${pkg.package_id}" value="${pg.goods_name}">
														<input type="text"  name="mname${pkg.package_id}" id="mname${pg.goods_id}"  value="${pg.goods_name}"  style="width:90px;display: none;" />
													</li>
													<li class="addgods">
														<p name="spec${pkg.package_id}" id="spec${pg.goods_id}">${pg.spec}</p>
														<input type="hidden" name="pg_spec${pkg.package_id}" value="${pg.spec}">
														<input type="text"  name="mspec${pkg.package_id}"  id="mspec${pg.goods_id}"   value="${pg.spec}" style="width:90px;display: none;">
													</li>
													<li class="addgods">
														<input style="width:90px;display: none;" id="mcatalog${pg.goods_id}"  onclick="ShowCataDialog(${pg.goods_id});" name="mcatalog${pkg.package_id}"/>
														<input type="hidden" id="catalogKey${pg.goods_id}"  name="pg_goods_type${pkg.package_id}" value="${pg.goods_type_id}">
														<p name="catalog${pkg.package_id}"  id="catalog${pg.goods_id}">${pg.name_specs}</p>
													</li>
													<li class="addgods">
														<p name="price${pkg.package_id}"  id="price${pg.goods_id}">
															<fmt:formatNumber value="${pg.price+0.00001}" type="currency" pattern="#,###.##" />
														</p>
														<input type="hidden" name="pg_price${pkg.package_id}" value="${pg.price}">
														<input type="text"  name="mprice${pkg.package_id}"  id="mprice${pg.goods_id}"  value="${pg.price}" style="width:90px;display: none;">
													</li>
													<li class="addgods">
														<p name="brand${pkg.package_id}" id="brand${pg.goods_id}">${pg.brand}</p>
														<input type="hidden" name="pg_brand${pkg.package_id}" value="${pg.brand}">
														<input type="text"  name="mbrand${pkg.package_id}"  id="mbrand${pg.goods_id}"   value="${pg.brand}" style="width:90px;display: none;">
													</li>
													<li class="addgods">
														<p name="num${pkg.package_id}"  id="num${pg.goods_id}">${pg.quantity}</p>
														<input type="hidden" name="pg_quantity${pkg.package_id}" value="${pg.quantity}">
														<input type="text" name="mnum${pkg.package_id}"  id="mnum${pg.goods_id}"  class="miut" value="${pg.quantity}" style="display: none;">
														<div class="PM" name="modifyNum${pkg.package_id}" style="display: none;">
															<a class="icon33" href="javascript:ModifyListNum(1,${pg.goods_id})"></a>
															<a class="icon34" href="javascript:ModifyListNum(-1,${pg.goods_id})"></a>
														</div>
														<div class="delgods"><a href="javascript:DelItem(this,${pg.goods_id})" name="mDel${pkg.package_id}" style="display: none;">删除</a></div>
													</li>
												</ol>
											</li>
											</c:forEach>                                           											
                                        </ul>
                                    </td>
                                    <td class="text-center bg-gray" width="106">包裹重量</td>
                                    <td width="90" class="text-center">${pkg.weight}磅</td>
                                    <td rowspan="5"  class="border-bottom-no text-center bg-gray" width="24">装运总费用</td>
                                    <td rowspan="5"  class="border-bottom-no text-center">
                                        <p class="col_red">
                                        	<fmt:formatNumber value="${pkg.transport_cost+0.00001}" type="currency" pattern="#,###.##" />$
                                        </p>
                                        <small class="col_666">税费<fmt:formatNumber value="${pkg.customs_cost+0.00001}" type="currency" pattern="#,###.##" />$</small>
                                    </td>
                                    <td rowspan="5"  class="border-bottom-no handle-list border-right-blue">
                                    	<c:if test="${pkg.pay_status==1 and pkg.transport_cost>0.0001 and pkg.exception_package_flag !='Y'}">
                                        <a style="width: 72px;" class="btn btn-orange" onClick="payment('${pkg.logistics_code}')">待支付</a>
                                        </c:if>
                                        <c:if test="${pkg.pay_status==2}">
                                        <a style="width: 72px;" class="btn btn-orange">已支付</a>
                                        </c:if>
                                        <c:if test="${pkg.pay_status ==3 and pkg.pay_tax>0.0001 and pkg.exception_package_flag !='Y'}">
                                        <a style="width: 72px;" class="btn btn-orange" onClick="payTax('${pkg.logistics_code}')">待支付</a>
                                        </c:if>
                                        <c:if test="${pkg.pay_status==4}">
                                        <a style="width: 72px;" class="btn btn-orange">已支付</a>
                                        </c:if>
                                        <c:if test="${pkg.pay_status==5}">
                                        <a style="width: 90px;" class="btn btn-orange">已支付[预扣]</a>
                                        </c:if>
                                        <c:if test="${pkg.pay_status==6}">
                                        <a style="width: 86px;" class="btn btn-orange">预扣失败</a>
                                        </c:if>                                        
                                        <a href="javascript:query_logistics_detail(${pkg.logistics_code});" class="col_blue">物流追踪</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center bg-gray">运费</td>
                                    <td class="text-center">
										<fmt:formatNumber value="${pkg.freight+0.00001}" type="currency" pattern="#,###.##" />$
									</td>
                                </tr>
                                <tr style="">
                                    <td class="text-center bg-gray">
										<div style="position:relative;">
											<div item="${pkg.status}" item1="${pkg.waitting_merge_flag}" item2="${pkg.exception_package_flag}" id="cliSev${pkg.package_id}" onMouseOver="cliService(${pkg.package_id},this)">增值服务</div>											
											<div id="divService_${pkg.package_id}" val="${pkg.package_id}" class="divService" style="display: none; position: absolute; top: -50px; left: 50px;">
												<div style="background-color: #fff; border: 1px solid #0097db; padding: 0px 12px 6px 12px; width: 120px;">
													<ul class="pd" style="line-height: 26px;" id="divService_ul_${pkg.package_id}">
														
													</ul>
												</div>
												<ss> <si></si></ss>
											</div>
										</div>
									</td>
                                    <td class="text-center" id="attachPrice${pkg.package_id}">${pkg.attach_service_price}$</td>
									<td>
										
									</td>
                                </tr>
                                <tr>
                                    <td rowspan="2" class="border-left-no border-bottom-no">
                                        <div class="address-box">
                                            <p class="address-detail"><span class="col_blue">仓库信息：</span><input type="hidden"value="${pkg.osaddr_id}" />${pkg.warehouseIndexof}
                                            <c:if test="${pkg.express_package == 0}">
                                            	<a href="#" class="col_blue" style="margin-left: 5px;">F渠道</a>
                                            	<c:if test="${pkg.status == 0 }">
													<a href="javascript:updateExpressPackage(${pkg.package_id});" class="col_blue" id="ExpressPackage" style="margin-left: 5px;">[修改]</a>
												</c:if>
                                            </c:if>
                                            <c:if test="${pkg.express_package == 1}">
												<a href="#" class="col_blue" style="margin-left: 5px;">A渠道</a>
												<c:if test="${pkg.status == 0 }">
													<a href="javascript:updateExpressPackage(${pkg.package_id});" class="col_blue" id="ExpressPackage" style="margin-left: 5px;">[修改]</a>
												</c:if>
											</c:if>
											<c:if test="${pkg.express_package == 2}">
												<a href="#" class="col_blue" style="margin-left: 5px;">B渠道</a>
												<c:if test="${pkg.status == 0 }">
													<a href="javascript:updateExpressPackage(${pkg.package_id});" class="col_blue" id="ExpressPackage" style="margin-left: 5px;">[修改]</a>
												</c:if>
											</c:if> 
                                            </p>
                                            <p class="address-detail"><span class="col_blue">收货地址：</span><span id="address${pkg.package_id}">${pkg.frontUserAddress.addressStr} ${pkg.frontUserAddress.receiver}  ${pkg.frontUserAddress.mobile}</span>
                                            <c:if test="${pkg.status < 2}"> 
                                            	<a href="javascript:ModifyAddress(${pkg.package_id})" class="col_blue">[修改]</a> 
                                            </c:if>
                                            <c:if test="${pkg.status > 1}">
                                            	<a href="javascript:;" class="col_blue" id="address${pkg.package_id}">[收货地址]</a> 
											</c:if>
                                            </p>
                                        </div>
										<div class="Collapse2_1" id="BtnModify${pkg.package_id}" style="display: none;">
											<input type="hidden" name="packageId" value="" /> 
											<a class="Block input06 pull-right size16 white" href="javascript:ModifyProduct(${pkg.package_id});">完成</a> 
											<a class="pull-right size16" href="javascript:Cancel(${pkg.package_id});" style="">取消</a>
											<a class="Block input06 pull-left size16 white" href="javascript:AddListProduct(${pkg.package_id});">+ 添加</a>
											<span style="float: left; padding-left: 10px; height: 50px; width: 200px; line-height: 1.5; font-family: initial;"><span>
											<span style="float: left;"><span class="cbox" onClick="checkBoxClick(this,${pkg.package_id},${pkg.status});"></span></span>
											<span style="padding: 5px">需等待合箱</span></span> 
											<br />提示：若勾选需等待合箱，<br />则包裹到库后仓库不会进行打包
											</span>
										</div>
                                    </td>
                                    <td class="text-center bg-gray">申报总价值</td>
                                    <td class="text-center">
                                    <fmt:formatNumber value="${pkg.total_worth+0.00001}" type="currency" pattern="#,###.##" />$
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-bottom-no text-center bg-gray">关税总费用</td>
                                    <td class="border-bottom-no text-center">
                                    <fmt:formatNumber value="${pkg.customs_cost+0.00001}" type="currency" pattern="#,###.##" />$
                                    </td>
                                </tr>
								
                                </tbody>
                                <!--展开内容块结束-->
                                <tbody>
                                    <tr>
                                        <td colspan="7" class="ht15"></td>
                                    </tr>
                                </tbody>
                                </c:forEach>
                            </table>
                        </div>
						<div class="page-list">
							<jsp:include page="paper.jsp"></jsp:include>
							    <script type="text/javascript"> 
								    function pagerClick(index){
								    	//业务查询
 								    	window.location.href="${mainServer}/transport?status="+status+"&pageIndex="+index;
								    }
								    </script>
						</div>
                        <!-- <div class="page-list">
                            <p class="page-btn floatR">
                                <a href="#"><i class="glyphicon glyphicon-menu-left"></i></a>
                                <a href="#" class="active">1</a>
                                <a href="#">2</a>
                                <a href="#">3</a>
                                <a href="#"><i class="glyphicon glyphicon-menu-right"></i> </a>
                            </p>
                            <p class="word floatL">
                                <span>总记录数：7条</span>
                                <span>每页显示：15条</span>
                                <span>总页数：7页</span>
                            </p>
                        </div> -->
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!--右侧内容结束-->

</div>
<!--中间内容结束-->

<!--底部开始-->
<div class="footer">
    深圳市程露祥供应链管理有限公司版权所有 粤ICP备17086482 Copyright 2017 su8exp.com Inc. All Rights Reserved.
</div>
<!--底部结束-->
<script>
$(function(){ 
	$('.contr').click(function(){
		var flag=$(this).find('i').attr("temp");
		if(flag){
			$("tbody[id^=glt]").slideUp();
			$(this).removeClass('open');
			$(this).find('i').attr("temp","false");
		}
		if(!$("tbody[id^=glt]").is(':visible')){
			$("tbody[id^=glt]").slideDown();
			$(this).addClass('open');
			$(this).find('i').attr("temp","true");
		}		
	});
	$('#importPkg').click(function(){
		  if(!validateLogin()){
			  return;
		  }
		window.location.href = "${mainServer}/homepkg/init_importPkg?timestamp="+ new Date().getTime();
	});
});
var exportPackageWindow;
function showExportPackageWindow() {
	showIndex = 0;
	var off = 0;
	off = ($(window).height() - 200) / 2;
	exportPackageWindow = $.layer({
		bgcolor: '#fff',
		type: 1,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [0, 0.3, '#666', true],          
		area: ['450px', '150px'],
		offset: [off, ''],
		shadeClose:true,
		title: false,
		zIndex:10002,
		page: { dom: '#exportPkgWinId' },
		closeBtn: [0, false],//显示关闭按钮
		close: function (index) {
			layer.close(index);
			$('#exportPkgWinId').hide();
			//ActivityID = 0;
		}

	});
}
$('#closeExportPkgWinId').on('click', function(){
	layer.close(exportPackageWindow);
});
//导出包裹
function exportPkgConfirm(){
	var fromDate=$("#fromDate").val();
	var toDate=$("#toDate").val();
	window.location.href = "${mainServer}/homepkg/exportFrontEndPackage?fromDate="
			+ fromDate
			+ "&toDate="
			+ toDate
			;
	layer.close(exportPackageWindow);
}
//上传身份证
function uploadId(){
	window.open("${mainServer}/id"); 
}
//列表产品新增
function AddListProduct(ID) {
	var key = new Date().getMilliseconds();
	var row = $("<li class=\"show\" id=\"item" + key + "\" name=\"item" + ID + "\" type=\"new\" dtl='"+key+"'></li>");

	var ol = $("<ol></ol>");

	var td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"name" + ID + "\" id=\"name" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_goods_name"+ID+"\" value=''>"));
	td.append($("<input type='text'  name=\"mname" + ID + "\" id=\"mname" + key + "\"  value=''  style='width:90px;' placeholder=\"请输入商品名称\" />"));
	ol.append(td);
	
	td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"spec" + ID + "\" id=\"spec" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_spec"+ID+"\" value='300'>"));
	td.append($("<input type=\"text\"  name=\"mspec" + ID + "\" id=\"mspec" + key + "\"  value=\"\" style='width:90px;'  placeholder=\"请填写规格\">"));
	ol.append(td);
	
	td = $("<li class=\"addgods\"></li>");
	td.append($("<input style=\"width:90px;\" id=\"mcatalog" + key + "\" onclick=\"ShowCataDialog(" + key + ");\" name=\"mcatalog" + ID + "\" style='width:80px;'  placeholder=\"请选择类别\" /><input type='hidden' id=\"catalogKey" + key + "\" name=\"pg_goods_type"+ID+"\">"));
	td.append($("<p name=\"catalog" + ID + "\" id=\"catalog" + key + "\" style=\"display: none;\"></p>"));
	ol.append(td);

	td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"price" + ID + "\" id=\"price" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_price"+ID+"\" value='300'>"));
	td.append($("<input type=\"text\"  name=\"mprice" + ID + "\" id=\"mprice" + key + "\"  value=\"\" style='width:90px;'  placeholder=\"请填写单价\">"));
	ol.append(td);
													
	td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"brand" + ID + "\" id=\"brand" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_brand"+ID+"\" value='300'>"));
	td.append($("<input type=\"text\"  name=\"mbrand" + ID + "\" id=\"mbrand" + key + "\"  value=\"\" style='width:90px;'  placeholder=\"请填写品牌\">"));
	ol.append(td);
	
	td = $("<li class=\"addgods\"></li>");
	td.append($("<p name=\"num" + ID + "\" id=\"num" + key + "\" style=\"display: none;\"></p>"));
	td.append($("<input type='hidden' name=\"pg_quantity"+ID+"\" value='2'>"));
	td.append($("<input type=\"text\" name=\"mnum" + ID + "\" id=\"mnum" + key + "\" class=\"miut\" value=\"1\">"));
	td.append($("<div class=\"PM\" name=\"modifyNum" + ID + "\"><a class=\"icon33\" href=\"javascript:ModifyListNum(1," + key + ")\"></a><a class=\"icon34\" href=\"javascript:ModifyListNum(-1," + key + ")\"></a></div>"));
	td.append($("<div class=\"delgods\"><a href=\"javascript:DelItem(this," + key + ")\" name=\"mDel" + ID + "\" >删除</a></div>"));
	ol.append(td);

	row.append(ol);
	$('#ul'+ID).append(row);
}
//删除产品信息
function DelItem(o,ID) {
	//a->div->li->ol->li
	var cont=$(o).parent().parent().parent().parent().parent().find('.show').length;
	if(cont==1){
		layer.msg("必须最少有一个商品", 2, 5);
		return;
	}
	var k = layer.confirm('确定删除该产品？', function () {
		$("#item" + ID).hide();
		$("#item" + ID).removeClass('show');
		$("#item" + ID).addClass('dele');
		layer.close(k);
	});
}
//列表取消修改
function Cancel(id) {
	var old_originalNum=$("#old_originalNum"+ id).val();
	$("#label_d"+ id).html(old_originalNum);
	//ActivityID = 0;
	$("p[name=name" + id + "]").show();
	$("input[name=mname" + id + "]").hide();
	$("p[name=catalog" + id + "]").show();
	$("input[name=mcatalog" + id + "]").hide();
	$("p[name=price" + id + "]").show();
	$("input[name=mprice" + id + "]").hide();
	$("p[name=brand" + id + "]").show();
	$("input[name=mbrand" + id + "]").hide();
	$("p[name=num" + id + "]").show();
	$("input[name=mnum" + id + "]").hide();
	$("div[name=modifyNum" + id + "]").hide();

	$("a[name=mDel" + id + "]").hide();
	$("a[name=mDel" + id + "]").parent().parent().css({"height":"0","line-height":"0"});

	$("input[name=mprice" + id + "]").parent().css("width","110px");
	
	$("p[name=suppnum" + id + "]").attr("style","");

	$(".editCatalog"+id).remove();
	//还原值
	$("p[name=name" + id + "]").each(function () {
		var id = this.id.replace("name", "");
		if($("#item"+id).attr("type") != "new")
		{
			$("#mname" + id).val($("#name" + id).html());
			$("#mcatalog" + id).val($("#catalog" + id).html());
			$("#mprice" + id).val($("#price" + id).html());
			$("#mnum" + id).val($("#num" + id).html());
		}
	});
	
	$("#ul" + id).find('.dele').each(function(){                   
		$(this).removeClass('dele');
		$(this).addClass('show');
		$(this).css("display","block");
		//$(this).attr("Del","");
		//$(this).attr("style","");
	});

	if ($("#ul" + id + " li").length == 0) {
		$("#ul" + id).html("<li></li>");
	}

	$("#BtnModify" + id).hide();
}
//提交修改包裹数据
  function ModifyProduct(ID) {
	var list=$("#ul" + ID).find('.show');
	var isValid=true;
 
	//获取用户的类型：同行2，直客1
	var userType=$("#userType").val();
	 
	$.each(list,function(i,li){
		var inputs=$(this).find("input[type='text']");		
		$.each(inputs,function(j,input){
			var name=input.name;
			var val=input.value;
			if(('mname'+ID)==name){
				if($.trim(val)=="" || val=="请输入商品名称"){
					layer.msg("请输入商品名称", 2, 5);
					isValid=false;
					return false;
				}
			}
			if(('mcatalog'+ID)==name){
				if(userType!="2"){
					if(val==""){
						layer.msg("请选择商品类别", 2, 5);
						isValid=false;
						return false;
					}
				}
				
			}
			if(('mprice'+ID)==name){
				var reg = /^\d+(?=\.{0,1}\d+$|$)/;
				if(reg.test(val)==false){
					layer.msg("商品价格只能为数字", 2, 5);
					isValid=false;
					return false;
				}
				if(val==""){
					layer.msg("请输入商品价格", 2, 5);
					isValid=false;
					return false;
				}
			}
			if(('mnum'+ID)==name){
				var reg=/^\d+$/;
				if(reg.test(val)==false){
					layer.msg("商品数量只能为数字", 2, 5);
					isValid=false;
					return false;
				}
				if(val==""){
					layer.msg("请输入商品数量", 2, 5);
					isValid=false;
					return false;
				}
			}
		});
	});
	
	var old_originalNum=$("#old_originalNum"+ID).val();
	var new_originalNum=$("#new_originalNum"+ID).val();
	var arrive_status=$("#item"+ID).attr("arrive_status");
	var reg = /^[0-9a-zA-Z]+$/;
	if(arrive_status==0)
	{

	if(old_originalNum!=null&&old_originalNum!='')
	{
		if(new_originalNum==null||new_originalNum==''){
			layer.msg("请输入包裹关联单号", 2, 5);
			return false;
		}else if(!reg.test(new_originalNum))
		{
			layer.msg("关联单号只能是数字或英文", 2, 5);
			return false;
		}
	}
	
	if(old_originalNum!=new_originalNum)
	{
		var url = "${mainServer}/homepkg/checkOriginalnum";
		var needReturn=false;
		$.ajax({
		   url:url,
		   type:'post',
		   dataType:'json',
		   async:false,
		   data:{'original_num':new_originalNum},
		   success:function(data){
				var msg=data['msg'];
				if(msg=="1"){
					layer.msg("关联单号已存在", 2, 5);
					needReturn=true;
				}else if(msg=="3"){
					layer.msg("该包裹已到仓库为未认领状态，请联系客服", 2, 5);
					needReturn=true;
				}
		   }
		});
		if(needReturn)
		{
			return false;
		}
	}
	
	}

	if(isValid==false){
		return;
	}
	var url = "${mainServer}/homepkg/updatepkg";
	var goodslist=[];
	
	$.each(list,function(i,li){
		var goods={};	
		var self=$(li);	
		var goods_nameVal=self.find("input[name=mname"+ID+"]").val();
		var specVal=self.find("input[name=mspec"+ID+"]").val();
		var goods_typeVal=self.find("input[name=pg_goods_type"+ID+"]").val();
		var priceVal=self.find("input[name=mprice"+ID+"]").val();
		var brandVal=self.find("input[name=mbrand"+ID+"]").val();
		var quantityVal=self.find("input[name=mnum"+ID+"]").val();
			
		goods['goods_name']=goods_nameVal;
		goods['spec']=specVal;
		goods['goods_type_id']=goods_typeVal;
		goods['price']=priceVal;
		goods['brand']=brandVal;
		goods['quantity']=quantityVal;
		goodslist.push(goods);
	});
	
	if(goodslist.length==0)
	{
		layer.msg("内件为空，不能保存", 2, 5);
		return;
	}
	var v_waitting_merge_flag=$("#waitting_merge_flag"+ID).val();
	var jsStr=JSON.stringify(goodslist);

	
	$.ajax({
	   url:url,
	   data:{'package_id':ID,'goodsList':jsStr,'originalNum':new_originalNum,'waitting_merge_flag':v_waitting_merge_flag},
	   type:'post',
	   success:function(data){
		   if(data){
			layer.msg("修改包裹成功", 3, 1);
			 if(data=='OK_AND_ALERT'){
				 alert("请编辑包裹内件并选择收货地址");
			 }
			window.location.href = "${mainServer}/transport";
		   }				
	   }
	});	 
}
//全选
function checkALLItem(key) {
	var checkedOfAll = $("#checkALL" + key).attr("checked") == "checked";
	$("input[name='checkALL']").attr("checked",!checkedOfAll);	
	$("input[name='chkOrder']").each(function () {
		if (this.checked == checkedOfAll) {               
			this.click();
		}
	});
	var count=$("input[name='chkOrder']:checked").length;
	if(!checkedOfAll){
		$("#orderCount2").html(count);
	}else{
		$("#orderCount2").html(0);
	}		
}
//选择包裹选项
function checkItem() {
	var num=$("input[name='chkOrder']:checked").length;
	$("#orderCount2").html(num);
}
// 已入库状态渠道修改  
function updateExpressPackage(ID,status){
	$.layer({
		bgcolor: '#fff',
		type: 2,
		title: "渠道修改",
		shadeClose: false,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [1, 0.3, '#666', true],
		area: ['400px', '300px'],
		closeBtn: [0, true], //显示关闭按钮
		iframe: { src: '${mainServer}/expressPackage?package_id=' + ID},
		success: function () {
			//$("#susu").attr("item",express_package);
		}
	});
};
//===================打印面单==========
function showPrint(package_id) {
	$.ajax({
		url:'${mainServer}/homepkg/canPrint',
		type:'post',
		data:{pkgid:package_id},
		dataType:"json",
		async:false,
		success:function(data){
			/*if(0 == data.canprint){
				var needP=$("#needPrintd").html();
				var np=Number(needP)-1; 
				$("#needPrintd").html(np);
				var doneP=$("#donePrintd").html();
				var dp=Number(doneP)+1; 
				$("#donePrintd").html(dp);
			}*/
		}			
	});
	
	$.layer({
		title :'打印面单',
		type: 2,
		shadeClose: true,
		shade: 0.8,
		offset: ['10px', '400px'],
		area: ['590px', '800px'],
		iframe: { src: '${mainServer}/homepage/print?package_id='+package_id }
	}); 
}
//分箱按钮
$(".disassemble").click(function(){
	var pkgid = $(this).attr("relateid").trim();
	$.ajax({
		url:'${mainServer}/homepkg/packageMergeSplitCheck',
		type:'post',
		data:{packageIds:pkgid,checkException:'Y'},
		dataType:"json",
		async:false,
		success:function(data){
			if("F" == data.result){
				if("Y" == data.needEdit){
					alert("包裹信息不完整，无法进行分箱合箱");
				}else
				{
					alert("您所选择包裹已进行分箱｜合箱服务，不能再次分箱!");
				}
			}else
			{
				window.location.href='${mainServer}/homepkg/unboxHomepkg?pkgid='+pkgid;
			/* 	$.layer({
					bgcolor: '#fff',
					type: 2,
					title: "分箱",
					shadeClose: false,
					fix: false,
					shade: [0.5, '#ccc', true],
					border: [1, 0.3, '#666', true],
					area: ['1000px', '500px'],
					closeBtn: [0, true], //显示关闭钮
					iframe: { src: '${mainServer}/homepkg/unboxHomepkg?pkgid='+pkgid},
					success: function () {
					},
					end: function () {
					}
					}); */
			}
		}			
	});
});
//列表修改
function Modify(id,type) {
	var arrive_status=$("#item"+id).attr("arrive_status");
	var isMargeSplit=false;
	$.ajax({
		url:'${mainServer}/homepkg/packageMergeSplitCheck',
		type:'post',
		data:{packageIds:id,checkException:'N'},
		dataType:"json",
		async:false,
		success:function(data){
			if("F" == data.result){
				isMargeSplit=true;
			}
		}			
	});
	if(arrive_status==0&&!isMargeSplit)
	{
		var old_originalNum=$("#old_originalNum"+ id).val();
		$("#label_d"+ id).html("<input type=\"text\" id=\"new_originalNum"+id+"\" value=\""+old_originalNum+"\"/>");					
	}
	
	$("p[name=name" + id + "]").hide();
	$("input[name=mname" + id + "]").show();
	$("p[name=catalog" + id + "]").hide();
	$("input[name=mcatalog" + id + "]").show();
	$("p[name=price" + id + "]").hide();
	$("input[name=mprice" + id + "]").show();
	$("p[name=brand" + id + "]").hide();
	$("input[name=mbrand" + id + "]").show();
	$("p[name=num" + id + "]").hide();
	$("input[name=mnum" + id + "]").show();
	$("div[name=modifyNum" + id + "]").show();
	
	$("p[name=suppnum" + id + "]").attr("style","padding-top:10px;"); 
	
   $("a[name=mDel" + id + "]").show();
   $("a[name=mDel" + id + "]").parent().parent().css({"height":"40px","line-height":"40px"});

	
	$("input[name=mprice" + id + "]").css("width","64px");
	$("input[name=mprice" + id + "]").parent().css("width","80px");

	$("#BtnModify" + id).show();

	$("input[name=mname" + id + "]").attr("placeholder","请输入商品名称");
	$("input[name=mcatalog" + id + "]").attr("placeholder","请选择类别");
	$("input[name=mprice" + id + "]").attr("placeholder","请填写单价");

	$("input[name=mname" + id + "]").unbind("click,blur").bind("click",function(){
		if($(this).val().trim() == "请输入商品名称"){
			$(this).val("");
		}
	}).bind("blur",function(){
		if($(this).val().trim() == ""){
			$(this).val("请输入商品名称");
		}
	});

	$("input[name=mcatalog" + id + "]").unbind("blur").bind("click",function(){
		if($(this).val().trim() == "请选择类别"){
			$(this).val("");
		}
	}).bind("blur",function(){
		if($(this).val().trim() == ""){
			$(this).val("请选择类别");
		}
	});

	$("input[name=mprice" + id + "]").unbind("click,blur").bind("click",function(){
		if($(this).val().trim() == "请填写单价"){
			$(this).val("");
		}
	}).bind("blur",function(){
		if($(this).val().trim() == ""){
			$(this).val("请填写单价");
		}
	});
}
//删除包裹
function delPkg(package_id){
	var pageIndex=$("#pageIndex").val();
	var status=$("#statusId").val();
	var paystatus=$("#payStatusId").val();
	var attachId=$("#item"+package_id).attr("attach_id");
	if(attachId == 4){
		var k = layer.confirm('分箱相关包裹将全部被删除，若想重新分箱请重新下单', function () {
		var url = "${mainServer}/homepkg/delHomepkg?package_id="+package_id+"&pageIndex="+pageIndex+"&status="+status+"&paystatus="+paystatus;
		window.location.href = url;
		layer.close(k);
		});
	}else if(attachId == 7){
		var k = layer.confirm('合箱相关包裹将全部被删除，若想重新合箱请重新下单', function () {
		var url = "${mainServer}/homepkg/delHomepkg?package_id="+package_id+"&pageIndex="+pageIndex+"&status="+status+"&paystatus="+paystatus;
		window.location.href = url;
		layer.close(k);
		});
	}else{
		var k = layer.confirm('确定删除该包裹？', function () {
		var url = "${mainServer}/homepkg/delHomepkg?package_id="+package_id+"&pageIndex="+pageIndex+"&status="+status+"&paystatus="+paystatus;
		window.location.href = url;
		layer.close(k);
		});
	}				
}
//展开收缩
function telesp(id){
	console.log("1");
	if (!$("#glt" + id).is(":visible")) {
        $("#glt" + id).show();                  
    }else{
		$("#glt" + id).hide();
	}
}
//物流追踪
function query_logistics_detail(logistics_code){
	$.layer({
		bgcolor: '#fff',
		type: 2,
		title: "物流追踪",
		shadeClose: false,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [1, 0.3, '#666', true],
		area: ['500px', '600px'],
		closeBtn: [0, true], //显示关闭按钮
		iframe: { src: '${mainServer}/query_logistics_detail?logistics_code='+logistics_code},
		success: function () {
		},
		end: function () {
		}
	});
}
//包裹状态 -- tab 
function showTransportStatus(status){
	window.location.href="${mainServer}/transport?status="+status;
}
//所有状态的包裹
function showAllTransport(){
	window.location.href="${mainServer}/transport";
}
//增值服务
function cliService(tempid, elem) {
	var needShow=false;
	$.ajax({
		url:'${mainServer}/homepkg/needShowAttachPhoto',
		type:'post',
		data:{package_id:tempid},
		dataType:"json",
		success:function(data){
			if(data.needShow=='Y')
			{
				needShow=true;
			}
		}
	});
	$("#divService_" + tempid).show();				
	$("#divService_ul_" + tempid).html("");
	$.ajax({
		url:'${mainServer}/homepkg/cliService',
		type:'post',
		data:{package_id:tempid},
		dataType:"json",
		success:function(data){
			$("#divService_ul_" + tempid).html("");
			var list=data.list;
			//alert(list.length);
			if(list!=null && list.length>0){
				for(var i=0;i<list.length;i++){
					var item=list[i];			 
					var $itemLi=$('<li></li>');
					var $itemA=$('<a class="'+item.service_class+' icon59" href="javascript:SetService('+tempid+',\''+item.service_name+'\','+item.attach_id+')">'+item.service_name+'</a>');
					//拍照服务
					if(item.attach_id==18)
					{
						if("blue"==item.service_class&&needShow)
						{
							$itemA=$('<a class="'+item.service_class+' icon59" href="javascript:SetService('+tempid+',\''+item.service_name+'\','+item.attach_id+')">'+item.service_name+'</a><a target="_blank" href="${mainServer}/homepkg/showAttachPhoto?package_id='+tempid+'">[点击查看]</a>');											
						}
					}									
					
					//保价服务
					if(item.attach_id==32)
					{
						$itemA=$('<a class="'+item.service_class+' icon59" href="javascript:keepPriceDisplay('+tempid+')">'+item.service_name+'</a>');
					}
					$itemLi.append($itemA);
					$("#divService_ul_" + tempid).append($itemLi);	
				}
			}
		}			
	});
}
function SetService(package_id,service_name,attach_id){
	var pkg_status=$("#cliSev"+package_id).attr("item");
	//var freight_cost=$("#freightCost"+package_id).attr("item");
	//包裹已入库状态后就不能选择增值服务
	if(pkg_status>0){
		layer.msg("该包裹已无法添加增值服务", 2, 5);
		return;
	}
	
	var waitting_merge_flag=$("#cliSev"+package_id).attr("item1");
	if(waitting_merge_flag=='Y'){
		layer.msg("您的包裹需等待合箱，无法选择其他增值服务", 2, 5);
		return;
	}
	var exception_package_flag=$("#cliSev"+package_id).attr("item2");
	if(exception_package_flag=='Y'){
		layer.msg("您的包裹信息不完整，无法选择其他增值服务", 2, 5);
		return;
	}
	$.ajax({
		url:'${mainServer}/homepkg/setService',
		type:'post',
		data:{package_id:package_id,attach_id:attach_id,service_name:service_name,service_price:0},
		dataType:"json",
		async:false,
		success:function(data){
			var status=data['status'];
			if("0" == status){
				window.location.href="${mainServer}/login";
			}
			var attach_price=data['attach_price'];
			if("1" == status){
				alert("您已成功选择["+service_name+"]服务");
			}else if("2" == status){
				alert("您已取消["+service_name+"]服务");
			}else{
				alert("本包裹["+service_name+"]增值服务已经完成,不可取消");
			}
			if(attach_price!=null || attach_price!=""){
				//var transpCost=parseFloat(freight_cost)+parseFloat(attach_price);
				$("#attachPrice"+package_id).html(attach_price);
				//$("#transpCost"+package_id).html(transpCost);
			}
			$("#divService_ul_" + package_id).html("");
			$("#divService_" + package_id).hide();
			//$("#service" + package_id).html("");
		}			
	});
}
//保价
function keepPriceDisplay(package_id){
	var pkg_status=$("#cliSev"+package_id).attr("item");
	if(pkg_status>0){
		layer.msg("该包裹已无法添加增值服务", 2, 5);
		return;
	}
	var waitting_merge_flag=$("#cliSev"+package_id).attr("item1");
	if(waitting_merge_flag=='Y'){
		layer.msg("您的包裹需等待合箱，无法选择其他增值服务", 2, 5);
		return;
	}
	var exception_package_flag=$("#cliSev"+package_id).attr("item2");
	if(exception_package_flag=='Y'){
		layer.msg("您的包裹信息不完整，无法选择其他增值服务", 2, 5);
		return;
	}
	$.layer({
		bgcolor: '#fff',
		type: 2,
		title: "包裹保价",
		shadeClose: false,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [1, 0.3, '#666', true],
		area: ['400px', '330px'],
		closeBtn: [0, false], //显示关闭按钮
		iframe: { src: '${mainServer}/homepkg/keepPrice?pkgid='+package_id},
		success: function () {
		},
		end: function () {
		}
		});
}
//查询
function ShowData(){
	var searchVal=$("#SearchKey").val();
	if(searchVal=="输入运单号、关联单号、收件人、手机号、商品名称"){
		layer.msg("请输入搜索名称", 2, 5);
		return;
	}
	searchVal = encodeURI(encodeURI(searchVal));
	window.location.href="${mainServer}/transport?searchVal="+searchVal;
}
function payment(logistics_code) {
   if(!validateLogin()){
	   return ;
   }	
 var url = "${mainServer}/member/frontUserStatus";
 $.ajax({
     url:url,
     type:'post',
     async:false,
     dataType:'json',
     success:function(data){
         // 账户正常
         if(data.result =="1"){
             window.location.href = "${mainServer}/payment/paymentList?logisticsCodes="+logistics_code;
         }
         else{
            alert(data.message);
         }
     }
 });
}
//支付关税
function payTax(logistics_code) {
   if(!validateLogin()){
	   return ;
   }
     var url = "${mainServer}/member/frontUserStatus";
     $.ajax({
         url:url,
         type:'post',
         async:false,
         dataType:'json',
         success:function(data){
             // 账户正常
             if(data.result =="1"){
                 window.location.href = "${mainServer}/payment/taxList?logisticsCodes="+logistics_code;
             }
             else{
                alert(data.message);
             }
         }
     });
 }
//修改收货地址
function ModifyAddress(ID){
	$.ajax({
		url:'${mainServer}/homepkg/sessionCheck',
		type:'post',
		dataType:"json",
		async:false,
		success:function(data){
			var status=data['status'];
			if("0" == status){
				window.location.href="${mainServer}/login";
			}else{
				var off = ($(window).height() - 760) / 2;
				$.layer({
				bgcolor: '#fff',
				type: 2,
				title: false,
				shadeClose: true,
				fix: false,
				shade: [0.5, '#ccc', true],
				scrollbar: true,
				border: [1, 0.3, '#666', true],
				area: ['750px', '700px'],
				iframe: { src: '${mainServer}/useraddr/initupdateAddress?package_id='+ID},
				success: function () {
				},
				end: function () {
				}
				});	
			}
		}			
	});							
}
function newtransp(flag){
	//1:新建包裹 2:下单
	window.location.href = "${mainServer}/newPackage?flag="+flag;
}
function newtransp2(flag,logistics_code,osaddr_id){
	//1:新建包裹 2:下单
	window.location.href = "${mainServer}/newPackage?flag="+flag+"&logistics_code="+logistics_code+"&osaddr_id="+osaddr_id;
}
//类别选择页
function ShowCataDialog(ID) {
	var off = ($(window).height() - 320) / 2;
	$.layer({
		bgcolor: '#fff',
		type: 2,
		title: false,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [1, 0.3, '#666', true],
		offset: [off, ''],
		area: ['850px', '370px'],
		closeBtn: [0, false], //显示关闭按钮
		iframe: { src: '${mainServer}/catalog/add?ID='+ID},
		});

	}
//设定类别信息
function SetCatalog(id, name, taxs, key, msg,tip,checkResult,service,producttip) {
	//alert($("#taxs" + key).val());
	//判断是列表页面还是新增页面
	if ($("#taxs" + key).val() != undefined) {
		$("#catalog" + key).val(name);
		$("#catalogKey" + key).val(id);
		//alert(key);
		$("#taxs" + key).val(taxs);
		GetChargeMsg();
		SetProductTip(id,producttip,key);
		
		//存在备注信息
		if (msg != "") {
		
			$("#chkCata" + key).remove();
			$("#chkMsg" + key).remove();
			$("#tip" + key).remove();
			$("#catalog" + key).parent().attr("style", "");

			$("#catalog" + key).attr("style", $("#catalog" + key).attr("style").replace(";float:left;margin:0px 20px 0px 25px;", ""));

			if(tip !=undefined && tip!=null && tip != ""){
				if(parseInt(checkResult)  == 1){
					$("#catalog" + key).after("<input type=\"checkbox\"  value=\"" + msg + "\" style=\"float:left;margin:3px 0px 0px 25px\" id=\"\chkCata" + key + "\" checked='checked' service='"+service+"'><span style=\"width:50px;display:block;padding:0 0 0 2px;line-height:0px;float:left;margin-top:9px;text-align:left;\"  id=\"\chkMsg" + key + "\">" + msg + "</span><span class=\"tip\" id='tip"+key+"' style='padding:0;cursor:pointer;' tipHtml = '"+tip+"'></span>");
				}else{
					$("#catalog" + key).after("<input type=\"checkbox\"  value=\"" + msg + "\" style=\"float:left;margin:3px 0px 0px 25px\" id=\"\chkCata" + key + "\" service='"+service+"'><span style=\"width:50px;display:block;padding:0 0 0 2px;line-height:0px;float:left;margin-top:9px;text-align:left;\"  id=\"\chkMsg" + key + "\">" + msg + "</span><span class=\"tip\" id='tip"+key+"' style='padding:0;cursor:pointer;' tipHtml = '"+tip+"'></span>");
				}
			}else{
				if(parseInt(checkResult)  == 1){
					$("#catalog" + key).after("<input type=\"checkbox\"  value=\"" + msg + "\" style=\"float:left;margin:3px 0px 0px 25px\" id=\"\chkCata" + key + "\" checked='checked' service='"+service+"'><span style=\"width:50px;display:block;padding:0 0 0 2px;line-height:0px;float:left;margin-top:9px;text-align:left;\"  id=\"\chkMsg" + key + "\">" + msg + "</span>");
				}else{
					$("#catalog" + key).after("<input type=\"checkbox\"  value=\"" + msg + "\" style=\"float:left;margin:3px 0px 0px 25px\" id=\"\chkCata" + key + "\" service='"+service+"'><span style=\"width:50px;display:block;padding:0 0 0 2px;line-height:0px;float:left;margin-top:9px;text-align:left;\"  id=\"\chkMsg" + key + "\">" + msg + "</span>");
				}
			}
			

			$("#catalog" + key).attr("style", $("#catalog" + key).attr("style") + ";float:left;margin:0px 20px 0px 25px;");
			$("#catalog" + key).parent().attr("style", "height:50px;");
		}
		else {

			$("#chkCata" + key).remove();
			$("#chkMsg" + key).remove();
			$("#tip" + key).remove();
			$("#catalog" + key).parent().attr("style", "");

			if(tip !=undefined && tip!=null && tip != ""){
				$("#catalog" + key).attr("style", $("#catalog" + key).attr("style").replace(";float:left;margin:0px 20px 0px 25px;", ""));
				if(isIE){
					$("#catalog" + key).after("<span class=\"tip\" id='tip"+key+"' style='padding:0;cursor:pointer;margin-left:88px;' tipHtml = '"+tip+"'></span>");
				}else{
					$("#catalog" + key).after("<span class=\"tip\" id='tip"+key+"' style='padding:0;cursor:pointer;margin:-6px 0 0 90px;' tipHtml = '"+tip+"'></span>");
				}
				
				$("#catalog" + key).attr("style", $("#catalog" + key).attr("style") + ";float:left;margin:0px 20px 0px 25px;");
				$("#catalog" + key).parent().attr("style", "height:50px;");
			}
			
			$("#catalog" + key).attr("style", $("#catalog" + key).attr("style").replace(";float:left;margin:0px 20px 0px 25px;", ""));
	
		}
	}
	else {
		//=============修改逻辑===========
		var tempid = $("#mcatalog" + key).attr("temp");
		$(".div"+key).hide();
		$("#mcatalog" + key).val(name);
		$("#catalogKey" + key).val(id);

		$(".editdiv"+key).hide();
		$(".cataDIV"+key).remove();
		if (msg != "") {
			if(tip !=undefined && tip!=null && tip != ""){
				if(parseInt(checkResult)  == 1){
					$("#catalog" + key).after("<div style='width:100;overflow:hidden;height:auto;' class='cataDIV"+key+" editCatalog"+tempid+"' itemid='"+key+"' tempid='"+tempid+"'><input type=\"checkbox\"  value=\"" + msg + "\" style=\"float:left;margin:3px 0px 0px 18px\" id=\"\chkCata" + key + "\" checked='checked' service='"+service+"'><span style=\"width:52px;display:block;padding:0 0 0 2px;line-height:0px;float:left;margin-top:9px;text-align:left;\"  id=\"\chkMsg" + key + "\">" + msg + "</span><span class=\"tip\" id='tip"+key+"' style='padding:0;cursor:pointer;' tipHtml = '"+tip+"'></span></div>");
				}else{
					$("#catalog" + key).after("<div style='width:100;overflow:hidden;height:auto;' class='cataDIV"+key+" editCatalog"+tempid+"' itemid='"+key+"' tempid='"+tempid+"'><input type=\"checkbox\"  value=\"" + msg + "\" style=\"float:left;margin:3px 0px 0px 18px\" id=\"\chkCata" + key + "\" service='"+service+"'><span style=\"width:52px;display:block;padding:0 0 0 2px;line-height:0px;float:left;margin-top:9px;text-align:left;\"  id=\"\chkMsg" + key + "\">" + msg + "</span><span class=\"tip\" id='tip"+key+"' style='padding:0;cursor:pointer;' tipHtml = '"+tip+"'></span></div>");
				}
			}else{
				if(parseInt(checkResult)  == 1){
					$("#catalog" + key).after("<div style='width:100;overflow:hidden;height:auto;' class='cataDIV"+key+" editCatalog"+tempid+"' itemid='"+key+"' tempid='"+tempid+"'><input type=\"checkbox\"  value=\"" + msg + "\" style=\"float:left;margin:3px 0px 0px 18px\" id=\"\chkCata" + key + "\" checked='checked' service='"+service+"'><span style=\"width:52px;display:block;padding:0 0 0 2px;line-height:0px;float:left;margin-top:9px;text-align:left;\"  id=\"\chkMsg" + key + "\">" + msg + "</span></div>");
				}else{
					$("#catalog" + key).after("<div style='width:100;overflow:hidden;height:auto;' class='cataDIV"+key+" editCatalog"+tempid+"' itemid='"+key+"' tempid='"+tempid+"'><input type=\"checkbox\"  value=\"" + msg + "\" style=\"float:left;margin:3px 0px 0px 18px\" id=\"\chkCata" + key + "\" service='"+service+"'><span style=\"width:52px;display:block;padding:0 0 0 2px;line-height:0px;float:left;margin-top:9px;text-align:left;\"  id=\"\chkMsg" + key + "\">" + msg + "</span></div>");
				}
			}
		}
		else {
			if(tip !=undefined && tip!=null && tip != ""){
				if(isIE){
					$("#catalog" + key).after("<div style='width:100;overflow:hidden;height:auto;' class='cataDIV"+key+" editCatalog"+tempid+"' itemid='"+key+"' tempid='"+tempid+"'><span class=\"tip\" id='tip"+key+"' style='padding:0;cursor:pointer;margin-left:88px;' tipHtml = '"+tip+"'></span></div>");
				}else{
					$("#catalog" + key).after("<div style='width:100;overflow:hidden;height:auto;' class='cataDIV"+key+" editCatalog"+tempid+"' itemid='"+key+"' tempid='"+tempid+"'><span class=\"tip\" id='tip"+key+"' style='padding:0;cursor:pointer;margin:0 0 0 88px;' tipHtml = '"+tip+"'></span></div>");
				}
			}
		}
	}
	
	$("#tip"+key).mouseover(function () {
		var truename = $(this).attr("tipHtml").trim();
		if(truename ==undefined || truename ==null || truename == "")
		{
			return;
		}
		var content = $(this).attr("tipHtml");
		var elem = $(this)[0];
		var left = pageX(elem);
		var top = pageY(elem);
		//======当前浏览器窗口宽度===========
		var windowWidth = $(window).width();
		if(left+70+200 > windowWidth)
		{
			$("#categoryDiv").html(content).addClass("right").css("position", "absolute").css("z-index","11000").css("left", left - 242).css("top", top - 45).show();
		}else{
			$("#categoryDiv").html(content).addClass("left").css("position", "absolute").css("z-index","11000").css("left", left+27).css("top", top-22).show();
		}
	})
	.mouseout(function () {
		$("#categoryDiv").removeClass("left").removeClass("right").html("").hide();
	});
}
//批量打印
function printMany() {       
    var packageIdArray=[];
	var logistics_codesArray=[];
	var original_numArray =[];
	$("input[name='chkOrder']:checked").each(function(i,element) {
        var pkgId=element.value;
        var logistics_code= $.trim($(this).next().find('.waybill').text());
        var original_num= $.trim($(this).next().next().find('.rela').text());
    	logistics_codesArray.push(logistics_code);
   	 	packageIdArray.push(pkgId);
   	 	original_numArray.push(original_num);
    });
var url='${mainServer}/homepkg/initPkgManyPrint?package_ids='+packageIdArray.join(',')+'&logistics_codes='+logistics_codesArray.join(',')+"&original_nums="+original_numArray.join(',');
$.layer({
			bgcolor: '#fff',
			type: 2,
			title: "批量打印清单",
			shadeClose: true,
			fix: false,
			shade: [0.5, '#ccc', true],
			border: [1, 0.3, '#666', true],
			area: ['750px', '800px'],
			closeBtn: [0, true], //显示关闭按钮
			iframe: { src: url},
			success: function () {
			} 
		});
}
//批量删除包裹
function deleMany(){
	if (parseInt($("#orderCount2").html()) < 2) {
		layer.msg("最少需勾选两条才能批量删除", 2, 5);
        return;
    }
	var str="";
	var isBox=true;
	var isNeedRemoveRadio=false;
	$("input[name='chkOrder']:checked").each(function(i,element) {
        var pkgId=element.value;
		//str=str+pkgId+",";
		//增值服务ID
		var attach_id=$(this).attr("attach_id");
		//增值服务状态(未完成，已完成)
		var attach_status=$(this).attr("attach_status");
		if(attach_id=="4" && attach_status=="1"){
			isBox=false;
			return;
		}
		//到库包裹不能删除
		var arrive_status=$(this).attr("arrive_status");
		if(arrive_status=="1"){
			isBox=false;
			return;
		}
        var status=$(this).attr("pkg_status");
        var attach_status=$(this).attr("attach_status");
        var arrive_status=$(this).attr("arrive_status");
        if(status==0 &&(attach_status ==0 || attach_status ==1)&&arrive_status==0)
        {
        	str=str+pkgId+",";
        }else
        {
        	isNeedRemoveRadio=true;
        }
    });
	if(!isBox){
      	layer.msg("所选包裹不能删除", 2, 5);
      	return;
    }
	if(isNeedRemoveRadio){
      	alert("所选包裹状态未能进行删除的已经移除");
    }
	var url = "${mainServer}/homepkg/delManyHomepkg";
	$.ajax({
		url:url,
		type:'post',
		data:{str:str},
		success:function(data){
			window.location.href = "${mainServer}/transport";
			layer.msg("删除成功", 3, 1);
		}
	});
	
}
//合并
function Merger() {       
    var packageIdArray=[];
	var logistics_codesArray=[];
	var osaddr_id ='';
	var isOnlyOne=true;
	var isBox=true;
	var needEdit=false;
	var needShowRemoveRadio=false;
	$("input[name='chkOrder']:checked").each(function(i,element) {
        var pkgId=element.value;
        var logistics_code= $.trim($(this).next().find('.waybill').text());
        
		//增值服务ID
		var attach_id=$(this).attr("attach_id");
		if(attach_id=="4"){
			isBox=false;
			return;
		}
			
		$.ajax({
			url:'${mainServer}/homepkg/packageMergeSplitCheck',
			type:'post',
			data:{packageIds:pkgId,checkException:'Y'},
			dataType:"json",
			async:false,
			success:function(data){
				if("F" == data.result){
					isBox=false;
				}
				if("Y" ==data.needEdit)
				{
					needEdit=true;
				}
				
			}			
		});
		if(!isBox){
			return;
		}
		
        //获取当前仓库id
        var currOsaddr_id=$.trim($(this).next().next().next().find('input').val());
        if(osaddr_id==''){
        	osaddr_id=currOsaddr_id;
        } 
        
        //校验包裹是否为同一海外仓库
        if(osaddr_id!=''&&osaddr_id!=currOsaddr_id){
        	isOnlyOne=false;
        	return 
        }
        
        /* 不同渠道不能合箱   */
		/* var express_package = $(this).attr("express_package");
		if(express_package == "2"){
			isBox = false;
			return;
		}  */
		 
		
        var status=$(this).attr("pkg_status");
        var attach_status=$(this).attr("attach_status");
        if(status!=0 || attach_status==2)
        {
        	needShowRemoveRadio=true;
        }else
        {
        	logistics_codesArray.push(logistics_code);
       	 	packageIdArray.push(pkgId);
        }
    });
	if(!isBox){
		if(needEdit)
		{
          	layer.msg("包裹信息不完整，无法进行分箱合箱", 2, 5);
		}else
		{
          	layer.msg("所选包裹不能参与合箱", 2, 5);	
		}
      	return;
      }		
     if(!isOnlyOne){
		layer.msg("包裹所属仓库不同，不能合并", 2, 5);
      	return;
      }
     if(needShowRemoveRadio){
			alert("所选包裹状态未能进行合并的已经移除");
      }	    
	var url='${mainServer}/homepkg/initMergePkg?package_ids='+packageIdArray.join(',')+'&logistics_codes='+logistics_codesArray.join(',')+"&osaddr_id="+osaddr_id;
	$.ajax({
			url:'${mainServer}/homepkg/sessionCheck',
			type:'post',
			dataType:"json",
			async:false,
			success:function(data){
				var status=data['status'];
				if("0" == status){
					window.location.href="${mainServer}/login";
				}else{
					$.layer({
						bgcolor: '#fff',
						type: 2,
						title: "合箱",
						shadeClose: true,
						fix: false,
						shade: [0.5, '#ccc', true],
						border: [1, 0.3, '#666', true],
						area: ['750px', '600px'],
						closeBtn: [0, true], //显示关闭按钮
						iframe: { src: url},
						success: function () {
						} 
					});
				}
			}			
		});
	
}
//返回修改收货地址信息
function updateUserAddress(str,receiver,package_id){
	$("#address"+package_id).html(str);
}
//批量支付
function payMany() {
	   if(!validateLogin()){
		   return ;
	   }
     var url = "${mainServer}/member/frontUserStatus";
     $.ajax({
         url:url,
         type:'post',
         async:false,
         dataType:'json',
         success:function(data){
                 // 账户正常
                 if(data.result =="1"){
                	 window.location.href = "${mainServer}/payment/payManyInit";
                    /*  var rechargeOffHeight = ($(window).height() - 570) / 2;
                     $.layer({
                         title :'批量支付',
                         type: 2,
                         fix: false,
                         shadeClose: true,
                         shade: [0.5, '#ccc', true],
                         border: [1, 0.3, '#666', true],
                         offset: [rechargeOffHeight + 'px', ''],
                         area: ['915px', '570px'],
                         iframe: { src: '${mainServer}/payment/payManyInit'},
                         end: function () {
                           var logisticsCodes = $("#logisticsCodes").val();
                           var payFlag = $("#payFlag").val();
                           if(logisticsCodes != null && logisticsCodes != ""){
                              $("#logisticsCodes").val("");
                               if(payFlag =="1"){
                                   
                                   window.location.href = "${mainServer}/payment/paymentList?logisticsCodes=" + logisticsCodes;
                               }else{
                                   
                                   window.location.href = "${mainServer}/payment/taxList?logisticsCodes=" + logisticsCodes; 
                               }
                              
                           }
                         }
                     }); */
                 }
                else{
                 alert(data.message);
                }
         }
     });
 }
//包裹支付状态
function showTransportPayStatus(){
	var sval=$("#payst option:selected").val();
	if(sval != -1){
		window.location.href="${mainServer}/transport?payStatus="+sval;
	}
	
}
//包裹打印状态
function showTransportPkgPrint(){
	var pval=$("#prntst option:selected").val();
	if(pval != -1){
		window.location.href="${mainServer}/transport?pkgPrint="+pval;
	}	
}
</script>
<script>
$(function(){
	$(".divService").hover(function () {
		}, function () {
		var tempid = $(this).attr("val");
		$("#divService_" + tempid).hide();
	});
});
</script>
</body>
</html>