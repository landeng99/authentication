<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0109)http://www.birdex.cn/Transport/UploadImgPage.aspx?ID=0&type=0&fileNameVal=/2015/05/20/20150520120502kg9c0.png -->
<html>
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" type="text/css" href="${resource_path}/css/common.css"/>
<link href="${resource_path}/css/syfault.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="${resource_path}/js/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>
<style>
.ke-upload-area{
	position: relative;
	overflow: hidden;
	margin: 0;
	padding: 0;
	*height: 25px;
}
.uploadify-queue{
	display:none;
}
.uploadify{
	position: absolute;
	font-size: 60px;
	top: 0;
	right: 0;
	padding: 0;
	margin: 0;
	z-index: 811212;
	border: 0 none;
	opacity: 0;
	filter: alpha(opacity=0);
	width:250px;
	height:40px;
}
</style>
</head>
<body>
<form method="post"  id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="">
</div>

    <div class="modal-dialog" id="uploadOrderImgDiv" style="border: 0px; padding: 0px; overflow-x: hidden;">
        <div class="disass" style="width: 700px; height: auto; padding-left: 15px;">
            <div class="Whole" style="width: 700px; margin-left: 13px; height: 11px; padding: 17px;">
                <h1 class="blue" style="float: left; font-size: 16px; color:#0097db;">
                    上传购物图片</h1>
                    <br>
                    <h1 class="blue" style="font-size: 13px; color:#666;">
                    温馨提示:上传的购物图需带上购物网址及购物单价</h1>
            </div>
            <div style="width: 655px; margin-left: 30px; padding-top: 18px;">
                <img src="${resource_path}/img/images-2014092401-1.jpg" style="border-left:1px solid #E1E1E1;border-right:1px solid #E1E1E1;">
            </div>
        </div>
        <div style="width: 750px; height: 70px; background-color: #fff; margin-left: 0px;
            padding-top: 0px; overflow: hidden;">
            <div style="width: 250px; height: 40px; text-align: center; margin: 18px 0px 0px 250px;display: block;">
                <a href="javascript:;" style="text-align:left;" class="notSafari">
					<div class="ke-inline-block ">
						<div class="ke-upload-area">
						<div class="ke-button-common" style="width: 250px; height: 40px; cursor: pointer; background-image: url(${resource_path}/img/uploagorderimg.png);">
							<input type="button" class="ke-button-common ke-button" value="" style="margin-right: 0px; width: 250px; height: 40px; cursor: pointer; background-image: url(${resource_path}/img/uploagorderimg.png); background-position: 0% 50%;"/></div>
						<input type="file" class="uploadify" id="ke-upload-file" name="imgFile" />
						</div>
					</div>
					<input type="file" name="fileOrderImg" id="fileOrderImg" style="display: none;"/>
				</a>
                <a href="javascript:;" class="isSafari" style="display: none;"><input type="file" id="safarifileOrderImg" name="safarifileOrderImg"></a>
            </div>
           
        </div>
	        <div style="width: 655px; margin-left: 45px;height: 0px;" id="divOrderImg">
	        	<c:forEach var="shopImg"  items="${shopImgList}">
	        	<div class="divImgOrder" val="${shopImg.img_id}" style="width: 120px; height: 120px; margin-top:5px;margin-left:8px;float:left;border:1px solid #E1E1E1;" id="divOrderImg${shopImg.img_id}">
					<div id="divDel${shopImg.img_id}" style="margin:5px 0px 0px 105px;width:15px;position:absolute;"><img alt="删除" title="删除" style="cursor: pointer; display: none;" id="delOrderImg${shopImg.img_id}" onclick="delOrderImg(${shopImg.img_id})" src="${resource_path}/img/close-x.png"/></div>
					<div style="width: 120px;height:120px;display:table-cell;vertical-align: middle;text-align: center;"><img onclick="showImg(this)" alt="" src="${resource_path}${shopImg.img_path}" id="imgOrder${shopImg.img_id}" style="cursor:pointer;width:120px;"/><input type="hidden" id="hdOrderImg${shopImg.img_id}" value="20150618154724_872.png"/></div>
				</div>
				</c:forEach>
	        </div>
	        <input type="hidden" id="count" value="${count}"/>       
        <div style="padding: 0px 0px 0px 22px; float: left; width: 646px;">
            <a href="javascript:Close()" class="lbAction modal-dialog-title-close close" rel="deactivate" style="background: transparent url(&#39;http://img.cdn.birdex.cn//images/close-x.png&#39;) no-repeat scroll;
                background-position: 50% 50%; position: fixed; right: 10px;"></a>
        </div>
    </div>
    <div id="layer_showImg" style="width:660px;height:660px;overflow:hidden;margin:0 auto;display:none;">
        <div style="width:660px; height:650px;overflow:hidden;margin:10px auto 0;display:table-cell;vertical-align: middle;text-align: center;">
            <img style="vertical-align:middle;" src="" name="layerImg" id="layerImg"/>
        </div>
        <div style="width: 660px;">
            <a class="lbAction modal-dialog-title-close close" rel="deactivate" style="background: transparent url(&#39;http://img.cdn.birdex.cn//images/close-x.png&#39;) no-repeat scroll;
                background-position: 50% 50%; position: absolute; right: 10px;"></a>
        </div>
    </div>
    </form>
<script type="text/javascript">
	var index = parent.layer.getFrameIndex(window.name);
	var package_id=${package_id};
	var count=$("#count").val();
	setHeight(count);
	 $(".divImgOrder").hover(function () {
         $("#delOrderImg" + $(this).attr("val")).show();
     }, function () {
         $("#delOrderImg" + $(this).attr("val")).hide();
     });
	$(document).ready(function() { 
	    $("#ke-upload-file").uploadify({
	        'height'        : 40,
	        'swf'           : '${resource_path}/js/uploadify.swf',
	        'uploader'      : '${mainServer}/upload/uploadTicket?package_id='+package_id,
	        'width'         : 250,
	        'fileObjName'   : 'the_files',
	        'fileSizeLimit':'1MB',
	        'fileTypeDesc':'Image Files',
	        'fileTypeExts': '*.jpg;*.png',
	        'onUploadSuccess':function(file, data, response) {
	            if(data !=""){
	            	initOrderImg(data);
	            }else{
	            	layer.msg("上传数量不能超过10", 5, 5);
	            }
	        }
	    });
	});
	function Close() {
        parent.layer.close(index);
    }
	function initOrderImg(data) {
       var strs= new Array();
       strs = data.split("^");
       //alert(strs);
       if (strs != "") {
                var htmlStr = "<div class='divImgOrder' val='" + strs[2] + "' style='width: 120px; height: 120px; margin-top:5px;margin-left:8px;float:left;border:1px solid #E1E1E1;' id='divOrderImg" + strs[2] + "'>";
                htmlStr += "<div id='divDel" + strs[2] + "' style='margin:5px 0px 0px 105px;width:15px;position:absolute;'>";
                htmlStr += "<img alt='删除' title='删除' style='cursor:pointer;display:none;' id='delOrderImg" + strs[2] + "' onclick='delOrderImg(" + strs[2] + ")' src='${resource_path}/img/close-x.png' /></div>";
               	htmlStr += "<div style='width: 120px;height:120px;display:table-cell;vertical-align: middle;text-align: center;'>";
                htmlStr += "<img onclick='showImg(this)' alt='' src='" + '${resource_path}'+strs[0] + "' id='imgOrder" + strs[2] + "' style='cursor:pointer;width:120px;' />";
                //htmlStr += "<img onclick='showImg(this)' onload=\"javascript:DrawImage(this,'120','120')\" alt='' src='" + imgUrl + "' id='imgOrder" + i + "' style='cursor:pointer;' />";
                htmlStr += "<input type='hidden' id='hdOrderImg" + strs[2] + "' value='" + strs[0] + "' /></div>";
                htmlStr += "</div>";
                $("#divOrderImg").append(htmlStr);
          }
        $(".divImgOrder").hover(function () {
            $("#delOrderImg" + $(this).attr("val")).show();
        }, function () {
            $("#delOrderImg" + $(this).attr("val")).hide();
        });
        setHeight(strs[1]);
    }
	function showImg(obj) {
        var imgUrl = obj.src.replace('-w80h50', '');
        $("#layer_showImg #layerImg").attr("src", imgUrl);
        var cleindex = $.layer({
            shade: [0.5, '#ccc', true],
            shadeClose: true,
            fadeIn: 350,
            closeBtn: [0, false], //显示关闭按钮
            type: 1,
            offset: [25, ''],
            area: ['auto', 'auto'],
            title: false,
            border: [2, 0.5, '#666', true],
            page: { dom: '#layer_showImg' },
            close: function (index) {
                layer.close(index);
            }
        });

        var new_image = new Image();
        new_image.src = imgUrl;
        if (new_image.width > 650)
            $("#layer_showImg #layerImg").css("width", 650);
        else if (new_image.width > 0)
            $("#layer_showImg #layerImg").css("width", new_image.width);
        //alert(imgUrl + "-" + new_image.width);

        $("#layer_showImg .close").click(function () {
            layer.close(cleindex);
        });
    }
	function setHeight(imgHeight){
		var heiVal = 500;
        if ((imgHeight % 5) == 0){
            heiVal += (imgHeight / 5) * 150;
        }else{
            heiVal += (parseInt(imgHeight / 5) + 1) * 150;
        }
        if (parent.layer.index > 0) {
            window.parent.setHeight(index, heiVal, imgHeight);
        }
        $("#uploadOrderImgDiv").css("height", heiVal);
    }
	function delOrderImg(img_id) {
		var url = "${mainServer}/upload/deleteTicket";
		$.ajax({
			url:url,
			type:'post',
			dataType:'json',
			async:false,
			data:{img_id:img_id},
			success:function(data){
				if(data){
					var img_id=data['img_id'];
					$("#divOrderImg"+img_id).empty();
					$("#divOrderImg"+img_id).remove();
				}
			}
		});
	}
</script>
</body>
</html>