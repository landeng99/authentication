<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!-- 全部记录 -->
<c:if test="${accountType==0}">
	<table class="table table-list table-hover">
		<colgroup>
			<col width="220">
			<col width="160">
			<col width="160">
			<col width="160">
			<col width="">
		</colgroup>
		<thead>
			<tr>
				<th>时间</th>
				<th>运单号</th>
				<th>金额</th>
				<th>税金</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="frontAccountLog" items="${recordInfoList}">
				<tr>
					<td>${frontAccountLog.opr_time_string}</td>
					<td>${frontAccountLog.logistics_code}</td>
					<td><span class="text-info"><fmt:formatNumber
								value="${frontAccountLog.amount}" type="currency"
								pattern="$#,###.##" /></span></td>
					<td><span class="text-info"><fmt:formatNumber
								value="${frontAccountLog.customs_cost}" type="currency"
								pattern="$#,###.##" /></span></td>
					<td>${frontAccountLog.description}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<!-- 消费记录 -->
<c:if test="${accountType==2}">
	<table class="table table-list table-hover">
		<colgroup>
			<col width="220">
			<col width="160">
			<col width="160">
			<col width="160">
			<col width="">
		</colgroup>
		<thead>
			<tr>
				<th>时间</th>
				<th>运单号</th>
				<th>金额</th>
				<th>税金</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="frontAccountLog" items="${recordInfoList}">
				<tr>
					<td>${frontAccountLog.opr_time_string}</td>
					<td>${frontAccountLog.logistics_code}</td>
					<td><span class="text-info"><fmt:formatNumber
								value="${frontAccountLog.amount}" type="currency"
								pattern="$#,###.##" /></span></td>
					<td><span class="text-info"><fmt:formatNumber
								value="${frontAccountLog.customs_cost}" type="currency"
								pattern="$#,###.##" /></span></td>
					<td>${frontAccountLog.description}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<!-- 充值记录 -->
<c:if test="${accountType==1}">
	<table class="table table-list table-hover">
		<colgroup>
			<col width="220">
			<col width="160">
			<col width="">
		</colgroup>
		<thead>
			<tr>
				<th>时间</th>
				<th>金额</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="frontAccountLog" items="${recordInfoList}">
				<tr>
					<td>${frontAccountLog.opr_time_string}</td>
					<td><span class="text-info"><fmt:formatNumber
								value="${frontAccountLog.amount}" type="currency"
								pattern="$#,###.##" /></span></td>
					<td>${frontAccountLog.description}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<!-- 提现记录 -->
<c:if test="${accountType==3}">
	<table class="table table-list table-hover">
		<colgroup>
			<col width="220">
			<col width="160">
			<col width="">
		</colgroup>
		<thead>
			<tr>
				<th>时间</th>
				<th>金额</th>
				<th>状态</th>
				<th>操作</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="frontAccountLog" items="${recordInfoList}">
				<tr>
					<td>${frontAccountLog.opr_time_string}</td>
					<td><fmt:formatNumber value="${frontAccountLog.amount}"
							type="currency" pattern="$#,###.##" /></td>
					<td><c:if test="${frontAccountLog.status==1}">申请中</c:if> <c:if
							test="${frontAccountLog.status==2}">完成</c:if> <c:if
							test="${frontAccountLog.status==3}">拒绝</c:if> <c:if
							test="${frontAccountLog.status==4}">已取消</c:if></td>
					<td><c:if test="${frontAccountLog.status==1}">
							<a class="blue" href="javascript:void(0);"
								onClick="cancel('${frontAccountLog.log_id}')"
								style="cursor: pointer;">取消</a>
						</c:if></td>
					<td>${frontAccountLog.description}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<!-- 索赔记录 -->
<c:if test="${accountType==4}">
	<table class="table table-list table-hover">
		<colgroup>
			<col width="220">
			<col width="160">
			<col width="">
		</colgroup>
		<thead>
			<tr>
				<th>时间</th>
				<th>运单号</th>
				<th>金额</th>
				<th>备注</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="frontAccountLog" items="${recordInfoList}">

				<tr>
					<td>${frontAccountLog.opr_time_string}</td>
					<td>${frontAccountLog.logistics_code}</td>
					<td><span class="text-info"><fmt:formatNumber
								value="${frontAccountLog.amount}" type="currency"
								pattern="$#,###.##" /></span></td>
					<td>${frontAccountLog.description}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>

<!-- tab内容 end -->
<div class="page-list">
	<jsp:include page="paper.jsp"></jsp:include>
	<script type="text/javascript">
		function pagerClick(index) {
			//业务查询
			ajaxRecord(index);
		}
	</script>
</div>