<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>

</head>
<body>

	<jsp:include page="topIndexPage.jsp"></jsp:include>
	<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/subPage.css"></link>
	<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/p-set.css"></link>
	
	<div class="wrap-box pb20 pt20 clearfix">
        <ul class="breadcrumb">
            <li><a><i class="iconfont icon-shouye"></i>首页</a></li>
            <li class="active"><span>费用相关</span></li>
        </ul>
        <div class="clearfix" id="s-content">
            
        </div>
    </div>

	<jsp:include page="footer-1.jsp"></jsp:include>
	 
	<script type="text/javascript">
	$(function () {
		queryLeftIndexPage();
	});
	
	function queryLeftIndexPage() {
		$.ajax({
			type: "post",
			dataType: "html",
			url: '${mainServer}/queryLeftIndexPage',
			data: {},
			success: function (data) {
				$("#s-content").empty();
				$("#s-content").append(data);
			}
		});
	}
	</script> 
</body>
</html>