<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="notice">
	 <div class="mian">
		  <div class="notice-fi" style="float:left;margin-top:9px;">通知：</div><br/>
		  <ul class="guendong" style="padding-left:10px;">
		  <c:forEach items="${noticeLists}" var="notice">
		 	 <li>${notice.title }<a href="${mainServer}/noticeInfo?noticeId=${notice.noticeId}" style="padding-left:15px;color:#CFC;text-decoration:none;">查看详情</a></li>
		  </c:forEach>
		  </ul>
	 </div>
</div>

<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?af1ee61b096f3dd3dba5d363741cc417";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>