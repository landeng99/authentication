<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<jsp:include page="../common/commonCss.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/news.css"> <!-- 前店公共框架样式 头部 -->

</head>
<body>

 	<!-- 头部页面 -->
	<jsp:include page="topIndexPage.jsp"></jsp:include>
	
	<div class="min-banner">
        <div class="bgImg"></div>
        <!-- <div class="logo"></div> -->
        <span class="txt">
            	新闻公告
        </span>
    </div>
    
    <!-- 内容 -->
    <div class="wrap bg-gray">
        <div class="wrap-box pt20 pb20">
            <div class="newBox">
            	<h1>${noticePojo.title}</h1>
                   <p class="submitted">
                	<span class="pr20">发布人：<cite title="admin">admin</span> 
                	<time class="pr20" datetime="2017-08-09T11:01:01+08:00">
                	<fmt:parseDate value="${noticePojo.create_time}" pattern="yyyy-MM-dd HH:mm:ss" var="receiveDate"></fmt:parseDate>
       					<fmt:formatDate value="${receiveDate}" pattern="yyyy-MM-dd HH:mm:ss" ></fmt:formatDate>
                	</time>
              	 </p>
              	 <em class="intro">${noticePojo.tt_profile}</em>
              	 <div class="con">${noticePojo.context}</div>
                
                <!-- 更多公告信息 -->
                <div class="newList">
                    <h2 class="tit"><a href="">更多公告</a></h2>
                    <ul class="newList-ul list-unstyled clearfix">
	                    <c:forEach items="${noticeInfoList}" var="noticeInfo" varStatus="status">
		                    <c:if test="${status.count <= 6}">
			                     <li>
			                     	<a href="${mainServer}/noticeInfoList?noticeId=${noticeInfo.noticeId}">
			                     		${noticeInfo.title}
			                     	</a>
			               		</li>
		               		</c:if>
						</c:forEach>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <!-- 底部页面 -->
	<jsp:include page="bottomPage.jsp"></jsp:include>

</body>
</html>	
	

<%-- <div class="banner01 bannerpic01"><div class="wrap"><h1>新闻公告</h1><p>速8快递为您提供安全快捷的转运服务</p></div></div>
<div class="op"><div class="wrap"><i class="ico16">&nbsp;</i>网站首页 > 新闻公告</div></div>
<div class="wrap" style="padding:50px 0px;">
	<div class="detail">
   	  <div class="detail-title">${noticePojo.title}</div>
      <div class="detail-time">来源：admin    时间：${noticePojo.effectTime}</div>
        <div class="">
        ${noticePojo.context}
        </div>
  </div>
  </div>
 <jsp:include page="footer-1.jsp"></jsp:include> --%>