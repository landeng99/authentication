<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0021)http://www.birdex.cn/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/syembgo.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
<script type="text/javascript" src="${resource_path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
</head>
<body>

<jsp:include page="headNotice.jsp"></jsp:include>
<jsp:include page="headNavigation.jsp"></jsp:include>

<div class="mian">

		  <div  style="float:left;font-size: 20px;padding-top: 10px;">
			<c:if test="${preId!=-1}">
			  <a href="${mainServer}/consultShow?consult_id=${preId}"  style="color:#EF5E78;">上一篇</a>
		  </c:if>
		  </div>
		  <div  style="float:right;font-size: 20px;padding-top: 10px;">
			<c:if test="${netId!=-1}">
			  <a href="${mainServer}/consultShow?consult_id=${netId}" style="color:#EF5E78;">下一篇</a>
		  </c:if>
		  </div>
<div style="border-top:3px solid #2473ba;">
				<div style=" padding-top: 30px;"><span style="font-size: 20px;font-weight: bold;padding-left: 250px;">${consultInfo.consult_title}</span></div>
				<div>
				<table style="width: 100%;">
				<tr>
					<td style="text-align: center;">
					       			<a href="${consultInfo.forward_url}">
						<img src="${resource_path}/${consultInfo.display_image}"/>
				</a>
					</td>
				</tr>
				</table>

				</div>
				<div style="height:450px;">
					${consultInfo.consult_detail}
				</div>
</div>
</div>
 
 <jsp:include page="footer.jsp"></jsp:include>
 
<div class="tip_showMore" style="width: 200px; height: auto; font-size: 13px; overflow: visible; position: absolute; left: 420px; top: 1137px; display: none;" id="reciveDiv"></div>


<script>

    function pageX(elem) {
        return elem.offsetParent ? (elem.offsetLeft + pageX(elem.offsetParent)) : elem.offsetLeft;
    }

    function pageY(elem) {
        return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
    }

    $(document).ready(function () {
        $("#divItemDetail .close").click(function () {
            $.BDEX.Close();
            $('#divItemDetail').hide();
        });

        $("a.showMore").mouseover(function () {
            var id = $(this).attr("value");
            var str = $("#hdItemDetail_" + id).val();

            var elem = $(this)[0];
            var left = pageX(elem);
            var top = pageY(elem);

            var showHTML = "<div style='word-wrap:break-word;word-break:keep-all;'>" + str + "</div><div></div>";
            //======当前浏览器窗口宽度===========
            var windowWidth = $(window).width();
            if (left + 70 + 200 > windowWidth) {
                $("#reciveDiv").html(showHTML).addClass("right").css("position", "absolute").css("left", left - 242).css("top", top - 45).show();
            } else {
                $("#reciveDiv").html(showHTML).addClass("left").css("position", "absolute").css("left", left + 70).css("top", top - 45).show();
            }
        }).mouseout(function () {
            $("#reciveDiv").removeClass("left").removeClass("right").html("").hide();
        });
    });
</script>
</body>
</html>