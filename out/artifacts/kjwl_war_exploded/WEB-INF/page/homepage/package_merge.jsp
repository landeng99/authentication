<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="overflow: hidden;">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
	<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
	<link href="${resource_path}/css/sydialog.css" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
	<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
	<script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script>	
	<script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>
<style type="text/css">
.logistics_code_context{padding-left:25px;width: 600px;font-size: 15px;}
.logistics_codes{  
	margin-top:0px;
	height:250px;
	overflow-y: scroll;
	border:1px solid #ccc;
	width:700px; 
}
.logistics_codes li{
	float: left;
	list-style: none;
	margin:5px;
	/*margin-right:50px;*/
	padding: 3px 5px;
	text-align: center;
	min-width: 10px;
	border: 1px solid #ccc;
}
 #logistics_codes li{float: left;font-size: 15px;}
 </style>
</head>
<body>    
<div style="padding-top:20px;">
	<input type="hidden" value="${package_ids}" id="package_ids"/>
	<input type="hidden" value="${osaddr_id}" id="osaddr_id"/>
	<div>
		<ul>
			<li style="float:left;margin-left: 200px;"><input type="text" value="请输入运单号或关联单号" style="width:250px;height:32px;" id="inputpkg" onFocus="if (this.value==this.defaultValue) this.value='';"  onblur="if (this.value=='') this.value=this.defaultValue;"/></li>
			<li style="float:left;margin-left: 30px;"><input type="button" value="查询" style="width:64px;height:36px;" id="searchpkg"/></li>
		</ul>
	</div>
	<div id="msg" style="color:#F92312;clear:both;text-align: center;">
		
	</div>
	<div class="Whole" id="addressid" style="margin-bottom:10px;clear: both;">
		<div class="logistics_code_context"><span>待合并包裹<b>（系统只支持最多3个包裹合箱）</b>:</span>
			<ul id="logistics_codes" class="logistics_codes">
				
			</ul>
		</div>
		<div class="clear"></div>
		<div style="margin-top: 20px;">
			<div>
			<select id="address_id" name="address_id" style="margin-left: 25px;border:1px solid #ccc;height:42px;width: 400px;">
				<option value="0">--------------------------------选择收货地址----------------------------------------------</option>
				<c:forEach var="address" items="${addressList}">
					<option value="${address.address_id}">${address.addressStr}|${address.receiver}|${address.mobile}</option>
				</c:forEach>
			</select>
			</div>
			<div>
				<input type="text" id="inName" placeholder="请输入姓名或手机号..."style="width: 120px;float: left;margin-top: -35px;height: 30px;margin-left: 450px;"/>
				<button type="button" id="searchAddress" onclick="searchAddress()" style="float: left;margin-top: -30px;margin-left: 600px;width: 60px;height: 30px;">查询</button>
			</div>
		</div>
		<div class="PushButton" style="margin-top: 40px;">
				<a class="Block input14 white" href="javascript:save()" style="margin-right:30px;" id="saveButton_id">保存</a> 
				<a style="text-decoration: none;
					cursor: pointer; color: #666; width: 70px; font-family: &#39;微软雅黑&#39;; background-image: url(http://img.cdn.birdex.cn/images/input36.png);
					text-align: center;" class="button" id="cancelNew" href="javascript:close()">取 消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	var logistics_codes='${logistics_codes}';
	var warehouse='${warehouse}';
	var package_ids='${package_ids}';
	var osaddr_id=$("#osaddr_id").val();
	if(logistics_codes!=""){
		var packageArry=new Array();
		packageArry=package_ids.split(',');
		var array=logistics_codes.split(',');
		$.each(array,function(i,str){
			var ul=$("#logistics_codes");
			var li=$("<li id=\"pakg"+packageArry[i]+"\"></li>");
			li.append("<span>运单号:"+str+"</span>");
			li.append("<span style=\"margin-left:10px;\">仓库:"+warehouse+"</span>");
			li.append("<input type=\"hidden\" value=\'"+osaddr_id+"\'/>");
			li.append("<a href=\"javascript:;\" class=\"del\" style=\"cursor:pointer;margin-left:10px;\">删除</a>");
			ul.append(li);
		});
	}
	
	$("#searchpkg").click(function(){
		var logistics_code=$.trim($("#inputpkg").val());
		var url = "${mainServer}/homepkg/searchpkgBylogistics";
		$.ajax({
			url:url,
			type:'post',
			dataType:'json',
			async:false,
			data:{'logistics_code':logistics_code},
			success:function(data){
				$("#msg").html("");
				var msg=data['msg'];
				if(msg=="2"){
					window.parent.location.href="${mainServer}/login";
				}else{
					var package_id=data['package_id'];
					var logistics_code=data['logistics_code'];
					var warehouse=data['warehouse'];
					var osaddr_id=data['osaddr_id'];
					var isOnlyPkg=true;
					var $lis=$('#logistics_codes').find('li');
					$.each($lis,function(i,li){
						var id=$(li).attr('id').replace("pakg","");
						if(package_id==id){
							isOnlyPkg=false;
					        return;
						}
					});
					if(!isOnlyPkg){
						layer.msg("该包裹已在列表中", 2, 5);
						return;
					}
					if(msg=="Ok"){
						var ul=$("#logistics_codes");
						var li=$("<li id=\"pakg"+package_id+"\"></li>");
						li.append("<span>运单号:"+logistics_code+"</span>");
						li.append("<span style=\"margin-left:10px;\">仓库:"+warehouse+"</span>");
						li.append("<input type=\"hidden\" value=\""+osaddr_id+"\"/>");
						li.append("<a href=\"javascript:;\" class=\"del\" style=\"cursor:pointer;margin-left:10px;\">删除</a>");												
						ul.append(li);
					}else{
						$("#msg").html(msg);
					}
				}
			}
		});
	});
})

//提交
function save(){
	var address_id=$("#address_id").val();
	if(address_id==""){
		layer.msg("请选择收货地址", 2, 5);
		return;
	}
	var osaddr_id ='';
	var isOnlyOne=true;
	var package_ids=[];
	var $lis=$('#logistics_codes').find('li');
	$.each($lis,function(i,li){
		var id=$(li).attr('id').replace("pakg","");
		//获取当前仓库id
		var currOsaddr_id=$.trim($(li).find('input').val());
		if(osaddr_id==''){
        	osaddr_id=currOsaddr_id;
        }
		//校验包裹是否为同一海外仓库
        if(osaddr_id!=''&& osaddr_id!=currOsaddr_id){
        	isOnlyOne=false;
        	return 
        }
		package_ids.push(id);		
	});
	if(!isOnlyOne){
		layer.msg("包裹所属仓库不同，不能合并", 2, 5);
        return;
      }	
	if(package_ids.length==0){
		layer.msg("请搜索需要合箱的包裹", 2, 5);
		return;
	}
	if(package_ids.length==1){
		layer.msg("合箱包裹至少需要两个", 2, 5);
		return;
	}
	if(package_ids.length>3){
		layer.msg("合箱包裹不能超过3个", 2, 5);
		return;
	}
	
	$('#saveButton_id').attr("href","#");
	var url = '${mainServer}/homepkg/mergepkg';
	$.ajax({
		url:url,
		type:'post',
		dataType:'json',
		async:false,
		data:{package_ids:package_ids.join(","),address_id:address_id,osaddr_id:osaddr_id},
		success:function(data){
			$('#saveButton_id').attr("href","javascript:save()");
			var msg=data['msg'];
			if(msg=="2"){
				window.location.href="${mainServer}/login";
			}else{
				
				if(data['result']){
					layer.msg("包裹合并成功", 3, 1);
					  //刷新父窗口的页面
					  window.parent.location.href=window.parent.location.href;				 
				}else
				{
					layer.msg(msg, 2, 5);
				}
			}
			
		}
	});
}
function close(){
	window.parent.layer.closeAll();
}

/*var $lis=$('#logistics_codes').find('li');
$.each($lis,function(i,li){
	var id=$(li).attr('id');
	
	
});*/
//删除包裹
$('.del').live('click',function(){
	var $li=$(this).parent();
	$li.remove();
});

/* 根据收件人或手机查询用户地址    */
function searchAddress(){
	var inName = $("#inName").val();
	var url = "${mainServer}/homepkg/searchAddress";
	$.ajax({
		url:url,
		type:'post',
		data:{
			"inName":inName,
		},
		datatype:"json",
		async:false,
		success:function(data){
			if(data.flag == true){
				$("#address_id").val(data.address_id);
			}
		}
	});
}

/* 查询后清除数据   */
$(function(){
	$("#searchAddress").click(function(){
		$("#inName").val("");
	});
});
</script>
</body>
</html>