<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <title>SU8-个人中心-充值</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <jsp:include page="../common/commonCss.jsp"></jsp:include> <!-- 前店公共框架样式 头部 -->
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/p-pay.css"> 
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
</head> 
<body>
	<!-- 头部页面  -- 新的支付界面 -->
	<jsp:include page="topPage.jsp"></jsp:include> 
	<!-- banner -->
	<div class="min-banner">
	    <div class="bgImg"></div>
	    <div class="logo"></div>
	</div> 
	<!-- 内容 -->
	<div class="wrap bg-gray"> 
	      <!-- 运费-->
	    <input type="hidden" id="total" value="<fmt:formatNumber value='${total+0.00001}' type='currency' pattern='#.##'/>">
        <input type="hidden" id="logisticsCodes" value="${logisticsCodes}">
        <input type="hidden" id="taxtlogisticsCodes" value="${taxtlogisticsCodes}">
        
        <input type="hidden" id="pay_type" value="1">
          <!-- 运费+税金-->
        <input type="hidden" id="actualPay" value="<fmt:formatNumber value='${alltotal+0.00001}' type='currency' pattern='#.##'/>"> 
          <!-- 税金-->       
        <input type="hidden" id="totalTax" value="<fmt:formatNumber value='${totalTax+0.00001}' type='currency' pattern='#.##'/>">   
        <div class="wrap-box pb20 pt20 clearfix">
             <!-- 右边内容-->
          <div class="p-content">
                <ul class="breadcrumb">
                    <li><a><i class="iconfont icon-shouye"></i>我的个人中心</a></li>
                    <li class="active"><span>支付</span></li>
                </ul>
                <div class="pl20 pr20 pay">
                    <from class="form-horizontal ">
                    <div class="p10 bg-gray mb20">
                       <div class="form-group">
                           <label for="" class="col-sm-2 control-label">账户余额：</label>
                           <div class="col-sm-1">
                                 <span class="txt"><strong class="text-info p5 "><fmt:formatNumber value='${frontUser.balance}' type='currency' pattern='$#,###.##'/></strong></span>
                           </div>
                           <label for="" class="col-sm-2 control-label">可用余额：</label>
                           <div class="col-sm-1">
                                 <span class="txt pr10"><strong class="text-success p5 "><fmt:formatNumber value='${frontUser.able_balance}' type='currency' pattern='$#,###.##'/></strong></span>
                           </div>
                           <label for="" class="col-sm-2 control-label">冻结金额：</label>
                           <div class="col-sm-1">
                                 <span class="txt"><strong class="p5 "><fmt:formatNumber value='${frontUser.frozen_balance}' type='currency' pattern='$#,###.##'/></strong></span>
                           </div>
                       </div> 
                       </div>

                        <h5 class="tit">订单列表</h5>
                        <div class="alert  alert-warning col-sm-12 ">
                             <i class="glyphicon glyphicon-info-sign pr5"></i>
                                                                                                       账户可用余额不足时，需要支付宝支付不足部分；如果充值后账户余额不正确，请刷新页面。
                         </div>
                        <c:if test="${logisticsCodes!=null && logisticsCodes!=''}">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">运费单号：</label>
                            <div class="col-sm-8 sport"> 
                            	<c:forEach var="coupon"  items="${logisticsCodesList}">
                            	            <span class="txt pr5">${coupon}</span> 
 								</c:forEach>
                            </div>
                        </div>   
                        	</c:if> 
                        <c:if test="${taxtlogisticsCodes!=null && taxtlogisticsCodes!=''}">
                         <div class="form-group">
                            <label for="" class="col-sm-2 control-label">关税单号：</label>
                            <div class="col-sm-8">
                            <c:forEach var="coupon"  items="${taxtlogisticsCodesList}">
                            	            <span class="txt pr5">${coupon}</span> 
 							</c:forEach>
                             </div>
                        </div>
                        	</c:if> 
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">合计：</label>
                             <div class="col-sm-7 txt" style="margin-left: -15px;">
                                   <span>运费<strong class="text-info p5"><fmt:formatNumber value="${total+0.00001}" type="currency" pattern="$#,###.##"/></strong></span>
                                   <span class="p10">+</span>
                                   <span>税率<strong class="text-info p5"><fmt:formatNumber value="${totalTax+0.00001}" type="currency" pattern="$#,###.##"/></strong></span>  
                                   <span class="p10">=</span>  
                                   <span >  
                                   <strong class="text-danger"><fmt:formatNumber value="${alltotal+0.00001}" type="currency" pattern="$#,###.##"/></strong>
                                   </span>
                                    <c:if test="${message!=null}">
                                   <span class="text-warning  f12 pl10"> <i class="glyphicon glyphicon-info-sign pr5"></i>账户可用余额不足！</span>
                                   </c:if> 
                           </div>  
                        </div>
                        
                          <div class="form-group">
                            <label for="" class="col-sm-2 control-label">可用优惠券：</label>
                            <div class="col-sm-10">
                                    <div class="col-sm-4" style="margin-left: -15px;">
                                        	<select id="coupon_id" name="coupon_id" onchange="selectCoupon()" class="form-control">
											<option value="-1">--</option>
											<c:forEach var="coupon"  items="${avliableCouponList}">
											<option value="${coupon.coupon_id}">${coupon.coupon_code} | ${coupon.coupon_name}| <fmt:formatNumber value="${coupon.denomination+0.00001}" type="currency" pattern="$#,###.##"/></option>
											</c:forEach>
											</select>
                                    </div> 
                            </div>
                        </div>
                    </from>
                    <hr>
                    <div class="text-center pb20">
                          <a class="btn btn-primary2 btn-p-radius"   id="return" onclick="window.history.back(-1); return false" >返回上一页</a> 
                        <button class="btn btn-primary btn-p-radius" id="pay">支付</button>
                    </div> 
                </div>
            </div>
             <!-- 右边内容-->
            <!-- 左侧菜单 -->
  			<jsp:include page="leftPage.jsp"></jsp:include>
          </div>
            <!-- 支付提示 弹出框 -->
<div class="modal fade" id="prompt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;top:-10px;overflow: scroll !important;" >
  <div class="modal-dialog" role="document" style="width:400px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">支付提示</h4>
      </div>
      <div class="modal-body">
              <!-- 内容 -->
              <div class="blackfont text-center p20"> 
                     <a  onclick="reflesh()"   href="javascript:void(0)" class="btn btn-primary  ok" style="margin-left: 15px;" >重新支付</a>
                    <a   onclick="self.location=document.referrer;"  href="javascript:void(0)" class="btn btn-primary  ok"  style="margin-left: 15px;"  >付款成功</a>
              </div> 
      </div>
    </div>
  </div>
</div>
    </div>
    
	<!-- 底部页面 -->
	<jsp:include page="bottomPage.jsp"></jsp:include>
	
	<script type="text/javascript" src="${resource_path}/font/iconfont.js"></script>
	<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="${resource_path}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/util/util_new.js"></script>  
	 <script type="text/javascript">  
	 // 支付成功，跳转回个人中心页
  function  goback(){
		 javascript:history.back();
	     location.reload();
	 } 
	$("#pay").click(function () {
		//运费
 	    var totalsport = $('#total').val();
		//税金
 	   var totaltax = $('#totalTax').val();
	    var logisticsCodes =$('#logisticsCodes').val();
	    var taxtlogisticsCodes = $('#taxtlogisticsCodes').val();
	    var coupon_id=$('#coupon_id').val();
	    var actualPay=$('#actualPay').val();
	       var url="${mainServer}/payment/payment";
	        $.ajax({
	            url:url,
	            data:{
	            	"sportotal":totalsport,
	            	"totaltax":totaltax,
	                "total":actualPay,
	                "coupon_id":coupon_id,
	                "logisticsCodes":logisticsCodes,
	                taxtlogisticsCodes:taxtlogisticsCodes
	                },
	            type:'post',
	            async:false,
	            dataType:'json',
	            success:function(data){
	                if(data.result ==1|| data.result ==3 || data.result ==6 ){
	                	confirm(function(){
	                		window.location.href ="${mainServer}/transport";
	                	}, "", data.message);
	                  /*  alert("这里需要一个提示框");
	               /*     layer.alert(data.message,function(){ */
	                    //  window.location.href ="${mainServer}/transport";
	                  // } ); */
	                }else if(data.result ==2){
	                	//部分已支付成功，剩余部分使用线上支付
	                  var alowpay = data.alowpay;
 	                   selectPayType(alowpay,logisticsCodes,coupon_id);
	                }
	                else {
	                	confirm(function(){
	                		window.location.href ="${mainServer}/exit";
	                	}, "", data.message);
	                	// alert("这里需要一个提示框");
	                   // layer.alert(data.message,function(){
	                      //  window.location.href ="${mainServer}/exit";
	                   // });
	                }
	            }
	        });

	    });
	//包裹状态
	function selectPayType(actualPay,logisticsCodes,coupon_id){    
		  window.location.href="${mainServer}/payment/selectPayType_new?total="+actualPay; 
		// window.open('${mainServer}/payment/selectPayType_new?total='+total);
		//出一个弹框
	     /*    var rechargeOffHeight = ($(window).height() - 650) / 2;
	       $.layer({
	            title :'支付方式选择',
	            type: 2,
	            fix: false,
	            shadeClose: true,
	            shade: [0.5, '#ccc', true],
	            border: [1, 0.3, '#666', true],
	            offset: [rechargeOffHeight + 'px', ''],
	            area: ['950px', '650px'],
	            iframe: { src: '${mainServer}/payment/selectPayType?total='+total+'&logisticsCodes='+logisticsCodes+'&flag='+1+'&coupon_id='+coupon_id},
	            end: function (data) {
	                var height = ($(window).height() - 430) / 2;
	                $.layer({
	                    title :'支付确认',
	                    type: 2,
	                    fix: true,
	                    closeBtn: false,
	                    shadeClose: false,
	                    shade: [0.8, '#ccc', true],
	                    border: [1, 0.3, '#666', true],
	                    offset: [height + 'px', ''],
	                    area: ['520px', '230px'],
	                    iframe: { src: '${mainServer}/payment/payConfirm?couponLogistics_code='+logisticsCodes+'&coupon_id='+coupon_id},
	                    end: function (data) {
	                        window.location.href ="${mainServer}/transport"
	                   }
	                });
	               }
	        }); */ 
	}

	//包裹状态
	function showTransportStatus(status){
		
		window.location.href="${mainServer}/transport?status="+status;
	}
	//所有状态的包裹
	function showAllTransport(){
		window.location.href="${mainServer}/transport";
	}
	//包裹支付状态
	function showTransportPayStatus(payStatus){
		
		window.location.href="${mainServer}/transport?payStatus="+payStatus;
	}

	function selectCoupon(){
	    var total =$('#actualPay').val();
	    var coupon_id=$('#coupon_id').val();
	    var url="${mainServer}/payment/selectCouponCountLeftPay";
	    $.ajax({
	        url:url,
	        data:{
	            "total":total,
	            "coupon_id":coupon_id},
	        type:'post',
	        async:false,
	        dataType:'json',
	        success:function(data){
	        	$('#leftPay').html(data.leftPay);
	        	$('#actualPay').val(data.actualPay);
	        }
	    });
	}
      </script>
</body>
</html>