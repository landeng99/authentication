<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>优惠券</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/common.js"></script>
    <script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script> 
	
	<!-- 新增的css -->
	<jsp:include page="../common/commonCss.jsp"></jsp:include>
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css"> <!-- 前店公共框架样式 头部 -->
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/dropify/dist/css/dropify.min.css"> <!-- 图片上传控件样式 --> 
	<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/p-coupons.css"> <!-- 前店公共框架样式 头部 -->
	
</head>
<body>
	<!-- 头部页面 -->
	<jsp:include page="topPage.jsp"></jsp:include>
	
	<!-- banner -->
    <div class="min-banner">
        <div class="bgImg"></div>
        <div class="logo"></div>
        <!-- <span class="txt">
            SU8-个人中心-优惠券
        </span> -->
    </div>
    
    <!-- 内容 -->
    <div class="wrap bg-gray">
        <div class="wrap-box pb20 pt20  clearfix">
            <div class="p-content">
                <div class="bg-white  clearfix">
                    <div class="container-fluid m20 ">
                        <div class="alert  alert-info row ">
                            <div class="col-sm-10">优惠券：<span class="amount-coupons">${countAll}</span>张</div>
                            <!-- <div class="col-sm-2 text-right"><a style="text-decoration: underline;" href="#">如何获得优惠券？</a></div> -->
                        </div>
                         <div class="alert  alert-warning row">
                            <div class="col-sm-12">
                                <p><strong>优惠券说明：</strong></p>
                                <p>
                                    1.只有单个包裹进行支付时才能使用优惠券，批量支付不支持使用优惠券；<br>
                                    2.一个包裹只能使用一张优惠券<br>
                                    3.到期后的优惠券将会无效且无法使用；<br>
                                    4.优惠券最终解释权归速8快递所有。
                                </p>
                            </div>
                         </div>
                     </div>
                </div>
                <div class="bg-white mt20  clearfix">
                    <div class="m20 ">
                        <h4 class="tit">优惠券<small class="amount-coupons-small">共<span class="">${countAll}</span>张</small></h4>
                        
                        <div class="tab-box">
                           <!-- tab -->
                           <ul class="nav nav-tabs " role="tablist">
                               <li class="active">
                                    <a href="#one" data-toggle="tab" role="tab" aria-expanded="true">  
                                       <span>可使用优惠券</span>
                                   </a>
                               </li>
                               <li class="">
                                   <a href="#two" data-toggle="tab" role="tab" aria-expanded="false">
                                       <span>已使用优惠券</span>
                                   </a>
                               </li>
                               <li class="">
                                   <a href="#three" data-toggle="tab" role="tab" aria-expanded="false">
                                       <span>已过期优惠券</span>
                                   </a>
                               </li>
                           </ul>
                           <!-- tab end-->
                           
                        	<!-- tab内容 -->
                           <div class="tab-content p20">
                                <div role="tabpanel" class="tab-pane active" id="one">
			                        <ul class="coupons-ul">
			                        	<!-- 生效优惠券 -->
				                        <c:forEach var="couponUsed" items="${avliableCouponList}">
				                        	 <c:forEach begin="1" end="${couponUsed.quantity}" step="1">
						                         <li class="item">
					                                <div class="c-box">
					                                    <div class="title">
					                                    	<%-- <fmt:formatNumber value="${couponUsed.denomination}" type="currency" pattern="$#,###.00"/>元 --%>
					                                    	${couponUsed.denomination}$
					                                    	${couponUsed.coupon_name}
					                                    </div>
					                                    <div class="rule">无使用规则</div>
					                                    <div class="triangle"></div>
					                                    <div class="bg">
					                                        <i class="iconfont icon-quan"></i>
					                                    </div>
					                                </div>
					                                <div class="triangle2"></div>
					                             </li>
				                             </c:forEach>
				                        </c:forEach>
			                        </ul>
			                  </div>
			                  <div role="tabpanel" class="tab-pane" id="two">
                                   <ul class="coupons-ul">
                                    	<!-- 已使用优惠券 -->
				                        <c:forEach var="couponUsed" items="${usedCouponList}">
				                        	 <c:forEach begin="1" end="${couponUsed.quantity}" step="1">
						                         <li class="item">
					                                <div class="c-box">
					                                    <div class="title">
					                                    	<%-- <fmt:formatNumber value="${couponUsed.denomination}" type="currency" pattern="$#,###.00"/> --%>
					                                    	${couponUsed.denomination}$
					                                    	${couponUsed.coupon_name}
					                                    </div>
					                                    <div class="rule">无使用规则</div>
					                                    <div class="triangle"></div>
					                                    <div class="bg">
					                                        <i class="iconfont icon-quan"></i>
					                                    </div>
					                                </div>
					                                <div class="triangle2"></div>
					                             </li>
				                             </c:forEach>
				                        </c:forEach>
			                        </ul>
                              </div>
                              <div role="tabpanel" class="tab-pane" id="three">
                              		<ul class="coupons-ul">
				                        <!-- 失效过期优惠券 -->
				                        <c:forEach var="couponUsed" items="${expiredCouponList}">
				                        	 <c:forEach begin="1" end="${couponUsed.quantity}" step="1">
						                         <li class="item disabled">
					                                <div class="c-box">
					                                    <div class="title">
					                                    	<%-- <fmt:formatNumber value="${couponUsed.denomination}" type="currency" pattern="$#,###.00"/>元 --%>
					                                    	${couponUsed.denomination}$
					                                    	${couponUsed.coupon_name}
					                                    </div>
					                                    <div class="rule">无使用规则</div>
					                                    <div class="triangle"></div>
					                                    <div class="bg">
					                                        <i class="iconfont icon-quan"></i>
					                                    </div>
					                                </div>
					                                <div class="triangle2"></div>
					                             </li>
				                             </c:forEach>
				                        </c:forEach>
		                       		</ul>
                              </div>
                          </div>
                          </div>
                    </div>
                </div>
            </div>
            <!-- 左侧页面 -->
			<jsp:include page="leftPage.jsp"></jsp:include>
        </div>
    </div>
    
    <!-- 底部页面 -->
	<jsp:include page="bottomPage.jsp"></jsp:include>
	
	<script src="../font/iconfont.js"></script>
    <script src="../jquery-1.11.3/jquery.min.js"></script>
    <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</body>
</html>