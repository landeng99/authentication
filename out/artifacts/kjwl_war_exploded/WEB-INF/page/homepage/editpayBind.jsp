<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">
p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}


.mg18 {
    margin-bottom: 18px;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
}
.hr {
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.List06 {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.List06 li {
    height: 52px;
    line-height: 32px;
    list-style-type:none
}
.List06 li span.pull-left {
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: right;
    width: 63px;
}
.List06 li input {
    margin-right: 4px;
}
.List06 li .text08 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 178px;
}

.List06 li .text09 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 78px;
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 270px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button2.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 72px;
}

.span4 .label{
     width:100px;
     float: left;
}

</style>
<body>
<div style="background-image:none;border-right:none;width:350px;padding-top:5px;"/>
<input type="hidden" id="checkMobileResult" value="1">
<input type="hidden" id="mobile" value="${old_mobile}">
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
        <div>
            <div class="hr mg18"></div>
            <div style="display: block;" id="withdrawal">
                <ul class="List06">
                	  <li class="span4">
                         <p>
                            <span class="label">原绑定支付宝号：</span>
                            <span>${pay_treasure}</span>
                            <input type="hidden" value="${pay_treasure}"/>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label">新绑定支付宝号：</span>
                            <input type="text" class="text08" id="payBind" name="pay_treasure" style="padding-left: 5px;">
                            <span style="color:Red;">*</span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label">验证码：</span>
                            <input type="text" class="text09" id="code" name="code" style="padding-left: 5px;">
                            <span style="color:Red;">*</span>
                            <input type="button" value="获取验证码" style="width:100px;height:30px;" id="getCode" name="getCode"/>
                         </p>
                      </li>
                  </ul>
                  <div class="PushButton">
                       <a id="no"  class="button" style="cursor:pointer;">取消</a>
                       <a id="yes" class="button" style="cursor:pointer;">确定</a>
                  </div>
           </div>
      </div>
   </div>

    <script type="text/javascript">
       
    var second=120;
    var recover=function (){
        $("#getCode").removeAttr('disabled');
        $("#getCode").val('免费获取验证码');
        clearInterval(sh);
        clearInterval(timer_sh);
        //120秒之后重新设置为120
        second=120;
    };
    
  	//计时器开始
    var timer=function(){
        second=second-1;
        $("#getCode").val(second+" 获取中......");
    }
    var sh=null;
    var timer_sh=null;
    $("#getCode").click(function () {
        var pay_treasure =$('#payBind').val();
        var mobile=$('#mobile').val();
        if(pay_treasure==null || pay_treasure.trim()==""){
            alert("支付宝号不能为空！");
            $('#payBind').focus();
            return;
        }
             
        $(this).attr('disabled','disabled');
        $(this).val('120 获取中......');
        sh=setInterval(recover,120000);
        timer_sh=setInterval(timer,1000);

        $.ajax({
            url:'${mainServer}/sms/sendRegistCode',
            type:'post',
            data:{"mobile":mobile},
            success:function(data){ 
            }
        });
    });

    //获取窗口索引
    var index = parent.layer.getFrameIndex(window.name);
    $("#no").click(function () {
        // 关闭窗口
        parent.layer.close(index);
    }); 

      $("#yes").click(function () {
    	  
          var pay_treasure =$('#payBind').val();
          var code =$('#code').val();

          if(pay_treasure==null || pay_treasure.trim()==""){
              alert("支付宝号不能为空！");
              $('#payBind').focus();
              return;
          }
           		  
          if(code==null || code.trim()==""){
              alert("验证码不能为空！");
              $('#code').focus();
              return;
          }
                  
          var url="${mainServer}/account/payBind";
          $.ajax({
              url:url,
              data:{"pay_treasure":pay_treasure,"code":code,"uncheckImgVC":"Y" },
              type:'post',
              async:false,
              dataType:'json',
              success:function(data){
                  alert(data.message);
                  if(data.result){
                	  var pay_treasure=data['pay_treasure'];
                	  //alert(newMobile);
                	  //将新的手机号码传给父页面
                	  //showNewMobile(newMobile);
                      // 标识点击了保存 给父窗口传值标识修改成功
                      parent.$('#returnCode').val(pay_treasure);
                      // 关闭窗口
                      parent.layer.close(index);
                  }
              }
          });
      }); 
</script>
</body>
</html>