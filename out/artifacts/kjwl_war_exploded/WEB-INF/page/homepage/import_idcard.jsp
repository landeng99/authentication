<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${resource_path}/css/uploadify.css">
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="${resource_path}/js/uploadify/swfobject.js"></script>
<script type="text/javascript" src="${resource_path}/js/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
<title>Insert title here</title>
 
<script type="text/javascript">
	var mainServer = '${mainServer}';
	var backServer = '${backServer}';
	var jsFileServer = '${backJsFile}';
	var cssFileServer = '${backCssFile}';
	var imgFileServer = '${backImgFile}';
	var uploadPath = '${uploadPath}';
</script>
</head>
<style type="text/css">
.idcard_cxt h4{
	color:#333;
	display: block;
	border:1px solid #ccc;
	border-bottom:0px;
	margin: 0px;
	padding: 5px 0px;
	background: #ccc;
	font-size: 13px;
	 
}
.idcard_list{
	margin-top:0px;
	height:250px;
	overflow-y: scroll;
	border:1px solid #ccc; 
}

.idcard_list li{
	float: left;
	list-style: none;
	margin:5px;
	margin-right:50px;
	/*background-color: #0097db;*/ 
	padding: 3px 7px;
	text-align: center;
	min-width: 10px;
	/*color: #fff;*/
	/*border-radius: 2em;*/
	border: 1px solid #ccc;
}
.note{
	color:red;
	font-size: 12px;
}
#fileQueue{
    background-color: #fff;
    border-radius: 3px;
    box-shadow: 0 1px 3px;
    height: 150px;
    margin: 10px auto;
    overflow: auto;
    padding: 5px 10px;
    width: 700px;
}
.buttonlist{
	text-align: center;
}
.green{
	color:#1DC24B;
}
.red{
	color:#E0350C;
}
</style>
<body>
<div class="idcard_cxt">
<h4>待上传身份证</h4>
	<ul class="idcard_list">
		<c:forEach var="userAddress" items="${userAddressList}">
			 <c:if test="${null!=userAddress.idcard &&''!=userAddress.idcard}"> 
				<li>
					<span>${userAddress.idcard}</span>
					<c:if test="${null==userAddress.front_img || ''==userAddress.front_img}">
						<span class="red">正面</span>
					</c:if>
					<c:if test="${null!=userAddress.front_img}">
						<span class="green">正面</span>
					</c:if>
					<c:if test="${null==userAddress.back_img || ''==userAddress.back_img}">
						<span class="red">反面</span>
					</c:if>
					<c:if test="${null!=userAddress.back_img}">
						<span class="green">反面</span>
					</c:if>
				</li>
		</c:if>
		</c:forEach>
	</ul>
	<div style="float:left;">待上传身份证数量:${count}</div>
	<div style="float:right;margin-right:10px;"><span style="background:#1DC24B;display: inline-block;width:16px;height:16px;margin-right:5px;">&nbsp;</span><span>已上传</span></div>
	<div style="float:right;margin-right:10px;"><span style="background:#E0350C;display: inline-block;width:16px;height:16px;margin-right:5px;">&nbsp;</span><span>未上传</span></div>
</div>
<div style="clear: both">

</div>
<div class="buttonlist">
	<input type="file" name="uploadify" id="uploadify"/>
</div>

<div id="fileQueue"></div>
<div>
	<ul>
		<li>上传中数目:<span id="uping"></span></li>
		<li>总上传数目:<span id="upover"></span></li>
	</ul>
	<input type="hidden" id="cn" value="0"/>
</div>
<div class="note">
	<p>注意，必须上传身份证正反面，同时身份证的文件名称必须为如下格式,否则上传不成功。</p>
	<p>正面：4526241794655228-1.jpg,反面：4526241794655228-2.jpg</p>
	<p>最多一次上传10张，最大为1M</p>
</div>
<script type="text/javascript">

$(document).ready(function(){
    $("#uploadify").uploadify({
         'uploader': '${mainServer}/homepkg/importIdcard',
         'formData':{'user_id':'${sessionScope.frontUser.user_id}','count':'${count}'},
         'swf':"${resource_path}/js/uploadify/uploadify.swf",
         'cancelImg': '${resource_path}/img/uploadify-cancel.png',
         'queueID': 'fileQueue',
         'method':'post',
         'auto': true,
         'buttonText': '选择身份证图片',
         'multi': true,
         'uploadLimit':10, 
         'progressData':'speed',
         'queueSizeLimit' : 10,
         'fileTypeDesc':'Image Files',
          'fileSizeLimit':'1MB', 
         'fileTypeExts': '*.jpg;*.png',
         'onUploadSuccess':function(file,data,response){
        	 var upingCount=$("#cn").val();
        	 $("#uping").html(parseInt(upingCount)-1);
        	 $("#cn").val(parseInt(upingCount)-1);
        	 //alert(data);
        	 if(data !=""){
        		 $("#upover").html(data);
        	 }
         },
         'onUploadStart':function(file){
        	 //var strs= new Array();
             //strs = data.split("-1");
        	 //var str=file.name
         },
         'onSelect':function(file){
        	 if(!validateLogin(1)){
        		 return ;
        	 }
        	 
        	 var upingCount=$("#cn").val();
        	 $("#uping").html(parseInt(upingCount)+1);
        	 $("#cn").val(parseInt(upingCount)+1);
         },
         'onError': function(event, queueID, fileObj) {
             alert("文件:" + fileObj.name + "上传失败");
         }
     });
}); 
</script>
</body>
</html>