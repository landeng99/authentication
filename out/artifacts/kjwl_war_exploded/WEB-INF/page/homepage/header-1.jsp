<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
<link rel="stylesheet" type="text/css" href="${resource_path}/theme/css/kj.css">
<title>跨境物流</title>
</head>
<body>
<div class="top"><div class="wrap">
	<div class="fl notice1"><i class="ico01">&nbsp;</i>
	<c:if test="${null != lastNotice}">
	<a href="${mainServer}/noticeInfo?noticeId=${lastNotice.noticeId}">${lastNotice.title}<span>${lastNotice.effectShortYear}-${lastNotice.effectShortDate}</span></a>
	</c:if>
	<c:if test="${null == lastNotice}">
	暂无公告｜新闻<span></span>
	</c:if>
	</div>
    <div class="fr">
    <c:if test="${null == frontUser}">
    <a href="${mainServer}/login"><i class="ico02">&nbsp;</i>登录</a>
    &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="${mainServer}/registe"><i class="ico03">&nbsp;</i>注册</a>
    </c:if>
    <c:if test="${null != frontUser}">
    <a href="${mainServer}/transport"><i class="ico02">&nbsp;</i>我的账户</a>
    &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="${mainServer}/exit"><i class="ico03">&nbsp;</i>退出</a>
     </c:if>
    </div>
</div></div>
<div class="wrap">
	<div class="logo"><img src="${resource_path}/theme/images/logo.png"/></div>
    <div class="menu"><ul>
    	<li class="cur"><a href="${mainServer}">网站首页</a></li>
        <li><a href="${mainServer}/prohibition">禁运物品</a></li>
        <li><a href="${mainServer}/help">用户指南</a></li>
        <li><a href="${mainServer}/free">费用相关</a></li>
        <li><a href="${mainServer}/help?item=93">海淘教程</a></li>
        <li><a href="${mainServer}/noticeInfoList">新闻公告</a></li>
    </ul></div>
</div>