<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style type="text/css">
    .notice2{
	margin-left:auto;
	margin-right:auto;
	margin-top:100px;
	display: block;
	color:#000;
	width:60%;
	border: solid 8px #3A7BD5;
	border-radius: 10px;
}
	.notice-fi2{
	text-align:center;
	font-size:22px;
	font-weight:bold;
	margin-top:10px;
	padding-top:5px;
	padding-bottom:20px;
}
	.li1{
	text-align:left;
	line-height:18px;
	margin-left:auto;
	margin-right:auto;
	width:430px;
	font-size:16px;
	color:#000;
	padding-bottom:15px;
}
</style>



<div class="notice2">
	 <div class="mian2">
		  <div class="notice-fi2">所有通知</div>
          <c:forEach items="${noticeLists}" var="notice">
		 	 <li class="li1">${notice.title }<a href="${mainServer}/noticeInfo?noticeId=${notice.noticeId}" style="padding-left:15px;color:#F00;text-decoration:none;">查看详情</a>
			 </li1>
          </c:forEach>
	 </div>
</div>
