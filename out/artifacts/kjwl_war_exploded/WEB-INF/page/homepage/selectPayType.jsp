<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">

.size18 {
    font-size: 18px;
}

p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}

.orange, a.orange {
    color: #FF8B00;
}

.recharge span {
display:-moz-inline-box;
display:inline-block;
width:150px; 
}

.List-bank {
    overflow-x: hidden;
    overflow-y: hidden;
    padding-top: 0;
    width: 100%;
    margin-left:-50px;

}
.List-bank li {
    height: 50px;
    list-style:none;
    float:left;
    margin-left: 40px;
}
.List-bank li input {
    float: left;
    margin-top: 16px;
}
.List-bank li img {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-bottom-color: #CECECE;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: #CECECE;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: solid;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: 1px;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: #CECECE;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: solid;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: 1px;
    border-top-color: #CECECE;
    border-top-style: solid;
    border-top-width: 1px;
    float: left;
    height: 39px;
    margin-left: 8px;
    width: 149px;
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button1.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 119px;
}

.span-title {
display:-moz-inline-box;
display:inline-block;
width:100px; 
font-size: 14px;
font-weight: bold;
}
.span-money {
display:-moz-inline-box;
display:inline-block;
width:100px;
font-size: 16px;
}
.line1 {
    margin-left: 0;
    margin-right: 0;
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

</style>
<body>
<div style="background-image:none;border-right:none;width:897px;padding-top:5px;" id="content">
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
    <input type="hidden" id="logisticsCodes" value="${logisticsCodes}">
    <input type="hidden" id="total" value="<fmt:formatNumber value="${actualPay + 0.00001}" type="currency" pattern="#.##"/>">
    <input type="hidden" id="flag" value="${flag}">
    <input type="hidden" id="coupon_id" value="${userCoupon.coupon_id}">
    
        <div>
           <div style="height: 40px; margin-top:15px;">
               <p>
                   <span class="span-title">本次支付总额：</span> 
                   <span class="orange size18 span-money"><fmt:formatNumber value="${actualPay + 0.00001}" type="currency" pattern="$#,###.##"/></span>
                   <span class="span-title">账户可用余额：</span>   
                   <span class="orange size18 span-money"><fmt:formatNumber value="${frontUser.able_balance + 0.00001}" type="currency" pattern="$#,###.##"/></span>
                   <span class="span-title">仍需支付金额：</span>  
                   <span class="orange size18 span-money"><fmt:formatNumber value="${total_fee + 0.00001}" type="currency" pattern="$#,###.##"/></span>
              </p> 
           </div>
           <div><p><span class="span-title">请选择支付方式</span></p></div>
            <div class="line1"></div>
            <div id="recharge" style="display: block;">
                <jsp:include page="bankList.jsp"></jsp:include>
                <div class="PushButton">
                   <a id="cancel" class="button" style="cursor:pointer;">关　闭</a>
                   <a id="submitCharge" class="button" style="cursor:pointer;">立即支付</a>
                </div> 
           </div>
    </div>
    
</div>

    <script type="text/javascript">

      $("#submitCharge").click(function () {
        //获取窗口索引
          var index = parent.layer.getFrameIndex(window.name);
          var logisticsCodes =$('#logisticsCodes').val();
          var total =$('#total').val();
          var coupon_id=$('#coupon_id').val();
          var bankCode =$("input[name='bankCode']:checked").val();
          var flag =$('#flag').val();
          var url="${mainServer}/payment/paymentOnline?logisticsCodes="
              +logisticsCodes+"&total="+total
              +"&bankCode="+bankCode+"&flag="+flag+"&coupon_id="+coupon_id;

          window.open(url);
       // 关闭窗口
          parent.layer.close(index);
      }); 
      
      $("#cancel").click(function () {
          //获取窗口索引
            var index = parent.layer.getFrameIndex(window.name);
         // 关闭窗口
            parent.layer.close(index);
        }); 

        </script>
</body>
</html>