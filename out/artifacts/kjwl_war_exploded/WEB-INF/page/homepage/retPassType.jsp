<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0021)http://www.birdex.cn/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<!-- 
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/sylogin.css" rel="stylesheet" type="text/css"/>
 -->
<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
<script type="text/javascript" src="${resource_path}/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
</head>
<body onkeydown="doKeyDown()">
<jsp:include page="topIndexPage.jsp"></jsp:include>
<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/login.css"></link>
<%--
<form method="get" action="${mainServer}/retrieveUser" id="regform">
<div id="wrap" style="height: 660px;width:100%;">
<div id="subwrap">
    <div class="forgotpassword" style="padding-top: 0px; padding-bottom: 0px; height: 660px;" id="wrapModal">
         <div class="modal-dialog" style="margin-left:35%;margin-top:90px;">
              <div class="bt2"><span style="font-size:20px;">找回密码</span><span style="margin-left:5px;color:#B2A9A9;">RetrievePassword</span></div>  
          
              <dl class="Box02">
                  <dt><span>手机号码： </dt>
                  <dd>
                  <input name="type" type="hidden" value="${type}"/>
                  <input name="account" id="account" 
                  	type="text" class="input_Light_red input_all_red"  
                  	style="width:280px;height:40px;background-color: beige;" 
                  	value="${account}" readonly="readonly"/>
                  	 </span>
                  </dd>
                  <dt><span>设置新密码：</span></dt>
                  <dd><input name="user_pwd" id="user_pwd" 
                  	type="password" class="input_Light_blue" 
                  	value="" style="width:280px;height:40px;" 
                  	onblur="user_pwdblur()"/>
                  	<br /><span id="user_pwdNone"></span>
                  </dd>
                  <dt><span>再次输入密码：</span></dt>
                  <dd><input name="user_pwd_fu" id="user_pwd_fu" 
                  	type="password" class="input_Light_blue" 
                  	value="" style="width:280px;height:40px;" 
                  	onblur="user_pwd_fublur()"/>
                  	<br /><span id="user_pwd_fuNone"></span>
                  </dd>
                  
                  <dt><span>验证码：</span></dt>
                  <dd>
                    <div style="height: 50px;">
                    <input id="txtCode" type="text"  style="width:160px;height:30px;line-height:30px" maxlength="4" name="txtCode" value="请输入右边图形验证码" onFocus="if (this.value==this.defaultValue) this.value='';" onblur="LostFocus_Code();"/>
                    <img id="imgVC" src="${mainServer}/authImg" alt="看不清？点击刷新。" width="80" height="30" hspace="2" onclick="refresh()"
                        style="cursor: pointer;" class="verification-code" />
                    </div>
					<input name="sms_code" id="sms_code" type="text" class="input_Light_blue" style="width:160px;height:40px;" value="" onblur="sms_codeblur()"/>
					<input type="button" value="免费获取验证码" style="width:100px;height:40px;" id="freeCode" name="freeCode"/>
					<br /><span id="sms_codeNone"></span>
                    <label id="errCode" class="txtErr" style="color:#F50D0D;">
                     
                    </label>
				  </dd>
                  
              </dl>
			  <div style="margin:0 auto;width:90%;margin-top:10px;text-align:center;font-size:16px;color:#F50D0D;">
				   ${failReg}
		      </div>
              <div class="Bottm2">
                   <input id="btnreg" type="button" value="立即找回" class="button_bule button pull-left" style="width:110px;margin-left:10px;">
				   <input type="button" id="btnReset" value="上一步" class="button_bule button pull-left" style="width:110px;margin-left:40px;">
              </div>
         </div>
    </div>
</div>
</div>
</form>
 --%>
 
 <div class="wrap bgImg">
        <div class="panel ">
            <h1 class="tit">找回密码</h1>
            <div id="div_err" class="alert alert-danger hidden" role="alert">${failReg}</div>
            <div id="div_success" class="alert alert-success hidden" role="alert"></div>
            <form class="floating-labels " method="post" action="${mainServer}/retrieveUser" id="regform">
                <div class="form-group mb40">
                    <input class="form-control" name="account" id="account" type="text" value="${account}" readonly="readonly" />
					<input name="type" type="hidden" value="${type}"/>
					<span class="input-group-btn J-select-country"><!--加 selected -->
                        <button id="mobile_prefix" class="btn btn-default btn-outline bootstrap-touchspin-up" type="button" data-value="+86" data-text="中国">
                        	中国<div class="caret"></div>
                        </button>
                        <ul class="select-country J-select-country-ul"> <!-- 选择内容 -->
                            <li data-value="+86" data-text="中国" class="selected"><a ><i class="glyphicon glyphicon-ok text-success"></i>中国</a></li>
                            <li data-value="+1" data-text="美国"><a><i class="glyphicon glyphicon-ok text-success"></i>美国</a></li>
                        </ul>
                    </span>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="input1">手机号码</label>
                    <span class="help-block hidden"><small></small></span>
                </div>
                <div class="form-group mb40">
                    <input class="form-control" name="user_pwd" id="user_pwd" type="password" placeholder="最少8位密码" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="input1">设置新密码</label> 
                    <span class="help-block hidden"><small></small></span>
                </div>
                <div class="form-group mb40">
                    <input class="form-control" name="user_pwd_fu" id="user_pwd_fu" type="password" placeholder="最少8位密码" />
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="input1">再次输入密码</label> 
                    <span class="help-block hidden"><small></small></span>
                </div>
                <div class="form-group form-group-btn mb40">
                    <input class="form-control" id="txtCode" name="txtCode" type="text" placeholder="输入右边图形验证码">
                    <span class="input-group-btn">
                        <img id="imgVC" src="${mainServer}/authImg" alt="看不清？点击刷新。" hspace="2" onclick="refresh()" class="ver-code" />
                    </span>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="">验证码</label>
                    <span class="help-block hidden"><small></small></span>
                </div>
                 <div class="form-group form-group-btn mb40">
                    <input class="form-control" name="sms_code" id="sms_code" type="text" placeholder="输入短信验证码" />
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-outline bootstrap-touchspin-up" type="button" id="freeCode" name="freeCode">获取验证码</button>
                    </span>
                    <span class="highlight"></span>
                    <span class="bar"></span>
                    <span class="glyphicon glyphicon-remove form-control-feedback t-0 hidden"></span>
					<span class="glyphicon glyphicon-ok form-control-feedback t-0 hidden"></span>
                    <label for="">短信验证码</label>
                    <span class="help-block hidden"><small></small></span>
                </div>
			    <input type="hidden" id="trecommand_user_id" name="trecommand_user_id" value="${recommand_user_id}"/>
			    <input type="hidden" id="source" name="source" value="${source}"/>
			    <input type="hidden" id="pushLinkId" value="${pushLinkId}"/>
            </form>
            <div class="text-center mb20">
            	<button id="btnreg" class="btn-primary btn-input btn">立即找回</button>
            	<button id="btnReset" class="btn-primary btn-input btn">上一步</button>
            </div>
        </div>
    </div>
<script>
function doKeyDown(event) {
    var e=event || window.event || arguments.callee.caller.arguments[0];
   if (e.keyCode == 13) {
	   $("#btnreg").click();
   }
}
var second=120;
function resetClick(opType) {
	if (opType == 'sms') {
		$("#freeCode").off('click');
		$("#freeCode").on('click', function(){
			$("#freeCode").off('click');
			user_pwdblur($('#user_pwd'), opType);
		});
	} else if (opType == 'reg') {
		$("#btnreg").off('click');
		$("#btnreg").on('click', function(){
			user_pwdblur($('#user_pwd'), opType);
		});
	}
}
$(function () {
	$("#freeCode").on('click', function(){
		$("#freeCode").off('click');
		user_pwdblur($('#user_pwd'), 'sms');
	});
	$("#btnreg").on('click', function(){
		$("#btnreg").off('click');
		user_pwdblur($('#user_pwd'), 'reg');
	});
	$("#btnReset").on('click', function(){
		Reset();
	});
	$('form.floating-labels').on('blur', 'input.form-control', function(){
		var val=$(this).val().trim();
		$(this).val(val);
		var opType = 'blur';
		switch ($(this).prop('id')) {
			case 'user_pwd':
				user_pwdblur(this, opType);
				break;
			case 'user_pwd_fu':
				user_pwd_fublur(this, opType);
				break;
			case 'sms_code':
				sms_codeblur(this, opType);
				break;
			case 'txtCode':
				LostFocus_Code(this, opType);
				break;
			default:
				break;
		}
	});
	$(".J-select-country").click(function() {
        $(this).toggleClass("selected");
    });
    $(".J-select-country-ul li").click(function() {
        $(this).addClass("selected");
        $(this).siblings().removeClass("selected");
        var value = $(this).data('value');
        var text = $(this).data('text');
        $('#mobile_prefix').data('value', value);
        $('#mobile_prefix').data('text', text);
        $('#mobile_prefix').html(text+'<div class="caret"></div>');
    }); 
});

function inputErr(obj){
	$(obj).parent().removeClass('has-success');
	$(obj).parent().addClass('has-error');
	$(obj).siblings('span.help-block').removeClass('hidden');
	$(obj).siblings('span.glyphicon-remove').removeClass('hidden');
	$(obj).siblings('span.glyphicon-ok').addClass('hidden');
}

function inputOk(obj){
	$(obj).parent().removeClass('has-error');
	$(obj).siblings('span.help-block').addClass('hidden');
	$(obj).siblings('span.glyphicon-remove').addClass('hidden');
	$(obj).siblings('span.glyphicon-ok').removeClass('hidden');
	$(obj).parent().addClass('has-success');
}
//返回上一步重新选择找回方式
function Reset(){ 
	window.location.href = "${mainServer}/retPassword";
}
//密码问题
function user_pwdblur(obj, opType){
	var pwd=$(obj).val();
	if(pwd== ""){
		$(obj).siblings('span.help-block').html('<small>密码不能为空!</small>');
		inputErr(obj);
		resetClick(opType);
		return false; 
	}
	var bValidate = RegExp(/^((?=.*\d)(?=.*\D)|(?=.*[a-zA-Z])(?=.*[^a-zA-Z]))^.{8,16}$/).test(pwd);
	if(!bValidate){
		$(obj).siblings('span.help-block').html('<small>长度8~16位，数字、字母、符号至少包含两种!</small>');
		inputErr(obj);
		resetClick(opType);
		return false;
	}
	inputOk(obj);
	if (opType != 'blur') {
		user_pwd_fublur($('#user_pwd_fu'), opType);
	}
}
function user_pwd_fublur(obj, opType){
	if($(obj).val() != $("#user_pwd").val()){
		$(obj).siblings('span.help-block').html('<small>俩次密码输入不相同!</small>');
		inputErr(obj);
		resetClick(opType);
		return false;
	}
	inputOk(obj);
	if (opType != 'blur') {
		LostFocus_Code($('#txtCode'), opType);
	}
}
//图形验证码
function LostFocus_Code(obj, opType) {
    if ($(obj).val() == "") {
		$("#txtCode").siblings('span.help-block').html('<small>图形验证码不能为空!</small>');
		inputErr($("#txtCode"));
		return false;
    } else {
    	inputOk($("#txtCode"));
    	if (opType == 'sms') {
    		sendCode();
    	} else if (opType == 'reg') {
    		sms_codeblur($('#sms_code'), opType);
    	}
    }
}
// 获取验证码
function sendCode(){    
	var mobile=$("#account").val();
	var code = $("#txtCode").val();
	var mobilePrefix=$("#mobile_prefix").data('value');
	setTimeout(timer, 1000);	 
    $.ajax({
		url:'${mainServer}/sms/sendRegistCode_new',
		type:'post',
		data:{mobile:mobile,txtCode:code,mobile_prefix:mobilePrefix},
		success:function(data){
			switch (data) {
			case '1':
				$('#div_err').text("验证码错误");
				$('#div_err').removeClass('hidden');
				second = 3
				break;
			case '2':
				$('#div_err').text("发送失败");
				$('#div_err').removeClass('hidden');
				second = 3
				break;
			default:
				$('#div_err').addClass('hidden');
				break;
			}
		}
	});
}
//计时器
var timer=function(){
	second--;
	if (second > 0) {
		$("#freeCode").html(second+" 获取中......");
		setTimeout(timer, 1000);
	} else {
		resetClick('sms');
		$("#freeCode").html('获取验证码');
		second=120;
	}
}
//验证码问题
function sms_codeblur(obj, opType){
	if ($(obj).val() == "") { 
		$(obj).siblings('span.help-block').html('<small>验证码不能为空！</small>');
		inputErr(obj);
		resetClick(opType);
		return false; 
	}
	inputOk(obj);
	if (opType == 'reg') {
		Registe();
	}
}
function Registe(){
	var sms_code = $('#sms_code').val();
	$.ajax({
		url:'${mainServer}/existSmsCode',
		type:'post',
		data:{smsCode:sms_code},
		success:function(data){
			if(data == 1){
				$('#regform').submit();
			}else{
				$('#div_err').text("修改失败！原因：验证码错误");
				$('#div_err').removeClass('hidden');
				resetClick('reg');
			}
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			resetClick('reg');
		}		
	});
}
function refresh(){
	document.getElementById("imgVC").src="${mainServer}/authImg?change="+(new Date()).getTime();
}
</script>
<jsp:include page="footer-1.jsp"></jsp:include>
</body>
</html>