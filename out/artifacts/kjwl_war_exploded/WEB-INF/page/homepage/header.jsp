<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" charset="utf-8" async="" src="${resource_path}/js/contains.js"></script>
<script type="text/javascript" charset="utf-8" async="" src="${resource_path}/js/localStorage.js"></script>
<script type="text/javascript" charset="utf-8" async="" src="${resource_path}/js/Panel.js"></script>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<script type="text/javascript">
    $(function () {
        var userName = $.cookie('BEYOND_NickName');
        if (userName == undefined || userName == "") {
            userName = $.cookie('BEYOND_UserName');
        }
        if (userName == undefined || userName == "") {
            userName = "会员用户";
        } else {
            if (userName.length > 8 && userName.indexOf("@") > -1) {
                userName = userName.substring(0, 7) + "...";
            }
        }
        $("#headerUserName").html(userName).attr("title", userName);

        var url = document.location.href;
        if (url == "http://www.birdex.cn/")
            url = "${backServer}/homepage/index";
        else if (url == "http://birdex.cn/")
            url = "${backServer}/homepage/index";

        $("ul.nav>li").each(function () {
            if (url.indexOf($(this).attr("val")) > -1) {
                $(this).addClass("current");
            }
        });

        var userID = $.cookie('User::TUserID');
        if (userID != undefined && userID != null && parseInt(userID) > 0) {
            $("#logout").show();
            $(".nav").css("right", "0");
        } else {
            $(".nav").css("right", "0");
            $("#login").show();
        }

        $(".ggnew").click(function () {
            if ($("#ggtab").is(':visible')) {
                $("#ggtab").slideUp("slow");
            } else {
                $("#ggtab").slideDown("slow");
            };
            var img = $(this).find("span").css("background-image");
            if (img.indexOf('header01') > -1) {
                $(this).find("span").css("background-image", img.replace('header01.png', 'header_up.png'));
            } else {
                $(this).find("span").css("background-image", img.replace('header_up.png', 'header01.png'));
            }
        });

        $("#tags1 li").mouseover(function (index, item) {
            $("#tags1 li").removeClass("selectTag");
            $(this).addClass("selectTag");
            $("#tags1").parent().find("div").each(function () {
                if ($(this).hasClass("selectTag")) {
                    $(this).removeClass("selectTag").addClass("tagContent");
                }
            });
            $("#" + $(this).attr("obj")).removeClass("tagContent").addClass("selectTag");
        });

        if (url.indexOf('index') > -1 || document.URL == 'http://www.birdex.cn/' || document.location.href.indexOf('index') > -1) {
            $(".ggnew").click();
        }

        var headerName = $.cookie('User::NickName');
        if (headerName == undefined || headerName == null || headerName == "") {
            headerName = $.cookie('User::TrueName');
            if (headerName == undefined || headerName == null || headerName == "") {
                headerName = $.cookie('BEYOND_UserName');
                if (headerName == undefined || headerName == null) {
                    headerName = "";
                }
            }
        }

        $("#headername").html(headerName);
    });
</script>
<style type="text/css">
    #header{ border-bottom:1px solid #dcdcdc; overflow:hidden; height:32px; line-height:32px;}
    #header a{ font-size:14px; color:#6a6a6b;}
    #header .pull-right a{ margin:0 10px;}
    #header .ggnew{ cursor:pointer;}
    #header .ggnew i{ color:#eb7424; font-style:normal;}
    #header .ggnew span{ float:left; display:block; width:42px; height:32px; background:url('http://img.cdn.birdex.cn/images/header01.png') no-repeat center center #0593d3; overflow:hidden; margin-right:5px;}
    #ggtab{ display:none; background-color:#0593d3; padding:10px 0; overflow:hidden;}
    #ggtab .hbt{ font-size:22px; font-weight:bold; color:#f57121; padding-left:32px; background:url('http://img.cdn.birdex.cn/images/header02.png') no-repeat left center; overflow:hidden; line-height:32px;}
    #sevedu{ padding-top:5px; overflow:hidden;}
    #sevedu .tagContent{display:none;}
    #sevedu .tagContent p,
    #sevedu .selectTag p{ font-size:16px; color:#edf6ff;}
    #sevedu .tagContent p a,
    #sevedu .selectTag p a{ color:#f57121; text-decoration:underline; margin-left:8px; font-weight:bold;}
    #sevedu .tag{ width:100%; padding-top:5px; text-align:right; overflow:hidden;}
    #sevedu .tag li{display:inline;}
    #sevedu .tag a{display:inline-block; width:8px; height:9px; background:url('http://img.cdn.birdex.cn/images/header03.png') no-repeat; overflow:hidden; margin:0 3px;}
    #sevedu .tag a:hover,
    #sevedu .tag .selectTag a{ width:56px; background-image:url('http://img.cdn.birdex.cn/images/header04.png');}
</style>
</head>
<body>
<div id="ggtab" style="display: block;">
     <div class="mian">
          <div class="hbt">公告</div>
          
          <div id="sevedu">
                <div class="selectTag" id="li0"><p>中国清明节（4月4日至4月6日）假期安排通知：除4月5日（星期天）清明节当天放假外，其余时间均有国内客服正<a href="http://www.birdex.cn/Information-14.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li1"><p>关于新增渠道详细介绍公告<a href="http://www.birdex.cn/Information-21.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li2"><p>关于系统问题导致订单申报数据错误的相关处理方案<a href="http://www.birdex.cn/Information-19.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li3"><p>所有速8快递的客户以及淘友，经过对目前现状的考察和认真考虑，速8快递不得已今天对税单相关政策作出调整如下：<a href="http://www.birdex.cn/Information-16.html" target="_blank">查看详情</a></p></div><ul id="tags1" class="tag"><li obj="li0" class="selectTag"><a href="javascript:void(0)"></a></li><li obj="li1" class=""><a href="javascript:void(0)"></a></li><li obj="li2" class=""><a href="javascript:void(0)"></a></li><li obj="li3" class=""><a href="javascript:void(0)"></a></li></ul>
          </div>
     </div>
</div>
<div id="header" class="noticeHeader">
     <div class="mian">
         <div class="pull-right" id="login" style="">
              <a href="${mainServer}/homepage/login">登录</a>|
              <a href="${mainServer}/homepage/registe">注册</a>
         </div>
         <div class="pull-right" id="logout" style="display:none;">
               <span id="headername"></span><a href="http://passport.birdex.cn/LogOut.aspx" style="">退出</a>
          </div>
         <a class="ggnew"><span style="background-image: url(http://img.cdn.birdex.cn/images/header_up.png);"></span>网站最新公告 <i>4</i> 条</a>
     </div>
</div>
<div id="head" style="border-bottom-style: none;">
     <div class="mian">
          <a href="${mainServer}/homepage/index" class="logo"><img src="${resource_path}/img/logo.jpg" alt="跨境" title="跨境"><span>跨境</span></a> 
          <ul class="nav" style="right: 0px;">
              <li val="help" style="padding-right:5px;"><a href="${mainServer}/homepage/help">帮助中心</a></li>
              <li val="Information" id="headInformation"><a href="${mainServer}/homepage/prohibition">禁运物品</a></li>
              <li val="account" id="headAccount"><a href="${mainServer}/homepage/transport">我的帐户</a></li>
              <li val="index" class="current"><a href="${mainServer}/homepage/index">首页</a></li>
          </ul>
     </div>
</div>