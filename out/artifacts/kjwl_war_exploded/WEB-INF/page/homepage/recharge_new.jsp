<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <title>SU8-个人中心-充值</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="../common/commonCss.jsp"></jsp:include> <!-- 前店公共框架样式 头部 -->
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/p-pay.css">
    
    <script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script> 
</head> 
<body>
	<!-- 头部页面  -- 新的支付界面 -->
	<jsp:include page="topPage.jsp"></jsp:include>
	
	<!-- banner -->
	<div class="min-banner">
	    <div class="bgImg"></div>
	    <div class="logo"></div>
	</div> 
	<!-- 内容 -->
	<div class="wrap bg-gray"> 
        <div class="wrap-box pb20 pt20 clearfix">
             <!-- 右边内容-->
          <div class="p-content">
                <ul class="breadcrumb">
                    <li><a><i class="iconfont icon-shouye"></i>我的个人中心</a></li>
                    <li class="active"><span>充值</span></li>
                </ul>
                <div class="paybox p20 ">
                    <!-- <h5 class="mb20  tit">账户信息</h5> -->
                    <!-- <h5 class="mb20  tit">账户信息</h5> -->
                     <div class="alert  alert-warning ">
                         <i class="glyphicon glyphicon-info-sign pr5"></i>
                                                                                             信用卡充值的账户余额不可提现
                     </div>
                   <from class="form-horizontal ">

                       <div class="form-group">
                           <label for="" class="col-sm-2 control-label">充值账户：</label>
                           <div class="col-sm-3">
                               <span class="txt">${frontUser.user_name}</span>
                           </div>
                       </div>
                       <div class="form-group">
                           <label for="" class="col-sm-2 control-label">账户余额：</label>
                           <div class="col-sm-1">
                                 <span class="txt"><strong class="text-info p5 "><fmt:formatNumber value="${frontUser.balance+0.00001}" type="currency" pattern="$#,###.##" /></strong></span>
                           </div>
                           <label for="" class="col-sm-2 control-label">可用余额：</label>
                           <div class="col-sm-1">
                                 <span class="txt pr10"><strong class="text-success p5 "><fmt:formatNumber value="${frontUser.able_balance+0.00001}" type="currency" pattern="$#,###.##" /></strong></span>
                                 <span class="txt">￥<strong class="text-success p5">${frontUser.able_balance_yuan}</strong></span>
                                 
                           </div>
                           <label for="" class="col-sm-2 control-label">冻结金额：</label>
                           <div class="col-sm-1">
                                 <span class="txt">$<strong class="p5 ">${frontUser.frozen_balance}</strong></span>
                           </div>
                       </div> 
                       <hr>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">充值金额：</label>
                            <div class="col-sm-3">
                                <input class="form-control " id="rechargeAmount" placeholder="" type="text" value="">
                            </div>
                            <div class="col-sm-1"> <span class="txt">元</span></div>
                        </div>
                        <hr>
                    </from> 
                   <h5 class="mb20  tit">支付方式</h5>
                    <div class="bankBox ">
                        <ul class="bankItem clearfix">
                            <li class="item">
                                <input type="radio" name="bankCode" value="zhifubao">
                                <img src="../images/pay/alipay2.jpg" class="bankImg">
                            </li>
                        </ul>
                        <h5 class="pt20">网银支付</h5>
                        <ul class="bankItem clearfix">
                  <li class="item"> <input type="radio" value="ICBC-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_ICBC.jpg"></li>
                  <li class="item"> <input type="radio" value="CMB-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_CMB.jpg"></li>
                  <li class="item"> <input type="radio" value="CCB-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_CCB.jpg"></li>
                  <li class="item"> <input type="radio" value="ABC" name="bankCode"><img src="${resource_path}/img/BANK_ABC.jpg"></li>
                  <li class="item"> <input type="radio" value="COMM-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_COMM.jpg"></li>
                  <li class="item"> <input type="radio" value="GDB-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_GDB.jpg"></li>
                  <li class="item"> <input type="radio" value="BOC-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_BOC.jpg"></li>
                  <li class="item"> <input type="radio" value="CEB-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_CEB.jpg"></li>
                  <li class="item"> <input type="radio" value="SPDB-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_SPDB.png"></li>
                  <li class="item"> <input type="radio" value="PSBC-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_PSBC.jpg"></li>
                  <li class="item"> <input type="radio" value="BJBANK" name="bankCode"><img src="${resource_path}/img/BANK_BJBANK.jpg"></li>
                  <li class="item"> <input type="radio" value="WZCBB2C-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_WZCB.jpg"></li>
                  <li class="item"> <input type="radio" value="CMBC" name="bankCode"><img src="${resource_path}/img/BANK_CMBC.jpg"></li>
                  <li class="item"> <input type="radio" value="BJRCB" name="bankCode"><img src="${resource_path}/img/BANK_BJRCB.jpg"></li>
                  <li class="item"> <input type="radio" value="SPA-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_SPA.jpg"></li>
                  <li class="item"> <input type="radio" value="CITIC-DEBIT" name="bankCode"><img src="${resource_path}/img/BANK_CITIC.jpg"></li>
                        </ul>
                        <hr>
                        <div class="text-center pt20">
                            <button class="btn btn-primary  ok"   id="submitCharge">提交</button>
                        </div>
                    </div>
                </div>
            </div>
             <!-- 右边内容-->
            <!-- 左侧菜单 -->
			<jsp:include page="leftPage.jsp"></jsp:include>
        </div>
            <!-- 支付提示 弹出框 -->
<div class="modal fade" id="prompt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;top:-10px;overflow: scroll !important;" >
  <div class="modal-dialog" role="document" style="width:400px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">支付提示</h4>
      </div>
      <div class="modal-body">
              <!-- 内容 -->
              <div class="blackfont text-center p20"> 
                     <a  onclick="reflesh()"   href="javascript:void(0)" class="btn btn-primary  ok" style="margin-left: 15px;" >重新支付</a>
                    <a   onclick="self.location=document.referrer;"  href="javascript:void(0)" class="btn btn-primary  ok"  style="margin-left: 15px;"  >付款成功</a>
              </div> 
      </div>
    </div>
  </div>
</div>
 </div>
    
	<!-- 底部页面 -->
	<jsp:include page="bottomPage.jsp"></jsp:include>
	
	<script type="text/javascript" src="${resource_path}/font/iconfont.js"></script>
	<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="${resource_path}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	 <script type="text/javascript">  
	 // 支付成功，跳转回个人中心页
	 function  callbackPay(){ 
 		 // window.location.href="${mainServer}/personalCenter"; 
	 }
	 // 刷星当前页
	 function  reflesh(){
         window.location.href="${mainServer}/account/torecharge"; 
	} 
	 //设置菜单被选中
	 $("#pesernsenter").addClass("selected");
      $("#submitCharge").click(function () { 
          var rechargeAmount =$('#rechargeAmount').val();
          var bankCode =$("input[name='bankCode']:checked").val();
          if(bankCode == null || bankCode.trim() == "" )
          {
        	  alert("支付方式不能为空！");  
        	  return;
          }
          if(rechargeAmount==null||rechargeAmount.trim()==""){
              alert("金额不能为空！");
              $('#rechargeAmount').focus();
              return;
          }
          var exp = /^([1-9][\d]{0,7}|0)(\.[\d]{1,2})?$/;
          if(!exp.test(rechargeAmount)){ 
              alert("金额格式错误！");
              $('#rechargeAmount').focus();
              return;
            }
          if(parseFloat(rechargeAmount) < parseFloat(0.1)){
              alert("最小金额为0.1！");
              $('#rechargeAmount').focus();
              return;
          } 
     	 $("#prompt").modal("show");
          var url="${mainServer}/member/recharge?rechargeAmount="+rechargeAmount+"&bankCode="+bankCode; 
          window.open(url); 
      });
      </script>
</body>
</html>