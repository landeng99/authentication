<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
<script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/common.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
     <link rel="stylesheet" type="text/css" href="${resource_path}/css/commom.css">
     <link rel="stylesheet" href="${resource_path}/css/common.css">

        <script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script>  
        <script src="${backJsFile}/layer/layer.js"></script>
        <script type="text/javascript">
            $(function(){
                    setInterval(function(){ 
                    var newList=$('.guendong').children().first().clone(true); 
                    $('.guendong').append(newList); 
                    $('.guendong').children().first().remove(); 
                    },2000); 
                 
                })
            function queryAllnoticeInfo(noticeId){
                $("#noticeId").val(noticeId);
                noticeInfoForm.submit();
            }
        </script>
       <style type="text/css">
 .p1 {
    font-size: 14px;
    height:30px;
}

.line1 {
	margin-top: 20px;
    margin-left: 0;
    margin-right: 0;
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.span1 {
display:-moz-inline-box;
display:inline-block;
width:80px; 
font-size: 16px;
}
.span2 {
display:-moz-inline-box;
display:inline-block;
width:160px;
font-size: 16px;
}
.span3 {
display:-moz-inline-box;
display:inline-block;
width:100%;
margin-top: 16px;
font-size: 16px;
}
.listpay {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.listpay li {
 
    width: 600px;
    float:left;
}

.payment {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 450px;
    margin-top: 16px;
}
.payment a {
    float: left;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.payment a:hover {
   text-decoration: none;
}
.payment a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button1.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 119px;
}
 </style>
        </style>
    </head>
<body class="bg-f1">
    <div>
    <!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->
<!--中间内容开始-->
<div class="main">
	<!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="${mainServer}/personalCenter"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li><a href="${mainServer}/transport"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="${mainServer}/warehouse"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="${mainServer}/useraddr/destination"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="${mainServer}/member/record"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="${mainServer}/account/accountInit"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="${mainServer}/account/couponInit"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
         </ul>
    </div>
    <!--左侧导航栏结束-->
    <!--右侧内容开始-->
    	<div class="right">
    	 <div class="column mt25 p20">
        <input type="hidden" id="total" value="${total}">
        <input type="hidden" id="logisticsCodes" value="${logisticsCodes}">
               <div >
                   <span class="span1">账户余额：</span> 
                   <span class="orange size18 span2"><fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#,###.##"/></span>
                   <span class="span1">冻结余额：</span>   
                   <span class="orange size18 span2"><fmt:formatNumber value="${frontUser.frozen_balance}" type="currency" pattern="$#,###.##"/></span>
                   <span class="span1">可用余额：</span>  
                   <span class="orange size18 span2"><fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#,###.##"/></span>
               </div>

               <div><span class="span3">关税清单</span></div>
               <div class="line1"></div>
                  <ul class="listpay">
                  <c:forEach var="pkg"  items="${pkgList}">
                     <li>
                          <div style="float:left;width:200px;">
                          <p class="p1"><strong>单号:</strong>
                          	<span class="orange size18" >
                               ${pkg.logistics_code}
                           </span>
                          </p>
                          </div>
                          <%-- <div style="float:left;">
                              <c:forEach var="good"  items="${pkg.goods}">
                                <p class="p1">${good.goods_name}：<fmt:formatNumber value="${good.customs_cost+0.00001}" type="currency" pattern="$#,###.##"/></p>
                              </c:forEach>
                              <p class="p1">包裹合计：<fmt:formatNumber value="${pkg.customs_cost+0.00001}" type="currency" pattern="$#,###.##"/></p>
                          </div> --%>
                     </li>
                  </c:forEach>
                     <li>
                       <div style="float:left;width:200px;">
                           <p class="p1"><strong>总计:</strong>
                           <span class="orange size18">
                               <fmt:formatNumber value="${total+0.00001}" type="currency" pattern="$#,###.##"/>
                           </span>
                           </p>
                       </div>
                       <div style="float:left;">
                           <p class="p1">
                                 <span class=" size18" style="color: red;">
                                    ${message}
                                 </span>
                           </p>
                       </div>
                     </li>
                     <li>
                       <div style="float:left;width:400px;">
                           <p style="font-size: 14px;height:40px;"  id ="message">账户可用余额不足时，需要支付宝支付不足部分。<br/>如果充值后账户余额不正确，请刷新页面。</p>
                       </div>
                     </li>
                 </ul>
               <div class="payment" id ="payButton">
                       <a id="pay" class="button" style="cursor:pointer;">立即支付</a>
                       <a id="close" class="button" style="cursor:pointer;" onclick="javascript:history.go(-1)">不着急</a>
               </div>
               </div>
          </div>
</div>     
</div>
<script type="text/javascript" src="${resource_path}/js/ZeroClipboard.js"></script>
<script type="text/javascript">
$(function(){
    $(".left-nav").hover(function () {
        $(this).css("background","url(${resource_path}/img/is5.png) no-repeat");
    }, function () {
        $(this).css("background","url(${resource_path}/img/is6.png) no-repeat");
        });
});

$("#charge").click(function () {
    var rechargeOffHeight = ($(window).height() - 570) / 2;
    $.layer({
        title :'充值',
        type: 2,
        fix: false,
        shadeClose: true,
        shade: [0.5, '#ccc', true],
        border: [1, 0.3, '#666', true],
        offset: [rechargeOffHeight + 'px', ''],
        area: ['950px', '570px'],
        iframe: { src: '${mainServer}/member/rechargeInit'}
    });
});

$("#pay").click(function () {
    var total =$('#total').val();
    var logisticsCodes =$('#logisticsCodes').val();

       var url="${mainServer}/payment/payTax";
        $.ajax({
            url:url,
            data:{
                "total":total,
                "logisticsCodes":logisticsCodes},
            type:'post',
            async:false,
            dataType:'json',
            success:function(data){
                alert(data.message);
                if(data.result ==1 || data.result ==6 || data.result ==3){
                   window.location.href ="${mainServer}/transport" 
                }else if(data.result ==2){
                   
                	selectPayType(total,logisticsCodes);
                }
            }
        });
   /*    if($('#pay_type').val() =="2"){

             var rechargeOffHeight = ($(window).height() - 650) / 2;
             $.layer({
                 title :'支付方式选择',
                 type: 2,
                 fix: false,
                 shadeClose: true,
                 shade: [0.5, '#ccc', true],
                 border: [1, 0.3, '#666', true],
                 offset: [rechargeOffHeight + 'px', ''],
                 area: ['950px', '650px'],
                 iframe: { src: '${mainServer}/payment/selectPayType?total='+total+'&logisticsCodes='+logisticsCodes+'&flag='+2}
             });
       }
*/
    });
    
    
function selectPayType(total,logisticsCodes){
    
    var rechargeOffHeight = ($(window).height() - 650) / 2;
    $.layer({
        title :'支付方式选择',
        type: 2,
        fix: false,
        shadeClose: true,
        shade: [0.5, '#ccc', true],
        border: [1, 0.3, '#666', true],
        offset: [rechargeOffHeight + 'px', ''],
        area: ['950px', '650px'],
        iframe: { src: '${mainServer}/payment/selectPayType?total='+total+'&logisticsCodes='+logisticsCodes+'&flag='+2},
        end: function (data) {
            var height = ($(window).height() - 430) / 2;
            $.layer({
                title :'支付确认',
                type: 2,
                fix: true,
                closeBtn: false,
                shadeClose: false,
                shade: [0.8, '#ccc', true],
                border: [1, 0.3, '#666', true],
                offset: [height + 'px', ''],
                area: ['520px', '230px'],
                iframe: { src: '${mainServer}/payment/payConfirm'},
                end: function (data) {
                    window.location.href ="${mainServer}/transport"
               }
            });
           }
    });

}
    
$("#paySuccess").click(function () {

     window.location.href ="${mainServer}/transport" 
 
    });
//包裹状态
function showTransportStatus(status){
	
	window.location.href="${mainServer}/transport?status="+status;
}
//所有状态的包裹
function showAllTransport(){
	window.location.href="${mainServer}/transport";
}
//包裹支付状态
function showTransportPayStatus(payStatus){
	
	window.location.href="${mainServer}/transport?payStatus="+payStatus;
}
</script>
