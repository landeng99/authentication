<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>速8导入包裹</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
</head>
<body class="bg-f1">

<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="#"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li class="active"><a href="#"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="#"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="#"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="#"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="#"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="#"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <!--<li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>-->
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
        <h3 class="public-title mt20"><i class="title-icon01"></i>导入包裹<a href="#" class="back">返回</a> </h3>
        <form id="form" action="${mainServer}/homepkg/importPkg" method="post"  enctype="multipart/form-data">
			<div class="column p20 pb60">
				<dl class="tips">
					<dt>导入包裹注意</dt>
					<dd>请查看<a href="${mainServer}/help?item=59">表格填写要求</a> ，严格按照填写要求进行填写，否则导入包裹可能不成功，建议重新下载一份新的模板进行填写；奶粉与咖啡请务必在品名中填写相应的克数；请在包裹预报时选择好需要的增值服务；包裹一旦入库不可再选择增值服务；请按物品实际价格如实申报。</dd>
				</dl>
				<div class="leading-box mt15">
					<h4 class="title">选择导入的文件</h4>
					<div class="up-download">
						<div class="download-file floatR">
							<p class="tip"><span class="btn-close floatR"><i class="icon iconfont icon-cuowu"></i></span> <i class="icon iconfont icon-warning"></i>模板中新增渠道选择，请重新下载模板</p>
							<p class="form-radio">
								<label class="tit">选择模板：</label>
								<label><input type="radio" name="check-radio" checked="checked" value="1">xls</label>
								<label><input type="radio" name="check-radio" value="2">xlsx</label>
							</p>
							<p class="btn-box">
								<button class="btn btn-black">下载模板</button>
							</p>
						</div>
						<div class="up-file floatL">
							<span class="sflnm">未选择任何文件</span>
							<b><i class="icon-check"></i> 选择文件</b>
							<input type="file" style="position: absolute;top: 0px;right: 0px;">
						</div>
					</div>
				</div>
				<input type="hidden" id="haveImport" name="haveImport" value="${haveImport}"/>
				<div class="leading-box mt20">
					<h4 class="title">选择海外仓库地址</h4>
					<div class="select-box">
						<select id="osaddr_id" name="osaddr_id">
							<option value="-1" selected="selected">请选择海外收获仓库</option>
							<c:forEach var="overSeaaddress" items="${frontOverSeadAddressList}">
								<option value="${overSeaaddress.id}">${overSeaaddress.country} ${overSeaaddress.state}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="public-btn pl46 pt40">
					<button class="btn btn-orange">保存</button><button class="btn btn-gray">取消</button>
				</div>
			</div>
		</form>
    </div>
    <!--右侧内容结束-->

</div>
<!--中间内容结束-->

<!--底部开始-->
<div class="footer">
    粤ICP备14073551-2号，Copyright © 2016 www.su8exp.com Inc.All Rights Reserved.07693329337
</div>
<!--底部结束-->

<script>
$(function(){
  $(".btn-black").click(function(){
	var selectTyle =$("input[name='check-radio']:checked").val();
		if(selectTyle == 1){
			window.open('${resource_path}/upload/template_001.xls');
		}else{
			window.open('${resource_path}/upload/template_001.xlsx');
		}
  });
  $(".up-file").on("change","input[type='file']",function(){
	    var filePath=$(this).val();
	    if(filePath.indexOf("xls")!=-1 || filePath.indexOf("xlsx")!=-1){
	        $(".fileerrorTip").html("").hide();
	        var arr=filePath.split('\\');
	        var fileName=arr[arr.length-1];
	        $(".sflnm").html(fileName);
	    }else{
	        $(".sflnm").html("");
	        alert("您未上传文件，或者您上传文件类型有误！");
	        return false 
	    }
	});
  var isSaveConfirm=true;
	$('.btn-orange').click(function(){
		if(!validateLogin(1)){
			return ;
		}
		var excel=$('#excel').val();
		
		if(excel==''){
			alert("请选择ECXEL文件！");
			return ;
		}
		
		var index =excel.lastIndexOf(".");

		if(0> index  ){
			alert("文件格式错误！");
			return ;
		}
		
		var exName = excel.substring(excel.lastIndexOf(".") + 1,excel.length);
		if(exName !="xls" && exName !="xlsx"){
			alert("文件格式错误！");
			return ;
		}
		var haveImport=$('#haveImport').val();
		var osaddr_id=$('#osaddr_id').val();
		if(osaddr_id==-1){
			alert('请选择海外仓库地址！');
			return ;
		}else{
			if(isSaveConfirm&&(haveImport==null||haveImport==''))
			{
				isSaveConfirm=false;
				$('#form').submit();
				$('.btn-orange').attr("href","#");
			}else
			{
				alert('已经提交成功，重复提交无效！');
			}
		}
			
	});
});
</script>
</body>
</html>