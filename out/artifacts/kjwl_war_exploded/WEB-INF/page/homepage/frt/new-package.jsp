<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>速8新建包裹</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
	<script src="${resource_path}/js/layer.min.js"></script>
	<script src="${resource_path}/js/common.js"></script>
	<script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
	<link rel="stylesheet" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" href="${resource_path}/css/common.css">
	<script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script>	
	<style type="text/css">
	.incrde{    
		border: 1px solid #b3d1db;
	    margin-bottom: 3px;
	   	display: block;
	   	background: #fff;
	   }
	</style>	
</head>
<body class="bg-f1">

<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="${mainServer}/personalCenter"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li class="active"><a href="${mainServer}/transport"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="${mainServer}/warehouse"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="${mainServer}/useraddr/destination"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="${mainServer}/member/record"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="${mainServer}/account/accountInit"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="${mainServer}/account/couponInit"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
        <h3 class="public-title mt20"><i class="glyphicon glyphicon-plus"></i>新建包裹<a href="javascript:history.back(-1)" class="back">返回</a> </h3>
        <div class="column p20">
			<form method="post" action="${mainServer}/homepkg/addpkg" id="pkgGoods" name="pkgGoods">
			<div class="leightbox1 modal-dialog" id="inputKeepPrice"
				style="background-image: url('');">
				<a href="javascript:;"
					class="lbAction modal-dialog-title-close close" rel="deactivate"
					style="background: transparent url(${resource_path}/img/close-x.png) no-repeat scroll;background-position: 25% 25%;"
					id="closeDivKeepPrice"></a>
				<div class="Whole" style="width: 100%;">
					<h1 class="blue">
						<span style="float: left;">请输入保价金额：</span>
					</h1>
					<div class="Search">
						<input type="text"
							onFocus="if (this.value==this.defaultValue) this.value='';"
							onblur="if(this.value=='') this.value=this.defaultValue;"
							name="new_pkg_keep_price" id="new_pkg_keep_price"
							class="Block input_none_02 pull-left"
							onkeyup="this.value=this.value.replace(/[^0-9]/gi,&#39;&#39;);"
							onchange="new_package_keep_price_service_cout()"
							style="padding-left: 5px; width: 120px; overflow: auto; height: 32px; line-height: normal;" />
					</div>
					<div>
						保费(元)：<span id="new_pkg_keep_price_service"></span>
					</div>
					<div class="PushButton" style="width: 120px;">
						<a class="Block input14 white"
							href="javascript:inputKeepPriceSave()">确定</a>
					</div>
				</div>
			</div>
			<input type="hidden" value="${frontUser.user_type}" id="userType" name="userType" />
            <div class="order-fill">
                <label>包裹关联单号：</label><input type="text" placeholder="请填写您的购物网站，商家发货的物流单号" name="original_num" id="txtDeliveryCode">
            </div>

            <h4 class="title new-title">海外仓库地址：</h4>
            <div class="check-address order-address">
                <select name="osaddr_id" id="osaddr_id">
                    <option value="" selected="selected">请选择收货地址</option>
					<c:forEach var="overSeaaddress" items="${frontOverSeadAddressList}">
						<option value="${overSeaaddress.id}">${overSeaaddress.country}${overSeaaddress.state}</option>
					</c:forEach>
                </select>
            </div>

            <h4 class="title new-title">新包裹-商品申报：</h4>
            <div class="order-table">
                <table class="table text-center">
                    <thead>
                    <tr>
                        <th width="290" class="text-left">商品名称</th>
                        <th width="136">申报类别</th>
                        <th width="100">品牌</th>
                        <th width="128">单价</th>
                        <th width="140">数量</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="tabProduct">
                    <tr id="tr127" class="new">
                        <td>
                            <input type="text" placeholder="建议直接复制商家英文商品名称" name="goods_name" id="product127" value="">
                        </td>
                        <td>
                            <input type="text" class="wt100" id="catalog127" onClick="ShowCataDialog(127);" value=""></input>
							<input type="hidden" id="taxs127"> 
							<input type="hidden" id="catalogKey127" value="" name="goods_type_id">
                        </td>
                        <td>
                            <input type="text" class="wt84" name="brand" value="">
                        </td>
                        <td>
                            <input type="text" class="wt84" name="price" value="" id="price127" onKeyUp="GetChargeMsg();" placeholder="$0.00"><span class="ml5">元</span>
                        </td>
                        <td>
                            <input type="text" class="wt54 ml20 floatL" name="quantity" id="count127" onKeyUp="GetChargeMsg();">
                            <p class="up-down floatL" >
                                <a onClick="ModifyNum(1,127)"><i class="glyphicon glyphicon-menu-up incrde"></i> </a>
                                <a onClick="ModifyNum(-1,127)"><i class="glyphicon glyphicon-menu-down incrde"></i></a>
                            </p>
                        </td>
                        <td>
                            <a href="javascript:delProduct(127)" class="delete"><i class="icon iconfont icon-cuowu"></i>删除</a>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="order-table-btn">
                    <a class="btn btn-primary" onClick="addProd()"><i class="glyphicon glyphicon-plus"></i> 添加新商品</a>
                </div>
                <div class="order-price">
                    <p>申报总价：<span class="col_red" id="lblTotalPrice">0.00元</span>
						<input type="hidden" name="total_worth" id="total_worth" value="30" /> 
					</p>
                    <p>
                        <label>备注信息：</label><textarea placeholder="xxxxxx"></textarea>
                    </p>
                </div>

            </div>

            <h4 class="title new-title">渠道选择：（包裹可选下方特殊渠道，默认选中行邮渠道）</h4>
            <div class="channel-list">
				<input type="hidden" id="express_package" name="express_package" value="0" />
                <div class="channel-list-box checked">
                    <input type="radio" name="channel" id="select_0_checkbox" value="0">
                    <dl>
                        <dt>默认(F)渠道</dt>
                        <dd class="one">
                            <p class="col_red">首重：3.5美金/磅</p>
                            <p class="col_666">续重：1.75美金/0.5磅</p>
                        </dd>
                        <dd class="two"><p>默认行邮渠道 默认行邮渠道 默认行邮渠道 默认行邮渠道 默认行邮渠道 默认行邮渠道 默认行邮渠道 默认行邮渠道 默认行邮渠道 默认行邮渠道默认行邮渠道</p></dd>
                        <dd class="three"><i class="icon iconfont icon-xuanzhong1"></i></dd>
                    </dl>
                </div>
                <div class="channel-list-box">
                    <input type="radio" name="channel" id="select_A_checkbox" value="1">
                    <dl>
                        <dt>A渠道</dt>
                        <dd class="one">
                            <p class="col_red">首重：4.5美金/磅</p>
                            <p class="col_666">续重：2.7美金/磅，首重**磅</p>
                        </dd>
                        <dd class="two"><p>行邮渠道</p></dd>
                        <dd class="three"><i class="icon iconfont icon-xuanzhong1"></i></dd>
                    </dl>
                </div>
                <div class="channel-list-box">
                    <input type="radio" name="channel" id="select_B_checkbox" value="2">
                    <dl>
                        <dt>B渠道</dt>
                        <dd class="one">
                            <p class="col_red">首重：4.5美金/磅</p>
                            <p class="col_666">续重：2.7美金/磅，首重**磅</p>
                        </dd>
                        <dd class="two"><p>行邮渠道</p></dd>
                        <dd class="three"><i class="icon iconfont icon-xuanzhong1"></i></dd>
                    </dl>
                </div>
            </div>

            <h4 class="title new-title">增值服务：（请在预报时选择好增值服务，包裹入库后无法再选择增值服务）</h4>
            <div class="add-service mr110">
                <div class="add-service-check">
					<c:forEach items="${attachServiceList}" var="asPojo" varStatus="asSta">
						<label class="checkbox-inline">
							<input type="checkbox" price='${asPojo.service_price}' name="chkServices" onClick="clickboxService(this)" value="${asPojo.attach_id}" id="newPkgcheckboxId${asPojo.attach_id}">
							<a class="icon${asSta.count + 50}" href="javascript:;" onClick="clickService(this,${asSta.count},${asPojo.attach_id});">${asPojo.service_name}</a>
						</label>					
					</c:forEach>
                </div>
				<input type="hidden" name="pkgServicePrice" value="" id="pkgServicePrice" />
                <p><input name="hidService" type="hidden" id="hidService" value="${TOOL_PRICE}">增值服务费：<span class="col_red" id="serviceAmount">0.00元</span> </p>
            </div>

            <h4 class="title new-title">收货地址：</h4>
            <div class="address-list">
                    <ul>
						<c:forEach var="address" items="${addressList}">
							<li class="one selt-addres">
								<p class="address-word">${address.addressStr}(${address.receiver}收)</p>
								<p class="name-phone">${address.receiver}&nbsp;${address.mobile}&nbsp;<a href="#" class="col_blue">修改</a> </p>
								<i class="default"></i>
								<i class="icon iconfont icon-icon14"></i>
								<input type="hidden" id="adres" name="adres" value="${address.address_id}"/>
							</li>
						</c:forEach>
                        
                        <li class="two">
                            <p class="address-word">
                                <select>
                                    <option>请选择收获地址</option>
                                </select>
                            </p>
                            <p class="name-phone">
                                <span>姓名：<input type="text" class="wt54"></span>
                                <span>手机号：<input type="text" class="wt94"></span>
                            </p>
                            <p class="search-btn">
                                <button class="btn btn-primary">查询</button>
                            </p>
                        </li>
                        <li class="three">
                            <span><i>+</i></span><br>
                            添加新的收货地址
                        </li>
                    </ul>
                </div>

            <h4 class="title new-title">转运费用：</h4>
            <div class="money-cheak pt30 pb40">
                <label class="radio-inline col_red">
					<input type="radio" name="pay_type" <c:if test="${frontUser.default_pay_type==1}">checked="checked"</c:if> value="1" id="autoPay" title="入库后可快速出库"> 快速出库（账户余额自动支付）<i>建议</i>
				</label>
                <label class="radio-inline">
					<input type="radio" name="pay_type" value="2" <c:if test="${frontUser.default_pay_type==2}">checked="checked"</c:if> id="noAutoPay" title="入库后需等待支付再出库">网上在线支付
				</label>
            </div>
            <div class="public-btn pt20 pb20">
                <a class="btn btn-orange" onClick="save()">保存</a><a class="btn btn-gray" onClick="canle()">取消</a>
            </div>
			</form>
        </div>
    </div>
    <!--右侧内容结束-->

</div>
<!--中间内容结束-->

<!--底部开始-->
<div class="footer">
    粤ICP备14073551-2号，Copyright © 2016 www.su8exp.com Inc.All Rights Reserved.07693329337
</div>
<!--底部结束-->
<script>
$(function(){
	$(".channel-list-box").click(function(){
		var this_=$(this);
		this_.parent().find('.channel-list-box').removeClass('checked');
		this_.addClass('checked');
		var select_box=this_.find("input[name='channel']").val();
		$('#express_package').val(select_box);
	});
	$('#closeDivKeepPrice').on('click', function(){
		var keep_price=$("#new_pkg_keep_price").val();
		if(keep_price>3000)
		{
			//alert("保价金额最高为RMB 3000元！");
			$("#new_pkg_keep_price").val(0);
			$("#new_pkg_keep_price_service").html(0);
			clickboxService($('#newPkgcheckboxId32'));
			layer.close(asdKeepPrice);
		}else
		{
		if(keep_price!=null&&keep_price!=''&&keep_price>0)
		{
			layer.close(asdKeepPrice);
			attdServicePrice=keep_price*0.01;
			$("#serviceAmount").html((attdServicePrice + parseFloat($("#serviceAmount").html())).toFixed(2));
		}else
		{
				clickboxService($('#newPkgcheckboxId32'));
				layer.close(asdKeepPrice);
		}
		}
	});
	$(".selt-addres").click(function(){
		var this_=$(this);
		this_.parent().find('.one').removeClass('active');
		this_.addClass('active');
	});
});
var asdKeepPrice;
//新增页面修改产品数量
function ModifyNum(num, id) {
	if ($("#count" + id).val() == "" || isNaN($("#count" + id).val())) {
		$("#count" + id).val(1);
	}

	if ($("#count" + id).val() == "1" && num < 0) {
		return;
	}
	$("#count" + id).val(parseInt($("#count" + id).val()) + num);
	GetChargeMsg();
}
 //删除产品
function delProduct(key) {
	if ($("#tabProduct tr").length == 1) {
		layer.msg("必须最少有一个商品", 2, 5);
		return;
	}

	var k = layer.confirm('确定删除该产品？', function () {
		$("#tr" + key).remove();
		GetChargeMsg();
		layer.close(k);
	});
}
//获取收费信息
function GetChargeMsg() {
	var price = 0;
	var taxs = 0;
	$("#tabProduct tr").each(function () {
		//存在数据的才进行统计
		if ($(this).attr("ID") != undefined) {
			var id = $(this).attr("ID").replace("tr", "");
			if (RegRule.Number.test($("#count" + id).val()) && RegRule.Decimal.test($("#price" + id).val()) && RegRule.Decimal.test($("#taxs" + id).val())) {
				price = price + (parseFloat($("#price" + id).val()) * parseFloat($("#count" + id).val()))
				//alert(price);
				taxs = taxs + (parseFloat($("#price" + id).val()) * parseFloat($("#count" + id).val()) * parseFloat($("#taxs" + id).val()));
			}
		}

	});
	$("#lblTotalPrice").html(price.toFixed(2));
	$("#total_worth").val(price.toFixed(2));
}
//选择服务
function clickboxService(obj)
{
	$(obj).parent().find("a").click();
}
function clickService(obj,num,attach_id) {
	if ($(obj).attr("class") != undefined) {
		var attdServicePrice=parseFloat($("#hidService").val().split(",")[num - 1]);
		if(attach_id==32)
		{
			attdServicePrice=$("#new_pkg_keep_price").val()*0.01;
		}
		var css = $(obj).attr("class").replace("icon", "");
		//存放增值服务价格
		var servicePrice=$("#pkgServicePrice").val();
		if (css < 50) {
			$(obj).attr("class", "icon" + (parseInt(css) + 31));
			$(obj).prev().removeAttr("checked");
			if (num != undefined) {
				$("#serviceAmount").html((parseFloat($("#serviceAmount").html()) - attdServicePrice).toFixed(2));
				//获取选中的增值服务价格
				var currServicePrice=$("#hidService").val().split(",")[num - 1];
			}
	   
		}
		else {

		
			var attdServicePrice=parseFloat($("#hidService").val().split(",")[num - 1]);
			if(attach_id==32)
			{
				ShowNewTransportKeepPriceInput(1,'');
				attdServicePrice=0.0;
			}
			$(obj).attr("class", "icon" + (css - 31));
			$(obj).prev().attr("checked", "checked");
			if (num != undefined) {
				//获取选中的增值服务价格
				var currServicePrice=$("#hidService").val().split(",")[num - 1];
				$("#serviceAmount").html((attdServicePrice + parseFloat($("#serviceAmount").html())).toFixed(2));
				if(servicePrice !=""){
					servicePrice=servicePrice+","+currServicePrice;
				}else{
					servicePrice=servicePrice+currServicePrice+",";
				}
			}
		}

	}
	else {
		var css = $(obj).next().attr("class").replace("icon", "");

		if ($(obj).attr("checked") != "checked") {
			$(obj).next().attr("class", "icon" + (parseInt(css) + 31));
		}
		else {
			$(obj).next().attr("class", "icon" + (css - 31));
		}
	}
}
//弹出输入保价输入层
function ShowNewTransportKeepPriceInput(id, code) {
	showIndex = 0;
	var off = 0;
	off = ($(window).height() - 400) / 2;
	asdKeepPrice = $.layer({
		bgcolor: '#fff',
		type: 1,
		fix: false,
		shade: [0.5, '#ccc', true],
		border: [0, 0.3, '#666', true],          
		area: ['50px', '50px'],
		offset: [off, ''],
		shadeClose:false,
		title: false,
		zIndex:10002,
		page: { dom: '#inputKeepPrice' },
		closeBtn: [0, false],//显示关闭按钮
		close: function (index) {
			layer.close(index);
			$('#inputKeepPrice').hide();
		}
	});
}
//关闭新增窗口
function inputKeepPriceSave(index) {
	//ActivityID = 0;
	var reg = /^[0-9]+$/;
	var keep_price =$('#new_pkg_keep_price').val();
	  if(keep_price==null || keep_price.trim()==""){
			alert("保价金额输入有误");
			return;
	  }
	  if(keep_price>3000)
	  {
			alert("保价金额最高为RMB 3000元！");
			return;
	  }
	if(keep_price==0)
	{
		
		clickboxService($('#newPkgcheckboxId32'));
		layer.close(asdKeepPrice);
	}
	if(keep_price!=null&&keep_price!=''&&keep_price>0)
	{
		layer.close(asdKeepPrice);
		attdServicePrice=keep_price*0.01;
		$("#serviceAmount").html((attdServicePrice + parseFloat($("#serviceAmount").html())).toFixed(2));
	}
}
 function new_package_keep_price_service_cout(){
	var reg = /^[0-9]+$/;
	var keep_price =$('#new_pkg_keep_price').val();
	if(keep_price==null || keep_price.trim()==""||!reg.test(keep_price)){
		alert("保价金额输入有误");
		return;
	}
	var needpay=keep_price*0.01;
	$('#new_pkg_keep_price_service').html(needpay);
}
//提交
function save(){
	//提交之前做登录验证
	  if(!validateLogin()){
		  return;
	  }
	var reg = /^[0-9a-zA-Z]+$/;
	var txtDeliveryCodeVal=$("#txtDeliveryCode").val();
	if(txtDeliveryCodeVal=="请输入包裹关联单号"){
// 				layer.msg("请输入包裹关联单号", 2, 5);
// 				return;
	}else if(!reg.test(txtDeliveryCodeVal))
	{
		layer.msg("关联单号只能是数字或英文", 2, 5);
		return;
	}
	//获取用户的类型：同行2，直客1
	var userType=$("#userType").val();
	var isValid=true;
	$(".new").each(function(i,n){
		var inputs=$(n).find('input');
		$.each(inputs,function(j,input){
			var name=input.name;
			var val=input.value;
			if('goods_name'==name){
				if(val==""){
					layer.msg("请输入商品名称", 2, 5);
					isValid=false;
					return false;
				}
				
				if(val.length>100){
					layer.msg("商品名称长度不能超过100个字符", 2, 5);
					isValid=false;
					return false;							
				}
			}
			if('goods_type_id'==name){
				if(val==""){
					layer.msg("申报类别不能为空", 2, 5);
					isValid=false;
					return false;
				}
			}
			//会员类型为同行则需要
			if('brand'==name){
				if(val==""){
					$(this).val("无");
				
				}
				if(val==""&&userType==2){
					layer.msg("商品品牌不能为空", 2, 5);
					isValid=false;
					return false;
				}
				if(val.length>30){
					layer.msg("品牌名称长度不能超过30个字符", 2, 5);
					isValid=false;
					return false;							
				}
			}
			if('price'==name){
				var reg = /^\d+(?=\.{0,1}\d+$|$)/;
				if(reg.test(val)==false){
					layer.msg("商品价格只能为数字", 2, 5);
					isValid=false;
					return false;
				}
				if(val==""){
					layer.msg("请输入商品价格", 2, 5);
					isValid=false;
					return false;
				}
			}
			if('quantity'==name){
				var reg=/^\d+$/;
				if(reg.test(val)==false){
					layer.msg("商品数量只能为数字", 2, 5);
					isValid=false;
					return false;
				}
				if(val==""){
					layer.msg("请输入商品数量", 2, 5);
					isValid=false;
					return false;
				}
			}
		});
	});
	if(	isValid==false){
		return;
	}			
	var address_id=$(".address-list").find('.active').find("input[name='adres']").val();
	if(address_id==""){
		layer.msg("请选择收货地址", 2, 5);
		return;
	}
	var osaddr_id=$("#osaddr_id").val();
	if(osaddr_id==""){
		layer.msg("请选择海外仓库地址", 2, 5);
		return;
	}

	var priceArr=[];
	$("input[name='chkServices']:checked").each(function(i,element) {
		var price=$(this).attr("price");
		priceArr.push(price);
	});
	$("#pkgServicePrice").val(priceArr.join(","));
	var url = "${mainServer}/homepkg/checkOriginalnum";
	$.ajax({
	   url:url,
	   type:'post',
	   dataType:'json',
	   async:false,
	   data:{'original_num':txtDeliveryCodeVal},
	   success:function(data){
			var msg=data['msg'];
			if(msg=="2"){
				window.location.href="${mainServer}/login";
			}else if(msg=="1"){
				layer.msg("关联单号已存在", 2, 5);
			}else if(msg=="3"){
				layer.msg("该包裹已到仓库为未认领状态，请联系客服", 2, 5);
			}else{
				$("#pkgGoods").submit();
			}
	   }
	});
	
	
}
//取消
function canle() {
	window.history.go(-1);
}
//添加商品
function addProd(){
	var key = new Date().getMilliseconds();
	console.log(key);
	var row = $("<tr id=\"tr" + key + "\" class=\"new\"></tr>");
	var td = $("<td></td>");	
	td.append($("<input type='text' name='goods_name' value='' placeholder='建议直接复制商家英文商品名称'  id=\"product" + key + "\" value=''/>"));		
	row.append(td);
	
	td = $("<td></td>");
	td.append($("<input type=\"text\" class=\"wt100\"  id=\"catalog" + key + "\" onclick=\"ShowCataDialog(" + key + ");\"  value='' /><input type='hidden' id=\"taxs" + key + "\"><input type='hidden' name='goods_type_id' value='' id=\"catalogKey" + key + "\">"));		
	row.append(td);
	
	td = $("<td></td>");
	td.append($("<input type='text' name='brand' value=''  class=\"wt84\" />"));
	row.append(td);
					
	td = $("<td></td>");
	td.append($("<input type='text' name='price' class=\"wt84\" value='' id=\"price" + key + "\" onkeyup=\"GetChargeMsg();\" placeholder='$0.00'><span class=\"ml5\">元</span>"));
	row.append(td);

	td = $("<td></td>");
	td.append($("<input type='text' name='quantity' class=\"wt54 ml20 floatL\" value='1' id=\"count" + key + "\"'  onkeyup=\"GetChargeMsg();\">"));
	td.append($("<p class=\"up-down floatL\" ><a onclick=\"ModifyNum(1," + key + ")\"><i class=\"glyphicon glyphicon-menu-up incrde\"></i> </a><a onclick=\"ModifyNum(-1," + key + ")\"><i class=\"glyphicon glyphicon-menu-down incrde\"></i></a></p>"));
	row.append(td);
	
	td = $("<td></td>");
	td.append($("<a href=\"javascript:delProduct(" + key + ")\" class=\"delete\"><i class=\"icon iconfont icon-cuowu\"></i>删除</a>"));
	row.append(td);
	$("#tabProduct").append(row);
}
</script>

</body>
</html>