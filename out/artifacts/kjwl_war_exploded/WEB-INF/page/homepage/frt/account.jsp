<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>速8账户设置</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <script type="text/javascript" src="${resource_path}/js/common.js"></script>
    <script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
    <script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
    <link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css">
    <link href="${resource_path}/css/syaccount.css" rel="stylesheet" type="text/css">
</head>
<body class="bg-f1">
<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="#"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li><a href="#"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="#"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="#"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="#"><i class="icon-money"></i>资金记录</a> </li>
            <li class="active"><a href="#"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="#"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <!--<li><a href="${mainServer}/account/recommand"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>-->
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
    	<div class="column mt25 p20">
    		<input type="hidden" id="returnCode" />
        <div class="record">
           <p class="size20"><strong>账户设置</strong></p>
          <div class="hr mg18"></div>
          <ul class="list07">
              <li class="border-top-none">
                  <ol>
                      <li class="span2 size14">电子邮件地址：</li>
                      <li class="span6 size14">
                      <span>${frontUser.email}</span>
                      <span style="margin-left:5px;color:#0097db;">
                           <c:if test="${frontUser.status==0}">未激活</c:if>
                           <c:if test="${frontUser.status==1}">正常</c:if>
                           <c:if test="${frontUser.status==2}">禁用</c:if>
                       </span>
                      </li>
                      <li class="span4 size14 text-right">
                          <a href="javascript:void(0);" class="blue" id="editPassword" ">[修改登录密码]</a>
                      </li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">手机号码：</li>
                      <li class="span6 size14">${frontUser.mobile}</li>
                      <li class="span4 size14 text-right"><a href="javascript:void(0);" class="blue" id="editMobile">[修改手机]</a></li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">用户名：</li>
                      <li class="span6 size14" id ="user_name">
                          <c:if test="${null!=frontUser.user_name && ''!=frontUser.user_name}">
                             ${frontUser.user_name}
                          </c:if>
                          <c:if test="${null==frontUser.user_name || ''==frontUser.user_name}">
                             &nbsp;
                          </c:if>
                      </li>
                      <li class="span4 size14 text-right"><a href="javascript:void(0);" class="blue" id="editUserName">[修改]</a></li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">注册时间：</li>
                      <li class="span6 size14">${frontUser.register_time_string}</li>
                      <li class="span4 size14 text-right"></li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">可用余额：</li>
                      <li class="span6 size20 orange"><fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#,###.##"/>
                      <span style="margin-left:150px;color:red;">
                      		冻结金额：
                      <fmt:formatNumber value="${frontUser.frozen_balance}" type="currency" pattern="$#,###.##"/>
                       </span>
                       </li>                     
                      <li class="span4 size14 text-right">
                          <a href="javascript:void(0);" class="blue" id="accountCharge">[马上充值]</a>
                          <a href="javascript:void(0);" class="blue" id="withdrawal">[提现]</a>
                      </li>

                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">会员类型：</li>
                      <li class="span6 size14">
                      </li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">会员积分：</li>
                      <li class="span6 size14">
                          ${frontUser.integral}
                      </li>
                      
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">银行卡号：</li>
                      <li class="span6 size14">
                          <c:if test="${null!=frontUser.bank_card && ''!=frontUser.bank_card}">
                             ${frontUser.bank_card}
                          </c:if>
                          <c:if test="${null==frontUser.bank_card || ''==frontUser.bank_card}">
                             &nbsp;
                          </c:if>
                      </li>
                      <li class="span4 size14 text-right">
                          <a href="javascript:void(0);" class="blue" id="bankBind">[修改]</a>
                      </li>
                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">支付宝账号：</li>
                      <li class="span6 size14">
                          <c:if test="${null!=frontUser.pay_treasure && ''!=frontUser.pay_treasure}">
                             ${frontUser.pay_treasure}
                          </c:if>
                          <c:if test="${null==frontUser.pay_treasure || ''==frontUser.pay_treasure}">
                             &nbsp;
                          </c:if>
                      </li>
                      <li class="span4 size14 text-right">
                          <a href="javascript:void(0);" class="blue" id="payBind">[修改]</a>
                      </li>

                  </ol>
              </li>
              <li>
                  <ol>
                      <li class="span2 size14">默认支付方式：</li>
                      <li class="span6 size14">
							<input
							type="radio" <c:if test="${frontUser.default_pay_type==1}">checked="checked"</c:if> value="1" name="pay_type" id="autoPay"
							title="入库后可快速出库"><label for="autoPay" title="入库后可快速出库">自动扣款（账户余额自动支付）</label>
							<input type="radio"
							<c:if test="${frontUser.default_pay_type==2}">checked="checked"</c:if>  value="2" name="pay_type" id="noAutoPay"
							title="入库后需等待支付再出库" style="margin-left: 20px;"><label
							for="noAutoPay" title="入库后需等待支付再出库">网上在线支付</label>
                      </li>
                      <li class="span4 size14 text-right">
                          <a href="javascript:void(0);" class="blue" id="modifyDefaultPayType">[确认修改]</a>
                      </li>					  
                  </ol>				  
              </li>           
          </ul>
          </div>
    	</div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#editPassword").click(function () {
         	if(!validateLogin()){
        		return ;
        	}
            var height = ($(window).height() - 320) / 2;
            $.layer({
                title :'修改密码',
                type: 2,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [height + 'px', ''],
                area: ['400px', '330px',],
                iframe: { src: '${mainServer}/account/editPasswordInit' }
            });
        });
        
        $("#editUserName").click(function () {
         	if(!validateLogin()){
        		return ;
        	}
            var height = ($(window).height() - 320) / 2;
            $.layer({
                title :'修改姓名',
                type: 2,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [height + 'px', ''],
                area: ['400px', '330px',],
                iframe: { src: '${mainServer}/account/editUserNameInit' },
                end : function(index) {
                    // 点击保存的时候 刷新页面
                    if ($('#returnCode').val() != "") {
                        window.location.href = "${mainServer}/account/accountInit";
                    }
                }
                
            });
        });
        
        $("#editMobile").click(function () {
         	if(!validateLogin()){
        		return ;
        	}
            var height = ($(window).height() - 320) / 2;
            $.layer({
                title :'修改手机号',
                type: 2,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [height + 'px', ''],
                area: ['400px', '330px',],
                iframe: { src: '${mainServer}/account/editMobileInit' },
                end : function(index) {
                    // 点击保存的时候 刷新页面
                    if ($('#returnCode').val() != "") {
                        window.location.href = "${mainServer}/account/accountInit";
                    }
                }
            });
        });
        
        $("#bankBind").click(function () {
         	if(!validateLogin()){
        		return ;
        	}
            var height = ($(window).height() - 320) / 2;
            $.layer({
                title :'绑定银行卡',
                type: 2,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [height + 'px', ''],
                area: ['400px', '360px',],
                iframe: { src: '${mainServer}/account/bankBindInit' },
                end : function(index) {
                    // 点击保存的时候 刷新页面
                    if ($('#returnCode').val() != "") {
                        window.location.href = "${mainServer}/account/accountInit";
                    }
                }
            });
        });

        $("#payBind").click(function () {
         	if(!validateLogin()){
        		return ;
        	}
            var height = ($(window).height() - 320) / 2;
            $.layer({
                title :'绑定支付宝',
                type: 2,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [height + 'px', ''],
                area: ['400px', '330px',],
                iframe: { src: '${mainServer}/account/payBindInit' },
                end : function(index) {
                    // 点击保存的时候 刷新页面
                    if ($('#returnCode').val() != "") {
                        window.location.href = "${mainServer}/account/accountInit";
                    }
                }
            });
        });

        $("#modifyDefaultPayType").click(function () {
         	if(!validateLogin()){
        		return ;
        	}
         	var canHandle=true;
         	var t_virtual_pay_flag=$('input[name="virtual_pay_flag"]:checked').val();
         	//
         	var pay_type=$('input[name="pay_type"]:checked').val();
         	if(t_virtual_pay_flag!=null&&t_virtual_pay_flag=='Y')
         	{
         		if(pay_type==2)
         		{
         			canHandle=false;
         			alert("你已经选择了【虚拟预付款】，暂不可以再修改默认支付设置");
         			$("input[name=pay_type]:eq(0)").attr("checked",'checked'); 
         		}
         	}
         	if(canHandle)
         	{

        	$.ajax({
				url:'${mainServer}/account/modifyDefaultPayType',
				type:'post',
				data:{defaultPayType:pay_type},
				dataType:"json",
				async:false,
				success:function(data){
					alert(data.message); 
				}			
			});
     		
         	}
        });
        
        $("#modifyVirtualPayFlag").click(function () {
         	if(!validateLogin()){
        		return ;
        	}
         	//
         	var t_virtual_pay_flag=$('input[name="virtual_pay_flag"]:checked').val();
        	$.ajax({
				url:'${mainServer}/account/modifyVirtualPayFlag',
				type:'post',
				data:{virtual_pay_flag:t_virtual_pay_flag},
				dataType:"json",
				async:false,
				success:function(data){
					alert(data.message); 
				}			
			});
        });
    });
</script>
<script type="text/javascript" src="${resource_path}/js/ZeroClipboard.js"></script>
<script type="text/javascript">
$(function(){
    $(".left-nav").hover(function () {
        $(this).css("background","url(${resource_path}/img/is5.png) no-repeat");
    }, function () {
        $(this).css("background","url(${resource_path}/img/is6.png) no-repeat");
        });
    
    $("#charge,#accountCharge").click(function () {
    	
    	if(!validateLogin()){
    		return ;
    	}
        var url = "${mainServer}/member/frontUserStatus";
        $.ajax({
            url:url,
            type:'post',
            async:false,
            dataType:'json',
            success:function(data){
                // 账户正常
                if(data.result =="1"){
                    var rechargeOffHeight = ($(window).height() - 600) / 2;
                    $.layer({
                        title :'充值',
                        type: 2,
                        fix: false,
                        shadeClose: true,
                        shade: [0.5, '#ccc', true],
                        border: [1, 0.3, '#666', true],
                        offset: [rechargeOffHeight + 'px', ''],
                        area: ['950px', '600px'],
                        iframe: { src: '${mainServer}/member/rechargeInit'},
                        end : function(index) {
                            // 点击保存的时候 刷新页面
                                window.location.href = "${mainServer}/account/accountInit";
                            }
                    });
                }
                else{
                   alert(data.message);
                }
            }
        });
    });
    
    $("#withdrawal").click(function () {
    	if(!validateLogin()){
    		return ;
    	}
    	
        var url = "${mainServer}/member/frontUserStatus";
        $.ajax({
            url:url,
            type:'post',
            async:false,
            dataType:'json',
            success:function(data){
                // 账户正常
                if(data.result =="1"){
                    var rechargeOffHeight = ($(window).height() - 600) / 2;
                    $.layer({
                        title :'提现',
                        type: 2,
                        fix: false,
                        shadeClose: true,
                        shade: [0.5, '#ccc', true],
                        border: [1, 0.3, '#666', true],
                        offset: [rechargeOffHeight + 'px', ''],
                        area: ['950px', '600px'],
                        iframe: { src: '${mainServer}/member/withdrawalInit'},
                        end : function(index) {
                        // 点击保存的时候 刷新页面
                            window.location.href = "${mainServer}/account/accountInit";
                            }
                    });
                }
                else{
                   alert(data.message);
                }
            }
        });
    });
});
//包裹状态
function showTransportStatus(status){
	//$("#transportStatus").val(status); 
	//$("#statusId").submit();
	window.location.href="${mainServer}/transport?status="+status;
}
//所有状态的包裹
function showAllTransport(){
	window.location.href="${mainServer}/transport";
}
//包裹支付状态
function showTransportPayStatus(payStatus){
	//$("#transportPayStatus").val(payStatus);
	//$("#payStatusId").submit();
	window.location.href="${mainServer}/transport?payStatus="+payStatus;
}
</script>
</body>
</html>