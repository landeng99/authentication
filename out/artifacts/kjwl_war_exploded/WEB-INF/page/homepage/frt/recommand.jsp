<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>推荐有礼</title>
    <script src="${resource_path}/js/jquery.1.11.3.js"></script>
    <script src="${resource_path}/js/jquery.js"></script>
    <script src="${resource_path}/js/bootstrap.min.js"></script>
    <script src="${resource_path}/js/slideUp.js"></script>
    <script src="${resource_path}/js/layer.min.js"></script>
    <link rel="stylesheet" href="${resource_path}/css/iconfont.css">
    <link rel="stylesheet" href="${resource_path}/css/bootstrap.css">
    <link rel="stylesheet" href="${resource_path}/css/global.css">
    <link rel="stylesheet" href="${resource_path}/css/indexs.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
     <link rel="stylesheet" type="text/css" href="${resource_path}/css/commom.css">
     <link href="${resource_path}/css/syaccount.css" rel="stylesheet" type="text/css">
	<style type="text/css">
        .couponli{width:33%; float:left; overflow:hidden;}
     </style>
</head>
<body class="bg-f1">
<!--头部开始-->
<div class="top">
    <div class="wrap1200">
        <ul class="right floatR">
            <li><a href="#">返回首页</a></li>
            <li><a href="#">资费说明</a></li>
            <li><a href="#">禁运物品</a></li>
            <li><a href="#">用户指南</a></li>
            <li><a href="#"><i class="icon iconfont icon-qq"></i>客服中心</a></li>
        </ul>
        <p class="left floatL">HI,<a href="#" class="col_orange">tong@qq.com</a><a href="#">[退出]</a> </p>
    </div>
</div>
<div class="header">
    <div class="wrap1200">
        <div class="right floatR">
            <input type="text" placeholder="请输入运单号查询">
            <button><i class="icon iconfont icon-iconfontsousuo"></i></button>
        </div>
        <div class="left floatL">
            <a href="#"><img src="${resource_path}/img/logo.jpg"></a>
            <h3><img src="${resource_path}/img/logo-titile.jpg"></h3>
        </div>
    </div>
</div>
<!--头部结束-->

<!--中间内容开始-->
<div class="main">
    <!--左侧导航栏开始-->
    <div class="left floatL">
        <ul class="sidebar">
            <li><a href="#"><i class="icon iconfont icon-shouye-shouye"></i>个人中心首页</a> </li>
            <li><a href="#"><i class="icon iconfont icon-tijikongjian"></i>我的包裹<small>${pkgCount}</small></a> </li>
            <li><a href="#"><i class="icon iconfont icon--guoji"></i>海外仓库地址</a> </li>
            <li><a href="#"><i class="icon iconfont icon-dingwei"></i>收货地址</a> </li>
            <li><a href="#"><i class="icon-money"></i>资金记录</a> </li>
            <li><a href="#"><i class="icon iconfont icon-shezhi"></i>账户设置</a> </li>
            <li><a href="#"><i class="icon iconfont icon-yhq"></i>优惠券<small>${countAvailable}</small></a> </li>
            <li class="active"><a href="#"><i class="icon iconfont icon-ren"></i>推荐有礼</a> </li>
        </ul>
    </div>
    <!--左侧导航栏结束-->

    <!--右侧内容开始-->
    <div class="right">
        <div class="column mt25 p20">
        	<div class="record">
          <div class="hr mg18 lastline"></div>
          <a name="Coupon"></a>
           <p class="size20"><strong>推荐有礼</strong></p>
          <div class="hr mg18"></div>
          <p class="size18" style="margin-left:0;"><strong class="pd10">我要推荐：</strong></p>
          <ul class="list07">
              <li class="border-top-none">
                  <ol>
                      <li class="span2 size14">我的推广链接：</li>
                      <li class="span6 size14">
                      <span id="copy_url_span">${frontUser.person_recommand_url}</span>
                      <input type="hidden" id="copy_url" name="copy_url" value="${frontUser.person_recommand_url}"/>
                      </li>
                      <li class="span4 size14">
                          <a href="javascript:void(0);" class="blue copyit">复制链接</a>
                      </li>
                  </ol>
              </li>
              <li class="border-top-none">
                  <ol>
                      <li class="span2 size14">我的推广二维码：</li>
                      <li class="span6 size14">
                      <span>
                      <img  alt="" src="${frontUser.person_qr_code}" style="cursor:pointer;width:120px;"/>
                      </span>
                      </li>
                  </ol>
              </li>              
           </ul>  
		    <div class="hr mg18 lastline"></div>
		   <div class="size14">**推荐规则说明**</div> 
		    <div class="hr mg18 lastline"></div>
		   <br>
           <li class="border-top-none">
                  <ol>
                      <li class="span10 size13">1.复制上面的推广链接给好友，好友点击进入链接的注册页面注册成功，并且通过网站下单成功支付了第一个包裹的运费后，推荐者会自动获得一张推荐优惠券，可用于抵扣运费</li>
                      <li class="span10 size13">2.也可将上面的推广二维码发给好友，好友通过扫描该二维码进入页面注册成功，并且通过网站下单成功支付了第一个包裹的运费后，推荐者会自动获得一张推荐优惠券，可用于抵扣运费</li>
					  <li class="span10 size13">3.推荐有效用户越多，就能获得越多的优惠券（有效用户指的是：通过推广链接或二维码进入页面成功注册且成功支付了第一个包裹费用的用户）</li>
				      <li class="span10 size13">4.获取的优惠券只能用于抵扣运费，不能提现</li>
					  <br><br>
				  </ol>
              </li> 
			  <br><br><br><br><br><br>
			    <div class="hr mg18 lastline"></div>
           <div class="size14">**您有以下方式可以获得优惠券**</div> 
            <br/> 
			<li class="border-top-none">
                  <ol>
                      <li class="span10 size13">1.成功注册后会获得注册优惠券</li>
                      <li class="span10 size13">2.成功晒单且通过审核后会获得晒单优惠券</li>
					  <li class="span10 size13">3.成功推荐好友注册且消费后会获得推荐优惠券</li>
				  </ol>
              </li> 
			  
               <div class="opt">
                   <ul class="function pull-left couponul" style="width:400px;margin-top:15px;">
                       <li class="current couponli" id="one">
                       <a style="cursor:pointer;">我的推荐</a>
                       </li>
                       <li class="couponli" id="two"><a style="cursor:pointer;">推广赠送统计</a></li>
					   <li class="couponli" id="three"><a style="cursor:pointer;">推广详情</a></li>
                   </ul>
               </div>
               
               <div id="avliable">
               <p>我邀请的好友</p>
               <ul class="list01 radius3" id="recordlist">
                   <li class="th">
                       <ol>
                           <li style="text-align:center;width:207px;padding:0;" id="title1">账号</li>
                           <li style="text-align:center;width:220px;padding:0;" id="title2">注册时间</li>
                           <li style="text-align:center;width:170px;padding:0;" id="title3">来源</li>
                           <li style="text-align:center;width:220px;padding:0;" id="title4">是否生效</li>
                       </ol>
                   </li>
                  <c:forEach var="tfrontUser"  items="${myInviteUserList}">
                   <li style="background-color:White;">
                       <ol>
                           <li style="text-align:center;width:207px;padding:0;">${tfrontUser.account}</li>
                           <li style="text-align:center;width:220px;padding:0;">${tfrontUser.register_time}</li>
                           <li style="text-align:center;width:170px;padding:0;">${tfrontUser.source}</li>
                           <li style="text-align:center;width:220px;padding:0;">
                           <c:if test="${tfrontUser.is_sent_recommand_coupon==1}">生效</c:if>
                           <c:if test="${tfrontUser.is_sent_recommand_coupon==0}">未生效</c:if>
                           </li>
                       </ol>
                   </li>
                 </c:forEach>
               </ul>
               </div>
               
               <div id="used" style="display: none">
                <p>我获得的优惠券</p>
               <ul class="list01 radius3" id="recordlist">
                   <li class="th">
                       <ol>
                           <li style="text-align:center;width:157px;padding:0;" id="title1">编号</li>
                           <li style="text-align:center;width:120px;padding:0;" id="title2">名称</li>
                           <li style="text-align:center;width:90px;padding:0;" id="title3">面额</li>
                           <li style="text-align:center;width:90px;padding:0;" id="title4">数量</li>
                           <li style="text-align:center;width:120px;padding:0;" id="title5">有效期至</li>
                           <li style="text-align:center;width:120px;padding:0;" id="title6">是否使用</li>
                           <li style="text-align:center;width:120px;padding:0;" id="title7">来源好友</li>
                       </ol>
                   </li>
                  <c:forEach var="couponUsed"  items="${recommandCouponList}">
                   <li style="background-color:White;">
                       <ol>
                           <li style="text-align:center;width:157px;padding:0;">${couponUsed.coupon_code}</li>
                           <li style="text-align:center;width:120px;padding:0;">${couponUsed.coupon_name}</li>
                           <li style="width:90px;padding:0;">
                               <div style="float:right;margin-right:10px;">
                                 <fmt:formatNumber value="${couponUsed.denomination}" type="currency" pattern="$#,###.00"/>
                               </div>
                           </li>
                           <li style="text-align:center;width:90px;padding:0;">${couponUsed.quantity}</li>
                           <li style="text-align:center;width:120px;padding:0;">${couponUsed.valid_date_toStr}</li>
                           <li style="text-align:center;width:120px;padding:0;">
                           <c:if test="${couponUsed.haveUsedCount==0}">未使用</c:if>
                           <c:if test="${couponUsed.haveUsedCount!=0}">已使用</c:if>
                           </li>
                           <li style="text-align:center;width:120px;padding:0;">${couponUsed.source_friend}</li>
                       </ol>
                   </li>
                 </c:forEach>
               </ul>
               </div>        

               <div id="reContent" style="display: none">
                <p>推广详情</p>
               <ul class="list01 radius3" id="recordlist">
                   <li class="th">
                       <ol>
                           <li style="text-align:center;width:207px;padding:0;" id="title1">下单时间</li>
                           <li style="text-align:center;width:190px;padding:0;" id="title2">用户识别码</li> 
                           <li style="text-align:center;width:200px;padding:0;" id="title3">运单号</li>
                           <li style="text-align:center;width:220px;padding:0;" id="title4">包裹费用</li>
                       </ol>
                   </li>
                  <c:forEach var="push"  items="${pushDetailList}">
                   <li style="background-color:White;">
                       <ol>
                           <li style="text-align:center;width:207px;padding:0;"><fmt:formatDate value="${push.createTime}" pattern="yyyy-MM-dd HH:mm"/></li>
                           <li style="text-align:center;width:190px;padding:0;">${push.last_name}</li> 
                           <li style="text-align:center;width:200px;padding:0;">${push.logistics_code}</li>
                           <li style="text-align:center;width:220px;padding:0;">$${push.transport_cost}</li>
                       </ol>
                   </li>
                 </c:forEach>
               </ul>
               </div> 
    		 </div>
        </div>
     </div>
 </div>
 <script type="text/javascript">
    $(function () {
        // 优惠券
        $(".couponli").click(function () {
            
            $("ul.couponul>li").removeClass("current");
            $(this).addClass("current");
			
            var id =(this).id;
            if(id=="one"){
                $("#avliable").show();
                $("#used").hide();
				$("#reContent").hide();
			}else if(id=="two"){
                $("#avliable").hide();
                $("#used").show();
				$("#reContent").hide();
            }else{
                $("#avliable").hide();
                $("#used").hide();
				$("#reContent").show();
            }           
        });
    });
        
</script>
 <script type="text/javascript" src="${resource_path}/js/ZeroClipboard.js"></script>
</body>
</html>