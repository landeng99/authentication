<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >

<!-- 尾部 -->
<div class="wrap footer">
    <div class="wrap-box ">
        <div class="item">
            <div class="logo-foot">
                
            </div>
            <p>在这里，我们真实传递速8快递发生的每件事情；在这里，你们及时分享速8快递发展的点滴故事。</p>
            <a href="" class="mo">了解我们</a>
        </div>
        <div class="item">
            <h4><i class="iconfont icon-zhinanzhen"></i>办公地址</h4>
            <table class="table table-nobor">

                <tbody>
                    <tr>
                        <td width="50">地址：</td>
                        <td>广东省深圳市宝安区福永街道桥南新区宝华大厦301</td>
                    </tr>
                    <tr>
                        <td>电话：</td>
                        <td>0755-23204850</td>
                    </tr>
                    <tr>
                        <td>邮箱：</td>
                        <td>service@su8exp.com</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="item">
            <h4><i class="iconfont icon-kefu1"></i>在线客服</h4>
            <div class="share">
                <a href=""><i class="iconfont icon-weixin"></i><span>微信</span></a>
                <a href=""><i class="iconfont icon-qq"></i><span>QQ</span></a>
            </div>
        </div>
        <div class="item">
            <h4><i class="iconfont icon-shijian"></i>营业时间</h4>
            <p>周一至周五（9：00-18：00）</p>
            <p>周六（9：00-12：00）</p>
        </div>
    </div>
     <div class="copyright">© 深圳市程露祥供应链管理有限公司版权所有 粤ICP备17086482 Copyright 2017 su8exp.com Inc. All Rights Reserved.</div>
</div>

