<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>速8快递 专业的跨境转运</title>
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>

</head>
<body>

	<jsp:include page="topIndexPage.jsp"></jsp:include>
	<link rel="stylesheet" type="text/css" href="${resource_path}/new_css/news.css"></link>

<%-- <div id="standard" class="standard">
	<div class="pricest" style="font-size:24px;">跨境物流渠道收费标准和打包要求</div>
	<div class="pricest-line" style="margin-bottom: 40px;"></div>

	
<div id="price01" style="margin-left:auto; margin-right:auto; width:100%">
<table cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse; border-color:#7A8EC1">
 <tr style="height:40px;">
    <td colspan="3" width="821" style="padding-left:5px; background-color:#7A8EC1; color:#FFF; font-size:18px; font-weight:bold; border-color:#000000">渠道A:首重4$/磅，续0.4$/0.1磅</td>
  </tr>
  <col width="25%" />
  <col width="50%" />
  <col width="25%" />
  <tr style="font-size:16px; height:40px; background-color:#7A8EC1; color:#FFF;  font-weight:bold; border-color:#000000">
    <td style="padding-left:5px; font-size:17px;">类别</td>
    <td style="padding-left:5px; font-size:17px; ">品名</td>
    <td style="padding-left:5px; font-size:17px; ">打包要求</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">母婴用品类</td>
    <td style="padding-left:5px; font-size:14px;">奶瓶、辅食研磨碗、尿片、吸管、婴儿餐具、婴儿洗护品、奶粉等</td>
    <td style="padding-left:5px; font-size:17px;">3.5KG(奶粉限重2KG内)</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">生活日用品类</td>
	 <td style="padding-left:5px; font-size:14px;">成人洗护用品（洗发水、生发液、润肤露、沐浴露）、运动物品-装备、玩具、手动剃须刀、牙膏、文具、餐具、酒具、茶具、台灯、杯子等</td>
  	<td style="padding-left:5px; font-size:17px;">3KG</td>
  </tr>
  
    <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">食品类</td>
	 <td style="padding-left:5px; font-size:14px;">泡芙、坚果、膨化食品、饼干、米粉、糖果、巧克力、果泥等</td>
  	<td style="padding-left:5px; font-size:17px;">3KG</td>
  </tr>
  
    <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">普通服装</td>
	 <td style="padding-left:5px; font-size:14px;">童鞋、童装、毛衣、衬衫、T恤、大衣、棉衣、内衣、裤装、帽子、皮带、双肩背包、旅行包等</td>
  	<td style="padding-left:5px; font-size:17px;">6件（3KG内）</td>
  </tr>
  
    <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">鞋类</td>
	 <td style="padding-left:5px; font-size:14px;">MINNETONKA、UGG、ECCO、ASICS、KAPPA、NINEWEST、NIKE、JORDAN、ROCKPORT、JESSICA SIMPSON等</td>
  	<td style="padding-left:5px; font-size:17px;">鞋2双(2KG内)</td>
  </tr>
</table>
</div>
<br/>


<div id="price02" style="margin-left:auto; margin-right:auto; width:100%">
<table cellspacing="0" cellpadding="0" border="1" style="border-collapse:collapse; border-color:#7A8EC1" width="100%">
 <tr style="height:40px;">
    <td colspan="3" width="821" style="padding-left:5px; background-color:#7A8EC1; color:#FFF; font-size:18px; font-weight:bold; border-color:#000000">渠道B：首6$/磅，续0.6$/0.1磅</td>
  </tr>
  <col width="25%" />
  <col width="50%" />
  <col width="25%" />
  <tr style="font-size:16px; height:40px; background-color:#7A8EC1; color:#FFF;  font-weight:bold; border-color:#000000">
    <td style="padding-left:5px; font-size:17px;">类别</td>
    <td style="padding-left:5px; font-size:17px; ">品名</td>
    <td style="padding-left:5px; font-size:17px; ">打包要求</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">箱包</td>
    <td style="padding-left:5px; font-size:14px;">COACH、MICHAEL KROS、KATE SPADE、KIPLING、JANSPORT、LACOSTE等</td>
    <td style="padding-left:5px; font-size:17px;">大包1个+钱包1个</td>
  </tr>
  <tr style="font-size:15px; ">
    <td style="padding-left:5px; font-size:17px;">普通化妆品类</td>
	 <td style="padding-left:5px; font-size:14px;">	
润唇膏、眼霜、面膜、防晒霜、香水、眼影、睫毛膏、洗面奶、BB霜等</td>
  	<td style="padding-left:5px; font-size:17px;">1KG</td>
  </tr>
</table>
</div>
<br/> --%>

	<div class="min-banner">
        <div class="bgImg"></div>
        <div class="logo"></div>
        <!-- <span class="txt">
            常见商品税率参考
        </span> -->
    </div>
    <div class="wrap bg-gray">
        <div class="wrap-box pt20 pb20">
            <div class="newBox">
                <h1 style="font-size: 22px;text-align: center;margin-bottom: 50px">跨境物流渠道收费标准和打包要求</h1>
                <div class="box">
                    <h5 class="tit" style="margin-bottom: 20px">渠道A:首重4$/磅，续0.4$/0.1磅</h5>
                    <table class="table table-bordered table-set">
                      <colgroup>
                       <col width="180" style="background-color: #fff;">
                      </colgroup>
                      <thead>
                        <tr class="bg-gray">
                          <th>类别</th>
                          <th>品名</th>
                          <th>打包要求</th>
                        </tr>
                      </thead>
                      <tbody>
                         <tr>
                           <td>母婴用品类</td>
                           <td>奶瓶、辅食研磨碗、尿片、吸管、婴儿餐具、婴儿洗护品、奶粉等</td>
                           <td>3.5KG(奶粉限重2KG内)</td>
                         </tr>
                        <tr>
                          <td>生活日用品类</td>
                          <td>成人洗护用品（洗发水、生发液、润肤露、沐浴露）、运动物品-装备、玩具、手动剃须刀、牙膏、文具、餐具、酒具、茶具、台灯、杯子等</td>
                          <td>3KG</td>
                        </tr>
                        <tr>
                           <td>食品类</td>
                           <td>泡芙、坚果、膨化食品、饼干、米粉、糖果、巧克力、果泥等</td>
                           <td>3KG</td>
                         </tr>
                         <tr>
                           <td>普通服装</td>
                           <td>童鞋、童装、毛衣、衬衫、T恤、大衣、棉衣、内衣、裤装、帽子、皮带、双肩背包、旅行包等</td>
                           <td>6件（3KG内）</td>
                         </tr>
                         <tr>
                           <td>鞋类</td>
                           <td>MINNETONKA、UGG、ECCO、ASICS、KAPPA、NINEWEST、NIKE、JORDAN、ROCKPORT、JESSICA SIMPSON等</td>
                           <td>鞋2双(2KG内)</td>
                         </tr>
                      </tbody>
                    </table>
                    <h5 class="tit" style="margin-bottom: 20px ;margin-top:30px;">渠道B：首6$/磅，续0.6$/0.1磅</h5>
                    <table class="table table-bordered table-set">
                      <colgroup>
                       <col width="180" style="background-color: #fff;">
                      </colgroup>
                      <thead>
                        <tr class="bg-gray">
                          <th>类别</th>
                          <th>品名</th>
                          <th>打包要求</th>
                        </tr>
                      </thead>
                      <tbody>
                         <tr>
                           <td>箱包</td>
                           <td>COACH、MICHAEL KROS、KATE SPADE、KIPLING、JANSPORT、LACOSTE等</td>
                           <td>大包1个+钱包1个</td>
                         </tr>
                        <tr>
                          <td>普通化妆品类</td>
                          <td>润唇膏、眼霜、面膜、防晒霜、香水、眼影、睫毛膏、洗面奶、BB霜等</td>
                          <td>1KG</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
                
               
            </div>
        </div>
    </div>

	<jsp:include page="footer-1.jsp"></jsp:include>
</body>
</html>