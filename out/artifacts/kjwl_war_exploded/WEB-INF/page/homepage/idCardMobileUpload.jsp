<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>速8快递 专业的跨境转运</title>
    <link href="${resource_path}/img/favicon.ico" media="screen" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="${resource_path}/jqueryMoblie/css/themes/default/jquery.mobile-1.4.5.min.css">
    <script src="${resource_path}/jqueryMoblie/js/jquery.min.js"></script>
    <script src="${resource_path}/jqueryMoblie/js/jquery.mobile-1.4.5.min.js"></script>
<script >
	var mainServer = '${mainServer}';
</script>
</head>
<body>
 <div data-role="page">
    <div data-role="header">
    <span style=" font-size: x-large; line-height:2">&nbsp;速8快递-身份证上传&nbsp;&nbsp;
    <input type="button" name="button" class="btn" value="返回首页" onClick="location='${mainServer}/'"/>
    </span>
    </div>
    <div role="main" class="ui-content">
<span style="font-size: small;">1.证件照只供海关清关使用，请上传真实有效的证件图片</span>
<br>
<span style="font-size: small;">2.所上传身份证人名要与收件人名一致</span>
<br>
<span style="font-size: small;">3.请将身份证正面与反面单独分开上传</span>
<br>
<span style="font-size: small;">4.若自己添加水印，请勿遮挡身份证中信息以及头像</span>
<br>
<span style="font-size: small;">5.请保持四角齐全，尽量清晰，不要有反光，缺损</span>
<br>
<span style="font-size: small;">6.图片格式为jpg或jpeg或png，大小建议控制在600KB以内</span>
<br>
<br>
<span id="error_span" style="color: red; font-style: normal;"></span>
<br>
<br>
<form action="${mainServer}/homepkg/idcardMobileUploadHandle" method="post" enctype="multipart/form-data"  data-ajax="false" id="id_card_form">

<label for="logistics_code">运单号（与手机号选填其一）：</label>
<input type="text" id="logistics_code" name="logistics_code" value="" onBlur="logistics_code_keyUp();">

<label for="mobile">手机号（与运单号选填其一）：</label>
<input type="text" id="mobile" name="mobile" value="">

<label for="real_name">姓名（必填）：</label>
<input type="text" id="real_name" name="real_name" value="">

<label for="id_card_num">身份证号（必填）：</label>
<input type="text" id="id_card_num" name="id_card_num" value="">

<label for="id_card_face">身份证正面:</label>
<input type="file" id="id_card_face" name="id_card_face" value="">   
       
<label for="id_card_back">身份证反面:</label>
<input type="file" id="id_card_back" name="id_card_back" value="">

<input type="button" name="button" class="btn" value="上传" onClick="submitUpload();"/>
<input type="reset" name="reset" class="btn" value="重置" />   
</form>
    </div>
    <div data-role="footer">

    </div>
</div>
</body>
<script>
function submitUpload()
{
    var faceImgPath=$("#id_card_face").val();
    var backImgPath=$("#id_card_back").val();
    var mobile=$("#mobile").val();
    var real_name=$("#real_name").val();
    var id_card_num=$("#id_card_num").val();
    if(mobile==null||mobile=='')
    {
        alert("必须填入手机号码");
        $("#error_span").html("必须填入手机号码");
        return false;
    }
    if(!(/^1[3|4|5|7|8]\d{9}$/.test(mobile))){ 
        alert("手机号码有误，请重填");
        $("#error_span").html("手机号码有误，请重填");
        return false; 
    }
    if(real_name==null||real_name=='')
    {
        alert("必须填入姓名");
        $("#error_span").html("必须填入姓名");
        return false;
    }
    if(id_card_num==null||id_card_num=='')
    {
        alert("必须填入身份证");
        $("#error_span").html("必须填入身份证");
        return false;
    }
    var regIdCard=/^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
    if(!(regIdCard.test(id_card_num))){ 
        alert("身份证号码有误，请重填");
        $("#error_span").html("身份证号码有误，请重填");
        return false; 
    }
    if(faceImgPath==null||faceImgPath==''||backImgPath==null||backImgPath=='')
    {
        alert("请上传身份证照片（正反面都需要上传）");
        $("#error_span").html("请上传身份证照片（正反面都需要上传）");
        return false;
    }
    var canSubmit=false;
    $.ajax({
        url:'${mainServer}/homepkg/validByMobileReceiver',
        type:'post',
        data:{i_mobile:mobile,i_real_name:real_name},
        dataType:"json",
        async:false,
        success:function(data){
            if("S" == data.result){
                canSubmit=true;
            }else
            {
                canSubmit=false;
                alert("输入信息有误，手机号码、姓名不匹配，请重新核对。");
                $("#error_span").html("输入信息有误，手机号码、姓名不匹配，请重新核对。");
            }
        }           
    });
    if(canSubmit)
    {
        $("#id_card_form").submit();
    }
}
function logistics_code_keyUp()
{
	var v_logistics_code=$("#logistics_code").val();
	if(v_logistics_code==null||v_logistics_code=='')
	{
		//alert("请输入正确的运单号");
		return false;
	}
	$.ajax({
		url:'${mainServer}/homepkg/queryMobileNumByLogisticsCode',
		type:'post',
		data:{logistics_code:v_logistics_code},
		dataType:"json",
		async:false,
		success:function(data){
			if("S" == data.result){
				$("#mobile").val(data.mobile);
			}else
			{
				alert("请输入正确的运单号");
			}
		}			
	});
}
</script>
</html>