<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>运费-税金支付</title>
    <script  type="text/javascript" src="${resource_path}/js/jquery.js"></script>
    <script type="text/javascript" src="${resource_path}/js/util/util_new.js"></script>  
    <jsp:include page="../common/commonCss.jsp"></jsp:include> <!-- 前店公共框架样式 头部 -->
    <script src="${resource_path}/js/payment.js"></script> 
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/person-centre.css">
    <link rel="stylesheet" type="text/css" href="${resource_path}/new_css/p-pay.css">   
<style type="text/css">
 .p1 {
    font-size: 14px;
    height:30px;
}

.line1 {
	margin-top: 20px;
    margin-left: 0;
    margin-right: 0;
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.span1 {
display:-moz-inline-box;
display:inline-block;
width:80px; 
font-size: 16px;
}
.span2 {
display:-moz-inline-box;
display:inline-block;
width:160px;
font-size: 16px;
}
.span3 {
display:-moz-inline-box;
display:inline-block;
width:100%;
margin-top: 16px;
font-size: 16px;
}
.listpay {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.listpay li {
 
    width: 600px;
    float:left;
}

.payment {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 450px;
    margin-top: 16px;
}
.payment a {
    float: left;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.payment a:hover {
   text-decoration: none;
}
.payment a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button1.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 119px;
}
 </style>
</head>
<body class="bg-f1"> 
	<!-- 头部页面  -- 新的支付界面 -->
	<jsp:include page="topPage.jsp"></jsp:include> 
	<!-- banner -->
	<div class="min-banner">
	    <div class="bgImg"></div>
	    <div class="logo"></div>
	</div> 
<!--头部结束-->

<!--中间内容开始我改登陆，提交-->
 
	<!-- 内容 -->
	<div class="wrap bg-gray"> 
        <div class="wrap-box pb20 pt20 clearfix">
             <!-- 右边内容-->
         <div class="p-content">
              <ul class="breadcrumb" style="margin-bottom: 0;">
              <input type="hidden" value="${mainServer}" id="mainServer">
                  <li><a><i class="iconfont icon-baoguo"></i>我的包裹</a></li>
                  <li><a><i class="iconfont "></i>支付</a></li>
                  <li class="active"><span>选择包裹</span></li>
              </ul>
              <form class="form-horizontal ">
                <div class="p10 bg-gray m20 clearfix">
                   <div class="form-group clearfix " style="margin-bottom: 0px;">
                       <label for="" class="col-sm-2 control-label">运费总计：</label>
                       <div class="col-sm-3">
                             <span class="txt"><strong class="text-info pr10 "  id="sportmoney">	
								<fmt:formatNumber value="${transportCostTotal+0.00001}" type="currency" pattern="$#,###.##"/>
								</strong></span> 
                       </div>
                      <label for="" class="col-sm-2 control-label">包裹总计：</label>
                       <div class="col-sm-3">
                             <span class="txt"><strong class="text-info pr10 " id="sportcount">${payTransportCostListcount}</strong>个</span>
                       </div>
                      <div class="col-sm-2 text-right">
                        <a class="btn btn-primary2 f12 btn-sm" onclick="showhide('divTransportdetail','sporta')" id="sporta">展开</a>
                      </div>
                   </div>
                </div>
              </form>
              <div class="m20" id="divTransportdetail" style="display:none" >
                <table class="table table-list table-hover bg-white mt20">
                   <colgroup>
                     <col width="100">
                     <col width="170">
                     <col width="">
                     <col width="120">
                   </colgroup>
                    <thead>
                         <tr>
                          <th style="padding-left: 20px;" >
                              <input id="checkALLItemsport" type="checkbox" onclick="checkALLItemsport();">
                              <label for="ex">全选</label>
                            </th>
                             <th>单号</th>
                            <th>商品/数量</th>
                            <th>运费</th>
                            <th>增值运费</th>
                            <th>合计</th>
                         </tr>
                    </thead>
                    <tbody> 
                    <c:if test="${payTransportCostList!=null &&  payTransportCostListcount >0}">
                         <c:forEach var="pkg"  items="${payTransportCostList}">
                          <tr>
                            <td><input type="checkbox" name="select1"  id="${pkg.logistics_code}" value="${pkg.transport_cost}" class="checkbox1"></td>
                            <td>${pkg.logistics_code}</td>
                            <td>  <c:forEach var="goods"  items="${pkg.goods}">
											<p class="p1">${goods.goods_name}*${goods.quantity}</p>
										  </c:forEach>
							 </td>
                             <td>
                              <span class="text-info pr10"><fmt:formatNumber value="${pkg.freight+0.00001}" type="currency" pattern="$#,###.##"/></span>
                            </td>
                              <td>
                              <span class="text-info pr10"><fmt:formatNumber value="${pkg.attach_service_price+0.00001}" type="currency" pattern="$#,###.##"/></span>
                            </td>
                              <td>
                              <span class="text-info pr10"><fmt:formatNumber value="${pkg.transport_cost+0.00001}" type="currency" pattern="$#,###.##"/></span>
                            </td>
                           </tr> 
						</c:forEach> 
                       </c:if> 
                    </tbody>
                </table>
              </div>
              <form class="form-horizontal ">
                 <div class="p10 bg-gray m20">
                    <div class="form-group clearfix " style="margin-bottom: 0px;">
                        <label for="" class="col-sm-2 control-label">税费总计：</label>
                        <div class="col-sm-3 ">
                              <span class="txt"><strong class="text-info pr10 " id="payTaxtListmoney">
                              <fmt:formatNumber value="${payTaxtTotal+0.00001}" type="currency" pattern="$#,###.##"/></strong></span>
                              <span class="text-muted text-center">|</span>
                         </div>
                        <label for="" class="col-sm-2 control-label">包裹总计：</label>
                         <div class="col-sm-3">
                               <span class="txt"><strong class="text-info pr10 " id="payTaxtListcount">${payTaxtListcount}</strong>个</span>
                              
                         </div>
                        <div class="col-sm-2 text-right">
                        <a class="btn btn-primary2 f12 btn-sm" onclick="showhide('divTaxtdetail','taxa')" id="taxa">展开</a>
                      </div>
                    </div>
                 </div>
              </form>
              <div class="m20" id="divTaxtdetail" style="display:none"  >
                 <table class="table table-list table-hover bg-white mt20">
                    <colgroup>
                     <col width="100">
                     <col width="170">
                     <col width="">
                     <col width="150">
                   </colgroup>
                     <thead>
                          <tr>
                            <th style="padding-left: 20px;" >
                              <input id="checkALLItemstaxt" type="checkbox" onclick="checkALLItemstaxt()">
                              <label for="ex">全选</label>
                            </th>
                             <th>单号</th>
                             <th>商品/数量</th>
                             <th>关税</th>
                          </tr>
                     </thead>
                     <tbody>
                     	  <c:forEach var="pkg"  items="${payTaxtList}">
                     	     <tr>
                             <td><input type="checkbox" name="select2" id="${pkg.logistics_code}" value="${pkg.customs_cost}"  class="checkbox2"></td>
                             <td>${pkg.logistics_code}</td>
                             <td>  <c:forEach var="goods"  items="${pkg.goods}">
										 ${goods.goods_name}*${goods.quantity} 
										  </c:forEach>
							</td>
                             <td>
                              <span class="text-info pr10"><fmt:formatNumber value="${pkg.customs_cost+0.00001}" type="currency" pattern="$#,###.##"/> 
                             </span>
                             </td>
                             </tr> 
							   </c:forEach>  
                     </tbody>
                 </table>
              </div>
       <!--        <div class="duties-coupon">
                  <form class="form-horizontal">
                    <div class="p10 bg-gray m20">
                       <div class="form-group clearfix " style="margin-bottom: 0px;">
                           <label for="" class="col-sm-2 control-label text-right">使用优惠券：</label>
                           <div class="col-sm-3 ">
                                 <select class="form-control">
                                   <option>1</option>
                                 </select>
                           </div> 
                       </div>
                    </div>
                  </form>
              </div> -->
              <div class="text-center pb20">
                   <a class="btn btn-primary2 btn-p-radius"   id="return" onclick="window.history.back(-1); return false" >返回上一页</a> 
                  <button class="btn btn-primary btn-p-radius" id="toSettlement" onclick="toSettlement()">去结算</button>
              </div>
             
            </div>
             <!-- 右边内容-->
            <!-- 左侧菜单 -->
			<jsp:include page="leftPage.jsp"></jsp:include>
        </div>
            <!-- 支付提示 弹出框 -->
<div class="modal fade" id="prompt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;top:-10px;overflow: scroll !important;" >
  <div class="modal-dialog" role="document" style="width:400px">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">支付提示</h4>
      </div>
      <div class="modal-body">
              <!-- 内容 -->
              <div class="blackfont text-center p20"> 
                     <a  onclick="reflesh()"   href="javascript:void(0)" class="btn btn-primary  ok" style="margin-left: 15px;" >重新支付</a>
                    <a   onclick="self.location=document.referrer;"  href="javascript:void(0)" class="btn btn-primary  ok"  style="margin-left: 15px;"  >付款成功</a>
              </div> 
      </div>
    </div>
  </div>
</div>
    </div> 
	<!-- 底部页面 -->
	<jsp:include page="bottomPage.jsp"></jsp:include> 
	<script type="text/javascript" src="${resource_path}/font/iconfont.js"></script>
	<script type="text/javascript" src="${resource_path}/js/jquery-1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="${resource_path}/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script>
$(function(){ 
     $("#checkALLItemsport").attr("checked", true);
     $("#checkALLItemstaxt").attr("checked", true);
     $("input[name='select1']").prop("checked",true); 
     $("input[name='select2']").prop("checked",true); 
     sumSportcount();
     sumTaxcount();
	// 选择框选中
  $(".checkbox1").click(function() {
	  sumSportcount();
	  });
  $(".checkbox2").click(function() {
	  sumTaxcount();
	}); 
})
  </script>
</body>
</html>