<%@ page language="java" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
<script src="${backJsFile}/layer/layer.js"></script>
<script type="text/javascript" src="${resource_path}/js/common.js"></script>
<style type="text/css">




p {
    color: #666666;
    font-family: "微软雅黑";
    font-size: 12px;
    font-weight: normal;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}


.mg18 {
    margin-bottom: 18px;
    margin-left: 0;
    margin-right: 0;
    margin-top: 18px;
}
.hr {
    background-color: #EBEBEB;
    height: 1px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}

.List06 {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 100%;
}
.List06 li {
    height: 52px;
    line-height: 32px;
    list-style-type:none
}
.List06 li span.pull-left {
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: right;
    width: 63px;
}
.List06 li input {
    margin-right: 4px;
}
.List06 li .text08 {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/input24.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    border-bottom-color: -moz-use-text-color;
    border-bottom-style: none;
    border-bottom-width: medium;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: -moz-use-text-color;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: none;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: medium;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: -moz-use-text-color;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: none;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: medium;
    border-top-color: -moz-use-text-color;
    border-top-style: none;
    border-top-width: medium;

    height: 32px;
    line-height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    width: 178px;
}

.PushButton {
    overflow-x: hidden;
    overflow-y: hidden;
    width: 250px;
}
.PushButton a {
    float: right;
    font-size: 16px;
    line-height: 32px;
    margin-left: 16px;
}
.PushButton a.button {
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgba(0, 0, 0, 0);
    background-image: url("${resource_path}/img/button2.png");
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: no-repeat;
    background-size: auto auto;
    color: #FFFFFF;
    height: 32px;
    overflow-x: hidden;
    overflow-y: hidden;
    text-align: center;
    width: 72px;
}

.span4 .label{
     width:70px;
     float: left;
}

</style>
<body>
<div style="background-image:none;border-right:none;width:350px;padding-top:5px;"/>
<input type="hidden" id="checkUserNameResult" value="1">
<input type="hidden" id="old_user_name" value="${old_user_name}">
    <div class="left"></div>
    <div style="margin-left:20px;" class="right">
        <div>
            <div class="hr mg18"></div>
            <div style="display: block;" id="withdrawal">
                <ul class="List06">
                      <li class="span4">
                         <p>
                            <span class="label">旧用户名：</span>
                            <span >${old_user_name}</span>
                         </p>
                      </li>
                      <li class="span4">
                         <p>
                            <span class="label">新用户名：</span>
                            <input type="text" class="text08" id="user_name" name="user_name" style="padding-left: 5px;" onblur="checkUserName()" onkeyup="value=value.replace(/[^\u4E00-\u9FA5^a-z^A-Z^0-9]/g,'') " onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\u4E00-\u9FA5^a-z^A-Z^0-9]/g,''))" >
                            <span style="color:Red;">*</span>
                         </p>
                         <p>
                             <span class="label" >&nbsp;</span>
                             <span id ="message1" style="color: red;"></span>
                         </p>
                      </li>
                  </ul>
                  <div class="PushButton">
                       <a id="no"  class="button" style="cursor:pointer;">取消</a>
                       <a id="yes" class="button" style="cursor:pointer;">确定</a>
                  </div>
           </div>
      </div>
   </div>

    <script type="text/javascript">
    
    
    function checkUserName(){

        var url = "${mainServer}/account/checkUserName";
        var user_name =$('#user_name').val();
        var old_user_name =$('#old_user_name').val();

        if(user_name==null || user_name.trim()=="" ||old_user_name == user_name){
            $('#checkUserNameResult').val("1");
            return;
        }
        $.ajax({
            url:url,
            type:'post',
            async:false,
            data:{"user_name":user_name},
            success:function(data){
                $('#checkUserNameResult').val(data);
                if(data!="1"){
                  $('#message1').html("该用户名已经存在！");
                }
            }
            
        });
    }

    //获取窗口索引
    var index = parent.layer.getFrameIndex(window.name);
    $("#no").click(function () {
        // 关闭窗口
        parent.layer.close(index);
    }); 

      $("#yes").click(function () {

          var user_name =$('#user_name').val();

          if(user_name==null || user_name.trim()==""){
              $('#message1').html("新用户名不能为空！");
              return;
          }
          
          if($('#checkUserNameResult').val()!=1){
              $('#message1').html("该用户名已经存在！");
              return;
           }

          $('#message1').empty();
          var url="${mainServer}/account/saveUserName";
          $.ajax({
              url:url,
              data:{"user_name":user_name},
              type:'post',
              async:false,
              dataType:'json',
              success:function(data){
                  alert(data.message);
                  
                  if(data.result){
                      // 标识点击了保存 给父窗口传值标识修改成功
                      parent.$('#returnCode').val("1");
                      // 关闭窗口
                      parent.layer.close(index);
                  }
              }
          });
      }); 
        </script>
</body>
</html>