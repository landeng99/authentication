<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form id="gopage">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td  style="float:left;">&nbsp;&nbsp;总记录数:${pageView.rowCount}条 |每页显示:${pageView.pageSize}条 | 总页数:${pageView.pageCount}页</td>
            <td><table border="0" align="right" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="40" class="STYLE4">
                  <a href="javascript:;" onclick="pageNow(1);">
                  	首页
                  </a>
                  </td>
                  <td width="45" class="STYLE4">
                  <a href="javascript:void();" onclick="return pageNow(${pageView.pageNow - 1})">
                  	上一页
                  </a>
                  </td>
                  <td align="center">
                  <c:forEach begin="${pageView.pageindex.startindex}" end="${pageView.pageindex.endindex}" var="key">
						<c:if test="${pageView.pageNow==key}">
							&nbsp;<span class="current" style="color: red;font-size: 20px;"> ${key}</span>
						</c:if>
						<c:if test="${pageView.pageNow!=key}">
							&nbsp;<a href="javascript:;" onclick="pageNow(${key})">${key}</a>
						</c:if>
					</c:forEach>&nbsp;
                  </td>
                  <td width="45" class="STYLE4">
                  <a href="javascript:;" onclick="pageNow(${pageView.pageNow + 1})">
                  	下一页
                  </a>
                  </td>
                  <td width="48" class="STYLE4">
                  <a href="javascript:;" onclick="pageNow(${pageView.pageCount})">
                 	&nbsp;&nbsp;尾页&nbsp;
                  </a>
                  </td>
                   <td><input  name="pagenoto" id="pagenoto" type="text" class="text05"  style="overflow: auto; height: 24px; line-height: normal;border: 1px solid #ccc;width: 32px;margin-right:10px;text-align:center;" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" onafterpaste="this.value=this.value.replace(/[^0-9]/g,'')"></td>
                  <td class="STYLE4">
                  <a class="back" href="javascript:gotopage()">前往</a>
                  </td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
    <input type="hidden"  name="allpageno" id="allpageno" type="text" class="text05"  style="overflow: auto; height: 24px; line-height: normal;border: 1px solid #ccc;width: 32px;" value="${pageView.pageCount}" >

   </form>
    <script type="text/javascript"> 
    function pageNow(index){
    	pagerClick(index);
    }
    function gotopage(){
   	 var pagenoto = document.getElementById("pagenoto").value;
   	 var allpage = document.getElementById("allpageno").value;
   	 if(pagenoto > allpage){
   		layer.msg("输入页码不能大于总页码", 2, 5);
   		return;
   	 }
   	 pageNow(pagenoto);
   }
    
    </script>