<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>速8快递 专业的跨境转运</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
<meta name="keywords" content="速8快递，跨境转运，转运公司，海淘，海外购物"/>
<meta name="description" content="速8快递，拥有美国仓库及专业物流配送体系"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link href="${resource_path}/css/syindex.css" rel="stylesheet" type="text/css"/>
	<link href="${resource_path}/css/sydialog.css" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="${resource_path}/img/favicon.ico" media="screen" />
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycommon.css">
	<link rel="stylesheet" type="text/css" href="${resource_path}/css/sycount.css">
	<link href="${resource_path}/css/common.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="${resource_path}/js/jquery-1.8.3.all.js"></script>
	<script type="text/javascript" src="${resource_path}/js/TransportNewList.js"></script>	
	<script type="text/javascript" src="${resource_path}/js/layer.min.js"></script>
	<script type="text/javascript" src="${resource_path}/js/common.js"></script>
	<script type="text/javascript" src="${resource_path}/js/json2.js"></script>
	<script type="text/javascript" src="${resource_path}/js/validateLogin.js"></script>
	<script src="${resource_path}/js/json2.js"></script>
	<script type="text/javascript">
		var mainServer = '${mainServer}';
		var backServer = '${backServer}';
		var jsFileServer = '${backJsFile}';
		var cssFileServer = '${backCssFile}';
		var imgFileServer = '${backImgFile}';
		var uploadPath = '${uploadPath}';
	</script>
	<script>
        document.domain='wagang.net';
    </script>
</head>

