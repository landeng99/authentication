<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

 <style type="text/css">
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 80px;
	}
	
	.field-group .field-item .interval_label{
		float: left;
	}
	.field-group .field-item .field{
		float:left;
		width: 203px;
	}
	
	.pickorder-list{
		margin-top: 20px;
	}
</style>

<body>
	<div class="admin">
	<div class="admin_context">
		    <div class="field-group">
				<form id="form" action="${backServer}/pickorder/search">
				 <div class="field-item">
					  <div class="label"><label for="pick_code">提单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="pick_code" name="pick_code" value="${params.pick_code}" style="width:200px" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                    </div>
				 </div>
                <div class="field-item">
                	 <div class="label"><label for="flight_num">空运单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="flight_num" name="flight_num" value="${params.flight_num}" style="width:200px" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                </div>
               <div class="field-item">
                	 <div class="label"><label for="pick_code">运单号:</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="logistics_code" name="logistics_code" value="${params.logistics_code}" style="width:200px"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>
               <div class="field-item">
                	 <div class="label"><label for="lastname">快递口岸:</label></div>
                    <div class="field">
                    	<select  class="input" id="seaport_id" name="seaport_id" alue="" style="width:200px" >
                    		  <option value="">-------选择口岸-------</option>
                    		<c:forEach var="seaport" items="${seaportList}">
                    			<option value="${seaport.sid }">${seaport.sname}</option>
                    		</c:forEach>
                    	</select>
                    </div>
                </div>
                <div class="field-item">
                	 <div class="label"><label for="status">状态:</label></div>
                    <div class="field">
                        <select  class="input" id="status" name="status" style="width:200px" >
                        	<option value="">-------选择提单状态-------</option>
                    		<option value="1">已出库</option>
                    		<option value="2">空运中</option>
                    		<option value="3">待清关</option>
                    	</select>
                    </div>
                </div>
                 <div class="field-item">
                	 <div class="label"><label for="status">仓库:</label></div>
                    <div class="field">
                    	<select  class="input" id="overseas_address_id" name="overseas_address_id" style="width:200px" >
                   		  <option value="">---------选择仓库---------</option>
                    		<c:forEach var="userOverseasAddress" items="${userOverseasAddressList}">
                    			<option value="${userOverseasAddress.id}">${userOverseasAddress.warehouse}</option>
                    		</c:forEach>
                   		</select>
                    </div>
                </div>
                
                <div class="field-item">
                	<div class="label"><label for="lastname">创建时间:</label></div>
                    <div class="field">
                 	<input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                  			onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="${params.fromDate}"  onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                   	<div class="interval_label"><label for ="interval">~</label></div>
                   	<div class="field">
                	<input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                   		onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
            	value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                   	</div>
                </div>
                   <div style="clear: both"></div>
                 <div class="form-button">
                     	<input type="submit" class="button bg-main icon-search" id="search" value="查询"/>
				  </div>
				 </form> 
             </div>
             <div style="clear: both"></div>
             <form method="post">
	         <div class="panel admin-panel pickorder-list">
	         	     <div class="panel-head"><strong>提单列表</strong></div>
	         	     <div class="padding border-bottom">
	         	    <shiro:hasPermission name="pickorder:add">
  						 <input type="button" id="add" value="添加" class="button button-small border-green"/>
   	   			     </shiro:hasPermission>
			        </div>
	         	     <table class="table table-hover" id="pickorderlist">
	         	     	<tr>
	         	     		<th >提单号</th>
	         	     		<th >空运单号</th>
	         	     		<th >口岸</th>
	         	     		<th >仓库</th>
	         	     		<th >托盘数</th>
	         	     		<th >包裹数</th>
	         	     		<th >状态</th>
	         	     		<th >创建时间</th>
	         	     		<th >操作</th>
	         	     	</tr>
	         	     	<c:forEach var="pickorder"  items="${pickOrderList}">
	         	     	<tr>
		         	     	<td>${pickorder.pick_code}</td>
		         	     	<td>${pickorder.flight_num}</td>
		         	     	<td>${pickorder.sname}</td>
		         	     	<td>${pickorder.warehouse}</td>
		         	     	<td>${pickorder.palletCnt}</td>
		         	     	<td>${pickorder.pkgCnt}</td>
		         	     	<td>
		         	     		<c:if test="${pickorder.status==1}">已出库</c:if>
		         	     		<c:if test="${pickorder.status==2}">空运中</c:if>
		         	     		<c:if test="${pickorder.status==3}">待清关</c:if>
		         	     	</td>
		         	     	<td><fmt:formatDate value="${pickorder.create_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		         	     	<td>
		         	     	 <shiro:hasPermission name="pickorder:edit">
			         	     	<a href="${backServer}/pickorder/initedit?pick_id=${pickorder.pick_id}" class="button button-small border-green">编辑</a>&nbsp; 
			         	     </shiro:hasPermission>
			         	     <shiro:hasPermission name="pickorder:select">
			         	     	<a href="${backServer}/pickorder/detail?pick_id=${pickorder.pick_id}"  class="button button-small border-green">详情</a>&nbsp; 
			         	     </shiro:hasPermission>
			         	     <a href="javascript:;" onclick="exportPickorder(${pickorder.pick_id},'${pickorder.pick_code}')" class="button button-small border-green">导出包裹</a>
			         	    <a href="javascript:;" onclick="exportIdcard(${pickorder.pick_id},'${pickorder.pick_code}')"  class="button button-small border-green">导出身份证</a>
			         	     <shiro:hasPermission name="pickorder:delete">
			         	     	<c:if test="${pickorder.status==1 and pickorder.pkgCnt==0}">
			         	     		<a href="javascript:;"  onclick="del(${pickorder.pick_id})" class="button button-small border-yellow">删除</a>&nbsp; 
			         	     	</c:if>
			         	     </shiro:hasPermission>
			         	     
		         	     	</td>
	         	     	</tr>
	         	     	</c:forEach>
	         	     </table>
		        	<div class="panel-foot text-center">
	      				<jsp:include page="pager.jsp"></jsp:include>
	      			</div>	
	         </div>	
	         </form>
	         </div>
	</div>
	<script type="text/javascript">
	    function initSearchField(){
	    	$('#seaport_id').val('${params.seaport_id}');
	    	$('#overseas_address_id').val('${params.overseas_address_id}');
	    	$('#status').val('${params.status}');
	    }
	    initSearchField(); 
		$(function(){
			$('#add').click(function(){
				window.location.href="${backServer}/pickorder/initadd";
			});				
		});	
 
		$('#delbatch').click(function(){
			
			var checkboxs=$('#pickorderlist').find('input:checked');
			
			var array=[];
			$.each(checkboxs,function(i,checkbox){
				var pick_id=$(checkbox).val();
				array.push(pick_id);
			});
			
			if(array.length==0){
				
				alert("请选择，再删除");
			}else{
				window.location.href="${backServer}/pickorder/delpickorderbatch?pick_ids="+array.join(',');
			}
			 
		});
		function del(pick_id){
			
			var url="${backServer}/pickorder/delpickorder?pick_id="+pick_id;
			if(window.confirm("确定删除吗?")){
				window.location.href=url;
			}
		}
		
		function exportIdcard(pick_id,pick_code){
			window.open('${backServer}/pickorder/exportIdcard?pick_id='+pick_id+"&pick_code="+pick_code);
		}
		
		function exportPickorder(pick_id,pick_code){
			layer.open({
			    type: 2,
			    title: '导出列表',
			    shift: 2,
			    shadeClose: true,  
			    area: ['420px', '240px'], 
			    content: '${backServer}/pickorder/initexport?pick_id='+pick_id+"&pick_code="+pick_code
			});
		}
		
	     //回车事件
	     $(function(){
	       document.onkeydown = function(e){
	           var ev = document.all ? window.event : e;
	           if(ev.keyCode==13) {

	               $('#form').submit(); 
	            }
	       }
	     });
	</script>
</body>
</html>