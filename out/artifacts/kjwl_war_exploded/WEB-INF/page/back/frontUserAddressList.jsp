<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<body>

<div class="admin">
    <div class="panel admin-panel">
    <input type="hidden" id="user_type_bk" value="${params.user_type}">
    <input type="hidden" id="idcard_status_bk" value="${params.idcard_status}">
      <div class="panel-head"><strong>账号审核列表</strong></div>
      <div class="tab-body">
        <br />
            <form action="${backServer}/frontUser/frontUserAddressSearch" method="get" id="myform" name="myform">
                 <div style="height: 40px;">
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户类型:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <select id="user_type" name="user_type" class="input" style="width:150px">
                            <option value="">请选择</option>
                            <option value="1">普通用户</option>
                            <option value="2">同行用户</option>
                         </select>
                    </div>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>身份状态:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <select id="idcard_status" name="idcard_status" class="input" style="width:150px">
                            <option value="">请选择</option>
                            <option value="0">未审核</option>
                            <option value="1">审核通过</option>
                            <option value="2">审核拒绝</option>
                         </select>
                    </div>
                    
                    
                    <%-- <div style="float:left;margin-top:5px;margin-left:10px"><span><strong>注册时间:</strong></span></div>
                    <div style="float:left;margin-left:10px;margin-top:6px">
                       <span>从</span>
                    </div>
                    <div style="float:left;">
                        <input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                        onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
                            value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                    <div style="float:left;margin-left:10px;margin-top:6px">
                       <span>到</span>
                    </div>
                    <div style="float:left;">
                       <input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                      onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div> --%>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>身份证号:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="idcard" name="idcard" style="width:164px"
                                value="${params.idcard}"/>
                    </div>
                    
                    
                 </div>
                 
                 <div style="height: 40px;" >
                 
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户账号:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="account" name="account" style="width:150px"
                                value="${params.account}"/>
                    </div>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户姓名:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="user_name" name="user_name" style="width:150px"
                                value="${params.user_name}"/>
                    </div>
                    
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>手机号码:</strong></span></div>
                   	<div style="float:left;margin-left:10px;width:160px">
                       	<input type="text" class="input" id="mobile" name="mobile" style="width:164px"
                               value="${params.mobile}"/>
                  	</div>
                    
                    
                    
                 </div>
                <%--  <div style="height: 40px;" >
                 	<div style="float:left;margin-top:5px;margin-left:10px;"><span><strong>用户邮箱:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                        <input type="text" class="input" id="email" name="email" style="width:164px"
                               value="${params.email}"/>
                    </div>
                	 
                 </div> --%>
                
            </form>
            	<div style="height: 40px;">
                 
                 	<div class="padding border-bottom">
                    	<input type="button" class="button bg-main" onclick="search()" value="查询" />
                 	</div>
                 </div> 
                 
                 <table class="table" style="margin-top: 12px;">
                    <tr>
                        <th width="9%">用户类型</th>
                        <th width="17%">用户账号</th>
                        <th width="16%">用户姓名</th>
                        <th width="18%">身份证号</th>
                        <th width="17%">手机号码</th>
                        <th width="8%">身份状态</th>
                        <th width="15%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="frontUserAddress"  items="${frontUserAddressList}">
                     <tr>
                         <td width="9%">
                           <c:if test="${frontUserAddress.user_type==1}">普通用户</c:if>
                           <c:if test="${frontUserAddress.user_type==2}">同行用户</c:if>
                         </td>
                         <td width="17%">${frontUserAddress.account}</td>
                         <td width="16%">${frontUserAddress.user_name}</td>
                         <td width="18%">
                         	${frontUserAddress.idcard}
                             <%-- <c:if test="${fn:length(frontUserAddress.street)>30}">
                               ${fn:substring(frontUserAddress.street,0,30)}......
                             </c:if>
                             <c:if test="${fn:length(frontUserAddress.street)<=30}">
                               ${frontUserAddress.street}
                             </c:if> --%>
                         </td>
                         <td width="17%">${frontUserAddress.mobile}</td>
                         <td width="8%">
                           <c:if test="${frontUserAddress.idcard_status==0}">未审核</c:if>
                           <c:if test="${frontUserAddress.idcard_status==1}">审核通过</c:if>
                           <c:if test="${frontUserAddress.idcard_status==2}">审核拒绝</c:if>
                         </td>
                         <td width="15%">
                         <shiro:hasPermission name="frontUserfrontUserAddressInit:select">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="detail('${frontUserAddress.user_id}','${frontUserAddress.address_id}')">详情</a>
                         </shiro:hasPermission><shiro:hasPermission name="frontUserfrontUserAddressInit:edit">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="update('${frontUserAddress.user_id}','${frontUserAddress.address_id}')">审核</a>
                         </shiro:hasPermission>
                         </td>
                         </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
        
            //选择框选中
            $("#user_type").val($("#user_type_bk").val());
            $("#idcard_status").val($("#idcard_status_bk").val());
            $('#idcard_status').focus();
        });
        
        function detail(user_id,address_id){
            var url = "${backServer}/frontUser/frontUserAddressDetail?user_id="+user_id+"&address_id="+address_id;
            window.location.href = url;
        }
        function update(user_id,address_id){
            var url = "${backServer}/frontUser/frontUserAddressUpdate?user_id="+user_id+"&address_id="+address_id;
            window.location.href = url;
        }

        function search(){
            document.getElementById('myform').submit();
        }
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search();
               }
          }
        });

    </script>
</body>
</html>