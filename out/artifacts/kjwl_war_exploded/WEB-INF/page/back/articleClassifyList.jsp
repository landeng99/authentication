<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %> 
<link rel="stylesheet" href="${backCssFile}/jquery.treetable.css">
<link rel="stylesheet" href="${backCssFile}/jquery.treetable.theme.modified.css">
<script type="text/javascript" src="${backJsFile}/jquery.treetable.js"></script>
 

<body>
<div class="admin">
    <div class="panel admin-panel">
    	<div class="panel-head"><strong>文章分类列表</strong></div>
      <div class="padding border-bottom">
      		<input type="button" class="button button-small border-green" onclick="add('0')" value="添加文章分类" />
      </div>
      <table id="pkg_category" class="table table-hover">
      	<tr>
      		<th>文章分类名称</th>
      		<th>操作</th>
      	</tr>	
      	<c:forEach var="list"  items="${articleClassifyLists}">
      		<tr data-tt-id="${list.id }" data-tt-parent-id="${list.parent_id }">
	 			<td>${list.name }</td>
	 			<td>
	 		<shiro:hasPermission name="articleClassify:edit">
				<a class="button border-blue button-little" href="javascript:void(0);" onclick="add('${list.id }')">编辑</a>&nbsp;&nbsp;
			</shiro:hasPermission><shiro:hasPermission name="articleClassify:add">
				<a class="button border-blue button-little" href="javascript:void(0);" onclick="dele('${list.id }')">删除</a>
			</shiro:hasPermission>
	 			</td>
		   </tr>
      	
      	</c:forEach>
      </table>
   		 
      </div>
</div>

</body>
<script type="text/javascript">
	$("#pkg_category").treetable({expandable: true,initialState:"expanded" });

	function add(id){
		var url = "${backServer}/articleClassify/add?id="+id;
		window.location.href = url;
	}
	function dele(id){
		var url = "${backServer}/articleClassify/deleArticleClassify?id="+id;
		window.location.href = url;
	}
</script>
</html>