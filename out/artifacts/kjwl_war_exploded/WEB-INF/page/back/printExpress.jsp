<%@ page language="java"  pageEncoding="utf-8"%>
<script src="${resource_path}/js/jquery-1.9.0.js"></script>
<script src="${resource_path}/js/jquery.PrintArea.js"></script>
<%-- <script src="${resource_path}/js/jquery-1.7.2.min.js"></script> --%>
<%-- <script src="${resource_path}/js/jquery.jqprint.js"></script> --%>
<script src="${resource_path}/js/jquery-barcode.js"></script>
<link rel="stylesheet" href="${resource_path}/css/print.css" media="print" />
<body>
<style>
	body{
		font-size: 12px;
	}

 	.my_show{border: 1px solid #000;height:700px;}
 	.my_show .top{margin-top: 10px;}
	.my_show .logo_title{
		float:left;margin-left:60px;margin-top:0px;
		
	}
	 .logo_title h1{
		margin:auto;
		margin:0px;
		font-size:60px;	
		font-weight:bold;
	}
	
	
	.my_show .top .part_2{
			border-top: 1px solid #000;
			height: 80px;
	}

.my_show .top .part_3{
		border-top:1pt solid #000;
		height: 100px;
	}
 .my_show  .middle{
	    border-top:1px dashed #000;
	}
.my_show  .middle  .part_1{
	height: 100px;
}
.my_show  .middle .part_2{
	border-top:1pt solid #000;
	height: 25px;
	line-height: 25px;
}
.my_show  .middle .part_3{
	border-top:1px solid #000;
	height: 100px;
}
.my_show .buttom{
	border-top:1px dashed #000;
}
.my_show .buttom .part_1{
	margin-top: 10px;
}
.td_style1 {
	BORDER-RIGHT: 1px ; 
	BORDER-LEFT:  1px ; 
	BORDER-TOP: 1px solid ;
	BORDER-BOTTOM: 1px solid ;
}

.td_style2 {
	BORDER-TOP: 1px dashed;
	BORDER-BOTTOM: 1px solid;
	height: 25px;margin-top:7px;
}

.td_style3 {
	BORDER-BOTTOM: 1px solid;
}
 	.title_black {
		font-size:12pt;
		color: #000000;
		font-style: 宋体;
		font-weight:bold;
		float:left;
		margin-left:10px;
	}
.text_left{
	float:left;margin-left:20px;width:150px;font-size:13px;
}
.text_addr{width: 300px;}
#128Code1{float:left;margin-left:20px;margin-top:20px;}

#128Code2{
	float:left;margin-left:20px;margin-top:14px;
}
.qr_code{
	float: left;
	margin-left: 40px;
}
.code2{float: left}
.addr_context{height: 30px;	clear: both;margin-top: 10px;}
</style>
    <div class="admin">
        <div >
            <div style="float:left;">
              <input type="button" id="print"  value="打印"/>
              <input type="hidden" id="code"  value="${pkgDetail.logistics_code}"/>
            </div>
            <br/>
                  <div style="height:750px;margin-top:10px ">	
                        <div class ="my_show" style="width:558px;">
                            <div class="top">
                            	<div class="part_1">
                                     <div class="logo_title" style="">
                                   	  <H1><strong>跨&nbsp境</strong></H1>
                                     </div>
                                     <div id="128Code1"></div> 
                            	</div>
                            	<div class="part_2">
                            		 <div style="height: 30px;">
                                        <div class="title_black" ><span>寄件人(from)：</span></div>
                                        <div class="text_left" >
                                          <span>${frontUser.user_name}</span>
                                        </div>
                                       <div class="title_black" style="float:left;"><span>电话/Tel：</span></div>
                                       <div  style="float:left;margin-left:5px;">
                                           <Span>${frontUser.mobile}</Span>
                                        </div>
                                      </div>
                                     <div class="addr_context">
                                       <div class="title_black" style="float:left;margin-left:10px"><span >地址/Address：</span></div>
                                       <div class="text_addr"  style="float:left;margin-left:20px;width:300px;">
                                         <Span>${osaddr}</Span>
                                       </div>
                                     </div>
                            	</div>
                            	<div class="part_3">
                            		 <div style="height: 25px;margin-top:7px;">
                                       <div class="title_black" style="float:left;margin-left:10px">
                                       <span class ="title_black">收件人/To：</span></div>
                                        <div class="text_left" >
                                          <Span>${userAddress.receiver}</Span>
                                        </div>
                                        <div class="title_black" style="float:left;">
                                        <span class ="title_black">电话/Tel：</span></div>
                                        <div style="float:left;margin-left:5px;">
                                           <Span>${userAddress.mobile}</Span>
                                        </div>
                                     </div>
                                     <div class="addr_context">
                                        <div class="title_addr" style="float:left;margin-left:10px">
                                        <span class ="title_black">地址/Address：</span></div>
                                        <div class="text_addr" style="float:left;margin-left:20px;">
                                           <Span>${address}</Span>
                                        </div>
                                     </div>
                                     <div style="height: 25px;">
                                        <div style="float:left;margin-left:294px"><span class ="title_black">邮编/Post Code：</span></div>
                                        <div  style="float:left;">
                                          <Span>${userAddress.postal_code}</Span>
                                        </div>
                                     </div>
                            	</div>
                             </div>
                             <div class="middle">
                             	<div class="part_1">
                                 <div style="height: 20px;margin-top:7px;">
                                       <div  style="float:left;margin-left:10px"><span >实际重量：</span></div>
                                       <div style="float:left;width:60px;">
                                          <Span>${pkgDetail.actual_weight}</Span>
                                       </div>
                                       <div style="float:left;margin-left:30px;">
                                         <Span>磅</Span>
                                       </div>
                                     </div>
                                     <div style="height: 20px;">
                                        <div style="float:left;margin-left:10px;font-weight:bold;font-size:16px;">关联单号：</div>
                                     </div>
									 <div style="float:left;margin-left:10px;font-size:22px;font-weight:bold">${pkgDetail.original_num}</div>
                             	</div>
                             <div class="part_2">
                                       <div style="float:left;margin-left:10px;width:200px;">收件人签名：</div>
                                       <div style="float:left;margin-left:20px;">
                                         <span>签收时间： &nbsp&nbsp&nbsp&nbsp年&nbsp&nbsp&nbsp&nbsp月 &nbsp&nbsp&nbsp日 &nbsp&nbsp&nbsp时</span>
                                       </div>
                                   
                             </div>
                             <div class="part_3">
                             	    <div style="height:25px;margin-top:7px;">
                                       <div style="float:left;margin-left:10px"><span >寄件人(from)：</span></div>
                                       <div class="text_left">
                                          <span>${frontUser.user_name}</span>
                                       </div>
                                       <div style="float:left;"><span>电话/Tel：</span></div>
                                       <div style="float:left;margin-left:5px;">
                                         <Span>${frontUser.mobile}</Span>
                                       </div>
                                     </div>
                                     <div style="height: 50px;">
                                        <div class="title_addr" style="float:left;margin-left:10px"><span >地址/Address：</span></div>
                                        <div class="text_addr" style="float:left;margin-left:20px;">
                                          <Span>${osaddr}</Span>
                                        </div>
                                     </div>
                             </div>
                             </div>
                             
                             <div class="buttom">
                             	
                             	<div class="part_1">
                                   <div class="button_addr" style="height:25px;">
                                        <div style="float:left;margin-left:10px"><span class ="title_black">收件人/To：</span></div>
                                         <div class="text_left">
                                            <span>${userAddress.receiver}</span>
                                         </div>
                                         <div style="float:left;"><span class ="title_black">电话/Tel：</span></div>
                                         <div  style="float:left;margin-left:5px;">
                                            <Span>${userAddress.mobile}</Span>
                                        </div>
                                     </div>
                                     <div class="addr_context">
                                       <div class="title_addr" style="float:left;margin-left:10px"><span class ="title_black">地址/Address：</span></div>
                                       <div class="text_addr" style="float:left;margin-left:20px;">
                                         <Span>${address}</Span>
                                       </div>
                                     </div>
                                     <div class="print_footer" style="height:100px;clear: both;">
                                          <div class="code2">
	                                          <div id="128Code2"></div>
	                                          <div style="float:left;margin-left:120px;margin-top:10px;clear: both;"><span>原寄地：${country}</span></div>
                                          </div>
                                          <div class="qr_code" >
                                          	<%-- <img src="${resource_path}/img/site.png" />
                                          	<div class="site">www.su8exp.com</div> --%>
                                          </div>
                                     </div>
                             	</div>
                             </div>
                        </div>
                     </div>
                     
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function() {
        var code =$('#code').val();
        $('#128Code1').barcode(code, "code128",{barWidth:2,fontSize:20});
        $('#128Code2').barcode(code, "code128",{barWidth:2,fontSize:20});
         $('#print').click(function() {
     /*        $('.my_show').jqprint({
            	debug:true,
            	importCSS:true,
            }); */
            $('.my_show').printArea();
        }); 
    });
</script>
</body>
</html>