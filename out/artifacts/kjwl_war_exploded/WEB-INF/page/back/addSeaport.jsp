<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>
<style>
	.text_item {padding: 30px 20px;}
	.text_item span {margin-left: 10px;}
</style>
<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>添加口岸</strong></div>
      <div class="tab-body">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
         <li class="active"><a href="#tab-basic" >基本信息</a></li>
          <li ><a href="#tab-order-info">面单信息</a></li>
        </ul>
      </div>
      
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-basic">
                 <input type="hidden" id="checkSnameResult" value="0">
                 <input type="hidden" id="checkScodeResult" value="0">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>口岸分类:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="stype" name="stype" style="width:150px" />
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="stypeTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>口岸名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="sname" name="sname" style="width:150px" onblur="checkSname()"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="snameTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>口岸编码:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="scode" name="scode" style="width:150px" onblur="checkScode()"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="scodeTextArea"></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>信息数量:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="userinfo_limit" name="userinfo_limit" style="width:70px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="limitTextArea"></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>是否需要身份证图片:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
						<input
							type="radio"  value="1" name="ischeck_idcard_img" id="ischeck_idcard_img" 
							>是
							<input type="radio"
							  value="0" name="ischeck_idcard_img" id="ischeck_idcard_img" checked="checked"
							 style="margin-left: 20px;">否
                    </div>
                 </div>
<!--                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>是否快件口岸:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
						<input
							type="radio"  value="1" name="express_seaport" id="express_seaport" 
							>是
							<input type="radio"
							  value="0" name="express_seaport" id="express_seaport" checked="checked"
							 style="margin-left: 20px;">否
                    </div>
                 </div> -->
        </div>

        <div class="tab-panel" id="tab-order-info">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>导入报关号码:</strong></span></div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>表格导入:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="button" onclick="showDeclarationDialog()" value="请选择EXCEL">
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>剩余提醒:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="leave_warning_count" name="leave_warning_count" value="0" style="width:150px" 
                              onkeyup="value=value.replace(/[^\d\.\/]/g,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="left_warning_countTextArea"></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>模板设计</strong></span></div>
                    <div style="float:left;margin-top:5px;margin-left:100px;">
						<span><strong>查看效果</strong></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label">
                    	  <input type="button" value="导入模板" onclick="showUploadSeaportPrintDialog()">
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:50px;">
						<input type="button" value="点击预览" id="click_review" name="click_review" onclick="reviewDemoDialog()" style="width:100px"/>
                    </div>
                 </div>
        </div>

        </div>
   </div> 

          <div class="padding border-bottom" >
          <input type="button" class="button bg-main" onclick="add()" value="添加" />
          <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
          </div>
      </div>
    </div>
</div>

<!-- 导入 报关号码-->
<div id="declarationDialog">
     <form id="importDeclarationForm"  enctype="multipart/form-data" target="importDeclarationIFrame" method="post" action="${backServer}/seaport/importDeclaration">
            <div class="text_item">
            	<span >
              		<input type="file" title="请选择EXCEL" id="import_data_file" name="import_data_file" style="width:150px"/>
				</span>
				<br>
				<br>
				<span>
                	<button type="submit" class="button bg-main">导入</button>
                	&nbsp;&nbsp;&nbsp;
                	<a id="downTemplate" target="_blank" href="${resource_path}/download/seaport_declaration_template.xlsx">下载导入模板模板</a>
             	</span>
             </div>
      </form>
      <input type="hidden" name="cacheReImportId" id="cacheReImportId">
      <iframe id="importDeclarationIFrame" name="importDeclarationIFrame" style="display: none;" onload="importOnloadDeclaration(this)"></iframe>
</div>

<!-- 上传口岸打印出库面单模板-->
<div id="uploadSeaportPrintDialog">
     <form id="uploadSeaportPrintForm"  enctype="multipart/form-data" target="uploadSeaportPrintIFrame" method="post" action="${backServer}/seaport/uploadSeaportPrint">
            <div class="text_item">
            	<span >
              		<input type="file" title="请选择口岸打印出库面单模板" id="import_model_file" name="import_model_file" style="width:150px"/>
				</span>
				<br>
				<br>
				<span>
                	<button type="submit" class="button bg-main">上传</button>
                	&nbsp;&nbsp;&nbsp;
                	<a id="downTemplate" target="_blank" href="${resource_path}/download/seaport_print_template.zip">下载口岸打印出库面单设计模板</a>
             	</span>
             </div>
      </form>
      <input type="hidden" name="template_path" id="template_path">
      <iframe id="uploadSeaportPrintIFrame" name="uploadSeaportPrintIFrame" style="display: none;" onload="importOnloadSeaportPrint(this)"></iframe>
</div>

    <script type="text/javascript">
    $(function(){
        $('#stype').focus();
    });
    
    //口岸名称文本框鼠标失去焦点，校验口岸名称是否已经存在
    function checkSname(){
        var url = "${backServer}/seaport/checkSname";
        var sname =$('#sname').val();
        if(sname==null||sname.trim()==""){
            return;
        }
        $.ajax({
            url:url,
            type:'post',
            data:{"sname":sname},
            success:function(data){
                $('#checkSnameResult').val(data);
                if(data=="1"){
                    
                    $('#snameTextArea').text('口岸名称可以使用!');
                    $('#snameTextArea').css('color','green');
                }else{
                    $('#snameTextArea').text('口岸名称已经存在!');
                    $('#snameTextArea').css('color','red');
                }
            }
            
        });

    }
    
    //口岸编码文本框鼠标失去焦点，校验口岸名称是否已经存在
    function checkScode(){
        var url = "${backServer}/seaport/checkScode";
        var scode =$('#scode').val();
        if(scode==null||scode.trim()==""){
           return;
        }
        $.ajax({
            url:url,
            type:'post',
            data:{"scode":scode},
            success:function(data){
                $('#checkScodeResult').val(data);
                if(data=="1"){
                    
                    $('#scodeTextArea').text('口岸编码可以使用!');
                    $('#scodeTextArea').css('color','green');
                }else{
                    $('#scodeTextArea').text('口岸编码已经存在!');
                    $('#scodeTextArea').css('color','red');
                }
            }
            
        });

    }

    function add(){
        var checkSnameResult =$('#checkSnameResult').val();
        var checkScodeResult =$('#checkScodeResult').val();

        var url = "${backServer}/seaport/insertSeaport";
        
        var stype =$('#stype').val();
        var sname =$('#sname').val();
        var scode =$('#scode').val();
        var userinfo_limit =$('#userinfo_limit').val();
        var checkResult ="0"
        var ischeck_idcard_img=$('input[name="ischeck_idcard_img"]:checked').val();
        //var express_seaport=$('input[name="express_seaport"]:checked').val();
        if(stype==null||stype.trim()==""){
            $('#stypeTextArea').text('口岸分类不能为空!');
            $('#stypeTextArea').css('color','red');
            checkResult ="1";
        }

        if(sname==null||sname.trim()==""){
            $('#snameTextArea').text('口岸名称不能为空!');
            $('#snameTextArea').css('color','red');
            checkResult ="1";
        }
        if(scode==null||scode.trim()==""){
            $('#scodeTextArea').text('口岸编码不能为空!');
            $('#scodeTextArea').css('color','red');
            checkResult ="1";
        }
        if(userinfo_limit==null||userinfo_limit.trim()==""){
            $('#limitTextArea').text('信息数量不能为空!');
            $('#limitTextArea').css('color','red');
            checkResult ="1";
        }
        var checkNum = /^[1-9]+[0-9]*]*$/;
        if(!checkNum.test(userinfo_limit)){
            $('#limitTextArea').text('信息数量必须为正整数!');
            $('#limitTextArea').css('color','red');
            checkResult ="1";
        }
        
        var leave_warning_count=$('#leave_warning_count').val();
        if(leave_warning_count==null||leave_warning_count.trim()==""){
            $('#limitTextArea').text('剩余提醒数量不能为空!');
            $('#limitTextArea').css('color','red');
            checkResult ="1";
        }
        var checkNum1 = /^[0-9]+[0-9]*]*$/;
        if(!checkNum1.test(leave_warning_count)){
            $('#limitTextArea').text('剩余提醒数量必须为0或者正整数!');
            $('#limitTextArea').css('color','red');
            checkResult ="1";
        }
        
        if(checkResult =="1"){
           return;
        }
        
        if(checkSnameResult!="1"){
            alert("口岸名称已经存在!");
            return;
        }
        if(checkScodeResult!="1"){
            alert("口岸编码已经存在!");
            return;
        }
        
        $('#stypeTextArea').empty();
        $('#snameTextArea').empty();
        $('#scodeTextArea').empty();
        $('#limitTextArea').empty();
        
        var template_path=$('#template_path').val();
        var cacheReImportId=$('#cacheReImportId').val();
       
        $.ajax({
            url:url,
            data:{
                "stype":stype,
                "sname":sname,
                "scode":scode,
                "ischeck_idcard_img":ischeck_idcard_img,
                //"express_seaport":express_seaport,
                "template_path":template_path,
                "cacheReImportId":cacheReImportId,
                "leave_warning_count":leave_warning_count,
                "userinfo_limit":userinfo_limit},
            type:'post',
            dataType:'text',
            success:function(data){
               alert("添加成功！");
               window.location.href = "${backServer}/seaport/seaportInit";
            }
        });
    }
    
    //显示导入报关号码对话框
    function  showDeclarationDialog(){
    	layer.open({
            title :'导入报关号码',
            type: 1,
            shadeClose: true,
            shade: 0.8,
            area: ['350px', '200px'],
            content:$('#declarationDialog'),
            end:function(){
            }
        });
    }
    function importOnloadDeclaration(dom)
    {
    	var text = $("#importDeclarationIFrame").contents().find("body").text();
    	if(text == ""){return;}
    	try
    	{
    		data = JSON.parse(text);
    		console.log(data)
    		if(data.flag=='S')
    		{
    			$('#cacheReImportId').val(data.cacheReImportId);
    		}
    		alert(data.message);
    		layer.closeAll();
    	}catch(e)
    	{
    		alert("导入错误")
    	}
    }
    //显示上传口岸打印出库面单模板对话框
    function  showUploadSeaportPrintDialog(){
    	layer.open({
            title :'上传口岸打印出库面单模板',
            type: 1,
            shadeClose: true,
            shade: 0.8,
            area: ['350px', '200px'],
            content:$('#uploadSeaportPrintDialog'),
            end:function(){
            }
        });
    }
    function importOnloadSeaportPrint(dom)
    {
    	var text = $("#uploadSeaportPrintIFrame").contents().find("body").text();
    	if(text == ""){return;}
    	try
    	{
    		data = JSON.parse(text);
    		console.log(data)
    		if(data.flag=='S')
    		{
    			$('#template_path').val(data.template_path);
    		}
    		alert(data.message);
    		layer.closeAll();
    	}catch(e)
    	{
    		alert("上传错误")
    	}
    }
    
    function  reviewDemoDialog(){
    	var template_path=$('#template_path').val();
    	if(template_path==null||template_path=='')
    	{
    		alert("请先上传模板！");
    		return;
    	}
    	layer.open({
            title :'预览打印出库面单模板',
	        type: 2,
	        shadeClose: true,
	        shade: 0.8,
	        offset: ['10px', '300px'],
	        area: ['590px', '650px'],
	        content:'${backServer}/seaport/reviewOutputTemplateDemo?template_path='+template_path
        });
    }
    </script>
</body>
</html>