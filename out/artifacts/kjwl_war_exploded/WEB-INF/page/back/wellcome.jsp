<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include  file="header.jsp"%>
 
 <style>
 	.section_main_info_ul li{ list-style: none;line-height: 32px;}
 
 </style>
<body>
<div class="admin">
	<div class="line-big">
    	<div class="xm3">
        	<div class="panel border-back">
            	<div class="panel-body text-center">
                	<img src="${backImgFile}/face.jpg" width="120" class="radius-circle" /><br />
                    admin
                </div>
                <div class="panel-foot bg-back border-back">您好，admin，欢迎您登录跨境物流系统。</div>
            </div>
            <br />
<!--         	<div class="panel">
            	<div class="panel-head"><strong>统计</strong></div>
                <ul class="list-group">
                	<li><span class="float-right badge bg-red">88</span><span class="icon-user"></span> 用户统计</li>
                    <li><span class="float-right badge bg-main">828</span><span class="icon-hdd-o"></span> 包裹统计</li>
                    <li><span class="float-right badge bg-main">828</span><span class="icon-shopping-cart"></span> 晒单统计</li>
                </ul>
            </div> -->
            <br />
        </div>
		<div class="xm8"> 
		 	<div class="panel">
		 		<div class="panel-head"><strong>登录信息</strong></div>
		  		<div class="section_main">
       	 			<ul class="section_main_info_ul">
			            <li>管理员：admin</li>
			            <li>当前登录时间：2015-06-02 09:29</li>
			            <li>当前登录IP：14.153.125.148</li>
			            <li>上次登录时间：2015-06-02 09:29</li>
			            <li>上次登录IP：14.153.125.148</li>
			            <li>登录次数：5</li>
      	 	 		</ul>
   			   </div>
			 </div>
		</div>        
        
        <div class="xm11">
            <div class="panel">
            	<div class="panel-head"><strong>待处理</strong></div>
  <!--               <table class="table">
                	<tr><th width="210" >工作名称</th><th>描述信息</th><th width="110" >时间</th><th>操作</th></tr>
                    <tr><td >增值服务</td><td>包裹增值服务</td><td>2015-5-15</td><td><a>处理</a></td></tr>
                    <tr><td>增值服务</td><td>包裹增值服务</td><td>2015-5-15</td><td><a>处理</a></td></tr>
                    <tr><td>增值服务</td><td>包裹增值服务</td><td>2015-5-15</td><td><a>处理</a></td></tr>
                    <tr><td >增值服务</td><td>包裹增值服务</td><td>2015-5-15</td><td><a>处理</a></td></tr>
                  </table> -->
            </div>
        </div>
</div>
</div>
</body>
</html>