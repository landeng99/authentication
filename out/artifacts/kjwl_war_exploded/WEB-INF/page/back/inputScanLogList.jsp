<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
	.field-item{
		float:left;
		margin-right: 20px;
	}
	.field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 80px;
	}
	
	.field-item .interval_label{
		width: 10px;
		float:left;
		margin: auto 10px;
	}
	
	.field-item .field{
		float:left;
		width: 200px;
	}
    .div-title{
        font-weight: bold;
        float:left;
        width:90px;
        margin-top:5px;
        margin-left:10px;
    }
        .div-input{
        float:left;
        width:220px;
    }
    
    .input-width{
        width:200px;
    }
    
    .even {
	background-color: #C1CDCD;
	}
.td1{border:solid #c00; border-width:0px 1px 1px 0px; padding-left:10px;}
.table1{border:solid #c00; border-width:1px 0px 0px 1px;}
}

</style>
<div class="admin">
    <div class="panel admin-panel">
        <div class="panel-head"><strong>入库扫描日志记录</strong></div>
        </br>
          <form action="${backServer}/inputScanLog/search" method="post" id="myform" name="myform">

             <div style="height: 40px;">
                  <div class="div-title">时间:</div>
                  <div class="div-input">
					<input type="text" class="input" id="scan_date" name="scan_date" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd'})"
                            value="${params.scan_date}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
                  <div class="field-item">
                	 <div class="label"><label for="status">单号:</label></div>
                    <div class="field">
						<input type="text" class="input" id="pkg_no" name="pkg_no" style="width:200px" onKeyUp="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>               
             </div>
          </form>
             <div class="padding border-bottom">
                  <input type="button" class="button bg-main" onclick="search()" value="查询" />
                  <input type="button" class="button bg-main" onclick="batchExport()" value="导出" />
                  <input type="button" class="button bg-main" onclick="returnPage()" value="返回" />
             </div>
             <table class="table">
                    <tr>
                    <th class="td" width="20%">扫描时间</th>
                    <th class="td" width="20%">单号</th>
                    <th class="td" width="30%">包裹状态</th>
                    <th class="td" width="15%">扫描人员</th>
                    <th class="td" width="15%">扫描次数</th>
                  </tr>
               <c:forEach var="inputScan"  items="${inputScanLogList}" varStatus="status">
                   <tr class="${status.count % 2 == 0 ? 'odd' : 'even'}"> 
                     
                      <td class="td" width="20%"><fmt:formatDate value="${inputScan.scan_date}" pattern="yyyy-MM-dd HH:mm"/></td>
                      <td class="td" width="20%">${inputScan.pkg_no}</td>
                      <td class="td" width="30%">${inputScan.pkg_status}</td>
                      <td class="td" width="15%">${inputScan.scan_user_name}</td>
                       <td class="td" width="15%">${inputScan.scan_count}</td>
                  </tr>
               </c:forEach>
             </table>
                  <div class="panel-foot text-center">
                   <jsp:include page="webfenye.jsp"></jsp:include>
                 </div>
             </div>
</div>
</body>
<script type="text/javascript">
function initSearchField(){
	$('#pkg_no').val('${params.pkg_no}');
}
initSearchField(); 

function search(){

    document.getElementById('myform').submit();
}

	//回车事件
	$(function() {
		document.onkeydown = function(e) {
			var ev = document.all ? window.event : e;
			if (ev.keyCode == 13) {

				search();
			}
		}
	});
	
	function returnPage(){
		history.go(-1);
	}
	
function batchExport() {
		var scan_date = $("#scan_date").val();
		var pkg_no = $("#pkg_no").val();
		window.location.href = "${backServer}/inputScanLog/exportInputScanLog?scan_date="
				+ scan_date
				+ "&pkg_no="
				+ pkg_no
				;
	}
</script>

</html>