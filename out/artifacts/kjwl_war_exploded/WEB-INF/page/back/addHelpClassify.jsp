<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
</style>
 
<body>
	 <div class="admin">
	<form action="${backServer}/articleClassify/addArticleClassify" method="post" onsubmit="return check()">
	  <div class="field-group">
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="category_name">文章类名</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="hidden" 
		 				name="id" value="${pojo.id}"/>
		 			<input  class="input" type="text"  style="width:200px"  
		 				name="name" id="artclass" value="${pojo.name}"/>
		 		</div>
		 	</div>

		 	  <div class="field-item">
		 		<div class="label">
		 			<label for="parent_id">父分类</label>
		 		</div>
		 		<div class="field">
		 			<select  class="input" style="width:200px" 
		 				name="parent_id">
		 				<option value="0">--选择父分类--</option>
		 				<c:forEach var="cate" items="${list}">
					 			<option value="${cate.id}" 
					 				<c:if test="${cate.id==pojo.parent_id}">
					 				 selected="selected" </c:if>
					 			>${cate.name}</option>
		 				</c:forEach>
		 			</select>
		 		</div>
		 	</div>

	 	</div>
	 		<div style="clear: both"></div>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<input type="submit" class="button bg-main" value="保存">
	  	   		<a class="button bg-main" href="javascript:history.go(-1)">取消</a>
	  	   </div>
	  	   </form>
	</div>
</body>
<script type="text/javascript">
function check(){
	   var name = document.getElementById("artclass").value;
	   if(name ==  null || name == ''){
	        alert("类名不能为空");
	        return false;
	   }
	   return true;
}
</script>
</html>