<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style type="text/css">
.input-help {
  float: left;
  width: 70%;
  height: 30px;
  line-height: 32px;
  margin-left: 25px;
}
</style>



  
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">公司运单号信息</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active form-x" id="tab-set">
        		<div class="form-group">
                    <div class="label" style="width:9%"><label for="readme1">公司运单号</label></div>
                    <div class="field" style="width:57%">
                    	<input type="text" style="width: 22%;float:left;" value="${identifier_Last_num}" class="input" id="last_num" name="last_num" size="50" placeholder="公司运单号" data-validate="required:请填写你的公司运单号" />
                    </div>
                </div>  
                
<input type="hidden" value="${identifier_Last_num}" id="last_num_fu"/>
                <div class="form-button" style="margin-left: 2%">
                	<a href="javascript:void(0);" class="button bg-main" onclick="save()">保存</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	
				</div>
        </div>
	</div>
</div>

<script type="text/javascript">
  
function save(){
	var vartset = /^\d{13}$/;
    var last_num=$("#last_num").val();
    if(!vartset.test(last_num)){
    	alert("新单号只能是13个数字组成");
    	return;
    }
    var last_num_fu=$("#last_num_fu").val();
    if(last_num < last_num_fu){
    	alert("修改的新单号不能比旧单号小");
    	return;
    }
    if(last_num == last_num_fu){
    	alert("单号在系统中已经存在");
    	return;
    }
    alert("修改成功");
    var url = "${backServer}/identifier/updateIdentifier?last_num="+last_num;
    window.location.href = url;
  }
</script>
</body>