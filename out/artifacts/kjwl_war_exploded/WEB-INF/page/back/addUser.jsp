<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">添加用户</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="myform" name="myform">
				<input type="hidden" id="adduser_check" value="0">
                <div class="form-group">
                    <div class="label"><label for="username">用户名</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="username" name="username" style="width:200px" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" onblur="checkUser()" />
                   		<span id="usernameNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="password">密码</label></div>
                    <div class="field">
                    	<input type="password" class="input" id="password" name="password" style="width:200px" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="surepassword">确认密码</label></div>
                    <div class="field">
                    	<input type="password" class="input" id="surepassword" name="surepassword" style="width:200px" />
                    </div>
                </div>
               	 请选择角色：
               	 <div id="choic" class="roleChoic">
               	 	<ul>
                	<c:forEach var="role"  items="${roleLists}" varStatus="status">
                		<li > 
                		<input type="checkbox" value="${role.id }">&nbsp;&nbsp;${role.rolename }&nbsp;&nbsp;
                		</li><c:if test="${status.count%3==0}"><br/></c:if>
                	</c:forEach>
                	</ul>
              	 </div>
                <!-- <input type="submit" value="提交"> -->
                <div class="form-button" style="margin-left: 10px;margin-top: 150px;"><a href="javascript:void(0);" class="button bg-main" onclick="save()">提交</a></div>
             </form>
        </div>
        </div>
      </div>
	</div>
</div>
</body>
<script type="text/javascript">


	function save(){
		
		var roleId = $("#role_id").val();
		var username = $("#username").val();
		var password = $("#password").val();
		var surepassword = $("#surepassword").val();
		
		var usercheck = $('#adduser_check').val();
		
		var inputs=$('#choic input:checked');
		var checkedArr=[];
		
		$.each(inputs,function(i,input){
			checkedArr.push($(input).val());		
		});
		
		var roleStr=checkedArr.join(',');
		
		if(username==""){
			alert("用户名不能为空");
			document.myform.username.focus();
			return false;
		}
		if(password==""){
			alert("密码不能为空");
			document.myform.password.focus();
			return false;
		}
		if(surepassword==""){
			alert("确认密码不能为空");
			document.myform.surepassword.focus();
			return false;
		}
		if(password!=surepassword)
		{
			alert("输入的两次密码不相同");
			surepassword.value = "";
			password.value = "";
			return;
		}
		
		if(checkedArr.length==0){
			alert("请选择要勾选的角色！");
			return false;
		}
		
		/*
			1.前台主要做密码是否一致
			2.ajax请求后台，传递用户名校验用户名是否存在，如果存在则提示用户名已经被注册，不存在则提示用户名可用
			3.如果用户名未被使用、密码正确、权限已经勾选，单击提交
			4.数据保存。
		*/
				
		if(usercheck==1){
			var url = "${backServer}/user/addUser";
			$.ajax({
				url:url,
				type:"POST",
				data:{"roleStr":roleStr,"username":username,"password":password,"surePassword":surepassword},
				success:function(data){
					if(data){
			            alert("添加成功");
			            window.location.href = "${backServer}/user/queryAll";
					}
				}
			});
		}else{
			alert("用户名已重复");
		}
		
	}
	
	//用户名文本框鼠标失去焦点，校验用户名是否已经存在
	function checkUser(){
		var username = $("#username").val();
		if(username==""){
			$('#usernameNote').text('用户名不能为空!');
			$('#usernameNote').css('color','red');
			
		}if(username!=""){
		$.ajax({
			url:'${backServer}/user/checkUser',
			type:'post',
			data:{username:username},
			success:function(data){
				if(data['result']==true){
					$('#adduser_check').val("1");
					$('#usernameNote').text('该用户名可以使用!');
					$('#usernameNote').css('color','green');
				}else{
					$('#adduser_check').val("0");
					$('#usernameNote').text('该用户名已经存在!');
					$('#usernameNote').css('color','red');
				}
			}
			
		});
		}
	}
</script>
</html>