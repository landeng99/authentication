<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp"%> 
<body>
<div class="lefter">
    <div class="logo"><a href="#" target="_blank"><img src="${backImgFile}/logo.png" alt="后台管理系统" style="height:48px" /></a></div>
</div>
<div class="righter nav-navicon" id="admin-nav">
    <div class="mainer">
        <div class="admin-navbar">
            <span class="float-right">
               	<a class="button button-little bg-main" href="${mainServer}" target="_blank">前台首页</a>
                <a class="button button-little bg-yellow" href="${backServer}/logout">注销登录</a>
            </span>
            <ul class="nav nav-inline admin-nav">

                <c:forEach var="pmenu" items="${listParent}" varStatus="pmVs">
                <c:if test="${pmVs.count == 1}">
                <li class="root active">
                </c:if><c:if test="${pmVs.count != 1}">
                <li class="root">
                </c:if>
              		<a href="javascript:;" class="${pmenu.link}">${pmenu.name}</a>
                	<ul>
                	<c:forEach var="menu" items="${pmenu.menus}">
          			<%--  <shiro:hasPermission name="index:edit"> --%>
                		<li> <a href="javascript:;" url="${backServer}${menu.link}" class="active">
						${menu.name}</a></li>
				<%-- 	</shiro:hasPermission> --%>
                	</c:forEach> 
                	</ul>
                </li> 
              </c:forEach>
            </ul>
        </div>
        <div class="admin-bread">
            <span>您好，${back_user.username }，欢迎您的光临。</span>
            <ul class="bread">
                <li><a href="${backServer}/manager/index" class="icon-home"> 开始</a></li>
                <li>后台首页</li>
            </ul>
        </div>
    </div>
</div>
 
 <iframe frameborder="0"  height="87%" src="${backServer}/manager/wellcome" id="iframe" ></iframe>
<!--  <div class="footer" style="clear: both;">
 @kingleadsw.com....
 
 </div> -->
 
<script type="text/javascript">
$(".admin-nav .root").click(function(){
 
	$(".admin-nav .root").removeClass('active');
	$(this).addClass('active');
});

$(".admin-nav .root").find('li').click(function(){
	var url = $(this).children('a').attr('url');
	
	if (url) {
		$(".admin-nav .root").find('li').removeClass('active');
		$(this).addClass('active');
		$("#iframe").attr("src", url);
	}
});
</script>
</body>
</html>