<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<body>
<div class="admin">

    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">用户管理</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="userform">
				<div class="form-group">
                    <div class="label"><label for="username">用户名</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="username" name="username" style="width:200px" value="${back_user.username }" readonly="true" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="old_password">原密码</label></div>
                    <div class="field">
                    	<input type="password" class="input" id="old_password" name="old_password" style="width:200px" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" onblur="checkPwd()" >
                    	<span id="pwdNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="new_password">新密码</label></div>
                    <div class="field">
                    	<input type="password" class="input" id="new_password" name="new_password" style="width:200px" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="sure_password">确认密码</label></div>
                    <div class="field">
                    	<input type="password" class="input" id="sure_password" name="sure_password" style="width:200px" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                </div>
             </form>
             </div>
             </div>
        <div class="form-button"><a href="javascript:void(0);" class="button bg-main" onclick="update_user()" >保存</a>
          <a href="javascript:history.go(-1)" class="button bg-main">取消</a> 
        </div>
</body>
<script type="text/javascript">
	function update_user(){
		var url = "${backServer}/user/modifyPwd";
		var old_password=$("#old_password").val();
		var sure_password=document.getElementById("sure_password").value;
		var new_password=document.getElementById("new_password").value;
		if(old_password == ""){
			alert("原密码不能为空");
			return false;
		}
		if(new_password == ""){
			alert("新密码不能为空");
			return false;
		}
		if(sure_password == ""){
			alert("确认密码不能为空");
			return false;
		}
		if(new_password!=sure_password)
		{
			alert("输入的两次密码不相同");
			sure_password.value = "";
			new_password.value = "";
			return;
		}
			
		$.ajax({
			url:url,
			type:"POST",
			data:{"password":sure_password,"oldPassword":old_password},
			success:function(data){
				if(data){
		            alert("密码修改成功！请重新登录");
		            window.parent.location.href ="${backServer}/logout"; 
		           	//window.parent.location.href ="${backServer}/back/login.jsp"; 
		           //	document.execCommand('Refresh'); 
				}
			}
		});
	}
	//文本框失去焦点事件，校验原密码是否输入正确
	function checkPwd(){
		var password = $("#old_password").val();
		if(password==""){
			$('#pwdNote').text('原密码不能为空!');
			$('#pwdNote').css('color','red');
			
		}if(password!=""){
		$.ajax({
			url:'${backServer}/user/checkPwd',
			type:'post',
			data:{password:password},
			success:function(data){
				if(data['result']==true){
					$('#pwdNote').text('密码输入正确!');
					$('#pwdNote').css('color','green');
				}else{
					$('#pwdNote').text('原密码输入错误!');
					$('#pwdNote').css('color','red');
				}
			}
			
		});
		}
	}
	
	
</script>
</html>