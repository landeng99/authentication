<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>
<%
String path=request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
    <script src="${backJsFile}/LodopFuncs.js"></script>
    <style type="text/css">
    .field-group .field-item{
        float:left;
        margin-right: 20px;
        width: 1000px;
    }
    .field-group .field-item .label{
        float:left;
        line-height: 33px;
        margin-right: 10px;
    }
    .field-group .field-item .field{
        float:left;
    }
    
    .pallet-list{
        margin-top: 20px;
    }
</style>
<body>
  <div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">合箱服务详情</a>
          </li>
        </ul>
      </div>
      <div class="tab-body">
        <div class="admin_context">
          <div class="field-group">
            <input type="hidden" id="package_id" value="${pkg.package_id}" />
            <input type="hidden" id="abandon_packages" value="${abandon_packages}" />
            <div class="field-item">
              <div style="float: left; width: 150px;"><span><strong>合箱后新包裹运单号:</strong></span></div>
              <div style="float: left; width: 200px; margin-left: 10px;"><span>${pkg.logistics_code}</span></div>
              <div style="float: left; width: 200px; margin-left: 10px;">包裹状态：${pkg.statusDesc}</div>
            </div>
                  <div class="field-item">
                  	<div style="margin-top:20px;margin-bottom: 10px">
                  		<a class="button border-blue button-little" href="javascript:void(0);" onclick="ShowCataDialog('${package_id}')">预览打印面单</a>
	                            	   	<a class="button border-blue button-little" href="javascript:void(0);" onclick="ShowCataDialog_noreview('${package_id}')">打印面单</a>
                                  <div class="attachServiceStatus">
		                          <c:if test="${pkgAttachService.status==2}">
				                   	 <input type="radio" name="${pkg.package_id}" checked="checked" value="2">已完成  &nbsp &nbsp
				                     <input type="radio" name="${pkg.package_id}" value=1>未完成</td>
		                          </c:if>
		                           <c:if test="${pkgAttachService.status==1}">
		                          		  <input type="radio" name="${pkg.package_id}"  value=2>已完成  &nbsp &nbsp
		                          		   <input type="radio" name="${pkg.package_id}"  checked="checked"  value=1>未完成</td>
		                           </c:if>
	                         </div>  
                           </div>
                    <table border="1" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="300" align="center">商品名称</td>
                           <td width="100" align="center">品牌</td>
                          <td width="100" align="center">商品申报类别</td>
                          <td width="100" align="center">价格(元)</td>
                          <td width="100" align="center">数量</td>
                          <td width="100" align="center">关税费用(元)</td>
                        </tr>
                        <c:forEach var="packageGoods" items="${pkg.goodsList}">
                          <tr>
                            <td width="300" align="center">${packageGoods.goods_name}</td>
                            <td width="300" align="center">${packageGoods.brand}</td>
                            <td width="100" align="center">${packageGoods.name_specs}</td>
                            <td width="100" align="center">${packageGoods.price}</td>
                            <td width="100" align="center">${packageGoods.quantity}</td>
                            <td width="100" align="center"><fmt:formatNumber value="${packageGoods.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                          </tr>
                        </c:forEach>
                      </table>
             </div> 
                     
            <div class="field-item">
               <div style="float: left; width: 1000px;margin-top: 10px;"></div>
            </div>
              <div style="clear: both"></div>
              <div class="panel admin-panel" id="pkgList">
	         	     <div class="panel-head"><strong>待合箱包裹</strong></div>
              <c:forEach var="packageGoods" items="${packageGoodsList}">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                  <tr>
                    <td colspan="2">&nbsp</td>
                  </tr>
                  <tr>
                     <td width=300>待合箱包裹运单号：${packageGoods.pkg.logistics_code}</td>
                     <td width=300>物流状态：${packageGoods.pkg.statusDesc}</td>
                     <td >关联单号：${packageGoods.pkg.original_num}</td>
                  </tr>
                   <tr>
                     <td>是否到库：<c:if test="${packageGoods.pkg.arrive_status==0}">未到库</c:if>
                                <c:if test="${packageGoods.pkg.arrive_status==1}">已到库</c:if>
                                <input type="hidden" value="${packageGoods.pkg.arrive_status}" name="arrive_status">
                     </td>
                     <td >到库时间：<fmt:formatDate value="${packageGoods.pkg.arrive_time}" pattern="yyyy/MM/dd ，E ，HH:mm"/></td>
                     <td ><a class="button border-blue button-little" href="javascript:void(0);"
                           onclick="ShowCataDialog('${packageGoods.pkg.package_id}')">预览打印面单</a>
							<a class="button border-blue button-little" href="javascript:void(0);"
                           onclick="ShowCataDialog_noreview('${packageGoods.pkg.package_id}')">打印面单</a>                           
                     </td>
                  </tr>
                  <tr>
                    <td colspan="4">
                      <table border="1" cellpadding="0" cellspacing="0" >
                        <tr>
                          <td width="300" align="center">商品名称</td>
                          <td width="300" align="center">品牌</td>
                          <td width="100" align="center">商品申报类别</td>
                          <td width="100" align="center">价格(元)</td>
                          <td width="100" align="center">数量</td>
                          <td width="100" align="center">关税费用(元)</td>
                        </tr>
                        <c:forEach var="good" items="${packageGoods.goodList}">
                          <tr>
                            <td width="300" align="center">${good.goods_name}</td>
                            <td width="300" align="center">${good.brand}</td>
                            <td width="100" align="center">${good.name_specs}</td>
                            <td width="100" align="center">${good.price}</td>
                            <td width="100" align="center">${good.quantity}</td>
                            <td width="100" align="center"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                          </tr>
                        </c:forEach>
                      </table></td>
                  </tr>
                </table>
              </c:forEach>
            </div>
          </div>
    
          <div class="buttonList" style="clear: both; padding-top: 20px;">
            <input type="button" class="button bg-main" onclick="finish()"value="提交" />
            <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function ShowCataDialog(package_id) {
      layer.open({
        title : '打印面单',
        type : 2,
        shadeClose : true,
        shade : 0.8,
        offset: ['10px', '300px'],
        area: ['590px', '650px'],
        content : '${backServer}/pkg/print?package_id=' + package_id
      });
    }
	function ShowCataDialog_noreview(package_id) {
		LODOP=getLodop();  
		LODOP.PRINT_INIT("打印面单");
		LODOP.SET_PRINT_PAGESIZE(1,1016,1524," ");
		LODOP.SET_PRINT_MODE("PRINT_PAGE_PERCENT","73%");
		LODOP.ADD_PRINT_URL(0,0,'100%','100%','<%=basePath%>backprint?package_id='+package_id);
		LODOP.PRINT();//直接打印
		//LODOP.PRINTA();//选择打印机打印
		//LODOP.PREVIEW();//打印预览
	}
    function finish(package_id) {
    	var inputs=$('#pkgList').find("input[name='arrive_status']");
    	var serviceable=true;
    	$.each(inputs,function(i,input){
    		var arrive_status=$(input).val();
    		if(arrive_status==0){
    			serviceable=false;
    			return;
    		}
    	});
    	
    	if(serviceable==false){
    		layer.alert("有包裹未完全到库，不能操作该服务");
    		return;
    	}
    	
      var url = "${backServer}/pkg/merge";
      var package_id = $('#package_id').val();
      var input=$('.attachServiceStatus input:checked');
      var data={};
      data[package_id]=$(input).val();
     
      
      //原包裹废弃
      var abandon_packages = $('#abandon_packages').val();
      $.ajax({
        url : url,
        data : {
          "package_id" : JSON.stringify(data),
          "abandon_packages" : abandon_packages
        },
        type : 'post',
        dataType : 'text',
        async : false,
        success : function(data) {
          layer.alert("操作成功",function(){
	          window.location.href = "${backServer}/breadcrumb/back";
          });
        }
      });
    }
  </script>
</body>
</html>