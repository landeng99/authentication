<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>  
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@	taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>翔锐物流-后台管理</title>
    <link rel="stylesheet" href="${backCssFile}/admin.css">
    <link rel="stylesheet" href="${backJsFile}/layer/skin/layer.css">
    
    <link rel="stylesheet" href="${resource_path}/zTree_v3/css/zTreeStyle/zTreeStyle.css">
    <link rel="stylesheet" href="${resource_path}/zTree_v3/css/zTreePage.css">
     <script src="${backJsFile}/jquery.js"></script>
	<script src="${resource_path}/zTree_v3/js/jquery.ztree.core-3.5.js"></script>
	<script src="${resource_path}/zTree_v3/js/jquery.ztree.excheck-3.5.js"></script>
 	<script src="${backJsFile}/layer/layer.js"></script>
</head>
<body>
<style type="text/css">
.warehouse{width: 800px;}
.warehouse li{
	float: left;
	margin-right: 10px;
}
</style>
<div class="admin">

    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">编辑角色</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="roleform">
                <div class="form-group">
                    <div class="form-group">
                	<input type="hidden" id="addrole_check" value="0">
                	<input type="hidden" id="id" value="${role.id }">
                    <div class="label" style="float: left;"><label for="role_name">角色名称</label></div>
                    <div class="field" style="float: left;">
                    	<input type="text" class="input" id="rolename" name="rolename" value="${rolename }" style="width:200px" readonly="true" >
                   		<span id="rolenameNote"></span>
                    </div>
                
                    <div class="label" style="float: left; margin-left: 100px;"><label for="desc">角色描述</label></div>
                    <div class="field" style="float: left;">
                    	<input type="text" class="input" id="desc" name="desc" value="${desc }" style="width:300px" readonly="true" >
                    </div>
                </div>
				<div style="clear: both;" >
					<ul id="treeDemo" class="ztree">
				</div>
				<div style="clear: both;" ></div>
				<div class="warehouse"><span>仓库权限：</span><ul>
					<c:forEach var="roleOverseasAddress" items="${roleOverseasAddressList}">
						<li><input type="checkbox" id="${roleOverseasAddress.id}" <c:if test="${roleOverseasAddress.role_id!=0}">checked="checked"</c:if>/>${roleOverseasAddress.warehouse}</li>
					</c:forEach>
				</ul></div>
				
				<div style="clear: both;">
					<ul id="roleBusinessStr" class="ztree">
				</div>				
          		<div style="clear: both;" ></div>
				<div class="roleBusinessStr"><span>业务权限：</span><ul>
					<c:forEach var="businessName" items="${businessNameList}">
						<li><input type="checkbox" id="${businessName.business_name}" <c:if test="${businessName.role_id!=0}">checked="checked"</c:if>/>${businessName.business_name}</li>
					</c:forEach>
				</ul></div>
				
				<div class="form-button" style="margin-left: 10px;margin-top: 80px; clear: both; margin-bottom: 50px;">
					<a href="javascript:void(0);" class="button bg-main transitionCss" onclick="save()" >保存</a>
        		    <a href="javascript:history.go(-1)" class="button bg-main transitionCss">取消</a> 
        		</div>
             </form>
             </div>
             </div><br>
</body>
<script type="text/javascript">

	var setting = {
        view: {
            addHoverDom: false,
            removeHoverDom: false,
            selectedMulti: false
        },
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        edit: {
            enable: false
        }
    };
	
	$(document).ready(function(){
		$.fn.zTree.init($("#treeDemo"), setting, <%=request.getAttribute("zNodess")%>);
    });
	
	function save(){
		var id=$("#id").val();
		var rolename=$("#rolename").val();
		var desc=$("#desc").val(); 
		var treeObj=$.fn.zTree.getZTreeObj("treeDemo");
		var nodes=treeObj.getCheckedNodes(true);	      
		var menus = [];
        for(var i=0;i<nodes.length;i++){
        	menus.push(nodes[i].id);
        }
        
        if(menus.length==0){
        	layer.alert("请选择菜单权限");
        	return ;
        }
        
        var inputs= $('.warehouse').find('input');
        var roleWarehouse=[];
        $.each(inputs,function(i,input){
        	if(this.checked){
        		roleWarehouse.push($(this).attr('id'));
        	}
        });
        
        var roleBusinessinputs= $('.roleBusinessStr').find('input');
        var roleBusiness=[];
        $.each(roleBusinessinputs,function(i,input){
        	if(this.checked){
        		roleBusiness.push($(this).attr('id'));
        	}
        });
        
		var url = "${backServer}/role/update";
		if(confirm("确认选择吗？")){
			$.ajax({
				url:url,
				type:"POST",
				data:{"id":id,"rolename":rolename,"desc":desc,'menusStr':menus.join(','),'roleWarehouseStr':roleWarehouse.join(','),'roleBusinessStr':roleBusiness.join(',')},
				success:function(data){
					if(data){
						layer.alert("编辑成功",function(){
							window.location.href = "${backServer}/role/queryAll";
						});
					}
				}
			});
		}else{
			return false;
		}
		
	}
	
	
</script>
</html>