<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">用户编辑</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<input type="hidden" id="id" value="${user_id }">
			<input type="hidden" id="adduser_check" value="0">
                <div class="form-group">
                    <div class="label"><label for="username">用户名</label></div>
                    <div class="field">
                    	<input type="text" class="input" 
                    	id="username" name="username" 
                    	style="width:200px" value="${username }" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" onblur="checkUser()" />
                    	<span id="usernameNote"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="password">密码</label></div>
                    <div class="field">
                    	<input type="password" class="input" 
                    	id="password" name="password" 
                    	style="width:200px" value="${password }" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="surepassword">确认密码</label></div>
                    <div class="field">
                    	<input type="password" class="input" 
                    	id="surepassword" name="surepassword" 
                    	style="width:200px" value="${password }" onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                    </div>
                </div>
               	 用户详情：
               	 <div id="choic">
               	 	<ul>
                	<c:forEach var="role"  items="${roleLists}">
                		<li style="list-style: outside none none;float: left;width:300px;" > 
                		<input type="checkbox" class="role_${role.id }" value="${role.id }">&nbsp;&nbsp;${role.rolename }&nbsp;&nbsp;
                		</li>
                	</c:forEach>
                	</ul>
              	 </div>
              <div class="form-button" style="margin-left: 10px;margin-top: 150px;">
              		<a href="javascript:void(0);" class="button bg-main" onclick="save()">保存</a>
              		<a href="javascript:history.go(-1)" class="button bg-main">返回</a> 
              </div>
        </div>
        </div>
      </div>
	</div>
</div>
</body>
<script type="text/javascript">
$(function(){
	var id = $("#id").val();
	$.ajax({
		url:"${backServer}/user/queryUserRole",
		type:"POST",
		data:{"user_id":id},
		success:function(data){
			if(data){
				$.each(data,function(i,ele){
					$(".role_"+ele.role_id).attr('checked', 'checked');
				});
			}
		}
	});
});
//用户名文本框鼠标失去焦点，校验用户名是否已经存在
function checkUser(){
	var username = $("#username").val();
	var user_id =  $("#id").val();
	if(username==""){
		$('#usernameNote').text('用户名不能为空!');
		$('#usernameNote').css('color','red');
		
	}if(username!=""){
	$.ajax({
		url:'${backServer}/user/checkUpdateUser',
		type:'post',
		data:{username:username,user_id:user_id},
		success:function(data){
			if(data['result']==true){
				$('#adduser_check').val("1");
				$('#usernameNote').text('该用户名可以使用!');
				$('#usernameNote').css('color','green');
			}else if(data['result']==false){
				$('#adduser_check').val("0");
				$('#usernameNote').text('该用户名已经存在!');
				$('#usernameNote').css('color','red');
			}
		}
		
	});
	}
}

function save(){
	var username = $("#username").val();
	var password = $("#password").val();
	var surepassword = $("#surepassword").val();
	var id = $("#id").val();
	var usercheck = $('#adduser_check').val();

	var inputs=$('#choic input:checked');
	var checkedArr=[];
	
	$.each(inputs,function(i,input){
		checkedArr.push($(input).val());		
	});
	
	var roleStr=checkedArr.join(',');
	
	if(username==""){
		alert("用户名不能为空");
		document.myform.username.focus();
		return false;
	}
	if(password==""){
		alert("密码不能为空");
		document.myform.password.focus();
		return false;
	}
	if(surepassword==""){
		alert("确认密码不能为空");
		document.myform.surepassword.focus();
		return false;
	}
	if(password!=surepassword)
	{
		alert("输入的两次密码不相同");
		surepassword.value = "";
		password.value = "";
		return;
	}

	if(checkedArr.length==0){
		alert("请选择要勾选的角色！");
		return false;
	}else{
		usercheck = 1;
	}
	if(usercheck==1){
		$.ajax({
			url:"${backServer}/user/updateUserInfo",
			type:"post",
			data:{"user_id":id,
				"roleStr":roleStr,
				"username":username,
				"password":password,
				"surePassword":surepassword},
			success:function(data){
				//if(data){
		            alert("编辑成功");
		            window.location.href = "${backServer}/user/queryAll";
				//}
			}
		});
		
	}
	
}
</script>
</html>