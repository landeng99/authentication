<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">

    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">账号信息</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="userform">
                <div class="form-group">
                    <div class="label"><label for="username">用户名</label></div>
                    <div class="field">
                    	<input type="text" class="input" id="username" name="username" style="width:200px" value="${back_user.username }" readonly="true" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="label"><label for="password">密码</label></div>
                    <div class="field">
                    	<input type="password" class="input" id="password" name="password" style="width:200px" value="${back_user.password }" readonly="true" />
                    </div>
                </div>
             </form>
             <br/>
        <div class="form-button"><a href="javascript:void(0);" class="button bg-main" onclick="update()" >修改密码</a>
                 
		
</body>
<script type="text/javascript">
	function update(){
		var url = "${backServer}/user/updatePwd";
		$.ajax({
			url:url,
			type:"POST",
			success:function(msg){
				if(msg){
		            window.location.href = url;
				}
			}
		});
		
	}
</script>

</html>