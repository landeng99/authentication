<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
 <link rel="stylesheet" href="${backCssFile}/jquery.treetable.css">
 <link rel="stylesheet" href="${backCssFile}/jquery.treetable.theme.modified.css">
<script type="text/javascript" src="${backJsFile}/jquery.treetable.js"></script>
 
	
<body>
	<div class="admin">
	<div class="admin_context">
	    <div class="panel admin-panel">
		  <div class="padding border-bottom">
		  <shiro:hasPermission name="pkgcategory:add">
	      		<a href="${backServer}/pkgcategory/initadd" class="button button-small border-green" />添加分类</a>
	      </shiro:hasPermission>
	      </div>
		<table id="pkg_category" class="table table-hover">
		 <thead>
			<tr>
				<th>分类名称</th><th>操作</th>
			</tr>
		 </thead>
		 
		 	<c:forEach var="pkgCategory" items="${pkgCategoryList}">
			<tr data-tt-id="${pkgCategory.id}" data-tt-parent-id="${pkgCategory.parent_id}">
	 			<td>${pkgCategory.category_name}</td>
	 			<td>
	 			<shiro:hasPermission name="pkgcategory:edit">
	 				<a class="button border-blue button-little" href="${backServer}/pkgcategory/initedit?id=${pkgCategory.id}">编辑</a>
	 			</shiro:hasPermission>
	 				</td>
		   </tr>
		   </c:forEach>
		 </table>	
		</div>
	</div>
	</div>
<script type="text/javascript">
	$("#pkg_category").treetable({expandable: true,initialState:"expanded" });
</script>
</body>
</html>