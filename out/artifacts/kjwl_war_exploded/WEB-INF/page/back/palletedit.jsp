<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
</style>
<body>
<div class="admin">
<div class="admin_context">
		<form id="palletFom" action="${backServer}/pallet/edit" method="post">
	    <div class="field-group">
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="pick_code">托盘号</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="text"  style="width:200px"  name="pallet_code" id="pallet_code" value="${pallet.pallet_code}" readonly="readonly"/>
		 		</div>
		 	</div>
	 		<div class="field-item">
		 		<div class="label">
		 			<label for="pick_code">提单号</label>
		 		</div>
		 		<div class="field">
		 			<input type="hidden" value="${pallet.pallet_id}" name="pallet_id" id="pallet_id"/>
		 			<input type="hidden" value="${pallet.pick_id}" name="pick_id" id="pick_id"/>
		 			<input type="hidden" value="${pallet.seaport_id}" name="seaport_id" id="seaport_id"/>
		 		    <input type="hidden" value="${pallet.overseas_address_id}" name="overseas_address_id" id="overseas_address_id"/>
		 			<input  class="input" type="text"  style="width:200px"  name="pick_code" id="pick_code" value="${pallet.pick_code}" readonly="readonly"/>
		 		</div>
		 	</div>
		   <div class="field-item">
		 		<div class="label">
		 			<label >描述信息</label>
		 		</div>
		 		<div class="text-field">
		 			 <textarea rows="3" cols="50" name="description">${pallet.description}</textarea>
		 		</div>
		 	</div>
	  		</div>
	  	 <div style="clear: both"></div>
	  	<div class="panel admin-panel pickorder-list">
	  	      	  	 <div class="panel-head"><strong>包裹列表</strong>
	  	      	  	 <input type="button"  onclick="showScanLog(${pallet.pick_id})" value="扫描日志" />
	  	      	  	 <input type="button"  onclick="batchExport(${pallet.pick_id})" value="下载日志" />
	  	      	  	 </div>
	         	     <div class="padding border-bottom">
			            <c:if test="${pallet.status==1}">
				            <input type="button" value="扫描添加"  id="scanAdd" class="button button-small border-green">
				            <input type="button" value="添加包裹"  id="add" class="button button-small border-green">
			            </c:if>
			        </div>
			        <div style="min-height:200px;">
	         	     <table class="table table-hover " id="palletPkgList">
	         	     <thead>
	         	     	<tr>
	         	     		<th  width="10%">公司运单号</th>
	         	     		<th  width="10%">关联单号</th>
	         	     		<th  width="10%">用户账号</th>
	         	     		<th  width="15%">收件人</th>
	         	     		<th  width="15%">身份证号</th>
	         	     		<th  width="10%">联系方式</th>
	         	     		<th  width="20%">收件地址</th>
	         	     		<th width="10%">操作</th>
	         	     	</tr>
	         	      </thead>
	         	      <tbody>
	         	      <c:forEach var="pkg" items="${pallet.pkgs}">
	         	      <tr>
	         	      	<td>${pkg.logistics_code}</td>
	         	      	<td>${pkg.original_num}</td>
	         	      	<td>${pkg.email}</td>
	         	      	<td>${pkg.receiver}</td>
	         	      	<td>${pkg.idcard}</td>
	         	      	<td>${pkg.mobile}</td>
	         	      	<td>${pkg.address}</td>
	         	      	<td>
	         	      	 <c:if test="${pallet.status==1}">
	         	      	 	<a class="button  border-yellow"  href="javascript:;" onclick="del(${pkg.package_id})">删除</a>
	         	      	 </c:if>
	         	      	<a class='button  border-green' href="${backServer}/pkg/detail?package_id=${pkg.package_id}">详情</a>
	         	      	</td>
	         	      </tr>
	         	      </c:forEach>
	         	      </tbody>
	         	     </table>
	         	      <div class="panel-foot text-center">
	      				<jsp:include page="webfenye.jsp"></jsp:include>
	      			</div>	
	         	     </div>
	  	   </div>
	  	   <input type="hidden" id="packages_str" name="packages_str"/>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<input type="button" class="button bg-main" id="save" value="保存">
	  	   		<a class="button bg-main" href="javascript:;" id="goback" >返回</a>
	  	   </div>
	  	  </form>
	 </div>
	</div> 
	 <div id="pkgs" style="display:none;position: relative;"></div>
	 <script type="text/javascript">
	 	//window.location.reload();
		//手动加入托盘
		$('#add').click(function(){
		  	var seaport_id=$('#seaport_id').val();
		  	var pick_id=$('#pick_id').val();
		  	var pallet_id=$('#pallet_id').val();
		  	var overseas_address_id=$('#overseas_address_id').val();
		 	window.location.href='${backServer}/pallet/queryforaddpallet?seaport_id='+seaport_id+"&pick_id="+pick_id+"&pallet_id="+pallet_id+"&overseas_address_id="+overseas_address_id;
		}); 
		
		//扫描包裹加入托盘
		$('#scanAdd').click(function(){
		  	var seaport_id=$('#seaport_id').val();
		  	var pick_id=$('#pick_id').val();
		  	var pallet_id=$('#pallet_id').val();
		  	window.location.href='${backServer}/pallet/initscanforaddpallet?seaport_id='+seaport_id+"&pick_id="+pick_id+"&pallet_id="+pallet_id;
		});
		
		$('#save').click(function(){
		  	$('#palletFom').submit();
		});
		
		$('#goback').click(function(){
		//	var pick_id=$('#pick_id').val();
		//	var url='${backServer}/pickorder/initedit?pick_id='+pick_id;
			//window.location.href=url;
			 window.location.href="${backServer}/breadcrumb/back";
		})
		
		function del(package_id){
			
			var url="${backServer}/pallet/delPalletPkg?package_id="+package_id+"&pallet_id=${pallet.pallet_id}&pick_id=${pallet.pick_id}";
		
			if(window.confirm("确定删除")){
				window.location.href=url;
			}
		}
		function showScanLog(pick_id)
		{
		    layer.open({
		        title :'扫描日志',
		        type:2,
		        shadeClose: true,
		        shade: 0.8,
		        offset: ['10px', '100px'],
		        area: ['900px', '800px'],
		        content:'${backServer}/outputScanLog/showAllScanLog?pick_id='+pick_id
		    }); 
		}
		function batchExport(pick_id) {
			window.location.href = "${backServer}/outputScanLog/exportOutputScanLog?pick_id="
					+ pick_id
					;
		}
	 </script>
</body>
</html>