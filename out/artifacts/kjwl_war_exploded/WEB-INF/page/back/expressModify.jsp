<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>
<body>
    <div class="admin">
        <div class="tab-body">
            <input type="hidden" id="logistics_code" name="logistics_code" value="${expressInfo.logistics_code}">
            <input type="hidden" id="express_info_id" name="express_info_id" value="${expressInfo.express_info_id}">
            <input type="hidden" id="checkEmsCode" name="checkEmsCode" value=1>
            <div style="height: 40px; ">
                <div style="float: left; margin-left: 10px" class="label">
                    <span><strong>旧快递单号:</strong> </span>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <span>${expressInfo.nu}</span>
                </div>
            </div>
            <div style="height: 50px;">
                <div style="float: left; margin-top: 10px; margin-left: 10px"
                    class="label">
                    <span><strong>新快递单号:</strong> </span>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <input type="text" class="input" id="newEmsCode" name="newEmsCode"
                        style="width: 200px" onblur="check()"
                        onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                </div>
                <div style="float: left; margin-left: 94px;">
                    <span id="newEmsCodeTextArea"></span>
                </div>
            </div>
               <!---------------------------------------------------------------->
               <div style="height: 50px;">
                <div style="float: left; margin-top: 10px; margin-left: 25px" class="label">
                    <span><strong>当前快递:</strong></span>
                    
                </div>
                <div style="float: left; margin-top: 10px; margin-left: 10px;">
                    <span>
                   		<c:if test="${expressInfo.com == 'ems'}">EMS</c:if>
                   		<c:if test="${expressInfo.com == 'shunfeng'}">顺丰速运</c:if>
                   		<c:if test="${expressInfo.com == 'yuantong'}">圆通速递</c:if>
                   		<c:if test="${expressInfo.com == 'shentong'}">申通快递</c:if>
                   		<c:if test="${expressInfo.com == 'zhongtong'}">中通速递</c:if>
                   		<c:if test="${expressInfo.com == 'yunda'}">韵达快运</c:if>
                   		<c:if test="${expressInfo.com == 'tiantian'}">天天快递</c:if>
                   		<c:if test="${expressInfo.com == 'Zhaijisong'}">宅急送</c:if>
                   		<c:if test="${expressInfo.com == 'debangwuliu'}">德邦物流</c:if>
                   		<c:if test="${expressInfo.com == 'huitongkuaidi'}">百世汇通</c:if>
                    </span>
                </div>
            </div>
            <div style="height: 40px;">
                <div style="float: left; margin-top: 10px; margin-left: 25px" class="label">
                    <!--  <span><strong>快递公司:</strong></span>-->
                    <span class="span-title"><strong>修改快递:</strong></span>
                          
                </div>
                <div style="float: left; margin-left: 10px;width:10px">
                	<select id="com" name="com" class="input" style="width: 200px">
                		<option value="">--请选择--</option>
                		<option value="ems">EMS</option>
                		<option value="shunfeng">顺丰速运</option>
                		<option value="yuantong">圆通速递</option>
                		<option value="shentong">申通快递</option>
                		<option value="zhongtong">中通速递</option>
                		<option value="yunda">韵达快运</option>
                		<option value="Zhaijisong">宅急送</option>
                		<option value="tiantian">天天快递</option>
                		<option value="debangwuliu">德邦物流</option>
                		<option value="huitongkuaidi">百世汇通</option>
                	</select>       
                </div>
            </div>
               
               <!---------------------------------------------------------------->
            <div style="float: left; margin-top: 15px;margin-left: 80px;">
                <input type="button" class="button bg-main" onclick="save()" value="保存" /> 
                <input type="button" class="button bg-main" onclick="cancel()" value="取消" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        //初始化焦点
        $(document).ready(function() {
            $('#newEmsCode').focus();
        });

        //新快递单号文本框鼠标失去焦点，校验新快递单号是否已经存在
        function check() {
            $('#newEmsCodeTextArea').empty();
            var url = "${backServer}/express/checkEmsCode";
            var newEmsCode = $('#newEmsCode').val();

            if (newEmsCode == null || newEmsCode.trim() == "") {
                return;
            }
            $.ajax({
                url : url,
                type : 'post',
                data : {
                    "newEmsCode" : newEmsCode
                },
                success : function(data) {
                    $('#checkEmsCode').val(data);
                    if (data != "1") {
                        $('#newEmsCodeTextArea').text('快递单号已经存在!');
                        $('#newEmsCodeTextArea').css('color', 'red');
                    }
                }
            });
        }

        //获取窗口索引
        var index = parent.layer.getFrameIndex(window.name);

        function save() {

            $('#newEmsCodeTextArea').empty();

            var url = "${backServer}/express/updateEmsCode";

            var logistics_code = $('#logistics_code').val();
            var newEmsCode = $('#newEmsCode').val();
            var express_info_id = $('#express_info_id').val();
            var checkEmsCode = $('#checkEmsCode').val();
            var com = $('#com').val();
			
            if (newEmsCode == null || newEmsCode.trim() == "") {
                $('#newEmsCodeTextArea').text('新快递单号不能为空!');
                $('#newEmsCodeTextArea').css('color', 'red');
                $('#newEmsCode').focus();
                return;
            }
            if (checkEmsCode != "1") {
                alert("新快递单号已经存在!");
                $('#newEmsCode').focus();
                return;
            }

            $.ajax({
                url : url,
                data : {
                    "logistics_code" : logistics_code,
                    "newEmsCode" : newEmsCode,
                    "express_info_id":express_info_id,
                    "com":com
                },
                type : 'post',
                dataType : 'text',
                success : function(data) {
                    if(data ==1){
                        alert("新快递单号已经存在!");
                        $('#newEmsCode').focus();
                    }else{
                        
                        // 标识点击了保存 给父窗口传值
                        parent.$('#returnCode').val("1");
                        // 关闭窗口
                        parent.layer.close(index);
                    }

                }
            });
        }

        // 关闭窗口
        function cancel() {
            parent.layer.close(index);
        }
    </script>
</body>
</html>