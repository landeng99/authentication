<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>快递信息列表</strong></div>
      <div class="tab-body">
      <input type="hidden" id="returnCode" />
      <input type="hidden" id="subscribe_status_bk" value="${params.subscribe_status}"/>
      <input type="hidden" id="monitoring_status_bk" value="${params.monitoring_status}"/>
      
        <br/>
           <form action="${backServer}/express/searchExpress" method="post" id="myform" name="myform">
             <div style="height:40px;">
                <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>订阅状态:</strong></span></div>
                <div style="float:left;margin-left:10px;width:160px">
                     <select id="subscribe_status" name="subscribe_status" class="input" style="width:150px">
                       <option value="">请选择</option>
                       <option value="0">未订阅</option>
                       <option value="1">订阅成功</option>
                       <option value="2">订阅失败</option>
                       <option value="3">订阅中</option>
                     </select>
                </div>
                <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>公司运单号:</strong></span></div>
                <div style="float:left;margin-left:10px;width:160px">
                     <input type="text" class="input" id="logistics_code" name="logistics_code" style="width:150px"
                            onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                            value="${params.logistics_code}"/>
                </div>
                <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>派送单号:</strong></span></div>
                <div style="float:left;margin-left:10px;width:160px">
                     <input type="text" class="input" id="nu" name="nu" style="width:150px"
                            onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                            value="${params.nu}"/>
                </div>
                
                <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>监控状态:</strong></span></div>
                <div style="float:left;margin-left:10px;width:160px">
                     <select id="monitoring_status" name="monitoring_status" class="input" style="width:150px">
                       <option value="">请选择</option>
                       <option value="polling">监控中</option>
                       <option value="shutdown">结束</option>
                       <option value="abort">中止</option>
                       <option value="updateall">重新推送</option>
                     </select>
                </div>
             </div>
           </form>
           <div class="padding border-bottom">
                <input type="button" class="button bg-main" onclick="search()" value="查询" />
           </div>
              <table class="table">
                 <tr>
                     <th width="12%">公司运单号</th>
                     <th width="12%">派送单号</th>
                     <th width="10%">订阅状态</th>
                     <th width="10%">监控状态</th>
                     <th width="20%">监控消息</th>
                     <th width="16%">更新时间</th>
                     <th width="20%">操作</th>
                 </tr>    
              </table>
              <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="expressInfo"  items="${expressInfoList}">
                     <tr>
                           <td width="12%">${expressInfo.logistics_code}</td>
                            <td width="12%">${expressInfo.nu}</td>
                         <td width="10%">
                            <c:if test="${expressInfo.subscribe_status==0}">未订阅</c:if>
                            <c:if test="${expressInfo.subscribe_status==1}">订阅成功</c:if>
                            <c:if test="${expressInfo.subscribe_status==2}">订阅失败</c:if>
                            <c:if test="${expressInfo.subscribe_status==3}">订阅中...</c:if>
                         </td>
                         <td width="10%">
                            <c:if test="${expressInfo.monitoring_status=='polling'}">监控中</c:if>
                            <c:if test="${expressInfo.monitoring_status=='shutdown'}">结束</c:if>
                            <c:if test="${expressInfo.monitoring_status=='abort'}">中止</c:if>
                            <c:if test="${expressInfo.monitoring_status=='updateall'}">重新推送</c:if>
                         </td>
                         <td width="20%">${expressInfo.monitoring_message}</td>
                         <td width="16%"><fmt:formatDate value="${expressInfo.receive_time}" type="both"/></td>
                         <td width="20%">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="detail('${expressInfo.express_info_id}')">详情</a>
                            <c:if test="${expressInfo.monitoring_status!='shutdown'}">
                               <a class="button border-blue button-little" href="javascript:void(0);"onclick="modify('${expressInfo.express_info_id}')">修改单号</a>
                               <a class="button border-blue button-little" href="javascript:void(0);"onclick="operate('${expressInfo.express_info_id}',1)">重新订阅</a>
                            </c:if>
                         </td>
                     </tr>
                  </c:forEach>
              </table>
              <div class="panel-foot text-center">
                 <jsp:include page="webfenye.jsp"></jsp:include>
              </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function() {

            //选择框选中
            $('#subscribe_status').val($('#subscribe_status_bk').val());
            $('#monitoring_status').val($('#monitoring_status_bk').val());
            $('#subscribe_status').focus();
        });

        function detail(express_info_id) {
            var url = "${backServer}/express/expressDetail?express_info_id="
                    + express_info_id;
            window.location.href = url;
        }

        function operate(express_info_id) {
  
            if (confirm("要重新订阅吗？")) {
                var url = "${backServer}/express/restart";
                $.ajax({
                        url : url,
                        data : {"express_info_id" : express_info_id},
                        type : 'post',
                        dataType : 'json',
                        success : function(data) {
                            alert(data.message);
                            if(data.result){
                                window.location.href = "${backServer}/express/expressInit";
                            }
                        }
                    });
            }
        }

        function search() {
            document.getElementById('myform').submit();
        }

        function refresh() {
            window.location.href = "${backServer}/express/updateEmsCode";
        }

        function modify(express_info_id) {
            // 弹出修改窗口
            layer.open({
                        title : '修改快递单号',
                        type : 2,
                        shadeClose : true,
                        shade : 0.8,
                        area : [ '400px', '300px' ],
                        content : '${backServer}/express/modify?express_info_id=' + express_info_id,
                        end : function(index) {
                            // 点击保存的时候 刷新页面
                            if ($('#returnCode').val() == 1) {
                                alert("修改成功！");
                                window.location.href = "${backServer}/express/expressInit";
                            }
                        }
                    });
        }
        
      //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search();
               }
          }
        });
    </script>
</body>
</html>