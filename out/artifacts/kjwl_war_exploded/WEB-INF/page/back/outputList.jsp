<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>出库管理</strong></div>
      <div class="tab-body">
        <br />
             <div class="padding border-bottom">
                    <input type="button" class="button button-small border-green" onclick="add()" value="新增出库"/>
               </div>
                 <table class="table">
                    <tr>
                        <th width="20%">出库编号</th>
                        <th width="20%">创建日期</th>
                        <th width="20%">仓库</th>
                        <th width="20%">口岸</th>
                        <th width="20%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="output"  items="${outputLists}">
                     <tr>
                         <td width="20%">${output.output_sn}</td>
                         <td width="20%"><fmt:formatDate value="${output.create_time}" type="both"/></td>
                         <td width="20%">${output.warehouse}</td>
                         <td width="20%">${output.seaportDisplay}</td>
 
                         <td width="20%">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="scanOutput('${output.seq_id}')">扫描出库</a>
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="detail('${output.output_sn}')">编辑</a>
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="del('${output.seq_id}')">删除</a>
                         </td>
                         </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
            $('#stype').focus();
        });
        
        function detail(output_sn){
            var url = "${backServer}/output/outputDetail?output_sn="+output_sn;
            window.location.href = url;
        }
        
        function add(){
            var url = "${backServer}/output/outputAdd";
            window.location.href = url;
        }
        
        function del(sid){
            
            if(confirm("是否要删除该条记录？")){
                 var url = "${backServer}/output/deleteOutput"
                 $.ajax({
                  url:url,
                  data:{
                      "id":sid},
                  type:'post',
                  dataType:'json',
                  success:function(data){
                     alert(data.message);
                     window.location.href = "${backServer}/output/queryAll";
                  }
                 });
            }
          }
		function scanOutput(sid)
		{
			window.location.href = "${backServer}/pallet/initscanforaddpallet_new?output_id="+sid;
		}
    </script>
</body>
</html>