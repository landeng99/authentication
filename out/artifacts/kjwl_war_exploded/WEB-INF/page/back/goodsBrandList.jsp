<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
    <div class="panel admin-panel">
    	<div class="panel-head"><strong>商品类别</strong></div>
      <div class="padding border-bottom">
      <shiro:hasPermission name="goodsBrand:add">
      		<input type="button" class="button button-small border-green" onclick="add()" value="添加" />
      </shiro:hasPermission>
      </div>
      <table class="table table-hover">
      		<tr><th>品牌编号</th>
      		<th>品牌名称</th>
      		<th>操作</th></tr>
      			
      		<c:forEach var="list"  items="${goodsBrandLists}">
      		<tr>
      		<td>${list.goodsTypeId }</td>
      		<td>${list.brandName }</td>
      		<td>
      		<shiro:hasPermission name="goodsBrand:select">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateGoodsBrand('${list.goodsTypeId }')">查看详情</a>&nbsp;&nbsp;
      		</shiro:hasPermission><shiro:hasPermission name="goodsBrand:edit">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updGoodsBrand('${list.goodsTypeId }')">编辑</a>&nbsp;&nbsp;
      		</shiro:hasPermission><shiro:hasPermission name="goodsBrand:delete">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="del('${list.goodsTypeId }')">删除</a>
			</shiro:hasPermission>
						</td></tr>	
      		</c:forEach>
      </table>
   		<div class="panel-foot text-center">
      		<jsp:include page="webfenye.jsp"></jsp:include>
      	</div>
      </div>
</div>
</body>

<script type="text/javascript">
	function updateGoodsBrand(goodsTypeId){
		var url = "${backServer}/goodsBrand/toUpdateGoodsBrand?goodsTypeId="+goodsTypeId;
		window.location.href = url;
	}
	function updGoodsBrand(goodsTypeId){
		var url = "${backServer}/goodsBrand/updateGoodsBrand?goodsTypeId="+goodsTypeId;
		window.location.href = url;
	}
	function add(){
		var url = "${backServer}/goodsBrand/addGoodsBrand";
		window.location.href = url;
	}
	
	function del(goodsTypeId){
		if(confirm("确认删除吗[建议查看本类关联的物品后在选择删除]？")){
		var url = "${backServer}/goodsBrand/deleteGoodsBrand?goodsTypeId="+goodsTypeId;
		$.ajax({
			url:url,
			type:'GET',
			success:function(data){
				 alert("删除成功！");
				 window.location.href = "${backServer}/goodsBrand/queryAll";
			}
		});
		}
	}	
</script>
</html>