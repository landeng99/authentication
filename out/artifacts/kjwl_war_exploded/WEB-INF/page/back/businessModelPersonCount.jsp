<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="${resource_path}/js/Chart.js"></script>
<body>

	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>客户基本信息</strong>
			</div>
			<div class="tab-body">
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>用户姓名:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
							${frontUser.user_name}
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>用户类型:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
						<c:choose>
							<c:when test="${frontUser.user_type==1}">普通用户</c:when>
							<c:when test="${frontUser.user_type==2}">同行用户</c:when>
						</c:choose>
					</div>

					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>业务员:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
						${frontUser.business_name}
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>用户账号:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
							${frontUser.account}
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>First_name:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
						${frontUser.first_name}
					</div>

					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>用户积分:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
						${frontUser.integral}
					</div>
				</div>
				
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>用户邮箱:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
							${frontUser.email}
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>Last_name:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
						${frontUser.last_name}
					</div>

					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>用户等级:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
						${frontUser.member_rate}
					</div>
				</div>
				
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>手机号码:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
							${frontUser.mobile}
					</div>

					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>注册时间:</strong></span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 160px">
						<fmt:formatDate value="${frontUser.register_time}" pattern="yyyy-MM-dd hh:mm"/>
					</div>
				</div>								
			</div>
		</div>
		
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>客户包裹统计</strong>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>总包裹数:</span>
							${businessModelTotalInfo.total_package_count}
						&nbsp;&nbsp;&nbsp;&nbsp;<span>总重量:</span>
						${businessModelTotalInfo.total_actual_weight_count_str}
			</div>
			<div class="tab-body">
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>近一周入库包裹数:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
							${businessModel7TotalInfo.total_package_count}
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>总重量:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
						${businessModel7TotalInfo.total_actual_weight_count_str}
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left;  margin-left: 10px"
						class="label">
						<span>近半月入库包裹数:</span>
					</div>
					<div style="float: left; margin-left: 10px; width: 70px">
							${businessModel15TotalInfo.total_package_count}
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>总重量:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
						${businessModel15TotalInfo.total_actual_weight_count_str}
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>近一月入库包裹数:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
							${businessModel30TotalInfo.total_package_count}
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>总重量:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
						${businessModel30TotalInfo.total_actual_weight_count_str}
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>近三月入库包裹数:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
							${businessModel90TotalInfo.total_package_count}
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>总重量:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
						${businessModel90TotalInfo.total_actual_weight_count_str}
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>近半年入库包裹数:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
							${businessModel180TotalInfo.total_package_count}
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span>总重量:</span>
					</div>
					<div style="float: left; margin-left: 10px; margin-top: 5px; width: 70px">
						${businessModel180TotalInfo.total_actual_weight_count_str}
					</div>
				</div>																			
			</div>
		</div>
		<div style="width:30%">
			<div>
			<br>
			<div style="float: left; margin-left: 200px; margin-top: 5px;"><strong>${currentYear}年走货情况趋势</strong></div>
			<div>
				<canvas id="canvas" height="300" width="400"></canvas>
			</div>
			</div>
		</div>
        <div class="form-button" style="float:left;margin-left:500px;">
            <a href="javascript:history.go(-1)" class="button bg-main">返回</a> 
        </div>
	</div>
	<%
	 Object currentYearLabel = request.getAttribute("currentYearLabel");
	Object currentYearData = request.getAttribute("currentYearData");
	%>
	<script type="text/javascript">
	var currentYearLabel = <%=currentYearLabel%>;
	var currentYearData = <%=currentYearData%>;
	var lineChartData = {
		labels : currentYearLabel,
		datasets : [
			{
				label: "走货情况趋势",
				fillColor : "rgba(151,187,205,0.2)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(151,187,205,1)",
				data : currentYearData
			}
		]
	}

window.onload = function(){
	var ctx = document.getElementById("canvas").getContext("2d");
	window.myLine = new Chart(ctx).Line(lineChartData, {
		responsive: true
	});
}		
	</script>
</body>
</html>