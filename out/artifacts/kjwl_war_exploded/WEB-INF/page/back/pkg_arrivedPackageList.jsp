<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>

<style type="text/css">
	.field-item{
		float:left;
		margin-right: 20px;
	}
	.field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 80px;
	}
	
	.field-item .interval_label{
		width: 10px;
		float:left;
		margin: auto 10px;
	}
	
	.field-item .field{
		float:left;
		width: 200px;
	}
.div-title {
	font-weight: bold;
	float: left;
	width: 90px;
	margin-top: 5px;
	margin-left: 10px;
}

.div-input {
	float: left;
	width: 220px;
}

.input-width {
	width: 200px;
}

.even {
	background-color: #C1CDCD;
}

.td1 {
	border: solid #0BF187;
	border-width: 0px 1px 1px 0px;
	padding-left: 10px;
}

.table1 {
	border: solid #0BF187;
	border-width: 1px 0px 0px 1px;
}
}
</style>
<div class="admin">
	<div class="panel admin-panel">
		<div class="panel-head">
			<strong>到库包裹查询</strong>
		</div>
		</br>
		<form action="${backServer}/pkg/arrivedPackageSearch"
			method="post" id="myform" name="myform">

			<div style="height: 40px;">
				<div class="div-title">公司运单号:</div>
				<div class="div-input">
					<input type="text" class="input input-width" id="logistics_code"
						name="logistics_code" value="${params.logistics_code}"
						onkeyup="value=$.trim(value)" />
				</div>
				<div class="div-title">关联单号:</div>
				<div class="div-input">
					<input type="text" class="input input-width" id="original_num"
						name="original_num" value="${params.original_num}"
						onkeyup="value=$.trim(value)" />
				</div>
				<div class="div-title">用户名:</div>
				<div class="div-input">
					<input type="text" class="input input-width" id="user_name"
						name="user_name" value="${params.user_name}"
						onkeyup="value=$.trim(value)" />
				</div>
				<div class="div-title">物流单号:</div>
				<div class="div-input">
					<input type="text" class="input input-width" id="express_num"
						name="express_num" value="${params.express_num}"
						onkeyup="value=$.trim(value)" />
				</div>

			</div>

			<div style="height: 40px;">

				<div class="div-title">LASTNAME:</div>
				<div class="div-input">
					<input type="text" class="input input-width" id="last_name"
						name="last_name" value="${params.last_name}"
						onkeyup="value=$.trim(value)" />
				</div>
				<div class="div-title">入库起始时间:</div>
				<div class="div-input">
					<input type="text" class="input" id="fromDate" name="fromDate"
						style="width: 200px"
						onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						value="${params.fromDate}"
						onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
						onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
				</div>
				<div class="div-title">入库终止时间:</div>
				<div class="div-input">
					<input type="text" class="input" id="toDate" name="toDate"
						style="width: 200px"
						onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						value="${params.toDate}"
						onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"
						onkeyup="value=value.replace(/[^\w\.\/]/ig,'')">
				</div>
				<div class="field-item">
                	 <div class="label"><label for="status">仓库:</label></div>
                    <div class="field">
                        	<select  class="input" id="overseasAddress_id" name="overseasAddress_id" style="width:200px" >
                   		  <option value="">---------选择仓库---------</option>
                    		<c:forEach var="userOverseasAddress" items="${overseasAddressList}">
                    			<option value="${userOverseasAddress.id}">${userOverseasAddress.warehouse}</option>
                    		</c:forEach>
                   		</select>
                    </div>
                </div>
                <div class="field-item">
                	 <div class="label"><label for="status">快件包裹:</label></div>
                    <div class="field">
                         <select  class="input" id="express_package" name="express_package" style="width:200px" >
                   		  <option value="-1">---------选择快件包裹---------</option>
						  <option value="1">是</option>
						  <option value="0">否</option>
                   		</select>
                    </div>
                </div>                    
			</div>
		</form>
		<div class="padding border-bottom">
			<input type="button" class="button bg-main" onclick="search()"
				value="查询" /> <input type="button" class="button bg-main"
				onclick="batchExport()" value="批量导出" />
		</div>
		<table class="table1">
			<tr class='odd'>
				<th class="td1" width="8%">公司运单号</th>
				<th class="td1" width="15%">关联单号</th>
				<th class="td1" width="10%">物流单号</th>
				<th class="td1" width="10%">到库日期</th>
				<th class="td1" width="8%">LASTNAME</th>
				<th class="td1" width="16%">内件明细</th>
				<th class="td1" width="5%">实际重量</th>
				<th class="td1" width="10%">增值服务</th>
				<th class="td1" width="8%">包裹状态</th>
				<th class="td1" width="10%">存放位置</th>
			</tr>
			<c:forEach var="pkgOperate" items="${pkgOperateList}"
				varStatus="status">
				<tr class="${pkgOperate.displayColor % 2 == 0 ? 'odd' : 'even'}">
					<c:if test="${pkgOperate.mergeCount!=-1}">
						<td class="td1" width="8%"
							<c:if test="${pkgOperate.mergeCount>0}">rowspan="${pkgOperate.mergeCount}"</c:if>
							style="vertical-align: middle;"><c:choose>
									<c:when test="${pkgOperate.mergeCount==0}">${pkgOperate.logistics_code}</c:when>
									<c:otherwise>${pkgOperate.afterMergeLogistics_code}</c:otherwise>
								</c:choose></td>
					</c:if>
					<c:if test="${pkgOperate.splitCount!=-1}">
						<td class="td1" width="15%"
							<c:if test="${pkgOperate.splitCount>0}">rowspan="${pkgOperate.splitCount}"</c:if>
							style="vertical-align: middle;"><div
								style="width: 180px; word-wrap: break-word;"><a
							href="javascript:void(0);"
							onclick="detail(${pkgOperate.package_id},${pkgOperate.splitPackage_id},${pkgOperate.splitCount})">${pkgOperate.original_num}</a></div></td>
					</c:if>
					<td class="td1" width="10%">${pkgOperate.express_num}</td>
<%-- 					<c:choose>
						<c:when test="${pkgOperate.attach_id=='4'}">
							<c:if test="${pkgOperate.splitCount!=-1}">
								<td class="td1" width="10%"
									<c:if test="${pkgOperate.splitCount>0}">rowspan="${pkgOperate.splitCount}"</c:if>
									style="vertical-align: middle;"><fmt:formatDate
										value="${pkgOperate.arrive_time}" pattern="yyyy-MM-dd" /></td>
							</c:if>
						</c:when>
						<c:when test="${pkgOperate.attach_id=='-100'}">
							<c:if test="${pkgOperate.expressNumCount!=-1}">
								<td class="td1" width="10%"
									<c:if test="${pkgOperate.expressNumCount>0}">rowspan="${pkgOperate.expressNumCount}"</c:if>
									style="vertical-align: middle;"><fmt:formatDate
										value="${pkgOperate.arrive_time}" pattern="yyyy-MM-dd" /></td>
							</c:if>
						</c:when>
						<c:otherwise>
							<td class="td1" width="10%" style="vertical-align: middle;"><fmt:formatDate
									value="${pkgOperate.arrive_time}" pattern="yyyy-MM-dd" /></td>
						</c:otherwise>
					</c:choose> --%>
<%-- 					<td class="td1" width="10%" style="vertical-align: middle;"><fmt:formatDate
									value="${pkgOperate.arrive_time}" pattern="yyyy-MM-dd" /></td> --%>
					<c:if test="${pkgOperate.splitCount!=-1}">
						<td class="td1" width="10%"
							<c:if test="${pkgOperate.splitCount>0}">rowspan="${pkgOperate.splitCount}"</c:if>
							style="vertical-align: middle;"><fmt:formatDate
									value="${pkgOperate.arrive_time}" pattern="yyyy-MM-dd" /></td>
					</c:if>									
					<td class="td1" width="8%">${pkgOperate.last_name}</td>
					<td class="td1" width="16%">${pkgOperate.goods_name}</td>
					<td class="td1" width="5%">${pkgOperate.actual_weight}</td>
					<c:choose>
						<c:when test="${pkgOperate.attach_id=='4'}">
							<c:if test="${pkgOperate.splitCount!=-1}">
								<td class="td1" width="10%"
									<c:if test="${pkgOperate.splitCount>0}">rowspan="${pkgOperate.splitCount}"</c:if>
									style="vertical-align: middle;">${pkgOperate.attach_service}</td>
							</c:if>
						</c:when>
						<c:when test="${pkgOperate.attach_id=='7'}">
							<c:if test="${pkgOperate.mergeCount!=-1}">
								<td class="td1" width="10%"
									<c:if test="${pkgOperate.mergeCount>0}">rowspan="${pkgOperate.mergeCount}"</c:if>
									style="vertical-align: middle;">${pkgOperate.attach_service}</td>
							</c:if>
						</c:when>
						<c:when test="${pkgOperate.attach_id=='-100'}">
							<c:if test="${pkgOperate.expressNumCount!=-1}">
								<td class="td1" width="10%"
									<c:if test="${pkgOperate.expressNumCount>0}">rowspan="${pkgOperate.expressNumCount}"</c:if>
									style="vertical-align: middle;">${pkgOperate.attach_service}</td>
							</c:if>
						</c:when>
						<c:otherwise>
							<td class="td1" width="10%" style="vertical-align: middle;">${pkgOperate.attach_service}</td>
						</c:otherwise>
					</c:choose>
					<td class="td1" width="8%">
						
						<c:choose>
							<c:when test="${pkgOperate.arrive_status==1}">
								<span>已到库</span>
							</c:when>
							<c:otherwise>
								<span style="color: red;">未到库</span>
							</c:otherwise>
						</c:choose>
						
					</td>
					<td class="td1" width="10%">
					<c:if test="${pkgOperate.exception_package_flag!=null}">异常区</c:if>
					</td>

				</tr>
			</c:forEach>
		</table>
		<div class="panel-foot text-center">
			<jsp:include page="webfenye.jsp"></jsp:include>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">
function initSearchField(){
	$('#overseasAddress_id').val('${params.overseasAddress_id}');
}
initSearchField(); 
	//选择框选中
	$("#status").val($("#status_bk").val());
	//选择框选中
	$("#pay_status").val($("#pay_status_bk").val());

	//初始化焦点
	$('#logistics_code').focus();

	function search() {

		document.getElementById('myform').submit();
	}

	function detail(package_id,splitPackage_id,splitCount) {
		var real_package_id=package_id;
		if(splitCount>0)
		{
			real_package_id=splitPackage_id;
		}
		var url = "${backServer}/pkg/detail?package_id=" + real_package_id;
		window.location.href = url;
	}
	function update(package_id) {
		var url = "${backServer}/pkg/update?package_id=" + package_id;
		window.location.href = url;
	}

	function ShowCataDialog(package_id) {
		layer.open({
			title : '打印面单',
			type : 2,
			shadeClose : true,
			shade : 0.8,
			offset : [ '10px', '300px' ],
			area : [ '590px', '650px' ],
			content : '${backServer}/pkg/print?package_id=' + package_id
		});
	}

	function batchExport() {
		var fromDate = $("#fromDate").val();
		var toDate = $("#toDate").val();
		var logistics_code = $("#logistics_code").val();
		var original_num = $("#original_num").val();
		var user_name = $("#user_name").val();
		var last_name = $("#last_name").val();
		var express_num = $("#express_num").val();
	    var overseasAddress_id=($('#overseasAddress_id').find("option:selected"))[0].value;
	    var express_package=($('#express_package').find("option:selected"))[0].value;
		window.location.href = "${backServer}/pkg/exportArrivedPackage?fromDate="
				+ fromDate
				+ "&toDate="
				+ toDate
				+ "&logistics_code="
				+ logistics_code
				+ "&original_num="
				+ original_num
				+ "&user_name="
				+ user_name
				+ "&last_name="
				+ last_name
				+ "&express_num=" + express_num
				+ "&express_package=" + express_package
				+ "&overseasAddress_id=" + overseasAddress_id;
	}
	//回车事件
	$(function() {
		document.onkeydown = function(e) {
			var ev = document.all ? window.event : e;
			if (ev.keyCode == 13) {

				search();
			}
		}
	});
</script>

</html>