<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
  <style type="text/css">
    .field-group .field-item{
        float:left;
        margin-right: 20px;
    }
    .field-group .field-item .label{
        float:left;
        line-height: 33px;
        margin-right: 10px;
        width: 80px;
    }
    
    .field-group .field-item .interval_label{
        float: left;
    }
    .field-group .field-item .field{
        float:left;
        width: 203px;
    }
    
    .bill-list{
        margin-top: 35px;
    }
</style>
<body>
     <div class="admin">
         <div class="admin_context">
           <div class="tab">
              <div class="tab-head">
                <ul class="tab-nav">
                  <li class="active"><a href="#accounting">同行用户账单</a></li>
               </ul>
              </div>
                <div class="tab-body">
                    <div class="tab-panel active" id="accounting">
                      <div class="field-group">
                        <form id="form" action="${backServer}/memberbill/search">
                          <div class="field-item">
                            <div class="label"><label for="pick_code">账号:</label></div>
                            <div class="field">
                                <input type="text" class="input" id="account" name="account" style="width:200px" value="${params.account}" >
                            </div>
                         </div>
                         <div class="field-item">
                           <div class="label"><label for="flight_num">用户名:</label></div>
                           <div class="field">
                               <input type="text" class="input" id="user_name" name="user_name" style="width:200px" value="${params.user_name}">
                           </div>
                         </div>
                         <div class="form-button">
                              <input type="submit" class="button bg-main icon-search" id="search" value="查询"/>
                         </div>
                       </form> 
                     </div>
                    <div style="float:left;width:800px;">
                        <a class="button border-blue button-little" href="javascript:void(0);" id ="selectAll">全选</a>
                        <a class="button border-blue button-little" href="javascript:void(0);" id ="reverse">反选</a>
                      	<shiro:hasPermission name="memberbill:batch_export">
	                        <a class="button border-blue button-little" href="javascript:void(0);" id ="exportList">批量导出</a>
                      	</shiro:hasPermission>
                      	<shiro:hasPermission name="memberbill:import">
	                        <a class="button border-blue button-little" href="javascript:void(0);" id ="importList">导入</a>
                      	</shiro:hasPermission>
                   </div>
                   <div class="panel admin-panel bill-list">

                      <table class="table">
                          <tr>
                              <th width="3%"></th>
                              <th width="20%">会员账号</th>
                              <th width="20%">会员名称</th>
                              <th width="20%">待合计包裹数</th>
                              <th width="20%">操作</th>
                          </tr>
                      </table>
                      <table class="table table-hover">
                        <c:forEach var="user"  items="${billList}">
                           <tr> 
                             <td width="3%">
                                <input type="checkbox" class="checkbox" name="select" id="${user.user_id}" value="${user.user_id}">
                             </td>
                             <td width="20%">${user.account}</td>
                             <td width="20%">${user.user_name}</td>
                             <td width="20%">${user.package_count}</td>
                             <td width="20%">
                             <shiro:hasPermission name="memberbill:export">
	                               <a class="button border-blue button-little" href="javascript:void(0);" onclick="exportList('${user.user_id}')">导出</a>
                             </shiro:hasPermission>
                                    <a class="button border-blue button-little" href="javascript:void(0);" onclick="detail('${user.user_id}')">详情</a>                 
                             </td>
                          </tr>
                        </c:forEach>
                       </table>
                       <div class="panel-foot text-center">
                          <jsp:include page="webfenye.jsp"></jsp:include>
                       </div>    
                   </div>
               </div>
             </div>
           </div>
     </div>
     <script type="text/javascript">
         function batchlist(){
             window.location.href="${backServer}/memberbill/batchlist";
         }
         
         function exportList(user_id){
             window.location.href="${backServer}/memberbill/exportList?userIds="+user_id;
         }
         
         function excelChange() {
             $('#download').empty();
         }
         
         // 全选
         $("#selectAll").click(function() {
             $("input[name='select']").prop("checked",true);
             });
         // 反选
         $("#reverse").click(function() {
             $("input[name='select']").each(function(){
                 $(this).prop("checked",!this.checked);
                  });
             });

         // 结算关税
         $("#exportList").click(function() {  
             var count=$("input[name='select']:checked").length;
             if(count==0){
                  alert("会员未选择！");
                  return;
             }
             
             var array=new Array(); 
             $("input[name='select']:checked").each(function () {
                 array.push($(this).val());
             });
             
             window.location.href="${backServer}/memberbill/exportList?userIds="+array.join(',');
             });
         
         $('#importList').click(function() {
             window.location.href="${backServer}/memberbill/importInit";
         });

         function detail(user_id){
        	    var url = "${backServer}/memberbill/showDetail?user_id="+user_id;
        	    window.location.href = url;
        	}         
     </script>
</body>
</html>