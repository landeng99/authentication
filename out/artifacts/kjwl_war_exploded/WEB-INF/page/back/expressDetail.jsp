<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:80px;
    }
    .div-front{
        float:left;
        margin-left:10px;
        width:300px;
    }
    
    .div-front-List{
        float:left;
        margin-left:10px;
    }
</style>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>快递信息</strong></div>
      <div class="tab-body">
        <br />
                <div style="height: 40px;">
                     <div class ="div-front">
                           <span class="span-title">订阅状态:</span>
                           <span>
                             <c:if test="${expressInfo.subscribe_status==1}">订阅成功</c:if>
                             <c:if test="${expressInfo.subscribe_status==2}">订阅失败</c:if>
                           </span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">公司运单号:</span>
                           <span>${expressInfo.logistics_code}</span>
                      </div>
                </div>
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">快递公司:</span>
                           <!-- -------------------------------------------------------- -->
                           <span>
                           		<c:if test="${expressInfo.com == 'ems'}">EMS</c:if>
                           		<c:if test="${expressInfo.com == 'shunfeng'}">顺丰速运</c:if>
                           		<c:if test="${expressInfo.com == 'yuantong'}">圆通速递</c:if>
                           		<c:if test="${expressInfo.com == 'shentong'}">申通快递</c:if>
                           		<c:if test="${expressInfo.com == 'zhongtong'}">中通速递</c:if>
                           		<c:if test="${expressInfo.com == 'yunda'}">韵达快运</c:if>
                           		<c:if test="${expressInfo.com == 'tiantian'}">天天快递</c:if>
                           		<c:if test="${expressInfo.com == 'zhaijisong'}">宅急送</c:if>
                           		<c:if test="${expressInfo.com == 'debangwuliu'}">德邦物流</c:if>
                           		<c:if test="${expressInfo.com == 'huitongkuaidi'}">百世汇通</c:if>
                           </span>
                           <!-- ----------------------------------------------------------- -->
                      </div>
                      <div style="float:left;">
                           <span class="span-title">派送单号:</span>
                           <span>${expressInfo.nu}</span>
                      </div>
               </div>
               <div style="height: 40px;">
                    <div class ="div-front">
                    <span class="span-title">监控状态:</span>
                       <span>
                          <c:if test="${expressInfo.monitoring_status=='polling'}">监控中</c:if>
                          <c:if test="${expressInfo.monitoring_status=='shutdown'}">结束</c:if>
                          <c:if test="${expressInfo.monitoring_status=='abort'}">中止</c:if>
                          <c:if test="${expressInfo.monitoring_status=='updateall'}">重新推送</c:if>
                       </span>
                    </div>
                    <div style="float:left;">
                       <span class="span-title">监控消息:</span>
                       <span >${expressInfo.monitoring_message}</span>
                    </div>
               </div>
               <div style="height: 40px;">
                    <div class ="div-front">
                       <span class="span-title">查询结果:</span>
                       <span>${expressInfo.message}</span>
                    </div>
                    <div style="float:left;">
                       <span class="span-title">签收状态:</span>
                       <span>
                          <c:if test="${expressInfo.state==0}">在途中</c:if>
                          <c:if test="${expressInfo.state==1}">已揽收</c:if>
                          <c:if test="${expressInfo.state==2}">疑难</c:if>
                          <c:if test="${expressInfo.state==3}">已签收</c:if>
                          <c:if test="${expressInfo.state==4}">退签</c:if>
                          <c:if test="${expressInfo.state==5}">同城派送中</c:if>
                          <c:if test="${expressInfo.state==6}">退回送</c:if>
                          <c:if test="${expressInfo.state==7}">转单</c:if>
                       </span>
                    </div>
               </div>
               
               <div style="height:210px;">
                     <div class="div-front-List"><span class="span-title">快递详情:</span></div>
                     <div style="float:left;margin-left:5px;" >
                      <span><c:if test="${exist!=1}">暂无信息</c:if></span>
                      <span>
                      <c:if test="${exist==1}">
                           <table>
                              <tr>
                                 <td>
                                   <table border="1" cellpadding="0" cellspacing="0">
                                     <tr>
                                       <td width ="150">时间</td>
                                       <td width ="550">地点和跟踪进度</td>
                                     </tr>
                                   </table>
                                 </td>
                                 <td width="17">
                                 </td>
                              </tr>
                              <tr>
                                 <td colspan="2">
                                   <div style="height:176px;overflow:auto;" >
                                       <table border="1" cellpadding="0" cellspacing="0">
                                         <c:forEach var="result"  items="${resultList}">
                                          <tr>
                                            <td width ="150">${result.time}</td>
                                            <td width ="550">${result.context}</td>
                                          </tr>
                                         </c:forEach>
                                       </table>
                                   </div>
                                 </td>
                              </tr>
                             </table>
                         </c:if>
                       </span>
                    </div>
                 </div>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="返回" />
                 </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
    </script>
</body>
</html>