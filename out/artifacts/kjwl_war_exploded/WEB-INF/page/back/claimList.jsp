<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
</style>
<body>
	<div class="admin">
		<input type="hidden" id="messageHidden" name="messageHidden" value="${MESSAGE_INFO}"></input>
		<div class="padding border-bottom" id="findDisplayDiv" style="width: 90%; height: 100px;">
			<!-- 提供登录人用户id userId 信息 -->
			<div style="float:left;height: 32px; margin-left: 10px;">
				<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;申请人&nbsp;</label>
					<input type="text" class="input"id="userName" name="userName" style="width:200px;margin-left: 10px;float:left;" 
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
		 	
	 			<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;包裹编号&nbsp;</label>
					<input type="text" class="input"id="logistics_code" name="logistics_code" style="width:200px;margin-left: 10px;float:left;" 
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
			
			
				<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;状态&nbsp;</label>
				<select id="status" name="status" class="input" style="width:80px;float:left;margin-left: 10px;">
					<option value="-1" selected="selected">请选择</option>
					<option value="1">申请</option>
					<option value="2">拒绝</option>
					<option value="3">审批</option>
					<option value="4">退款中</option>
					<option value="5">完成</option>
				</select>
			</div><div style="float:left;height: 32px; margin-left: 10px; margin-top: 10px;">
				<div style="float: left;width: 280px;">
					<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;金额&nbsp;</label>
					<input type="text" class="input" style="width:100px;float:left;margin-left: 10px;"
						id="amountStart" name="amountStart"
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
					<span style="line-height: 30px; float:left; text-align: center; margin-left: 8px;">到</span>
					<input type="text" class="input"style="width:100px;float:left;margin-left: 8px"
						id="amountEnd" name="amountEnd"
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
				</div><div style="float: left;width: 350px;">
					<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;时间&nbsp;</label>
					<input type="text" class="input" style="width:100px;float:left;margin-left: 10px;"
						id="applyTimeStart" name="applyTimeStart" 
						onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'applyTimeEnd\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
					<span style="line-height: 30px; float:left; text-align: center; margin-left: 8px;">到</span>
					<input type="text" class="input"style="width:100px;float:left;margin-left: 8px"
						id="applyTimeEnd" name="applyTimeEnd" 
						onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'applyTimeStart\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
						value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
				</div>
				<div class="form-button" style="float:left;"><a href="javascript:void(0);" class="button bg-main" onclick="queryAll()">查询</a></div>
			</div>     
		</div>
		<div class="panel admin-panel">
			<div class="panel-head" style="height: 50px;">
				<strong style="float:left;text-align: center;margin-left: 10px;">索赔列表</strong>
			</div>
			<table class="table table-hover">
				<tr><th>申请人</th>
				<th>包裹编号</th>
				<th>申请原因</th>
				<th>状态</th>
				<th>金额</th>
				<th>时间</th>
				<th>操作</th></tr>
			<c:forEach var="claim" items="${claimLists}">
				<tr><td>${claim.userName }</td>
				<td>${claim.logistics_code }</td>
				<td>${claim.reason }</td>
				<td><c:if test="${claim.status==1 }">申请</c:if>
					<c:if test="${claim.status==2 }">拒绝</c:if>
					<c:if test="${claim.status==3 }">审批</c:if>
					<c:if test="${claim.status==4 }">退款中</c:if>
					<c:if test="${claim.status==5 }">完成</c:if></td>
				<td><fmt:formatNumber value="${claim.amount }" type="currency" pattern="$#0.00#"/></td>
				<td>${claim.applyTime }</td>
				<td>
				<shiro:hasPermission name="claim:select">
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="finkClaim('${claim.claimId }')">查看详情</a>&nbsp;&nbsp;
				</shiro:hasPermission><shiro:hasPermission name="claim:edit">
				<c:if test="${claim.status==2 || claim.status==5}">
					<a disabled="disabled" class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateClaim('${claim.claimId }','complete')">审核</a>&nbsp;&nbsp;
      			</c:if><c:if test="${claim.status!=2 && claim.status!=5}">
					<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateClaim('${claim.claimId }','complete')">审核</a>&nbsp;&nbsp;
      			</c:if>		
      			</shiro:hasPermission><shiro:hasPermission name="claim:delete">
      				<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="del('${claim.claimId }')">删除</a>&nbsp;&nbsp;
				</shiro:hasPermission>
				</td></tr>	
			</c:forEach>
			</table>
	 		<div class="panel-foot text-center">
				<jsp:include page="webfenye.jsp"></jsp:include>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	function finkClaim(claimId){
		var url = "${backServer}/claim/toFinkClaim?claimId="+claimId;
		window.location.href = url;
	}
	
	function add(userId){
		var url = "${backServer}/claim/toAddClaim?userId="+userId;
		window.location.href = url;
	}

	function queryAll(){
		var userName = $('#userName').val();
		var logistics_code = $('#logistics_code').val();
		var status = $('#status').val();
		//数字为空取 -1
		var amountStart = $('#amountStart').val();
		if(null == amountStart || "" == amountStart){
			amountStart = -1;
		}
		var amountEnd = $('#amountEnd').val();
		if(null == amountEnd || "" == amountEnd){
			amountEnd = -1;
		}
		var applyTimeStart = $('#applyTimeStart').val();
		var applyTimeEnd = $('#applyTimeEnd').val();
		var url = "${backServer}/claim/queryAll?userName="+userName+
				"&logistics_code="+logistics_code+"&status="+status+
				"&amountStart="+amountStart+"&amountEnd="+amountEnd+
				"&applyTimeStart="+applyTimeStart+"&applyTimeEnd="+applyTimeEnd;
		window.location.href = url;
	}
	
	function updateClaim(claimId,typeString){
		var url = "${backServer}/claim/toFinkClaimOne?claimId="+claimId;
		window.location.href = url;
		<%-- var url;
		var message;
		if("through" == typeString){
			if(confirm("确认审核吗(确定包裹退款的各种要求)？")){
				url = "${backServer}/claim/updateClaimThrough?claimId="+claimId;
				message = "审核成功";
			}
		}else if("refusing" == typeString){
			if(confirm("确认拒绝审核吗？")){
				url = "${backServer}/claim/updateClaimApplication?claimId="+claimId;
				message = "拒绝成功";
			}
		}else if("processing" == typeString){
			if(confirm("确定索赔付款吗(进行付款计算/修改)？")){
				url = "${backServer}/claim/updateClaimProcessing?claimId="+claimId;
				message = "财务付款计算";
				window.location.href = url;
			}
			return false;
		}else if("complete" == typeString){
			if(confirm("确定索赔支付吗？")){
				url = "${backServer}/claim/updateClaimComplete?claimId="+claimId;
				message = "财务支付成功";
			}
		}else{
			return false;
		}
		$.ajax({
			url:url,
			type:'GET',
			success:function(data){
				 alert(message);
				 window.location.href = "${backServer}/claim/queryAll";
			}
		}); --%>
	}
	
	function del(claimId){
		if(confirm("确认删除吗？")){
			var url = "${backServer}/claim/deleteClaim?claimId="+claimId;
			$.ajax({
				url:url,
				type:'GET',
				success:function(data){
					 alert("删除成功！");
					 window.location.href = "${backServer}/claim/queryAll";
				}
			});
		}
	}
	
    //回车事件
    $(function(){
      document.onkeydown = function(e){
          var ev = document.all ? window.event : e;
          if(ev.keyCode==13) {

              queryAll();
           }
      }
    });
</script>
</html>