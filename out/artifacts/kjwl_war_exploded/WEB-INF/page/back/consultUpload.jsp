<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
	<title>修改文章</title>
	<link rel="stylesheet" href="<%=basePath %>resource/css/default2.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>resource/css/prettify.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>back/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>back/css/admin.css">
	<script charset="utf-8" src="<%=basePath %>resource/js/kindeditor.js"></script>
	<script charset="utf-8" src="<%=basePath %>resource/js/zh_CN.js"></script>
    <script src="<%=basePath %>back/js/My97DatePicker/WdatePicker.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="context"]', {
				uploadJson : '<%=basePath %>kindeditor/uploadImg',
				fileManagerJson : '<%=basePath %>kindeditor/fileMgr',
				allowFileManager : true
			});
		});
	</script>
</head>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">海淘咨询信息新建</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br /><div class="tab-panel active form-x" id="tab-set">
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="myform" name="myform" action="${backServer}/consult/addConsultInfo" method="post" enctype="multipart/form-data" >
                <div class="form-group" style="clear:left; float:left; width: 80%;">
                    <div class="label" style="float: left;padding-top:8px;"><label for="name">标题说明:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float: left; width: 80%;">
                    	<input type="text" class="input" id="consult_title" name="consult_title"
                    		placeholder="请填写标题说明" data-validate="required:请填写你的标题说明"/>
                    </div>
                  </div>                
                <div class="form-group" style="clear:left; width: 80%; float:left; margin-top: 10px; margin-bottom: 10px;">
                    <div class="label" style="float: left;padding-top:8px;"><label for="name">&nbsp;直达链接&nbsp;&nbsp;:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float: left; width: 80%;">
                    	<input type="text" class="input" id="forward_url" name="forward_url" />
                    </div>
                </div>
                
                <div class="form-group" style="clear:left; float:left; width: 80%;">
                    <div class="label" style="float: left;padding-top:8px; width: 14%;"><label for="name">&nbsp;&nbsp;状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态&nbsp;&nbsp;:&nbsp;</label></div>
                    <div class="field" style="float: left;">
                    	<select id="status" name="status" class="input" style="width:80px;float:left;margin-left: 10px;">
							<option value="1" selected="selected">启用</option>
							<option value="2">禁用</option>
						</select>
                    </div>
                </div>
                
               <!--  <input type="hidden"  id="consult_detail" name="consult_detail" /> -->
              <div class="form-group" style="clear:left; width: 80%; float:left; margin-top: 10px; margin-bottom: 10px;">
                    <div class="label" style="float: left;padding-top:8px;"><label for="name">&nbsp;海淘咨询信息内容:&nbsp;&nbsp;:&nbsp;&nbsp;</label></div>
                    <div class="field" style="float: left; width: 80%;">
                    	<textarea rows="10" cols="80"
				id="consult_detail" name="consult_detail"></textarea>
                    </div>
                </div>   
	                
               <div class="form-group" style="clear: both; float:left; margin-top: 20px; margin-bottom: 20px; width: 80%;">
                     <div class="label" style="float:left;margin-top:5px; width: 14%;""><label for="name">上传封面图片:</label></div>
                     <div style="float:left;margin-top:5px;margin-left:40px;width:100px">
                     <input class=" input-file" type="file"  id="bannerPic" name="bannerPic" />
                     </div>
                </div>
                <div class="form-button" style="clear:left; float:left;">
                	<a href="javascript:void(0);" class="button bg-main" onclick="save()">保存</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
             </form>
        </div>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">
document.getElementById('keyword').focus();

var img_suffix_ary = new Array();
img_suffix_ary[0] = ".bmp";
img_suffix_ary[1] = ".jpg";
img_suffix_ary[2] = ".jpeg";
img_suffix_ary[3] = ".png";
img_suffix_ary[4] = ".gif";
img_suffix_ary[5] = ".pcx";
img_suffix_ary[6] = ".raw";
img_suffix_ary[7] = ".tiff";
img_suffix_ary[8] = ".tga";
img_suffix_ary[9] = ".exif";
img_suffix_ary[10] = ".fpx";
img_suffix_ary[11] = ".svg";
img_suffix_ary[12] = ".psd";
img_suffix_ary[13] = ".cdr";
img_suffix_ary[14] = ".pcd";
img_suffix_ary[15] = ".dxf";
img_suffix_ary[16] = ".ufo";
img_suffix_ary[17] = ".eps";
img_suffix_ary[18] = ".hdri";
img_suffix_ary[19] = ".ai";
    //获取文件后缀
function test(file_name){
	var result =/\.[^\.]+/.exec(file_name);
	return result;
}

var imgurlVal = "";
function bannerPicChange(){
	//文件名
	var file_name = $("#bannerPic").val();
	//后缀
	var result = test(file_name);
	//后缀是否存在于图片格式中
	var isImgSuffix = false;
	
	var x;
	for (x in img_suffix_ary)
	{	
		if(result == img_suffix_ary[x]){
			//存在
			isImgSuffix = true;
			break;
		}
	}
	//
	if(isImgSuffix == false){
		 $("#bannerPicNote").html("文件["+file_name+"]不是图片格式,请选择图片上传");
		 $("#bannerPicNote").show();
		return;
	}else{
		$("#bannerPicNote").hide();
	}
	
    var url = "${backServer}/banner/uploadBanner";
      $.ajaxFileUpload({
          url : url,
          data:{"imgText":imgurlVal},
          type : 'post',
          secureuri : false,
          fileElementId : 'bannerPic',
          dataType : 'text/html',
          success : function(data) {
          	$('#urlArea').attr("src","${resource_path}/"+data);
          	imgurlVal = data;
          },
          error : function(data, status, e) {
              alert("error" + e);
          }
      });
  }
  
function LostFocus_Code(text,ereIdName,idName) {
    var code = $("#"+idName).val();
    if (code == "" || code == "请输入"+text) {
        $("#"+ereIdName).html(text+"不能为空!");
        $("#"+ereIdName).show();
        return false;
    } else {
        $("#"+ereIdName).hide();
        return true;
    }
}

function save(){
	var context = $('#contextin').val();
	//alert(JSON.stringify(context));
	//$("#consult_detail").val(context);
	$("#myform").submit();
  }
  
  
</script>
</body>