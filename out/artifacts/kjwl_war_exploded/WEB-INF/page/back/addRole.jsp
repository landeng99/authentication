<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>  
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@	taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>翔锐物流-后台管理</title>
    <link rel="stylesheet" href="${backCssFile}/admin.css">
    <link rel="stylesheet" href="${backJsFile}/layer/skin/layer.css">
    
    <link rel="stylesheet" href="${resource_path}/zTree_v3/css/zTreeStyle/zTreeStyle.css">
    <link rel="stylesheet" href="${resource_path}/zTree_v3/css/zTreePage.css">
    <script src="${backJsFile}/jquery.js"></script>
	<script src="${resource_path}/zTree_v3/js/jquery.ztree.core-3.5.js"></script>
	<script src="${resource_path}/zTree_v3/js/jquery.ztree.excheck-3.5.js"></script>
    <script src="${backJsFile}/layer/layer.js"></script>
</head> 
<style type="text/css">
.warehouse{width: 800px;}
.warehouse li{
	float: left;
	margin-right: 10px;
}
</style>
<script>

    function isInFrame(){

   	 if( window.top == window.self ){

     }else{
    	 var isUser="${sessionScope.back_user.username}";
    	 if(isUser==''){
    		 
    		 window.parent.location.href="${backServer}/admin/login";
    		 
    	 }
           
    	 //alert('页面是在框架中打开的');
    	 
     }

}
    isInFrame();
</script>
	
<body>
<div class="admin">

    <div class="tab">
      <div class="tab-head">
        <!-- <strong>角色管理</strong> -->
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">添加角色</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="roleform">
			<input type="hidden" id="addrole_check" value="0">
                <div class="form-group">
                    <div class="label" style="float: left;">
                    	<label for="role_name">角色名称</label></div>
                    <div class="field" style="float: left;">
                    	<input type="text" class="input" id="rolename" name="rolename" style="width:200px" onblur="checkRole()" onkeyup="value=value.replace(/[^\w\u4E00-\u9FA5]/g, '')" >
                    	<span id="rolenameNote"></span>
                    </div>
                
                    <div class="label" style="float: left; margin-left: 100px;"><label for="desc">角色描述</label></div>
                    <div class="field" style="float: left;">
                    	<input type="text" class="input" id="desc" name="desc" style="width:300px" >
                    </div>
                </div>

<div style="clear: both;">
	<ul id="treeDemo" class="ztree">
</div>
          		<div style="clear: both;" ></div>
				<div class="warehouse"><span>仓库权限：</span><ul>
					<c:forEach var="overseasAddress" items="${overseasAddressList}">
						<li><input type="checkbox" id="${overseasAddress.id}"/>${overseasAddress.warehouse}</li>
					</c:forEach>
				</ul></div>

<div style="clear: both;">
	<ul id="roleBusinessStr" class="ztree">
</div>				
          		<div style="clear: both;" ></div>
				<div class="roleBusinessStr"><span>业务权限：</span><ul>
					<c:forEach var="businessName" items="${businessNameList}">
						<li><input type="checkbox" id="${businessName.business_name}"/>${businessName.business_name}</li>
					</c:forEach>
				</ul></div>
							
				<div class="form-button" style="margin-left: 10px;margin-top: 80px; clear: both;">
					<a href="javascript:void(0);" class="button bg-main transitionCss" onclick="save()" >保存</a>
          			<a href="javascript:history.go(-1)" class="button bg-main transitionCss" >取消</a> 
      			</div>
      			<div class="form-button" style="height:80px; clear: both;">
      			</div>
             </form>
             </div>
             </div><br>
             
             
             <div>


             </div>
</body>
<script type="text/javascript">
	var setting = {
        view: {
            addHoverDom: false,
            removeHoverDom: false,
            selectedMulti: false
        },
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        edit: {
            enable: false
        }
    };
	
	$(document).ready(function(){
		$.fn.zTree.init($("#treeDemo"), setting, <%=request.getAttribute("zNodess")%>);
    });
 	
	function save(){
		var rolename=$("#rolename").val();
		var desc=$("#desc").val();
		 
		if(rolename==""){
			layer.alert("角色名称不能为空");
			return false;
		}
		
		/* if(checkedArr.length==0){
			alert("请选择要勾选的权限！");
			return false;
		}
		 */
		
		if($('#addrole_check').val()==1){
			
			var treeObj=$.fn.zTree.getZTreeObj("treeDemo");
			var nodes=treeObj.getCheckedNodes(true);	      
			var menus = [];
	        for(var i=0;i<nodes.length;i++){
	        	menus.push(nodes[i].id);
	        }
	        
	        if(menus.length==0){
	        	layer.alert("请选择菜单权限");
	        	return ;
	        }
	        
	        var inputs= $('.warehouse').find('input');
	        var roleWarehouse=[];
	        $.each(inputs,function(i,input){
	        	if(this.checked){
	        		roleWarehouse.push($(this).attr('id'));
	        	}
	        });
	        
	        var roleBusinessinputs= $('.roleBusinessStr').find('input');
	        var roleBusiness=[];
	        $.each(roleBusinessinputs,function(i,input){
	        	if(this.checked){
	        		roleBusiness.push($(this).attr('id'));
	        	}
	        });
	        
			var url = "${backServer}/role/insert";
			if(confirm("确认创建吗？")){
				$.ajax({
					url:url,
					type:"POST",
					data:{"rolename":rolename,"desc":desc,'menusStr':menus.join(','),'roleWarehouseStr':roleWarehouse.join(','),'roleBusinessStr':roleBusiness.join(',')},
					success:function(data){
						if(data){
							layer.alert("添加成功",function(){
								  window.location.href = "${backServer}/role/queryAll";
							});
					      
						}
					}
				});
			}else{
				return false;
			}
		}else{
			layer.alert("角色名重复,不可添加");
		}
		 
	}
	
	
	function checkRole(){
		var rolename = $("#rolename").val();
		if(rolename==""){
			$('#rolenameNote').text('角色名不能为空!');
			$('#rolenameNote').css('color','red');
			
		}if(rolename!=""){
		$.ajax({
			url:'${backServer}/role/checkRole',
			type:'post',
			data:{rolename:rolename},
			success:function(data){
				if(data['result']==true){
					$('#addrole_check').val("1");
					$('#rolenameNote').text('该角色名可以使用!');
					$('#rolenameNote').css('color','green');
				}else{
					$('#addrole_check').val("0");
					$('#rolenameNote').text('该角色名已经存在!');
					$('#rolenameNote').css('color','red');
				}
			}
			
		});
		}
	}
</script>
</html>