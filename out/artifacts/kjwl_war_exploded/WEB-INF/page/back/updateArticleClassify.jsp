<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
	<title>KindEditor JSP</title>
	<link rel="stylesheet" href="<%=basePath %>resource/css/default2.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>resource/css/prettify.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>back/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>back/css/admin.css">
	<script charset="utf-8" src="<%=basePath %>resource/js/kindeditor.js"></script>
	<script charset="utf-8" src="<%=basePath %>resource/js/zh_CN.js"></script>
</head>
<body>
<div class="admin">
	<div class="tab">
	<div class="form-group">
		<form method="post" action="<%=basePath %>articleClassify/updataArticleClassify">
			<input type="hidden" name="id" value="${id}">
			<div style="display: block;">
				<div class="form-group" style="height:40px">
					<div style="float:left;">文章类别:</div>
					<div style="float:left;margin-left:20px;"><input type="text" name="name" value="${name}"/></div>
				</div>
				<div class="form-group" style="height:40px">	
					<div style="float:left;">上一级ID:</div>
					<div style="float:left;margin-left:20px;"><input type="text" name="parent_id" value="${parent_id}"/></div>
				</div>
				<div class="form-group" ><input type="submit" name="button" value="修改" /></div>
			</div>	
		</form>
	</div>
	</div>
</div>
</body>
</html>