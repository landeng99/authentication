<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<body>
<div class="admin">
    <div class="panel admin-panel">
    	<div class="panel-head"><strong>友情链接列表</strong></div>
      <div class="padding border-bottom">
      <shiro:hasPermission name="friendlylink:add">
      		<input type="button" class="button button-small border-green" onclick="add()" value="添加链接" />
      </shiro:hasPermission>
      </div>
      <table class="table table-hover">
      		<tr><th>标题</th>
      			<th>链接</th>
      			<th>打开方式</th>
      			<th>时间</th>
      			<th>状态</th>
      			<th>操作</th></tr>	
      		<c:forEach var="list"  items="${linkLists}">
      		<tr><td>${list.name }</td>
      			<td>${list.link }</td>
      			<td>${list.opentype }</td>
      			<td>${list.createtime }</td>
      			<td>
      				<c:if test="${list.status==2 }">禁用</c:if>
      				<c:if test="${list.status==1 }">启用</c:if></td>
      			<td>
      			<shiro:hasPermission name="friendlylink:update">
      				<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="update('${list.id }')">修改</a>&nbsp;&nbsp;
      				<c:if test="${list.status==1 }">
      				<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateStatus('${list.id }')">禁用</a>&nbsp;&nbsp;
      				</c:if><c:if test="${list.status==2 }">
      				<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="updateStatus('${list.id }')">启用</a>&nbsp;&nbsp;
      				</c:if>
      			</shiro:hasPermission><shiro:hasPermission name="friendlylink:delete">
      				<a class="button border-blue button-little" href="javascript:void(0);" 
      					onclick="del('${list.id }')">删除</a>
      			</shiro:hasPermission></td></tr>	
      		</c:forEach>
      </table>
   		<div class="panel-foot text-center">
      		<jsp:include page="webfenye.jsp"></jsp:include>
      	</div>
      </div>
</div>

</body>
<script type="text/javascript">
	function add(){
		var url = "${backServer}/friendlylink/toAddLink";
		window.location.href = url;
	}
	
	function update(id){
		var url = "${backServer}/friendlylink/toUpdateLink?id="+id;
		window.location.href = url;
	}
	
	function updateStatus(id){
		var url = "${backServer}/friendlylink/udpateStatus?id="+id;
		$.ajax({
			url:url,
			type:'GET',
			success:function(data){
				 window.location.href = "${backServer}/friendlylink/queryAll";
			}
		});
	}
	
	function del(id){
		if(confirm("确认删除吗？")){
		var url = "${backServer}/friendlylink/delLink?id="+id;
		$.ajax({
			url:url,
			type:'GET',
			success:function(data){
				 alert("删除成功！");
				 window.location.href = "${backServer}/friendlylink/queryAll";
			}
		});
		}
	}
	
	
</script>

</html>