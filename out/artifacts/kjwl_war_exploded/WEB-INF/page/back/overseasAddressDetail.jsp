<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>海外地址详情</strong></div>
      <div class="tab-body">
        <br/>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>国家:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span>${overseasAddress.country}</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>县/郡:</strong></span></div>
                    <div style="float:left;margin-left:30px;">
                       <span>${overseasAddress.county}</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>州:</strong></span></div>
                    <div style="float:left;margin-left:50px;">
                       <span>${overseasAddress.state}</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>城市:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span>${overseasAddress.city}</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>仓库:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span>${overseasAddress.warehouse}</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>地址:</strong></span></div>
                    <div style="float:left;margin-left:30px;">
                       <span>${overseasAddress.address_first}</span>
                    </div>
                 </div>
<%--                  <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>地址2:</strong></span></div>
                    <div style="float:left;margin-left:30px;">
                       <span>${overseasAddress.address_second}</span>
                    </div>
                 </div> --%>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>邮编:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span>${overseasAddress.postcode}</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>电话:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span>${overseasAddress.tell}</span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>是否免税:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <span>
                         <c:if test="${overseasAddress.is_tax_free==1}">免税</c:if>
                         <c:if test="${overseasAddress.is_tax_free==2}">不免税</c:if>
                       </span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>图标:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <img src="${resource_path}/${overseasAddress.flag_path}" width="20px" height="20px"/>
                    </div>
                 </div>
                 
                 
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label"><span><strong>只对以下客户可见:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <span>${authAccount}</span>
                    </div>
                 </div>
                  <div class="padding border-bottom" >
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="返回" />
                 </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
    </script>
</body>
</html>