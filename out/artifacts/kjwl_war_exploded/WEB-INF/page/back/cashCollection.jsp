<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<style type="text/css">
.label-class {
	font: bold;
}
</style>
<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>现金收款</strong>
			</div>
			<div class="tab-body">
				<br />
<%-- 				<form action="${backServer}/output/saveOutput" id="form" --%>
<!-- 					method="post"> -->
				<div class="field-group">
					<div>
						<div class="label" style="margin-left: 10px;">
							<label for="c_user">收银员:</label> <input type="hidden"
								id="cur_user" name="cur_user" value="${curUser}" /> ${curUser}
						</div>
					</div>
					<div class="field-item">
						<div class="label"><label for="pickorder">单号录入:</label></div>
	                    <div class="field">
	                    	<input type="text" class="input" id="original_num" name="original_num" value="" style="width:200px"/>
	                    </div>
					 </div>
				</div>
				<div class="panel admin-panel pickorder-list">
					<div class="panel-head">
						<strong>待支付包裹列表</strong>
					</div>
					<div class="padding border-bottom">
						<shiro:hasPermission name="pickorder:add">
							<input type="button" id="add" value="添加"
								class="button button-small border-green" />
						</shiro:hasPermission>
					</div>
					<table class="table table-hover" id="pkgList">
						<tr>
							<th>关联单号</th>
							<th>公司运单号</th>
							<th>用户账号</th>
							<th>运费（$）</th>
							<th>税金（$）</th>
							<th>详情</th>
							<th>操作</th>
						</tr>
					</table>
				</div>
				<li style="text-align: left; padding-left: 560px;">
					共
					<span style="color: Orange; font-size: 18px;" id="pkgAmount">0</span>
					个包裹
				</li>
				<li style="text-align: left; padding-left: 560px;">
					运费合计：
					<span style="color: Orange; font-size: 18px;">$</span>
					<span style="color: Orange; font-size: 18px;" id="freightAmount">0.00</span>
				</li>
				<li style="text-align: left; padding-left: 560px;">
					税金合计：
					<span style="color: Orange; font-size: 18px;">$</span>
					<span style="color: Orange; font-size: 18px;" id="taxAmount">0.00</span>
				</li>
				<li style="text-align: left; padding-left: 560px;">
					应付金额：
					<span style="color: Orange; font-size: 18px;">$</span>
					<span style="color: Orange; font-size: 18px;" id="totalAmount">0.00</span>
				</li>
			</div>
			<div style="clear: both"></div>
			<div class="form-button" style="margin-top: 20px;">
				<a class="button bg-main" onclick="confirmPay()" id="confirm">确认收款</a>
				<a class="button bg-main" href="${backServer}/pkg/cashCollectionInit?curUser=${curUser}" id="back">取消</a>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('#finish').click(function() {
			var seaport_id = '';
			$("input[name='seaport_id']:checked").each(
			function()
			{
				seaport_id+=this.value+",";
			}
			);
			if (seaport_id == '') {
				alert('请选择口岸');
			} else {
				$("#seaport_id_list").val(seaport_id);
				$('#form').submit();
			}
		});

		$('#back').click(function() {
			window.location.href = "${backServer}/output/queryAll";
		});
		
		$("#original_num").keydown(function(e) {  
	           if (e.keyCode == 13) {
// 	        	   addNumbers();
	        	   if( isOriginalExist() ){
	        		   alert("此单号已加入下方列表，请不要重复加入！");
	        	   }
	        	   else{
	        		   addNumbers();
	        	   }
	        	   clearOriginaNum();
	           }  
	    });
		
        function addNumbers(){
        	var url = "${backServer}/pkg/getPkgByOriginalNumUnpaid";
        	
        	var original_num=$('#original_num').val();
            if( ""==original_num||original_num==null){
                alert("单号不能为空");
                $('#original_num').focus();
                return
            }
        	$.ajax({
                url:url,
                data:{"originalNum":original_num},
                type:'post',
                success:function(data){
                    if(data.result != true){
                    	alert(data.msg);
                    	return
                    }else{
                    	var tmp = "<tr>"
	                    	+ "<td>" + data.pkg.original_num + "</td>"
	                    	+ "<td>" + data.pkg.logistics_code + "</td>"
	                    	+ "<td>" + data.pkg.account + "</td>"
	                    	+ "<td>" + data.pkg.freight + "</td>"
// 	                    	+ "<td>" + data.pkg.customs_cost + "</td>"
 	                    	+ "<td>"
 	                    		+ "<div class=\"field\">"
 	                    			+ "<input type=\"text\" oninput=\"OnInput (event)\" onpropertychange=\"OnPropChanged (event)\" class=\"input\" value=\""
 	                    			+ data.pkg.customs_cost
 	                    			+ "\" style=\"width:200px\"/>"
 	                    		+ "</div>"
 	                    	+ "</td>"
// 	                    	+ "<td style=\"display:none;\">" + data.pkg.logistics_code + "</td>"
	                    	+ "<td>"
	                    		+ "<a class=\"button border-blue button-little\" href=\"javascript:void(0);\" onclick=\"detail('"
	                    			+ data.pkg.package_id
	                    			+ "')\">详情</a>"
	                    	+ "</td>"
	                    	+ "<td>"
                    			+ "<a class=\"button border-blue button-little\" href=\"javascript:void(0);\" onclick=\"deleteTr(this)\">删除</a>"
                    		+ "</td>"
                    	+ "</tr>";
                        $('#pkgList').append(tmp);
                    }
                    
                    calcAmoiunt();
                }
            });
        }
        
        function detail(package_id){
            var url = "${backServer}/pkg/detail?package_id="+package_id;
            window.location.href = url;
        }
        
        function deleteTr(obj) {
       	 	$(obj).closest('tr').remove();
       	 	calcAmoiunt();
       	}
        
        function isOriginalExist() {
        	var inputOriginalNum=$('#original_num').val();
        	var isExist = false;
        	$("#pkgList").find("tr").each(function(){
        	    var tdArr = $(this).children();
        	    var tmpOriginal = tdArr.eq(0).text();
//         	    alert('tmpOriginal:'+tmpOriginal+',inputOriginalNum:'+inputOriginalNum);
        	    if( inputOriginalNum==tmpOriginal ){
        	    	isExist = true;
        	    	return false;	
        	    }
        	    else{
        	    	tmpOriginal = tdArr.eq(1).text(); // 两个单号都要对比
        	    	if( inputOriginalNum==tmpOriginal ){
            	    	isExist = true;
            	    	return false;	
            	    }
        	    }
        	  });
        	
//         	alert('isExist:'+isExist);
        	return isExist;
        }
        
        // 统计各种总价
        function calcAmoiunt() {
        	var freightAmount = 0.0;
        	var taxAmount = 0.0;
        	var i=0;
        	$("#pkgList").find("tr").each(function(){
        	    var tdArr = $(this).children();
        	    var tmpFreight = tdArr.eq(3).text();
				var tmpTax = tdArr.eq(4).find("input").val();
				if(i>0){// 第一次拿到的是标题，不是列表中的内容
					freightAmount += Number(tmpFreight);
					taxAmount += Number(tmpTax);
				}
        	    i++;
        	  });
        	$("#freightAmount").html((freightAmount).toFixed(2));
        	$("#taxAmount").html((taxAmount).toFixed(2));
        	$("#totalAmount").html((taxAmount+freightAmount).toFixed(2));
        	$("#totalAmount").val((taxAmount+freightAmount).toFixed(2));
        	$("#pkgAmount").html(i-1);
        }
        
    	function confirmPay() {
			var url = "${backServer}/pkg/payByCash";
			var logisticsCodes = "";
			var cost = $("#totalAmount").val();
			var taxes = "";	// 由于税金也改变了，所以还要用逗号分隔传回去
			var i=0;
        	$("#pkgList").find("tr").each(function(){
        	    var tdArr = $(this).children();
        	    var tmpCode = tdArr.eq(1).text();
        	    var tmpTax = tdArr.eq(4).find("input").val();
				if(i>0){// 第一次拿到的是标题，不是列表中的内容
					if( i>1 ){
						logisticsCodes += ","; // 用逗号分隔多个 code
						taxes += ",";
					}
					logisticsCodes += tmpCode;
					taxes += tmpTax;
				}
        	    i++;
        	  });
        	if( 1==i ){
        		alert('没有要支付的包裹');
        		return false;
        	}
			$.ajax({
			    url:url,
			    data:{"logisticsCodes":logisticsCodes,
			    	"transportCost":cost,
			    	"taxes":taxes},
			    	type:'post',
	                async:false,
			    success:function(data){
			        // 账户正常
			        if(data.result==true){
			        	alert('支付成功');
			        	var url = "${backServer}/pkg/cashCollectionInit?curUser="+$("#cur_user").val();
			            window.location.href = url;
			        }
			        else{
			        	alert('支付失败');
			        }
			    }
			});
     	}
    	
    	function clearOriginaNum(){
    		$("#original_num").val("");
    		$('#original_num').focus();
    	}
    	
    	// Firefox, Google Chrome, Opera, Safari, Internet Explorer from version 9
        function OnInput (event) {
//             alert ("The new content: " + event.target.value);
        	calcAmoiunt();
        }
    // Internet Explorer
        function OnPropChanged (event) {
            if (event.propertyName.toLowerCase () == "value") {
//                 alert ("The new content: " + event.srcElement.value);
            	calcAmoiunt();
            }
        }
	</script>
</body>
</html>