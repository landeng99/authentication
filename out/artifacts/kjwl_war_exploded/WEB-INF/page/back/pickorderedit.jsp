<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
		width: 500px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
	.upload{float: left;width: 400px;line-height: 50px;height:50px;}
	#pick_code_url{
		display: block;
		background: url(${resource_path}/img/pdf.png) no-repeat;
		height:16px;
		line-height:16px;
		float:right; 
		padding-left: 20px;
	    margin: 15px auto;
	}
	#pick_code_url{
		display: none;
	}
</style>
 
<body>
	 <div class="admin">
	 <div  class="content">
	  <div class="field-group">
	  		<input type="hidden" value="${pickOrder.pick_id}" name="pick_id" id="pick_id"/>
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="pick_code">提单号</label>
		 		</div>
		 		<div class="field">
		 			 ${pickOrder.pick_code}
		 		</div>
		 	</div>
	 		<div class="field-item">
		 		<div class="label">
		 			<label for="flight_num">空运单号</label>
		 		</div>
		 		<div class="field">
		 		 	<input  class="input" id="flight_num" type="text" value="${pickOrder.flight_num}" style="width:200px"  name="flight_num"/>
		 		</div>
		 	</div>
		 	<div class="upload">
		 		 
		 			<input id="flight_num_upload" 
		 				type="file" name="flight_num_upload" 
		 				onchange="uploadFlight('${pickOrder.pick_code}','${pickOrder.pick_id}')"/>
		 		
					<a id="pick_code_url" <c:if test="${isPicPdfFileExists}"> class="show" </c:if>  target=“_blank” 
						href="${resource_path}/upload/${pickOrder.pick_code}.pdf">
					<span>下载空运单号</span></a>
				
		 	</div>
		 	<div class="field-item">
                <div class="label">
                    <label for="status">提单状态</label>
                </div>
                <div class="field">
                    <select id="status" name="status" class="input" style="width:200px">
                            <option value="">---选择状态---</option>
                            <option value=1>已出库</option>
                            <option value=2>空运中</option>
                            <option value=3>待清关</option>
    
                        </select>
                </div>
            </div>
		 	  <div class="field-item">
		 		<div class="label">
		 			<label for="seaport_id">口岸</label>
		 		</div>
		 		<div class="field">
		 			${pickOrder.sname}
		 		</div>
		 	</div>
		 	 <div class="field-item">
		 		<div class="label">
		 			<label for="seaport_id">仓库</label>
		 		</div>
		 		<div class="field">
		 			${pickOrder.warehouse}
		 		</div>
		 	</div>
		 	
		   <div class="field-item">
		 		<div class="label">
		 			<label >描述信息</label>
		 		</div>
		 		<div class="field">
		 			  <textarea rows="3" cols="50" id="description" name="description">${pickOrder.description}</textarea>
		 		</div>
		 	</div>
	 	</div>
	 	
   		 <div style="clear: both"></div>
	  	<div class="panel admin-panel pickorder-list">
	  	  <form >
	  	      	  	 <div class="panel-head"><strong>托盘列表</strong>
	  	      	  	 <input type="button"  onclick="showScanLog(${pickOrder.pick_id})" value="扫描日志" />
	  	      	  	 <input type="button"  onclick="batchExport(${pickOrder.pick_id})" value="下载日志" />	  	  	      	  	 
	  	      	  	 </div>
	         	     <div class="padding border-bottom">
			            <c:if test="${pickOrder.status==1}">
			            	<input type="button" id="add" value="添加托盘" class="button button-small border-green"/>
			            </c:if>
			        </div>
			        <div style="min-height:200px;" >
			      
	         	     <table class="table table-hover " id="palletPkgList">
	         	     <thead>
	         	     	<tr>
	         	     		<th>托盘单号</th>
	         	     		<th>包裹数</th>
	         	     		<th>状态</th>
	         	     		<th>描述信息</th>
	         	     		<th>创建时间</th>
	         	     		<th>操作</th>
	         	     	</tr>
	         	      </thead>
	         	     	<tbody>
	         	     		<c:forEach var="pallet"  items="${pickOrder.pallets}">
	         	     		<tr>
		         	     		<td>${pallet.pallet_code}</td>
		         	     		<td>${pallet.pkgCnt}</td>
		         	     		 <td>
			         	     		<c:if test="${pallet.status==1}">已出库</c:if>
			         	     		<c:if test="${pallet.status==2}">空运中</c:if>
			         	     		<c:if test="${pallet.status==3}">待清关</c:if>
		         	     		</td>
		         	     		<td>${pallet.description}</td>
		         	     		<td>${pallet.create_time}</td>
		         	     		<td>
		         	     			 <c:if test="${pickOrder.status==1}"><a  href="${backServer}/pallet/initedit?pallet_id=${pallet.pallet_id}"  class="button button-small border-green">编辑</a></c:if>&nbsp;
		         	     			<a  href="${backServer}/pallet/detail?pallet_id=${pallet.pallet_id}"  class="button button-small border-green">详情</a>&nbsp;
		         	     			<c:if test="${pickOrder.status==1}">
		         	     				<a href="javascript:;" onclick="del(${pallet.pallet_id},${pallet.pkgCnt})" class="button button-small border-yellow">删除</a>
		         	     			</c:if>
		         	     		</td>
	         	     		</tr>
	         	     		</c:forEach>
	         	     	</tbody> 
	         	     </table>
	         	     <div class="panel-foot text-center">
	      				<jsp:include page="webfenye.jsp"></jsp:include>
	      			</div>	
	         	     </div>
	         	  </form>
	  	   </div>
	  	   <div style="clear: both"></div>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<input type="button" class="button bg-main" id="save" value="保存">
	  	   		<a class="button bg-main" href="javascript:;" onclick='back()'>返回</a>
	  	   </div>
	  	   </div>
	</div>
	<script type="text/javascript">
	function test(file_name){
		var result =/\.[^\.]+/.exec(file_name);
		return result;
	}
	function uploadFlight(code,pick_id){
		//文件名
		var file_name = $("#flight_num_upload").val();
		if("" == file_name){
			return;
		}
		//后缀
		var result = test(file_name);
		if(result == ".pdf"){
			var url = "${backServer}/pickorder/upload";
	        $.ajaxFileUpload({
	            url : url,
	            type : 'post',
	            secureuri : false,
	            fileElementId : 'flight_num_upload',
	            data:{'code':code},
	            dataType : 'text',
	            success : function(data) {
	            	layer.alert("导入成功",function(index){
	            		$('#pick_code_url').show();
	            		$('#flight_num_upload').val("");
	            		 layer.close(index);
	                	//window.location.href="${backServer}/pickorder/initedit?pick_id="+pick_id;
	                	 
                	});	
	            	
	           /*  	if(data == "1"){
	                	layer.alert("导入成功",function(){
		                	window.location.href="${backServer}/pickorder/initedit?pick_id="+pick_id;
	                	});	
	            	} */
	            },
	            error : function(data, status, e) {
	                layer.alert("error" + e);
	            }
	        });
		}else{
			layer.alert("文件必须是pdf格式");
			$('#flight_num_upload').val("");
		}
	};
	
		$('#status').val('${pickOrder.status}');
		$('#add').click(function(){
			window.location.href="${backServer}/pallet/add?pick_id=${pickOrder.pick_id}&pick_code=${pickOrder.pick_code}&seaport_id=${pickOrder.seaport_id}&overseas_address_id=${pickOrder.overseas_address_id}";
		});
		$('#save').click(function(){
				var param={};
				param['pick_id']=$('#pick_id').val();
				param['flight_num']=$('#flight_num').val();
				param['status']=$('#status').val();
				param['description']=$('#description').val();
				
				$.ajax({
					url:'${backServer}/pickorder/edit',
					data:param,
					dataType:'json',
					type:'post',
					async:false,
					success:function(data){
						if(data.result){
							layer.alert('保存成功',function(){
								window.location.href=window.location.href;
							});
							
						}else{
							layer.alert('保存失败');
						}
					}
				});			
		});	
		
		function del(pallet_id,pkgCnt){
	 		var url='${backServer}/pickorder/delpallet?pick_id=${pickOrder.pick_id}&pallet_id='+pallet_id;
	 		var msg="";
	 		if(pkgCnt>0){
	 			msg="包裹数量大于0，确定删除？";
	 		}
	 		else{
	 			msg="确定删除？";
	 		}
	 		
			if(window.confirm(msg)){
				window.location.href=url;
			} 
		}
		 function back(){
			 window.location.href="${backServer}/breadcrumb/back";
		 }
		 
			function showScanLog(pick_id)
			{
			    layer.open({
			        title :'扫描日志',
			        type:2,
			        shadeClose: true,
			        shade: 0.8,
			        offset: ['10px', '100px'],
			        area: ['900px', '800px'],
			        content:'${backServer}/outputScanLog/showAllScanLog?pick_id='+pick_id
			    }); 
			}
			function batchExport(pick_id) {
				window.location.href = "${backServer}/outputScanLog/exportOutputScanLog?pick_id="
						+ pick_id
						;
			}		 
	</script>
</body>
</html>