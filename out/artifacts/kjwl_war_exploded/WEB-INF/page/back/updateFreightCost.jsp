<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>
<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>会员费用修改</strong></div>
      <div class="tab-body">
        <br/>
                 <input type="hidden" id="freight_id" value="${freight_id}">
                 <input type="hidden" id="rate_name" value="${rate_name}">
                 <input type="hidden" id="rate_id" value="${rate_id}">
                 <div style="height: 40px;">
                    <div style="float:left; margin-left:10px" class="label">
                    	<span><strong>等级名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;">${rate_name}</div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <input type="text" class="input" id="unit_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                              value="${unit_price}" />
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/磅</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="unitPriceTextArea"></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>首重单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <input type="text" class="input" id="first_weight_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                              value="${first_weight_price}" />
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/磅</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="firstPriceTextArea"></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>续重单价:</strong></span></div>
                    <div style="float:left;margin-left:38px;">
                       <input type="text" class="input" id="go_weight_price" style="width:100px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                              value="${go_weight_price}" />
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/磅</span>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="goPriceTextArea"></span>
                    </div>
                 </div>
                 
                  <%-- <c:forEach items="${list}" var="freightCost">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label">
                    	<span><strong>${freightCost.attachService.service_name}:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                    	<input type="hidden" name="attach_id"
                               value="${freightCost.attachService.attach_id}" />
                        <input type="text" class="input" name="service_price" style="width:100px"
                               onkeyup="value=value.replace(/\D/gi,"")"
                               value="${freightCost.attachService.service_price}" />
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span>元/票</span>
                    </div>
                 </div>
				 </c:forEach> --%>
				 
                  <div class="padding border-bottom" >
                    <input type="button" class="button bg-main" onclick="save()" value="保存" />
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
                 </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
    $(function(){
        $('#unit_price').focus();
    });
    
      function save() {

      var url = "${backServer}/freightCost/update";
      var freight_id = $('#freight_id').val();
      var rate_name = $('#rate_name').val();
      var unit_price = $('#unit_price').val();
      var first_weight_price = $('#first_weight_price').val();
      var go_weight_price = $('#go_weight_price').val();
      var rate_id = $('#rate_id').val();
   /*    var attachIdCount = $('input[name="attach_id"]').length;
      var attachId = "";
      var servicePrice = "";
      for(var i=0; i<attachIdCount; i++){
    	  attachIdObj = $('input[name="attach_id"]')[i].value+",";
    	  attachId += attachIdObj;
    	  servicePriceObj = $('input[name="service_price"]')[i].value+",";
    	  servicePrice += servicePriceObj;
      }
      alert(attachId);
      alert(servicePrice); */
      $.ajax({
              url : url,
              data : {
                  "freight_id" : freight_id,
                  "rate_name" : rate_name,
                  "unit_price": unit_price,
                  "first_weight_price": first_weight_price,
                  "go_weight_price": go_weight_price,
                  "rate_id": rate_id
                  },
              type : 'post',
              async:false,
              dataType : 'text',
              success : function(data) {
                  alert("修改成功");
                  window.location.href = "${backServer}/freightCost/freightCostList";
              }
          });
      }
      </script>
</body>
</html>