<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>导出模板添加</strong></div>
      <div class="tab-body">
        <br />
         <input type="hidden" id="checkResult" value="0">
          <div style="height: 30px;">
            <div style="float:left; margin-left:10px;width:255px" class="label"><span><strong>项目名称:</strong></span></div>
            <div style="float:left;" class="label"><span><strong>模板:</strong></span></div>
         </div>
         <div style="height: 400px;">
            <div style="float:left;margin-left:10px;">
              <select id="col_name" name="col_name" multiple="multiple" class="input" style="width:200px;height:400px">
                  <c:forEach var="excelColumn"  items="${excelColumnList}">
                        <option value="${excelColumn.col_key}">${excelColumn.col_name}</option>
                  </c:forEach>
              </select>
            </div>
            <div style="float:left;margin-top:5px; margin-left:10px" class="label">
               <input type="button" class="button border-blue button-little" onclick="add()" value="添加" /></br></br>
               <input type="button" class="button border-blue button-little" onclick="up()" value="上移" /></br></br>
               <input type="button" class="button border-blue button-little" onclick="down()" value="下移" /></br></br>
               <input type="button" class="button border-blue button-little" onclick="del()" value="删除" /></br></br>
               <input type="button" class="button border-blue button-little" onclick="addAll()" value="全选" /></br></br>
               <input type="button" class="button border-blue button-little" onclick="delAll()" value="全删" /></br></br>
               <input type="button" class="button border-blue button-little" onclick="refresh()" value="复位" />
            </div>
            
            <div style="float:left;margin-left:10px;">
              <select id="template_col" name="template_col" multiple="multiple" class="input" style="width:200px;height:400px">
              </select>
            </div>
            <div  style="float:left;margin-left:10px;">
            <div class="label"><span><strong>模板名称:</strong></span></div>
            <div>
                <input type="text" class="input" id="template_name" name="template_name" style="width:200px" onblur="checkTemplateName()"/>
                <span id="message"></span>
            </div>
            
 
            <div class="label"><span><strong>描述:</strong></span></div>
            <div >
                 <textarea class="input" rows="3" cols="50" id="description" name="description"></textarea>
            </div>
            </div>
        </div>
        <div class="padding border-bottom" >
             <a href="javascript:void(0);" class="button bg-main" onclick="save()">保存</a>
             <a href="javascript:history.go(-1)" class="button bg-main">取消</a>
        </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
    
    function add(){
        
       $('#col_name :selected').appendTo($('#template_col'));
          //最大索引
       var maxindex=$('#template_col').find('option:last').index();
         //最后一个选中
       $('#template_col').get(0).selectedIndex=maxindex;
    }
    
    function del(){
        
        $('#template_col :selected').appendTo($('#col_name'));
           //最大索引
        var maxindex=$('#col_name').find('option:last').index();
          //最后一个选中
        $('#col_name').get(0).selectedIndex=maxindex;
     }
    
    function up(){
        
        $('#template_col :selected').insertBefore($('#template_col :selected').prev());
        }
    
    function down(){
        
        $('#template_col :selected').insertAfter($('#template_col :selected').next());
        }
    
    function addAll(){
        
        $('#col_name option').appendTo($('#template_col'));
           //最大索引
        var maxindex=$('#template_col').find('option:last').index();
          //最后一个选中
        $('#template_col').get(0).selectedIndex=maxindex;
     }
    function delAll(){
        
        $('#template_col option').appendTo($('#col_name'));
           //最大索引
        var maxindex=$('#col_name').find('option:last').index();
          //最后一个选中
        $('#col_name').get(0).selectedIndex=maxindex;
     }
    
    function refresh(){
        var url = "${backServer}/template/refresh";
        $.ajax({
            url:url,
            type:'post',
            dataType:'json',
            async:false,
            success:function(data){
                
                $('#col_name').empty();
                $('#template_col').empty();
                
                $.each(data, function(i, excelColumn) {
                
                   var option = "<option value="+excelColumn.col_key+">"
                                 +excelColumn.col_name+"</option>";
                
                $("#col_name").append(option);
                
                })
            
            }
        });
        
    }

    function checkTemplateName(){
        $('#message').empty();
        var url = "${backServer}/template/checkTemplateName";
        var template_name=$('#template_name').val();
        if(template_name==null||template_name.trim()==""){
            return;
         }

        $.ajax({
            url:url,
            data:{"template_name":template_name},
            type:'post',
            dataType:'text',
            async:false,
            success:function(data){
                $('#checkResult').val(data);
                if(data=="0"){
                   $('#message').text('该模板名称已经存在!');
                   $('#message').css('color','red');
                }
            }
        });
    }

    function save(){
        var url = "${backServer}/template/save";

        var options=$('#template_col').find('option');
        var template_name=$('#template_name').val();
        var description=$('#description').val();
        
        if(template_name==null||template_name.trim()==""){
            alert("模板名称不能为空");
            return;
         }
        
        if(options==null||options.length==0){
            alert("请选择模板项目");
            return;
         }
        
        if($('#checkResult').val()!=1){
            alert("该模板名称已经存在!");
            return;
         }

        var arr = [];
        for (var i = 0; i < options.length; i++) {

          var  str = "{\"col_key\":\"" +options.eq(i).val()
                +"\",\"col_name\":\""+options.eq(i).html()+"\"}"
            arr.push(str);
        }
        
        // 用逗号链接数组里的值
        var items = "["+arr.join(',')+"]";

        $.ajax({
            url:url,
            data:{"template_name":template_name,
                  "items":items,
                  "description":description},
            type:'post',
            dataType:'text',
            async:false,
            success:function(data){
               alert("保存成功");
               window.location.href = "${backServer}/template/templateInit";
            }
        });
    }
    </script>
</body>
</html>