<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:90px;
    }
    .div-front{
        float:left;
        width:400px;
    }
    .div-front-List{
        float:left;
    }
    
     .th-title{
        height:30px;
        vertical-align:middle;
    }
    
        .td-List-left{
        height:30px;
        vertical-align:middle;
        text-align:left;
        padding-left:5px;
    }
        .td-List-right{
        height:30px;
        vertical-align:middle;
        text-align:right;
        padding-right:5px;
        
    }
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">提现审核</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <div class="tab-panel active" id="tab-set">
        <input type="hidden" id="log_id" value="${withdrawalLog.log_id}">
        <div class="field-group ">
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">申请人:</span>
                           <span>${frontUser.user_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">手机号:</span>
                           <span>${frontUser.mobile}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户账户:</span>
                           <span>${frontUser.account}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">账户余额:</span>
                           <span><fmt:formatNumber value="${frontUser.balance}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">可用余额:</span>
                           <span><fmt:formatNumber value="${frontUser.able_balance}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">冻结余额:</span>
                           <span><fmt:formatNumber value="${frontUser.frozen_balance}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">提现方式:</span>
                           <span>${withdrawalLog.bank_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">账号:</span>
                           <span>${withdrawalLog.alipay_no}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">开户姓名:</span>
                           <span>${withdrawalLog.user_name}</span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">提现金额:</span>
                           <span><fmt:formatNumber value="${withdrawalLog.amount}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">状态:</span>
                           <span>
                              <c:if test="${withdrawalLog.status==1}">申请中</c:if>
                              <c:if test="${withdrawalLog.status==2}">审核通过</c:if>
                              <c:if test="${withdrawalLog.status==3}">审核拒绝</c:if>
                              <c:if test="${withdrawalLog.status==4}">取消</c:if>
                           </span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">申请时间:</span>
                           <span><fmt:formatDate value="${withdrawalLog.time}" type="both"/></span>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">交易号:</span>
                           <span>${withdrawalLog.trans_id}</span>
                      </div>

                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">审核人员:</span>
                           <span>${withdrawalLog.opreate_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">审核时间:</span>
                           <span><fmt:formatDate value="${withdrawalLog.operate_time}" type="both"/></span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;"><span"><strong>审核结果:</strong></span></div>
                      <div style="float:left;margin-left:40px;margin-top:-5px;">
                        <input type="radio" class="input" id="yes" name="status" style="width:30px" value = 2 >
                      </div>
                      <div style="float:left;">
                      <span>同意</span>
                      </div>
                      <div style="float:left;margin-left:40px;margin-top:-5px;">
                        <input type="radio" class="input" id="no" name="status" style="width:30px" value = 3 checked="checked" >
                      </div>
                      <div style="float:left;">
                      <span>拒绝</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;margin-top:5px;"><span><strong>提现备注:</strong></span></div>
                      <div style="float:left;margin-left:40px;width:800px;">
                        <span>${withdrawalLog.remark}</span>
                      </div>
                 </div>

         </div>
        <div class="form-button" >
        <a href="javascript:void(0);" class="button bg-main" onclick="save()" >保存</a>
                    <a href="javascript:history.go(-1)" class="button bg-main">返回</a> 
        </div>
             </div>
             </div>
        </div>
        </div>
<script type="text/javascript">

//选择框选中
    function save(){
        var url = "${backServer}/withdrawalLog/update";

        var log_id=$('#log_id').val();
        var status =$("input[name='status']:checked").val();
        var remark =$('#remark').val();
        
        if(3==remark &&(remark==null||remark.trim()=="")){
            alert("拒绝理由不能为空");
            $('#approve_reason').focus();
            return
        }

        $.ajax({
            url:url,
            data:{"log_id":log_id,
                  "status":status,
                  "remark":remark},
            type:'post',
            dataType:'text',
            async:false,
            success:function(data){
               alert("保存成功");
               window.location.href = "${backServer}/withdrawalLog/queryAllOne";
            }
        });
    }
    
    $('#yes').click(function(){
        $('#remark').attr('disabled',"disabled");
    });
    $('#no').click(function(){
        $('#remark').removeAttr('disabled');
    });
    </script>
</body>
</html>