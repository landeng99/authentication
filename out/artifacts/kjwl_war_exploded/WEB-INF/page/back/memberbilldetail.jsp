<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
  <style type="text/css">
    .field-group .field-item{
        float:left;
        margin-right: 20px;
    }
    .field-group .field-item .label{
        float:left;
        line-height: 33px;
        margin-right: 10px;
        width: 80px;
    }
    
    .field-group .field-item .interval_label{
        float: left;
    }
    .field-group .field-item .field{
        float:left;
        width: 203px;
    }
    
    .bill-list{
        margin-top: 35px;
    }
</style>
<body>
     <div class="admin">
         <div class="admin_context">
           <div class="tab">
              <div class="tab-head">
                <ul class="tab-nav">
                  <li class="active"><a href="#accounting">同行用户账单详情</a></li>
               </ul>
              </div>
               <div class="panel admin-panel bill-list">

                      <table class="table">
                          <tr>
                              <th width="10%">包裹创建日期</th>
                              <th width="10%">公司运单号</th>
                              <th width="10%">所属仓库</th>
                              <th width="5%">用户名称</th>
                              <th width="5%">业务</th>
                              <th width="15%">品名</th>
                              <th width="10%">品牌</th>
                              <th width="5%">申报类别</th>
                              <th width="5%">申报总价值(RMB)</th>
                              <th width="5%">重量(磅)</th>
                              <th width="5%">重量(收费/磅)</th>
                              <th width="5%">单价(RMB)</th>
                              <th width="5%">运费(RMB)</th>
                              <th width="5%">合计(RMB)</th>
                          </tr>
                      </table>
                      <table class="table table-hover">
                        <c:forEach var="user"  items="${billList}">
                           <tr> 
                              <td width="10%"><fmt:formatDate value="${user.createTime}" type="both"/></td>
                              <td width="10%">${user.logistics_code}</td>
                              <td width="10%">${user.warehouse}</td>
                              <td width="5%">${user.user_name}</td>
                              <td width="5%">${user.business_name}</td>
                              <td width="15%">${user.goods_name}</td>
                              <td width="10%">${user.brand}</td>
                              <td width="5%">${user.goods_type}</td>
                              <td width="5%">${user.total_worth}</td>
                              <td width="5%">${user.actual_weight}</td>
                              <td width="5%">${user.weight}</td>
                              <td width="5%">${user.price}</td>
                              <td width="5%">${user.freight}</td>
                              <td width="5%">${user.transport_cost}</td>
                          </tr>
                        </c:forEach>
                       </table>
                   </div>
               </div>
        <div class="form-button" >
                    <a href="javascript:history.back()" class="button bg-main">返回</a> 
        </div>               
             </div>
           </div>
</body>
</html>