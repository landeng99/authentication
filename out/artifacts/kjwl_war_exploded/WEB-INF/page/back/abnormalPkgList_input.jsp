<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>
<style type="text/css">
    .div-title{
        font-weight: bold;
        float:left;
        width:90px;
        margin-top:5px;
        margin-left:10px;
    }
        .div-input{
        float:left;
        width:220px;
    }
    
    .input-width{
        width:200px;
    }

</style>
<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>异常包裹</strong></div>
      <div class="tab-body">
        <br />
            <form action="${backServer}/unusualPkg/input_search" method="post" id="myform" name="myform">
                 <div style="height: 40px;">
                    <div class="div-title">关联单号:</div>
                    <div class="div-input">
                        <input type="text" class="input input-width" id="original_num" name="original_num"
                               onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
                               value="${params.original_num}"/>
                    </div>
                    
                    <div class="div-title">状态:</div>
                    <div class="div-input">
                      <select name="unusual_status" id="unusual_status">
                      <option value="">--选择状态--</option>
                      <option value="-1">未认领</option>
                      <option value="1">已认领</option>
                      </select>
                    </div>

                    <div class="div-title">仓库:</div>
                    <div class="div-input">
						<select id="osaddr_id"
							name="osaddr_id" style="width: 200px">
							<option value="">---------选择仓库---------</option>
							<c:forEach var="userOverseasAddress"
								items="${overseasAddressList}">
								<option value="${userOverseasAddress.id}">${userOverseasAddress.warehouse}</option>
							</c:forEach>
                      </select>
                    </div>                    
                 </div>
             
                 
                 <div style="height: 40px;">
                    <div class="div-title">起始时间:</div>
                    <div class="div-input">
                     <input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
                            value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                    <div class="div-title">终止时间:</div>
                    <div class="div-input">
                     <input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})" 
                            value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                    
                    <div class="div-title">LASTNAME:</div>
                    <div class="div-input">
                        <input type="text" class="input input-width" id="last_name" name="last_name"
                               value="${params.last_name}"/>
                    </div>
                    
                   <div class="div-title">下单状态:</div>
                    <div class="div-input">
                      <select name="down_flag" id="down_flag">
                      <option value="">--选择下单状态--</option>
                      <option value="Y">未下单</option>
                      <option value="N">已下单</option>
                      </select>
                    </div>
                 </div>
            </form>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" onclick="search()" value="查询" />
                    <input type="button" class="button bg-main" onclick="batchExport()" value="批量导出" />
                 </div>
                 <table class="table">
                    <tr>
                        <th width="10%">关联单号</th>
                        <th width="8%">包裹重量(磅)</th>
                        <th width="10%">创建时间</th>
                        <th width="7%">状态</th>
                        <th width="10%">创建人</th>
                        <th width="10%">LASTNAME</th>
                        <th width="10%">存放位置</th>
                        <th width="7%">下单状态</th>
                        <th width="8%">备注</th>
                        <shiro:hasPermission name="input_pkgabnormalPkg:edit">
                        <th width="7%">操作</th>
                        </shiro:hasPermission>
                        <shiro:hasPermission name="input_pkgabnormalPkg:union">
                        <th width="7%">操作</th>
                        </shiro:hasPermission>
                        <shiro:hasPermission name="input_pkgabnormalPkg:dis_union">
                        <th width="6%">操作</th>
                        </shiro:hasPermission>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="unusualPkg"  items="${unusualPkgList}">
                     <tr>
                     	 <td width="10%">${unusualPkg.original_num}</td>
                         <td width="8%">${unusualPkg.actual_weight}</td>
                         <td width="10%"><fmt:formatDate value="${unusualPkg.create_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                         <td width="7%">
	                         <c:if test="${unusualPkg.unusual_status==-1}">未认领</c:if>
	                         <c:if test="${unusualPkg.unusual_status==1}">已认领</c:if>
                         </td>
                         <td width="10%">${unusualPkg.username}</td>
                          <td width="10%">${unusualPkg.last_name}</td>
                         <td width="10%"></td>
                         <td width="7%">
                         	<c:choose>
                         		<c:when test="${unusualPkg.doneOrder=='Y'}">已下单</c:when>
                         	<c:otherwise>
                         		未下单
                         	</c:otherwise>
                         	</c:choose>
                         </td>
                         <td width="8%">${unusualPkg.remark}</td>
                         <shiro:hasPermission name="input_pkgabnormalPkg:edit">
                         <td width="7%"><a href="${backServer}/unusualPkg/unusualPkgEditInt?unusual_pkg_id=${unusualPkg.unusual_pkg_id}">编辑</a></td>
                         </shiro:hasPermission>
                         <shiro:hasPermission name="input_pkgabnormalPkg:union">
                         <td width="7%"><a href="javascript:connect(${unusualPkg.unusual_pkg_id},${unusualPkg.connect_package_id})">关联</a></td>
                        </shiro:hasPermission>
                        <shiro:hasPermission name="input_pkgabnormalPkg:dis_union">
                         <td width="6%"><a href="javascript:disconnect(this,${unusualPkg.unusual_pkg_id})">取消关联</a></td>
                         </shiro:hasPermission>
                     </tr>
                  </c:forEach>
                  </table>
            <div class="panel-foot text-center">
              <jsp:include page="webfenye.jsp"></jsp:include>
            </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
	 	$('#osaddr_id').val('${params.overseasAddress_id}');    	
   	 	$('#unusual_status').val('${params.unusual_status}');
   	 	$('#down_flag').val('${params.down_flag}');
   	 
        $(function(){
            $('#originalNum').focus();
        });
        
        function associate(package_id){
            var url = "${backServer}/pkg/associate?package_id="+package_id;
            window.location.href = url;
        }
        
        function search(){

            document.getElementById('myform').submit();
        }
        
      //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search();
               }
          }
        });
      
      //取消关联
      function disconnect(dis_a_element,unusal_pkg_id)
      {
			var k = layer.confirm('确定取消关联该包裹？', function () {
	            var url = "${backServer}/unusualPkg/disConnectUnusualPackage?unusual_pkg_id="+unusal_pkg_id;
	            window.location.href = url;
			});
      }
      
      //关联包裹
	function connect(unusal_pkg_id,connect_package_id)
		{
    	  if(connect_package_id<1)
    	 {
		       layer.open({
				bgcolor: '#fff',
				type: 2,
				title: "关联包裹",
				shadeClose: false,
				fix: false,
				shade: [0.5, '#ccc', true],
				border: [1, 0.3, '#666', true],
				area: ['400px', '330px'],
				//closeBtn: [0, true], //显示关闭按钮
				content:'${backServer}/unusualPkg/connectUnusualPackageInit?unusual_pkg_id='+unusal_pkg_id,
				success: function () {
				},
				end: function () {
				}
				});
    	 }
		}
      
	function batchExport() {
		var fromDate = $("#fromDate").val();
		var toDate = $("#toDate").val();
		var osaddr_id = $("#osaddr_id").val();
		var original_num = $("#original_num").val();
		var last_name = $("#last_name").val();
		var unusual_status = ($('#unusual_status')
				.find("option:selected"))[0].value;
		var down_flag = ($('#down_flag')
				.find("option:selected"))[0].value;
		window.location.href = "${backServer}/unusualPkg/exportUnusualPackage?fromDate="
				+ fromDate
				+ "&toDate="
				+ toDate
				+ "&original_num="
				+ original_num
				+ "&last_name="
				+ last_name				
				+ "&osaddr_id="
				+ osaddr_id
				+ "&unusual_status="
				+ unusual_status
				+"&down_flag="
				+down_flag;
	}
    </script>
</body>
</html>