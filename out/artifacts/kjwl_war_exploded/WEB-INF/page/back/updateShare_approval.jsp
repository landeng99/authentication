<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
</style>
<body>
<div class="admin">
	<div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">晒单信息详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
		 
			<form id="myform" name="myform">
			<input type="hidden" id="share_id" name="share_id" value="${sharePojo.share_id}">
			 <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>用户账号:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                     <span>${sharePojo.account}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong>用户名:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${sharePojo.user_name}</span>
                   </div>
              </div>
              <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>包裹单号:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                     <span>${sharePojo.logistics_code}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong>晒单时间:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${sharePojo.share_time}</span>
                   </div>
              </div>
              <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>审核状态:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${sharePojo.approval_status==1?'未审核':sharePojo.approval_status==2?'通过':'拒绝'}</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>审核原因:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${sharePojo.reason }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
              <br>

             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>优惠券编号:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${userCoupon.coupon_code }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>优惠券名称:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${userCoupon.coupon_name }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>优惠券数量:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${userCoupon.quantity }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>是否已使用:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>
                      <c:if test="${userCoupon.coupon_name !=null}">
                      ${userCoupon.haveUsedCount==0?'否':'是' }
                      </c:if>
                      </span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
              
              <br>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>晒单链接:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>${sharePojo.share_link }</span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
             <div style="height: 30px;">
                   <div style="float:left;"><span for="userName"><strong>晒单图片:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span></span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
               <div style="height: 120px;">                            
                    <c:forEach var="shopImg"  items="${pkgImgList}">
        	              <div class="divImgOrder" val="${shopImg.img_id}" style="width: 120px; height: 300px; margin-top:5px;margin-left:8px;float:left;border:1px solid #E1E1E1;" id="divOrderImg${shopImg.img_id}">
						  	<div style="width: 120px;height:120px;display:table-cell;vertical-align: middle;text-align: center;">
					      	<a href="${resource_path}${shopImg.img_path}" target="_blank"><img  alt="" src="${resource_path}${shopImg.img_path}" id="imgOrder${shopImg.img_id}" style="cursor:pointer;width:120px;"/></a>
				         	</div>
						  </div>
			</c:forEach>
			</div>
			<br>
			     
               <div style="height: 120px; margin-top:200px;">
                   <div style="float:left;"><span for="createTime"><strong>选择操作:</strong></span></div>
                   <div style="float:left;margin-left:40px;">
                   
      			<input name="approval_status" value="refusing" type="radio"/>拒绝            
      			<input name="approval_status" value="isrefusing" type="radio" checked="checked"/>通过
                     <input type="hidden" value="${sharePojo.share_id}" 
                     	name="shareId" id="shareId"/>
                   </div>
              </div>
              
             <div style="height: 40px;">
                   <div style="float:left;"><span for="userName"><strong>选择赠送优惠券：</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span>
                      	<select id="awardCouponId" name="awardCouponId">
                      	<c:forEach var="coupon"  items="${couponLists}">
                      	<option value="${coupon.couponId}" <c:if test="${coupon.couponId==2}">selected="selected" </c:if>>
                      	${coupon.couponCode} | ${coupon.coupon_name} | <c:if test="${coupon.left_count==-1}">无限 </c:if>
                      	<c:if test="${coupon.left_count!=-1}">${coupon.left_count} </c:if></option>
                      	</c:forEach>
                      	</select>
                      </span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>
              
             <div style="height: 40px;">
                   <div style="float:left;"><span for="userName"><strong>数量:</strong></span></div>
                   <div style="float:left;margin-left:40px;width:400px;">
                      <span><input id="awardCount" name="awardCount" onkeyup="value=value.replace(/[^\d.]/g,'')"></span>
                   </div>
                   <div style="float:left;"><span for="createTime"><strong></strong></span></div>
                   <div style="float:left;margin-left:10px;">
                   </div>
              </div>                            
                <div style="height: 40px;">
                     <div style="float:left;"><span for="address"><strong>操作原因:</strong></span></div>
                     <div style="float:left;margin-left:40px;">
                       <textarea rows="3" cols="50" 
               	    	id="reason" name="reason">
               	    	这个理由是新增的,操作的原因</textarea>
                     </div>
                </div>
               
             </form>
        </div>
        
         <div class="form-button" style="float:left;margin-left:100px;margin-top:10px; clear: both;">
         		<c:if test="${null != couponLists}">        
         		<a class="button bg-main" href="javascript:void(0);" 
      				onclick="save()">保存</a>&nbsp;&nbsp;
      		</c:if>
      			<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;
      	 </div>		
      	 
        </div>
      </div>
	</div>
</div>
</body>
<script type="text/javascript"> 
	function inShare(url){
		window.location.href = url;
		
	}
	function save(){
		var awardCouponId=$('#awardCouponId').val();
		var awardCount=$('#awardCount').val();
		var reason=$('#reason').val();
		var type = $('input[name="approval_status"][type="radio"]:checked').val();
		if("refusing" == type){
			if(reason==null|reason=='')
			{
				alert("必须填入操作原因!");
			}else
			{
				updateShare($('#share_id').val(),3,awardCouponId,awardCount,reason);
			}
		}else if("isrefusing" == type){
			if(awardCount<1)
			{
				alert("数量必须大于0!");
				return ;
			}
			$.ajax({
				url:'${backServer}/share/checkCanAwardCoupon',
				type:'post',
				data:{"coupon_id":awardCouponId,"awardCount":awardCount},
				dataType:"json",
				async:false,
				success:function(data){
					if("N" == data.canAward){
						alert("赠送数量大于优惠券剩余数量!");
					}else
					{
						updateShare($('#share_id').val(),2,awardCouponId,awardCount,reason);
					}
				}			
			});
		}else{
			alert("请选择操作状态");
		}
	}
	function updateShare(shareId,approvalStatus,awardCouponId,awardCount,reason){
		var url = "${backServer}/share/updateShare?share_id="+shareId+"&approval_status="+approvalStatus+"&awardCouponId="+awardCouponId+"&awardCount="+awardCount+"&reason="+reason;
		window.location.href = url;
	 }
	
</script>
</html>