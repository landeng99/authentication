<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<% String path = request.getContextPath(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=path%>/resource/css/dispatch.css">
	<script src="http://apps.bdimg.com/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="tab-content">
   <!-- 配货查询、导出、新增 -->
   <form class="form-inline" action="${backServer}/dispatch/search" method="post">
		<div class="col-lg-12">
			<div class="form-group">
			   <label class="danhao" for="logistics_code">公司单号:</label>
			   <input type="text" class="form-control biankuang" name="logistics_code" id="logistics_code" placeholder="请输入..." style="width: 250px">
			</div>
			<div class="form-group">
			   <label class="guanlian" for="original_num">关联单号:</label>
			   <input type="text" class="form-control biankuang" name="original_num" id="original_num" placeholder="请输入..." style="width: 250px">
		    </div>	
		    <button type="submit" class="btn btn-info search" ><i class="glyphicon glyphicon-search">查 询</i></button>
		 </div> 
	</form>
	<form class="form-inline" action="${backServer}/dispatch/exportPackage" method="post">       
		 <div class="col-lg-12">          
	        <label class="baoguo" for="checkbox_font">包裹状态:</label>
	        	<div class="checkbox">
					<label class="checkbox_font"><input type="radio" value="0" name="status[]" checked="checked"> 待入库</label>
					<label class="checkbox_font"><input type="radio" value="1" name="status[]"> 已入库</label>
				    <label class="checkbox_font"><input type="radio" value="2" name="status[]"> 待发货</label> 
			    </div>
		   <button type="submit" class="btn btn-info export"><i class="glyphicon glyphicon-export">导出包裹</i></button>
		   <a class="btn btn-info add" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-refresh">口岸列表</i></a> 
		 </div>
    </form>
	 
	<!-- 配货详细列表 -->
	<table class="table table-striped table-bordered table-hover table-responsive "> 
	    <caption></caption> 
	    <thead > 
	        <tr class="success"> 
	            <th>序号</th> 
	            <th>列表名</th> 
	            <th>口岸</th> 
	            <th>包裹数</th> 
	            <th>创建时间</th> 
	            <th>操作</th> 
	        </tr> 
	    </thead>  
	    <tbody>
	    <c:forEach var="li" items="${list}">
	    	<tr id="table_tr" class="active">
 	            <td>${li.id}</td> 
 	    		<td>${li.name}</td> 
 	    		<td>${li.sname}</td> 
 	    		<td>${li.count}</td> 
 	    		<td><fmt:formatDate value="${li.createTime}" pattern="yyyy-MM-dd HH:mm"/></td>
	    		<td>
	            	<a class="btn btn-info btn-large btn-primary" href="${backServer}/dispatch/dispatchDetail?id=${li.id}&sid=${li.sid}"><i class="glyphicon glyphicon-send">详情</i></a> 
	            	<a class="btn btn-info btn-large btn-primary" href="${backServer}/dispatch/exportExcel?id=${li.id}"><i class="glyphicon glyphicon-export">导出包裹</i></a>
	            	<button type="submit" class="btn btn-info btn-large btn-primary" onclick="dele(${li.id},this)"><i class="glyphicon glyphicon-trash">删除</i></button>
	            </td> 
	    	</tr>
	    </c:forEach>
	    </tbody>
	 </table>  
	 <div class="panel-foot text-center">
        <jsp:include page="webfenye.jsp"></jsp:include>
     </div>
     
	<!-- 新增口岸列表 -->
 	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog ">
			<div class="modal-content" >
				<div class="modal-header ">
					<span class="modal-title glyphicon glyphicon-pencil">新增</span>	
				</div>
				<div class="modal-body  form-horizontal" role="form">
					<div class="control-group info ">
				        <label for="inputIn" class="control-label col-lg-2">选择口岸:</label> 
			            <select id="seaport" class="form-control input">
							<c:forEach var="seaport" items="${seaportList}">
						  		<option value="${seaport.sid}">${seaport.sname}</option>
							</c:forEach>
						</select>
					</div>
					<div class="control-group info">
				    	<label for="inputIn" class="control-label col-lg-2">列表名字:</label> 
				        <input type="text" id="listName" class="form-control input" placeholder="请输入...">
					</div>
					<div class="control-group info">
						<label for="inputIn" class="control-label col-lg-2">添加备注:</label> 
				        <input type="text" id="description" class="form-control input" placeholder="请输入...">
				    </div> 
				</div>
				<div class="modal-footer">
					<button type="submit" id="submit" class="btn btn-info submit" data-dismiss="modal" onclick="submit()"><span class="glyphicon glyphicon-ok">提交</span></button>
					<button type="button" class="btn btn-info" data-dismiss="modal"><span class="glyphicon glyphicon-remove">关闭</span></button>
				</div>
		   </div>
	    </div>
 	</div>
</div>

<script type="text/javascript">

	<!--新增口岸列表-->
	function submit(){
		//口岸
		var sid = $("#seaport").val();
		//列表名字
		var name = $("#listName").val();
		//备注
		var description = $("#description").val();
        
		if((name == null || name == "") || (description == null || description ==" ")){
			alert("提示！列表名字或备注不能为空!");
		}
		/* if(description == null || description ==" "){
			alert("提示！备注不能为空!");
		}  */
		else{
			var url = "${backServer}/dispatch/appendList";
			$.ajax({
				url:url,
				data:{
					"sid":sid,
					"name":name,
					"description":description,
				},
				type:"post",
				datatype:"text",
				async:true,
				success:function(data){
					alert("新增成功");
				}
			});
		}
	 window.location.href="${backServer}/dispatch/init";
	}
	<!--新增口岸列表自动清空模态框数据-->
	$(function(){
		$("#submit").click(function(){
			$("#listName").val("");
			$("#description").val("");
		});
	});
	
	<!--删除列表数据-->
	function dele(id,obj){
		
		var url = "${backServer}/dispatch/deleteLine";
		if(confirm("确定删除吗?")){
			$(obj).parent().parent().remove();
			$.ajax({
				url:url,
				data:{
					"id":id,
				},
				type:"post",
				datatype:"text",
				success:function(data){
					alert("提示！删除成功!");
				}
			});
		}else{
			alert("小手一抖，数据全没有🍓！！！");
			return false;
		}
	}
</script>
</body>
</html>