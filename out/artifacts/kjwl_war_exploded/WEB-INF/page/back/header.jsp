<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@	taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
    <title>跨境物流-后台管理</title>
    <link rel="stylesheet" href="${backCssFile}/pintuer.css">
    <link rel="stylesheet" href="${backCssFile}/admin.css">
    <link rel="stylesheet" href="${backJsFile}/layer/skin/layer.css">
    <link rel="stylesheet" href="${backCssFile}/uploadify.css">
    <script src="${backJsFile}/jquery.js"></script>
    <script src="${backJsFile}/json2.js"></script>
    <script src="${backJsFile}/pintuer.js"></script>
    <script src="${backJsFile}/respond.js"></script>
    <script src="${backJsFile}/admin.js"></script>
    <script src="${backJsFile}/layer/layer.js"></script>
    <script src="${backJsFile}/jquery.datagrid-extend.js"></script>
    <script src="${backJsFile}/My97DatePicker/WdatePicker.js"></script>
    <script src="${backJsFile}/ajaxfileupload.js"></script>
    <script src="${backJsFile}/swfobject.js"></script>
    <script src="${backJsFile}/jquery.uploadify.min.js"></script>
    
    <link type="image/x-icon" href="${resource_path}/img/favicon.ico" rel="shortcut icon" />
    <link href="${resource_path}/img/favicon.ico" rel="bookmark icon" /> 
    <!-- ajaxfileupload -->
	<style type="text/css">
	#fileQueue{
    background-color: #fff;
    border-radius: 3px;
    box-shadow: 0 1px 3px;
    height: 150px;
    margin: 10px auto;
    overflow: auto;
    padding: 5px 10px;
    width: 200px;
	}
	</style>
</head> 

<script>
    function isInFrame(){
   	 if( window.top == window.self ){

     }else{
    	 var isUser="${sessionScope.back_user.username}";
    	 if(isUser==''){
    		 window.parent.location.href="${backServer}/login";
    	 }
     }
}
    isInFrame();
</script>

