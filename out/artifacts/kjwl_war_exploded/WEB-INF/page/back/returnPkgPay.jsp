<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:100px;
    }
        .div-front{
        float:left;
        width:300px;
    }
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">包裹详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br/>
        <div class="tab-panel active" id="tab-set">
        <div class="field-group ">
        <input type="hidden" id="package_id" name ="package_id" value="${pkgDetail.package_id}">
        <input type="hidden" id="status1" name ="status1" value="${pkgDetail.status}">
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户名称:</span>
                           <span>${frontUser.user_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">手机号:</span>
                           <span>${frontUser.mobile}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户账户:</span>
                           <span>${frontUser.account}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">包裹创建时间:</span>
                           <span><fmt:formatDate value="${pkgDetail.createTime}" type="both"/><span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">关联单号:</span>
                           <span>${pkgDetail.original_num}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">公司运单号:</span>
                           <span>${pkgDetail.logistics_code}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;">
                           <span class="span-title">包裹地址:</span>
                           <span>${address}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;">
                           <span class="span-title">海外仓库地址:</span>
                           <span>${osaddr}</span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">包裹重量:</span>
                           <span>${pkgDetail.weight}KG</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">退货费用:</span>
                           <span><fmt:formatNumber value="${pkgDetail.return_cost}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">退货单号:</span>
                           <span>${pkgDetail.return_code}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">退货支付状态:</span>
                           <span>
                                 <c:if test="${pkgDetail.retpay_status==1}">未支付</c:if>
                                 <c:if test="${pkgDetail.retpay_status==2}">已支付</c:if>
                           </span>
                      </div>
                 </div>

                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">退货理由:</span>
                           <span>${pkgReturn.reason}</span>
                      </div>
                 </div>
                 
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">审核结果:</span>
                           <span>同意</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">审核者:</span>
                           <span>${operater}</span>
                      </div>
                 </div>

                 <div style="height: 250px;">
                      <div style="float:left;margin-top:5px;"><span><strong>上传照片:</strong></span></div>
                      <div style="float:left;margin-left:40px;width:100px">
                        <shiro:hasPermission name="pkgreturn:upload">
                        <input type="file"  id="pkgPic" name="pkgPic" style="width:65px" onchange="picChange()"/>
                        </shiro:hasPermission>
                      </div>
                      <div style="float:left;margin-left:40px;">
                        <img src="${resource_path}${path}" id="picArea"  name ="picArea" width="320" height="240"/>
                      </div>
                 </div>
         </div>
        <div class="form-button" style="float:left;margin-left:100px;">
        <shiro:hasPermission name="pkgreturn:add">
          <a href="javascript:void(0);" class="button bg-main" onclick="save()">保存</a>
         </shiro:hasPermission>
          <a href="javascript:history.go(-1)" class="button bg-main">取消</a> 
        </div>
             </div>
             </div>
        </div>
        </div>
    </div>
<script type="text/javascript">

    function picChange(){
      var package_id=$('#package_id').val();
      var url = "${backServer}/pkg/picUp";
        $.ajaxFileUpload({
            url : url,
            data:{"package_id":package_id},
            type : 'post',
            secureuri : false,
            fileElementId : 'pkgPic',
            dataType : 'text',
            success : function(data ) {
            	$('#picArea').attr("src","${resource_path}"+data);
            },
            error : function(data, status, e) {
                alert("error" + e);
            }
        });
    }

    function save(){
        var package_id=$('#package_id').val();
        var url = "${backServer}/pkg/savePic";
          $.ajaxFileUpload({
              url : url,
              data:{"package_id":package_id},
              type : 'post',
              secureuri : false,
              fileElementId : 'pkgPic',
              dataType : 'text',
              success : function(data ) {
              alert("保存成功!");
              },
              error : function(data, status, e) {
                  alert("error" + e);
              }
          });
      }

    </script>
</body>
</html>