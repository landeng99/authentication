<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<body>

<div class="admin">
    <div class="panel admin-panel">
    <input type="hidden" id="user_type_bk" value="${params.user_type}">
    <input type="hidden" id="query_day_bk" value="${params.query_day}">
      <div class="panel-head"><strong>前台用户列表</strong></div>
      <div class="tab-body">
        <br />
            <form action="${backServer}/businessModel/search" method="post" id="myform" name="myform">
                 <div style="height: 40px;">
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户类型:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <select id="user_type" name="user_type" class="input" style="width:150px">
                            <option value="">请选择</option>
                            <option value="1">普通用户</option>
                            <option value="2">同行用户</option>
                         </select>
                    </div>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户姓名:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="user_name" name="user_name" style="width:150px"
                                value="${params.user_name}"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px"><span><strong>注册时间:</strong></span></div>
                    <div style="float:left;margin-left:10px;margin-top:6px">
                       <span>从</span>
                    </div>
                    <div style="float:left;">
                        <input type="text" class="input" id="start_time" name="start_time" style="width:200px" 
                        onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'end_time\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.start_time}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                    <div style="float:left;margin-left:10px;margin-top:6px">
                       <span>到</span>
                    </div>
                    <div style="float:left;">
                       <input type="text" class="input" id="end_time" name="end_time" style="width:200px" 
                       onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'start_time\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.end_time}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                 </div>
                 
               <div style="height: 40px;">
                 
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户账号:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="account" name="account" style="width:150px"
                                value="${params.account}"/>
                    </div>

                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>手机号码:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="mobile" name="mobile" style="width:150px"
                                value="${params.mobile}"/>
                    </div>
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>未走货客户:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <select id="query_day" name="query_day" class="input" style="width:150px">
                            <option value="">请选择</option>
                            <option value="15">半个月</option>
                            <option value="30">1个月</option>
                            <option value="90">3个月</option>
                            <option value="180">半年</option>
                            <option value="365">1年</option>
                         </select>
                    </div>                    
                 </div>
                 
               <div style="height: 40px;">
                 
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>LAST NAME:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="last_name" name="last_name" style="width:150px"
                                value="${params.last_name}"/>
                    </div>
                
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>业务名:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                        	<select  class="input" id="business_name" name="business_name" style="width:200px" >
                   		  <option value="">---------选择业务员---------</option>
                    		<c:forEach var="businessName" items="${businessNameList}">
                    			<option value="${businessName.business_name}">${businessName.business_name}</option>
                    		</c:forEach>
                   		</select>
                    </div>
               </div>
            </form>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" onclick="search()" value="查询" />
                    <input type="button" class="button bg-main" onclick="totalCount()" value="总统计" />
                 </div>
                 <table class="table">
                    <tr>
                        <th width="15%">用户名</th>
                        <th width="15%">账号</th>
                        <th width="10%">LAST NAME</th>
                        <th width="15%">手机号码<th>
                        <th width="10%">业务名</th>
                        <!--  <th width="10%">包裹数</th>
                          <th width="10%">总重量</th> -->
                        <th width="15%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="frontUser"  items="${businessModelList}">
                     <tr>
                         <td width="15%">${frontUser.user_name}</td>
                         <td width="15%">${frontUser.account}</td>
                         <td width="10%">${frontUser.last_name}</td>
                         <td width="15">${frontUser.mobile}</td>
                         <td width="10%">${frontUser.business_name}</td>
                         <%-- <td width="10%">${frontUser.package_count}</td>
                         <td width="10%">${frontUser.actual_weight_count_str}</td> --%>
                         <td width="15%">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="personCount('${frontUser.user_id}')">统计</a>
                         
                         </td>
                         </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
<%--         合计：<br/>
        总客户数：${businessModelTotalInfo.total_client }<br/>
        包裹数：${businessModelTotalInfo.total_package_count } 个<br/>
        总重量：${businessModelTotalInfo.total_actual_weight_count_str } 磅   <br/>    --%>      
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
             //选择框选中
            $("#user_type").val($("#user_type_bk").val());
            $("#query_day").val($("#query_day_bk").val());
            $('#business_name').val('${params.business_name}');
            $('#user_type').focus();
        });
        
        function personCount(user_id){
            var url = "${backServer}/businessModel/personDetailCount?user_id="+user_id;
            window.location.href = url;
        }
        
        function totalCount(){
        	var business_name=$('#business_name').val();
            var url = "${backServer}/businessModel/totalCount?business_name="+encodeURI(business_name);
            window.location.href = url;
        }
        
        function ban(user_id,status){
            var url = "${backServer}/frontUser/frontUserStatus";

            $.ajax({
                url:url,
                data:{"user_id":user_id,
                    "status":status},
                type:'post',
                dataType:'text',
                async:false,
                success:function(data){
                   alert("操作成功！");
                   window.location.href = "${backServer}/frontUser/frontUserInit";
                }
            });
        }
        function search(){
            var fromDate=$('#fromDate').val();
            var toDate =$('#toDate').val();
            if(fromDate!=null && fromDate!=""&&
               toDate!=null && toDate!=""&&
               fromDate>toDate){
                    alert("日期起点必须小于日期终点!"); 
                    return ;
                 }
            document.getElementById('myform').submit();
        }
        
        function add(){
            var url = "${backServer}/frontUser/frontUserAdd";
            window.location.href = url;
        }
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search();
               }
          }
        });
    </script>
</body>
</html>