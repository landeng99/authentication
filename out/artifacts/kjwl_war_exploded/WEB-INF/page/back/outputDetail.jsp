<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

 <style type="text/css">
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 80px;
	}
	
	.field-group .field-item .interval_label{
		float: left;
	}
	.field-group .field-item .field{
		float:left;
		width: 203px;
	}
	
	.pickorder-list{
		margin-top: 20px;
	}
</style>

<body>
	<div class="admin">
	<div class="admin_context">
		    <div class="field-group">
				<div class="panel-head"><strong>出库编辑</strong></div>
				<table style="margin-left: 10px;">
					<tr>
						<td>编号：</td>
						<td>${output.output_sn}
						</td>
					</tr>
					<tr>
						<td>创建日期：</td>
						<td><fmt:formatDate value="${create_time}" type="both"/></td>
					</tr>
					<tr>
						<td>已选出库口岸：</td>
						<td>${seaportDisplay}</td>
					</tr>
					<tr>
						<td><input type="button" value="添加口岸" onclick="addSeaport()"></td>
					</tr>
					<tr>
						<td>自动打印页面：</td>
						<td><input type="checkbox"  id="set_auto_print" <c:if test="${output.auto_print==1}">checked="checked"</c:if>> </td>
					</tr>																				
				</table>
				<div style="float: right;margin-right: 250px;">
	  	      	<input type="button"  onclick="showScanLog()" value="扫描日志" /><br>
	  	      	<input type="button"  onclick="batchExport()" value="下载日志" /><br>	
				<input type="button" value="扫描出库" onclick="scanOutput()">
				</div>
             </div>
             <div style="clear: both"></div>
             <form method="post">

	         <div class="panel admin-panel pickorder-list">
	         	     <div class="panel-head"><strong>出库列表</strong></div>
	         	     <table class="table table-hover" id="pickorderlist">
	         	     	<tr>
	         	     		<th >提单号</th>
	         	     		<th >空运单号</th>
	         	     		<th >口岸</th>
	         	     		<th >仓库</th>
	         	     		<th >托盘数</th>
	         	     		<th >包裹数</th>
	         	     		<th >状态</th>
	         	     		<th >创建时间</th>
	         	     		<th >操作</th>
	         	     	</tr>
	         	     	<c:forEach var="pickorder"  items="${pickOrderList}">
	         	     	<tr>
		         	     	<td>${pickorder.pick_code}</td>
		         	     	<td>${pickorder.flight_num}</td>
		         	     	<td>${pickorder.sname}</td>
		         	     	<td>${pickorder.warehouse}</td>
		         	     	<td>${pickorder.palletCnt}</td>
		         	     	<td>${pickorder.pkgCnt}</td>
		         	     	<td>
		         	     		<c:if test="${pickorder.status==1}">已出库</c:if>
		         	     		<c:if test="${pickorder.status==2}">空运中</c:if>
		         	     		<c:if test="${pickorder.status==3}">待清关</c:if>
		         	     	</td>
		         	     	<td><fmt:formatDate value="${pickorder.create_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		         	     	<td>
			         	     	<a href="${backServer}/pickorder/initedit?pick_id=${pickorder.pick_id}" class="button button-small border-green">编辑</a>&nbsp; 
			         	     	<a href="${backServer}/pickorder/detail?pick_id=${pickorder.pick_id}"  class="button button-small border-green">详情</a>&nbsp; 
			         	     <a href="javascript:;" onclick="exportPickorder(${pickorder.pick_id},'${pickorder.pick_code}')" class="button button-small border-green">导出包裹</a>
			         	    <a href="javascript:;" onclick="exportIdcard(${pickorder.pick_id},'${pickorder.pick_code}')"  class="button button-small border-green">导出身份证</a>
			         	     	<c:if test="${pickorder.status==1 and pickorder.pkgCnt==0}">
			         	     		<a href="javascript:;"  onclick="del(${pickorder.pick_id})" class="button button-small border-yellow">删除</a>&nbsp; 
			         	     	</c:if>
			         	     
		         	     	</td>
	         	     	</tr>
	         	     	</c:forEach>
	         	     </table>
		        	<div class="panel-foot text-center">
	      				<jsp:include page="pager.jsp"></jsp:include>
	      			</div>	
	         </div>	
	         </form>
	         </div>
	</div>
	
	<!-- 添加口岸-->
<div id="addSeaportDialog" style="display: none;">
     <form id="importDeclarationForm" method="post" action="${backServer}/output/addSeaport">
            <input type="hidden" value="${output.seq_id}" id="seq_id" name="seq_id">
            <input type="hidden" value="${output.output_sn}" id="output_sn" name="output_sn">
            <input type="hidden" name="seaportListStr" id="seaportListStr" />
            <div style="margin-left: 100px;">
						<table>
									<c:forEach var="seaport" items="${seaportList}">
										<tr>
											<td style="text-align: right;">${seaport.sname}：</td>
											<td><input name="seaport_id" type="checkbox" value="${seaport.sid}"></td>
										</tr>
									</c:forEach>
						</table>
				<br>
				<span>
                	<button type="button" class="button bg-main" id="finish">添加</button>
             	</span>
             </div>
      </form>
</div>
	<script type="text/javascript">
		$(function(){
			$('#add').click(function(){
				window.location.href="${backServer}/pickorder/initadd";
			});				
		});	
 
		$('#delbatch').click(function(){
			
			var checkboxs=$('#pickorderlist').find('input:checked');
			
			var array=[];
			$.each(checkboxs,function(i,checkbox){
				var pick_id=$(checkbox).val();
				array.push(pick_id);
			});
			
			if(array.length==0){
				
				alert("请选择，再删除");
			}else{
				window.location.href="${backServer}/pickorder/delpickorderbatch?pick_ids="+array.join(',');
			}
			 
		});
		function del(pick_id){
			
			if(window.confirm("确定删除吗?")){
				var output_sn=$('#output_sn').val().trim();
				var seq_id=$('#seq_id').val();
	            var url = "${backServer}/pickorder/delPickOrderAjax"
	                $.ajax({
	                 url:url,
	                 data:{
	                     "pick_id":pick_id,
	                 	"seq_id":seq_id    
	                 },
	                 type:'post',
	                 dataType:'text',
	                 success:function(data){
	                     var url = "${backServer}/output/outputDetail?output_sn="+output_sn;
	                     window.location.href = url;
	                 }
	                });
			}
		}
		
		function exportIdcard(pick_id,pick_code){
			window.open('${backServer}/pickorder/exportIdcard?pick_id='+pick_id+"&pick_code="+pick_code);
		}
		
		function exportPickorder(pick_id,pick_code){
			layer.open({
			    type: 2,
			    title: '导出列表',
			    shift: 2,
			    shadeClose: true,  
			    area: ['420px', '240px'], 
			    content: '${backServer}/pickorder/initexport?pick_id='+pick_id+"&pick_code="+pick_code
			});
		}
		
        $("#set_auto_print").click(function () {//自动打印设置
        	var autoPrint=0;
        	if($('#set_auto_print').get(0).checked)
        	{
        		autoPrint=1;
        	}
        	var sid=$("#seq_id").val();
            var url = "${backServer}/output/setAutoPrint"
                $.ajax({
                 url:url,
                 data:{
                     "seqId":sid,
                     "autoPrint":autoPrint},
                 type:'post',
                 dataType:'json',
                 success:function(data){
                    alert(data.message);
                    window.location.href = "${backServer}/output/queryAll";
                 }
                });
        });
        
        function addSeaport()
        {
        	layer.open({
                title :'添加口岸',
                type: 1,
                shadeClose: true,
                shade: 0.8,
                area: ['300px', '500px'],
                content:$('#addSeaportDialog'),
                end:function(){
                }
            });
        }
		$('#finish').click(function() {
			var seaport_id = '';
			$("input[name='seaport_id']:checked").each(
			function()
			{
				seaport_id+=this.value+",";
			}
			);
			if (seaport_id == '') {
				alert('请选择口岸');
			} else {
				$("#seaportListStr").val(seaport_id);
				$('#importDeclarationForm').submit();
			}
		});
		
		function scanOutput()
		{
			var sid=$("#seq_id").val();
			window.location.href = "${backServer}/pallet/initscanforaddpallet_new?output_id="+sid;
		}
		
		function showScanLog()
		{
			var sid=$("#seq_id").val();
		    layer.open({
		        title :'扫描日志',
		        type:2,
		        shadeClose: true,
		        shade: 0.8,
		        offset: ['10px', '100px'],
		        area: ['900px', '800px'],
		        content:'${backServer}/outputScanLog/showAllScanLog?pick_id=out'+sid
		    }); 
		}
		function batchExport() {
			var sid=$("#seq_id").val();
			window.location.href = "${backServer}/outputScanLog/exportOutputScanLog?pick_id=out"+ sid;
		}
	</script>
</body>
</html>