<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>

<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>会员等级修改</strong>
			</div>
			<div class="tab-body">
				<br />
				 <input type="hidden" id="rate_id" value="${memberRate.rate_id}">
				 <input type="hidden" id="rate_name_bk" value="${memberRate.rate_name}">
				 <input type="hidden" id="integral_min_bk" value="${memberRate.integral_min}">
				 <input type="hidden" id="rate_type" value="${memberRate.rate_type}">
				 <input type="hidden" id="checkRateNameResult" value="1">
				 <input type="hidden" id="checkRateIntrgralMinResult" value="1">
				<div style="height: 40px;">
					<div style="float: left; margin-left: 10px" class="label">
						<span><strong>会员类型:</strong>
						</span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<c:if test="${memberRate.rate_type==1}">普通用户</c:if>
						<c:if test="${memberRate.rate_type==2}">同行用户</c:if>
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>等级名称:</strong>
						</span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<input type="text" class="input" id="rate_name"
							style="width: 100px" onblur="checkRateName()"
							value="${memberRate.rate_name}" />
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px;">
						<span id="rateNameTextArea"></span>
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>积分下限:</strong>
						</span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<input type="text" class="input" id="integral_min"
							style="width: 100px" onblur="checkRateIntrgralMin()" 
							onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"
							value="${memberRate.integral_min}" 
							<c:if test="${memberRate.integral_min == 0}">
							readonly="readonly"
							</c:if>
							/>
					</div>
					<c:if test="${memberRate.integral_min == 0}">
					<div style="float: left; margin-top: 5px; margin-left: 10px;">
						<span style="color: red;">基础会员默认积分</span>
					</div>
					</c:if>
					<div style="float: left; margin-top: 5px; margin-left: 10px;">
						<span id="integralMinTextArea"></span>
					</div>
				</div>
				<div class="padding border-bottom">
					<input type="button" class="button bg-main" onclick="add()"
						value="保存" /> <input type="button" class="button bg-main"
						onclick="javascript:history.go(-1)" value="取消" />
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">

		//会员等级积分文本框鼠标失去焦点，校验会员等级是否已经存在
		function checkRateIntrgralMin() {
			$('#integralMinTextArea').empty();
			var url = "${backServer}/memberRate/checkRateIntrgralMin";
			var integral_min = $('#integral_min').val();
            var integral_min_bk = $('#integral_min_bk').val();
            var rate_type = $('#rate_type').val();

            if (integral_min == null || integral_min.trim() == ""){
                return;
            }
            if (integral_min == integral_min_bk) {
                $('#checkRateIntrgralMinResult').val("1");
                return;
            }
			$.ajax({
				url : url,
				type : 'post',
				data : {
					"integral_min" : integral_min,
					"rate_type" : rate_type
				},
				async : false,
				success : function(data) {
					$('#checkRateIntrgralMinResult').val(data);
					if (data == "0") {
						$('#integralMinTextArea').text('等级积分已经存在!');
						$('#integralMinTextArea').css('color', 'red');
					}
				}

			});
		}
		//会员等级名称文本框鼠标失去焦点，校验会员等级是否已经存在
		function checkRateName() {

			$('#rateNameTextArea').empty();
			var url = "${backServer}/memberRate/checkRateName";
			var rate_name = $('#rate_name').val();
			var rate_name_bk = $('#rate_name_bk').val();
			var rate_type = $('#rate_type').val();

			if (rate_name == null || rate_name.trim() == ""){
				return;
			}
			if (rate_name == rate_name_bk) {
				$('#checkRateNameResult').val("1");
				return;
			}
			$.ajax({
				url : url,
				type : 'post',
				data : {
					"rate_name" : rate_name,
					"rate_type" : rate_type
				},
				async : false,
				success : function(data) {
					$('#checkRateNameResult').val(data);
					if (data == "0") {
						$('#rateNameTextArea').text('会员等级已经存在!');
						$('#rateNameTextArea').css('color', 'red');
					}
				}

			});
		}

		function add() {

			$('#rateNameTextArea').empty();
			$('#integralMinTextArea').empty();

			var url = "${backServer}/memberRate/update";

			var checkRateNameResult = $('#checkRateNameResult').val();
			var rate_id = $('#rate_id').val();
			var rate_name = $('#rate_name').val();
			var integral_min = $('#integral_min').val();

			var checkResult = "0"

			// 数字检验（非负整数）
			var exp = /^(0|[1-9]\d*)$/;

			if (rate_name == null || rate_name.trim() == "") {
				$('#rateNameTextArea').text('等级名称不能为空!');
				$('#rateNameTextArea').css('color', 'red');
				checkResult = "1";
			}

			if (integral_min == null || integral_min.trim() == "") {
				$('#integralMinTextArea').text('积分下限不能为空!');
				$('#integralMinTextArea').css('color', 'red');
				checkResult = "1";
			} else if (!exp.test(integral_min)) {
				$('#integralMinTextArea').text('积分下限格式错误！');
				$('#integralMinTextArea').css('color', 'red');
				checkResult = "1";
			}

			if (checkResult == "1") {
				return;
			}

			if ($('#checkRateNameResult').val() == "0") {
				alert("会员等级名称已经存在!");
				return;
			}
			if ($('#checkRateIntrgralMinResult').val() == "0") {
				alert("会员等级积分已经存在!");
				return;
			}
			$
					.ajax({
						url : url,
						data : {
							"rate_id" : rate_id,
							"rate_name" : rate_name,
							"integral_min" : integral_min
						},
						type : 'post',
						async : false,
						dataType : 'text',
						success : function(data) {
							if(data == "1"){
								alert("添加成功");
								window.location.href = "${backServer}/memberRate/memberRateList";
							}else{
								alert("基础会员默认积分");
								$('#integral_min').val(0);
							}
						}
					});
		}
	</script>
</body>
</html>