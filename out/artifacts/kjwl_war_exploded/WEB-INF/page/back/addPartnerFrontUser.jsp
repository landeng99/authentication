<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>同行用户添加</strong></div>
      <div class="tab-body">
        <br/>
                 <input type="hidden" id="checkAccountResult" value="0">
                 <input type="hidden" id="checkUserNameResult" value="0">
                 <input type="hidden" id="checkMobileResult" value="0">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户账户:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="account" style="width:200px"
                              onblur="checkAccount()"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="accountTextArea"></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="user_name" style="width:200px" onkeyup="value=value.replace(/[^\u4E00-\u9FA5^a-z^A-Z^0-9]/g,'') "
                              onblur="checkUserName()"  onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\u4E00-\u9FA5^a-z^A-Z^0-9]/g,''))"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="userNameTextArea"></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>手机号码:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="mobile" style="width:200px" onkeyup="value=value.replace(/[\u3000-\uFFA0]/g,'')"   onblur="checkMobile()"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="mobileTextArea"></span>
                    </div>
                 </div>
                 
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>业务名:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="business_name" style="width:200px" />
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="mobileTextArea"></span>
                    </div>
                 </div>
                                  
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户密码:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="password" class="input" id="user_pwd" style="width:200px"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="passwordTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>确认密码:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="password" class="input" id="surepassword" style="width:200px"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="surepasswordTextArea"></span>
                    </div>
                 </div>

                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>快件渠道:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="hidden"  id="select_express_channel" value="0"/>
                       <input type="checkbox"  id="select_express_channel_checkbox" onclick="express_channel_function()"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="surepasswordTextArea"></span>
                    </div>
                 </div>
                 
                  <div class="padding border-bottom" >
                    <input type="button" class="button bg-main" onclick="add()" value="添加" />
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
                 </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
    $(function(){
        $('#account').focus();
    });
    function express_channel_function()
    {
    	var select_express_channel_val=0;
    	if($("#select_express_channel_checkbox:checked").length==1)
    	{
    		select_express_channel_val=1;
    	}
    	$('#select_express_channel').val(select_express_channel_val);
    
    }
    //用户账户文本框鼠标失去焦点，校验用户账户是否已经存在
    function checkAccount(){

        var url = "${backServer}/frontUser/checkPartnerAccount";
        var account =$('#account').val();

        if(account==null||account.trim()==""){
            return;
        }

        if(!account.match(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/)){
            $('#accountTextArea').text('邮箱格式有误!');
            $('#accountTextArea').css('color','red');
            $('#checkAccountResult').val(2);
            return;
        }
        
        $.ajax({
            url:url,
            type:'post',
            data:{"account":account},
            success:function(data){
                $('#checkAccountResult').val(data);
                if(data=="1"){
                    
                    $('#accountTextArea').text('该用户账户可以使用!');
                    $('#accountTextArea').css('color','green');
                }else{
                    $('#accountTextArea').text('该用户账户已经存在!');
                    $('#accountTextArea').css('color','red');
                }
            }
            
        });
    }
    
    
    
    //手机号码文本框鼠标失去焦点，校验用户账户是否已经存在
    function checkMobile(){

        var url = "${backServer}/frontUser/checkPartnerMobile";
        var mobile =$('#mobile').val();

        if(mobile==null||mobile.trim()==""){
            return;
        }
/*
         if(!mobile.match(/^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/)){
            
            $('#mobileTextArea').text('手机号码格式错误 !');
            $('#mobileTextArea').css('color','red');
            $('#checkMobileResult').val(2);
            return;
        } 
        */
        $.ajax({
            url:url,
            type:'post',
            data:{"mobile":mobile},
            success:function(data){
                $('#checkMobileResult').val(data);
                if(data=="1"){
                    
                    $('#mobileTextArea').text('该手机号码可以使用!');
                    $('#mobileTextArea').css('color','green');
                }else{
                    $('#mobileTextArea').text('该手机号码已经存在!');
                    $('#mobileTextArea').css('color','red');
                }
            }
            
        });
    }
    
    
    //用户名称文本框鼠标失去焦点，校验用户名称是否已经存在
    function checkUserName(){

        var url = "${backServer}/frontUser/checkPartnerUserName";
        var user_name =$('#user_name').val();

        if(user_name==null||user_name.trim()==""){
            return;
        }

        $.ajax({
            url:url,
            type:'post',
            data:{"user_name":user_name},
            success:function(data){
                $('#checkUserNameResult').val(data);
                if(data=="1"){
                    
                    $('#userNameTextArea').text('该用户名称可以使用!');
                    $('#userNameTextArea').css('color','green');
                }else{
                    $('#userNameTextArea').text('该用户名称已经存在!');
                    $('#userNameTextArea').css('color','red');
                }
            }
            
        });
    }

    function add(){
        
        $('#accountTextArea').empty();
        $('#userNameTextArea').empty();
        $('#mobileTextArea').empty();
        $('#passwordTextArea').empty();
        $('#surepasswordTextArea').empty();
        var url = "${backServer}/frontUser/partnerFrontUserInsert";

        var account =$('#account').val();
        var user_name =$('#user_name').val();
        var mobile =$('#mobile').val();
        var user_pwd =$('#user_pwd').val();
        var surepassword =$('#surepassword').val();
        var checkResult ="0";
        var business_name=$('#business_name').val();
        if(account==null||$.trim(account)==""){
            $('#accountTextArea').text('用户账户不能为空!');
            $('#accountTextArea').css('color','red');
            checkResult ="1";
        }
        if(mobile==null||$.trim(mobile)==""){
            $('#mobileTextArea').text('手机号码不能为空!');
            $('#mobileTextArea').css('color','red');
            checkResult ="1";
        }
        
        
        if(user_name==null||$.trim(user_name)==""){
            $('#userNameTextArea').text('用户名称不能为空!');
            $('#userNameTextArea').css('color','red');
            checkResult ="1";
        }

        if(user_pwd==null||$.trim(user_pwd)==""){
            $('#passwordTextArea').text('用户密码不能为空!');
            $('#passwordTextArea').css('color','red');
            checkResult ="1";
        }
        
        var bValidate = RegExp(/^((?=.*\d)(?=.*\D)|(?=.*[a-zA-Z])(?=.*[^a-zA-Z]))^.{8,16}$/).test(user_pwd);
        
        if(!bValidate){
        	
           $('#passwordTextArea').text('长度8~16位，数字、字母、符号至少包含两种!');
           $('#passwordTextArea').css('color','red');
           
           checkResult ="1";
        }
        
        if(checkResult =="1"){
           return;
        }
        
        var checkAccountResult =$('#checkAccountResult').val();
        var checkUserNameResult =$('#checkUserNameResult').val();
        var checkMobileResult =$('#checkMobileResult').val();
        if(checkAccountResult=="0"){
            alert("该用账户已经存在!");
            return;
        }
        else if(checkAccountResult=="2"){
            alert("邮箱格式有误!");
            return;
        }
        
        if(checkMobileResult=="0"){
            alert("该手机号码已经存在!");
            return;
        }
        else if(checkMobileResult=="2"){
            alert("手机号码格式有误!");
            return;
        }
        
        if(checkUserNameResult=="0"){
            alert("该用户名称已经存在!");
            return;
        }
        
        
        if(user_pwd!=surepassword){
            $('#surepasswordTextArea').text('输入的两次密码不相同!');
            $('#surepasswordTextArea').css('color','red');
            $('#user_pwd').val("");
            $('#surepassword').val("");
            return;
        }
        var select_express_channel=$('#select_express_channel').val();
        $.ajax({
            url:url,
            data:{
                "account":account,
                "user_name":user_name,
                "mobile":mobile,
                "user_pwd":user_pwd,
                "select_express_channel":select_express_channel,
                "business_name":business_name},
            type:'post',
            dataType:'json',
            success:function(data){
                if(data.result){

                  if(confirm("保存成功!是否要继续添加同行用户？")){
                      $('#account').val("");
                      $('#user_name').val("");
                      $('#mobile').val("");
                      $('#user_pwd').val("");
                      $('#surepassword').val("");
                      $('#account').focus();
                   }else{
                     window.location.href = "${backServer}/frontUser/frontUserInit";
                 }
               }else{
                alert("添加失败，请与管理员联系！");
               }
            }
        });
    }
    </script>
</body>
</html>