<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>
<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:100px;
    }
        .div-front{
        float:left;
        width:450px;
    }
        .div-front-List{
        float:left;
    }
        .th-title{
        height:30px;
        vertical-align:middle;
    }
    
        .td-List-left{
        height:30px;
        vertical-align:middle;
        text-align:left;
        padding-left:5px;
    }
        .td-List-right{
        height:30px;
        vertical-align:middle;
        text-align:right;
        padding-right:5px;
        
    }
    
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">包裹编辑</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <div class="tab-panel active" id="tab-set">
        <div class="field-group ">
        <input type="hidden" id="package_id" name ="package_id" value="${pkgDetail.package_id}">
        <input type="hidden" id="user_id" name ="user_id" value="${pkgDetail.user_id}">
        <input type="hidden" id="status_bk" name ="status_bk" value="${pkgDetail.status}">
        <input type="hidden" id="payType" value="${pkgDetail.pay_type}">
        <input type="hidden" id="expressPackage" value="${pkgDetail.express_package}"> 
        <input type="hidden" id="osaddr_id" value="${pkgDetail.osaddr_id}">
        <input type="hidden" id="address_id" value="${pkgDetail.address_id}">
                <div style="height: 50px;">
                      <div class ="div-front">
                           <span class="span-title">客户名称:</span>
                           <span>${frontUser.user_name}</span> 
                      </div>
                      <div class ="div-front">
                           <span class="span-title">手机号:</span>
                           <span>${frontUser.mobile}</input>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">客户账户:</span>
                           <span>${frontUser.account}</span>
                      </div>
                 </div>
                 <div style="height: 50px;">
                      <div class ="div-front">
                           <span class="span-title">包裹创建时间:</span>
                           <span><fmt:formatDate value="${pkgDetail.createTime}" pattern="yyyy-MM-dd HH:mm"/><span>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">关联单号:</span>
                        <span> 
                        <input type="hidden" id="old_originalNum" name="old_originalNum" value = "${pkgDetail.original_num}" />
                        <input type="text"  id="new_originalNum" name="new_originalNum"  value = "${pkgDetail.original_num}" <c:if test="${pkgDetail.original_num==null}">disabled="disabled"</c:if>/>
                       </span> 
                      </div>
                      <div class ="div-front">
                      	<label class="span-title" for="status"><strong>包裹状态:</strong></label>
                        <select id="status" name="status" style="height: 33px;width: 110px;float: right;margin-right: 235px;margin-top: -7px;" class="input">
                            <option value=-1>异常</option>
                            <option value=0>待入库</option>
                            <option value=1>已入库</option>
                            <option value=2>待发货</option>
                            <option value=3>已出库</option>
                            <option value=4>空运中</option>
                            <option value=5>待清关</option>
                            <option value=7>已清关派件中</option>
                            <option value=9>已收货</option>
                            <option value=20>废弃</option>
                            <option value=21>退货</option>
                        </select>
                      </div>
                 </div>
                 <div style="height: 50px;">
                      <div class ="div-front">
                            <span class="span-title">派送单号:</span>
                            <span>${pkgDetail.ems_code}</span>
                      </div>
                      <div class ="div-front">
                           <label class="span-title" for="receiver">收件人:</label>
                           <input type="text" id="receiver" value="${userAddress.receiver}" placeholder="请输入..." style="height: 33px;width: 110px;padding: 5px;float: right;margin-right: 235px;margin-top: -7px;" class="input">
                      </div>
                      <div class ="div-front">
                           <span class="span-title">公司单号:</span>
                           <span>${pkgDetail.logistics_code}</span>
                      </div>
                 </div>
                 <div style="height: 60px;">
                      <div class ="div-front">
                           <label class="span-title" for="idcard">收件人身份证:</label>
                           <input type="text" id="idcard" value="${userAddress.idcard}" placeholder="请输入..." style="height: 33px;width: 170px;float: right;margin-right: 180px;margin-top: -7px;" class="input">
                      </div>
                      <div class ="div-front">
                           <label class="span-title" for="mobile">收件人电话:</label>
                           <input type="text" id="mobile" value="${userAddress.mobile}" placeholder="请输入..." style="height: 33px;width: 110px;float: right;margin-right: 235px;margin-top: -5px;" class="input">
                      </div>
<!--                       <div style="float:left;"> -->
<!--                            <span class="span-title">包裹地址---:</span> -->
<%--                            <span>${address}</span> --%>
<!--                       </div> -->
					   <div class ="div-front">
						    <label class="span-title" for="mobile">包裹地址:</label>
						    <input type="text" id="addrInput" value="${address}" placeholder="请输入..." style="height: 33px;width: 110px;float: right;margin-right: 235px;margin-top: -5px;" class="input">
					   </div>
                 </div> 
                 <div style="height: 50px;">
                      <div class ="div-front">
                           <label class="span-title" for="osaddr_id">海外仓库地址:</label>
                           <select id="osaddr" style="height: 33px;width: 110px;float: right;margin-right: 235px;margin-top: -7px;" class="input">
                     			<c:forEach var="sea" items="${seasAddressList}">
	                     			<option value="${sea.id}">${sea.warehouse}</option>
 -->                     		</c:forEach>
                           </select>
                      </div>
                      <div class ="div-front">
                           <label class="span-title" for="express_package">所属渠道:</label>
                           <select id="express_package" style="height: 33px;width: 110px;float: right;margin-right: 235px;margin-top: -7px;" class="input">
                           		<option value="0">默认</option>
                           		<option value="1">A渠道</option>
                           		<option value="2">B渠道</option>
                           		<!-- <option value="3">C渠道</option>
                           		<option value="4">D渠道</option>
                           		<option value="5">E渠道</option> -->
                           </select>
                      </div>
                      <div class ="div-front">
                           <label class="span-title" for="seaport">所属口岸:</label>
 						   <span>${seaport.sname}</span> 
 					  </div>
                 </div>
                <div style="height: 60px;margin-top:20px">
                      <div class="div-front-List"><span class="span-title" style="margin-top:20px">商品:</span></div>
                      <span>
                           <table border="1" cellpadding="0" cellspacing="0" id="goodsTable">
                             <tr>
                               <th width ="300" class="th-title">商品名称</th>
                               <th width ="100" class="th-title">品牌</th>
                               <th width ="100" class="th-title">商品申报类别</th>
                               <th width ="100" class="th-title">单价</th>
                               <th width ="50" class="th-title">数量</th>
                               <th width ="100" class="th-title">规格</th>
                               <th width ="100" class="th-title">关税费用</th>
                             </tr>
<%--                              <c:forEach var="good"  items="${pkgGoodsList}"> --%>
<!--                                 <tr> -->
<%--                                   <td width ="300" class="td-List-left">${good.goods_name}</td> --%>
<%--                                   <td width ="100" class="td-List-left">${good.brand}</td> --%>
<%--                                   <td width ="100" class="td-List-left">${good.name_specs}</td> --%>
<%--                                   <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.price}" type="currency" pattern="$#0.00#"/></td> --%>
<%--                                   <td width ="50" class="td-List-right">${good.quantity}</td> --%>
<%--                                   <td width ="100" class="td-List-left">${good.spec}</td> --%>
<%--                                   <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td> --%>
<!--                                 </tr> -->
<%--                              </c:forEach> --%>
								<c:forEach var="good"  items="${pkgGoodsList}">
                                <tr>
                                  <td width ="300" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.goods_name}"/>
                                  	</div>
                                  </td>
                                  <td width ="100" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.brand}"/>
                                  	</div>
                                  </td>
                                  <td width ="100" class="td-List-left">${good.name_specs}</td>
<%--                                   <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.price}" type="currency" pattern="$#0.00#"/></td> --%>
								  <td width ="100" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.price}"/>
                                  	</div>
                                  </td>
<%--                                   <td width ="50" class="td-List-right">${good.quantity}</td> --%>
								  <td width ="100" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.quantity}"/>
                                  	</div>
                                  </td>
                                  <td width ="100" class="td-List-left">
                                  	<div class="field">
                                  		<input type="text" class="input" value="${good.spec}"/>
                                  	</div>
                                  </td>
                                  <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                                  <td style="display:none;">${good.goods_id}</td>
                                </tr>
                             </c:forEach>
                           </table>
                      </span>
                 </div> 
				
                 <div style="height: 50px;margin-top:60px">
                      <div style="float:left;">
                      	<label for="weight"><strong>包 裹 体 积:</strong></label>
                      </div>
                      <div style="float:left;margin-left:30px;">
                      	<label for="length">长:</label>
                      </div>
                      <div style="float:left;margin-left:10px;margin-top:-8px;">
                        <input type="text" class="input" id="length" name="length" style="width:40px" value = "${pkgDetail.length}" />
                      </div>
                      <div style="float:left;">
                      	<span>CM</span>
                      </div>
                      
                      <div style="float:left;margin-left:20px;">
                      	<label for="width">宽:</label>
                      </div>
                      <div style="float:left;margin-left:10px;margin-top:-8px;">
                        <input type="text" class="input" id="width" name="width" style="width:40px" value = "${pkgDetail.width}" />
                      </div>
                      <div style="float:left;">
                      	<span>CM</span>
                      </div>
                      
                      <div style="float:left;margin-left:20px;">
                      	<label for="height">高:</label>
                      </div>
                      <div style="float:left;margin-left:10px;margin-top:-8px;">
                        <input type="text" class="input" id="height" name="height" style="width:40px" value = "${pkgDetail.height}" />
                      </div>
                      <div style="float:left;">
                      	<label>CM</label>
                      </div>
                 </div>

                 <div style="height: 50px;">
                 	 <div class ="div-front">
	                 	 <label class="span-title"><strong>包裹实际重量:</strong></label> 
	                     <span>${pkgDetail.actual_weight}磅</span>
                 	 </div>
                     <div class ="div-front">
                     	<label style="float:left;" for="freight"><strong>运费:</strong></label>
                     	<input type="text" class="input" id="freight" name="freight" style="width:100px;float: right;margin-right: 245px;margin-top: -10px;" value = "${pkgDetail.freight}" pattern="$" />
                     </div>
                	 <div class ="div-front">
	                  	 <span class="span-title"><strong>收费重量:</strong></span> 
	                     <span>${pkgDetail.weight}磅</span>
                 	</div>
                 </div>
                 <div style="height: 50px;">
                      <div class ="div-front">
                           <span class="span-title">申报总价值:</span>
                           <span><fmt:formatNumber value="${total_worth}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">税金:</span>
                           <span><fmt:formatNumber value="${pkgDetail.customs_cost}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">清关审核:</span>
                           <span>${pkgDetail.tax_status}</span>
                      </div>
                 </div>
                 <div style="height: 50px;">
                      <div class ="div-front">
                           <span class="span-title">转运总费用:</span>
                           <span id ="transport_cost"><fmt:formatNumber value="${pkgDetail.transport_cost}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div class ="div-front">
                           <label class="span-title" for="pay_type">支付方式:</label>
                           <select id="pay_type" style="height: 33px;width: 100px;float: right;margin-right: 245px;margin-top: -7px;" class="input">
                           		<option value="0" hidden="true">在线支付</option>
                           		<option value="1">快速出库</option>
                           		<option value="2">在线支付</option>
                           </select>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">退货单号:</span>
                           <span>${pkgDetail.return_code}</span>
                      </div>
                 </div>
                 <div style="height: 50px;">
                      <div class ="div-front">
                           <span class="span-title">退货费用:</span>
                           <span><fmt:formatNumber value="${pkgDetail.return_cost}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">包裹支付状态:</span>
                           <span>
                              <c:if test="${pkgDetail.pay_status==1}">运费未支付</c:if>
                              <c:if test="${pkgDetail.pay_status==2}">运费已支付</c:if>
                              <c:if test="${pkgDetail.pay_status==3}">关税未支付</c:if>
                              <c:if test="${pkgDetail.pay_status==4}">关税已支付</c:if>
                           </span>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">退货支付状态:</span>
                           <span>
                              <c:if test="${pkgDetail.retpay_status==0}">未支付</c:if>
                              <c:if test="${pkgDetail.retpay_status==1}">已支付</c:if>
                           </span>
                      </div>
                 </div>
                 <div style="height: 50px;">
                      <div class ="div-front">
                           <span class="span-title">物流单号:</span>
                           <span>${pkgDetail.express_num}</span>
                      </div>
                      <div class ="div-front">
                           <label class="span-title" for="need_check_out_flag">需要取出:</label>
                           <span>
                              <select id="need_check_out_flag" name="need_check_out_flag" onchange="setNeedCheckOutFlag()" style="height: 33px;width: 100px;float: right;margin-right: 245px;margin-top: -7px;" class="input">
	                              <option value="N" <c:if test="${pkgDetail.need_check_out_flag=='N'}">selected="selected"</c:if>>否</option>
	                              <option value="Y" <c:if test="${pkgDetail.need_check_out_flag=='Y'}">selected="selected"</c:if>>是</option>
                              </select>
                           </span>
                      </div>
                      <div class ="div-front">
                           <span class="span-title">报关单号:</span>
                           <span>${pkgDetail.declaration_code}</span>
                      </div>
                 </div>
         </div>
                 <div class="form-button">
                    <a href="javascript:void(0);" class="button bg-main" onclick="save()" >保存</a>
                    <a href="javascript:history.go(-1)" class="button bg-main">取消</a> 
                </div>
             </div>
             </div>
        </div>
    <div style="width: 800px; height: 300px; position: absolute; z-index: 100;left: -300px; top: -300px">
           <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="socket" width="100%" height="100%"
                codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
                <param name="movie" value="${backJsFile}/socket.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#869ca7" />
                <param name="allowScriptAccess" value="sameDomain" />
                <embed src="${backJsFile}/socket.swf" quality="high" bgcolor="#869ca7"
                    width="100%" height="100%" name="socket" align="middle"
                    play="true"
                    loop="false"
                    quality="high"
                    allowScriptAccess="sameDomain"
                    type="application/x-shockwave-flash"
                    pluginspage="http://www.adobe.com/go/getflashplayer">
                </embed>
        </object>      
   </div>
</div>
<script type="text/javascript">

//默认下拉列表选择框选中
$('#status').val($('#status_bk').val());
$("#pay_type").val($("#payType").val());
$("#express_package").val($("#expressPackage").val());
$("#osaddr").val($("#osaddr_id").val());
//
$('#status').change(function(){

    var url = "${backServer}/pkg/checkPackageInPallet";
    var package_id =$('#package_id').val();
    var status =$('#status').val();
     $.ajax({
         url:url,
         data:{"package_id":package_id,"status":status},
         type:'post',
         dataType:'json',
         async:false,
         success:function(data){
            if(!data.result){
                $('#status').val($('#status_bk').val());
                layer.alert(data.message);
             }else{
            	 $('#status').val($('#status_bk').val());
                 //layer.alert(data.message);
                 alert(data.message);
                 window.location.href = "${backServer}/pkg/query";
             }
         }
     });
 });

    function save(){
        var url = "${backServer}/pkg/editPackage";
        var package_id=$('#package_id').val();
        var freight =$('#freight').val();
        var status =$('#status').val();
        var length =$('#length').val();
        var width =$('#width').val();
        var height =$('#height').val();
		var pay_type = $("#pay_type").val();
		var express_package = $("#express_package").val();
		var osaddr_id = $("#osaddr").val();
		var receiver = $("#receiver").val();
		var idcard = $("#idcard").val();
		var mobile = $("#mobile").val();
		var address_id = $("#address_id").val();
		var address = $("#addrInput").val();
		
		if( osaddr_id==null ){
			layer.alert("海外仓库地址不能为空");
	        return;
		}
		
		if(address==''){
            layer.alert("地址不能为空");
            return;
        }
		
        if(isNaN(length)||length<0){
            layer.alert("长度必须为大于0数字");
            return;
        }
        if(isNaN(width)||width<0){
            layer.alert("宽度必须为大于0数字");
            return;
        }
            
        if(isNaN(height)||height<0){
            layer.alert("高度必须为大于0数字");
            return;
        }
    
        if(isNaN(freight)||freight<0){
        	layer.alert("运费必须为大于0数字");
            return;
         }
		var old_originalNum=$("#old_originalNum").val();
		var new_originalNum=$("#new_originalNum").val();
		if(old_originalNum!=null&&old_originalNum!='')
		{
			var reg = /^[0-9a-zA-Z]+$/;
			if(new_originalNum==null||new_originalNum==''){
				layer.alert("请输入包裹关联单号");
				return;
			}else if(!reg.test(new_originalNum))
			{
				layer.alert("关联单号只能是数字或英文");
				return;
			}
		}
		
		if(old_originalNum!=new_originalNum)
		{
			var url1 = "${backServer}/pkg/checkOriginalnum";
			var needReturn=false;
			$.ajax({
			   url:url1,
			   type:'post',
			   dataType:'json',
			   async:false,
			   data:{'original_num':new_originalNum,'noCheckSession':'Y'},
			   success:function(data){
					var msg=data['msg'];
					if(msg=="0"){
						layer.alert("关联单号已存在");
						needReturn=true;
					}
			   }
			});
			if(needReturn)
			{
				return;
			}
		}
		
		var jsonGoods = "[";
		var i=0;
		$("#goodsTable").find("tr").each(function(){
		    var tdArr = $(this).children();
		    var tmpName = tdArr.eq(0).find("input").val();
		    var tmpBrand = tdArr.eq(1).find("input").val();
		    var tmpPrice = tdArr.eq(3).find("input").val();
		    var tmpQuantity = tdArr.eq(4).find("input").val();
		    var tmpSpec = tdArr.eq(5).find("input").val();
		    var tmpId = tdArr.eq(7).text();
			if(i>0){// 第一次拿到的是标题，不是列表中的内容
				if( i>1 ){
					jsonGoods += ","; // 用逗号分隔多个 code
				}
				jsonGoods += combineRow(tmpId, tmpName, tmpBrand, tmpSpec, tmpPrice, tmpQuantity);
			}
		    i++;
		  });
		if( 1==i ){
			alert('没有要支付的包裹');
			return ;
		}
		jsonGoods += "]";
		
        $.ajax({
            url:url,
            data:{"package_id":package_id,
                "freight":freight,
                "status":status,
                "length":length,
                "width":width,
                "original_num":new_originalNum,
                "height":height,
                "pay_type":pay_type,
                "express_package":express_package,
                "osaddr_id":osaddr_id,
                "receiver":receiver,
                "idcard":idcard,
                "mobile":mobile,
                "address_id":address_id,
                "address":address,
                "jsonGoods":jsonGoods
                },
            type:'post',
//             dataType:'text',
            async:false,
            success:function(data){
            	if(data.result==true){
		        	alert('保存成功');
// 		        	window.location.href = "${backServer}/pkg/query";
		        }
		        else{
		        	alert('保存失败');
		        }
            }
        });
    }
    
	function combineRow(id, name, brand, spec, price, quantity) {
		return "{" + "\"goods_id\":\"" + id + "\","
				   + "\"goods_name\":\"" + name + "\","
				   + "\"brand\":\"" + brand + "\","
				   + "\"price\":\"" + price + "\","
				   + "\"quantity\":\"" + quantity + "\","
				   + "\"name_specs\":\"" + spec + "\""
			+ "}"
	}
    
    function setNeedCheckOutFlag()
    {
    	var url1 = "${backServer}/pkg/updateNeedCheckOutFlag";
    	var package_id=$('#package_id').val();
    	var need_check_out_flag=$('#need_check_out_flag').val();
        $.ajax({
            url:url1,
            data:{"package_id":package_id,
                "need_check_out_flag":need_check_out_flag},
            type:'post',
            dataType:'text',
            async:false,
            success:function(data){
                alert("成功设置包裹取出标志");
                window.location.href = "${backServer}/pkg/query";
            }
        });
    }
</script>
</body>
</html>