<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
</style>
<body>
<div class="admin">
	  
  <div class="padding border-bottom" id="findDisplayDiv" style="width: 90%; height: 100px;">
      		
      		<div style="float:left;height: 32px; margin-left: 10px;">
            
		            <label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;申请人&nbsp;</label>
		      		<input type="text" class="input"  id="user_name" name="user_name" style="width:200px;margin-left: 10px;float:left;" 
		            	value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
		           	
		           	<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;包裹编号&nbsp;</label>
		      		<input type="text" class="input"  id="logistics_code" name="logistics_code" style="width:200px;margin-left: 10px;float:left;" 
		            	value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
		           	
		           	<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;状态&nbsp;</label>
		           	<select id="approve_status" name="approve_status" class="input" style="width:80px;float:left;margin-left: 10px;">
		           		<option value="-1" selected="selected">请选择</option>
		           		<option value="0">未审批</option>
		           		<option value="1">审批通过</option>
		           		<option value="2">审批拒绝</option>
		           	</select>
      		 </div><div style="float:left;height: 32px; 
      		 	margin-left: 10px; margin-top: 10px;">
	           	
	           
		           	
				<div style="float: left;width: 350px;">
		           	<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;申请时间&nbsp;</label>
		      		<input type="text" class="input" style="width:100px;float:left;margin-left: 10px;"
		      			id="createTimeStart" name="createTimeStart" 
		      			fromDate
		      			onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'createTimeEnd\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
		            	value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
		            <span style="line-height: 30px; float:left; text-align: center; margin-left: 8px;">到</span>
		            <input type="text" class="input"  style="width:100px;float:left;margin-left: 8px"
		            	id="createTimeEnd" name="createTimeEnd" 
		            	onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'createTimeStart\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"  
		            	value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
	            </div>
	           	
	           	<div class="form-button" style="float:left;"><a href="javascript:void(0);" class="button bg-main" onclick="queryAll()">查询</a></div>
            </div>
            
      </div>
  <div class="panel admin-panel">
    	<div class="panel-head" style="height: 50px;">
    		<strong style="float:left;text-align: center;margin-left: 10px;">退货列表</strong>
    	</div>
      <table class="table table-hover">
      		<tr><th>申请人</th>
      		<th >包裹编号</th>
      		<th>状态</th>
      		<th>审批人员</th>
      		<th>申请时间</th>
      		<th>审批时间</th>
      		<th>操作</th></tr>
      		<c:forEach var="list"  items="${repkgLists}">
      		<tr><td>${list.user_name }</td>
      		<td>${list.logistics_code }</td>
      		<td><c:if test="${list.approve_status==0 }">未审批</c:if>
      			<c:if test="${list.approve_status==1 }">审批通过</c:if>
      			<c:if test="${list.approve_status==2 }">审批拒绝</c:if></td>
      		<td>${list.operater_name }</td>
      		<td>${list.create_time }</td>
      		<td>${list.approve_time }</td>
      		<td>
      		<shiro:hasPermission name="repkg:select">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      				onclick="finkRepkg('${list.return_id }')">查看详情</a>&nbsp;&nbsp;
      		</shiro:hasPermission><shiro:hasPermission name="repkg:edit">
      		<c:if test="${list.approve_status!=0 }">
      			<a disabled="disabled" class="button border-blue button-little" href="javascript:void(0);" 
						onclick="finkRepkgOne('${list.return_id  }','refusing')">审核</a>&nbsp;&nbsp;
			</c:if><c:if test="${list.approve_status==0 }">
				<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="finkRepkgOne('${list.return_id  }','refusing')">审核</a>&nbsp;&nbsp;
			</c:if>			
			</shiro:hasPermission><shiro:hasPermission name="repkg:delete">
      			<a class="button border-blue button-little" href="javascript:void(0);" 
      				onclick="del('${list.return_id }')">删除</a>&nbsp;&nbsp;
      		</shiro:hasPermission>
      			</td></tr>
      		</c:forEach>
      </table>
   		<div class="panel-foot text-center">
      		<jsp:include page="webfenye.jsp"></jsp:include>
      	</div>
      </div>
</div>
</body>
<script type="text/javascript">
	function finkRepkg(return_id){
		var url = "${backServer}/repkg/toFinkRepkg?return_id="+return_id;
		window.location.href = url;
	}
	function finkRepkgOne(return_id){
		var url = "${backServer}/repkg/toFinkRepkgOne?return_id="+return_id;
		window.location.href = url;
	}
	function queryAll(){
		var user_name = $('#user_name').val();
		var logistics_code = $('#logistics_code').val();
		var approve_status = $('#approve_status').val();
		var amountStart = $('#amountStart').val();
		if(null == approve_status || "" == approve_status){
			approve_status = -1;
		}
		var createTimeStart = $('#createTimeStart').val();
		var createTimeEnd = $('#createTimeEnd').val();
		var url = "${backServer}/repkg/queryAll?user_name="+user_name+
				"&approve_status="+approve_status+
				"&logistics_code="+logistics_code+
				"&createTimeStart="+createTimeStart+
				"&createTimeEnd="+createTimeEnd;
		window.location.href = url;
	}
</script>
</html>