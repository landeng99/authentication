<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
 <link rel="stylesheet" href="${backCssFile}/jquery.treetable.css">
 <link rel="stylesheet" href="${backCssFile}/jquery.treetable.theme.modified.css">
<script type="text/javascript" src="${backJsFile}/jquery.treetable.js"></script>
 
	
<body>
	<div class="admin">
	    <div class="panel admin-panel">
		  <div class="padding border-bottom">
		  <shiro:hasPermission name="navigation:add">
	      		<a href="${backServer}/navigation/toAddNavigation" class="button button-small border-green" />添加</a>
	      </shiro:hasPermission>
	      </div>
		<table id="pkg_category" class="table table-hover">
		 <thead>
			<tr>
				<th>导航栏名称</th><th>链接</th><th>状态</th><th>操作</th>
			</tr>
		 </thead>
		 
		 <c:forEach var="navigation" items="${navigationLists}">
			<tr data-tt-id="${navigation.oneId}" data-tt-parent-id="${navigation.twoId}">
	 			<td>${navigation.nav_name}</td>
	 			<td>${navigation.url}</td>
	 			<td>${navigation.status==1?'启用':'禁用'}</td>
	 			<td>
	 			<shiro:hasPermission name="navigation:enable">
	 			<c:if test="${navigation.status==2}">
	 			<a class="button border-blue button-little" href="${backServer}/navigation/updateNavigationStatus?nav_id=${navigation.nav_id}">启用</a>
	 			</c:if><c:if test="${navigation.status==1}">
	 			<a class="button border-blue button-little" href="${backServer}/navigation/updateNavigationStatus?nav_id=${navigation.nav_id}">禁用</a>
	 			</c:if>
	 			</shiro:hasPermission>
	 			
	 			<shiro:hasPermission name="navigation:update">
	 			<a class="button border-blue button-little" href="${backServer}/navigation/toUpdateNavigation?nav_id=${navigation.nav_id}">修改</a>
	 			</shiro:hasPermission><shiro:hasPermission name="navigation:delete">
	 			<a class="button border-blue button-little" href="${backServer}/navigation/deleteNavigation?nav_id=${navigation.nav_id}">删除</a>
	 			</shiro:hasPermission>
	 			</td>
		   </tr>
		   </c:forEach> 
		 </table>
		 <div class="panel-foot text-center">
				<jsp:include page="webfenye.jsp"></jsp:include>
			</div>	
		</div>
		
	</div>
<script type="text/javascript">
	$("#pkg_category").treetable({expandable: true,initialState:"expanded" });
</script>
</body>
</html>