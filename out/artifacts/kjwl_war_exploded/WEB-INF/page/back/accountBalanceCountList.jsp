<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
	.div-from{
		float:left;height: 32px; margin-left: 10px;
	}
	
	.div-from div{
		float: left;width: 380px;
	}
	.div-from div label{
		float:left;line-height: 30px;text-align: center;
	}
	.label-input-text{
		width:200px;margin-left: 10px;float:left;
	}
	.label-input-time{
		width:140px;margin-left: 10px;float:left;
	}
	.div-from div span{
		line-height: 30px; float:left; text-align: center; margin-left: 8px;
	}
</style>
<body>
	<div class="admin">
<div class="tab-body">
	<input type="hidden" id="messageHidden" name="messageHidden" value="${MESSAGE_INFO}"></input>
	<div class="padding border-bottom" id="findDisplayDiv" style="width: 90%; height: 100px;">
		<!-- 提供登录人用户id userId 信息 -->
		<div class="div-from">
			<div>
				<label for="name">用户名</label>
				<input type="text" class="input label-input-text" 
					id="user_name" name="user_name" 
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
			</div><div>
				<label for="name">记录时间</label>
				<input type="text" class="input label-input-time"
					id="start_time" name="start_time" 
					onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'end_time\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
				<span style="">到</span>
				<input type="text" class="input label-input-time"
					id="end_time" name="end_time" 
					onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'start_time\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
			</div>
			<div class="form-button"><a href="javascript:void(0);" class="button bg-main" onclick="queryAll()">查询</a></div>
		</div>
	</div>	
</div>
<div class="tab-body">
	<div class="tab-panel active panel admin-panel" id="tab-set">
	
			<div class="panel-head" style="height: 50px;">
				<strong style="float:left;text-align: center;margin-left: 10px;">每日对账单列表</strong>
			</div>
			<table class="table table-hover">
				<tr><th>用户名</th>
				<th>充值金额</th>
				<th>索赔金额</th>
				<th>消费金额</th>
				<th>提现金额</th>
				<th>上期结余</th>
				<th>本期结余</th>
				<th>差额</th>
				<th>本期时间</th>
				<th>操作</th></tr>
			<c:forEach var="accountBalance" items="${accountBalanceLists}">
				<tr><td>${accountBalance.user_id == 0 ? '所有用户':accountBalance.user_id}</td>
				<td><fmt:formatNumber value="${accountBalance.total_recharge}" type="currency" pattern="$#0.00#"/>
				</td>
				<td><fmt:formatNumber value="${accountBalance.total_claim_cost}" type="currency" pattern="$#0.00#"/>
				</td>
				<td><fmt:formatNumber value="${accountBalance.total_expend}" type="currency" pattern="$#0.00#"/>
				</td>
				<td><fmt:formatNumber value="${accountBalance.total_withdraw}" type="currency" pattern="$#0.00#"/>
				</td>
				<td><fmt:formatNumber value="${accountBalance.last_balance}" type="currency" pattern="$#0.00#"/>
				</td>
				<td><fmt:formatNumber value="${accountBalance.curr_balance}" type="currency" pattern="$#0.00#"/>
				</td>
				<td><fmt:formatNumber value="${accountBalance.difference}" type="currency" pattern="$#0.00#"/>
				</td>
				<td>${accountBalance.timeStart}</td>
				<td>
				    
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="finkAccountBalance('${accountBalance.balance_id }','log')">查看详情</a>&nbsp;&nbsp;
				
			 	</td></tr>	
			</c:forEach>
			</table>
			<div class="panel-foot text-center">
				<jsp:include page="webfenye.jsp"></jsp:include>
			</div>
	</div>
</div>
	</div>	

</body>
<script type="text/javascript">
	
	function finkAccountBalance(balance_id){
		var url = "${backServer}/accountBalance/toFinkAccountBalance?balance_id="+balance_id;
		window.location.href = url;
	}
	
	function queryAll(){
		var user_name = $('#user_name').val(); 
		var start_time = $('#start_time').val();
		var end_time = $('#end_time').val();
		var url = "${backServer}/accountBalance/queryAllCount?user_name="+user_name+
				"&start_time="+start_time+
				"&end_time="+end_time;
		window.location.href = url;
	}
	
    //回车事件
    $(function(){
      document.onkeydown = function(e){
          var ev = document.all ? window.event : e;
          if(ev.keyCode==13) {

        	  queryAll();
           }
      }
    });
	
	
</script>
</html>