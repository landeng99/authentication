<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .div-title{
        font-weight: bold;
        float:left;
        width:90px;
        margin-top:5px;
        margin-left:10px;
    }
        .div-input{
        float:left;
        width:220px;
    }
    
    .input-width{
        width:200px;
    }

</style>
<div class="admin">
    <div class="panel admin-panel">
         <input type="hidden" id="status_bk" value="${params.status}">
         <input type="hidden" id="account_type_bk" value="${params.account_type}">
         <input type="hidden" id="rebatesme_type_bk" value="${params.rebatesme_type}">
        <div class="panel-head"><strong>账户记录</strong></div>
        </br>
          <form action="${backServer}/accountLog/search" method="post" id="myform" name="myform">

             <div style="height: 40px;">
                  <div class="div-title">姓名:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="user_name" name="user_name"
                             value="${params.user_name}" onkeyup="value=$.trim(value)"/>
                  </div>
                  <div class="div-title">账户:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="account" name="account"
                             value="${params.account}" onkeyup="value=$.trim(value)"/>
                  </div>
                  <div class="div-title">状态:</div>
                  <div class="div-input">
                        <select id="status" name="status" class="input input-width" >
                            <option value="">请选择</option>
                            <option value=1>申请中</option>
                            <option value=2>成功</option>
                            <option value=3>失败</option>
                            <option value=4>取消</option>
                        </select>
                  </div>
                  
             </div>

             <div style="height: 40px;">
                  <div class="div-title">类型:</div>
                  <div class="div-input">
                      <select id="account_type" name="account_type" class="input input-width" >
                            <option value="">请选择</option>
                            <option value=1>充值</option>
                            <option value=2>消费</option>
                            <option value=3>提现</option>
                            <option value=4>索赔</option>
                        </select>
                  </div>
                  <div class="div-title">起始时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="timeStart" name="timeStart" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'timeEnd\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.timeStart}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
                  <div class="div-title">终止时间:</div>
                  <div class="div-input">
                     <input type="text" class="input" id="timeEnd" name="timeEnd" style="width:200px" 
                     onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'timeStart\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"  
                            value="${params.timeEnd}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                  </div>
             </div>
             
             <div style="height: 40px;">
                  <div class="div-title">订单号:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="order_id" name="order_id"
                             onkeyup="value=$.trim(value)"
                             value="${params.order_id}"/>
                  </div>
                  <div class="div-title">包裹单号:</div>
                  <div class="div-input">
                      <input type="text" class="input input-width" id="logistics_code" name="logistics_code"
                             onkeyup="value=$.trim(value)"
                             value="${params.logistics_code}"/>
                  </div>
                  <div class="div-title">用户类型:</div>
                  <div class="div-input">
                      <select id="user_type" name="user_type" class="input input-width" >
                            <option value="">请选择</option>
                            <option value=1>普通用户</option>
                            <option value=2>同行用户</option>
                        </select>
                  </div>
                  <div class="div-title">是否返利:</div>
                  <div class="div-input">
                      <select id="rebatesme_type" name="rebatesme_type" class="input input-width" >
                            <option value="">请选择</option>
                            <option value=1>返利</option>
                            <option value=2>不返利</option>
                        </select>
                  </div>
             </div>
          </form>
             <div class="padding border-bottom">
                  <input type="button" class="button bg-main" onclick="search()" value="查询" />
                  <input type="button" class="button bg-main" onclick="batchExport()" value="批量导出" />                  
             </div>         
             <table class="table">
                  <tr>
                    <th width="15%">姓名</th>
                    <th width="15%">账户</th>
                    <th width="10%">类型</th>
                    <th width="10%">状态</th>
                    <th width="10%">金额</th>
                    <th width="15%">时间</th>
                    <th width="20%">操作</th>
                  </tr>
             </table>
             <table class="table table-hover">
               <c:forEach var="accountLog"  items="${accountLogList}">
                   <tr> 
                      <td width="15%">${accountLog.real_name}</td>
                      <td width="15%">${accountLog.account}</td>
                      <td width="10%">
                          <c:if test="${accountLog.account_type==1}">充值</c:if>
                          <c:if test="${accountLog.account_type==2}">消费</c:if>
                          <c:if test="${accountLog.account_type==3}">提现</c:if>
                          <c:if test="${accountLog.account_type==4}">索赔</c:if>
                      </td>
                      <td width="10%">
                          <c:if test="${accountLog.status==1}">申请中</c:if>
                          <c:if test="${accountLog.status==2}">成功</c:if>
                          <c:if test="${accountLog.status==3}">失败</c:if>
                          <c:if test="${accountLog.status==4}">取消</c:if>
                      </td>
                      
                      <td width="10%"><fmt:formatNumber value="${accountLog.amount}" type="currency" pattern="$#0.00#"/></td>

                      <td width="15%"><fmt:formatDate value="${accountLog.opr_time}" type="both"/></td>
                      <td width="20%">
                        <a class="button border-blue button-little" href="javascript:void(0);" onclick="detail('${accountLog.log_id}')">详情</a>
                      </td>
                  </tr>
               </c:forEach>
             </table>
                  <div class="panel-foot text-center">
                   <jsp:include page="webfenye.jsp"></jsp:include>
                 </div>
             </div>
</div>
</body>
<script type="text/javascript">

//选择框选中
$("#status").val($("#status_bk").val());
//选择框选中
$("#account_type").val($("#account_type_bk").val());

//选择框选中
$("#rebatesme_type").val($("#rebatesme_type_bk").val());

//初始化焦点
$('#user_name').focus();


function search(){

    document.getElementById('myform').submit();
}

function batchExport(){
	var user_name=$("#user_name").val();
    var account=$("#account").val();
    var status=($('#status').find("option:selected"))[0].value;
    var account_type=($('#account_type').find("option:selected"))[0].value;
    var timeStart=$("#timeStart").val();
    var timeEnd=$("#timeEnd").val();
    var order_id=$("#order_id").val();
    var logistics_code=$("#logistics_code").val();
    var user_type=($('#user_type').find("option:selected"))[0].value;
    var rebatesme_type=($('#rebatesme_type').find("option:selected"))[0].value;
    window.location.href="${backServer}/accountLog/exportList?user_name="+user_name+"&account="+account+"&status="+status+"&account_type="+account_type+"&timeStart="+timeStart+"&timeEnd="+timeEnd+"&order_id="+order_id+"&logistics_code="+logistics_code+"&user_type="+user_type+"&rebatesme_type="+rebatesme_type;
}

function detail(log_id){
    var url = "${backServer}/accountLog/detail?log_id="+log_id;
    window.location.href = url;
}


//回车事件
      $(function(){
        document.onkeydown = function(e){
            var ev = document.all ? window.event : e;
            if(ev.keyCode==13) {

                search();
             }
        }
      });

</script>

</html>