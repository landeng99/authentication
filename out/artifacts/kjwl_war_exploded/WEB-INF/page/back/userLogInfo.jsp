<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">用户操作信息详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="myform" name="myform">
              
              <div style="height: 40px; float: left; width:200px;">
                   <div style="float:left;"><span><strong>用户名称&nbsp;</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${userLogPojo.user_name}</span>
                   </div>
              </div>
              <div style="height: 40px; float: left; width:300px;">
                   <div style="float:left;"><span><strong>处理时间&nbsp;:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${userLogPojo.create_time}</span>
                   </div>
               </div>
              <div style="height: 40px; float: left; width:200px;">
                   <div style="float:left;"><span><strong>用户ip&nbsp;:</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${userLogPojo.ip_values}</span>
                   </div>
               </div>
              <div style="float: left; width:800px; clear: left;">
                   <div style="float:left;"><span><strong>操作内容&nbsp;</strong></span></div>
                   <div style="float:left;margin-left:10px;">
                     <span>${userLogPojo.content_desc}</span>
                   </div>
              </div>
             </form>
        </div> 
         <div class="form-button" style="float:left;margin-left:100px; margin-top:10px; clear: both;">
        		<a href="javascript:history.go(-1)" class="button bg-main">返回</a>&nbsp;&nbsp;
      	 </div>		
      	 
        </div>
      </div>
	</div>
</div>

 
</body>