<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="${resource_path}/js/Chart.js"></script>
<body>

	<div class="admin">		
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>客户包裹统计</strong>
			</div>
			<div class="tab-body">
			<table style="width: 100%">
			<tr>
				<td width="10%">
						<div style=" margin-left: 10px">
						<span>合计:</span>
						</div>
				</td>
				<td width="10%"></td>
				<td width="10%">半月未走货客户:</td>
				<td width="70%"><div style="word-break: break-all;width: 600px;">${businessClientInfo15String}</div></td>
			</tr>
			<tr>
				<td width="10%">
						<div style=" margin-left: 10px">
						<span>总客户数:</span>
						</div>
				</td>
				<td width="10%">${businessModelTotalInfo.total_client}</td>
				<td width="10%">一月未走货客户:</td>
				<td width="70%"><div style="word-break: break-all;width: 600px;">${businessClientInfo30String}</div></td>
			</tr>
			<tr>
				<td width="10%">
						<div style=" margin-left: 10px">
						<span>包裹数:</span>
						</div>
				</td>
				<td width="10%">${businessModelTotalInfo.total_package_count}个</td>
				<td width="10%">三月未走货客户:</td>
				<td width="70%"><div style="word-break: break-all;width: 600px;">${businessClientInfo90String}</div></td>
			</tr>
			<tr>
				<td width="10%">
						<div style=" margin-left: 10px">
						<span>总重量:</span>
						</div>
				</td>
				<td width="10%">${businessModelTotalInfo.total_actual_weight_count_str}磅</td>
				<td width="10%">六月未走货客户:</td>
				<td width="70%"><div style="word-break: break-all;width: 600px;">${businessClientInfo180String}</div></td>
			</tr>									
			</table>
		</div>
		<div>
		<br>
		<table style="width: 1000px">
		<tr>
		<td align="center"><strong>${currentYear}年客户走货总计</strong></td>
		<td align="center"><strong>${previousYear}年客户走货总计</strong></td>
		</tr>
		<tr>
		<td align="center"><canvas id="canvas1" height="300" width="400"></canvas></td>
		<td align="center"><canvas id="canvas2" height="300" width="400"></canvas></td>
		</tr>
		<tr>
		<td colspan="2" align="center"><a href="javascript:history.go(-1)" class="button bg-main">返回</a> </canvas></td>
		</tr>		
		</table>
		</div>
	</div>
	<%
	 Object currentYearLabel = request.getAttribute("currentYearLabel");
	 Object currentYearData = request.getAttribute("currentYearData");
	 Object previousYearLabel = request.getAttribute("previousYearLabel");
	 Object previousYearData = request.getAttribute("previousYearData");
	%>
	<script type="text/javascript">
	var currentYearLabel= <%=currentYearLabel%>;
	var currentYearData = <%=currentYearData%>;
	var lineChartData1 = {
		labels : currentYearLabel,
		datasets : [
			{
				label: "走货情况趋势",
				fillColor : "rgba(151,187,205,0.2)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(151,187,205,1)",
				data : currentYearData
			}
		]
	};
	var previousYearLabel= <%=previousYearLabel%>;
	var previousYearData = <%=previousYearData%>;
	var lineChartData2 = {
		labels : previousYearLabel,
		datasets : [
			{
				label: "走货情况趋势",
				fillColor : "rgba(151,187,205,0.2)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(151,187,205,1)",
				data : previousYearData
			}
		]
	};
window.onload = function(){
	var ctx1 = document.getElementById("canvas1").getContext("2d");
	window.myLine = new Chart(ctx1).Line(lineChartData1, {
		responsive: true
	}
	);
	var ctx2 = document.getElementById("canvas2").getContext("2d");
	window.myLine = new Chart(ctx2).Line(lineChartData2, {
		responsive: true
	}
	);
}		
	</script>
</body>
</html>