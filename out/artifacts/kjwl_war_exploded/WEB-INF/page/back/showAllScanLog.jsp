<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<div class="admin">
<table>
 <c:forEach var="outputScan"  items="${outputScanLogList}" varStatus="status">
      <tr style="color:${outputScan.add_pallet_success == 1 ? 'green' : 'red'}"> 
		<td>
${outputScan.scan_dateStr}，${outputScan.pkg_no}，${outputScan.pkg_status}，
<c:if test="${outputScan.add_pallet_success == 0}">
${outputScan.add_fail_reson}，
</c:if>
${outputScan.scan_user_name}，扫描次数：${outputScan.scan_count}

<c:if test="${outputScan.add_pallet_success == 1}">
&nbsp;&nbsp; √
</c:if>
		</td>
	</tr>
</c:forEach>
</table>
<c:if test="${ outputScanLogList.size()==0}">尚未有扫描日志记录</c:if>
</div>
</body>
</html>