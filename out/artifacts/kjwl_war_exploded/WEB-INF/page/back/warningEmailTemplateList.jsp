<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>邮件模板设置</strong>
			</div>
			<div class="padding border-bottom">
				<input type="button" class="button button-small border-green"
					onclick="addOrEdit(-1)" value="添加模板" />
			&nbsp;&nbsp;&nbsp;允许发送邮件：		
			<select id="enableEmail" name="enableEmail" onchange="enableEmailChange();">
				<option value="N" <c:if test="${enableEmail == 'N'}">selected="selected"</c:if>>禁用</option>
				<option value="Y" <c:if test="${enableEmail == 'Y'}">selected="selected"</c:if>>启用</option>
			</select>
			&nbsp;&nbsp;&nbsp;允许发送邮件时间段：<input type="text" id="emailTimeRange" name="emailTimeRange" value="${emailTimeRange}" onchange="setEmailTimeRange();"/>&nbsp;输入样例：08:00~18:00
			</div>
			<table class="table table-hover">
				<tr>
					<th>标题</th>
					<th>创建时间</th>
					<th>状态</th>
					<th>操作</th>
				</tr>

				<c:forEach var="list" items="${warningEmailTemplateList}">
					<tr>
						<td>${list.temp_title}</td>
						<td><fmt:formatDate value="${list.create_time}" pattern="yyyy-MM-dd hh:mm"/></td>
						<td><c:if test="${list.enable=='N'}">禁用</c:if> <c:if
								test="${list.enable=='Y'}">启用</c:if></td>
						<td><a class="button border-blue button-little"
							href="javascript:void(0);"
							onclick="show('${list.seq_id }')">查看详情</a>&nbsp;&nbsp;
							<a class="button border-blue button-little"
							href="javascript:void(0);"
							onclick="addOrEdit('${list.seq_id }')">编辑</a>&nbsp;&nbsp;
							<a class="button border-blue button-little"
							href="javascript:void(0);" onclick="del('${list.seq_id }')">删除</a>
						</td>
					</tr>
				</c:forEach>
			</table>
			<div class="panel-foot text-center">
				<jsp:include page="webfenye.jsp"></jsp:include>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
	function addOrEdit(seq_id) {
		var url = "${backServer}/warningEmailTemplate/editInit?seq_id=" + seq_id;
		window.location.href = url;
	}
	function show(seq_id) {
		var url = "${backServer}/warningEmailTemplate/show?seq_id=" + seq_id;
		window.location.href = url;
	}

	function del(seq_id) {
		if (confirm("确认删除吗？")) {
			var url = "${backServer}/warningEmailTemplate/delete?seq_id=" + seq_id;
			$.ajax({
				url : url,
				type : 'GET',
				success : function(data) {
					alert("删除成功！");
					window.location.href = "${backServer}/warningEmailTemplate/queryAllOne";
				}
			});
		}
	}
	
	function enableEmailChange() {
			var enableEmail=$('#enableEmail').val();
			var url = "${backServer}/warningEmailTemplate/enableEmailHandle?enableEmail=" + enableEmail;
			$.ajax({
				url : url,
				type : 'GET',
				success : function(data) {
					alert("设置成功！");
					window.location.href = "${backServer}/warningEmailTemplate/queryAllOne";
				}
			});
	}
	
	 function setEmailTimeRange() {
			var emailTimeRange=$('#emailTimeRange').val();
			var regTimeRange=/^[0-9][0-9]:[0-9][0-9]~[0-9][0-9]:[0-9][0-9]$/;
		    if(!(regTimeRange.test(emailTimeRange))){ 
		        alert("允许发送短信时间段有误，请重填");  
		        return false; 
		    }
			var url = "${backServer}/warningEmailTemplate/setEmailTimeRange?emailTimeRange=" + emailTimeRange;
			$.ajax({
				url : url,
				type : 'GET',
				success : function(data) {
					alert("设置成功！");
					window.location.href = "${backServer}/warningEmailTemplate/queryAllOne";
				}
			});
	}
</script>
</html>