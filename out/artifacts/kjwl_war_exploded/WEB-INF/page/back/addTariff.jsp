<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>资费添加</strong></div>
      <div class="tab-body">
        <br/>
                  <input type="hidden" id="checkTariffNameResult" value="1">
                  <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>资费名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="tariff_name" name="tariff_name" style="width:200px"
                              onblur="checkTariffName()" />
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="tariffNameTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>资费类型:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <select id="tariff_type" name="tariff_type" class="input" style="width:200px;">
                            <option value="">请选择</option>
                            <option value=1>铜</option>
                            <option value=2>银</option>
                            <option value=3>金</option>
                            <option value=4>铂金</option>
                            <option value=5>钻石</option>
                       </select>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="tariffTypeTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>资费税率:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <input type="text" class="input" id="percent" name="percent" style="width:70px"
                              onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="percentTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>资费类型:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                       <select id="unit" name="unit" class="input" style="width:100px;">
                            <option value="0">请选择</option>
                            <option value=1>元/KG</option>
                            <option value=2>美元/磅</option>
                       </select>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="unitTextArea"></span>
                    </div>
                 </div>
                 <div style="height: 100px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>描述信息:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:250px">
                      <textarea class="input" rows="3" cols="50" id="description" name="description"></textarea>
                    </div>
                  </div>
        
                  <div class="padding border-bottom" >
                    <input type="button" class="button bg-main" onclick="add()" value="添加" />
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
                 </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
    $(function(){
        $('#tariff_name').focus();
    });
    
    //资费名称文本框鼠标失去焦点，校验资费名称是否已经存在
    function checkTariffName(){
        $('#tariffNameTextArea').empty();
        var url = "${backServer}/tariff/checkTariffName";
        var tariff_name =$('#tariff_name').val();

        if(tariff_name==null || tariff_name.trim()==""){
            return;
        }
        $.ajax({
            url:url,
            type:'post',
            data:{"tariff_name":tariff_name},
            success:function(data){
                $('#checkTariffNameResult').val(data);
                if(data=="1"){
                    
                    $('#tariffNameTextArea').text('资费名称可以使用!');
                    $('#tariffNameTextArea').css('color','green');
                }else{
                    $('#tariffNameTextArea').text('资费名称已经存在!');
                    $('#tariffNameTextArea').css('color','red');
                }
            }
            
        });

    }

    function add(){
        
        $('#tariffNameTextArea').empty();
        $('#tariffTypeTextArea').empty();
        $('#percentTextArea').empty();
        $('#unitTextArea').empty();
        
        var url = "${backServer}/tariff/insertTariff";
        var tariff_name =$('#tariff_name').val();
        var tariff_type =$('#tariff_type').val();
        var percent =$('#percent').val();
        var unit =$('#unit').val();
        var description =$('#description').val();
        var checkResult ="0"
        var checkTariffNameResult= $('#checkTariffNameResult').val();
        if(tariff_name==null||tariff_name.trim()==""){
                $('#tariffNameTextArea').text('名称类型不能为空!');
                $('#tariffNameTextArea').css('color','red');
                checkResult ="1";
        }
        if(tariff_type==null||tariff_type.trim()==""){
            $('#tariffTypeTextArea').text('资费类型不能为空!');
            $('#tariffTypeTextArea').css('color','red');
            checkResult ="1";
        }
        if(percent==null||percent.trim()==""){
            $('#percentTextArea').text('资费税率不能为空!');
            $('#percentTextArea').css('color','red');
            checkResult ="1";
        }
        if(isNaN(percent)||percent<=0){ 
            $('#percentTextArea').text('资费税率必须为正数!');
            $('#percentTextArea').css('color','red');
            checkResult ="1";
          }
        if(unit==null||unit.trim()==""){
            $('#unitTextArea').text('资费单位不能为空!');
            $('#unitTextArea').css('color','red');
            checkResult ="1";
        }
        if(checkResult =="1"){
           return;
        }
        if(checkTariffNameResult!="1"){
            alert("资费名称已经存在!");
            return;
        }
        $.ajax({
            url:url,
            data:{"tariff_name":tariff_name,
                  "tariff_type":tariff_type,
                  "percent":percent,
                  "unit":unit,
                  "description":description},
            type:'post',
            dataType:'text',
            success:function(data){
               alert("添加成功");
               window.location.href = "${backServer}/tariff/tariffInit";
            }
        });
    }
    </script>
</body>
</html>