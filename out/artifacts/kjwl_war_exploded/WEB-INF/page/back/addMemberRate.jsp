<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>

<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>会员等级添加</strong>
			</div>
			<div class="tab-body">
				<br/> 
				<input type="hidden" id="checkRateNameResult" value="1">
				<input type="hidden" id="checkRateIntrgralMinResult" value="1">
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>会员类型:</strong>
						</span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<select id="rate_type" class="input" style="width: 100px">
							<option value="">请选择</option>
							<option value=1>普通用户</option>
							<option value=2>同行用户</option>
						</select>
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px;">
						<span id="rateTypeTextArea"></span>
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>等级名称:</strong>
						</span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<input type="text" class="input" id="rate_name"
							style="width: 100px"/>
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px;">
						<span id="rateNameTextArea"></span>
					</div>
				</div>
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>积分下限:</strong>
						</span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<input type="text" class="input" id="integral_min"
							style="width: 100px"
							onkeyup="value=value.replace(/[^\w\/]/ig,'')" />
					</div>
					<div style="float: left; margin-top: 5px; margin-left: 10px;">
						<span id="integralMinTextArea"></span>
					</div>
				</div>
				<div class="padding border-bottom">
					<input type="button" class="button bg-main" onclick="add()"
						value="保存" /> <input type="button" class="button bg-main"
						onclick="javascript:history.go(-1)" value="取消" />
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#rate_type').focus();
		});

		function add() {

			$('#rateTypeTextArea').empty();
			$('#rateNameTextArea').empty();
			$('#integralMinTextArea').empty();

			var url = "${backServer}/memberRate/insert";

			var checkRateNameResult = $('#checkRateNameResult').val();

			var rate_type = $('#rate_type').val();
			var rate_name = $('#rate_name').val();
			var integral_min = $('#integral_min').val();

			var checkResult = "0"

			// 数字检验（非负整数）
			var exp = /^(0|[1-9]\d*)$/;

			if (rate_type == null || rate_type.trim() == "") {
				$('#rateTypeTextArea').text('会员类型不能为空!');
				$('#rateTypeTextArea').css('color', 'red');
				checkResult = "1";
			}

			if (rate_name == null || rate_name.trim() == "") {
				$('#rateNameTextArea').text('等级名称不能为空!');
				$('#rateNameTextArea').css('color', 'red');
				checkResult = "1";
			}

			if (integral_min == null || integral_min.trim() == "") {
				$('#integralMinTextArea').text('积分下限不能为空!');
				$('#integralMinTextArea').css('color', 'red');
				checkResult = "1";
			} else if (!exp.test(integral_min)) {
				$('#integralMinTextArea').text('积分下限格式错误！');
				$('#integralMinTextArea').css('color', 'red');
				checkResult = "1";
			}
			if (checkResult == "1") {
				return false;
			}
			$
					.ajax({
						url : url,
						data : {
							"rate_type" : rate_type,
							"rate_name" : rate_name,
							"integral_min" : integral_min
						},
						type : 'post',
						dataType : 'text',
						success : function(data) {
							
							if(data =="1"){
								$('#rateNameTextArea').text('该会员类型下等级名称已经存在!');
				                $('#rateNameTextArea').css('color', 'red');
							}else if(data =="2"){
								$('#integralMinTextArea').text('该会员类型下积分下限已经存在!');
				                $('#integralMinTextArea').css('color', 'red');
							}else{
	                            alert("添加成功");
	                            window.location.href = "${backServer}/memberRate/memberRateList";
							}

						}
					});
		}
	</script>
</body>
</html>