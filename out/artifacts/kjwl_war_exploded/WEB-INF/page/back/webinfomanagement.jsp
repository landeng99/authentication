<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">网站管理信息</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active form-x" id="tab-set">
				<div class="form-group">
                    <div class="label" style="width:9%"><label for="readme">管理title</label></div>
                    <div class="field" style="width:91%">
                    	<input type="text" value="${siteInfoPojo.title}" class="input" id="title" name="title" size="50" placeholder="管理title" data-validate="required:请填写你的管理title" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="label" style="width:9%"><label for="sitename">网站logo</label></div>
                    <div class="field" style="width:91%">
                    	<input type="file" value="${siteInfoPojo.logo}" class="input-file" id="logoPic" name="logoPic" onchange="logoChange()"/>
                    </div>
                    <div style="clear: both;margin-left:100px;width:320px;height:240px;border: 1px dotted #ccc;" >
                       <img src="${resource_path}/${siteInfoPojo.logo}" style="width:320px;height:240px;" id="urlArea"  name ="urlArea"/>
                     </div>
                </div>
                <div class="form-group">
                    <div class="label" style="width:9%"><label for="siteurl">网址版权信息</label></div>
                    <div class="field" style="width:91%">
                    	<input type="text" value="${siteInfoPojo.copyright}" class="input" id="copyright" name="copyright" size="50" placeholder="网址版权信息" data-validate="required:请填写你的网址版权信息" />
                    </div>
                </div>
                <div class="form-button">
                	<a href="javascript:void(0);" class="button bg-main" onclick="save()">保存</a>&nbsp;&nbsp;&nbsp;&nbsp;               	
				</div>
        </div>
	</div>
</div>

<script type="text/javascript">
var img_suffix_ary = new Array();
img_suffix_ary[0] = ".bmp";
img_suffix_ary[1] = ".jpg";
img_suffix_ary[2] = ".jpeg";
img_suffix_ary[3] = ".png";
img_suffix_ary[4] = ".gif";
img_suffix_ary[5] = ".pcx";
img_suffix_ary[6] = ".raw";
img_suffix_ary[7] = ".tiff";
img_suffix_ary[8] = ".tga";
img_suffix_ary[9] = ".exif";
img_suffix_ary[10] = ".fpx";
img_suffix_ary[11] = ".svg";
img_suffix_ary[12] = ".psd";
img_suffix_ary[13] = ".cdr";
img_suffix_ary[14] = ".pcd";
img_suffix_ary[15] = ".dxf";
img_suffix_ary[16] = ".ufo";
img_suffix_ary[17] = ".eps";
img_suffix_ary[18] = ".hdri";
img_suffix_ary[19] = ".ai";
//获取文件后缀
function test(file_name){
	var result =/\.[^\.]+/.exec(file_name);
	return result;
}

var logoVal = "";
function logoChange(){
	//文件名
	var file_name = $("#logoPic").val();
	//后缀
	var result = test(file_name);
	//后缀是否存在于图片格式中
	var isImgSuffix = false;
	
	var x;
	for (x in img_suffix_ary)
	{	
		if(result == img_suffix_ary[x]){
			//存在
			isImgSuffix = true;
			break;
		}
	}
	//
	if(isImgSuffix == false){
		 alert("文件["+file_name+"]不是图片格式,请选择图片上传");
		return;
	}
	
    var url = "${backServer}/siteInfo/uploadSiteInfoLogo";
      $.ajaxFileUpload({
          url : url,
          data:{"imgText":logoVal},
          type : 'post',
          secureuri : false,
          fileElementId : 'logoPic',
          dataType : 'text/html',
          success : function(data ) { 
          	$('#urlArea').attr("src","${resource_path}/"+data);
          	logoVal = data;
          },
          error : function(data, status, e) {
              alert("error" + e);
          }
      });
  }
function save(){
    var copyright=$("#copyright").val();
    var title=$("#title").val();
    var url = "${backServer}/siteInfo/updateSiteInfo?copyright="+copyright+
    		"&title="+title+"&logo="+logoVal;
    window.location.href = url;
  }
</script>
</body>