<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>
<body>
<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:100px;
    }
    .div-front{
        float:left;
        width:400px;
    }
    .div-front-List{
        float:left;
    }
    
     .th-title{
        height:30px;
        vertical-align:middle;
    }
    
        .td-List-left{
        height:30px;
        vertical-align:middle;
        text-align:left;
        padding-left:5px;
    }
        .td-List-right{
        height:30px;
        vertical-align:middle;
        text-align:right;
        padding-right:5px;
        
    }
</style>
<div class="admin" >
    <div class="panel admin-panel">
      <div class="panel-head"><strong>包裹详情</strong></div>
      <div class="tab-body">
      </br>
                 <input type="hidden" id="package_id" name ="package_id" value="${pkgDetail.package_id}">
                 <div style="height: 40px;">
                      <div style="float:left;margin-left:15px;"><span><strong>关联单号:</strong></span></div>
                      <div style="float:left;margin-left:38px;width:220px;">
                      <span>${pkgDetail.original_num}</span>
                      </div>
                      <div style="float:left;"><span><strong>公司运单号:</strong></span></div>
                      <div style="float:left;margin-left:24px;">
                      <span>${pkgDetail.logistics_code}</span>
                      </div>
                 </div>
                 <div style="height:40px;">
                      <div style="float:left;margin-left:15px;"><span><strong>海外仓库地址:</strong></span></div>
                      <div style="float:left;margin-left:10px;">
                        <Span>${osaddr}</Span>
                      </div>
                 </div>
                 
                 <div>
                      <div style="float:left;margin-left:15px;"><span class="span-title">商品:</span></div>
                      <span>
                           <table border="1" cellpadding="0" cellspacing="0">
                             <tr>
                               <th width ="300" class="th-title">商品名称</th>
                               <th width ="100" class="th-title">品牌</th>
                               <th width ="100" class="th-title">商品申报类别</th>
                               <th width ="100" class="th-title">价格</th>
                               <th width ="50" class="th-title">数量</th>
                               <th width ="100" class="th-title">规格</th>
                               <th width ="100" class="th-title">关税费用</th>
                             </tr>
                             <c:forEach var="good"  items="${pkgGoodsList}">
                                <tr>
                                  <td width ="300" class="td-List-left">${good.goods_name}</td>
                                  <td width ="100" class="td-List-left">${good.brand}</td>
                                  <td width ="100" class="td-List-left">${good.name_specs}</td>
                                  <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.price}" type="currency" pattern="$#0.00#"/></td>
                                  <td width ="50" class="td-List-right">${good.quantity}</td>
                                  <td width ="100" class="td-List-left">${good.spec}</td>
                                  <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                                </tr>
                             </c:forEach>
                           </table>
                      </span>
                 </div>

                 <div>&nbsp;</div>

                 <div style="height: 40px;">
                      <div style="float:left;margin-left:15px;"><span><strong>包裹体积:</strong></span></div>
                      <div style="float:left;margin-left:38px;"><span>长:</span></div>
                      <div style="float:left;margin-left:30px;">
                        <span>${pkgDetail.length}</span>
                      </div>
                      <div style="float:left;"><span>CM</span></div>
                      
                      <div style="float:left;margin-left:20px;"><span>宽:</span></div>
                      <div style="float:left;margin-left:30px;">
                         <span>${pkgDetail.width}</span>
                      </div>
                      <div style="float:left;"><span>CM</span></div>
                      
                      <div style="float:left;margin-left:20px;"><span>高:</span></div>
                      <div style="float:left;margin-left:30px;">
                         <span>${pkgDetail.height}</span>
                      </div>
                      <div style="float:left;width:50px;"><span>CM</span></div>
                      <div style="float:left;margin-left:15px;"><span><strong>包裹重量:</strong></span></div>
                      <div style="float:left;margin-left:18px;">
                        <span>${pkgDetail.weight}磅</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;margin-left:15px;"><span><strong>运费:</strong></span></div>
                      <div style="float:left;margin-left:64px;width:350px;">
                        <span><fmt:formatNumber value="${pkgDetail.freight}" type="currency" pattern="$#0.00#"/></Span>
                      </div>
                      <div style="float:left;"><span for="return_cost"><strong>总价值:</strong></span></div>
                      <div style="float:left;margin-left:32px;">
                        <span><fmt:formatNumber value="${total_worth}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                 </div>
         </div>

        </div>
         <div class="panel admin-panel">    
         <div class="panel-head"><strong>客户详情</strong></div>
         <div class="tab-body">
         </br>
                <input type="hidden" id="user_id" name ="user_id">
                <div style="height: 40px;">
                      <div style="float:left;margin-left:15px;"><span><strong>客户账号:</strong></span></div>
                      <div style="float:left;margin-left:40px;margin-top:-8px;">
                        <input type="text" class="input" id="account" name="account" style="width:200px"
                               onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                      </div>

                      
                      <div style="float:left;margin-left:20px;"><span><strong>客户姓名:</strong></span></div>
                      <div style="float:left;margin-left:10px;margin-top:-8px;">
                        <input type="text" class="input" id="user_name" name="user_name" style="width:200px"/>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;margin-left:15px;"><span><strong>手机号码:</strong></span></div>
                      <div style="float:left;margin-left:40px;margin-top:-8px;">
                        <input type="text" class="input" id="mobile" name="mobile" style="width:200px"
                               onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" />
                      </div>
                      <div style="float:left;margin-left:20px;"><span><strong>客户邮箱:</strong></span></div>
                      <div style="float:left;margin-left:10px;margin-top:-8px;">
                        <input type="text" class="input" id="email" name="email" style="width:200px"/ >
                      </div>
                 </div>
                 <div style="height:40px;">
                   <div  style="float:left;margin-left:116px;">
                     <input type="button" class="button bg-main" onclick="search()" value="查询" />
                     <span id="textArea"></span>
                   </div>
                </div>
                 <div style="height:100px;">
                      <div style="float:left;margin-left:15px;"><span for="goods"><strong>客户详情:</strong></span></div>
                      <div style="float:left;margin-left:40px;" >
                         <table border="1" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width ="150" align="center">客户账号</td>
                              <td width ="150" align="center">客户姓名</td>
                              <td width ="100" align="center">手机号码</td>
                              <td width ="200" align="center">客户邮箱</td>
                              <td width ="100" align="center">客户类型</td>
                            </tr>
                            <tr>
                              <td align="center" id ="userInfoAccount">&nbsp;</td>
                              <td align="center" id ="userInfoUserName"></td>
                              <td align="center" id ="userInfoMobile"></td>
                              <td align="center" id ="userInfoEmail"></td>
                              <td align="center" id ="userInfoUserType"></td>
                            </tr>
                        </table>
                      </div>
                 </div>
                 <div style="height:40px;">
                   <div id ="buttonArea" style="float:left;margin-left:116px;">
                   </div>
                </div>
         </div>
        </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $('#originalNum').focus();
        });
        
        function associate(package_id){
            var url = "${backServer}/pkg/associate?package_id="+package_id;
            window.location.href = url;
        }
        
        function search(){

            $('#user_id').val("");
            $('#textArea').empty();
            $('#userInfoAccount').empty();
            $('#userInfoUserName').empty();
            $('#userInfoMobile').empty();
            $('#userInfoEmail').empty();
            $('#userInfoUserType').empty();
            $('#buttonArea').empty();
            $('#userInfoAccount').append("&nbsp;");

             var url = "${backServer}/pkg/frontUserInfo";

             var account=$('#account').val();
             var user_name =$('#user_name').val();
             var mobile =$('#mobile').val();
             var email =$('#email').val();
             if(( ""==account||account==null)&&
                     ( ""==user_name||user_name==null)&&
                     ( ""==mobile||mobile==null)&&
                     ( ""==email||email==null)){
                      alert("查询条件不能全为空");
                      $('#account').focus();
                      return
                  }
             $.ajax({
                 url:url,
                 data:{"account":account,
                     "user_name":user_name,
                     "mobile":mobile,
                     "email":email},
                 type:'post',
                 success:function(data){

                     if(data.result == false){
                         $('#textArea').text('未查询到该客户信息!');
                         $('#textArea').css('color','red');
                     }else{
                         $('#user_id').val(data.userInfo.user_id);
                         
                         $('#userInfoAccount').append(data.userInfo.account);
                         $('#userInfoUserName').append(data.userInfo.user_name);
                         $('#userInfoMobile').append(data.userInfo.mobile);
                         $('#userInfoEmail').append(data.userInfo.email);
                         
                         var userTypeName ="普通用户"
                         if(data.userInfo.user_type==2){
                           userTypeName ="同行用户"
                         }
                         $('#userInfoUserType').append(userTypeName);
                         
                         $('#buttonArea').append("<input type='button' class='button bg-main' onclick='associateFrontUser()' value='关联' />");
                     }
                 }
             });
        }

        function associateFrontUser(){
            var url = "${backServer}/pkg/associateFrontUser";
            var user_id =$('#user_id').val();
            var package_id =$('#package_id').val();

            $.ajax({
                url:url,
                data:{"package_id":package_id,
                    "user_id":user_id
                    },
                type:'post',
                async:false,
                dataType:'text',
                success:function(data){
                alert("关联成功");
                window.location.href = "${backServer}/pkg/abnormalPkg";
                }
            });
        }
    </script>
</body>
</html>