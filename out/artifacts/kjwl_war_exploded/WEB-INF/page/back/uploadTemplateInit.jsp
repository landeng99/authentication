<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
     
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">模板上传</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br /><div class="tab-panel active form-x" id="tab-set">
        <div class="tab-panel active" id="tab-set">
		<div class="form-group">
			<form id="myform" name="myform">
			<div class="form-group" style="clear: both; float:left;margin-left:45px; margin-bottom: 20px; width: 80%;">
                <span>说明： 分别先后上传内容一致xls格式和xlsx格式两种模板，以免用户下载不同格式模板时内容不一致。</span>
             </div>
                
               <div class="form-group" style="clear: both; float:left; margin-bottom: 20px; width: 80%;">
                     <div class="label" style="float:left;margin-top:5px; width: 14%;""><label for="name">上传模板:</label></div>
                     <div style="float:left;margin-top:5px;margin-left:40px;width:100px">
                        
                       <input class="input-file" type="file"  id="excelFile" name="excelFile" 
                       		onchange="excelFileChange()"/>
                        
                       <span style="color: red;" id="excelFileNote"></span>
                     </div> 
                </div>
             </form>
        </div>
        </div>
      </div>
	</div>
</div>

<script type="text/javascript">

var img_suffix_ary = new Array();
img_suffix_ary[0] = ".xls";
img_suffix_ary[1]='.xlsx';
    //获取文件后缀
function test(file_name){
	var result =/\.[^\.]+/.exec(file_name);
	return result;
}
 
function excelFileChange(){
	//文件名
	var file_name = $("#excelFile").val();
	if(file_name==''){
		return ;
	}
	//后缀
	var result = test(file_name);
	//后缀是否存在于图片格式中
	var isImgSuffix = false;
	
	var x;
	for (x in img_suffix_ary)
	{	
		if(result == img_suffix_ary[x]){
			//存在
			isImgSuffix = true;
			break;
		}
	}
	//
	if(isImgSuffix == false){
		 $("#excelFileNote").html("文件["+file_name+"]不是excel文件格式,请选择正确模板上传");
		 $("#excelFileNote").show();
		return;
	}else{
		$("#excelFileNote").hide();
	}
	
    var url = "${backServer}/banner/uploadTemplate";
      $.ajaxFileUpload({
          url : url,
          type : 'post',
          secureuri : false,
          fileElementId : 'excelFile',
          dataType : 'text/html',
          success : function(data) { 
        	 $("#excelFileNote").html("模板上传成功");
        	 $('#excelFileNote').css('color','#8CBB34');
     		 $("#excelFileNote").show();
     		 $('#excelFile').val('');
          },
          error : function(data, status, e) {
              layer.alert("error" + e);
          }
      });
  } 
</script>
</body>