<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
</style>
 
<body>
	 <div class="admin">
	 	<c:if test="${navigationPojo.nav_id == 0}">
	<form action="${backServer}/navigation/addNavigation" method="post">
		</c:if><c:if test="${navigationPojo.nav_id > 0}">
	<form action="${backServer}/navigation/updateNavigation" method="post">
		<input type="hidden" name="nav_id" id="nav_id" value="${navigationPojo.nav_id}"/>
		</c:if>
	  <div class="field-group">
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="category_name">导航栏名</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="text" name="nav_name" id="nav_name" value="${navigationPojo.nav_name}"/>
		 		</div>
		 	</div>
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="category_name">链接</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="text" name="url" id="url" value="${navigationPojo.url}"/>
		 		</div>
		 	</div>
		 	
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="parent_id">状态</label>
		 		</div>
		 		<div class="field">
		 			<select  class="input" name="status">
		 				<option value="1" selected="selected">启用</option>
		 				<option value="2">禁用</option>
		 			</select>
		 		</div>
		 	</div>
		 	
		 	  <div class="field-item">
		 		<div class="label">
		 			<label for="parent_id">父分类</label>
		 		</div>
		 		<div class="field">
		 			<select  class="input" name="parent_id">
		 				<option value="0">--选择父分类--</option>
		 			
	 				<c:forEach var="cate" items="${navigationLists}">
	 					<option value="${cate.nav_id}">${cate.nav_name}</option>
				 	</c:forEach>
				 	
				 	<c:if test="${navigationPojo.parent_id > 0}">
				 		<option value="${navigationParentPojo.nav_id}" selected="selected">${navigationParentPojo.nav_name}</option>
				 	</c:if>
				 	
		 			</select>
		 		</div>
		 	</div>

	 	</div>
	 		<div style="clear: both"></div>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<input type="submit" class="button bg-main" value="保存">
	  	   		<a class="button bg-main" href="javascript:;" onclick="back()">取消</a>
	  	   </div>
	  	   </form>
	</div>
	
	 <script type="text/javascript">
		 function back(){
			 window.history.go(-1);		 
		 }
	 </script>
	
</body>
</html>