<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:80px;
    }
    .div-front{
        float:left;
        width:300px;
    }

</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">用户地址详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br/>
        <div class="tab-panel active" id="tab-set">
        <div class="field-group ">

                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">用户账号:</span>
                           <span>${frontUser.account}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户姓名:</span>
                           <span>${frontUser.user_name}</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">手机号码:</span>
                           <span>${frontUser.mobile}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户邮箱:</span>
                           <span>${frontUser.email}</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">注册时间:</span>
                           <span><fmt:formatDate value="${frontUser.register_time}" type="both"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户类型:</span>
                           <span>
                             <c:if test="${frontUser.user_type==1}">普通用户</c:if>
                             <c:if test="${frontUser.user_type==2}">同行用户</c:if>
                           </span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">first_name:</span>
                           <span>${frontUser.first_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">last_name:</span>
                           <span> ${frontUser.last_name}</span>
                      </div>
                </div>

                 <div style="height: 40px;">
                      <div style="float:left;"><span><strong>用户地址:</strong></span></div>
                      <div style="float:left;margin-left:30px;">
                       <Span>${address}</Span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;"><span><strong>身份证号:</strong></span></div>
                      <div style="float:left;margin-left:30px;">
                       <Span>${receiveAddress.idcard}</Span>
                      </div>
                 </div>

                 <div style="height: 250px;">
                      <div style="float:left;"><span><strong>身份证照片:</strong></span></div>
                      <div style="float:left;margin-left:12px;">
                        <img src="${resource_path}${receiveAddress.front_img}" id="picArea"  name ="picArea" width="320" height="240"/>
                      </div>
                      <div style="float:left;margin-left:40px;">
                        <img src="${resource_path}${receiveAddress.back_img}" id="picArea"  name ="picArea" width="320" height="240"/>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;"><span><strong>审核结果:</strong></span></div>
                      <div style="float:left;margin-left:30px;">
                      <shiro:hasPermission name="frontUserfrontUserAddressInit:application">
                          <Span><c:if test="${receiveAddress.idcard_status==1}">审核通过</c:if>
                          </Span>
                      </shiro:hasPermission><shiro:hasPermission name="frontUserfrontUserAddressInit:refusing">
                          <Span><c:if test="${receiveAddress.idcard_status==2}">审核拒绝</c:if>
                          </Span>
                       </shiro:hasPermission>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;"><span><strong>操作理由:</strong></span></div>
                      <div style="float:left;margin-left:30px;">
                         <Span>${receiveAddress.refuse_reason}</Span>
                      </div>
                 </div>
         </div>
        <div class="form-button" style="float:left;margin-left:100px;">
            <a href="javascript:history.go(-1)" class="button bg-main">返回</a> 
        </div>
             </div>
             </div>
        </div>
        </div>
    </div>
<script type="text/javascript">

    </script>
</body>
</html>