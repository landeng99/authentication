<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:100px;
    }
        .div-front{
        float:left;
        width:300px;
    }
    
         .th-title{
        height:30px;
        vertical-align:middle;
    }
    
        .td-List-left{
        height:30px;
        vertical-align:middle;
        text-align:left;
        padding-left:5px;
    }
        .td-List-right{
        height:30px;
        vertical-align:middle;
        text-align:right;
        padding-right:5px;
        
    }
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">包裹详情</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br/>
        <div class="tab-panel active" id="tab-set">
        <div class="field-group ">
        <input type="hidden" id="package_id" name ="package_id" value="${pkgDetail.package_id}">
        <input type="hidden" id="status1" name ="status1" value="${pkgDetail.status}">
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户名称:</span>
                           <span>${frontUser.user_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">手机号:</span>
                           <span>${frontUser.mobile}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">客户账户:</span>
                           <span><fmt:formatNumber value="${frontUser.account}" type="currency" pattern="$#0.00#"/></span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">包裹创建时间:</span>
                           <span><fmt:formatDate value="${pkgDetail.createTime}" type="both"/><span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class="div-front">
                           <span class="span-title">关联单号:</span>
                           <span>${pkgDetail.original_num}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">公司运单号:</span>
                           <span>${pkgDetail.logistics_code}</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;">
                           <span class="span-title">包裹地址:</span>
                           <Span>${address}</Span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;">
                           <span class="span-title">海外仓库地址:</span>
                           <Span>${osaddr}</Span>
                      </div>
                 </div>
                 <div>
                      <div style="float:left;"><span class="span-title">商品:</span></div>
                      <span>
                           <table border="1" cellpadding="0" cellspacing="0">
                             <tr>
                               <th width ="300" class="th-title">商品名称</th>
                               <th width ="100" class="th-title">品牌</th>
                               <th width ="100" class="th-title">商品申报类别</th>
                               <th width ="100" class="th-title">价格</th>
                               <th width ="50" class="th-title">数量</th>
                               <th width ="100" class="th-title">规格</th>
                               <th width ="100" class="th-title">关税费用</th>
                             </tr>
                             <c:forEach var="good"  items="${pkgGoodsList}">
                                <tr>
                                  <td width ="300" class="td-List-left">${good.goods_name}</td>
                                  <td width ="100" class="td-List-left">${good.brand}</td>
                                  <td width ="100" class="td-List-left">${good.name_specs}</td>
                                  <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.price}" type="currency" pattern="$#0.00#"/></td>
                                  <td width ="50" class="td-List-right">${good.quantity}</td>
                                  <td width ="100" class="td-List-left">${good.spec}</td>
                                  <td width ="100" class="td-List-right"><fmt:formatNumber value="${good.customs_cost}" type="currency" pattern="$#0.00#"/></td>
                                </tr>
                             </c:forEach>
                           </table>
                      </span>
                 </div>

                 <div>&nbsp;</div>
                 
                 <div style="height: 40px;">
                      <div style="float:left;"><span class="span-title">包裹体积:</span></div>
                      <div style="float:left;width:100px;">
                           <span>长:</span>
                           <span>${pkgDetail.length}CM</span>
                      </div>
                      <div style="float:left;width:100px;">
                           <span>宽:</span>
                           <span>${pkgDetail.width}CM</span></div>
                      <div style="float:left;width:100px;">
                           <span>高:</span>
                           <span>${pkgDetail.height}CM</span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class="div-front">
                           <span class="span-title">包裹重量:</span>
                           <Span>${pkgDetail.weight}KG</Span>
                      </div>
                      <div style="float:left;"><span class="span-title">退货费用:</span></div>
                      <div style="float:left;margin-top:-8px;">
                        <input type="text" class="input" id="return_cost" name="return_cost" style="width:100px" value = ${pkgDetail.return_cost}>
                      </div>
                      <div style="float:left;"><span>元</span></div>
                 </div>
                 <div style="height: 40px;">
                      <div class="div-front">
                           <span class="span-title">退货单号:</span>
                           <Span>${pkgDetail.return_code}</Span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">退货支付状态:</span>
                           <Span>
                              <c:if test="${pkgDetail.retpay_status==0}">未支付</c:if>
                              <c:if test="${pkgDetail.retpay_status==1}">已支付</c:if>
                           </Span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class="div-front">
                           <span class="span-title">退货理由:</span>
                           <Span>${pkgReturn.reason}</Span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;"><span"><strong>审核结果:</strong></span></div>
                      <div style="float:left;margin-left:40px;margin-top:-5px;">
                        <input type="radio" class="input" id="yes" name="approve_status" style="width:30px" value = 1 >
                      </div>
                      <div style="float:left;">
                      <Span>同意</Span>
                      </div>
                      <div style="float:left;margin-left:40px;margin-top:-5px;">
                        <input type="radio" class="input" id="no" name="approve_status" style="width:30px" value = 2 checked="checked" >
                      </div>
                      <div style="float:left;">
                      <Span>拒绝</Span>
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div style="float:left;margin-top:5px;"><span><strong>拒绝理由:</strong></span></div>
                      <div style="float:left;margin-left:40px;width:800px;">
                        <input type="text" class="input" id="approve_reason" name="approve_reason" style="width:600px"
                               value="${pkgReturn.approve_reason}"/>
                      </div>
                 </div>
         </div>
        <div class="form-button" style="float:left;margin-left:100px;">
        <shiro:hasPermission name="pkgreturn:add">
         			<a href="javascript:void(0);" class="button bg-main" onclick="save()" >保存</a>
        </shiro:hasPermission>
                   <a href="javascript:history.go(-1)" class="button bg-main">取消</a> 
                </div>
             </div>
             </div>
        </div>
        </div>
    </div>
<script type="text/javascript">

//选择框选中
    function save(){
        var url = "${backServer}/pkg/approve";

        var package_id=$('#package_id').val();
        var return_cost =$('#return_cost').val();
        var approve_status =$("input[name='approve_status']:checked").val();
        var approve_reason =$('#approve_reason').val();
        
        if(isNaN(return_cost)||return_cost<0){
            alert("退货费用必须为大于0数字");
            $('#return_cost').focus();
            return;
        }
        if(2==approve_status &&(approve_reason==null||approve_reason.trim()=="")){
            alert("拒绝理由不能为空");
            $('#approve_reason').focus();
            return
        }
        
        $.ajax({
            url:url,
            data:{"package_id":package_id,
                  "return_cost":return_cost,
                  "approve_status":approve_status,
                  "approve_reason":approve_reason},
            type:'post',
            dataType:'text',
            async:false,
            success:function(data){
               alert("保存成功");
               window.location.href = "${backServer}/pkg/return";
            }
        });
    }
    
    $('#yes').click(function(){
        $('#approve_reason').attr('disabled',"disabled");
    });
    $('#no').click(function(){
        $('#approve_reason').removeAttr('disabled');
    });
    </script>
</body>
</html>