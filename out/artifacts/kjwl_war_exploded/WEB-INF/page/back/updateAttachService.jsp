<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>
<style>
.label{float:left;margin-top:5px; margin-left:10px;width: 150px;}
.text_field{float:left;margin-left:10px;}
.unit{font-size: 12px;margin-left: 10px;}
    .span1{
        display:-moz-inline-box;
        display:inline-block;
        width:25px;
        height:20px;
    }
</style>
<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>增值服务修改</strong></div>
      <div style="height:30px;margin-top: 10px; width: 200px; float: right;">
                     <span style="background:blue;" class="span1">&nbsp</span>
                     <span id="excelSpan">同行会员</span>
                  </div>
                 <div style="height:30px;margin-top: 10px; width: 200px; float: right;">
                     <span style="background:red;" class="span1">&nbsp</span>
                     <span id="excelSpan">普通会员</span>
                  </div>
      		<div class="tab-body">
       			 <br />
                <input type="hidden" id="attach_id" value="${attachService.attach_id}">
                <input type="hidden" id="service_name_bk" value="${attachService.service_name}">
                 <div style="height: 40px;">
                    <div  class="label"><span><strong>增值服务名称:</strong></span></div>
                    <div class="text_field">
                       <input type="text" class="input" id="service_name" name="service_name"  readonly="readonly" value="${attachService.service_name}"  />
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="serviceNameTextArea"></span>
                    </div>
                 </div>
                 
                    <c:forEach items="${memberRates}" var="pojo">
                    	<c:if test="${pojo.isdeleted == 0}">
                 <div style="height: 40px;">
                    <div  class="label">
	                    <span><strong>
						<c:if test="${pojo.rate_type == 1}">
							<font color="red">${pojo.rate_name}</font>
						</c:if>
						<c:if test="${pojo.rate_type == 2}">
							<font color="blue">${pojo.rate_name}</font>
						</c:if>	
						服务价:</strong></span>
				  </div>
                    
                    <div class="text_field">
                       <input type="text" class="input service_price"  id="${pojo.rate_id}"
                    <c:forEach items="${memberAttachs}" var="atpojo" >
                       <c:if test="${atpojo.memberRateAttach.rate_id==pojo.rate_id}">
                       		value="${atpojo.service_price}"
                      </c:if>
                    </c:forEach>
                            onkeyup="value=value.replace(/[^\d.]/g,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;">
                       <span class="unit">(单位:元)</span>
                    </div>
                    <div class="percentTextArea" style="float:left;margin-top:5px;color: red"></div>
                 </div>
                 		</c:if>
                   </c:forEach>
                 <div style="height: 100px;">
                    <div  class="label"><span><strong>描述信息:</strong></span></div>
                    <div class="text_field">
                      <textarea class="input" rows="3" cols="50" id="description" name="description">${attachService.description}</textarea>
                    </div>
                  </div>
                  <div class="padding border-bottom" >
                    <input type="button" class="button bg-main" onclick="save()" value="保存" />
                    <input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="取消" />
                 </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
            $('#tariff_type').focus();
        });

        function checkVal(){
            $("input[class='service_price'][type='text']").each(function(i,input){
            	var self=$(this).parent().parent();
            	if($(this).val()==null||$(this).val().trim()==""){
            		//如果为空，则补上0
            	   $(this).val(0);
                }
            });
        }
        
        function save(){
            var url = "${backServer}/attachService/updateAttachService";
            var attach_id=$('#attach_id').val();
            var service_name =$('#service_name').val();
            var service_price =0; 
            var description =$('#description').val();
            checkVal();
            var ratePriceList={};
            $("input[class='input service_price'][type='text']").each(function(i,input){
            	ratePriceList[$(this).attr('id')]=$(this).val();
            });
       
            $.ajax({
                url:url,
                data:{"attach_id":attach_id,
                    "service_name":service_name,
                    "service_price":service_price,
                    "description":description,
                    "ratePriceList":JSON.stringify(ratePriceList)},
                type:'post',
                dataType:'json',
                async:false,
                success:function(data){
                	if(data['result']){
                		 layer.alert("保存成功！");
                         window.location.href = "${backServer}/attachService/attachServiceInit";
                	}else{
                		layer.alert("保存失败，请检查数据是否正确");	
                	}
                }
            });
        }
    </script>
</body>
</html>