<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>
<style>
 
    .span1{
        display:-moz-inline-box;
        display:inline-block;
        width:20px;
        height:25px;
    }
</style>
<body>

	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>增值服务添加</strong>
				
				<div style="height:30px;margin-bottom: 10px; width: 200px; float: right;">
                     <span style="background:blue;" class="span1">&nbsp</span>
                     <span id="excelSpan">同行会员</span>
                  </div>
                 <div style="height:30px;margin-bottom: 10px; width: 200px; float: right;">
                     <span style="background:red;" class="span1">&nbsp</span>
                     <span id="excelSpan">普通会员</span>
                  </div>
			</div>
			<div class="tab-body">
				<br /> <input type="hidden" id="checkServiceName" value="1">
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>增值服务名称:</strong> </span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<input type="text" class="input" id="service_name"
							name="service_name" style="width: 200px; margin-left: 30px;"
							onblur="checkServiceName()" />
					</div>
					<div
						style="float: left; margin-top: 5px; margin-left: 10px; width: 200px;">
						<span id="serviceNameTextArea"></span>
					</div>
				</div>



				<c:forEach items="${memberRates}" var="pojo" varStatus="vsPojo">
					<c:if test="${pojo.isdeleted == 0}">
						<div>
							<div style="height: 40px;">
								<c:if test="${vsPojo.count > 1}">
									<div
										style="float: left; margin-top: 5px; margin-left: 10px; width: 120px;"
										class="label">
								</c:if>
								<c:if test="${vsPojo.count == 1}">
									<div
										style="float: left; margin-top: 5px; margin-left: 10px; width: 120px;"
										class="label">
								</c:if>
								<span><strong>
<c:if test="${pojo.rate_type == 1}">
	<font color="red">${pojo.rate_name}</font>
</c:if>
<c:if test="${pojo.rate_type == 2}">
	<font color="blue">${pojo.rate_name}</font>
</c:if>	
								的价格:</strong> </span>
							</div>

							<div style="float: left; margin-left: 10px; width: 120px;">
								<input type="text" class="input" class="input service_price" 
									style="width: 115px" value="0" name="service_price_ary"
									onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /> 
								<input
									type="hidden" class="rate_id" value="${pojo.rate_id}" />
							</div>
							<div style="float: left; margin-top: 5px;">
								<span>元</span>
							</div>

							<div style="float: left; margin-top: 5px; margin-left: 10px; width: 200px;">
								<span id="servicePriceTextArea"></span>
							</div>

						</div>
					</c:if>
				</c:forEach>
				<div style="height: 100px;">
					<div style="float: left; margin-top: 5px; margin-left: 10px"
						class="label">
						<span><strong>描述信息:</strong> </span>
					</div>
					<div style="float: left; margin-left: 68px; width: 250px">
						<textarea class="input" rows="3" cols="50" id="description"
							name="description"></textarea>
					</div>
				</div>

				<div class="padding border-bottom">
					<input type="button" class="button bg-main" onclick="add()"
						value="保存" /> <input type="button" class="button bg-main"
						onclick="javascript:history.go(-1)" value="取消" />
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#tariff_type').focus();
		});

		//增值服务名称文本框鼠标失去焦点，校验增值服务名称是否已经存在
		function checkServiceName() {
			var url = "${backServer}/attachService/checkServiceName";
			var service_name = $('#service_name').val();

			if (service_name == null || service_name.trim() == "") {
				return;
			}
			$.ajax({
				url : url,
				type : 'post',
				data : {
					"service_name" : service_name
				},
				success : function(data) {
					$('#checkServiceName').val(data);
					if (data == "1") {

						$('#serviceNameTextArea').text('增值服务名称可以使用!');
						$('#serviceNameTextArea').css('color', 'green');
					} else {
						$('#serviceNameTextArea').text('增值服务名称已经存在!');
						$('#serviceNameTextArea').css('color', 'red');
					}
				}

			});
		}

		function add() {
			var url = "${backServer}/attachService/insertAttachService";

			var checkServiceName = $('#checkServiceName').val();

			var service_name = $('#service_name').val();
			var service_price = $('#service_price').val();
			var description = $('#description').val();

			var checkResult = "0"
			if (service_name == null || service_name.trim() == "") {
				$('#serviceNameTextArea').text('增值服务名称不能为空!');
				$('#serviceNameTextArea').css('color', 'red');
				checkResult = "1";
			}

			var resultPrice = "";
			var errorflag="0";
			$("input[name='service_price_ary'][type='text']").each(
					function(i) {
						var priceVal = $(this).val();
						var exp =/^\d+(\.{0,1}\d+){0,1}$/;
						if(exp.test(priceVal)){
							resultPrice += priceVal + ",";
						}else{
							errorflag="1";
						}

					});
			if(errorflag=="1"){
				alert("增值服务的各种价格都得大于等于0!");
                return;
			}
			var resultRateId = "";
			$("input[class='rate_id'][type='hidden']").each(function(i) {
				resultRateId += $(this).val() + ",";
			});

			if (checkResult == "1") {
				return;
			}
			if (checkServiceName != "1") {
				alert("增值服务名称已经存在!");
				return;
			}
			$
					.ajax({
						url : url,
						data : {
							"service_name" : service_name,
							"resultRateId" : resultRateId,
							"resultPrice" : resultPrice,
							"service_price" : service_price,
							"description" : description
						},
						type : 'post',
						dataType : 'text',
						success : function(data) {
							alert("添加成功！");
							window.location.href = "${backServer}/attachService/attachServiceInit";
						}
					});
		}
	</script>
</body>
</html>