<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>


<style>
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
	}
</style>
<body>
<div class="admin">
	 <div class="padding border-bottom" id="findDisplayDiv" style="width: 90%; height: 100px;">
		<div style="float:left;height: 32px; margin-left: 10px;">
			<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;用户&nbsp;</label>
			<input type="text" class="input"  
				id="user_name" name="user_name" style="width:200px;margin-left: 10px;float:left;" 
				value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
			
			<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;包裹&nbsp;</label>
			<input type="text" class="input"  
				id="logistics_code" name="logistics_code" style="width:200px;margin-left: 10px;float:left;" 
				value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>

			<div style="float: left;width: 350px;">
				<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;有效期&nbsp;</label>
				<input type="text" class="input" style="width:100px;float:left;margin-left: 10px;"
					id="shareTimeStrat" name="shareTimeStrat"  
					value=""  
					onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'shareTimeEnd\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
				<span style="line-height: 30px; float:left; text-align: center; margin-left: 8px;">到</span>
				<input type="text" class="input"  style="width:100px;float:left;margin-left: 8px"
					id="shareTimeEnd" name="shareTimeEnd"  
					value=""  
					onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'shareTimeStrat\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
			</div>
		 </div><div style="float:left;height: 32px;margin-left: 10px; margin-top: 10px;">
			<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;赠送状态&nbsp;</label>
			<select id="status" name="status" class="input" style="width:100px;float:left;margin-left: 10px;">
				<option value="-1" selected="selected">是否赠送</option>
				<option value="2">已赠送</option>
				<option value="1">未赠送</option>
			</select>
			<div style="float: left;width: 310px;">
				<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;获取数目&nbsp;</label>
				<input type="text" class="input" style="width:100px;float:left;margin-left: 10px;"
					id="couponNumStrat" name="couponNumStrat"  
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
				<span style="line-height: 30px; float:left; text-align: center; margin-left: 8px;">到</span>
				<input type="text" class="input"  style="width:100px;float:left;margin-left: 8px;"
					id="couponNumEnd" name="couponNumEnd"  
					value="" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"/>
			</div>
			
			<label for="name" style="float:left;line-height: 30px;text-align: center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;是否通过&nbsp;</label>
			<select id="approval_status" name="approval_status" class="input" style="width:110px;float:left;margin-left: 10px;">
				<option value="-1" selected="selected">是否已通过</option>
				<option value="2">通过</option>
				<option value="3">拒绝</option>
				<option value="1">申请</option>
			</select>
			<div class="form-button" style="float:left;margin-left: 20px;">
				<a href="javascript:void(0);" class="button bg-main" 
					onclick="queryAll()">查询</a></div>
		</div>
	</div>
 	<div class="panel admin-panel">
		<div class="panel-head" style="height: 50px;">
			<strong style="float:left;text-align: center;margin-left: 10px;">晒单列表</strong>
		</div>
		<table class="table table-hover">
			<tr><th>用户名</th>
			<th>用户账号</th>
				<th>包裹</th>
				<th>时间</th>
				<th>优惠券</th>
				<th>赠送数目</th>
				<th>审核状态</th>
				<th>操作</th>
			</tr>
			<c:forEach var="list"  items="${shareLists}">
			<tr><td>${list.user_name }</td>
			<td>${list.account }</td>
				<td>${list.logistics_code }</td>
				<td>${list.share_time }</td>
				<td>
					<c:if test="${list.status == 1 }">
						未赠送
					</c:if><c:if test="${list.status == 2 }">
						已赠送
					</c:if></td>
				<td>${list.coupon_num }</td>
				<td>
					<c:if test="${list.approval_status==1 }">未审核</c:if>
					<c:if test="${list.approval_status==2 }">审核通过</c:if>
					<c:if test="${list.approval_status==3 }">审核拒绝</c:if></td>
				<td>
				<shiro:hasPermission name="share:select">
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="upd('${list.share_id }')">查看详情</a>&nbsp;&nbsp;
				</shiro:hasPermission><shiro:hasPermission name="share:edit">
					<c:if test="${list.approval_status==2 }">
					<a disabled="disabled" class="button border-blue button-little" href="javascript:void(0);" 
	      				onclick="updOne('${list.share_id }')">审核</a>&nbsp;&nbsp;
	      			</c:if><c:if test="${list.approval_status!=2 }">
					<a class="button border-blue button-little" href="javascript:void(0);" 
	      				onclick="updOne('${list.share_id }')">审核</a>&nbsp;&nbsp;
	      			</c:if>
	      		</shiro:hasPermission>
	      		<c:if test="${list.approval_status!=2 }">
	      		<shiro:hasPermission name="share:delete">
					<a class="button border-blue button-little" href="javascript:void(0);" 
						onclick="del('${list.share_id }')">删除</a>&nbsp;&nbsp;
				</shiro:hasPermission>
				</c:if>
				<c:if test="${list.canCancelAward }">
					<a class="button border-blue button-little" href="javascript:void(0);" 
	      				onclick="cancelAward('${list.share_id }+','+${list.award_coupon_id }')">取消赠送</a>&nbsp;&nbsp;
	      			</c:if>
				</td>
			</tr>	
			</c:forEach>
		</table>
		<div class="panel-foot text-center">
			<jsp:include page="webfenye.jsp"></jsp:include>
		</div>
	</div>
</div>
</body>
<script type="text/javascript"> 
	function upd(shareId){	
		var url = "${backServer}/share/toUpdateShare?share_id="+shareId;
		window.location.href = url;
	}
	function updOne(shareId){	
		var url = "${backServer}/share/toUpdateShareOne?share_id="+shareId;
		window.location.href = url;
	}
	function queryAll(){
		var couponNumStrat = $('#couponNumStrat').val();
		if(null == couponNumStrat || "" == couponNumStrat){
			couponNumStrat = -1;
		}
		var couponNumEnd = $('#couponNumEnd').val();
		if(null == couponNumEnd || "" == couponNumEnd){
			couponNumEnd = -1;
		}
		var url = "${backServer}/share/queryAllnew?user_name="+$('#user_name').val()+
				"&logistics_code="+$('#logistics_code').val()+
				"&status="+$('#status').val()+
				"&couponNumStrat="+couponNumStrat+
				"&couponNumEnd="+couponNumEnd+
				"&shareTimeStrat="+$('#shareTimeStrat').val()+
				"&shareTimeEnd="+$('#shareTimeEnd').val()+
				"&approval_status="+$('#approval_status').val();
		window.location.href = url;
	}
	function del(shareId){
		if(confirm("确认删除吗？")){
			var url = "${backServer}/share/deleteShare?share_id="+shareId;
			$.ajax({
				url:url,
				type:'GET',
				success:function(data){
					 alert("删除成功！");
					 window.location.href = "${backServer}/share/queryAll";
				}
			});
		}
	}
	
	function cancelAward(share_id,award_coupon_id){
		if(confirm("确认取消赠送吗？")){
			var url = "${backServer}/share/cancelAward?share_id="+share_id+"&award_coupon_id="+award_coupon_id;
			$.ajax({
				url:url,
				type:'GET',
				success:function(data){
					 alert("取消成功！");
					 window.location.href = "${backServer}/share/queryAll";
				}
			});
		}
	}
</script>
</html>