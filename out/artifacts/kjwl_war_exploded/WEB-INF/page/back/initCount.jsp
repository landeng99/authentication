<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<% String path = request.getContextPath(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- 分页 -->
	<link rel="stylesheet" href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css"> 
	<link type="text/css" rel="stylesheet" href="<%=path%>/resource/css/pushCount.css"> 
	<script src="http://apps.bdimg.com/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<%-- <script type="text/javascript" src="<%=path%>/resource/js/jquery.zclip.min.js"></script> --%>
	<script type="text/javascript" src="<%=path%>/resource/js/jquery-1.8.0.js"></script>
</head>
<body>
<!-- <div class="tabbable"> -->
	<div class="tab-content"> 
	<!-- 推广统计查询  -->
    <!-- <div class="tab-pane fade active" id="count">  -->
    	<form id="form" action="${backServer}/pushLink/search" method="post" class=" form-horizontal"> 
    	  <fieldset>
    	  <legend></legend>
		    <div class="control-group info pull-left"> 
		        <label class="col-sm-4 control-label" for="businessName" >业务名：</label> 
		        <div class="col-sm-8 controls"> 
		            <input type="text" class="form-control" name="businessName" id="businessName" placeholder="请输入..."> 
		        </div> 
		    </div> 
		    <div class="control-group info pull-left"> 
		        <label class="col-sm-4 control-label" for="user_name">客户名：</label>
		        <div class="col-sm-8 controls"> 
		            <input type="text" class="form-control" name="user_name" id="user_name" placeholder="请输入..."> 
		        </div> 
		    </div> 
		    <div class="control-group info pull-left"> 
		        <label class="col-sm-4 control-label" for="account">客户账号：</label> 
		        <div class="col-sm-8 controls"> 
		            <input type="text" class="form-control" name="account" id="account" placeholder="请输入..."> 
		        </div> 
		    </div> 
		    <div class="control-group info pull-left">
			    <label class="col-sm-4 control-label" for="inputIn"></label> 
			    <div class="col-sm-8 controls">
				  	<input type="submit" class="btn btn-info btn-large btn-primary"><!-- <i class="glyphicon glyphicon-search">查询</i> -->
				</div>
			</div>
		</fieldset>
	  </form>
	<!-- 推广统计表格 -->
	<table class="table table-striped table-bordered table-hover table-responsive "> 
	    <caption></caption> 
	    <thead > 
	        <tr class="success"> 
	            <th>业务名</th> 
	            <th>注册客户</th> 
	            <th>客户名</th> 
	            <th>注册日期</th> 
	        </tr> 
	    </thead>  
	    <tbody>
	    <c:forEach var="count" items="${countList}">
	        <tr> 
	            <td align="center">${count.businessName}</td> 
	            <td align="center">${count.email}</td> 
	            <td align="center">${count.user_name}</td> 
	            <td align="center">${count.register_time}</td> 
	        </tr> 
	    </c:forEach>
	    </tbody>
	 </table> 
  <!--   </div>-->
 	</div> 
<script type="text/javascript">

</script>
</body>
</html>