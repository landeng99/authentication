<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>
<div class="admin">
	<table class="table table-hover">
		<thead>
			<tr>
				<th colspan="7">公司运单号:${packageCode}</th>
			</tr>
			<tr>
				<th>商品名称</th>
				<th>品牌</th>
				<th>商品申报类型</th>
				<th>单价</th>
				<th>数量</th>
				<th>规格</th>
				<th>关税金额</th>
			</tr>
		</thead>
		<tbody>
			<form id="goodsForm" action="${backServer}/taxPkgGoods/updatePkGoodsByList" method="post">
			<input type="hidden" name="logistics_code" value="${packageCode}"/>
			<input type="hidden" name="packageId" value="${packageId}"/>
				<c:forEach var="list" items="${taxPkgGoodsPojo}" varStatus="status">
					<tr>
						<td><input type="hidden" name="goods[${ status.index}].goodsId" value="${list.goods_id}"/><input type="hidden" name="goods[${ status.index}].goodsName" value="${list.goods_name}"/>${list.goods_name}</td>
						<td><input type="hidden" name="goods[${ status.index}].brand" value="${list.brand}"/>${list.brand}</td>
						<td><input type="hidden" name="goods[${ status.index}].goodsTypeId" value="${list.goods_type_id}"/>${list.goods_type_id}</td>
						<td><input type="hidden" name="goods[${ status.index}].price" value="${list.price}"/>${list.price}</td>
						<td><input type="hidden" name="goods[${ status.index}].quantity" value="${list.quantity}"/>${list.quantity}</td>
						<td><input type="hidden" name="goods[${ status.index}].spec" value="${list.spec}"/>${list.spec}</td>
						<td><input type="number" name="goods[${ status.index}].taxPaymentTotal" value="${list.tax_payment_total}"/></td>
					</tr>
				</c:forEach>
			</form>
		</tbody>
	</table>
	 <div class="form-button"><a href="javascript:void(0);" class="button bg-main" onclick="save()">提交</a>
		<a href="javascript:history.go(-1);" class="button bg-main">返回</a>
	</div>
</div>

<script type="text/javascript">
	function save(){
		$("#goodsForm").submit();
	}
</script>
</body>