<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<style type="text/css">
    .span1{
        display:-moz-inline-box;
        display:inline-block;
        width:25px;
        height:25px;
    }
</style>
<body>
<div class="admin">
	<div class="tab">
		<div class="tab-head">
			<ul class="tab-nav">
				<li class="active"><a href="#tab-set">关税清单</a>
				</li>
				<li><a href="#tab-two">审核清单</a>
				</li>
			</ul>
		</div>
		<div class="tab-body">
			<div class="tab-panel active" id="tab-set">
				<div style="height: 80px;">
					<div class="label">
						<label for="excel">导入EXCEL[关税清单]文件:</label>
					</div>
					<div class="field">
						<input type="file" id="excel" name="excel" />
					</div>
				</div>
				<div class="form-button">
					<a href="javascript:void(0);" class="button bg-main" id="import">导入</a>
				</div>
				
				<div id="excelDiv" style="display: none; margin-top: 20px;">
					<div id="excelDivDownload"  style="color:#FF0000;height: 40px;"></div>
					
					<div style="height:30px;margin-bottom: 10px;">
						<!-- <span>excel用</span> -->
                        <span style="background:red;" class="span1">&nbsp</span>
                        <span id="excelSpan">标记文件出错位置,括号里的是出错原因!</span>
                     </div>
                     
                     <div style="height:30px; display: none;" id="excelMassage2">
                        <span style="color:##FF00D5;">错误原因:税单号已在系统中的包裹里使用</span>
                     </div>
                     
                     <div style="height:30px; display: none;" id="excelMassage3">
                        <span style="color:##FF00D5;">错误原因:税单号出现在多个包裹中</span>
                     </div>
                     
                     <div style="height:30px; display: none;" id="excelMassage4">
                        <span style="color:##FF00D5;">错误原因:在包裹中出现的多个税单号</span>
                     </div>
                     
                     <div style="height:30px; display: none;" id="excelMassage5">
                        <span style="color:##FF00D5;">错误原因:包裹编号在系统中不存在</span>
                     </div>
				</div>
				
				<div id="excelFileDiv" style="display: none; margin-top: 20px;">
					<span>请导入正确的文件内容</span>
				</div>
			</div>

			<div class="tab-panel" id="tab-two">
				<div style="height: 80px;">
					<div class="label">
						<label for="excel">导入EXCEL[审核清单]文件:</label>
					</div>
					<div class="field">
						<input type="file" id="excelone" name="excelone" />
					</div>
				</div>
				<div class="form-button">
					<a href="javascript:void(0);" class="button bg-main"
						id="importone">导入</a>
				</div>
				
				
				<div id="exceloneDiv" style="display: none; margin-top: 20px;">
					<div id="exceloneDivDownload"  style="color:#FF0000;height: 40px;"></div>
					
					<div style="height:30px;margin-bottom: 10px;">
						<!-- <span>excel用</span> -->
                        <span style="background:red;" class="span1">&nbsp</span>
                        <span id="exceloneSpan">标记文件出错位置,括号里的是出错原因!</span>
                     </div>
                     
                     <div style="height:30px; display: none;" id="exceloneMassage2">
                        <span style="color:##FF00D5;">错误原因:税单号系统中已存在</span>
                     </div>
                     
                     <div style="height:30px; display: none;" id="exceloneMassage3">
                        <span style="color:##FF00D5;">错误原因:税单号出现在多个包裹中</span>
                     </div>
                     
                     <div style="height:30px; display: none;" id="exceloneMassage4">
                        <span style="color:##FF00D5;">错误原因:同一包裹出现多个税单号</span>
                     </div>
                     
                     <div style="height:30px; display: none;" id="exceloneMassage5">
                        <span style="color:##FF00D5;">错误原因:包裹单号在系统中不存在</span>
                     </div>
				</div>
				
				<div id="exceloneFileDiv" style="display: none; margin-top: 20px;">
					<span>请导入正确的文件内容</span>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$('#excel').focus();
    $('#import').click(function(){
        var url = "${backServer}/taxPkgGoods/excel";
        $.ajaxFileUpload({
            url : url,
            type : 'post',
            secureuri : false,
            fileElementId : 'excel',
            dataType : 'text',
            success : function(data) {
            	if(data == "1"){ 
                	layer.alert("导入成功");
                	window.location.href = "${backServer}/taxPkgGoods/queryAllOne";	
            	}
            	/* else{
            		var biao = data.substr(0,1);
            		var path = data.substr(1);
            		var cuo = path.substr(0,1);
            		if("/" == cuo){
	                    $('#excelFileDiv').hide();
	                    
	            		$('#excelDivDownload').empty();
	                    var a = "<a style='color:blue;' href='"+"${resource_path}"+path+"'>点击此处查看错误信息</a>";
	                    $('#excelDivDownload').append(a);
	                    
	                    $('#excelDiv').show();
	                    
	            		if(biao == "2"){
	            			$('#excelSpan').html("税单号已在系统中的包裹里使用");
	            			alert("税单号已在系统中的包裹里使用");
	            	 
	            		}else if(biao == "3"){
	            			$('#excelSpan').html("税单号出现在多个包裹中");
	            			alert("税单号出现在多个包裹中");
	            		 
	    	        	}else if(biao == "4"){
	            			$('#excelSpan').html("在包裹中出现的多个税单号");
	    	        		alert("在包裹中出现的多个税单号");
	             
	    	        	}else if(biao == "5"){
	            			$('#excelSpan').html("包裹的运单号在系统中不存在");
	    	        		alert("包裹的运单号在系统中不存在");
	            		 
	    	        	} else if(biao == "6"){
	            			$('#exceloneSpan').html("包裹的审核状态存在重复");
	    	        		alert("包裹的审核状态存在重复");
	             
	    	        	} else if(biao == "9"){
	            			$('#exceloneSpan').html("包裹的内件不全或者内件名称不对");
	    	        		alert("包裹的内件不全或者内件名称不对");
	            		 
	    	        	} 

	                    
            		}  */
            		 else{
            			$('#excelDiv').hide();
            			$('#excelFileDiv').show();
            			alert("文件类型错误！请选择Excel文件");
            		} 
            	//}
           },
            error : function(data, status, e) {
                alert("error" + e);
            }
        });
    }); 
    
    
    $('#excelone').focus();
    $('#importone').click(function(){
        var url = "${backServer}/taxPkgGoods/excelone";
        $.ajaxFileUpload({
            url : url,
            type : 'post',
            secureuri : false,
            fileElementId : 'excelone',
            dataType : 'text',
            success : function(data) {
         		 
        		var jsonData=$.parseJSON(data);
        	 
            	if(jsonData['result'] == true){
                	layer.alert("导入成功");
                	window.location.href = "${backServer}/taxPkgGoods/queryAllOne";	
            	}else{
            	 
            			var path = jsonData['path'];
            	 
	                    $('#exceloneFileDiv').hide();
	                    
	            		$('#exceloneDivDownload').empty();
	                    var a = "<a style='color:blue;' href='"+"${resource_path}"+path+"'>点击此处查看错误信息</a>";
	                    $('#exceloneDivDownload').append(a);
	                    
	                    $('#exceloneDiv').show();
	                    
	                    //errorType: 1单号为空，2状态为空，3单号重复，4，单号不存 错误;
	                    var errorType=jsonData['errorType'];
	                    if(errorType == "1"){
	            			$('#exceloneSpan').html("公司运单号不能为空");
	            			layer.alert("公司运单号不能为空");
	            
	            		}else if(errorType == "2"){
	            			$('#exceloneSpan').html("税单状态不能为空");
	            			layer.alert("税单状态不能为空");
	            		 
	    	        	}else if(errorType == "3"){
	            			$('#exceloneSpan').html("运单号不能重复");
	    	        		layer.alert("运单号不能重复");
	            	 
	    	        	}else if(errorType == "4"){
	            			$('#exceloneSpan').html("运单号不正确");
	    	        		layer.alert("导入的运单号正确");
	            	 
	    	        	}else {
	            			$('#exceloneSpan').html(errorType);
	    	        		layer.alert(errorType);
	    	        	} 
	               
            	}
            },
            error : function(data, status, e) {
                alert("error" + e);
            }
        });
    }); 
});

function tis(){
	
}

</script>
</body>