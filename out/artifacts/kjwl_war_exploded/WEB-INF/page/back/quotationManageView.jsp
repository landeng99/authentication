<%@ page language="java" pageEncoding="utf-8"%>
<%@include file="header.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
    .span1{
        display:-moz-inline-box;
        display:inline-block;
        width:20px;
        height:25px;
    }
	.hided{display: none;}
	.hided1{display: block;}
</style>
<script type="text/javascript">
$(function(){
	/* 修改渠道部分数据不能回显得问题 */
	/* 修改渠道部分数据不能回显得问题 */
	function TypeOfPrice(){     
		if($("#account_type").find("option:selected").attr("grade")==3){
						$("#see").removeClass().addClass("hided");
		 }
		
	}
	TypeOfPrice();
	
});

</script>
<body>
	<div class="admin">
		<div class="panel admin-panel">
			<div class="panel-head">
				<strong>报价单查看</strong>
			</div>
			<div class="tab-body">
				<br /> <input type="hidden" id="checkServiceName" value="1">
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>报价单名称:</strong> </span>
					</div>
					<div style="float: left; margin-left: 10px;">
						<input type="text" class="input" id="quota_name"
							name="quota_name" style="width: 200px; margin-left: 30px;"
							onblur="checkServiceName()" value='${qm.quota_name}' disabled/>
					</div>
				</div>
				
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>仓库:</strong> </span>
					</div>
					<div style="float: left; margin-left: 80px;">
						<select  class="input" id="osaddr_id" name="osaddr_id" style="width:150px" disabled="disabled">
	             		  <option value="">--选择仓库--</option>
		              		<c:forEach var="userOverseasAddress" items="${overseasAddressList}">
		              			<option value="${userOverseasAddress.id}" <c:if test="${qm.osaddr_id == userOverseasAddress.id}">selected="selected"</c:if> >${userOverseasAddress.warehouse}</option>
		              		</c:forEach>
	             		</select>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">
						<span><strong>用户类型:</strong> </span>
					</div>
					<div style="float: left; margin-left: 20px;">
						 <select name="user_type" class="input" style="width:150px" id="user_type"  disabled>
						 	<option value="${qm.user_type}" <c:if test="${qm.user_type == 1}">selected="selected"</c:if> >普通用户</option>
						 	<option value="${qm.user_type}" <c:if test="${qm.user_type == 2}">selected="selected"</c:if> >同行用户</option>
	                    </select>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label" >
						<span><strong>选择渠道:</strong> </span>
					</div>
					<div style="float: left; margin-left: 30px;" class="" >
						 <select name="express_package" id = "express_package" class="input" style="width:150px" disabled>
                             <option value="${qm.express_package}" <c:if test="${qm.express_package == 0}">selected="selected"</c:if> >默认</option>
                             <option value="${qm.express_package}" <c:if test="${qm.express_package == 1}">selected="selected"</c:if> >A渠道</option>
                             <option value="${qm.express_package}" <c:if test="${qm.express_package == 2}">selected="selected"</c:if> >B渠道</option>
                             <option value="${qm.express_package}" <c:if test="${qm.express_package == 3}">selected="selected"</c:if> >C渠道</option>
                             <option value="${qm.express_package}" <c:if test="${qm.express_package == 4}">selected="selected"</c:if> >D渠道</option>
                             <option value="${qm.express_package}" <c:if test="${qm.express_package == 5}">selected="selected"</c:if> >E渠道</option>
                         </select>
					</div>
				</div>
				
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>报价类型:</strong> </span>
					</div>
					<div style="float: left; margin-left:50px;">
						<select name="account_type" id="account_type"  class="input" style="width:150px" disabled>
							<option value="${qm.account_type}" <c:if test="${qm.account_type == 0}">selected="selected"</c:if> grade="3">默认报价</option>
							<option value="${qm.account_type}" <c:if test="${qm.account_type == 1}">selected="selected"</c:if> grade="4">特殊报价</option>
	                    </select>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">
						<span><strong>计量单位:</strong> </span>
					</div>
					<div style="float: left; margin-left: 20px;">
						 <select name="unit" id = "unit" class="input" style="width:150px" disabled>
						 	<option value="${qm.unit}" <c:if test="${qm.unit == 1}">selected="selected"</c:if> >g(克)</option>
						 	<option value="${qm.unit}" <c:if test="${qm.unit == 2}">selected="selected"</c:if> >kg(千克)</option>
						 	<option value="${qm.unit}" <c:if test="${qm.unit == 3}">selected="selected"</c:if> >lb(磅)</option>
	                    </select>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">
						<span><strong>是否含税:</strong> </span>
					</div>
					<div style="float: left; margin-left: 30px;">
						<select id="isHaveTax" name="isHaveTax" class="input" style="width:150px" disabled>
							<option value="${qm.isHaveTax}" <c:if test="${qm.isHaveTax == 1}">selected="selected"</c:if> >含税</option>
							<option value="${qm.isHaveTax}" <c:if test="${qm.isHaveTax == 0}">selected="selected"</c:if> >不含税</option>
                     	</select>
					</div>
				</div>
				
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>首重:</strong> </span>
					</div>
					<div style="float: left; margin-left: 50px;">
						<input type="text" class="input" id="first_weight"
							name="first_weight" style="width: 150px; margin-left: 30px;" value="${qm.first_weight}" disabled/>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">
						<span><strong>进位:</strong> </span>
					</div>
					<div style="float: left; margin-left: 15px;">
						<input type="text" class="input" id="carry"
							name="carry" style="width: 150px; margin-left: 30px;" value="${qm.carry}" disabled/>
					</div>
				</div>
				
				<div style="height: 40px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>首重单价:</strong> </span>
					</div>
					<div style="float: left; margin-left: 50px;">
						<input type="text" class="input" id="first_weight_price"
							name="first_weight_price" style="width: 150px; margin-left: 30px;" value="${qm.first_weight_price}" disabled/>
					</div>
					
					<div style="float: left; margin-top: 5px; margin-left: 180px"
						class="label">续重单价:</strong> </span>
					</div>
					<div style="float: left; margin-left: 15px;">
						<input type="text" class="input" id="go_weight_price"
							name="go_weight_price" style="width: 150px; margin-left: 30px;" value="${qm.go_weight_price}" disabled/>
					</div>
				</div>

				<div style="height: 100px;">
					<div style="float: left; margin-top: 5px; margin-left: 30px"
						class="label">
						<span><strong>描述信息:</strong> </span>
					</div>
					<div style="float: left; margin-left: 50px; width: 250px">
						<textarea class="input" rows="3" cols="50" id="description" 
							name="description" disabled>${qm.description}</textarea>
					</div>
				</div>
				
					<!-- 只对以下客户适用 -->
				<div style="height: 40px;margin-bottom:10px" id="see">
                    <div style="float:left;margin-top:5px; margin-left:30px" class="label"><span><strong>只对以下客户适用:</strong></span></div>
                    <div style="float:left;margin-left:18px;">
						<textarea rows="6" cols="80" readonly="readonly"  onclick="addGroupMember()" id="authAccount" name="authAccount" disabled>${authAccount}</textarea>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px;">
                       <span id="taxTextArea"></span>
                    </div>
                 </div><br>

				<div class="padding border-bottom" style="clear:both;margin-left:20px">
					<input type="button" class="button bg-main" onclick="javascript:history.go(-1)" value="返回" />
				</div>
				
			</div>
		</div>
	</div>

</body>
</html>