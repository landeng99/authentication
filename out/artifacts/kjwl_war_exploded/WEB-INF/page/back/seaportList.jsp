<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>

<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>口岸列表</strong></div>
      <div class="tab-body">
        <br />
            <form action="${backServer}/seaport/seaportSearch" method="post" id="myform" name="myform">
                 <div style="height: 40px;">
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>口岸分类:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <select id="stype" name="stype" class="input" style="width:150px">
                            <c:forEach var="stype"  items="${stypeList}">
                               <option value="${stype}">${stype}</option>
                            </c:forEach>
                         </select>
                    </div>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>口岸名称:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="sname" name="sname" style="width:150px"
                                onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px"><span><strong>口岸编码:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                        <input type="text" class="input" id="scode" name="scode" style="width:150px"
                               onkeyup="value=value.replace(/[^\w\.\/]/ig,'')"/>
                    </div>
                 </div>
            </form>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" onclick="search()" value="查询" />
                <shiro:hasPermission name="seaport:add">
                    <input type="button" class="button button-small border-green" onclick="add()" value="添加口岸"/>
                </shiro:hasPermission>
                 </div>
                 <table class="table">
                    <tr>
                        <th width="10%">口岸分类</th>
                        <th width="20%">口岸名称</th>
                        <th width="15%">口岸编码</th>
                         <th width="15%">号码段使用</th>
                        <th width="20%">限制用户信息数</th>
                        <th width="20%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="seaport"  items="${seaportList}">
                     <tr>
                         <td width="10%">${seaport.stype}</td>
                         <td width="20%">${seaport.sname}</td>
                         <td width="15%">${seaport.scode}</td>
                         <td width="15%">${seaport.usedCount}/${seaport.totalCount}</td>
                         <td width="20%">${seaport.userinfo_limit}</td>
 
                         <td width="20%">
                         <shiro:hasPermission name="seaport:select">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="detail('${seaport.sid}')">编辑</a>
                         </shiro:hasPermission><shiro:hasPermission name="seaport:delete">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="del('${seaport.sid}')">删除</a>
                         </shiro:hasPermission>
                         </td>
                         </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
            $('#stype').focus();
        });
        
        function detail(sid){
            var url = "${backServer}/seaport/seaportDetail?sid="+sid;
            window.location.href = url;
        }

        function search(){
            document.getElementById('myform').submit();
        }
        
        function add(){
            var url = "${backServer}/seaport/addSeaport";
            window.location.href = url;
        }
        
        function del(sid){
            
            if(confirm("是否要删除该条记录？")){
                 var url = "${backServer}/seaport/deleteSeaport"
                 $.ajax({
                  url:url,
                  data:{
                      "sid":sid},
                  type:'post',
                  dataType:'text',
                  success:function(data){
                     alert("删除成功");
                     window.location.href = "${backServer}/seaport/seaportInit";
                  }
                 });
            }
          }
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search(); 
               }
          }
        });
    </script>
</body>
</html>