<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="renderer" content="webkit">
	<title>修改文章</title>
	<link rel="stylesheet" href="<%=basePath %>resource/css/default2.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>resource/css/prettify.css" type="text/css"/>
	<link rel="stylesheet" href="<%=basePath %>back/css/pintuer.css">
    <link rel="stylesheet" href="<%=basePath %>back/css/admin.css">
	<script charset="utf-8" src="<%=basePath %>resource/js/kindeditor.js"></script>
	<script charset="utf-8" src="<%=basePath %>resource/js/zh_CN.js"></script>
    <script src="<%=basePath %>back/js/My97DatePicker/WdatePicker.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="context"]', {
				uploadJson : '<%=basePath %>kindeditor/uploadImg',
				fileManagerJson : '<%=basePath %>kindeditor/fileMgr',
				allowFileManager : true
			});
		});
	</script>
</head>
<body>
<div class="admin">
	<div class="tab">
	<div class="form-group">
	
<form method="post" action="${backServer}/notice/saveNotice">
	<div style="height: 60px;">
		<div style="float:left;">
			<span for="userName" style=" line-height: 30px;"><strong>公告标题:</strong></span></div>
		<div style="float:left;margin-left:10px;width:300px;">
			<input type="text" 
				id="title" name="title" style="width: 170px;"
				value="${noticePojo.title}" class="input"/></div>
		
		<div style="float:left;">
			<span for="createTime" style=" line-height: 30px;"><strong>公告状态:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<select id="status" name="status" class="input">
		<c:if test="${noticePojo.status == 0}">
				<option value="0" selected="selected">禁用</option>
				<option value="1">启用</option>
		</c:if>	
		<c:if test="${noticePojo.status == 1}">
				<option value="0">禁用</option>
				<option value="1" selected="selected">启用</option>
		</c:if>	
			</select></div>
	</div>
	
	<div style="height: 60px;">
		<div style="float:left;">
			<span for="userName" style=" line-height: 30px;"><strong>生效时间:</strong></span></div>
		<div style="float:left;margin-left:10px;width:300px;">
			<input type="text"
				id="effectTime" name="effectTime" style="width: 170px;"
				onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'timeOut\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
				value="${noticePojo.effectTime}" class="input"/></div>
			
		<div style="float:left;">
			<span for="createTime" style=" line-height: 30px;"><strong>失效时间:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<input type="text"
				id="timeOut" name="timeOut"  style="width: 170px;"
				onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'effectTime\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
				value="${noticePojo.timeOut}" class="input"/></div>
	</div>
	
	<div style="height: 60px;">
		<div style="float:left;">
			<span for="tt_profile" style=" line-height: 30px;"><strong>公告简介:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<textarea rows="3" cols="80"
				id="tt_profile" name="tt_profile">${noticePojo.tt_profile}</textarea>
		</div>
	</div>
	
	<div>
		<div >
			<span for="address" style=" line-height: 30px;"><strong>公告内容:</strong></span></div>
		<div style="float:left;margin-left:10px;">
			<textarea rows="10" cols="80"
				id="context" name="context">${noticePojo.context}</textarea>
		</div>
	</div>
	
	<div style="height: 40px; float: left; clear: both; margin-top:10px; margin-left: 40px;">
		<div style="float:left;">
	<c:if test="${noticePojo.noticeId != 0}">
		<input type="hidden" name="noticeId" value="${noticePojo.noticeId}" />
		<input type="submit" name="button" value="更新" />
	</c:if><c:if test="${noticePojo.noticeId == 0}">
		<input type="submit" name="button" value="增加" />
	</c:if>
		<input type="button" name="button" value="返回" onclick="returnPage()"/>	
			</div>
	</div>
</form> 
		 
	</div>
	</div>
</div>
</body>
<script type="text/javascript">
	function sub(noticeId){
		var title = $('#title').val();
		var status = $('#status').val();
		var context = $('#contextin').val();
		var timeOut = $('#timeOut').val();
		var effectTime = $('#effectTime').val();
        var url = "<%=basePath %>notice/saveNotice?noticeId="+noticeId+
        	"&title="+title+
        	"&status="+status+
        	"&context="+context+
        	"&timeOut="+timeOut+
        	"&effectTime="+effectTime;
        window.location.href = url;
	}
	
	function returnPage(){
		history.go(-1);
	}
</script>
</html>