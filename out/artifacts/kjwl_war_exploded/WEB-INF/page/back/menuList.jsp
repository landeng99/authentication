<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<body>
<div class="admin">
	<div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">选择权限</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br />
        <div class="tab-panel active" id="tab-set">
        <form id="menuform">
			<div class="form-group">
			仓库管理:&nbsp;
				<input type="checkbox" name="rkgl" value="1">&nbsp;入库管理&nbsp;
				<input type="checkbox" name="rkgl" value="2">&nbsp;包裹入库查询&nbsp;
				<input type="checkbox" name="rkgl" value="3">&nbsp;统计&nbsp;
				<input type="checkbox" name="rkgl" value="4">&nbsp;托盘管理&nbsp;
				<input type="checkbox" name="rkgl" value="5">&nbsp;提单管理&nbsp;
			</div>
			<div class="form-group">
			包裹管理:&nbsp;
				<input type="checkbox" name="bggl" value="包裹列表">&nbsp;包裹列表&nbsp;
				<input type="checkbox" name="bggl" value="境外退运包裹">&nbsp;境外退运包裹&nbsp;
				<input type="checkbox" name="bggl" value="异常包裹">&nbsp;异常包裹&nbsp;
				<input type="checkbox" name="bggl" value="国内运单管理">&nbsp;国内运单管理&nbsp;
			</div>
			<div class="form-group">
			财务管理:&nbsp;
				<input type="checkbox" name="cwgl" value="关税管理">&nbsp;关税管理&nbsp;
				<input type="checkbox" name="cwgl" value="提现申请">&nbsp;提现申请&nbsp;
				<input type="checkbox" name="cwgl" value="理赔处理">&nbsp;理赔处理&nbsp;
				<input type="checkbox" name="cwgl" value="对账单">&nbsp;对账单&nbsp;
				<input type="checkbox" name="cwgl" value="操作记录">&nbsp;操作记录&nbsp;
				<input type="checkbox" name="cwgl" value="优惠券">&nbsp;优惠券&nbsp;
				<input type="checkbox" name="cwgl" value="晒单返现管理">&nbsp;晒单返现管理&nbsp;
				<input type="checkbox" name="cwgl" value="退货费用">&nbsp;退货费用&nbsp;
			</div>
			<div class="form-group">
			虚拟账户:&nbsp;
				<input type="checkbox" value="支付宝对接">&nbsp;支付宝对接&nbsp;
				<input type="checkbox" value="虚拟账号">&nbsp;虚拟账号&nbsp;
				<input type="checkbox" value="虚拟帐号安全监控">&nbsp;虚拟帐号安全监控&nbsp;
			</div>
			<div class="form-group">
			系统管理:&nbsp;
				<input type="checkbox" value="资费管理">&nbsp;资费管理&nbsp;
				<input type="checkbox" value="系统帐号管理">&nbsp;系统帐号管理&nbsp;
			</div>
			<div class="form-group">
			用户管理:&nbsp;
				<input type="checkbox" value="资费管理">&nbsp;资费管理&nbsp;
				<input type="checkbox" value="系统帐号管理">&nbsp;系统帐号管理&nbsp;
			</div>
			<div class="form-group">
			海外仓库管理:&nbsp;
				<input type="checkbox" value="增加">&nbsp;增加&nbsp;
				<input type="checkbox" value="修改">&nbsp;修改&nbsp;
				<input type="checkbox" value="删除">&nbsp;删除&nbsp;
			</div>
			<div class="form-group">
			数据统计:&nbsp;
				<input type="checkbox" value="包裹统计">&nbsp;包裹统计&nbsp;
				<input type="checkbox" value="晒单统计">&nbsp;晒单统计&nbsp;
				<input type="checkbox" value="用户统计">&nbsp;用户统计&nbsp;
				<input type="checkbox" value="报关管理">&nbsp;报关管理&nbsp;
			</div>
			<div class="form-group">
			网站管理:&nbsp;
				<input type="checkbox" value="轮播图管理">&nbsp;轮播图管理&nbsp;
				<input type="checkbox" value="网站信息管理">&nbsp;网站信息管理&nbsp;
				<input type="checkbox" value="导航栏管理">&nbsp;导航栏管理&nbsp;
				<input type="checkbox" value="公告管理">&nbsp;公告管理&nbsp;
				<input type="checkbox" value="友情链接管理">&nbsp;友情链接管理&nbsp;
			</div>
			</form>
			
		</div><br>
	 	<div class="form-button"><a href="javascript:void(0);" onclick="save()" >保存</a>&nbsp;&nbsp;
          <a href="javascript:history.go(-1)">取消</a> 
        </div>
	 </div>
	</div>
</div>
      
</body>
</html>