<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>

<body>
<div class="admin">
    <div class="panel admin-panel">
      <div class="panel-head"><strong>海外地址列表</strong></div>
      <div class="tab-body">
        <br />
        <form action="${backServer}/overSea/overseasAddressSerach" method="post" id="myform" name="myform">
        <input type="hidden" id="service_name_bk" value="${service_name_bk}">
        <input type="hidden" id="status_bk" value="${status_bk}">
                 <div style="height: 40px;">
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>国家:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:200px">
                        <select id="country" name="country" class="input" style="width:200px">
                            <option value="">请选择</option>
                            <c:forEach var="name"  items="${countryList}">
                               <option value="${name}">${name}</option>
                            </c:forEach>
                         </select>
                    </div>

                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>免税区分:</strong></span></div>
                    <div style="float:left;margin-left:10px;">
                         <select id="is_tax_free" name="is_tax_free" class="input" style="width:100px">
                            <option value="">请选择</option>
                            <option value=1>免税</option>
                            <option value=2>不免税</option>
                         </select>
                    </div>
                 </div>
        </form>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" onclick="search()" value="查询" />
                 <shiro:hasPermission name="overSea:add">
                    <input type="button" class="button button-small border-green" onclick="add()" value="添加海外地址"/>
                 </shiro:hasPermission>
                 </div>
                 <table class="table">
                    <tr><th width="20%">国家</th>
                        <th width="40%">地区</th>
                        <th width="10%">仓库</th>
                        <th width="10%">是否免税</th>
                        <th width="20%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="overseasAddress"  items="${overseasAddressList}">
                     <tr><td width="20%">${overseasAddress.country}</td>
                         <td width="40%">${overseasAddress.city}${overseasAddress.state}${overseasAddress.county}</td>
                         <td width="10%">${overseasAddress.warehouse}</td>
                         <td width="10%">
                             <c:if test="${overseasAddress.is_tax_free==1}">免税</c:if>
                             <c:if test="${overseasAddress.is_tax_free==2}">不免税</c:if>
                         </td>
                         <td width="20%">
                         <shiro:hasPermission name="overSea:select">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="detail('${overseasAddress.id}')">详情</a>
                         </shiro:hasPermission><shiro:hasPermission name="overSea:edit">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="update('${overseasAddress.id}')">编辑</a>
                         </shiro:hasPermission><shiro:hasPermission name="overSea:delete">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="del('${overseasAddress.id}')">删除</a>
                         </shiro:hasPermission>
                         </td>

                     </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                   <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
            $('#country').focus();
        })
        function detail(id){
            var url = "${backServer}/overSea/overseasAddressDetail?id="+id;
            window.location.href = url;
        }
        function update(id){
            var url = "${backServer}/overSea/overseasAddressEdit?id="+id;
            window.location.href = url;
        }

        function search(){

            document.getElementById('myform').submit();
        }
        
        function add(){
            var url = "${backServer}/overSea/addOverseasAddress";
            window.location.href = url;
        }
        
        function del(id){
            
            if(confirm("是否要删除该条记录？")){
                 var url = "${backServer}/overSea/deleteOverseasAddress"
                 $.ajax({
                  url:url,
                  data:{
                      "id":id},
                  type:'post',
                  dataType:'text',
                  success:function(data){
                     alert("删除成功");
                     window.location.href = "${backServer}/overSea/overseasAddressInit";
                  }
                 });
            }
          }
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search();
               }
          }
        });
    </script>
</body>
</html>