<%@ page language="java"  pageEncoding="utf-8"%>
<%@include file="header.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<body>

<div class="admin">
    <div class="panel admin-panel">
    <input type="hidden" id="user_type_bk" value="${params.user_type}">
    <input type="hidden" id="status_bk" value="${params.status}">
      <div class="panel-head"><strong>前台用户列表</strong></div>
      <div class="tab-body">
        <br />
            <form action="${backServer}/frontUser/frontUserSearch" method="post" id="myform" name="myform">
                 <div style="height: 40px;">
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户类型:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <select id="user_type" name="user_type" class="input" style="width:150px">
                            <option value="">请选择</option>
                            <option value="1">普通用户</option>
                            <option value="2">同行用户</option>
                         </select>
                    </div>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>账号状态:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <select id="status" name="status" class="input" style="width:150px">
                            <option value="">请选择</option>
                            <option value="0">未激活</option>
                            <option value="1">正常</option>
                            <option value="2">禁用</option>
                         </select>
                    </div>
                    <div style="float:left;margin-top:5px;margin-left:10px"><span><strong>注册时间:</strong></span></div>
                    <div style="float:left;margin-left:10px;margin-top:6px">
                       <span>从</span>
                    </div>
                    <div style="float:left;">
                        <input type="text" class="input" id="fromDate" name="fromDate" style="width:200px" 
                        onfocus="WdatePicker({readOnly:true,maxDate:'#F{$dp.$D(\'toDate\',{d:-1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.fromDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                    <div style="float:left;margin-left:10px;margin-top:6px">
                       <span>到</span>
                    </div>
                    <div style="float:left;">
                       <input type="text" class="input" id="toDate" name="toDate" style="width:200px" 
                       onfocus="WdatePicker({readOnly:true,minDate:'#F{$dp.$D(\'fromDate\',{d:1})}',dateFmt:'yyyy-MM-dd HH:mm:ss'})"
                            value="${params.toDate}" onkeyup="this.value=this.value.replace(/^ +| +$/g,'')"  onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" >
                    </div>
                 </div>
                 
               <div style="height: 40px;">
                 
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户账号:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="account" name="account" style="width:150px"
                                value="${params.account}"/>
                    </div>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>用户姓名:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="user_name" name="user_name" style="width:150px"
                                value="${params.user_name}"/>
                    </div>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>手机号码:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="mobile" name="mobile" style="width:150px"
                                value="${params.mobile}"/>
                    </div>
                    
                    <div style="float:left;margin-top:5px;margin-left:10px;width:84px;"><span><strong>用户邮箱:</strong></span></div>
                    <div style="float:left;">
                        <input type="text" class="input" id="email" name="email" style="width:200px"
                               value="${params.email}"/>
                    </div>
                 </div>
                 
               <div style="height: 40px;">
                 
                 <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>LAST NAME:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="last_name" name="last_name" style="width:150px"
                                value="${params.last_name}"/>
                    </div>
                    <div style="float:left;margin-top:5px; margin-left:10px" class="label"><span><strong>业务名:</strong></span></div>
                    <div style="float:left;margin-left:10px;width:160px">
                         <input type="text" class="input" id="business_name" name="business_name" style="width:150px"
                                value="${params.business_name}"/>
                    </div>
               </div>
            </form>
                 <div class="padding border-bottom">
                    <input type="button" class="button bg-main" onclick="search()" value="查询" />
                 <shiro:hasPermission name="frontUserfrontUserInit:add">
                    <input type="button" class="button button-small border-green" onclick="add()" value="添加同行账户"/>
                 </shiro:hasPermission>
                 </div>
                 <table class="table">
                    <tr>
                        <th width="10%">用户类型</th>
                        <th width="15%">用户姓名</th>
                        <th width="15%">用户邮箱</th>
                        <th width="15%">手机号码<th>
                        <th width="10%">账号状态</th>
                        <th width="15%">业务名</th>
                        <th width="20%">操作</th>
                    </tr>    
                  </table>
                  <table class="table table-hover" id ="list" name ="list">
                  <c:forEach var="frontUser"  items="${frontUserList}">
                     <tr>
                         <td width="10%">
                           <c:if test="${frontUser.user_type==1}">普通用户</c:if>
                           <c:if test="${frontUser.user_type==2}">同行用户</c:if>
                         </td>
                         <td width="15%">${frontUser.user_name}</td>
                         <td width="15%">${frontUser.email}</td>
                         <td width="15%">${frontUser.mobile}</td>
                         <td width="10">
                           <c:if test="${frontUser.status==0}">未激活</c:if>
                           <c:if test="${frontUser.status==1}">正常</c:if>
                           <c:if test="${frontUser.status==2}">禁用</c:if>
                         </td>
                         <td width="15%">${frontUser.business_name}</td>
                         <td width="20%">
                         <shiro:hasPermission name="frontUserfrontUserInit:select">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="detail('${frontUser.user_id}')">详情</a>
                         </shiro:hasPermission><shiro:hasPermission name="frontUserfrontUserInit:edit">
                            <a class="button border-blue button-little" href="javascript:void(0);"onclick="update('${frontUser.user_id}')">编辑</a>
                         </shiro:hasPermission>
                         
                         </td>
                         </tr>
                  </c:forEach>
                  </table>
                  <div class="panel-foot text-center">
                     <jsp:include page="webfenye.jsp"></jsp:include>
                  </div>
      </div>
    </div>
</div>
    <script type="text/javascript">
        $(function(){
             //选择框选中
            $("#user_type").val($("#user_type_bk").val());
            $("#status").val($("#status_bk").val());
            $('#user_type').focus();
        });
        
        function detail(user_id){
            var url = "${backServer}/frontUser/frontUser?user_id="+user_id;
            window.location.href = url;
        }
        
        function update(user_id){
            var url = "${backServer}/frontUser/frontUserUpdate?user_id="+user_id;
            window.location.href = url;
        }
        function ban(user_id,status){
            var url = "${backServer}/frontUser/frontUserStatus";

            $.ajax({
                url:url,
                data:{"user_id":user_id,
                    "status":status},
                type:'post',
                dataType:'text',
                async:false,
                success:function(data){
                   alert("操作成功！");
                   window.location.href = "${backServer}/frontUser/frontUserInit";
                }
            });
        }
        function search(){
            var fromDate=$('#fromDate').val();
            var toDate =$('#toDate').val();
            if(fromDate!=null && fromDate!=""&&
               toDate!=null && toDate!=""&&
               fromDate>toDate){
                    alert("日期起点必须小于日期终点!"); 
                    return ;
                 }
            document.getElementById('myform').submit();
        }
        
        function add(){
            var url = "${backServer}/frontUser/frontUserAdd";
            window.location.href = url;
        }
        
        //回车事件
        $(function(){
          document.onkeydown = function(e){
              var ev = document.all ? window.event : e;
              if(ev.keyCode==13) {

                  search();
               }
          }
        });
    </script>
</body>
</html>