<%@ page language="java"  pageEncoding="UTF-8"%>  
<%@include file="header.jsp" %>

<style type="text/css">
    .span-title{
        font-weight: bold;
        display:-moz-inline-box;
        display:inline-block;
        width:80px;
    }
    .div-front{
        float:left;
        margin-left:10px;
        width:300px;
    }
    
    .div-front-List{
        float:left;
        margin-left:10px;
    }
</style>
<body>
<div class="admin">
    <div class="tab">
      <div class="tab-head">
        <ul class="tab-nav">
          <li class="active"><a href="#tab-set">前台用户审核</a></li>
        </ul>
      </div>
      <div class="tab-body">
        <br/>
           <div class="tab-panel active" id="tab-set">
              <input type="hidden" id="user_id" name ="user_id" value="${frontUser.user_id}">
                 <div style="height: 40px;">
                      <div class="div-front-List"><span class="span-title">账户操作:</span></div>
                      <div class="form-button" style="float:left;">
                        <c:if test="${frontUser.status==1}">
                          <a href="javascript:void(0);" class="button border-yellow button-little" onclick="ban('${frontUser.user_id}','2')" >禁用</a>
                        </c:if>
                        <c:if test="${frontUser.status==2}">
                          <a href="javascript:void(0);" class="button border-yellow button-little" onclick="ban('${frontUser.user_id}','1')" >解禁</a>
                        </c:if>
                        <c:if test="${frontUser.status==0}">
                            <a class="button border-yellow button-little" href="javascript:void(0);"onclick="ban('${frontUser.user_id}',1)">激活</a>
                       </c:if>
                       <c:if test="${frontUser.isfreeze==0}">
                            <a class="button border-yellow button-little" href="javascript:void(0);"onclick="banfreeze('${frontUser.user_id}',1)">冻结</a>
                       </c:if><c:if test="${frontUser.isfreeze!=0}">
                            <a class="button border-yellow button-little" href="javascript:void(0);"onclick="banfreeze('${frontUser.user_id}',0)">解冻</a>
                       </c:if>
                       
                       <a class="button border-yellow button-little" href="javascript:void(0);"onclick="updateFrontUserBussinessNameAndSelectExpressChannel('${frontUser.user_id}')">确定更新</a>
                        <a href="javascript:history.go(-1)" class="button border-blue button-little">返回列表</a> 
                      </div>
                 </div>
                 <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">用户类型:</span>
                           <span>
                             <c:if test="${frontUser.user_type==1}">普通用户</c:if>
                             <c:if test="${frontUser.user_type==2}">同行用户</c:if>
                           </span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">注册时间:</span>
                           <span><fmt:formatDate value="${frontUser.register_time}" type="both"/></span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">用户账号:</span>
                           <span>${frontUser.account}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户姓名:</span>
                           <span>${frontUser.user_name}</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">手机号码:</span>
                           <span>${frontUser.mobile}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户邮箱:</span>
                           <span>${frontUser.email}</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">账户余额:</span>
                           <span>${frontUser.balance}元</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">可用余额:</span>
                           <span>${frontUser.able_balance}元</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">用户积分:</span>
                           <span>${frontUser.integral}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">用户等级:</span>
                           <span>${frontUser.member_rate}</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">first_name:</span>
                           <span>${frontUser.first_name}</span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">last_name:</span>
                           <span>${frontUser.last_name}</span>
                      </div>
                </div>
                
                <div style="height: 40px;">
                      <div class ="div-front">
                           <span class="span-title">业务名:</span>
                           <span>
                           <input id="business_name" name="business_name" value="${frontUser.business_name}"/>
                           </span>
                      </div>
                      <div style="float:left;">
                           <span class="span-title">快件渠道:</span>
                           <span>
                           	<input
							type="radio"  value="1" name="select_express_channel" id="select_express_channel" <c:if test="${frontUser.select_express_channel==1}">checked="checked"</c:if>
							>是
							<input type="radio"
							  value="0" name="select_express_channel" id="select_express_channel" <c:if test="${frontUser.select_express_channel==0}">checked="checked"</c:if>
							 style="margin-left: 20px;">否
                           </span>
                      </div>
                </div>
                
                 <div style="height:300px;">
                      <div class="div-front-List"><span class="span-title">包裹:</span></div>
                      <span>
                           <table>
                              <tr>
                                 <td>
                                   <table border="1" cellpadding="0" cellspacing="0">
                                     <tr>
                                       <td width ="80"  align="middle">包裹状态</td>
                                       <td width ="120" align="middle">物流跟踪号</td>
                                       <td width ="80"  align="middle">重量(KG)</td>
                                       <td width ="80"  align="middle">价值(元)</td>
                                       <td width ="80"  align="middle">运费(元)</td>
                                       <td width ="100" align="middle">收件人</td>
                                       <td width ="400" align="middle">地址:</td>
                                     </tr>
                                   </table>
                                 </td>
                                 <td width="17">
                                 </td>
                              </tr>
                              <tr>
                                 <td colspan="2">
                                   <div style="height:253px;overflow:auto;" >
                                     <table border="1" cellpadding="0" cellspacing="0">
                                       <c:forEach var="pkg"  items="${pkgList}">
                                          <tr>
                                            <td width ="80" valign="middle">
                                              <c:if test="${pkg.status==-1}">异常</c:if>
                                                <c:if test="${pkg.status==0}">待入库</c:if>
                                                <c:if test="${pkg.status==1}">已入库</c:if>
                                                <c:if test="${pkg.status==2}">待发货</c:if>
                                                <c:if test="${pkg.status==3}">已出库</c:if>
                                                <c:if test="${pkg.status==4}">空运中</c:if>
                                                <c:if test="${pkg.status==5}">待清关</c:if>
                                                <c:if test="${pkg.status==7}">已清关</c:if>
                                                <c:if test="${pkg.status==8}">派件中</c:if>
                                                <c:if test="${pkg.status==9}">已收货</c:if>
                                                <c:if test="${pkg.status==20}">废弃</c:if>
                                                <c:if test="${pkg.status==21}">退货</c:if>
                                            </td>
                                            <td width ="120">${pkg.logistics_code}</td>
                                            <td width ="80" align="right">${pkg.weight}</td>
                                            <td width ="80" align="right">${pkg.total_worth}</td>
                                            <td width ="80" align="right">${pkg.freight}</td>
                                            <td width ="100">${pkg.receiver}</td>
                                            <td width ="400">${pkg.street}</td>
                                          </tr>
                                       </c:forEach>
                                     </table>
                                   </div>
                                 </td>
                              </tr>
                             </table>
                      </span>
                 </div>
             </div>
	              <div class="form-button" style="float:left;margin-left:100px;">
	            	<a href="javascript:history.go(-1)" class="button bg-main">返回</a> 
	             </div>
             </div>
        </div>
        </div>
    </div>
<script type="text/javascript">


function ban(user_id,status){
    var url = "${backServer}/frontUser/frontUserStatus";
    $.ajax({
        url:url,
        data:{"user_id":user_id,
            "status":status},
        type:'post',
        dataType:'text',
        async:false,
        success:function(data){
           alert("操作成功！");
           window.location.href = "${backServer}/frontUser/frontUserInit";
        }
    });
}

function banfreeze(user_id,isfreeze){
    var url = "${backServer}/frontUser/frontUserIsFreeze";
    $.ajax({
        url:url,
        data:{"user_id":user_id,
            "isfreeze":isfreeze},
        type:'post',
        dataType:'text',
        async:false,
        success:function(data){
           alert("操作成功！");
           window.location.href = "${backServer}/frontUser/frontUserInit";
        }
    });
}

function updateFrontUserBussinessNameAndSelectExpressChannel(user_id){
    var url = "${backServer}/frontUser/updateFrontUserBussinessNameAndSelectExpressChannel";
    var business_name=$("#business_name").val();
    var select_express_channel=$('input[name="select_express_channel"]:checked').val();
    $.ajax({
        url:url,
        data:{"user_id":user_id,
        	"select_express_channel":select_express_channel,
            "business_name":business_name},
        type:'post',
        dataType:'text',
        async:false,
        success:function(data){
           alert("操作成功！");
           window.location.href = "${backServer}/frontUser/frontUserInit";
        }
    });
}


    </script>
</body>
</html>