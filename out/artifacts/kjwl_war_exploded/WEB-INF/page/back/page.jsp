<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path1 = request.getContextPath();
String basePath1 = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path1+"/";
%>

<table>
	<tr>
		<td>每页<span>&nbsp;${pageination.perRecord}&nbsp;条&nbsp;&nbsp;</span></td>
		<td>共&nbsp;<span>${pageination.pageCount}</span>&nbsp;
		页 | 第 <span>${pageination.pageCurrent}&nbsp;</span>页&nbsp;</td>&nbsp;
		<td align="right">[<a class="right-font08" onclick="targetPage(1)">&nbsp;首页</a> | 
			<a <c:if test="${pageination.pageCurrent != 1}">onclick="targetPage(${pageination.pageCurrent-1})"</c:if>>上一页</a> | 
			<a class="right-font08" <c:if test="${pageination.pageCurrent != pageination.pageCount}">onclick="targetPage(${pageination.pageCurrent+1})"</c:if>>下一页</a> | 
			<a class="right-font08" onclick="targetPage(${pageination.pageCount})">末页&nbsp;</a>]
			转至：<input type="text" style="width:50px" id="gotoPage" onkeydown = "if(event.keyCode==13) targetPage(this.value);"/>
			<input type="hidden" id="pageCount" value="${pageination.pageCount}"/>
			<button id="goPage" onclick="gotoPager()">go</button>
		</td>
	</tr>
</table>
<script type="text/javascript">
function targetPage(pageIndex){
	 
	var targetUrl='${pageination.targetUrl}';
	if (pageIndex) {
		var pageCount = document.getElementById("pageCount").value;
		if(pageIndex>pageCount){
			pageIndex=pageCount;
		}
		if(pageIndex<1){
			pageIndex=1;
		}
		window.location.href="<%=basePath1%>"+targetUrl+"?page="+pageIndex;
		 
	} else {
		var gotoPage = document.getElementById("gotoPage").value;
		var pageCount = document.getElementById("pageCount").value;
		var reg = new RegExp("^[0-9]*$");
		var targetUrl='${targetUrl}';
		
		if(reg.test(gotoPage)){
			gotoPage = parseInt(gotoPage);
			if ((gotoPage > 0) && gotoPage <= pageCount) {
			
				window.location.href="<%=basePath1%>"+targetUrl+"?page="+gotoPage;
			}
		}
	}
}
 
 
function gotoPager(){
 
	var val=document.getElementById("gotoPage").value;
	
	if(val){
		if(!isNaN(val) ){
			   targetPage(val);
		 }
	}
}
</script>

