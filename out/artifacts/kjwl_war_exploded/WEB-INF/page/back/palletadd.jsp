<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@include file="header.jsp" %>
 <style type="text/css">
 	.field-group{
 		width: 500px;
 	}
 	
	.field-group .field-item{
		float:left;
		margin-right: 20px;
	}
	.field-group .field-item .label{
		float:left;
		line-height: 33px;
		margin-right: 10px;
		width: 150px;
	}
	.field-group .field-item .field{
		float:left;
		line-height: 30px;
	}
	.field-group .field-item  .text-field{
		float:left;
		line-height: 35px;
		width: 200px;
	}
	.pickorder-list{
		margin-top: 20px;
	}
</style>
<body>
<div class="admin">
		<form id="palletFom" action="${backServer}/pallet/add" method="post">
	    <div class="field-group">
		 	<div class="field-item">
		 		<div class="label">
		 			<label for="pick_code">托盘号</label>
		 		</div>
		 		<div class="field">
		 			<input  class="input" type="text"  style="width:200px"  name="pallet_code" id="pallet_code" value="${pallet_code}" readonly="readonly"/>
		 		</div>
		 	</div>
	 		<div class="field-item">
		 		<div class="label">
		 			<label for="pick_code">提单号</label>
		 		</div>
		 		<div class="field">
		 			<input type="hidden" value="${pick_id}" name="pick_id" id="pick_id"/>
		 			<input type="hidden" value="${seaport_id}" name="seaport_id" id="seaport_id"/>
		 			<input  class="input" type="text"  style="width:200px"  name="pick_code" id="pick_code" value="${pick_code}" readonly="readonly"/>
		 		</div>
		 	</div>
		   <div class="field-item">
		 		<div class="label">
		 			<label >描述信息</label>
		 		</div>
		 		<div class="text-field">
		 			 <textarea rows="3" cols="50" name="description"></textarea>
		 		</div>
		 	</div>
	  		</div>
	  	 <div style="clear: both"></div>
	  	<div class="panel admin-panel pickorder-list" style="width:800px;">
	  	      	  	 <div class="panel-head"><strong>包裹列表</strong>
	  	      	  	 <input type="button"  onclick="showScanLog(${pick_id})" value="扫描日志" />
	  	      	  	 <input type="button"  onclick="batchExport(${pick_id})" value="下载日志" />
	  	      	  	 </div>
	         	     <div class="padding border-bottom">
			            <input type="button" value="全选" for="id" name="checkall" class="button button-small checkall">
			            <input type="button" value="扫描添加"  id="scanAdd" class="button button-small border-green">
			            <input type="button" value="添加包裹"  id="add" class="button button-small border-green">
			            <input type="button" value="批量删除" class="button button-small border-yellow">
			        </div>
			        <div style="min-height:200px;" >
	         	     <table class="table table-hover " id="palletPkgList">
	         	     <thead>
	         	     	<tr>
	         	     		<th id="package_id" width="45" formatter="getCheckBox">选择</th>
	         	     		<th id="logistics_code" width="45">包裹单号</th>
	         	     		<th id="status" width="45" formatter="fmtstatus">状态</th>
	         	     		<th id="opt" width="45" formatter="pkgOption">操作</th>
	         	     	</tr>
	         	      </thead>
	         	     </table>
	         	     </div>
	  	   </div>
	  	   
	  	   <input type="hidden" id="packages_str" name="packages_str"/>
	  	   <div class="form-button" style="margin-top: 20px;">
	  	   		<input type="button" class="button bg-main" id="save" value="保存">
	  	   		<a class="button bg-main"  >返回</a>
	  	   </div>
	  	  </form>
	 </div>
	 <div id="pkgs" style="display:none;position: relative;"></div>
	 <script type="text/javascript">
	 
	 var datagrid=[];
	 
	 //初始化table
	 var $datagrid=$('#palletPkgList').datagrid({data:datagrid});
	  function fmtstatus(val,rowData){
		  if(val==2){
			  return "未出库";
		  }
		  return val;
		  
	  }
	  
	 function getCheckBox(val,rowData){
		return "<input type='checkbox' name='check' value='"+val+"'>"; 
	 }
	function pkgOption(val,rowData){
		var $html=$("<div><a class='button  border-green'  href='#'>详情</a> <a class='button  border-yellow del' href='#'>删除</a></div>");
		 
		//删除按钮触发事件 
		$html.find('a.del').bind('click',function(){
									
			if(window.confirm("确定删除吗?")){
				//div->td->tr
				var self=$(this).parent().parent().parent();
				
				 var  index=$(self).parent().find('tr').index(self[0]);
				 $datagrid.datagrid('remove',index);
			}
		});
		return $html;
		
	}		 
	  
		//手动加入托盘
		$('#add').click(function(){
		  	$.get('${backServer}/pkg/queryforaddpallet', function(result){
			   $('#pkgs').html(result);
			   
				$('.pkg-list .checkall').click(function(){
					var e=$(this);
					var name=e.attr("name");
					var checkfor=e.attr("checkfor");
					var type;
					if (checkfor!='' && checkfor!=null && checkfor!=undefined){
						type=e.closest('form').find("input[name='"+checkfor+"']");
					}else{
						type=e.closest('form').find("input[type='checkbox']");
					};
					if (name=="checkall"){
						$(type).each(function(index, element){
							element.checked=true;
						});
						e.attr("name","ok");
					}else{
						$(type).each(function(index, element){
							element.checked=false;
						});
						e.attr("name","checkall");
					}	
				});
			
			  });  
			
			//加载弹出层
		  	var index= layer.open({
				    type: 2,
				    title: '包裹列表',
				    shadeClose: true,
				    shade: 0.8,
				    area: ['900px', '450px'],
				    content: '${backServer}/pkg/queryforaddpallet',
				    success:function(index){
					
				    }
				});  
		   	$('#close').bind('click',function(){
				 layer.close(index);
			});
		}); 
		
		
		function addPkgs(array){
	  		 var hasAddData =$datagrid.datagrid('getAllData');
	  		var addData=[];
	  		 for(j=0;j<array.length;j++){
	  			
	  			var package_id=array[j].package_id;
	  			var bool=true;
	  			for(i=0;i<hasAddData.length;i++){
		  			var temp=hasAddData[i];
		  			
		  			if(temp['package_id']==package_id){
		  				 
		  				bool=false;
		  				break;
		  			} 
		  		}
	  			if(bool){
		  			addData.push(array[j]);
	  			}
	  		 }
	  		 
	  		if(addData.length==0){
	  			alert("包裹已经被放入托盘,请选择其他包裹");
	  			return ;
	  		}
	  		
	 		var hasData=$datagrid.datagrid('getAllData');
	  		
	  		var tempArray=[];
	  		for(i=0;i<hasData.length;i++){
	  			var temp=hasData[i];
	  			var data={};
	  			data['package_id']=temp['package_id'];
	  			data['logistics_code']=temp['logistics_code'];
	  			data['status']=temp['status'];
	  			tempArray.push(data);
	  		}
	  		
	  		var allData=$.merge(tempArray,addData);

	  		//校验包裹是否可放入托盘中
	  		var isValid=validatePkg(allData);
	  		
	  		if(isValid){
	  			//将包裹合并并放入托盘
					for(i=0;i<addData.length;i++){
						$datagrid.datagrid('add',addData[i]);
					}
					  var r=confirm("包裹已经放入托盘，是否继续添加");
				  if (r==true)
				    {
				    }
				  	else
				    {
				   		 layer.closeAll();
				    }
	  		}
 
	  		
		}
		//校验包裹是否可放入托盘中
		function validatePkg(allData,addData){
			var isValid=false;
			var seaport_id=$('#seaport_id').val();
			  	var pick_id=$('#pick_id').val();
			  	var params={'packages_str':JSON.stringify(allData),'seaport_id':seaport_id,'pick_id':pick_id};
				  	$.ajax({
			  		url:'${backServer}/pallet/validatepkg',
			  		data:params,
			  		type:'post',
			  		dataType:'json',
			  		async:false,
			  		success:function(data){
			  			if(data['result']){
			  				isValid=true;
			  			}else{
			  				isValid=false;
			  				alert(data['msg']);
			  			}			
			  		}
			  	});
			return isValid;
		}
		//扫描包裹加入托盘
		$('#scanAdd').click(function(){
			$.get('${backServer}/pkg/initscanforaddpallet', function(result){
				 $('#pkgs').html(result);
				 //加载弹出层
				  	var index= layer.open({
					    type: 1,
					    title: '包裹扫描',
					    shadeClose: true,
					    shade: 0.8,
					    area: ['900px', '450px'],
					    content: $('#pkgs'),
					    success:function(index){
							
					    }
					});
				 	
				  	$('#scanInput').change(function(){
						var logistics_code= $(this).val();
				  		var hasAddData =$datagrid.datagrid('getAllData');
				  		for(i=0;i<hasAddData.length;i++){
				  			var temp=hasAddData[i];
				  			if(temp['logistics_code']==logistics_code){
				  				$('#note').text("包裹已经加入托盘，请继续扫描... ...");
				  				return;
				  			}
				  		}
				  		
						var allData=[];
				  		for(i=0;i<hasAddData.length;i++){
				  			var temp=hasAddData[i];
				  			var data={};
				  			data['package_id']=temp['package_id'];
				  			data['logistics_code']=temp['logistics_code'];
				  			data['status']=temp['status'];
				  			allData.push(data);
				  		}
				  		
				  		
					  	var seaport_id=$('#seaport_id').val();
					  	var pick_id=$('#pick_id').val();
						$.ajax({
							url:'${backServer}/pkg/scanforaddpallet',
							data:{'packages_str':JSON.stringify(allData),logistics_code:logistics_code,'seaport_id':seaport_id,'pick_id':pick_id},
							type:'post',
							dataType:'json',
							async:false,
							success:function(data){
								 
								var pkgList=data.pkgList;
								if(pkgList){
									for(i=0;i<pkgList.length;i++){
					  					$datagrid.datagrid('add',pkgList[i]);
					  				}
									
									$('#note').text("包裹加入托盘成功，请继续扫描... ...");
								}else{
									$('#note').text(data.msg);
								}
							}
						});
						
				  		$(this).val('');
						
				  		
				  	});
				 
			});
		});
		
		$('#save').click(function(){
			var datalist= $datagrid.datagrid('getAllData');
		  	$('#packages_str').val(JSON.stringify(datalist));
		  	$('#palletFom').submit();
		});
		
		function showScanLog(pick_id)
		{
		    layer.open({
		        title :'扫描日志',
		        type:2,
		        shadeClose: true,
		        shade: 0.8,
		        offset: ['10px', '100px'],
		        area: ['900px', '800px'],
		        content:'${backServer}/outputScanLog/showAllScanLog?pick_id='+pick_id
		    }); 
		}
		function batchExport(pick_id) {
			window.location.href = "${backServer}/outputScanLog/exportOutputScanLog?pick_id="
					+ pick_id
					;
		}
	 </script>
	 
</body>
</html>