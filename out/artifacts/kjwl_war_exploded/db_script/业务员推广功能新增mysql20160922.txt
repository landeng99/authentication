#t_front_user表中新增pushLinkId字段
ALTER TABLE `t_front_user`
ADD COLUMN `pushLinkId`  int(10) default NULL COMMENT '业务推广链接id';




#新增t_push_link表
DROP TABLE IF EXISTS `t_push_link`;
CREATE TABLE `t_push_link` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '推广ID，主键自增长',
  `businessName` varchar(20) DEFAULT NULL COMMENT '业务员',
  `pushLink` varchar(100) DEFAULT NULL COMMENT '推广链接',
  `pushCode` varchar(100) DEFAULT NULL COMMENT '推广二维码',
  `deleteStatus` int(2) NOT NULL COMMENT '删除状态  0：未删除  1：删除',
  `createTime` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8;




#新增推广链接菜单
INSERT INTO `lmp_test_new`.`t_menu` (`id`, `name`, `menu_type`, `link`, `parent_id`, `permission`, `orderType`) VALUES ('211', '推广链接', '1', '/pushLink/initPage', '6', NULL, '9999999');
INSERT INTO `lmp_test_new`.`t_menu` (`id`, `name`, `menu_type`, `link`, `parent_id`, `permission`, `orderType`) VALUES ('212', '推广统计', '1', '/pushLink/initCount', '6', NULL, '9999999');






