/*
Navicat MySQL Data Transfer

Source Server         : LocalHost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : lmp00

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2016-12-01 17:59:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_export_config
-- ----------------------------
DROP TABLE IF EXISTS `t_export_config`;
CREATE TABLE `t_export_config` (
  `export_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(30) NOT NULL COMMENT '模板名称',
  `items` varchar(3000) NOT NULL COMMENT '导出项目',
  `description` varchar(300) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`export_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='导出配置';

-- ----------------------------
-- Records of t_export_config
-- ----------------------------
INSERT INTO `t_export_config` VALUES ('24', '导出提单', '[{\"col_key\":\"logistics_code\",\"col_name\":\"公司运单号\"},{\"col_key\":\"original_num\",\"col_name\":\"关联单号\"},{\"col_key\":\"business_name\",\"col_name\":\"业务\"},{\"col_key\":\"receiver\",\"col_name\":\"收件人\"},\r\n{\"col_key\":\"province\",\"col_name\":\"省份\"},{\"col_key\":\"city\",\"col_name\":\"城市\"},{\"col_key\":\"street\",\"col_name\":\"收件人地址\"},{\"col_key\":\"receiver_mobile\",\"col_name\":\"收件人电话\"},\r\n{\"col_key\":\"idcard\",\"col_name\":\"收件人身份证\"},{\"col_key\":\"goods_names\",\"col_name\":\"物品名称\"},{\"col_key\":\"goods_detail\",\"col_name\":\"包裹明细\"},{\"col_key\":\"brand\",\"col_name\":\"品牌\"},\r\n{\"col_key\":\"quantity\",\"col_name\":\"数量\"},{\"col_key\":\"unit\",\"col_name\":\"单位\"},{\"col_key\":\"actual_weight\",\"col_name\":\"毛重\"},{\"col_key\":\"price\",\"col_name\":\"单价\"},{\"col_key\":\"spec\",\"col_name\":\"规格\"},\r\n{\"col_key\":\"postal_code\",\"col_name\":\"邮编\"},{\"col_key\":\"user_name\",\"col_name\":\"发件人\"},{\"col_key\":\"send_mobile\",\"col_name\":\"发件人电话\"},{\"col_key\":\"send_address\",\"col_name\":\"发件人地址\"},\r\n{\"col_key\":\"keep_price\",\"col_name\":\"保费\"},{\"col_key\":\"express_package\",\"col_name\":\"渠道\"}]', '添加业务 保费');
