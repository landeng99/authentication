#20160924

DROP TABLE IF EXISTS `t_seaport_declaration`;
CREATE TABLE `t_seaport_declaration` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `declaration_code` varchar(50) DEFAULT NULL COMMENT '报关号码',
  `seaport_id` int(11) DEFAULT NULL COMMENT '所属口岸ID',
  `package_id` int(11) DEFAULT NULL COMMENT '对应包裹ID',
  `use_flag` int(11) DEFAULT '0' COMMENT '使用标志 0：未使用 1：已使用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建者用户ID',
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `t_seaport`
ADD COLUMN `output_print_template`  varchar(300) NULL COMMENT '出库打印模板路径' AFTER `express_seaport`;

ALTER TABLE `t_seaport`
ADD COLUMN `leave_warning_count`  int DEFAULT 0 COMMENT '剩余提醒 数' AFTER `output_print_template`;


#20160927
ALTER TABLE `t_package`
ADD COLUMN `seaport_id`  int NULL DEFAULT '-1' COMMENT '包裹所属口岸ID，出库扫描时用到' AFTER `need_check_out_flag`;


ALTER TABLE `t_pickorder`
ADD COLUMN `output_id`  int NULL COMMENT '所属出库ID' AFTER `overseas_address_id`;

DROP TABLE IF EXISTS `t_output`;
CREATE TABLE `t_output` (
  `seq_id` int(11) NOT NULL AUTO_INCREMENT,
  `output_sn` varchar(20) DEFAULT NULL COMMENT '出库编号',
  `osaddr_id` int(11) DEFAULT NULL COMMENT '海外仓库地址',
  `seaport_id_list` varchar(255) DEFAULT NULL COMMENT '已出库口岸ID列表',
  `auto_print` int(11) DEFAULT '0' COMMENT '自动打印面单 0:否 1：打印',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建者ID',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`seq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




#20160928
#仓库管理菜单下添加出库包裹菜单
INSERT INTO t_menu VALUES ( (select max(id)+1 from t_menu t),'出库包裹', '1', '/output/queryAll', '1', null, '9999999');

ALTER TABLE `t_pallet`
ADD COLUMN `output_id`  int NULL COMMENT '所属出库ID' AFTER `pick_id`;




#20161010
#添加包裹加入托盘时间
ALTER TABLE `t_pallet_package`
ADD COLUMN `create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' AFTER `package_id`;




