<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0037)http://www.birdex.cn/destination.html -->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" charset="utf-8" async="" src="${mainServer}/resource/img/crmqq.php"></script>
	<script type="text/javascript" charset="utf-8" async="" src="${mainServer}/resource/js/contains.js"></script>
	<script type="text/javascript" charset="utf-8" async="" src="${mainServer}/resource/js/localStorage.js"></script>
	<script type="text/javascript" charset="utf-8" async="" src="${mainServer}/resource/js/Panel.js"></script>
    <title>收货地址- 最注重海淘用户体验的转运公司。</title>
    <meta name="renderer" content="ie-stand">
    <meta name="keywords" content="翔锐物流，笨鸟转运，转运公司,美国快递、美国转运">
    <meta name="description" content="首家转运免首重，免税州直飞，24小时出库，丢货全赔，6*10小时贴心客服。翔锐物流用互联网思维打造转运公司，颠覆转运行业时效慢，体验差，安全性低，价格高的用户体验。">
    <link href="${mainServer}/resource/css/account.css" rel="stylesheet" type="text/css">
    <link href="${mainServer}/resource/css/default.css" rel="stylesheet" type="text/css">
<link href="${mainServer}/resource/css/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${mainServer}/resource/js/jquery-1.8.3.all.js"></script>
<script type="text/javascript" src="${mainServer}/resource/js/layer.min.js"></script>
<script type="text/javascript" src="${mainServer}/resource/js/common.js"></script>
<script type="text/javascript">
    $(function () {
        var userName = $.cookie('BEYOND_NickName');
        if (userName == undefined || userName == "") {
            userName = $.cookie('BEYOND_UserName');
        }
        if (userName == undefined || userName == "") {
            userName = "会员用户";
        } else {
            if (userName.length > 8 && userName.indexOf("@") > -1) {
                userName = userName.substring(0, 7) + "...";
            }
        }
        $("#headerUserName").html(userName).attr("title", userName);

        var url = document.location.href;
        if (url == "http://www.birdex.cn/")
            url = "http://www.birdex.cn/index.html";
        else if (url == "http://birdex.cn/")
            url = "http://www.birdex.cn/index.html";

        $("ul.nav>li").each(function () {
            if (url.indexOf($(this).attr("val")) > -1) {
                $(this).addClass("current");
            }
        });

        var userID = $.cookie('User::TUserID');
        if (userID != undefined && userID != null && parseInt(userID) > 0) {
            $("#logout").show();
            $(".nav").css("right", "0");
        } else {
            $(".nav").css("right", "0");
            $("#login").show();
        }

        $(".ggnew").click(function () {
            if ($("#ggtab").is(':visible')) {
                $("#ggtab").slideUp("slow");
            } else {
                $("#ggtab").slideDown("slow");
            };
            var img = $(this).find("span").css("background-image");
            if (img.indexOf('header01') > -1) {
                $(this).find("span").css("background-image", img.replace('header01.png', 'header_up.png'));
            } else {
                $(this).find("span").css("background-image", img.replace('header_up.png', 'header01.png'));
            }
        });

        $("#tags1 li").mouseover(function (index, item) {
            $("#tags1 li").removeClass("selectTag");
            $(this).addClass("selectTag");
            $("#tags1").parent().find("div").each(function () {
                if ($(this).hasClass("selectTag")) {
                    $(this).removeClass("selectTag").addClass("tagContent");
                }
            });
            $("#" + $(this).attr("obj")).removeClass("tagContent").addClass("selectTag");
        });

        if (url.indexOf('index') > -1 || document.URL == 'http://www.birdex.cn/' || document.location.href.indexOf('index') > -1) {
            $(".ggnew").click();
        }

        var headerName = $.cookie('User::NickName');
        if (headerName == undefined || headerName == null || headerName == "") {
            headerName = $.cookie('User::TrueName');
            if (headerName == undefined || headerName == null || headerName == "") {
                headerName = $.cookie('BEYOND_UserName');
                if (headerName == undefined || headerName == null) {
                    headerName = "";
                }
            }
        }

        $("#headername").html(headerName);
    });
</script>
<meta property="qc:admins" content="15411542256212450636">
<style type="text/css">
    #header{ border-bottom:1px solid #dcdcdc; overflow:hidden; height:32px; line-height:32px;}
    #header a{ font-size:14px; color:#6a6a6b;}
    #header .pull-right a{ margin:0 10px;}
    #header .ggnew{ cursor:pointer;}
    #header .ggnew i{ color:#eb7424; font-style:normal;}
    #header .ggnew span{ float:left; display:block; width:42px; height:32px; background:url('http://img.cdn.birdex.cn/images/header01.png') no-repeat center center #0593d3; overflow:hidden; margin-right:5px;}
    #ggtab{ display:none; background-color:#0593d3; padding:10px 0; overflow:hidden;}
    #ggtab .hbt{ font-size:22px; font-weight:bold; color:#f57121; padding-left:32px; background:url('http://img.cdn.birdex.cn/images/header02.png') no-repeat left center; overflow:hidden; line-height:32px;}
    #sevedu{ padding-top:5px; overflow:hidden;}
    #sevedu .tagContent{display:none;}
    #sevedu .tagContent p,
    #sevedu .selectTag p{ font-size:16px; color:#edf6ff;}
    #sevedu .tagContent p a,
    #sevedu .selectTag p a{ color:#f57121; text-decoration:underline; margin-left:8px; font-weight:bold;}
    #sevedu .tag{ width:100%; padding-top:5px; text-align:right; overflow:hidden;}
    #sevedu .tag li{display:inline;}
    #sevedu .tag a{display:inline-block; width:8px; height:9px; background:url('http://img.cdn.birdex.cn/images/header03.png') no-repeat; overflow:hidden; margin:0 3px;}
    #sevedu .tag a:hover,
    #sevedu .tag .selectTag a{ width:56px; background-image:url('http://img.cdn.birdex.cn/images/header04.png');}
</style>
</head>
<body>
<div id="ggtab">
     <div class="mian">
          <div class="hbt">公告</div>
          
          <div id="sevedu">
                <div class="selectTag" id="li0"><p>中国清明节（4月4日至4月6日）假期安排通知：除4月5日（星期天）清明节当天放假外，其余时间均有国内客服正<a href="http://www.birdex.cn/Information-14.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li1"><p>关于新增渠道详细介绍公告<a href="http://www.birdex.cn/Information-21.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li2"><p>关于系统问题导致订单申报数据错误的相关处理方案<a href="http://www.birdex.cn/Information-19.html" target="_blank">查看详情</a></p></div><div class="tagContent" id="li3"><p>所有笨鸟的客户以及淘友，经过对目前现状的考察和认真考虑，笨鸟不得已今天对税单相关政策作出调整如下：<a href="http://www.birdex.cn/Information-16.html" target="_blank">查看详情</a></p></div><ul id="tags1" class="tag"><li obj="li0" class="selectTag"><a href="javascript:void(0)"></a></li><li obj="li1"><a href="javascript:void(0)"></a></li><li obj="li2"><a href="javascript:void(0)"></a></li><li obj="li3"><a href="javascript:void(0)"></a></li></ul>
          </div>
     </div>
</div>
<div id="header" class="noticeHeader">
     <div class="mian">
         <div class="pull-right" id="login" style="display:none;">
              <a href="http://passport.birdex.cn/Login.aspx">登录</a>|<a href="http://passport.birdex.cn/Register.aspx">注册</a>
         </div>
         <div class="pull-right" id="logout" style="">
               <span id="headername">502725157@qq.com</span><a href="http://passport.birdex.cn/LogOut.aspx" style="">退出</a>
          </div>
         <a class="ggnew"><span></span>网站最新公告 <i>4</i> 条</a>
     </div>
</div>
<div id="head">
     <div class="mian">
          <a href="http://www.birdex.cn/index.html" class="logo"><img src="${mainServer}/resource/img/logo.jpg" alt="翔锐物流" title="翔锐物流"><span>笨鸟</span></a>
          
          
          <ul class="nav" style="right: 0px;">
              <li val="help" style="padding-right:5px;"><a href="http://www.birdex.cn/help.html">帮助中心</a></li>
              <li val="Information" id="headInformation"><a href="http://www.birdex.cn/Information/prohibition.aspx">禁运物品</a></li>
              <li val="account" id="headAccount" class="current"><a href="http://www.birdex.cn/transport.html">我的帐户</a></li>
              <li val="index"><a href="http://www.birdex.cn/index.html">首页</a></li>
          </ul>
     </div>
</div>
    <div id="content">
        
<script type="text/javascript">
    var isOverDomain = 0;
    var old = '1';
    $(function () {
        $("ul.sideaav li").each(function () {
            if (document.location.href.toLowerCase().indexOf($(this).attr("item").toLowerCase()) > -1) {
                $(this).addClass("current");
                $(this).find("i").css("background-image", "url('" + $(this).attr("img") + "')");
                $("ul.nav>li").removeClass("current");
                $("#headAccount").addClass("current");
            }
        });

        var overDomainArray = ["destination", "transport", "store", "order"];
        for (var item in overDomainArray) {
            if (document.location.href.toLowerCase().indexOf(overDomainArray[item]) > -1) {
                document.domain = domainUrl;
                isOverDomain = 1;
            }
        }
        $("#charge,#accountCharge,#accountLeveUp").click(function () {
            var rechargeOffHeight = ($(window).height() - 570) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                fix: false,
                title: false,
                shade: [0.5, '#ccc', true],
                border: [1, 0.3, '#666', true],
                offset: [rechargeOffHeight + 'px', ''],
                area: ['900px', '570px'],
                iframe: { src: '/UserBase/Recharge.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#editPass,#accountEditPass").click(function () {
            var editPassOffHeight = 0;
            var height = 338;
            if (old > 0) {
                editPassOffHeight = ($(window).height() - 338) / 2;
            } else {
                editPassOffHeight = ($(window).height() - 270) / 2;
                height = 270;
            }
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#fff', true],
                border: [0, 0.3, '#000', true],
                offset: [editPassOffHeight + 'px', ''],
                area: ['335px', height + 'px'],
                iframe: { src: '/UserBase/ModifyP.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#emailActive,#accountValidEmail").click(function () {
            var emailActiveOffHeight = ($(window).height() - 320) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [emailActiveOffHeight + 'px', ''],
                //area: ['850px', '400px'],
                area: ['335px', '278px'],
                //iframe: { src: '/UserBase/ActiveEmail.aspx?overdomain=' + isOverDomain }
                iframe: { src: '/UserBase/ActiveE.aspx?overdomain=' + isOverDomain }
            });
        });

        $("#mobileActive,#accountActiveMobile").click(function () {
            var mobileActiveOffHeight = ($(window).height() - 277) / 2;
            $.layer({
                closeBtn: false,
                type: 2,
                title: false,
                fix: true,
                shade: [0.5, '#ccc', true],
                border: [0, 0.3, '#666', true],
                offset: [mobileActiveOffHeight + 'px', ''],
                area: ['335px', '277px'],
                iframe: { src: '/UserBase/ActiveMobile.aspx?overdomain=' + isOverDomain }
            });
        });
    });
</script>
<div class="left" style="margin-bottom:250px;">
          <h3 class="size14"></h3>
          <p><span style="float:right;">已激活</span> <span style="overflow:hidden;text-overflow:ellipsis;display:block;white-space:nowrap;width:100px">${sessionScope.frontUser.email}</span></p>
          <p><!----></p>
          <ul class="recharge">
            
              <li class="span5 pull-right text-center">
                  <i class="icon06"></i>
                  <h3 class="size14">5张</h3>
                  <span style="font-size:13px;"><a href="http://www.birdex.cn/account.html#Coupon" class="blue">优惠劵</a></span>
                  
              </li>
              
              <li class="span5 pull-left text-center" style="width:auto;">
                  <a title="钱包余额" style="text-decoration:none;cursor:pointer;"><i class="icon05"></i>
                  <h3 class="size14">$<span style="padding-left:1px;">0.00</span></h3></a>
                  <a style="cursor:pointer;font-size:13px;" class="blue" id="charge">充值</a>
              </li>

          </ul>

          <ul class="modification">
              <li class="span6"><a style="cursor:pointer;" class="blue icon07" id="editPass">修改密码</a></li>
              <li class="span6" style="display:none;"><a href="" class="blue icon08">修改订阅</a></li>
              <li class="span6" style="display:none;"><a href="" class="blue icon09">使用习惯</a></li>
          </ul>

          <ul class="sideaav">
            <!--
              <li class="span12" item="transport" img="/images/icon10_fff.png"><a href="/transport.html" class="size16"><i class="icon10"></i>我的包裹管理</a></li>
              <li class="span12" item="transport" img="/images/icon10_fff.png"><a href="/transport.html" class="size16"><i class="icon10"></i>我的订单</a></li>
              -->
              <li class="span12" item="transport" img="${mainServer}/resource/img/icon10_fff.png"><a href="${mainServer}/transport" class="size16"><i class="icon10"></i>我的包裹管理</a></li>
              <li class="span12" item="warehouse" img="${mainServer}/resource/img/icon11_fff.png"><a href="${mainServer}/warehouse.jsp" class="size16"><i class="icon11"></i>海外仓库地址</a></li>
              <li class="span12 current" item="destination" img="${mainServer}/resource/img/icon12_fff.png"><a href="${mainServer}/destination.jsp" class="size16"><i class="icon12"></i>收货地址管理</a></li>
              <li class="span12" item="record" img="${mainServer}/resource/img/icon13_fff.png"><a href="${mainServer}/record.jsp" class="size16"><i class="icon13"></i>账户记录</a></li>
              <li class="span12" item="account" img="${mainServer}/resource/img/icon14_fff.png"><a href="${mainServer}/account.jsp" class="size16"><i class="icon14" style="background-image: url(${mainServer}/resource/img/icon14.png);"></i>账户设置</a></li>
          </ul>
</div>
        <div class="right">
            <div class="record">
                <div class="bt">
                    <strong class="size14 blue pd10" style="cursor: pointer;" id="addAddress">新增收货地址</strong>电话号码、手机号选填一项，其余均为必填项</div>
                <form>
                <ul class="destination">
                    <li><span>所在地区：<i class="red" style="color: Red;">*</i></span>
                        <select name="modifyProvince" id="modifyProvince" onchange="SelectModifyChange(this,2);" style="color: #666; font-family: @微软雅黑;"><option value="-1">请选择省市...</option><option value="77" p="6">北京市</option><option value="80" p="6">天津市</option><option value="83" p="6">河北省</option><option value="95" p="6">山西省</option><option value="107" p="6">内蒙古自治区</option><option value="120" p="6">辽宁省</option><option value="135" p="6">吉林省</option><option value="145" p="6">黑龙江省</option><option value="159" p="6">上海市</option><option value="162" p="6">江苏省</option><option value="176" p="6">浙江省</option><option value="188" p="6">安徽省</option><option value="206" p="6">福建省</option><option value="216" p="6">江西省</option><option value="228" p="6">山东省</option><option value="246" p="6">河南省</option><option value="264" p="6">湖北省</option><option value="279" p="6">湖南省</option><option value="294" p="6">广东省</option><option value="316" p="6">广西壮族自治区</option><option value="331" p="6">海南省</option><option value="335" p="6">重庆市</option><option value="338" p="6">四川省</option><option value="360" p="6">贵州省</option><option value="370" p="6">云南省</option><option value="387" p="6">西藏自治区</option><option value="395" p="6">陕西省</option><option value="406" p="6">甘肃省</option><option value="421" p="6">青海省</option><option value="430" p="6">宁夏回族自治区</option><option value="436" p="6">新疆维吾尔自治区</option></select>
                        <select name="modifyCity" id="modifyCity" onchange="SelectModifyChange(this,3);" style="color: #666; font-family: @微软雅黑;">
                            <option>请选择城市...</option>
                        </select>
                        <select name="modifyQzone" id="modifyQzone" style="color: #666; font-family: @微软雅黑;">
                            <option>请选择区/县...</option>
                        </select>
                    </li>
                    <li><span>邮政编码：<i class="red" style="color: Red;">*</i></span>
                        <input name="PostCode" id="PostCode" type="text" class="text05" value="请填写邮政编码" tip="请填写邮政编码" style="overflow: auto; height: 32px; line-height: normal;">
                    </li>
                    <li><span>街道地址：<i class="red" style="color: Red;">*</i></span>
                        <textarea name="Address" id="Address" cols="" rows="" style="color: #666; font-family: @微软雅黑;">不需要重复填写省市区，必须大于5个字符，小于120个字符</textarea>
                    </li>
                    <li><span>收货人姓名：<i class="red" style="color: Red;">*</i></span>
                        <input name="TrueName" id="TrueName" type="text" class="text05" value="长度不超过25个字" tip="长度不超过25个字" passname="" style="overflow: auto; height: 32px; line-height: normal;">
                    </li>
                    <li><span>手机号码：<i class="red"></i></span>
                        <input name="Mobile" id="Mobile" type="text" class="text05" value="电话、手机号码必须填一项" tip="电话、手机号码必须填一项" style="overflow: auto; height: 32px; line-height: normal;">
                    </li>
                    <li><span>电话号码：<i class="red"></i></span>
                        <input name="Code" id="Code" type="text" class="text06" value="区号" tip="区号" style="overflow: auto; height: 32px; line-height: normal;">
                        <input name="Tel" id="Tel" type="text" class="text07" value="电话号码" tip="电话号码" style="overflow: auto; height: 32px; line-height: normal;">
                        <input name="Extension" id="Extension" type="text" class="text06" value="分机" tip="分机" style="overflow: auto; height: 32px; line-height: normal;">
                    </li>
                    <li><span>设为默认地址：<i class="red"></i></span>
                        <input name="IsDefault" id="IsDefault" type="checkbox">
                        设置为默认收货地址 </li>
                    <li><span>证件号码：<i class="red" style="color: Red;">*</i></span>
                        <input name="Identity" id="Identity" type="text" class="text05" value="证件号码" tip="证件号码" style="overflow: auto; height: 32px; line-height: normal;">
                        <div style="width: 55px; height: 32px; background-color: #0fa6d8; float: left; border-radius: 5px;
                            color: #fff; text-align: center; display: none; cursor: pointer;" id="auditDiv" title="该身份证号已审核通过">
                            已审核</div>
                    </li>
                    <li style="display: none;"><span>证件照片地址：<i class="red"></i></span>
                        <input name="saveIdentity" id="saveIdentity" type="text" class="text05" value="" style="overflow: auto; height: 32px; line-height: normal;">
                        <input name="saveOriginalIdentity" id="saveOriginalIdentity" type="text" class="text05" value="" style="overflow: auto; height: 32px; line-height: normal;">
                        <input name="saveIdentityEx" id="saveIdentityEx" type="text" class="text05" value="" style="overflow: auto; height: 32px; line-height: normal;">
                        <input name="saveOriginalIdentityEx" id="saveOriginalIdentityEx" type="text" class="text05" value="" style="overflow: auto; height: 32px; line-height: normal;">
                    </li>
                    <li class="Special"><a href="javascript:;" class="button_bule pull-right" name="save" id="save" style="margin-top: 15px;">保存</a> <a href="javascript:;" class="button_gray pull-right" name="cancel" id="cancel" style="margin-top: 15px;">取消</a> <span>证件照片：<i class="red"></i></span>
                        <div style="width: 120px; height: auto; min-height: 40px; overflow: hidden; float: left;">
                            <div style="float: left; margin-top: 8px;" class="normal">
                                <a href="javascript:;" class="notSafari">
                                    <div class="ke-inline-block ">
									<form class="ke-upload-area ke-form" method="post" enctype="multipart/form-data" target="kindeditor_upload_iframe_1428056495178" action="http://upload.birdex.cn//UploadHandlerForEditor.ashx?FileType=Image&Directory=MemberIDCard" style="width: 120px;">
									<span class="ke-button-common" style="width: 120px; height: 36px; cursor: pointer; background-image: url(http://img.cdn.birdex.cn//images/btn_front.png);">
									<input type="button" class="ke-button-common ke-button" value="" style="margin-right: 0px; width: 120px; height: 36px; cursor: pointer; background-image: url(http://img.cdn.birdex.cn//images/btn_front.png); background-position: 100% 50%;">
									</span>
									<input type="file" class="ke-upload-file" name="imgFile" tabindex="-1" style="">
									</form>
									</div>
									<input type="file" name="IdentityImg" id="IdentityImg" style="display: none;"></a> 
									<a href="javascript:;" class="isSafari" style="display: none;">
                                        <input type="file" id="safariIdentityImg" name="safariIdentityImg"></a>
                            </div>
                            <div style="float: left; margin-top: 5px; width: 116px; margin-left: 2px;" class="pass">
                                <img src="${mainServer}/resource/img/Iden.jpg" width="116" id="showIDCard" alt="证件照正面">
                            </div>
                        </div>
                        <div style="width: 130px; height: auto; min-height: 40px; overflow: hidden; float: left;">
                            <div style="float: left; margin-left: 10px; margin-top: 8px;" class="normal">
                                <a href="javascript:;" class="notSafari">
                                    <div class="ke-inline-block ">
									<form class="ke-upload-area ke-form" method="post" enctype="multipart/form-data" target="kindeditor_upload_iframe_1428056495182" action="http://upload.birdex.cn//UploadHandlerForEditor.ashx?FileType=Image&Directory=MemberIDCard" style="width: 120px;">
									<span class="ke-button-common" style="width: 120px; height: 36px; cursor: pointer;background-image: url(http://img.cdn.birdex.cn//images/btn_verso.png);">
									<input type="button" class="ke-button-common ke-button" value="" style="margin-right: 0px; width: 120px; height: 36px; cursor: pointer; background-image: url(http://img.cdn.birdex.cn//images/btn_verso.png); background-position: 100% 50%;">
									</span>
									<input type="file" class="ke-upload-file" name="imgFile" tabindex="-1" style="">
									</form>
									</div>
									<input type="file" name="IdentityImgEx" id="IdentityImgEx" style="display: none;"></a> 
									<a href="javascript:;" class="isSafari" style="display: none;">
                                        <input type="file" id="safariIdentityImgEx" name="safariIdentityImgEx"></a>
                            </div>
                            <div style="float: left; margin-top: 5px; width: 116px; margin-left: 12px;" class="pass" id="lastPass">
                                <img src="${mainServer}/resource/img/IdenEx.jpg" width="116" id="showIDCardEx" alt="证件照反面">
                            </div>
                        </div>
                        <div style="float: left;" class="normal">
                            <label id="show" style="margin-left: 5px;">
                            </label>
                        </div>
                    </li>
                    <li><span><i class="red"></i></span><i style="color: Red; margin-right: 5px; margin-left: -10px;">
                        *</i><b id="infoTip" style="font-weight: normal;">证件图只供海关清关用,请放心上传真实有效的证件图片.建议图片大小在200KB以内</b>
                    </li>
                </ul>
                </form>
                <div class="destination" style="width:60%;float:left;">
                    <ul>
                        <li><span><i class="red"></i></span><i style="color: Red; margin-right: 5px; margin-left: -10px;">
                            *</i><b id="uploadTip" style="font-weight: normal;">身份证上传要求如下，请参照范例（如右图）</b>
                        </li>
                        <li style="padding-top: 0px;"><span><i class="red"></i></span><b id="B1" style="font-weight: normal;">
                            1.身份证人名要与收件人名一致</b> </li>
                        <li style="padding-top: 0px;"><span><i class="red"></i></span><b id="B2" style="font-weight: normal;">
                            2.正面与反面要单独分开上传</b> </li>
                        <li style="padding-top: 0px;"><span><i class="red"></i></span><b id="B3" style="font-weight: normal;">
                            3.若自己添加水印，请不要遮挡住身份证里的信息及头像</b> </li>
                        <li style="padding-top: 0px;"><span><i class="red"></i></span><b id="B4" style="font-weight: normal;">
                            4.身份证四角边框要齐全，清晰可见，不得有缺损，反光</b> </li>
                        <li style="padding-top: 0px;"><span><i class="red"></i></span><b id="B5" style="font-weight: normal;">
                            5.不得有其他背景图片在身份证照片当中</b> </li>
                        <li style="padding-top: 0px;"><span><i class="red"></i></span><b id="B6" style="font-weight: normal;">
                            6.图片格式为JPG格式，大小在200KB以内</b> </li>
                        <li style="padding-top: 0px;"><span><i class="red"></i></span><b id="B7" style="font-weight: normal;">
                            7.身份证要在有效期内</b> </li>
                    </ul>
                </div>
                <div class="destination" style="width:40%; float:left;">
                    <img style="padding-left: 28px;" height="270px" src="${mainServer}/resource/img/idcarddemo.jpg">
                </div>
                <div class="hr mg18" style="margin-top: 20px;">
                </div>
                <div class="bt" style="margin: 20px 0;">
                    <strong class="size14 blue">已保存的收货地址</strong></div>
                <ul class="list01 radius3" id="addressli"><li class="th"><ol><li class="p no" style="padding-left:0;text-align:center;width:80px;">收件人</li><li class="q" style="width: 160px;padding-left:0;text-align:center;">地区</li><li class="s" style="width:180px;padding-left:0;text-align:center;">街道地址</li><li class="p" style="padding-left:0;text-align:center;width:60px;">邮编</li><li class="q" style="padding-left:0;text-align:center;width:120px;">电话/手机</li><li class="p" style="padding-left:0;width:65px;text-align:center;">身份证</li><li class="q" style="width:60px;padding-left:0;"></li><li class="r" style="padding-left:0;width:90px;text-align:center;">操作</li></ol></li><li style="height:48px;line-height:48px;text-align:center;">暂无收货地址</li></ul>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var fileDirectory = "MemberIDCard";
        document.domain = domainUrl;

        $(function () {
            var browserInfo = getBrowserInfo()[0].toLowerCase();
            if (browserInfo != undefined && browserInfo != null && browserInfo != "") {
                if (browserInfo.indexOf("msie") > -1) {
                    $("input[type='text']").css({ "overflow": "auto", "height": "32px", "line-height": "32px", "vertical-align": "middle" });
                } else {
                    $("input[type='text']").css({ "overflow": "auto", "height": "32px", "line-height": "normal" });
                }
            } else {
                $("input[type='text']").css({ "overflow": "auto", "height": "32px", "line-height": "normal" });
            }
        });

        function pageX(elem) {
            return elem.offsetParent ? (elem.offsetLeft + pageX(elem.offsetParent)) : elem.offsetLeft;
        }

        function pageY(elem) {
            return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
        }
        //-----------------------------上传事件控件绑定---------------------
        function ImgUploadConfig(fileUrl, fileID, path, saveUrl, saveOriginalUrl, showImg) {
            if (fileID == undefined) {
                return;
            }
            fileID.uploadify({
                'auto': true, //选定文件后是否自动上传，默认false
                'fileDesc': '*.jpg;*.png;*.gif', //出现在上传对话框中的文件类型描述
                'height': 36, // The height of the flash button
                'width': 120, // The width of the flash button
                'uploader': '/Common/uploadifyp.swf',
                'fileDataName': 'imgFile',
                'buttonImg': path,
                'fileExt': '*.jpg;*.gif;*.png', //控制可上传文件的扩展名，启用本项时需同时声明fileDesc
                'isonSelect': false, // The height of the flash button
                'sizeLimit': 1572864, //控制上传文件的大小，单位byte common.js中定义
                'onSelect': function (e, queueId, fileObj) {
                    var size = fileObj.size;
                    if (size > 1572864) {
                        $.Beyond.Warn("请将图片尺寸控制在1.5MB以下");
                    }
                    $.BDEX.Wait();
                },
                'scriptData': { 'FileType': 'Image', 'F': fileUrl },
                'onComplete': function (e, queueID, fileObj, response, data) {
                    $.BDEX.Close();
                    if (response != undefined && response.indexOf("|") > -1) {
                        var arr = response.split("|");
                        saveUrl.val(arr[1]);
                        //$(".pass").show();
                        showImg.attr("src", arr[2]);
                        saveOriginalUrl.val(arr[3]);
                    }
                }
            });
        };
    </script>
    <script type="text/javascript" src="${mainServer}/resource/js/area.js"></script>
    <script type="text/javascript" src="${mainServer}/resource/js/kindeditor-min.js"></script>
    <script type="text/javascript" src="${mainServer}/resource/js/cutImg.js"></script>
    <script type="text/javascript" src="${mainServer}/resource/js/destination.js"></script>
    <script type="text/javascript" src="${mainServer}/resource/js/jquery.uploadify.v2.1.0.js"></script>
    <style type="text/css">
        .uploadifyQueue
        {
            display: none;
        }
    </style>
    <div id="foot">
     <div class="mian">
          <span class="pull-right"><img src="${mainServer}/resource/img/logo.gif" alt="翔锐物流"></span>
          
          <div class="pull-left">
              <h5>联系我们</h5>
              <p>4008-890-788</p>
              <p>0755-86054577</p>
              <p id="QQ"><!-- WPA Button Begin -->
		<script charset="utf-8" type="text/javascript" src="${mainServer}/resource/img/wpa.php"></script>
<!-- WPA Button End --></p>
          </div>
          <div class="pull-left">
              <h5>邮件</h5>
              <p><a href="mailto:service@birdex.cn" style="color:#FFFFFF">service@birdex.cn</a></p>
          </div>
          <div class="pull-left" style="width:200px">
            <h5>友情链接</h5>
            <a href="http://www.etao.com/tuan/index.html?partnerid=4852" target="_blank" style="color:#FFFFFF">一淘海淘团</a><br>
<a href="http://www.smzdm.com/" target="_blank" style="color:#FFFFFF">什么值得买</a><br>
            <a href="http://www.55haitao.com/" target="_blank" style="color:#FFFFFF">55海淘</a>
            
          </div>
     </div>
    <div class="mian" style="margin:0 auto;text-align:center;color:#818181;font-size:14px;vertical-align:bottom;">
        <div>粤ICP备14015306号-1</div> 
     </div>
</div>
<div style="display:none;">
<script type="text/javascript">
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F68c503fc431b1f102540a9c79f1ffc78' type='text/javascript'%3E%3C/script%3E"));
</script>
<script src="${mainServer}/resource/js/h.js" type="text/javascript"></script>
<a href="http://tongji.baidu.com/hm-web/welcome/ico?s=68c503fc431b1f102540a9c79f1ffc78" target="_blank">
<img border="0" src="${mainServer}/resource/img/21.gif" width="20" height="20"></a>
</div>
<div id="lrToolRb" class="lr-tool-rb" style="z-index:10000;"><a class="lr-gotop" title="返回顶部" style="color: rgb(102, 102, 102);"><span class="lricon iconArrow" style="background-image: url(http://img.birdex.cn//images/gototop01.jpg);"></span><br>TOP</a><a class="lr-QQ" title="单击联系QQ客服" onclick="SetQQ();" style="color: rgb(102, 102, 102);"><span class="lricon"></span><br>联系客服</a><a class="lr-qrcode" title="关注获取最新优惠，实时跟踪包裹进度" style="color: rgb(102, 102, 102);"><span class="lricon"></span><br>微信关注</a><div class="lr-pop"><span class="lr-close">关闭</span><span class="lr-qcimg"></span></div></div>
<div times="13" showtime="[object Object]" style="z-index: 19891027; width: auto; height: auto; position: absolute; margin-left: 0px; left: 937px; top: 182px;" id="xubox_layer13" class="xubox_layer" type="tips">
<div style="background-color:; z-index:19891027" class="xubox_main"><div class="xubox_tips" style="color: rgb(255, 255, 255); padding-right: 10px; background-color: rgb(15, 166, 216);">
<div class="xubox_tipsMsg">请选择省市！</div><i class="layerTipsG layerTipsR" style="border-bottom-color: rgb(15, 166, 216);"></i></div><span class="xubox_botton"></span></div></div></body></html>