package com.xiangrui.lmp.business.admin.pkg.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.PkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.pkg.service.PkgAttachServiceService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachService;
import com.xiangrui.lmp.business.admin.pkg.vo.PkgAttachServiceGroup;
import com.xiangrui.lmp.business.admin.store.mapper.StorePkgAttachServiceMapper;
import com.xiangrui.lmp.business.admin.store.service.impl.StoreServiceImpl;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.base.BaseAttachService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.NumberUtils;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.StringUtil;

@Service(value = "pkgAttachServiceService")
public class PkgAttachServiceServiceImpl implements PkgAttachServiceService
{
	@Autowired
	private PkgAttachServiceMapper pkgAttachServiceMapper;
	@Autowired
	private StorePkgAttachServiceMapper storePkgAttachServiceMapper;

	@Autowired
	private PackageMapper packageMapper;

	/**
	 * 增值服务
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public List<PkgAttachService> queryAllPkgAttachService(Map<String, Object> params)
	{

		return pkgAttachServiceMapper.queryAllPkgAttachService(params);

	}

	@Override
	public List<PkgAttachServiceGroup> queryPkgAttachServiceGroup(Map<String, Object> params)
	{

		List<PkgAttachServiceGroup> list = pkgAttachServiceMapper.queryPkgAttachServiceGroup(params);
		for (PkgAttachServiceGroup serviceGroup : list)
		{

			String package_id_group = serviceGroup.getOg_package_id_group();
			// 分隔符
			String separator = ", ";
			StringBuffer logistics_codeBuffer = new StringBuffer();

			StringBuffer original_numBuffer = new StringBuffer();

			StringBuffer serviceableDesc = new StringBuffer();

			if (null != package_id_group && !package_id_group.trim().equals(""))
			{
				String[] strs = package_id_group.split(",");

				for (String str : strs)
				{
					if (!str.trim().equals(""))
					{
						int package_id = Integer.parseInt(str.trim());
						Pkg pkg = packageMapper.queryPackageById(package_id);
						if (pkg == null)
						{
							continue;
						}
						logistics_codeBuffer.append(pkg.getLogistics_code());
						logistics_codeBuffer.append(separator);
						String original_num = pkg.getOriginal_num() == null ? "" : pkg.getOriginal_num();
						original_numBuffer.append(original_num);
						original_numBuffer.append(separator);

						// 未到库包裹 信息
						if (Pkg.STORE_PACKAGE_NOT_ARRIVED == pkg.getArrive_status())
						{
							serviceableDesc.append(pkg.getLogistics_code() + "未到库");
							serviceableDesc.append(separator);
						}
					}
				}

				String opt_status_str = serviceGroup.getOpt_status_str();

				String status = String.valueOf(PkgAttachService.ATTACH_UNFINISH);

				// 只要包含有未完成状态的，则标记为未完成
				if ((null != opt_status_str && opt_status_str.contains(status)) || null == opt_status_str)
				{
					serviceGroup.setStatus(PkgAttachService.ATTACH_UNFINISH);
				}
				else
				{
					serviceGroup.setStatus(PkgAttachService.ATTACH_FINISH);
				}

				if (logistics_codeBuffer.length() > 0)
				{
					// 删除最后一个分隔符
					logistics_codeBuffer.deleteCharAt(logistics_codeBuffer.length() - separator.length());
				}

				if (original_numBuffer.length() > 0)
				{
					original_numBuffer.deleteCharAt(original_numBuffer.length() - separator.length());
				}
				serviceGroup.setOg_logistics_codes(logistics_codeBuffer.toString());
				serviceGroup.setOg_original_nums(original_numBuffer.toString());

				boolean serviceable = true;

				// 任意一个增值服务未
				if (serviceableDesc.length() > 0)
				{
					serviceableDesc.deleteCharAt(serviceableDesc.length() - separator.length());
					serviceable = false;
				}
				else
				{
					serviceableDesc.append("包裹已经到库");
					serviceable = true;
				}
				serviceGroup.setServiceableDesc(serviceableDesc.toString());
				serviceGroup.setServiceable(serviceable);
			}

			String pkgDescription = StringUtils.trimToEmpty(serviceGroup.getPkgDescription());
			if (pkgDescription.contains("分箱(自动)"))
			{
				String serviceName = serviceGroup.getService_names();
				serviceName = serviceName.replace("分箱", "分箱(自动)");
				serviceGroup.setService_names(serviceName);
			}

		}

		return list;

	}

	@Override
	public List<PkgAttachServiceGroup> queryPkgAttachServiceGroupForScanDisplay(int pkgId)
	{

		List<PkgAttachServiceGroup> list = queryAllPkgAttachServiceByPkgId(pkgId);
		for (PkgAttachServiceGroup serviceGroup : list)
		{

			String package_id_group = serviceGroup.getOg_package_id_group();

			// 分隔符
			String separator = ", ";
			StringBuffer logistics_codeBuffer = new StringBuffer();

			StringBuffer original_numBuffer = new StringBuffer();

			StringBuffer serviceableDesc = new StringBuffer();

			if (null != package_id_group && !package_id_group.trim().equals(""))
			{
				String[] strs = package_id_group.split(",");

				for (String str : strs)
				{
					if (!str.trim().equals(""))
					{
						int package_id = Integer.parseInt(str.trim());
						Pkg pkg = packageMapper.queryPackageById(package_id);
						if (pkg == null)
						{
							continue;
						}
						logistics_codeBuffer.append(pkg.getLogistics_code());
						logistics_codeBuffer.append(separator);
						String original_num = pkg.getOriginal_num() == null ? "" : pkg.getOriginal_num();
						original_numBuffer.append(original_num);
						original_numBuffer.append(separator);

						// 未到库包裹 信息
						if (Pkg.STORE_PACKAGE_NOT_ARRIVED == pkg.getArrive_status())
						{
							serviceableDesc.append(pkg.getLogistics_code() + "未到库");
							serviceableDesc.append(separator);
						}
					}
				}

				String opt_status_str = serviceGroup.getOpt_status_str();

				String status = String.valueOf(PkgAttachService.ATTACH_UNFINISH);

				// 只要包含有未完成状态的，则标记为未完成
				if ((null != opt_status_str && opt_status_str.contains(status)) || null == opt_status_str)
				{
					serviceGroup.setStatus(PkgAttachService.ATTACH_UNFINISH);
				}
				else
				{
					serviceGroup.setStatus(PkgAttachService.ATTACH_FINISH);
				}

				if (logistics_codeBuffer.length() > 0)
				{
					// 删除最后一个分隔符
					logistics_codeBuffer.deleteCharAt(logistics_codeBuffer.length() - separator.length());
				}

				if (original_numBuffer.length() > 0)
				{
					original_numBuffer.deleteCharAt(original_numBuffer.length() - separator.length());
				}
				serviceGroup.setOg_logistics_codes(logistics_codeBuffer.toString());
				serviceGroup.setOg_original_nums(original_numBuffer.toString());

				boolean serviceable = true;

				// 任意一个增值服务未
				if (serviceableDesc.length() > 0)
				{
					serviceableDesc.deleteCharAt(serviceableDesc.length() - separator.length());
					serviceable = false;
				}
				else
				{
					serviceableDesc.append("包裹已经到库");
					serviceable = true;
				}
				serviceGroup.setServiceableDesc(serviceableDesc.toString());
				serviceGroup.setServiceable(serviceable);
			}
		}

		return list;

	}

	/**
	 * 更新
	 * 
	 * @param pkgAttachService
	 * @param user
	 *            包裹日志操作人
	 * @return
	 */
	@SystemServiceLog(description = "处理增值服务")
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public int updatePkgAttachServiceAndPackage(Map<String, Object> params, User user)
	{
		String package_idstr = (String) params.get("package_ids");
		Map<String, String> package_ids = JSONTool.getMapFromJson(package_idstr);
		int attach_id = (Integer) params.get("attach_id");

		int cnt = 0;
		for (Entry<String, String> entry : package_ids.entrySet())
		{
			String package_id = entry.getKey();
			int status = Integer.parseInt(entry.getValue());
			Map<String, Object> attachMap = new HashMap<String, Object>();
			attachMap.put("package_id", Integer.parseInt(package_id));
			attachMap.put("attach_id", attach_id);
			attachMap.put("status", status);

			PkgAttachService temp = new PkgAttachService();
			temp.setAttach_id(attach_id);
			temp.setPackage_id(Integer.parseInt(package_id));

			PkgAttachService pkgAttachService = pkgAttachServiceMapper.queryAllPkgAttachServiceById(temp);

			// 当前增值服务状态不相同，则需要变更合计价格
			if (pkgAttachService.getStatus() != status)
			{

				Pkg currPkg = packageMapper.queryPackageById(Integer.parseInt(package_id));
				float currTransport_cost = currPkg.getTransport_cost();

				Pkg pkg = new Pkg();

				pkg.setPackage_id(Integer.parseInt(package_id));

				float totalTransport_cost = 0;

				// 说明增值服务费为未完成变成已完成，则需要合计累加
				if (PkgAttachService.ATTACH_FINISH == status)
				{

					// 现有的合计累加上增值服务价
					totalTransport_cost = (float) NumberUtils.add(currTransport_cost, pkgAttachService.getService_price());

					pkg.setTransport_cost(totalTransport_cost);

					packageMapper.update(pkg);
				}

				// 说明 增值服务由完成变成未完成，则需要从合计中减去累加的价格
				if (PkgAttachService.ATTACH_UNFINISH == status)
				{

					// 现有的合计减去增值服务价
					totalTransport_cost = (float) NumberUtils.subtract(currTransport_cost, pkgAttachService.getService_price());

					pkg.setTransport_cost(totalTransport_cost);

					packageMapper.update(pkg);
				}

			}

			// 更新增值服务状态
			cnt = pkgAttachServiceMapper.updatePkgAttachService(attachMap);
		}

		String abandon_packages = (String) params.get("abandon_packages");

		// 废弃
		if (StringUtil.isNotEmpty(abandon_packages))
		{
			List<String> abandon_package_ids = StringUtil.toStringList(abandon_packages, ",");
			List<Pkg> packageList = new ArrayList<Pkg>();
			for (String id : abandon_package_ids)
			{
				Pkg pkg = new Pkg();
				pkg.setPackage_id(Integer.parseInt(id));
				pkg.setStatus(Pkg.LOGISTICS_DISCARD);
				packageList.add(pkg);

				// 同步日志操作记录
				PackageLogUtil.log(pkg, user);
			}
			// 更新包裹的状态：废弃
			packageMapper.batchUpdateStatus(packageList);
		}

		return cnt;
	}

	/**
	 * 包裹所有的增值服务
	 * 
	 * @param package_id
	 * @return
	 */
	@Override
	public List<PkgAttachService> queryPkgAttachServiceBypackageId(int package_id)
	{
		return pkgAttachServiceMapper.queryPkgAttachServiceBypackageId(package_id);
	}

	@Override
	public PkgAttachService queryAllPkgAttachServiceById(int package_id, int attach_id)
	{
		PkgAttachService attachService = new PkgAttachService();
		attachService.setAttach_id(attach_id);
		attachService.setPackage_id(package_id);
		return pkgAttachServiceMapper.queryAllPkgAttachServiceById(attachService);
	}
	
	public List<PkgAttachService> queryAllPkgAttachServiceByPackageGroup(int package_group, int attach_id)
	{
		PkgAttachService attachService = new PkgAttachService();
		attachService.setAttach_id(attach_id);
		attachService.setPackage_group(""+package_group);
		return pkgAttachServiceMapper.queryAllPkgAttachServiceByPackageGroup(attachService);
	}

	@SystemServiceLog(description = "为包裹创建增值服务")
	@Override
	public void addPkgAttachService(PkgAttachService pkgAttachService)
	{
		pkgAttachServiceMapper.addPkgAttachService(pkgAttachService);
	}

	@SystemServiceLog(description = "为包裹创建增值服务")
	@Override
	public void addPkgAttachServiceOnList(List<PkgAttachService> list)
	{
		for (PkgAttachService pkgAttachService : list)
		{
			pkgAttachServiceMapper.addPkgAttachService(pkgAttachService);
		}
	}

	@Override
	public void pkgAttachServiceClear(PkgAttachService pkgAttachService)
	{
		pkgAttachServiceMapper.delPkgAttachService(pkgAttachService);
	}

	/**
	 * 计算总增值服务费用用
	 * 
	 * @param package_id
	 */
	@Override
	public List<PkgAttachService> queryPkgAttachServiceByPackage(int package_id)
	{

		return pkgAttachServiceMapper.queryPkgAttachServiceByPackage(package_id);
	}

	/**
	 * 根据参数查询待分箱包裹的子包裹的增值服务
	 * 
	 * @param params
	 * @return
	 */
	public List<PkgAttachService> queryForSplit(Map<String, Object> params)
	{
		return pkgAttachServiceMapper.queryForSplit(params);
	}

	/**
	 * 更新包裹增值服务
	 * 
	 * @param params
	 * @return
	 */
	@SystemServiceLog(description = "非分箱合箱增值服务操作")
	public int updatePkgAttachService(Map<String, Object> params, User user)
	{
		String attach_idsStr = (String) params.get("attach_ids");
		Map<String, Integer> attach_ids = JSONTool.getMapFromJson(attach_idsStr);
		Integer package_id = (Integer) params.get("package_id");

		// 查询当前增值服务的包裹信息
		Pkg currPkg = packageMapper.queryPackageById(package_id);

		float totalTransport_cost = currPkg.getTransport_cost();

		Pkg pkg = new Pkg();
		pkg.setStatus(0);
		int cnt = 0;
		for (Entry<String, Integer> entry : attach_ids.entrySet())
		{
			boolean needToUpdate = true;
			String attach_id = entry.getKey();

			Map<String, Object> attachMap = new HashMap<String, Object>();
			attachMap.put("package_id", package_id);
			attachMap.put("attach_id", Integer.parseInt(attach_id));

			int status = entry.getValue();
			attachMap.put("status", entry.getValue());

			PkgAttachService temp = new PkgAttachService();
			temp.setAttach_id(Integer.parseInt(attach_id));
			temp.setPackage_id(package_id);

			// 查询当前的增值服务信息
			PkgAttachService pkgAttachService = pkgAttachServiceMapper.queryAllPkgAttachServiceById(temp);

			// 当前增值服务状态不相同，则需要变更合计价格
			if (pkgAttachService.getStatus() != status)
			{

				// 说明增值服务费为未完成变成已完成，则需要合计累加
				if (PkgAttachService.ATTACH_FINISH == status)
				{

					// 现有的合计累加上增值服务价
					totalTransport_cost = (float) NumberUtils.add(totalTransport_cost, pkgAttachService.getService_price());
					// 完成退货
					if (Integer.parseInt(attach_id) == 29)
					{
						pkg.setStatus(BasePackage.LOGISTICS_RETURN);
						pkg.setLogistics_code(currPkg.getLogistics_code());
						pkg.setPackage_id(package_id);
						pkg.setTransport_cost(currPkg.getTransport_cost());
						packageMapper.update(pkg);
						List<Pkg> packageList = new ArrayList<Pkg>();
						packageList.add(pkg);
						packageList = packageMapper.queryPackageBylogisticsCodes(packageList);

						for (Pkg basePkg : packageList)
						{
							PackageLogUtil.log(basePkg, user);
						}
					}

				}

				// 说明 增值服务由完成变成未完成，则需要从合计中减去累加的价格
				if (PkgAttachService.ATTACH_UNFINISH == status)
				{
						// 现有的合计减去增值服务价
						totalTransport_cost = (float) NumberUtils.subtract(totalTransport_cost, pkgAttachService.getService_price());
				}
					// 更新增值服务状态：完成
					cnt = pkgAttachServiceMapper.updatePkgAttachService(attachMap);
			}
		}

		// 更新包裹总运费

		pkg.setPackage_id(package_id);
		pkg.setTransport_cost(totalTransport_cost);
		packageMapper.update(pkg);
		return cnt;
	}

	public void updatePkgAttachServicePrice(Map<String, Object> params)
	{
		pkgAttachServiceMapper.updatePkgAttachServicePrice(params);
	}

	private boolean checkSplitAllServiceDisplay(List<PkgAttachServiceGroup> list, String addachId)
	{
		boolean allDisplay = false;
		boolean containsSplitService = false;
		if (list != null && list.size() > 0)
		{
			for (PkgAttachServiceGroup serviceGroup : list)
			{
				if (serviceGroup.getAttach_ids().equals("" + BaseAttachService.SPLIT_PKG_ID))
				{
					containsSplitService = true;
				}
			}
		}
		if (containsSplitService)
		{
			if (!("" + BaseAttachService.SPLIT_PKG_ID).equals(addachId))
			{
				allDisplay = true;
			}
		}
		return allDisplay;
	}

	public List<PkgAttachServiceGroup> queryAllPkgAttachServiceByPkgId(int pkgId)
	{
		// 查询包裹增值服务
		List<PkgAttachServiceGroup> allPkgAttachServiceList = new ArrayList<PkgAttachServiceGroup>();
		List<PkgAttachServiceGroup> pkgAttachServiceList = storePkgAttachServiceMapper.queryPkgAttach(pkgId);
		allPkgAttachServiceList.addAll(pkgAttachServiceList);
		if (pkgAttachServiceList != null && pkgAttachServiceList.size() == 1)
		{
			PkgAttachServiceGroup tempPkgAttachServiceGroup = pkgAttachServiceList.get(0);
			if (tempPkgAttachServiceGroup.getAttach_ids().equals("" + BaseAttachService.SPLIT_PKG_ID) || tempPkgAttachServiceGroup.getAttach_ids().equals("" + BaseAttachService.MERGE_PKG_ID))
			{
				String tgtPackageIdGroup = tempPkgAttachServiceGroup.getTgt_package_id_group();
				if (!tgtPackageIdGroup.contains("" + pkgId))
				{
					String[] needQueryPkgAttachServicePackageIds = tgtPackageIdGroup.split(",");
					if (needQueryPkgAttachServicePackageIds != null && needQueryPkgAttachServicePackageIds.length > 0)
					{
						for (String needQueryPkgAttachServicePackageId : needQueryPkgAttachServicePackageIds)
						{
							List<PkgAttachServiceGroup> partPkgAttachServiceList = storePkgAttachServiceMapper.queryPkgAttach(Integer.parseInt(needQueryPkgAttachServicePackageId));
							if (partPkgAttachServiceList != null && partPkgAttachServiceList.size() > 0)
							{
								for (PkgAttachServiceGroup tempPartPkgAttachServiceGroup : partPkgAttachServiceList)
								{
									if (!StoreServiceImpl.isPkgAttachServiceGroupListContains(allPkgAttachServiceList, tempPartPkgAttachServiceGroup))
									{
										allPkgAttachServiceList.add(tempPartPkgAttachServiceGroup);
									}
								}
							}
						}
					}
				}
			}
		}
		return allPkgAttachServiceList;
	}
}
