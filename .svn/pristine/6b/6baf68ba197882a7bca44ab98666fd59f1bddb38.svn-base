package com.xiangrui.lmp.business.admin.pkg.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.UnusualPkgService;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.UnusualPkg;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.homepage.controller.HomePkgGoodsController;
import com.xiangrui.lmp.business.homepage.service.FrontUserAddressService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.constant.PkgConstant;
import com.xiangrui.lmp.util.PageView;
import com.xiangrui.lmp.util.StringUtil;

@Controller
@RequestMapping("/admin/unusualPkg")
public class UnusualPkgController
{

	/**
	 * 检索条件 记录
	 */
	private static final String SESSION_KEY_SEARCH_CONDITION = "search_condition";

	@Autowired
	private PackageService packageService;

	@Autowired
	private UnusualPkgService unusualPkgService;
	@Autowired
	private OverseasAddressService overseasAddressService;
	@Autowired
	private FrontUserAddressService frontUserAddressService;
	/**
	 * 包裹商品
	 */
	@Autowired
	private PkgGoodsService frontPkgGoodsService;

	@RequestMapping("/addpkg")
	public String addPkg(HttpServletRequest req, HttpServletResponse resp, UnusualPkg pkg)
	{
		if (!privateCheckExist(pkg.getOriginal_num()))
		{
			Timestamp creatTime = new Timestamp(System.currentTimeMillis());
			pkg.setCreate_time(creatTime);
			User user = (User) req.getSession().getAttribute("back_user");
			pkg.setCreate_user(user.getUser_id());
			pkg.setUnusual_status(UnusualPkg.STATUS_UNUSUAL);
			unusualPkgService.insertPkg(pkg);
			UnusualPkg addedpkg = unusualPkgService.queryUnusualPkgByOriginal_num(pkg.getOriginal_num());
			// 尝试关联包裹
			unusualPkgService.connectPackage(addedpkg);
		}
		return "back/pkgaddsuccess";
	}

	private boolean privateCheckExist(String original_num)
	{
		boolean isExist = false;
		Pkg pkg = packageService.queryPackageByOriginal_num(original_num);
		UnusualPkg unusualPkg = unusualPkgService.queryUnusualPkgByOriginal_num(original_num);

		if (pkg != null)
		{
			isExist = true;

		}
		else if (unusualPkg != null)
		{
			isExist = true;

		}

		return isExist;
	}

	/**
	 * 异常包裹 初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/unusualPkgList")
	public String unusualPkgList(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}
		Map<String, Object> paramsO = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			paramsO.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
			params.put("user_id", user.getUser_id());
		}

		// 分页查询参数
		params.put("pageView", pageView);

		List<UnusualPkg> unusualPkgList = unusualPkgService.queryList(params);
		checkDoneOrder(unusualPkgList);
		request.setAttribute("pageView", pageView);
		request.setAttribute("unusualPkgList", unusualPkgList);
		return "back/abnormalPkgList";
	}

	@RequestMapping("/search")
	public String search(HttpServletRequest request, String original_num, String fromDate, String toDate, String unusual_status, String osaddr_id, String last_name, String down_flag)
	{
		// 分页查询
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		}
		else
		{
			pageView = new PageView(1);

			// 异常包裹
			params.put("status", PkgConstant.LOGISTICS_UNUSUALLY);

			// 关联单号
			params.put("original_num", original_num);

			// 起始时间
			params.put("fromDate", fromDate);

			// 终止时间
			params.put("toDate", toDate);

			// 认领状态
			params.put("unusual_status", unusual_status);

			// 海外仓库ID
			params.put("overseasAddress_id", osaddr_id);
			// LAST_NAME
			params.put("last_name", last_name);

			params.put("down_flag", down_flag);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}
		Map<String, Object> paramsO = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			paramsO.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
			// 用户ID
			params.put("user_id", user.getUser_id());
		}
		// 分页查询参数
		params.put("pageView", pageView);

		// 异常包裹查询
		List<UnusualPkg> unusualPkgList = unusualPkgService.queryList(params);
		checkDoneOrder(unusualPkgList);
		// 页面显示信息
		request.setAttribute("unusualPkgList", unusualPkgList);

		request.setAttribute("pageView", pageView);

		// 查询条件保持
		request.setAttribute("params", params);

		return "back/abnormalPkgList";
	}

	/**
	 * 仓库菜单下异常包裹 初始化
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/input_unusualPkgList")
	public String input_unusualPkgList(HttpServletRequest request)
	{
		// 分页查询
		PageView pageView = null;
		Map<String, Object> params = new HashMap<String, Object>();
		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);
		}
		else
		{
			pageView = new PageView(1);
		}
		Map<String, Object> paramsO = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			paramsO.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
			params.put("user_id", user.getUser_id());
		}

		// 分页查询参数
		params.put("pageView", pageView);

		List<UnusualPkg> unusualPkgList = unusualPkgService.queryList(params);
		checkDoneOrder(unusualPkgList);
		request.setAttribute("pageView", pageView);
		request.setAttribute("unusualPkgList", unusualPkgList);
		return "back/abnormalPkgList_input";
	}

	@RequestMapping("/input_search")
	public String input_search(HttpServletRequest request, String original_num, String fromDate, String toDate, String unusual_status, String osaddr_id, String last_name, String down_flag)
	{
		// 分页查询
		PageView pageView = null;

		Map<String, Object> params = new HashMap<String, Object>();

		String pageIndex = request.getParameter("pageIndex");
		if (StringUtil.isNotEmpty(pageIndex))
		{
			int pageCurrent = Integer.parseInt(pageIndex);
			pageView = new PageView(pageCurrent);

			params = (Map<String, Object>) request.getSession().getAttribute(SESSION_KEY_SEARCH_CONDITION);
		}
		else
		{
			pageView = new PageView(1);

			// 异常包裹
			params.put("status", PkgConstant.LOGISTICS_UNUSUALLY);

			// 关联单号
			params.put("original_num", original_num);

			// 起始时间
			params.put("fromDate", fromDate);

			// 终止时间
			params.put("toDate", toDate);

			// 认领状态
			params.put("unusual_status", unusual_status);

			// 海外仓库ID
			params.put("overseasAddress_id", osaddr_id);
			// LAST_NAME
			params.put("last_name", last_name);
			params.put("down_flag", down_flag);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);
		}
		Map<String, Object> paramsO = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			paramsO.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService.queryOverseasAddressByUserId(user.getUser_id());
			request.setAttribute("overseasAddressList", overseasAddressList);
			// 用户ID
			params.put("user_id", user.getUser_id());
		}
		// 分页查询参数
		params.put("pageView", pageView);

		// 异常包裹查询
		List<UnusualPkg> unusualPkgList = unusualPkgService.queryList(params);
		checkDoneOrder(unusualPkgList);
		// 页面显示信息
		request.setAttribute("unusualPkgList", unusualPkgList);

		request.setAttribute("pageView", pageView);

		// 查询条件保持
		request.setAttribute("params", params);

		return "back/abnormalPkgList_input";
	}

	@RequestMapping("/validateExist")
	@ResponseBody
	public Map<String, Object> validateExist(String original_num)
	{
		Pkg pkg = packageService.queryPackageByOriginal_num(original_num);
		UnusualPkg unusualPkg = unusualPkgService.queryUnusualPkgByOriginal_num(original_num);

		Map<String, Object> result = new HashMap<String, Object>();

		if (pkg != null||HomePkgGoodsController.checkOriginalNumExist(original_num))
		{
			result.put("result", true);

		}
		else if (unusualPkg != null)
		{
			result.put("result", true);

		}
		else
		{
			result.put("result", false);
		}
		return result;

	}

	/**
	 * 异常包裹编辑初始化
	 * 
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequestMapping("/unusualPkgEditInt")
	public String unusualPkgEditInt(HttpServletRequest req, HttpServletResponse resp, String unusual_pkg_id)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		UnusualPkg unusualPkg = unusualPkgService.queryUnusualPkgByUnusual_pkg_id(Integer.parseInt(unusual_pkg_id));
		// 获取未删除地址
		params.put("isdeleted", OverseasAddress.ISDELETE_UNDELETED);
		Map<String, Object> paramsO = new HashMap<String, Object>();
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("back_user");
		if (user != null)
		{
			paramsO.put("user_id", user.getUser_id());
			List<OverseasAddress> overseasAddressList = overseasAddressService.queryOverseasAddressByUserId(user.getUser_id());
			req.setAttribute("overseasAddressList", overseasAddressList);
		}
		req.setAttribute("unusualPkg", unusualPkg);

		return "back/unusual_pkg_edit";
	}

	/**
	 * 异常包裹编辑后保存
	 * 
	 * @param req
	 * @param resp
	 * @param pkg
	 * @return
	 */
	@RequestMapping("/unusualPkgEdt")
	public String unusualPkgEdit(HttpServletRequest req, HttpServletResponse resp, UnusualPkg pkg)
	{
		UnusualPkg editpkg = unusualPkgService.queryUnusualPkgByUnusual_pkg_id(pkg.getUnusual_pkg_id());
		editpkg.setAccount(pkg.getAccount());
		editpkg.setActual_weight(pkg.getActual_weight());
		editpkg.setLast_name(pkg.getLast_name());
		editpkg.setOsaddr_id(pkg.getOsaddr_id());
		editpkg.setRemark(pkg.getRemark());
		unusualPkgService.updateUnusualPkg(editpkg);
		// 先取消关联
		if (editpkg.getConnect_package_id() > 0)
		{
			unusualPkgService.disConnectPackage(pkg.getUnusual_pkg_id());
		}
		// 尝试关联包裹
		unusualPkgService.connectPackage(editpkg);
		return "back/unusual_pkg_editsuccess";
	}

	/**
	 * 关联异常包裹初始化
	 * 
	 * @param unusual_pkg_id
	 * @param lastName
	 * @param account
	 * @return
	 */
	@RequestMapping("/connectUnusualPackageInit")
	public String connectUnusualPackageInit(HttpServletRequest request, int unusual_pkg_id)
	{
		UnusualPkg unusualPkg = unusualPkgService.queryUnusualPkgByUnusual_pkg_id(unusual_pkg_id);
		request.setAttribute("unusualPkg", unusualPkg);
		return "back/unusual_pkg_connect";
	}

	/**
	 * 关联异常包裹
	 * 
	 * @param unusual_pkg_id
	 * @param lastName
	 * @param account
	 * @return
	 */
	@RequestMapping("/connectUnusualPackage")
	@ResponseBody
	public Map<String, Object> connectUnusualPackage(HttpServletRequest request, int unusual_pkg_id, String lastName, String account)
	{
		UnusualPkg unusualPkg = unusualPkgService.queryUnusualPkgByUnusual_pkg_id(unusual_pkg_id);
		Map<String, Object> result = new HashMap<String, Object>();
		unusualPkg.setLast_name(lastName);
		unusualPkg.setAccount(account);
		if (unusualPkgService.connectPackage(unusualPkg))
		{
			result.put("result", true);
		}
		else
		{
			result.put("result", false);
		}
		return result;
	}

	/**
	 * 取消关联异常包裹
	 * 
	 * @param unusual_pkg_id
	 * @param lastName
	 * @param account
	 * @return
	 */
	@RequestMapping("/disConnectUnusualPackage")
	public String disConnectUnusualPackage(HttpServletRequest request, int unusual_pkg_id)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		if (unusualPkgService.disConnectPackage(unusual_pkg_id))
		{
			result.put("result", true);
		}
		else
		{
			result.put("result", false);
		}
		return unusualPkgList(request);
	}

	/**
	 * 导出异常包裹
	 * 
	 * @param request
	 * @param original_num
	 * @param fromDate
	 * @param toDate
	 * @param unusual_status
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/exportUnusualPackage")
	public void exportUnusualPackage(HttpServletRequest request, HttpServletResponse response, String original_num, String fromDate, String toDate, String unusual_status, String osaddr_id, String last_name, String down_flag)
	{

		try
		{
			// 设置response方式,使执行此controller时候自动出现下载页面,而非直接使用excel打开
			response.reset();
			// 中文名称
			String fileName = "异常包裹";

			response.setContentType("multipart/form-data");
			// 注意，如果去掉下面一行代码中的attachment; 那么也会使IE自动打开文件。
			response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("GB2312"), "ISO-8859-1") + ".xlsx");
			request.setCharacterEncoding("UTF-8");
			Map<String, Object> params = new HashMap<String, Object>();
			// 异常包裹
			params.put("status", PkgConstant.LOGISTICS_UNUSUALLY);
			// 关联单号
			params.put("original_num", original_num);
			// 起始时间
			params.put("fromDate", fromDate);
			// 终止时间
			params.put("toDate", toDate);
			// 认领状态
			params.put("unusual_status", unusual_status);
			// 海外仓库ID
			params.put("overseasAddress_id", osaddr_id);
			// LAST_NAME
			params.put("last_name", last_name);
			params.put("down_flag", down_flag);
			request.getSession().setAttribute(SESSION_KEY_SEARCH_CONDITION, params);

			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("back_user");
			if (user != null)
			{
				// 用户ID
				params.put("user_id", user.getUser_id());
			}
			// 异常包裹查询
			List<UnusualPkg> unusualPkgList = unusualPkgService.queryList(params);
			XSSFWorkbook xssfWorkbook = unusualPkgService.createUnusualPkgWorkBook(fileName, unusualPkgList);
			OutputStream oStream = response.getOutputStream();
			xssfWorkbook.write(oStream);
			oStream.flush();
			oStream.close();
		} catch (IOException e)
		{
			e.printStackTrace();

		}
	}

	@RequestMapping("/validateSamePerson")
	@ResponseBody
	public Map<String, Object> validateSamePerson(String account, String lastName)
	{

		Map<String, Object> result = new HashMap<String, Object>();
		if (unusualPkgService.validateSamePerson(account, lastName))
		{
			result.put("result", true);
		}
		else
		{
			result.put("result", false);
		}
		return result;
	}

	@RequestMapping("/validateAccount")
	@ResponseBody
	public Map<String, Object> validateAccount(String account)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		if (unusualPkgService.validateAccount(account))
		{
			result.put("result", true);
		}
		else
		{
			result.put("result", false);
		}
		return result;
	}

	@RequestMapping("/validateLastName")
	@ResponseBody
	public Map<String, Object> validateLastName(String lastName)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		if (unusualPkgService.validateLastName(lastName))
		{
			result.put("result", true);
		}
		else
		{
			result.put("result", false);
		}
		return result;
	}

	private void checkDoneOrder(List<UnusualPkg> unusualPkgList)
	{
		if (unusualPkgList != null && unusualPkgList.size() > 0)
		{
			for (UnusualPkg unusualPkg : unusualPkgList)
			{
				String doneOrder = "N";

				int pacakgeId = unusualPkg.getConnect_package_id();
				if (pacakgeId != 0)
				{
					Pkg pkg = packageService.queryPackageById(pacakgeId);
					if (pkg != null)
					{
						// 根据address_id查询包裹地址
						if (StringUtils.trimToNull("" + pkg.getAddress_id()) != null)
						{
							List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());
							FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(pkg.getAddress_id());
							// 客户在编辑包裹时没有勾选需等待合箱，且包裹内件数大于等于1，且包裹有选择收货地址，点击完成，那么包裹从前台页面的“待处理包裹”下消失
							if (frontUserAddress != null && gooldList.size() > 0)
							{
								doneOrder = "Y";
							}
						}
					}
				}
				unusualPkg.setDoneOrder(doneOrder);
			}
		}
	}
}
