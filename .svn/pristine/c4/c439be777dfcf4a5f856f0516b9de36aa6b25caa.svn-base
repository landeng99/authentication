package com.xiangrui.lmp.business.api.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.freightcost.vo.MemberRate;

public interface BaseApiMapper
{
	/**
	 * 通过积分下限查询用户的等级
	 * 
	 * @param params
	 * @return
	 */
	List<MemberRate> getMemberRate(Map<String, Object> params);

	/**
	 * 获取添加包裹时增值服务列表接口
	 * 
	 * @return
	 */
	List<HashMap<String, Object>> getAllExtendValues(int rate_id);

	/**
	 * 获取用户指南列表接口
	 * 
	 * @return
	 */
	List<HashMap<String, Object>> getUserDocument();

	/**
	 * 根据国家获取海外地址仓库ID和名字
	 * 
	 * @param country
	 * @return
	 */
	List<HashMap<String, Object>> getOverseasByCountry(String country);

	/**
	 * 根据地区ID获取地区名字
	 * 
	 * @param id
	 * @return
	 */
	String getAreaName(String id);

	/**
	 * 获取所有国家海外地址
	 * 
	 * @return
	 */
	List<HashMap<String, Object>> getAllCountryOverseas(int user_id);

	/**
	 * 获取海外仓库明细
	 * 
	 * @param id
	 * @return
	 */
	Map<String, Object> getOverseasDeatil(int id);

	/**
	 * 获取所有城市列表
	 * 
	 * @return
	 */
	List<Map<String, Object>> getAllCitys();

	/**
	 * 获取所有省份列表
	 * 
	 * @return
	 */
	List<Map<String, Object>> getAllProvince();

	/**
	 * 获取所有区县列表
	 * 
	 * @return
	 */
	List<Map<String, Object>> getAllArea();

	/**
	 * 获取所有通知
	 * 
	 * @return
	 */
	List<Map<String, Object>> getAllNotice();

	/**
	 * 根据notice_id通知详情
	 * 
	 * @param notice_id
	 * @return
	 */
	Map<String, Object> getNoticeDeatil(int notice_id);
}
