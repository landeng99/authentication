package com.xiangrui.lmp.util.newExcel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * excel表的分解读取
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-6-6 上午9:40:35
 * </p>
 */
public class MapTable
{

    private Map<String, MapTableSingle> mapTableSingles;
    private Map<String, List<List<String>>> mapTable;
    /**
     * 设置表头的数据库列名
     */
    private List<List<String>> mapTableRowNameParmas;
    /**
     * 设置表头的数据库表名
     */
    private List<String> mapTableNameParmas;
    /**
     * 设置表头位置
     */
    private int[] mapTableRowIndex;

    public Map<String, MapTableSingle> getMapTableSingles()
    {
        return mapTableSingles;
    }

    public void setMapTableSingles(Map<String, MapTableSingle> mapTableSingles)
    {
        this.mapTableSingles = mapTableSingles;
    }

    /**
     * 获取map,各个表的表头数据库列名
     * 
     * @return
     */
    public List<List<String>> getMapTableRowNameParmas()
    {
        return mapTableRowNameParmas;
    }

    /**
     * 设置map,各个表头的数据库列名
     * 
     * @param mapTableRowNameParmas
     */
    public void setMapTableRowNameParmas(
            List<List<String>> mapTableRowNameParmas)
    {
        this.mapTableRowNameParmas = mapTableRowNameParmas;
    }

    /**
     * 设置map,各个表头的数据库表名
     * 
     * @return
     */
    public List<String> getMapTableNameParmas()
    {
        return mapTableNameParmas;
    }

    /**
     * 设置map,各个表头的数据表名
     * 
     * @param mapTableNameParmas
     */
    public void setMapTableNameParmas(List<String> mapTableNameParmas)
    {
        this.mapTableNameParmas = mapTableNameParmas;
    }

    /**
     * 获取map,各个表头的位置,第一行为0
     * 
     * @return
     */
    public int[] getMapTableRowIndex()
    {
        return mapTableRowIndex;
    }

    /**
     * 设置map,各个表表头位置,第一行为0
     * 
     * @param mapTableRowIndex
     */
    public void setMapTableRowIndex(int[] mapTableRowIndex)
    {
        this.mapTableRowIndex = mapTableRowIndex;
    }

    /**
     * 获取map,来源于excel表的读取
     * 
     * @param mapTable
     */
    public void setMapTable(Map<String, List<List<String>>> mapTable)
    {
        this.mapTable = mapTable;
    }

    /**
     * 获取map.来源于excel表的读取
     * 
     * @return
     */
    public Map<String, List<List<String>>> getMapTable()
    {
        return mapTable;
    }

    /**
     * 获取maptable的所有sheet表名
     * 
     * @return 为空时,没有maptable数据
     */
    public Set<String> getMapTableNames()
    {
        if (this.mapTable == null)
            return null;
        return this.mapTable.keySet();
    }

    /**
     * 获取maptable各个表的行数数组
     * 
     * @return 为空时,没有maptable数据
     */
    public int[] getMapTableColNums()
    {
        if (this.mapTable == null)
            return null;
        else
        {
            int[] temp = new int[getMapTableNames().size()];
            int i = 0;
            for (String key : this.mapTable.keySet())
            {
                List<List<String>> tempList = this.mapTable.get(key);
                if(tempList.size() > 0){
                    temp[i] = tempList.get(0).size();
                }else{
                    temp[i] = 0;
                }
                i++;
            }
            return temp;
        }
    }

    /**
     * 获取maptable各个表的列数数组
     * 
     * @return 为空时,没有maptable数据
     */
    public int[] getMapTableRowNums()
    {
        if (this.mapTable == null)
            return null;
        else
        {
            int[] temp = new int[getMapTableNames().size()];
            int i = 0;
            for (String key : getMapTableNames())
            {
                List<List<String>> tempList = this.mapTable.get(key);
                temp[i] = tempList.size();
            }
            i++;
            return temp;
        }
    }

    /**
     * 获取map,各个表的表头
     * 
     * @return
     */
    public List<List<String>> getMapTableRowNames()
    {
        if (this.mapTable == null)
            return null;
        else
        {
            List<List<String>> setTemp = new ArrayList<List<String>>();
            int i = 0;
            for (String key : getMapTableNames())
            {
                List<String> tempList = this.mapTable.get(key).get(
                        getMapTableRowIndex()[i++]);
                setTemp.add(tempList);
            }
            return setTemp;
        }
    }

    public MapTable()
    {
        super();
    }

    /**
     * 设置mapTableSingle,通过List<List<String>>转换
     * 
     * @param mapTable
     */
    @SuppressWarnings("unused")
    public void createMapTableInMapTableSingles(
            Map<String, List<List<String>>> mapTableList)
    {
        Map<String, MapTableSingle> mapTableSingleMap = new HashMap<String, MapTableSingle>();
        int i = 0;
        int[] mapTableColNums = getMapTableColNums();
        int[] mapTableRowNums = getMapTableRowNums();
        int[] mapTableRowIndex = getMapTableRowIndex();
        
        // 各个表的表头
        List<List<String>> mapTableRowNames = getMapTableRowNames();
        
        // 各个表的表头数据库列名
        List<List<String>> mapTableRowNameParmas = getMapTableRowNameParmas();
        for (String key : mapTableList.keySet())
        {
            MapTableSingle mapTableSingle = new MapTableSingle();
            mapTableSingle.setName(key);
            mapTableSingle.setColNum(mapTableColNums[i]);
            mapTableSingle.setRowMum(mapTableRowNums[i] + 1);
            mapTableSingle.setRowCount(mapTableRowNums[i] - 
                    mapTableRowIndex[i]);
            mapTableSingle.setRowIndex(mapTableRowIndex[i]);
            mapTableSingle.setTable(mapTableList.get(key));
            mapTableSingle.setColNames(mapTableRowNames.get(i));
            i++;
            mapTableSingleMap.put(key, mapTableSingle);
        }
        this.mapTableSingles = mapTableSingleMap;
        this.mapTable.clear();
    }
}
