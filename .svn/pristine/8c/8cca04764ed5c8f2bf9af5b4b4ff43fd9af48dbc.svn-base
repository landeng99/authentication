package com.xiangrui.lmp.util;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import com.xiangrui.lmp.util.content.SysContent;

/**
 * 用户操作工具附件
 * <p>
 * @author hsjing
 * </p>
 * <p>
 * 2015-5-25 下午6:41:43
 * </p>
 */
public class UserLogAspectUtil
{

    /**
     * 获取ip地址
     * 
     * @return
     */
    public static String optionIpValues()
    {
        HttpServletRequest request = SysContent.getRequest();
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
            ip = request.getHeader("Proxy-Client-IP");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
            ip = request.getHeader("WL-Proxy-Client-IP");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
            ip = request.getRemoteAddr();
        return ip;
    }

    /**
     * 反射解析对象的get方法中的值,组成字符串
     * 
     * @param info
     * @param methods
     * @return
     */
    public static String getDeclaredMethods(Object info, Method[] methods)
    {
        StringBuffer rs = new StringBuffer("");
        // 遍历方法，判断get方法
        for (Method method : methods)
        {
            String methodName = method.getName();
            // 判断是不是get方法
            if (methodName.indexOf("get") == -1) // 不是get方法
                continue;// 不处理
            Object rsValue = null;
            try
            {
                // 调用get方法，获取返回值
                rsValue = method.invoke(info);
                if (rsValue == null) // 没有返回值
                    continue;
            } catch (Exception e)
            {
                continue;
            }
            // 将值加入内容中
            rs.append("(" + methodName.substring(3) + ":" + rsValue + ")");
        }
        return rs.toString();
    }

    /**
     * 获取操作内容
     * 
     * @param args
     * @param mName
     * @return
     */
    public static String optionContentDesc(Object[] args, String mName)
    {
        if (args == null)
            return null;
        StringBuffer rs = new StringBuffer();
        rs.append(mName);
        String className = null;
        // 遍历参数对象
        for (Object info : args)
        {
            // 获取对象类型
            className = info.getClass().getName();
            className = className.substring(className.lastIndexOf(".") + 1);
            rs.append("[");
            // 父类方法
            Method[] superMethods = info.getClass().getSuperclass()
                    .getDeclaredMethods();
            rs.append(getDeclaredMethods(info, superMethods));
            // 获取对象的所有方法
            Method[] methods = info.getClass().getDeclaredMethods();
            rs.append(getDeclaredMethods(info, methods));
            rs.append("]");
        }
        return rs.toString();
    }
}
