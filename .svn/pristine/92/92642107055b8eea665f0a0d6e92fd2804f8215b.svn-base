package com.xiangrui.lmp.business.homepage.mapper;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.homepage.vo.Pkg;

public interface FrontPkgMapper {

	/**
     * 根据package_id
     * @param package_id
     * @return List
     */
	Pkg queryPackageByPackageId(int package_id);
	/**
	 * 删除包裹
	 * @param package_id
	 */
	void deletePkgByPackageId(int package_id);
	/**
	 * 删除包裹里的所有商品
	 * @param package_id
	 */
    void deletePkgInGoodsByPackageId(int package_id);
    /**
     * 删除包裹的增值服务
     * @param package_id
     */
    void deletePkgAttachByPackageId(int package_id);
    /**
	 * 根据user_id
	 * 查询所有
	 * @param user_id
	 * @return List
	 */
	List<Pkg> queryPackageByUserId(int user_id);

    /**
     * 根据logistics_code单号查询包裹
     * @param val
     * @return
     */
    List<Pkg> queryPackageByLogisticsCode(String val);
    
    /**
     * 根据关联单号查询包裹
     * @param original_num
     * @return
     */
    List<Pkg> queryPackageByOriginalNum(String original_num);

    /**
     * 根据参数查询包裹信息
     * @param params
     * @return
     */
    List<Pkg> queryPkgByParam(Map<String,Object> params);
   
    /**
     * 根据user_id和包裹状态查询包裹
     * @param pkg
     * @return list
     */    
    List<Pkg> queryPackageByUserIdAndStatus(Map<String,Object> params);
    
    /**
     * 根据user_id和包裹状态查询非异常状态包裹
     * @param pkg
     * @return list
     */    
    List<Pkg> queryPackageByUserIdAndStatusNormal(Map<String,Object> params);

    /**
     * 根据user_id和包裹支付状态查询包裹
     * @param pkg
     * @return list
     */    
    List<Pkg> queryPackageByUserIdAndPayStatus(Map<String,Object> params);
    List<Pkg> queryPackageByUserIdAndPayStatusCustom(Map<String,Object> params); // 只查关税状态
    List<Pkg> queryPackageByUserIdAndPayStatusFreight(Map<String,Object> params);	// 只查运费状态
    List<Pkg> queryPackageByUserIdAndPaid(Map<String,Object> params); // 查已关税与运费都已经支付的状态
    List<Pkg> queryPackageByUserIdAndUnPaid(Map<String,Object> params); // 查已关税或运费至少有一项未支付的状态
    
    /**
     * 根据user_id和包裹打印状态查询包裹
     * @param pkg
     * @return list
     */ 
    List<Pkg> queryPackageByUserIdAndPkgPrint(Map<String,Object> params);
    
    /**
     * 包裹支付状态更新
     * @param pkgList
     * @return 
     */    
    void updatePackageForPayment(List<Pkg> pkgList);
    
    /**
     * 更新运费新状态
     * @param pkgList
     *2017年9月4日
     */
    void updatePackageForPaymentFreight(List<Pkg> pkgList);
    
    /**
     * 更新收关税状态
     * @param pkgList
     *2017年9月4日
     */
    void updatePackageForPaymentCustom(List<Pkg> pkgList);
    
    /**
     * 包裹收货地址更新
     * @param pkg
     * @return 
     */ 
    void updatePkgForUseraddress(Pkg pkg);
    
    /**
     * 更新包裹状态根据关联单号
     * @param  status original_num
     */
    void updatePkgStatusByOriginalNum(Pkg pkg);
    
    /**
     * 根据运单号，关联单号，收件人，手机号码，商品名称查询包裹
     * @param params
     * @return 
     */
    List<Pkg> queryPkgBySearch(Map<String,Object> params);
    
    /**
     * 根据运单号查询包裹(用户查询 )
     * @param params
     * @return 
     */
    Pkg queryPkgBylogistics(Map<String,Object> params);
    
    /**
     * 根据包裹关联单号查询包裹
     * @param params
     * @return 
     */
    Pkg queryPkgByOriginalNum(Map<String,Object> params);
    
    /**
     * 根据包裹关联单号和异常状态查询包裹
     * @param params
     * @return 
     */
    Pkg queryPkgByOriginalNumByStatus(Map<String,Object> params);
    
    
    /**
     * <!--包裹ID（多）查询包裹-->
     * @param list
     * @return List
     */
    List<Pkg> queryPackageByPackageIds(List <String> list);
    
    /**
     * <!--公司单号（多）查询包裹-->
     * @param list
     * @return List
     */
    List<Pkg> queryPackageBylogisticsCodes(List <String> list);
    
    /**
     * 根据user_id和包裹状态查询到库包裹
     * @param params<enable>
     * @param params<user_id>
     * @param params<status>
     * @return
     */
    List<Pkg> queryArrvivedPackage(Map<String,Object> params);
    
    /**
     * 根据user_id和包裹状态查询前台待处理包裹（后台异常包裹关联后的显示）
     * @param user_id,status
     * @return list
     */    
    List<Pkg> queryNeedToHandlePackage(Map<String,Object> params);
    
    /**
     * 根据运单号、包裹创建时间范围、打印状态查询包裹
     * @param params
     * @return 
     */
    List<Pkg> queryPkgBylogisticsAndPackageCreateTimeRangeAndPrintStatus(Map<String,Object> params);
    /**
     * 根据用户ID,包裹新建时间段查询导出包裹
     * @param params
     * @return
     */
    List<Pkg> queryFrontEndExportPackage(Map<String,Object> params);
    /**
     * 根据包裹ID列表查询导出包裹（主要是给合、分箱用）
     * @param pacakgeIds
     * @return
     */
    List<Pkg> queryFrontEndExportPackageByPackageIdList(List<String> pacakgeIds);
    
    
    /**
     * 根据单号查询包裹
     * @param pkg
     * @return 
     */ 
    void updatePkgForOriginalNum(Pkg pkg);
    
    /**
     * 根据pacakge_id查询带物流单号的包裹 
     * @param package_id
     * @return
     */
    Pkg queryExpressPackageByPackageId(Integer package_id);
    
    /**
     * 根据express_num查询带物流单号的包裹
     * @param package_id
     * @return
     */
    List<Pkg> queryExpressPackageByExpressNum(Map<String,Object> params);
    
    Integer queryCountByUserId(Integer userId); 
    List<Pkg>  queryMypagesByqueryPames(Map<String, Object> paramsCount);
    Integer queryMypagesCountByqueryPames(Map<String, Object> paramsCount) ;
 }
