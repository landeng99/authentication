package com.xiangrui.lmp.business.admin.attachService.service;

import java.util.List;
import java.util.Map;

import com.xiangrui.lmp.business.admin.attachService.vo.MemberAttach;
import com.xiangrui.lmp.business.admin.attachService.vo.MemberRateAttach;

/**
 * 增值与会员关联的价格
 * <p>@author <b>hsjing</b></p>
 * <p>2015-6-19 上午11:35:37</p>
 */
public interface MemberAttachService
{
    List<MemberRateAttach> findRate();
    /**
     * 新增价格
     * @param memberAttach
     */
    void saveMemberAttach(MemberAttach memberAttach);
    /**
     * 通过增值id删除所有关联的价格信息
     * @param attachId 增值di
     */
    void deleteByAttachId(int attachId);
    /**
     * 通过会员id删除所有关联的价格信息
     * @param rateId 会员id
     */
    void deleteByRateId(int rateId);
    /**
     * 删除具体某条价格细心
     * @param attachId 增值id
     * @param rateId 会员id
     */
    void deleteById(int attachId,int rateId);
    /**
     * 更新某个增值服务的所有价格
     * @param price 修改值
     * @param attachId 增值id条件
     */
    void updateMemberAttachByAttachId(float price,int attachId);
    /**
     * 更新某个会员的所有价格
     * @param price 修改值
     * @param rateId 会员id
     */
    void updateMemberAttachByRateId(float price,int rateId);
    /**
     * 更新某个具体价格的值
     * @param price 修改值
     * @param attachId 增值id
     * @param rateId 会员id
     */
    void updateMemberAttach(float price,int attachId,int rateId);
    /**
     * 查询价格关联会员
     * @param map
     * RATE_ID 会员id,ATTACH_ID 增值id,RATE_NAME 会员名称
     * @return
     */
    List<MemberAttach> queryRate(Map<String, Object> map);
    /**
     * 查询价格关联增值服务
     * @param map
     * @return
     */
    List<MemberAttach> queryAttach(Map<String, Object> map);
    /**
     * 查询价格关联会员和增值服务
     * @param map 
     * @param map RATE_ID 会员id,ATTACH_ID 增值id,RATE_NAME 会员名称,
     *  SERVICE_NAME 增值服务名,ISDELETED 增值服务删除状态
     * @return
     */
    List<MemberAttach> queryAll(Map<String, Object> map);
    /**
     * 单独查询价格信息,无关联
     * @param map
     * @return
     */
    List<MemberAttach> queryMemberAttach(Map<String, Object> map);
    /**
     * 查询某种会员的所有价格,关联会员
     * @param rateId
     * @return
     */
    List<MemberAttach> queryByRateId(int rateId);
    /**
     * 查询某个增值服务的素颜价格,关联增值服务
     * @param attachId
     * @return
     */
    List<MemberAttach> queryByAttachId(int attachId);
    /**
     * 查询某个具体的价格
     * @param attachId
     * @param rateId
     * @return
     */
    MemberAttach queryById(int attachId,int rateId);
    /**
     * 查询某个具体的价格,并且关联会员和增值服务
     * @param attachId
     * @param rateId
     * @return
     */
    MemberAttach queryAllById(int attachId,int rateId);
    /**
     * 更新具体集合价格 
     * @param price 修改值
     * @param attachId 增值id
     * @param rateId 会员id
     */
    void updateMemberAttach(List<MemberAttach> list);
    /**
     * 更新某个会员集合价格 
     * @param price 修改值
     * @param attachId 增值id
     * @param rateId 会员id
     */
    void updateMemberAttachByRateId(List<MemberAttach> list);
}
