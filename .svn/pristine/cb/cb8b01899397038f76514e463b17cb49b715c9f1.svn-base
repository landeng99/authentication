package com.xiangrui.lmp.business.admin.pkglog.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.business.admin.pkglog.mapper.PkgLogMapper;
import com.xiangrui.lmp.business.admin.pkglog.service.PkgLogService;
import com.xiangrui.lmp.business.admin.pkglog.vo.PkgLog;

/**
 * 包裹状态记录逻辑实现
 * <p>
 * @author <b>hsjing</b>
 * </p>
 * <p>
 * 2015-7-2 下午2:44:16
 * </p>
 */
@Service("pkgLogService")
public class PkgLogServiceImpl implements PkgLogService
{

    @Autowired
    private PkgLogMapper pkgLogMapper;

    @Override
    public void pkgLogCreate(PkgLog pkgLog)
    {
        pkgLogMapper.pkgLogCreate(pkgLog);
    }

    @Override
    public List<PkgLog> queryPkgLogByPackageId(int package_id)
    {
        return pkgLogMapper.queryPkgLogByPackageId(package_id);
    }

    @Override
    public void pkgLogUpdate(PkgLog pkgLog)
    {
        pkgLogMapper.pkgLogUpdate(pkgLog);
    }

    @Override
    public PkgLog queryPkgLogByPackageIdAndStatus(int package_id,
            int operated_status)
    {
        PkgLog pkgLog = new PkgLog();
        pkgLog.setPackage_id(package_id);
        pkgLog.setOperated_status(operated_status);
        return pkgLogMapper.queryPkgLog(pkgLog);
    }
    
    
    @Override
    public void pkgLogBatchCreate(List<PkgLog> pkgLogList)
    {
        pkgLogMapper.pkgLogBatchCreate(pkgLogList);
    }

	/**
	 * 通过packageID和清关日期区间来查询包裹是否存在
	 * 
	 * @param packageId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public List<PkgLog> queryPkgLogByPackageId(Integer packageId,String fromDate, String toDate)
	{
		Map<String, Object> params = new HashMap<String, Object>();
		// 起始时间
		params.put("fromDate", fromDate);
		// 终止时间
		params.put("toDate", toDate);
		params.put("package_id", packageId);
		return pkgLogMapper.queryPkgLogByPackageIdAndDateRange(params);
	}
}
