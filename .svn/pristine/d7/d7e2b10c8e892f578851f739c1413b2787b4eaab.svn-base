<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.xiangrui.lmp.business.admin.pallet.mapper.PalletMapper">
	
	<sql id="select">
		select a.*,IFNULL(c.pkgCnt,0) as pkgCnt from t_pallet a
		 left join (select pallet_id, count(*) as pkgCnt from t_pallet_package group by pallet_id) c
    	on a.pallet_id=c.pallet_id
	</sql>
	<sql id="where">
		<where>
			1=1
		   <if test="pallet_id !=null and pallet_id != ''">
               AND <![CDATA[ a.pallet_id = #{pallet_id} ]]>
            </if>
             <if test="pick_id !=null and pick_id != ''">
               AND <![CDATA[ a.pick_id = #{pick_id} ]]>
            </if>
		</where>	
	</sql>
	
	<select id="query" parameterType="pallet" resultType="pallet">
			 <include refid="select"/>
			 <include refid="where"/>
	</select>
	
	<select id="queryAll" parameterType="pallet" resultType="pallet">
		select a.*,b.pick_code, IFNULL(c.pkgCnt,0) as pkgCnt from t_pallet a inner join t_pickorder b 
		on a.pick_id=b.pick_id 
     	left join (select pallet_id, count(*) as pkgCnt from t_pallet_package group by pallet_id) c
    	on a.pallet_id=c.pallet_id
		<where>
			1=1
		   <if test="pallet_id !=null and pallet_id != ''">
               AND <![CDATA[ a.pallet_id = #{pallet_id} ]]>
            </if>
             <if test="pick_id !=null and pick_id != ''">
               AND <![CDATA[ a.pick_id = #{pick_id} ]]>
            </if> 
		    <if test="pick_code !=null and pick_code != ''">
               AND <![CDATA[ b.pick_code = #{pick_code} ]]>
            </if>
		</where>
		order by create_time desc 
	</select>
	
 	 <select id="queryAllByMap" parameterType="java.util.Map" resultType="pallet">
				select a.*,b.pick_code, IFNULL(c.pkgCnt,0) as pkgCnt,p.warehouse from t_pallet a inner join t_pickorder b
				on a.pick_id=b.pick_id
		     	left join (select pallet_id, count(*) as pkgCnt from t_pallet_package group by pallet_id) c
		    	on a.pallet_id=c.pallet_id
		    	left join t_overseas_address p
		    	 on b.overseas_address_id=p.id
		<where>
			1=1
		     AND  b.overseas_address_id in  
             <foreach item="item" index="index" collection="userOverseasAddressList"   open="(" separator="," close=")">   
                   #{item.id}
              </foreach>
		   <if test="pallet_code !=null and pallet_code != ''">
               AND <![CDATA[ a.pallet_code = #{pallet_code} ]]>
            </if>
             <if test="status !=null and status != ''">
               AND <![CDATA[ a.`status` = #{status} ]]>
            </if> 
		    <if test="pick_code !=null and pick_code != ''">
               AND <![CDATA[ b.pick_code = #{pick_code} ]]>
            </if>
            <if test="fromDate !=null">
            AND	<![CDATA[a.create_time>=#{fromDate}]]>
            </if>
            <if test="toDate!=null">
            AND	<![CDATA[a.create_time<=#{toDate}]]>
            </if>
              <if test="logistics_code!=null and logistics_code!=''">
        	    AND	<![CDATA[  
        	             exists(select 1 from t_pallet_package d 
						 inner join  t_package e
						on d.package_id=e.package_id
						 where e.logistics_code=#{logistics_code} and d.pallet_id=a.pallet_id) 
			 ]]>
            </if>
              <if test="overseas_address_id !=null and overseas_address_id != ''">
               AND    <![CDATA[ b.overseas_address_id = #{overseas_address_id} ]]>
            </if>
		</where>	
	</select> 
	

	<insert id="insertPallet" parameterType="pallet">
			<![CDATA[
				insert into
				t_pallet(pallet_id,pallet_code,description,create_time,create_user,`status`,pick_id,declaration_time,output_id)
				values(null,#{pallet_code},#{description},#{create_time},#{create_user},#{status},#{pick_id},#{declaration_time},#{output_id})
			]]>
		 <selectKey keyProperty="pallet_id" resultType="java.lang.Integer" order="AFTER">  
			 SELECT LAST_INSERT_ID() AS pallet_id
    	</selectKey> 
	</insert>
	
	<select id="isPkgExist" parameterType="java.util.Map" resultType="java.lang.Integer">
			select count(*) from t_pallet_package where package_id=#{package_id} 
	</select>
	
	<insert id="insertPalletPkg" parameterType="java.util.Map">
		insert into t_pallet_package (pallet_id,package_id,create_time)values(#{pallet_id},#{package_id},now())
	</insert>
	
	<delete id="delPallet" parameterType="pallet">
		delete  from  t_pallet where pallet_id=#{pallet_id}
	</delete>

	<delete id="delPalletPkg" parameterType="pallet">
		delete from t_pallet_package where pallet_id=#{pallet_id}
	</delete>
	
	<delete id="delPalletByPickId" parameterType="java.lang.Integer">
		delete from t_pallet where pick_id=#{pick_id}
	</delete>
	
	<delete id="delPalletPkgByPickId" parameterType="java.lang.Integer">
		delete  from t_pallet_package  where  EXISTS(select 1 FROM t_pallet b where b.pick_id=#{pick_id} and  t_pallet_package.pallet_id=b.pallet_id) 
	</delete>
	
	<select id="queryPalletDetail"  parameterType="pallet" resultType="pallet">
		select a.*,b.pick_code ,b.seaport_id,b.overseas_address_id from t_pallet a
		inner join t_pickorder b 
		on a.pick_id=b.pick_id
		where pallet_id=#{pallet_id}
	</select>
	<update id="updatePallet"  parameterType="pallet">
		update t_pallet set description=#{description} where pallet_id=#{pallet_id}
	</update>
	
	<!-- 更新同一提单下所有托盘的物流状态 -->
	<update id="updatePalletStatusByPickId"  parameterType="java.util.Map">
        UPDATE t_pallet a
           SET a.status  = #{status}
         WHERE pick_id = #{pick_id}
    </update>
	<delete id="delPkg" parameterType="java.util.Map">
		delete from t_pallet_package where pallet_id=#{pallet_id} and package_id=#{package_id}
	</delete>
	
	<select id="queryPalletBySeaportIdAndOutputId"  parameterType="java.util.Map" resultType="pallet">
		select a.*,b.pick_code ,b.seaport_id,b.overseas_address_id from t_pallet a
		inner join t_pickorder b 
		on a.pick_id=b.pick_id
		where b.seaport_id=#{seaport_id} and b.output_id=#{output_id}
	</select>
</mapper>