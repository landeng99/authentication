package com.xiangrui.lmp.business.homepage.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.banner.service.BannerService;
import com.xiangrui.lmp.business.admin.freightcost.service.FreightCostService;
import com.xiangrui.lmp.business.admin.freightcost.vo.FreightCost;
import com.xiangrui.lmp.business.admin.friendlylink.service.FriendlyLinkService;
import com.xiangrui.lmp.business.admin.navigation.service.NavigationService;
import com.xiangrui.lmp.business.admin.navigation.vo.Navigation;
import com.xiangrui.lmp.business.admin.notice.service.NoticeService;
import com.xiangrui.lmp.business.admin.overseasAddress.service.OverseasAddressService;
import com.xiangrui.lmp.business.admin.overseasAddress.vo.OverseasAddress;
import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.admin.pkg.service.UserAddressService;
import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;
import com.xiangrui.lmp.business.admin.prohibition.service.ProhibitionService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.homepage.service.FrontUserAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.HomepageService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.business.admin.pkg.vo.PackPrint;

import com.xiangrui.lmp.servlet.AuthImg;
import com.xiangrui.lmp.util.JSONUtil;
import com.xiangrui.lmp.util.StringUtil;

/**
 * 翔锐物流首页
 * 
 * @author ltp
 * @date 2015年4月23日20:00:03
 */
@Controller
@RequestMapping(value = "/homepage")
public class HomepageController
{

    public HomepageController()
    {
        super();
    }

    /**
     * LOG.
     */
    private static final Logger logger = Logger
            .getLogger(HomepageController.class);

    /**
	 * 
	 */
    @Autowired
    private HomepageService homepageService;

    @Autowired
    private PkgGoodsService frontPkgGoodsService;

    @Autowired
    private FrontUserService ffrontUserService;

    @Autowired
    private FrontUserService memberService;
    
    @Autowired
    private PkgService frontPkgService;

    @Autowired
    private FrontUserAddressService frontUserAddressService;
    @Autowired
    private PackageService packageService;
    @Autowired
    private UserAddressService userAddressService;
    @Autowired
    private OverseasAddressService overseasAddressService;
    /**
     * 底部链接查询
     */
    @Autowired
    private FriendlyLinkService friendlyLinkService;

    /**
     * 禁运物品查询
     */
    @Autowired
    private ProhibitionService prohibitionService;

    /**
     * 导航栏查询
     */
    @Autowired
    private NavigationService navigationService;
    /**
     * 公告信息查询
     */
    @Autowired
    private NoticeService noticeService;

    /**
     * 轮播图信息查询
     */
    @Autowired
    private BannerService bannerService;
    /**
     * 主页查询重量的类型
     */
    private static final String QUERY_TYPE_WEIGHT = "#weight";
    /**
     * 主页查询包裹单号的类型
     */
    private static final String QUERY_TYPE_COST = "#cost";
    /**
     * 插叙重量用的单位
     */
    private static final String[] QUERY_WEIGHT_UNIT = { "kg", "g", "千克", "克",
            "t", "吨", "mg", "毫克", "ug" };
    /**
     * 会员价格信息表
     */
    @Autowired
    private FreightCostService freightCostService;

    /**
     * . 跳转到首页
     * 
     * @param modelMap
     *            ModelMap
     * @param httpRequest
     *            HttpServletRequest
     * @param httpSession
     *            HttpSession
     * @return String String
     */
    @RequestMapping(value = "/index")
    public String getHomepagelList(ModelMap modelMap, HttpServletRequest req,
            HttpSession httpSession)
    {
        // String account=req.getParameter("account");
        // 头部信息
        setHeadRequestAttribute(req, "/homepage/index");

        // 主页轮播图信息
        req.setAttribute("bannerLists", bannerService.queryALlStatus(1));

        // 底部链接数据
        setFooterRequestAttribute(req);

        return "/homepage/index";
    }

    /**
     * 查询重量,或者包裹
     * 
     * @return
     */
    @RequestMapping(value = "queryType", produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String queryType(String type, String val)
    {
        if (QUERY_TYPE_WEIGHT.equals(type))
        {
            // val参数是否的单位格式
            boolean isValPrice = false;

            // 默认重量单位为kg
            String unitDefault = "kg";

            // 当type为重量查询时, val的值为,"输入数字_输入内容"
            String[] valSplit = val.split("_");

            // 输入数字 == 输入内容
            if (valSplit[1].equals(valSplit[0]))
            {
                isValPrice = true;
            } else
            {
                // 遍历重量单位对比输入内容中的单位
                for (String unit : QUERY_WEIGHT_UNIT)
                {
                    // 输入内容 - 输入数字 = 单位
                    if (valSplit[1].substring(valSplit[0].length())
                            .equals(unit))
                    {
                        isValPrice = true;
                        unitDefault = unit;
                        break;
                    }
                }
            }

            if (isValPrice)
            {
                float unitVal = Float.parseFloat(valSplit[0]);
                StringBuffer sb = new StringBuffer("[");
                // 记录表头的json
                sb.append("{type:'weight',value:'" + valSplit[0] + unitDefault
                        + "',");

                sb.append(getWeightLevel(freightCostService.queryAll(null), unitVal));

                sb.append("},");

                sb.append("]");

                return sb.toString();
            } else
            {
                return "[type:'error',value:'对  " + valSplit + "  的输入格式无法判断!']";
            }
        } else
        {
            List<Pkg> pkgs = frontPkgService.queryPackageByLogisticsCode(val);
            if (null != pkgs && pkgs.size() > 0)
            {
                Pkg pkg = pkgs.get(0);

                StringBuffer sb = new StringBuffer("[");
                sb.append("{");

                // 记录表头
                sb.append("type:'code',value:'" + pkg.getLogistics_code()
                        + "',");

                // 记录状态
                sb.append("status:{");
                sb.append("text:'" + getPkgStatus(pkg.getStatus())
                        + "',value:'" + pkg.getStatus() + "',");
                sb.append("},");

                // 记录状态列表
                List<String> levelLists = getPkgLevel(pkg.getStatus());
                sb.append("level:[");
                for (String levelVal : levelLists)
                {
                    String title = levelVal.split(",")[0];
                    String price = levelVal.split(",")[1];
                    sb.append("{");
                    sb.append("title:'" + title + "',");
                    sb.append("price:'" + price + "',");
                    sb.append("},");
                }
                sb.append("],");

                sb.append("},");

                sb.append("]");

                return sb.toString();
            } else
            {
                return "[type:'error',value:'无包裹信息']";
            }
        }
    }

    /**
     * . 跳转到禁运物品
     * 
     * @param httpRequest
     *            HttpServletRequest
     * @return String String
     */
    @RequestMapping(value = "/prohibition")
    public String getProhibition(HttpServletRequest httpRequest)
    {
        // 头部信息
        setHeadRequestAttribute(httpRequest, "/homepage/prohibition");
        // 头部登录信息

        // 禁运物品数据
        httpRequest.setAttribute("prohibitionLists",
                prohibitionService.queryAll(null));

        // 底部链接数据
        setFooterRequestAttribute(httpRequest);

        return "/homepage/prohibition";
    }

    /**
     * . 跳转到公告信息
     * 
     * @param httpRequest
     *            HttpServletRequest
     * @return String String
     */
    @RequestMapping(value = "/noticeInfo")
    public String getNoticeInfo(HttpServletRequest httpRequest, int noticeId)
    {
        // 头部信息
        setHeadRequestAttribute(httpRequest, "/homepage/notice");

        // 公告信息
        httpRequest.setAttribute("noticePojo",
                noticeService.queryAllId(noticeId));

        // 底部数据
        setFooterRequestAttribute(httpRequest);

        return "/homepage/notice";
    }

    /**
     * . 跳转到我的包裹
     * 
     * @param httpRequest
     *            req
     * @return String String
     */
    @RequestMapping(value = "/transport")
    public String transport(HttpServletRequest httpRequest)
    {

        // 没有登录用户
        if (null == httpRequest.getSession().getAttribute("frontUser"))
        {
            return "/homepage/login";
        }

        // 头部信息
        setHeadRequestAttribute(httpRequest, "/homepage/transport");
        // 低部信息
        setFooterRequestAttribute(httpRequest);

        HttpSession session = httpRequest.getSession();
        FrontUser frontUser = (FrontUser) session.getAttribute("frontUser");
        // 查询包裹信息 
        List<Pkg> pkgList = frontPkgService.queryPackageByUserId(frontUser
                .getUser_id());
        for (Pkg pkg : pkgList)
        {
            List<PkgGoods> gooldList = frontPkgGoodsService
                    .queryPkgGoodsById(pkg.getPackage_id());
            pkg.setGoods(gooldList);
        }
        httpRequest.setAttribute("pkgList", pkgList);

        return "/homepage/transport";
    }

    
    /**
     * . 跳转到帮助中心
     * 
     * @param httpRequest
     *            HttpServletRequest
     * @return String String
     */
    @RequestMapping(value = "/help")
    public String getHelp(HttpServletRequest httpRequest)
    {
        // 头部信息
        setHeadRequestAttribute(httpRequest, "/homepage/help");

        // 底部数据
        setFooterRequestAttribute(httpRequest);

        return "/homepage/help";
    }

    /**
     * . 跳转到登录
     * 
     * @param httpRequest
     *            HttpServletRequest
     * @return String String
     */
    @RequestMapping(value = "/login")
    public String getLogin(HttpServletRequest httpRequest)
    {

        return "/homepage/login";
    }

    /**
     * . 前台登录验证
     * 
     * @param httpRequest
     *            HttpServletRequest
     * @return String String
     */
    @RequestMapping(value = "/flogin")
    public String flogin(FrontUser frontUser, HttpServletRequest req,
            HttpServletResponse resp, String txtCode)
    {
        String count = req.getParameter("count");
        int ct;
        if (null != count && !"".equals(count))
        {
            int cts = Integer.parseInt(count);
            ct = cts + 1;
        } else
        {
            ct = 1;
        }
        // 用户登录信息校验失败，则返回重新登录
        if (!validateUser(frontUser, req))
        {
            req.setAttribute("count", ct);
            return "homepage/login";
        }

        // 查询用户信息
        FrontUser f = memberService.queryFrontUserByEmail(frontUser.getEmail());
        System.out.println(frontUser.getEmail());

        HttpSession session = req.getSession();

        session.setAttribute("frontUser", f);

        return "redirect:/homepage/transport";
    }

    /**
     * . 用户退出
     * 
     * @param req
     * @return
     */
    @RequestMapping(value = "/exit")
    public String exit(HttpServletRequest req)
    {
        HttpSession session = req.getSession();
        session.removeAttribute("frontUser");
        return "redirect:/index";
    }

    /**
     * . 跳转到注册
     * 
     * @param httpRequest
     *            HttpServletRequest
     * @return String String
     */
//    @RequestMapping(value = "/registe")
//    public String getRegiste(HttpServletRequest httpRequest)
//    {
//
//        return "/homepage/registe";
//    }

    /**
     * . 用户注册
     * 
     * @param httpRequest
     *            HttpServletRequest
     * @return String String
     */
//    @RequestMapping(value = "/fregiste")
//    public String fregiste(FrontUser frontUser, HttpServletRequest req)
//    {
//        if (!validateRegUser(frontUser, req))
//        {
//            return "homepage/registe";
//        }
//     
//        memberService.addFrontUser(frontUser);
//        return "/homepage/login";
//    }

    /**
     * . 发送验证码
     * 
     * @param httpRequest
     *            HttpServletRequest
     * @return String String
     */
    @RequestMapping(value = "/sendCode")
    public void sendCode(HttpServletRequest httpRequest, String mobile)
    {

    }

    
    
    /**
     *  打印面单
     * 
     * @param request
     * @param package_id
     * @return String
     */
    @RequestMapping("/print")
    public String print(HttpServletRequest request, String package_id)
    {

        // 包裹信息
        com.xiangrui.lmp.business.admin.pkg.vo.Pkg pkgDetail = packageService
                .queryPackageById(Integer.parseInt(package_id));

    /*    FrontUser frontUser = memberService.queryFrontUserByUserId(pkgDetail
                .getUser_id());*/

        // 包裹地址
        UserAddress userAddress = userAddressService.queryAddressById(pkgDetail
                .getAddress_id());
        // 地址拼接
        String address = addressInfo(userAddress.getRegion());

        // 海外仓库地址:寄件地址
   /*     OverseasAddress overseasAddress = overseasAddressService
                .queryOverseasAddressById(pkgDetail.getOsaddr_id());*/
        // 海外地址拼接
/*        String osaddr = joinOverseasAddress(overseasAddress);
*/        
        pkgDetail.setPkg_print(BasePackage.PACKAGE__YES_PRINT);
        packageService.update(pkgDetail, null);
        //获取商品信息
		List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(Integer.parseInt(package_id));
        String goodsname = "";
        int goodcount = 0;
		if(gooldList.size()>0){
	         goodsname = gooldList.get(0).getGoods_name();
	         goodcount = gooldList.get(0).getQuantity();
		}
        // 面单信息
        request.setAttribute("goodsname", goodsname);
        request.setAttribute("goodcount", goodcount);

   /*     request.setAttribute("frontUser", frontUser);
        request.setAttribute("country", overseasAddress.getCountry());
        request.setAttribute("osaddr", osaddr);*/
        request.setAttribute("pkgDetail", pkgDetail);
        request.setAttribute("userAddress", userAddress);
        request.setAttribute("address", address + userAddress.getStreet());

        return "homepage/printExpress";
    }
    /**
     * PkgManyPrintmore 批量打印
     */
	 /**
     *  打印面单
     * 
     * @param request
     * @param package_id
     * @return String
     */
    @RequestMapping("/PkgManyPrintmore")
    public String PkgManyPrintmore(HttpServletRequest request )
    { 
     	List<PackPrint> listPackPrint = new ArrayList<PackPrint>();
    	String package_ids = request.getParameter("package_ids");
    	String logistics_codes ="";
    	String[] packArr = null;
    	if(package_ids!= null){
    	      packArr =  package_ids.split(","); 
    	}
    	for(int i = 0 ; i < packArr.length;i++){
    		PackPrint packPrint = new PackPrint();
        	String  package_id = packArr[i];
            // 包裹信息
            com.xiangrui.lmp.business.admin.pkg.vo.Pkg pkgDetail = packageService
                    .queryPackageById(Integer.parseInt(package_id)); 
            // 包裹地址
            UserAddress userAddress = userAddressService.queryAddressById(pkgDetail
                    .getAddress_id());
            // 地址拼接
            String address = addressInfo(userAddress.getRegion()); 
            pkgDetail.setPkg_print(BasePackage.PACKAGE__YES_PRINT);
            packageService.update(pkgDetail, null);
            //获取商品信息
    		List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(Integer.parseInt(package_id));
            String goodsname = "";
            int goodcount = 0;
    		if(gooldList.size()>0){
    	         goodsname = gooldList.get(0).getGoods_name();
    	         goodcount = gooldList.get(0).getQuantity();
    		}
    		 // 面单信息
    		packPrint.setAddress(address + userAddress.getStreet());
    		packPrint.setUserAddress(userAddress);
    		packPrint.setGoodsname(goodsname);
    		packPrint.setGoodscount(goodcount); 
    		packPrint.setPkgDetail(pkgDetail); 
    		packPrint.setPageorder(i+1);
            listPackPrint.add(packPrint);
             if(i>0){
            	logistics_codes+=",";
            }
            logistics_codes+= pkgDetail.getLogistics_code();
            
    	} 
    	int listPackPrintcount = listPackPrint.size();
        request.setAttribute("listPackPrint", listPackPrint);
        request.setAttribute("listPackPrintcount", listPackPrintcount);
        //物流单号列表
        request.setAttribute("logistics_codes", logistics_codes);
        
        return "homepage/printExpress_more";
    }

    /**
     * 
     * 校验登录信息
     * 
     * @param user
     * @return
     */
    private boolean validateUser(FrontUser frontUser, HttpServletRequest req)
    {
        String imgVCValues = (String) req.getSession().getAttribute(
                "AUTHIMG_CODE");
        String txtCode = req.getParameter("txtCode");
        System.out.println(imgVCValues);
        System.out.println(txtCode);
//        if (null != imgVCValues && !imgVCValues.equals(txtCode)
//                && !txtCode.equals("请输入验证码"))
        if(!AuthImg.checkAuthingCode(imgVCValues, txtCode))
        {
            req.setAttribute("CodeErr", "验证码错误");
            return false;
        }
        FrontUser ff = memberService.frontUserLogin(frontUser);
        if (null == ff)
        {
            req.setAttribute("fail", "登录失败！用户名或密码错误！");
            return false;
        }
        return true;

    }

    /**
     * 
     * 校验注册信息
     * 
     * @param user
     * @return
     */
    private boolean validateRegUser(FrontUser frontUser, HttpServletRequest req)
    {
        HttpSession session = req.getSession();
        String smsCode = (String) session.getAttribute("smsCode");
        System.out.println(smsCode);
        if (null != smsCode && !smsCode.equals(frontUser.getSms_code()))
        {
            req.setAttribute("smsCodeErr", "验证码错误！");
            return false;
        }
        FrontUser ff = memberService
                .queryFrontUserByEmail(frontUser.getEmail());
        if (null != ff)
        {
            req.setAttribute("failReg", "注册失败，该用户已存在！");
            return false;
        }
        return true;
    }

    /**
     * 主页查询包裹编号,状态轨迹list字符串
     * 
     * @param status
     *            查询包裹的状态值
     * @return list的格式是 状态名称,真假值(0/1)
     */
    private List<String> getPkgLevel(int status)
    {
        List<String> levelLists = new ArrayList<String>();
        if (status == 0 || status == 1)
        {
            levelLists.add("等待入库,1");
            levelLists.add("出库,0");
            levelLists.add("空运中,0");
            levelLists.add("清关,0");
            levelLists.add("收货,0");
        } else if (status == 2 || status == 3)
        {
            levelLists.add("等待入库,1");
            levelLists.add("出库,1");
            levelLists.add("空运中,0");
            levelLists.add("清关,0");
            levelLists.add("收货,0");
        } else if (status == 4)
        {
            levelLists.add("等待入库,1");
            levelLists.add("出库,1");
            levelLists.add("空运中,1");
            levelLists.add("清关,0");
            levelLists.add("收货,0");
        } else if (status >= 5 && status <= 7)
        {
            levelLists.add("等待入库,1");
            levelLists.add("出库,1");
            levelLists.add("空运中,1");
            levelLists.add("清关,1");
            levelLists.add("收货,0");
        } else if (status == 8 && status == 9)
        {
            levelLists.add("等待入库,1");
            levelLists.add("出库,1");
            levelLists.add("空运中,1");
            levelLists.add("清关,1");
            levelLists.add("收货,1");
        }
        return levelLists;
    }

    /**
     * 主页查询包裹编号转换状态
     * 
     * @param val
     * @return
     */
    private String getPkgStatus(int val)
    {
        switch (val)
        {
        case -1:
            return "异常";

        case 0:
            return "待入库";
        case 1:
            return "已入库";

        case 2:
            return "待发货";
        case 3:
            return "已出库";

        case 4:
            return "空运中";

        case 5:
            return "待清关";
        case 6:
            return "清关中";
        case 7:
            return "已清关";

        case 8:
            return "派件中";
        case 9:
            return "已收货";

        case 20:
            return "废弃";
        case 21:
            return "退货";
        case 22:
            return "废弃（分箱、合箱之后原包裹被废弃）";
        default:
            return "错误";
        }
    }

    /**
     * 头部公用信息
     * 
     * @param request
     * @param navClass
     *            目标界面/跳转界面
     */
    private void setHeadRequestAttribute(HttpServletRequest request,
            String navClass)
    {
        // 公告信息
        request.setAttribute("noticeLists", noticeService.queryAllHomepage());

        // 导航信息
        Navigation navigation = new Navigation();
        navigation.setParent_id(0);
        request.setAttribute("navigationParents",
                navigationService.queryAllHomepage(navigation));
        navigation.setParent_id(1);
        request.setAttribute("navigationChilds",
                navigationService.queryAllHomepage(navigation));
        request.setAttribute("navClass", navClass);
    }

    /**
     * 底部公用信息
     * 
     * @param request
     */
    private void setFooterRequestAttribute(HttpServletRequest request)
    {
        // 底部链接数据
        request.setAttribute("friendlyLinkLists",
                friendlyLinkService.queryAllHomepage());
    }

    /**
     * 获取重量查询的会员信息的json段代码
     * 
     * @param freightCosts
     *            会员集合
     * @param unitVal
     *            重量/已经转换成为..x*500b
     * @return
     */
    private String getWeightLevel(List<FreightCost> freightCosts, float unitVal)
    {
        StringBuffer sb = new StringBuffer();

        sb.append("level:[");
        for (FreightCost levelVal : freightCosts)
        {
            // 会员名称
            String title = levelVal.getMemberRate().getRate_name();
            // 单价
            float price = levelVal.getUnit_price() * unitVal;
            sb.append("{title:'" + title + "',");
            sb.append("price:'" + price + "'},");
        }
        sb.append("]");
        return sb.toString();
    }
    
    /**
     * 海外地址拼接
     * 
     * @param osa
     * @return String
     */
    private String joinOverseasAddress(OverseasAddress osa)
    {
        String osaddr = osa.getAddress_first()
                + osa.getCity() + osa.getState() + osa.getCounty()
                + osa.getCountry();

        return osaddr;
    };

    /**
     * 解析省市县
     * 
     * @param request
     * @param jsonString
     * @return
     */
    private String addressInfo(String jsonString)
    {
        if (StringUtil.isEmpty(jsonString))
        {
            return "";
        }
        List<String> aList = new ArrayList<String>();
        try
        {
            aList = JSONUtil.readValueFromJson(jsonString, "name");
        } catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
        StringBuffer address = new StringBuffer("");
        for (String city : aList)
        {
            address.append(city);
        }

        return address.toString().replaceAll("\"", "");
    }

}
