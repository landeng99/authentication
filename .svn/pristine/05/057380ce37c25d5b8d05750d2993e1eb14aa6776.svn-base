package com.xiangrui.lmp.business.admin.pallet.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiangrui.lmp.annotation.SystemServiceLog;
import com.xiangrui.lmp.business.admin.pallet.mapper.PalletMapper;
import com.xiangrui.lmp.business.admin.pallet.service.PalletService;
import com.xiangrui.lmp.business.admin.pallet.vo.Pallet;
import com.xiangrui.lmp.business.admin.pkg.mapper.PackageMapper;
import com.xiangrui.lmp.business.admin.pkg.mapper.UserAddressMapper;
import com.xiangrui.lmp.business.admin.pkg.vo.Pkg;
import com.xiangrui.lmp.business.admin.pkg.vo.UserAddress;
import com.xiangrui.lmp.business.admin.seaport.mapper.SeaportMapper;
import com.xiangrui.lmp.business.admin.seaport.vo.Seaport;
import com.xiangrui.lmp.business.admin.user.vo.User;
import com.xiangrui.lmp.business.base.Region;
import com.xiangrui.lmp.util.JSONTool;
import com.xiangrui.lmp.util.PackageLogUtil;
import com.xiangrui.lmp.util.PageView;

/**
 * @author Administrator
 * 
 */
@Service(value = "palletService")
public class PalletServiceImpl implements PalletService
{

    @Autowired
    private PalletMapper palletMapper;

    @Autowired
    private UserAddressMapper userAddressMapper;

    @Autowired
    private SeaportMapper seaportMapper;

    @Autowired
    private PackageMapper packageMapper;

    @Override
    public List<Pallet> queryAll(Pallet pallet)
    {

        return palletMapper.queryAll(pallet);
    }

    @SystemServiceLog(description = "创建包裹托盘信息")
    @Override
    public int insert(Pallet pallet)
    {
        int count = palletMapper.insertPallet(pallet);
        return count;
    }

    @Override
    public boolean isPkgInfoExist(Map<String, Object> params)
    {

        int count = palletMapper.isPkgExist(params);
        boolean isPkgExist = (count > 0) ? true : false;
        return isPkgExist;
    }

    @Override
    public boolean isPkgUserInfoExist(Map<String, Object> params,
            List<Pkg> pkgList)
    {
        Seaport temp = new Seaport();

        // 查询口岸信息
        int sid = Integer.valueOf((String) params.get("seaport_id"));
        temp.setSid(sid);

        List<Seaport> seaports = seaportMapper.querySeaport(temp);

        Seaport seaport = seaports.get(0);

        // 类型允许通过的数量
        int limit = seaport.getUserinfo_limit();

        // 查询包裹用户信息是否允许范围内
        boolean isaccept = validateUserInfoExist(params, limit, pkgList);
        return isaccept;
    }

    /**
     * 
     * 首先查询提单中所有包裹用户信息。 将当前待插入的包裹的用户信息与其进行比对。
     * 
     * @return
     */
    private synchronized boolean validateUserInfoExist(
            Map<String, Object> params, int limit, List<Pkg> pkgList)
    {
        List<UserAddress> userAddressChecks = new ArrayList<UserAddress>();

        for (Pkg pkg : pkgList)
        {

            Map<String, Object> pkgParam = new HashMap<String, Object>();

            pkgParam.put("package_id", pkg.getPackage_id());

            // 待插入包裹的地址信息
            List<UserAddress> pkgAddressList = userAddressMapper
                    .queryUserAddrByPkg(pkgParam);
            UserAddress userAddress = pkgAddressList.get(0);
            userAddressChecks.add(userAddress);
        }

        Map<String, Integer> idcardMap = new HashMap<String, Integer>();
        Map<String, Integer> streetMap = new HashMap<String, Integer>();
        Map<String, Integer> mobileMap = new HashMap<String, Integer>();
        for (UserAddress userAddress : userAddressChecks)
        {
            String idcard = userAddress.getIdcard();
            String mobile = userAddress.getMobile();
            String street = userAddress.getStreet();
            if (idcardMap.containsKey(idcard))
            {
                int idcardCnt = idcardMap.get(idcard) + 1;
                idcardMap.put(idcard, idcardCnt);
            } else
            {
                if (idcard != null && !"".equals(idcard.trim()))
                {
                    idcardMap.put(idcard, 1);
                }
            }

            if (mobileMap.containsKey(mobile))
            {
                int mobileCnt = mobileMap.get(mobile) + 1;
                mobileMap.put(mobile, mobileCnt);
            } else
            {
                if (mobile != null && !"".equals(mobile.trim()))
                {
                    mobileMap.put(mobile, 1);
                }
            }

            if (streetMap.containsKey(street))
            {
                int streetCnt = streetMap.get(street) + 1;
                streetMap.put(street, streetCnt);
            } else
            {
                if (street != null && !"".equals(street.trim()))
                {
                    streetMap.put(street, 1);
                }
            }
        }

        // 在查询数据库之前校验一下各个信息出现重复的情况。
//        if (isLimit(limit, streetMap))
//        {
//            return true;
//        }
//        if (isLimit(limit, mobileMap))
//        {
//            return true;
//        }
        if (isLimit(limit, idcardMap))
        {
            return true;
        }

        // 提单中包裹的地址信息
        List<UserAddress> pickOrderPkgAddressList = userAddressMapper
                .queryUserAddrByPickOrder(params);

        // 计算数据库及当前的数量总和
        for (UserAddress pickOrderPkgAddress : pickOrderPkgAddressList)
        {
            String idcard = pickOrderPkgAddress.getIdcard();
            String mobile = pickOrderPkgAddress.getMobile();
            String street = pickOrderPkgAddress.getStreet();
            if (idcardMap.containsKey(idcard))
            {
                int idcardCnt = idcardMap.get(idcard) + 1;
                idcardMap.put(idcard, idcardCnt);
            } else
            {
                if (idcard != null && !"".equals(idcard.trim()))
                {
                    idcardMap.put(idcard, 1);
                }
            }

            if (mobileMap.containsKey(mobile))
            {
                int mobileCnt = mobileMap.get(mobile) + 1;
                mobileMap.put(mobile, mobileCnt);
            } else
            {
                if (mobile != null && !"".equals(mobile.trim()))
                {
                    mobileMap.put(mobile, 1);
                }
            }

            if (streetMap.containsKey(street))
            {
                int streetCnt = streetMap.get(street) + 1;
                streetMap.put(street, streetCnt);
            } else
            {
                if (street != null && !"".equals(street.trim()))
                {
                    streetMap.put(street, 1);
                }
            }
        }

        // 再次校验一下各个信息出现重复的情况。
//        if (isLimit(limit, streetMap))
//        {
//            return true;
//        }
//        if (isLimit(limit, mobileMap))
//        {
//            return true;
//        }
        if (isLimit(limit, idcardMap))
        {
            return true;
        }
        return false;

    }

    private boolean isLimit(int limit, Map<String, Integer> map)
    {

        for (Entry<String, Integer> entry : map.entrySet())
        {

            int cnt = entry.getValue();
            if (cnt > limit)
            {
                return true;
            }
        }

        return false;
    }

    @SystemServiceLog(description = "删除托盘")
    @Override
    public int delPallet(Pallet pallet)
    {

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("pallet_id", pallet.getPallet_id());
        param.put("status", Pkg.LOGISTICS_SEND_WAITING);
        packageMapper.updateStatusByPalletId(param);
        // 删除托盘包裹
        palletMapper.delPalletPkg(pallet);
        int delPalletCnt = 0;

        // 删除托盘
        delPalletCnt = palletMapper.delPallet(pallet);
        return delPalletCnt;
    }

    @Override
    public Pallet queryPalletDetail(Pallet pallet, PageView pageView)
    {

        Pallet p = palletMapper.queryPalletDetail(pallet);

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("pallet_id", pallet.getPallet_id());
        param.put("pageView", pageView);
        List<Pkg> pkgs = packageMapper.queryPalletPkgsByParam(param);
        p.setPkgs(pkgs);

        for (Pkg pkg : pkgs)
        {
            List<Region> regionlist = (List<Region>) JSONTool.getDTOList(
                    pkg.getRegion(), Region.class);

            StringBuffer buffer = new StringBuffer("");

            // 拼接地址信息
            for (Region region : regionlist)
            {
                buffer.append(region.getName());
                buffer.append(",");
            }

            String street = pkg.getStreet();
            buffer.append(street == null ? "" : street);
            pkg.setAddress(buffer.toString());
        }
        return p;
    }

    @SystemServiceLog(description = "编辑托盘信息")
    @Override
    public boolean updatePallet(Pallet pallet)
    {
        int cnt = palletMapper.updatePallet(pallet);
        return (cnt > 0);
    }

    @Override
    public List<Pallet> queryAllByMap(Map<String, Object> params)
    {

        return palletMapper.queryAllByMap(params);
    }

    @Override
    public int insertPalletPkg(List<Map<String, Object>> pkgParams, User user)
    {
        // 将包裹状态更新为已出库
        List<Pkg> list = new ArrayList<Pkg>();
        int cnt = 0;
        for (Map<String, Object> pkgParam : pkgParams)
        {
            Pkg pkg = new Pkg();
            Object pkg_id = pkgParam.get("package_id");
            pkg.setStatus(Pkg.LOGISTICS_SENT_ALREADY);

            if (Integer.class.isAssignableFrom(pkg_id.getClass()))
            {
                pkg.setPackage_id((Integer) pkg_id);
            } else
            {
                pkg.setPackage_id(Integer.parseInt((String) pkg_id));
            }

            list.add(pkg);

            // 同步包裹日志--在本出的更新日志中数据库的包裹状态还没变动
            PackageLogUtil.log(pkg, user);
            cnt = palletMapper.insertPalletPkg(pkgParam);
        }

        // 更新包裹状态 (多条)
        packageMapper.batchUpdateStatus(list);
        return cnt;
    }

    @Override
    public int delPkg(Map<String, Object> pkgParam, User user)
    {
        // 将包裹状态更新为待出库
        List<Pkg> list = new ArrayList<Pkg>();
        Pkg pkg = new Pkg();
        Object pkg_id = pkgParam.get("package_id");
        pkg.setStatus(Pkg.LOGISTICS_SEND_WAITING);

        if (Integer.class.isAssignableFrom(pkg_id.getClass()))
        {
            pkg.setPackage_id((Integer) pkg_id);
        } else
        {
            pkg.setPackage_id(Integer.parseInt((String) pkg_id));
        }

        // 同步包裹日志--在本出的更新日志中数据库的包裹状态还没变动
        list.add(pkg);
        PackageLogUtil.log(pkg, user);

        // 更新包裹包裹为待出库状态
        packageMapper.updateStatusByPkg(pkg);

        return palletMapper.delPkg(pkgParam);
    }
    
    public Pallet queryPalletBySeaportIdAndOutputId(String seaport_id,String output_id)
    {
    	Pallet pallet=null;
    	Map<String, Object> pkgParam = new HashMap<String, Object>();
    	pkgParam.put("seaport_id", seaport_id);
    	pkgParam.put("output_id", output_id);
    	List<Pallet> list=palletMapper.queryPalletBySeaportIdAndOutputId(pkgParam);
    	if(list!=null&&list.size()>0)
    	{
    		pallet=list.get(0);
    	}
    	return pallet;
    }
}
