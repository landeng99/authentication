package com.xiangrui.lmp.business.homepage.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiangrui.lmp.business.admin.pkg.service.PackageService;
import com.xiangrui.lmp.business.base.BasePackage;
import com.xiangrui.lmp.business.base.Region;
import com.xiangrui.lmp.business.homepage.mapper.FrontPkgMapper;
import com.xiangrui.lmp.business.homepage.mapper.FrontUserAddressMapper;
import com.xiangrui.lmp.business.homepage.service.FrontCityService;
import com.xiangrui.lmp.business.homepage.service.FrontReceiveAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontUserAddressService;
import com.xiangrui.lmp.business.homepage.service.FrontUserService;
import com.xiangrui.lmp.business.homepage.service.PkgGoodsService;
import com.xiangrui.lmp.business.homepage.service.PkgService;
import com.xiangrui.lmp.business.homepage.vo.FrontCity;
import com.xiangrui.lmp.business.homepage.vo.FrontReceiveAddress;
import com.xiangrui.lmp.business.homepage.vo.FrontUser;
import com.xiangrui.lmp.business.homepage.vo.FrontUserAddress;
import com.xiangrui.lmp.business.homepage.vo.Pkg;
import com.xiangrui.lmp.business.homepage.vo.PkgGoods;
import com.xiangrui.lmp.init.SystemParamUtil;
import com.xiangrui.lmp.util.PageView;

@Controller
@RequestMapping("/useraddr")
public class UserAddressController
{
	/**
	 * LOG.
	 */
	private static final Logger logger = Logger.getLogger(HomepageController.class);

	@Autowired
	private FrontCityService frontCityService;

	@Autowired
	private FrontUserAddressService frontUserAddressService;

	@Autowired
	private FrontReceiveAddressService frontReceiveAddressService;

	@Autowired
	private PkgService frontPkgService;

	@Autowired
	private PackageService packageService;
	/**
	 * 包裹商品
	 */
	@Autowired
	private PkgGoodsService frontPkgGoodsService;

	/**
	 * 前台用户
	 */
	@Autowired
	private FrontUserService memberService;

	@Autowired
	private FrontUserAddressMapper frontUserAddressMapper;

	@Autowired
	private FrontPkgMapper frontPkgMapper;

	/**
	 * 身份证审核通过状态
	 */
	private static final int IDCARD_STATUS = 1;

	/**
	 * 包裹状态：可用
	 */
	private static final int PKG_ENABLE = 1;

	@RequestMapping("/listaddr")
	public String listAddr(HttpServletRequest request, String path, Model model)
	{

		model.addAttribute("path", path);
		return "/homepage/uploadPic";
	}

	/**
	 * 跳转到收货地址
	 * 
	 * @param request
	 */
	@RequestMapping("/destination")
	public String destination(HttpServletRequest request, HttpServletResponse response)
	{
		// 没有登录用户
		if (null == request.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}

		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);

		String pageIndexStr = request.getParameter("pageIndex");

		String userName = request.getParameter("q_userName");

		if (userName != null)
		{
			try
			{
				userName = new String(userName.getBytes("iso-8859-1"), "utf-8");
			} catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
		}

		String moblieNum = request.getParameter("q_moblieNum");

		PageView pageView = null;

		if (null != pageIndexStr && !"".equals(pageIndexStr))
		{
			int pageIndex = Integer.valueOf(pageIndexStr);
			pageView = new PageView(pageIndex);
		}
		else
		{
			pageView = new PageView(1);
		}

		Map<String, Object> params = new HashMap<String, Object>();
		pageView.setPageSize(10);
		params.put("pageView", pageView);

		params.put("user_id", frontUser.getUser_id());
		params.put("receiver", userName);
		params.put("mobile", moblieNum);
		// 获取用户邮寄地址
		// List<FrontReceiveAddress> userAddressList =
		// frontReceiveAddressService.queryReceiveAddress(params);
		List<FrontReceiveAddress> userAddressList = frontReceiveAddressService.queryReceiveAddressByUserIdAndMobileAndReceiver(params);
		for (FrontReceiveAddress addr : userAddressList)
		{
			String regionStr = addr.getRegion();
			try
			{
				List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);

				StringBuffer buffer = new StringBuffer();

				// 拼接地址信息
				for (Region region : regionlist)
				{
					buffer.append(region.getName());
					buffer.append("&nbsp;");
				}

				addr.setAddressStr(buffer.toString());
			} catch (Exception e)
			{

				e.printStackTrace();
			}
		}

		// 获取省的数据
		List<FrontCity> allCityList = getCity(0);

		JSONArray provinceArray = new JSONArray();

		JSONArray cityArray = new JSONArray();

		JSONArray areaArray = new JSONArray();

		for (FrontCity province : allCityList)
		{
			// 省
			JSONObject provinceObject = new JSONObject();
			provinceObject.put("city_id", province.getCity_id());
			provinceObject.put("father_id", province.getFather_id());
			provinceObject.put("city_name", province.getCity_name());
			provinceObject.put("postal_code", province.getPostal_code());
			provinceArray.add(provinceObject);
			// 获取市的数据
			List<FrontCity> cityList = getCity(province.getCity_id());

			for (FrontCity city : cityList)
			{
				// 市
				JSONObject cityObject = new JSONObject();
				cityObject.put("city_id", city.getCity_id());
				cityObject.put("father_id", city.getFather_id());
				cityObject.put("city_name", city.getCity_name());
				cityObject.put("postal_code", city.getPostal_code());
				cityArray.add(cityObject);

				// 获取区的数据
				List<FrontCity> areaList = getCity(city.getCity_id());

				for (FrontCity area : areaList)
				{
					// 区
					JSONObject areaObject = new JSONObject();
					areaObject.put("city_id", area.getCity_id());
					areaObject.put("father_id", area.getFather_id());
					areaObject.put("city_name", area.getCity_name());
					areaObject.put("postal_code", area.getPostal_code());
					areaArray.add(areaObject);
				}
			}
		}
		String provinceString = provinceArray.toString();
		String cityString = cityArray.toString();
		String areaString = areaArray.toString();

		if (null == userAddressList || userAddressList.size() == 0)
		{
			request.setAttribute("addressList", null);
		}
		else
		{
			request.setAttribute("addressList", userAddressList);
			request.setAttribute("pageView", pageView);
		}
		// 根据user_id找到用户各种包裹状态的数据
		int[] cAry = getTransportSum(frontUser.getUser_id());
		request.setAttribute("provinceString", provinceString);
		request.setAttribute("cityString", cityString);
		request.setAttribute("areaString", areaString);
		request.setAttribute("frontUser", frontUser);
		request.setAttribute("c1", cAry[0]);
		request.setAttribute("c2", cAry[1]);
		request.setAttribute("c3", cAry[2]);
		request.setAttribute("c4", cAry[3]);
		request.setAttribute("c5", cAry[4]);
		request.setAttribute("c6", cAry[5]);

		// 有新的待处理包裹
		Map<String, Object> tparams = new HashMap<String, Object>();
		tparams.put("user_id", frontUser.getUser_id());
		tparams.put("status", 0);
		tparams.put("enable", PKG_ENABLE);
		List<Pkg> needToHandlepkgList = frontPkgService.queryNeedToHandlePackage(tparams);
		int neetToHandleCount = 0;
		if (needToHandlepkgList != null && needToHandlepkgList.size() > 0)
		{
			neetToHandleCount = needToHandlepkgList.size();
			request.setAttribute("neetToHandleCount", neetToHandleCount);
		}
		return "/homepage/destination";
	}

	private List<FrontCity> getCity(int father_id)
	{

		List<FrontCity> cityList = (List<FrontCity>) SystemParamUtil.getData("frontCityList");

		List<FrontCity> targetList = new ArrayList<FrontCity>();
		for (FrontCity city : cityList)
		{

			if (city.getFather_id() == father_id)
			{
				targetList.add(city);
			}
		}

		return targetList;
	}

	@SuppressWarnings("rawtypes")
	private List getDTOList(String jsonString, Class clazz) throws Exception
	{
		JSONArray array = null;
		try
		{

			array = JSONArray.fromObject(jsonString);
			List list = new ArrayList();
			for (Iterator iter = array.iterator(); iter.hasNext();)
			{
				JSONObject jsonObject = (JSONObject) iter.next();
				list.add(JSONObject.toBean(jsonObject, clazz));
			}
			return list;

		} catch (Exception e)
		{

			logger.error("json字符串转换为json对象解析错误", e);
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * 编辑收货地址
	 * 
	 * @param request
	 */
	@RequestMapping("/addAddress")
	public String addAddress(HttpServletRequest request, FrontReceiveAddress frontReceiveAddress)
	{
		HttpSession session = request.getSession();
		FrontUser frontUser = (FrontUser) session.getAttribute("frontUser");
		String region = request.getParameter("region");
		String tel = request.getParameter("tel");
		String address_id = request.getParameter("address_id");
		String isdefault = request.getParameter("isdefault");

		frontReceiveAddress.setRegion(region);
		frontReceiveAddress.setTel(tel);
		frontReceiveAddress.setUser_id(frontUser.getUser_id());
		frontReceiveAddress.setIsdefault(isdefault);

		// 上传后处理
		String ctxPath = request.getSession().getServletContext().getRealPath("/") + File.separator + "resource";
		String img_path = null;

		String newFilename = "";

		img_path = "/" + "upload" + "/" + "idcard" + "/";
		try
		{
			String id_card_face = frontReceiveAddress.getFront_img();
			String id_card_num = frontReceiveAddress.getIdcard();
			// 正面
			String subfix = StringUtils.substringAfterLast(id_card_face, ".");
			newFilename = id_card_num + "-1" + "." + subfix;
			// 将文件重命名拷贝
			String path = ctxPath + img_path + newFilename;
			File uploadFile = new File(path);
			File faceIdcardFile = new File(ctxPath + id_card_face);
			FileCopyUtils.copy(faceIdcardFile, uploadFile);
			faceIdcardFile.delete();
			frontReceiveAddress.setFront_img(img_path + newFilename);

			// 反面
			String id_card_back = frontReceiveAddress.getBack_img();
			String backsubfix = StringUtils.substringAfterLast(id_card_back, ".");
			newFilename = id_card_num + "-2" + "." + backsubfix;
			// 将文件重命名拷贝
			String backpath = ctxPath + img_path + newFilename;
			File backuploadFile = new File(backpath);
			File backIdcardFile = new File(ctxPath + id_card_back);
			FileCopyUtils.copy(backIdcardFile, backuploadFile);
			backIdcardFile.delete();
			frontReceiveAddress.setBack_img(img_path + newFilename);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		if (null != address_id && !"-1".equals(address_id))
		{

			int addressId = Integer.parseInt(address_id);
/*			FrontReceiveAddress existFrontReceiveAddress = frontReceiveAddressService.queryReceiveAddressByAddressId(addressId);
			if (existFrontReceiveAddress != null)
			{
				String fronImg = StringUtils.trimToNull(existFrontReceiveAddress.getFront_img());
				String backImg = StringUtils.trimToNull(existFrontReceiveAddress.getBack_img());
				try
				{
					if (fronImg != null)
					{
						File faceIdcardFile = new File(ctxPath + fronImg);
						faceIdcardFile.delete();
					}
					if (backImg != null)
					{
						File backIdcardFile = new File(ctxPath + backImg);
						backIdcardFile.delete();
					}
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}*/
			frontReceiveAddress.setAddress_id(addressId);
			frontReceiveAddressService.updateReceiveAddress(frontReceiveAddress);
		}
		else
		{
			frontReceiveAddressService.insertFrontReceiveAddress(frontReceiveAddress);
		}
		//更新同时和同电话号码的其它地址的身份证信息
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobile", frontReceiveAddress.getMobile());
		params.put("receiver", frontReceiveAddress.getReceiver());
		params.put("idcard", frontReceiveAddress.getIdcard());
		//正面
		params.put("img_path", frontReceiveAddress.getFront_img());
		frontUserAddressService.updateUserFrontIdcardAndPhoto(params);
		frontReceiveAddressService.updateReceiveFrontIdcardAndPhoto(params);
		// 反面
		params.put("img_path", frontReceiveAddress.getBack_img());
		frontUserAddressService.updateUserBackIdcardAndPhoto(params);
		frontReceiveAddressService.updateReceiveBackIdcardAndPhoto(params);
		return "redirect:/useraddr/destination";
	}

	/**
	 * 删除收货地址
	 * 
	 * @param request
	 */
	@RequestMapping("/deleteAddress")
	public String deleteAddress(HttpServletRequest request, String addRessId)
	{
		System.out.println(addRessId);
		int addRess_id = Integer.parseInt(addRessId);
		frontReceiveAddressService.deleteReceiveAddress(addRess_id);
		;
		return "/homepage/destination";
	}
	
	@RequestMapping("/getAddressByUserId")
	@ResponseBody
	public List<FrontReceiveAddress> getAddressByUserId(int userId){
		Map<String, Object> paramsUser = new HashMap<String, Object>();
		paramsUser.put("user_id", userId);
		List<FrontReceiveAddress> userAddressList = frontReceiveAddressService.queryReceiveAddressByUserId(paramsUser);
		List<FrontReceiveAddress> userAddressNewList = new ArrayList<FrontReceiveAddress>();
		for (FrontReceiveAddress addr : userAddressList)
		{
			// 处理地址数据
			String regionStr = addr.getRegion();
			try
			{
				// json数据转换
				@SuppressWarnings("unchecked")
				List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);
				Collections.sort(regionlist);

				StringBuffer buffer = new StringBuffer();

				// 拼接地址信息
				for (Region region : regionlist)
				{
					buffer.append(region.getName());
					buffer.append(",");
				}
				String street = addr.getStreet();
				buffer.append(street);

				addr.setAddressStr(buffer.toString());

				userAddressNewList.add(addr);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		return userAddressNewList;
	}

	/**
	 * 跳转包裹修改收货地址
	 * 
	 * @param request
	 */
	@RequestMapping("/initupdateAddress")
	public String initupdateAddress(HttpServletRequest request, String package_id)
	{
		// 没有登录用户
		if (null == request.getSession().getAttribute("frontUser"))
		{
			return "/homepage/login";
		}

		HttpSession session = request.getSession();
		FrontUser frontUser = (FrontUser) session.getAttribute("frontUser");

		// 根据包裹ID查询address_id
		Pkg pkg = frontPkgService.queryPackageByPackageId(Integer.parseInt(package_id));

		// 根据address_id查询包裹地址
		FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(pkg.getAddress_id());
		if (frontUserAddress != null)
		{
			String regionStrUser = frontUserAddress.getRegion();
			try
			{
				List<Region> regionlistUser = (List<Region>) getDTOList(regionStrUser, Region.class);

				StringBuffer buffer = new StringBuffer();

				// 拼接包裹地址信息
				for (Region regionUser : regionlistUser)
				{
					buffer.append(regionUser.getName());
					buffer.append(",");
				}
				String street = frontUserAddress.getStreet();
				buffer.append(street);
				frontUserAddress.setAddressStr(buffer.toString());
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", frontUser.getUser_id());

		// 获取用户邮寄地址
		List<FrontReceiveAddress> userAddressList = frontReceiveAddressService.queryReceiveAddressByUserId(params);
		for (FrontReceiveAddress addr : userAddressList)
		{
			String regionStrReceive = addr.getRegion();
			try
			{
				List<Region> regionlist = (List<Region>) getDTOList(regionStrReceive, Region.class);

				StringBuffer buffer = new StringBuffer();

				// 拼接收货地址信息
				for (Region region : regionlist)
				{
					buffer.append(region.getName());
					buffer.append(",");
				}
				String street = addr.getStreet();
				buffer.append(street);
				addr.setAddressStr(buffer.toString());
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		// 获取省的数据
		List<FrontCity> allCityList = getCity(0);

		JSONArray provinceArray = new JSONArray();

		JSONArray cityArray = new JSONArray();

		JSONArray areaArray = new JSONArray();

		for (FrontCity province : allCityList)
		{
			// 省
			JSONObject provinceObject = new JSONObject();
			provinceObject.put("city_id", province.getCity_id());
			provinceObject.put("father_id", province.getFather_id());
			provinceObject.put("city_name", province.getCity_name());
			provinceObject.put("postal_code", province.getPostal_code());
			provinceArray.add(provinceObject);
			// 获取市的数据
			List<FrontCity> cityList = getCity(province.getCity_id());

			for (FrontCity city : cityList)
			{
				// 市
				JSONObject cityObject = new JSONObject();
				cityObject.put("city_id", city.getCity_id());
				cityObject.put("father_id", city.getFather_id());
				cityObject.put("city_name", city.getCity_name());
				cityObject.put("postal_code", city.getPostal_code());
				cityArray.add(cityObject);

				// 获取区的数据
				List<FrontCity> areaList = getCity(city.getCity_id());

				for (FrontCity area : areaList)
				{
					// 区
					JSONObject areaObject = new JSONObject();
					areaObject.put("city_id", area.getCity_id());
					areaObject.put("father_id", area.getFather_id());
					areaObject.put("city_name", area.getCity_name());
					areaObject.put("postal_code", area.getPostal_code());
					areaArray.add(areaObject);
				}
			}
		}
		String provinceString = provinceArray.toString();
		String cityString = cityArray.toString();
		String areaString = areaArray.toString();

		request.setAttribute("provinceString", provinceString);
		request.setAttribute("cityString", cityString);
		request.setAttribute("areaString", areaString);

		request.setAttribute("frontUserAddress", frontUserAddress);
		request.setAttribute("addressList", userAddressList);
		request.setAttribute("package_id", package_id);

		return "/homepage/transportUpdateAddress";
	}

	/**
	 * 包裹修改收货地址
	 * 
	 * @param request
	 *            新表 address_id
	 */
	@RequestMapping("/updateAddress")
	@ResponseBody
	public Map<String, String> updateAddress(HttpServletRequest request, String address_id, String package_id)
	{

		Map<String, String> result = new HashMap<String, String>();

		// 根据包裹ID查询address_id
		Pkg pkg = frontPkgService.queryPackageByPackageId(Integer.parseInt(package_id));

		// 根据address_id查询包裹地址
		FrontUserAddress frontUserAddress = frontUserAddressService.queryUseraddress(pkg.getAddress_id());
		boolean needInsert = false;
		if (frontUserAddress == null)
		{
			frontUserAddress = new FrontUserAddress();
			needInsert = true;
		}

		// 根据address_id查询用户收货地址
		FrontReceiveAddress frontReceiveAddress = frontReceiveAddressService.queryReceiveAddressByAddressId(Integer.parseInt(address_id));

		// 更新包裹地址
		frontUserAddress.setUser_id(frontReceiveAddress.getUser_id());
		frontUserAddress.setRegion(frontReceiveAddress.getRegion());
		frontUserAddress.setStreet(frontReceiveAddress.getStreet());
		frontUserAddress.setPostal_code(frontReceiveAddress.getPostal_code());
		frontUserAddress.setReceiver(frontReceiveAddress.getReceiver());
		frontUserAddress.setMobile(frontReceiveAddress.getMobile());
		frontUserAddress.setTel(frontReceiveAddress.getTel());
		frontUserAddress.setIsdefault(frontReceiveAddress.getIsdefault());
		frontUserAddress.setIdcard(frontReceiveAddress.getIdcard());
		frontUserAddress.setFront_img(frontReceiveAddress.getFront_img());
		frontUserAddress.setBack_img(frontReceiveAddress.getBack_img());
		frontUserAddress.setIdcard_status(frontReceiveAddress.getIdcard_status());
		frontUserAddress.setRefuse_reason(frontReceiveAddress.getRefuse_reason());

		if (needInsert)
		{
			frontUserAddressMapper.insertFrontUserAddress(frontUserAddress);
			pkg.setAddress_id(frontUserAddress.getAddress_id());
			frontPkgMapper.updatePkgForUseraddress(pkg);
		}
		else
		{
			frontUserAddressService.updateUserAddressByReceiveAddress(frontUserAddress);
		}
		String regionStr = frontUserAddress.getRegion();
		try
		{
			List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);

			StringBuffer buffer = new StringBuffer();

			// 拼接地址信息
			for (Region region : regionlist)
			{
				buffer.append(region.getName());
				buffer.append(",");
			}
			String street = frontUserAddress.getStreet();
			buffer.append(street);
			result.put("addressStr", buffer.toString());
			result.put("receiver", frontUserAddress.getReceiver());
			result.put("mobile", frontUserAddress.getMobile());
			result.put("package_id", package_id);
			// frontUserAddress.setAddressStr(buffer.toString());

			// 异常包裹没有选择待合箱服务时，编辑包裹时要让其在前台待处理栏上消失
			if ((!"Y".equals(StringUtils.trimToEmpty(pkg.getWaitting_merge_flag()))) && "Y".equals(StringUtils.trimToEmpty(pkg.getException_package_flag())))
			{
				// 包裹的商品
				List<PkgGoods> gooldList = frontPkgGoodsService.queryPkgGoodsById(pkg.getPackage_id());
				// 客户在编辑包裹时没有勾选需等待合箱，且包裹内件数大于等于1，且包裹有选择收货地址，点击完成，那么包裹从前台页面的“待处理包裹”下消失
				if (gooldList != null && gooldList.size() > 0)
				{
					packageService.updateExceptionPackageFlag(Integer.parseInt(package_id), "N");
				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		//更新同时和同电话号码的其它地址的身份证信息
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobile", frontUserAddress.getMobile());
		params.put("receiver", frontUserAddress.getReceiver());
		params.put("idcard", frontUserAddress.getIdcard());
		//正面
		params.put("img_path", frontUserAddress.getFront_img());
		frontUserAddressService.updateUserFrontIdcardAndPhoto(params);
		frontReceiveAddressService.updateReceiveFrontIdcardAndPhoto(params);
		// 反面
		params.put("img_path", frontUserAddress.getBack_img());
		frontUserAddressService.updateUserBackIdcardAndPhoto(params);
		frontReceiveAddressService.updateReceiveBackIdcardAndPhoto(params);
		return result;
	}

	/**
	 * 包裹修改原有的收货地址
	 * 
	 * @param request
	 */
	@RequestMapping("/updateCurrentAddress")
	@ResponseBody
	public Map<String, String> updateCurrentAddress(HttpServletRequest request, FrontUserAddress frontUserAddress, String package_id)
	{
		Map<String, String> result = new HashMap<String, String>();

		frontUserAddressService.updateUserAddress(frontUserAddress);
		String regionStr = frontUserAddress.getRegion();
		try
		{
			List<Region> regionlist = (List<Region>) getDTOList(regionStr, Region.class);

			StringBuffer buffer = new StringBuffer();

			// 拼接地址信息
			for (Region region : regionlist)
			{
				buffer.append(region.getName());
				buffer.append(",");
			}
			String street = frontUserAddress.getStreet();
			buffer.append(street);
			result.put("addressStr", buffer.toString());
			result.put("receiver", frontUserAddress.getReceiver());
			result.put("mobile", frontUserAddress.getMobile());
			result.put("package_id", package_id);
			result.put("idcard", frontUserAddress.getIdcard());
			// frontUserAddress.setAddressStr(buffer.toString());
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 根据user_id找到用户各种包裹状态的数据
	 * 
	 * @param user_id
	 * @return int[]数组,总长度为6,6个值,分别是待入库数,已入库数,未支付数,支付数,未打印数，打印数
	 */
	public int[] getTransportSum(int user_id)
	{
		int[] array = new int[] { 0, 0, 0, 0, 0, 0 };
		// 待入库状态
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", user_id);
		params.put("enable", PKG_ENABLE);
		params.put("status", BasePackage.LOGISTICS_STORE_WAITING);
		List<Pkg> pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[0] = pkgList.size();

		// 已入库状态
		params.put("status", BasePackage.LOGISTICS_STORAGED);
		pkgList = frontPkgService.queryPackageByUserIdAndStatus(params);
		array[1] = pkgList.size();

		params.remove("status");
		// 未支付状态
		params.put("payStatus", BasePackage.PAYMENT_UNPAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
		array[2] = pkgList.size();

		// 已支付状态
		params.put("payStatus", BasePackage.PAYMENT_PAID);
		pkgList = frontPkgService.queryPackageByUserIdAndPayStatus(params);
		array[3] = pkgList.size();

		params.remove("payStatus");
		// 未打印状态
		params.put("pkgPrint", BasePackage.PACKAGE__NO_PRINT);
		pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		array[4] = pkgList.size();

		// 打印状态
		params.put("pkgPrint", BasePackage.PACKAGE__YES_PRINT);
		pkgList = frontPkgService.queryPackageByUserIdAndPkgPrint(params);
		array[5] = pkgList.size();

		return array;
	}

	/**
	 * 通过姓名或者（和）电话查询收货地址
	 * 
	 * @param request
	 * @param package_id
	 * @return String
	 */
	@RequestMapping("/queryAddressByUserNameAndMobile")
	@ResponseBody
	public Map<String, Object> queryAddressByUserNameAndMobile(HttpServletRequest request, String userName, String moblieNum)
	{
		Map<String, Object> result = new HashMap<String, Object>();
		int user_id = ((FrontUser) request.getSession().getAttribute("frontUser")).getUser_id();

		FrontUser frontUser = memberService.queryFrontUserByUserId(user_id);
		String flag = "N";
		int address_id = -100;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("user_id", frontUser.getUser_id());
		params.put("receiver", userName);
		params.put("mobile", moblieNum);
		List<FrontReceiveAddress> userAddressList = frontReceiveAddressService.queryReceiveAddressByUserIdAndMobileAndReceiver(params);
		if (userAddressList != null && userAddressList.size() > 0)
		{
			address_id = userAddressList.get(0).getAddress_id();
			flag = "Y";
		}
		result.put("flag", flag);
		result.put("address_id", address_id);
		return result;
	}
}
